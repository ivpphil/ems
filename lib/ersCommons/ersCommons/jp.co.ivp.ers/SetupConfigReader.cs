﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Web.Configuration;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation.range_check;

namespace jp.co.ivp.ers.util
{
    public class SetupConfigReader
    {
		/// <summary>
        /// Get the pooled object of GetCommonConfigFileValueCache
		/// </summary>
        public static Configuration CommonConfiguration
        {
            get
            {
                var returnVal = (Configuration)ErsCommonContext.GetPooledObject("GetCommonConfigFileValueCache");
                if (returnVal == null)
                {
                    var ConfigurationFilePath = ErsCommonContext.MapPath("~/../setup/");
                    var fileMap =  new WebConfigurationFileMap();
                    var vDirMapBase = new VirtualDirectoryMapping(ConfigurationFilePath, true, "web.config");
                    fileMap.VirtualDirectories.Add("/virtual_common_config", vDirMapBase);

                    returnVal = System.Web.Configuration.WebConfigurationManager.OpenMappedWebConfiguration(fileMap, "/virtual_common_config", "virtual_common_config");
                    ErsCommonContext.SetPooledObject("GetCommonConfigFileValueCache", returnVal);
                }
                return returnVal;
            }
        }

        public static string CommonConfigGlobalizationCulture
        {
            get
            {
                try
                {
                    Configuration config = WebConfigurationManager.OpenWebConfiguration("/");

                    GlobalizationSection configSection =
                      (GlobalizationSection)config.GetSection("system.web/globalization");

                    return configSection.Culture;
                }
                catch (Exception)
                {
                    
                    return string.Empty;
                }

            }
        }

        /// <summary>
        /// Get the pooled object of GetCommonConfigFileValueCache
        /// </summary>
        public static Configuration SiteConfiguration
        {
            get
            {
                var returnVal = (Configuration)ErsCommonContext.GetPooledObject("GetSiteConfigFileValueCache");
                if (returnVal == null)
                {
                    if (ErsCommonContext.IsBatch)
                    {
                        //バッチからweb.configを読み込む
                        var ConfigurationFilePath = ErsCommonContext.MapPath("~/../setup/batch/");
                        var vDirMapBase = new VirtualDirectoryMapping(ConfigurationFilePath, true, "web.config");
                        var fileMap =  new WebConfigurationFileMap();
                        fileMap.VirtualDirectories.Add("/virtual_batch_config", vDirMapBase);

                        returnVal = System.Web.Configuration.WebConfigurationManager.OpenMappedWebConfiguration(fileMap, "/virtual_batch_config", "virtual_batch_config");
                    }
                    else
                    {
                        returnVal = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
                    }
                    ErsCommonContext.SetPooledObject("GetSiteConfigFileValueCache", returnVal);
                }
                return returnVal;
            }
        }

		/// <summary>
		/// Gets the configuration value using a specified key from common setting file.
		/// </summary>
        public static string GetCommonConfigFileValue(string key)
        {
            var element = CommonConfiguration.AppSettings.Settings[key];
            if (element == null)
            {
                return null;
            }
            else
            {
                return element.Value;
            }
        }

		/// <summary>
        /// Returns a collection of configuration keys from common setting file.
		/// </summary>
        public static IEnumerable<string> GetCommonConfigFileKeys()
        {
            foreach (string key in CommonConfiguration.AppSettings.Settings.AllKeys)
            {
                if (!key.Contains("ConnectionStrings"))
                    yield return key;
            }
        }

        /// <summary>
        /// Gets the configuration value using a specified key from site setting file.
        /// </summary>
        public static string GetSiteConfigFileValue(string key)
        {
            var element = SiteConfiguration.AppSettings.Settings[key];
            if (element == null)
            {
                return GetCommonConfigFileValue(key);
            }
            else
            {
                return element.Value;
            }
        }

        /// <summary>
        /// Returns a collection of configuration keys from site setting file.
        /// </summary>
        public static IEnumerable<string> GetSiteConfigFileKeys()
        {
            foreach (string key in SiteConfiguration.AppSettings.Settings.AllKeys)
            {
                if (!key.Contains("ConnectionStrings"))
                    yield return key;
            }
        }

        protected internal SetupConfigReader()
        {
        }

		/// <summary>
		/// Returns the configuration value of nor_url
		/// </summary>
        public virtual string nor_url
        {
            get
            {
                if (this.enableSSL == EnumRequireHttps.Required)
                {
                    return this.sec_url;
                }
                else
                {
                    return SiteTypeVariables.GetNorUrl(this.site_type, this.site_id);
                }
            }
        }

		/// <summary>
        /// Returns the configuration value of sec_url
        /// </summary>
        public virtual string sec_url
        {
            get
            {
                return SiteTypeVariables.GetSecUrl(this.site_type, this.site_id);
            }
        }

      
        /// <summary>
        /// Gets System setting of Postfix backup type using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual EnumSiteType site_type
        {
            get
            {
                return (EnumSiteType)Enum.Parse(typeof(EnumSiteType), (GetSiteConfigFileValue("site_type")));
            }
        }

        public virtual int site_id
        {
            get
            {
                return int.Parse(GetSiteConfigFileValue("site_id"));
            }
        }

        public string root_path
        {
            get
            {
                return GetCommonConfigFileValue("root_path");
            }
        }

        public string pc_nor_url
        {
            get
            {
                if (this.enableSSL == EnumRequireHttps.Required)
                {
                    return this.pc_sec_url;
                }
                else
                {
                    return GetCommonConfigFileValue("pc_nor_url");
                }
            }
        }

        public string pc_sec_url
        {
            get
            {
                return GetCommonConfigFileValue("pc_sec_url");
            }
        }

        public string mobile_nor_url
        {
            get
            {
                if (this.enableSSL == EnumRequireHttps.Required)
                {
                    return this.mobile_sec_url;
                }
                else
                {
                    return GetCommonConfigFileValue("mobile_nor_url");
                }
            }
        }

        public string mobile_sec_url
        {
            get
            {
                return GetCommonConfigFileValue("mobile_sec_url");
            }
        }

        public string smartphone_sec_url
        {
            get
            {
                return GetCommonConfigFileValue("smartphone_sec_url");
            }
        }

        public string smartphone_nor_url
        {
            get
            {
                if (this.enableSSL == EnumRequireHttps.Required)
                {
                    return this.smartphone_sec_url;
                }
                else
                {
                    return GetCommonConfigFileValue("smartphone_nor_url");
                }
            }
        }

        public string contact_nor_url
        {
            get
            {
                if (this.enableSSL == EnumRequireHttps.Required)
                {
                    return this.contact_sec_url;
                }
                else
                {
                    return GetCommonConfigFileValue("contact_nor_url");
                }
            }
        }

        public string contact_sec_url
        {
            get
            {
                return GetCommonConfigFileValue("contact_sec_url");
            }
        }


        public string emp_change_url
        {
            get
            {
                return GetCommonConfigFileValue("emp_change_url");
            }
        }
        public string admin_nor_url
        {
            get
            {
                if (this.enableSSL == EnumRequireHttps.Required)
                {
                    return this.admin_sec_url;
                }
                else
                {
                    return GetCommonConfigFileValue("admin_nor_url");
                }
            }
        }

        public string admin_sec_url
        {
            get
            {
                return GetCommonConfigFileValue("admin_sec_url");
            }
        }

        /// <summary>
        /// 許可HTMLタグ
		/// <para>HTML tags allowed</para>
        /// </summary>
        public virtual string[] AllowHtmlTag
        {
            get
            {
                if (_AllowHtmlTag == null)
                {
                    _AllowHtmlTag = GetCommonConfigFileValue("allowHtmlTag").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (var i = 0; i < _AllowHtmlTag.Length; i++)
                    {
                        _AllowHtmlTag[i] = _AllowHtmlTag[i].ToLower();
                    }
                }
                return _AllowHtmlTag;
            }
        }

        protected static string[] _AllowHtmlTag
        {
            get
            {
                return (string[])ErsCommonContext.GetPooledObject("_AllowHtmlTag");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_AllowHtmlTag", value);
            }
        }

        /// <summary>
        /// Ignore Route Downloaded File Extensions
        /// </summary>
        public virtual string ignore_file_extensions
        {
            get
            {
                return GetCommonConfigFileValue("ignore_file_extensions");
            }
        }

        /// <summary>
        /// デバッグ
		/// <para>Debug</para>
        /// </summary>
        public virtual bool debug
        {
            get
            {
                return bool.Parse(GetCommonConfigFileValue("debug"));
            }
        }

        //ログパス
		//Log Path
        public virtual string log_path
        {
            get
            {
                return GetCommonConfigFileValue("log_path");
            }
        }

        //ログパス
        //Log Path
        public virtual string site_log_path
        {
            get
            {
                var pathUri = new Uri(this.log_path);
                return new Uri(pathUri, GetSiteConfigFileValue("log_directory") + "\\").LocalPath;
            }
        }

        /// <summary>
        /// ログファイル書き込みユーザ
        /// </summary>
        public string logFileUserName
        {
            get
            {
                return GetCommonConfigFileValue("logFileUserName");
            }
        }

        /// <summary>
        /// ログファイル書き込みユーザ
        /// </summary>
        public string logFileUserPassword
        {
            get
            {
                return GetCommonConfigFileValue("logFileUserPassword");
            }
        }

        public virtual string ProhibitionChars
        {
            get
            {
                return GetCommonConfigFileValue("ProhibitionChars");
            }
        }

        public virtual string ProhibitionCharsReplace
        {
            get
            {
                return GetCommonConfigFileValue("ProhibitionCharsReplace");
            }
        }

        /// <summary>
        /// Get number of links at Page ejection.
        /// </summary>
        public int PagerLinkNumber
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("PagerLinkNumber"));
            }
        }

        /// <summary>
        /// Get number of item on a page 
        /// </summary>
        public int DefaultItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("DefaultItemNumberOnPage"));
            }
        }

        /// <summary>
        /// Get number of item on a zip search page 
        /// </summary>
        public int ZipSearchItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("ZipSearchItemNumberOnPage"));
            }
        }
		/// <summary>
		/// Get configuration value of MessageResource
		/// </summary>
        public string MessageResource
        {
            get
            {
                return GetCommonConfigFileValue("MessageResource");
            }
        }
		/// <summary>
		/// Get configuration value of FieldNameResource
		/// </summary>
        public string FieldNameResource
        {
            get
            {
                return GetCommonConfigFileValue("FieldNameResource");
            }
        }
		/// <summary>
        /// Get configuration value of ReturnUrlResource
		/// </summary>
        public string ReturnUrlResource
        {
            get
            {
                return GetCommonConfigFileValue("ReturnUrlResource");
            }
        }

        /// <summary>
		/// Get configuration value of enableEncryption
		/// </summary>

        public bool enableEncryption
        {
            get
            {
                return Convert.ToBoolean(GetCommonConfigFileValue("enableEncryption"));
            }
        }
		/// <summary>
		/// Get configuration value of enableSSL
		/// </summary>
        public virtual EnumRequireHttps enableSSL
        {
            get
            {
                bool returnVal;
                if (Boolean.TryParse(GetCommonConfigFileValue("enableSSL"), out returnVal))
                {
                    return returnVal ? EnumRequireHttps.valTrue : EnumRequireHttps.valFalse;
                }

                return EnumRequireHttps.Required; 
            }
        }
		/// <summary>
		/// Get configuration value of onVEX
		/// </summary>
        public virtual bool onVEX
        {
            get
            {
                return Convert.ToBoolean(GetCommonConfigFileValue("onVEX"));
            }
        }

		/// <summary>
		/// Get configuration value of ValueRangeCheckerClass
		/// </summary>
        public EnumTextValueRangeChecker DefaultTextValueRangeChecker
        {
            get
            {
                EnumTextValueRangeChecker resultValue;
                if(!Enum.TryParse(GetCommonConfigFileValue("DefaultTextValueRangeChecker"), out resultValue))
                {
                    return EnumTextValueRangeChecker.LengthValueRangeChecker;
                }
                return resultValue;
            }
        }

        /// <summary>
        /// Get configuration value of ValueRangeCheckerClass
        /// </summary>
        public virtual IValueRangeChecker GetValueRangeChecker(EnumTextValueRangeChecker? enumRangeChecker)
        {
            if (!enumRangeChecker.HasValue)
            {
                enumRangeChecker = this.DefaultTextValueRangeChecker;
            }

            switch (enumRangeChecker)
            {
                case EnumTextValueRangeChecker.ByteValueRangeChecker:
                    return new ByteValueRangeChecker();
                case EnumTextValueRangeChecker.LengthValueRangeChecker:
                    return new LengthValueRangeChecker();
            }

            return null;
        }

        /// <summary>
        /// Get byte limit for file upload 
        /// </summary>
        public int default_upload_limit_byte
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("default_upload_limit_byte"));
            }
        }

        /// <summary>
        /// Get byte limit for csv upload 
        /// </summary>
        public int csv_upload_limit_byte
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("csv_upload_limit_byte"));
            }
        }

        /// <summary>
        /// Get byte limit for image upload 
        /// </summary>
        public int image_upload_limit_byte
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("image_upload_limit_byte"));
            }
        }

        public string default_send_testmail
        {
            get
            {
                return GetCommonConfigFileValue("default_send_testmail");
            }
        }

        /// <summary>
        /// Editor Reader Encoding
        /// </summary>
        public string editor_encoding
        {
            get
            {
                return GetCommonConfigFileValue("editor_encoding");
            }
        }

        /// <summary>
        /// Throw Message in duplicated ransu
        /// </summary>
        public string duplicated_ransu_error_code
        {
            get
            {
                return GetCommonConfigFileValue("duplicated_ransu_error_code");
            }
        }

        public IEnumerable<string> reject_logging_field_names
        {
            get
            {
                var reject_logging_field_names = GetCommonConfigFileValue("reject_logging_field_names");
                if (reject_logging_field_names.HasValue())
                {
                    return reject_logging_field_names.Split(';');
                }
                return new List<string>();
            }
        }

        public bool enableSni
        {
            get
            {
                return Convert.ToBoolean(GetCommonConfigFileValue("enableSni"));
            }
        }

        public string pc_application_root
        {
            get
            {
                return GetCommonConfigFileValue("pc_application_root");
            }
        }

        public string mobile_application_root
        {
            get
            {
                return GetCommonConfigFileValue("mobile_application_root");
            }
        }

        public string contact_application_root
        {
            get
            {
                return GetCommonConfigFileValue("contact_application_root");
            }
        }

        public string admin_application_root
        {
            get
            {
                return GetCommonConfigFileValue("admin_application_root");
            }
        }

        public string smartphone_application_root
        {
            get
            {
                return GetCommonConfigFileValue("smartphone_application_root");
            }
        }

        public string[] notAllowedUpdatingTables
        {
            get
            {
                return GetCommonConfigFileValue("notAllowedUpdatingTables").Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public bool EnableMiniProfiler
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("EnableMiniProfiler"));
            }
        }

        public virtual string[] MiniProfilerAllowedIPs
        {
            get
            {
                return GetCommonConfigFileValue("MiniProfilerAllowedIPs").Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public bool Multiple_sites
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("multiple_sites"));
            }
        }

        public string multiple_pc_nor_url(int site_id)
        {
            if (this.enableSSL == EnumRequireHttps.Required)
            {
                return this.multiple_pc_sec_url(site_id);
            }
            else
            {
                return GetCommonConfigFileValue("pc_" + site_id.ToString() + "_nor_url");
            }
        }

        public string multiple_pc_sec_url(int site_id)
        {
            return GetCommonConfigFileValue("pc_" + site_id.ToString() + "_sec_url");
        }

        public string multiple_mobile_nor_url(int site_id)
        {
            if (this.enableSSL == EnumRequireHttps.Required)
            {
                return this.multiple_mobile_sec_url(site_id);
            }
            else
            {
                return GetCommonConfigFileValue("mobile_" + site_id.ToString() + "_nor_url");
            }
        }

        public string multiple_mobile_sec_url(int site_id)
        {
            return GetCommonConfigFileValue("mobile_" + site_id.ToString() + "_sec_url");
        }

        public string multiple_smartphone_sec_url(int site_id)
        {
            return GetCommonConfigFileValue("smartphone_" + site_id.ToString() + "_sec_url");
        }

        public string multiple_smartphone_nor_url(int site_id)
        {
            if (this.enableSSL == EnumRequireHttps.Required)
            {
                return this.multiple_smartphone_sec_url(site_id);
            }
            else
            {
                return GetCommonConfigFileValue("smartphone_" + site_id.ToString() + "_nor_url");
            }
        }

        public string multiple_contact_nor_url(int site_id)
        {
            if (this.enableSSL == EnumRequireHttps.Required)
            {
                return this.multiple_contact_sec_url(site_id);
            }
            else
            {
                return GetCommonConfigFileValue("contact_" + site_id.ToString() + "_nor_url");
            }
        }

        public string multiple_contact_sec_url(int site_id)
        {
            return GetCommonConfigFileValue("contact_" + site_id.ToString() + "_sec_url");
        }

        public string multiple_pc_application_root(int site_id)
        {
            return GetCommonConfigFileValue("pc_" + site_id.ToString() + "_application_root");
        }

        public string multiple_mobile_application_root(int site_id)
        {
            return GetCommonConfigFileValue("mobile_" + site_id.ToString() + "_application_root");
        }

        public string multiple_contact_application_root(int site_id)
        {
            return GetCommonConfigFileValue("contact_" + site_id.ToString() + "_application_root");
        }

        public string multiple_smartphone_application_root(int site_id)
        {
            return GetCommonConfigFileValue("smartphone_" + site_id.ToString() + "_application_root");
        }

        public string[] ersSiteId
        {
            get
            {
                return GetCommonConfigFileValue("ers_sites_id").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public virtual string cookieSessionPrefix
        {
            get
            {
                if (this.Multiple_sites)
                {
                    return GetCommonConfigFileValue("cookieSessionPrefix_" + this.site_id);
                }
                return GetCommonConfigFileValue("cookieSessionPrefix");
            }
        }

        /// <summary>
        /// Gets value for validation of non-number only. Forces at least one alphabet input.
        /// </summary>
        public virtual bool ValidationForNotNumberOnlyEnabled
        {
            get
            {
                return Convert.ToBoolean(GetCommonConfigFileValue("ValidationForNotNumberOnlyEnabled"));
            }
        }

        public string csvpath
        {
            get
            {
                return GetCommonConfigFileValue("csvpath");
            }
        }

        public string MdbConnectionStrings
        {
            get
            {
                return GetCommonConfigFileValue("MdbConnectionStrings");
            }
        }


        public string newSchedDays
        {
            get
            {
                return GetCommonConfigFileValue("newSchedDays");
            }
        }

        public string weekDays
        {
            get
            {
                return GetCommonConfigFileValue("weekDays");
            }
        }

        public string chrome_driver_path
        {
            get
            {
                return GetCommonConfigFileValue("chrome_driver_path");
            }
        }

        public string desknet_login_page
        {
            get
            {
                return GetCommonConfigFileValue("desknet_login_page");
            }
        }

        public string desknet_username_element
        {
            get
            {
                return GetCommonConfigFileValue("desknet_username_element");
            }
        }

        public string desknet_password_element
        {
            get
            {
                return GetCommonConfigFileValue("desknet_password_element");
            }
        }

        public string desknet_username
        {
            get
            {
                return GetCommonConfigFileValue("desknet_username");
            }
        }

        public string desknet_password
        {
            get
            {
                return GetCommonConfigFileValue("desknet_password");
            }
        }

        public string login_button
        {
            get
            {
                return GetCommonConfigFileValue("login_button");
            }
        }

        public string desknet_export_page
        {
            get
            {
                return GetCommonConfigFileValue("desknet_export_page");
            }
        }

        public string xpath_date_from
        {
            get
            {
                return GetCommonConfigFileValue("xpath_date_from");
            }
        }

        public string xpath_date_to
        {
            get
            {
                return GetCommonConfigFileValue("xpath_date_to");
            }
        }

        public string xpath_select_button
        {
            get
            {
                return GetCommonConfigFileValue("xpath_select_button");
            }
        }

        public string xpath_ivp_global_anchor_element
        {
            get
            {
                return GetCommonConfigFileValue("xpath_ivp_global_anchor_element");
            }
        }

        public string xpath_development_division_anchor_element
        {
            get
            {
                return GetCommonConfigFileValue("xpath_development_division_anchor_element");
            }
        }

        public string xpath_ers_team_anchor_element
        {
            get
            {
                return GetCommonConfigFileValue("xpath_ers_team_anchor_element");
            }
        }

        public string xpath_add_all_members
        {
            get
            {
                return GetCommonConfigFileValue("xpath_add_all_members");
            }
        }

        public string xpath_ok_button
        {
            get
            {
                return GetCommonConfigFileValue("xpath_ok_button");
            }
        }

        public string xpath_export_button
        {
            get
            {
                return GetCommonConfigFileValue("xpath_export_button");
            }
        }

        public string ems_sched_import_page
        {
            get
            {
                return GetCommonConfigFileValue("ems_sched_import_page");
            }
        }

        public string download_path_folder
        {
            get
            {
                return GetCommonConfigFileValue("download_path_folder");
            }
        }

        public string csv_file
        {
            get
            {
                return GetCommonConfigFileValue("csv_file");
            }
        }

        public string btn_import_sched
        {
            get
            {
                return GetCommonConfigFileValue("btn_import_sched");
            }
        }


        public int newSchedGetLength
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("newSchedGetLength"));
            }
        }

        public int thisWeekSchedGetLength
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("thisWeekSchedGetLength"));
            }
        }

        public string tampered_csv
        {
            get
            {
                return GetCommonConfigFileValue("tampered_csv");
            }
        }

        public string batch_success
        {
            get
            {
                return GetCommonConfigFileValue("batch_success");
            }
        }

        public string batch_executed
        {
            get
            {
                return GetCommonConfigFileValue("batch_executed");
            }
        }
        
        public string batch_error
        {
            get
            {
                return GetCommonConfigFileValue("batch_error");
            }
        }

        public string no_csv_file
        {
            get
            {
                return GetCommonConfigFileValue("no_csv_file");
            }
        }

        public string ivp_logo_img
        {
            get
            {
                return GetCommonConfigFileValue("ivp_logo_img");
            }
        }

        public string[] default_signatories
        {
            get
            {
                return GetCommonConfigFileValue("default_signatories").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public string[] irrelevant_fields_info
        {
            get
            {
                return GetCommonConfigFileValue("irrelevant_fields_info").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public long? maxEmployeeCount
        {
            get
            {
                return Convert.ToInt64(GetCommonConfigFileValue("maxEmployeeCount"));
            }
        }

        public string leave_carryover
        {
            get
            {
                return GetCommonConfigFileValue("leave_carryover");
            }
        }

        public int carryover
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("carryover"));
            }
        }

        public int sl_default
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("sl_default"));
            }
        }

        public int vl_default
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("vl_default"));
            }
        }

        public double sl_count_medcert
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("sl_count_medcert"));
            }
        }

        public int job_title_default
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("job_title_default"));
            }
        }
    }
}
