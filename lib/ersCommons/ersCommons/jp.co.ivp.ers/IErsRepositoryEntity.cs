﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers
{
    public interface IErsRepositoryEntity
        : IErsCollectable, IOverwritable
    {
        int? id { get; set; }

        Dictionary<string, object> GetPropertiesAsDictionary(ErsDB_parent table);
    }
}
