﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Collections;
using System.Reflection;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.db
{
    public interface ICriteria
    {
        void Add(CriterionBase criterion);

        /// <summary>
        /// LIMIT
        /// </summary>
        long? LIMIT { get; set; }

        /// <summary>
        /// OFFSET
        /// </summary>
        long? OFFSET { get; set; }
    }
}
