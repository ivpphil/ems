﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using jp.co.ivp.ers.util;
using System.IO;

namespace jp.co.ivp.ers.db
{
    public class ErsCopySerializer
        : IDisposable
    {
        NpgsqlCopyIn _objCopyIn;
        ErsConnection _connection;
        NpgsqlCopySerializer _objSerializer;
        bool IsStartCopyIn;
        bool HasEmptyHeader = false;

        public ErsCopySerializer(NpgsqlConnection conn)
        {
            _connection = ErsDB_parent.OpenConnection(conn);
            _objSerializer = new NpgsqlCopySerializer(conn);
        }

        public ErsCopySerializer(NpgsqlConnection conn, NpgsqlCopyIn objCopyIn)
            : this(conn)
        {
            _objCopyIn = objCopyIn;
        }

        public void SerializeClose()
        {
            if (_objSerializer.IsActive)
                _objSerializer.Close();
        }

        public void SerializeEndFlush()
        {
            SerializeEndRow();
            SerializeFlush();
        }

        public void SerializeEndRow()
        {
            _objSerializer.EndRow();
        }

        public void SerializeFlush()
        {
            _objSerializer.Flush();
        }

        public void AddValueByType(Type type, object value, bool IsConvert = true)
        {
            object concreteValue = IsConvert ? ErsBindModel.GetConcreteValue(type, value) : value;

            if (concreteValue != null)
            {
                if (type == typeof(String) || type == typeof(string))
                {
                    if (Convert.ToString(concreteValue).HasValue())
                        _objSerializer.AddString((string)concreteValue);
                    else
                        _objSerializer.AddNull();
                }
                else if (type == typeof(Int16))
                {
                    _objSerializer.AddInt32((short)concreteValue);
                }
                else if (type == typeof(Int32))
                {
                    _objSerializer.AddInt32((int)concreteValue);
                }
                else if (type == typeof(Int64) || type == typeof(long))
                {
                    _objSerializer.AddInt64((long)concreteValue);
                }
                else if (type.IsArray)
                {
                    _objSerializer.AddString("{" + String.Join(",", ConvertToArrayString(concreteValue)) + "}");
                }
                else if (type == typeof(DateTime))
                {
                    if ((DateTime)concreteValue == DateTime.MinValue)
                        _objSerializer.AddNull();
                    else
                        _objSerializer.AddDateTime((DateTime)concreteValue);
                }

                return;
            }

            _objSerializer.AddNull();
        }

        public static object GetConcreteValue(Type type, object value, bool IsConvert = true)
        {
            object concreteValue = IsConvert ? ErsBindModel.GetConcreteValue(type, value) : value;

            if (concreteValue != null)
            {
                if (type == typeof(String) || type == typeof(string))
                {
                    if (Convert.ToString(concreteValue).HasValue())
                        return (string)concreteValue;
                    else
                        return NpgsqlCopySerializer.DEFAULT_NULL;
                }
                else if (type.IsArray)
                {
                    return "{" + String.Join(",", ConvertToArrayString(concreteValue)) + "}";
                }
                else if (type == typeof(DateTime))
                {
                    if ((DateTime)concreteValue == DateTime.MinValue)
                        return NpgsqlCopySerializer.DEFAULT_NULL;
                }

                return concreteValue;
            }

            return NpgsqlCopySerializer.DEFAULT_NULL;
        }

        protected static string[] ConvertToArrayString(object objArray)
        {
            var list = new List<string>();

            foreach (var value in objArray as Array)
                list.Add(Convert.ToString(value));

            return list.ToArray();

        }

        public void StartCopyIn()
        {
            if (!IsStartCopyIn)
                _objCopyIn.Start();

            IsStartCopyIn = true;
        }

        public void EndCopyIn()
        {
            if (_objCopyIn.IsActive)
            {
                _objCopyIn.End();
                IsStartCopyIn = false;
            }
        }

        public Stream ToStream
        {
            get
            {
                return _objSerializer.ToStream;
            }
        }

        public void SetCopyInQueryCommand(string queryCommand)
        {
            _objCopyIn.NpgsqlCommand.CommandText = queryCommand;
        }

        public void SetCopyIn(NpgsqlCopyIn objCopyIn)
        {
            _objCopyIn = objCopyIn;
        }

        public void SetDelimeter(string delimiter)
        {
            _objSerializer.Delimiter = delimiter;
        }

        public void SetEmptyHeader()
        {
            if (!HasEmptyHeader)
            {
                _objSerializer.EndRow();
                HasEmptyHeader = true;
            }
        }

        public void Dispose()
        {
            IsStartCopyIn = false;
            _connection.Dispose();

        }
    }
}
