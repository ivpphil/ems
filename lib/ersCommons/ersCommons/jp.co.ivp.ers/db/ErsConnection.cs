﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace jp.co.ivp.ers.db
{
    public class ErsConnection
        : IDisposable
    {
        DbConnection objDb;
        DbTransaction transaction;
        bool OpenedConnection = false;
        bool BegunTransaction = false;

        public ErsConnection(DbConnection objDb)
        {
            this.objDb = objDb;
        }

        public void OpenConnection()
        {
            if (objDb.State == System.Data.ConnectionState.Closed)
            {
                this.objDb.Open();
                this.OpenedConnection = true;
            }
        }

        internal void BeginTransaction()
        {
            this.transaction = objDb.BeginTransaction();
            this.BegunTransaction = true;
        }

        public void Commit()
        {
            if (BegunTransaction && transaction != null)
            {
                transaction.Commit();
            }
        }

        public void Dispose()
        {
            if (objDb != null && objDb.State == System.Data.ConnectionState.Open)
            {
                //トランザクションを閉じる
				//Close the transaction
                if (BegunTransaction && transaction != null)
                {
                    transaction.Dispose();
                    this.BegunTransaction = false;
                }

                //コネクションを閉じる
				//Close the connection
                if (OpenedConnection)
                {
                    objDb.Close();
                    this.OpenedConnection = false;
                }
            }
        }


    }
}
