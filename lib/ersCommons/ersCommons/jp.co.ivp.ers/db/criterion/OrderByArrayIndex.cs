﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db
{
    public class OrderByArrayIndex<T>
        : OrderByBase
    {
        public T[] arrayValue { get; set; }

        public Criteria.OrderBy orderBy { get; set; }

        public OrderByArrayIndex(string field, T[] arrayValue, Criteria.OrderBy orderBy)
            : base(field)
        {
            this.arrayValue = arrayValue;
            this.orderBy = orderBy;
        }

        public override string GetStatement()
        {
            var placeHolder = ":" + field + this.DbParameterCount;
            if (typeof(T) == typeof(string))
            {
                placeHolder = "CAST(" + placeHolder + " AS character varying[])";
            }
            var orderString = (this.orderBy == Criteria.OrderBy.ORDER_BY_ASC) ? "ASC" : "DESC";
            return " idx(" + placeHolder + ", " + this.field + ") " + orderString + " ";
        }

        public override IEnumerable<System.Data.Common.DbParameter> GetParameter(ErsDatabase objDB)
        {
            if (this.arrayValue is ColumnName)
                yield break;

            yield return objDB.CreateParameter(this.field + this.DbParameterCount, this.arrayValue);
        }
    }
}
