﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// 複数のCriterionをOrで結合する。
	/// <para>Join in the multi-Criterion with OR</para>
    /// </summary>
    public class OR_JOIN_CRITERION
        : JOIN_CRITERION<CriterionBase>
    {
        public OR_JOIN_CRITERION(IEnumerable<CriterionBase> value) : base(string.Empty, value, " OR ") { }

        public override IEnumerable<CriterionBase> GetCriterionList(IEnumerable<CriterionBase> value)
        {
            return value;
        }
    }

    /// <summary>
    /// 複数のCriterionをANDで結合する。
	/// <para>Join in the multi-Criterion with AND</para>
    /// </summary>
    public class AND_JOIN_CRITERION
        : JOIN_CRITERION<CriterionBase>
    {
        public AND_JOIN_CRITERION(IEnumerable<CriterionBase> value) : base(string.Empty, value, " AND ") { }

        public override IEnumerable<CriterionBase> GetCriterionList(IEnumerable<CriterionBase> value)
        {
            return value;
        }
    }

    /// <summary>
    /// 複数のCriterionを指定文字列で結合する。
	/// <para>Join in the multi-Criterion with more than one string</para>
    /// </summary>
    public abstract class JOIN_CRITERION<T>
        : CriterionBase
    {
        protected string joinString;

        protected new IEnumerable<T> value;

        public JOIN_CRITERION(string field, IEnumerable<T> value, string joinString)
            : base(field, null)
        {
            this.joinString = joinString;
            this.value = value;
        }

        /// <summary>
        /// Criteriaからパラメータのリストを取得する
		/// <para>Gets the list of parameters from the Criteria</para>
        /// </summary>
        /// <param name="objDB"></param>
        /// <param name="criteria"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public override IEnumerable<DbParameter> GetParameter(ErsDatabase objDB)
        {
            foreach (var criterion in this.GetCriterionList(value))
            {
                foreach (var param in criterion.GetParameter(objDB))
                {
                    yield return param;
                }
            }
        }

        /// <summary>
        /// Criteriaからパラメータ付きのWhere句を取得する。
		/// <para>Get the Where clause with the parameters from the Criteria.</para>
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public override string GetWhere()
        {
            var retVal = string.Empty;
            foreach (var criterion in this.GetCriterionList(value))
            {
                var strWhere = criterion.GetWhere();
                if (!string.IsNullOrEmpty(strWhere))
                    retVal += joinString + strWhere;
            }

            if (string.IsNullOrEmpty(retVal))
                return string.Empty;
            else
                return "(" + retVal.Substring(joinString.Length) + ")";
        }

        public abstract IEnumerable<CriterionBase> GetCriterionList(IEnumerable<T> value);
    }
}
