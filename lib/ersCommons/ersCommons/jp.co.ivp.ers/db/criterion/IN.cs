﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// IN句・NOT IN句の値となるクラス
	/// <para>IN clause and NOT IN clause class</para>
    /// </summary>
    public class IN_ITEM
        : CriterionBase
    {
        public IN_ITEM(string field, object value) : base(field, value) { }

        public override string GetWhere()
        {
            if (value is ColumnName)
            {
                return " " + ((ColumnName)value).value + " ";
            }
            else
            {
                return " :" + field + this.DbParameterCount + " ";
            }
        }
    }

    /// <summary>
    /// IN句とNOT IN句の共通親クラス
	/// <para>IN clause and NOT IN clause common parent class</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class IN_Common<T>
        : JOIN_CRITERION<T>
    {
        protected List<IN_ITEM> inItem;

        public IN_Common(string field, IEnumerable<T> value)
            : base(field, value, ",")
        {
        }

        public override IEnumerable<CriterionBase> GetCriterionList(IEnumerable<T> value)
        {
            if (inItem == null)
            {
                inItem = new List<IN_ITEM>();
                foreach (var val in value)
                {
                    inItem.Add(new IN_ITEM(this.field, val));
                }
            }
            return inItem;
        }

    }

    /// <summary>
    /// IN句を作成する。
	/// <para>Creat an IN clause</para>
    /// </summary>
    public class IN<T>
        : IN_Common<T>
    {

        public IN(string field, IEnumerable<T> value)
            : base(field, value)
        {
        }

        public override string GetWhere()
        {
            return " " + field + " IN " + base.GetWhere() + "";
        }

    }

    /// <summary>
    /// NOT IN句を作成する。
	/// <para>Creat a NOT IN clause</para>
    /// </summary>
    public class NOT_IN<T>
        : IN_Common<T>
    {

        public NOT_IN(string field, IEnumerable<T> value)
            : base(field, value)
        {
        }

        public override string GetWhere()
        {
            return " " + field + " NOT IN " + base.GetWhere() + "";
        }

    }

}
