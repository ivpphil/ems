﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// OrderBy句用Criterionの親クラス
	/// <para>Criterion for the parent class of OrderBy clause</para>
    /// </summary>
    public abstract class OrderByBase
    {
        public string field { get; protected set; }

        protected int DbParameterCount { get; set; }
        
        public OrderByBase(string field)
        {
            this.field = field;
            this.DbParameterCount = Criteria.DbParameterCountInThread;
        }

        /// <summary>
        /// WHERE句を取得 
        /// </summary>
        /// <returns></returns>
        public abstract string GetStatement();

        /// <summary>
        /// Parameterを取得
        /// <para>Get the Parameter</para>
        /// </summary>
        /// <param name="objDB"></param>
        /// <returns></returns>
        public virtual IEnumerable<DbParameter> GetParameter(ErsDatabase objDB)
        {
            yield break;
        }
    }

    /// <summary>
    /// ORDER BY field ASCを生成
    /// </summary>
    public class ORDER_BY_ASC
        : OrderByBase
    {

        public ORDER_BY_ASC(string field) : base(field) { }

        public override string GetStatement()
        {
            return this.field + " ASC ";
        }
    }

    /// <summary>
    /// ORDER BY field DESCを生成
    /// </summary>
    public class ORDER_BY_DESC
        : OrderByBase
    {

        public ORDER_BY_DESC(string field) : base(field) { }

        public override string GetStatement()
        {
            return this.field + " DESC ";
        }
    }
}
