﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// Criterionの親クラス
    /// </summary>
    public abstract class CriterionBase
    {
        protected internal string field { get; set; }
        protected object _value { get; set; }
        protected object value 
        {
            get
            {
                return _value;
            }
            set
            {
                _value = this.ReplaceEscapes(value);
            }
        }

        protected int DbParameterCount { get; set; }
        public CriterionBase(string field, object value)
        {
            this.field = field;
            this.value = value;
            this.DbParameterCount = Criteria.DbParameterCountInThread;
        }

        /// <summary>
        /// WHERE句を取得
		/// <para>Get the Where clause</para>
        /// </summary>
        /// <returns></returns>
        public abstract string GetWhere();

        /// <summary>
        /// Parameterを取得
		/// <para>Get the Parameter</para>
        /// </summary>
        /// <param name="objDB"></param>
        /// <returns></returns>
        public virtual IEnumerable<DbParameter> GetParameter(ErsDatabase objDB)
        {
            if (this.value is ColumnName)
                yield break;

            yield return objDB.CreateParameter(this.field + this.DbParameterCount, this.value);
        }

        /// <summary>
        /// エスケープ文字などを置換する
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected virtual object ReplaceEscapes(object value)
        {
            return value;
        }
    }
}
