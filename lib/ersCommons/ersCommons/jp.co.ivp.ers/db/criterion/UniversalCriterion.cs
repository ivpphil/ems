﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// table.field1 = table.field2 といった条件を作成
    /// </summary>
    public class UniversalCriterion
        : CriterionBase
    {
        private Criteria criteria;
        private Dictionary<string, object> parameterDictionary = new Dictionary<string, object>();

        public UniversalCriterion(string statement) : base(statement, null) { }

        public UniversalCriterion(string statement, Dictionary<string, object> parameters)
            : base(statement, null)
        {
            if (parameters != null)
            {
                foreach (var entity in parameters)
                {
                    var fieldName = entity.Key;
                    var fieldValue = entity.Value;

                    var parameterName = fieldName;
                    var newParameterName = parameterName + "_" + this.DbParameterCount;

                    if (!statement.Contains(parameterName))
                        throw new Exception("this Statement have to contain place holder named '" + parameterName + "'." + Environment.NewLine + statement);

                    Regex regex = new Regex("(:" + parameterName + @")([^\d]|$)");
                    this.field = regex.Replace(this.field, "$1_" + this.DbParameterCount + "$2");

                    this.parameterDictionary.Add(newParameterName, fieldValue);
                }
            }
        }

        /// <summary>
        /// Instantiate with criteria
        /// </summary>
        /// <param name="statement"></param>
        /// <param name="criteria"></param>
        public UniversalCriterion(string statement, Criteria criteria)
            : base(statement, null)
        {
            this.criteria = criteria;
        }

        public override IEnumerable<DbParameter> GetParameter(ErsDatabase objDB)
        {
            if(criteria != null)
            {
                // Get Parameter from criteria
                foreach (var parameter in criteria.GetParameter(objDB))
                {
                    yield return parameter;
                }
            }
            else
            {
                // Get parameter from specified dictionary
                foreach (var parameter in parameterDictionary)
                {
                    yield return objDB.CreateParameter(parameter.Key, this.ReplaceEscapes(parameter.Value));
                }
            }
        }

        public override string GetWhere()
        {
            return field;
        }
    }
}
