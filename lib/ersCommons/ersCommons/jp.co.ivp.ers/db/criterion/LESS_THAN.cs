﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// field < value を生成
    /// </summary>
    public class LESS_THAN
        : CriterionBase
    {

        public LESS_THAN(string field, object value) : base(field, value) { }

        public override string GetWhere()
        {
            if (value is ColumnName)
            {
                return " " + field + " < " + ((ColumnName)value).value;
            }
            else
            {
                return " " + field + " < :" + field + this.DbParameterCount;
            }
        }
    }

    /// <summary>
    /// field <= value を生成
    /// </summary>
    public class LESS_EQUAL
        : CriterionBase
    {

        public LESS_EQUAL(string field, object value) : base(field, value) { }

        public override string GetWhere()
        {
            if (value is ColumnName)
            {
                return " " + field + " <= " + ((ColumnName)value).value;
            }
            else
            {
                return " " + field + " <= :" + field + this.DbParameterCount;
            }
        }
    }
}
