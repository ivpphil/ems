﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// (value BETWEEN fromFieldName AND toFieldName) を生成
    /// </summary>
    public class BETWEEN
        : CriterionBase
    {

        private object _fromValue;
        private object fromValue
        {
            get
            {
                return _fromValue;
            }
            set
            {
                _fromValue = this.ReplaceEscapes(value); ;
            }
        }

        private object _toValue;
        private object toValue
        {
            get
            {
                return _toValue;
            }
            set
            {
                _toValue = this.ReplaceEscapes(value);
            }
        }

        public BETWEEN(object fromValue, object toValue, object value)
            : base("between", value)
        {
            this.fromValue = fromValue;
            this.toValue = toValue;
        }

		/// <summary>
		/// Returns the where condition
		/// </summary>
        public override string GetWhere()
        {
            var strRet = " (";
            strRet += CreateField(value, string.Empty);

            strRet += " BETWEEN ";

            strRet += CreateField(fromValue, "_from");

            strRet += " AND ";

            strRet += CreateField(toValue, "_to");

            return strRet + ")";
        }

        public override IEnumerable<System.Data.Common.DbParameter> GetParameter(ErsDatabase objDB)
        {
            if (!(value is ColumnName))
                yield return objDB.CreateParameter(this.field + this.DbParameterCount, this.value);

            if (!(fromValue is ColumnName))
                yield return objDB.CreateParameter(this.field + this.DbParameterCount + "_from", this.fromValue);

            if (!(toValue is ColumnName))
                yield return objDB.CreateParameter(this.field + this.DbParameterCount + "_to", this.toValue);
        }

        private string CreateField(object value, string postfix)
        {
            if (value is ColumnName)
            {
                return ((ColumnName)value).value;
            }
            else
            {
                return ":" + field + this.DbParameterCount + postfix;
            }
        }
    }
}
