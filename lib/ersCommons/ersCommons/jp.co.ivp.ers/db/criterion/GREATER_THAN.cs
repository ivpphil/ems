﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// field > value を生成
    /// </summary>
    public class GREATER_THAN
        : CriterionBase
    {

        public GREATER_THAN(string field, object value) : base(field, value) { }

        public override string GetWhere()
        {
            if (value is ColumnName)
            {
                return " " + field + " > " + ((ColumnName)value).value;
            }
            else
            {
                return " " + field + " > :" + field + this.DbParameterCount;
            }
        }
    }

    /// <summary>
    /// field >= value を生成
    /// </summary>
    public class GREATER_EQUAL
        : CriterionBase
    {

        public GREATER_EQUAL(string field, object value) : base(field, value) { }

        public override string GetWhere()
        {
            if (value is ColumnName)
            {
                return " " + field + " >= " + ((ColumnName)value).value;
            }
            else
            {
                return " " + field + " >= :" + field + this.DbParameterCount;
            }
        }
    }

}
