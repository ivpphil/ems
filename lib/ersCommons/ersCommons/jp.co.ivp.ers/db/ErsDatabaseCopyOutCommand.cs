﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Data.Common;
using System.IO;
using Microsoft.VisualBasic.FileIO;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.db
{
    public class ErsDatabaseCopyOut
    {
        static ErsConnection ersConnection;
        static NpgsqlConnection ersDBConnection;

        public static void ExecuteCommand(string copyOutQuery, Stream streamer)
        {
            ExecuteSetupConnection();

            NpgsqlCommand command = new NpgsqlCommand(copyOutQuery, ersDBConnection);
            NpgsqlCopyOut cmdCopyOut = new NpgsqlCopyOut(command, ersDBConnection, streamer);

            //Streamer automatically Closed after after Start() process
            cmdCopyOut.Start();
        }

        public static Stream GetCopyStream(string copyOutQuery)
        {
            ExecuteSetupConnection();

            NpgsqlCommand command = new NpgsqlCommand(copyOutQuery, ersDBConnection);
            NpgsqlCopyOut cmdCopyOut = new NpgsqlCopyOut(command, ersDBConnection);
            cmdCopyOut.Start();

            return cmdCopyOut.CopyStream;
        }

        public static Stream GetStreamWriter(string filePath, FileMode fileMode, FileAccess fileAccess, FileShare fileShare = FileShare.Read)
        {
            return new FileStream(filePath, fileMode, FileAccess.ReadWrite, fileShare);
        }

        public static List<string[]> GetRowListStream(Stream stream, string[] delimeters, Encoding encoding = null)
        {
            if (encoding == null)
                encoding = ErsEncoding.ShiftJIS;

            //Using MemoryStream to avoid auto disposed when reading last line
            var memoryStream = new MemoryStream();
            stream.CopyTo(memoryStream);
            memoryStream.Position = 0;

            var parser = new TextFieldParser(memoryStream, encoding) { Delimiters = delimeters };
            var list = new List<string[]>();

            using (parser)
            {
                while (!parser.EndOfData)
                {
                    list.Add(parser.ReadFields());
                }
            }

            return list;
        }

        private static void ExecuteSetupConnection(ErsDatabase objDB = null)
        {
            if (objDB == null)
                objDB = ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase();

            CreateConnection(objDB);
            OpenConnection();
        }

        public static void CreateConnection(DbConnection conn)
        {
            ersDBConnection = new NpgsqlConnection(conn.ConnectionString);

            ersConnection = new ErsConnection(ersDBConnection);
        }

        public static void OpenConnection()
        {
            ersConnection.OpenConnection();
        }
    }
}
