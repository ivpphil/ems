﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StackExchange.Profiling.Data;
using System.Data;
using System.Data.Common;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// DBアクセスをhookしてSQLのログを出力するクラス
    /// The class that hooks db access and outputs sql logs
    /// </summary>
    public class TraceDbProfiler : IDbProfiler
    {

        public TraceDbProfiler(ErsDatabase objDb)
        {
            this.objDb = objDb;
        }

        private ErsDatabase objDb { get; set; }

        public void ExecuteFinish(IDbCommand profiledDbCommand, ExecuteType executeType, DbDataReader reader)
        {
            ErsDebug.WriteSQL(profiledDbCommand);
        }

        public void ExecuteStart(IDbCommand profiledDbCommand, ExecuteType executeType)
        {

        }

        public bool IsActive
        {
            get { return true; }
        }

        public void OnError(IDbCommand profiledDbCommand, ExecuteType executeType, Exception exception)
        {
            this.objDb.HandleException(exception, profiledDbCommand);
        }

        public void ReaderFinish(IDataReader reader)
        {

        }
    }
}
