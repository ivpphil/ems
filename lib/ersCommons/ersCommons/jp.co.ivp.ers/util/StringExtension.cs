﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers
{
    public static class StringExtension
    {
        /// <summary>
        /// Check if this string has value (Extension)
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static bool HasValue(this string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// 指定した文字列の書式項目を、指定した配列内の対応するオブジェクトの文字列形式に置換します。
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Format(string format, IDictionary<string, object> args)
        {
            var sb = new StringBuilder(format);

            // 括弧をエスケープ
            sb.Replace("{{", "{{_");
            sb.Replace("}}", "_}}");

            // string.Format に渡すフォーマット作成
            // ついでに string.Format に渡す配列も作成
            var values = args.Select((pair, index) =>
            {
                sb.Replace("{" + pair.Key + "}", "{" + index + "}");
                return pair.Value;
            }).ToArray();

            // エスケープしたものを戻す
            sb.Replace("{{_", "{{");
            sb.Replace("_}}", "}}");

            // 整形は string.Format にまかせる
            return string.Format(sb.ToString(), values);
        }

        /// <summary>
        /// Return Without White Space
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string RemoveWhiteSpace(this string text)
        {
            return Regex.Replace(text, @"\s+", string.Empty);
        }
    }
}
