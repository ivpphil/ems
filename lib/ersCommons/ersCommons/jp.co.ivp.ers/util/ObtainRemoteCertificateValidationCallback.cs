﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace jp.co.ivp.ers.util
{
    /// <summary>
    /// 信頼できないSSL証明書を「問題なし」にする(デバッグ時のみ)
    /// </summary>
    public class ObtainRemoteCertificateValidationCallback
    {
        /// <summary>
        /// 信頼できないSSL証明書を「問題なし」にする(デバッグ時のみ)
        /// </summary>
        /// <param name="force">デバッグ時以外も強制的に問題なしにする。</param>
        /// <returns></returns>
        public virtual RemoteCertificateValidationCallback Obtain(bool force = false)
        {
            if (force || new SetupConfigReader().debug)
            {
                return new RemoteCertificateValidationCallback(this.OnRemoteCertificateValidationCallback);
            }
            else
            {
                //デバッグ以外ではエラーにする。
                //It is not possible to use this class in product environment because of security matter.
                return null;
            }
        }

        // 信頼できないSSL証明書を「問題なし」にするメソッド
        protected virtual bool OnRemoteCertificateValidationCallback(
          Object sender,
          X509Certificate certificate,
          X509Chain chain,
          SslPolicyErrors sslPolicyErrors)
        {
                return true;  // 「SSL証明書の使用は問題なし」と示す
        }
    }
}
