﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.util
{
    public static class ErsDateTime
    {
        private static TimeZoneInfo jstZoneInfo  = System.TimeZoneInfo.FindSystemTimeZoneById("Tokyo Standard Time");
        private static TimeZoneInfo localZoneInfo = TimeZoneInfo.Local;

        /// <summary>
        /// Convert to japanese time zone
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime ToJstDateTime(DateTime date)
        {
            return TimeZoneInfo.ConvertTime(date, localZoneInfo, jstZoneInfo);
        }

        /// <summary>
        /// Convert to local time zone
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime ToLocalDateTime(DateTime date)
        {
            return TimeZoneInfo.ConvertTime(date, jstZoneInfo, localZoneInfo);
        }
    }
}
