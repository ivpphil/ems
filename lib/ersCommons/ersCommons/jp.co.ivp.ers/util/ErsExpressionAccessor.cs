﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections.Concurrent;

namespace jp.co.ivp.ers.util
{
    /// <summary>
    /// Delegate accessor created by expression tree.
    /// Cache mechanism is contained.
    /// </summary>
    /// <typeparam name="TObject"></typeparam>
    /// <typeparam name="TProperty"></typeparam>
    public class ErsExpressionAccessor<TObject, TProperty>
    {
        private static ConcurrentDictionary<Tuple<Type, string>, Func<TObject, TProperty>> lambdaGetterCache
            = new ConcurrentDictionary<Tuple<Type, string>, Func<TObject, TProperty>>();

        private static ConcurrentDictionary<Tuple<Type, string>, Action<TObject, TProperty>> lambdaSetterCache
            = new ConcurrentDictionary<Tuple<Type, string>, Action<TObject, TProperty>>();

        /// <summary>
        /// Create and cache delegate, and get value for property
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static TProperty GetPropertyValue(TObject obj, string propertyName)
        {
            var type = typeof(TObject);

            var getterDelegate = lambdaGetterCache.GetOrAdd(Tuple.Create(type, propertyName), _ =>
                {
                    return CreateGetDelegate(propertyName);
                });

            return getterDelegate(obj);
        }

        /// <summary>
        /// Create typed getter delegate
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static Func<TObject, TProperty> CreateGetDelegate(string propertyName)
        {
            // (object target) => (object)((T)target).PropertyName
            var target = Expression.Parameter(typeof(TObject), "target");

            var lambda = Expression.Lambda<Func<TObject, TProperty>>(
                    Expression.Property(
                        target
                        , propertyName)
                    , target);

            return lambda.Compile();
        }


        /// <summary>
        /// Create and cache delegate, and set value to property
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <param name="setValue"></param>
        public static void SetPropertyValue(TObject obj, string propertyName, TProperty setValue)
        {
            var type = typeof(TObject);

            var setterDelegate = lambdaSetterCache.GetOrAdd(Tuple.Create(type, propertyName), _ =>
                {
                    return CreateSetDelegate(propertyName);
                });

            setterDelegate(obj, setValue);
        }

        /// <summary>
        /// Create typed setter delegate
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static Action<TObject, TProperty> CreateSetDelegate(string propertyName)
        {
            var target = Expression.Parameter(typeof(TObject), "target");
            var value = Expression.Parameter(typeof(TProperty), "value");

            var left = Expression.Property(target, propertyName);

            var lambda = Expression.Lambda<Action<TObject, TProperty>>(
                Expression.Assign(left, value),
                target, value);

            return lambda.Compile();
        }
    }
}
