﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections.Concurrent;

namespace jp.co.ivp.ers.util
{
    public class ErsExpressionInstantiator<TObject>
    {
        private static ConcurrentDictionary<Type, Func<TObject>> lambdaInstantiationCache = new ConcurrentDictionary<Type, Func<TObject>>();

        public static TObject GetInstantiation()
        {
            var type = typeof(TObject);

            Func<TObject> funcInstantiation;
            if (!lambdaInstantiationCache.TryGetValue(type, out funcInstantiation))
            {
                funcInstantiation = GetInstantiationDelegate(type);
                lambdaInstantiationCache[type] = funcInstantiation;
            }

            return funcInstantiation();
        }

        private static Func<TObject> GetInstantiationDelegate(Type type)
        {
            var newExp = Expression.New(type);
            var lamda = Expression.Lambda<Func<TObject>>(newExp);
            return lamda.Compile();
        }
    }
}
