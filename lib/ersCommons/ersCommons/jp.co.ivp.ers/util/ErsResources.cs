﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Reflection;
using System.ComponentModel;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.template;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers
{
    public class ErsResources
    {
        private ErsResources() { }

        /// <summary>
        /// フィールド名や単語のリソースを取得する
		/// <para>Get the resource field names and words</para>
        /// </summary>
        public static string GetFieldName(string key)
        {
            string retVal = null;
            var ers = (ErsState)ErsCommonContext.GetPooledObject("_sessionState");
            string culture = string.Empty;
            if (ers != null)
            {
                culture = ers.Get("culture");
            }

            if(string.IsNullOrEmpty(culture) || !FieldNameResourceDictionary.dicFieldNameResource.ContainsKey(culture))
            {
                culture = FieldNameResourceDictionary.defaultCulture;
            }
            
            if (FieldNameResourceDictionary.dicFieldNameResource[culture].ContainsKey(key))
            {
                retVal = FieldNameResourceDictionary.dicFieldNameResource[culture][key];
            }
            else if (key.Contains('.'))
            {
                //カンマを取り除いてフィールドだけを取得
                string newkey = key.Split('.')[1];
                if (FieldNameResourceDictionary.dicFieldNameResource[culture].ContainsKey(newkey))
                {
                    retVal = FieldNameResourceDictionary.dicFieldNameResource[culture][newkey];
                }
            }

            if (!string.IsNullOrEmpty(retVal))
            {
                return retVal;
            }

            return key;
        }

        /// <summary>
        /// メッセージのリソースを取得する
		/// <para>Gets the resource of the message</para>
        /// </summary>
        public static string GetMessage(string key, params object[] args)
        {
            var ers = (ErsState)ErsCommonContext.GetPooledObject("_sessionState");
            string culture = string.Empty;
            if (ers != null)
            {
                culture = ers.Get("culture");
            }

            if (string.IsNullOrEmpty(culture) || !MessageResourceDictionary.dicMessageResource.ContainsKey(culture))
            {
                culture = MessageResourceDictionary.defaultCulture;
            }
            
            if (!MessageResourceDictionary.dicMessageResource[culture].ContainsKey(key))
            {
                if(args != null && args.Length > 0)
                {
                    return key + "(" + string.Join(",", args) + ")";
                }
                else
                {
                    return key;
                }
            }

            string rtn = MessageResourceDictionary.dicMessageResource[culture][key];
            if (args != null && args.Length != 0)
            {
                rtn = string.Format(rtn, args);
            }

            return rtn.Replace("\\r\\n", Environment.NewLine);
        }

        public static string GetDisplayName(IEnumerable<Attribute> attributes)
        {
            foreach (var attr in attributes)
            {
                //Validator検索
                var attrDisplayName = attr as DisplayNameAttribute;
                if (attrDisplayName != null)
                    return attrDisplayName.DisplayName;
            }

            return string.Empty;
        }
    }
}
