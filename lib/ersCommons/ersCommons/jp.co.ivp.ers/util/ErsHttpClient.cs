﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace jp.co.ivp.ers.util
{
    public class ErsHttpClient
    {
        #region "Post"
        /// <summary>
        /// Send HTTP Request with POST method.
        /// this function can't send file.
        /// sends characters with utf-8
        /// </summary>
        /// <param name="url"></param>
        /// <param name="SendValues"></param>
        /// <returns></returns>
        public static IDictionary<string, object> Post(string url, Dictionary<string, object> SendValues, Encoding encoding = null)
        {
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }
            CookieCollection cookies = null;
            var result = PostSend(url, SendValues, encoding, ref cookies);
            return ErsCommon.GetDictionaryWithHttpString(result, "&", "=");
        }

        /// <summary>
        /// Send HTTP Request with POST method.
        /// this function can't send file.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="SendValues"></param>
        /// <param name="encording"></param>
        /// <returns></returns>
        public static string PostSend(string url, Dictionary<string, object> SendValues, Encoding encording, ref CookieCollection cookies, string id = null, string pass = null)
        {
            var postData = ErsCommon.HttpJoinDictionary(SendValues, "&", "=", encording);

            var objRequest = (HttpWebRequest)WebRequest.Create(url);
            objRequest.Method = "POST";
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.ContentLength = encording.GetByteCount(postData);

            //CookieContainerプロパティを設定する
            objRequest.CookieContainer = new System.Net.CookieContainer();
            //要求元のURIに関連したCookieを追加し、要求に使用する
            if (cookies == null)
            {
                cookies = objRequest.CookieContainer.GetCookies(objRequest.RequestUri);
            }
            objRequest.CookieContainer.Add(objRequest.RequestUri, cookies);

            //認証を追加
            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(pass))
            {
                objRequest.Credentials = new NetworkCredential(id, pass);
            }

            using (StreamWriter myWriter = new StreamWriter(objRequest.GetRequestStream()))
            {
                myWriter.Write(postData);
            }
            string result;
            using (var response = objRequest.GetResponse())
            {
                cookies = objRequest.CookieContainer.GetCookies(objRequest.RequestUri);

                using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), encording))
                {
                    result = sr.ReadToEnd();
                }
            }
            return result;
        }
        #endregion

        #region "Post with file"
        /// <summary>
        /// Send HTTP Request with POST method.
        /// If you want to sent a file, convert the file to byte array  and put it to the SendValues as value.
        /// sends characters with utf-8
        /// </summary>
        /// <param name="url"></param>
        /// <param name="SendValues"></param>
        /// <returns></returns>
        public static IDictionary<string, object> PostWithFile(string url, Dictionary<string, object> SendValues)
        {
            var result = PostWithFileSend(url, SendValues, Encoding.UTF8);
            return ErsCommon.GetDictionaryWithHttpString(result, "&", "=");
        }

        /// <summary>
        /// Send HTTP Request with POST method.
        /// If you want to sent a file, convert the file to byte array  and put it to the SendValues as value.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="SendValues"></param>
        /// <param name="encording"></param>
        /// <returns></returns>
        public static string PostWithFileSend(string url, Dictionary<string, object> SendValues, Encoding encording)
        {
            var objRequest = (HttpWebRequest)WebRequest.Create(url);
            objRequest.Method = "POST";

            string boundary = System.Environment.TickCount.ToString();
            objRequest.ContentType = "multipart/form-data; boundary=" + boundary;

            var postData = GetPostDataWithFile(SendValues, encording, boundary);
            var resulta = Encoding.UTF8.GetString(postData);
            objRequest.ContentLength = postData.Length;
            using (var reqStream = objRequest.GetRequestStream())
            {
                reqStream.Write(postData, 0, postData.Length);

                reqStream.Close();
            }
            string result;
            using (var response = objRequest.GetResponse())
            {
                result = GetHttpResponse(response);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SendValues"></param>
        /// <param name="encording"></param>
        /// <param name="boundary"></param>
        private static byte[] GetPostDataWithFile(Dictionary<string, object> SendValues, Encoding encording, string boundary)
        {
            var listByte = new List<byte>();
            foreach (var name in SendValues.Keys)
            {
                listByte.AddRange(encording.GetBytes("--" + boundary + "\r\n"));

                listByte.AddRange(GetContentDisposition(name, SendValues[name], encording));
            }

            listByte.AddRange(encording.GetBytes("--" + boundary + "--\r\n"));

            return listByte.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="p"></param>
        /// <param name="encording"></param>
        /// <returns></returns>
        private static IEnumerable<byte> GetContentDisposition(string name, object value, Encoding encording)
        {
            if (value is ErsHttpPostFile)
            {
                return GetFileContentDisposition(name, (ErsHttpPostFile)value, encording);
            }
            else
            {
                return GetCharacterContentDisposition(name, value, encording);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="postFileValue"></param>
        /// <param name="encording"></param>
        /// <returns></returns>
        private static IEnumerable<byte> GetFileContentDisposition(string name, ErsHttpPostFile postFileValue, Encoding encording)
        {
            var strContent = "Content-Disposition: form-data; name=\"" + name + "\"; filename=\"" + postFileValue.fileName + "\"\r\n" +
               "Content-Type: " + postFileValue.contentType + "\r\n" +
               "Content-Transfer-Encoding: binary\r\n\r\n";
            foreach (var byteValue in encording.GetBytes(strContent))
            {
                yield return byteValue;
            }
            foreach (var byteValue in postFileValue.fileValue)
            {
                yield return byteValue;
            }
            foreach (var byteValue in encording.GetBytes("\r\n"))
            {
                yield return byteValue;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="encording"></param>
        /// <returns></returns>
        protected static IEnumerable<byte> GetCharacterContentDisposition(string name, object value, Encoding encording)
        {
            var strContent = "Content-Disposition: form-data; name=\"" + name + "\"\r\n\r\n" +
                    value + "\r\n";
            return encording.GetBytes(strContent);
        }
        #endregion

        /// <summary>
        /// 不正なSSLを無視する。（できるだけ使用しない）
        /// </summary>
        public static void SetIgnoreInvalidSSL()
        {
            ServicePointManager.ServerCertificateValidationCallback = new ObtainRemoteCertificateValidationCallback().Obtain(true);
        }

        /// <summary>
        /// Read response from WebResponse instance
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public static string GetHttpResponse(WebResponse response)
        {
            string result;
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
                streamReader.Close();
            }
            return result;
        }

        /// <summary>
        /// HTTP(GET/Stream)
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="id">User</param>
        /// <param name="pass">Password</param>
        /// <returns></returns>
        public static Stream HttpGetStream(string url, string id = null, string pass = null)
        {
            var objRequest = (HttpWebRequest)WebRequest.Create(url);
            objRequest.Method = "GET";

            //This request will not be redirect to another page automatically.
            objRequest.AllowAutoRedirect = false;
            
            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(pass))
            {
                objRequest.Credentials = new NetworkCredential(id, pass);
            }

            return objRequest.GetResponse().GetResponseStream();
        }

    }
}
