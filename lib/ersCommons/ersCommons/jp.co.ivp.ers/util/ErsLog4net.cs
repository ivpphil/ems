﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.util
{
    public class ErsLog4net
    {
        /// <summary>
        /// log4netを初期化する
        /// </summary>
        protected internal static void InitLogger()
        {
            var configPath = ErsCommonContext.MapPath("~/log4net.config");
            InitLogger(configPath);
        }

        public static void InitLogger(string configPath)
        {
            var setup = new SetupConfigReader();
            var username = setup.logFileUserName;
            var password = setup.logFileUserPassword;
            var doChangeAuth = !string.IsNullOrEmpty(username);

            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(configPath));
            var hierarchy = log4net.LogManager.GetRepository() as log4net.Repository.Hierarchy.Hierarchy;
            string log_path = setup.site_log_path;
            if (!log_path.EndsWith(@"\"))
            {
                log_path += @"\";
            }

            //権限の偽装
            using (var changelogin = ChangeLogonUserHelper.BeginChange(username, password, doChangeAuth))
            {
                InitAppender(hierarchy, "exceptionLogger", "exceptionLog", log_path + "exception");
            }
        }

        /// <summary>
        /// FileAppenderのパスを設定ファイルから取得
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <param name="loggerName"></param>
        /// <param name="appenderName"></param>
        /// <param name="logfileprefix"></param>
        private static void InitAppender(log4net.Repository.Hierarchy.Hierarchy hierarchy, string loggerName, string appenderName, string log_path)
        {
            var appender = (log4net.Appender.FileAppender)hierarchy
                .GetLogger(loggerName, hierarchy.LoggerFactory)
                .GetAppender(appenderName);

            if (appender != null)
            {
                //出力先ファイルを設定
                appender.File = log_path;
                appender.ActivateOptions();
            }
        }
    }
}
