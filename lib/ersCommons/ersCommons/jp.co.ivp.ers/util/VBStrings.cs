﻿using System;
using Microsoft.VisualBasic;

/// -----------------------------------------------------------------------------
/// <summary>
///     Microsoft.VisualBasic.Strings をカバーした静的クラスです。
/// <summary>
/// -----------------------------------------------------------------------------
namespace jp.co.ivp.ers.util
{

    public class VBStrings
    {

        #region　Left メソッド

        /// -----------------------------------------------------------------------------------
        /// <summary>
        ///     文字列の左端から指定された文字数分の文字列を返します。
		/// <para>Returns a string with a specified number of characters from the left-side of a string</para>
		/// </summary>
        /// <param name="stTarget">
        ///     取り出す元になる文字列。
		///		<para>String to be extracted</para>
		/// </param>
        /// <param name="iLength">
        ///     取り出す文字数。
		///		<para>Number of characters to extract</para>
		/// </param>
        /// <returns>
        ///     左端から指定された文字数分の文字列。
        ///     文字数を超えた場合は、文字列全体が返されます。
		///		<para>a string with a specified number of characters from the left-side</para>
		///		<para>If specified length exceeds the total number of characters, the entire string will be returned</para>
		/// </returns>
        /// -----------------------------------------------------------------------------------
        public static string Left(string stTarget, int iLength)
        {
            if (iLength <= stTarget.Length)
            {
                return stTarget.Substring(0, iLength);
            }

            return stTarget;
        }

        #endregion

        #region　Mid メソッド (+1)

        /// -----------------------------------------------------------------------------------
        /// <summary>
        ///     文字列の指定された位置以降のすべての文字列を返します。
		///     <para>Returns a substring that will start at the specified position in a string </para>
		/// </summary>
        /// <param name="stTarget">
        ///     取り出す元になる文字列。
		///     <para>String to be extracted</para>
		/// </param>
        /// <param name="iStart">
        ///     取り出しを開始する位置。
		///     <para>The desired position which the substring will start</para>
		/// </param>
        /// <returns>
        ///     指定された位置以降のすべての文字列。
		///     <para>All subsequent string at the specified position</para>
		/// </returns>
        /// -----------------------------------------------------------------------------------
        public static string Mid(string stTarget, int iStart)
        {
            if (iStart <= stTarget.Length)
            {
                return stTarget.Substring(iStart - 1);
            }
            return string.Empty;
        }

        /// -----------------------------------------------------------------------------------
        /// <summary>
        ///     文字列の指定された位置から、指定された文字数分の文字列を返します。
		///     <para>Returns a substring starting from a specified position and with the specified number of characters</para>
		/// </summary>
        /// <param name="stTarget">
        ///     取り出す元になる文字列。
		///     <para>String to be extracted</para>
		/// </param>
        /// <param name="iStart">
        ///     取り出しを開始する位置。
		///     <para>The starting position at which to begin extraction </para>
		/// </param>
        /// <param name="iLength">
        ///     取り出す文字数。
		///     <para>Length of characters to extract</para>
		/// </param>
        /// <returns>
        ///     指定された位置から指定された文字数分の文字列。
        ///     文字数を超えた場合は、指定された位置からすべての文字列が返されます。
		///     <para>a string with a specified number of characters started from a specified position within a string </para>
		///     <para>If specified length exceeds the total number of characters, the entire string will be returned</para>
		/// </returns>
        /// -----------------------------------------------------------------------------------
        public static string Mid(string stTarget, int iStart, int iLength)
        {
            if (iStart <= stTarget.Length)
            {
                if (iStart + iLength - 1 <= stTarget.Length)
                {
                    return stTarget.Substring(iStart - 1, iLength);
                }

                return stTarget.Substring(iStart - 1);
            }

            return string.Empty;
        }

        #endregion

        #region　Right メソッド (+1)

        /// -----------------------------------------------------------------------------------
        /// <summary>
        ///     文字列の右端から指定された文字数分の文字列を返します。
		///     <para>Returns a string with a specified number of characters from the left-side of a string</para>
		/// </summary>
        /// <param name="stTarget">
        ///     取り出す元になる文字列。
		///     <para>string to be extracted</para>
		/// </param>
        /// <param name="iLength">
        ///     取り出す文字数。
		///     <para>Number of characters to extract</para>
		/// </param>
        /// <returns>
        ///     右端から指定された文字数分の文字列。
        ///     文字数を超えた場合は、文字列全体が返されます。
		///     <para>a string with a specified number of characters from the right-side</para>
		///		<para>If specified length exceeds the total number of characters, the entire string will be returned</para>
		/// </returns>
        /// -----------------------------------------------------------------------------------
        public static string Right(string stTarget, int iLength)
        {
            if (iLength <= stTarget.Length)
            {
                return stTarget.Substring(stTarget.Length - iLength);
            }

            return stTarget;
        }

        #endregion

        #region 半角カナ変換
        /// <summary>
        /// 半角カナ・全角カナに変換します。
        /// <para>Converts string to half-width kana</para>
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string HalfKana(string val)
        {
            if (val == null)
            {
                return null;
            }
            return Strings.StrConv(val, VbStrConv.Narrow, 0x0411);
        }
		/// <summary>
		/// Converts string to half-width kana
		/// </summary>
        public static string FullKana(string val)
        {
            if (val == null)
            {
                return null;
            }
            return Strings.StrConv(val, VbStrConv.Wide, 0x0411);
        }
        #endregion

    }
}