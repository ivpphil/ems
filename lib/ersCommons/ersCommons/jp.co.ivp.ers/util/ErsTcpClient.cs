﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.IO;
using System.Web.Mvc;

namespace jp.co.ivp.ers.util
{
    public class ErsTcpClient
        : IDisposable
    {
        private ErsTcpConnection state;

        private ManualResetEvent asyncDone = new ManualResetEvent(false);

        protected int messageTimeout { get; set; }


        /// <summary>
        /// Get new tcp client
        /// </summary>
        /// <param name="encoding"></param>
        /// <param name="messageTimeout"></param>
        public ErsTcpClient(int messageTimeout)
        {
            this.messageTimeout = messageTimeout;
        }

        /// <summary>
        /// connect to the main server
        /// </summary>
        /// <returns></returns>
        public void Connect(string hostname, int port, Encoding encoding)
        {
            this.asyncDone.Reset();

            this.state = new ErsTcpConnection(encoding);

            this.state.BeginConnect(hostname, port, (state) => this.asyncDone.Set());

            if (!this.asyncDone.WaitOne(this.messageTimeout))
            {
                // タイムアウト通知
                throw new TimeoutException("Tcp Connect Timeout");
            }

            this.state.CheckResult();
        }

        /// <summary>
        /// send msg to the server and get response
        /// </summary>
        /// <param name="msg"></param>
        public string SendMessage(string data)
        {
            this.asyncDone.Reset();

            this.state.Send(data, (state) => this.asyncDone.Set());

            if (!this.asyncDone.WaitOne(this.messageTimeout))
            {
                // タイムアウト通知
                throw new TimeoutException("Tcp Send Timeout");
            }

            this.state.CheckResult();

            this.asyncDone.Reset();

            this.state.Receive((client) => this.asyncDone.Set());

            if (!this.asyncDone.WaitOne(this.messageTimeout))
            {
                // タイムアウト通知
                throw new TimeoutException("Tcp Receive Timeout");
            }

            this.state.CheckResult();

            return this.state.GetReceivedMessage();
        }

        /// <summary>
        /// send msg to the server and get response
        /// </summary>
        /// <param name="msg"></param>
        internal void Send(string data)
        {
            this.asyncDone.Reset();

            this.state.Send(data, (state) => this.asyncDone.Set());

            this.state.CheckResult();
        }

        /// <summary>
        /// Implements for IDisposable
        /// </summary>
        public void Dispose()
        {
            if (this.state != null)
            {
                this.state.CloseSocket();
                this.state = null;
            }
        }
    }
}
