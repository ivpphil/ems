﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using jp.co.ivp.ers.db;
using System.Web;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.util
{
    public static class ErsReflection
    {

        private static Dictionary<string, object> InstanceCache
        {
            get
            {
                var result = (Dictionary<string, object>)ErsCommonContext.GetPooledObject("jp.co.ivp.ers.util.ErsReflection.InstanceCache");
                if (result == null)
                {
                    result = new Dictionary<string, object>();
                    ErsCommonContext.SetPooledObject("jp.co.ivp.ers.util.ErsReflection.InstanceCache", result);
                }
                return result;
            }
            set
            {
                ErsCommonContext.SetPooledObject("jp.co.ivp.ers.util.ErsReflection.InstanceCache", value);
            }
        }

        /// <summary>
        /// namespace を含むクラス名からインスタンスを作成する
		/// <para>Creates an instance from specified classname, incuding namespace</para>
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public static Object CreateInstanceFromFullName(string strName, bool enableCache = false)
        {

            if (enableCache && InstanceCache.ContainsKey(strName))
            {
                return InstanceCache[strName];
            }

            foreach (var assem in AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {

                    foreach (var ta in assem.GetExportedTypes())
                    {
                        if (ta.FullName == strName)
                        {
                            var instance = Activator.CreateInstance(ta);

                            if (enableCache)
                            {
                                InstanceCache[strName] = instance;
                            }
                            return instance;
                        }
                    }
                }
                catch(Exception ex)
                {
                    //バッチから起動の場合でなければ例外とする
                    if (!ErsCommonContext.IsBatch)
                    {
                        throw new Exception(ex.ToString());
                    }
                        
                }
            }

            return null;
        }


        /// <summary>
        /// クラス名からインスタンスを作成する
		/// <para>Creates an instance from the specified classname</para>
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public static Object CreateInstanceFromName(string strName)
        {

            foreach (var assem in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var ta in assem.GetExportedTypes())
                {
                    if (ta.Name == strName)
                    {
                        var instance = Activator.CreateInstance(ta);

                        return instance;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Dictionaryからオブジェクトのプロパティへ、値を全てセット
		/// <para>Sets the values from the dictionary to the property of an object</para>
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="dic"></param>
        public static void SetPropertyAll(object obj, IDictionary<string, object> dic, string bindTargetName = null)
        {
            var properties = obj.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!isBindTarget(property, bindTargetName))
                {
                    continue;
                }

                string dictionaryKey = property.Name;
                if (property.CanWrite && dic.ContainsKey(dictionaryKey))
                {
                    SetProperty(property, obj, dic[dictionaryKey], null);
                }
            }
        }

        /// <summary>
        /// バインド対象かどうかを検証する
        /// </summary>
        /// <param name="property"></param>
        /// <param name="bindTargetName"></param>
        /// <returns></returns>
        public static bool isBindTarget(PropertyInfo property, string bindTargetName)
        {
            if(bindTargetName == null)
            {
                return true;
            }

            var bindTargetAttrbutes = property.GetCustomAttributes(typeof(BindTargetAttribute), false);
            foreach (BindTargetAttribute bindTarget in bindTargetAttrbutes)
            {
                if (bindTarget.bindTargetName == bindTargetName)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Nullable型を考慮してキャストしたのちプロパティにセット
        /// </summary>
        /// <param name="property"></param>
        /// <param name="obj"></param>
        /// <param name="value"></param>
        /// <param name="index"></param>
        public static void SetProperty(PropertyInfo property, object obj, object value, object[] index)
        {
            if (value is string && string.IsNullOrEmpty((string)value))
                value = null;

            if (property == null)
                return;
            else if (!property.CanWrite)
                return;

            value = ConvertValue(property.PropertyType, value);

            //exit function if the value cannot be casted to the property type
            if (value != null && !property.PropertyType.IsAssignableFrom(value.GetType()))
                return;

            try
            {
                property.SetValue(obj, value, null);
            }
            catch (InvalidCastException e)
            {
                throw new InvalidCastException(
                    string.Format("{0}({2})を{1}にCastできません。"
                    , value.GetType().Name
                    , property.PropertyType.Name
                    , property.Name), e);
            }
        }

        /// <summary>
        /// Convert the property type of a value
        /// </summary>
        /// <param name="propertyType"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static object ConvertValue(Type propertyType, object value)
        {
            if (IsNullable(propertyType))
            {
                if(value is string && string.IsNullOrEmpty((string)value))
                    return null;

                propertyType = GetNullableBaseType(propertyType);
            }

            if (value is DBNull)
                value = null;

            if (propertyType.IsEnum)
            {
                //文字列からの変換をtryする
                if (value != null)
                {
                    if (value is string && Enum.GetNames(propertyType).Contains((string)value))
                    {
                        return Enum.Parse(propertyType, (string)value);
                    }
                    else if (!(value is string) || !string.IsNullOrEmpty((string)value))
                    {
                        var underlyingType = Enum.GetUnderlyingType(propertyType);

                        var underlyingValue = ConvertValue(underlyingType, value);

                        if (underlyingValue == null)
                        {
                            return null;
                        }

                        return Enum.ToObject(propertyType, underlyingValue);
                    }
                }
            }
            else if (value is IConvertible || value is Array)
            {
                if (value is string && string.IsNullOrEmpty((string)value) && propertyType.IsValueType && !IsNullable(propertyType))
                {
                    //Gets a default value if the propertyType is a value type.
                    return Activator.CreateInstance(propertyType);
                }

                try
                {
                    return Convert.ChangeType(value, propertyType);
                }
                catch
                {
                    return null;
                }
            }

            return value;
            
        }

        public static Type GetNullableBaseType(Type propertyType)
        {
            //nullableのキャストを考慮(int16 -> int32?とか)
            return propertyType.GetGenericArguments()[0];
        }

        /// <summary>
        /// 文字列を","で区切り、指定した型の配列を取得する
		/// <para>Gets an array of property types from a "," concatenated string</para>
        /// </summary>
        /// <param name="value"></param>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        public static object GetArrayBySplit(object value, Type propertyType, char[] separator)
        {
            var stringVal = string.Empty;
            if (value is Array)
            {
                foreach (var val in (Array)value)
                {
                    stringVal += separator[0] + val.ToString();
                }
                if (!string.IsNullOrEmpty(stringVal))
                    stringVal = stringVal.Substring(1);
            }
            else if(propertyType.IsArray)
            {
                stringVal = Convert.ToString(value);
            }
            else
            {
                stringVal = Convert.ToString(value);
            }

            //配列
            var arrVal = stringVal.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            var newArr = Array.CreateInstance(propertyType.GetElementType(), arrVal.Length);
            for (var i = 0; i < arrVal.Length; i++)
            {
                newArr.SetValue(ErsReflection.ConvertValue(propertyType.GetElementType(), arrVal[i]), i);
                //newArr.SetValue(Convert.ChangeType(arrVal[i], propertyType.GetElementType()), i);

            }
            return newArr;
        }

        /// <summary>
        /// propertyを戻す
		/// <para>Returns a property</para>
        /// </summary>
        /// <returns></returns>
        public static PropertyInfo GetProperty(object o, string name, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static)
        {
            return o.GetType().GetProperty(name, bindingAttr);
        }

        /// <summary>
        /// propertyを戻す
		/// <para>Returns a property</para>
        /// </summary>
        /// <returns></returns>
        public static object GetPropertyValue(object o, string name, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static)
        {

            var prop = o.GetType().GetProperty(name, bindingAttr);
            if (prop == null)
                return null;

            return prop.GetValue(o, null);

        }

        /// <summary>
        /// public propertyをDictionaryにセットして戻す
		/// <para>Returns a dictionary of an object's public properties</para>
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, object> GetPropertiesAsDictionary(object o, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static, string bindTargetName = null, Type attr = null)
        {
            var t = o.GetType();

            Dictionary<string, object> dicModel;
            if (o is IErsModelBase)
            {
                dicModel = new ErsModelDictionary<string, object>();
            }
            else
            {
                dicModel = new Dictionary<string, object>();
            }

            var properties = GetProperties(bindingAttr, attr, t);

            foreach (PropertyInfo property in properties)
            {
                if (!isBindTarget(property, bindTargetName))
                {
                    continue;
                }

                var value = property.GetValue(o, null);

                var actualValue = GetPropertyValueForErs(property, value);

                dicModel[property.Name] = actualValue;
            }
            return dicModel;
        }

        public class ErsModelDictionary<TKey, TValue>
            : Dictionary<TKey, TValue>
        {
            public ErsModelDictionary()
                : base()
            {
            }

            public ErsModelDictionary(IDictionary<TKey, TValue> baseObject)
                : base(baseObject)
            {
            }
        }

        /// <summary>
        /// public propertyをDictionaryにセットして戻す
        /// <para>Returns a dictionary of an object's public properties</para>
        /// </summary>
        /// <returns></returns>
        public static object[] GetPropertiesAsArray(object o, Type attr, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static)
        {
            var t = o.GetType();

            var listValue = new List<object>();

            var properties = GetProperties(bindingAttr, attr, t);
            foreach (PropertyInfo property in properties)
            {
                var value = property.GetValue(o, null);

                var actualValue = GetPropertyValueForErs(property, value);

                listValue.Add(actualValue);
            }
            return listValue.ToArray();
        }

        public static object GetPropertyValueForErs(PropertyInfo property, object value)
        {
            if (property.CanRead)
            {
                return value;
            }

            return null;
        }


        /// <summary>
        /// プロパティを取得します。ソート可能な場合はソートします。
        /// </summary>
        /// <param name="bindingAttr"></param>
        /// <param name="attr"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        private static IEnumerable<PropertyInfo> GetProperties(BindingFlags bindingAttr, Type attr, Type t)
        {
            IEnumerable<PropertyInfo> properties;
            if (attr != null)
            {
                properties = t.GetProperties(bindingAttr)
                    .Where(prop => prop.GetCustomAttributes(attr, false).Length > 0);

                //ソート可能な場合はソートする
                if (typeof(ISortableAttribute).IsAssignableFrom(attr))
                {
                    properties = properties.OrderBy(
                        prop => prop.GetCustomAttributes(false).OfType<ISortableAttribute>().First().Order
                        );
                }
            }
            else
            {
                properties = t.GetProperties(bindingAttr);
            }
            return properties;
        }

        /// <summary>
        /// ジェネリックDictionary型かどうかを判定する。
		/// <para>Determine if the argument is a GenericType</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsGenericDictionary(Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Dictionary<,>))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// ジェネリックDictionary型かどうかを判定する。
		/// <para>Determine if the argument is a GenericList</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsGenericList(Type type)
        {
            if (type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(IList<>) || type.GetGenericTypeDefinition() == typeof(List<>)))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Nullable型かどうかを判定する。
		/// <para>Determine if the argument is nullable type</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsNullable(Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Enum型かどうかを判定する。
		/// <para>Determine if the argument is an Enum type</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsEnum(Type type)
        {
            if (type.IsEnum)
                return true;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>) && type.GetGenericArguments()[0].IsEnum)
            {
                return true;
            }

            return false;
        }


        /// <summary>
        /// get declared vlues only within the class as dictionary for angular response
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static Dictionary<string, object> GetCurrentModelPropertiesAsDictionary(object o)
        {
            var modelProperties = o.GetType().GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);

            // Current Model properties = are the fields that are declared on the model itself excluding the properties coming from the parent class
            List<string> currentModelPropertyNames = new List<string>();
            for (int i = 0; i < modelProperties.Length; i++)
            {
                currentModelPropertyNames.Add(modelProperties[i].Name);
            }

            var modelDictionary = GetPropertiesAsDictionary(o);
            Dictionary<string, object> currentModelProperties = new Dictionary<string, object>();

            foreach (var model in modelDictionary)
            {
                if (currentModelPropertyNames.Contains(model.Key))
                {
                    currentModelProperties.Add(model.Key, model.Value);
                }
            }
            return currentModelProperties;
        }

    }

}
