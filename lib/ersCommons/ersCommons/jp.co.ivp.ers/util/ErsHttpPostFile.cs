﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.util
{
    public class ErsHttpPostFile
    {
        public ErsHttpPostFile()
        {
            this.contentType = "application/octet-stream";
        }

        public string fileName { get; set; }
        public string contentType { get; set; }
        public byte[] fileValue { get; set; }
    }
}
