﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers.util;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using jp.co.ivp.ers.mvc;
using System.Web.Caching;
using System.Web;

namespace jp.co.ivp.ers.util
{
    public class ErsResizeImage
    {
        protected float max_width { get; set; }
        protected float max_height { get; set; }

        private const string CacheKeyPrefix = "ResizeImage_Exists_";
        private const int cacheSeconds = 10;//10秒

        /// <summary>
        /// *** 排他制御のため、わざとstaticで定義しています。 ***
        /// </summary>
        private static object semiLockObject = new object();

        /// <summary>
        /// *** 排他制御のため、わざとstaticで定義しています。 ***
        /// </summary>
        private static System.Collections.Hashtable lockObjectList = System.Collections.Hashtable.Synchronized(new System.Collections.Hashtable());

        /// <summary>
        /// format of the file
        /// </summary>
        public ImageFormat ImageFormat { get; protected set; }

        public string ContentType { get; protected set; }

        public void InitSize(int max_width, int max_height = 0)
        {
            this.max_width = max_width;
            this.max_height = max_height;
        }

        public byte[] CreateResizedImageWithCache(string fullSrcPath, string temporaryDirectory)
        {
            this.LoadImageFormat(fullSrcPath);

            //サイズ指定がない場合はそのまま
            if (this.max_width == 0 && this.max_height == 0)
            {
                return File.ReadAllBytes(fullSrcPath);
            }

            ErsDirectory.CreateDirectories(temporaryDirectory);

            //リサイズ後のファイル名は、拡張子にサイズが付属する。
            var destFileName = Path.GetFileNameWithoutExtension(fullSrcPath) + "_" + this.max_width + "_" + this.max_height + Path.GetExtension(fullSrcPath);
            var fullDestPath = temporaryDirectory + destFileName;

            var cachedValue = HttpContext.Current.Cache[CacheKeyPrefix + destFileName];
            if (cachedValue != null)
            {
                //変換済みファイルがある場合はそのまま返却
                return File.ReadAllBytes(fullDestPath);
            }

            //再変換
            lock (semiLockObject)
            {
                if (!lockObjectList.ContainsKey(destFileName))
                {
                    lockObjectList.Add(destFileName, new object());
                }
            }

            byte[] imageByte;
            lock (lockObjectList[destFileName])
            {
                var destFileInfo = new FileInfo(fullDestPath);
                var srcFileInfo = new FileInfo(fullSrcPath);

                if (destFileInfo.Exists && destFileInfo.LastWriteTime == srcFileInfo.LastWriteTime)
                {
                    //変更がない場合はそのまま返却
                    imageByte = File.ReadAllBytes(fullDestPath);
                }
                else
                {
                    imageByte = this.CreateResizedImage(fullSrcPath, temporaryDirectory);
                    File.WriteAllBytes(fullDestPath, imageByte);
                    File.SetLastWriteTime(fullDestPath, srcFileInfo.LastWriteTime);
                }

                //ファイルの再読み込みをスキップする為のキャッシュ
                HttpContext.Current.Cache.Add(CacheKeyPrefix + destFileName, 1, null, DateTime.Now.AddSeconds(cacheSeconds), Cache.NoSlidingExpiration, CacheItemPriority.BelowNormal, null);
            }
            return imageByte;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullSrcPath"></param>
        /// <returns></returns>
        public byte[] CreateResizedImage(string fullSrcPath, string temporaryDirectory)
        {
            try
            {
                using (var original = Image.FromFile(fullSrcPath))
                {
                    if (original.Width < max_width)
                    {
                        //create image scaled down
                        using (var ms = new MemoryStream())
                        {
                            original.Save(ms, this.ImageFormat);
                            var result = ms.ToArray();
                            return result;
                        }
                    }
                }
            }
            catch (OutOfMemoryException ex)
            {
                byte[] item = new byte[0];
                return item;
            }

            //create image scaled down
            using (var fs = new FileStream(fullSrcPath, FileMode.Open, FileAccess.Read))
            {
                using (Bitmap src = new Bitmap(fs))
                {
                    using (Bitmap resizedBitmap = this.GetResizeBitmap(src, fullSrcPath))
                    {
                        this.Resize(src, resizedBitmap);

                        using (var ms = new MemoryStream())
                        {
                            resizedBitmap.Save(ms, this.ImageFormat);
                            var result = ms.ToArray();
                            return result;
                        }
                    }
                }
            }

        }

        /// <summary>
        /// create resized image. this method save the image as file.
        /// </summary>
        /// <param name="originalImageFilePath"></param>
        /// <param name="imageOutputPath"></param>
        /// <param name="imageFileName">exclude extension</param>
        public void CreateResizedImage(string originalImageFilePath, string imageOutputPath, string imageFileName)
        {
            //not create image file not to override if output path is equal to original path
            if (Path.GetDirectoryName(originalImageFilePath) == imageOutputPath)
                return;

            //file path for output image (use original extension)
            var imageOutputfilePath = imageOutputPath + "\\" + imageFileName;

            //create target directory if it doesn't exist
            Directory.CreateDirectory(imageOutputPath);

            this.LoadImageFormat(originalImageFilePath);

            //create image scaled down
            using (var fs = new FileStream(originalImageFilePath, FileMode.Open, FileAccess.Read))
            {

                using (Bitmap src = new Bitmap(fs))
                {

                    using (Bitmap resizedBitmap = this.GetResizeBitmap(src, imageOutputfilePath))
                    {

                        this.Resize(src, resizedBitmap);

                        resizedBitmap.Save(imageOutputfilePath, this.ImageFormat);
                    }
                }
            }
        }

        /// <summary>
        /// Get new Bitmap
        /// </summary>
        /// <param name="sourceImage"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private Bitmap GetResizeBitmap(Bitmap sourceImage, string outputfileFileName)
        {
            this.SetSize(sourceImage);

            return new Bitmap(Convert.ToInt32(max_width), Convert.ToInt32(max_height));
        }

        /// <summary>
        /// Paint new size image
        /// </summary>
        /// <param name="src"></param>
        /// <param name="resizedBitmap"></param>
        private void Resize(Bitmap src, Bitmap resizedBitmap)
        {
            Graphics g = Graphics.FromImage(resizedBitmap);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic; //高品質双三次補間;
            g.DrawImage(src, 0, 0, max_width, max_height);
        }


        /// <summary>
        /// set width and height
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="scale"></param>
        private void SetSize(Bitmap bitmap, float scale = 1.0f)
        {

            //calc scale if not define width or height
            if (max_width <= 0 && max_height <= 0)
            {
                max_width = bitmap.Width * scale;
                max_height = bitmap.Height * scale;
            }
            else
            {
                //not scale down if specified width exceed original one
                if (max_width > bitmap.Width)
                {
                    max_width = bitmap.Width;
                }

                //not scale down if specified height exceed original one
                if (max_height > bitmap.Height)
                {
                    max_height = bitmap.Height;
                }

                //scale down file keeping width ratio if not define height
                if (max_width != 0 && max_height == 0)
                {
                    max_height = bitmap.Height * (max_width / bitmap.Width);
                }
                //scale down file keeping height ratio if not define width
                if (max_width == 0 && max_height != 0)
                {
                    max_width = bitmap.Width * (max_height / bitmap.Height);
                }
            }
        }

        /// <summary>
        /// get image format
        /// </summary>
        /// <param name="outputfileFileName"></param>
        /// <returns></returns>
        private void LoadImageFormat(string outputfileFileName)
        {
            var extension = Path.GetExtension(outputfileFileName).ToLower();
            if (".gif" == extension)
            {
                this.ImageFormat = ImageFormat.Gif;
                this.ContentType = "image/gif";
            }
            else if (".jpg" == extension || ".jpeg" == extension)
            {
                this.ImageFormat = ImageFormat.Jpeg;
                this.ContentType = "image/jpeg";
            }
            else if (".png" == extension)
            {
                this.ImageFormat = ImageFormat.Png;
                this.ContentType = "image/png";
            }
            else
            {
                throw new NotImplementedException("The image format is not implemented.");
            }

        }
    }
}
