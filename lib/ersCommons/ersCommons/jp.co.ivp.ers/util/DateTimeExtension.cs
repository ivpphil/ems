﻿using System;

namespace jp.co.ivp.ers.util
{
    public static class DateTimeExtension
    {
        /// <summary>
        /// Get for search
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static DateTime GetForSearch(this DateTime datetime, string format)
        {
            return DateTime.Parse(datetime.ToString(format));
        }

        /// <summary>
        /// Get start for search
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static DateTime GetStartForSearch(this DateTime datetime)
        {
            return DateTime.Parse(datetime.ToString("yyyy/MM/dd 00:00:00"));
        }

        /// <summary>
        /// Get now for search
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static DateTime GetNowForSearch(this DateTime datetime)
        {
            return DateTime.Parse(datetime.ToString("yyyy/MM/dd HH:mm:ss"));
        }

        /// <summary>
        /// Get end for search
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static DateTime GetEndForSearch(this DateTime datetime)
        {
            return DateTime.Parse(datetime.ToString("yyyy/MM/dd 23:59:59"));
        }

        /// <summary>
        /// Get start day of month
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static DateTime GetStartDayOfMonth(this DateTime datetime)
        {
            return new DateTime(datetime.Year, datetime.Month, 1);
        }

        /// <summary>
        /// Get end day of month
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static DateTime GetEndDayOfMonth(this DateTime datetime)
        {
            return new DateTime(datetime.Year, datetime.Month, DateTime.DaysInMonth(datetime.Year, datetime.Month));
        }

        /// <summary>
        /// Get the start day of the this week
        /// </summary>
        /// <param name="datetime"></param>
        /// <param name="startDay"></param>
        /// <returns></returns>
        public static DateTime GetStartDayOfThisWeek(this DateTime datetime, DayOfWeek startDay)
        {
            var result = startDay - datetime.DayOfWeek;
            return new DateTime(datetime.Year, datetime.Month, datetime.Day).AddDays(result <= 0 ? result : result - 7);
        }

        /// <summary>
        ///  Get the end day of the this week
        /// </summary>
        /// <param name="datetime"></param>
        /// <param name="startDay"></param>
        /// <returns></returns>
        public static DateTime GetEndDayOfThisWeek(this DateTime datetime, DayOfWeek startDay)
        {
            return datetime.GetStartDayOfThisWeek(startDay).AddDays(6);
        }
    }
}
