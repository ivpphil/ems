﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSharpCode.SharpZipLib.GZip;
using System.IO;
using ICSharpCode.SharpZipLib.Tar;

namespace jp.co.ivp.ers.util
{
    public class TarZipFileManager
    {
        /// <summary>
        /// ファイルの圧縮
        /// </summary>
        /// <param name="sourcePath">圧縮前パス</param>
        /// <param name="destPath">圧縮後パス</param>
        public virtual void Compress(string sourcePath, string destPath)
        {
            using (var stream = new GZipOutputStream(File.Create(destPath)))
            {
                using (var archive = TarArchive.CreateOutputTarArchive(stream, TarBuffer.DefaultBlockFactor))
                {
                    archive.SetKeepOldFiles(false);
                    archive.AsciiTranslate = false;
                    archive.SetUserInfo(0, "", 0, "None");

                    TarEntry entry = TarEntry.CreateEntryFromFile(sourcePath);

                    //吐き出すファイル名のみを取得
                    entry.Name = System.IO.Path.GetFileName(sourcePath);
                    archive.WriteEntry(entry, true);

                    archive.Close();
                }
                stream.Close();
            }
        }

        /// <summary>
        /// ファイルの解凍
        /// </summary>
        /// <param name="inputFilePaht">解凍前パス</param>
        /// <param name="outPutFilePath">解凍後パス</param>
        public virtual void Extract(string sourcePath, string destDirectoryPath)
        {
            using (var stream = new GZipInputStream(File.OpenRead(sourcePath)))
            {
                using (var archive = TarArchive.CreateInputTarArchive(stream, TarBuffer.DefaultBlockFactor))
                {
                    archive.SetKeepOldFiles(false);
                    archive.AsciiTranslate = false;
                    archive.SetUserInfo(0, "", 0, "None");

                    //フォルダに吐かれる
                    archive.ExtractContents(destDirectoryPath);

                    archive.Close();
                }
                stream.Close();
            }
        }
    }
}
