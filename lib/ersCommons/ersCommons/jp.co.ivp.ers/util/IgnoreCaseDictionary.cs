﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.util
{
    public class ErsIgnoreCaseDictionary
        : IDictionary<string, object>
    {
        private Dictionary<string, object> _dictionary;

        public ErsIgnoreCaseDictionary()
        {
            _dictionary = new Dictionary<string, object>();
        }

		/// <summary>
		/// Append a key to a dictionary
		/// </summary>
        public void Add(string key, object value)
        {
            _dictionary.Add(key.ToLower(), value);
        }

		/// <summary>
		/// Determine if dictionary contains the string argument
		/// </summary>
        public bool ContainsKey(string key)
        {
            return _dictionary.ContainsKey(key.ToLower());
        }

		/// <summary>
		/// Gets the dictionary.Keys
		/// </summary>
        public ICollection<string> Keys
        {
            get { return _dictionary.Keys; }
        }

		/// <summary>
		/// Remove a key
		/// </summary>
        public bool Remove(string key)
        {
            return _dictionary.Remove(key.ToLower());
        }

		/// <summary>
		/// Determine if the specified key is in dictionary
		/// </summary>
        public bool TryGetValue(string key, out object value)
        {
            return _dictionary.TryGetValue(key.ToLower(), out value);
        }

		/// <summary>
		/// Gets dictionary values
		/// </summary>
        public ICollection<object> Values
        {
            get { return _dictionary.Values; }
        }

		/// <summary>
		/// Gets or Sets the dictionary values
		/// </summary>
        public object this[string key]
        {
            get
            {
                return _dictionary[key.ToLower()];
            }
            set
            {
                _dictionary[key.ToLower()] = value;
            }
        }

		/// <summary>
		/// Adds a keyvaluepair in the dictionary
		/// </summary>
        public void Add(KeyValuePair<string, object> item)
        {
            var keyValuePair = new KeyValuePair<string, object>(item.Key.ToLower(), item.Value);
            ((ICollection<KeyValuePair<string, object>>)_dictionary).Add(keyValuePair);
        }

		/// <summary>
		/// clear ictionary
		/// </summary>
        public void Clear()
        {
            _dictionary.Clear();
        }


		/// <summary>
		/// Determines if the specified KeyValuePair exists in the dictionary
		/// </summary>
        public bool Contains(KeyValuePair<string, object> item)
        {
            var keyValuePair = new KeyValuePair<string, object>(item.Key.ToLower(), item.Value);
            return _dictionary.Contains(keyValuePair);
        }

		/// <summary>
		/// Copy KeyValuePair array to dictionary with the specified index
		/// </summary>
        public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
        {
            ((ICollection<KeyValuePair<string, object>>)_dictionary).CopyTo(array, arrayIndex);
        }

		/// <summary>
		/// Gets the dictionary property count
		/// </summary>
        public int Count
        {
            get { return _dictionary.Count; }
        }

		/// <summary>
		/// Determine if dictionary has readonly property
		/// </summary>
        public bool IsReadOnly
        {
            get { return ((System.Collections.IDictionary)_dictionary).IsReadOnly; }
        }

		/// <summary>
		/// Remove a KeyValuePair item from the dictionary
		/// </summary>
        public bool Remove(KeyValuePair<string, object> item)
        {
            var keyValuePair = new KeyValuePair<string, object>(item.Key.ToLower(), item.Value);
            return ((ICollection<KeyValuePair<string, object>>)_dictionary).Remove(keyValuePair);
        }

		/// <summary>
		/// Returns an enumerator that iterates through the dictionary
		/// </summary>
        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }
    }
}
