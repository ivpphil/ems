﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ionic.Zip;
using Ionic.Zlib;

namespace jp.co.ivp.ers.util
{
    /// <summary>
    /// ZIPファイルをコントロールする
    /// </summary>
    public class ZipFileManager
    {
        /// <summary>
        /// ZIP化して保存
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="destPath"></param>
        public virtual void Compress(string sourcePath, string destPath)
        {
            using (ZipFile zip = new ZipFile(Encoding.UTF8))
            {
                // 圧縮レベルを設定
                zip.CompressionLevel = CompressionLevel.BestCompression;

                // ファイルを追加
                zip.AddFile(sourcePath, string.Empty);

                // ZIPファイルを保存
                zip.Save(destPath);
            }
        }

        /// <summary>
        /// ZIP化して保存（リスト）
        /// </summary>
        /// <param name="sourcePathList"></param>
        /// <param name="destPath"></param>
        /// <param name="funcRename"></param>
        public virtual void CompressList(IEnumerable<string> sourcePathList, string destPath, Func<string, string> funcRename = null)
        {
            using (ZipFile zip = new ZipFile(Encoding.UTF8))
            {
                // 圧縮レベルを設定
                zip.CompressionLevel = CompressionLevel.BestCompression;

                // ファイルを追加
                foreach (var path in sourcePathList)
                {
                    var entry = zip.AddFile(path, string.Empty);

                    // リネーム
                    if (funcRename != null)
                    {
                        entry.FileName = funcRename(entry.FileName);
                    }
                }

                // ZIPファイルを保存
                zip.Save(destPath);
            }
        }

        public virtual void Extract(string sourcePath, string destPath)
        {
            throw new NotImplementedException();
        }
    }
}
