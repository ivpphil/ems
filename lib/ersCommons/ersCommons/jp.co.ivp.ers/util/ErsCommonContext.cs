﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections;
using System.Threading;
using jp.co.ivp.ers.mvc;
using System.Net;

namespace jp.co.ivp.ers.util
{
    public class ErsCommonContext
    {
        public static ThreadLocal<IDictionary> HttpContextCurrentItemOfBatch = new ThreadLocal<IDictionary>(()=>new Dictionary<string, object>());

        private static ThreadLocal<IDictionary<string, object>> HttpContextCurrentApplicationOfBatch = new ThreadLocal<IDictionary<string, object>>(() => new Dictionary<string, object>());

        public static HttpContext HttpContext { get; set; }

        private static string applicationRootPath;

        /// <summary>
        /// 共通コンテクストを初期化する
        /// </summary>
        /// <param name="p"></param>
        public static void Initialize(HttpContext httpContext, string applicationRootPath)
        {
            ErsCommonContext.HttpContext = httpContext;
            ErsCommonContext.applicationRootPath = applicationRootPath;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        }

        /// <summary>
        /// バッチでの実行時はTrue
        /// </summary>
        public static bool IsBatch
        {
            get
            {
                return ErsCommonContext.HttpContext == null;
            }
        }

        /// <summary>
        /// アプリケーションルートからの物理ファイルパスを返します
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string MapPath(string path)
        {
            if (string.IsNullOrEmpty(ErsCommonContext.applicationRootPath))
            {
                throw new Exception("ErsCommonContext is not initialized. please call ErsCommonContext.Initilize at the entry point of your application.");
            }

            if (!path.StartsWith("\\")
                && !path.StartsWith("/")
                && !path.StartsWith(".")
                && !path.StartsWith("~"))
           {
                //フルパスが渡ってきた場合は、そのままを返す。
                return path;
            }

            var relativePath = path;
            if (relativePath.StartsWith("~/"))
            {
                relativePath = path.Substring(2);
            }

            //MapPathでは、サイトをまたいでのパス取得で不具合があるため、絶対パスからの相対パスで取得する
            var uriSource = new Uri(ErsCommonContext.applicationRootPath);
            return new Uri(uriSource, relativePath).LocalPath;
        }

        public static HttpApplicationState Application
        {
            get
            {
                if (IsBatch)
                {
                    return null;
                }

                return HttpContext.Application;
            }
        }

        public static IDictionary CurrentItems
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return HttpContextCurrentItemOfBatch.Value;
                }

                //ここは、ErsCommonContext.HttpContextから値を取得しない（セッションごとのオブジェクトを使用するため）
                return HttpContext.Current.Items;
            }
        }

        /// <summary>
        /// Gets the pooled common object used in this request.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object GetPooledObject(string key)
        {
            return CurrentItems["PooledObject" + key];
        }

        /// <summary>
        /// Pool the common object used in this request.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static void SetPooledObject(string key, object value)
        {
            CurrentItems["PooledObject" + key] = value;
        }


        internal static void SetObjectToApplication(string key, object obj)
        {
            if (IsBatch)
            {
                HttpContextCurrentApplicationOfBatch.Value[key] = obj;
            }
            else
            {
                Application[key] = obj;
            }
        }

        internal static object GetObjectFromApplication(string key)
        {
            if (IsBatch)
            {
                if (!HttpContextCurrentApplicationOfBatch.Value.ContainsKey(key))
                {
                    return null;
                }

                return HttpContextCurrentApplicationOfBatch.Value[key];
            }
            else
            {
                return Application[key];
            }
        }
    }
}
