﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Security.Permissions;
using System.ComponentModel;

namespace jp.co.ivp.ers.util
{
    public class ChangeLogonUserHelper
        : IDisposable
    {
        [DllImport("advapi32.dll", SetLastError = true)]
        static extern bool LogonUser(
        string principal,
        string authority,
        string password,
        LogonSessionType logonType,
        LogonProvider logonProvider,
        out IntPtr token);

        [DllImport("advapi32.dll", SetLastError = true)]
        static extern bool DuplicateToken(
        IntPtr existingTokenHandle,
        ImpersonationLevel SECURITY_IMPERSONATION_LEVEL,
        ref IntPtr duplicateTokenHandle);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool CloseHandle(IntPtr handle);
        enum LogonSessionType : uint
        {
            Interactive = 2,
            Network,
            Batch,
            Service,
            NetworkCleartext = 8,
            NewCredentials
        }

        enum LogonProvider : uint
        {
            Default = 0,
            WinNT35,
            WinNT40,
            WinNT50
        }

        public enum ImpersonationLevel : uint
        {
            SecurityAnonymous = 0,
            SecurityIdentification = 1,
            SecurityImpersonation = 2,
            SecurityDelegation = 3
        }

        IntPtr token = IntPtr.Zero;
        IntPtr duptoken = IntPtr.Zero;
        WindowsIdentity newIdentity = null;
        WindowsImpersonationContext impersonatedUser = null;

        /// <summary>
        /// ユーザ権限の変更を開始する。
        /// </summary>
        /// <param name="localUserName"></param>
        /// <param name="localUserPass"></param>
        /// <param name="doChangeAuthUser"></param>
        /// <returns></returns>
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public static ChangeLogonUserHelper BeginChange(string localUserName, string localUserPass, bool doChangeAuthUser)
        {
            var changeLogonUserHelper = new ChangeLogonUserHelper();
            changeLogonUserHelper.ChangeLogonUser(localUserName, localUserPass, doChangeAuthUser, Environment.MachineName, LogonSessionType.Network);
            return changeLogonUserHelper;
        }

        /// <summary>
        /// ユーザ権限の変更を開始する。
        /// </summary>
        /// <param name="remoteMachineName">接続先マシン名（IP）</param>
        /// <param name="localUserName"></param>
        /// <param name="localUserPass"></param>
        /// <param name="doChangeAuthUser"></param>
        /// <returns></returns>
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public static ChangeLogonUserHelper BeginChange(string remoteMachineName, string localUserName, string localUserPass, bool doChangeAuthUser)
        {
            var changeLogonUserHelper = new ChangeLogonUserHelper();
            changeLogonUserHelper.ChangeLogonUser(localUserName, localUserPass, doChangeAuthUser, remoteMachineName, LogonSessionType.NewCredentials);
            return changeLogonUserHelper;
        }

        /// <summary>
        /// 権限変更処理
        /// </summary>
        /// <param name="localUserName"></param>
        /// <param name="localUserPass"></param>
        /// <param name="doChangeAuthUser"></param>
        /// <param name="machineName"></param>
        /// <param name="sesstionType"></param>
        private void ChangeLogonUser(string localUserName, string localUserPass, bool doChangeAuthUser, string machineName, LogonSessionType sesstionType)
        {
            if (doChangeAuthUser)
            {
                //ファイル操作権限を変更する。
                if (!LogonUser(localUserName, machineName, localUserPass, sesstionType, LogonProvider.Default, out token))
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error());
                }

                if (!DuplicateToken(token, ImpersonationLevel.SecurityImpersonation, ref duptoken))
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error());
                }

                newIdentity = new WindowsIdentity(duptoken, "WindowsAuthentication");
                impersonatedUser = newIdentity.Impersonate();
            }
        }

        /// <summary>
        /// IDisposableの実装（権限を戻す）
        /// </summary>
        public void Dispose()
        {
            if (impersonatedUser != null)
                impersonatedUser.Undo();

            if (duptoken != IntPtr.Zero)
                CloseHandle(duptoken);

            if (token != IntPtr.Zero)
                CloseHandle(token);
        }
    }
}
