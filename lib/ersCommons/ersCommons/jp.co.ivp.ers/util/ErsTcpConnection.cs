﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;

namespace jp.co.ivp.ers.util
{
    public class ErsTcpConnection
    {
        private Socket Socket { get; set; }

        public Encoding encoding { get; protected set; }

        public byte[] ReceiveBuffer { get; private set; }

        private StringBuilder ReceivedData = new StringBuilder();

        private Action<ErsTcpConnection> connectedCallback { get; set; }

        private Action<ErsTcpConnection> sentCallback { get; set; }

        private Action<ErsTcpConnection> receivedCallback { get; set; }

        private Exception lastException { get; set; }

        private object lockObject = new object();

        private void ResetState()
        {
            lock (this.lockObject)
            {
                this.lastException = null;
                this.connectedCallback = null;
                this.sentCallback = null;
                this.receivedCallback = null;
            }
        }


        public void CheckResult()
        {
            lock (this.lockObject)
            {
                if (this.lastException != null)
                {
                    var e = lastException;
                    this.ResetState();
                    throw new Exception("error at async.", e);
                }
            }
        }

        /// <summary>
        /// Constractor
        /// </summary>
        /// <param name="encoding"></param>
        public ErsTcpConnection(Encoding encoding)
            : this(encoding, new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
        {
        }

        /// <summary>
        /// Constractor
        /// </summary>
        /// <param name="encoding"></param>
        public ErsTcpConnection(Encoding encoding, Socket socket)
        {
            lock (this.lockObject)
            {
                this.encoding = encoding;
                this.Socket = socket;
                this.ReceiveBuffer = new byte[1024];
            }
        }

        /// <summary>
        /// Connect to endpoint
        /// </summary>
        /// <param name="hostname"></param>
        /// <param name="port"></param>
        /// <param name="connectedCallback"></param>
        internal void BeginConnect(string hostname, int port, Action<ErsTcpConnection> connectedCallback)
        {
            lock (this.lockObject)
            {
                this.ResetState();

                this.connectedCallback = connectedCallback;

                this.Socket.BeginConnect(hostname, port, new AsyncCallback(ConnectCallback), this);
            }
        }

        /// <summary>
        /// Connect callback
        /// </summary>
        /// <param name="ar"></param>
        private void ConnectCallback(IAsyncResult ar)
        {
            // Retrieve the socket from the state object.
            var state = (ErsTcpConnection)ar.AsyncState;

            if (state != null)
            {
                lock (state.lockObject)
                {
                    try
                    {
                        // Complete the connection.
                        state.Socket.EndConnect(ar);
                    }
                    catch (Exception e)
                    {
                        state.lastException = e;
                    }
                    finally
                    {
                        if (state.connectedCallback != null)
                        {
                            // Signal that the connection has been made.
                            state.connectedCallback(state);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// send message to endpoint
        /// </summary>
        /// <param name="data"></param>
        /// <param name="sentCallback"></param>
        public void Send(string data, Action<ErsTcpConnection> sentCallback)
        {
            lock (this.lockObject)
            {
                this.ResetState();

                this.sentCallback = sentCallback;

                // Convert the string data to byte data using encoding.
                byte[] byteData = this.encoding.GetBytes(data);

                // Begin sending the data to the remote device.
                this.Socket.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), this);
            }
        }

        /// <summary>
        /// send message callback
        /// </summary>
        /// <param name="ar"></param>
        private void SendCallback(IAsyncResult ar)
        {
            // Retrieve the socket from the state object.
            var state = (ErsTcpConnection)ar.AsyncState;

            if (state != null)
            {
                lock (state.lockObject)
                {
                    try
                    {
                        // Complete sending the data to the remote device.
                        int bytesSent = state.Socket.EndSend(ar);
                    }
                    catch (Exception e)
                    {
                        state.lastException = e;
                    }
                    finally
                    {
                        if (state.sentCallback != null)
                        {
                            state.sentCallback(state);
                        }
                    }
                }
            }
        }

        public void Receive(Action<ErsTcpConnection> receivedCallback)
        {
            lock (this.lockObject)
            {
                this.ResetState();

                this.receivedCallback = receivedCallback;

                // Begin receiving the data from the remote device.
                this.Socket.BeginReceive(this.ReceiveBuffer, 0, this.ReceiveBuffer.Length, 0, new AsyncCallback(ReceiveCallback), this);
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            // Retrieve the state object and the client socket 
            // from the asynchronous state object.
            var state = (ErsTcpConnection)ar.AsyncState;

            if (state != null)
            {
                lock (state.lockObject)
                {
                    int bytesRead = 0;

                    try
                    {
                        // Read data from the remote device.
                        bytesRead = state.Socket.EndReceive(ar);

                        if (bytesRead > 0)
                        {
                            // There might be more data, so store the data received so far.
                            state.AppendReceivedMessage(state.encoding.GetString(state.ReceiveBuffer, 0, bytesRead));

                            Thread.Sleep(300);
                        }
                    }
                    catch (Exception e)
                    {
                        state.lastException = e;
                    }
                    finally
                    {
                        if (state.lastException == null && state.HasRestReceiveData)
                        {
                            // Get the rest of the data.
                            state.Receive(state.receivedCallback);
                        }
                        else
                        {
                            // Signal that all bytes have been received.
                            if (state.receivedCallback != null)
                            {
                                state.receivedCallback(state);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 受信結果を追加する
        /// </summary>
        /// <returns></returns>
        private void AppendReceivedMessage(string value)
        {
            lock (this.lockObject)
            {
                this.ReceivedData.Append(value);
            }
        }

        /// <summary>
        /// 受信結果を取得する
        /// </summary>
        /// <returns></returns>
        public string GetReceivedMessage()
        {
            lock (this.lockObject)
            {
                var message = this.ReceivedData.ToString();
                this.ReceivedData.Clear();
                return message;
            }
        }

        public bool HasRestReceiveData
        {
            get
            {
                lock (this.lockObject)
                {
                    return this.Socket.Available > 0;
                }
            }
        }

        internal void CloseSocket()
        {
            lock (this.lockObject)
            {
                if (this.Socket != null)
                {
                    // Release the socket.
                    try
                    {
                        this.Socket.Shutdown(SocketShutdown.Both);
                    }
                    catch { }
                    this.Socket.Close();
                    this.Socket = null;
                }
            }
        }
    }
}
