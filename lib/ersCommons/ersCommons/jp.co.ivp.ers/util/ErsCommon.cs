﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using System.Xml;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers
{
	/// <summary>
	/// Contains common functions used for ERS
	/// </summary>
    public class ErsCommon
    {
        public static string[] SetArrayString(params string[] array)
        {
            return array;
        }
        public static string ConcatFullAndUpperFields(string[] array, string separator)
        {
            string retValue = string.Empty;
            foreach (var item in array)
            {
                if (String.IsNullOrEmpty(retValue))
                    retValue = ConvertToFull(item).ToUpper();
                else
                    retValue += ConvertToFull(separator).ToUpper() + ConvertToFull(item).ToUpper();
            }

            return retValue;
        }
        public static string ConvertToFullAlphabet(string value)
        {
            for (int i = 0; i < alphabet_table_wide.Length; i++)
            {
                value = value.Replace(alphabet_table_narrow[i], alphabet_table_wide[i]);
            }
            return value;
        }
        static string sign_table_wide = "、。・」「゛，＜．＞／？＿｝］＊：＋；｛［￣＠｜￥｀＾＝－）（＆％＄＃”！";
        static string sign_table_narrow = "､｡･｣｢ﾞ,<.>/?_}]*:+;{[~@|\\`^=-)(&%$#\"!";
        public static string ConvertToFullSign(string value)
        {
            for (int i = 0; i < sign_table_wide.Length; i++)
            {
                value = value.Replace(sign_table_narrow[i], sign_table_wide[i]);
            }
            return value;
        }
        public static string ConvertToFull(string value)
        {
            value = ErsCommon.ConvertToFullKana(value);
            value = ErsCommon.ConvertToFullAlphabet(value);
            value = ErsCommon.ConvertToFullNumber(value);
            value = ErsCommon.ConvertToFullSign(value);
            return value;
        }
        /// <summary>
		/// <para>指定したファイルの作成日時と現在の日時の差を分で返す（ファイルが存在しない場合は０）</para>
		/// <para>Returns difference between the file's creation date and the current date in minutes. If file does not exist it will return 0</para>
        /// </summary>
        public static long gGetDiffFileCreatedDate(string strFileName)
        {
            if (File.Exists(strFileName) == false)
            {
                return 0;
            }
            TimeSpan ts = DateTime.Now.Subtract(File.GetCreationTime(strFileName));

            return Convert.ToInt64(ts.TotalMinutes);

        }

		/// <summary>
		/// Returns an object value within the dictionary using a specific key
		/// </summary>
        public static object ObjVal(IDictionary<string, object> obj, string key)
        {
            if (obj == null)
                return null;

            if (!obj.ContainsKey(key))
                return null;

            return obj[key];
        }

		/// <summary>
		/// Returns a string value within the dictionary using a specific key
		/// </summary>
        public static string StringVal(IDictionary<string, object> obj, string key)
        {
            var value = ObjVal(obj, key);
            if (value == null)
            {
                return null;
            }
            else
            {
                return Convert.ToString(obj[key]);
            }
        }

		/// <summary>
		/// Returns an integer value within the dictionary using a specific key
		/// </summary>
        public static int? IntVal(IDictionary<string, object> obj, string key)
        {
            var value = ObjVal(obj, key);
            if (value == null)
            {
                return null;
            }
            else
            {
                return Convert.ToInt32(obj[key]);
            }
        }

		/// <summary>
		/// Returns a short value within the dictionary using a specific key
		/// </summary>
        public static short? ShortVal(Dictionary<string, object> obj, string key)
        {
            var value = ObjVal(obj, key);
            if (value == null)
            {
                return null;
            }
            else
            {
                return Convert.ToInt16(obj[key]);
            }
        }

		/// <summary>
		/// Returns a string of delimited joined dictionary values
		///</summary>
        public static string HttpJoinDictionary(Dictionary<string, object> SendValues, string delimiter, string valueDelimiter, Encoding encording = null)
        {
            if (encording == null)
            {
                encording = Encoding.UTF8;
            }

            var retVal = string.Empty;
            foreach (var key in SendValues.Keys)
            {
                if (SendValues[key] == null)
                {
                    continue;
                }

                var value = SendValues[key];
                var type = value.GetType();
                if (!(value is string) &&!type.IsPrimitive && !ErsReflection.IsNullable(type) && !ErsReflection.IsEnum(type))
                {
                    continue;
                }

                var val = Convert.ToString(value);
                retVal += delimiter + key + valueDelimiter + HttpUtility.UrlEncode(val, encording);
            }
            return retVal.Substring(delimiter.Length);
        }

        /// <summary>
		/// Returns a string of delimited joined dictionary values in Bindable Table
		///</summary>
        public static string HttpJoinBindTableDictionary(ErsModelBase model, string delimiter, string valueDelimiter, Encoding encording = null)
        {
            var t = model.GetType();
            var listValue = new List<string>();

            //モデルの項目にあるエラーを列挙
            foreach (var property in t.GetProperties())
            {
                if (property.GetCustomAttributes(typeof(BindTableAttribute), false).Length > 0)
                {
                    var bindTable = property.GetValue(model, null) as IEnumerable<ErsBindableModel>;
                    if (bindTable != null)
                    {
                        foreach (var bindModel in bindTable)
                        {
                            string htmlJoinValue = string.Empty;
                            //For Nested Bindable Table
                            //Returns a string of delimited joined dictionary values Bindable Table
                            htmlJoinValue = HttpJoinBindTableDictionary(bindModel, delimiter, valueDelimiter);
                            if (htmlJoinValue.HasValue())
                                listValue.Add(htmlJoinValue);

                            var SendValues = GetBindModelSendValues(bindModel, property.Name);
                            htmlJoinValue = HttpJoinDictionary(SendValues, delimiter, valueDelimiter, encording);

                            if (htmlJoinValue.HasValue())
                                listValue.Add(htmlJoinValue);
                        }
                    }
                }
            }
            return String.Join(delimiter, listValue.ToArray());
        }

        /// <summary>
        /// Returns New Bindable Form Dictionary
        /// </summary>
        /// <param name="model"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static Dictionary<string,  object> GetBindModelSendValues(ErsBindableModel model, string propertyName)
        {
            string formatKey = "{0}{1}_{2}";

            var retDic = new Dictionary<string, object>();

            foreach (var dic in model.GetPropertiesAsDictionary())
            {
                string newKey = String.Format(formatKey, propertyName, model.lineNumber - 1, dic.Key);
                retDic.Add(newKey, dic.Value);
            }

            return retDic;
        }

        /// <summary>
        /// Overwrite the Dictionary Form in the Bindable Models
        /// </summary>
        /// <param name="model"></param>
        /// <param name="dicForm"></param>
        public static void SetBindTableWithPaypalFormValues(ErsModelBase model, IDictionary<string, object> dicForm)
        {
            var t = model.GetType();
            //モデルの項目にあるエラーを列挙
            foreach (var property in t.GetProperties())
            {
                if (property.GetCustomAttributes(typeof(BindTableAttribute), false).Length > 0)
                {
                    //Overwrite the Dictionary Form in the Bindable Models
                    //▼For Nested Bindable Models
                    var fieldType = property.PropertyType.GetGenericArguments()[0];
                    var bindTable = (ErsBindableModel)Activator.CreateInstance(fieldType);
                    SetBindTableWithPaypalFormValues(bindTable, dicForm);
                    //▲For Nested Bindable Models

                    var newForm = new Dictionary<string, object>();
                    //Filter Contains With Lower Property Name
                    foreach (var record in dicForm)
                    {
                        var propertyName = property.Name.ToLower();
                        if (record.Key.Contains(propertyName))
                            newForm.Add(record.Key.Split(new[] { propertyName }, StringSplitOptions.None)[1], record.Value);
                    }

                    //Set Property checked values
                    object checkedValue;
                    BindTableAttribute bindTableAttr = new BindTableAttribute(property.Name);
                    var resultValues = bindTableAttr.BindProperty(model, newForm, property.Name, property.PropertyType, null, null, out checkedValue);

                    if (checkedValue != null)
                    {
                        ErsReflection.SetProperty(property, model, checkedValue, null);
                    }
                }

            }
        }

        /// <summary>
        /// 結果文字列をDictionaryにする。
        /// </summary>
        /// <param name="retHttp"></param>
        /// <returns></returns>
        public static IDictionary<string, object> GetDictionaryWithHttpString(string retHttp, string delimiter, string valueDelimiter)
        {
            var dic = new ErsIgnoreCaseDictionary();
            var splitData = retHttp.Split(new[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string val in splitData)
            {
                if (val.Contains(valueDelimiter))
                {
                    var arrVal = val.Split(new[] { valueDelimiter }, StringSplitOptions.None);
                    dic.Add(arrVal[0].ToLower(), HttpUtility.UrlDecode(arrVal[1]));
                }
            }
            return (IDictionary<string, object>)dic;
        }

        /// <summary>
        /// 文字列のbyte数を取得
		/// <para>Returns the byte count of the string</para>
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int GetByteCount(string str)
        {
            return ErsEncoding.ShiftJIS.GetByteCount(str);
        }

        /// <summary>
        /// システムエラーログを出力する
		/// <para>Log System errors</para>
        /// </summary>
        /// <param name="message"></param>
        public static void loggingException(string message)
        {
            var setup = new SetupConfigReader();
            var username = setup.logFileUserName;
            var password = setup.logFileUserPassword;
            var doChangeAuth = !string.IsNullOrEmpty(username);

            //権限の偽装
            using (var changelogin = ChangeLogonUserHelper.BeginChange(username, password, doChangeAuth))
            {
                var logger = log4net.LogManager.GetLogger("exceptionLogger");
                logger.Error(message);
            }
        }

        /// <summary>
        /// 値を比較して、変更のあったカラム名のみを返す。
		/// <para>Returns an array of column names which values have been modified</para>
        /// </summary>
        /// <param name="old_obj"></param>
        /// <param name="new_obj"></param>
        /// <returns></returns>
        public static string[] GetChangedKeys(Dictionary<string, object> old_obj, Dictionary<string, object> new_obj)
        {
            ErsDebug.WriteLine("Updating values in update query");

            var list = new List<string>();
            //比較して更新のあったデータのみUPDATE。
            foreach (var key in new_obj.Keys)
            {
                if (old_obj.ContainsKey(key))
                {
                    if (!CompareValue(old_obj[key], new_obj[key]))
                    {
                        ErsDebug.WriteUpdatedValues(key, old_obj[key], new_obj[key]);
                        list.Add(key);
                    }
                }
                else
                {
                    list.Add(key);
                }
            }
            return list.ToArray<string>();
        }

        /// <summary>
        /// conpare value
        /// </summary>
        /// <param name="old_value"></param>
        /// <param name="new_value"></param>
        /// <returns></returns>
        public static bool CompareValue(object old_value, object new_value)
        {
            if (old_value is string && string.IsNullOrEmpty((string)old_value))
            {
                old_value = null;
            }

            if (new_value is string && string.IsNullOrEmpty((string)new_value))
            {
                new_value = null;
            }

            if (old_value == null && new_value == null)
            {
                return true;
            }
            else if (old_value == null || new_value == null)
            {
                return false;
            }
            else if (old_value is Array && new_value is Array)
            {
                var old_arr = (Array)new_value;
                var new_arr = (Array)old_value;
                if (old_arr.Length != new_arr.Length)
                {
                    return false;
                }
                else
                {
                    for (var i = 0; i < new_arr.Length; i++)
                    {
                        if (!CompareValue(old_arr.GetValue(i), new_arr.GetValue(i)))
                            return false;
                    }
                    return true;
                }
            }
            else if (old_value.Equals(new_value))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Dictionaryを上書きする。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj1">上書きされるDictionary</param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        public static Dictionary<string, T> OverwriteDictionary<T>(IDictionary<string, T> baseObject, IDictionary<string, T> obj2)
        {
            Dictionary<string, T> retVal;
            if (baseObject is ErsReflection.ErsModelDictionary<string, T> || obj2 is ErsReflection.ErsModelDictionary<string, T>)
            {
                retVal = new ErsReflection.ErsModelDictionary<string, T>(baseObject);
            }
            else
            {
                retVal = new Dictionary<string, T>(baseObject);
            }
            foreach (var key in obj2.Keys)
            {
                retVal[key] = obj2[key];
            }
            return retVal;
        }

        /// <summary>
        /// RepositoryEntityのListを、List&gt;Dictionary&gt;string, object&lt;&lt;にコンバートする。（表示用）
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<Dictionary<string, object>> ConvertEntityListToDictionaryList<T>(IEnumerable<T> list)
            where T : IErsCollectable
        {
            var ret = new List<Dictionary<string, object>>();

            foreach (var item in list)
            {
                ret.Add(item.GetPropertiesAsDictionary());
            }

            return ret;
        }

        static string zenhan_table_wide = "アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワヲン゛゜ァィゥェォャュョッー　";
        static string zenhan_table_narrow = "ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝﾞﾟｧｨｩｪｫｬｭｮｯｰ ";

        static string dakuon_table_wide = "ガギグゲゴザジズゼゾダヂヅデドバビブベボ";
        static string dakuon_table_narrow = "ｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾊﾋﾌﾍﾎ";
        static string handaku_table_wide = "パピプペポ";
        static string handaku_table_narrow = "ﾊﾋﾌﾍﾎ";

		/// <summary>
		/// Converts string to Full-width Katakana
		/// </summary>
        public static string ConvertToFullKana(string value)
        {
            for (int i = 0; i < dakuon_table_narrow.Length; i++)
            {
                value = value.Replace(dakuon_table_narrow[i] + "ﾞ", Convert.ToString(dakuon_table_wide[i]));
            }
            for (int i = 0; i < handaku_table_narrow.Length; i++)
            {
                value = value.Replace(handaku_table_narrow[i] + "ﾟ", Convert.ToString(handaku_table_wide[i]));
            }
            for (int i = 0; i < zenhan_table_narrow.Length; i++)
            {
                value = value.Replace(zenhan_table_narrow[i], zenhan_table_wide[i]);
            }
            value = value.Replace(" ", "");
            return value;
        }

        static string alphabet_table_wide = "ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ　";
        static string alphabet_table_narrow = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ";

		/// <summary>
		/// Convert a string to Half-width Alphabet
		/// </summary>
        public static string ConvertToHalfAlphabet(string value)
        {
            for (int i = 0; i < alphabet_table_narrow.Length; i++)
            {
                value = value.Replace(alphabet_table_wide[i], alphabet_table_narrow[i]);
            }
            return value;
        }

        static string number_table_wide = "１２３４５６７８９０－　";
        static string number_table_narrow = "1234567890- ";

		/// <summary>
		/// Converts string to Half Numeric
		/// </summary>
        public static string ConvertToHalfNumber(string value)
        {
            for (int i = 0; i < number_table_narrow.Length; i++)
            {
                value = value.Replace(number_table_wide[i], number_table_narrow[i]);
            }
            return value;
        }

        public static string ConvertToFullNumber(string value)
        {
            for (int i = 0; i < number_table_narrow.Length; i++)
            {
                value = value.Replace(number_table_narrow[i], number_table_wide[i]);
            }
            return value;
        }

        /// <summary>
        /// Regex用エスケープを行う [Escape characters for Regex]
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string EscapeForRegex(string value)
        {
            // Escape characters for Regex
            string[] strEscape = new string[] { "*", "+", ".", "?", "{", "}", "(", ")", "[", "]", "^", "$", "-", "|", "/" };

            string escaped = value.Replace("\\", "\\\\");
            foreach (var str in strEscape)
            {
                escaped = escaped.Replace(str, "\\" + str);
            }

            return escaped;
        }

        /// <summary>
        /// XMLからkeyとvalueを読み込む
		/// <para>Reads the key and values taken from the XML file</para>
        /// </summary>
        /// <returns></returns>
        internal static Dictionary<string, string> LoadXml(string path)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            var xmlDoc = new XmlDocument();

            xmlDoc.Load(path);
            var xmlElement = xmlDoc.DocumentElement;
            foreach (XmlNode msgNode in xmlElement.ChildNodes)
            {
                if (msgNode is XmlElement)
                {
                    var msgElement = (XmlElement)msgNode;
                    string name = msgElement.GetAttribute("name");
                    string value = msgElement.InnerText;
                    ret[name] = value;
                }
            }
            return ret;
        }

        /// <summary>
        /// 名前からイニシャル作成
		/// <para>Creates an initials for a given name</para>
        /// </summary>
        /// <param name="fname"></param>
        /// <param name="lname"></param>
        /// <returns></returns>
        public static string MakeInitial(string fname, string lname)
        {
            string ret = "";
            Dictionary<string, string> chk_dic;
            chk_dic = new Dictionary<string,string>();
            chk_dic["A"] = "ア";
            chk_dic["I"] = "イ";
            chk_dic["U"] = "ウ";
            chk_dic["E"] = "エ";
            chk_dic["O"] = "オ";
            chk_dic["K"] = "カキクケコ";
            chk_dic["S"] = "サシスセソ";
            chk_dic["T"] = "タチツテト";
            chk_dic["N"] = "ナニヌネノ";
            chk_dic["H"] = "ハヒフヘホ";
            chk_dic["M"] = "マミムメモ";
            chk_dic["Y"] = "ヤユヨ";
            chk_dic["R"] = "ラリルレロ";
            chk_dic["W"] = "ワヰヱヲ";
            chk_dic[""] = "ン";
            //念の為全角化
            fname = VBStrings.FullKana(fname);
            lname = VBStrings.FullKana(lname);

            if (fname != null)
            {
                foreach (string key in chk_dic.Keys)
                {
                    if (chk_dic[key].Contains(VBStrings.Left(fname, 1)))
                    {
                        ret = key + ".";
                        break;
                    }
                }
            }
            if (lname != null)
            {
                foreach (string key in chk_dic.Keys)
                {
                    if (chk_dic[key].Contains(VBStrings.Left(lname, 1)))
                    {
                        ret = ret + key + ".";
                        break;
                    }
                }
            }


            return ret;
        }


        /// <summary>
        /// 日付に時刻追加
        /// </summary>
        /// <param name="date">日付文字列</param>
        /// <param name="mode">mode(0:最小値/1:最大値)</param>
        /// <returns></returns>
        public static DateTime SetDateTimeFormat(DateTime date, int mode)
        {
            string cnst_start_time = "00:00:00"; 
            string cnst_end_time = "23:59:59";

            DateTime ret_date = new DateTime();
            if (date != null)
            {
                ret_date = date;
                //初期値の場合
                if (ret_date.Hour == 0 &&
                    ret_date.Minute == 0 &&
                        ret_date.Second == 0)
                {
                    switch (mode)
                    {
                        case 0:
                            ret_date = DateTime.Parse(ret_date.ToString("yyyy/MM/dd " + cnst_start_time));
                            break;

                        case 1:
                            ret_date = DateTime.Parse(ret_date.ToString("yyyy/MM/dd " + cnst_end_time));
                            break;
                    }
                }
            }
            return ret_date;
        }

        //20120919:全角カナ→半角カナ
        public static string ConvertToHalfKana(string value)
        {
            for (int i = 0; i < dakuon_table_wide.Length; i++)
            {
                //value = value.Replace(dakuon_table_narrow[i] + "ﾞ", Convert.ToString(dakuon_table_wide[i]));
                value = value.Replace(Convert.ToString(dakuon_table_wide[i]), Convert.ToString(dakuon_table_narrow[i]) + "ﾞ");
            }

            for (int i = 0; i < handaku_table_wide.Length; i++)
            {
                //value = value.Replace(handaku_table_narrow[i] + "ﾟ", Convert.ToString(handaku_table_wide[i]));
                value = value.Replace(Convert.ToString(handaku_table_wide[i]), Convert.ToString(handaku_table_narrow[i]) + "ﾟ");
            }

            for (int i = 0; i < zenhan_table_wide.Length; i++)
            {
                value = value.Replace(zenhan_table_wide[i], zenhan_table_narrow[i]);
            }
            //20121018スペースは置換対象外 by onoe
            //value = value.Replace(" ", "");
            return value;
        }

        //20120919:記号以外を半角
        public static string ConvertToHalfChr(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                value = ConvertToHalfAlphabet(value);
                value = ConvertToHalfNumber(value);
                value = ConvertToHalfKana(value);
            }

            return value;
        }

        /// <summary>
        /// 文字列指定バイト数分割
        /// </summary>
        /// <param name="inStr"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string[] mbStrSplit(string inStr, int length)
        {
            List<string> outArray = new List<string>(); // 分割結果の保存領域
            string outStr = "";                 // 現在処理中の分割後文字列
            Encoding enc = ErsEncoding.ShiftJIS;


            // パラメータチェック
            if (inStr == null || length < 1)
            {
                return outArray.ToArray();
            }

            //--------------------------------------
            // 全ての文字を処理するまで繰り返し
            //--------------------------------------
            for (int offset = 0; offset < inStr.Length; offset++)
            {
                //----------------------------------------------------------
                // 今回処理する文字と、その文字を含めた分割後文字列長を取得
                //----------------------------------------------------------
                string curStr = inStr[offset].ToString();
                int curTotalLength = enc.GetByteCount(outStr) + enc.GetByteCount(curStr);

                //-------------------------------------
                // この文字が、分割点になるかチェック
                //-------------------------------------
                if (curTotalLength == length)
                {
                    // 処理中の文字を含めると、ちょうどピッタリ
                    outArray.Add(outStr + curStr);
                    outStr = "";
                }
                else if (curTotalLength > length)
                {
                    // 処理中の文字を含めると、あふれる
                    outArray.Add(outStr);
                    outStr = curStr;
                }
                else
                {
                    // 処理中の文字を含めてもまだ余裕あり
                    outStr += curStr;
                }
            }

            // 最後の行の文を追加する
            if (!outStr.Equals(""))
            {
                outArray.Add(outStr);
            }

            // 分割後データを配列に変換して返す
            return outArray.ToArray();
        }

        /// <summary>
        /// UNICODE文字変換（Windows用）
        /// </summary>
        /// <param name="src">変換文字列</param>
        /// <returns>変換後文字列</returns>
        public static string ConvertUnicodeCharacterForWin(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            // "〜" ⇔ "～" 等
            char[][] replaceChar = new[] { 
                new[] { (char)0x2212, (char)0xFF0D },
                new[] { (char)0x301C, (char)0xFF5E },
                new[] { (char)0x00A2, (char)0xFFE0 },
                new[] { (char)0x00A3, (char)0xFFE1 },
                new[] { (char)0x00AC, (char)0xFFE2 },
                new[] { (char)0x2014, (char)0x2015 },
                new[] { (char)0x2016, (char)0x2225 },
            };

            foreach (var arrchar in replaceChar)
            {
                value = value.Replace(arrchar[0], arrchar[1]);
            }

            return value;
        }
        public static string ConvertToFullAndUpperString(string value)
        {
            value = value.ToUpper();
            value = ErsCommon.ConvertToFullKana(value);
            value = ErsCommon.ConvertToFullAlphabet(value);
            value = ErsCommon.ConvertToFullNumber(value);
            value = ErsCommon.ConvertToFullSign(value);
            return value;
        }
    }
}
