﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumEck
    {
        /// <summary>
        /// 0
        /// </summary>
        Normal,

        /// <summary>
        /// 1
        /// </summary>
        Error,

        /// <summary>
        /// 2
        /// </summary>
        BackPage
    }
}
