﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc.MapperProcessor.Mapper
{
    public interface IMapper<TMappable>
        where TMappable : IMappable
    {
        void Map(TMappable objMappable);
    }
}
