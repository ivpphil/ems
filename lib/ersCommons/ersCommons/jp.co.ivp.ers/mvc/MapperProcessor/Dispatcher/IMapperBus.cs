﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc.MapperProcessor.Dispatcher
{
    public interface IMapperBus
    {
        void Map<TMappable>(TMappable mappable) where TMappable : IMappable;
    }
}
