﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace jp.co.ivp.ers.mvc.MapperProcessor.Dispatcher
{
    public class DefaultMapperBus
        : IMapperBus
    {

        public void Map<TMappable>(TMappable objMappable)
            where TMappable : IMappable
        {
            var mapper = DependencyResolver.Current.GetService<IMapper<TMappable>>();
            if (mapper == null || !(mapper is IMapper<TMappable>))
            {
                throw new MapperNotFoundException(typeof(TMappable));
            }
            mapper.Map(objMappable);
        }
    }
}
