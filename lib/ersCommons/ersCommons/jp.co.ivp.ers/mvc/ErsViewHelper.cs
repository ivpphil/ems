﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using jp.co.ivp.ers.util;
using System.Web;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.mvc.template;
using System.Reflection;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc.htmlExpand;

namespace jp.co.ivp.ers.mvc
{
    /// <summary>
    /// このクラスはコンパイルされたViewから呼ばれる。プログラムからは呼ばない。（プログラムからはErsViewContext使う）
    /// <para>This class has been called by the compiled view. Does not called from the program. （ErsViewContex）  </para>
    /// </summary>
    public class ErsViewHelper
    {
        /// <summary>
        /// テンプレートコンパイル時のTextWriter
        /// <para>Textwriter for template compilation</para>
        /// </summary>
        public static TextWriter TextWriter
        {
            private get
            {
                return (TextWriter)ErsCommonContext.GetPooledObject("TextWriter");
            }
            set
            {
                ErsCommonContext.SetPooledObject("TextWriter", value);
            }
        }

        /// <summary>
        /// ビュー用データ
        /// <para>Data for view</para>
        /// </summary>
        public static ViewDataDictionary ViewData
        {
            get
            {
                return (ViewDataDictionary)ErsCommonContext.GetPooledObject("ViewData");
            }
            set
            {
                ErsCommonContext.SetPooledObject("ViewData", value);
            }
        }

        public static readonly string TAG_NEW_LINE = "<br />";

        /// <summary>
        /// Modelの値を取得するC#のCodeを出力する
        /// <para>Output of C# code to get the value of the model</para>
        /// </summary>
        /// <param name="baseVariable"></param>
        /// <returns></returns>
        public static string GetViewVariableString(string baseVariable)
        {
            if (baseVariable.Contains('('))
            {
                //関数呼び出しは変換しない（すでに行われているものと想定）
                return baseVariable;
            }

            var arrVal = SplitVariableString(baseVariable);

            string variable = arrVal[0];
            if (string.IsNullOrEmpty(variable))
                variable = "ErsViewHelper.ViewData";

            var regex = new Regex("^(.+)\\[(.+?)\\]$");

            for (int i = 1; i < arrVal.Count; i++)
            {
                var propertyName = arrVal[i].Trim();

                //Modify make be able to show array.
                string arrayKeys = string.Empty;
                var match = regex.Match(propertyName);
                if (match.Groups[2].Success)
                {
                    propertyName = match.Groups[1].Value;
                    arrayKeys = match.Groups[2].Value;
                }

                variable = "ErsViewHelper.GetVariableValue(" + variable + ", \"" + propertyName + "\")";

                if (!string.IsNullOrEmpty(arrayKeys))
                {
                    variable = "ErsViewHelper.GetSpecifiedKeyValue(\"" + propertyName + "\", " + variable + ", new[] {" + GetArrayKeys(arrayKeys) + "})";
                }
            }
            return variable;
        }

        /// <summary>
        /// Split string to variable in consideration of Array/IList/IDictionary.
        /// </summary>
        /// <param name="baseVariable"></param>
        /// <returns></returns>
        public static List<string> SplitVariableString(string baseVariable)
        {
            var arrVariable = baseVariable.Split('.');
            var resultList = new List<string>();
            var tempString = string.Empty;
            foreach (var variable in arrVariable)
            {
                var strVariable = variable.Trim();
                if (strVariable.Contains('[') || !string.IsNullOrEmpty(tempString))
                {
                    tempString += "." + strVariable;
                    if (strVariable.Contains(']'))
                    {
                        resultList.Add(tempString.Substring(1));
                        tempString = string.Empty;
                    }
                }
                else
                {
                    resultList.Add(strVariable);
                }
            }
            return resultList;
        }

        /// <summary>
        /// Get value of specified property name from specified object.
        /// </summary>
        /// <param name="variableValue"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static object GetVariableValue(object variableValue, string propertyName)
        {
            propertyName = propertyName.Trim();
            if (variableValue == null)
            {
                return null;
            }

            if (variableValue is IDictionary<string, object>)
            {
                var dictionary = (IDictionary<string, object>)variableValue;
                if (dictionary.ContainsKey(propertyName))
                {
                    return dictionary[propertyName];
                }
                else
                {
                    return GetCachedPropertiesValue(variableValue, propertyName);
                }
            }
            else if (variableValue is IDictionary)
            {
                var dictionary = (IDictionary)variableValue;
                if (dictionary.Contains(propertyName))
                {
                    return dictionary[propertyName];
                }
                else
                {
                    return GetCachedPropertiesValue(variableValue, propertyName);
                }
            }
            else
            {
                return GetCachedPropertiesValue(variableValue, propertyName);
            }
        }

        private static object GetCachedPropertiesValue(object variableValue, string propertyName)
        {
            var PooledObject = (Dictionary<object, Dictionary<object, object>>)ErsCommonContext.GetPooledObject("ErsViewHelper_GetCachedPropertiesAsDictionary");
            if (PooledObject == null)
            {
                PooledObject = new Dictionary<object, Dictionary<object, object>>();
                ErsCommonContext.SetPooledObject("ErsViewHelper_GetCachedPropertiesAsDictionary", PooledObject);
            }

            Dictionary<object, object> pooledProperties = null;
            if (PooledObject.ContainsKey(variableValue))
            {
                pooledProperties = PooledObject[variableValue];
            }
            else
            {
                pooledProperties = new Dictionary<object, object>();
                PooledObject.Add(variableValue, pooledProperties);
            }

            if (!pooledProperties.ContainsKey(propertyName))
            {
                var property = ErsReflection.GetProperty(variableValue, propertyName);
                if (property == null || !property.CanRead)
                {
                    pooledProperties.Add(propertyName, null);
                }
                else
                {
                    object actualValue;

                    var model = variableValue as ErsModelBase;
                    if (!IsValidField(propertyName, model, property))
                    {
                        actualValue = model.InvalidValues[propertyName];
                    }
                    else
                    {
                        var propValue = property.GetValue(variableValue, null);
                        actualValue = ErsReflection.GetPropertyValueForErs(property, propValue);
                    }

                    pooledProperties.Add(propertyName, actualValue);
                }
            }

            return pooledProperties[propertyName];
        }

        /// <summary>
        /// 不正な値かをチェック
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        private static bool IsValidField(string propertyName, ErsModelBase model, PropertyInfo property)
        {
            if (model != null && !model.IsValid && !model.IsValidField(propertyName) && model.InvalidValues.ContainsKey(propertyName))
            {
                //エラーの場合は、false (BindTableのプロパティは、BindTableにまかせる。)
                var attributes = property.GetCustomAttributes(false).OfType<BindTableAttribute>();
                if (attributes.Count() == 0)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Parse array indexs in consideration of variable.
        /// </summary>
        /// <param name="arrayKeys"></param>
        /// <returns></returns>
        public static string GetArrayKeys(string arrayKeys)
        {
            var keys = arrayKeys.Split(',');
            var resultKeys = string.Empty;
            foreach (var strKey in keys)
            {
                long intKey;
                if (!Int64.TryParse(Convert.ToString(strKey), out intKey))
                {
                    //If the type of key is not long, regards it as variable.
                    resultKeys += "," + GetViewVariableString(strKey);
                }
                else
                {
                    resultKeys += "," + strKey;
                }
            }
            return resultKeys.Substring(1);
        }

        /// <summary>
        /// Get specified index value.
        /// </summary>
        /// <param name="propertyValue"></param>
        /// <param name="arrKeys"></param>
        /// <returns></returns>
        public static object GetSpecifiedKeyValue(string variableName, object propertyValue, object[] arrKeys)
        {
            if (propertyValue is Array)
            {
                return GetArrayValue((Array)propertyValue, arrKeys);
            }
            else if (propertyValue is IList)
            {
                return GetIListValue((IList)propertyValue, arrKeys);
            }
            else
            {
                return GetIDictionaryValue(propertyValue, arrKeys);
            }
        }

        /// <summary>
        /// Get specified key value of IDictionary.
        /// </summary>
        /// <param name="propertyValue"></param>
        /// <param name="arrKeys"></param>
        /// <returns></returns>
        private static object GetIDictionaryValue(object propertyValue, object[] arrKeys)
        {
            if (arrKeys.Length != 1)
            {
                return null;
            }

            return GetVariableValue(propertyValue, Convert.ToString(arrKeys[0]));
        }

        /// <summary>
        /// Get specified index value of IList.
        /// </summary>
        /// <param name="propertyValue"></param>
        /// <param name="indexs"></param>
        /// <returns></returns>
        private static object GetIListValue(IList propertyValue, object[] indexs)
        {
            if (indexs.Length != 1)
            {
                return null;
            }
            int index;
            if (!Int32.TryParse(Convert.ToString(indexs[0]), out index))
            {
                //If the index is not int, return null;
                return null;
            }

            try
            {
                return propertyValue[index];
            }
            catch (IndexOutOfRangeException)
            {
                //if the index is out of range, return null;
                return null;
            }
        }

        /// <summary>
        /// Get specified index value of Array.
        /// </summary>
        /// <param name="propertyValue"></param>
        /// <param name="indexs"></param>
        /// <returns></returns>
        private static object GetArrayValue(Array propertyValue, object[] indexs)
        {
            var listLong = new List<long>();
            foreach (var strIndex in indexs)
            {
                long index;
                if (!Int64.TryParse(Convert.ToString(strIndex), out index))
                {
                    //If the index is not long, return null;
                    return null;
                }
                listLong.Add(index);
            }

            //if ther is no index, return null;
            if (listLong.Count == 0)
            {
                return null;
            }

            try
            {
                return propertyValue.GetValue(listLong.ToArray());
            }
            catch (IndexOutOfRangeException)
            {
                //if the index is out of range, return null;
                return null;
            }
        }

        /// <summary>
        /// セッションを付加して良いURLかをチェック
        /// <para>Check the session URL</para>
        /// </summary>
        /// <param name="p"></param>
        public static bool CheckSessionUrl(string url)
        {
            url = url.ToLower();

            //JavaScriptはNG
            if (url.StartsWith("javascript"))
                return false;

            //絶対パスはOK
            if (url.StartsWith("/"))
                return true;

            //sec_urlの値はOK
            var sec_url = new SetupConfigReader().sec_url.ToLower();
            if (url.StartsWith(sec_url))
                return true;

            //それ以外で、URLの場合はアウト
            if (url.StartsWith("http://") || url.StartsWith("https://"))
                return false;

            //それ以外の相対パスはOK
            return true;
        }

        /// <summary>
        /// ERSのURLかを判定
        /// <para>Check ERS URL</para>
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static bool CheckErsUrl(string url)
        {
            url = url.ToLower();

            //JavaScriptはNG
            if (url.StartsWith("javascript"))
                return false;

            //絶対パスはOK
            if (url.StartsWith("/"))
                return true;

            var setup = new SetupConfigReader();

            //nor_urlの値はOK
            var nor_url = setup.nor_url;
            if (!string.IsNullOrWhiteSpace(nor_url) && url.StartsWith(nor_url.ToLower()))
            {
                return true;
            }

            //sec_urlの値はOK
            var sec_url = setup.sec_url;
            if (!string.IsNullOrWhiteSpace(sec_url) && url.StartsWith(sec_url.ToLower()))
            {
                return true;
            }

            //それ以外で、URLの場合はアウト
            if (url.StartsWith("http://") || url.StartsWith("https://"))
                return false;

            //それ以外の相対パスはOK
            return true;
        }

        /// <summary>
        /// &lt;%=***%&gt;を'" + *** + "'に展開する
        /// <para>Expand &lt;%=***%&gt;を'" + *** + "'</para>
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string ExpandVariable(string code)
        {
            string pattern = "<%=\\s*([^()%]+)\\s*%>";
            MatchCollection mc = new Regex(pattern).Matches(code);
            foreach (Match m in mc)
            {
                code = code.Replace(m.Value, "\" + " + ErsViewHelper.GetViewVariableString(m.Groups[1].Value) + " + \"");
            }

            pattern = "<%=\\s*([^%]+)\\s*%>";
            mc = new Regex(pattern).Matches(code);
            foreach (Match m in mc)
            {
                code = code.Replace(m.Value, "\" + (" + m.Groups[1].Value + ") + \"");
            }
            return code;
        }

        /// <summary>
        /// 絶対パスに置き換える。
        /// </summary>
        /// <param name="url"></param>
        public static void ConvertToAbsoluteUri(string url)
        {
            string resultUrl = url;
            var checkUrl = url.ToLower();

            //Parameter Manipulation の脆弱性への対応
            //Hostを書き換えられると、別サイトへ誘導されてしまうので、setup.configのドメインと同一化をチェックし、同一の場合のみ書き換えるように修正
            Uri configUrl;
            var setup = new SetupConfigReader();
            if (ErsCommonContext.IsBatch)
            {
                configUrl = null;
            }
            else if (HttpContext.Current.Request.IsSecureConnection)
            {
                configUrl = new Uri(setup.sec_url);
            }
            else
            {
                configUrl = new Uri(setup.nor_url);
            }

            if (!checkUrl.StartsWith("http://") && !checkUrl.StartsWith("https://") && !checkUrl.StartsWith("?") && !checkUrl.StartsWith("#") && !checkUrl.StartsWith("mailto") && !checkUrl.StartsWith("javascript") && configUrl != null && configUrl.Host == HttpContext.Current.Request.Url.Host)
            {
                //URLからアンカー抽出
                var arrUrl = url.Split(new[] { '#' });
                var anchor = (arrUrl.Length > 1) ? arrUrl[1] : string.Empty;

                //URLからパラメータ抽出
                arrUrl = arrUrl[0].Split(new[] { '?' });
                var parameters = (arrUrl.Length > 1) ? arrUrl[1] : string.Empty;

                //基本URL
                var baseUrl = arrUrl[0];

                //遷移先に拡張子が含まれるかチェック(拡張子が含まれないのは、遷移先URLとしてただしくない)
                var targetUrl = baseUrl.Split('/');
                var targetFileName = targetUrl[targetUrl.Length - 1];

                if (!string.IsNullOrEmpty(baseUrl) && (string.IsNullOrEmpty(targetFileName) || targetFileName.Contains('.')))
                {
                    //現在のURLを取得
                    var objUrl = HttpContext.Current.Request.Url;
                    var requestedUrl = objUrl.AbsoluteUri.Split('?', '#')[0];
                    if (configUrl != null)
                    {
                        requestedUrl = requestedUrl.Replace(objUrl.Authority, configUrl.Authority);
                        requestedUrl = requestedUrl.Replace(objUrl.Scheme, configUrl.Scheme);
                    }

                    if (baseUrl.StartsWith("/"))
                    {
                        //絶対パス用
                        resultUrl = requestedUrl.Replace(objUrl.AbsolutePath, baseUrl);
                    }
                    else
                    {
                        //相対パス
                        var fileName = objUrl.Segments[objUrl.Segments.Length - 1];
                        if (fileName.Contains('.'))
                        {
                            resultUrl = requestedUrl.Replace(objUrl.AbsolutePath, string.Join(string.Empty, objUrl.Segments, 0, objUrl.Segments.Length - 1) + baseUrl);
                        }
                        else
                        {
                            if (requestedUrl.EndsWith("/"))
                            {
                                resultUrl = requestedUrl + baseUrl;
                            }
                            else
                            {
                                resultUrl = objUrl.AbsoluteUri + "/" + baseUrl;
                            }
                        }

                        //SNIが有効な場合は、ポートは除外する。（ポート指定でアクセスされるため）
                        if (new SetupConfigReader().enableSni)
                        {
                            resultUrl = resultUrl.Replace(":" + objUrl.Port, string.Empty);
                        }
                    }

                    if (!string.IsNullOrEmpty(parameters))
                    {
                        resultUrl += "?" + parameters;
                    }
                    if (!string.IsNullOrEmpty(anchor))
                    {
                        resultUrl += "#" + anchor;
                    }
                }
            }

            ErsViewHelper.WriteValue(resultUrl);
        }

        /// <summary>
        /// オブジェクトをエンコード済みのHTML表記に変換する。
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string HtmlEncode(object o)
        {
            return HttpUtility.HtmlEncode(HtmlExpander.ExpandValue(o));
        }

        /// <summary>
        /// オブジェクトをエンコードしてViewへ出力する
        /// </summary>
        /// <param name="o"></param>
        public static void WriteValue(object o)
        {
            ErsViewHelper.Write(HtmlEncode(o));
        }

        /// <summary>
        /// オブジェクトをエンコードなしでViewへ出力する
        /// </summary>
        /// <param name="value"></param>
        public static void Write(object value)
        {
            ErsViewHelper.TextWriter.Write(value);
        }
    }
}