﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.htmlExpand;

namespace jp.co.ivp.ers.mvc
{
    /// <summary>
    /// hiddenタグを出力するプロパティを指定します。
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ErsOutputHiddenAttribute
        :Attribute
    {

        public static readonly string DEFAULT_GROUP_NAME = "default";

        public const string PROCESS_COMPLETION_CHECK_RANSU_KEY = "process_completion_check_ransu";

        protected internal List<string> groupNames = new List<string>();

        public string OutputDateFormat { get; set; }

        /// <summary>
        /// ModelのOutputHidden時に出力する（規定のグループ）
		/// <para>output of the Model OutputHidden (add a default group)</para>
        /// </summary>
        public ErsOutputHiddenAttribute()
        {
            //全てのOutputHiddenはdefaultに所属する。
            this.groupNames.Add(DEFAULT_GROUP_NAME);
        }

        /// <summary>
        /// ModelのOutputHidden時に出力する（規定のグループと指定されたグループに所属。）
		/// <para>output of the Model OutputHidden (Belong to a specified group)</para>
        /// </summary>
        public ErsOutputHiddenAttribute(params string[] groupNames)
            : this()
        {
            foreach (var name in groupNames)
            {
                this.groupNames.Add(name);
            }
        }

        internal object FormatValue(PropertyInfo property, object value)
        {
            var formatValue = ErsReflection.GetPropertyValueForErs(property, value);
            if (!string.IsNullOrEmpty(this.OutputDateFormat) && formatValue != null)
            {
                DateTime dateValue;
                if (DateTime.TryParse(Convert.ToString(formatValue), out dateValue))
                {
                    formatValue = string.Format("{0:" + this.OutputDateFormat + "}", dateValue);
                }
            }
            return formatValue;
        }
    }
}
