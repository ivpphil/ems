﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.Reflection;
using System.Collections.Specialized;


namespace jp.co.ivp.ers.mvc
{
    abstract public class ErsState
    {
        protected ErsBindableModel model;

        public ErsState()
        {
            this.model = this.GetModel();
        }

        protected internal abstract ErsBindableModel GetModel();
        protected abstract int? GetCookieExpiration(PropertyInfo prop);
        protected abstract string GetCookieDomain();
        protected abstract string GetCookiePath();
        protected abstract EnumRequireHttps GetEnableSSL();

        /// <summary>
        /// sessionのprefix
        /// </summary>
        protected virtual string prefix
        {
            get
            {
                var setup = new SetupConfigReader();
                return setup.cookieSessionPrefix;
            }
        }

        public ValueSourceAttribute GetVALUE_SOURCE(PropertyInfo prop)
        {
            var attrs = prop.GetCustomAttributes(typeof(ValueSourceAttribute), false);
            if (attrs.Length == 0)
            {
                return null;
            }
            return ((ValueSourceAttribute)attrs[0]);
        }

        /// <summary>
        /// 値を復元する / Restore the value
        /// </summary>
        /// <param name="request"></param>
        public virtual void Restore(HttpCookieCollection Cookies,
            NameValueCollection Form,
            NameValueCollection QueryString)
        {
            //取得する値を指定する。/ Specifies the value to be retrieved
            var pair = new Dictionary<string, object>();

            foreach (var prop in model.GetType().GetProperties())
            {
                string session_key = prefix + prop.Name;
                var valueSource = GetVALUE_SOURCE(prop);
                if (valueSource == null)
                {
                    continue;
                }

                switch (valueSource.VALUE_SOURCE)
                {
                    case VALUE_SOURCE.COOKIES:
                        if (Cookies[session_key] != null)
                            pair.Add(session_key.Substring(prefix.Length), Cookies[session_key].Value);
                        break;
                    case VALUE_SOURCE.FORM:
                        if (Form[session_key] != null)
                            pair.Add(session_key.Substring(prefix.Length), Form[session_key]);
                        break;
                    case VALUE_SOURCE.QUERY_STRING:
                        if (QueryString[session_key] != null)
                            pair.Add(session_key.Substring(prefix.Length), QueryString[session_key]);
                        break;
                    case VALUE_SOURCE.FORM_QUERY_STRING:
                        if (Form[session_key] != null)
                            pair.Add(session_key.Substring(prefix.Length), Form[session_key]);
                        else if (QueryString[session_key] != null)
                            pair.Add(session_key.Substring(prefix.Length), QueryString[session_key]);
                        break;
                    default:
                        throw new Exception("未実装");
                }
            }

            ErsBindModel.ModelBind(model, pair, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Add(string key, object value)
        {
            var property = this.model.GetType().GetProperty(key);
            if (property == null)
            {
                //throw new Exception("The specified key '" + key + "' is not defined in " + model.GetType().Name + ".");
                return false;
            }

            var attributes = property.GetCustomAttributes(false).OfType<Attribute>();

            var validators = ErsBindModel.GetValidators(attributes);
            if (validators.Count() == 0)
            {
                //検証クラスが定義されていないクラスにはバインドしない
                ErsReflection.SetProperty(property, model, null, null);
                return false;
            }

            object checkedValue;

            var result = ErsBindModel.Validate(model, value, property, out checkedValue);

            if (result == null || result.Count() > 0)
            {
                ErsReflection.SetProperty(property, model, null, null);
                return false;
            }
            else
            {
                var actualValue = ErsBindModel.GetConcreteValue(property.PropertyType, checkedValue);
                ErsReflection.SetProperty(property, model, actualValue, null);
                return true;
            }
        }

        public string Get(string key)
        {
            if (this.model.GetType().GetProperty(key) == null)
            {
                return null;
            }
            var value = Convert.ToString(this.model.GetType().GetProperty(key).GetValue(model, null));
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            return value;
        }

        public T Get<T>(string key)
        {
            if (this.model.GetType().GetProperty(key) == null)
            {
                return default(T);
            }
            var value = this.model.GetType().GetProperty(key).GetValue(model, null);
            if (value == null)
            {
                return default(T);
            }
            return (T)value;
        }

        /// <summary>
		/// フォームから取得した値のみセットする。/ Only set the value obtained from the form.
        /// </summary>
        /// <param name="list"></param>
        public virtual void OutputHidden(List<Dictionary<string, object>> list, bool isSecure)
        {
            foreach (var prop in model.GetType().GetProperties())
            {
                var valueSource = GetVALUE_SOURCE(prop);
                if (valueSource == null)
                    continue;

                if (isSecure == valueSource.IsSecure && (valueSource.VALUE_SOURCE == VALUE_SOURCE.FORM || valueSource.VALUE_SOURCE == VALUE_SOURCE.FORM_QUERY_STRING))
                {
                    var dic = new Dictionary<string, object>();
                    dic["name"] = prefix + prop.Name;
                    dic["value"] = Convert.ToString(prop.GetValue(model, null));
                    list.Add(dic);
                }
            }
        }

        /// <summary>
        /// Cookieに値をセットする / Set the value of the cookie
        /// </summary>
        /// <returns></returns>
        public void SetCookiesValue()
        {
            foreach (var prop in model.GetType().GetProperties())
            {
                var cookieDomain = this.GetCookieDomain();
                var cookiePath = this.GetCookiePath();
                var cookieExpiration = this.GetCookieExpiration(prop);

                var valueSource = GetVALUE_SOURCE(prop);
                if (valueSource == null)
                    continue;

                if (valueSource.VALUE_SOURCE == VALUE_SOURCE.COOKIES && 
                    (!valueSource.IsSecure
                    || this.GetEnableSSL() == EnumRequireHttps.valFalse
                    || HttpContext.Current.Request.IsSecureConnection))
                {
                    var cookieSource = (ValueSourceCookieAttribute)valueSource;

                    var cookie = HttpContext.Current.Response.Cookies.Get(prefix + prop.Name);
                    if (cookie == null)
                    {
                        cookie = new HttpCookie(prefix + prop.Name);

                        HttpContext.Current.Response.Cookies.Add(cookie);
                    }

                    cookie.Value = Convert.ToString(prop.GetValue(model, null));

                    if (!string.IsNullOrEmpty(cookieDomain))
                    {
                        cookie.Domain = cookieDomain;
                    }
                    if (!string.IsNullOrEmpty(cookiePath))
                    {
                        cookie.Path = cookiePath;
                    }
                    if (cookieExpiration != null)
                    {
                        cookie.Expires = DateTime.Now.AddMinutes(cookieExpiration.Value);
                    }

                    cookie.Secure = (this.GetEnableSSL() == EnumRequireHttps.Required || (this.GetEnableSSL() == EnumRequireHttps.valTrue && cookieSource.IsSecure));

                    cookie.HttpOnly = cookieSource.IsHttpOnly;
                }
            }
        }

        /// <summary>
        /// 保持情報をクリアする / To clear the retention information
        /// </summary>
        public virtual void ClearLoginData()
        {
            foreach (var prop in this.model.GetType().GetProperties())
            {
                if (prop.GetCustomAttributes(false).OfType<NotLoginDataAttribute>().Count() == 0)
                {
                    this.SetDefaultValue(prop);
                }
            }
        }

        private void SetDefaultValue(PropertyInfo prop)
        {
            if(!prop.CanWrite)
                return;

            if (prop.PropertyType.IsValueType)
            {
                prop.SetValue(this.model, Activator.CreateInstance(prop.PropertyType), null);
            }
            else
            {
                prop.SetValue(this.model, null, null);
            }
        }

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            foreach (var prop in model.GetType().GetProperties())
            {
                yield return new KeyValuePair<string, string>(prop.Name, Convert.ToString(prop.GetValue(model, null)));
            }
        }


    }
}
