﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.compile.entity;

namespace jp.co.ivp.ers.mvc.compile
{
    public class ErsViewCompilerFactory
    {
        /// <summary>
        /// メール用のテンプレートパーサーを取得する。
		/// <para>get the template parser for Mail</para>
        /// </summary>
        /// <param name="viewContext"></param>
        /// <returns></returns>
        public ErsMailParser GetErsMailParser(ErsViewContext viewContext, Encoding enc)
        {
            return new ErsMailParser(viewContext, enc);
        }

        /// <summary>
        /// HTML用のテンプレートパーサーを取得する。
		/// <para> Gets the template parser for HTML.</para>
        /// </summary>
        /// <param name="viewContext"></param>
        /// <returns></returns>
        public ErsTemplateParser GetErsTemplateParser(ErsViewContext viewContext, Encoding enc)
        {
            return new ErsTemplateParser(viewContext, enc);
        }

    }
}
