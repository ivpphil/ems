﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagForeach
        : ErsTagBase
    {
		/// <summary>
		/// Returns "foreach"
		/// </summary>
        protected override string ersTagName { get { return "ers:foreach"; } }

        public static IEnumerable ForEach(string sourceName, object o)
        {
            var list = o as IEnumerable;

            if (list == null)
                yield break;

            foreach (var val in list)
            {
                yield return val;
            }
        }

        /// <summary>
        /// ERSタグの開始タグをセットする
		/// <para>Set the start tag of ERS tags</para>
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            if (attributes.ContainsKey("roopcounter"))
            {
                //ループカウンターあり, loop
                var roopcountername = attributes["roopcounter"];
                var variable = ErsViewHelper.GetViewVariableString(attributes["src"]);

                var code = "<%{{var {3} = -1;" +
                    "foreach(object {2} in ErsTagForeach.ForEach(\"{0}\", {1})){{" +
                    "{3}++;%>";
                return string.Format(code, attributes["src"], variable, attributes["variable"], roopcountername);
            }
            else
            {
                //ループカウンターなし, no loop
                var variable = ErsViewHelper.GetViewVariableString(attributes["src"]);
                var code = "<%{{foreach(object {2} in ErsTagForeach.ForEach(\"{0}\", {1})){{ %>";
                return string.Format(code, attributes["src"], variable, attributes["variable"]);
            }
        }

        /// <summary>
        /// ERSタグの閉じタグをセットする
		/// <para>Set the closing tag for ERS</para>
        /// </summary>
        /// <param name="operation"></param>
        /// <returns></returns>
        protected override string ReplaceCloseErsTag()
        {
            return "<%}}%>";
        }

    }
}
