﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagDispInfo
        : ErsTagBase
    {

        protected override string ersTagName
        {
            get { return "ers:dispInfo"; }
        }

        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            var information = ErsViewHelper.GetViewVariableString(".ers_information_massage");

            //プロパティ名に指定がなければ、すべてのエラー出力
            var infoFilePath = ErsCommonContext.MapPath("~/Views/Partial/info_input.htm");
            if (File.Exists(infoFilePath))
            {
                var infoCode = File.ReadAllText(infoFilePath, this.parser.enc);
                infoCode = this.parser.ParseErsTemplate(infoCode);
                return string.Format("<%if(!ErsTagIsEqual.IsEqual(\".Information\", {0}, \"\")){{%>{1}<%}}%>", information, infoCode);
            }

            return string.Empty;
        }
    }
}
