﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagOutputQuery
        : ErsTagBase
    {
        /// <summary>
        /// 改行コードを除去する（開始タグ）
        /// </summary>
        protected override bool IsRemoveOpenNewLine
        {
            get { return false; }
        }

        protected override string ersTagName { get { return "ers:outputQuery"; } }

        /// <summary>
        /// ERSタグの開始タグをセットする
        /// <para>Set the start tag of the ERS tags</para>
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            var group_name = ErsOutputHiddenUtility.GetGroupName(attributes);

            return "<%=ErsTagOutputQuery.OutputQueryString(" + ErsViewHelper.GetViewVariableString(".OutputHidden") + ", new [] { " + group_name + " })%>";
        }

        /// <summary>
        /// ERSタグの閉じタグをセットする
        /// <para>Set the closing tag for tag ERS</para>
        /// </summary>
        /// <param name="operation"></param>
        /// <returns></returns>
        protected override string ReplaceCloseErsTag()
        {
            return string.Empty;
        }

        /// <summary>
        /// OutputHiddenをQueryString情報に出力する。
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="sourceName"></param>
        /// <param name="url"></param>
        /// <param name="values"></param>
        public static string OutputQueryString(object values, IEnumerable<string> group_name)
        {
            var targetValues = values as List<ErsOutputHiddenTarget>;
            if (targetValues == null)
                return null;

            var listHidden = targetValues.FirstOrDefault((target) => group_name.Contains(target.groupName));
            if (listHidden.values != null)
            {

                var queryString = string.Empty;
                queryString += ErsTemplateAnchor.GetQueryString(string.Empty, listHidden.values);

                if (string.IsNullOrEmpty(queryString))
                    return null;

                return queryString.Substring(1);
            }

            return null;
        }
    }
}
