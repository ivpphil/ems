﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagNonEncode
                : ErsTagBase
    {
        /// <summary>
        /// 改行コードを除去する（開始タグ）
        /// </summary>
        protected override bool IsRemoveOpenNewLine
        {
            get { return false; }
        }

		/// <summary>
		/// Returns "nonencode"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:nonencode"; }
        }
		/// <summary>
		/// Returns ErsTagNonEncode tag
		/// </summary>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
               return string.Format("<%ErsTagNonEncode.Display(\"{0}\", {1});%>", attributes["src"], ErsViewHelper.GetViewVariableString(attributes["src"]));
        }

        /// <summary>
        /// ers:description
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="tw"></param>
        /// <param name="sourceName"></param>
        /// <param name="o"></param>
        /// <param name="value"></param>
        public static void Display(string sourceName, object o)
        {
            if (o != null)
            {
                ErsViewHelper.Write(HtmlEncodeTagNotAllowed(o.ToString()));
            }
        }

        protected static string HtmlEncodeTagNotAllowed(string rawString)
        {
            string retString = rawString;

            var setup = new SetupConfigReader();
            var htmlTag = setup.AllowHtmlTag;


            var mc = new Regex("(<\\s*/?\\s*([^\\s/>]+).*?>)").Matches(rawString);

            foreach (Match m in mc)
            {
                string tagName;
                if (m.Groups[2] != null)
                    tagName = m.Groups[2].Value.ToLower();
                else
                    tagName = string.Empty;


                if (!htmlTag.Contains(tagName) && !tagName.StartsWith("ers:"))
                {
                    retString = retString.Replace(m.Groups[1].Value, HttpUtility.HtmlEncode(m.Groups[1].Value));
                }
            }

            return retString;
        }
    }
}
