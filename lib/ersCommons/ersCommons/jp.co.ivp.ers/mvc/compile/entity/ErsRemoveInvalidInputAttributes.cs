﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.compile.entity;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    /// <summary>
    /// IEにてエラーとなるため、name=""の値を一括して除去
    /// </summary>
    public class ErsRemoveInvalidInputAttributes
        : ErsTagBase
    {
        public override string Parse(string code)
        {
            if (!code.HasValue())
            {
                return code;
            }
            code = code.Replace("name=\"\"", "");
            code = code.Replace("id=\"\"", "");
            return code;
        }

        protected override string ersTagName
        {
            get { throw new NotImplementedException(); }
        }

        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            throw new NotImplementedException();
        }
    }
}
