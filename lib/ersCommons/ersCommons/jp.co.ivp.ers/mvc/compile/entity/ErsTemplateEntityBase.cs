﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public abstract class ErsTemplateEntityBase
    {
        protected internal ErsTemplateParser parser { get; set; }

        /// <summary>
        /// コードをParseして返す。
		/// <para>Parse the code</para>
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public abstract string Parse(string code);

        /// <summary>
        /// 改行コードを除去する（開始タグ）
        /// </summary>
        protected virtual bool IsRemoveOpenNewLine { get { return true; } }

        /// <summary>
        /// 改行コードを除去する（終了タグ）
        /// </summary>
        protected virtual bool IsRemoveCloseNewLine { get { return true; } }
    }
}
