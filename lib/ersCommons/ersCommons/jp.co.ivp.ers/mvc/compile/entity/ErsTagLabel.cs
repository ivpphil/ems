﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagLabel
        : ErsTagBase
    {
        /// <summary>
        /// 改行コードを除去する（開始タグ）
        /// </summary>
        protected override bool IsRemoveOpenNewLine
        {
            get { return false; }
        }

		/// <summary>
		/// Returns  "label"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:label"; }
        }

        /// <summary>
        /// Replace open ERS tag 
        /// </summary>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            if (attributes.ContainsKey("model"))
            {
                var arrvals = ErsViewHelper.SplitVariableString(attributes["model"]);
                if (!string.IsNullOrEmpty(arrvals[0]))
                {
                    return string.Format("<%ErsTagLabel.Display({0}, \"{1}\");%>", arrvals[0], arrvals[1]);
                }
                else
                {
                    return string.Format("<%ErsTagLabel.Display({0}, \"{1}\");%>", ErsViewHelper.GetViewVariableString(".Model"), attributes["model"]);
                }
            }
            else
            {
                return string.Format("<%ErsTagLabel.DisplayFieldName(\"{0}\");%>", attributes["code"]);
            }
            
        }

        /// <summary>
        /// Returns empty string
        /// </summary>
        protected override string ReplaceCloseErsTag()
        {
            return string.Empty;
        }

        /// <summary>
        /// Displays the specified argument model.
        /// </summary>
        /// <param name="argModel">The argument model.</param>
        /// <param name="sourceName">Name of the source.</param>
        public static void Display(object argModel, string sourceName)
        {
            var t = argModel.GetType();
            if (t == (new Dictionary<string, object>()).GetType())
            {
                ErsViewHelper.Write(ErsResources.GetFieldName(sourceName));
                return;

            }
            var sProperty = ErsViewHelper.SplitVariableString(sourceName)[sourceName.Split('.').Count() - 1];
            foreach (var property in t.GetProperties())
            {
                if (property.Name == sProperty && property.GetCustomAttributes(typeof(DisplayNameAttribute), false).Length <= 0)
                {
                    ErsViewHelper.Write(ErsResources.GetFieldName(sProperty));
                    return;
                }
                if (property.Name == sProperty && property.GetCustomAttributes(typeof(DisplayNameAttribute), false).Length > 0)
                {
                    var attributes = property.GetCustomAttributes((new DisplayNameAttribute()).GetType(), true);
                    foreach (var attr in attributes)
                    {
                        var disp = (DisplayNameAttribute) attr;
                        ErsViewHelper.Write(ErsResources.GetFieldName(disp.DisplayName));
                        return;
                    }
                }

            }
        }

        /// <summary>
        /// ers:label[code=x]
        /// </summary>
        /// <param name="code">The code.</param>
        public static void DisplayFieldName(string code)
        {
            if (!string.IsNullOrEmpty(code))
            {
                ErsViewHelper.Write(ErsResources.GetFieldName(code));
            }
        }
    }
}
