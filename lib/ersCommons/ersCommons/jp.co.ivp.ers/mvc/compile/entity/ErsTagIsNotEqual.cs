﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagIsNotEqual
        : ErsTagBase
    {
		/// <summary>
		/// Returns "isNotEqual"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:isNotEqual"; }
        }

		/// <summary>
		/// Returns ErsTagIsEqual tag
		/// </summary>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            if (attributes.ContainsKey("value"))
            {
                //値との比較, Comparison with the "value"
                return string.Format("<%if(!ErsTagIsEqual.IsEqual(\"{0}\", {1}, \"{2}\")){{%>", attributes["src"], ErsViewHelper.GetViewVariableString(attributes["src"]), attributes["value"]);
            }
            else if (attributes.ContainsKey("variable"))
            {
				//変数との比較, Comparison with the "variable"
                return string.Format("<%if(!ErsTagIsEqual.IsEqual(\"{0}\", {1}, {2})){{%>", attributes["src"], ErsViewHelper.GetViewVariableString(attributes["src"]), ErsViewHelper.GetViewVariableString(attributes["variable"]));
            }
            else if (attributes.ContainsKey("enum"))
            {
                //列挙との比較
                return string.Format("<%if(!ErsTagIsEqual.IsEnumEqual(\"{0}\", {1}, {2})){{%>", attributes["src"], ErsViewHelper.GetViewVariableString(attributes["src"]), attributes["enum"]);
            }

            return string.Empty;
        }

        protected override string ReplaceCloseErsTag()
        {
            return "<%}%>";
        }
    }
}
