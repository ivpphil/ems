﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public abstract class ErsTagBase
        : ErsTemplateEntityBase
    {
        /// <summary>
        /// タグ名
		/// <para>Tag Name</para>
        /// </summary>
        protected abstract string ersTagName { get; }

        /// <summary>
        /// コードをParseして返す。
		/// <para>Return Parsed code</para>
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public override string Parse(string code)
        {
            foreach (var m in this.GetOpenErsTag(code, ersTagName))
            {
                var dicAttr = this.GetTagAttributes(m.attributes);

                //C#コードに置換
                code = code.Replace(m.tag, ReplaceOpenErsTag(dicAttr));
            }

            foreach (var m in this.GetCloseErsTag(code, ersTagName))
            {
                //C#コードに置換
                code = code.Replace(m.tag, ReplaceCloseErsTag());
            }

            return code;
        }

        /// <summary>
        /// ERSタグの開始タグをセットする
		/// <para>Set the start tag for ERS tag</para>
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        protected abstract string ReplaceOpenErsTag(ErsTagAttributes attributes);

        /// <summary>
        /// ERSタグの閉じタグをセットする
		/// <para>Set the close tag for ERS tag</para>
        /// </summary>
        /// <param name="operation"></param>
        /// <returns></returns>
        protected virtual string ReplaceCloseErsTag()
        {
            return string.Empty;
        }

        private int GetCharCount(string value, char charValue)
        {
            return value.Count((val) => val== charValue);
        }

        /// <summary>
        /// 開始タグを取得する
        /// </summary>
        /// <param name="code"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        protected virtual IEnumerable<ErsTagMatch> GetOpenErsTag(string code, string tagName)
        {
            string operationTagStart = "[ 　\\t]*?<" + tagName + "\\s+";
            var matches = Regex.Matches(code, operationTagStart, RegexOptions.IgnoreCase);
            foreach (Match match in matches)
            {
                var delimiter = new[] { '>' };
                var lastIndex = code.IndexOfAny(delimiter, match.Index);
                if (lastIndex < 0)
                {
                    continue;
                }
                //タグの終了を検索
                var value = code.Substring(match.Index, lastIndex + 1 - match.Index);
                while (this.GetCharCount(value, '<') != this.GetCharCount(value, '>'))
                {
                    lastIndex = code.IndexOfAny(delimiter, lastIndex + 1);
                    if (lastIndex < 0)
                    {
                        break;
                    }
                    value = code.Substring(match.Index, lastIndex + 1 - match.Index);
                }
                if (lastIndex<0)
                {
                    continue;
                }
                yield return new ErsTagMatch()
                {
                    tag = value,
                    attributes = value.Substring(match.Value.Length, value.Length - match.Value.Length - 1)
                };
            }
        }

        /// <summary>
        /// 終了タグを取得する
        /// </summary>
        /// <param name="code"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        protected virtual IEnumerable<ErsTagMatch> GetCloseErsTag(string code, string tagName)
        {
            //ersタグ検索, Search ers tag（ers:は省略可能とする）
            string operationTagEnd = "[ 　\\t]*?<\\s?/\\s?" + tagName + "\\s*?>\\s?" + (this.IsRemoveCloseNewLine ? "(\\r\\n|\\n|\\r)?" : string.Empty);
            var matches =  new Regex(operationTagEnd, RegexOptions.IgnoreCase).Matches(code);

            foreach (Match match in matches)
            {
                yield return new ErsTagMatch()
                {
                    tag = match.Value
                };
            }
        }

        /// <summary>
        /// 属性をDictionaryにセット, Attribute set to Dictionary
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        protected ErsTagAttributes GetTagAttributes(string attribute)
        {
            //nameを取得(=まで)
            var dicAttr = new ErsTagAttributes();
            dicAttr.Parse(attribute);
            return dicAttr;
        }
    }
}
