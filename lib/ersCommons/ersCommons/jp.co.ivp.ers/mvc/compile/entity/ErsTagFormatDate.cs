﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagFormatDate
        : ErsTagFormatString
    {
        /// <summary>
        /// 改行コードを除去する（開始タグ）
        /// </summary>
        protected override bool IsRemoveOpenNewLine
        {
            get { return false; }
        }

		/// <summary>
		/// Returns  "formatDate"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:formatDate"; }
        }
	
        /// <summary>
		/// Returns  "yyyy/MM/dd"
		/// </summary>
        protected override string defaultValue
        {
            get
            {
                return "yyyy/MM/dd";
            }
        }

        /// <summary>
        /// ers:formatDate
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="tw"></param>
        /// <param name="sourceName"></param>
        /// <param name="o"></param>
        /// <param name="value"></param>
        public static new void Format(string sourceName, object o, string value, int? padLeftLength, int? padRightLength, int? fixedLength, string post_fix)
        {
            DateTime tempValue;
            if (o != null && DateTime.TryParse(o.ToString(), out tempValue))
            {
                var resultString = ErsTagFormatString.FormatValue(tempValue, value, padLeftLength, padRightLength, fixedLength, post_fix);

                ErsViewHelper.WriteValue(resultString);
            }
        }
    }
}
