﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagMessage
        : ErsTagBase
    {
        /// <summary>
        /// 改行コードを除去する（開始タグ）
        /// </summary>
        protected override bool IsRemoveOpenNewLine
        {
            get { return false; }
        }

		/// <summary>
		/// Returns  "message"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:message"; }
        }
	
        /// <summary>
        /// Returns a render message tag
        /// </summary>
        /// <param name="attributes"></param>
        /// <returns></returns>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            var arguments = "null";
            string attrVal = string.Empty;
            if (attributes.ContainsKey("value"))
            {
                arguments = string.Join(",", attributes["value"].Split(new[] { ',' }).Select((value) => ErsViewHelper.GetViewVariableString(value.Trim())));
            }            
            
            if (attributes.ContainsKey("src"))
            {
                return string.Format("<%ErsTagMessage.DisplayFromSource(\"{0}\", {1}, {2});%>", attributes["src"], ErsViewHelper.GetViewVariableString(attributes["src"]), arguments);
            }

            return string.Format("<%ErsTagMessage.Display(\"{0}\", {1});%>", attributes["code"], arguments);
        }

        /// <summary>
        /// Returns empty string
        /// </summary>
        protected override string ReplaceCloseErsTag()
        {
            return string.Empty;
        }

        /// <summary>
        /// ers:message
        /// </summary>
        /// <param name="code">The code.</param>
        public static void Display(string code, params object[] values)
        {
            if (!string.IsNullOrEmpty(code))
            {
                ErsViewHelper.Write(ErsResources.GetMessage(code, values));
            }
        }

        /// <summary>
        /// ers:message
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="tw"></param>
        /// <param name="sourceName"></param>
        /// <param name="o"></param>
        /// <param name="value"></param>
        public static void DisplayFromSource(string sourceName, object o, params object[] values)
        {
            if (!string.IsNullOrEmpty(sourceName))
            {
                if (o != null)
                {
                    ErsViewHelper.Write(ErsResources.GetMessage(o.ToString(), values));
                }
            }
        }
    }
}
