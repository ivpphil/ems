﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagIsElse
          : ErsTagBase
    {
        protected override string ersTagName
        {
            get { return "ers:isElse"; }
        }

        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            return "<%} else {%>";
        }
    }
}
