﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagAttributes
    {
        IList<ErsTagAttributesKeyValuePair<string, string>> attributes = new List<ErsTagAttributesKeyValuePair<string, string>>();

        public string this[string key]
        {
            get
            {
                var list = attributes.Where((entity) => (key == entity.Key));

                if (list.Count() == 0)
                {
                    throw new Exception(string.Format("attribut {{{0}}} is not defined in ers tag.", key));
                }

                return list.First().Value;
            }
            set
            {
                var list = attributes.Where((entity) => (key == entity.Key));

                if (list.Count() == 0)
                {
                    this.attributes.Add(new ErsTagAttributesKeyValuePair<string,string>(key, value, '"'));
                }

                var val = list.First();
                val.Value = value;
            }
        }

        public bool ContainsKey(string key)
        {
            var list = attributes.Where((entity) => (key == entity.Key));

            return (list.Count() > 0);
        }

        public override string ToString()
        {
            string result = string.Empty;
            foreach (var attribute in this.attributes)
            {
                if (attribute.Key.HasValue())
                {
                    result += string.Format(" {0}={2}{1}{2}", attribute.Key, attribute.Value, attribute.Delimiter);
                }
                else
                {
                    result += string.Format(" {0}", attribute.Value);
                }
            }
            return result;
        }

        internal void Remove(string key)
        {
            var list = attributes.Where((entity) => (key == entity.Key));

            if (list.Count() == 0)
            {
                return;
            }

            var val = list.First();
            attributes.Remove(val);
        }

        #region "Parse"
        public void Parse(string attribute)
        {
            attribute = attribute.Trim(new[] { ' ', '/' });

            while (true)
            {
                var keyValue = this.GetNextAttribute(ref attribute);
                if (keyValue == null)
                {
                    break;
                }
                this.attributes.Add(keyValue);
            }
        }

        private ErsTagAttributesKeyValuePair<string, string> GetNextAttribute(ref string attribute)
        {
            if (!attribute.HasValue())
            {
                return null;
            }

            string key;
            if (!this.GetNextValue(ref attribute, new[] { '=' }, out key))
            {
                //キーを取得できない場合は、からのキーにデータを格納
                var pairValue = new ErsTagAttributesKeyValuePair<string, string>(string.Empty, attribute, null);
                attribute = null;
                return pairValue;
            }

            if (attribute.HasValue())
            {
                //"または'の分すすめる
                char delimiter = Convert.ToChar(attribute.TrimStart().Substring(0, 1));
                attribute = attribute.TrimStart().Substring(1);

                string value;
                if (this.GetNextValue(ref attribute, new[] { delimiter }, out value))
                {
                    return new ErsTagAttributesKeyValuePair<string, string>(key.Trim(), value, delimiter);
                }
            }

            return new ErsTagAttributesKeyValuePair<string, string>(key.Trim(), null, '\"');
        }

        private bool GetNextValue(ref string attribute, char[] delimiters, out string nextValue)
        {
            //デリミタまでを取得
            var index = attribute.IndexOfAny(delimiters);
            if (index < 0)
            {
                nextValue = null;
                return false;
            }

            //タグを考慮した値を取得
            var value = attribute.Substring(0, index);
            while (this.GetCharCount(value, '<') != this.GetCharCount(value, '>'))
            {
                index = attribute.IndexOfAny(delimiters, index + 1);
                if (index < 0)
                {
                    nextValue = null;
                    return false;
                }
                value = attribute.Substring(0, index);
            }

            nextValue = value;
            attribute = attribute.Substring(index + 1);

            return true;
        }

        private int GetCharCount(string value, char charValue)
        {
            return value.Count((val) => val== charValue);
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="K"></typeparam>
    public class ErsTagAttributesKeyValuePair<TKey, KValue>
    {
        public TKey Key;
        public KValue Value;
        public char? Delimiter;

        public ErsTagAttributesKeyValuePair(TKey key, KValue value, char? delimiter)
        {
            this.Key = key;
            this.Value = value;
            this.Delimiter = delimiter;
        }
    }
}
