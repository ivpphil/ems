﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers.util;
using System.Web.Mvc;
using System.Web.Configuration;
using System.Configuration;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagPartial
        : ErsTagBase
    {

		/// <summary>
		/// Returns "partial"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:partial"; }
        }

		/// <summary>
		/// Returns a render partial tag
		/// </summary>
		/// <param name="attributes"></param>
		/// <returns></returns>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            string fileName = string.Empty;
            object siteId = new SetupConfigReader().site_id;
            if (attributes.ContainsKey("file"))
            {
                fileName = "\"" + attributes["file"] + "\"";
            }
            else if (attributes.ContainsKey("variable"))
            {
                fileName = ErsViewHelper.GetViewVariableString(attributes["variable"]);
            }

            var partialModel = "null";
            if (attributes.ContainsKey("partialModel"))
                partialModel = ErsViewHelper.GetViewVariableString(attributes["partialModel"]);

            if (!attributes.ContainsKey("siteType"))
            {
                return "<%ErsTagPartial.RenderPartial(context, " + fileName + ", tw, viewData, " + partialModel + ");%>";
            }

            if (attributes.ContainsKey("siteId"))
            {
                siteId = ErsViewHelper.GetViewVariableString(attributes["siteId"]);
            }

            return string.Format("<%ErsTagPartial.RenderPartial(context, {0}, tw, viewData, {1}, \"{2}\",{3});%>", fileName, partialModel, attributes["siteType"],siteId);
        }

		/// <summary>
		/// Returns empty string
		/// </summary>
        protected override string ReplaceCloseErsTag()
        {
            return string.Empty;
        }

		/// <summary>
		/// Render a Partial view
		/// </summary>
        public static void RenderPartial(ErsViewContext context, object fileName, TextWriter tw, ViewDataDictionary viewData, object partialModel)
        {
            new ErsViewShared().Init(Convert.ToString(fileName), context, new SetupConfigReader().site_type, new SetupConfigReader().site_id).Render(viewData, tw, partialModel);
        }

        public static void RenderPartial(ErsViewContext context, object fileName, TextWriter tw, ViewDataDictionary viewData, object partialModel, object siteType, object siteId)
        {
            var enumSiteType = (EnumSiteType?)ErsReflection.ConvertValue(typeof(EnumSiteType?), siteType);

            if (enumSiteType == null)
            {
                return;
            }

            int site_id = (int)siteId;

            new ErsViewShared().Init(Convert.ToString(fileName), context, enumSiteType.Value, site_id).Render(viewData, tw, partialModel);
        }


    }
}
