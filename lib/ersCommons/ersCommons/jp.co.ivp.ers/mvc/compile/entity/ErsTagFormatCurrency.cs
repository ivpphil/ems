﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagFormatCurrency
        : ErsTagFormatNumber
    {
        /// <summary>
        /// 改行コードを除去する（開始タグ）
        /// </summary>
        protected override bool IsRemoveOpenNewLine
        {
            get { return false; }
        }

        /// <summary>
        /// Returns "formatCurrency"
        /// </summary>
        protected override string ersTagName
        {
            get { return "ers:formatCurrency"; }
        }

        /// <summary>
        /// ers:formatCurrency
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="tw"></param>
        /// <param name="sourceName"></param>
        /// <param name="o"></param>
        /// <param name="value"></param>
        public static new void Format(string sourceName, object o, string value, int? padLeftLength, int? padRightLength, int? fixedLength, string post_fix)
        {
            ErsTagFormatNumber.GetFormattingValue(ref o, ref value);

            var resultString = ErsTagFormatString.FormatValue(o, value, padLeftLength, padRightLength, fixedLength, post_fix);

            ErsViewHelper.Write(ErsResources.GetMessage("currency", resultString));
        }
    }
}
