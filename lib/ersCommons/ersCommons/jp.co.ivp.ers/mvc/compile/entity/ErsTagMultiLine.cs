﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagMultiLine
        : ErsTagBase
    {
        /// <summary>
        /// 改行コードを除去する（開始タグ）
        /// </summary>
        protected override bool IsRemoveOpenNewLine
        {
            get { return false; }
        }

		/// <summary>
		/// Returns "multiLine"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:multiLine"; }
        }

		/// <summary>
		/// Returns ErsTagMultiLine tag
		/// </summary>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            return string.Format("<%ErsTagMultiLine.Display(\"{0}\", {1});%>", attributes["src"], ErsViewHelper.GetViewVariableString(attributes["src"]));
        }

        /// <summary>
        /// ers:multiLine
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="tw"></param>
        /// <param name="sourceName"></param>
        /// <param name="o"></param>
        /// <param name="value"></param>
        public static void Display(string sourceName, object o)
        {
            if (o != null)
            {
                var value = HttpUtility.HtmlEncode(o.ToString());
                ErsViewHelper.Write(value.Replace("\r\n", "\n").Replace("\r", ErsViewHelper.TAG_NEW_LINE + "\r").Replace("\n", ErsViewHelper.TAG_NEW_LINE + "\n"));
            }
        }
    }
}
