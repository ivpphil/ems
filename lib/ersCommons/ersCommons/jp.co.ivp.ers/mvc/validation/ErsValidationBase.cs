﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace jp.co.ivp.ers.mvc.validation
{
    public abstract class ErsValidationBase
         : Attribute
    {
        /// <summary>
        /// 配列かどうか / Whether the sequence 
        /// </summary>
        public bool isArray { get; set; }

        /// <summary>
        /// Gets value for validation of non-number only. Forces at least one alphabet input.
        /// </summary>
        public bool requireAlphabet { get; set; }

        /// <summary>
        /// 配列かどうか / Whether the sequence 
        /// </summary>
        public bool CutDown { get; set; }

        public virtual EnumTextValueRangeChecker rangeChecker { set { _rangeChecker = value; } get { throw new NotImplementedException(); } }
        protected virtual EnumTextValueRangeChecker? _rangeChecker { get; set; }

        /// <summary>
		/// エラー文言表示用のキー / Key words for display error
        /// </summary>
        protected internal string displayNameKey { get; set; }

        public string Validate(out ValidationResult validationResult, IEnumerable<Attribute> propertyAttributes, string propertyName, string checkValue)
        {
            var displayName = ErsResources.GetDisplayName(propertyAttributes);
            if (!displayName.HasValue())
            {
                displayName = this.GetDefaultDisplayName(propertyName);
            }
            displayName = ErsResources.GetFieldName(displayName);
            return this.Validate(out validationResult, displayName, propertyName, checkValue);
        }

        public string Validate(out ValidationResult validationResult, string displayName, string propertyName, string checkValue)
        {
            var result = this.IsValid(checkValue, displayName);

            //エラー時 / Error
            if (!string.IsNullOrEmpty(result))
            {
                validationResult = new ValidationResult(result, new[] { propertyName });
                return this.NormalizeValue(checkValue);
            }
            else
            {
                validationResult = null;
                return this.FormatValue(checkValue);
            }
        }

        private string IsValid(object value, string displayName)
        {

            string result = string.Empty;

			//型によって処理を切り替える。/ Switch the type
            if (value is Array)
            {
                //配列 / Array
                foreach (object o in (System.Collections.IEnumerable)value)
                {
                    string checkVal = (o == null ? string.Empty : o.ToString());
                    result += check(checkVal, displayName);
                }
            }
            else
            {
                string checkVal = (value == null ? string.Empty : value.ToString());
                result = check(checkVal, displayName);
            }

            return result;
        }

        protected internal string valueReplaceByValidator = string.Empty;

        abstract protected internal string check(string value, string displayName);

        /// <summary>
        /// Normalize the object value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected internal abstract string FormatValue(string value);

        /// <summary>
        /// Normalize the object value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected internal abstract string NormalizeValue(string value);

        protected abstract string GetDefaultDisplayName(string propertyName);
    }
}
