﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    class ErsFormatFullKana
        : ErsFormatFullString
    {
        public override string FormatValue(string value, bool CutDown, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            value = this.RemoveInvalidByte(value);

            if (CutDown)
            {
                value = this.CutDown(value, rangeFrom, rangeTo, rangeChecker);
            }

            return ErsCommon.ConvertToFullKana(value);
        }
    }
}
