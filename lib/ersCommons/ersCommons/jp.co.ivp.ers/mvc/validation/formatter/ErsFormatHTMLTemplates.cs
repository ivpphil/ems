﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;
using System.Net;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    class ErsFormatHtmlTemplates
        : ErsFormatHtml
    {
        public override string FormatValue(string value, bool CutDown, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            //xhtmlでないhtmlに対応 / replace xhtml that do not correspond to html
            value = base.FormatValue(WebUtility.HtmlDecode(value), CutDown,rangeFrom, rangeTo, rangeChecker);

            if (CutDown)
            {
                value = this.CutDown(value, rangeFrom, rangeTo, rangeChecker);
            }

            return value;
        }

        public override string NormalizeValue(string value)
        {
			//xhtmlでないhtmlに対応 / replace xhtml that do not correspond to html
            return base.NormalizeValue(WebUtility.HtmlDecode(value));
        }
    }
}
