﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    class ErsFormatNumericString
        : ErsFormatBase
    {
		/// <summary>
		/// Convert string to half number and replace value to white space base on [^\\d] expression
		/// </summary>
        public override string NormalizeValue(string value)
        {
            value = this.RemoveInvalidByte(value);

            value = ErsCommon.ConvertToHalfNumber(value);
            return new Regex("[^\\d]").Replace(value, string.Empty);
        }

        public override string FormatValue(string value, bool CutDown, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            value = this.RemoveInvalidByte(value);

            value = ErsCommon.ConvertToHalfNumber(value);

            if (CutDown)
            {
                value = this.CutDown(value, rangeFrom, rangeTo, rangeChecker);
            }

            return value;
        }
    }
}
