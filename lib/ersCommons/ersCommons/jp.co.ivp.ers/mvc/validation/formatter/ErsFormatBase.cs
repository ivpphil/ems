﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.mvc.validation.range_check;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    public abstract class ErsFormatBase
    {
        public virtual string prohibitionChars
        {
            get
            {
                if (this._prohibitionChars.HasValue())
                {
                    return this._prohibitionChars;
                }
                return new SetupConfigReader().ProhibitionChars;
            }
            set
            {
                this._prohibitionChars = value;
            }
        }

        private string _prohibitionChars;

        /// <summary>
        /// 
        /// </summary>
        public ErsFormatBase()
        {
        }

        /// <summary>
        /// Format the string value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual string FormatValue(string value, bool CutDown, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            value = this.RemoveInvalidByte(value);

            if (CutDown)
            {
                value = this.CutDown(value, rangeFrom, rangeTo,rangeChecker);
            }

            return value;
        }

        /// <summary>
        /// Normalize the string value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual string NormalizeValue(string value)
        {
           string prohibitionChars = this.prohibitionChars;

           if (string.IsNullOrEmpty(prohibitionChars))
           {
               return value;
           }

            value = this.RemoveInvalidByte(value);

            if (string.IsNullOrEmpty(value))
            {
                return value;
            }
            return new Regex("[" + prohibitionChars.Replace("\\", "\\\\") + "]").Replace(value, string.Empty);
        }

        /// <summary>
        /// 不正なByte文字列を除外する
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected virtual string RemoveInvalidByte(string value)
        {
            if (value == null)
            {
                return null;
            }
            return value.Replace(Convert.ToString((char)0), string.Empty);
        }

        /// <summary>
        /// 不正なByte文字列を除外する
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected virtual string CutDown(string value, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? enumRangeChecker)
        {
            var rangeChecker = this.GetValueRangeChecker(enumRangeChecker);
            return rangeChecker.CutDown(value, rangeFrom, rangeTo);
        }

        protected virtual IValueRangeChecker GetValueRangeChecker(EnumTextValueRangeChecker? enumRangeChecker)
        {
            return new SetupConfigReader().GetValueRangeChecker(enumRangeChecker);
        }
    }
}
