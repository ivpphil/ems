﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation.formatter;

namespace jp.co.ivp.ers.mvc.validation
{
    /// <summary>
    /// 汎用的な入力チェックを指定します。
    /// </summary>
    public class ErsUniversalValidationAttribute
        :ErsValidationBase
    {
        /// <summary>
        /// 検証タイプ / Validation type
        /// </summary>
        public CHK_TYPE type { get; set; }

        /// <summary>
        /// 閾値下限 / Threshold limit
        /// </summary>
        public string rangeDateFrom { get { throw new NotImplementedException(); } set { _rangeFrom = value; } }
        public int rangeFrom { get { throw new NotImplementedException(); } set { _rangeFrom = value.ToString(); } }
        private string _rangeFrom;

        /// <summary>
		/// 閾値上限 / Threshold limit
        /// </summary>
        public string rangeDateTo { get { throw new NotImplementedException(); } set { _rangeTo = value; } }
        public int rangeTo { get { throw new NotImplementedException(); } set { _rangeTo = value.ToString(); } }
        private string _rangeTo;

        /// <summary>
        /// 正規表現 / Regular expression
        /// </summary>
        public string regExpPattern
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                _regExpPattern = value;
            }
        }
        private string _regExpPattern;

        /// <summary>
		/// メッセージID（正規表現用）/ Message ID (for regular expressions)
        /// </summary>
        public string messageId
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                _messageId = value;
            }
        }
        private string _messageId;

        /// <summary>
        /// 禁則文字 / Prohibition characters
        /// </summary>
        public string prohibitionChars
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                _prohibitionChars = value;
            }
        }
        private string _prohibitionChars;


        public ErsUniversalValidationAttribute()
        {
            this.type = CHK_TYPE.All;
        }

        protected internal override string check(string value, string displayName)
        {
            if (string.IsNullOrEmpty(displayName))
                displayName = this.displayNameKey;

            return new ErsCheckUniversal().Check(type, value, displayName, _rangeFrom, _rangeTo, _regExpPattern, _messageId, _prohibitionChars, false, this.isArray, this.requireAlphabet, this.CutDown, this._rangeChecker);
        }

        protected internal override string FormatValue(string value)
        {
            return ErsFormatFactory.GetFormatter(this.type, this.isArray).FormatValue(value, this.CutDown, _rangeFrom, _rangeTo, this._rangeChecker);
        }

        protected internal override string NormalizeValue(string value)
        {
            return ErsFormatFactory.GetFormatter(this.type, this.isArray).NormalizeValue(value);
        }

        protected override string GetDefaultDisplayName(string propertyName)
        {
            return propertyName;
        }
    }
}
