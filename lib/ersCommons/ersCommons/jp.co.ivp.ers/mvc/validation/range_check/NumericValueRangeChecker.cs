﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.validation.validator;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.range_check
{
    public class NumericValueRangeChecker
        : IValueRangeChecker
    {
          /// <summary>
        /// レンジチェック / Range Check
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="rangeFrom"></param>
        /// <param name="rangeTo"></param>
        public virtual bool CheckRange(string argTarget, string argRangeFrom, string argRangeTo, ErsValidatorBase validator)
        {
            bool isValid = true;

            string target = argTarget;
            int? rangeFrom = ConvertToNullable.ParseToInt32(argRangeFrom);
            int? rangeTo = ConvertToNullable.ParseToInt32(argRangeTo);

            decimal dcmTarget;
            if (!decimal.TryParse(target, out dcmTarget))
            {
                //from-to
                if (rangeFrom != null && rangeTo != null)
                    validator.AppendError(ErsResources.GetMessage("10022", validator.displayName, rangeFrom, rangeTo));

                //from
                else if (rangeFrom != null)
                    validator.AppendError(ErsResources.GetMessage("10023", validator.displayName, rangeFrom));

                //to
                else if (rangeTo != null)
                    validator.AppendError(ErsResources.GetMessage("10024", validator.displayName, rangeTo));
                
                else
                    validator.AppendError(ErsResources.GetMessage("10025", validator.displayName));

                return false;
            }

            //from-to
            if (rangeFrom != null && rangeTo != null && (dcmTarget < rangeFrom || dcmTarget > rangeTo))
            {
                validator.AppendError(ErsResources.GetMessage("10022", validator.displayName, rangeFrom, rangeTo));
                isValid = false;
            }
            //from
            else if (rangeFrom != null && dcmTarget < rangeFrom)
            {
                validator.AppendError(ErsResources.GetMessage("10023", validator.displayName, rangeFrom));
                isValid = false;
            }
            //to
            else if (rangeTo != null && dcmTarget > rangeTo)
            {
                validator.AppendError(ErsResources.GetMessage("10024", validator.displayName, rangeTo));
                isValid = false;
            }

            return isValid;
        }


        public string CutDown(string argTarget, string argRangeFrom, string argRangeTo)
        {
            int? rangeFrom = ConvertToNullable.ParseToInt32(argRangeFrom);
            int? rangeTo = ConvertToNullable.ParseToInt32(argRangeTo);

            decimal dcmTarget;
            if (!decimal.TryParse(argTarget, out dcmTarget))
            {
                return argTarget;
            }

            //from
            if (rangeFrom != null && dcmTarget < rangeFrom)
            {
                return rangeFrom.ToString();
            }
            //to
            else if (rangeTo != null && dcmTarget > rangeTo)
            {
                return rangeTo.ToString();
            }

            return argTarget;
        }
    }
}
