﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateIP
        : ErsValidatorBase
    {
        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>Returns False</para>
        /// </summary>
        public override bool AllowFullString { get { return false; } }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>Returns true</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return true; } }

        /// <summary>
        /// ｢/｣と半角数値 / Numberic characters and [/]
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="target"></param>
        /// <param name="fieldName"></param>
        public override bool CheckType(string target)
        {
            bool isValid = true;

            var arrVal = target.Split('.');
            if (arrVal.Length != 4)
                return SetInvalid();

            foreach (var val in arrVal)
            {
                if (!Regex.IsMatch(val, @"^[0-9]*$"))
                    return SetInvalid();

                var intVal = Convert.ToInt32(val);
                if (0 > intVal && 255 < intVal)
                    return SetInvalid();
            }

            return isValid;
        }

        protected bool SetInvalid()
        {
            this.AppendError(ErsResources.GetMessage("10034", displayName));
            return false;
        }
    }
}
