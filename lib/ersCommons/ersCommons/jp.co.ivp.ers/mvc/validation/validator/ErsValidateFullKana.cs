﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateFullKana
        : ErsValidateFullString
    {
        /// <summary>
        /// 全角を含むことができる場合True
        /// <para>Returns true</para>
        /// </summary>
        public override bool AllowFullString { get { return true; } }

        /// <summary>
        /// 半角を含むことができる場合True
        /// <para>Returns false</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return false; } }

        /// <summary>
        /// 全角カナのみ / FullKana only 
        /// 全角・半角数字・全角空白も禁止(#15459にて変更)
        /// </summary>
        /// <param name="target"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public override bool CheckType(string target)
        {
            if (!IsFull(target))
            {
                this.AppendError(ErsResources.GetMessage("10003", displayName));
                return false;
            }
            else if (!Regex.IsMatch(target, @"^(\p{IsKatakana}|　)*$"))
            {
                this.AppendError(ErsResources.GetMessage("10003", displayName));
                return false;
            }

            return true;
        }
    }
}
