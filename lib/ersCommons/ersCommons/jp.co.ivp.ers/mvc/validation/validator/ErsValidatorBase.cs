﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.mvc.validation.range_check;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    public abstract class ErsValidatorBase
    {

        private StringBuilder ErrorMessageBuilder = new StringBuilder();

        protected internal void AppendError(string errorMessage)
        {
            this.ErrorMessageBuilder.Append(Environment.NewLine + errorMessage);
        }

        /// <summary>
        /// エラーメッセージ / Error message
        /// </summary>
        public virtual string ErrorMessage
        {
            get
            {
                var val = ErrorMessageBuilder.ToString();
                if (string.IsNullOrEmpty(val))
                    return val;
                else
                    return val.Substring(Environment.NewLine.Length);
            }
        }

        /// <summary>
        /// フィールド名（論理名）
		/// <para>Field Name (Logical name)</para>
        /// </summary>
        protected internal virtual string displayName { get; set; }

        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>True if characters can include full string</para>
        /// </summary>
        public abstract bool AllowFullString { get; }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>True if characters can include half string</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public abstract bool AllowHalfString { get; }

        public virtual string prohibitionChars
        {
            get
            {
                if (this._prohibitionChars.HasValue())
                {
                    return this._prohibitionChars;
                }
                return new SetupConfigReader().ProhibitionChars;
            }
            set
            {
                this._prohibitionChars = value;
            }
        }

        private string _prohibitionChars;

        /// <summary>
        /// 必須チェック / Mandatory checks
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="target"></param>
        /// <param name="columnName"></param>
        public virtual bool CheckRequired(string target, bool required)
        {
            bool isValid = true;

            if (required && string.IsNullOrEmpty(target))
            {
                this.AppendError(ErsResources.GetMessage("10000", displayName));
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// 禁則チェック / Check prohibition
        /// </summary>
        /// <param name="type"></param>
        /// <param name="DisplayName"></param>
        /// <param name="ret"></param>
        /// <param name="valForCheckProhibition"></param>
        /// <returns></returns>
        public virtual bool CheckProhibition(string target)
        {
            bool isValid = true;

            if (!string.IsNullOrEmpty(this.prohibitionChars) && Regex.IsMatch(target, "[" + this.prohibitionChars.Replace("\\", "\\\\") + "]"))
            {
                this.AppendError(ErsResources.GetMessage("10002", displayName));
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// 文字種別チェックを行う / Character type checking
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public abstract bool CheckType(string target);


        protected virtual IValueRangeChecker GetValueRangeChecker(EnumTextValueRangeChecker? rangeChecker)
        {
            return new SetupConfigReader().GetValueRangeChecker(rangeChecker);
        }

        /// <summary>
        /// レンジチェック / Range check
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="rangeFrom"></param>
        /// <param name="rangeTo"></param>
        public virtual bool CheckRange(string target, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            return this.GetValueRangeChecker(rangeChecker).CheckRange(target, rangeFrom, rangeTo, this);
        }

        /// <summary>
        /// 正規表現チェック / Check the regular expression
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="rangeFrom"></param>
        /// <param name="rangeTo"></param>
        public virtual bool CheckRegExp(string target, string regExpPattern, string messageId)
        {
            bool isValid = true;

            if (!string.IsNullOrEmpty(regExpPattern))
            {
                if (!Regex.IsMatch(target, "^" + regExpPattern + "$"))
                {
                    this.AppendError(ErsResources.GetMessage(!string.IsNullOrEmpty(messageId) ? messageId : "10025", displayName));
                    isValid = false;
                }
            }

            return isValid;
        }

        /// <summary>
        /// 全角文字かを判定 / Determine whether the parameter is a double-byte character
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        protected virtual bool IsFull(string str)
        {
            return ErsCommon.GetByteCount(str) == (str.Length * 2);
        }

        /// <summary>
		/// 半角文字かを判定 / Determine whether the parameter is a single-byte character
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        protected virtual bool IsHalf(string str)
        {
            return ErsCommon.GetByteCount(str) == str.Length;
        }

        /// <summary>
        /// Determines wether the parameter includes atleast one alphabet character.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public virtual bool CheckRequiredAlphabet(string target)
        {
            var setup = new SetupConfigReader();
            
            bool isValid = true;

            if (setup.ValidationForNotNumberOnlyEnabled)
            {    
                if (!target.Any(char.IsLetter))
                {
                    this.AppendError(ErsResources.GetMessage("10053", displayName));

                    isValid = false;
                }
            }

            return isValid;
        }
    }
}
