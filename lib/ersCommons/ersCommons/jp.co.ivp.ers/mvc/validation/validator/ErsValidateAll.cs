﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateAll
        : ErsValidatorBase
    {
        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>Return true if characters can include</para>
        /// </summary>
        public override bool AllowFullString { get { return true; } }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>Return true if characters can include</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return true; } }

        /// <summary>
        /// 文字種別はチェックしない
		/// <para>Character type is not checked</para>
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public override bool CheckType(string target)
        {
            //すべて許可 / Allow all
            return true;
        }
    }
}
