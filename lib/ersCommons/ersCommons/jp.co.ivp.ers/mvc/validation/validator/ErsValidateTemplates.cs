﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateTemplates
        : ErsValidatorBase
    {
		/// <summary>
		/// Returns True
		/// </summary>
        public override bool AllowFullString
        {
            get { return true; }
        }

		/// <summary>
		/// Returns True
		/// </summary>
        public override bool AllowHalfString
        {
            get { return true; }
        }

        /// <summary>
        /// 禁則チェック(スルーされる) / Check prohibition
        /// </summary>
        /// <param name="type"></param>
        /// <param name="DisplayName"></param>
        /// <param name="ret"></param>
        /// <param name="valForCheckProhibition"></param>
        /// <returns></returns>
        public override bool CheckProhibition(string target)
        {
            //文字チェックする際に禁則チェックを行うので、ここではチェックしない
            return true;
        }

        /// <summary>
        /// テンプレート(&lt;%=%&gt;と&lt;ers:**&gt;を許可。
		/// <para>Template (&lt;%=%&gt;と&lt;ers:**&gt;を許可。</para>
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public override bool CheckType(string target)
        {
            var targetForCheck = Regex.Replace(target, "(<%=[^%]+%>|</?ers:[^>]+>)", string.Empty, RegexOptions.IgnoreCase);

            return base.CheckProhibition(targetForCheck);

        }
    }
}
