﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateZip
        : ErsValidatorBase
    {
        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>Returns False</para>
        /// </summary>
        public override bool AllowFullString { get { return false; } }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>Returns True</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return true; } }

        /// <summary>
        /// 郵便番号 / Postal Code
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="target"></param>
        /// <param name="fieldName"></param>
        public override bool CheckType(string target)
        {
            bool isValid = true;

            if (!Regex.IsMatch(target, @"^[0-9]{3}-?[0-9]{4}$") // 国内配送
                && !Regex.IsMatch(target, @"^[0-9]([0-9-])+[0-9]$"))      //海外配送
            {
                this.AppendError(ErsResources.GetMessage("10050", displayName));
                isValid = false;
            }

            return isValid;
        }
    }
}
