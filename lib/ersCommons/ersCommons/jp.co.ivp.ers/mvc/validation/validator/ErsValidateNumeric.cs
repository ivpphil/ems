﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.mvc.validation.range_check;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateNumeric
        : ErsValidatorBase
    {
        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>Returns False</para>
        /// </summary>
        public override bool AllowFullString { get { return false; } }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>Returns True</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return true; } }

        protected override range_check.IValueRangeChecker GetValueRangeChecker(EnumTextValueRangeChecker? rangeChecker)
        {
            if (rangeChecker.HasValue)
            {
                throw new Exception("CHK_TYPE.Numeric don't support specifying rangeChecker.");
            }

            return new NumericValueRangeChecker();
        }

        /// <summary>
		/// 半角数値 / Numeric characters
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="target"></param>
        /// <param name="fieldName"></param>
        public override bool CheckType(string target)
        {
            bool isValid = true;

            if (!Regex.IsMatch(target, @"^-?[0-9]+\.?[0-9]*$"))
            {
                this.AppendError(ErsResources.GetMessage("10008", displayName));
                isValid = false;
            }

            return isValid;
        }
    }
}
