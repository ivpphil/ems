﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateWebAddress
        : ErsValidatorBase
    {
        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>Returns False</para>
        /// </summary>
        public override bool AllowFullString { get { return false; } }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>Returns True</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return true; } }

        /// <summary>
        /// URL
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="target"></param>
        /// <param name="fieldName"></param>
        public override bool CheckType(string target)
        {
            bool isValid = true;

            string pattern = @"^(https?://([a-zA-Z0-9\-_]+\.)+[a-zA-Z0-9\-_]+)?(:\d+)?/?(([a-zA-Z0-9 ./?%&=_~\-]|(&amp;?|&#165;?|&quot;?|&lt;?|&gt;?))*)?$"; 

            if (!Regex.IsMatch(target, pattern))
            {
                this.AppendError(ErsResources.GetMessage("10009", displayName));
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// 禁則チェック(スルーされる)
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public override bool CheckProhibition(string target)
        {
            //HTMLは、文字チェックする際に禁則チェックを行うので、ここではチェックしない
            return true;
        }

    }
}
