﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateFullString
        : ErsValidatorBase
    {
        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>Returns true</para>
        /// </summary>
        public override bool AllowFullString { get { return true; } }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>Returns false</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return false; } }

        /// <summary>
        /// 全角のみ / Full string only
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="target"></param>
        /// <param name="fieldName"></param>
        public override bool CheckType(string target)
        {

            bool isValid = true;

            if (!IsFull(target.Trim()))
            {
                this.AppendError(ErsResources.GetMessage("10011", displayName));
                isValid = false;
            }

            return isValid;
        }

    }
}
