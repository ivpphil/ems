﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateHtml
        : ErsValidatorBase
    {
        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>Returns True</para>
        /// </summary>
        public override bool AllowFullString { get { return true; } }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>Returns True</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return true; } }

        /// <summary>
        /// 禁則チェック(スルーされる)
		/// <para>Check prohibition</para>
        /// </summary>
        /// <param name="type"></param>
        /// <param name="DisplayName"></param>
        /// <param name="ret"></param>
        /// <param name="valForCheckProhibition"></param>
        /// <returns></returns>
        public override bool CheckProhibition(string target)
        {
            //HTMLは、文字チェックする際に禁則チェックを行うので、ここではチェックしない
			//Since checks prohibitive when character check, HTML is not checked here
            return true;
        }

        /// <summary>
        /// HTML許可タグ取得
        /// </summary>
        public virtual string[] AllowTag
        {
            get
            {
                var setup = new SetupConfigReader();
                return  setup.AllowHtmlTag;
            }
        }

        /// <summary>
        /// HTML許可＋HTML以外の禁則チェック
		/// <para>HTML Allowed + Check prohibitions other than HTML</para>
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="target"></param>
        /// <param name="DisplayName"></param>
        /// <returns></returns>
        public override bool CheckType(string target)
        {
            //コメントを無視する。
            target = new Regex("<!--|-->").Replace(target, "");
            var targetForCheck = target;

            var htmlTag = this.AllowTag;

            var errorTagList = new List<string>();

//            var mc = new Regex("<(\\s*/?\\s*([^\\s/]+).*?)>").Matches(target);
            //var mc = new Regex("<(\\s*/?\\s*([^\\s/>]+).*?)>").Matches(target);
            var mc = new Regex("<(\\s*/?\\s*([^\\s/>]+).*?)>", RegexOptions.Singleline).Matches(target);
            
            foreach (Match m in mc)
            {
                string tagName;
                if (m.Groups[2] != null)
                    tagName = m.Groups[2].Value.ToLower();
                else
                    tagName = string.Empty;
                
                if (!htmlTag.Contains(tagName))
                    errorTagList.Add(tagName);
                                 
                targetForCheck = targetForCheck.Replace(m.Groups[0].Value, "");
            }


            //許可されていないタグを入力された場合のエラー
			//Will print error if tag entered is not allowed
            if (errorTagList.Count != 0)
            {
                var errortag = string.Empty;
                errorTagList.ForEach(val => errortag += ",&lt" + val + "&gt");

                this.AppendError(ErsResources.GetMessage("10028", displayName, errortag.Substring(1)));
                return false;
            }
        
            // HTML特殊文字除去
            targetForCheck = this.DeleteHtmlSpecialCharacter(targetForCheck);

			//タグ以外の部分を禁則チェック / Check prohibitions other than the tags
            return base.CheckProhibition(targetForCheck);
        }

        /// <summary>
        /// HTML特殊文字除去
        /// </summary>
        /// <param name="target">変換元</param>
        /// <returns>除去後文字列</returns>
        protected string DeleteHtmlSpecialCharacter(string target)
        {
            if (string.IsNullOrEmpty(target))
            {
                return target;
            }
           
            var mc = new Regex("&.+?;").Matches(target);

            foreach (Match m in mc)
            {
                var matchValue = m.Groups[0].Value;

                // 変換前と変換後が違った場合は特殊文字
                if (matchValue != HttpUtility.HtmlDecode(matchValue))
                {
                    target = target.Replace(matchValue, string.Empty);
                }
            }

            return target;
        }

    }
}
