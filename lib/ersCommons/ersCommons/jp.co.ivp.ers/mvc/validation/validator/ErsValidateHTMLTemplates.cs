﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateHtmlTemplates
        : ErsValidateHtml
    {
        /// <summary>
        /// HTML許可＋HTML以外の禁則チェック
		/// <para>HTML Allowed + Check prohibitions other than HTML</para>
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="target"></param>
        /// <param name="DisplayName"></param>
        /// <returns></returns>
        public override bool CheckType(string target)
        {
            var targetForCheck = Regex.Replace(target, "(<%=[^%]+%>|</?ers:[^>]+>)", string.Empty, RegexOptions.IgnoreCase);

            return base.CheckType(targetForCheck);
        }

    }
}
