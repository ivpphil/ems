﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation.validator;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    public class ErsValidatorFactory
    {
        /// <summary>
        /// Validatorを取得する。/ Get validator
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static ErsValidatorBase GetValidator(CHK_TYPE type, bool isArray)
        {
            ErsValidatorBase validator = null;

            if (type == CHK_TYPE.All)
                validator = new ErsValidateAll();

            else if (type == CHK_TYPE.EMailAddr)
                validator = new ErsValidateEMailAddr();

            else if (type == CHK_TYPE.FullKana)
                validator = new ErsValidateFullKana();

            else if (type == CHK_TYPE.Hiragana)
                validator = new ErsValidateHiragana();

            else if (type == CHK_TYPE.External)
                validator = new ErsValidateExternal();

            else if (type == CHK_TYPE.FullString)
                validator = new ErsValidateFullString();

            else if (type == CHK_TYPE.HalfAlphabetOrNumber)
                validator = new ErsValidateHalfAlphabetOrNumber();

            else if (type == CHK_TYPE.Numeric)
                validator = new ErsValidateNumeric();

            else if (type == CHK_TYPE.NumericString)
                validator = new ErsValidateNumericString();

            else if (type == CHK_TYPE.Date)
                validator = new ErsValidateDate();

            else if (type == CHK_TYPE.DateWithOutTime)
                validator = new ErsValidateDateWithOutTime();

            else if (type == CHK_TYPE.WebAddress)
                validator = new ErsValidateWebAddress();

            else if (type == CHK_TYPE.OneByteCharacter)
                validator = new ErsValidateOneByteCharacter();

            else if (type == CHK_TYPE.HTML)
                validator = new ErsValidateHtml();

            else if (type == CHK_TYPE.Templates)
                validator = new ErsValidateTemplates();

            else if (type == CHK_TYPE.IP)
                validator = new ErsValidateIP();

                // takemoto そのうち正規表現
            else if (type == CHK_TYPE.Zip)
                validator = new ErsValidateZip();

            else if (type == CHK_TYPE.HyphenNumber)
                validator = new ErsValidateHyphenNumber();

            else if (type == CHK_TYPE.SlashNumber)
                validator = new ErsValidateSlashNumber();

            else if (type == CHK_TYPE.Tel)
                validator = new ErsValidateTel();

            //for @mail Html_Body field
            else if (type == CHK_TYPE.HTMLTemplates)
                validator = new ErsValidateHtmlTemplates();

            else if (type == CHK_TYPE.ConnectionString)
                validator = new ErsValidateConnectionString();

            else
                throw new Exception("未実装のチェック型" + type.ToString());

            if (isArray)
            {
                return new ErsValidateArrayDecorator(validator);
            }
            else
            {
                return validator;
            }
        }

        public static ErsValidatorBase GetValidator(CHK_TYPE type, string displayName, bool isArray)
        {
            var validator = GetValidator(type, isArray);
            validator.displayName = displayName;
            return validator;
        }
    }
}
