﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    /// <summary>
    /// 入力チェックを、配列対応にする。/ Corresponding to the sequence 
    /// </summary>
    class ErsValidateArrayDecorator
        : ErsValidatorBase
    {
        protected ErsValidatorBase validator;

        public ErsValidateArrayDecorator(ErsValidatorBase validator)
        {
            this.validator = validator;
        }

        public override string ErrorMessage
        {
            get
            {
                return validator.ErrorMessage;
            }
        }

        protected internal override string displayName
        {
            get
            {
                return validator.displayName;
            }
            set
            {
                validator.displayName = value;
            }
        }

        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>Returns true</para>
        /// </summary>
        public override bool AllowFullString { get { return validator.AllowFullString; } }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>Returns true</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return validator.AllowHalfString; } }

        /// <summary>
        /// 禁則チェック / Check prohibition
        /// </summary>
        /// <param name="DisplayName"></param>
        /// <param name="ret"></param>
        /// <param name="valForCheckProhibition"></param>
        /// <returns></returns>
        public override bool CheckProhibition(string target)
        {
            //配列の場合は、カンマを外してチェック。/ In the case of an array, check and remove the comma
            return validator.CheckProhibition(target.Replace(",", string.Empty));
        }

        /// <summary>
        /// レンジチェック / Range Check
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="rangeFrom"></param>
        /// <param name="rangeTo"></param>
        public override bool CheckRange(string target, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            bool isValid = true;
            foreach (var val in target.Split(','))
            {
                isValid = validator.CheckRange(val, rangeFrom, rangeTo, rangeChecker);
                if (!isValid)
                {
                    break;
                }
            }
            return isValid;
        }

        /// <summary>
        /// 半角数値(配列) / Numeric characters (array)
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="target"></param>
        /// <param name="fieldName"></param>
        public override bool CheckType(string target)
        {

            bool isValid = true;

            foreach (var val in target.Split(','))
            {
                isValid = validator.CheckType(val);
                if (!isValid)
                {
                    break;
                }
            }

            return isValid;
        }


    }
}
