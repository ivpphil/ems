﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation.formatter;

namespace jp.co.ivp.ers.mvc.validation
{
    /// <summary>
    /// テーブルのカラムコメントによる入力チェックを指定します。
    /// </summary>
    public class ErsSchemaValidationAttribute
         : ErsValidationBase
    {
        protected SchemaForErsValidation checkSchema = null;

        protected internal string tableName { get; set; }
        protected internal string fieldName { get; set; }

        public virtual DateTime rangeDateFrom { set { _rangeFrom = value.ToString(); } get { throw new NotImplementedException(); } }
        public virtual int rangeFrom { set { _rangeFrom = value.ToString(); } get { throw new NotImplementedException(); } }
        protected virtual string _rangeFrom { get; set; }

        public virtual DateTime rangeDateTo { set { _rangeFrom = value.ToString(); } get { throw new NotImplementedException(); } }
        public virtual int rangeTo { set { _rangeTo = value.ToString(); } get { throw new NotImplementedException(); } }
        protected virtual string _rangeTo { get; set; }

        public virtual string regExpPattern { set { _regExpPattern = value; } get { throw new NotImplementedException(); } }
        protected virtual string _regExpPattern { get; set; }

        public virtual string messageId { set { _messageId = value; } get { throw new NotImplementedException(); } }
        protected virtual string _messageId { get; set; }

        public virtual string prohibitionChars { set { _prohibitionChars = value; } get { throw new NotImplementedException(); } }
        protected virtual string _prohibitionChars { get; set; }

        public ErsSchemaValidationAttribute(string className)
        {
            if (!className.Contains("."))
            {
                throw new Exception("The className is Invalid format.The table name or column name are missing.");
            }
            this.displayNameKey = className;
            this.tableName = className.Split(new char[] { '.' })[0];
            this.fieldName = className.Split(new char[] { '.' })[1];
        }

        protected internal override string check(string value, string displayName)
        {
            checkSchema = new SchemaForErsValidation(tableName, fieldName);

            if (this._rangeFrom != null)
            {
                checkSchema.rangeFrom = this._rangeFrom;
            }

            if (this._rangeTo != null)
            {
                checkSchema.rangeTo = this._rangeTo;
            }

            if (!string.IsNullOrEmpty(this._regExpPattern))
            {
                checkSchema.regExpPattern = this._regExpPattern;
            }

            if (!string.IsNullOrEmpty(this._messageId))
            {
                checkSchema.messageId = this._messageId;
            }

            if (this._rangeChecker.HasValue)
            {
                checkSchema.rangeChecker = this._rangeChecker.Value;
            }

            if (this._prohibitionChars.HasValue())
            {
                checkSchema.prohibitionChars = this._prohibitionChars;
            }

            return new ErsCheckUniversal().CheckWithSchema(displayName, checkSchema, value, (checkSchema.isArray || this.isArray), this.requireAlphabet, this.CutDown);
        }

        protected internal override string FormatValue(string value)
        {
            return ErsFormatFactory.GetFormatter(checkSchema.CHK_TYPE, (checkSchema.isArray || this.isArray)).FormatValue(value, this.CutDown, checkSchema.rangeFrom, checkSchema.rangeTo, checkSchema.rangeChecker);
        }

        protected internal override string NormalizeValue(string value)
        {
            return ErsFormatFactory.GetFormatter(checkSchema.CHK_TYPE, (checkSchema.isArray || this.isArray)).NormalizeValue(value);
        }

        protected override string GetDefaultDisplayName(string propertyName)
        {
            return this.tableName + "." + this.fieldName;
        }
    }
}