﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Text;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation.validator;
using jp.co.ivp.ers.mvc.validation.formatter;

namespace jp.co.ivp.ers.mvc.validation
{
    public class ErsCheckUniversal
    {
        /// <summary>
        /// スキーマによるチェック
		/// <para>Check with the schema</para>
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="column"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public virtual string CheckWithSchema(string displayName, SchemaForErsValidation schema, string target, bool isArray, bool requireAlphabet, bool CutDown)
        {
            return Check(schema.CHK_TYPE, target, displayName, schema.rangeFrom, schema.rangeTo, schema.regExpPattern, schema.messageId, schema.prohibitionChars, schema.Required, isArray, requireAlphabet, CutDown, schema.rangeChecker);
        }

        /// <summary>
        /// チェック / Check
        /// </summary>
        /// <param name="type"></param>
        /// <param name="target"></param>
        /// <param name="fieldName"></param>
        /// <param name="rangeFrom"></param>
        /// <param name="rangeTo"></param>
        /// <param name="required"></param>
        /// <param name="isArray"></param>
        /// <returns></returns>
        public virtual string Check(CHK_TYPE type, string target, string displayName, string rangeFrom, string rangeTo, string regExpPattern, string messageId, string prohibitionChars, bool required, bool isArray, bool requireAlphabet, bool CutDown, EnumTextValueRangeChecker? rangeChecker)
        {
            if (target == null)
                target = string.Empty;

            target = target.Trim();

            if (!string.IsNullOrEmpty(messageId) && string.IsNullOrEmpty(regExpPattern))
            {
                throw new Exception("pattern is required when specify the message_id");
            }

            var validator = ErsValidatorFactory.GetValidator(type, displayName, isArray);

            var formatter = ErsFormatFactory.GetFormatter(type, isArray);

            if (prohibitionChars.HasValue())
            {
                validator.prohibitionChars = prohibitionChars;
                formatter.prohibitionChars = prohibitionChars;
            }

            target = formatter.FormatValue(target, false, null, null, null);

            //禁則チェック
			//Check prohibition
            if (!validator.CheckProhibition(target))
            {
                return validator.ErrorMessage;
            }

            //必須チェック
			//Mandatory checks
            if (!validator.CheckRequired(target, required))
            {
                return validator.ErrorMessage;
            }

            if (!string.IsNullOrEmpty(target))
            {
                //文字種チェック
				//Check the character type
                if (!validator.CheckType(target))
                {
                    return validator.ErrorMessage;
                }

                //文字数チェック
				//Check the number of characters
                if (!CutDown)
                {
                    if (!validator.CheckRange(target, rangeFrom, rangeTo, rangeChecker))
                    {
                        return validator.ErrorMessage;
                    }
                }

                //Requires input to have atleast one alphabet character.
                if (requireAlphabet)
                {
                    if (!validator.CheckRequiredAlphabet(target))
                    {
                        return validator.ErrorMessage;
                    }
                }
                
                //正規表現チェック
				//Check the regular expression
                if (!validator.CheckRegExp(target, regExpPattern, messageId))
                {
                    return validator.ErrorMessage;
                }
            }

            return string.Empty;
        }
    }
}