﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using jp.co.ivp.ers.db;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation
{
    public class SchemaForErsValidation
    {
        protected SchemaForErsValidation()
        {
        }

        /// <summary>
        /// Analyzes comment of column.
        /// </summary>
        protected void AnalyzeComment(DataColumn column)
        {
            //Analyze range of value.
            var range_match = Regex.Match(column.Caption, "range (\\d+ )?to( \\d+)?");

            var fromMatch = range_match.Groups[1];
            if (fromMatch.Success && !string.IsNullOrEmpty(fromMatch.Value))
            {
                this.rangeFrom = fromMatch.Value;
            }

            var toMatch = range_match.Groups[2];
            if (toMatch.Success && !string.IsNullOrEmpty(toMatch.Value))
            {
                this.rangeTo = toMatch.Value;
            }

            //Analyze range of date value.
            var range_date_match = Regex.Match(column.Caption, "range (\"(.*?)\" )?to( \"(.*?)\")?");

            var fromDateMatch = range_date_match.Groups[2];
            if (fromDateMatch.Success && !string.IsNullOrEmpty(fromDateMatch.Value))
            {
                this.rangeFrom = fromDateMatch.Value;
            }

            var toDateMatch = range_date_match.Groups[4];
            if (toDateMatch.Success && !string.IsNullOrEmpty(toDateMatch.Value))
            {
                this.rangeTo = toDateMatch.Value;
            }

            //Analyze regExpPattern of value
            var regExpPattern_match = Regex.Match(column.Caption, "pattern \"((\\\\\"|[^\"])*)\"");

            var regExpPatternMatch = regExpPattern_match.Groups[1];
            if (regExpPatternMatch.Success && !string.IsNullOrEmpty(regExpPatternMatch.Value))
            {
                this.regExpPattern = regExpPatternMatch.Value;
            }

            //Analyze messageId of value
            var messageId_match = Regex.Match(column.Caption, "message_id \"((\\\\\"|[^\"])*)\"");

            var messageIdMatch = messageId_match.Groups[1];
            if (messageIdMatch.Success && !string.IsNullOrEmpty(messageIdMatch.Value))
            {
                this.messageId = messageIdMatch.Value;
            }

            //Analyze range_checker of value
            var rangeChecker_match = Regex.Match(column.Caption, "range_checker \"(.*?)\"");

            var rangeCheckerMatch = rangeChecker_match.Groups[1];
            if (rangeCheckerMatch.Success && !string.IsNullOrEmpty(rangeCheckerMatch.Value))
            {
                var strRangeChecker = rangeCheckerMatch.Value;
                EnumTextValueRangeChecker rangeChecker;
                if (!Enum.TryParse<EnumTextValueRangeChecker>(strRangeChecker, out rangeChecker))
                {
                    throw new Exception(strRangeChecker + " is not defined in EnumTextValueRangeChecker.");
                }
                this.rangeChecker = rangeChecker;
            }

            //Analyze prohibitionChars of value
            var prohibitionCharsPattern = Regex.Match(column.Caption, @"prohibitionChars (.*?)\s");

            var prohibitionCharsMatch = prohibitionCharsPattern.Groups[1];
            if (prohibitionCharsMatch.Success && prohibitionCharsMatch.Value.HasValue())
            {
                this.prohibitionChars = prohibitionCharsMatch.Value;
            }

            //Analyze type of value.
            CHK_TYPE chkType;
            var CHK_TYPE_match = Regex.Match(column.Caption, "CHK_TYPE_([^\\s]+)");

            var CHK_TYPEMatch = CHK_TYPE_match.Groups[1];
            if (!CHK_TYPEMatch.Success)
            {
                //if there is not definition about validation type in the comment of column, infer the validation type.
                chkType = GetChkType(column.DataType);
                this.isArray = column.DataType.IsArray;
            }
            else
            {
                var arrayString = "Array";

                var strCHK_TYPE = CHK_TYPEMatch.Value;

                //Checks if the Caption contains Array keyword.
                this.isArray = strCHK_TYPE.EndsWith(arrayString, true, null);

                if (this.isArray)
                {
                    strCHK_TYPE = VBStrings.Left(strCHK_TYPE, strCHK_TYPE.Length - arrayString.Length);
                }

                if (!Enum.TryParse<CHK_TYPE>(strCHK_TYPE, out chkType))
                {
                    throw new Exception(strCHK_TYPE + " is not defined in CHK_TYPE.");
                }
            }

            this.CHK_TYPE = chkType;

        }

        /// <summary>
        /// Initializes a new instance of the SchemaForErsValidation class.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnName"></param>
        public SchemaForErsValidation(string tableName, string columnName)
        {
            if (!dbPool.ContainsKey(tableName))
            {
                dbPool[tableName] = new ErsDB_parent(tableName);
                dbPool[tableName].SetCommentToCaption();
            }

            var column = dbPool[tableName].Schema[columnName];

            if (column == null)
                throw new Exception(string.Format("An error occurred on validating. {1} is not defined in {0}.", tableName, columnName));

            AnalyzeComment(column);
        }

        private static Dictionary<string, ErsDB_parent> _dbPool
        {
            get
            {
                return (Dictionary<string, ErsDB_parent>)ErsCommonContext.GetPooledObject("_dbPool");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_dbPool", value);
            }
        }

        /// <summary>
        /// Get Dictionary which pooling an instance of ersDB parent.
        /// </summary>
        private Dictionary<string, ErsDB_parent> dbPool
        {
            get
            {
                if (_dbPool == null)
                    _dbPool = new Dictionary<string, ErsDB_parent>();

                return _dbPool;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the field is required. 
        /// </summary>
        public virtual bool Required
        {
            get
            {
                //modified to be false always
                return false;
            }
        }

        public virtual CHK_TYPE CHK_TYPE { get; private set; }

        public virtual string rangeFrom { get; set; }

        public virtual string rangeTo { get; set; }

        public virtual EnumTextValueRangeChecker? rangeChecker { get; set; }

        public virtual string regExpPattern { get; set; }

        public virtual string messageId { get; set; }

        public virtual string prohibitionChars { get; set; }

        public virtual bool isArray { get; set; }

        public virtual bool requireAlphabet { get; set; }

        /// <summary>
        /// Infers the validation type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private CHK_TYPE GetChkType(Type type)
        {
            if (type == typeof(String) || type == typeof(String[]))
            {
                return CHK_TYPE.All;
            }
            else if (type == typeof(Int16) || type == typeof(Int16[])
                || type == typeof(Int32) || type == typeof(Int32[])
                || type == typeof(Int64) || type == typeof(Int64[]))
            {
                return CHK_TYPE.Numeric;
            }
            else if (type == typeof(DateTime) || type == typeof(DateTime[]))
            {
                return CHK_TYPE.Date;
            }

            throw new Exception("未実装のカラム型" + type.ToString());
        }
    }
}
