﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.mvc
{
    public abstract class ErsBindableModel
        : ErsModelBase
    {
        /// <summary>
        /// 行の名前
        /// </summary>
        public override string lineName
        {
            get
            {
                return ErsResources.GetMessage("line_name", this.lineNumber);
            }
        }

        public ErsBindableModel()
        {
        }

        public override ErsControllerBase controller
        {
            get
            {
                if (this.containerModel != null)
                {
                    return this.containerModel.controller;
                }
                else
                {
                    return base.controller;
                }
            }
            internal set
            {
                base.controller = value;
            }
        }

        public virtual string record_key { get; set; }
    }
}
