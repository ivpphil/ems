﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc
{
    public class ValueSourceCookieAttribute
        : ValueSourceAttribute
    {
        /// <summary>
        /// Add HttpOnly attribute
        /// </summary>
        public bool IsHttpOnly { get; set; }

        public ValueSourceCookieAttribute(bool IsSecure)
            : base(VALUE_SOURCE.COOKIES, IsSecure)
        {
        }
    }
}
