﻿using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.CommandProcessor.Dispatcher;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace jp.co.ivp.ers.mvc
{
    public static class ErsControllerExtensions
    {
        public static void AddModelErrors<TModel>(this Controller controller, IEnumerable<ValidationResult> validationResults, IErsModelBase model, string defaultErrorKey = null)
        {
            AddModelErrors(controller.ModelState, validationResults, model, defaultErrorKey);
        }

        public static void AddModelErrors(this ModelStateDictionary modelState, IEnumerable<ValidationResult> validationResults, IErsModelBase model, string defaultErrorKey = null)
        {
            if (validationResults == null) return;

            foreach (var validationResult in validationResults)
            {
                if (validationResult != null)
                {
                    if (validationResult.MemberNames.Count() > 0)
                    {
                        foreach (var MemberName in validationResult.MemberNames)
                        {
                            SetErrorToModelState(modelState, MemberName, defaultErrorKey, validationResult);
                            SetErrorToModel(model, MemberName, defaultErrorKey, validationResult);
                        }
                    }
                    else
                    {
                        SetErrorToModelState(modelState, null, defaultErrorKey, validationResult);
                        SetErrorToModel(model, null, defaultErrorKey, validationResult);
                    }
                }
            }
        }

        private static void SetErrorToModelState(ModelStateDictionary modelState, string MemberName, string defaultErrorKey, ValidationResult validationResult)
        {
            string key = MemberName ?? defaultErrorKey ?? string.Empty;
            modelState.AddModelError(key, validationResult.ErrorMessage);
        }

        private static void SetErrorToModel(IErsModelBase model, string MemberName, string defaultErrorKey, ValidationResult validationResult)
        {
            string key = MemberName ?? defaultErrorKey ?? string.Empty;
            model.AddInvalidField(key, validationResult.ErrorMessage);
        }
    }
}
