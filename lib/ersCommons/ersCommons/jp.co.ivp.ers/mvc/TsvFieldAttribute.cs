﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.util
{
    /// <summary>
    /// TSV出力対象のプロパティに指定する。
	/// Specifies the properties of output TSV
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class TsvFieldAttribute
        : SortableAttribute
    {
        public int fixedLength { get; set; }

        public const int NON_FIXED = -1;

        /// <summary>
        /// Sets the fixedlength property of TSV
        /// </summary>
        /// <param name="fixedLength">
		/// 固定長。指定しない場合はTsvFieldAttribute.NON_FIXEDを指定
		/// <para>If fixed length is not specified, specify the TsvFieldAttribute.NON_FIXED</para>
		/// </param>
        public TsvFieldAttribute(int fixedLength, [CallerLineNumber]int order = 0)
            : base(order)
        {
            this.fixedLength = fixedLength;
        }
    }
}
