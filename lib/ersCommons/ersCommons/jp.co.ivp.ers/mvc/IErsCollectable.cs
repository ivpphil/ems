﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc
{
    public interface IErsCollectable
    {
        Dictionary<string, object> GetPropertiesAsDictionary();
    }
}
