using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using System.Xml.Xsl;
using System.Xml.Linq;
using System.IO;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Collections;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;
using System.Text;
using jp.co.ivp.ers.mvc.compile;
using jp.co.ivp.ers.mvc.template;
using System.Web.Configuration;

namespace jp.co.ivp.ers.mvc
{
    public class ErsView
        : ErsViewBase , IView
    {
        /// <summary>
        /// ControllerContextからErsViewContextを作成してViewを初期化
        /// </summary>
        /// <param name="path"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual ErsView Init(string path, ControllerContext context)
        {
            this.site_type = new SetupConfigReader().site_type;
            this.site_id = new SetupConfigReader().site_id;
            this.context = new ErsViewContext();
            this._path = ErsCommonContext.MapPath(path);
            this.enc = this.GetViewContextEncoding(this.site_type,this.site_id);
            return this;
        }

        /// <summary>
        /// テンプレートを出力する
        /// </summary>
        /// <param name="viewContext"></param>
        /// <param name="writer"></param>
        public virtual void Render(ViewContext viewContext, System.IO.TextWriter writer)
        {
            var newViewContext = context.GetViewData(viewContext);
            this.GetStatic(newViewContext);

            var func = GetFunction();
            func(writer, context, newViewContext, null);
            Console.WriteLine(writer);
        }


        /// <summary>
        /// 設定ファイル値をViewDataにセットする
        /// <para>ViewData configuration file is set to the value</para>
        /// </summary>
        /// <param name="viewData"></param>
        protected override void GetStatic(IDictionary<string, Object> viewData)
        {
            var setup = new SetupConfigReader();

            viewData["nor_url"] = setup.nor_url;

            viewData["sec_url"] = setup.sec_url;

            if (!ErsCommonContext.IsBatch)
            {
                viewData["dynamic_url"] = (HttpContext.Current.Request.IsSecureConnection) ? setup.sec_url : setup.nor_url;
                viewData["pc_dynamic_url"] = (HttpContext.Current.Request.IsSecureConnection) ? setup.pc_sec_url : setup.pc_nor_url;
            }

            foreach (string key in SetupConfigReader.GetSiteConfigFileKeys())
            {
                //既に値がある場合はセットしない
                if (!viewData.ContainsKey(key))
                {
                    viewData[key] = SetupConfigReader.GetSiteConfigFileValue(key);
                }
            }
            
            foreach (string key in SetupConfigReader.GetCommonConfigFileKeys())
            {
                //既に値がある場合はセットしない
                if (!viewData.ContainsKey(key))
                {
                    viewData[key] = SetupConfigReader.GetCommonConfigFileValue(key);
                }
            }
        }
    }
}