﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.Web.Mvc;
using System.ComponentModel;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.util
{
    /// <summary>
    /// 
    /// </summary>
    public class ErsBindModel
    {

        /// <summary>
        /// modelにDictionaryの値をBindする。
		/// <para>Bind Dictionary to the value of the model</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <param name="dictionary"></param>
        /// <param name="lineName"></param>
        /// <returns></returns>
        public static void ModelBind<T>(T model, IDictionary<string, object> dictionary, bool DoNotSetErrorValue)
            where T : IErsModelBase
        {
            foreach (var prop in model.GetType().GetProperties())
            {
                var attributes = prop.GetCustomAttributes(false).OfType<Attribute>();

                var validators = GetValidators(attributes);
                if (validators.Count() == 0)
                {
                    //検証クラスが定義されていないクラスにはバインドしない
                    continue;
                }

                var htmlBinding = GetIHtmlBinding(attributes);
                if (htmlBinding == null)
                    htmlBinding = new HtmlDefaultBinding();

                if (htmlBinding == null)
                    continue;

                var value = htmlBinding.GetRequestValue(prop.Name, dictionary);
                object checkedValue;
                var tempResults = Validate(model, value, prop, out checkedValue);
                if (tempResults != null)
                {
                    var results = tempResults.ToList();//何度も実行されてしまうので、一度Listに変換する。
                    if (results.Count > 0)
                    {
                        //表示用に不正な値を保持
                        model.InvalidValues[prop.Name] = value;
                    }

                    foreach (var result in results)
                    {
                        if (result != null)
                        {
                            foreach (var name in result.MemberNames)
                            {
                                model.AddInvalidField(name, result.ErrorMessage);
                            }
                        }
                    }
                }

                if (checkedValue != null && (!DoNotSetErrorValue || (DoNotSetErrorValue && tempResults != null && tempResults.Count() == 0)))
                {
                    SetProperty(model, prop, checkedValue);
                }
            }

            ErsBindModel.CallValidate(model);
        }

        /// <summary>
        /// プロパティを検証する。
		/// <para>Validate each properties</para>
        /// </summary>
        /// <param name="index"></param>
        /// <param name="model"></param>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static IEnumerable<ValidationResult> Validate<T>(T model, object value, System.Reflection.PropertyInfo prop, out object checkedValue)
            where T : IErsModelBase
        {
            var attributes = prop.GetCustomAttributes(false).OfType<Attribute>();

            var validators = GetValidators(attributes);
            if (validators.Count() == 0)
            {
                //検証クラスが定義されていないクラスにはバインドしない
                checkedValue = null;
                return null;
            }

            var htmlBinding = GetIHtmlBinding(attributes);
            if (htmlBinding == null)
                htmlBinding = new HtmlDefaultBinding();

            //HTMLの特殊バインド
            //HTML special bind
            return htmlBinding.BindProperty(
                model,
                value,
                prop.Name,
                prop.PropertyType,
                attributes,
                validators,
                out checkedValue);
        }

        /// <summary>
        /// validatorを取得する。/ Get validators
        /// </summary>
        /// <param name="propertyDescriptor"></param>
        /// <returns></returns>
        public static IEnumerable<ErsValidationBase> GetValidators(IEnumerable<Attribute> attributes)
        {
            foreach (var attr in attributes)
            {
                //Validator検索
                var validator = attr as ErsValidationBase;
                if (validator != null)
                    yield return validator;
            }
        }

        /// <summary>
        /// IHtmlBindingを取得する / Get IHTML Binding
        /// </summary>
        /// <param name="propertyDescriptor"></param>
        /// <returns></returns>
        public static IHtmlBinding GetIHtmlBinding(IEnumerable<Attribute> attributes)
        {
            foreach (Attribute attr in attributes)
            {
                if (attr is IHtmlBinding)
                    return (IHtmlBinding)attr;
            }
            return null;
        }

        /// <summary>
        /// オブジェクトに値をセットする。（値がカンマ区切りの場合は、配列に変換してプロパティにセットする。）
		/// <para>Sets the value of an object (If the value is comma-delimited it will be converted to an array)</para>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prop"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static void SetProperty<T>(T model, System.Reflection.PropertyInfo prop, object value)
            where T : IErsModelBase
        {
            value = GetConcreteValue(prop.PropertyType, value);
            ErsReflection.SetProperty(prop, model, value, null);
        }

        /// <summary>
        /// フィールドチェックが成功したら、全体チェックを行う
		/// <para>Field check sucessful, perform a thorough check</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        public static void CallValidate<T>(T model)
            where T : IErsModelBase
        {
            foreach (var result in model.Validate())
            {
                if (result != null)
                {
                    if (result.MemberNames.Count() > 0)
                    {
                        foreach (var field in result.MemberNames)
                        {
                            model.AddInvalidField(field, result.ErrorMessage);
                        }
                    }
                    else
                    {
                        model.AddInvalidField(string.Empty, result.ErrorMessage);
                    }
                }
            }
        }

        /// <summary>
        /// 値を検証する。/ Validate the value
        /// </summary>
        /// <param name="bindingContext"></param>
        /// <param name="propertyDescriptor"></param>
        /// <param name="checkValue"></param>
        /// <param name="validators"></param>
        /// <returns></returns>
        public static IEnumerable<ValidationResult> Validate(string propertyName, string checkValue, IEnumerable<ErsValidationBase> validators, out object checkedValue, Type propertyType, IEnumerable<Attribute> propertyAttributes)
        {
            //Enum対応
            if (GetActualType(propertyType).IsEnum)
            {
                checkValue = Convert.ToString(ConvertToEnum(GetActualType(propertyType), checkValue));
            }

            checkedValue = null;
            var retResult = new List<ValidationResult>();
            foreach (var validator in validators)
            {
                ValidationResult validationResult;
                checkedValue = validator.Validate(out validationResult, propertyAttributes, propertyName, checkValue);
                if (validationResult != null)
                {
                    retResult.Add(validationResult);
                }
            }
            return retResult;
        }

        /// <summary>
        /// Enumへコンバートする
        /// </summary>
        /// <param name="propertyType"></param>
        /// <param name="checkValue"></param>
        /// <returns></returns>
        private static object ConvertToEnum(Type propertyType, string checkValue)
        {
            object value;
            try
            {
                value = Enum.Parse(propertyType, checkValue, true);
            }
            catch (Exception e)
            {
                if (e is ArgumentException ||
                    e is OverflowException)
                {
                    return checkValue;
                }

                throw;
            }

            return Convert.ToInt32(value);
        }

        /// <summary>
        /// 処理に必要な型を取得する
        /// </summary>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        private static Type GetActualType(Type propertyType)
        {
            var checkPropertyType = propertyType;
            if (ErsReflection.IsNullable(propertyType))
            {
                checkPropertyType = ErsReflection.GetNullableBaseType(propertyType);
            }
            return checkPropertyType;
        }

        /// <summary>
        /// Create the concrete instance of the property type.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static object GetConcreteValue(Type type, object value)
        {
            if (value != DBNull.Value && type.IsArray)
            {
                if (value is string && string.IsNullOrEmpty((string)value))
                {
                    return null;
                }
                else if (value == null)
                {
                    return null;
                }
                return ErsReflection.GetArrayBySplit(value, type, new[] { ',' });
            }

            return ErsReflection.ConvertValue(type, value);
        }
    }
}
