using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc
{
    public class ErsViewEngine : VirtualPathProviderViewEngine
    {

        // ビュー・スクリプトの検索先を定義（コンストラクタ）
        public ErsViewEngine()
        {
            this.ViewLocationFormats =
            this.PartialViewLocationFormats = new[] {
                "~/Views/{1}/{0}.htm",
                "~/Views/{1}/{0}.html",
                "~/Views/Shared/{0}.htm",
                "~/Views/Shared/{0}.html",
                "~/Views/Partial/{0}.htm",
                "~/Views/Partial/{0}.html"
            };
        }

        // ビューを生成するためのメソッド
        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            return new ErsView().Init(viewPath, controllerContext);
        }

        // 部分ビューを生成するためのメソッド
        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            return new ErsView().Init(partialPath, controllerContext);
        }
    }
}