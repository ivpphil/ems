﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.util
{
    /// <summary>
    /// pdf field to be printed
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PdfFieldAttribute
        : SortableAttribute
    {
        
        public int[] request_types { get; set; }

        public PdfFieldAttribute([CallerLineNumber]int order = 0)
            : base(order)
        {
        }
    }
}
