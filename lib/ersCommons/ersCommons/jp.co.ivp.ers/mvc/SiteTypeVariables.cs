﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.IO;

namespace jp.co.ivp.ers.mvc
{
    public class SiteTypeVariables
    {
        /// <summary>
        /// Returns the configuration value of nor_url
        /// </summary>
        public static string GetNorUrl(EnumSiteType site_type, int site_id)
        {
            var setup = new SetupConfigReader();
            if (setup.Multiple_sites)
            {
                switch (site_type)
                {
                    case EnumSiteType.PC:
                        return setup.multiple_pc_nor_url(site_id);
                    case EnumSiteType.MOBILE:
                        return setup.multiple_mobile_nor_url(site_id);
                    case EnumSiteType.CONTACT:
                        return setup.multiple_contact_nor_url(site_id);
                    case EnumSiteType.ADMIN:
                        return setup.admin_nor_url;
                    case EnumSiteType.SMARTPHONE:
                        return setup.multiple_smartphone_nor_url(site_id);
                    case EnumSiteType.MONITOR:
                        return setup.admin_nor_url;
                    default:
                        throw new Exception("Can not decide the site type.");
                }
            }
            else
            {
                switch (site_type)
                {
                    case EnumSiteType.PC:
                        return setup.pc_nor_url;
                    case EnumSiteType.MOBILE:
                        return setup.mobile_nor_url;
                    case EnumSiteType.CONTACT:
                        return setup.contact_nor_url;
                    case EnumSiteType.ADMIN:
                        return setup.admin_nor_url;
                    case EnumSiteType.SMARTPHONE:
                        return setup.smartphone_nor_url;
                    case EnumSiteType.MONITOR:
                        return setup.admin_nor_url;
                    default:
                        throw new Exception("Can not decide the site type.");
                }
            }
        }

        /// <summary>
        /// Returns the configuration value of sec_url
        /// </summary>
        public static string GetSecUrl(EnumSiteType site_type, int site_id)
        {
            var setup = new SetupConfigReader();
            if (setup.Multiple_sites)
            {
                switch (site_type)
                {
                    case EnumSiteType.PC:
                        return setup.multiple_pc_sec_url(site_id);
                    case EnumSiteType.MOBILE:
                        return setup.multiple_mobile_sec_url(site_id);
                    case EnumSiteType.CONTACT:
                        return setup.multiple_contact_sec_url(site_id);
                    case EnumSiteType.ADMIN:
                    case EnumSiteType.MONITOR:
                    case EnumSiteType.BATCH:
                        return setup.admin_sec_url;
                    case EnumSiteType.SMARTPHONE:
                        return setup.multiple_smartphone_sec_url(site_id);
                    default:
                        throw new Exception("Can not decide the site type.");
                }
            }
            else
            {
                switch (site_type)
                {
                    case EnumSiteType.PC:
                        return setup.pc_sec_url;
                    case EnumSiteType.MOBILE:
                        return setup.mobile_sec_url;
                    case EnumSiteType.CONTACT:
                        return setup.contact_sec_url;
                    case EnumSiteType.ADMIN:
                    case EnumSiteType.MONITOR:
                    case EnumSiteType.BATCH:
                        return setup.admin_sec_url;
                    case EnumSiteType.SMARTPHONE:
                        return setup.smartphone_sec_url;
                    default:
                        throw new Exception("Can not decide the site type.");
                }
            }
        }


        /// <summary>
        /// サイトタイプに属するアプリケーションルートの配列を返す
        /// </summary>
        public static List<string> GetApplicationRoot(EnumSiteType site_type)
        {
            var setup = new SetupConfigReader();
            var sites = setup.ersSiteId;

            var retList = new List<string>();
            
            switch (site_type)
            {
                case EnumSiteType.PC:

                    for (int i = 0; i < sites.Length; i++)
                    {
                        retList.Add( Path.Combine(setup.root_path, setup.multiple_pc_application_root(int.Parse(sites[i]))));
                    }
                    return retList;

                case EnumSiteType.MOBILE:
                    for (int i = 0; i < sites.Length; i++)
                    {
                        retList.Add(Path.Combine(setup.root_path, setup.multiple_mobile_application_root(int.Parse(sites[i]))));
                    }
                    return retList;

                case EnumSiteType.CONTACT:
                    for (int i = 0; i < sites.Length; i++)
                    {
                        retList.Add(Path.Combine(setup.root_path, setup.multiple_contact_application_root(int.Parse(sites[i]))));
                    }
                    return retList;
                case EnumSiteType.ADMIN:
                case EnumSiteType.MONITOR:
                case EnumSiteType.BATCH:
                    retList.Add(Path.Combine(setup.root_path, setup.admin_application_root));
                    return retList;
                
                case EnumSiteType.SMARTPHONE:
                    for (int i = 0; i < sites.Length; i++)
                    {
                        retList.Add(Path.Combine(setup.root_path, setup.multiple_smartphone_application_root(int.Parse(sites[i]))));
                    }
                    return retList;

                default:
                    throw new Exception("Can not decide the site type.");
            }

        }

        /// <summary>
        /// Returns the configuration value of sec_url
        /// </summary>
        public static string GetApplicationRoot(EnumSiteType site_type, int site_id)
        {
            var setup = new SetupConfigReader();
            if (setup.Multiple_sites)
            {
                switch (site_type)
                {
                    case EnumSiteType.PC:
                        return Path.Combine(setup.root_path, setup.multiple_pc_application_root(site_id));
                    case EnumSiteType.MOBILE:
                        return Path.Combine(setup.root_path, setup.multiple_mobile_application_root(site_id));
                    case EnumSiteType.CONTACT:
                        return Path.Combine(setup.root_path, setup.multiple_contact_application_root(site_id));
                    case EnumSiteType.ADMIN:
                    case EnumSiteType.MONITOR:
                    case EnumSiteType.BATCH:
                        return Path.Combine(setup.root_path, setup.admin_application_root);
                    case EnumSiteType.SMARTPHONE:
                        return Path.Combine(setup.root_path, setup.multiple_smartphone_application_root(site_id));
                    default:
                        throw new Exception("Can not decide the site type.");
                }
            }
            else
            {
                switch (site_type)
                {
                    case EnumSiteType.PC:
                        return Path.Combine(setup.root_path, setup.pc_application_root);
                    case EnumSiteType.MOBILE:
                        return Path.Combine(setup.root_path, setup.mobile_application_root);
                    case EnumSiteType.CONTACT:
                        return Path.Combine(setup.root_path, setup.contact_application_root);
                    case EnumSiteType.ADMIN:
                    case EnumSiteType.MONITOR:
                    case EnumSiteType.BATCH:
                        return Path.Combine(setup.root_path, setup.admin_application_root);
                    case EnumSiteType.SMARTPHONE:
                        return Path.Combine(setup.root_path, setup.smartphone_application_root);
                    default:
                        throw new Exception("Can not decide the site type.");
                }
            }
        }
    }
}
