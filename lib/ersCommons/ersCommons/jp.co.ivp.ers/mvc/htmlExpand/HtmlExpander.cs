﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc.htmlExpand
{
    public abstract class HtmlExpander
    {
        private static IList<HtmlExpander> listExpander;

        static HtmlExpander()
        {
            listExpander = new List<HtmlExpander>();

            //if you add htmlExpander, please add to listExpander at this.
            listExpander.Add(new ArrayExpander());



            listExpander.Add(new DefaultExpander());//this expander has to be insert at last.
        }

        protected abstract bool IfHandleValueOf(object value);

        protected abstract object Expand(object value);

        public static object ExpandValue(object value)
        {
            foreach(var expander in listExpander)
            {
                if(expander.IfHandleValueOf(value))
                {
                    return expander.Expand(value);
                }
            }
            return string.Empty;
        }
    }
}
