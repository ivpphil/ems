﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc.htmlExpand
{
    public class DefaultExpander
        : HtmlExpander
    {
		/// <returns>True</returns>
        protected override bool IfHandleValueOf(object value)
        {
            return true;
        }

		/// <summary>
		/// Converts the value of an object to its equivalent string representation
		/// </summary>
        protected override object Expand(object value)
        {
            return value;
        }
    }
}
