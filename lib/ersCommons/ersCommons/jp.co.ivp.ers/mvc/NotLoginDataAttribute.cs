﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Marks as that this property is not Login Data.(this is not cleared when the user logout.)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NotLoginDataAttribute : Attribute
    {
    }
}
