﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using System.CodeDom.Compiler;
using System.IO;
using System.Xml;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.compile.entity;
using jp.co.ivp.ers.mvc.compile;

namespace jp.co.ivp.ers.mvc
{
    abstract public class ErsMvcApplication : System.Web.HttpApplication
    {

        protected abstract string[] GetNamespace();
 
        protected virtual void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        protected virtual void RegisterRoutes(RouteCollection routes)
        {
            return;
        }

        protected virtual void Application_Start()
        {
            // MVCバージョンを非表示
            MvcHandler.DisableMvcResponseHeader = true;

            ErsCommonContext.Initialize(Context, Server.MapPath("~/"));

            //Factoryをセット / Set the Factory
            this.SetFactory();

            //Resolver設定
            this.SetResolver();
            
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            ValueProviderFactories.Factories.Remove(ValueProviderFactories.Factories.OfType<JsonValueProviderFactory>().FirstOrDefault());
            ValueProviderFactories.Factories.Add(new JsonDotNetValueProviderFactory());

            //ModelBinder上書き / Overwrite ModelBinder
            ModelBinders.Binders.DefaultBinder = new ErsModelBinder();

            //ViewEngine上書き / Overwrite ViewEngine
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new ErsViewEngine());

            //コンパイル済みView / Compiled View
            ManageTemplate.Initialize();

            //ViewのコンパイラをAddする。
            ErsTemplateParser.ParserList = this.GetErsTagParserList();

            //log4net設定 / log4net configuration
            this.InitLogger();
        }

        protected void InitLogger()
        {
            ErsLog4net.InitLogger();
        }

        protected abstract void SetFactory();

        protected abstract void SetResolver();

        /// <summary>
        /// ERS独自タグのパーサーのリストを取得する。
        /// </summary>
        /// <param name="viewContext"></param>
        /// <returns></returns>
        public virtual List<ErsTemplateEntityBase> GetErsTagParserList()
        {
            var retVal = new List<ErsTemplateEntityBase>();

            retVal.Add(new ErsTagUrlEncode());
            retVal.Add(new ErsTagComment());
            retVal.Add(new ErsTagOutputQuery());
            retVal.Add(new ErsTemplateForm());
            retVal.Add(new ErsTemplateAnchor());
            retVal.Add(new ErsTagPartial());
            retVal.Add(new ErsTagDispError());
            retVal.Add(new ErsTagDispInfo());
            retVal.Add(new ErsTagIsError());
            retVal.Add(new ErsTagForeach());
            retVal.Add(new ErsTagIsEqual());
            retVal.Add(new ErsTagIsNotEqual());
            retVal.Add(new ErsTagIsElse());
            retVal.Add(new ErsTagFormatString());
            retVal.Add(new ErsTagFormatCurrency());
            retVal.Add(new ErsTagFormatNumber());
            retVal.Add(new ErsTagFormatDate());
            retVal.Add(new ErsTagMessage());
            retVal.Add(new ErsTagLabel());
            retVal.Add(new ErsTagMultiLine());
            retVal.Add(new ErsTagTable());
            retVal.Add(new ErsTagNonEncode());
            retVal.Add(new ErsTagFormatHtml());
            retVal.Add(new ErsRemoveInvalidInputAttributes());
            retVal.Add(new ErsTagMiniProfiler());

            return retVal;
        }
    }
}
