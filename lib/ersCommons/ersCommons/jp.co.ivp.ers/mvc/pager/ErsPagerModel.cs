﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using System.IO;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace jp.co.ivp.ers.mvc.pager

{
    /// <summary>
    /// TODO:仕様書に追記
    /// </summary>
    public class ErsPagerModel
        : ErsModelBase
    {

        public void Init(ErsControllerBase controller, string modelNameInView, int pageCnt, long maxItemCount)
        {
            var setup = new SetupConfigReader();
            if (setup.onVEX)
            {
                maxItemCount = 2;
            }
            this.Init(pageCnt, maxItemCount);
            controller.AddModelToView(modelNameInView, this);
            this.MaxPagerCount = setup.PagerLinkNumber; 
        }

        public void Init(ErsControllerBase controller, string modelNameInView, int pageCnt, long maxItemCount, int maxPagerCount)
        {
            var setup = new SetupConfigReader();
            if (setup.onVEX)
            {
                maxItemCount = 2;
            }
            this.Init(pageCnt, maxItemCount);
            controller.AddModelToView(modelNameInView, this);
            this.MaxPagerCount = maxPagerCount; 
 
        }

        public void Init(int pageCnt, long maxItemCount)
        {
            this.pageCnt = pageCnt;
            if (this.pageCnt <= 0)
            {
                this.pageCnt = 1;
            }

            this.maxItemCount = maxItemCount;
            this.current_filename = Path.GetFileName(HttpContext.Current.Request.Url.AbsolutePath);
            this.MaxPagerCount = new SetupConfigReader().PagerLinkNumber; 

        }

        /// <summary>
        /// POST先のURL
        /// Gets current filename.(default value is the filename of a request url.)
        /// </summary>
        public virtual string current_filename { get; private set; }

        /// <summary>
        /// ページャーのaction先URLを指定する。
        /// Sets the action url.
        /// </summary>
        /// <param name="file_name"></param>
        public virtual void SetCurrent_filename(string file_name)
        {
            this.current_filename = file_name;
        }

        //ページ送り　検索件数
		//Search paging number
        protected long maxItemCount;

        //ページ送りの作成
		//Creating a page feed
        public virtual List<Dictionary<string, object>> PageList { get; private set; }

        /// <summary>
        /// CriteriaにLimitとOffsetを指定する
		/// <para>Specify the Offset and Limit to Criteria</para>
        /// </summary>
        /// <param name="criteria"></param>
        public virtual void SetLimitAndOffsetToCriteria(ICriteria criteria)
        {
            criteria.LIMIT = maxItemCount;
            criteria.OFFSET = (this.pageCnt - 1) * maxItemCount;
        }

        /// <summary>
        /// CriteriaにLimitとOffsetを指定する
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="recordCount"></param>
        public virtual void SetLimitAndOffsetToCriteria(ICriteria criteria, long recordCount)
        {
            this.allRecordCnt = Convert.ToInt32(recordCount);
            this.LastPage = this.CountAllPage(recordCount);

            if (this.LastPage > 0 && this.pageCnt >= this.LastPage)
            {
                this.pageCnt = Convert.ToInt32(this.LastPage);
                this.fromCnt = this.GetFromCnt();
                this.toCnt = this.GetToCnt();
            }

            criteria.LIMIT = maxItemCount;
            criteria.OFFSET = (this.pageCnt - 1) * maxItemCount;
        }

        /// <summary>
        /// number of links at Page ejection.
        /// </summary>
        public int MaxPagerCount { get; protected set; }

        // 総件数
        public int allRecordCnt { get; protected set; }

        // 現在のページ
        public int pageCnt { get; protected set; }

        // 現在のページの表示件数
        public long rCnt { get; protected set; }

        // 現在のページの開始
        public int fromCnt { get; protected set; }

        // 現在のページの終了
        public int toCnt { get; protected set; }

        //ページ送り：前への表示許可フラグ(0:非表示 / 1:表示)
		//flag to enable previous display on paging (0: display / 1: hidden)
        public bool PagePrevFlg { get; protected set; }
        public int backCnt { get { return pageCnt - 1; } }

        //ページ送り：次への表示許可フラグ(0:非表示 / 1:表示)
		//flag to enable next display on paging (0: display / 1: hidden)
        public bool PageNextFlg { get; protected set; }
        public int nextCnt { get { return pageCnt + 1; } }

        //ページ送り：最終ページへ数
        public long LastPage { get; protected set; }

        /// <summary>
        /// Load Pager view data source.
        /// </summary>
        public virtual void LoadPageList()
        {
            long page_start = this.GetFirstPageNumber(this.LastPage);

            long page_end = this.GetLastPageNumber(this.LastPage, page_start);

            this.PagePrevFlg = this.GetPagePrevFlg(this.LastPage, page_start);

            this.PageNextFlg = this.GetPageNextFlg(this.LastPage, page_end);

            this.rCnt = this.GetRCnt();

            this.fromCnt = this.GetFromCnt();

            this.toCnt = this.GetToCnt();

            this.PageList = this.GetPageList(page_start, page_end);
        }

        /// <summary>
        /// Load Pager view data source.
        /// </summary>
        /// <param name="recordCount"></param>
        public virtual void LoadPageList(long recordCount)
        {
            long pageall = this.CountAllPage(recordCount);
            this.LastPage = pageall;
            this.allRecordCnt = (int)recordCount;
            this.LoadPageList();
        }


        /// <summary>
        /// count all page
        /// </summary>
        /// <param name="recordCount"></param>
        /// <returns></returns>
        protected virtual long CountAllPage(long recordCount)
        {
            long pageall = recordCount / maxItemCount;
            if (recordCount % maxItemCount > 0)
            {
                pageall += 1;	// 余りは切り上げ, for remainder add another page
            }
            return pageall;
        }

        /// <summary>
        /// 次リンクの表示許可
		/// <para>Permission to view the next page link</para>
        /// </summary>
        /// <param name="pageall"></param>
        /// <param name="page_end"></param>
        /// <returns></returns>
        protected virtual bool GetPageNextFlg(long pageall, long page_end)
        {
            if (pageall > 0)
            {
                if (pageCnt < pageall)
                {
                    return true;  //「次へ」の表示許可, permission to view next
                }
            }
            return false;
        }

        /// <summary>
        /// 前リンクの表示許可
		/// <para>Permission to view previous page link</para>
        /// </summary>
        /// <param name="pageall"></param>
        /// <param name="page_start"></param>
        /// <returns></returns>
        protected virtual bool GetPagePrevFlg(long pageall, long page_start)
        {
            if (pageall > 0)
            {
                if (pageCnt > 1)
                {
                    return true;   //「前へ」の表示許可, permission to view previous
                }
            }
            return false;
        }

        /// <summary>
        /// 開始ページの決定
		/// <para>Determine starting page number</para>
        /// </summary>
        /// <param name="pageall"></param>
        /// <returns></returns>
        protected virtual long GetFirstPageNumber(long pageall)
        {
            long page_start = 1;

            if (pageall > MaxPagerCount)
            {
                page_start = pageCnt - (int)MaxPagerCount / 2;

                if (page_start < 1)
                {
                    page_start = 1;
                }

                if ((page_start + (MaxPagerCount - 1)) > pageall)
                {
                    page_start = pageall - (MaxPagerCount - 1);
                }
            }
            return page_start;
        }

        /// <summary>
        /// 終了ページの決定
		/// <para>Get last page number</para>
        /// </summary>
        /// <param name="pageall"></param>
        /// <param name="page_start"></param>
        /// <returns></returns>
        protected virtual long GetLastPageNumber(long pageall, long page_start)
        {
            long page_end = pageall;
            if (pageall > MaxPagerCount)
            {
                page_end = page_start + (MaxPagerCount - 1);

                if (page_end > pageall)
                {
                    page_end = pageall;
                }
            }
            return page_end;
        }

        /// <summary>
        /// 現在の表示件数
        /// </summary>
        /// <returns></returns>
        protected virtual int GetRCnt()
        {
            int rCnt = 0;

            if (this.allRecordCnt > 0)
            {
                if (this.pageCnt == this.LastPage)
                {
                    rCnt = Convert.ToInt32(this.allRecordCnt - (pageCnt - 1) * maxItemCount);
                }
                else
                {
                    rCnt = Convert.ToInt32(maxItemCount);
                }
            }

            return rCnt;
        }

        /// <summary>
        /// 現在のページの開始
        /// </summary>
        /// <returns></returns>
        protected virtual int GetFromCnt()
        {
            int fromCnt = 0;

            if (this.allRecordCnt > 0)
            {
                fromCnt = Convert.ToInt32(((this.pageCnt - 1) * this.maxItemCount) + 1);
            }

            return fromCnt;
        }

        /// <summary>
        /// 現在のページの終了
        /// </summary>
        /// <returns></returns>
        protected virtual int GetToCnt()
        {
            int toCnt = 0;

            if (this.allRecordCnt > 0)
            {
                if (this.pageCnt == this.LastPage)
                {
                    toCnt = Convert.ToInt32((this.pageCnt * this.maxItemCount) - ((this.pageCnt * this.maxItemCount) - this.allRecordCnt));
                }
                else
                {
                    toCnt = Convert.ToInt32(this.pageCnt * this.maxItemCount);
                }
            }

            return toCnt;
        }

        /// <summary>
        /// ページリンクをセット
		/// <para>Set the page link</para>
        /// </summary>
        /// <param name="page_start"></param>
        /// <param name="page_end"></param>
        /// <returns></returns>
        protected virtual List<Dictionary<string, object>> GetPageList(long page_start, long page_end)
        {
            var Setlist = new List<Dictionary<string, object>>();
            for (long icnt = page_start; icnt <= page_end; icnt++)
            {
                var dic = new Dictionary<string, object>();
                dic["pagecnt"] = icnt;
                Setlist.Add(dic);
            }
            return Setlist;
        }
    }
}