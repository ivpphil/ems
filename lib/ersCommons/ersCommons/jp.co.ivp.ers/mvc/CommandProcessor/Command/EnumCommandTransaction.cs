﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc.CommandProcessor.Command
{
    public enum EnumCommandTransaction
    {
        BeginTransaction,
        WithoutBeginTransaction
    }
}
