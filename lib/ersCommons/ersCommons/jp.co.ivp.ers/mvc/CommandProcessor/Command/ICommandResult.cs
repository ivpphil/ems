﻿namespace jp.co.ivp.ers.mvc.CommandProcessor.Command
{
    public interface ICommandResult
    {
        bool Success { get; }
    }
}

