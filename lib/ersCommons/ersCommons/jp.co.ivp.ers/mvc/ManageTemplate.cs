﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom.Compiler;
using System.IO;
using jp.co.ivp.ers.util;
using System.Web;
using System.Xml;
using System.Security.Cryptography;
using jp.co.ivp.ers.mvc.template;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.mvc
{
    internal class ManageTemplate
    {
        private static HashAlgorithm crypto = new SHA1CryptoServiceProvider();

        private static IList<FileSystemWatcher> ListViewDirectoryWatcher
        {
            get
            {
                return (IList<FileSystemWatcher>)ErsCommonContext.GetObjectFromApplication("ListViewDirectoryWatcher");
            }
            set
            {
                ErsCommonContext.SetObjectToApplication("ListViewDirectoryWatcher", value);
            }
        }

        internal static void Initialize()
        {
            MessageResourceDictionary.Reload();
            FieldNameResourceDictionary.Reload();
            ReturnUrlResourceDictionary.Reload();

            

            ListViewDirectoryWatcher = new List<FileSystemWatcher>();
            foreach (EnumSiteType site_type in Enum.GetValues(typeof(EnumSiteType)))
            {
                var application_roots = SiteTypeVariables.GetApplicationRoot(site_type);

                foreach (var application_root in application_roots)
                {
                    var monitorPath = Path.Combine(application_root, "Views");

                    if (!Directory.Exists(monitorPath))
                    {
                        continue;
                    }

                    //テンプレートフォルダ監視（ファイルが更新されたらコンパイル済みViewを削除）
                    //Monitoring of template folder (Delete a View if a compiled file has been updated)
                    var ViewDirectoryWatcher = new System.IO.FileSystemWatcher(monitorPath);

                    ViewDirectoryWatcher.NotifyFilter = NotifyFilters.Attributes | NotifyFilters.CreationTime | NotifyFilters.DirectoryName | NotifyFilters.FileName | NotifyFilters.LastWrite | NotifyFilters.Security | NotifyFilters.Size;

                    ViewDirectoryWatcher.EnableRaisingEvents = true;
                    ViewDirectoryWatcher.IncludeSubdirectories = true;

                    ViewDirectoryWatcher.Changed += new FileSystemEventHandler(OnChanged);

                    ListViewDirectoryWatcher.Add(ViewDirectoryWatcher);
                }
            }
        }

        /// <summary>
        /// ファイルが更新されたらコンパイル済みViewを削除。リソースもリセット
		/// Delete a compiled View if the file has been updated
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            var filePath = e.FullPath;

            CompilerResultsDictionary.Reload(filePath);

            if (MessageResourceDictionary.MessageResourceFileName == filePath)
            {
                MessageResourceDictionary.Reload();
            }

            if (FieldNameResourceDictionary.FieldNameResourceFileName == filePath)
            {
                FieldNameResourceDictionary.Reload();
            }

            if (MessageResourceDictionary.defaultMessageResourceFileName == filePath)
            {
                MessageResourceDictionary.Reload();
            }

            if (FieldNameResourceDictionary.defaultFieldNameResourceFileName == filePath)
            {
                FieldNameResourceDictionary.Reload();
            }

            if (ReturnUrlResourceDictionary.ReturnUrlResourceFileName == filePath)
            {
                ReturnUrlResourceDictionary.Reload();
            }
        }

		/// <summary>
		/// Gets the ClassName from the specified path
		/// </summary>
        internal static string GetClassName(string requestedPath)
        {
            string path;
            if (ErsCommonContext.IsBatch)
            {
                path = requestedPath;
            }
            else
            {
                path = ErsCommonContext.MapPath(requestedPath);
            }
            
            path = ErsCommonContext.MapPath("~/") + path;

            var fileName = Path.GetFileName(path);
            var hashstring = BitConverter.ToString(ComputeHash(path));
            return (hashstring + fileName).Replace("-", "").Replace(".", "");
        }

        private static byte[] ComputeHash(string path)
        {
            lock (crypto)
            {
                return crypto.ComputeHash(System.Text.Encoding.UTF8.GetBytes(path));
            }
        }
    }
}