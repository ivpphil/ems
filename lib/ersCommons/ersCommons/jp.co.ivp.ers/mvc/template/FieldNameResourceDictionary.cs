﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using System.Web;

namespace jp.co.ivp.ers.mvc.template
{
    public class FieldNameResourceDictionary
    {
        public static string defaultFieldNameResourceFileName { get { return ErsCommonContext.MapPath("~/Views/FieldNameResource.en-US.config"); } }
        public static string defaultCulture { get { return "ja-JP"; } }
        private static string culture { get; set; }

        /// <summary>
        /// Get FieldNameResource.config location.
        /// </summary>
        public static string FieldNameResourceFileName
        {
            get
            {
                var setup = new SetupConfigReader();
                var fieldNameResourceFileName = setup.FieldNameResource;

                if (string.IsNullOrEmpty(fieldNameResourceFileName))
                {
                    fieldNameResourceFileName = "~/Views/FieldNameResource.en-US.config";
                }
                culture = SetupConfigReader.CommonConfigGlobalizationCulture;
                if (!string.IsNullOrEmpty(culture))
                {
                    var cultureFile = fieldNameResourceFileName.Replace(".config", "." + culture + ".config");
                    if (File.Exists(ErsCommonContext.MapPath(cultureFile))) fieldNameResourceFileName = cultureFile;
                    
                }
                return ErsCommonContext.MapPath(fieldNameResourceFileName);
            }
        }

        /// <summary>
        /// Get FieldNameResource.config location.
        /// </summary>
        public static List<string> FieldNameResourceFileNames
        {
            get
            {
                var files = Directory.GetFiles(ErsCommonContext.MapPath("~/Views/"));
                var fieldFiles = new List<string>();
                foreach (string file in files)
                {
                    if (file.Contains("FieldNameResource") && File.Exists(file))
                    {
                        fieldFiles.Add(file);
                    }
                }
                return fieldFiles;
            }
        }

        /// <summary>
        /// フィールド名や単語のリソースを取得する
        /// <para>Gets the resource of field names and words</para>
        /// </summary>
        internal static Dictionary<string, Dictionary<string, string>> dicFieldNameResource
        {
            get
            {
                var result = (Dictionary<string, Dictionary<string, string>>)ErsCommonContext.GetObjectFromApplication("dicFieldNameResource");
                if (result == null)
                {
                    Reload();
                    result = (Dictionary<string, Dictionary<string, string>>)ErsCommonContext.GetObjectFromApplication("dicFieldNameResource");
                }
                return result;
            }
            private set
            {
                ErsCommonContext.SetObjectToApplication("dicFieldNameResource", value);
            }
        }

        /// <summary>
        /// フィールドリソースを初期化 
		/// <para>Initializes the resource fields</para>
        /// </summary>
        public static void Reload()
        {
            var toPool = new Dictionary<string, Dictionary<string, string>>();
            foreach (string file in FieldNameResourceFileNames)
            {
                var fname = new FileInfo(file);
                string[] fnamepart = fname.Name.Split('.');
                if (fnamepart.Length == 3)
                {
                    toPool.Add(fnamepart[1], ErsCommon.LoadXml(file));
                }
                else
                {
                    toPool.Add(defaultCulture, ErsCommon.LoadXml(defaultFieldNameResourceFileName));
                }
            }
            dicFieldNameResource = toPool;
        }
    }
}
