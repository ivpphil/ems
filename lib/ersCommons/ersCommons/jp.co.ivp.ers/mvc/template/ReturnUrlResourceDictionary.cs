﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.IO;

namespace jp.co.ivp.ers.mvc.template
{
    class ReturnUrlResourceDictionary
    {
        /// <summary>
        /// Get MessageResource.config location.
        /// </summary>
        public static string ReturnUrlResourceFileName
        {
            get
            {
                var setup = new SetupConfigReader();
                var returnUrlResourceFileName = setup.ReturnUrlResource;

                if (string.IsNullOrEmpty(returnUrlResourceFileName))
                {
                    returnUrlResourceFileName = "~/Views/ReturnUrlResource.config";
                }

                return ErsCommonContext.MapPath(returnUrlResourceFileName);
            }
        }

        /// <summary>
        /// メッセージのリソースを取得する
        /// </summary>
        internal static Dictionary<string, string> dicReturnUrlResource
        {
            get
            {
                var result = (Dictionary<string, string>)ErsCommonContext.GetObjectFromApplication("dicReturnUrlResource");
                if (result == null)
                {
                    result = (Dictionary<string, string>)ErsCommonContext.GetObjectFromApplication("dicReturnUrlResource");
                }
                return result;
            }
            private set
            {
                ErsCommonContext.SetObjectToApplication("dicReturnUrlResource", value);
            }
        }

        /// <summary>
        /// メッセージリソースを初期化
        /// </summary>
        internal static void Reload()
        {
            if (File.Exists(ReturnUrlResourceFileName))
            {
                dicReturnUrlResource = ErsCommon.LoadXml(ReturnUrlResourceFileName);
            }
        }
    }
}
