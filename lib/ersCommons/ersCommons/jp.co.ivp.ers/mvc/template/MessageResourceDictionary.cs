﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using System.Web;

namespace jp.co.ivp.ers.mvc.template
{
    public class MessageResourceDictionary
    {
        public static string defaultMessageResourceFileName { get { return ErsCommonContext.MapPath("~/Views/MessageResource.en-US.config"); } }

        public static string defaultCulture {get { return "ja-JP"; }}

        private static string culture { get; set; }

        /// <summary>
        /// Get MessageResource.config location.
        /// </summary>
        public static string MessageResourceFileName
        {
            get
            {
                var setup = new SetupConfigReader();
                var messageResourceFileName = setup.MessageResource;
                
                if (string.IsNullOrEmpty(messageResourceFileName))
                {
                    messageResourceFileName = "~/Views/MessageResource.en-US.config";
                }
                culture = SetupConfigReader.CommonConfigGlobalizationCulture;
                if (!string.IsNullOrEmpty(culture))
                {
                    var cultureFile = messageResourceFileName .Replace(".config", "." + culture + ".config");
                    if (File.Exists(ErsCommonContext.MapPath(cultureFile))) messageResourceFileName = cultureFile;
                }
                return ErsCommonContext.MapPath(messageResourceFileName);
            }
        }

        public static List<string> MessageResourceFileNames
        {
            get
            {
                var files = Directory.GetFiles(ErsCommonContext.MapPath("~/Views/"));
                var fieldFiles = new List<string>();
                foreach (string file in files)
                {
                    if (file.Contains("MessageResource") && File.Exists(file))
                    {
                        fieldFiles.Add(file);
                    }
                }
                return fieldFiles;
            }
        }

        /// <summary>
        /// メッセージのリソースを取得する
		/// <para>Gets the resource of the message</para>
        /// </summary>
        public static Dictionary<string, Dictionary<string, string>> dicMessageResource
        {
            get
            {
                var result = (Dictionary<string, Dictionary<string, string>>)ErsCommonContext.GetObjectFromApplication("dicMessageResource");
                if (result == null)
                {
                    Reload();
                    result = (Dictionary<string, Dictionary<string, string>>)ErsCommonContext.GetObjectFromApplication("dicMessageResource");
                }
                return result;
            }
            private set
            {
                ErsCommonContext.SetObjectToApplication("dicMessageResource", value);
            }
        }

        /// <summary>
        /// メッセージリソースを初期化
		/// <para>Initializes the message resource</para>
        /// </summary>
        public static void Reload()
        {
            var toPool = new Dictionary<string, Dictionary<string, string>>();

            foreach (string file in MessageResourceFileNames)
            {
                var fname = new FileInfo(file);
                string[] fnamepart = fname.Name.Split('.');
                if (fnamepart.Length == 3)
                {
                    toPool.Add(fnamepart[1], ErsCommon.LoadXml(file));
                }
                else
                {
                    toPool.Add(defaultCulture, ErsCommon.LoadXml(defaultMessageResourceFileName));
                }
            }
            dicMessageResource = toPool;
        }
    }
}
