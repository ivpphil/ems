﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using jp.co.ivp.ers.util;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc.validation;
using System.Web;
using jp.co.ivp.ers.mvc.htmlExpand;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace jp.co.ivp.ers.mvc
{
    public interface IErsModelBase
         : IErsCollectable, IOverwritable, ICloneable
    {
        /// <summary>
        /// 検証結果
        /// <para>Verification result</para>
        /// </summary>
        bool IsValid { get; }

        /// <summary>
        /// 行番号/Line Number
        /// </summary>
        int lineNumber { get; set; }

        /// <summary>
        /// 行番号
        /// <para>Line number</para>
        /// </summary>
        string lineName { get; }

        /// <summary>
        /// フィールドの検証結果を取得する
        /// </summary>
        bool IsValidField(params string[] fieldName);

        /// <summary>
        /// エラーありフィールド名のリストを取得する
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> GetInvalidFieldNames();

        /// <summary>
        /// 入力値検証後の複合チェックを行う。
        /// <para>Input validation</para>
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        IEnumerable<ValidationResult> Validate(ValidationContext validationContext);

        /// <summary>
        /// 入力値検証後の複合チェックを行う。(CSV/TableBind用)
        /// <para>Input validation (for CSV/TableBind)</para>
        /// </summary>
        /// <param name="lineName"></param>
        /// <returns></returns>
        IEnumerable<ValidationResult> Validate();

        /// <summary>
        ///  不正入力された値が入る（View表示用）
        /// </summary>
        Dictionary<string, object> InvalidValues { get; }

        /// <summary>
        /// ErsOutputHidden属性がついたプロパティをすべて出力する
        /// <para>Print all the property that has SetOutputHidden attribute</para>
        /// </summary>
        /// <param name="model"></param>
        void SetOutputHidden(bool value);

        /// <summary>
        /// 指定したグループ名を持つErsOutputHidden属性がついたプロパティを出力する
        /// <para>Print all the property that has SetOutputHidden attribute with a specified group</para>
        /// </summary>
        /// <param name="model"></param>
        void SetOutputHidden(string groupName, bool value);

        /// <summary>
        /// 指定されたpublic propertyにDictionaryの値をセットする
        /// <para>Get public properties as Dictionary with a specified attribute</para>
        /// </summary>
        /// <returns></returns>
        void OverwriteWithParameter(IDictionary<string, object> dictionary, string bindTargetName);

        /// <summary>
        /// 指定されたpublic propertyをDictionaryにセットして戻す
        /// </summary>
        /// <param name="targetProperty"></param>
        /// <returns></returns>
        Dictionary<string, object> GetPropertiesAsDictionary(string bindTargetName);

        /// <summary>
        /// 指定した属性をもつpublic propertyをDictionaryにセットして戻す
        /// <para>Get public properties as Dictionary with a specified attribute</para>
        /// </summary>
        /// <returns></returns>
        Dictionary<string, object> GetPropertiesAsDictionary(Type attr);

        /// <summary>
        /// 追加の必須チェック。Validateメソッド内で呼ぶ。
        /// <para>Check additional required field called in the Validate method</para>
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        ValidationResult CheckRequired(string displayName, string propertyName, object val);

        /// <summary>
        /// 追加の必須チェック。Validateメソッド内で呼ぶ。
        /// <para>Check for the additional required values</para>
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        ValidationResult CheckRequired(string lineName, string propertyName);

        /// <summary>
        /// 追加の必須チェック。Validateメソッド内で呼ぶ。
        /// <para>Check for the additional required values</para>
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        ValidationResult CheckRequired(string propertyName);

        /// <summary>
        /// エラーありフィールド名のリストに追加する。
        /// <para>Add to a list of fieldnames that has an error</para>
        /// </summary>
        /// <param name="fieldName"></param>
        void AddInvalidField(string fieldName, string errorMessage);

        /// <summary>
        /// 検証結果をロードします
        /// </summary>
        /// <param name="listValidationResult"></param>
        void AddInvalidField(IEnumerable<ValidationResult> listValidationResult);

        /// <summary>
        /// エラーありフィールド名のリストから削除する。
        /// </summary>
        /// <param name="key"></param>
        void RemoveInvalidField(string key);

        /// <summary>
        /// コントローラーへの参照
        /// </summary>
        ErsControllerBase controller { get; }

        /// <summary>
        /// 親モデルインスタンス
        /// </summary>
        IErsModelBase containerModel { get; set; }

        /// <summary>
        /// 入力チェックの結果を返却する
        /// </summary>
        List<string> GetAllErrorMessageList(string parentLineName = "");

        /// <summary>
        /// モデルの不正な値を削除する。
        /// </summary>
        /// <param name="key"></param>
        void ClearInvalidFields();
    }
}
