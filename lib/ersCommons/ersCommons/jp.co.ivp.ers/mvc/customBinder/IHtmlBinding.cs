﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using System.Web.Routing;
using System.Reflection;

namespace jp.co.ivp.ers.mvc
{
    /// <summary>
    /// HTMLをバインドする際の、特殊なバインドを定義する。
	/// Define when you want to bind the HTML, the special binding.
    /// </summary>
    public interface IHtmlBinding
    {
        IEnumerable<ValidationResult> BindProperty(object model, object requestValue, string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators, out object checkedValue);

        object GetRequestValue(string propertyName, RouteData routeData);

        object GetRequestValue(string propertyName, IDictionary<string, object> valueSource);

        void GetOutputHidden(List<ErsOutputHiddenTarget> listTarget, PropertyInfo property, string propertyName_frefix, string propertyName, Func<object> valueFunc, Dictionary<string, bool> OutputHidden);
    }

}
