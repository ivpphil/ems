﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using jp.co.ivp.ers.mvc;
using System.IO;
using Microsoft.VisualBasic.FileIO;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc
{
    public abstract class ErsCsvContainerBase
    {
        /// <summary>
        /// 親モデルインスタンス
        /// </summary>
        public IErsModelBase containerModel { get; protected internal set; }

        /// <summary>
        /// アップロードデータ
        /// <para>Upload data</para>
        /// </summary>
        public HttpPostedFileBase csv_file { get; protected internal set; }

        /// <summary>
        /// テンポラリに保存したファイル名
        /// <para>name of temporary file</para>
        /// </summary>
        public virtual string fileName { get; protected internal set; }

        protected internal virtual bool IsValidAtBinding { get; set; }

        public abstract void LoadPostedFile();
    }

    public class ErsCsvContainer<T>
        : ErsCsvContainerBase
        where T : ErsBindableModel
    {
        protected string UPLOAD_FILE_PATH;

        public ErsCsvContainer()
        {
            if (ErsCommonContext.IsBatch)
            {
                UPLOAD_FILE_PATH = string.Empty;
            }
            else
            {
                UPLOAD_FILE_PATH = ErsCommonContext.MapPath("~/file_upload/");
            }

            //Load schema
            this.LoadSchemaOfModel();
        }

        List<string> listSchema { get; set; }//TODO:staticな変数へPUTするように変更。

        private void LoadSchemaOfModel()
        {
            var listSchema = new List<string>();
            var t = typeof(T);

            var properties = t.GetProperties()
                .Where(prop => prop.GetCustomAttributes(typeof(CsvFieldAttribute), false).Length > 0)
                .OrderBy(
                    prop => prop.GetCustomAttributes(false).OfType<CsvFieldAttribute>().First().Order
                    );

            foreach (var prop in properties)
            {
                listSchema.Add(prop.Name);
            }

            this.listSchema = listSchema;
        }

        /// <summary>
        /// エンコーディング
        /// </summary>
        protected virtual Encoding encoding
        {
            get
            {
                return ErsEncoding.ShiftJIS;
            }
        }

        /// <summary>
        /// 正常なデータだったインデックスのリスト
		/// <para>List of normal indexes</para>
        /// </summary>
        public IEnumerable<int> validIndexes
        {
            get
            {
                if (this.csvValues == null)
                {
                    yield break;
                }

                for (var index = 0; index < this.csvValues.Count; index++)
                {
                    if (!this.invalidIndexes.Contains(index))
                    {
                        yield return index;
                    }
                }
            }
        }

        /// <summary>
        /// 不正なデータだったインデックスのリスト
		/// <para>List of invalid indexes</para>
        /// </summary>
        public List<int> invalidIndexes = new List<int>();

        /// <summary>
        /// CSVファイルから読み込んだDataTable
		/// <para>Read DataTable from CSV file</para>
        /// </summary>
        protected List<object> csvValues;

        /// <summary>
        /// 検証結果が正常なModelのIEnumerableを返す(表示用)。
        /// <para>Model validation results of normal returns IEnumerable (for display).</para>
        /// </summary>
        /// <returns>Modelのプロパティ(ErsDictionary)</returns>
        public IEnumerable<Dictionary<string, object>> ValidModelList
        {
            get
            {
                foreach (var index in this.validIndexes)
                {
                    yield return GetValidModel(index).GetPropertiesAsDictionary();
                }
            }
        }

        /// <summary>
        /// CSVファイルを読み込む。(call by ErsModelBinder)
        /// <para>read a CSV file. (called by ErsModelBinder)</para>
        /// </summary>
        public override void LoadPostedFile()
        {

            if (!this.IsValidAtBinding)
                return;
            
            //アップロードデータ読み込み
            //Read data for upload
            if (string.IsNullOrEmpty(this.fileName))
                this.fileName = this.SaveUploadFile();

            //保存済みテンポラリを読み込む
            //Reads temporary saved file
            this.csvValues = this.LoadTempFile(UPLOAD_FILE_PATH + this.fileName);
        }

        /// <summary>
        /// CSVファイルを読み込む。(call by ErsModelBinder)
        /// <para>read a CSV file. (called by ErsModelBinder)</para>
        /// </summary>
        public virtual void LoadPostedFile(string filePath)
        {
            //保存済みテンポラリを読み込む
            //Reads temporary saved file
            this.csvValues = this.LoadTempFile(filePath);

            this.IsValidAtBinding = true;
        }

        /// <summary>
        /// アップロードデータの読み込み
        /// <para>Saving uploaded file</para>
        /// </summary>
        protected virtual string SaveUploadFile()
        {
            this.fileName = Path.GetFileNameWithoutExtension(csv_file.FileName) + DateTime.Now.ToString("_yyyyMMddhhmmddss") + Path.GetExtension(csv_file.FileName);

            ///CSVファイル名が取得成功すれば、保存
            ///CSV file name if the acquisition is successful, save
            csv_file.SaveAs(UPLOAD_FILE_PATH + this.fileName);

            return fileName;
        }

        /// <summary>
        /// CSVファイルからDataTableを読み込む
        /// <para>Read the DataTable from CSV file for ModelState error.</para>
        /// </summary>
        /// <param name="filePath">物理ファイルパス/The physical file path</param>
        /// <returns>データリスト/Data List</returns>
        protected virtual List<object> LoadTempFile(string filePath)
        {
            var parser = new TextFieldParser(filePath, this.encoding) { Delimiters = this.GetDelimiters() };

            var retList = new List<object>();

            using (parser)
            {
                while (!parser.EndOfData)
                {
                    var row = parser.ReadFields();
                    retList.Add(row.ToArray<object>());
                }
            }
            return retList;
        }

        /// <summary>
        /// ファイル分割のデリミタを取得
        /// <para>Gets the delimiter character</para>
        /// </summary>
        /// <returns>デリミタ/Delimeter</returns>
        protected virtual string[] GetDelimiters()
        {
            return new[] { "," };
        }

        /// <summary>
        /// 検証結果が正常なModelのIEnumerableを返す。
        /// <para>List of Model(s) validated as normal, returns IEnumerable.</para>
        /// </summary>
        /// <returns>Modelリスト/Model list</returns>
        public virtual IEnumerable<T> GetValidModels()
        {
            if (!this.IsValidAtBinding || this.csvValues == null)
            {
                yield break;
            }

            foreach (var index in this.validIndexes)
            {
                yield return GetValidModel(index);
            }
        }

        /// <summary>
        ///  検証結果が正常なModelを返す。
        /// <para>returns validated as normal Model(s)</para>
        /// </summary>
        /// <param name="index">データインデックス / Index data</param>
        /// <returns>Model</returns>
        public virtual T GetValidModel(int index)
        {
            this.CheckIfValidationCalled();

            return this.GetBindModel(index);
        }

        protected void CheckIfValidationCalled()
        {
            if (!this.IsValidAtBinding || this.csvValues == null)
            {
                throw new Exception("Please call ValidateFile() at first, then check the result.");
            }
        }

        /// <summary>
        /// CSVファイルを検証する。
        /// <para>Validates the CSV file</para>
        /// </summary>
        /// <returns>ValidationResult</returns>
        public virtual IEnumerable<T> GetValidatedModels(bool skipFirstLine)
        {
            if (!this.IsValidAtBinding)
            {
                yield break;
            }
            
            var startIndex = 0;
            if (skipFirstLine)
            {
                this.invalidIndexes.Add(0);
                startIndex = 1;
            }
            for (var index = startIndex; index < csvValues.Count; index++)
            {
                var model = GetModelWithValidate(index);

                SaveValue(model);

                yield return model;
            }
        }

        /// <summary>
        /// 検証結果を付加したModelを返す。
        /// <para>returns the added Model with validation results.</para>
        /// </summary>
        /// <param name="index">データインデックス/Index data</param>
        /// <returns>Model</returns>
        protected virtual T GetModelWithValidate(int index)
        {
            var dr = (object[])csvValues[index];

            var t = typeof(T);
            var model = (T)Activator.CreateInstance(t);
           
            model.containerModel = this.containerModel;
            model.lineNumber = index + 1;

            var dicValues = this.GetValueDictionary(dr);

            ErsBindModel.ModelBind(model, dicValues, false);

            return model;
        }

        /// <summary>
        /// 不正な値としてマークする
        /// </summary>
        /// <param name="index"></param>
        public void MarkRecordAsInvalid(T record)
        {
            this.invalidIndexes.Add(record.lineNumber - 1);
        }

        /// <summary>
        ///  バインドしたModelを返す。
        /// </summary>
        /// <param name="index">データインデックス</param>
        /// <returns>Model</returns>
        protected virtual T GetBindModel(int index)
        {
            var dr = (object[])csvValues[index];

            var dicValues = this.GetValueDictionary(dr);
            
            var t = typeof(T);
            var model = (T)Activator.CreateInstance(t);

            model.containerModel = this.containerModel;
            model.lineNumber = index + 1;

            foreach (var columnName in this.listSchema)
            {
                var propValue = dicValues[columnName];
                var prop = t.GetProperty(columnName);
                ErsBindModel.SetProperty(model, prop, propValue);
            }

            return model;
        }

        private Dictionary<string, object> GetValueDictionary(object[] dr)
        {
            //listSchema
            var dicValeus = new Dictionary<string, object>();
            for (var columnIndex = 0; columnIndex < listSchema.Count; columnIndex++)
            {
                var columnName = listSchema[columnIndex];
                object columnValue = null;
                if (dr.Length > columnIndex)
                {
                    columnValue = dr[columnIndex];
                    if (columnValue is string && Convert.ToString(columnValue).StartsWith("'"))
                    {
                        columnValue = Convert.ToString(columnValue).Substring(1);
                    }
                }
                dicValeus.Add(columnName, columnValue);
            }
            return dicValeus;
        }

        /// <summary>
        /// SaveValue as csv value
        /// </summary>
        /// <param name="model"></param>
        public void SaveValue(T model)
        {
            var index = model.lineNumber - 1;
            this.csvValues[index] = ErsReflection.GetPropertiesAsArray(model, typeof(CsvFieldAttribute));
        }
    }
}
