﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using System.Web.Routing;
using System.Reflection;
using jp.co.ivp.ers.mvc.htmlExpand;

namespace jp.co.ivp.ers.mvc
{
    public class HtmlDefaultBinding
        : IHtmlBinding
    {
		/// <summary>
		/// Bind property 
		/// </summary>
        public IEnumerable<ValidationResult> BindProperty(object model, object requestValue, string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators, out object checkedValue)
        {

            if (requestValue == null)
            {
                checkedValue = null;
                return null;
            }

            //propertyDescriptor.Attributes
            var checkValue = Convert.ToString(requestValue);

            if (!string.IsNullOrEmpty(checkValue))
            {
                checkValue = checkValue.Trim();
            }

            object propertyValue;
            var retResult = ErsBindModel.Validate(propertyName, checkValue, validators, out propertyValue, propertyType, attributes);
            checkedValue = ErsBindModel.GetConcreteValue(propertyType, propertyValue);
            return retResult;
        }

		/// <summary>
		/// Get requested property value using RouteData
		/// </summary>
        public object GetRequestValue(string propertyName, RouteData routeData)
        {
            if (routeData.Values.ContainsKey(propertyName) && routeData.Values[propertyName] is string)
            {
                return routeData.Values[propertyName];
            }
            else
            {
                return HttpContext.Current.Request[propertyName];
            }
        }

		/// <summary>
		/// Get requested property value using IDictionary
		/// </summary>
        public object GetRequestValue(string propertyName, IDictionary<string, object> valueSource)
        {
            if (!valueSource.ContainsKey(propertyName))
                return null;

            return valueSource[propertyName];
        }


        public virtual void GetOutputHidden(List<ErsOutputHiddenTarget> listTarget, PropertyInfo property, string propertyName_frefix, string propertyName, Func<object> valueFunc, Dictionary<string, bool> OutputHidden)
        {
            var hiddenGroup = ErsOutputHiddenUtility.GetOutputHiddenGroup(OutputHidden, property);
            if (hiddenGroup != null)
            {
                var dic = new Dictionary<string, object>();
                dic["name"] = propertyName_frefix + propertyName;
                dic["value"] = this.FormatHiddenValue(property, valueFunc());
                ErsOutputHiddenUtility.AddHidden(listTarget, hiddenGroup, dic);
            }
        }

        protected virtual object FormatHiddenValue(PropertyInfo property, object value)
        {
            var outputHiddenAttribute = property.GetCustomAttributes(typeof(ErsOutputHiddenAttribute), false);
            var attrOutput = (ErsOutputHiddenAttribute)outputHiddenAttribute[0];
            return attrOutput.FormatValue(property, value);
        }
    }
}
