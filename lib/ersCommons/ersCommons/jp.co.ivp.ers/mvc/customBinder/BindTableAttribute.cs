﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Collections.Specialized;
using System.Web.Routing;
using System.Reflection;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Property)]
    public class BindTableAttribute
        : ErsUniversalValidationAttribute, IHtmlBinding
    {
        public static readonly string record_key = "record_key";

        public string table_name { get; protected set; }
        protected bool manuallyGetError;

        private const string HiddenKey = "_hidden_system";

        /// <summary>
        /// Bind table_name
        /// </summary>
        /// <param name="table_name"></param>
        public BindTableAttribute(string table_name)
        {
            this.table_name = table_name;
        }

        /// <summary>
        /// Bind Table_name and manuallyGetError
        /// </summary>
        /// <param name="table_name"></param>
        /// <param name="manuallyGetError"></param>
        public BindTableAttribute(string table_name, bool manuallyGetError)
        {
            this.table_name = table_name;
            this.manuallyGetError = manuallyGetError;
        }

        /// <summary>
        /// Bind validated properties to a model
        /// </summary>
        public IEnumerable<ValidationResult> BindProperty(object model, object requestValue,string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators, out object checkedValue)
        {
            if (requestValue == null)
            {
                checkedValue = null;
                return null;
            }

            if (!propertyType.IsGenericType || (propertyType.GetGenericTypeDefinition() != typeof(List<>) && propertyType.GetGenericTypeDefinition() != typeof(IList<>) && propertyType.GetGenericTypeDefinition() != typeof(IEnumerable<>)) || !propertyType.GetGenericArguments()[0].IsSubclassOf(typeof(ErsBindableModel)))
            {
                throw new Exception(model.GetType().Name + "." + propertyName + "は、" + "IEnumerable<ErsBindableModelのサブクラス>型、またはList<ErsBindableModelのサブクラス>型である必要があります。");
            }

            var baseDictionary = (Dictionary<string, object>)requestValue;

            var tableDictionary = new Dictionary<string, Dictionary<string, object>>();

            var separator = '_';

            //Dictionaryを行番号で分割
            //Dictionary divided by line number
            foreach (var key in baseDictionary.Keys)
            {
                if (string.IsNullOrEmpty(key) || !key.Contains(separator))
                {
                    continue;
                }

                var line_number = key.Split(separator)[0];
                int line_number_value;

                if (!int.TryParse(line_number, out line_number_value))
                {
                    continue;
                }

                var field_name = key.Substring(line_number.Length + 1);

                if (!tableDictionary.ContainsKey(line_number))
                    tableDictionary.Add(line_number, new Dictionary<string, object>());

                tableDictionary[line_number].Add(field_name, baseDictionary[key]);
            }

            //ジェネリック型の構築
            checkedValue = this.GetConcreteValue(propertyName, propertyType, tableDictionary, model);
            return null;
        }

        /// <summary>
        /// Get requested property value
        /// </summary>
        public object GetRequestValue(string propertyName, RouteData routeData)
        {
            var record_key = GetRecordKey();
            var hidden_record_key = this.GetHiddenRecordKey();

            if (record_key == null && hidden_record_key == null)
                return null;

            var retDictionary = new Dictionary<string, object>();
            if (hidden_record_key != null)
            {
                PutRequestDataToDictionary(hidden_record_key, routeData, retDictionary);
            }
            if (record_key != null)
            {
                PutRequestDataToDictionary(record_key, routeData, retDictionary);
            }
            return retDictionary;
        }

        /// <summary>
        /// リクエスト値をDictionaryに追加
        /// </summary>
        /// <param name="routeData"></param>
        /// <param name="record_key"></param>
        /// <param name="retDictionary"></param>
        private static void PutRequestDataToDictionary(string record_key, RouteData routeData, Dictionary<string, object> retDictionary)
        {
            HtmlDictionaryAttribute.AddRequestDataToDictionary(record_key, routeData.Values, retDictionary);
            HtmlDictionaryAttribute.AddRequestDataToDictionary(record_key, HttpContext.Current.Request.Form, retDictionary);
            HtmlDictionaryAttribute.AddRequestDataToDictionary(record_key, HttpContext.Current.Request.QueryString, retDictionary);
            HtmlDictionaryAttribute.AddRequestDataToDictionary(record_key, HttpContext.Current.Request.Files, retDictionary);
        }

        /// <summary>
        /// Get requested value using dictionary
        /// </summary>
        public object GetRequestValue(string propertyName, IDictionary<string, object> valueSource)
        {
            var record_key = GetRecordKey(valueSource);
            var hidden_record_key = this.GetHiddenRecordKey(valueSource);

            if (record_key == null && hidden_record_key == null)
                return null;

            var retDictionary = new Dictionary<string, object>();
            if (hidden_record_key != null)
            {
                HtmlDictionaryAttribute.AddRequestDataToDictionary(hidden_record_key, (Dictionary<string, object>)valueSource, retDictionary);
            }

            if (record_key != null)
            {
                HtmlDictionaryAttribute.AddRequestDataToDictionary(record_key, (Dictionary<string, object>)valueSource, retDictionary);
            }
            return retDictionary;
        }

        /// <summary>
        /// Get record key in a dictionary
        /// </summary>
        protected string GetRecordKey(IDictionary<string, object> valueSource = null)
        {
            var keys = this.GetRecordKeyArray(valueSource);

            if (keys != null)
            {
                foreach (var key in keys)
                {
                    if (!key.EndsWith(HiddenKey))
                    {
                        return key + "_";
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Get record key in a dictionary for hidden
        /// </summary>
        protected string GetHiddenRecordKey(IDictionary<string, object> valueSource = null)
        {
            var keys = this.GetRecordKeyArray(valueSource);

            if (keys != null)
            {
                foreach (var key in keys)
                {
                    if (key.EndsWith(HiddenKey))
                    {
                        return key + "_";
                    }
                }
            }

            return null;
        }

        private string[] GetRecordKeyArray(IDictionary<string, object> valueSource)
        {
            string key = null;

            if (valueSource == null)
            {
                key = Convert.ToString(HttpContext.Current.Request[record_key + this.table_name]);
            }
            else
            {
                if (valueSource.ContainsKey(record_key + this.table_name))
                {
                    key = Convert.ToString(valueSource[record_key + this.table_name]);
                }
            }
            if (string.IsNullOrEmpty(key))
                return null;

            return key.Split(new[] { ',' });
        }

        /// <summary>
        /// Get list of property validation results
        /// </summary>
        private System.Collections.IList GetConcreteValue(string propertyName, Type propertyType, Dictionary<string, Dictionary<string, object>> tableDictionary, object container)
        {
            var listType = typeof(List<>);
            var fieldType = propertyType.GetGenericArguments()[0];
            var genericType = listType.MakeGenericType(fieldType);

            var retList = (System.Collections.IList)Activator.CreateInstance(genericType);

            foreach (var lineNumber in tableDictionary.Keys)
            {
                var dictionary = tableDictionary[lineNumber];
                var model = this.GetModelInstance(fieldType, (IErsModelBase)container, dictionary);
                model.containerModel = (IErsModelBase)container;
                model.lineNumber = Convert.ToInt32(lineNumber) + 1;
                ErsBindModel.ModelBind(model, dictionary, false);

                retList.Add(model);
            }

            return retList;
        }

        /// <summary>
        /// バインドテーブルのレコードのインスタンスを取得します。
        /// </summary>
        /// <param name="fieldType"></param>
        /// <param name="container"></param>
        /// <param name="requestedValue"></param>
        /// <returns></returns>
        protected virtual ErsBindableModel GetModelInstance(Type fieldType, IErsModelBase container, Dictionary<string, object> requestedValue)
        {
            return (ErsBindableModel)Activator.CreateInstance(fieldType);
        }

        /// <summary>
        /// 指定したグループ名を持つErsOutputHidden属性がついたプロパティを出力する
        /// <para>Outputs a property that has the attribute ErsOutputHidden with specified group</para>
        /// </summary>
        /// <param name="model"></param>
        public virtual void GetOutputHidden(List<ErsOutputHiddenTarget> listTarget, PropertyInfo property, string propertyName_frefix, string propertyName, Func<object> valueFunc, Dictionary<string, bool> OutputHidden)
        {
            var modelList = (IEnumerable<ErsBindableModel>)(valueFunc());

            var listHiddenValue = new List<ErsOutputHiddenTarget>();
            var key = this.table_name + HiddenKey;

            var lineCount = -1;

            if (modelList != null)
            {
                foreach (var modelBase in modelList)
                {
                    lineCount++;

                    var modelType = modelBase.GetType();
                    foreach (var modelProp in modelType.GetProperties())
                    {
                        IHtmlBinding binding;
                        var attributes = modelProp.GetCustomAttributes(typeof(IHtmlBinding), false);
                        if (attributes.Count() != 0)
                        {
                            binding = (IHtmlBinding)attributes.First();
                        }
                        else
                        {
                            binding = new HtmlDefaultBinding();
                        }

                        Func<object> modelValueFunc = () => modelProp.GetValue(modelBase, null);
                        var innerPropertyName_frefix = propertyName_frefix + key + "_" + lineCount + "_";
                        binding.GetOutputHidden(listHiddenValue, modelProp, innerPropertyName_frefix, modelProp.Name, modelValueFunc, modelBase.OutputHidden);
                    }
                }
            }

            if (listHiddenValue.Count > 0)
            {
                var keydic = new Dictionary<string, object>();
                keydic["name"] = propertyName_frefix + record_key + this.table_name;
                keydic["value"] = key;
                var hiddenGroup = listHiddenValue.Select((target) => target.groupName);
                ErsOutputHiddenUtility.AddHidden(listTarget, hiddenGroup, keydic);
            }

            ErsOutputHiddenUtility.Concat(listTarget, listHiddenValue);
        }

        /// <summary>
        /// 指定したグループ名を持つErsOutputHidden属性がついたプロパティを出力する
        /// <para>Outputs a property that has an attribute ErsOutputHidden with specified group</para>
        /// </summary>
        /// <param name="model"></param>
        public void SetOutputHidden(System.Collections.IEnumerable modelList, string groupName, bool value)
        {
            if (modelList == null)
            {
                return;
            }

            foreach (var model in modelList)
            {
                var modelBase = (ErsBindableModel)model;
                modelBase.SetOutputHidden(groupName, value);
            }
        }
    }
}
