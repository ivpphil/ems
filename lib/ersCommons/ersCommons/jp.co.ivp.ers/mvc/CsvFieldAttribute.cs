﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.util
{
    /// <summary>
    /// CSV出力対象のプロパティに指定する。
    /// <para>Specifies the properties of the target CSV output.</para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class CsvFieldAttribute
        : SortableAttribute
    {
        /// <summary>
        /// CallerLineNumber属性はVWD2010でコンパイルした場合有効にならないため、
        /// ローカル環境で検証したい場合はbuild.batでコンパイルしてください。
        /// サーバー上ではbuild.bat内で使用しているmsbuildと同じものを使っているため、有効になります。
        /// 但しWindows Update KB2468871のパッチをインストールしている必要があります。
        /// 尚、VS2012以降であれば問題なく有効になります。
        /// </summary>
        /// <param name="order"></param>
        public CsvFieldAttribute([CallerLineNumber]int order = 0)
            : base(order)
        {
        }
    }
}
