﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.util
{
    /// <summary>
    /// XML出力対象のプロパティに指定する。
	/// <para>Specifies the Properties of the XML</para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class XmlFieldAttribute
        : Attribute
    {
    }
}
