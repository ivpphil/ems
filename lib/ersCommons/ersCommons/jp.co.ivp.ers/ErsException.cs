﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.Net;

namespace jp.co.ivp.ers.mvc
{
    public class ErsException
        : Exception, IErsHandlableException
    {

        public ErsException(string key, params object[] args)
            : this(string.Empty, null, ErsResources.GetMessage(key.ToString(), args))
        {
        }

        public ErsException(HttpStatusCode httpStatus, string key, params object[] args)
            : this(string.Empty, null, ErsResources.GetMessage(key.ToString(), args))
        {
            this.httpStatus = httpStatus;
        }

        public ErsException(string returnUrl, ErsModelBase model, string errorMessage)
            : base(errorMessage)
        {
            this.model = model;
            this.returnUrl = returnUrl;
        }

        public ErsException(string key, bool isNoBackTo, params object[] args)
            : this(string.Empty, null, ErsResources.GetMessage(key.ToString(), args))
        {
            this.isNoBackTo = isNoBackTo;
        }


        public bool isNoBackTo { get; set; }

        public string returnUrl { get; set; }

        public ErsModelBase model { get; set; }

        public HttpStatusCode? httpStatus { get; set; }
    }
}
