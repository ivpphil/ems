﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using System.Text.RegularExpressions;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSmtp
    {
        protected virtual string hostname { get; set; }
        protected virtual int port { get; set; }
        protected virtual string localFileUsername { get; set; }
        protected virtual string localFilePassword { get; set; }
        protected virtual string operationLogPath { get; set; }
        protected virtual string errLogPath { get; set; }
        protected virtual ErsSmtpRetryProperty retryProperty { get; set; }
        protected virtual string smtpAuthId { get; set; }
        protected virtual string smtpAuthPass { get; set; }

        /// <summary>
        /// <summary>Constructor specifying a host, port,localFileUsername , localFilePassword,operation log path, error log path</summary>
        /// </summary>
        /// <param name="hostname"></param>
        /// <param name="port"></param>
        /// <param name="localFileUsername"></param>
        /// <param name="localFilePassword"></param>
        /// <param name="operationLogPath"></param>
        /// <param name="errLogPath"></param>
        public ErsSmtp(string hostname, int port, string localFileUsername, string localFilePassword, string operationLogPath, string errLogPath, ErsSmtpRetryProperty retryProperty, string smtpAuthId = null, string smtpAuthPass = null)
        {
            this.hostname = hostname;
            this.port = port;
            this.localFileUsername = localFileUsername;
            this.localFilePassword = localFilePassword;
            this.operationLogPath = operationLogPath;
            this.errLogPath = errLogPath;
            this.retryProperty = retryProperty;
            this.smtpAuthId = smtpAuthId;
            this.smtpAuthPass = smtpAuthPass;
        }

        /// <summary>
        /// send synchronous
        /// </summary>
        /// <param name="mailFrom"></param>
        /// <param name="replyto"></param>
        /// <param name="mailTo"></param>
        /// <param name="cc"></param>
        /// <param name="bcc"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="htmlBody"></param>
        public virtual void SendSynchronous(string mailFrom, string replyto, string mailTo, IEnumerable<string> cc, IEnumerable<string> bcc, string subject, string body, string htmlBody)
        {
            this.Validate(mailFrom, mailTo, subject, body, htmlBody);

            this.Send(this.hostname, this.port, this.localFileUsername, this.localFilePassword, this.operationLogPath, this.errLogPath, mailFrom, replyto, mailTo, cc, bcc, subject, body, htmlBody, this.smtpAuthId, this.smtpAuthPass);
        }

        /// <summary>
        /// send asynchronous
        /// </summary>
        /// <param name="mailFrom"></param>
        /// <param name="replyto"></param>
        /// <param name="mailTo"></param>
        /// <param name="cc"></param>
        /// <param name="bcc"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="htmlBody"></param>
        public virtual void Send(string mailFrom, string replyto, string mailTo, IEnumerable<string> cc, IEnumerable<string> bcc, string subject, string body, string htmlBody)
        {
            this.Validate(mailFrom, mailTo, subject, body, htmlBody);

            //ここから非同期処理開始
            SendMethod asyncCall = new SendMethod(Send);
            asyncCall.BeginInvoke(this.hostname, this.port, this.localFileUsername, this.localFilePassword, this.operationLogPath, this.errLogPath, mailFrom, replyto, mailTo, cc, bcc, subject, body, htmlBody, this.smtpAuthId, this.smtpAuthPass, null, null);
        }

        /// <summary>
        /// Delegate for async sending
        /// </summary>
        /// <param name="hostname"></param>
        /// <param name="port"></param>
        /// <param name="localFileUsername"></param>
        /// <param name="password"></param>
        /// <param name="operationLogPath"></param>
        /// <param name="errLogPath"></param>
        /// <param name="mailFrom"></param>
        /// <param name="replyto"></param>
        /// <param name="mailTo"></param>
        /// <param name="cc"></param>
        /// <param name="bcc"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="htmlBody"></param>
        private delegate void SendMethod(string hostname, int port, string localFileUsername, string password, string operationLogPath, string errLogPath, string mailFrom, string replyto, string mailTo, IEnumerable<string> cc, IEnumerable<string> bcc, string subject, string body, string htmlBody, string smtpAuthId, string smtpAuthPass);

        /// <summary>
        /// Send email
        /// </summary>
        /// <param name="hostname"></param>
        /// <param name="port"></param>
        /// <param name="localFileUsername"></param>
        /// <param name="localFilePassword"></param>
        /// <param name="operationLogPath"></param>
        /// <param name="errLogPath"></param>
        /// <param name="mailFrom"></param>
        /// <param name="replyto"></param>
        /// <param name="mailTo"></param>
        /// <param name="cc"></param>
        /// <param name="bcc"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="asynchronous"></param>
        protected virtual void Send(string hostname, int port, string localFileUsername, string localFilePassword, string operationLogPath, string errLogPath, string mailFrom, string replyto, string mailTo, IEnumerable<string> cc, IEnumerable<string> bcc, string subject, string body, string htmlBody, string smtpAuthId, string smtpAuthPass)
        {
            var startDate = DateTime.Now;

            var doChangeAuth = !string.IsNullOrEmpty(localFileUsername);

            // Collect values to retry text
            ErsSmtpRetryTextContent content = null;
            if (this.retryProperty.enableRetry)
            {
                content = new ErsSmtpRetryTextContent()
                         {
                             mailFrom = mailFrom,
                             replyto = replyto,
                             mailTo = mailTo,
                             cc = cc,
                             bcc = bcc,
                             subject = subject,
                             body = body,
                         };
            }

            //権限変更
            using (var changelogin = ChangeLogonUserHelper.BeginChange(localFileUsername, localFilePassword, doChangeAuth))
            {
                //テンポラリへ、リカバリファイルを書き込む。
                string tempRetryTextFilePath = null;
                if (this.retryProperty.enableRetry)
                {
                    tempRetryTextFilePath = ErsSmtpRetryTextManager.OutputRetryText(this.retryProperty.retryTextPath, content, this.retryProperty.backupFileEncoding);
                }

                var doSmtpAuth = (smtpAuthId.HasValue() || smtpAuthPass.HasValue());

                while (true)
                {
                    try
                    {
                        using (var smtpClient = ErsSmtpClient.GetConnect(hostname, port))
                        {
                            smtpClient.SendHelo();

                            if (doSmtpAuth)
                            {
                                smtpClient.SendAuth(smtpAuthId, smtpAuthPass);
                            }

                            smtpClient.SendMailFrom(mailFrom);

                            smtpClient.SendReceiptTo(mailTo, cc, bcc);

                            smtpClient.SendData(replyto, mailFrom, mailTo, cc, subject, body, htmlBody);

                            smtpClient.SendQuit();
                        }

                        this.LogMessage(this.operationLogPath, "ok", mailFrom, mailTo, startDate); //operation log for successful execution
                        if (tempRetryTextFilePath.HasValue())
                        {
                            ErsSmtpRetryTextManager.DeleteRetryText(tempRetryTextFilePath);
                        }
                        break;

                    }
                    catch (ErsSmtpConnectionException e)
                    {
                        try
                        {
                            this.ErrorLogMessage(this.errLogPath, this.hostname, this.port, this.localFileUsername, e.ToString(), startDate);
                            this.LogMessage(this.operationLogPath, e.Message, mailFrom, mailTo, startDate); //operation log for successful execution
                            if (this.retryProperty.enableRetry)
                            {
                                //規定の期間リトライしたがエラーとなった場合のエラー処理
                                if (DateTime.Now < startDate.AddMinutes(this.retryProperty.retryTermMinutes))
                                {
                                    //指定分待機をして再送信する。
                                    Thread.Sleep(this.retryProperty.retryIntervalMinutes * 1000 * 60);
                                    continue;
                                }

                                //最初に作成失敗している場合は、再度書き込みをトライする
                                if (!tempRetryTextFilePath.HasValue())
                                {
                                    ErsSmtpRetryTextManager.OutputRetryText(this.retryProperty.retryTextPath, content, this.retryProperty.backupFileEncoding);
                                }
                            }
                        }
                        catch (Exception innerE)
                        {
                            System.Diagnostics.EventLog.WriteEntry("ErsSmtp", innerE.ToString(), System.Diagnostics.EventLogEntryType.Error);
                        }

                        throw;
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            this.ErrorLogMessage(this.errLogPath, this.hostname, this.port, this.localFileUsername, e.ToString(), startDate);
                            this.LogMessage(this.operationLogPath, e.Message, mailFrom, mailTo, startDate); //operation log for successful execution
                            if (this.retryProperty.enableRetry)
                            {
                                //最初に作成失敗している場合は、再度書き込みをトライする
                                if (!tempRetryTextFilePath.HasValue())
                                {
                                    ErsSmtpRetryTextManager.OutputRetryText(this.retryProperty.retryTextPath, content, this.retryProperty.backupFileEncoding);
                                }
                            }
                        }
                        catch (Exception innerE)
                        {
                            //Windowsログへ出力する。
                            System.Diagnostics.EventLog.WriteEntry("ErsSmtp", innerE.ToString(), System.Diagnostics.EventLogEntryType.Error);
                        }

                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Validate arguments
        /// </summary>
        /// <param name="mailFrom"></param>
        /// <param name="mailTo"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        private void Validate(string mailFrom, string mailTo, string subject, string body, string htmlBody)
        {
            if (string.IsNullOrWhiteSpace(ErsSmtpClient.GetEmailPart(mailFrom)))
            {
                throw new Exception("mail from is empty");
            }

            if (string.IsNullOrWhiteSpace(ErsSmtpClient.GetEmailPart(mailTo)))
            {
                throw new Exception("mailTo to is empty");
            }

            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new Exception("subject is empty");
            }

            if (string.IsNullOrWhiteSpace(body) && string.IsNullOrWhiteSpace(htmlBody))
            {
                throw new Exception("both body and htmlBody are empty");
            }
        }

        /// <summary>
        /// log the operation and error 
        /// </summary>
        /// <param name="thredID"></param>
        /// <param name="msg"></param>
        /// <param name="src"></param>
        /// <param name="isErr"></param>
        private bool LogMessage(string operationLogPath, string msg, string mailFrom, string mailTo, DateTime startDate)
        {
            try
            {
                if (string.IsNullOrEmpty(operationLogPath))
                {
                    return false;
                }

                ErsDirectory.CreateDirectories(operationLogPath);

                string logFullPath = Path.Combine(operationLogPath, "sendmailivplog.log");

                ErsFile.WriteAll(string.Format("{0:yyyyMMddHHmmss.fffffff},{1},{2},{3}\r\n", startDate, mailTo, mailFrom, msg), logFullPath);

                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.EventLog.WriteEntry("ErsSmtp", e.ToString(), System.Diagnostics.EventLogEntryType.Error);
                return false;
            }
        }

        /// <summary>
        /// log the operation and error 
        /// </summary>
        /// <param name="thredID"></param>
        /// <param name="msg"></param>
        /// <param name="errorLogPath"></param>
        /// <param name="isErr"></param>
        internal bool ErrorLogMessage(string errLogPath, string hostname, int port, string localFileUsername, string msg, DateTime startDate)
        {
            try
            {
                if (string.IsNullOrEmpty(errLogPath))
                {
                    return false;
                }

                ErsDirectory.CreateDirectories(errLogPath);

                var errorLogFileName = string.Format("{0:yyyyMMddHHmmss.fffffff}_{1}.log", startDate, Thread.CurrentThread.ManagedThreadId);

                string logFullPath = Path.Combine(errLogPath, errorLogFileName);

                StringBuilder errDetail = new StringBuilder();
                errDetail.AppendLine("smtp server address: " + hostname);
                errDetail.AppendLine("smtp server port: " + port);
                errDetail.AppendLine("local user: " + localFileUsername);
                errDetail.AppendLine("error details:" + msg.Trim());

                File.AppendAllText(logFullPath, errDetail.ToString(), ErsEncoding.ShiftJIS);

                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.EventLog.WriteEntry("ErsSmtp", e.ToString(), System.Diagnostics.EventLogEntryType.Error);
                return false;
            }
        }
    }
}
