﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.sendmail
{
    /// <summary>
    /// Store Values of Retry Text
    /// </summary>
    public class ErsSmtpRetryTextContent
    {
        /// <summary>
        /// From
        /// </summary>
        public string mailFrom { get; set; }

        /// <summary>
        /// Reply-to
        /// </summary>
        public string replyto { get; set; }

        /// <summary>
        /// To
        /// </summary>
        public string mailTo { get; set; }

        /// <summary>
        /// CC
        /// </summary>
        public IEnumerable<string> cc { get; set; }

        /// <summary>
        /// BCC
        /// </summary>
        public IEnumerable<string> bcc { get; set; }

        /// <summary>
        /// Subject
        /// </summary>
        public string subject { get; set; }

        /// <summary>
        /// Body
        /// </summary>
        public string body { get; set; }
    }
}
