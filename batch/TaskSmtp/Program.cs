﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers;
using jp.co.ivp.ers.sendmail;
using System.Configuration;
using jp.co.ivp.ers.batch.util;
using System.IO;

namespace TaskSmtp
{
    class Program
    {
        static void Main(string[] args)
        {
            ErsFactory.ersMailFactory = new ErsMailFactory();
            
            using (var mutex = new ErsMutex(System.Reflection.Assembly.GetCallingAssembly().Location))
            {
                if (mutex.isStarted)
                {
                    return;
                }

                // Load configuration
                var smtpHostName = ConfigurationManager.AppSettings["smtpHostName"];
                var smtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]);
                var smtpOperationLogPath = ConfigurationManager.AppSettings["smtpOperationLogPath"];
                var smtpErrorLogPath = ConfigurationManager.AppSettings["smtpErrorLogPath"];
                var smtpTextPath = ConfigurationManager.AppSettings["smtpTextPath"];

                //set to send without retry
                var retryProperty = ErsFactory.ersMailFactory.GetErsSmtpRetryProperty();
                retryProperty.SetDisableRetry();

                //Create send mail instance
                var sendmail = new ErsSmtp(smtpHostName, smtpPort, null, null, smtpOperationLogPath, smtpErrorLogPath, retryProperty, null, null);

                // Read and send each file.
                var mailFileList = Directory.EnumerateFiles(smtpTextPath, "*.txt");
                foreach (var mailFile in mailFileList)
                {
                    using (var streamMailText = new FileStream(mailFile, FileMode.Open, FileAccess.Read | FileAccess.Write, FileShare.None))
                    {
                        SendMail(sendmail, streamMailText);
                    }

                    File.Delete(mailFile);
                }
            }
        }

        /// <summary>
        /// ファイルを送信
        /// </summary>
        /// <param name="sendmail"></param>
        /// <param name="streamMailText"></param>
        private static void SendMail(ErsSmtp sendmail, FileStream streamMailText)
        {
            var contens = ErsSmtpRetryTextManager.ParseRetryText(streamMailText, ErsEncoding.ShiftJIS);

            //正常チェック
            sendmail.SendSynchronous(contens.mailFrom, contens.replyto, contens.mailTo, contens.cc, contens.bcc, contens.subject, contens.body, null);
        }
    }
}
