﻿--------------------------------------------------------------
TaskSmtp.exe
--------------------------------------------------------------

[処理内容]
リトライ用のテキストファイルを読み取って、メール送信を実行する。


[実行方法]
src\batch\TaskSmtp\bin\Debug\TaskSmtp.exe
をコマンドラインより実行。


[初期設定]
src\batch\TaskSmtp\bin\Debug\TaskSmtp.exe.configを修正する。

smtpHostName			：SMTPサーバー名		e.g.) ers-mail-smtp3.ivp.ne.jp
smtpPort				：SMTPサーバーポート	e.g.) 25
smtpOperationLogPath	：実行結果ログ（サマリ）e.g.) C:\sendmailivp\log\
smtpErrorLogPath		：エラー時のログパス	e.g.) C:\sendmailivp\log\error\
smtpTextPath			：リトライ用テキストパスe.g.) C:\sendmailivp\mqueue\


[エラー時]
ログパスにエラーを配置
エラー時はテキストは削除しません


[リトライテキスト]
"from","送信者名<from@ivp.co.jp>"
"rcpt","rcpt@ivp.co.jp"
"bcc","under"
bcc1@ivp.co.jp
bcc2@ivp.co.jp
BccEnd
"cc","under"
cc1@ivp.co.jp
cc2@ivp.co.jp
CcEnd
"subject","件名です"
"reply","reply@ivp.co.jp"
"body","under"
以降に本文を入力します。
