﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.batch
{
    public interface IErsBatchEnvironment
    {
        /// <summary>
        /// ERSの初期化処理を行う
        /// </summary>
        /// <param name="applicationRootPath"></param>
        void Initialization(string applicationRootPath);
    }
}
