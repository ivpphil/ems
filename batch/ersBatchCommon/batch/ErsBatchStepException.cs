﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.batch
{
    public class ErsBatchStepException
        : Exception
    {
        public ErsBatchStepException(string className, Exception innerException)
            : base(className, innerException)
        {
        }
 }
}
