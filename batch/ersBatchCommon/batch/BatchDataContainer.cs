﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch.util;

namespace jp.co.ivp.ers.batch
{
    public class BatchDataContainer
    {
        /// <summary>
        /// バッチ実行時間
        /// </summary>
        public DateTime? executeDate { get; set; }

        /// <summary>
        /// バッチID(batch_schedule_t.batch_id)
        /// </summary>
        public string batchId { get; set; }

        /// <summary>
        /// 実行中のバッチ名
        /// </summary>
        public string batchName { get; set; }

        /// <summary>
        /// スケジュール指定（crontab形式）
        /// </summary>
        public string schedule { get; set; }

        /// <summary>
        /// 最終実行時間
        /// </summary>
        public DateTime? lastDate { get; set; }

        /// <summary>
        /// 最終実行成功時間
        /// </summary>
        public DateTime? lastSuccessDate { get; set; }

        /// <summary>
        /// 各バッチのEXEパス
        /// </summary>
        public string batchExePath { get; set; }

        /// <summary>
        /// 即時実行指示での呼び出しの場合True
        /// </summary>
        public bool batchImmediate { get; set; }


        #region クラス実行用
        public string options { get; set; }

        public string class_name { get; set; }

        /// <summary>
        /// 多重起動を許可する場合はfalseを指定する。（規定値はTrue）
        /// </summary>
        public bool? enableMutex { get; set; }

        public string batchLocation { get; set; }
        #endregion

        public EnumBatchMode LoadOptions(IDictionary<string, object> options)
        {
            var listError = new List<string>();

            var batchMode = ErsObtainOption.GetInt32Value(options, "batchMode", listError);
            this.executeDate = ErsObtainOption.GetDateTimeValue(options, "executeDate", listError);
            this.batchId = ErsObtainOption.GetStringValue(options, "batchId", listError);
            this.batchName = ErsObtainOption.GetStringValue(options, "batchName", listError);
            this.schedule = ErsObtainOption.GetStringValue(options, "schedule", listError);
            this.lastDate = ErsObtainOption.GetDateTimeValue(options, "lastDate", listError);
            this.lastSuccessDate = ErsObtainOption.GetDateTimeValue(options, "lastSuccessDate", listError);
            this.options = ErsObtainOption.GetStringValue(options, "options", listError);
            this.class_name = ErsObtainOption.GetStringValue(options, "class_name", listError);
            this.enableMutex = ErsObtainOption.GetBoolValue(options, "enableMutex", listError);
            this.batchLocation = ErsObtainOption.GetStringValue(options, "batchLocation", listError);
            this.batchImmediate = ErsObtainOption.GetBoolValue(options, "batchImmediate", listError) ?? false;

            if (listError.Count > 0)
            {
                throw new Exception(string.Join(Environment.NewLine, listError));
            }

            return (EnumBatchMode)Convert.ToInt32(batchMode);
        }

        /// <summary>
        /// バッチに渡す引数を設定する。
        /// </summary>
        /// <param name="targetBatch"></param>
        /// <param name="executeDate"></param>
        /// <param name="ersRoot"></param>
        /// <returns></returns>
        public string GetArguments(EnumBatchMode batchMode)
        {
            var listArgument = new List<string>();
            listArgument.Add("-execute");
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("batchMode", Convert.ToString((int)batchMode), true));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("executeDate", Convert.ToString(this.executeDate), true));

            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("batchId", Convert.ToString(this.batchId), true));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("batchName", Convert.ToString(this.batchName), true));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("schedule", Convert.ToString(this.schedule), true));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("lastDate", Convert.ToString(this.lastDate), true));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("lastSuccessDate", Convert.ToString(this.lastSuccessDate), true));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("options", Convert.ToString(this.options), true));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("class_name", Convert.ToString(this.class_name), true));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("enableMutex", Convert.ToString(this.enableMutex), true));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("batchLocation", Convert.ToString(this.batchLocation), true));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("batchImmediate", Convert.ToString(this.batchImmediate), true));

            return string.Join(" ", listArgument);
        }
    }
}
