﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.batch
{
    public interface IErsBatchExecuter
    {
        /// <summary>
        /// バッチの一覧を取得
        /// </summary>
        /// <param name="executeDate"></param>
        /// <returns></returns>
        IList<BatchDataContainer> GetBatchDataList(DateTime executeDate);

        /// <summary>
        /// バッチの実行を行う
        /// </summary>
        /// <param name="batchContainer"></param>
        void Execute(BatchDataContainer batchContainer);

        /// <summary>
        /// バッチの実行を行う
        /// </summary>
        /// <param name="batchContainer"></param>
        void ExecuteClass(BatchDataContainer batchContainer);

        /// <summary>
        /// 即時実行の指示があるかをチェックする。
        /// </summary>
        /// <param name="executeDate"></param>
        /// <param name="batch_id"></param>
        /// <returns></returns>
        bool CheckExistsImmediate(DateTime? executeDate, string batch_id);

        /// <summary>
        /// 実行済みの即時実行の指示を無効化する。
        /// </summary>
        /// <param name="nullable"></param>
        void UpdateImmediateDone(DateTime? executeDate, string batch_id);
    }
}
