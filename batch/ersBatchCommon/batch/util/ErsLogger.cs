﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;

namespace jp.co.ivp.ers.batch.util
{
    public static class ErsLogger
    {
        [ThreadStatic]
        private static string _logFilePath;

        public static string LogFilePath { get { return _logFilePath; } }

        public static void Initialize(string logFilePath)
        {
            ErsLogger._logFilePath = logFilePath;
        }

        /// <summary>
        /// エラーログを出力する
        /// </summary>
        /// <param name="ex"></param>
        public static void OutputErrorLog(DateTime? executeDate, string batchId, string className, Exception ex)
        {
            // ファイル出力
            ErsFile.WriteAll(Convert.ToString(executeDate) + "\r\n" + className + "\r\n" + ex.ToString() + "\r\n\r\n\r\n", Path.Combine(_logFilePath, batchId, "err", "err.log"), true);
        }

        /// <summary>
        /// 実行ログを出力する
        /// </summary>
        /// <param name="p"></param>
        public static void OutputOperateLog(DateTime? executeDate, string message)
        {
            // ファイル出力
            ErsFile.WriteAll(Convert.ToString(executeDate) + "\t" + message + "\r\n", Path.Combine(_logFilePath, "operate.log"), true);
        }

        /// <summary>
        /// 終了ログを出力
        /// </summary>
        /// <param name="batchName"></param>
        public static void OutputEndLog(DateTime? executeDate, string batchId)
        {
            // ファイル出力
            ErsFile.WriteAll(Convert.ToString(executeDate) + "\t" + "正常終了", Path.Combine(_logFilePath, batchId, "log", "end.log"), false);
        }
    }
}
