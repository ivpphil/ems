﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;

namespace jp.co.ivp.ers.batch.util
{
    public class ErsMershaller
    {
        private string ersRoot { get; set; }

        public ErsMershaller(string ersRoot)
        {
            this.ersRoot = ersRoot;

            //Assemblyのロード時の検索イベントをハンドルする。
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
        }

        /// <summary>
        /// 指定されたクラス名のインスタンスを取得する
        /// </summary>
        /// <param name="ersRoot"></param>
        public T GetInstance<T>(string className)
        {
            var typeClass = this.GetType(className);
            return (T)Activator.CreateInstance(typeClass);
        }

        /// <summary>
        /// 型を取得する
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        private Type GetType(string className)
        {
            foreach (var asm in this.GetAssemblies())
            {
                var type = asm.GetType(className);
                if (type != null)
                {
                    return type;
                }
            }
            throw new Exception(className + " was not found.");
        }

        /// <summary>
        /// アセンブリのロードを行う
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            foreach (var asm in this.GetAssemblies())
            {
                if (asm.FullName == args.Name)
                {
                    return asm;
                }
            }
            throw new Exception(args.Name + " was not found.");
        }

        /// <summary>
        /// ERSのアセンブリ一覧を取得する
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Assembly> GetAssemblies()
        {
            foreach (var dll in Directory.GetFiles(this.ersRoot, "*.dll", SearchOption.AllDirectories))
            {
                yield return Assembly.LoadFile(dll);
            }
        }

    }
}
