﻿using System;
using System.Threading;

namespace jp.co.ivp.ers.batch.util
{
    public class ErsMutexLock
        : IDisposable
    {
        /// <summary>
        /// Mutexインスタンス
		/// <para>Mutex instance</para>
        /// </summary>
        private Mutex mutex;

        /// <summary>
        /// コンストラクタ
		/// <para>Constructor</para>
        /// </summary>
        /// <param name="name">Mutex名</param>
        public ErsMutexLock(string name)
        {
            mutex = new Mutex(false, name);
            mutex.WaitOne();
        }

        /// <summary>
        /// デストラクタ
		/// <para>Destructor</para>
        /// </summary>
        ~ErsMutexLock()
        {
            if (mutex != null)
            {
                Dispose();
            }
        }

        /// <summary>
        /// 解放
		/// <para>Release Mutex</para>
        /// </summary>
        public void Dispose()
        {
            mutex.ReleaseMutex();
            mutex.Close();
            mutex = null;
        }
    }
}