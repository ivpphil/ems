﻿using System;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;

namespace jp.co.ivp.ers.batch.util
{
    /// <summary>
    /// カスタムMutex [Customized Mutex]
    /// </summary>
    public class ErsMutex
        : IDisposable
    {
        /// <summary>
        /// Mutexインスタンス [Instance of Mutex]
        /// </summary>
        protected Mutex mutex;

        /// <summary>
        /// Mutex生成結果 [Result of Mutex was created]
        /// </summary>
        protected bool createdNew;

        /// <summary>
        /// 起動済み [Is started]
        /// </summary>
        public bool isStarted { get; private set; }


        /// <summary>
        /// 多重起動を防止する。（すでに起動済みかどうかをisStartedで判定すること。）
        /// </summary>
        /// <param name="name">Mutex名</param>
        public ErsMutex(string name)
        {
            //\\が含まれるとMutex内部でエラーとなるため修正。
            name = name.Replace("\\", "_._");

            // Mutex生成 [Create mutex]
            this.mutex = new Mutex(true, "Global\\" + name, out this.createdNew, this.GetMutexSecurity());

            // 起動済みかどうか [Judgement for started]
            this.isStarted = !(this.createdNew && mutex.WaitOne(0, false));
        }

        /// <summary>
        /// デストラクタ [Destructor]
        /// </summary>
        ~ErsMutex()
        {
            this.Dispose();
        }

        /// <summary>
        /// 解放 [Release]
        /// </summary>
        public void Dispose()
        {
            if (this.mutex != null)
            {
                if (this.createdNew)
                {
                    this.mutex.ReleaseMutex();
                }
                this.mutex.Close();
                this.mutex = null;
            }
        }

        /// <summary>
        /// Mutexセキュリティ設定取得 [Get Mutex security settings]
        /// </summary>
        /// <returns>Mutexセキュリティ [Mutex security]</returns>
        protected MutexSecurity GetMutexSecurity()
        {
            MutexSecurity security = new MutexSecurity();

            // 全ユーザに対して [For all users]
            SecurityIdentifier identity = new SecurityIdentifier(WellKnownSidType.WorldSid, null);

            // 許可：待機・解放 [Allow : Wait, Release]
            security.AddAccessRule(new MutexAccessRule(identity, MutexRights.Synchronize | MutexRights.Modify, AccessControlType.Allow));

            // 拒否：権限変更 [Deny : Change permission]
            security.AddAccessRule(new MutexAccessRule(identity, MutexRights.ChangePermissions, AccessControlType.Deny));

            return security;
        }
    }
}