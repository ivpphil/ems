﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.batch.util
{
    public class ErsObtainOption
    {
        public static string[] SplitArgs(string value)
        {
            if (value == null)
            {
                return new string[0];
            }

            if (value.StartsWith("\"") && value.EndsWith("\""))
            {
                value = value.Substring(1, value.Length - 2);
            }

            return value.Split(new[] { "\" \"" }, StringSplitOptions.None);
        }

        /// <summary>
        /// オプションのDictionaryを取得する。
        /// オプションが引数有り（opt=value）の場合は、value値に引数が入る。引数なしのオプションは、nullが入る。
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IDictionary<string, object> Obtain(string[] args, bool commandline)
        {
            var dictionary = new Dictionary<string, object>();
            try
            {
                foreach (var value in args)
                {
                    if (value.Contains('='))
                    {
                        var arrSplit = value.Split('=');
                        var optionKey = value.Split('=')[0];
                        var optionValue = value.Substring(value.IndexOf("=") + 1);
                        dictionary.Add(optionKey, commandline ? RevertOptionEscape(optionValue) : optionValue);
                    }
                    else
                    {
                        dictionary.Add(value, null);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("[" + string.Join("]\n[", args) + "]", ex);
            }
            return dictionary;
        }

        public static string GetStringValue(IDictionary<string, object> options, string optionName, List<string> listError)
        {
            if (!options.ContainsKey(optionName))
            {
                listError.Add(optionName + "の指定がありません。");
                return null;
            }

            return Convert.ToString(options[optionName]);
        }

        public static DateTime? GetDateTimeValue(IDictionary<string, object> options, string optionName, List<string> listError)
        {
            if (!options.ContainsKey(optionName))
            {
                listError.Add(optionName + "の指定がありません。");
                return null;
            }

            var value = options[optionName].ToString();
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            DateTime dateValue;
            if (!DateTime.TryParse(value, out dateValue))
            {
                listError.Add(optionName + "の指定が不正です。");
                return null;
            }

            return dateValue;
        }

        public static int? GetInt32Value(IDictionary<string, object> options, string optionName, List<string> listError)
        {
            if (!options.ContainsKey(optionName))
            {
                listError.Add(optionName + "の指定がありません。");
                return null;
            }

            int intValue;
            if (!Int32.TryParse(options[optionName].ToString(), out intValue))
            {
                listError.Add(optionName + "の指定が不正です。");
                return null;
            }

            return intValue;
        }

        internal static bool? GetBoolValue(IDictionary<string, object> options, string optionName, List<string> listError)
        {
            if (!options.ContainsKey(optionName))
            {
                listError.Add(optionName + "の指定がありません。");
                return null;
            }

            var value = options[optionName].ToString();

            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            bool boolValue;
            if (!Boolean.TryParse(value, out boolValue))
            {
                listError.Add(optionName + "の指定が不正です。");
                return null;
            }

            return boolValue;
        }

        /// <summary>
        /// オプションの値を取得
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetOptionKeyValuePair(string key, string value, bool commandline)
        {
            var argumentBase = "\"{0}\"";
            var keyValueBase = "{0}={1}";
            return string.Format(argumentBase, string.Format(keyValueBase, key, commandline ? OptionEscape(value) : value));
        }

        static string[] escapeString = new string[] { "<", ">" };
        static string escapeChar = "^";

        /// <summary>
        /// バッチの引数用にOptionをエスケープする
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string OptionEscape(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            var result = value.Replace(@"\", @"\\").Replace("\"", "\\\"");

            result = result.Replace(escapeChar, escapeChar + escapeChar);
            Array.ForEach(escapeString, (arrValue) => result = result.Replace(arrValue, escapeChar + arrValue));
            return result;
        }

        /// <summary>
        /// バッチの引数用にOptionをエスケープする
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string RevertOptionEscape(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            var result = value;
            Array.ForEach(escapeString, (arrValue) => result = result.Replace(escapeChar + arrValue, arrValue));
            return result.Replace(escapeChar + escapeChar, escapeChar);
        }
    }
}
