﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;


namespace jp.co.ivp.ers.batch.util
{
    public class ErsFile
    {

        static private Encoding _encode = System.Text.Encoding.UTF8;

        /// <summary>
        /// ファイルに書き込む
		/// <para>Write to file</para>
        /// </summary>
        /// <param name="strContents">内容</param>
        /// <param name="strPath">ファイルパス</param>
        /// <param name="blnAppend">追加するか上書きするか</param>
        /// <param name="encode">文字エンコード</param>
        /// <returns></returns>
        static public bool WriteAll(string strContents, string strPath, bool blnAppend, Encoding encode = null)
        {
            var autoResetEvent = new AutoResetEvent(false);

            var count = 0;//現在リトライ数
            var stopCount = 20;//最大リトライ数
            var retryInterval = 500;//リトライまでの待機時間(ミリ秒)

            while (true)
            {
                try
                {
                    try
                    {
                        using (var sw = new System.IO.StreamWriter(strPath, blnAppend, encode))
                        {
                            sw.Write(strContents);
                            //閉じる
                            sw.Close();
                        }
                        break;
                    }
                    catch (DirectoryNotFoundException)
                    {
                        ErsDirectory.CreateDirectories(Path.GetDirectoryName(strPath));
                        throw;
                    }
                }
                catch (IOException e)
                {
                    //ロック解除を検知
                    var fileSystemWatcher = new FileSystemWatcher(Path.GetDirectoryName(strPath))
                    {
                        EnableRaisingEvents = true
                    };

                    fileSystemWatcher.Changed += (o, ev) =>
                    {
                        if (Path.GetFullPath(ev.FullPath) == Path.GetFullPath(strPath))
                        {
                            autoResetEvent.Set();
                        }
                    };

                    //ファイルが開放されるか、xミリ秒経過するまで待機
                    if (!autoResetEvent.WaitOne(retryInterval) && ++count == stopCount)
                    {
                        //リトライ回数を超えたらエラー
                        throw new IOException(string.Format("Error when writing file \r\npath:{0}\r\ncontents{1}\r\n", strPath, strContents), e);
                    }
                }
            }

            return true;
        }


        /// <summary>
        /// ファイルに書き込む
		/// <para>Write to file</para>
        /// </summary>
        /// <param name="strContents">内容</param>
        /// <param name="strPath">ファイルパス</param>
        /// <param name="blnAppend">追加するか上書きするか</param>
        /// <returns></returns>
        static public bool WriteAll(string strContents, string strPath, bool blnAppend)
        {
            return WriteAll(strContents, strPath, blnAppend, _encode);
        }


        /// <summary>
        /// ファイルを削除する
		/// <para>Delete a file</para>
        /// </summary>
        /// <param name="strPath"></param>
        static public void DeleteExists(string strPath)
        {
            if (File.Exists(strPath))
            {
                File.Delete(strPath);
            }
        }
    }
}
