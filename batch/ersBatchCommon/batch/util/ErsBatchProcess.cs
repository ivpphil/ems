﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace jp.co.ivp.ers.batch.util
{
    public static class ErsBatchProcess
    {
        public static bool PrepareProcessExeFile(BatchDataContainer targetBatch)
        {
            var batchPath = targetBatch.batchLocation;
            var batchLocation = Path.GetDirectoryName(batchPath);

            //バッチをコピーして、そのexeを実行する。(どのプロセスがどのバッチを実行しているかを確認できるように。)
            var batchCopyLocation = string.Format(@"{0}\batches\{1}", batchLocation, targetBatch.batchId);

            var batchBasePath = string.Format(@"{0}\{1}", batchCopyLocation, Path.GetFileName(batchPath));
            var batchCopyPath = string.Format(@"{0}\{1}.exe", batchCopyLocation, targetBatch.batchId);

            targetBatch.batchExePath = batchCopyPath;

            //多重起動防止
            using (var mutex = new ErsMutex(batchCopyPath))
            {
                if (mutex.isStarted && targetBatch.enableMutex != false)
                {
                    return false;
                }

                ErsDirectory.CreateDirectories(batchCopyLocation);
                foreach (var file in Directory.GetFiles(batchLocation))
                {
                    Copy(file, string.Format(@"{0}\{1}", batchCopyLocation, Path.GetFileName(file)));
                }

                Copy(batchBasePath, batchCopyPath);
                Copy(batchBasePath + ".config", batchCopyPath + ".config");
            }

            return true;
        }

        /// <summary>
        /// Copy specified file if the file was modified.
        /// </summary>
        /// <param name="sorceFilePath"></param>
        /// <param name="destinationFilePath"></param>
        private static void Copy(string sorceFilePath, string destinationFilePath)
        {
            var objSorceFile = new FileInfo(sorceFilePath);
            var objDestinationFile = new FileInfo(destinationFilePath);
            if (!objDestinationFile.Exists ||
                objSorceFile.LastWriteTime != objDestinationFile.LastWriteTime)
            {
                objSorceFile.CopyTo(objDestinationFile.FullName, true);
            }
        }

        public static Process ExecuteProcess(BatchDataContainer targetBatch, EnumBatchMode batchMode)
        {
            if (string.IsNullOrEmpty(targetBatch.batchExePath))
            {
                throw new Exception("BatchDataContainer is not prepared. Please call ErsBatchProcess.PrepareProcessExeFile()");
            }

            System.Diagnostics.Debug.WriteLine("executed batch:[" + targetBatch.batchExePath + " " + targetBatch.GetArguments(batchMode) + "]");

            return ErsProcessExecuter.Start(targetBatch.batchExePath, targetBatch.GetArguments(batchMode));
        }
    }
}
