﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.batch.util
{
    public class ErsLog4netLogger
    {
        [ThreadStatic]
        private static string _log4netconfigPath;

        public static string Log4netconfigPath { get { return _log4netconfigPath; } }

        public static void InitLogger(string configPath)
        {
            if (string.IsNullOrEmpty(_log4netconfigPath))
            {
                _log4netconfigPath = configPath;
                InitLogger();
            }
        }

        /// <summary>
        /// log4netを初期化する
        /// </summary>
        private static void InitLogger()
        {
            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(_log4netconfigPath));
            var hierarchy = log4net.LogManager.GetRepository() as log4net.Repository.Hierarchy.Hierarchy;
            InitAppender(hierarchy, "operateLogger", "operateLog", ErsLogger.LogFilePath);
        }

        /// <summary>
        /// 実行ログを出力する
        /// </summary>
        /// <param name="p"></param>
        public static void OutputOperateLog(DateTime? executeDate, string message)
        {
            var logger = log4net.LogManager.GetLogger("operateLogger");
            logger.Info(Convert.ToString(executeDate) + "\t" + message + "\r\n");
        }

        /// <summary>
        /// FileAppenderのパスを設定ファイルから取得
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <param name="loggerName"></param>
        /// <param name="appenderName"></param>
        /// <param name="logfileprefix"></param>
        private static void InitAppender(log4net.Repository.Hierarchy.Hierarchy hierarchy, string loggerName, string appenderName, string log_path)
        {
            var appender = (log4net.Appender.FileAppender)hierarchy
                .GetLogger(loggerName, hierarchy.LoggerFactory)
                .GetAppender(appenderName);
            
            if (appender != null)
            {
                //出力先ファイルを設定
                appender.File = Path.Combine(log_path, "operate.log");
                appender.ActivateOptions();
            }
        }
    }
}
