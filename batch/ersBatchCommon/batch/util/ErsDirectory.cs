﻿using System.Text;
using System.IO;

namespace jp.co.ivp.ers.batch.util
{
    public class ErsDirectory
    {
        /// <summary>
        /// ディレクトリを作成する（全階層）
        /// </summary>
        /// <param name="strPath">フルパス</param>
        static public void CreateDirectories(string strPath)
        {
            if (strPath.EndsWith(@"\"))
            {
                strPath = strPath.Substring(0, strPath.Length -1);
            }

            if (!Directory.Exists(strPath))
            {
                var arrDir = strPath.Split(new char[] { '\\' });
                var currentPath = string.Empty;
                var baseIndex = arrDir.Length - 2;
                
                //起点となるディレクトリを取得
                for (; baseIndex >= 0; baseIndex--)
                {
                    currentPath = JoinDirectories(arrDir, baseIndex);
                    if (Directory.Exists(currentPath))
                    {
                        break;
                    }
                }

                //起点となるディレクトリからファイルを作っていく
                for (var dirCount = baseIndex + 1; dirCount < arrDir.Length; dirCount++)
                {
                    currentPath += @"\" + arrDir[dirCount];
                    if (!Directory.Exists(currentPath))
                    {
                        Directory.CreateDirectory(currentPath);
                    }
                }
            }
        }

        private static string JoinDirectories(string[] arrDir, int baseIndex)
        {
            var result = string.Empty;
            for (var counter = 0; counter <= baseIndex; counter++)
            {
                result += arrDir[counter] + @"\";
            }
            return result;
        }
    }
}
