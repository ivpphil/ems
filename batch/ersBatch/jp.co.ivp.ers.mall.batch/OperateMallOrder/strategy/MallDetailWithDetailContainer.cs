﻿using System;
using System.Collections.Generic;
using System.Reflection;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.batch.OperateMallOrder.strategy
{
    /// <summary>
    /// モール伝票明細＋伝票明細コンテナ [Container of mall order detail with order detail]
    /// </summary>
    public class MallDetailWithDetailContainer
        : IErsCollectable, IOverwritable
    {
        /// <summary>
        /// 送り状番号 [Tracking number]
        /// </summary>
        public virtual string sendno { get; set; }

        /// <summary>
        /// 出荷日 [Shipping date]
        /// </summary>
        public virtual DateTime? shipdate { get; set; }

        /// <summary>
        /// 配送方法 [Delivery method]
        /// </summary>
        public virtual EnumDelvMethod? deliv_method { get; set; }

        /// <summary>
        /// 商品ID（Amazon） [Item id (Amazon)]
        /// </summary>
        public virtual string a_item_id { get; set; }

        /// <summary>
        /// 数量 [Quantity]
        /// </summary>
        public virtual int? quantity { get; set; }


        /// <summary>
        /// プロパティ取得 [Get properties]
        /// </summary>
        /// <returns>プロパティ [Properties]</returns>
        public Dictionary<string, object> GetPropertiesAsDictionary()
        {
            return ErsReflection.GetPropertiesAsDictionary(this, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        }

        /// <summary>
        /// パラメータで上書き [Overwrite with parameters]
        /// </summary>
        /// <param name="dictionary">パラメータ [Parameter]</param>
        public void OverwriteWithParameter(IDictionary<string, object> dictionary)
        {
            var dic = ErsCommon.OverwriteDictionary(this.GetPropertiesAsDictionary(), dictionary);
            ErsReflection.SetPropertyAll(this, dic);
        }
    }
}
