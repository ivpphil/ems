﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.api.order_status;
using jp.co.ivp.ers.mall.mall_order;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.mall.batch.OperateMallOrder.strategy
{
    /// <summary>
    /// モール受注ステータス更新パラメータ取得 [Get the parameter for update mall order status]
    /// </summary>
    public class ObtainUpdateOrderStatusParamStgy
    {
        /// <summary>
        /// モール受注ステータス更新パラメータ取得 [Get the parameter for update mall order status]
        /// </summary>
        /// <param name="objOrder">伝票ヘッダ [Order header]</param>
        /// <param name="updateStatus">更新ステータス [Status for update]</param>
        /// <returns>モール受注ステータス更新パラメータ [The parameter for update mall order status]</returns>
        public UpdateOrderStatusParam Obtain(ErsOrder objOrder, EnumMallOrderStatus updateStatus)
        {
            UpdateOrderStatusParam param = default(UpdateOrderStatusParam);

            param.orderCode = objOrder.mall_d_no;
            param.orderDate = objOrder.intime;

            switch (objOrder.mall_shop_kbn)
            {
                // 楽天 [rakuten]
                case EnumMallShopKbn.RAKUTEN:
                    // 処理済み [Done]
                    if (updateStatus == EnumMallOrderStatus.Done)
                    {
                        // 伝票明細リスト取得 [Get the list of order detail]
                        var listData = this.GetOrderRecordList(objOrder.d_no);

                        // モール伝票明細リスト取得 [Get the list of mall order detail]
                        var listMallData = this.GetMallOrderDetailList(objOrder.mall_d_no);

                        var objData = listData.FirstOrDefault();
                        var objMallData = listMallData.FirstOrDefault();

                        param.deliveryName = objMallData.ship_name;
                        param.deliveryAddress = objMallData.ship_address;
                        param.invoiceNumber = objData.sendno;
                    }
                    break;

                // Amazon [Amazon]
                case EnumMallShopKbn.AMAZON:
                    // 出荷済み [Shipped]
                    if ((EnumMallOrderStatusAmazon)updateStatus == EnumMallOrderStatusAmazon.Shipped)
                    {
                        // 伝票明細リスト取得 [Get the list of order detail]
                        var listData = this.GetOrderRecordList(objOrder.d_no);

                        var objData = listData.FirstOrDefault();

                        var viewService = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService();

                        param.carrier = viewService.GetStringFromId(EnumCommonNameType.DelvMethod, EnumCommonNameColumnName.opt_chr1, (int)objData.deliv_method.Value);
                        param.deliveryDate = objData.shipdate;
                        param.deliveryMethod = viewService.GetStringFromId(EnumCommonNameType.DelvMethod, EnumCommonNameColumnName.namename, (int)objData.deliv_method.Value);
                        param.invoiceNumber = objData.sendno;

                        if (objOrder.pay == EnumPaymentType.CASH_ON_DELIVERY)
                        {
                            param.paymentMethod = "代引";
                        }

                        var listItems = new List<UpdateOrderStatusItemParam>();

                        foreach (var data in listData)
                        {
                            var itemParam = default(UpdateOrderStatusItemParam);

                            itemParam.itemId = data.mall_item_id;
                            itemParam.quantity = data.mall_quantity;

                            listItems.Add(itemParam);
                        }

                        param.listItems = listItems;
                    }
                    break;

                default:
                    break;
            }

            return param;
        }

        #region 伝票明細リスト取得 [Get the list of order detail]
        /// <summary>
        /// 伝票明細リスト取得 [Get the list of order detail]
        /// </summary>
        /// <param name="d_no">伝票番号 [Order number]</param>
        /// <returns>伝票明細リスト [The list of order detail]</returns>
        protected IList<ErsOrderRecord> GetOrderRecordList(string d_no)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();

            criteria.d_no = d_no;

            // 同梱商品以外 [Not bundled item]
            criteria.doc_bundling_flg = EnumDocBundlingFlg.OFF;

            // キャンセル以外 [Except canceled]
            criteria.order_status_not_in = new EnumOrderStatusType[] { EnumOrderStatusType.CANCELED };

            return repository.Find(criteria);
        }
        #endregion

        #region モール伝票明細リスト取得 [Get the list of mall order detail]
        /// <summary>
        /// モール伝票明細リスト取得 [Get the list of mall order detail]
        /// </summary>
        /// <param name="order_code">モール伝票番号 [Mall order number]</param>
        /// <returns>モール伝票明細リスト [The list of order detail]</returns>
        protected IList<ErsMallOrderDetail> GetMallOrderDetailList(string order_code)
        {
            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderDetailRepository();
            var criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderDetailCriteria();

            criteria.order_code = order_code;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            return repository.Find(criteria);
        }
        #endregion
    }
}
