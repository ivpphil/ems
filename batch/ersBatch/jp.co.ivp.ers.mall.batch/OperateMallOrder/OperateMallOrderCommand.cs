﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.api.order_status;
using jp.co.ivp.ers.mall.batch.OperateMallOrder.strategy;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.mall.batch.OperateMallOrder
{
    public class OperateMallOrderCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();

            var listError = new List<string>();

            // 対象伝票を取得
            var listOrder = this.GetListOrder();

            foreach (var newOrder in listOrder)
            {
                try
                {
                    var oldOrder = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(newOrder.GetPropertiesAsDictionary());

                    using (var tx = ErsDB_parent.BeginTransaction())
                    {
                        // モール伝票操作 [Operate mall order]
                        this.OperateMallOrder(newOrder);

                        repository.Update(oldOrder, newOrder);

                        tx.Commit();
                    }
                }
                catch (Exception e)
                {
                    listError.Add(e.ToString());
                }
            }

            if (listError.Count > 0)
            {
                throw new Exception(string.Join(Environment.NewLine, listError));
            }
        }

        /// <summary>
        /// 対象伝票を取得
        /// </summary>
        /// <returns></returns>
        private IList<ErsOrder> GetListOrder()
        {
            // -モール伝票
            // -配送済み
            // -モールステータスが新着

            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.mall_shop_kbn_not_equal = EnumMallShopKbn.ERS;
            criteria.mall_order_status = EnumMallOrderStatus.New;
            criteria.order_status_in = new[] { EnumOrderStatusType.DELIVERED };

            return repository.Find(criteria);
        }

        /// <summary>
        /// モール伝票操作 [Operate mall order]
        /// </summary>
        /// <param name="objOrder">伝票 [Order]</param>
        /// <param name="oldStatus">旧受注ステータス [Old order status]</param>
        /// <param name="newStatus">新受注ステータス [New order status]</param>
        protected virtual void OperateMallOrder(ErsOrder objOrder)
        {
            var newOrderRecordList = ErsFactory.ersOrderFactory.GetErsOrderRecordList(objOrder);
            var newStatus = ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy().ObtainFrom(newOrderRecordList.Values);

            // 更新用モール伝票ステータス取得 [Get mall order status for update]
            var status = ErsMallFactory.ersMallOrderFactory.GetObtainMallOrderStatusForUpdateStgy().Obtain(objOrder.mall_shop_kbn, newStatus);

            if (status == null || status == objOrder.mall_order_status)
            {
                return;
            }

            // モール受注ステータス更新パラメータ取得 [Get the parameter for update mall order status]
            UpdateOrderStatusParam param = new ObtainUpdateOrderStatusParamStgy().Obtain(objOrder, status.Value);

            // モール伝票ステータス更新 [Update mall order status]
            ErsMallFactory.ersMallOrderFactory.GetUpdateMallOrderStatusStgy().UpdateMallOrderStatus(objOrder.site_id, objOrder.mall_shop_kbn, param, status);

            objOrder.mall_order_status = status;
        }
    }
}
