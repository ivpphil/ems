﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.batch.OperateMallProduct.specification;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.OperateMallProduct.strategy
{
    /// <summary>
    /// 登録用モール商品テンポラリ（＋画像ディレクトリ）取得 [Get the branch product (+ Image directory) for operate]
    /// </summary>
    public class ObtainRegisterMallProductTmpWithImageDirectoryStgy
    {
        /// <summary>
        /// 登録用モール商品テンポラリ取得 [Get the branch product for operate]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateFrom">検索日時FROM [Search datetime (FROM)]</param>
        /// <param name="dateTo">検索日時TO [Search datetime (TO)]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>商品リスト [List of merchandise]</returns>
        public IList<ErsMallProductTmp> Obtain(int? siteId, DateTime dateFrom, DateTime dateTo, IList<string> listSpecifiedScode)
        {
            var spec = new SearchMallProductTmpWithImageDirectorySpec();
            var criteria = this.GetCriteria(siteId, dateFrom, dateTo, listSpecifiedScode);

            if (spec.GetCountData(criteria) == 0)
            {
                return null;
            }

            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

            var listFind = spec.GetSearchData(criteria);

            var listRet = new List<ErsMallProductTmp>();

            foreach (var data in listFind)
            {
                var obj = ErsMallFactory.ersMallProductFactory.GetErsMallProductTmp();
                obj.OverwriteWithParameter(data);
                listRet.Add(obj);
            }

            return listRet;
        }

        /// <summary>
        /// クライテリア取得 [Get the criteria]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateFrom">検索日時FROM [Search datetime (FROM)]</param>
        /// <param name="dateTo">検索日時TO [Search datetime (TO)]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>クライテリア [Criteria]</returns>
        protected virtual ErsMallProductTmpCriteria GetCriteria(int? siteId, DateTime dateFrom, DateTime dateTo, IList<string> listSpecifiedScode)
        {
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallProductTmpCriteria();

            var dateNow = DateTime.Now;

            // モール商品登録用 [For register mall products]
            criteria.SetSearchForRegister(dateFrom, dateTo);

            if (siteId != null)
            {
                criteria.site_id = siteId;
            }

            if (listSpecifiedScode != null)
            {
                criteria.scode_in = listSpecifiedScode;
            }

            return criteria;
        }
    }
}
