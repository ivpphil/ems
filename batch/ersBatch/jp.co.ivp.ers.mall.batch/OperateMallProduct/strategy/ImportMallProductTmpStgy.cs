﻿using System;
using System.Collections.Generic;
using System.Linq;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.batch.OperateMallProduct.specification;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.OperateMallProduct.strategy
{
    /// <summary>
    /// モール商品テンポラリインポート [Import mall product temporary]
    /// </summary>
    public class ImportMallProductTmpStgy
    {
        #region プロパティ [Properties]
        /// <summary>
        /// モール商品画像ディレクトリID [Mall product image directory id]
        /// </summary>
        protected virtual int? imageDirectoryId { get; set; }

        /// <summary>
        /// モール商品画像カウント [Mall product count of images]
        /// </summary>
        protected virtual long? countImage { get; set; }
        #endregion


        /// <summary>
        /// モール商品テンポラリインポート [Import mall product temporary]
        /// </summary>
        /// <param name="listMallProductTmp">モール商品テンポラリリスト [The list of mall product temporary]</param>
        public void Import(IList<ErsMallProductTmp> listMallProductTmp)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductTmpRepository();

            using (var tx = ErsDB_parent.BeginTransaction())
            {
                foreach (var data in listMallProductTmp)
                {
                    if (data.operation_type == null)
                    {
                        throw new Exception("Unexpected pattern.");
                    }

                    if (data.operation_type == EnumMallProductOperationType.REGIST)
                    {
                        // 登録用モール商品テンポラリデータセット [Set the data of the mall product temporary for Insert]
                        var objInsert = this.SetMallProductDataForInsert(data);
                        repository.Insert(objInsert);

                        // モール商品画像ディレクトリ登録 [Register mall product image directory]
                        this.RegisterMallProductImageDirectory(data);
                    }
                    else
                    {
                        // モール商品テンポラリ取得 [Get the mall product temporary]
                        var objMallProductTmp = this.ObtainMallProductTmp(data);

                        var objUpdate = default(ErsMallProductTmp);

                        if (data.operation_type == EnumMallProductOperationType.UPDATE)
                        {
                            // 更新用モール商品テンポラリデータセット [Set the data of the mall product temporary for Update]
                            objUpdate = this.SetMallProductDataForUpdate(objMallProductTmp, data);
                        }
                        else if (data.operation_type == EnumMallProductOperationType.DELETE)
                        {
                            // 削除用モール商品テンポラリデータセット [Set the data of the mall product temporary for Update]
                            objUpdate = this.SetMallProductDataForDelete(objMallProductTmp, data);
                        }
                        else
                        {
                            throw new Exception("Unexpected pattern.");
                        }

                        repository.Update(objMallProductTmp, objUpdate);
                    }
                }

                tx.Commit();
            }
        }

        #region モール商品画像ディレクトリ登録 [Register mall product image directory]
        /// <summary>
        /// モール商品画像ディレクトリ登録 [Register mall product image directory]
        /// </summary>
        /// <param name="objMallProductTmp">モール商品テンポラリ [The mall product temporary]</param>
        protected virtual void RegisterMallProductImageDirectory(ErsMallProductTmp objMallProductTmp)
        {
            // 楽天のみ [Rakuten only]
            if (objMallProductTmp.mall_shop_kbn != EnumMallShopKbn.RAKUTEN)
            {
                return;
            }

            if (this.imageDirectoryId == null)
            {
                // モール商品画像ディレクトリ情報初期化 [Initialize mall product image directory information]
                this.InitImageDirectoryInfo(objMallProductTmp.site_id);
            }

            // ディレクトリ判定＋カウントアップ [Judgement directory + Count up]
            if (this.countImage++ >= ErsMallCommonConst.MALL_PRODUCT_IMAGE_COUNT_BY_DIRECTORY)
            {
                this.countImage = 1;
                this.imageDirectoryId++;
            }

            // モール商品画像ディレクトリ登録 [Register mall product image directory]
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageDirectoryRepository();
            var obj = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageDirectory();

            obj.active = EnumActive.Active;
            obj.site_id = objMallProductTmp.site_id;
            obj.mall_shop_kbn = objMallProductTmp.mall_shop_kbn;
            obj.scode = objMallProductTmp.scode;
            obj.directory_id = this.imageDirectoryId;

            repository.Insert(obj);
        }

        /// <summary>
        /// モール商品画像ディレクトリ情報初期化 [Initialize mall product image directory information]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        protected virtual void InitImageDirectoryInfo(int? siteId)
        {
            var spec = new MaxDirectoryIdForMallProductImageDirectorySpec();
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageDirectoryRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageDirectoryCriteria();

            criteria.active = EnumActive.Active;
            criteria.site_id = siteId;

            // モール商品画像ディレクトリID最大値取得 [Get mall product image directory max id]
            this.imageDirectoryId = Convert.ToInt32(spec.GetSearchData(criteria).First()["max_directory_id"]);

            criteria.directory_id = this.imageDirectoryId;

            // モール商品画像カウント取得 [Get mall product count of images]
            this.countImage = repository.GetRecordCount(criteria);
        }
        #endregion

        #region モール商品テンポラリ取得 [Get the mall product temporary]
        /// <summary>
        /// モール商品テンポラリ取得 [Get the mall product temporary]
        /// </summary>
        /// <param name="objMallProductTmp">モール商品テンポラリ [The mall product temporary]</param>
        /// <returns>モール商品テンポラリ [The mall product temporary]</returns>
        protected ErsMallProductTmp ObtainMallProductTmp(ErsMallProductTmp objMallProductTmp)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductTmpRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallProductTmpCriteria();

            criteria.site_id = objMallProductTmp.site_id;
            criteria.scode = objMallProductTmp.scode;

            var listFind = repository.Find(criteria);

            return listFind.Count == 1 ? listFind[0] : null;
        }
        #endregion

        #region 更新用モール商品テンポラリデータセット [Set the data of the mall product temporary for Update]
        /// <summary>
        /// 更新用モール商品テンポラリデータセット [Set the data of the mall product temporary for Update]
        /// </summary>
        /// <param name="objOldMallProductTmp">旧モール商品テンポラリ [The old mall product temporary]</param>
        /// <param name="objNewMallProductTmp">新モール商品テンポラリ [The new mall product temporary]</param>
        /// <returns>モール商品テンポラリ [The mall product temporary]</returns>
        protected ErsMallProductTmp SetMallProductDataForUpdate(ErsMallProductTmp objOldMallProductTmp, ErsMallProductTmp objNewMallProductTmp)
        {
            objNewMallProductTmp.id = objOldMallProductTmp.id;
            objNewMallProductTmp.intime = objOldMallProductTmp.intime;
            objNewMallProductTmp.utime = objOldMallProductTmp.utime;
            objNewMallProductTmp.active = EnumActive.Active;

            return objNewMallProductTmp;
        }
        #endregion

        #region 登録用モール商品テンポラリデータセット [Set the data of the mall product temporary for Insert]
        /// <summary>
        /// 登録用モール商品テンポラリデータセット [Set the data of the mall product temporary for Insert]
        /// </summary>
        /// <param name="objMallProductTmp">モール商品テンポラリ [The mall product temporary]</param>
        /// <returns>モール商品テンポラリ [The mall product temporary]</returns>
        protected ErsMallProductTmp SetMallProductDataForInsert(ErsMallProductTmp objMallProductTmp)
        {
            objMallProductTmp.id = null;
            objMallProductTmp.intime = null;
            objMallProductTmp.utime = null;
            objMallProductTmp.active = null;

            return objMallProductTmp;
        }
        #endregion

        #region 削除用モール商品テンポラリデータセット [Set the data of the mall product temporary for Delete]
        /// <summary>
        /// 削除用モール商品テンポラリデータセット [Set the data of the mall product temporary for Delete]
        /// </summary>
        /// <param name="objOldMallProductTmp">旧モール商品テンポラリ [The old mall product temporary]</param>
        /// <param name="objNewMallProductTmp">新モール商品テンポラリ [The new mall product temporary]</param>
        /// <returns>モール商品テンポラリ [The mall product temporary]</returns>
        protected ErsMallProductTmp SetMallProductDataForDelete(ErsMallProductTmp objOldMallProductTmp, ErsMallProductTmp objNewMallProductTmp)
        {
            objNewMallProductTmp.id = objOldMallProductTmp.id;
            objNewMallProductTmp.intime = objOldMallProductTmp.intime;
            objNewMallProductTmp.utime = objOldMallProductTmp.utime;
            objNewMallProductTmp.active = EnumActive.NonActive;

            return objNewMallProductTmp;
        }
        #endregion
    }
}
