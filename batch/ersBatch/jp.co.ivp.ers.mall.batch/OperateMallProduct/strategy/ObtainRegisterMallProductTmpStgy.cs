﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.OperateMallProduct.strategy
{
    /// <summary>
    /// 登録用モール商品テンポラリ取得 [Get the mall product for operate]
    /// </summary>
    public class ObtainRegisterMallProductTmpStgy
    {
        /// <summary>
        /// 登録用モール商品テンポラリ取得 [Get the mall product for operate]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateFrom">検索日時FROM [Search datetime (FROM)]</param>
        /// <param name="dateTo">検索日時TO [Search datetime (TO)]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>商品リスト [List of merchandise]</returns>
        public IList<ErsMallProductTmp> Obtain(int? siteId, DateTime dateFrom, DateTime dateTo, IList<string> listSpecifiedScode)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductTmpRepository();
            var criteria = this.GetCriteria(siteId, dateFrom, dateTo, listSpecifiedScode);

            if (repository.GetRecordCount(criteria) == 0)
            {
                return null;
            }

            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

            return repository.Find(criteria);
        }

        /// <summary>
        /// クライテリア取得 [Get the criteria]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateFrom">検索日時FROM [Search datetime (FROM)]</param>
        /// <param name="dateTo">検索日時TO [Search datetime (TO)]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>クライテリア [Criteria]</returns>
        protected virtual ErsMallProductTmpCriteria GetCriteria(int? siteId, DateTime dateFrom, DateTime dateTo, IList<string> listSpecifiedScode)
        {
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallProductTmpCriteria();

            var dateNow = DateTime.Now;

            // モール商品登録用 [For register mall products]
            criteria.SetSearchForRegister(dateFrom, dateTo);

            if (siteId != null)
            {
                criteria.site_id = siteId;
            }

            if (listSpecifiedScode != null)
            {
                criteria.scode_in = listSpecifiedScode;
            }

            return criteria;
        }
    }
}
