﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.mall.batch.OperateMallProduct.strategy
{
    /// <summary>
    /// 操作モール商品取得 [Get the mall product for operate]
    /// </summary>
    public class ObtainOperateMallProductStgy
    {
        /// <summary>
        /// 登録モール商品件数取得 [Get the count of mall product for register]
        /// </summary>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <param name="executeDate">実行日時 [Execute date]</param>
        /// <param name="lastDate">前回実行日時 [Last execute date]</param>
        /// <returns>件数 [Count]</returns>
        public long ObtainCount(IList<string> listSpecifiedScode, DateTime? executeDate, DateTime? lastDate)
        {
            var spec = ErsMallFactory.ersMallProductFactory.GetSearchMallProductsSpec();
            var criteria = this.GetCriteria(listSpecifiedScode, executeDate, lastDate);

            return spec.GetCountData(criteria);
        }

        /// <summary>
        /// 登録モール商品取得 [Get the mall product for register]
        /// </summary>
        /// <param name="offset">オフセット [Offset]</param>
        /// <param name="limit">リミット [Limit]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <param name="executeDate">実行日時 [Execute date]</param>
        /// <param name="lastDate">前回実行日時 [Last execute date]</param>
        /// <returns>商品リスト [List of merchandise]</returns>
        public IList<ErsMallProductTmp> Obtain(int offset, int limit, IList<string> listSpecifiedScode, DateTime? executeDate, DateTime? lastDate)
        {
            var spec = ErsMallFactory.ersMallProductFactory.GetSearchMallProductsSpec();
            var criteria = this.GetCriteria(listSpecifiedScode, executeDate, lastDate);

            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

            criteria.OFFSET = offset;
            criteria.LIMIT = limit;

            var listFind = spec.GetSearchData(criteria);
            var listRet = new List<ErsMallProductTmp>();

            foreach (var data in listFind)
            {
                var obj = ErsMallFactory.ersMallProductFactory.GetErsMallProductTmp();
                obj.OverwriteWithParameter(data);
                listRet.Add(obj);
            }

            return listRet;
        }

        /// <summary>
        /// クライテリア取得 [Get the criteria]
        /// </summary>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <param name="executeDate">実行日時 [Execute date]</param>
        /// <param name="lastDate">前回実行日時 [Last execute date]</param>
        /// <returns>クライテリア [Criteria]</returns>
        protected virtual ErsMallMerchandiseCriteria GetCriteria(IList<string> listSpecifiedScode, DateTime? executeDate, DateTime? lastDate)
        {
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();

            var dateNow = DateTime.Now;

            // モール商品登録用 [For register mall products]
            criteria.SetSearchMallProductForRegister(lastDate.HasValue ? lastDate.Value : dateNow.AddYears(-10), executeDate.HasValue ? executeDate.Value : dateNow);

            if (listSpecifiedScode != null)
            {
                criteria.scode_in = listSpecifiedScode;
            }

            return criteria;
        }
    }
}
