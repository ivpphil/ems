﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.batch;

namespace jp.co.ivp.ers.mall.batch.OperateMallProduct
{
    /// <summary>
    /// コマンド [Command]
    /// </summary>
    public class OperateMallProductCommand
        : IErsBatchCommand
    {
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="batchName">バッチ名 [Batch name]</param>
        /// <param name="executeDate">実行日時 [Execute date]</param>
        /// <param name="argDictinary">引数 [Arguments]</param>
        /// <param name="lastDate">前回実行日時 [Last execute date]</param>
        /// <param name="batchLocation">バッチロケーション [Batch location]</param>
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> argDictinary, DateTime? lastDate, string batchLocation)
        {
            DateTime? lastSuccessDate = null;

            if (argDictinary != null && argDictinary.ContainsKey("last_success_date") && argDictinary["last_success_date"] != null)
            {
                lastSuccessDate = (DateTime?)Convert.ToDateTime(argDictinary["last_success_date"]);
            }

            new OperateMallProduct().Execute(argDictinary, executeDate, lastSuccessDate);
        }
    }
}
