﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.batch.OperateMallProduct.strategy;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.batch.OperateMallProduct.mall
{
    /// <summary>
    /// モール商品操作クラス（楽天） [Class for operate mall product (楽天)]
    /// </summary>
    public class OperateMallProductRakuten
        : OperateMallProductBase
    {
        #region ディレクトリパス・ファイル名 [Directory path, File name]
        /// <summary>
        /// 楽天CSVディレクトリパス [Rakuten CSV file name]
        /// </summary>
        protected const string RAKUTEN_CSV_DIRECTORY_PATH = "ritem/batch";

        /// <summary>
        /// 楽天CSVファイル名 [Rakuten CSV file name]
        /// </summary>
        protected const string RAKUTEN_CSV_FILE_NAME = "item.csv";

        /// <summary>
        /// 楽天カテゴリCSVファイル名 [Rakuten Category CSV file name]
        /// </summary>
        protected const string RAKUTEN_CATEGORY_CSV_FILE_NAME = "item-cat.csv";
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        public OperateMallProductRakuten(int? siteId, EnumMallShopKbn? shopKbn, IList<string> listSpecifiedScode)
            : base(siteId, shopKbn, listSpecifiedScode)
        {
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        public override void Execute(DateTime dateExecute)
        {
            // 登録用モール商品テンポラリ取得 [Get the mall product for operate]
            var listMallProductTmp = this.ObtainMallProductTmpList(this.siteId, dateExecute);

            if (listMallProductTmp == null)
            {
                return;
            }

            // CSVファイル生成 [Create the CSV file]
            this.CreateCsv(listMallProductTmp);

            // CSVファイル（カテゴリ）生成 [Create the CSV file (Category)]
            //this.CreateCategoryCsv(listMallProductTmp);
        }
        #endregion

        #region 登録用モール商品テンポラリ取得 [Get the branch product for operate]
        /// <summary>
        /// 登録用モール商品テンポラリ取得 [Get the branch product for operate]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <returns>抽出日時パラメータ [Parameter for import datetime]</returns>
        protected override IList<ErsMallProductTmp> ObtainMallProductTmpList(int? siteId, DateTime dateExecute)
        {
            // 抽出日時取得 [Get datetime for extract]
            this.extractDateTime = this.ObtainExtractDateTime(siteId, dateExecute);

            // 登録用モール商品テンポラリ取得 [Get the branch product for operate]
            return new ObtainRegisterMallProductTmpWithImageDirectoryStgy().Obtain(siteId, this.extractDateTime.dateFrom.Value, this.extractDateTime.dateTo.Value, this.listSpecifiedScode);
        }
        #endregion

        #region CSVファイル生成 [Create the CSV file]
        /// <summary>
        /// CSVファイル生成 [Create the CSV file]
        /// </summary>
        /// <param name="listMallProductTmp">商品リスト [List of merchandise]</param>
        protected void CreateCsv(IList<ErsMallProductTmp> listMallProductTmp)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            var dirPath = setup.mallProductCsvOutputPath;
            var delGetFileName = new jp.co.ivp.ers.mall.product.rakuten.RakutenTsvMapper.GetFileName(GetFileName);

            // ディレクトリ作成 [Create the directory]
            ErsDirectory.CreateDirectories(dirPath);

            // CSVファイル生成 [Create the CSV file]
            foreach (var fileName in ErsMallFactory.ersMallBatchFactory.GetRakutenTsvMapper().Map(dirPath, delGetFileName, listMallProductTmp, this.extractDateTime))
            {
                // モール商品ファイルアップロード管理登録 [Register mall product file upload management]
                this.RegisterMallProductFileUploadManagement(this.siteId.Value, this.shopKbn.Value, fileName, EnumMallProductUploadFileType.RAKUTEN_ITEM);
            }
        }

        /// <summary>
        /// ファイル名取得（デリゲート用） [Get file name (for Delegate)]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="divide">分割カウント [divide count]</param>
        /// <returns>ファイル名 [File name]</returns>
        public string GetFileName(int siteId, int divide)
        {
            return string.Format("{0}_{1}_{2}_rakuten_item.csv", DateTime.Now.ToString("yyyyMMddHHmmssfffffff"), siteId, divide);
        }

        /// <summary>
        /// CSVファイル（カテゴリ）生成 [Create the CSV file (Category)]
        /// </summary>
        /// <param name="listMallProductTmp">商品リスト [List of merchandise]</param>
        protected void CreateCategoryCsv(IList<ErsMallProductTmp> listMallProductTmp)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            var dirPath = setup.mallProductCsvOutputPath;
            var delGetFileName = new jp.co.ivp.ers.mall.product.rakuten.RakutenTsvMapper.GetFileNameCategory(GetFileNameCategory);

            // CSVファイル生成 [Create the CSV file]
            foreach (var fileName in ErsMallFactory.ersMallBatchFactory.GetRakutenTsvMapper().MapCategory(dirPath, delGetFileName, listMallProductTmp, this.extractDateTime))
            {
                // モール商品ファイルアップロード管理登録 [Register mall product file upload management]
                this.RegisterMallProductFileUploadManagement(this.siteId.Value, this.shopKbn.Value, fileName, EnumMallProductUploadFileType.RAKUTEN_CATEGORY);
            }
        }

        /// <summary>
        /// ファイル名取得（デリゲート用） [Get file name (for Delegate)]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="divide">分割カウント [divide count]</param>
        /// <returns>ファイル名 [File name]</returns>
        public string GetFileNameCategory(int siteId, int divide)
        {
            return string.Format("{0}_{1}_{2}_rakuten_item-cat.csv", DateTime.Now.ToString("yyyyMMddHHmmssfffffff"), siteId, divide);
        }
        #endregion
    }
}
