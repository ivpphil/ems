﻿using System;
using System.Collections.Generic;
using System.IO;
using jp.co.ivp.ers.common;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.batch.OperateMallProduct.strategy;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.OperateMallProduct.mall
{
    /// <summary>
    /// モール商品操作クラス（基底） [Class for operate mall product (Base)]
    /// </summary>
    public abstract class OperateMallProductBase
    {
        #region 基本パラメータ [Basic parameters]
        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        public virtual int? siteId { get; protected set; }

        /// <summary>
        /// 店舗タイプ [Shop type]
        /// </summary>
        public virtual EnumMallShopKbn? shopKbn { get; protected set; }

        /// <summary>
        /// 指定商品コードリスト [The list of specified scode]
        /// </summary>
        public virtual IList<string> listSpecifiedScode { get; protected set; }

        /// <summary>
        /// 抽出日時 [Extract datetime]
        /// </summary>
        public virtual ErsCommonStruct.DateTimeStartEnd extractDateTime { get; protected set; }
        #endregion

        #region 結果パラメータ [Result parameters]
        /// <summary>
        /// エラーログ [Error log]
        /// </summary>
        public virtual string errorLog { get; protected set; }
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        public OperateMallProductBase(int? siteId, EnumMallShopKbn? shopKbn, IList<string> listSpecifiedScode)
        {
            this.siteId = siteId;
            this.shopKbn = shopKbn;
            this.listSpecifiedScode = listSpecifiedScode;
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        public abstract void Execute(DateTime dateExecute);
        #endregion

        #region 抽出日時取得 [Get datetime for extract]
        /// <summary>
        /// 抽出日時取得 [Get datetime for extract]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <returns>抽出日時パラメータ [Parameter for import datetime]</returns>
        protected virtual ErsCommonStruct.DateTimeStartEnd ObtainExtractDateTime(int? siteId, DateTime dateExecute)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductExtractRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallProductExtractCriteria();

            criteria.site_id = siteId;
            criteria.LIMIT = 1;

            ErsCommonStruct.DateTimeStartEnd retParam = default(ErsCommonStruct.DateTimeStartEnd);

            // 存在確認 [Check exists]
            if (repository.GetRecordCount(criteria) > 0)
            {
                criteria.SetOrderByExtractDate(Criteria.OrderBy.ORDER_BY_DESC);

                var listFind = repository.Find(criteria);

                // 取得した日時を指定 [Specify get day]
                retParam.dateFrom = listFind[0].extract_date;
            }
            else
            {
                // 実行日時前日を指定 [Specify execute yesterday]
                retParam.dateFrom = dateExecute.AddDays(-1);
            }

            retParam.dateTo = dateExecute;

            return retParam;
        }
        #endregion

        #region 登録用モール商品テンポラリ取得 [Get the mall product for operate]
        /// <summary>
        /// 登録用モール商品テンポラリ取得 [Get the mall product for operate]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <returns>抽出日時パラメータ [Parameter for import datetime]</returns>
        protected virtual IList<ErsMallProductTmp> ObtainMallProductTmpList(int? siteId, DateTime dateExecute)
        {
            // 抽出日時取得 [Get datetime for extract]
            this.extractDateTime = this.ObtainExtractDateTime(siteId, dateExecute);

            // 登録用モール商品テンポラリ取得 [Get the mall product for operate]
            return new ObtainRegisterMallProductTmpStgy().Obtain(siteId, this.extractDateTime.dateFrom.Value, this.extractDateTime.dateTo.Value, this.listSpecifiedScode);
        }
        #endregion

        #region 画像ファイルが存在するかどうか [Is exists image files]
        /// <summary>
        /// 画像ファイルが存在するかどうか [Is exists image files]
        /// </summary>
        /// <param name="filePathNoExt">ファイルパス（拡張子除く） [File path (exclude extention)]</param>
        /// <returns>ファイルパス [File path]</returns>
        protected string IsExistsImageFile(string filePathNoExt)
        {
            foreach (var ext in ErsCommonConst.MERCHANDISE_IMAGE_EXTENSIONS)
            {
                var filePath = filePathNoExt + ext;

                if (File.Exists(filePath))
                {
                    return filePath;
                }
            }

            return string.Empty;
        }
        #endregion

        #region モール商品ファイルアップロード管理登録 [Register mall product file upload management]
        /// <summary>
        /// モール商品ファイルアップロード管理登録 [Register mall product file upload management]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">モール店舗区分 [Mall shop type]</param>
        /// <param name="fileName">ファイル名 [File name]</param>
        /// <param name="fileType">モール商品アップロードファイルタイプ [Mall product upload file type]</param>
        protected virtual void RegisterMallProductFileUploadManagement(int siteId, EnumMallShopKbn shopKbn, string fileName, EnumMallProductUploadFileType? fileType)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductFileUploadManageRepository();
            var obj = ErsMallFactory.ersMallProductFactory.GetErsMallProductFileUploadManage();

            obj.site_id = siteId;
            obj.mall_shop_kbn = shopKbn;
            obj.file_type = fileType;
            obj.file_name = fileName;

            repository.Insert(obj);
        }
        #endregion

        #region 抽出日時登録 [Register extract management]
        /// <summary>
        /// 抽出日時登録 [Register extract management]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        public virtual void RegisterProductExtract(DateTime dateExecute)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductExtractRepository();
            var obj = ErsMallFactory.ersMallProductFactory.GetErsMallProductExtract();

            obj.site_id = this.siteId;
            obj.mall_shop_kbn = this.shopKbn;
            obj.extract_date = dateExecute;

            repository.Insert(obj);
        }
        #endregion
    }
}
