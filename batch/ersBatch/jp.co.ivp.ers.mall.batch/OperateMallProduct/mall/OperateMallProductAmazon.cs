﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.batch.OperateMallProduct.mall
{
    /// <summary>
    /// モール商品操作クラス（Amazon） [Class for operate mall product (Amazon)]
    /// </summary>
    public class OperateMallProductAmazon
        : OperateMallProductBase
    {
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        public OperateMallProductAmazon(int? siteId, EnumMallShopKbn? shopKbn, IList<string> listSpecifiedScode)
            : base(siteId, shopKbn, listSpecifiedScode)
        {
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        public override void Execute(DateTime dateExecute)
        {
            // 登録用モール商品テンポラリ取得 [Get the mall product for operate]
            var listMallProductTmp = this.ObtainMallProductTmpList(this.siteId, dateExecute);

            if (listMallProductTmp == null)
            {
                return;
            }

            // TSVファイル生成 [Create the TSV file]
            this.CreateTsv(listMallProductTmp);
        }
        #endregion

        #region TSVファイル生成 [Create the TSV file]
        /// <summary>
        /// TSVファイル生成 [Create the TSV file]
        /// </summary>
        /// <param name="listMallProductTmp">商品リスト [List of merchandise]</param>
        protected void CreateTsv(IList<ErsMallProductTmp> listMallProductTmp)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            var dirPath = setup.mallProductCsvOutputPath;
            var delGetFileName = new jp.co.ivp.ers.mall.product.amazon.AmazonTsvMapperBase.GetFileName(GetFileName);

            // ディレクトリ作成 [Create the directory]
            ErsDirectory.CreateDirectories(dirPath);

            // CSVファイル生成 [Create the CSV file]
            foreach (var fileName in ErsMallFactory.ersMallBatchFactory.GetAmazonTsvMapper().Map(dirPath, delGetFileName, listMallProductTmp, this.extractDateTime))
            {
                // モール商品ファイルアップロード管理登録 [Register mall product file upload management]
                this.RegisterMallProductFileUploadManagement(this.siteId.Value, this.shopKbn.Value, fileName, EnumMallProductUploadFileType.AMAZON_ITEM);
            }
        }

        /// <summary>
        /// ファイル名取得（デリゲート用） [Get file name (for Delegate)]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="divide">分割カウント [divide count]</param>
        /// <returns>ファイル名 [File name]</returns>
        public string GetFileName(int siteId, int divide)
        {
            return string.Format("{0}_{1}_{2}_amazon.txt", DateTime.Now.ToString("yyyyMMddHHmmssfffffff"), siteId, divide);
        }
        #endregion
    }
}
