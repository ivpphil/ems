﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.batch.OperateMallProduct.specification
{
    /// <summary>
    /// モール商品画像ディレクトリID最大値取得 [Get mall product image directory max id]
    /// </summary>
    public class MaxDirectoryIdForMallProductImageDirectorySpec
         : SearchSpecificationBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return base.GetSearchData(criteria);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string GetSearchDataSql()
        {
            return "SELECT MAX(directory_id) AS max_directory_id FROM mall_product_image_directory_t";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countColumnAlias"></param>
        /// <returns></returns>
        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            throw new NotImplementedException();
        }
    }
}
