﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using jp.co.ivp.ers.mall.batch.OperateMallProduct.mall;
using jp.co.ivp.ers.mall.batch.OperateMallProduct.strategy;

namespace jp.co.ivp.ers.mall.batch.OperateMallProduct
{
    /// <summary>
    /// モール商品操作メイン [Operate mall product main]
    /// </summary>
    public class OperateMallProduct
    {
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="argDictinary">引数 [Arguments]</param>
        /// <param name="executeDate">実行日時 [Execute date]</param>
        /// <param name="lastDate">前回実行日時 [Last execute date]</param>
        public void Execute(IDictionary<string, object> argDictinary, DateTime? executeDate, DateTime? lastDate)
        {
            // 実行日時取得 [Get execute datetime]
            var dateExecute = DateTime.Now;

            // 指定商品コードリスト取得 [Get the list of specified scode]
            var listSpecifiedScode = this.GetSpecifiedScodeList(argDictinary);

            // モール商品テンポラリインポート [Import mall product temporary]
            this.ImportMallProductTmp(listSpecifiedScode, executeDate, lastDate);

            // 実行リスト取得 [Get the list of execute]
            var listExecute = this.GetExecuteList(dateExecute, listSpecifiedScode);

            // 実行 [Execute]
            var resultExecute = this.Execute(listExecute, DateTime.Now);

            if (!string.IsNullOrEmpty(resultExecute))
            {
                throw new Exception(resultExecute);
            }
        }

        #region モール商品テンポラリインポート [Import mall product temporary]
        /// <summary>
        /// モール商品テンポラリインポート [Import mall product temporary]
        /// </summary>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <param name="executeDate">実行日時 [Execute date]</param>
        /// <param name="lastDate">前回実行日時 [Last execute date]</param>
        protected virtual void ImportMallProductTmp(IList<string> listSpecifiedScode, DateTime? executeDate, DateTime? lastDate)
        {
            var stgyObtain = new ObtainOperateMallProductStgy();
            var stgyImport = new ImportMallProductTmpStgy();

            int count = 0;
            int divide = 5000;     // 5千件分割

            // 登録支店商品件数取得 [Get the count of branch product for register]
            long total = stgyObtain.ObtainCount(listSpecifiedScode, executeDate, lastDate);

            if (total == 0)
            {
                return;
            }

            while (true)
            {
                int start = count * divide;
                int end = (count + 1) * divide;

                // 操作モール商品取得 [Get the mall product for operate]
                var listMallProduct = stgyObtain.Obtain(start, divide, listSpecifiedScode, executeDate, lastDate);

                if (listMallProduct != null)
                {
                    // モール商品テンポラリインポート [Import mall product temporary]
                    stgyImport.Import(listMallProduct);
                }

                if (end >= total)
                {
                    break;
                }

                count++;
            }
        }
        #endregion

        #region 指定商品コードリスト取得 [Get the list of specified scode]
        /// <summary>
        /// 指定商品コードリスト取得 [Get the list of specified scode]
        /// </summary>
        /// <param name="argDictinary">引数 [Arguments]</param>
        /// <returns>指定商品コードリスト [The list of specified scode]</returns>
        protected IList<string> GetSpecifiedScodeList(IDictionary<string, object> argDictinary)
        {
            if (argDictinary == null)
            {
                return null;
            }
            if (!argDictinary.ContainsKey("scode"))
            {
                return null;
            }

            return argDictinary["scode"].ToString().Split(',').Select(value => value.Trim()).ToList<string>();
        }
        #endregion

        #region 実行リスト取得 [Get the list of execute]
        /// <summary>
        /// 実行リスト取得 [Get the list of execute]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>実行リスト [The list of execute]</returns>
        protected IList<OperateMallProductBase> GetExecuteList(DateTime dateExecute, IList<string> listSpecifiedScode)
        {
            var siteData = ErsMallFactory.ersSiteFactory.GetSiteData();
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();
            var listRet = new List<OperateMallProductBase>();

            foreach (var siteId in siteData.dicSiteData.Keys)
            {
                // モール店舗タイプ取得（サイトIDから） [Get mall shop type (from Site id)]
                var shopKbn = siteData.GetMallShopKbnFromSiteId(siteId);

                switch (shopKbn)
                {
                    // 楽天 [Rakuten]
                    case EnumMallShopKbn.RAKUTEN:
                        // 楽天 [Rakuten]
                        if (setup.doOperateProductRakuten && !this.isWithinExcludeDateTime(EnumMallShopKbn.RAKUTEN, dateExecute))
                        {
                            listRet.Add(new OperateMallProductRakuten(siteId, EnumMallShopKbn.RAKUTEN, listSpecifiedScode));
                        }
                        break;

                    // Yahoo! [Yahoo!]
                    case EnumMallShopKbn.YAHOO:
                        // Yahoo! [Yahoo!]
                        if (setup.doOperateProductYahoo && !this.isWithinExcludeDateTime(EnumMallShopKbn.YAHOO, dateExecute))
                        {
                            listRet.Add(new OperateMallProductYahoo(siteId, EnumMallShopKbn.YAHOO, listSpecifiedScode));
                        }
                        break;

                    // Amazon [amazon]
                    case EnumMallShopKbn.AMAZON:
                        // Amazon [amazon]
                        if (setup.doOperateProductAmazon && !this.isWithinExcludeDateTime(EnumMallShopKbn.AMAZON, dateExecute))
                        {
                            listRet.Add(new OperateMallProductAmazon(siteId, EnumMallShopKbn.AMAZON, listSpecifiedScode));
                        }
                        break;
                }
            }

            return listRet;
        }

        /// <summary>
        /// 除外日時内判定 [Judgement within exclude DateTime]
        /// </summary>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="dateNow">現在日時 [DateTime Now]</param>
        /// <returns>True : 除外 [Exclude] / False : 除外しない [Not exlude]</returns>
        protected bool isWithinExcludeDateTime(EnumMallShopKbn? shopKbn, DateTime dateNow)
        {
            return ErsMallFactory.ersMallStopTimeFactory.GetIsWithinExcludeDateTimeStgy().IsWithin(shopKbn, EnumMallFuncType.Product, dateNow);
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="listExec">実行リスト [The list of execute]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <returns>エラーログ [Error log]</returns>
        protected string Execute(IList<OperateMallProductBase> listExec, DateTime dateExecute)
        {
            var exceptions = new ConcurrentQueue<OperateMallProductException>();

            Parallel.ForEach(listExec, exec =>
            {
                try
                {
                    // 実行 [Execute]
                    exec.Execute(dateExecute);

                    // 抽出日時登録 [Register extract management]
                    exec.RegisterProductExtract(dateExecute);
                }
                catch (Exception e)
                {
                    // 例外追加 [Add the exception]
                    exceptions.Enqueue(new OperateMallProductException(e, DateTime.Now, exec.siteId, exec.shopKbn));
                }
            });

            string errorLog = string.Empty;

            // 例外エラーをまとめる [Aggregate the exception]
            if (exceptions.Count > 0)
            {
                foreach (var e in exceptions)
                {
                    errorLog += e.ToString() + Environment.NewLine;
                }
            }

            // エラーログをまとめる [Aggregate the error log]
            foreach (var exec in listExec)
            {
                errorLog += exec.errorLog + (string.IsNullOrEmpty(exec.errorLog) ? string.Empty : Environment.NewLine);
            }

            return errorLog;
        }
        #endregion
    }
}
