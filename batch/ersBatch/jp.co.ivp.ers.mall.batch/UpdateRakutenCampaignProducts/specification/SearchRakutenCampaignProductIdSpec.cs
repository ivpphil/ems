﻿using System.Collections.Generic;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.batch.UpdateRakutenCampaignProducts.specification
{
    /// <summary>
    /// 楽天キャンペーン対象商品ID検索 [Get rakuten campaign product id for update]
    /// </summary>
    public class SearchRakutenCampaignProductIdSpec
         : SearchSpecificationBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return base.GetSearchData(criteria);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string GetSearchDataSql()
        {
            return "SELECT mall_s_master_t.id FROM mall_s_master_t";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countColumnAlias"></param>
        /// <returns></returns>
        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(*) AS " + countColumnAlias + " FROM mall_s_master_t";
        }
    }
}
