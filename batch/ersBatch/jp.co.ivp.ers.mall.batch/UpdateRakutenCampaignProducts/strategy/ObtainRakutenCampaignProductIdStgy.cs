﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.batch.UpdateRakutenCampaignProducts.specification;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.UpdateRakutenCampaignProducts.strategy
{
    /// <summary>
    /// 楽天キャンペーン対象商品ID取得 [Get rakuten campaign product id for update]
    /// </summary>
    public class ObtainRakutenCampaignProductIdStgy
    {
        /// <summary>
        /// 楽天キャンペーン対象商品ID取得 [Get rakuten campaign product id for update]
        /// </summary>
        public IList<int> Obtain()
        {
            var spec = new SearchRakutenCampaignProductIdSpec();
            var criteria = this.GetCriteria();

            if (spec.GetCountData(criteria) == 0)
            {
                return null;
            }

            var listFind = spec.GetSearchData(criteria);

            var listRet = new List<int>();

            foreach (var data in listFind)
            {
                listRet.Add(Convert.ToInt32(data["id"]));
            }

            return listRet;
        }

        /// <summary>
        /// クライテリア取得 [Get the criteria]
        /// </summary>
        /// <returns>クライテリア [Criteria]</returns>
        protected ErsMallMerchandiseCriteria GetCriteria()
        {
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();

            criteria.mall_shop_kbn = EnumMallShopKbn.RAKUTEN;

            // 楽天キャンペーン対象 [Rakuten campaign]
            criteria.SetRakutenCampign();

            return criteria;
        }
    }
}
