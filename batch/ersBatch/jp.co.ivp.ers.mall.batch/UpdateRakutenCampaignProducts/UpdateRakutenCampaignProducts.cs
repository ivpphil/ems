﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.batch.UpdateRakutenCampaignProducts.strategy;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.batch.UpdateRakutenCampaignProducts
{
    /// <summary>
    /// 楽天キャンペーン商品更新メイン [Update rakuten products for campaign]
    /// </summary>
    public class UpdateRakutenCampaignProducts
    {
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        public void Execute()
        {
            // 楽天キャンペーン対象商品ID取得 [Get rakuten campaign product id for update]
            var listTarget = new ObtainRakutenCampaignProductIdStgy().Obtain();

            if (listTarget == null)
            {
                return;
            }

            // 更新 [Update]
            this.Update(listTarget);
        }

        #region 更新 [Update]
        /// <summary>
        /// 更新 [Update]
        /// </summary>
        /// <param name="listTarget">対象IDリスト [The list of target id]</param>
        protected void Update(IList<int> listTarget)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();
            var dateNow = DateTime.Now;

            foreach (var id in listTarget)
            {
                var objOld = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseById(id);
                if (objOld == null)
                {
                    throw new ErsException("10200");
                }

                var objNew = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandise();
                objNew.OverwriteWithParameter(objOld.GetPropertiesAsDictionary());

                //// 期間中 [In campaign]
                //if (objOld.opt_date_1 <= dateNow && dateNow <= objOld.opt_date_2)
                //{
                //    objNew.opt_flg_5 = 1;
                //}
                //else
                //{
                //    objNew.opt_flg_5 = 0;
                //}

                // 楽天キャンペーン対象商品更新 [Update rakuten campaign products for update]
                repository.Update(objOld, objNew);
            }
        }
        #endregion
    }
}
