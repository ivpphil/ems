﻿using System.Net;

namespace jp.co.ivp.ers.mall.batch.UploadMallProductFile.common
{
    /// <summary>
    /// FTPファイル存在確認 [Check exists file on FTP]
    /// </summary>
    public class CheckFTPFileExists
    {
        /// <summary>
        /// ファイル存在チェック [Check the file exists]
        /// </summary>
        /// <param name="uri">URI [URI]</param>
        /// <param name="user">FTPユーザ [FTP user]</param>
        /// <param name="pass">FTPパス [FTP pass]</param>
        /// <returns>true : 存在している [Exists] / false : 存在していない [Not exists]</returns>
        public bool ExistsFile(string uri, string user, string pass)
        {
            var request = (FtpWebRequest)WebRequest.Create(uri);

            request.Credentials = new NetworkCredential(user, pass);
            request.Method = WebRequestMethods.Ftp.GetDateTimestamp;

            try
            {
                using (var response = (FtpWebResponse)request.GetResponse())
                {
                }

                return true;
            }
            catch (WebException e)
            {
                using (var response = (FtpWebResponse)e.Response)
                {
                    if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    {
                        return false;
                    }
                }

                throw e;
            }
        }
    }
}
