﻿using System.IO;
using System.Net;
using jp.co.ivp.ers.mall.batch.UploadMallProductFile.common;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.UploadMallProductFile.mall
{
    /// <summary>
    /// モール商品ファイルアップロードクラス（楽天） [Class for upload mall product file (楽天)]
    /// </summary>
    public class UploadMallProductFileRakuten
        : UploadMallProductFileBase
    {
        #region ディレクトリパス・ファイル名 [Directory path, File name]
        /// <summary>
        /// 楽天CSVディレクトリパス [Rakuten CSV file name]
        /// </summary>
        protected const string RAKUTEN_CSV_DIRECTORY_PATH = "ritem/batch";

        /// <summary>
        /// 楽天CSVファイル名 [Rakuten CSV file name]
        /// </summary>
        protected const string RAKUTEN_CSV_FILE_NAME = "item.csv";

        /// <summary>
        /// 楽天カテゴリCSVファイル名 [Rakuten Category CSV file name]
        /// </summary>
        protected const string RAKUTEN_CATEGORY_CSV_FILE_NAME = "item-cat.csv";
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public UploadMallProductFileRakuten(int? siteId, EnumMallShopKbn? shopKbn)
            : base(siteId, shopKbn)
        {
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        public override void Execute()
        {
            // 実行（商品） [Execute (Product)]
            this.ExecuteItem();

            // 実行（カテゴリ） [Execute (Category)]
            //this.ExecuteCategory();
        }
        #endregion

        #region 実行（商品） [Execute (Product)]
        /// <summary>
        /// 実行（商品） [Execute (Product)]
        /// </summary>
        protected void ExecuteItem()
        {
            // モール商品ファイルアップロード管理取得 [Get mall product upload management]
            var objManage = this.ObtainMallProductFileUploadManage(EnumMallProductUploadFileType.RAKUTEN_ITEM);

            if (objManage == null)
            {
                return;
            }

            // アップロード（商品） [Upload (Product)]
            if (this.Upload(objManage, RAKUTEN_CSV_FILE_NAME))
            {
                // モール商品ファイルアップロード管理更新 [Update mall product upload management]
                this.UpdateMallProductFileUploadManage(objManage);
            }
        }
        #endregion

        #region 実行（カテゴリ） [Execute (Category)]
        /// <summary>
        /// 実行（カテゴリ） [Execute (Category)]
        /// </summary>
        protected void ExecuteCategory()
        {
            // モール商品ファイルアップロード管理取得 [Get mall product upload management]
            var objManage = this.ObtainMallProductFileUploadManage(EnumMallProductUploadFileType.RAKUTEN_CATEGORY);

            if (objManage == null)
            {
                return;
            }

            // アップロード（カテゴリ） [Upload (Category)]
            if (this.Upload(objManage, RAKUTEN_CATEGORY_CSV_FILE_NAME))
            {
                // モール商品ファイルアップロード管理更新 [Update mall product upload management]
                this.UpdateMallProductFileUploadManage(objManage);
            }
        }
        #endregion

        #region アップロード [Upload]
        /// <summary>
        /// アップロード [Upload]
        /// </summary>
        /// <param name="objManage">モール商品ファイルアップロード管理 [Mall product upload management]</param>
        /// <param name="csvFileName">CSVファイル名 [CSV file name]</param>
        /// <returns>アップロード結果 [Upload result]</returns>
        protected bool Upload(ErsMallProductFileUploadManage objManage, string csvFileName)
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var stgy = new CheckFTPFileExists();

            string ftpHost = mallSetup.GetRakutenFtpHost(this.siteId.Value);
            string ftpUser = mallSetup.GetRakutenFtpUser(this.siteId.Value);
            string ftpPass = mallSetup.GetRakutenFtpPass(this.siteId.Value);

            // パス設定 [Path settings]
            string uri = string.Format("ftp://{0}/{1}/", ftpHost, RAKUTEN_CSV_DIRECTORY_PATH);

            var fileURI = uri + csvFileName;

            // ファイル存在チェック [Check the file exists]
            if (stgy.ExistsFile(fileURI, ftpUser, ftpPass))
            {
                return false;
            }

            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            // アップロード [Upload]
            using (var wc = new WebClient())
            {
                wc.Credentials = new NetworkCredential(ftpUser, ftpPass);
                wc.UploadFile(fileURI, Path.Combine(setup.mallProductCsvOutputPath, objManage.file_name));
            }

            return true;
        }
        #endregion
    }
}
