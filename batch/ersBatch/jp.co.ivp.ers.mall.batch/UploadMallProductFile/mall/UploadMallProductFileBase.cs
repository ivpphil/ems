﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.UploadMallProductFile.mall
{
    /// <summary>
    /// モール商品ファイルアップロードクラス（基底） [Class for upload mall product file (Base)]
    /// </summary>
    public class UploadMallProductFileBase
    {
        #region 基本パラメータ [Basic parameters]
        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        public virtual int? siteId { get; protected set; }

        /// <summary>
        /// 支店タイプ [Shop type]
        /// </summary>
        public virtual EnumMallShopKbn? shopKbn { get; protected set; }
        #endregion

        #region 結果パラメータ [Result parameters]
        /// <summary>
        /// エラーログ [Error log]
        /// </summary>
        public virtual string errorLog { get; protected set; }
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public UploadMallProductFileBase(int? siteId, EnumMallShopKbn? shopKbn)
        {
            this.siteId = siteId;
            this.shopKbn = shopKbn;
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        public virtual void Execute()
        {
        }
        #endregion

        #region モール商品ファイルアップロード管理取得 [Get mall product upload management]
        /// <summary>
        /// モール商品ファイルアップロード管理取得 [Get mall product upload management]
        /// </summary>
        /// <param name="fileType">モール商品アップロードファイルタイプ [Mall product upload file type]</param>
        /// <returns>モール商品ファイルアップロード管理 [Mall product upload management]</returns>
        protected virtual ErsMallProductFileUploadManage ObtainMallProductFileUploadManage(EnumMallProductUploadFileType? fileType)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductFileUploadManageRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallProductFileUploadManageCriteria();

            criteria.active = EnumActive.Active;
            criteria.site_id = this.siteId;
            criteria.file_type = fileType;

            if (repository.GetRecordCount(criteria) == 0)
            {
                return null;
            }

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.LIMIT = 1;

            return repository.FindSingle(criteria);
        }
        #endregion

        #region モール商品ファイルアップロード管理更新 [Update mall product upload management]
        /// <summary>
        /// モール商品ファイルアップロード管理更新 [Update mall product upload management]
        /// </summary>
        /// <param name="objManage">モール商品ファイルアップロード管理 [Mall product upload management]</param>
        protected virtual void UpdateMallProductFileUploadManage(ErsMallProductFileUploadManage objManage)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductFileUploadManageRepository();
            var objOld = ErsMallFactory.ersMallProductFactory.GetErsMallProductFileUploadManage();

            objOld.OverwriteWithParameter(objManage.GetPropertiesAsDictionary());

            objManage.active = EnumActive.NonActive;

            repository.Update(objOld, objManage);
        }
        #endregion
    }
}
