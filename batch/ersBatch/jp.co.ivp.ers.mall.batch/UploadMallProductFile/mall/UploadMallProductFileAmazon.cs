﻿using System.IO;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.UploadMallProductFile.mall
{
    /// <summary>
    /// モール商品ファイルアップロードクラス（Amazon） [Class for upload mall product file (Amazon)]
    /// </summary>
    public class UploadMallProductFileAmazon
        : UploadMallProductFileBase
    {
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public UploadMallProductFileAmazon(int? siteId, EnumMallShopKbn? shopKbn)
            : base(siteId, shopKbn)
        {
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        public override void Execute()
        {
            // モール商品ファイルアップロード管理取得 [Get mall product upload management]
            var objManage = this.ObtainMallProductFileUploadManage(EnumMallProductUploadFileType.AMAZON_ITEM);

            if (objManage == null)
            {
                return;
            }

            // アップロード（商品） [Upload (Product)]
            if (this.Upload(objManage))
            {
                // モール商品ファイルアップロード管理更新 [Update mall product upload management]
                this.UpdateMallProductFileUploadManage(objManage);
            }
        }
        #endregion

        #region アップロード（商品） [Upload (Product)]
        /// <summary>
        /// アップロード（商品） [Upload (Product)]
        /// </summary>
        /// <param name="objManage">モール商品ファイルアップロード管理 [Mall product upload management]</param>
        /// <returns>アップロード結果 [Upload result]</returns>
        protected bool Upload(ErsMallProductFileUploadManage objManage)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            var factory = ErsMallFactory.ersMallAmazonFactory;
            var service = factory.GetObtainMWSSeriviceClientStgy().Obtain(this.siteId);

            var submissionId = string.Empty;

            using (var stream = File.Open(Path.Combine(setup.mallProductCsvOutputPath, objManage.file_name), FileMode.Open, FileAccess.Read))
            {
                // SubmitFeedリクエスト [Request SubmitFeed]
                submissionId = factory.GetSubmitFeed().Request(service, stream, EnumMallAmazonFeedType._POST_FLAT_FILE_LISTINGS_DATA_);
            }

            //EnumMallAmazonFeedProcessingStatus? status = null;

            //Stopwatch sw = Stopwatch.StartNew();
            //sw.Start();

            //while (true)
            //{
            //    // GetFeedSubmissionListリクエスト [Request GetFeedSubmissionList]
            //    status = factory.GetGetFeedSubmissionList().Request(service, submissionId);

            //    if (status != null)
            //    {
            //        if (status != EnumMallAmazonFeedProcessingStatus._IN_PROGRESS_ &&
            //            status != EnumMallAmazonFeedProcessingStatus._SUBMITTED_)
            //        {
            //            break;
            //        }
            //    }

            //    if (sw.Elapsed.Minutes >= 20)
            //    {
            //        throw new Exception(string.Format("Can't get processing status. submissionId = {0}", submissionId));
            //    }

            //    // ４５秒待つ（回復レート：４５秒毎につき１リクエスト）
            //    System.Threading.Thread.Sleep(45000);
            //}

            //sw.Stop();

            //if (status != EnumMallAmazonFeedProcessingStatus._DONE_)
            //{
            //    throw new Exception(string.Format("SubmitFeed was failed. status = {0}, submissionId = {1}", status.ToString(), submissionId));
            //}

            return true;
        }
        #endregion
    }
}
