﻿using System.IO;
using System.Net;
using jp.co.ivp.ers.mall.batch.UploadMallProductFile.common;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.UploadMallProductFile.mall
{
    /// <summary>
    /// モール商品ファイルアップロードクラス（Yahoo!） [Class for upload mall product file (Yahoo!)]
    /// </summary>
    public class UploadMallProductFileYahoo
        : UploadMallProductFileBase
    {
        #region ファイル名 [File name]
        /// <summary>
        /// Yahoo!CSVファイル名 [Yahoo! CSV file name]
        /// </summary>
        protected const string YAHOO_CSV_FILE_NAME = "data_add.csv";
        #endregion

        
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public UploadMallProductFileYahoo(int? siteId, EnumMallShopKbn? shopKbn)
            : base(siteId, shopKbn)
        {
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        public override void Execute()
        {
            // モール商品ファイルアップロード管理取得 [Get mall product upload management]
            var objManage = this.ObtainMallProductFileUploadManage(EnumMallProductUploadFileType.YAHOO_ITEM);

            if (objManage == null)
            {
                return;
            }

            // アップロード（商品） [Upload (Product)]
            if (this.Upload(objManage))
            {
                // モール商品ファイルアップロード管理更新 [Update mall product upload management]
                this.UpdateMallProductFileUploadManage(objManage);
            }
        }
        #endregion

        #region アップロード（商品） [Upload (Product)]
        /// <summary>
        /// アップロード（商品） [Upload (Product)]
        /// </summary>
        /// <param name="objManage">モール商品ファイルアップロード管理 [Mall product upload management]</param>
        /// <returns>アップロード結果 [Upload result]</returns>
        protected bool Upload(ErsMallProductFileUploadManage objManage)
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var stgy = new CheckFTPFileExists();

            string ftpHost = mallSetup.GetYahooFtpHost(this.siteId.Value);
            string ftpUser = mallSetup.GetYahooFtpUser(this.siteId.Value);
            string ftpPass = mallSetup.GetYahooFtpPass(this.siteId.Value);

            // パス設定 [Path settings]
            string uri = string.Format("ftp://{0}/", ftpHost);

            var fileURI = uri + YAHOO_CSV_FILE_NAME;

            // ファイル存在チェック [Check the file exists]
            if (stgy.ExistsFile(fileURI, ftpUser, ftpPass))
            {
                return false;
            }

            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            // アップロード [Upload]
            using (var wc = new WebClient())
            {
                wc.Credentials = new NetworkCredential(ftpUser, ftpPass);
                wc.UploadFile(fileURI, Path.Combine(setup.mallProductCsvOutputPath, objManage.file_name));
            }

            return true;
        }
        #endregion
    }
}
