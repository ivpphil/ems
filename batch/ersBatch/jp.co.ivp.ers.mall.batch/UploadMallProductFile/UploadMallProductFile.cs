﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using jp.co.ivp.ers.mall.batch.UploadMallProductFile.mall;

namespace jp.co.ivp.ers.mall.batch.UploadMallProductFile
{
    /// <summary>
    /// モール商品ファイルアップロードメイン [Upload mall product file]
    /// </summary>
    public class UploadMallProductFile
    {
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        public void Execute()
        {
            // 実行日時取得 [Get execute datetime]
            var dateExecute = DateTime.Now;

            // 実行リスト取得 [Get the list of execute]
            var listExecute = this.GetExecuteList(dateExecute);

            // 実行 [Execute]
            var resultExecute = this.Execute(listExecute, DateTime.Now);

            if (!string.IsNullOrEmpty(resultExecute))
            {
                throw new Exception(resultExecute);
            }
        }

        #region 実行リスト取得 [Get the list of execute]
        /// <summary>
        /// 実行リスト取得 [Get the list of execute]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>実行リスト [The list of execute]</returns>
        protected IList<UploadMallProductFileBase> GetExecuteList(DateTime dateExecute)
        {
            var siteData = ErsMallFactory.ersSiteFactory.GetSiteData();
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();
            var listRet = new List<UploadMallProductFileBase>();

            foreach (var siteId in siteData.dicSiteData.Keys)
            {
                // モール店舗タイプ取得（サイトIDから） [Get mall shop type (from Site id)]
                var shopKbn = siteData.GetMallShopKbnFromSiteId(siteId);

                switch (shopKbn)
                {
                    // 楽天 [Rakuten]
                    case EnumMallShopKbn.RAKUTEN:
                        // 楽天 [Rakuten]
                        if (setup.doUploadProductFileRakuten && !this.isWithinExcludeDateTime(EnumMallShopKbn.RAKUTEN, dateExecute))
                        {
                            listRet.Add(new UploadMallProductFileRakuten(siteId, EnumMallShopKbn.RAKUTEN));
                        }
                        break;

                    // Yahoo! [Yahoo!]
                    case EnumMallShopKbn.YAHOO:
                        // Yahoo! [Yahoo!]
                        if (setup.doUploadProductFileYahoo && !this.isWithinExcludeDateTime(EnumMallShopKbn.YAHOO, dateExecute))
                        {
                            listRet.Add(new UploadMallProductFileYahoo(siteId, EnumMallShopKbn.YAHOO));
                        }
                        break;

                    // Amazon [amazon]
                    case EnumMallShopKbn.AMAZON:
                        // Amazon [amazon]
                        if (setup.doUploadProductFileAmazon && !this.isWithinExcludeDateTime(EnumMallShopKbn.AMAZON, dateExecute))
                        {
                            listRet.Add(new UploadMallProductFileAmazon(siteId, EnumMallShopKbn.AMAZON));
                        }
                        break;
                }
            }

            return listRet;
        }

        /// <summary>
        /// 除外日時内判定 [Judgement within exclude DateTime]
        /// </summary>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="dateNow">現在日時 [DateTime Now]</param>
        /// <returns>True : 除外 [Exclude] / False : 除外しない [Not exlude]</returns>
        protected bool isWithinExcludeDateTime(EnumMallShopKbn? shopKbn, DateTime dateNow)
        {
            return ErsMallFactory.ersMallStopTimeFactory.GetIsWithinExcludeDateTimeStgy().IsWithin(shopKbn, EnumMallFuncType.Product, dateNow);
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="listExec">実行リスト [The list of execute]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <returns>エラーログ [Error log]</returns>
        protected string Execute(IList<UploadMallProductFileBase> listExec, DateTime dateExecute)
        {
            var exceptions = new ConcurrentQueue<UploadMallProductFileException>();

            Parallel.ForEach(listExec, exec =>
            {
                try
                {
                    // 実行 [Execute]
                    exec.Execute();
                }
                catch (Exception e)
                {
                    // 例外追加 [Add the exception]
                    exceptions.Enqueue(new UploadMallProductFileException(e, DateTime.Now, exec.siteId, exec.shopKbn));
                }
            });

            string errorLog = string.Empty;

            // 例外エラーをまとめる [Aggregate the exception]
            if (exceptions.Count > 0)
            {
                foreach (var e in exceptions)
                {
                    errorLog += e.ToString() + Environment.NewLine;
                }
            }

            // エラーログをまとめる [Aggregate the error log]
            foreach (var exec in listExec)
            {
                errorLog += exec.errorLog + (string.IsNullOrEmpty(exec.errorLog) ? string.Empty : Environment.NewLine);
            }

            return errorLog;
        }
        #endregion
    }
}
