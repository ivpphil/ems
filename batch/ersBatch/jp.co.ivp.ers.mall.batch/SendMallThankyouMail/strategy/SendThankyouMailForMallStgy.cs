﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mall.batch.SendMallThankyouMail.model;
using System.Collections.Generic;

namespace jp.co.ivp.ers.mall.batch.SendMallThankyouMail.strategy
{
    /// <summary>
    /// モール用注文完了メール送信 [Send order completion mail for mall]
    /// </summary>
    public class SendThankyouMailForMallStgy
    {
        /// <summary>
        /// モール用注文完了メール送信 [Send order completion mail for mall]
        /// </summary>
        /// <param name="objContainer">伝票コンテナ [The order container]</param>
        public void SendMail(ErsOrder objHeader, IEnumerable<ErsOrderRecord> orderRecords)
        {
            // メールモデル取得 [Get the mail model]
            var model = this.GetMailModel(objHeader, orderRecords);

            var objSendMail = ErsMallFactory.ersMallMailFactory.GetErsMallSendMailThankyou(objHeader.site_id);

            objSendMail.Send(objHeader.d_no, objHeader.email, EnumMformat.PC, model);
        }

        /// <summary>
        /// メールモデル取得 [Get the mail model]
        /// </summary>
        /// <param name="objContainer">伝票コンテナ [The order container]</param>
        /// <returns>メールモデル [The mail model]</returns>
        protected IErsModelBase GetMailModel(ErsOrder orderHeader, IEnumerable<ErsOrderRecord> orderRecords)
        {
            var model = new MallThankyouMailModel();

            model.OverwriteWithParameter(orderHeader.GetPropertiesAsDictionary());
            model.objOrder = orderHeader;
            model.listOrderRecords = orderRecords;

            return model;
        }
    }
}
