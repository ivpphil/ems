﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.batch.SendMallThankyouMail.model;
using jp.co.ivp.ers.mall.batch.SendMallThankyouMail.strategy;

namespace jp.co.ivp.ers.mall.batch.SendMallThankyouMail
{
    public class SendMallThankyouMailCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            var listError = new List<string>();

            // 対象伝票を取得
            var listOrder = this.GetListOrder();

            var repository = ErsFactory.ersOrderFactory.GetErsOrderMailRepository();

            foreach (var objOrder in listOrder)
            {
                var recordList = this.GetOrderRecords(objOrder);

                try
                {
                    using (var tx = ErsDB_parent.BeginTransaction())
                    {
                        foreach (var record in recordList)
                        {
                            // メールフラグを先に更新
                            var newOrderMail = this.GetOrderMail(record);
                            var oldOrderMail = ErsFactory.ersOrderFactory.GetErsOrderMail();
                            oldOrderMail.OverwriteWithParameter(newOrderMail.GetPropertiesAsDictionary());

                            newOrderMail.purchase_mail = EnumSentFlg.Sent;

                            repository.Update(oldOrderMail, newOrderMail);
                        }

                        var mailRecord = recordList.Where(orderRecord => orderRecord.doc_bundling_flg == EnumDocBundlingFlg.OFF);

                        if (mailRecord.Count() > 0)
                        {
                            // モール用注文完了メール送信 [Send order completion mail for mall]
                            new SendThankyouMailForMallStgy().SendMail(objOrder, mailRecord);
                        }

                        tx.Commit();
                    }
                }
                catch (Exception e)
                {
                    listError.Add(ErsResources.GetMessage("107001", objOrder.site_id, (int)objOrder.mall_shop_kbn, objOrder.d_no, e.ToString()));
                }
            }

            if (listError.Count > 0)
            {
                throw new Exception(string.Join(Environment.NewLine, listError));
            }
        }

        private ErsOrderMail GetOrderMail(ErsOrderRecord record)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderMailRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderMailCriteria();
            criteria.d_no = record.d_no;
            criteria.ds_id = record.id;
            criteria.active = EnumActive.Active;
            return repository.FindSingle(criteria);
        }

        /// <summary>
        /// 明細の一覧を取得
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private IList<ErsOrderRecord> GetOrderRecords(ErsOrder order)
        {
            // 該当伝票番号
            // メール未送信
            var orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var orderRecordCriteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
            orderRecordCriteria.d_no = order.d_no;
            orderRecordCriteria.purchase_mail = EnumSentFlg.NotSent;
            return orderRecordRepository.Find(orderRecordCriteria);
        }

        /// <summary>
        /// 対象伝票を取得
        /// </summary>
        /// <returns></returns>
        protected virtual IList<ErsOrder> GetListOrder()
        {
            // -モール伝票
            // -配送済み

            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.mall_shop_kbn_not_equal = EnumMallShopKbn.ERS;
            criteria.purchase_mail = EnumSentFlg.NotSent;

            return repository.Find(criteria);
        }
    }
}
