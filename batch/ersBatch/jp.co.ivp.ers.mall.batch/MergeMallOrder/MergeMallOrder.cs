﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.batch.MergeMallOrder.strategy;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mall.mall_order;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.mall.batch.MergeMallOrder
{
    /// <summary>
    /// モール伝票マージメイン [Merge mall order main]
    /// </summary>
    public class MergeMallOrder
    {
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="argDictinary">引数 [Arguments]</param>
        public void Execute(IDictionary<string, object> argDictinary)
        {
            // 指定受注日時パラメータ取得 [Get the parameter of specified order datetime]
            var specifiedOrderDateParam = this.GetSpecifiedOrderDateTimeParam(argDictinary);

            // マージモール伝票取得 [Get the mall order for merge]
            var listMallOrder = new ObtainMergeMallOrderStgy().Obtain(specifiedOrderDateParam);

            if (listMallOrder == null)
            {
                return;
            }

            // マージ [Merge]
            var errorMessage = this.Merge(listMallOrder);

            if (!string.IsNullOrEmpty(errorMessage))
            {
                // アラートメール送信 [Send an alert mail]
                this.SendAlertMail(errorMessage);
            }
        }

        #region 指定受注日時パラメータ取得 [Get the parameter of specified order datetime]
        /// <summary>
        /// 指定受注日時パラメータ取得 [Get the parameter of specified order datetime]
        /// </summary>
        /// <param name="argDictinary">引数 [Arguments]</param>
        /// <returns>指定受注番号リスト [The list of specified order number]</returns>
        protected ErsCommonStruct.DateTimeStartEnd GetSpecifiedOrderDateTimeParam(IDictionary<string, object> argDictinary)
        {
            var ret = default(ErsCommonStruct.DateTimeStartEnd);

            if (argDictinary == null)
            {
                return ret;
            }
            if (argDictinary.ContainsKey("order_date_from"))
            {
                DateTime from;

                if (!DateTime.TryParse(Convert.ToString(argDictinary["order_date_from"]), out from))
                {
                    throw new Exception("受注日時（開始）が不正です。");
                }

                ret.dateFrom = from;
            }
            if (argDictinary.ContainsKey("order_date_to"))
            {
                DateTime to;

                if (!DateTime.TryParse(Convert.ToString(argDictinary["order_date_to"]), out to))
                {
                    throw new Exception("受注日時（終了）が不正です。");
                }

                ret.dateTo = to;
            }

            return ret;
        }
        #endregion

        #region マージ [Merge]
        /// <summary>
        /// マージ [Merge]
        /// </summary>
        /// <param name="listMallOrder">モール伝票リスト [List of mall order for merge]</param>
        /// <returns>エラーメッセージ [Error message]</returns>
        protected virtual string Merge(IList<ErsMallOrder> listMallOrder)
        {
            var listErrorMessage = new List<string>();

            foreach (var objMallOrder in listMallOrder)
            {
                // モール伝票コンテナ取得 [Get the mall order container]
                var objMallContainer = this.GetMallOrderContainer(objMallOrder);

                // 伝票コンテナ取得 [Get the order container]
                var objContainer = this.GetOrderContainer(objMallContainer);

                EnumMergeStatus? mergeStatus = null;

                using (var tx = ErsDB_parent.BeginTransaction())
                {
                    // モール伝票バリデート [Validate mall order]
                    var errorMessage = this.ValidateMallOrder(objMallContainer);

                    if (string.IsNullOrEmpty(errorMessage))
                    {
                        // 登録 [Register]
                        if (objContainer == null)
                        {
                            // マージデータ取得（登録用） [Get the merger data (for Register)]
                            objContainer = this.GetMergeDataForRegister(objMallContainer);

                            // 伝票登録 [Register order]
                            this.RegistOrder(objContainer);
                        }
                        // 更新 [Update]
                        else
                        {
                            // 更新はしない
                        }

                        // モール伝票メールアドレスバリデート [Validate mall order email]
                        var alertMessage = ErsMallFactory.ersMallOrderFactory.GetValidateMallOrderEmailStgy().Validate(objContainer);

                        if (alertMessage.HasValue())
                        {
                            listErrorMessage.Add(alertMessage);
                        }

                        // 2 : マージ済み [Merged]
                        mergeStatus = EnumMergeStatus.Merged;
                    }
                    else
                    {
                        listErrorMessage.Add(errorMessage);

                        // 3 : 保留 [Pending]
                        mergeStatus = EnumMergeStatus.Pending;
                    }

                    // マージステータス更新 [Update merge status]
                    this.UpdateMergeStatus(objMallOrder, mergeStatus.Value);

                    tx.Commit();
                }
            }

            return listErrorMessage.Count == 0 ? string.Empty : String.Join(string.Empty, listErrorMessage);
        }

        /// <summary>
        /// モール伝票バリデート [Validate mall order]
        /// </summary>
        /// <param name="objMallContainer">モール伝票コンテナ [The mall order container]</param>
        /// <returns>エラーメッセージ [Error message]</returns>
        protected virtual string ValidateMallOrder(MallOrderContainer objMallContainer)
        {
            var listErrorMessage = new List<string>();

            foreach (var objMallOrderDetail in objMallContainer.listMallOrderDetail)
            {
                // 商品コードが無い場合は判定対象にしない [Not target when item_code is null or empty]
                if (objMallOrderDetail.ers_item_code.HasValue())
                {
                    var objSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(objMallOrderDetail.ers_item_code);

                    if (objSku == null)
                    {
                        var siteData = ErsMallFactory.ersSiteFactory.GetSiteData().dicSiteData;
                        var siteName = siteData[objMallContainer.objMallOrder.site_id.Value].site_name;
                        listErrorMessage.Add(ErsResources.GetMessage("107000", siteName, objMallContainer.objMallOrder.order_code, objMallContainer.objMallOrder.order_date, objMallOrderDetail.item_code));
                    }
                }
            }

            return listErrorMessage.Count == 0 ? string.Empty : String.Join(string.Empty, listErrorMessage);
        }

        /// <summary>
        /// マージデータ取得（登録用） [Get the merger data (for Register)]
        /// </summary>
        /// <param name="objMallContainer">モール伝票コンテナ [The mall order container]</param>
        /// <returns>伝票コンテナ [The order container]</returns>
        protected virtual ErsOrderContainer GetMergeDataForRegister(MallOrderContainer objMallContainer)
        {
            var objContainer = ErsFactory.ersOrderFactory.GetErsOrderContainer();

            // マージデータセット [Set the data for merge]
            return new SetMergeDataStgy().SetMergeData(objContainer, objMallContainer);
        }

        /// <summary>
        /// 伝票登録 [Register order]
        /// </summary>
        /// <param name="objContainer">伝票コンテナ [The order container]</param>
        protected virtual void RegistOrder(ErsOrderContainer objContainer)
        {
            var repositoryHeader = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var repositoryDetail = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var repositoryOrderMail = ErsFactory.ersOrderFactory.GetErsOrderMailRepository();

            // ヘッダ [Header]
            repositoryHeader.Insert(objContainer.OrderHeader);

            // ボディ [Detail]
            foreach (var objDetail in objContainer.OrderRecords)
            {
                repositoryDetail.Insert(objDetail.Value, true);

                var newOrderMail = this.GetOrderMail(objContainer, objDetail.Value);
                repositoryOrderMail.Insert(newOrderMail);
            }

            //set items
            ErsFactory.ersOrderFactory.GetRegistSetItemsStgy().Regist(objContainer.OrderRecords.Values);
        }

        /// <summary>
        /// メール送信判定レコード取得
        /// </summary>
        /// <param name="objContainer"></param>
        /// <param name="orderRecord"></param>
        /// <returns></returns>
        private ErsOrderMail GetOrderMail(ErsOrderContainer objContainer, ErsOrderRecord orderRecord)
        {
            var newOrderMail = ErsFactory.ersOrderFactory.GetErsOrderMail();

            newOrderMail.d_no = orderRecord.d_no;
            newOrderMail.ds_id = orderRecord.id;

            // メールアドレス不正の場合は送信済みとする [If e-mail address is invalid then set sent]
            if (ErsMallFactory.ersMallOrderFactory.GetValidateMallOrderEmailStgy().Validate(objContainer).HasValue())
            {
                newOrderMail.purchase_mail = EnumSentFlg.Sent;
                newOrderMail.shipped_mail = EnumSentFlg.Sent;
            }

            return newOrderMail;
        }

        /// <summary>
        /// マージステータス更新 [Update merge status]
        /// </summary>
        /// <param name="objMallOrder">モール伝票ヘッダ [The mall order header]</param>
        /// <param name="mergeStatus">マージステータス [Merge status]</param>
        protected virtual void UpdateMergeStatus(ErsMallOrder objMallOrder, EnumMergeStatus mergeStatus)
        {
            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderRepository();
            var objOld = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderWithID(objMallOrder.id.Value);

            objMallOrder.merge_status = mergeStatus;

            repository.Update(objOld, objMallOrder);
        }
        #endregion

        #region 伝票取得 [Get the order]
        /// <summary>
        /// 伝票コンテナ取得 [Get the order container]
        /// </summary>
        /// <param name="objMallContainer">モール伝票コンテナ [The mall order container]</param>
        /// <returns>伝票コンテナ [Order container]</returns>
        protected virtual ErsOrderContainer GetOrderContainer(MallOrderContainer objMallContainer)
        {
            var objMallOrder = objMallContainer.objMallOrder;

            // 伝票ヘッダ取得 [Get the order header]
            var objOrder = this.GetOrder(objMallOrder.site_id, objMallOrder.mall_shop_kbn, objMallOrder.order_code);

            if (objOrder == null)
            {
                return null;
            }

            var objContainer = ErsFactory.ersOrderFactory.GetErsOrderContainer();

            objContainer.OrderHeader = objOrder;
            objContainer.OrderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(objOrder);

            return objContainer;
        }

        /// <summary>
        /// 伝票ヘッダ取得 [Get the order header]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="mall_shop_kbn">モール店舗タイプ [Mall shop type]</param>
        /// <param name="mall_d_no">モール受注番号 [Mall order number]</param>
        /// <returns>伝票ヘッダ [The order header]</returns>
        protected virtual ErsOrder GetOrder(int? siteId, EnumMallShopKbn? mall_shop_kbn, string mall_d_no)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

            criteria.site_id = siteId;
            criteria.mall_shop_kbn = mall_shop_kbn;
            criteria.mall_d_no = mall_d_no;

            var listFind = repository.Find(criteria);

            return (listFind.Count > 0 ? listFind[0] : null);
        }
        #endregion

        #region モール伝票取得 [Get the mall order]
        /// <summary>
        /// モール伝票コンテナ取得 [Get the mall order container]
        /// </summary>
        /// <param name="objMallOrder">モール伝票 [Mall order for merge]</param>
        /// <returns>モール伝票コンテナ [The mall order container]</returns>
        protected virtual MallOrderContainer GetMallOrderContainer(ErsMallOrder objMallOrder)
        {
            var objContainer = ErsMallFactory.ersMallOrderFactory.GetMallOrderContainer();

            objContainer.objMallOrder = objMallOrder;
            objContainer.listMallOrderDetail = this.GetMallOrderDetailList(objMallOrder.d_no);

            return objContainer;
        }

        /// <summary>
        /// モール伝票ボディリスト取得 [Get the list of mall order detail]
        /// </summary>
        /// <param name="d_no">伝票番号 [d_no]</param>
        /// <returns>モール伝票ボディリスト [The list of mall order detail]</returns>
        protected virtual IList<ErsMallOrderDetail> GetMallOrderDetailList(string d_no)
        {
            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderDetailRepository();
            var criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderDetailCriteria();

            criteria.d_no = d_no;
            criteria.quantity_greater = 0;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            return repository.Find(criteria);
        }
        #endregion

        #region アラートメール送信 [Send an alert mail]
        /// <summary>
        /// アラートメール送信 [Send an alert mail]
        /// </summary>
        /// <param name="errorMessage">エラーメッセージ [Error message]</param>
        protected virtual void SendAlertMail(string errorMessage)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();
            var mail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();

            // メール送信 [Send an mail]
            mail.MailSend(
                setup.mergeMallOrderAlertMailTitle,
                errorMessage,
                Path.GetFileName(Assembly.GetCallingAssembly().Location),
                setup.mergeMallOrderAlertMailTo,
                setup.mergeMallOrderAlertMailFrom,
                setup.mergeMallOrderAlertMailCc,
                setup.mergeMallOrderAlertMailBcc);
        }
        #endregion
    }
}
