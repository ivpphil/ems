﻿using System;
using System.Collections.Generic;
using System.Linq;
using jp.co.ivp.ers.mall.mall_order;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.mall.batch.MergeMallOrder
{
    /// <summary>
    /// マージ支払い方法取得 [Get merge payment method]
    /// </summary>
    public class MallParameterConvertService
    {
        #region 構造体 [Structure]
        /// <summary>
        /// 住所情報 [Address information]
        /// </summary>
        public struct AddressInfo
        {
            /// <summary>
            /// 住所１ [Address 1]
            /// </summary>
            public string address { get; set; }

            /// <summary>
            /// 住所２ [Address 2]
            /// </summary>
            public string taddress { get; set; }

            /// <summary>
            /// 住所３ [Address 3]
            /// </summary>
            public string maddress { get; set; }
        }

        /// <summary>
        /// 配送情報 [Shipping information]
        /// </summary>
        public struct ShippingInfo
        {
            /// <summary>
            /// 配送希望日 [Delivery hope date]
            /// </summary>
            public DateTime? senddate { get; set; }

            /// <summary>
            /// 配送希望時間帯 [Delivery hope time zone]
            /// </summary>
            public int? sendtime { get; set; }

            /// <summary>
            /// 配送メモ [Delivery hope memo]
            /// </summary>
            public string sendmemo { get; set; }
        }
        #endregion

        #region 住所情報 [Address information]
        /// <summary>
        /// 郵便番号文字数 [Zip length]
        /// </summary>
        public const int MALL_ZIP_LENGTH = 10;

        /// <summary>
        /// 電話番号文字数 [Tel length]
        /// </summary>
        public const int MALL_TEL_LENGTH = 15;

        /// <summary>
        /// 住所１文字数 [Address 1 length]
        /// </summary>
        public const int MALL_ADDRESS_LENGTH = 128;

        /// <summary>
        /// 住所２文字数 [Address 2 length]
        /// </summary>
        public const int MALL_TADDRESS_LENGTH = 128;

        /// <summary>
        /// 住所３文字数 [Address 3 length]
        /// </summary>
        public const int MALL_MADDRESS_LENGTH = 128;
        #endregion

        #region Yahoo!支払い方法 [Yahoo! payment methods]
        /// <summary>
        /// クレジットカード [Credit card]
        /// </summary>
        public const string MALL_YAHOO_PAYMENT_METHOD_CREDIT_CARD = "YahooCreditCardSettle";

        /// <summary>
        /// 代金引換 [COD]
        /// </summary>
        public const string MALL_YAHOO_PAYMENT_METHOD_COD = "COD";

        /// <summary>
        /// YJPPOINT [YJPPOINT]
        /// </summary>
        public const string MALL_YAHOO_PAYMENT_METHOD_YJPPOINT = "YJPPOINT";
        #endregion

        #region 楽天支払い方法 [Rakuten payment methods]
        /// <summary>
        /// 代金引換 [COD]
        /// </summary>
        public const string MALL_RAKUTEN_PAYMENT_METHOD_COD = "代金引換";

        /// <summary>
        /// クレジットカード [Credit card]
        /// </summary>
        public const string MALL_RAKUTEN_PAYMENT_METHOD_CREDIT_CARD = "クレジットカード";
        #endregion

        #region Amazon支払い方法 [Amazon payment methods]
        /// <summary>
        /// Other [Other]
        /// </summary>
        public const string MALL_AMAZON_PAYMENT_METHOD_OTHER = "Other";

        /// <summary>
        /// 代金引換 [COD]
        /// </summary>
        public const string MALL_AMAZON_PAYMENT_METHOD_COD = "COD";

        /// <summary>
        /// コンビニ [CVS]
        /// </summary>
        public const string MALL_AMAZON_PAYMENT_METHOD_CVS = "CVS";
        #endregion

        #region 配送情報 [Shipping information]
        /// <summary>
        /// 配送情報項目名 [Item name for shipping information]
        /// </summary>
        public const string SHIPPING_INFO_ITEM_NAME = "[配送指定:]";
        #endregion

        #region Yahoo!決済ステータス [Yahoo! payment status]
        /// <summary>
        /// オーソリOK [Authorized]
        /// </summary>
        public const string MALL_YAHOO_PAYMENT_STATUS_AUTH = "1";
        #endregion

        #region 楽天決済ステータス [Rakuten payment status]
        /// <summary>
        /// オーソリ済み [Authorized]
        /// </summary>
        public const string MALL_RAKUTEN_PAYMENT_STATUS_AUTH = "オーソリ済み";

        /// <summary>
        /// カード対象外 [Not covered by credit card]
        /// </summary>
        public const string MALL_RAKUTEN_PAYMENT_STATUS_NOT_COVERED = "カード対象外";
        #endregion


        #region ラッピング金額取得 [Get wrapping price]
        /// <summary>
        /// ラッピング税額取得（明細から） [Get wrapping tax (from Details)]
        /// </summary>
        /// <param name="objMallOrder">モール伝票ヘッダ [The mall order header]</param>
        /// <param name="listMallDetail">モール伝票ボディリスト [The list of mall order detail]</param>
        /// <param name="out_tax">税額 [Tax]</param>
        /// <param name="out_cost">税抜き額 [Price exclude tax]</param>
        public virtual void GetWrappingPrice(ErsMallOrder objMallOrder, IList<ErsMallOrderDetail> listMallOrderDetail, out int out_tax, out int out_cost)
        {
            out_tax = 0;
            out_cost = 0;

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            float in_tax = setup.tax + 100;
            float tax = setup.tax;

            switch (objMallOrder.mall_shop_kbn)
            {
                // Yahoo! [Yahoo!]
                case EnumMallShopKbn.YAHOO:
                    break;

                // 楽天 [Rakuten]
                case EnumMallShopKbn.RAKUTEN:
                    for (var i = 0; i < listMallOrderDetail.Count; i++)
                    {
                        var objMallDetail = listMallOrderDetail[i];

                        // 商品コードが無い＋ラッピング [item_code is null or empty + it is warapping]
                        if (!objMallDetail.ers_item_code.HasValue() && objMallDetail.r_wrapping.HasValue())
                        {
                            if (objMallDetail.unit_price > 0)
                            {
                                var taxTmp = Convert.ToInt32(Math.Round(objMallDetail.unit_price * tax / in_tax));
                                var costTmp = objMallDetail.unit_price - taxTmp;

                                out_tax += taxTmp;
                                out_cost += costTmp;
                            }
                        }
                    }
                    break;

                // Amazon [Amazon]
                case EnumMallShopKbn.AMAZON:
                    break;
            }
        }
        #endregion

        #region 住所情報変換 [Convert address information]
        /// <summary>
        /// 住所情報変換 [Convert address information]
        /// </summary>
        /// <param name="address">住所 [Address]</param>
        /// <returns>住所情報 [Address information]</returns>
        public virtual AddressInfo ConvertAddressInfo(string address)
        {
            AddressInfo ret = default(AddressInfo);

            if (!string.IsNullOrEmpty(address) && address.Length > MALL_ADDRESS_LENGTH)
            {
                ret.address = address.Substring(0, MALL_ADDRESS_LENGTH);

                var taddress = address.Replace(ret.address, string.Empty);

                if (!string.IsNullOrEmpty(taddress) && taddress.Length > MALL_TADDRESS_LENGTH)
                {
                    ret.taddress = taddress.Substring(0, MALL_TADDRESS_LENGTH);
                    ret.maddress = taddress.Replace(ret.taddress, string.Empty);
                }
                else
                {
                    ret.taddress = taddress;
                }
            }
            else
            {
                ret.address = address;
            }

            return ret;
        }
        #endregion

        #region 都道府県変換 [Convert prefecture]
        /// <summary>
        /// 都道府県変換 [Convert prefecture]
        /// </summary>
        /// <param name="address">住所 [Address]</param>
        /// <param name="removed">都道府県除去住所 [Remove prefecture address]</param>
        /// <returns>都道府県 [Prefecture]</returns>
        public virtual int? ConvertPrefecture(string address, out string removed)
        {
            removed = address;

            if (string.IsNullOrEmpty(address))
            {
                return null;
            }

            return ErsMallFactory.ersMallCommonFactory.GetConvertPrefectureStgy().GetPrefecture(address, out removed);
        }
        #endregion

        #region 郵便番号変換 [Convert zip code]
        /// <summary>
        /// 郵便番号変換 [Convert zip code]
        /// </summary>
        /// <param name="zip_code">郵便番号 [Zip code]</param>
        /// <returns>郵便番号 [Zip code]</returns>
        public virtual string ConvertZipCode(string zip_code)
        {
            if (string.IsNullOrEmpty(zip_code))
            {
                return string.Empty;
            }

            var ret = zip_code.Trim();

            if (!ret.Contains("-") && ret.Length == 7)
            {
                return string.Format("{0}-{1}", ret.Substring(0, 3), ret.Substring(3, 4));
            }
            if (ret.Length > MALL_ZIP_LENGTH)
            {
                return ret.Substring(0, MALL_ZIP_LENGTH);
            }

            return ret;
        }
        #endregion

        #region 電話番号変換 [Convert phone number]
        /// <summary>
        /// 電話番号変換 [Convert phone number]
        /// </summary>
        /// <param name="phone_number">電話番号 [Phone number]</param>
        /// <returns>電話番号 [Phone number]</returns>
        public virtual string ConvertPhoneNumber(string phone_number)
        {
            if (string.IsNullOrEmpty(phone_number))
            {
                return string.Empty;
            }

            var ret = phone_number.Trim().Replace("-", string.Empty);

            if (ret.Length > MALL_TEL_LENGTH)
            {
                return ret.Substring(0, MALL_TEL_LENGTH);
            }

            return ret;
        }
        #endregion

        #region 支払い方法変換 [Convert payment method]
        /// <summary>
        /// 支払い方法変換 [Convert payment method]
        /// </summary>
        /// <param name="objMallOrder">モール伝票ヘッダ [The mall order header]</param>
        /// <returns>支払い方法 [Payment method]</returns>
        public EnumPaymentType ConvertPaymentMethod(ErsMallOrder objMallOrder)
        {
            if (objMallOrder.order_price == 0)
            {
                return EnumPaymentType.NON_NEEDED_PAYMENT;
            }

            switch (objMallOrder.mall_shop_kbn)            {
                // Yahoo! [Yahoo!]
                case EnumMallShopKbn.YAHOO:
                    switch (objMallOrder.payment_method)
                    {
                        case MALL_YAHOO_PAYMENT_METHOD_CREDIT_CARD:
                            return EnumPaymentType.CREDIT_CARD;

                        case MALL_YAHOO_PAYMENT_METHOD_COD:
                            return EnumPaymentType.CASH_ON_DELIVERY;

                        case MALL_YAHOO_PAYMENT_METHOD_YJPPOINT:
                            return EnumPaymentType.NON_NEEDED_PAYMENT;
                    }
                    break;

                // 楽天 [Rakuten]
                case EnumMallShopKbn.RAKUTEN:
                    switch (objMallOrder.payment_method)
                    {
                        case MALL_RAKUTEN_PAYMENT_METHOD_COD:
                            return EnumPaymentType.CASH_ON_DELIVERY;

                        case MALL_RAKUTEN_PAYMENT_METHOD_CREDIT_CARD:
                            return EnumPaymentType.CREDIT_CARD;
                    }
                    break;

                // Amazon [Amazon]
                case EnumMallShopKbn.AMAZON:
                    switch (objMallOrder.payment_method)
                    {
                        case MALL_AMAZON_PAYMENT_METHOD_OTHER:
                            return EnumPaymentType.CREDIT_CARD;

                        case MALL_AMAZON_PAYMENT_METHOD_COD:
                            return EnumPaymentType.CASH_ON_DELIVERY;
                    }
                    break;
            }

            return EnumPaymentType.NON_NEEDED_PAYMENT;
        }
        #endregion

        #region 配送方法変換 [Convert shipping information]
        /// <summary>
        /// 配送方法変換 [Convert shipping information]
        /// </summary>
        /// <param name="shopKbn">店舗タイプ [Shop type]</param>
        /// <param name="objMallOrderDetail">モール伝票ボディ [The mall order detail]</param>
        /// <returns>配送情報 [Shipping information]</returns>
        public virtual ShippingInfo ConvertShippingInfo(EnumMallShopKbn shopKbn, ErsMallOrderDetail objMallOrderDetail)
        {
            switch (shopKbn)
            {
                // Yahoo! [Yahoo!]
                case EnumMallShopKbn.YAHOO:
                    return this.ConvertShippingInfoYahoo(objMallOrderDetail);

                // 楽天 [Rakuten]
                case EnumMallShopKbn.RAKUTEN:
                    return this.ConvertShippingInfoRakuten(objMallOrderDetail);

                // Amazon [Amazon]
                case EnumMallShopKbn.AMAZON:
                    return this.ConvertShippingInfoAmazon(objMallOrderDetail);
            }

            return default(ShippingInfo);
        }

        /// <summary>
        /// 配送方法変換（楽天） [Convert shipping information (Rakuten)]
        /// </summary>
        /// <param name="objMallOrderDetail">モール伝票ボディ [The mall order detail]</param>
        /// <returns>配送情報 [Shipping information]</returns>
        protected ShippingInfo ConvertShippingInfoRakuten(ErsMallOrderDetail objMallOrderDetail)
        {
            ShippingInfo ret = default(ShippingInfo);

            if (!objMallOrderDetail.delivery_memo.HasValue())
            {
                return ret;
            }

            string key = "[配送日時指定:]";

            var target = objMallOrderDetail.delivery_memo.Substring(objMallOrderDetail.delivery_memo.IndexOf(key) + key.Length);
            var elements = target.Split(new string[] { "\r\n", "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);

            var service = ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService();

            DateTime? dateSenddate = null;
            int? intSendtime = null;
            IList<string> listMemo = new List<string>();

            foreach (var e in elements)
            {
                DateTime dateTmp;

                // 希望日 [Hope date]
                if (DateTime.TryParse(e, out dateTmp))
                {
                    dateSenddate = dateTmp;
                }
                else
                {
                    // 希望時間帯 [Hope time zone]
                    var tmp = service.GetIdFromString(ErsCommon.ConvertUnicodeCharacterForWin(e));

                    if (tmp != 0)
                    {
                        intSendtime = tmp;
                    }
                    // その他 [Other]
                    else
                    {
                        listMemo.Add(e);
                    }
                }
            }

            ret.senddate = dateSenddate;
            ret.sendtime = intSendtime;

            if (listMemo.Count > 0)
            {
                listMemo.Insert(0, SHIPPING_INFO_ITEM_NAME);
                ret.sendmemo = String.Join(Environment.NewLine, listMemo);
            }

            return ret;
        }

        /// <summary>
        /// 配送方法変換（Yahoo!） [Convert shipping information (Yahoo!)]
        /// </summary>
        /// <param name="objMallOrderDetail">モール伝票ボディ [The mall order detail]</param>
        /// <returns>配送情報 [Shipping information]</returns>
        protected ShippingInfo ConvertShippingInfoYahoo(ErsMallOrderDetail objMallOrderDetail)
        {
            ShippingInfo ret = default(ShippingInfo);

            var service = ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService();

            DateTime? dateSenddate = null;
            int? intSendtime = null;
            IList<string> listMemo = new List<string>();

            // 希望日 [Hope date]
            if (objMallOrderDetail.delivery_hope_date != null)
            {
                dateSenddate = objMallOrderDetail.delivery_hope_date;
            }

            // 希望時間帯 [Hope time zone]
            if (objMallOrderDetail.delivery_hope_time.HasValue())
            {
                var tmp = service.GetIdFromString(objMallOrderDetail.delivery_hope_time);

                if (tmp != 0)
                {
                    intSendtime = tmp;
                }
                else
                {
                    listMemo.Add(objMallOrderDetail.delivery_hope_time);
                }
            }

            // その他 [Other]
            if (objMallOrderDetail.delivery_memo.HasValue())
            {
                listMemo.Add(objMallOrderDetail.delivery_memo);
            }

            ret.senddate = dateSenddate;
            ret.sendtime = intSendtime;

            if (listMemo.Count > 0)
            {
                listMemo.Insert(0, SHIPPING_INFO_ITEM_NAME);
                ret.sendmemo = String.Join(Environment.NewLine, listMemo);
            }

            return ret;
        }

        /// <summary>
        /// 配送方法変換（Amazon） [Convert shipping information (Amazon)]
        /// </summary>
        /// <param name="objMallOrderDetail">モール伝票ボディ [The mall order detail]</param>
        /// <returns>配送情報 [Shipping information]</returns>
        protected ShippingInfo ConvertShippingInfoAmazon(ErsMallOrderDetail objMallOrderDetail)
        {
            ShippingInfo ret = default(ShippingInfo);

            var service = ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService();

            DateTime? dateSenddate = null;
            int? intSendtime = null;
            IList<string> listMemo = new List<string>();

            // 希望日 [Hope date]
            if (objMallOrderDetail.delivery_hope_date != null)
            {
                dateSenddate = objMallOrderDetail.delivery_hope_date;
            }

            // 希望時間帯 [Hope time zone]
            if (objMallOrderDetail.delivery_hope_time.HasValue())
            {
                var tmp = service.GetIdFromString(objMallOrderDetail.delivery_hope_time);

                if (tmp != 0)
                {
                    intSendtime = tmp;
                }
                else
                {
                    listMemo.Add(objMallOrderDetail.delivery_hope_time);
                }
            }

            // その他 [Other]
            if (objMallOrderDetail.delivery_memo.HasValue())
            {
                listMemo.Add(objMallOrderDetail.delivery_memo);
            }

            ret.senddate = dateSenddate;
            ret.sendtime = intSendtime;

            if (listMemo.Count > 0)
            {
                listMemo.Insert(0, SHIPPING_INFO_ITEM_NAME);
                ret.sendmemo = String.Join(Environment.NewLine, listMemo);
            }

            return ret;
        }
        #endregion

        #region コメント変換 [Convert comment]
        /// <summary>
        /// コメント変換 [Convert comment]
        /// </summary>
        /// <param name="objMallOrder">モール伝票ヘッダ [The mall order header]</param>
        /// <param name="objMallOrderDetail">モール伝票ボディ [The mall order detail]</param>
        /// <param name="shippingInfo">配送情報 [Shipping information]</param>
        /// <returns>コメント [Comment]</returns>
        public virtual string ConvertComment(ErsMallOrder objMallOrder, ErsMallOrderDetail objMallOrderDetail, ShippingInfo shippingInfo)
        {
            switch (objMallOrder.mall_shop_kbn)
            {
                // Yahoo! [Yahoo!]
                case EnumMallShopKbn.YAHOO:
                // Amazon [Amazon]
                case EnumMallShopKbn.AMAZON:
                    {
                        var listComment = new List<string>();

                        // 配送メモ [Delivery hope memo]
                        if (shippingInfo.sendmemo.HasValue())
                        {
                            listComment.Add(shippingInfo.sendmemo);
                        }

                        // 備考 [Remarks]
                        if (objMallOrder.remarks.HasValue())
                        {
                            listComment.Add(objMallOrder.remarks);
                        }

                        return listComment.Count > 0 ? String.Join(Environment.NewLine, listComment) : string.Empty;
                    }

                // 楽天 [Rakuten]
                case EnumMallShopKbn.RAKUTEN:
                    {
                        var listComment = new List<string>();

                        // クレジットカードエラー [Error of credit card]
                        if (objMallOrder.payment_method == MALL_RAKUTEN_PAYMENT_METHOD_CREDIT_CARD)
                        {
                            if (!this.IsValidCreditCardAuthorizationRakuten(objMallOrder))
                            {
                                listComment.Add(objMallOrder.r_card_info5);
                            }
                        }

                        // 配送メモ [Delivery hope memo]
                        if (shippingInfo.sendmemo.HasValue())
                        {
                            listComment.Add(shippingInfo.sendmemo);
                        }

                        // 備考 [Remarks]
                        if (objMallOrder.remarks.HasValue())
                        {
                            string tmp = objMallOrder.remarks;

                            // 備考に配送メモが含まれているので除去する [Remove delivery memo from remarks]
                            if (objMallOrderDetail.delivery_memo.HasValue())
                            {
                                tmp = tmp.Replace(objMallOrderDetail.delivery_memo, string.Empty);
                            }
                            if (tmp.HasValue())
                            {
                                listComment.Add(tmp);
                            }
                        }

                        return listComment.Count > 0 ? String.Join(Environment.NewLine, listComment) : string.Empty;
                    }
            }

            return string.Empty;
        }
        #endregion

        #region 配送コメント変換 [Convert shipping comment]
        /// <summary>
        /// 配送コメント変換 [Convert shipping comment]
        /// </summary>
        /// <param name="objMallOrder">モール伝票ヘッダ [The mall order header]</param>
        /// <param name="listMallDetail">モール伝票ボディリスト [The list of mall order detail]</param>
        /// <returns>配送コメント [Shipping comment]</returns>
        public virtual string ConvertShippingComment(ErsMallOrder objMallOrder, IList<ErsMallOrderDetail> listMallDetail)
        {
            switch (objMallOrder.mall_shop_kbn)
            {
                // Yahoo! [Yahoo!]
                case EnumMallShopKbn.YAHOO:
                    break;

                // 楽天 [Rakuten]
                case EnumMallShopKbn.RAKUTEN:
                    var listComment = new List<string>();

                    for (var i = 0; i < listMallDetail.Count; i++)
                    {
                        var objMallDetail = listMallDetail[i];

                        // 商品コードが無い＋ラッピング [item_code is null or empty + it is warapping]
                        if (!objMallDetail.ers_item_code.HasValue() && objMallDetail.r_wrapping.HasValue())
                        {
                            listComment.Add(objMallDetail.item_name);
                        }
                    }

                    if (listComment.Count > 0)
                    {
                        return ErsResources.GetMessage("107003", String.Join("、", listComment));
                    }
                    break;

                // Amazon [Amazon]
                case EnumMallShopKbn.AMAZON:
                    break;
            }

            return string.Empty;
        }
        #endregion

        #region 管理用メモ取得 [Get admin memo]
        /// <summary>
        /// 管理用メモ取得 [Get admin memo]
        /// </summary>
        /// <param name="objMallContainer">モール伝票コンテナ [The mall order container]</param>
        /// <param name="objHeader">伝票ヘッダ [Order header]</param>
        /// <returns>管理用メモ [Admin memo]</returns>
        public string GetAdminMemo(MallOrderContainer objMallContainer, ErsOrder objHeader)
        {
            // 受注確認理由取得 [Get the reason of order status confirmed]
            var orderStatusConfirmedReason = this.GetOrderStatusConfirmedReason(objMallContainer, objHeader);

            if (orderStatusConfirmedReason == null)
            {
                return null;
            }

            switch (orderStatusConfirmedReason)
            {
                case EnumOrderStatusConfirmedReason.AuthorizationError:
                    return ErsResources.GetMessage("107050");

                case EnumOrderStatusConfirmedReason.AddressError:
                    return ErsResources.GetMessage("107051");

                case EnumOrderStatusConfirmedReason.NonPaymentCod:
                    return ErsResources.GetMessage("107052");

                case EnumOrderStatusConfirmedReason.Mischief:
                    return ErsResources.GetMessage("107053", objMallContainer.objMallOrder.y_caution_reason);

                case EnumOrderStatusConfirmedReason.Overseas:
                    return ErsResources.GetMessage("107054");
            }

            return null;
        }
        #endregion

        #region 決済ステータス変換 [Convert payment status]
        /// <summary>
        /// 決済ステータス変換 [Convert payment status]
        /// </summary>
        /// <param name="objMallOrder">モール伝票ヘッダ [The mall order header]</param>
        /// <returns>決済ステータス [Payment status]</returns>
        public EnumOrderPaymentStatusType ConvertPaymentStatus(ErsMallOrder objMallOrder)
        {
            switch (objMallOrder.mall_shop_kbn)
            {
                // Yahoo! [Yahoo!]
                case EnumMallShopKbn.YAHOO:
                    if (objMallOrder.payment_method == MALL_YAHOO_PAYMENT_METHOD_CREDIT_CARD)
                    {
                        if (this.IsValidCreditCardAuthorizationYahoo(objMallOrder))
                        {
                            return EnumOrderPaymentStatusType.NOT_PAID;
                        }
                    }
                    break;

                // 楽天 [Rakuten]
                case EnumMallShopKbn.RAKUTEN:
                    if (objMallOrder.payment_method == MALL_RAKUTEN_PAYMENT_METHOD_CREDIT_CARD)
                    {
                        if (this.IsValidCreditCardAuthorizationRakuten(objMallOrder))
                        {
                            return EnumOrderPaymentStatusType.NOT_PAID;
                        }
                    }
                    break;

                // Amazon [Amazon]
                case EnumMallShopKbn.AMAZON:
                    if (objMallOrder.payment_method == MALL_AMAZON_PAYMENT_METHOD_CVS)
                    {
                        return EnumOrderPaymentStatusType.PAID;
                    }
                    break;
            }

            return EnumOrderPaymentStatusType.NOT_PAID;
        }

        /// <summary>
        /// 決済ステータス変換（Yahoo!） [Convert payment status (Yahoo!)]
        /// </summary>
        /// <param name="objMallOrder">モール伝票ヘッダ [The mall order header]</param>
        /// <returns>true : 与信取得済み [Authorized] ／ false : 与信未取得 [Not authorized]</returns>
        protected bool IsValidCreditCardAuthorizationYahoo(ErsMallOrder objMallOrder)
        {
            if (objMallOrder.y_payment_status == MALL_YAHOO_PAYMENT_STATUS_AUTH)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 決済ステータス変換（楽天） [Convert payment status (Rakuten)]
        /// </summary>
        /// <param name="objMallOrder">モール伝票ヘッダ [The mall order header]</param>
        /// <returns>true : 正常 [Valid] ／ false : 異常 [Invalid]</returns>
        protected bool IsValidCreditCardAuthorizationRakuten(ErsMallOrder objMallOrder)
        {
            // オーソリ済み or カード対象外＋0円 [Authorized or Not covered + \0]
            if (objMallOrder.r_card_info5.Contains(MALL_RAKUTEN_PAYMENT_STATUS_AUTH) ||
                (objMallOrder.r_card_info5.Contains(MALL_RAKUTEN_PAYMENT_STATUS_NOT_COVERED) && objMallOrder.order_price == 0))
            {
                return true;
            }

            return false;
        }
        #endregion

        #region 受注ステータス取得 [Get the order status]
        /// <summary>
        /// 受注ステータス取得 [Get the order status]
        /// </summary>
        /// <param name="objMallContainer">モール伝票コンテナ [The mall order container]</param>
        /// <param name="objHeader">伝票ヘッダ [Order header]</param>
        /// <returns>受注ステータス [The order status]</returns>
        public EnumOrderStatusType? GetOrderStatus(MallOrderContainer objMallContainer, ErsOrder objHeader)
        {
            // 受注確認理由取得 [Get the reason of order status confirmed]
            var orderStatusConfirmedReason = this.GetOrderStatusConfirmedReason(objMallContainer, objHeader);

            if (orderStatusConfirmedReason != null)
            {
                // 出荷待機
                return EnumOrderStatusType.DELIVER_WAITING;
            }

            return null;
        }

        /// <summary>
        /// 受注確認理由取得 [Get the reason of order status confirmed]
        /// </summary>
        /// <param name="objBranchContainer">支店伝票コンテナ [The branch order container]</param>
        /// <param name="objHeader">伝票ヘッダ [Order header]</param>
        /// <returns>受注確認理由 [The reason of order status confirmed]</returns>
        public EnumOrderStatusConfirmedReason? GetOrderStatusConfirmedReason(MallOrderContainer objMallContainer, ErsOrder objHeader)
        {
            var objMallDetail = objMallContainer.listMallOrderDetail.FirstOrDefault();

            switch (objHeader.mall_shop_kbn)
            {
                // Yahoo! [Yahoo!]
                case EnumMallShopKbn.YAHOO:
                    // いたずら [Mischief]
                    if (Convert.ToBoolean(objMallContainer.objMallOrder.y_caution))
                    {
                        // 4 : いたずら [Mischief]
                        return EnumOrderStatusConfirmedReason.Mischief;
                    }

                    // オーソリNG [Authorize failed]
                    if (this.ConvertPaymentMethod(objMallContainer.objMallOrder) == EnumPaymentType.CREDIT_CARD)
                    {
                        if (!this.IsValidCreditCardAuthorizationYahoo(objMallContainer.objMallOrder))
                        {
                            // 1 : オーソリエラー [Authorization error]
                            return EnumOrderStatusConfirmedReason.AuthorizationError;
                        }
                    }

                    // 都道府県判定不可 [Impossible judgment prefecture]
                    if (objHeader.pref == null || objHeader.add_pref == null)
                    {
                        // 2 : 住所エラー [Address error]
                        return EnumOrderStatusConfirmedReason.AddressError;
                    }

                    // 海外郵便番号
                    if (objHeader.pref == (int)EnumPrefecture.OVERSEAS || objHeader.add_pref == (int)EnumPrefecture.OVERSEAS)
                    {
                        // 5 : 海外
                        return EnumOrderStatusConfirmedReason.Overseas;
                    }

                    // 郵便番号文字数オーバー [Zip ia over length]
                    if (objMallDetail.ship_zipcode.HasValue() && objMallDetail.ship_zipcode.Length > MALL_ZIP_LENGTH)
                    {
                        // 2 : 住所エラー [Address error]
                        return EnumOrderStatusConfirmedReason.AddressError;
                    }
                    // 電話番号文字数オーバー [Tel ia over length]
                    if (objMallDetail.ship_phone.HasValue() && objMallDetail.ship_phone.Length > MALL_TEL_LENGTH)
                    {
                        // 2 : 住所エラー [Address error]
                        return EnumOrderStatusConfirmedReason.AddressError;
                    }

                    // 住所３文字数オーバー [Address 2 is over length]
                    if ((objHeader.maddress.HasValue() && objHeader.maddress.Length > MALL_MADDRESS_LENGTH) ||
                        (objHeader.add_maddress.HasValue() && objHeader.add_maddress.Length > MALL_MADDRESS_LENGTH))
                    {
                        // 2 : 住所エラー [Address error]
                        return EnumOrderStatusConfirmedReason.AddressError;
                    }

                    // 代引きかつ０円 [Payment method is Cash on delivery and payment price is zero]
                    if (objHeader.pay == EnumPaymentType.CASH_ON_DELIVERY && objHeader.total == 0)
                    {
                        // 3 : 代引き＋０円 [Non payment on COD]
                        return EnumOrderStatusConfirmedReason.NonPaymentCod;
                    }

                    break;

                // 楽天 [Rakuten]
                case EnumMallShopKbn.RAKUTEN:
                    // オーソリNG [Authorize failed]
                    if (objMallContainer.objMallOrder.payment_method == MALL_RAKUTEN_PAYMENT_METHOD_CREDIT_CARD)
                    {
                        if (!this.IsValidCreditCardAuthorizationRakuten(objMallContainer.objMallOrder))
                        {
                            // 1 : オーソリエラー [Authorization error]
                            return EnumOrderStatusConfirmedReason.AuthorizationError;
                        }
                    }

                    // 都道府県判定不可 [Impossible judgment prefecture]
                    if (objHeader.pref == null || objHeader.add_pref == null)
                    {
                        // 2 : 住所エラー [Address error]
                        return EnumOrderStatusConfirmedReason.AddressError;
                    }

                    // 海外郵便番号
                    if (objHeader.pref == (int)EnumPrefecture.OVERSEAS || objHeader.add_pref == (int)EnumPrefecture.OVERSEAS)
                    {
                        // 5 : 海外
                        return EnumOrderStatusConfirmedReason.Overseas;
                    }

                    // 郵便番号文字数オーバー [Zip ia over length]
                    if (objMallDetail.ship_zipcode.HasValue() && objMallDetail.ship_zipcode.Length > MALL_ZIP_LENGTH)
                    {
                        // 2 : 住所エラー [Address error]
                        return EnumOrderStatusConfirmedReason.AddressError;
                    }
                    // 電話番号文字数オーバー [Tel ia over length]
                    if (objMallDetail.ship_phone.HasValue() && objMallDetail.ship_phone.Length > MALL_TEL_LENGTH)
                    {
                        // 2 : 住所エラー [Address error]
                        return EnumOrderStatusConfirmedReason.AddressError;
                    }

                    // 住所３文字数オーバー [Address 2 is over length]
                    if ((objHeader.maddress.HasValue() && objHeader.maddress.Length > MALL_MADDRESS_LENGTH) ||
                        (objHeader.add_maddress.HasValue() && objHeader.add_maddress.Length > MALL_MADDRESS_LENGTH))
                    {
                        // 2 : 住所エラー [Address error]
                        return EnumOrderStatusConfirmedReason.AddressError;
                    }

                    // 代引きかつ０円 [Payment method is Cash on delivery and payment price is zero]
                    if (objHeader.pay == EnumPaymentType.CASH_ON_DELIVERY && objHeader.total == 0)
                    {
                        // 3 : 代引き＋０円 [Non payment on COD]
                        return EnumOrderStatusConfirmedReason.NonPaymentCod;
                    }

                    break;

                // Amazon [Amazon]
                case EnumMallShopKbn.AMAZON:
                    // 都道府県判定不可 [Impossible judgment prefecture]
                    if (objHeader.add_pref == null)
                    {
                        // 2 : 住所エラー [Address error]
                        return EnumOrderStatusConfirmedReason.AddressError;
                    }

                    // 海外郵便番号
                    if (objHeader.add_pref == (int)EnumPrefecture.OVERSEAS)
                    {
                        // 5 : 海外
                        return EnumOrderStatusConfirmedReason.Overseas;
                    }

                    // 郵便番号文字数オーバー [Zip ia over length]
                    if (objMallDetail.ship_zipcode.HasValue() && objMallDetail.ship_zipcode.Length > MALL_ZIP_LENGTH)
                    {
                        // 2 : 住所エラー [Address error]
                        return EnumOrderStatusConfirmedReason.AddressError;
                    }
                    // 電話番号文字数オーバー [Tel ia over length]
                    if (objMallDetail.ship_phone.HasValue() && objMallDetail.ship_phone.Length > MALL_TEL_LENGTH)
                    {
                        // 2 : 住所エラー [Address error]
                        return EnumOrderStatusConfirmedReason.AddressError;
                    }

                    // 住所３文字数オーバー [Address 2 is over length]
                    if (objHeader.add_maddress.HasValue() && objHeader.add_maddress.Length > MALL_MADDRESS_LENGTH)
                    {
                        // 2 : 住所エラー [Address error]
                        return EnumOrderStatusConfirmedReason.AddressError;
                    }
                    break;
            }

            return null;
        }
        #endregion
    }
}
