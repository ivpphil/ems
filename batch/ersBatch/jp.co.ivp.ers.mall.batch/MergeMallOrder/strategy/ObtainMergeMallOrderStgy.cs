﻿using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mall.mall_order;

namespace jp.co.ivp.ers.mall.batch.MergeMallOrder.strategy
{
    /// <summary>
    /// マージモール伝票取得 [Get the mall order for merge]
    /// </summary>
    public class ObtainMergeMallOrderStgy
    {
        /// <summary>
        /// マージモール伝票取得 [Get the mall order for merge]
        /// </summary>
        /// <param name="dateTimeParam">日時パラメータ [Datetime parameter]</param>
        /// <returns>モール伝票リスト [List of mall order for merge]</returns>
        public IList<ErsMallOrder> Obtain(ErsCommonStruct.DateTimeStartEnd dateTimeParam)
        {
            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderRepository();
            var criteria = this.GetCriteria(dateTimeParam);

            if (repository.GetRecordCount(criteria) == 0)
            {
                return null;
            }

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            return repository.Find(criteria);
        }

        /// <summary>
        /// クライテリア取得 [Get the criteria]
        /// </summary>
        /// <param name="dateTimeParam">日時パラメータ [Datetime parameter]</param>
        /// <returns>クライテリア [Criteria]</returns>
        protected virtual ErsMallOrderCriteria GetCriteria(ErsCommonStruct.DateTimeStartEnd dateTimeParam)
        {
            var criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderCriteria();

            // 要マージ [Need merge]
            criteria.merge_status = EnumMergeStatus.NeedMerge;

            if (dateTimeParam.dateFrom != null)
            {
                criteria.order_date_from = dateTimeParam.dateFrom.Value;
            }
            if (dateTimeParam.dateTo != null)
            {
                criteria.order_date_to = dateTimeParam.dateTo.Value;
            }

            return criteria;
        }
    }
}
