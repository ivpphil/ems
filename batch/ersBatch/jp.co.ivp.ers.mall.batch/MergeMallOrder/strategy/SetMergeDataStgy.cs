﻿using System;
using System.Linq;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.mall_order;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.mall.batch.MergeMallOrder.strategy
{
    /// <summary>
    /// マージデータセット [Set the data for merge]
    /// </summary>
    public class SetMergeDataStgy
    {
        /// <summary>
        /// マージデータセット [Set the data for merge]
        /// </summary>
        /// <param name="objContainer">伝票コンテナ [The order container]</param>
        /// <param name="objMallContainer">モール伝票コンテナ [The mall order container]</param>
        /// <param name="isRegister">登録 [Register]</param>
        /// <returns>伝票コンテナ [The order container]</returns>
        public ErsOrderContainer SetMergeData(ErsOrderContainer objContainer, MallOrderContainer objMallContainer)
        {
            var convertService = new MallParameterConvertService();

            string base_d_no = string.Empty;
            string d_no = string.Empty;

            objContainer.OrderRecords = new Dictionary<string, ErsOrderRecord>();
            base_d_no = ErsFactory.ersOrderFactory.GetErsOrderRepository().GetNextD_no();
            d_no = base_d_no + "-01";

            // ヘッダ [Header]
            var objHeader = ErsFactory.ersOrderFactory.GetErsOrder();

            // 伝票ヘッダデータセット [Set the order header data]
            objHeader = this.SetHeaderData(objHeader, objMallContainer);

            objHeader.intime = objMallContainer.objMallOrder.order_date;
            objHeader.ransu = ErsFactory.ersSessionStateFactory.GetObtainRansuStgy().CreateNewRansu();
            objHeader.d_no = d_no;
            objHeader.base_d_no = base_d_no;

            objContainer.OrderHeader = objHeader;

            // 受注ステータス取得 [Get the order status]
            EnumOrderStatusType? orderStatus = convertService.GetOrderStatus(objMallContainer, objContainer.OrderHeader);

            // ボディ [Detail]
            var createKeyStgy = ErsFactory.ersOrderFactory.GetCreateOrderRecordKeyStgy();

            for (var i = 0; i < objMallContainer.listMallOrderDetail.Count; i++)
            {
                var objMallDetail = objMallContainer.listMallOrderDetail[i];

                // 商品コードが無い場合は対象にしない [Not target when item_code is null or empty]
                if (!objMallDetail.ers_item_code.HasValue())
                {
                    continue;
                }

                ErsOrderRecord objDetail = null;

                objDetail = ErsFactory.ersOrderFactory.GetErsOrderRecord();

                // 伝票ボディデータセット [Set the order detail data]
                objDetail = this.SetDetailData(objContainer.OrderHeader, objDetail, objMallDetail, objMallContainer);

                objDetail.intime = objMallContainer.objMallOrder.order_date;
                objDetail.order_status = orderStatus ?? EnumOrderStatusType.NEW_ORDER;
                objDetail.d_no = d_no;

                var key = createKeyStgy.GetKey(objDetail, objContainer.OrderHeader.senddate);

                if (!objContainer.OrderRecords.ContainsKey(key))
                {
                    objContainer.OrderRecords[key] = objDetail;
                }
                else
                {
                    objContainer.OrderRecords[key] = this.AddDupulicatedOrderRecord(objContainer.OrderRecords[key], objDetail);
                }
            }

            return objContainer;
        }

        #region 伝票ヘッダデータセット [Set the order header data]
        /// <summary>
        /// 伝票ヘッダデータセット [Set the order header data]
        /// </summary>
        /// <param name="objHeader">伝票ヘッダ [The order header]</param>
        /// <param name="objMallContainer">モール伝票コンテナ [The mall order container]</param>
        /// <returns>伝票ヘッダ [The order header]</returns>
        protected virtual ErsOrder SetHeaderData(ErsOrder objHeader, MallOrderContainer objMallContainer)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var convertService = new MallParameterConvertService();
            var objMallHeader = objMallContainer.objMallOrder;
            var objMallDetail = objMallContainer.listMallOrderDetail.First();

            float in_tax = setup.tax + 100;
            float tax = setup.tax;

            objHeader.site_id = objMallHeader.site_id;
            objHeader.mall_shop_kbn = objMallHeader.mall_shop_kbn;
            objHeader.mall_d_no = objMallHeader.order_code;

            objHeader.pay = convertService.ConvertPaymentMethod(objMallHeader);

            // 金額 [Price]
            objHeader.total = objMallHeader.order_price;

            int carriage_tax = Convert.ToInt32(Math.Round(objMallHeader.delivery_cost * tax / in_tax));
            objHeader.carriage = objMallHeader.delivery_cost - carriage_tax;

            int etc_tax = Convert.ToInt32(Math.Round(objMallHeader.commission * tax / in_tax));
            objHeader.etc = objMallHeader.commission - etc_tax;

            int wrapping_tax = 0;
            int wrapping_cost = 0;
            convertService.GetWrappingPrice(objMallHeader, objMallContainer.listMallOrderDetail, out wrapping_tax, out wrapping_cost);

            int wrap_tax = Convert.ToInt32(Math.Round(objMallHeader.giftwrap_cost * tax / in_tax)) + wrapping_tax;
            objHeader.wrap_cost = objMallHeader.giftwrap_cost + wrapping_cost - wrap_tax;

            objHeader.p_service = objMallHeader.use_point;
            objHeader.coupon_discount = objMallHeader.discount;
            objHeader.tax = objMallHeader.order_tax + carriage_tax + etc_tax + wrap_tax;

            objHeader.subtotal = objHeader.total - objHeader.tax - objHeader.carriage - objHeader.etc - objHeader.wrap_cost + objHeader.p_service + objHeader.coupon_discount;

            var shippingInfo = convertService.ConvertShippingInfo(objMallHeader.mall_shop_kbn.Value, objMallDetail);
            objHeader.senddate = shippingInfo.senddate;
            objHeader.sendtime = shippingInfo.sendtime;

            objHeader.memo = convertService.ConvertComment(objMallHeader, objMallDetail, shippingInfo);

            objHeader.lname = objMallHeader.order_name;
            objHeader.fname = string.Empty;
            objHeader.lnamek = objMallHeader.order_kana;
            objHeader.zip = convertService.ConvertZipCode(objMallHeader.order_zipcode);

            string address = null;
            objHeader.pref = convertService.ConvertPrefecture(objMallHeader.order_address, out address);

            var isOverseas = ErsFactory.ersCommonFactory.GetIsOverseasZipSpec().IsOverseas(objHeader.zip);
            if (isOverseas && !objHeader.pref.HasValue)
            {
                objHeader.pref = (int)EnumPrefecture.OVERSEAS;
            }

            var addressInfo = convertService.ConvertAddressInfo(address);
            objHeader.address = addressInfo.address.HasValue() ? addressInfo.address : string.Empty;
            objHeader.taddress = addressInfo.taddress;
            objHeader.maddress = addressInfo.maddress;

            objHeader.tel = convertService.ConvertPhoneNumber(objMallHeader.order_phone);
            objHeader.email = objMallHeader.order_email;

            // 配送先 [Delivery destination]
            objHeader.send = EnumSendTo.ANOTHER_ADDRESS;

            // 別住所に必ず配送先を入れるようにする
            objHeader.add_lname = objMallDetail.ship_name;
            objHeader.add_lnamek = string.Empty;
            objHeader.add_zip = convertService.ConvertZipCode(objMallDetail.ship_zipcode);

            string add_address = null;
            objHeader.add_pref = convertService.ConvertPrefecture(objMallDetail.ship_address, out add_address);

            var isOverseasAddZip = ErsFactory.ersCommonFactory.GetIsOverseasZipSpec().IsOverseas(objHeader.add_zip);
            if (isOverseasAddZip && !objHeader.add_pref.HasValue)
            {
                objHeader.add_pref = (int)EnumPrefecture.OVERSEAS;
            }

            var addAddressInfo = convertService.ConvertAddressInfo(add_address);
            objHeader.add_address = addAddressInfo.address;
            objHeader.add_taddress = addAddressInfo.taddress;
            objHeader.add_maddress = addAddressInfo.maddress;

            objHeader.add_tel = convertService.ConvertPhoneNumber(objMallDetail.ship_phone);

            //objHeader.memo2 = null;

            objHeader.pm_flg = EnumPmFlg.Mall;

            objHeader.order_payment_status = convertService.ConvertPaymentStatus(objMallHeader);

            if (objHeader.order_payment_status == EnumOrderPaymentStatusType.PAID)
            {
                objHeader.paid_price = objHeader.total;
                objHeader.paid_date = DateTime.Now;
            }

            objHeader.mall_order_status = objHeader.mall_order_status ?? EnumMallOrderStatus.New;

            objHeader.memo3 = convertService.GetAdminMemo(objMallContainer, objHeader);

            return objHeader;
        }
        #endregion

        #region 伝票ボディデータセット [Set the order detail data]
        /// <summary>
        /// 伝票ボディデータセット [Set the order detail data]
        /// </summary>
        /// <param name="objHeader">伝票ヘッダ [The order header]</param>
        /// <param name="objDetail">伝票ボディ [The order detail]</param>
        /// <param name="objMallDetail">モール伝票ボディ [The mall order detail]</param>
        /// <param name="objMallContainer">モール伝票コンテナ [The mall order container]</param>
        /// <returns>伝票ボディ [The order detail]</returns>
        protected virtual ErsOrderRecord SetDetailData(ErsOrder objHeader, ErsOrderRecord objDetail, ErsMallOrderDetail objMallDetail, MallOrderContainer objMallContainer)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var convertService = new MallParameterConvertService();

            float in_tax = setup.tax + 100;
            float tax = setup.tax;

            // 商品情報取得 [Get the merchandise]
            var merchandise = this.GetMerchandise(objMallDetail.ers_item_code);

            objDetail.scode = objMallDetail.ers_item_code;
            objDetail.sname = objMallDetail.item_name;

            objDetail.amount = objMallDetail.quantity;

            // 金額 [Price]
            int price_tax = Convert.ToInt32(Math.Floor(objMallDetail.unit_price * tax / in_tax));
            var unit_price = objMallDetail.unit_price - price_tax;
            objDetail.price = unit_price;
            objDetail.price2 = objDetail.price;
            objDetail.total = unit_price * objDetail.amount;
            objDetail.point = 0;

            // 商品情報 [Merchandise]
            if (merchandise != null)
            {
                objDetail.s_sale_ptn = merchandise.s_sale_ptn;
                objDetail.gcode = merchandise.gcode;
                objDetail.gname = merchandise.gname;
                objDetail.m_gname = merchandise.m_gname;
                objDetail.m_sname = merchandise.m_sname;
                objDetail.date_from = merchandise.date_from;
                objDetail.date_to = merchandise.date_to;
                objDetail.cate1 = merchandise.cate1;
                objDetail.cate2 = merchandise.cate2;
                objDetail.cate3 = merchandise.cate3;
                objDetail.cate4 = merchandise.cate4;
                objDetail.cate5 = merchandise.cate5;
                objDetail.jancode = merchandise.jancode;
                objDetail.attribute1 = merchandise.attribute1;
                objDetail.attribute2 = merchandise.attribute2;
                objDetail.mixed_group_code = merchandise.mixed_group_code;
                objDetail.max_purchase_count = merchandise.max_purchase_count;
                objDetail.set_flg = merchandise.set_flg;
                objDetail.carriage_cost_type = merchandise.carriage_cost_type;
                objDetail.plural_order_type = merchandise.plural_order_type;
            }

            objDetail.order_type = EnumOrderType.Usually;

            objDetail.shipping_memo = convertService.ConvertShippingComment(objMallContainer.objMallOrder, objMallContainer.listMallOrderDetail);

            objDetail.mall_item_id = objMallDetail.a_item_id;
            objDetail.mall_quantity = objMallDetail.quantity;

            return objDetail;
        }

        /// <summary>
        /// 商品情報取得 [Get the merchandise]
        /// </summary>
        /// <param name="scode">商品コード [Product code]</param>
        /// <returns>商品情報 [The merchandise]</returns>
        protected virtual ErsMerchandise GetMerchandise(string scode)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();

            criteria.scode = scode;

            var listFind = repository.FindGroupBaseItemList(criteria);

            return listFind.Count > 0 ? listFind[0] : null;
        }

        /// <summary>
        /// 重複伝票明細加算 [Add publicated order detail to detail]
        /// </summary>
        /// <param name="objOrderDetail">対象レコード [Target order detail]</param>
        /// <param name="objOrderDetailAdd">追加レコード [Additional order detail]</param>
        /// <returns>加算後明細 [Added order detail]</returns>
        protected virtual ErsOrderRecord AddDupulicatedOrderRecord(ErsOrderRecord objOrderDetail, ErsOrderRecord objOrderDetailAdd)
        {
            objOrderDetail.amount += objOrderDetailAdd.amount;

            objOrderDetail.total += objOrderDetailAdd.total;
            objOrderDetail.point += objOrderDetailAdd.point;

            return objOrderDetail;
        }
        #endregion
    }
}
