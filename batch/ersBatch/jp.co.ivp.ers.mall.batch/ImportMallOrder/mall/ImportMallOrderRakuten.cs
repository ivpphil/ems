﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.api.order_status;

namespace jp.co.ivp.ers.mall.batch.ImportMallOrder.mall
{
    /// <summary>
    /// モール伝票取り込みクラス（楽天） [Class for import mall order (Rakuten)]
    /// </summary>
    public class ImportMallOrderRakuten
        : ImportMallOrderBase
    {
        #region 楽天決済ステータス [Rakuten payment status]
        /// <summary>
        /// オーソリ処理受付中 [Accept authorization]
        /// </summary>
        public const string MALL_RAKUTEN_PAYMENT_STATUS_ACCEPT_AUTH = "オーソリ処理受付中";
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="getStatus">取得ステータス [Get status]</param>
        /// <param name="updateStatus">更新ステータス [Update status]</param>
        /// <param name="isReverseImport">逆取込みフラグ [Is reverse import]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <param name="listSpecifiedOrderCode">指定受注番号リスト [The list of specified order code]</param>
        public ImportMallOrderRakuten(int? siteId, EnumMallShopKbn? shopKbn, EnumMallOrderStatus? getStatus, EnumMallOrderStatus? updateStatus, bool isReverseImport, DateTime dateExecute, IList<string> listSpecifiedOrderCode)
            : base(siteId, shopKbn, getStatus, updateStatus, isReverseImport, dateExecute, listSpecifiedOrderCode)
        {
            var paramApi = ErsMallFactory.ersMallAPIFactory.GetGetOrderInfosAPIParamBase();

            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();

            // APIパラメータ設定 [Set the parameters for API]
            paramApi.shop_id = mallSetup.GetShopId(siteId.Value);
            paramApi.shop_name = mallSetup.GetShopName(siteId.Value);

            // 取り込み日時取得 [Get datetime for import]
            var importDateTime = this.ObtainImportDateTime(siteId, dateExecute);

            paramApi.search_date_from = importDateTime.dateFrom;
            paramApi.search_date_to = importDateTime.dateTo;

            // API設定 [Set the API]
            this.apiGetOrder = ErsMallFactory.ersMallAPIFactory.GetGetOrderInfosAPIRakuten(paramApi);


            // クラスタイプ設定 [Set the class type]
            this.typeOrder = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderRakuten().GetType();
            this.typeOrderDetail = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderDetailRakuten().GetType();
        }
        #endregion

        #region 取り込み結果クラス変換 [Convert to the class of import results]
        /// <summary>
        /// 取り込み対象チェック [Check import target]
        /// </summary>
        /// <param name="dicOrder">伝票データ [Data of order]</param>
        /// <returns>true : 対象 [Target] ／ false : 非対象 [Not target]</returns>
        protected override bool IsImportTarget(Dictionary<string, object> dicOrder)
        {
            var result = base.IsImportTarget(dicOrder);

            if (result)
            {
                var dicOther = (Dictionary<string, object>)dicOrder["other"];

                if (dicOther.ContainsKey("card_info5"))
                {
                    var strTmp = Convert.ToString(dicOther["card_info5"]);

                    // オーソリ処理受付中 [Accept authorization]
                    if (strTmp.HasValue() && strTmp.Contains(MALL_RAKUTEN_PAYMENT_STATUS_ACCEPT_AUTH))
                    {
                        // 取り込み管理は登録しない [Don't register order import]
                        this.doRegisterOrderImport = false;
                        return false;
                    }
                }
            }

            return result;
        }
        #endregion

        #region ステータス変更 [Change status]
        /// <summary>
        /// ステータス変更API取得 [Get the API for change status]
        /// </summary>
        /// <returns>ステータス変更API [The API for change status]</returns>
        protected override ChangeOrderStatusAPIBase GetChangeOrderStatusAPI()
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIParamRakuten();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);
            apiParam.shop_name = mallSetup.GetShopName(this.siteId.Value);
            apiParam.successVerify = ErsMallFactory.ersMallUtilityFactory.getSetup().mall_order_update_status_verify;

            return ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIRakuten(apiParam);
        }
        #endregion
    }
}
