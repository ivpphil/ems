﻿using System;
using System.Collections.Generic;
using com.hunglead.harc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.api;
using jp.co.ivp.ers.mall.api.order;
using jp.co.ivp.ers.mall.api.order_status;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall.batch.ImportMallOrder.strategy;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mall.mall_order;

namespace jp.co.ivp.ers.mall.batch.ImportMallOrder.mall
{
    /// <summary>
    /// モール伝票取り込みクラス（Amazon） [Class for import mall order (Amazon)]
    /// </summary>
    public class ImportMallOrderAmazon
        : ImportMallOrderBase
    {
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="getStatus">取得ステータス [Get status]</param>
        /// <param name="updateStatus">更新ステータス [Update status]</param>
        /// <param name="isReverseImport">逆取込みフラグ [Is reverse import]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <param name="listSpecifiedOrderCode">指定受注番号リスト [The list of specified order code]</param>
        public ImportMallOrderAmazon(int? siteId, EnumMallShopKbn? shopKbn, EnumMallOrderStatus? getStatus, EnumMallOrderStatus? updateStatus, bool isReverseImport, DateTime dateExecute, IList<string> listSpecifiedOrderCode)
            : base(siteId, shopKbn, getStatus, updateStatus, isReverseImport, dateExecute, listSpecifiedOrderCode)
        {
            var paramApi = ErsMallFactory.ersMallAPIFactory.GetGetOrderInfosAPIParamAmazon();

            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();

            // APIパラメータ設定 [Set the parameters for API]
            paramApi.shop_id = mallSetup.GetShopId(siteId.Value);
            paramApi.shop_name = mallSetup.GetShopName(siteId.Value);

            // 取り込み日時取得 [Get datetime for import]
            var importDateTime = this.ObtainImportDateTime(siteId, dateExecute);

            paramApi.fromLastUpdateDate = importDateTime.dateFrom;
            paramApi.toLastUpdateDate = importDateTime.dateTo;

            // API設定 [Set the API]
            this.apiGetOrder = ErsMallFactory.ersMallAPIFactory.GetGetOrderInfosAPIAmazon(paramApi);


            // クラスタイプ設定 [Set the class type]
            this.typeOrder = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderAmazon().GetType();
            this.typeOrderDetail = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderDetailAmazon().GetType();
        }
        #endregion

        #region 取り込み日時取得 [Get datetime for import]
        /// <summary>
        /// 取り込み日時取得 [Get datetime for import]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="dateToday">今日の日時 [Today's datetime]</param>
        /// <returns>取り込み日時パラメータ [Parameter for import datetime]</returns>
        protected override ErsCommonStruct.DateTimeStartEnd ObtainImportDateTime(int? siteId, DateTime dateToday)
        {
            var spec = ErsMallFactory.ersMallOrderFactory.GetOrderDateParamSearchSpecification();
            var criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderImportCriteria();

            criteria.site_id = siteId;
            criteria.imported = EnumImported.Imported;

            ErsCommonStruct.DateTimeStartEnd retParam = default(ErsCommonStruct.DateTimeStartEnd);

            // 存在確認 [Check exists]
            if (spec.GetCountData(criteria) > 0)
            {
                criteria.SetOrderByImportTime(Criteria.OrderBy.ORDER_BY_DESC);

                var listFind = spec.GetSearchData(criteria);

                // 取得した日時を指定 [Specify get day]
                retParam.dateFrom = Convert.ToDateTime(listFind[0]["import_time"]);
            }
            else
            {
                // 実行日時を指定 [Specify execute day]
                retParam.dateFrom = Convert.ToDateTime(dateToday.ToString("yyyy/MM/dd 00:00:00"));
            }

            retParam.dateTo = dateToday.AddMinutes(-2);

            return retParam;
        }
        #endregion

        #region 取り込み結果取り込み [Import the result of import]
        /// <summary>
        /// 取り込み結果取り込み [Import the result of import]
        /// </summary>
        public override void ImportResult()
        {
            foreach (var result in this.listMallOrdersResult)
            {
                // 存在している [Exists data]
                if (result.isExists)
                {
                    // 差分なし [Not exists diff]
                    if (result.diff == null)
                    {
                        continue;
                    }
                    // 最終更新日のみ差分あり [Diff is last_update_date only]
                    else
                    {
                        var doImport = true;

                        // ヘッダ [Header]
                        if (result.diff.listColsChangedHeader != null &&
                            result.diff.listColsChangedHeader.Count == 1 &&
                            result.diff.listColsChangedHeader.Contains("a_last_update_date"))
                        {
                            // 明細 [Detail]
                            if ((result.diff.listDelItemCodesDetail == null || (result.diff.listDelItemCodesDetail != null && result.diff.listDelItemCodesDetail.Count == 0)) &&
                                (result.diff.listAddItemCodesDetail == null || (result.diff.listAddItemCodesDetail != null && result.diff.listAddItemCodesDetail.Count == 0)) &&
                                (result.diff.dicListColsChangedDetail == null || result.diff.dicListColsChangedDetail != null && result.diff.dicListColsChangedDetail.Count == 0))
                            {
                                doImport = false;
                            }
                        }

                        if (!doImport)
                        {
                            continue;
                        }
                    }
                }

                // モール伝票取り込み結果取り込み [Import the result of import mall order]
                (new ImportMallOrderResultStgy()).Import(result);
            }
        }
        #endregion

        #region 在庫更新 [Update stock]
        /// <summary>
        /// 在庫更新 [Update stock]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        public override void UpdateStock(HarcApiRequest request)
        {
            // 在庫更新無効 [Disable update stock]
            if (!ErsMallFactory.ersMallBatchFactory.getSetup().enableAmazonImportStock)
            {
                return;
            }

            // 取り込み結果無し [No need for update status OR No result of import]
            if (this.listMallOrdersResult.Count == 0)
            {
                return;
            }

            // 在庫更新API用パラメータ取得（差分から） [Get the API parameter for update stock (from Diff)]
            var listParam = (new ObtainUpdateStockApiParamFromDiffStgy()).Obtain(this.listMallOrdersResult, this.shopKbn);

            if (listParam == null || (listParam != null && listParam.Count == 0))
            {
                return;
            }

            try
            {
                // モール在庫更新 [Update stock for mall]
                var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);

                if (ret.HasValue())
                {
                    var listScode = this.GetUpdateStockScodeList(listParam);
                    this.errorLog += ErsResources.GetMessage("100002", this.siteId, (int)this.shopKbn, ret);
                }
            }
            catch (APIFailedException e)
            {
                var listScode = this.GetUpdateStockScodeList(listParam);
                this.errorLog += ErsResources.GetMessage("100001", this.siteId, (int)this.shopKbn, String.Join(", ", listScode), e.ToString());
            }
        }

        /// <summary>
        /// 在庫更新商品コードリスト取得 [Get the list of scode for Update stock]
        /// </summary>
        /// <param name="listParam">在庫更新API用パラメータ [The API parameter for update stock]</param>
        /// <returns>在庫更新商品コードリスト [The list of scode for Update stock]</returns>
        protected IList<string> GetUpdateStockScodeList(IList<UpdateStockParam> listParam)
        {
            IList<string> listRet = new List<string>();

            foreach (var param in listParam)
            {
                listRet.Add(param.productCode);
            }

            return listRet;
        }
        #endregion

        #region ステータス変更 [Change status]
        /// <summary>
        /// ステータス変更API取得 [Get the API for change status]
        /// </summary>
        /// <returns>ステータス変更API [The API for change status]</returns>
        protected override ChangeOrderStatusAPIBase GetChangeOrderStatusAPI()
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIParamBase();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);
            apiParam.shop_name = mallSetup.GetShopName(this.siteId.Value);

            return ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIAmazon(apiParam);
        }
        #endregion

        #region マージステータス更新 [Update merge status]
        /// <summary>
        /// マージステータス取得 [Get merge status]
        /// </summary>
        /// <param name="result">取り込み結果 [Result of import]</param>
        /// <returns>マージステータス [Merge status]</returns>
        protected override EnumMergeStatus? GetMergeStatus(MallOrderResult result)
        {
            EnumMallOrderStatusAmazon status;

            if (!Enum.TryParse(result.objMallOrder.order_status, out status))
            {
                // 要マージ [Need merge]
                return EnumMergeStatus.NeedMerge;
            }

            // 存在していない [Doesn't exists]
            if (!result.isExists)
            {
                switch (status)
                {
                    // 保留 [Pending]
                    case EnumMallOrderStatusAmazon.Pending:
                        // マージ準備中 [Ready]
                        return EnumMergeStatus.Ready;

                    // キャンセル [Canceled]
                    case EnumMallOrderStatusAmazon.Canceled:
                        // マージ不要 [No need merge]
                        return EnumMergeStatus.Merged;

                    // 出荷済み [Shipped]
                    case EnumMallOrderStatusAmazon.Shipped:
                        // マージ不要 [No need merge]
                        return EnumMergeStatus.Merged;

                    default:
                        // 要マージ [Need merge]
                        return EnumMergeStatus.NeedMerge;
                }
            }
            // 差分あり [Exists diff]
            else if (result.diff != null)
            {
                EnumMallOrderStatusAmazon oldStatus;

                if (Enum.TryParse(result.diff.oldOrderStatus, out oldStatus))
                {
                    switch (status)
                    {
                        // 保留 [Pending]
                        case EnumMallOrderStatusAmazon.Pending:
                            // 何もしない [Do nothing]
                            break;

                        // キャンセル [Canceled]
                        case EnumMallOrderStatusAmazon.Canceled:
                            // マージ不要 [No need merge]
                            return EnumMergeStatus.Merged;

                        // 出荷前 [Unshipped, PartiallyShipped]
                        case EnumMallOrderStatusAmazon.Unshipped:
                            // 保留→ [Pending ->]
                            if (oldStatus == EnumMallOrderStatusAmazon.Pending)
                            {
                                // 要マージ [Need merge]
                                return EnumMergeStatus.NeedMerge;
                            }
                            break;

                        // 出荷済み [Shipped]
                        case EnumMallOrderStatusAmazon.Shipped:
                            // マージ不要 [No need merge]
                            return EnumMergeStatus.Merged;

                        default:
                            // マージ不要 [No need merge]
                            return EnumMergeStatus.Merged;
                    }
                }
            }

            return null;
        }
        #endregion

        #region 取り込み管理登録 [Register import management]
        /// <summary>
        /// 取り込み管理登録 [Register import management]
        /// </summary>
        /// <param name="dateToday">実行日時 [Today's datetime]</param>
        public override void RegisterOrderImport(DateTime dateToday)
        {
            var stgy = ErsMallFactory.ersMallOrderFactory.GetUpdateAndRegisterMallOrderImportStgy();

            // 取り込み済み登録 [Register imported]
            stgy.Insert(this.siteId, ((GetOrderInfosAPIParamAmazon)this.apiGetOrder.param).toLastUpdateDate.Value, EnumImported.Imported);
        }
        #endregion
    }
}
