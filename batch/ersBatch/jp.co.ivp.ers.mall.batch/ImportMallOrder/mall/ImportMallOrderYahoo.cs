﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.hunglead.harc;
using jp.co.ivp.ers.mall.api.order;
using jp.co.ivp.ers.mall.api.order_status;
using jp.co.ivp.ers.mall.batch.ImportMallOrder.strategy;
using jp.co.ivp.ers.mall.mall_order;

namespace jp.co.ivp.ers.mall.batch.ImportMallOrder.mall
{
    /// <summary>
    /// モール伝票取り込みクラス（Yahoo!） [Class for import mall order (Yahoo!)]
    /// </summary>
    public class ImportMallOrderYahoo
        : ImportMallOrderBase
    {
        #region 定数 [Constant]
        /// <summary>
        /// Yahoo!受注ステータス：新規注文 [Yahoo! order status : new order]
        /// </summary>
        protected const string YAHOO_ORDER_STATUS_NEW_ORDER = "新規注文";
        #endregion

        #region 内部パラメータ [Internal parameters]
        /// <summary>
        /// いたずら解除失敗フラグ [Is failed to release mischief]
        /// </summary>
        protected virtual bool isReleaseMischiefFailed { get; set; }
        #endregion

        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="getStatus">取得ステータス [Get status]</param>
        /// <param name="updateStatus">更新ステータス [Update status]</param>
        /// <param name="isReverseImport">逆取込みフラグ [Is reverse import]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <param name="listSpecifiedOrderCode">指定受注番号リスト [The list of specified order code]</param>
        public ImportMallOrderYahoo(int? siteId, EnumMallShopKbn? shopKbn, EnumMallOrderStatus? getStatus, EnumMallOrderStatus? updateStatus, bool isReverseImport, DateTime dateExecute, IList<string> listSpecifiedOrderCode)
            : base(siteId, shopKbn, getStatus, updateStatus, isReverseImport, dateExecute, listSpecifiedOrderCode)
        {
            this.isReleaseMischiefFailed = false;

            var paramApi = ErsMallFactory.ersMallAPIFactory.GetGetOrderInfosAPIParamYahoo();

            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();

            // APIパラメータ設定 [Set the parameters for API]
            paramApi.shop_id = mallSetup.GetShopId(siteId.Value);
            paramApi.shop_name = mallSetup.GetShopName(siteId.Value);

            // 取り込み日時取得 [Get datetime for import]
            var importDateTime = this.ObtainImportDateTime(siteId, dateExecute);

            paramApi.search_date_from = importDateTime.dateFrom;
            paramApi.search_date_to = importDateTime.dateTo;

            // ストアクリエイターProへの対応 [Responding to Store creator pro]
            var dateChange = ErsMallFactory.ersMallUtilityFactory.getSetup().mallYahooStoreCreatorProDatetime;
            if (dateExecute > dateChange.Value.AddMinutes(10))
            {
                paramApi.managementTool = EnumMallYahooManagementToolType.StoreCreatorPro;
            }
            else
            {
                paramApi.managementTool = EnumMallYahooManagementToolType.StoreManager;
            }

            // API設定 [Set the API]
            this.apiGetOrder = ErsMallFactory.ersMallAPIFactory.GetGetOrderInfosAPIYahoo(paramApi);


            // クラスタイプ設定 [Set the class type]
            this.typeOrder = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderYahoo().GetType();
            this.typeOrderDetail = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderDetail().GetType();
        }
        #endregion

        #region 取り込み結果クラス変換 [Convert to the class of import results]
        /// <summary>
        /// 取り込み対象チェック [Check import target]
        /// </summary>
        /// <param name="dicOrder">伝票データ [Data of order]</param>
        /// <returns>true : 対象 [Target] ／ false : 非対象 [Not target]</returns>
        protected override bool IsImportTarget(Dictionary<string, object> dicOrder)
        {
            if (!base.IsImportTarget(dicOrder))
            {
                return false;
            }

            var other = (Dictionary<string, object>)dicOrder["other"];

            if (other.ContainsKey("caution_canceled"))
            {
                // いたずら解除失敗 [Failed to release mischief]
                if (!Convert.ToBoolean(other["caution_canceled"]))
                {
                    this.isReleaseMischiefFailed = true;
                    return false;
                }
            }

            return true;
        }
        #endregion

        #region ステータス変更 [Change status]
        /// <summary>
        /// ステータス更新 [Update status]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        public override void UpdateStatus(HarcApiRequest request)
        {
            // ステータス変更の必要なし or 取り込み結果無し [No need for update status OR No result of import]
            if (this.updateStatus == null || this.listMallOrdersResult.Count == 0)
            {
                return;
            }

            var listErrorMessage = new List<string>();
            var listTagetStatus = new List<string>() { YAHOO_ORDER_STATUS_NEW_ORDER };

            foreach (var targetStatus in listTagetStatus)
            {
                var listTarget = this.listMallOrdersResult.Where(obj => obj.objMallOrder.order_status == targetStatus).ToList<MallOrderResult>();

                if (listTarget.Count == 0)
                {
                    continue;
                }

                var errorMessage = string.Empty;

                try
                {
                    // ステータス更新 [Update status]
                    errorMessage = (new UpdateStatusForMallOrderResultStgy()).UpdateStatus(request, listTarget, this.GetChangeOrderStatusAPI(), this.GetUpdateStatusFromTargetOrderStatus(targetStatus));
                }
                catch (Exception e)
                {
                    errorMessage = e.ToString();
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    listErrorMessage.Add(errorMessage);
                }
            }

            if (listErrorMessage.Count > 0)
            {
                throw new Exception(String.Join(Environment.NewLine, listErrorMessage));
            }
        }

        /// <summary>
        /// 更新受注ステータス取得（対象受注ステータスから） [Get the order status for update (from Target order status)]
        /// </summary>
        /// <param name="targetStatus">対象受注ステータス [Target order status]</param>
        /// <returns></returns>
        protected virtual EnumMallOrderStatus? GetUpdateStatusFromTargetOrderStatus(string targetStatus)
        {
            switch (targetStatus)
            {
                case YAHOO_ORDER_STATUS_NEW_ORDER:
                    return this.updateStatus;

                default:
                    throw new NotSupportedException();
            }
        }

        /// <summary>
        /// ステータス変更API取得 [Get the API for change status]
        /// </summary>
        /// <returns>ステータス変更API [The API for change status]</returns>
        protected override ChangeOrderStatusAPIBase GetChangeOrderStatusAPI()
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIParamYahoo();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);
            apiParam.shop_name = mallSetup.GetShopName(this.siteId.Value);

            apiParam.managementTool = ((GetOrderInfosAPIParamYahoo)this.apiGetOrder.param).managementTool;

            return ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIYahoo(apiParam);
        }
        #endregion

        #region マージステータス更新 [Update merge status]
        /// <summary>
        /// 対象モール伝票取得 [Get target mall order]
        /// </summary>
        /// <param name="d_no">モール伝票番号 [Mall order number]</param>
        /// <param name="updateStatus">更新ステータス [Update status]</param>
        /// <returns>対象モール伝票 [Target mall order]</returns>
        protected override ErsMallOrder GetTargetBranchOrder(string d_no, EnumMallOrderStatus? updateStatus)
        {
            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderRepository();
            var criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderCriteria();

            criteria.d_no = d_no;

            // 保留を含める（いたずら） [Contain pending (mischief)]
            var listOrderStatus = new List<int>() { (int)EnumMallOrderStatus.Pending };

            if (updateStatus != null)
            {
                listOrderStatus.Add((int)updateStatus);
            }

            criteria.order_status_id_in = listOrderStatus;

            return repository.FindSingle(criteria);
        }
        #endregion

        #region 取り込み管理登録 [Register import management]
        /// <summary>
        /// 取り込み管理登録 [Register import management]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        public override void RegisterOrderImport(DateTime dateExecute)
        {
            // いたずら解除失敗 [Failed to release mischief]
            if (this.isReleaseMischiefFailed)
            {
                return;
            }

            base.RegisterOrderImport(dateExecute);
        }
        #endregion
    }
}
