﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.hunglead.harc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.api;
using jp.co.ivp.ers.mall.api.order;
using jp.co.ivp.ers.mall.api.order_status;
using jp.co.ivp.ers.mall.batch.ImportMallOrder.strategy;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mall.mall_order;

namespace jp.co.ivp.ers.mall.batch.ImportMallOrder.mall
{
    /// <summary>
    /// モール伝票取り込みクラス（基底） [Class for import mall order (Base)]
    /// </summary>
    public class ImportMallOrderBase
    {
        #region 基本パラメータ [Basic parameters]
        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public virtual int? siteId { get; protected set; }

        /// <summary>
        /// 店舗タイプ [Shop type]
        /// </summary>
        public virtual EnumMallShopKbn? shopKbn { get; protected set; }

        /// <summary>
        /// 取得ステータス [Get status]
        /// </summary>
        public virtual EnumMallOrderStatus? getStatus { get; protected set; }

        /// <summary>
        /// 変更ステータス [Update status]
        /// </summary>
        public virtual EnumMallOrderStatus? updateStatus { get; protected set; }

        /// <summary>
        /// 逆取込みフラグ [Is reverse import]
        /// </summary>
        public virtual bool isReverseImport { get; protected set; }

        /// <summary>
        /// 指定受注番号リスト [The list of specified order code]
        /// </summary>
        public virtual IList<string> listSpecifiedOrderCode { get; protected set; }

        /// <summary>
        /// 取り込み管理登録フラグ [Falg for register import management]
        /// </summary>
        public virtual bool doRegisterOrderImport { get; protected set; }
        #endregion

        #region 内部パラメータ [Internal parameters]
        /// <summary>
        /// モール伝票データ取得API [API for Get order]
        /// </summary>
        protected virtual GetOrderInfosAPIBase apiGetOrder { get; set; }

        /// <summary>
        /// モール伝票ヘッダエンティティタイプ [Entity type of mall order]
        /// </summary>
        protected virtual Type typeOrder { get; set; }

        /// <summary>
        /// モール伝票明細エンティティタイプ [Entity type of mall order detail]
        /// </summary>
        protected virtual Type typeOrderDetail { get; set; }
        #endregion

        #region 結果パラメータ [Result parameters]
        /// <summary>
        /// エラーログ [Error log]
        /// </summary>
        public virtual string errorLog { get; protected set; }

        /// <summary>
        /// モール伝票取込み結果クラスリスト [List of class of import results]
        /// </summary>
        public virtual List<MallOrderResult> listMallOrdersResult { get; protected set; }
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="getStatus">取得ステータス [Get status]</param>
        /// <param name="updateStatus">更新ステータス [Update status]</param>
        /// <param name="isReverseImport">逆取込みフラグ [Is reverse import]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <param name="listSpecifiedOrderCode">指定受注番号リスト [The list of specified order code]</param>
        public ImportMallOrderBase(int? siteId, EnumMallShopKbn? shopKbn, EnumMallOrderStatus? getStatus, EnumMallOrderStatus? updateStatus, bool isReverseImport, DateTime dateExecute, IList<string> listSpecifiedOrderCode)
        {
            this.siteId = siteId;
            this.shopKbn = shopKbn;
            this.getStatus = getStatus;
            this.updateStatus = updateStatus;
            this.isReverseImport = isReverseImport;
            this.listSpecifiedOrderCode = listSpecifiedOrderCode;

            this.doRegisterOrderImport = true;
        }
        #endregion

        #region モール伝票データ取得 [Get mall order data]
        /// <summary>
        /// モール伝票データ取得 [Get mall order data]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        public virtual void ImportOrder(HarcApiRequest request)
        {
            List<Dictionary<string, object>> listImportData = null;

            try
            {
                // モール伝票データ取得 [Get mall order data]
                listImportData = this.apiGetOrder.GetOrderInfos(request, this.getStatus);
            }
            catch (APIFailedException e)
            {
                string status = (this.getStatus != null ? ((int)this.getStatus).ToString() : "null");
                throw new Exception(ErsResources.GetMessage("100000", this.siteId, (int)this.shopKbn, status, e.ToString()));
            }

            // クラスに変換 [Convert tothe class]
            if (listImportData.Count != 0)
            {
                this.listMallOrdersResult = this.ConvertToMallOrderResult(listImportData);
            }
            else
            {
                this.listMallOrdersResult = new List<MallOrderResult>();
            }
        }
        #endregion

        #region 取り込み結果クラス変換 [Convert to the class of import results]
        /// <summary>
        /// 取り込み結果クラス変換 [Convert to the class of import results]
        /// </summary>
        /// <param name="listImportData">取り込みデータ [Imported data]</param>
        /// <returns>取り込み結果クラスリスト [The list of the class of import results]</returns>
        protected virtual List<MallOrderResult> ConvertToMallOrderResult(List<Dictionary<string, object>> listImportData)
        {
            var stgy = ErsMallFactory.ersMallOrderFactory.GetObtainMallOrderResultStgy();

            // 逆にする [Reverse]
            var listRet = new List<MallOrderResult>();

            if (this.isReverseImport)
            {
                listImportData.Reverse();
            }

            IList<string> listNewOrderCode = new List<string>();

            foreach (var dicOrder in listImportData)
            {
                // 取り込み対象チェック [Check import target]
                if (!this.IsImportTarget(dicOrder))
                {
                    continue;
                }

                string resultValidate = null;

                // 取り込みデータチェック [Check data of import]
                resultValidate = this.ValidateImportData(dicOrder);

                if (string.IsNullOrEmpty(resultValidate))
                {
                    // 取り込み結果クラス取得 [Get the class of import result]
                    var result = stgy.GetMallOrderResult(dicOrder, this.siteId, this.shopKbn, this.typeOrder, this.typeOrderDetail);

                    // 取り込み結果チェック [Check result of import]
                    resultValidate = this.ValidateMallOrderResult(result, listNewOrderCode);

                    if (string.IsNullOrEmpty(resultValidate))
                    {
                        listRet.Add(result);
                    }
                }

                if (!string.IsNullOrEmpty(resultValidate))
                {
                    this.errorLog += resultValidate;
                }
            }

            return listRet;
        }

        /// <summary>
        /// 取り込み対象チェック [Check import target]
        /// </summary>
        /// <param name="dicOrder">伝票データ [Data of order]</param>
        /// <returns>true : 対象 [Target] ／ false : 非対象 [Not target]</returns>
        protected virtual bool IsImportTarget(Dictionary<string, object> dicOrder)
        {
            if (this.listSpecifiedOrderCode != null)
            {
                // 指定受注番号判定 [Judgment specified order code]
                return listSpecifiedOrderCode.Contains(Convert.ToString(dicOrder["order_code"]));
            }

            var details = (ArrayList)dicOrder["details"];

            if (details.Count > 0)
            {
                var prefix = ErsMallCommonService.mallProductSkuPrefix;

                if (prefix.HasValue())
                {
                    foreach (Dictionary<string, object> detail in details)
                    {
                        var item_code = Convert.ToString(detail["item_code"]);

                        // 接頭語判定 [Judgement prefix]
                        if (item_code.HasValue() && !item_code.StartsWith(prefix))
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// 取り込みデータチェック [Check data of import]
        /// </summary>
        /// <param name="dicOrder">伝票データ [Data of order]</param>
        /// <returns>エラーメッセージ [Error message]</returns>
        protected virtual string ValidateImportData(Dictionary<string, object> dicOrder)
        {
            // 明細データが空 [Detail is empty]
            if (((ArrayList)dicOrder["details"]).Count == 0)
            {
                return ErsResources.GetMessage("100005", this.siteId, (int)this.shopKbn, dicOrder["order_code"]);
            }

            return string.Empty;
        }

        /// <summary>
        /// 取り込み結果チェック [Check result of import]
        /// </summary>
        /// <param name="result">取り込み結果 [Result of import]</param>
        /// <param name="listNewOrderCode">新規受注番号リスト [The list of New order code]</param>
        /// <returns>エラーメッセージ [Error message]</returns>
        protected virtual string ValidateMallOrderResult(MallOrderResult result, IList<string> listNewOrderCode)
        {
            // 取得ステータス・更新ステータスあり [Get status and Update status are specified]
            if (this.getStatus != null && this.updateStatus != null)
            {
                // 存在している [Exists]
                if (result.isExists)
                {
                    // 差分あり [Exists diff]
                    if (result.diff != null)
                    {
                        // 取得ステータスに戻った [Back to get status]
                        if (result.diff.listColsChangedHeader != null &&
                            result.diff.listColsChangedHeader.Contains("order_status_id") &&
                            result.objMallOrder.order_status_id == (int)this.getStatus)
                        {
                            return ErsResources.GetMessage("100003", this.siteId, (int)this.shopKbn, result.objMallOrder.order_status_id, result.objMallOrder.order_code, result.objMallOrder.d_no);
                        }
                    }
                }
                else
                {
                    // 重複チェック [Check duplicate order]
                    if (!listNewOrderCode.Contains(result.objMallOrder.order_code))
                    {
                        listNewOrderCode.Add(result.objMallOrder.order_code);
                    }
                    else
                    {
                        return ErsResources.GetMessage("100004", this.siteId, (int)this.shopKbn, result.objMallOrder.order_code, result.objMallOrder.d_no);
                    }
                }
            }

            return string.Empty;
        }
        #endregion

        #region 取り込み日時取得 [Get datetime for import]
        /// <summary>
        /// 取り込み日時取得 [Get datetime for import]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <returns>取り込み日時パラメータ [Parameter for import datetime]</returns>
        protected virtual ErsCommonStruct.DateTimeStartEnd ObtainImportDateTime(int? siteId, DateTime dateExecute)
        {
            var spec = ErsMallFactory.ersMallOrderFactory.GetOrderDateParamSearchSpecification();
            var criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderImportCriteria();

            criteria.site_id = siteId;
            criteria.import_time_less_than = dateExecute.Date;
            criteria.imported = EnumImported.NotImported;

            ErsCommonStruct.DateTimeStartEnd retParam = default(ErsCommonStruct.DateTimeStartEnd);

            // 存在確認 [Check exists]
            if (spec.GetCountData(criteria) > 0)
            {
                criteria.SetOrderByImportTime(Criteria.OrderBy.ORDER_BY_DESC);

                var listFind = spec.GetSearchData(criteria);

                // 取得した日時を指定 [Specify get day]
                retParam.dateFrom = retParam.dateTo = Convert.ToDateTime(listFind[0]["import_time"]);
            }
            else
            {
                // 実行日時を指定 [Specify execute day]
                retParam.dateFrom = retParam.dateTo = dateExecute.Date;

                // 実行当日分無し [Not exists data for execete day]
                if (!ErsMallFactory.ersMallOrderFactory.GetUpdateAndRegisterMallOrderImportStgy().IsExistsRecord(siteId, dateExecute))
                {
                    // 前日分も含ませる [Contain previous day]
                    retParam.dateFrom = retParam.dateFrom.Value.AddDays(-1);
                }
            }

            return retParam;
        }
        #endregion

        #region 取り込み結果取り込み [Import the result of import]
        /// <summary>
        /// 取り込み結果取り込み [Import the result of import]
        /// </summary>
        public virtual void ImportResult()
        {
            foreach (var result in this.listMallOrdersResult)
            {
                // 存在している and 差分なし [Exists AND Not exists diff]
                if (result.isExists && result.diff == null)
                {
                    continue;
                }

                // モール伝票取り込み結果取り込み [Import the result of import mall order]
                (new ImportMallOrderResultStgy()).Import(result);
            }
        }
        #endregion

        #region 在庫更新 [Update stock]
        /// <summary>
        /// 在庫更新 [Update stock]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        public virtual void UpdateStock(HarcApiRequest request)
        {
        }
        #endregion

        #region ステータス更新 [Update status]
        /// <summary>
        /// ステータス更新 [Update status]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        public virtual void UpdateStatus(HarcApiRequest request)
        {
            // ステータス変更の必要なし or 取り込み結果無し [No need for update status OR No result of import]
            if (this.updateStatus == null || this.listMallOrdersResult.Count == 0)
            {
                return;
            }

            // ステータス更新 [Update status]
            (new UpdateStatusForMallOrderResultStgy()).UpdateStatus(request, this.listMallOrdersResult, this.GetChangeOrderStatusAPI(), this.updateStatus);
        }

        /// <summary>
        /// ステータス変更API取得 [Get the API for change status]
        /// </summary>
        /// <returns>ステータス変更API [The API for change status]</returns>
        protected virtual ChangeOrderStatusAPIBase GetChangeOrderStatusAPI()
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIParamBase();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);
            apiParam.shop_name = mallSetup.GetShopName(this.siteId.Value);

            return ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIBase(apiParam);
        }
        #endregion

        #region マージステータス更新 [Update merge status]
        /// <summary>
        /// マージステータス更新 [Update merge status]
        /// </summary>
        public virtual void UpdateMergeStatus()
        {
            // 取り込み結果無し [No need for update status OR No result of import]
            if (this.listMallOrdersResult.Count == 0)
            {
                return;
            }

            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderRepository();

            foreach (var result in this.listMallOrdersResult)
            {
                // マージステータス取得 [Get merge status]
                var mergeStatus = this.GetMergeStatus(result);

                if (mergeStatus == null)
                {
                    continue;
                }

                var objOld = this.GetTargetBranchOrder(result.objMallOrder.d_no, this.updateStatus);

                if (objOld == null)
                {
                    continue;
                }

                var objNew = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderWithParameters(objOld.GetPropertiesAsDictionary());

                objNew.merge_status = mergeStatus;

                repository.Update(objOld, objNew);
            }
        }

        /// <summary>
        /// 対象モール伝票取得 [Get target mall order]
        /// </summary>
        /// <param name="d_no">モール伝票番号 [Mall order number]</param>
        /// <param name="updateStatus">更新ステータス [Update status]</param>
        /// <returns>対象モール伝票 [Target mall order]</returns>
        protected virtual ErsMallOrder GetTargetBranchOrder(string d_no, EnumMallOrderStatus? updateStatus)
        {
            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderRepository();
            var criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderCriteria();

            criteria.d_no = d_no;

            if (updateStatus != null)
            {
                criteria.order_status_id = (int)updateStatus;
            }

            return repository.FindSingle(criteria);
        }

        /// <summary>
        /// マージステータス取得 [Get merge status]
        /// </summary>
        /// <param name="result">取り込み結果 [Result of import]</param>
        /// <returns>マージステータス [Merge status]</returns>
        protected virtual EnumMergeStatus? GetMergeStatus(MallOrderResult result)
        {
            return EnumMergeStatus.NeedMerge;
        }
        #endregion

        #region 取り込み管理登録 [Register import management]
        /// <summary>
        /// 取り込み管理登録 [Register import management]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        public virtual void RegisterOrderImport(DateTime dateExecute)
        {
            if (!this.doRegisterOrderImport)
            {
                return;
            }

            var stgy = ErsMallFactory.ersMallOrderFactory.GetUpdateAndRegisterMallOrderImportStgy();

            // 前日以前の取り込み [Previous import]
            if (this.apiGetOrder.param.search_date_to.Value.Date != dateExecute.Date)
            {
                // 取り込み済み [Update imported]
                stgy.Update(this.siteId, this.apiGetOrder.param.search_date_to.Value);
            }
            else
            {
                // 存在チェック [Check exists]
                if (!stgy.IsExistsRecord(this.siteId, dateExecute.Date))
                {
                    // 未取り込み登録 [Register not imported]
                    stgy.Insert(this.siteId, dateExecute.Date, EnumImported.NotImported);
                }
            }
        }
        #endregion
    }
}
