﻿using System;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.mall_order;

namespace jp.co.ivp.ers.mall.batch.ImportMallOrder.strategy
{
    /// <summary>
    /// モール伝票取り込み結果取り込み [Import mall order]
    /// </summary>
    public class ImportMallOrderResultStgy
    {
        /// <summary>
        /// 取り込み [Import]
        /// </summary>
        /// <param name="result">取り込み結果 [Result of import]</param>
        public void Import(MallOrderResult result)
        {
            var stgyInsertResult = new InsertMallOrderResultStgy();

            using (var tx = ErsDB_parent.BeginTransaction())
            {
                // 存在する [Exists]
                if (result.isExists)
                {
                    // 一旦削除 [Delete once]
                    this.DeleteMallOrder(result);

                    // 更新日時セット [Set utime]
                    result.SetUtime(DateTime.Now);

                    // インサート [Insert]
                    stgyInsertResult.Insert(result);
                }
                else
                {
                    // 登録日時セット [Set intime]
                    result.SetIntime(DateTime.Now);

                    // インサート [Insert]
                    stgyInsertResult.Insert(result);
                }
                tx.Commit();
            }
        }

        /// <summary>
        /// モール伝票削除 [Delete mall order]
        /// </summary>
        /// <param name="result">取り込み結果 [Result of import]</param>
        protected virtual void DeleteMallOrder(MallOrderResult result)
        {
            var repositoryHeader = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderRepository();
            var repositoryDetail = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderDetailRepository();

            // ヘッダ [Header]
            repositoryHeader.Delete(result.objMallOrder);

            // ボディ [Body]
            foreach (var detail in result.listMallOrderDetail)
            {
                repositoryDetail.Delete(detail);
            }
        }
    }
}
