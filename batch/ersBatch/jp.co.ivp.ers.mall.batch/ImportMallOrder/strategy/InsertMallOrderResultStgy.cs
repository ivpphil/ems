﻿using jp.co.ivp.ers.mall.mall_order;

namespace jp.co.ivp.ers.mall.batch.ImportMallOrder.strategy
{
    /// <summary>
    /// モール伝票インサート [Insert mall order]
    /// </summary>
    public class InsertMallOrderResultStgy
    {
        /// <summary>
        /// インサート [Insert]
        /// </summary>
        /// <param name="dbType">DBタイプ [Type of DB]</param>
        /// <param name="result">取り込み結果 [Result of import]</param>
        public void Insert(MallOrderResult result)
        {
            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderRepository();
            var repository_detail = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderDetailRepository();

            // ヘッダ [Header]
            repository.Insert(result.objMallOrder);

            // ボディ [Body]
            foreach (var detail in result.listMallOrderDetail)
            {
                repository_detail.Insert(detail);
            }
        }
    }
}
