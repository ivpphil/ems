﻿using System.Collections.Generic;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall.mall_order;

namespace jp.co.ivp.ers.mall.batch.ImportMallOrder.strategy
{
    /// <summary>
    /// 在庫更新API用パラメータ取得（差分から） [Get the API parameter for update stock (from Diff)]
    /// </summary>
    public class ObtainUpdateStockApiParamFromDiffStgy
    {
        /// <summary>
        /// API用パラメータ取得 [Get the API parameter]
        /// </summary>
        /// <param name="listMallOrdersResult">モール伝票取り込み結果リスト [Th list of result of import mall order]</param>
        /// <param name="mall_shop_kbn">店舗タイプ [Type of shop]</param>
        /// <returns>API用パラメータ [The API parameter]</returns>
        public virtual IList<UpdateStockParam> Obtain(IList<MallOrderResult> listMallOrdersResult, EnumMallShopKbn? mall_shop_kbn)
        {
            var dicStockDiff = new Dictionary<string, int>();

            foreach (var result in listMallOrdersResult)
            {
                // 存在していない [Doesn't exists]
                if (!result.isExists)
                {
                    // 保留 or 新規（未出荷） [Pending or New (Unshipped)]
                    if (result.objMallOrder.order_status_id == (int)EnumMallOrderStatus.Pending ||
                        result.objMallOrder.order_status_id == (int)EnumMallOrderStatus.New)
                    {
                        foreach (var detail in result.listMallOrderDetail)
                        {
                            this.AddStockDiff(dicStockDiff, detail.ers_item_code, -detail.quantity.Value);
                        }
                    }
                }
                else if (result.diff != null)
                {
                    // → キャンセル [-> Cancel]
                    if (result.diff.listColsChangedHeader != null && 
                        result.diff.listColsChangedHeader.Contains("order_status_id") && 
                        result.objMallOrder.order_status_id == (int)EnumMallOrderStatus.Canceled)
                    {
                        foreach (var detail in result.listMallOrderDetail)
                        {
                            this.AddStockDiff(dicStockDiff, detail.ers_item_code, detail.quantity.Value);
                        }
                    }
                    else
                    {
                        foreach (var detail in result.listMallOrderDetail)
                        {
                            // 削除 [Delete]
                            if (result.diff.listDelItemCodesDetail != null && result.diff.listDelItemCodesDetail.Contains(detail.item_key))
                            {
                                this.AddStockDiff(dicStockDiff, detail.ers_item_code, detail.quantity.Value);
                            }
                            // 追加 [Add]
                            else if (result.diff.listAddItemCodesDetail != null && result.diff.listAddItemCodesDetail.Contains(detail.item_key))
                            {
                                this.AddStockDiff(dicStockDiff, detail.ers_item_code, -detail.quantity.Value);
                            }
                            // 数量 [Quantity]
                            else if (result.diff.dicQuantityDiffDetail != null && result.diff.dicQuantityDiffDetail.ContainsKey(detail.item_key))
                            {
                                this.AddStockDiff(dicStockDiff, detail.ers_item_code, result.diff.dicQuantityDiffDetail[detail.item_key]);
                            }
                        }
                    }
                }
            }

            // 在庫差分なし [No diff of stock]
            if (dicStockDiff.Count == 0)
            {
                return null;
            }

            // 商品コードリストに変換 [Convert to the list of scode]
            var listScode = new List<string>();

            foreach (var key in dicStockDiff.Keys)
            {
                listScode.Add(key);
            }

            // 平準化商品コードリスト取得 [Get the list of scode for leveling]
            var listFindScode = this.GetLevlingScodeList(listScode, mall_shop_kbn);

            // 平準化を行うパラメータリスト [The list of parameter for leveling]
            var listRet = new List<UpdateStockParam>();

            foreach (var diff in dicStockDiff)
            {
                if (listFindScode.Contains(diff.Key))
                {
                    var param = default(UpdateStockParam);

                    param.productCode = diff.Key;
                    param.quantity = diff.Value;
                    param.operation = EnumMallStockOperation.add;

                    listRet.Add(param);
                }
            }

            return listRet;
        }

        /// <summary>
        /// 平準化商品コードリスト取得 [Get the list of scode for leveling]
        /// </summary>
        /// <param name="listScode">商品コードリスト [The list of scode]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <returns>平準化商品コードリスト [The list of scode for leveling]</returns>
        protected virtual IList<string> GetLevlingScodeList(IList<string> listScode, EnumMallShopKbn? shopKbn)
        {
            var listRet = new List<string>();

            // 平準化商品検索 [Search the list of scode for leveling setting]
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();

            criteria.scode_in = listScode;
            criteria.mall_flg = EnumOnOff.On;

            criteria.SetGroupByScode();

            var listFind = repository.Find(criteria, new List<string> { "scode" });

            // 商品コードリストに変換 [Convert to the list of scode]
            foreach (var data in listFind)
            {
                listRet.Add(data.scode);
            }

            return listRet;
        }

        /// <summary>
        /// 在庫差分追加 [Add to stock diff]
        /// </summary>
        /// <param name="dicStockDiff">在庫差分リスト [The list of stock diff]</param>
        /// <param name="item_code">アイテムコード [Item code]</param>
        /// <param name="quantity">数量 [Quantity]</param>
        protected virtual void AddStockDiff(Dictionary<string, int> dicStockDiff, string item_code, int quantity)
        {
            if (!dicStockDiff.ContainsKey(item_code))
            {
                dicStockDiff[item_code] = quantity;
            }
            else
            {
                dicStockDiff[item_code] += quantity;
            }
        }
    }
}
