﻿using System;
using System.Collections.Generic;
using com.hunglead.harc;
using jp.co.ivp.ers.mall.api;
using jp.co.ivp.ers.mall.api.order_status;
using jp.co.ivp.ers.mall.mall_order;

namespace jp.co.ivp.ers.mall.batch.ImportMallOrder.strategy
{
    /// <summary>
    /// ステータス更新 [Update status]
    /// </summary>
    public class UpdateStatusForMallOrderResultStgy
    {
        /// <summary>
        /// ステータス更新 [Update status]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="listMallOrdersResult">モール伝票取り込み結果リスト [Th list of result of import mall order]</param>
        /// <param name="ChangeOrderStatusAPIBase">モール伝票ステータス変更API [API for update mall order status]</param>
        /// <param name="updateStatus">変更ステータス [Update status]</param>
        /// <returns>エラーメッセージ [Error message]</returns>
        public virtual string UpdateStatus(HarcApiRequest request, IList<MallOrderResult> listMallOrdersResult, ChangeOrderStatusAPIBase apiChangeOrderStatus, EnumMallOrderStatus? updateStatus)
        {
            // 更新用取り込み結果リスト取得 [Get the list of import result for update status]
            IList<MallOrderResult> listResultForUpdate = this.GetResultListForUpdateStatus(listMallOrdersResult);

            if (listResultForUpdate.Count == 0)
            {
                return string.Empty;
            }

            int divisionCount = 0;
            string errorLog = string.Empty;

            while (true)
            {
                // 分割更新用取り込み結果リスト取得 [Get the list of import result for division update status]
                IList<MallOrderResult> listResultForDivisionUpdate = this.GetResultListForDivisionUpdateStatus(listResultForUpdate, divisionCount);

                if (listResultForDivisionUpdate == null)
                {
                    break;
                }

                // リトライ管理用ディクショナリ [Dictionary of retry management]
                IDictionary<string, int> dicRetry = new Dictionary<string, int>();

                // リトライリスト [The list of retry]
                IList<string> listRetry = new List<string>();

                // エラーリスト [The list of error]
                IDictionary<string, string> dicError = new Dictionary<string, string>();

                // 更新用リスト [The list for update]
                IList<MallOrderResult> listUpdate = null;

                while (true)
                {
                    // APIリクエスト用モール伝票リスト取得 [Get the list of mall order for API request]
                    var listRequest = this.GetMallOrderListForAPIRequest(listResultForDivisionUpdate, listRetry);

                    // ステータス変更 [Change status]
                    var dicUpdateResult = this.ChangeStatus(request, apiChangeOrderStatus, listRequest, updateStatus);

                    // リトライ更新用リスト取得 [Get the list for retry update]
                    listUpdate = this.GetResultListForRetry(listResultForDivisionUpdate, listRetry);

                    foreach (var result in listUpdate)
                    {
                        string errorResult = string.Empty;

                        if (dicUpdateResult.ContainsKey(result.objMallOrder.order_code))
                        {
                            // 更新結果取得 [Get the result of update]
                            string updateResult = Convert.ToString(dicUpdateResult[result.objMallOrder.order_code]);

                            // 正常な結果かどうか [Is valid result]
                            if (!apiChangeOrderStatus.IsNormalResult(updateResult))
                            {
                                // リトライ可能なエラーかどうか [Is retryable error]
                                if (apiChangeOrderStatus.IsRetryAbleResultError(updateResult))
                                {
                                    if (!dicRetry.ContainsKey(result.objMallOrder.d_no))
                                    {
                                        dicRetry[result.objMallOrder.d_no] = 0;
                                    }
                                    // 指定回数リトライ [Retry specified times]
                                    if (dicRetry[result.objMallOrder.d_no]++ < ChangeOrderStatusAPIBase.API_ERROR_RETRY_COUNT)
                                    {
                                        if (!listRetry.Contains(result.objMallOrder.d_no))
                                        {
                                            listRetry.Add(result.objMallOrder.d_no);
                                        }
                                        continue;
                                    }
                                }

                                errorResult = updateResult;
                            }
                        }
                        // 更新結果なし [No update result]
                        else
                        {
                            errorResult = "No update result.";
                        }

                        // 失敗していた場合はエラーメッセージを代入する [Set the error message]
                        if (!string.IsNullOrEmpty(errorResult))
                        {
                            dicError[result.objMallOrder.d_no] = ErsResources.GetMessage("101002", result.objMallOrder.site_id, (int)result.objMallOrder.mall_shop_kbn, (int)updateStatus, result.objMallOrder.order_code, errorResult);
                        }

                        // リトライ回数オーバー or リトライの結果成功 [Over retry times OR Success after retry]
                        if (listRetry.Contains(result.objMallOrder.d_no))
                        {
                            listRetry.Remove(result.objMallOrder.d_no);
                        }
                    }

                    // リトライの必要がある場合はリトライする [Retry]
                    if (listRetry.Count > 0)
                    {
                        continue;
                    }

                    break;
                }

                foreach (var result in listResultForDivisionUpdate)
                {
                    // 正常終了した [Doesn't exist and Success]
                    if (!dicError.ContainsKey(result.objMallOrder.d_no))
                    {
                        // ERSステータス更新 [Update ERS status]
                        this.UpdateStatusErs(result, updateStatus);
                    }
                }

                // ステータス変更失敗エラーリスト [The list of failed update status]
                if (dicError.Count > 0)
                {
                    foreach (var error in dicError)
                    {
                        errorLog += error.Value;
                    }
                }

                divisionCount++;
            }

            return !errorLog.HasValue() ? string.Empty : ErsResources.GetMessage("101000", errorLog);
        }

        #region ステータス変更 [Change status]
        /// <summary>
        /// ステータス変更 [Change status]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="ChangeOrderStatusAPIBase">モール伝票ステータス変更API [API for update mall order status]</param>
        /// <param name="listOrder">モール伝票リスト [The list of mall order]</param>
        /// <param name="updateStatus">変更ステータス [Update status]</param>
        /// <returns>更新結果 [Update result]</returns>
        protected virtual Dictionary<string, object> ChangeStatus(HarcApiRequest request, ChangeOrderStatusAPIBase apiChangeOrderStatus, IList<ErsMallOrder> listOrder, EnumMallOrderStatus? updateStatus)
        {
            try
            {
                var listParam = new List<UpdateOrderStatusParam>();

                foreach (var order in listOrder)
                {
                    UpdateOrderStatusParam param = default(UpdateOrderStatusParam);

                    param.orderCode = order.order_code;
                    param.orderDate = order.order_date;

                    listParam.Add(param);
                }

                // ステータス変更 [Change status]
                return apiChangeOrderStatus.ChangeOrderStatus(request, listParam, updateStatus);
            }
            catch (APIFailedException e)
            {
                string errorMessage = string.Empty;

                foreach (var order in listOrder)
                {
                    errorMessage += ErsResources.GetMessage("101001", order.site_id, (int)order.mall_shop_kbn, (int)updateStatus, order.order_code);
                }

                errorMessage += e.ToString();

                throw new Exception(errorMessage);
            }
        }
        #endregion

        #region 各処理用リスト取得 [Get the list for each processes]
        /// <summary>
        /// 更新用取り込み結果リスト取得 [Get the list of import result for update status]
        /// </summary>
        /// <param name="listMallOrdersResult">モール伝票取り込み結果リスト [Th list of result of import mall order]</param>
        /// <returns>更新用取り込み結果リスト [The list of import result for update status]</returns>
        private IList<MallOrderResult> GetResultListForUpdateStatus(IList<MallOrderResult> listMallOrdersResult)
        {
            var listRet = new List<MallOrderResult>();

            // 存在しなかった [Doesn't exists]
            foreach (var result in listMallOrdersResult)
            {
                if (!result.isExists)
                {
                    listRet.Add(result);
                }
            }

            return listRet;
        }

        /// <summary>
        /// 分割更新用取り込み結果リスト取得 [Get the list of import result for division update status]
        /// </summary>
        /// <param name="listMallOrdersResult">モール伝票取り込み結果リスト [Th list of result of import mall order]</param>
        /// <param name="divisionCount">分割カウント [Division count]</param>
        /// <returns>分割更新用取り込み結果リスト [The list of import result for division update status]</returns>
        private IList<MallOrderResult> GetResultListForDivisionUpdateStatus(IList<MallOrderResult> listMallOrdersResult, int divisionCount)
        {
            var listRet = new List<MallOrderResult>();

            var start = ChangeOrderStatusAPIBase.API_PROCESSING_DEVISION_COUNT * divisionCount;
            var end = start + ChangeOrderStatusAPIBase.API_PROCESSING_DEVISION_COUNT;
            end = end > listMallOrdersResult.Count ? listMallOrdersResult.Count : end;

            if (start > end)
            {
                return null;
            }

            for (var i = start; i < end; i++)
            {
                listRet.Add(listMallOrdersResult[i]);
            }

            return listRet;
        }

        /// <summary>
        /// リトライ更新用リスト取得 [Get the list for retry update]
        /// </summary>
        /// <param name="listResultForUpdate">更新用取り込み結果リスト [The list of import result for update status]</param>
        /// <param name="listRetry">リトライリスト [The list of retry]</param>
        /// <returns>リトライ更新用リスト [The list for retry update]</returns>
        private IList<MallOrderResult> GetResultListForRetry(IList<MallOrderResult> listResultForUpdate, IList<string> listRetry)
        {
            if (listRetry.Count == 0)
            {
                return listResultForUpdate;
            }

            var listRet = new List<MallOrderResult>();

            // リトライの場合はリトライ分 [When retry, contains retry]
            foreach (var result in listResultForUpdate)
            {
                if (listRetry.Contains(result.objMallOrder.d_no))
                {
                    listRet.Add(result);
                }
            }

            return listRet;
        }

        /// <summary>
        /// APIリクエスト用モール伝票リスト取得 [Get the list of mall order for API request]
        /// </summary>
        /// <param name="listResultForUpdate">更新用取り込み結果リスト [The list of import result for update status]</param>
        /// <param name="listRetry">リトライリスト [The list of retry]</param>
        /// <returns>APIリクエスト用モール伝票リスト [The list of mall order for API request]</returns>
        private IList<ErsMallOrder> GetMallOrderListForAPIRequest(IList<MallOrderResult> listResultForUpdate, IList<string> listRetry)
        {
            var listRet = new List<ErsMallOrder>();

            if (listRetry.Count == 0)
            {
                foreach (var result in listResultForUpdate)
                {
                    listRet.Add(result.objMallOrder);
                }

                return listRet;
            }

            // リトライの場合はリトライ分 [When retry, contains retry]
            foreach (var result in listResultForUpdate)
            {
                if (listRetry.Contains(result.objMallOrder.d_no))
                {
                    listRet.Add(result.objMallOrder);
                }
            }

            return listRet;
        }
        #endregion

        #region ERSステータス更新 [Update status for ERS]
        /// <summary>
        /// ERSステータス更新 [Update status for ERS]
        /// </summary>
        /// <param name="result">取り込み結果 [Import result]</param>
        /// <param name="updateStatus">更新ステータス [Update status]</param>
        private void UpdateStatusErs(MallOrderResult result, EnumMallOrderStatus? updateStatus)
        {
            // モール伝票ステータス更新 [Update mall order status]
            this.UpdateOrderStatus(result.objMallOrder, updateStatus);
        }

        /// <summary>
        /// モール伝票ステータス更新 [Update mall order status]
        /// </summary>
        /// <param name="objMallOrder">モール伝票ヘッダ [Header of mall order]</param>
        /// <param name="updateStatus">更新ステータス [Update status]</param>
        protected void UpdateOrderStatus(ErsMallOrder objMallOrder, EnumMallOrderStatus? updateStatus)
        {
            if (updateStatus == null)
            {
                return;
            }

            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderRepository();

            var objOld = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderWithID(objMallOrder.id.Value);

            objMallOrder.order_status_id = (int)updateStatus;

            // 更新 [Update]
            repository.Update(objOld, objMallOrder);
        }
        #endregion
    }
}
