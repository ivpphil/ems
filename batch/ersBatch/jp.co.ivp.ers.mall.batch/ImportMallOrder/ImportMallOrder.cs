﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using com.hunglead.harc;
using jp.co.ivp.ers.mall.batch.ImportMallOrder.mall;

namespace jp.co.ivp.ers.mall.batch.ImportMallOrder
{
    /// <summary>
    /// モール伝票取り込みメイン [Import mall order main]
    /// </summary>
    public class ImportMallOrder
    {
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="argDictinary">引数 [Arguments]</param>
        public void Execute(IDictionary<string, object> argDictinary)
        {
            // 実行日時取得 [Get execute datetime]
            var dateExecute = DateTime.Now;

            // 指定受注番号リスト取得 [Get the list of specified order code]
            var dicSpecifiedOrderCode = this.GetSpecifiedOrderCodeList(argDictinary);

            // HARCログイン [Login to HARC]
            var request = ErsMallFactory.ersMallCommonFactory.GetHarcLoginStgy().HarcLogin();

            // 取り込みリスト取得 [Get the list of import]
            var listImportMallOrder = this.GetImportList(dateExecute, dicSpecifiedOrderCode);

            // 取り込み [Import (API)]
            var resultImport = this.ImportOrder(request, listImportMallOrder, dateExecute);

            if (!string.IsNullOrEmpty(resultImport))
            {
                throw new Exception(resultImport);
            }
        }

        #region 指定受注番号リスト取得 [Get the list of specified order code]
        /// <summary>
        /// 指定受注番号リスト取得 [Get the list of specified order number]
        /// </summary>
        /// <param name="argDictinary">引数 [Arguments]</param>
        /// <returns>指定受注番号リスト [The list of specified order number]</returns>
        protected IDictionary<int, IList<string>> GetSpecifiedOrderCodeList(IDictionary<string, object> argDictinary)
        {
            if (argDictinary == null)
            {
                return null;
            }
            if (!argDictinary.ContainsKey("order_code"))
            {
                return null;
            }

            var dicRet = new Dictionary<int, IList<string>>();

            var arrOrderInfos = argDictinary["order_code"].ToString().Split(',');

            foreach (var data in arrOrderInfos)
            {
                var arrOrderInfo = data.Split(':');

                if (arrOrderInfo.Length != 2)
                {
                    continue;
                }

                var site_id = Convert.ToInt32(arrOrderInfo[0]);

                // チェック用 [For check]
                var shopKbn = ErsMallFactory.ersSiteFactory.GetSiteData().GetMallShopKbnFromSiteId(site_id);

                if (!dicRet.ContainsKey(site_id))
                {
                    dicRet.Add(site_id, new List<string>());
                }

                dicRet[site_id].Add(arrOrderInfo[1]);
            }

            return dicRet;
        }
        #endregion

        #region 取り込みリスト取得 [Get the list of import]
        /// <summary>
        /// 取り込みリスト取得 [Get the list of import]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <param name="dicSpecifiedOrderCode">指定受注番号リスト [The list of specified order number]</param>
        /// <returns>取り込みリスト [The list of import]</returns>
        protected IList<ImportMallOrderBase> GetImportList(DateTime dateExecute, IDictionary<int, IList<string>> dicSpecifiedOrderCode)
        {
            var siteData = ErsMallFactory.ersSiteFactory.GetSiteData();
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();
            var listRet = new List<ImportMallOrderBase>();

            foreach (var siteId in siteData.dicSiteData.Keys)
            {
                // モール店舗タイプ取得（サイトIDから） [Get mall shop type (from Site id)]
                var shopKbn = siteData.GetMallShopKbnFromSiteId(siteId);

                switch (shopKbn)
                {
                    // 楽天 [Rakuten]
                    case EnumMallShopKbn.RAKUTEN:
                        if (setup.doImportRakuten && !this.isWithinExcludeDateTime(EnumMallShopKbn.RAKUTEN, dateExecute))
                        {
                            IList<string> listSpecifiedOrderCode = (dicSpecifiedOrderCode != null && dicSpecifiedOrderCode.ContainsKey(siteId)) ? dicSpecifiedOrderCode[siteId] : null;

                            // 取得 [Get] = 1 : 新規 [New]
                            // 更新 [Update] = 4 : 発送待ち [Waiting shipment]
                            // 逆取り込み [Reverse import]
                            listRet.Add(new ImportMallOrderRakuten(siteId, EnumMallShopKbn.RAKUTEN, EnumMallOrderStatus.New, EnumMallOrderStatus.WaitingShipment, true, dateExecute, listSpecifiedOrderCode));
                        }
                        break;

                    // Yahoo! [Yahoo!]
                    case EnumMallShopKbn.YAHOO:
                        if (setup.doImportYahoo && !this.isWithinExcludeDateTime(EnumMallShopKbn.YAHOO, dateExecute))
                        {
                            IList<string> listSpecifiedOrderCode = (dicSpecifiedOrderCode != null && dicSpecifiedOrderCode.ContainsKey(siteId)) ? dicSpecifiedOrderCode[siteId] : null;

                            // 取得 [Get] = 1 : 新規 [New]
                            // 更新 [Update] = 3 : 処理中 [Processing]
                            listRet.Add(new ImportMallOrderYahoo(siteId, EnumMallShopKbn.YAHOO, EnumMallOrderStatus.New, EnumMallOrderStatus.Processing, false, dateExecute, listSpecifiedOrderCode));
                        }
                        break;

                    // Amazon [amazon]
                    case EnumMallShopKbn.AMAZON:
                        if (setup.doImportAmazon && !this.isWithinExcludeDateTime(EnumMallShopKbn.AMAZON, dateExecute))
                        {
                            IList<string> listSpecifiedOrderCode = (dicSpecifiedOrderCode != null && dicSpecifiedOrderCode.ContainsKey(siteId)) ? dicSpecifiedOrderCode[siteId] : null;

                            // 取得 [Get] = null
                            // 更新 [Update] = null
                            listRet.Add(new ImportMallOrderAmazon(siteId, EnumMallShopKbn.AMAZON, null, null, false, dateExecute, listSpecifiedOrderCode));
                        }
                        break;
                }
            }

            return listRet;
        }

        /// <summary>
        /// 除外日時内判定 [Judgement within exclude DateTime]
        /// </summary>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="dateNow">現在日時 [DateTime Now]</param>
        /// <returns>True : 除外 [Exclude] / False : 除外しない [Not exlude]</returns>
        protected bool isWithinExcludeDateTime(EnumMallShopKbn? shopKbn, DateTime dateNow)
        {
            return ErsMallFactory.ersMallStopTimeFactory.GetIsWithinExcludeDateTimeStgy().IsWithin(shopKbn, EnumMallFuncType.Order, dateNow);
        }
        #endregion

        #region 取り込み [Import]
        /// <summary>
        /// 取り込み [Import]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="listImportMallOrder">取り込みリスト [The list of import]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <returns>エラーログ [Error log]</returns>
        protected string ImportOrder(HarcApiRequest request, IList<ImportMallOrderBase> listImportMallOrder, DateTime dateExecute)
        {
            var exceptions = new ConcurrentQueue<ImportMallOrderException>();

            Parallel.ForEach(listImportMallOrder, import =>
            {
                try
                {
                    // 取り込み [Import]
                    import.ImportOrder(request);

                    // 取り込み結果取り込み [Import the result of import]
                    import.ImportResult();

                    try
                    {
                        // 在庫更新 [Update stock]
                        import.UpdateStock(request);

                        // ステータス更新 [Update status]
                        import.UpdateStatus(request);

                        // マージステータス更新 [Update merge status]
                        import.UpdateMergeStatus();
                    }
                    catch (Exception e)
                    {
                        // 例外追加 [Add the exception]
                        exceptions.Enqueue(new ImportMallOrderException(e, DateTime.Now, import.siteId, import.shopKbn));
                    }

                    // 取り込み管理登録 [Register import management]
                    import.RegisterOrderImport(dateExecute);
                }
                catch (Exception e)
                {
                    // 例外追加 [Add the exception]
                    exceptions.Enqueue(new ImportMallOrderException(e, DateTime.Now, import.siteId, import.shopKbn));
                }
            });

            string errorLog = string.Empty;

            // 例外エラーをまとめる [Aggregate the exception]
            if (exceptions.Count > 0)
            {
                foreach (var e in exceptions)
                {
                    errorLog += e.ToString() + Environment.NewLine;
                }
            }

            // エラーログをまとめる [Aggregate the error log]
            foreach (var import in listImportMallOrder)
            {
                errorLog += import.errorLog + (string.IsNullOrEmpty(import.errorLog) ? string.Empty : Environment.NewLine);
            }

            return errorLog;
        }
        #endregion
    }
}
