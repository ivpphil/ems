﻿using System;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.mall.batch.ImportMallOrder
{
    /// <summary>
    /// 取り込み例外 [Exception of import]
    /// </summary>
    class ImportMallOrderException
    {
        #region プロパティ [Properties]
        /// <summary>
        /// 例外 [Exception]
        /// </summary>
        protected Exception exception { get; set; }

        /// <summary>
        /// 例外発生日時 [Datetime of exception]
        /// </summary>
        protected DateTime? dateException { get; set; }

        /// <summary>
        /// サイトID [Type of shop]
        /// </summary>
        protected int? siteId { get; set; }

        /// <summary>
        /// 店舗タイプ [Type of shop]
        /// </summary>
        protected EnumMallShopKbn? shopKbn { get; set; }
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="e"></param>
        /// <param name="dateException">例外発生日時 [Datetime of exception]</param>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public ImportMallOrderException(Exception exception, DateTime? dateException, int? siteId, EnumMallShopKbn? shopKbn)
        {
            this.exception = exception;
            this.dateException = dateException;
            this.siteId = siteId;
            this.shopKbn = shopKbn;
        }
        #endregion

        #region 文字列化 [To string]
        /// <summary>
        /// 文字列化 [To string]
        /// </summary>
        /// <returns>文字列 [String]</returns>
        public override string ToString()
        {
            return string.Format("[{0}] site_id = {1}, mall_shop_kbn = {2}\r\n{3}\r\n", this.dateException, this.siteId, (int)this.shopKbn, this.exception.ToString());
        }
        #endregion
    }
}
