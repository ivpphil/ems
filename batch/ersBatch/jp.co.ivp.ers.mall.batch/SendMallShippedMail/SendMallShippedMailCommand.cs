﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch.SendShippedMail;

namespace jp.co.ivp.ers.mall.batch.SendMallShippedMail
{
    public class SendMallShippedMailCommand
        : SendShippedMailCommand
    {
        protected override IList<order.ErsOrder> GetListOrder()
        {
            // モール伝票
            // -モールステータスが新着以外
            // -配送済み

            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.mall_shop_kbn_not_equal = EnumMallShopKbn.ERS;
            criteria.mall_order_status_not_equal = EnumMallOrderStatus.New;
            criteria.order_status_in = new[] { EnumOrderStatusType.DELIVERED };
            criteria.shipped_mail = EnumSentFlg.NotSent;

            return repository.Find(criteria);
        }

        protected override void SendMail(order.ErsOrder order, ers.batch.SendShippedMail.model.ShippedMailModel command)
        {
            ErsMallFactory.ersMallMailFactory.GetSendDeliveredMailStgy().SendMail(order, command);
        }
    }
}
