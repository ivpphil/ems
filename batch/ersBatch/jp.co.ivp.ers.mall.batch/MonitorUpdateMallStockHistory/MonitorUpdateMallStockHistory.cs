﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using com.hunglead.harc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.api;
using jp.co.ivp.ers.mall.batch.MonitorUpdateMallStockHistory.mall;
using jp.co.ivp.ers.mall.stock_error;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.batch.MonitorUpdateMallStockHistory
{
    /// <summary>
    /// モール在庫更新履歴監視メイン [Monitor update mall stock history main]
    /// </summary>
    public class MonitorUpdateMallStockHistory
    {
        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="argDictinary">引数 [Arguments]</param>
        public void Execute(IDictionary<string, object> argDictinary)
        {
            // HARCログイン [Login to HARC]
            var request = ErsMallFactory.ersMallCommonFactory.GetHarcLoginStgy().HarcLogin();

            var listResult = new List<MonitorUpdateStockHistoryResultContainer>();

            // 処理中ステータス状況確認 [Monitor the status of processing]
            var resultProcessingStatus = this.MonitorProcessingStatus(request);

            if (resultProcessingStatus != null)
            {
                listResult.Add(resultProcessingStatus.Value);
            }

            // 履歴確認 [Monitor the history]
            var resulHistory = this.MonitorHistory(request);

            if (resulHistory != null)
            {
                listResult.Add(resulHistory.Value);
            }

            if (listResult.Count == 0)
            {
                return;
            }

            var listNormalResult = new List<string>();
            var listIgnoreResult = new List<string>();

            // 結果統合 [Integrate results]
            foreach (var result in listResult)
            {
                if (!string.IsNullOrEmpty(result.normalError))
                {
                    listNormalResult.Add(result.normalError);
                }
                if (!string.IsNullOrEmpty(result.ignoreError))
                {
                    listIgnoreResult.Add(result.ignoreError);
                }
            }


            var errorLog = (listNormalResult.Count == 0 ? null : String.Join(Environment.NewLine, listNormalResult));

            if (errorLog.HasValue())
            {
                throw new Exception(errorLog);
            }
        }
        #endregion

        #region 処理中ステータス状況確認 [Monitor the status of processing]
        /// <summary>
        /// 処理中ステータス状況確認 [Monitor the status of processing]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <returns>在庫更新履歴結果コンテナ [Container for monitor update stock]</returns>
        protected MonitorUpdateStockHistoryResultContainer? MonitorProcessingStatus(HarcApiRequest request)
        {
            // 処理中リスト取得 [Get the list of processing data]
            var listFind = this.GetProcessingList();

            if (listFind == null)
            {
                return null;
            }

            var listResult = new List<string>();
            var listIgnoreResult = new List<string>();

            foreach (var data in listFind)
            {
                // 処理中データ確認 [Check the data of processing]
                var result = this.CheckProcessingData(request, data);

                if (result != null)
                {
                    if (!string.IsNullOrEmpty(result.Value.normalError))
                    {
                        listResult.Add(result.Value.normalError);
                    }
                    if (!string.IsNullOrEmpty(result.Value.ignoreError))
                    {
                        listIgnoreResult.Add(result.Value.ignoreError);
                    }
                }
            }

            if (listResult.Count == 0 && listIgnoreResult.Count == 0)
            {
                return null;
            }

            MonitorUpdateStockHistoryResultContainer resultContainer = default(MonitorUpdateStockHistoryResultContainer);

            resultContainer.normalError = (listResult.Count == 0 ? null : String.Join(Environment.NewLine, listResult));
            resultContainer.ignoreError = (listIgnoreResult.Count == 0 ? null : String.Join(Environment.NewLine, listIgnoreResult));

            return resultContainer;
        }

        /// <summary>
        /// 処理中リスト取得 [Get the list of processing data]
        /// </summary>
        /// <returns>処理中リスト [The list of processing data]</returns>
        protected IList<ErsMallStockError> GetProcessingList()
        {
            var repository = ErsMallFactory.ersMallStockErrorFactory.GetErsMallStockErrorRepository();
            var criteria = ErsMallFactory.ersMallStockErrorFactory.GetErsMallStockErrorCriteria();

            // 処理中 [Processing]
            criteria.stock_status = EnumMallStockPostingSlipStatus.Processing;

            if (repository.GetRecordCount(criteria) == 0)
            {
                return null;
            }

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            return repository.Find(criteria);
        }

        /// <summary>
        /// 処理中データ確認 [Check the data of processing]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="objMallStockError">モール連携在庫更新エラー [Mall update stock error]</param>
        /// <returns>在庫更新履歴結果コンテナ [Container for monitor update stock]</returns>
        protected MonitorUpdateStockHistoryResultContainer? CheckProcessingData(HarcApiRequest request, ErsMallStockError objMallStockError)
        {
            var result = default(MonitorUpdateStockHistoryResultContainer);
            var param = ErsMallFactory.ersMallAPIFactory.GetGetStockPostingSlipAPIParam();

            Dictionary<string, object> dicResult = null;

            try
            {
                // 在庫更新履歴検索取得 [Get the detail of history of update stock]
                dicResult = ErsMallFactory.ersMallAPIFactory.GetGetStockPostingSlipAPI(param).GetStockPostingSlipDetail(request, objMallStockError.stock_id);
            }
            catch (APIFailedException e)
            {
                result.normalError = ErsResources.GetMessage("104000", objMallStockError.site_id, (int)objMallStockError.mall_shop_kbn, objMallStockError.id, objMallStockError.stock_id, e.ToString());
                return result;
            }


            // 在庫更新ステータス取得 [Get the status of update stock]
            var status = (EnumMallStockPostingSlipStatus)Enum.ToObject(typeof(EnumMallStockPostingSlipStatus), dicResult["status"]);

            switch (status)
            {
                // 成功 [Success]
                case EnumMallStockPostingSlipStatus.Success:
                    // 在庫更新エラー更新 [Update mall update stock error]
                    ErsMallFactory.ersMallStockErrorFactory.GetRegisterAndUpdateMallStockErrorStgy().Update(dicResult, objMallStockError);
                    break;

                // 失敗 [Failure]
                case EnumMallStockPostingSlipStatus.Failure:
                    // 在庫更新エラー更新 [Update mall update stock error]
                    var objTmp = ErsMallFactory.ersMallStockErrorFactory.GetRegisterAndUpdateMallStockErrorStgy().Update(dicResult, objMallStockError);

                    var errorMessage = ErsResources.GetMessage("104001", objTmp.site_id, (int)objTmp.mall_shop_kbn, objTmp.id, objTmp.stock_id, objTmp.stock_result_message, objTmp.stock_product_info);

                    // 除外日時チェック [Check exclude datetime]
                    if (!MonitorUpdateStockHistoryBase.IsExcludeDateTime(objTmp.mall_shop_kbn, objTmp.stock_end))
                    {
                        // 無視エラーチェック [Check ignore error]
                        if (MonitorUpdateStockHistoryBase.IsIgnoreError(objTmp.mall_shop_kbn, objTmp.stock_result_message))
                        {
                            result.ignoreError = errorMessage;
                        }
                        else
                        {
                            result.normalError = errorMessage;
                        }
                    }

                    return result;

                // 処理中 [Processing]
                case EnumMallStockPostingSlipStatus.Processing:
                    var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

                    // 経過時間取得（分） [Get the elapsed time (Minutes)]
                    TimeSpan span = DateTime.Now - objMallStockError.stock_start.Value;

                    if (span.TotalMinutes > setup.monitorUpdateMallStockHistoryElapsedProcessingMin.Value)
                    {
                        result.normalError = ErsResources.GetMessage("104002", Convert.ToInt32(Math.Floor(span.TotalMinutes)), objMallStockError.site_id, (int)objMallStockError.mall_shop_kbn, objMallStockError.id, objMallStockError.stock_id, objMallStockError.stock_product_info);
                        return result;
                    }
                    break;
            }

            return null;
        }
        #endregion

        #region 履歴確認 [Monitor the history]
        /// <summary>
        /// 履歴確認 [Monitor the history]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <returns>在庫更新履歴結果コンテナ [Container for monitor update stock]</returns>
        protected MonitorUpdateStockHistoryResultContainer? MonitorHistory(HarcApiRequest request)
        {
            // 監視リスト取得 [Get the list of monitor]
            var listMonitor = this.GetMonitorList();

            var errorResults = new ConcurrentQueue<MonitorUpdateStockHistoryResultContainer>();

            var dateNow = DateTime.Now;
            var dateTmp = ErsMallFactory.ersMallStockErrorFactory.GetObtainLastSearchToStgy().Obtain();
            var dateFrom = dateTmp != null ? dateTmp : dateNow.GetStartForSearch();

            Parallel.ForEach(listMonitor, monitor =>
            {
                // 監視 [Monitor]
                var result = monitor.Monitor(request, dateFrom, dateNow);

                if (result != null)
                {
                    // エラー結果追加 [Add the error result]
                    errorResults.Enqueue(result.Value);
                }
            });

            MonitorUpdateStockHistoryResultContainer resultContainer = default(MonitorUpdateStockHistoryResultContainer);

            // 例外エラーをまとめる [Aggregate the error logs]
            if (errorResults.Count > 0)
            {
                foreach (var error in errorResults)
                {
                    resultContainer.normalError += (string.IsNullOrEmpty(error.normalError) ? string.Empty : error.normalError + Environment.NewLine);
                    resultContainer.ignoreError += (string.IsNullOrEmpty(error.ignoreError) ? string.Empty : error.ignoreError + Environment.NewLine);
                }
            }

            // 検索日時登録 [Register the datetime of search]
            this.RegisterSearchDatetime(listMonitor, dateFrom, dateNow);

            if (errorResults.Count > 0)
            {
                return resultContainer;
            }

            return null;
        }

        /// <summary>
        /// 監視リスト取得 [Get the list of monitor]
        /// </summary>
        /// <returns>監視リスト [The list of monitor]</returns>
        protected IList<MonitorUpdateStockHistoryBase> GetMonitorList()
        {
            var listRet = new List<MonitorUpdateStockHistoryBase>();

            // -1 : 実行中 [Processing] 
            listRet.Add(new MonitorUpdateStockHistoryProcessing(EnumMallStockPostingSlipStatus.Processing));

            // 1 : 異常終了 [Failure]
            listRet.Add(new MonitorUpdateStockHistoryFailure(EnumMallStockPostingSlipStatus.Failure));

            return listRet;
        }

        /// <summary>
        /// 検索日時登録 [Register the datetime of search]
        /// </summary>
        /// <param name="listMonitor">監視リスト [The list of monitor]</param>
        /// <param name="dateSearchFrom">検索日時（FROM） [Datetime of search (From)]</param>
        /// <param name="dateSearchTo">検索日時（TO） [Datetime of search (To)]</param>
        protected void RegisterSearchDatetime(IList<MonitorUpdateStockHistoryBase> listMonitor, DateTime? dateFrom, DateTime? dateTo)
        {
            int count = 0;

            foreach (var monitor in listMonitor)
            {
                // APIエラー [API error]
                if (monitor.listResult == null)
                {
                    return;
                }

                // 結果数を合計 [Sum the results]
                count += monitor.listResult.Count;
            }

            if (count == 0)
            {
                // 登録（エラーなし） [Register (No error)]
                ErsMallFactory.ersMallStockErrorFactory.GetRegisterAndUpdateMallStockErrorStgy().RegisterNoError(dateFrom, dateTo);
            }
        }
        #endregion
    }
}
