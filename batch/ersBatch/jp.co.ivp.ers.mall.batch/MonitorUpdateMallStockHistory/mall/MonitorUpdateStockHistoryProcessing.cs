﻿
namespace jp.co.ivp.ers.mall.batch.MonitorUpdateMallStockHistory.mall
{
    /// <summary>
    /// モール在庫更新履歴監視クラス（処理中） [Class for monitor update mall stock (Processing)]
    /// </summary>
    public class MonitorUpdateStockHistoryProcessing
        : MonitorUpdateStockHistoryBase
    {
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="getStatus">取得ステータス [Get status]</param>
        public MonitorUpdateStockHistoryProcessing(EnumMallStockPostingSlipStatus? getStatus)
            : base(getStatus)
        {
        }
        #endregion
    }
}
