﻿using System;
using System.Collections.Generic;

namespace jp.co.ivp.ers.mall.batch.MonitorUpdateMallStockHistory.mall
{
    /// <summary>
    /// モール在庫更新履歴監視クラス（異常終了） [Class for monitor update mall stock (Failure)]
    /// </summary>
    public class MonitorUpdateStockHistoryFailure
        : MonitorUpdateStockHistoryBase
    {
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="getStatus">取得ステータス [Get status]</param>
        public MonitorUpdateStockHistoryFailure(EnumMallStockPostingSlipStatus? getStatus)
            : base(getStatus)
        {
        }
        #endregion

        #region 監視 [Monitor]
        /// <summary>
        /// エラー登録 [Register error]
        /// </summary>
        /// <param name="listResult">結果リスト [List of results]</param>
        /// <param name="dateSearchFrom">検索日時（FROM） [Datetime of search (From)]</param>
        /// <param name="dateSearchTo">検索日時（TO） [Datetime of search (To)]</param>
        /// <returns>モール在庫更新履歴結果コンテナ [Container for monitor update mall stock]</returns>
        protected override MonitorUpdateStockHistoryResultContainer? RegisterError(List<Dictionary<string, object>> listResult, DateTime? dateFrom, DateTime? dateTo)
        {
            var stgy = ErsMallFactory.ersMallStockErrorFactory.GetRegisterAndUpdateMallStockErrorStgy();

            var listError = new List<string>();
            var listIgnoreError = new List<string>();

            foreach (var result in listResult)
            {
                // エラー登録 [Register error]
                var objMallStockError = stgy.Register(result, dateFrom, dateTo);

                if (objMallStockError != null)
                {
                    var siteId = objMallStockError.site_id.HasValue ? Convert.ToString(objMallStockError.site_id) : null;
                    var shopKbn = objMallStockError.mall_shop_kbn.HasValue ? Convert.ToString((int)objMallStockError.mall_shop_kbn) : null;
                    var errorMessage = ErsResources.GetMessage("104001", siteId, shopKbn, objMallStockError.id, objMallStockError.stock_id, objMallStockError.stock_result_message, objMallStockError.stock_product_info);

                    // 除外日時チェック [Check exclude datetime]
                    if (!MonitorUpdateStockHistoryBase.IsExcludeDateTime(objMallStockError.mall_shop_kbn, objMallStockError.stock_end))
                    {
                        // 無視エラーチェック [Check ignore error]
                        if (MonitorUpdateStockHistoryBase.IsIgnoreError(objMallStockError.mall_shop_kbn, objMallStockError.stock_result_message))
                        {
                            listIgnoreError.Add(errorMessage);
                        }
                        else
                        {
                            listError.Add(errorMessage);
                        }
                    }
                }
            }

            if (listError.Count == 0 && listIgnoreError.Count == 0)
            {
                return null;
            }

            // エラーあり [Exists any error]
            MonitorUpdateStockHistoryResultContainer resultContainer = default(MonitorUpdateStockHistoryResultContainer);

            resultContainer.normalError = (listError.Count == 0 ? null : String.Join(Environment.NewLine, listError));
            resultContainer.ignoreError = (listIgnoreError.Count == 0 ? null : String.Join(Environment.NewLine, listIgnoreError));

            return resultContainer;
        }
        #endregion
    }
}
