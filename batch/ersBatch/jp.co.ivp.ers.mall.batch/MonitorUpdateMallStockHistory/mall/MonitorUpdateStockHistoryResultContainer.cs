﻿
namespace jp.co.ivp.ers.mall.batch.MonitorUpdateMallStockHistory.mall
{
    /// <summary>
    /// モール在庫更新履歴結果コンテナ [Container for monitor update mall stock]
    /// </summary>
    public struct MonitorUpdateStockHistoryResultContainer
    {
        /// <summary>
        /// 通常エラー [Normal error]
        /// </summary>
        public string normalError { get; set; }

        /// <summary>
        /// 無視エラー [Ignore error]
        /// </summary>
        public string ignoreError { get; set; }
    }
}
