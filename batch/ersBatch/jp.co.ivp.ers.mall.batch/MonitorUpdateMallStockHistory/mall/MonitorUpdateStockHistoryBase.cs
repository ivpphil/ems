﻿using System;
using System.Collections.Generic;
using com.hunglead.harc;
using jp.co.ivp.ers.mall.api;

namespace jp.co.ivp.ers.mall.batch.MonitorUpdateMallStockHistory.mall
{
    /// <summary>
    /// モール在庫更新履歴監視クラス（基底） [Class for monitor update mall stock (Base)]
    /// </summary>
    public class MonitorUpdateStockHistoryBase
    {
        #region 内部パラメータ [Internal parameters]
        /// <summary>
        /// 無視エラーメッセージリスト（楽天） [List of ignore error message (Rakuten)]
        /// </summary>
        protected static IList<string> listIgnoreErrorMessageRakuten = new List<string>()
        {
            "previous file still exists",       // 更新中 [During updating]
            "code=W21-202",                     // 存在しない [Doesn't exists]
            "Could not connect to host",        // 接続不可 [Could not connect to host]
            "looks like we got no XML document" // レスポンスなし [No response]
        };

        /// <summary>
        /// 無視エラーメッセージリスト（Yahoo!） [List of ignore error message (Yahoo!)]
        /// </summary>
        protected static IList<string> listIgnoreErrorMessageYahoo = new List<string>()
        {
        };

        /// <summary>
        /// 無視エラーメッセージリスト（Amazon） [List of ignore error message (Amazon)]
        /// </summary>
        protected static IList<string> listIgnoreErrorMessageAmazon = new List<string>()
        {
            "Internal Error",                   // 内部エラー [Internal error]
            "Unable to construct MarketplaceWebService_Model_ErrorResponse"     // 内部エラー [Internal error]
        };
        #endregion

        #region 基本パラメータ [Basic parameters]
        /// <summary>
        /// 取得ステータス [Get status]
        /// </summary>
        public virtual EnumMallStockPostingSlipStatus? getStatus { get; protected set; }
        #endregion

        #region 結果パラメータ [Result parameters]
        /// <summary>
        /// 結果リスト [List of results]
        /// </summary>
        public virtual List<Dictionary<string, object>> listResult { get; protected set; }
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="getStatus">取得ステータス [Get status]</param>
        public MonitorUpdateStockHistoryBase(EnumMallStockPostingSlipStatus? getStatus)
        {
            this.getStatus = getStatus;
        }
        #endregion

        #region 監視 [Monitor]
        /// <summary>
        /// 監視 [Monitor]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="dateSearchFrom">検索日時（FROM） [Datetime of search (From)]</param>
        /// <param name="dateSearchTo">検索日時（TO） [Datetime of search (To)]</param>
        /// <returns>在庫更新履歴結果コンテナ [Container for monitor update stock]</returns>
        public virtual MonitorUpdateStockHistoryResultContainer? Monitor(HarcApiRequest request, DateTime? dateFrom, DateTime? datetTo)
        {
            var param = ErsMallFactory.ersMallAPIFactory.GetFindStockPostingSlipAPIParam();

            // APIパラメータセット [Set the parameter for API]
            param.searchFrom = dateFrom;
            param.searchTo = datetTo;

            try
            {
                // 在庫更新履歴検索 [Search history of update stock]
                this.listResult = ErsMallFactory.ersMallAPIFactory.GetFindStockPostingSlipAPI(param).FindStockPostingSlip(request, this.getStatus);
            }
            catch (APIFailedException e)
            {
                MonitorUpdateStockHistoryResultContainer result = default(MonitorUpdateStockHistoryResultContainer);
                result.normalError = ErsResources.GetMessage("104003", (int)this.getStatus, e.ToString());
                return result;
            }

            // エラー登録 [Register error]
            return this.RegisterError(this.listResult, param.searchFrom, param.searchTo);
        }

        /// <summary>
        /// エラー登録 [Register error]
        /// </summary>
        /// <param name="listResult">結果リスト [List of results]</param>
        /// <param name="dateSearchFrom">検索日時（FROM） [Datetime of search (From)]</param>
        /// <param name="dateSearchTo">検索日時（TO） [Datetime of search (To)]</param>
        /// <returns>在庫更新履歴結果コンテナ [Container for monitor update stock]</returns>
        protected virtual MonitorUpdateStockHistoryResultContainer? RegisterError(List<Dictionary<string, object>> listResult, DateTime? dateFrom, DateTime? dateTo)
        {
            var stgy = ErsMallFactory.ersMallStockErrorFactory.GetRegisterAndUpdateMallStockErrorStgy();

            foreach (var result in listResult)
            {
                // エラー登録 [Register error]
                stgy.Register(result, dateFrom, dateTo);
            }

            return null;
        }
        #endregion

        #region 無視エラーチェック [Check ignore error]
        /// <summary>
        /// 無視エラーチェック [Check ignore error]
        /// </summary>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="errorMessage">エラーメッセージ [Error message]</param>
        /// <returns>true : 無視するエラー [Ignore error] / false : 通常エラー [Normal error]</returns>
        public static bool IsIgnoreError(EnumMallShopKbn? shopKbn, string errorMessage)
        {
            if (!shopKbn.HasValue)
            {
                return false;
            }

            IDictionary<EnumMallShopKbn, IList<string>> dicMessageList = new Dictionary<EnumMallShopKbn, IList<string>>()
            {
                { EnumMallShopKbn.RAKUTEN, listIgnoreErrorMessageRakuten },
                { EnumMallShopKbn.YAHOO, listIgnoreErrorMessageRakuten },
                { EnumMallShopKbn.AMAZON, listIgnoreErrorMessageYahoo },
            };

            if (dicMessageList.ContainsKey(shopKbn.Value))
            {
                foreach (var message in dicMessageList[shopKbn.Value])
                {
                    if (errorMessage.Contains(message))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        #endregion

        #region 除外日時チェック [Check exclude datetime]
        /// <summary>
        /// 除外日時チェック [Check exclude datetime]
        /// </summary>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="stock_end">在庫リクエスト終了日時 [Request finish datetime]</param>
        /// <returns>true : 除外する日時 [Exclude datetime] / false : 除外しない日時 [Not exclude datetime]</returns>
        public static bool IsExcludeDateTime(EnumMallShopKbn? shopKbn, DateTime? stock_end)
        {
            if (!shopKbn.HasValue || !stock_end.HasValue)
            {
                return false;
            }

            // モール処理除外日時内判定 [Judgement within exclude dateTime for mall function]
            return ErsMallFactory.ersMallStopTimeFactory.GetIsWithinExcludeDateTimeStgy().IsWithin(shopKbn, EnumMallFuncType.Stock, stock_end.Value);
        }
        #endregion
    }
}
