﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.batch.OperateMallProductImage.specification;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage.strategy
{
    /// <summary>
    /// 登録用モール商品画像テンポラリ取得 [Get the mall product image for operate]
    /// </summary>
    public class ObtainRegisterMallProductImageTmpStgy
    {
        /// <summary>
        /// 登録用モール商品画像テンポラリ取得 [Get the mall product image for operate]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateFrom">検索日時FROM [Search datetime (FROM)]</param>
        /// <param name="dateTo">検索日時TO [Search datetime (TO)]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>商品画像リスト [List of merchandise image]</returns>
        public IList<ErsMallProductImageTmp> Obtain(int? siteId, DateTime dateFrom, DateTime dateTo, IList<string> listSpecifiedScode)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageTmpRepository();
            var criteria = this.GetCriteria(siteId, dateFrom, dateTo, listSpecifiedScode);

            if (repository.GetRecordCount(criteria) == 0)
            {
                return null;
            }

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            return repository.Find(criteria);
        }

        /// <summary>
        /// クライテリア取得 [Get the criteria]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateFrom">検索日時FROM [Search datetime (FROM)]</param>
        /// <param name="dateTo">検索日時TO [Search datetime (TO)]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>クライテリア [Criteria]</returns>
        protected virtual ErsMallProductImageTmpCriteria GetCriteria(int? siteId, DateTime dateFrom, DateTime dateTo, IList<string> listSpecifiedScode)
        {
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageTmpCriteria();

            // モール商品画像登録用 [For register mall product images]
            criteria.SetSearchForRegister(dateFrom, dateTo);

            if (siteId != null)
            {
                criteria.site_id = siteId;
            }

            if (listSpecifiedScode != null)
            {
                criteria.scode_in = listSpecifiedScode;
            }

            return criteria;
        }
    }
}
