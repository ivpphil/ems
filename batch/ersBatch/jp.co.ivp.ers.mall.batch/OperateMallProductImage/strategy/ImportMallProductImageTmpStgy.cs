﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using jp.co.ivp.ers.common;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage.strategy
{
    /// <summary>
    /// モール商品画像テンポラリインポート [Import mall product image temporary]
    /// </summary>
    public class ImportMallProductImageTmpStgy
    {
        #region クラス [Class]
        /// <summary>
        /// インポート画像タイプコンテナ [Container of import image]
        /// </summary>
        protected class ImportImageTypeContainer
        {
            /// <summary>
            /// ファイルパス [File path]
            /// </summary>
            public string filePath { get; set; }

            /// <summary>
            /// 画像タイプ [Image type]
            /// </summary>
            public EnumMallProductImageType imageType { get; set; }

            /// <summary>
            /// 画像インデックス [Image index]
            /// </summary>
            public int imageIndex { get; set; }


            /// <summary>
            /// コンストラクタ [Constructor]
            /// </summary>
            /// <param name="filePath">ファイルパス [File path]</param>
            /// <param name="imageType">画像タイプ [Image type]</param>
            /// <param name="imageIndex">画像インデックス [Image index]</param>
            public ImportImageTypeContainer(string filePath, EnumMallProductImageType imageType, int imageIndex)
            {
                this.filePath = filePath;
                this.imageType = imageType;
                this.imageIndex = imageIndex;
            }
        }
        #endregion

        /// <summary>
        /// モール商品画像テンポラリインポート [Import mall product image temporary]
        /// </summary>
        /// <param name="container">モール商品コンテナ [Container of mall sku]</param>
        /// <param name="fromDate">FROM [FROM]</param>
        /// <param name="toDate">TO [TO]</param>
        public void Import(MallSkuContainer container, DateTime fromDate, DateTime toDate)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var listImageTypeContainer = new List<ImportImageTypeContainer>();

            for (var i = 0; i < ErsCommonConst.MERCHANDISE_IMAGE_NUM; i++)
            {
                var index = i + 1;

                // SKU [SKU]
                listImageTypeContainer.Add(new ImportImageTypeContainer(string.Format(ErsCommonConst.SKU_IMAGE_PATH_FORMAT, setup.image_directory, container.scode, index), EnumMallProductImageType.Sku, index));
            }

            foreach (var imageTypeContainer in listImageTypeContainer)
            {
                // 対象ファイル情報取得 [Get target file information]
                var fileInfo = this.GetTargetFileInfo(imageTypeContainer.filePath);

                if (fileInfo != null)
                {
                    // インポートメイン [Import main]
                    this.ImportMain(container, imageTypeContainer.imageType, imageTypeContainer.imageIndex, fileInfo, fromDate, toDate);
                }
            }
        }

        #region 対象ファイル情報取得 [Get target file information]
        /// <summary>
        /// 対象ファイル情報取得 [Get target file information]
        /// </summary>
        /// <param name="filePathNoExt">ファイルパス（拡張子除く） [File path (exclude extention)]</param>
        /// <returns>ファイル情報 [File information]</returns>
        protected FileInfo GetTargetFileInfo(string filePathNoExt)
        {
            foreach (var ext in ErsCommonConst.MERCHANDISE_IMAGE_EXTENSIONS)
            {
                var filePath = filePathNoExt + ext;

                if (File.Exists(filePath))
                {
                    return new FileInfo(filePath);
                }
            }

            return null;
        }
        #endregion

        #region インポートメイン [Import main]
        /// <summary>
        /// インポートメイン [Import main]
        /// </summary>
        /// <param name="container">モール商品コンテナ [Container of mall sku]</param>
        /// <param name="imageType">画像タイプ [Image type]</param>
        /// <param name="index">インデックス [Index]</param>
        /// <param name="fileInfo">ファイル情報 [File information]</param>
        /// <param name="fromDate">FROM [FROM]</param>
        /// <param name="toDate">TO [TO]</param>
        protected void ImportMain(MallSkuContainer container, EnumMallProductImageType imageType, int index, FileInfo fileInfo, DateTime fromDate, DateTime toDate)
        {
            bool exists = container.image_indexes.Contains(index);

            if (!exists || this.IsTargetFile(fileInfo, fromDate, toDate))
            {
                this.Import(this.GetErsMallProductImageTmp(container.site_id.Value, container.mall_shop_kbn.Value, imageType, container.gcode, container.scode, index), exists);
            }
        }

        /// <summary>
        /// 対象ファイルかどうか [Is target file]
        /// </summary>
        /// <param name="fileInfo">ファイル情報 [File information]</param>
        /// <param name="fromDate">FROM [FROM]</param>
        /// <param name="toDate">TO [TO]</param>
        /// <returns>true : 対象 [target] ／ false : 非対象 [Not target]</returns>
        protected bool IsTargetFile(FileInfo fileInfo, DateTime fromDate, DateTime toDate)
        {
            if (fileInfo.LastWriteTime >= fromDate && fileInfo.LastWriteTime < toDate)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// モール商品画像テンポラリ取得 [Get the mall product image temporary]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Shop type]</param>
        /// <param name="imageType">画像タイプ [Image type]</param>
        /// <param name="gcode">グループコード [Group code]</param>
        /// <param name="scode">商品コード [Sku code]</param>
        /// <param name="index">インデックス [Index]</param>
        /// <returns>モール商品画像テンポラリ [The mall product image temporary]</returns>
        protected ErsMallProductImageTmp GetErsMallProductImageTmp(int siteId, EnumMallShopKbn shopKbn, EnumMallProductImageType imageType, string gcode, string scode, int index)
        {
            var objRet = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageTmp();

            objRet.active = EnumActive.Active;
            objRet.site_id = siteId;
            objRet.mall_shop_kbn = shopKbn;
            objRet.image_type = imageType;

            objRet.gcode = gcode.HasValue() ? gcode : null;
            objRet.scode = scode.HasValue() ? scode : null;

            objRet.image_index = index;

            return objRet;
        }
        #endregion

        #region モール商品画像テンポラリインポート [Import mall product image temporary]
        /// <summary>
        /// モール商品画像テンポラリインポート [Import mall product image temporary]
        /// </summary>
        /// <param name="objMallProductImageTmp">モール商品画像テンポラリ [The mall product image temporary]</param>
        /// <param name="exists">存在する [Exists]</param>
        protected void Import(ErsMallProductImageTmp objMallProductImageTmp, bool exists)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageTmpRepository();

            if (exists)
            {
                // モール商品画像テンポラリ取得 [Get the mall product image temporary]
                var objOld = this.ObtainMallProductImageTmp(objMallProductImageTmp);

                // 更新用モール商品画像テンポラリデータセット [Set the data of the mall product image temporary for Update]
                var objUpdate = this.SetMallProductDataForUpdate(objOld, objMallProductImageTmp);
                repository.Update(objOld, objUpdate);
            }
            else
            {
                // 登録用モール商品画像テンポラリデータセット [Set the data of the mall product image temporary for Insert]
                var objInsert = this.SetMallProductDataForInsert(objMallProductImageTmp);
                repository.Insert(objInsert);
            }
        }

        #region モール商品画像テンポラリ取得 [Get the mall product image temporary]
        /// <summary>
        /// モール商品画像テンポラリ取得 [Get the mall product image temporary]
        /// </summary>
        /// <param name="objMallProductImageTmp">モール商品画像テンポラリ [The mall product image temporary]</param>
        /// <returns>モール商品画像テンポラリ [The mall product image temporary]</returns>
        protected ErsMallProductImageTmp ObtainMallProductImageTmp(ErsMallProductImageTmp objMallProductImageTmp)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageTmpRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageTmpCriteria();

            criteria.site_id = objMallProductImageTmp.site_id;
            criteria.mall_shop_kbn = objMallProductImageTmp.mall_shop_kbn;
            criteria.image_type = objMallProductImageTmp.image_type;
            criteria.image_index = objMallProductImageTmp.image_index;
            criteria.gcode = objMallProductImageTmp.gcode;
            criteria.scode = objMallProductImageTmp.scode;

            var listFind = repository.Find(criteria);

            return listFind.Count == 1 ? listFind[0] : null;
        }
        #endregion

        #region 更新用モール商品画像テンポラリデータセット [Set the data of the mall product image temporary for Update]
        /// <summary>
        /// 更新用モール商品画像テンポラリデータセット [Set the data of the mall product image temporary for Update]
        /// </summary>
        /// <param name="objOldMallProductImageTmp">旧モール商品画像テンポラリ [The old mall product image temporary]</param>
        /// <param name="objNewMallProductImageTmp">新モール商品画像テンポラリ [The new mall product image temporary]</param>
        /// <returns>モール商品画像テンポラリ [The mall product image temporary]</returns>
        protected ErsMallProductImageTmp SetMallProductDataForUpdate(ErsMallProductImageTmp objOldMallProductImageTmp, ErsMallProductImageTmp objNewMallProductImageTmp)
        {
            objNewMallProductImageTmp.id = objOldMallProductImageTmp.id;
            objNewMallProductImageTmp.intime = objOldMallProductImageTmp.intime;
            objNewMallProductImageTmp.utime = DateTime.Now;
            objNewMallProductImageTmp.active = objOldMallProductImageTmp.active;

            return objNewMallProductImageTmp;
        }
        #endregion

        #region 登録用モール商品画像テンポラリデータセット [Set the data of the mall product image temporary for Insert]
        /// <summary>
        /// 登録用モール商品画像テンポラリデータセット [Set the data of the mall product image temporary for Insert]
        /// </summary>
        /// <param name="objMallProductImageTmp">モール商品画像テンポラリ [The mall product image temporary]</param>
        /// <returns>モール商品画像テンポラリ [The mall product image temporary]</returns>
        protected ErsMallProductImageTmp SetMallProductDataForInsert(ErsMallProductImageTmp objMallProductImageTmp)
        {
            objMallProductImageTmp.id = null;
            objMallProductImageTmp.intime = null;
            objMallProductImageTmp.utime = null;
            objMallProductImageTmp.active = null;

            return objMallProductImageTmp;
        }
        #endregion
        #endregion
    }
}
