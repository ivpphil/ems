﻿using System;
using System.Collections.Generic;
using System.IO;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage.strategy
{
    /// <summary>
    /// 画像最終アップロード日時更新 [Update last image upload time]
    /// </summary>
    public class UpdateLastImageUploadTimeStgy
        : ISpecificationForSQL
    {
        /// <summary>
        /// 画像最終アップロード日時更新 [Update last image upload time]
        /// </summary>
        /// <param name="listScode">商品コードリスト [The list of scode]</param>
        /// <param name="siteId">サイトID [Site id]</param>
        public void Update(IEnumerable<string> listScode, int? siteId)
        {
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();

            foreach (var data in listScode)
            {
                criteria.Clear();
                criteria.scode = data;
                criteria.site_id = siteId;

                var result = ErsRepository.UpdateSatisfying(this, criteria);
            }
        }

        /// <summary>
        /// SQL文 [SQL statement]
        /// </summary>
        /// <returns>SQL文 [SQL statement]</returns>
        public string asSQL()
        {
            return "UPDATE mall_s_master_t SET last_image_upload_time = now(), utime = now()";
        }
    }
}
