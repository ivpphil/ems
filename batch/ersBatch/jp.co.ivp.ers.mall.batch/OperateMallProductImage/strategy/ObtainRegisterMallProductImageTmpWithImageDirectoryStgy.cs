﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.batch.OperateMallProductImage.specification;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage.strategy
{
    /// <summary>
    /// 登録用モール商品画像テンポラリ（＋画像ディレクトリ）取得 [Get the branch product image (+ Image directory) for operate]
    /// </summary>
    public class ObtainRegisterMallProductImageTmpWithImageDirectoryStgy
    {
        /// <summary>
        /// カウント取得（画像ディレクトリが紐付かない） [Get the count (Not linked with image directory)]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateFrom">検索日時FROM [Search datetime (FROM)]</param>
        /// <param name="dateTo">検索日時TO [Search datetime (TO)]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>カウント [Count]</returns>
        public long ObtainNotLinkedCount(int? siteId, DateTime dateFrom, DateTime dateTo, IList<string> listSpecifiedScode)
        {
            var spec = new SearchMallProductImageTmpWithImageDirectorySpec();
            var criteria = this.GetCriteria(siteId, dateFrom, dateTo, listSpecifiedScode);

            // 紐付かない [Not linked]
            criteria.image_directory_id = null;

            return spec.GetCountData(criteria);
        }

        /// <summary>
        /// 登録用モール商品画像テンポラリ取得 [Get the branch product image for operate]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateFrom">検索日時FROM [Search datetime (FROM)]</param>
        /// <param name="dateTo">検索日時TO [Search datetime (TO)]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>商品画像リスト [List of merchandise image]</returns>
        public IList<ErsMallProductImageTmp> Obtain(int? siteId, DateTime dateFrom, DateTime dateTo, IList<string> listSpecifiedScode)
        {
            var spec = new SearchMallProductImageTmpWithImageDirectorySpec();
            var criteria = this.GetCriteria(siteId, dateFrom, dateTo, listSpecifiedScode);

            // 紐付く [Linked]
            criteria.image_directory_id_not_equal = null;

            if (spec.GetCountData(criteria) == 0)
            {
                return null;
            }

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            var listFind = spec.GetSearchData(criteria);

            var listRet = new List<ErsMallProductImageTmp>();

            foreach (var data in listFind)
            {
                var obj = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageTmp();
                obj.OverwriteWithParameter(data);
                listRet.Add(obj);
            }

            return listRet;
        }

        /// <summary>
        /// クライテリア取得 [Get the criteria]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateFrom">検索日時FROM [Search datetime (FROM)]</param>
        /// <param name="dateTo">検索日時TO [Search datetime (TO)]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>クライテリア [Criteria]</returns>
        protected virtual ErsMallProductImageTmpCriteria GetCriteria(int? siteId, DateTime dateFrom, DateTime dateTo, IList<string> listSpecifiedScode)
        {
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageTmpCriteria();

            // モール商品画像登録用 [For register mall product images]
            criteria.SetSearchForRegister(dateFrom, dateTo);

            if (siteId != null)
            {
                criteria.site_id = siteId;
            }

            if (listSpecifiedScode != null)
            {
                criteria.scode_in = listSpecifiedScode;
            }

            return criteria;
        }
    }
}
