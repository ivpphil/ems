﻿using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.batch.OperateMallProductImage;
using jp.co.ivp.ers.mall.batch.OperateMallProductImage.specification;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage.strategy
{
    /// <summary>
    /// モール商品コンテナリスト取得 [Get the list of container of mall sku]
    /// </summary>
    public class ObtainMallSkuContainerListStgy
    {
        /// <summary>
        /// モール商品コンテナリスト取得 [Get the list of container of mall sku]
        /// </summary>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>モール商品コンテナリスト [The list of container of mall sku]</returns>
        public virtual IList<MallSkuContainer> GetMallSkuList(IList<string> listSpecifiedScode)
        {
            var spec = new SearchMallSkuForRegisterImageSpec();
            var criteria = this.GetCriteria(listSpecifiedScode);

            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

            var listFind = spec.GetSearchData(criteria);

            var listRet = new List<MallSkuContainer>();

            foreach (var data in listFind)
            {
                var obj = new MallSkuContainer();
                obj.OverwriteWithParameter(data);
                listRet.Add(obj);
            }

            return listRet;
        }

        /// <summary>
        /// クライテリア取得 [Get the criteria]
        /// </summary>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>クライテリア [Criteria]</returns>
        protected virtual ErsMallMerchandiseCriteria GetCriteria(IList<string> listSpecifiedScode)
        {
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();

            criteria.mall_flg = EnumOnOff.On;
            criteria.deleted = EnumDeleted.NotDeleted;

            if (listSpecifiedScode != null)
            {
                criteria.scode_in = listSpecifiedScode;
            }

            return criteria;
        }
    }
}
