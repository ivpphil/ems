﻿using System.Collections.Generic;
using System.Reflection;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage
{
    /// <summary>
    /// モール商品コンテナ [Container of mall sku]
    /// </summary>
    public class MallSkuContainer
        : IErsCollectable, IOverwritable
    {
        /// <summary>
        /// gcode : ITEMコード
        /// </summary>
        public virtual string gcode { get; set; }

        /// <summary>
        /// scode : SKUコード
        /// </summary>
        public virtual string scode { get; set; }

        /// <summary>
        /// site_id : サイトID
        /// </summary>
        public virtual int? site_id { get; set; }

        /// <summary>
        /// mall_shop_kbn : モール店舗タイプ
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// image_indexes : 登録済み画像インデックス
        /// </summary>
        public virtual int[] image_indexes { get; set; }


        /// <summary>
        /// プロパティ取得 [Get properties]
        /// </summary>
        /// <returns>プロパティ [Properties]</returns>
        public Dictionary<string, object> GetPropertiesAsDictionary()
        {
            return ErsReflection.GetPropertiesAsDictionary(this, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        }

        /// <summary>
        /// パラメータで上書き [Overwrite with parameters]
        /// </summary>
        /// <param name="dictionary">パラメータ [Parameter]</param>
        public void OverwriteWithParameter(IDictionary<string, object> dictionary)
        {
            var dic = ErsCommon.OverwriteDictionary(this.GetPropertiesAsDictionary(), dictionary);
            ErsReflection.SetPropertyAll(this, dic);
        }
    }
}
