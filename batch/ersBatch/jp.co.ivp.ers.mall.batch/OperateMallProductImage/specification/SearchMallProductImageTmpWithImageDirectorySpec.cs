﻿using System;
using System.Linq;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage.specification
{
    public class SearchMallProductImageTmpWithImageDirectorySpec
         : SearchSpecificationBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return base.GetSearchData(criteria);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string GetSearchDataSql()
        {
            return "SELECT mall_product_image_tmp_t.*, mall_product_image_directory_t.directory_id AS image_directory_id FROM mall_product_image_tmp_t "
                + "LEFT JOIN mall_product_image_directory_t ON mall_product_image_tmp_t.scode = mall_product_image_directory_t.scode "
                + "AND mall_product_image_tmp_t.mall_shop_kbn = mall_product_image_directory_t.mall_shop_kbn ";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countColumnAlias"></param>
        /// <returns></returns>
        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(*) AS " + countColumnAlias + " "
                + "FROM mall_product_image_tmp_t "
                + "LEFT JOIN mall_product_image_directory_t ON mall_product_image_tmp_t.scode = mall_product_image_directory_t.scode "
                + "AND mall_product_image_tmp_t.mall_shop_kbn = mall_product_image_directory_t.mall_shop_kbn ";
        }
    }
}
