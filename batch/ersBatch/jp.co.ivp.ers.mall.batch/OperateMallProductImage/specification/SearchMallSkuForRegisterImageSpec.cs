﻿using System.Collections.Generic;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage.specification
{
    /// <summary>
    /// 画像登録用モール商品取得 [Get the mall sku for register image]
    /// </summary>
    public class SearchMallSkuForRegisterImageSpec
         : SearchSpecificationBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return base.GetSearchData(criteria);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string GetSearchDataSql()
        {
            return "SELECT gcode, scode, site_id, mall_shop_kbn, mall_flg, "
                + "ARRAY(SELECT p.image_index FROM mall_product_image_tmp_t AS p WHERE p.scode = mall_s_master_t.scode AND p.site_id = mall_s_master_t.site_id) AS image_indexes "
                + "FROM mall_s_master_t "
                + "WHERE true ";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countColumnAlias"></param>
        /// <returns></returns>
        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(*) AS " + countColumnAlias + " FROM mall_s_master_t";
        }
    }
}
