﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using jp.co.ivp.ers.common;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.batch.OperateMallProductImage.strategy;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage.mall
{
    /// <summary>
    /// モール商品画像操作クラス（基底） [Class for operate mall product image (Base)]
    /// </summary>
    public class OperateMallProductImageBase
    {
        #region 構造体 [Struct]
        /// <summary>
        /// アップロードファイル情報 [Upload file information]
        /// </summary>
        protected struct UploadFileInfo
        {
            /// <summary>
            /// モール商品画像テンポラリ [Mall product image temporary]
            /// </summary>
            public ErsMallProductImageTmp objMallProductImageTmp { get; set; }

            /// <summary>
            /// ファイル名 [File name]
            /// </summary>
            public string fileName { get; set; }

            /// <summary>
            /// アップロードファイルパス [Upload file path]
            /// </summary>
            public string uploadFilePath { get; set; }
        }
        #endregion

        #region 基本パラメータ [Basic parameters]
        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        public virtual int? siteId { get; protected set; }

        /// <summary>
        /// 店舗タイプ [Shop type]
        /// </summary>
        public virtual EnumMallShopKbn? shopKbn { get; protected set; }

        /// <summary>
        /// 指定商品コードリスト [The list of specified scode]
        /// </summary>
        public virtual IList<string> listSpecifiedScode { get; protected set; }

        /// <summary>
        /// 抽出日時登録フラグ [Flag for register extract datetime]
        /// </summary>
        public virtual bool doRegisterExtractDateTime { get; protected set; }
        #endregion

        #region 結果パラメータ [Result parameters]
        /// <summary>
        /// エラーログ [Error log]
        /// </summary>
        public virtual string errorLog { get; protected set; }
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        public OperateMallProductImageBase(int? siteId, EnumMallShopKbn? shopKbn, IList<string> listSpecifiedScode)
        {
            this.siteId = siteId;
            this.shopKbn = shopKbn;
            this.listSpecifiedScode = listSpecifiedScode;

            this.doRegisterExtractDateTime = true;
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        public virtual void Execute(DateTime dateExecute)
        {
        }
        #endregion

        #region 抽出日時取得 [Get datetime for extract]
        /// <summary>
        /// 抽出日時取得 [Get datetime for extract]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <returns>抽出日時パラメータ [Parameter for import datetime]</returns>
        protected virtual ErsCommonStruct.DateTimeStartEnd ObtainExtractDateTime(int? siteId, DateTime dateExecute)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageExtractRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageExtractCriteria();

            criteria.site_id = siteId;
            criteria.LIMIT = 1;

            ErsCommonStruct.DateTimeStartEnd retParam = default(ErsCommonStruct.DateTimeStartEnd);

            // 存在確認 [Check exists]
            if (repository.GetRecordCount(criteria) > 0)
            {
                criteria.SetOrderByExtractDate(Criteria.OrderBy.ORDER_BY_DESC);

                var listFind = repository.Find(criteria);

                // 取得した日時を指定 [Specify get day]
                retParam.dateFrom = listFind[0].extract_date;
            }
            else
            {
                // 実行日時前日を指定 [Specify execute yesterday]
                retParam.dateFrom = dateExecute.AddDays(-1);
            }

            retParam.dateTo = dateExecute;

            return retParam;
        }
        #endregion

        #region 登録用モール商品画像テンポラリ取得 [Get the mall product image for operate]
        /// <summary>
        /// 登録用モール商品画像テンポラリ取得 [Get the mall product image for operate]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <returns>抽出日時パラメータ [Parameter for import datetime]</returns>
        protected virtual IList<ErsMallProductImageTmp> ObtainMallProductImageTmpList(int? siteId, DateTime dateExecute)
        {
            // 抽出日時取得 [Get datetime for extract]
            var dateParam = this.ObtainExtractDateTime(siteId, dateExecute);

            // 登録用モール商品テンポラリ取得 [Get the mall product for operate]
            return new ObtainRegisterMallProductImageTmpStgy().Obtain(siteId, dateParam.dateFrom.Value, dateParam.dateTo.Value, this.listSpecifiedScode);
        }
        #endregion

        #region モール商品ファイルアップロード管理登録 [Register mall product file upload management]
        /// <summary>
        /// モール商品ファイルアップロード管理登録 [Register mall product file upload management]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">モール店舗区分 [Mall shop type]</param>
        /// <param name="fileName">ファイル名 [File name]</param>
        /// <param name="fileType">モール商品アップロードファイルタイプ [Mall product upload file type]</param>
        /// <param name="image_directory_id">画像ディレクトリID [Image directory id]</param>
        protected virtual void RegisterMallProductFileUploadManagement(int siteId, EnumMallShopKbn shopKbn, string fileName, EnumMallProductUploadFileType? fileType, int? image_directory_id = null)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductFileUploadManageRepository();
            var obj = ErsMallFactory.ersMallProductFactory.GetErsMallProductFileUploadManage();

            obj.site_id = siteId;
            obj.mall_shop_kbn = shopKbn;
            obj.file_type = fileType;
            obj.file_name = fileName;
            obj.image_directory_id = image_directory_id;

            repository.Insert(obj);
        }
        #endregion

        #region 抽出日時登録 [Register extract management]
        /// <summary>
        /// 抽出日時登録 [Register extract management]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        public virtual void RegisterProductImageExtract(DateTime dateExecute)
        {
            if (!this.doRegisterExtractDateTime)
            {
                return;
            }

            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageExtractRepository();
            var obj = ErsMallFactory.ersMallProductFactory.GetErsMallProductImageExtract();

            obj.site_id = this.siteId;
            obj.mall_shop_kbn = this.shopKbn;
            obj.extract_date = dateExecute;

            repository.Insert(obj);
        }
        #endregion

        #region 画像ファイルバックアップ [Backup image file]
        /// <summary>
        /// 画像ファイルバックアップ [Backup image file]
        /// </summary>
        /// <param name="listMallProductTmp">商品リスト [List of merchandise]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <returns>アップロードファイル情報リスト [The list of upload file information]</returns>
        protected IList<UploadFileInfo> BackupImage(IList<ErsMallProductImageTmp> listMallProductImageTmp, DateTime dateExecute)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var setupMall = ErsMallFactory.ersMallBatchFactory.getSetup();

            var dstDirPath = setupMall.mallProductImageBackupPath;

            // ディレクトリ作成 [Create the directory]
            ErsDirectory.CreateDirectories(dstDirPath);

            var listRet = new List<UploadFileInfo>();

            foreach (var data in listMallProductImageTmp)
            {
                var srcFilePath = string.Empty;
                var dstFileName = string.Empty;
                var fileName = string.Empty;

                switch (data.image_type)
                {
                    case EnumMallProductImageType.Sku:
                        srcFilePath = this.GetSrcFilePath(string.Format(ErsCommonConst.SKU_IMAGE_PATH_FORMAT, setup.image_directory, data.scode, data.image_index));
                        break;
                }

                fileName = Path.GetFileName(srcFilePath);
                dstFileName = string.Format("{0}_{1}_{2}_{3}_{4}", this.siteId, this.shopKbn.ToString(), data.image_type.ToString().ToUpper(), dateExecute.ToString("yyyyMMddHHmmssfffffff"), fileName);

                var dstFilePath = string.Format("{0}\\{1}", dstDirPath, dstFileName);

                // 画像コピー [Copy image]
                this.CopyImage(srcFilePath, dstFilePath);

                var uploadInfo = default(UploadFileInfo);

                uploadInfo.objMallProductImageTmp = data;
                uploadInfo.fileName = fileName;
                uploadInfo.uploadFilePath = dstFilePath;

                listRet.Add(uploadInfo);
            }

            return listRet;
        }

        /// <summary>
        /// コピー元ファイルパス取得 [Get the source file path for Copy]
        /// </summary>
        /// <param name="filePathNoExt">ファイルパス（拡張子除く） [File path (exclude extention)]</param>
        /// <param name="fromDate">FROM [FROM]</param>
        /// <param name="toDate">TO [TO]</param>
        /// <returns>ファイルパス [File path]</returns>
        protected string GetSrcFilePath(string filePathNoExt)
        {
            foreach (var ext in ErsCommonConst.MERCHANDISE_IMAGE_EXTENSIONS)
            {
                var filePath = filePathNoExt + ext;

                if (File.Exists(filePath))
                {
                    return filePath;
                }
            }

            return string.Empty;
        }
        #endregion

        #region 画像コピー [Copy image]
        /// <summary>
        /// 画像コピー [Copy image]
        /// </summary>
        /// <param name="srcPath">コピー元パス [The path of source path]</param>
        /// <param name="dstPath">コピー先パス [The path of destination path]</param>
        protected virtual void CopyImage(string srcPath, string dstPath)
        {
            int retry = 0;

            while (true)
            {
                try
                {
                    using (FileStream stSrc = new FileStream(srcPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    using (FileStream stDst = new FileStream(dstPath, FileMode.Create, FileAccess.ReadWrite, FileShare.None))
                    {
                        stSrc.CopyTo(stDst);
                    }

                    break;
                }
                catch (IOException e)
                {
                    if (retry++ > 50)
                    {
                        throw e;
                    }
                }
            }
        }
        #endregion

        #region 画像最終アップロード日時更新 [Update last image upload time]
        /// <summary>
        /// 画像最終アップロード日時更新 [Update last image upload time]
        /// </summary>
        /// <param name="listMallProductTmp">商品リスト [List of merchandise]</param>
        protected virtual void UpdateLastImageUploadTime(IList<ErsMallProductImageTmp> listMallProductImageTmp)
        {
            var listScode = listMallProductImageTmp.Select(objTmp => objTmp.scode).ToArray();

            // 画像最終アップロード日時更新 [Update last image upload time]
            new UpdateLastImageUploadTimeStgy().Update(listScode, this.siteId);
        }
        #endregion
    }
}
