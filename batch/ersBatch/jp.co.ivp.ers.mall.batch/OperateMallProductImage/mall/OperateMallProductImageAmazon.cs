﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage.mall
{
    /// <summary>
    /// モール商品画像操作クラス（Amazon） [Class for operate mall product image (Amazon)]
    /// </summary>
    public class OperateMallProductImageAmazon
        : OperateMallProductImageBase
    {
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        public OperateMallProductImageAmazon(int? siteId, EnumMallShopKbn? shopKbn, IList<string> listSpecifiedScode)
            : base(siteId, shopKbn, listSpecifiedScode)
        {
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        public override void Execute(DateTime dateExecute)
        {
            // 登録用モール商品画像テンポラリ取得 [Get the mall product for operate]
            var listMallProductImageTmp = this.ObtainMallProductImageTmpList(this.siteId, dateExecute);

            if (listMallProductImageTmp == null)
            {
                return;
            }

            // 画像最終アップロード日時更新 [Update last image upload time]
            this.UpdateLastImageUploadTime(listMallProductImageTmp);
        }
        #endregion
    }
}
