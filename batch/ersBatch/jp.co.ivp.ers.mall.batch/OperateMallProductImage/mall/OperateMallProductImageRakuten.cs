﻿using System;
using System.Collections.Generic;
using System.IO;
using jp.co.ivp.ers.mall.batch.OperateMallProductImage.strategy;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage.mall
{
    /// <summary>
    /// モール商品画像操作クラス（楽天） [Class for operate mall product image (楽天)]
    /// </summary>
    public class OperateMallProductImageRakuten
        : OperateMallProductImageBase
    {
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        public OperateMallProductImageRakuten(int? siteId, EnumMallShopKbn? shopKbn, IList<string> listSpecifiedScode)
            : base(siteId, shopKbn, listSpecifiedScode)
        {
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        public override void Execute(DateTime dateExecute)
        {
            // 登録用モール商品画像テンポラリ取得 [Get the mall product for operate]
            var listMallProductImageTmp = this.ObtainMallProductImageTmpList(this.siteId, dateExecute);

            if (listMallProductImageTmp == null)
            {
                return;
            }

            // 画像ファイルバックアップ [Backup image file]
            var listUploadFileInfo = this.BackupImage(listMallProductImageTmp, dateExecute);

            // テンポラリコピー [Copy to temporary]
            this.CopyToTemporary(listUploadFileInfo);

            // 画像最終アップロード日時更新 [Update last image upload time]
            this.UpdateLastImageUploadTime(listMallProductImageTmp);
        }
        #endregion

        #region 登録用モール商品画像テンポラリ取得 [Get the branch product image for operate]
        /// <summary>
        /// 登録用モール商品画像テンポラリ取得 [Get the branch product image for operate]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <returns>抽出日時パラメータ [Parameter for import datetime]</returns>
        protected override IList<ErsMallProductImageTmp> ObtainMallProductImageTmpList(int? siteId, DateTime dateExecute)
        {
            // 抽出日時取得 [Get datetime for extract]
            var dateParam = this.ObtainExtractDateTime(siteId, dateExecute);

            var stgy = new ObtainRegisterMallProductImageTmpWithImageDirectoryStgy();

            // カウント取得（画像ディレクトリが紐付かない） [Get the count (Not linked with image directory)]
            if (stgy.ObtainNotLinkedCount(siteId, dateParam.dateFrom.Value, dateParam.dateTo.Value, this.listSpecifiedScode) > 0)
            {
                // 抽出日時は登録しない [Don't register extract datetime]
                this.doRegisterExtractDateTime = false;
                return null;
            }

            // 登録用モール商品テンポラリ取得 [Get the branch product for operate]
            return stgy.Obtain(siteId, dateParam.dateFrom.Value, dateParam.dateTo.Value, this.listSpecifiedScode);
        }
        #endregion

        #region テンポラリコピー [Copy to temporary]
        /// <summary>
        /// テンポラリコピー [Copy to temporary]
        /// </summary>
        /// <param name="listUploadFileInfo">アップロードファイル情報リスト [The list of upload file information]</param>
        protected void CopyToTemporary(IList<UploadFileInfo> listUploadFileInfo)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            var dstDirPath = string.Format("{0}\\{1}", setup.mallProductImageTempPath, this.shopKbn.ToString().ToLower());

            // ディレクトリ作成 [Create the directory]
            ErsDirectory.CreateDirectories(dstDirPath);

            foreach (var info in listUploadFileInfo)
            {
                // コピー先ファイルパス取得 [Get the destination file path for copy]
                var dstFilePath = this.GetDstFilePath(dstDirPath, info);

                // 画像コピー [Copy image]
                this.CopyImage(info.uploadFilePath, dstFilePath);

                // モール商品ファイルアップロード管理登録 [Register mall product file upload management]
                this.RegisterMallProductFileUploadManagement(this.siteId.Value, this.shopKbn.Value, Path.GetFileName(dstFilePath), EnumMallProductUploadFileType.RAKUTEN_IMAGE, info.objMallProductImageTmp.image_directory_id);
            }
        }

        /// <summary>
        /// コピー先ファイルパス取得 [Get the destination file path for copy]
        /// </summary>
        /// <param name="dstDirPath">コピー先ディレクトリパス [The destination directory path for copy]</param>
        /// <param name="info">アップロードファイル情報 [Upload file information]</param>
        /// <returns>コピー先ファイルパス [The destination file path for copy]</returns>
        protected string GetDstFilePath(string dstDirPath, UploadFileInfo info)
        {
            var extension = Path.GetExtension(info.fileName);

            switch (info.objMallProductImageTmp.image_type)
            {
                case EnumMallProductImageType.Sku:
                    return string.Format("{0}\\{1}_{2}_{3:D2}{4}", dstDirPath, DateTime.Now.ToString("yyyyMMddHHmmssfffffff"), info.objMallProductImageTmp.mall_scode.ToLower(), info.objMallProductImageTmp.image_index, extension);
            }

            return string.Empty;
        }
        #endregion
    }
}
