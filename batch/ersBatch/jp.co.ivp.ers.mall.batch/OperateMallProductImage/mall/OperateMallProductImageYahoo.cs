﻿using System;
using System.Collections.Generic;
using System.IO;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage.mall
{
    /// <summary>
    /// モール商品画像操作クラス（Yahoo!） [Class for operate mall product image (Yahoo!)]
    /// </summary>
    public class OperateMallProductImageYahoo
        : OperateMallProductImageBase
    {
        #region ファイル名 [File name]
        /// <summary>
        /// Yahoo!画像ZIPファイル名 [Yahoo! image zip file name]
        /// </summary>
        protected const string YAHOO_IMAGE_ZIP_FILE_NAME = "img.zip";
        #endregion

        
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        public OperateMallProductImageYahoo(int? siteId, EnumMallShopKbn? shopKbn, IList<string> listSpecifiedScode)
            : base(siteId, shopKbn, listSpecifiedScode)
        {
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        public override void Execute(DateTime dateExecute)
        {
            // 登録用モール商品画像テンポラリ取得 [Get the mall product for operate]
            var listMallProductImageTmp = this.ObtainMallProductImageTmpList(this.siteId, dateExecute);

            if (listMallProductImageTmp == null)
            {
                return;
            }

            // 画像ファイルバックアップ [Backup image file]
            var listUploadFileInfo = this.BackupImage(listMallProductImageTmp, dateExecute);

            // テンポラリコピー [Copy to temporary]
            this.CopyToTemporary(listUploadFileInfo);
        }
        #endregion

        #region テンポラリコピー [Copy to temporary]
        /// <summary>
        /// テンポラリコピー [Copy to temporary]
        /// </summary>
        /// <param name="listUploadFileInfo">アップロードファイル情報リスト [The list of upload file information]</param>
        protected void CopyToTemporary(IList<UploadFileInfo> listUploadFileInfo)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            var dstDirPath = string.Format("{0}\\{1}", setup.mallProductImageTempPath, this.shopKbn.ToString().ToLower());

            // ディレクトリ作成 [Create the directory]
            ErsDirectory.CreateDirectories(dstDirPath);

            foreach (var info in listUploadFileInfo)
            {
                // コピー先ファイルパス取得 [Get the destination file path for copy]
                var dstFilePath = this.GetDstFilePath(dstDirPath, info);

                if (dstFilePath.HasValue())
                {
                    // 画像コピー [Copy image]
                    this.CopyImage(info.uploadFilePath, dstFilePath);

                    // モール商品ファイルアップロード管理登録 [Register mall product file upload management]
                    this.RegisterMallProductFileUploadManagement(this.siteId.Value, this.shopKbn.Value, Path.GetFileName(dstFilePath), EnumMallProductUploadFileType.YAHOO_IMAGE);
                }
            }
        }

        /// <summary>
        /// コピー先ファイルパス取得 [Get the destination file path for copy]
        /// </summary>
        /// <param name="dstDirPath">コピー先ディレクトリパス [The destination directory path for copy]</param>
        /// <param name="info">アップロードファイル情報 [Upload file information]</param>
        /// <returns>コピー先ファイルパス [The destination file path for copy]</returns>
        protected string GetDstFilePath(string dstDirPath, UploadFileInfo info)
        {
            var extension = Path.GetExtension(info.fileName);

            switch (info.objMallProductImageTmp.image_type)
            {
                case EnumMallProductImageType.Sku:
                    if (info.objMallProductImageTmp.image_index == 1)
                    {
                        return string.Format("{0}\\{1}_{2}{3}", dstDirPath, DateTime.Now.ToString("yyyyMMddHHmmssfffffff"), info.objMallProductImageTmp.mall_scode, extension);
                    }
                    else if (info.objMallProductImageTmp.image_index > 1)
                    {
                        return string.Format("{0}\\{1}_{2}_{3}{4}", dstDirPath, DateTime.Now.ToString("yyyyMMddHHmmssfffffff"), info.objMallProductImageTmp.mall_scode, info.objMallProductImageTmp.image_index, extension);
                    }
                    break;
            }

            return string.Empty;
        }
        #endregion
    }
}
