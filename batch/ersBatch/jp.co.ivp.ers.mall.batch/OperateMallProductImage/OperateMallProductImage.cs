﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using jp.co.ivp.ers.mall.batch.OperateMallProductImage.mall;
using jp.co.ivp.ers.mall.batch.OperateMallProductImage.strategy;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage
{
    /// <summary>
    /// モール商品画像操作メイン [Operate mall product image main]
    /// </summary>
    public class OperateMallProductImage
    {
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="argDictinary">引数 [Arguments]</param>
        /// <param name="executeDate">実行日時 [Execute date]</param>
        /// <param name="lastDate">前回実行日時 [Last execute date]</param>
        public void Execute(IDictionary<string, object> argDictinary, DateTime? executeDate, DateTime? lastDate)
        {
            // 実行日時取得 [Get execute datetime]
            var dateExecute = DateTime.Now;

            // 指定商品コードリスト取得 [Get the list of specified scode]
            var listSpecifiedScode = this.GetSpecifiedScodeList(argDictinary);

            // モール商品画像テンポラリインポート [Import mall product image temporary]
            this.ImportMallProductTmp(listSpecifiedScode, executeDate, lastDate);

            // 実行リスト取得 [Get the list of execute]
            var listExecute = this.GetExecuteList(dateExecute, listSpecifiedScode);

            // 実行 [Execute]
            var resultExecute = this.Execute(listExecute, DateTime.Now);

            if (!string.IsNullOrEmpty(resultExecute))
            {
                throw new Exception(resultExecute);
            }
        }

        #region モール商品画像テンポラリインポート [Import mall product image temporary]
        /// <summary>
        /// モール商品画像テンポラリインポート [Import mall product image temporary]
        /// </summary>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <param name="executeDate">実行日時 [Execute date]</param>
        /// <param name="lastDate">前回実行日時 [Last execute date]</param>
        protected virtual void ImportMallProductTmp(IList<string> listSpecifiedScode, DateTime? executeDate, DateTime? lastDate)
        {
            var dateNow = DateTime.Now;
            var fromDate = lastDate.HasValue ? lastDate.Value : dateNow.AddYears(-10);
            var toDate = executeDate.HasValue ? executeDate.Value : dateNow;

            // モール商品コンテナリスト取得 [Get the list of container of mall sku]
            var listMallSkuContainer = new ObtainMallSkuContainerListStgy().GetMallSkuList(listSpecifiedScode);

            var stgyImport = new ImportMallProductImageTmpStgy();

            Parallel.ForEach(listMallSkuContainer, container =>
            {
                // モール商品画像テンポラリインポート [Import mall product image temporary]
                stgyImport.Import(container, fromDate, toDate);
            });
        }
        #endregion

        #region 指定商品コードリスト取得 [Get the list of specified scode]
        /// <summary>
        /// 指定商品コードリスト取得 [Get the list of specified scode]
        /// </summary>
        /// <param name="argDictinary">引数 [Arguments]</param>
        /// <returns>指定商品コードリスト [The list of specified scode]</returns>
        protected IList<string> GetSpecifiedScodeList(IDictionary<string, object> argDictinary)
        {
            if (argDictinary == null)
            {
                return null;
            }
            if (!argDictinary.ContainsKey("scode"))
            {
                return null;
            }

            return argDictinary["scode"].ToString().Split(',').Select(value => value.Trim()).ToList<string>();
        }
        #endregion

        #region 実行リスト取得 [Get the list of execute]
        /// <summary>
        /// 実行リスト取得 [Get the list of execute]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <param name="listSpecifiedScode">指定商品コードリスト [The list of specified scode]</param>
        /// <returns>実行リスト [The list of execute]</returns>
        protected IList<OperateMallProductImageBase> GetExecuteList(DateTime dateExecute, IList<string> listSpecifiedScode)
        {
            var siteData = ErsMallFactory.ersSiteFactory.GetSiteData();
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();
            var listRet = new List<OperateMallProductImageBase>();

            foreach (var siteId in siteData.dicSiteData.Keys)
            {
                // モール店舗タイプ取得（サイトIDから） [Get mall shop type (from Site id)]
                var shopKbn = siteData.GetMallShopKbnFromSiteId(siteId);

                switch (shopKbn)
                {
                    // 楽天 [Rakuten]
                    case EnumMallShopKbn.RAKUTEN:
                        if (setup.doOperateProductImageRakuten && !this.isWithinExcludeDateTime(EnumMallShopKbn.RAKUTEN, dateExecute))
                        {
                            listRet.Add(new OperateMallProductImageRakuten(siteId, EnumMallShopKbn.RAKUTEN, listSpecifiedScode));
                        }
                        break;

                    // Yahoo! [Yahoo!]
                    case EnumMallShopKbn.YAHOO:
                        if (setup.doOperateProductImageYahoo && !this.isWithinExcludeDateTime(EnumMallShopKbn.YAHOO, dateExecute))
                        {
                            listRet.Add(new OperateMallProductImageYahoo(siteId, EnumMallShopKbn.YAHOO, listSpecifiedScode));
                        }
                        break;

                    // Amazon [Amazon]
                    case EnumMallShopKbn.AMAZON:
                        if (setup.doOperateProductImageAmazon && !this.isWithinExcludeDateTime(EnumMallShopKbn.AMAZON, dateExecute))
                        {
                            listRet.Add(new OperateMallProductImageAmazon(siteId, EnumMallShopKbn.AMAZON, listSpecifiedScode));
                        }
                        break;
                }
            }

            return listRet;
        }

        /// <summary>
        /// 除外日時内判定 [Judgement within exclude DateTime]
        /// </summary>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="dateNow">現在日時 [DateTime Now]</param>
        /// <returns>True : 除外 [Exclude] / False : 除外しない [Not exlude]</returns>
        protected bool isWithinExcludeDateTime(EnumMallShopKbn? shopKbn, DateTime dateNow)
        {
            return ErsMallFactory.ersMallStopTimeFactory.GetIsWithinExcludeDateTimeStgy().IsWithin(shopKbn, EnumMallFuncType.ProductImage, dateNow);
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="listExec">実行リスト [The list of execute]</param>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        /// <returns>エラーログ [Error log]</returns>
        protected string Execute(IList<OperateMallProductImageBase> listExec, DateTime dateExecute)
        {
            var exceptions = new ConcurrentQueue<OperateMallProductImageException>();

            Parallel.ForEach(listExec, exec =>
            {
                try
                {
                    // 実行 [Execute]
                    exec.Execute(dateExecute);

                    // 抽出日時登録 [Register extract management]
                    exec.RegisterProductImageExtract(dateExecute);
                }
                catch (Exception e)
                {
                    // 例外追加 [Add the exception]
                    exceptions.Enqueue(new OperateMallProductImageException(e, DateTime.Now, exec.siteId, exec.shopKbn));
                }
            });

            string errorLog = string.Empty;

            // 例外エラーをまとめる [Aggregate the exception]
            if (exceptions.Count > 0)
            {
                foreach (var e in exceptions)
                {
                    errorLog += e.ToString() + Environment.NewLine;
                }
            }

            // エラーログをまとめる [Aggregate the error log]
            foreach (var exec in listExec)
            {
                errorLog += exec.errorLog + (string.IsNullOrEmpty(exec.errorLog) ? string.Empty : Environment.NewLine);
            }

            return errorLog;
        }
        #endregion
    }
}
