﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall.stock_recovery;

namespace jp.co.ivp.ers.mall.batch.RecoveryMallStock
{
    /// <summary>
    /// モール在庫リカバリ [Recovery mall stock]
    /// </summary>
    public class RecoveryMallStock
    {
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        public void Execute()
        {
            // モール在庫リカバリデータ取得 [Get the mall stock data for recovery]
            var listRecovery = this.ObtainMallStockForRecovery();

            if (listRecovery == null)
            {
                return;
            }

            // リカバリ [Recovery]
            this.Recovery(listRecovery);

            // リカバリ終了 [Finish recovery]
            this.FinishRecovery(listRecovery);
        }

        #region モール在庫リカバリデータ取得 [Get the mall stock data for recovery]
        /// <summary>
        /// モール在庫リカバリデータ取得 [Get the mall stock data for recovery]
        /// </summary>
        /// <returns>モール在庫リカバリデータ [The mall stock data for recovery]</returns>
        protected virtual IList<ErsMallStockRecovery> ObtainMallStockForRecovery()
        {
            var repository = ErsMallFactory.ersMallStockRecoveryFactory.GetErsMallStockRecoveryRepository();
            var criteria = ErsMallFactory.ersMallStockRecoveryFactory.GetErsMallStockRecoveryCriteria();

            criteria.active = EnumActive.Active;

            if (repository.GetRecordCount(criteria) == 0)
            {
                return null;
            }

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            return repository.Find(criteria);
        }
        #endregion

        #region リカバリ [Recovery]
        /// <summary>
        /// リカバリ [Recovery]
        /// </summary>
        /// <param name="listRecovery">リカバリデータリスト [The list of recovery data]</param>
        protected void Recovery(IList<ErsMallStockRecovery> listRecovery)
        {
            var listParam = new List<UpdateStockParam>();

            foreach (var data in listRecovery)
            {
                UpdateStockParam param = default(UpdateStockParam);

                param.productCode = data.scode;
                param.quantity = data.stock;

                switch (data.operation)
                {
                    case EnumMallStockOperation.set:
                        // SETはリカバリしない
                        continue;
                    case EnumMallStockOperation.add:
                        param.operation = EnumMallStockOperation.sub;
                        break;
                    case EnumMallStockOperation.sub:
                        param.operation = EnumMallStockOperation.add;
                        break;
                }

                listParam.Add(param);
            }

            // モール在庫更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }
        }
        #endregion

        #region リカバリ終了 [Finish recovery]
        /// <summary>
        /// リカバリ終了 [Finish recovery]
        /// </summary>
        /// <param name="listRecovery">リカバリデータリスト [The list of recovery data]</param>
        protected virtual void FinishRecovery(IList<ErsMallStockRecovery> listRecovery)
        {
            var repository = ErsMallFactory.ersMallStockRecoveryFactory.GetErsMallStockRecoveryRepository();

            foreach (var data in listRecovery)
            {
                var objOld = data;
                var objNew = ErsMallFactory.ersMallStockRecoveryFactory.GetErsMallStockRecoveryWithParameters(objOld.GetPropertiesAsDictionary());

                objNew.active = EnumActive.NonActive;

                repository.Update(objOld, objNew);
            }
        }
        #endregion
    }
}
