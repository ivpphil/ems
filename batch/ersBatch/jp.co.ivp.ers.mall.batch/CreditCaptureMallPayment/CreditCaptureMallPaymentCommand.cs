﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.mall.batch.CreditCaptureMallPayment
{
    public class CreditCaptureMallPaymentCommand
        : jp.co.ivp.ers.batch.CreditCapturePayment.CreditCapturePaymentCommand
    {
        /// <summary>
        /// Send capture
        /// </summary>
        /// <param name="order"></param>
        protected override void DoCapture(order.ErsOrder order)
        {
            var param = ErsMallFactory.ersMallOrderFactory.GetMallOrderPaymentParam();

            param.siteId = order.site_id;
            param.shopKbn = order.mall_shop_kbn;
            param.order_code = order.mall_d_no;
            param.order_date = order.intime;
            param.operation = EnumMallCardPaymentOperation.Sales;
            param.payment_price = order.total;

            // モール伝票決済処理 [Execute mall order payment]
            ErsMallFactory.ersMallOrderFactory.GetExecuteMallOrderPaymentStgy().ExecuteMallOrderPayment(order.site_id, order.mall_shop_kbn, param);
        }

        /// <summary>
        /// Get Criteria instance for search order.
        /// </summary>
        /// <param name="baseDate"></param>
        /// <returns></returns>
        protected override ErsOrderCriteria GetCriteria()
        {
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

            criteria.pay_in = new[] { EnumPaymentType.CREDIT_CARD };
            criteria.order_status_in = new[] { EnumOrderStatusType.DELIVERED };
            criteria.order_payment_status_in = new[] { EnumOrderPaymentStatusType.NOT_PAID };

            //モール
            criteria.mall_shop_kbn_not_equal = EnumMallShopKbn.ERS;

            criteria.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);
            return criteria;
        }
    }
}
