﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using jp.co.ivp.ers.mall.batch.UploadMallProductFile.common;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.batch.UploadMallProductImageFile.mall
{
    /// <summary>
    /// 支店商品画像操作クラス（Yahoo!） [Class for operate branch product image (Yahoo!)]
    /// </summary>
    public class UploadMallProductImageFileYahoo
        : UploadMallProductImageFileBase
    {
        #region 定数 [Constant]
        /// <summary>
        /// Yahoo!画像アップロード最大バイト数 [Yahoo! Image upload max size (Byte)]
        /// </summary>
        protected const int YAHOO_IMAGE_MAX_BYTE = 52428800;

        /// <summary>
        /// Yahoo!画像アップロードFTPタイムアウト（ミリ秒） [Yahoo! Image upload ftp timeout (Millisecond)]
        /// </summary>
        protected const int YAHOO_IMAGE_UPLOAD_FTP_TIMEOUT_MSEC = 300 * 1000;
        #endregion

        #region ファイル名 [File name]
        /// <summary>
        /// Yahoo!画像ZIPファイル名 [Yahoo! image zip file name]
        /// </summary>
        protected const string YAHOO_IMAGE_ZIP_FILE_NAME = "img.zip";
        #endregion

        
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public UploadMallProductImageFileYahoo(int? siteId, EnumMallShopKbn? shopKbn)
            : base(siteId, shopKbn)
        {
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        public override void Execute()
        {
            // モール商品ファイルアップロード管理取得 [Get mall product upload management]
            var listManage = this.ObtainMallProductFileUploadManageList(EnumMallProductUploadFileType.YAHOO_IMAGE);

            if (listManage == null)
            {
                return;
            }

            // アップロードリスト取得 [Get the list for upload]
            var listUpload = this.ObtainUploadList(listManage);

            // アップロード [Upload]
            if (this.Upload(listUpload))
            {
                // モール商品ファイルアップロード管理更新 [Update mall product upload management]
                this.UpdateMallProductFileUploadManage(listUpload);
            }
        }
        #endregion

        #region アップロードリスト取得 [Get the list for upload]
        /// <summary>
        /// アップロードリスト取得 [Get the list for upload]
        /// </summary>
        /// <param name="listManage">モール商品ファイルアップロード管理リスト [The list of mall product upload management]</param>
        /// <returns>アップロードリスト [The list for upload]</returns>
        protected virtual IEnumerable<ErsMallProductFileUploadManage> ObtainUploadList(IList<ErsMallProductFileUploadManage> listManage)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            var dirPath = string.Format("{0}\\{1}", setup.mallProductImageTempPath, this.shopKbn.ToString().ToLower());
            long bytes = 0;

            for (var i = 0; i < listManage.Count; i++)
            {
                if (bytes >= YAHOO_IMAGE_MAX_BYTE)
                {
                    break;
                }

                var objManage = listManage[i];

                bytes += new FileInfo(Path.Combine(dirPath, objManage.file_name)).Length;

                yield return objManage;
            }
        }
        #endregion

        #region アップロード [Upload]
        /// <summary>
        /// アップロード [Upload]
        /// </summary>
        /// <param name="listUpload">アップロード用モール商品ファイルアップロード管理リスト [The list of mall product upload management for Upload]</param>
        /// <returns>アップロード結果 [Upload result]</returns>
        protected bool Upload(IEnumerable<ErsMallProductFileUploadManage> listUpload)
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();
            var stgy = new CheckFTPFileExists();

            string ftpHost = mallSetup.GetYahooFtpHost(this.siteId.Value);
            string ftpUser = mallSetup.GetYahooFtpUser(this.siteId.Value);
            string ftpPass = mallSetup.GetYahooFtpPass(this.siteId.Value);

            // パス設定 [Path settings]
            string uri = string.Format("ftp://{0}/", ftpHost);
            var fileURI = uri + YAHOO_IMAGE_ZIP_FILE_NAME;

            // ファイル存在チェック [Check the file exists]
            if (stgy.ExistsFile(fileURI, ftpUser, ftpPass))
            {
                return false;
            }

            // 圧縮用リスト取得 [Get the list for Compress]
            var listCompress = this.ObtainListForCompress(listUpload);

            // 圧縮 [Compress]
            var zipFilePath = this.Compress(listCompress);

            // アップロード [Upload]
            using (var wc = ErsMallFactory.ersMallUtilityFactory.GetMallWebClient(YAHOO_IMAGE_UPLOAD_FTP_TIMEOUT_MSEC))
            {
                wc.Credentials = new NetworkCredential(ftpUser, ftpPass);
                wc.UploadFile(fileURI, zipFilePath);
            }

            return true;
        }

        /// <summary>
        /// 圧縮用リスト取得 [Get the list for Compress]
        /// </summary>
        /// <param name="listUpload">アップロード用モール商品ファイルアップロード管理リスト [The list of mall product upload management for Upload]</param>
        /// <returns>圧縮用リスト [The list for Compress]</returns>
        protected IEnumerable<ErsMallProductFileUploadManage> ObtainListForCompress(IEnumerable<ErsMallProductFileUploadManage> listUpload)
        {
            // ファイル名でグループ化 [Group by file name]
            var groupFileName = from objManage in listUpload
                                group objManage by objManage.file_name.Substring(22);

            foreach (var fileName in groupFileName)
            {
                if (fileName.Count() == 1)
                {
                    yield return fileName.First();
                }
                else
                {
                    // 最新１件 [Latest one]
                    yield return fileName.OrderByDescending(data => data.intime).First();
                }
            }
        }

        /// <summary>
        /// 圧縮 [Compress]
        /// </summary>
        /// <param name="listUpload">アップロード用モール商品ファイルアップロード管理リスト [The list of mall product upload management for Upload]</param>
        /// <returns>圧縮ファイルパス [Compressed file path]</returns>
        protected string Compress(IEnumerable<ErsMallProductFileUploadManage> listUpload)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            var dstDirPath = setup.mallProductImageBackupPath;

            // ディレクトリ作成 [Create the directory]
            ErsDirectory.CreateDirectories(dstDirPath);

            var zipFileNanager = new ZipFileManager();

            var dstFilePath = string.Format("{0}\\{1}_yahoo_img.zip", dstDirPath, DateTime.Now.ToString("yyyyMMddHHmmssfffffff"));
            var dirPath = string.Format("{0}\\{1}", setup.mallProductImageTempPath, this.shopKbn.ToString().ToLower());

            // ZIP圧縮 [Compress by zip]
            zipFileNanager.CompressList(listUpload.Select(objManage => Path.Combine(dirPath, objManage.file_name)).ToList(), dstFilePath, (src => src.Substring(22)));

            return dstFilePath;
        }
        #endregion
    }
}
