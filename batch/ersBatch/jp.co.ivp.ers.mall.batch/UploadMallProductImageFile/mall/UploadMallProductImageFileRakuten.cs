﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using jp.co.ivp.ers.mall.batch.UploadMallProductImageFile.common;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.batch.UploadMallProductImageFile.mall
{
    /// <summary>
    /// 支店商品画像操作クラス（楽天） [Class for operate branch product image (楽天)]
    /// </summary>
    public class UploadMallProductImageFileRakuten
        : UploadMallProductImageFileBase
    {
        #region 定数 [Constant]
        /// <summary>
        /// 楽天画像アップロード最大バイト数 [Rakuten Image upload max size (Byte)]
        /// </summary>
        protected const int RAKUTEN_IMAGE_MAX_BYTE = 524288000;

        /// <summary>
        /// 楽天画像アップロードFTPタイムアウト（ミリ秒） [Rakuten Image upload ftp timeout (Millisecond)]
        /// </summary>
        protected const int RAKUTEN_IMAGE_UPLOAD_FTP_TIMEOUT_MSEC = 120 * 1000;
        #endregion

        #region ディレクトリパス・ファイル名 [Directory path, File name]
        /// <summary>
        /// 楽天画像ディレクトリパス [Rakuten image file name]
        /// </summary>
        protected const string RAKUTEN_IMAGE_DIRECTORY_PATH = "cabinet/images";
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public UploadMallProductImageFileRakuten(int? siteId, EnumMallShopKbn? shopKbn)
            : base(siteId, shopKbn)
        {
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        public override void Execute()
        {
            // モール商品ファイルアップロード管理取得 [Get mall product upload management]
            var listManage = this.ObtainMallProductFileUploadManageList(EnumMallProductUploadFileType.RAKUTEN_IMAGE);

            if (listManage == null)
            {
                return;
            }

            // アップロードリスト取得 [Get the list for upload]
            var listUpload = this.ObtainUploadList(listManage);

            // アップロード [Upload]
            var listError = this.Upload(listUpload);

            // モール商品ファイルアップロード管理更新 [Update mall product upload management]
            this.UpdateMallProductFileUploadManage(listUpload);

            if (listError.Count > 0)
            {
                this.errorLog += ErsResources.GetMessage("109000", String.Join("", listError));
            }
        }
        #endregion

        #region アップロードリスト取得 [Get the list for upload]
        /// <summary>
        /// アップロードリスト取得 [Get the list for upload]
        /// </summary>
        /// <param name="listManage">モール商品ファイルアップロード管理リスト [The list of mall product upload management]</param>
        /// <returns>アップロードリスト [The list for upload]</returns>
        protected virtual IEnumerable<ErsMallProductFileUploadManage> ObtainUploadList(IList<ErsMallProductFileUploadManage> listManage)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            var dirPath = string.Format("{0}\\{1}", setup.mallProductImageTempPath, this.shopKbn.ToString().ToLower());
            long bytes = 0;

            for (var i = 0; i < listManage.Count; i++)
            {
                if (bytes >= RAKUTEN_IMAGE_MAX_BYTE)
                {
                    break;
                }

                var objManage = listManage[i];

                bytes += new FileInfo(Path.Combine(dirPath, objManage.file_name)).Length;

                yield return objManage;
            }
        }
        #endregion

        #region アップロード [Upload]
        /// <summary>
        /// アップロード [Upload]
        /// </summary>
        /// <param name="listManage">モール商品ファイルアップロード管理リスト [The list of mall product upload management]</param>
        /// <returns>エラーメッセージリスト [The list of error message]</returns>
        protected IList<string> Upload(IEnumerable<ErsMallProductFileUploadManage> listManage)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var stgy = new CheckFTPDirectoryExists();

            string ftpHost = mallSetup.GetRakutenFtpHost(this.siteId.Value);
            string ftpUser = mallSetup.GetRakutenFtpUser(this.siteId.Value);
            string ftpPass = mallSetup.GetRakutenFtpPass(this.siteId.Value);

            // ディレクトリIDでグループ化＋ソート [Group by image directory id and sorting]
            var groupDirectory = from objManage in listManage 
                                 group objManage by objManage.image_directory_id into g
                                 orderby g.Key ascending select g;

            var listRet = new List<string>();

            foreach (var directory in groupDirectory)
            {
                // ディレクトリ名 [Directory name]
                var dirName = (!setup.rakutenFTPImageDir.HasValue() ? string.Format(ErsMallCommonConst.MALL_PRODUCT_IMAGE_DIRECTORY_NAME_FORMAT, directory.First().image_directory_id) : setup.rakutenFTPImageDir) + "/";

                // パス設定 [Path settings]
                string ftpUri = string.Format("ftp://{0}/{1}/", ftpHost, RAKUTEN_IMAGE_DIRECTORY_PATH) + dirName;

                // ディレクトリ存在チェック [Check the file exists]
                if (!stgy.ExistsDirectory(ftpUri, ftpUser, ftpPass))
                {
                    // FTPディレクトリ作成 [Make directory on ftp]
                    this.MakeFtpDirectory(ftpUri.TrimEnd('/'), ftpUser, ftpPass);
                }

                var dirPath = string.Format("{0}\\{1}", setup.mallProductImageTempPath, this.shopKbn.ToString().ToLower());

                // アップロード [Upload]
                using (var wc = ErsMallFactory.ersMallUtilityFactory.GetMallWebClient(RAKUTEN_IMAGE_UPLOAD_FTP_TIMEOUT_MSEC))
                {
                    wc.Credentials = new NetworkCredential(ftpUser, ftpPass);

                    foreach (var data in directory)
                    {
                        var fielPath = Path.Combine(dirPath, data.file_name);
                        var fileURI = ftpUri + Path.GetFileName(data.file_name).Substring(22);

                        int retry = 0;

                        while (true)
                        {
                            try
                            {
                                wc.UploadFile(fileURI, fielPath);
                                break;
                            }
                            catch (WebException e)
                            {
                                var response = (FtpWebResponse)e.Response;

                                // 550 の場合はディレクトリ作成を試みる [Try make directory when 550 error]
                                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                                {
                                    // ディレクトリ存在チェック [Check the file exists]
                                    if (!stgy.ExistsDirectory(ftpUri, ftpUser, ftpPass))
                                    {
                                        // FTPディレクトリ作成 [Make directory on ftp]
                                        this.MakeFtpDirectory(ftpUri.TrimEnd('/'), ftpUser, ftpPass);

                                        // 5回まで [Until 5 times]
                                        if (++retry < 5)
                                        {
                                            continue;
                                        }
                                    }
                                }

                                listRet.Add(ErsResources.GetMessage("109001", this.siteId, (int)this.shopKbn, data.id, data.file_name, response.StatusDescription));
                                break;
                            }
                        }
                    }
                }
            }

            return listRet;
        }

        /// <summary>
        /// FTPディレクトリ作成 [Make directory on ftp]
        /// </summary>
        /// <param name="ftpUri">URI [URI]</param>
        /// <param name="ftpUser">FTPユーザ [FTP user]</param>
        /// <param name="ftpPass">FTPパスワード [FTP password]</param>
        protected void MakeFtpDirectory(string ftpUri, string ftpUser, string ftpPass)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            // ディレクトリ作成 [Create directory]
            var request = (FtpWebRequest)WebRequest.Create(ftpUri);

            request.Credentials = new NetworkCredential(ftpUser, ftpPass);
            request.Method = WebRequestMethods.Ftp.MakeDirectory;

            using (var response = (FtpWebResponse)request.GetResponse())
            {
            }
        }
        #endregion
    }
}
