﻿using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.batch.UpdateMallSalesPeriodProducts.specification
{
    /// <summary>
    /// モール販売期間商品更新 [Update mall sales period products]
    /// </summary>
    public class UpdateMallSalesPeriodProductsSpec
         : ISpecificationForSQL
    {
        /// <summary>
        /// SQL文 [SQL statement]
        /// </summary>
        /// <returns></returns>
        public virtual string asSQL()
        {
            return "UPDATE mall_s_master_t SET "
                    + "sales_period_flg = (CASE WHEN mall_s_master_t.sales_period_flg = 1 THEN 0 ELSE 1 END), utime = now() "
                    + "FROM g_master_t "
                    + "WHERE g_master_t.gcode = mall_s_master_t.gcode";
        }
    }
}
