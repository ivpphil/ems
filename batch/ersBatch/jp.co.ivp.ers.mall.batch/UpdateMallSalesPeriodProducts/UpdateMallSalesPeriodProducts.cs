﻿using jp.co.ivp.ers.mall.batch.UpdateMallSalesPeriodProducts.specification;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.batch.UpdateMallSalesPeriodProducts
{
    /// <summary>
    /// モール販売期間商品更新メイン [Update mall products for sales period]
    /// </summary>
    public class UpdateMallSalesPeriodProducts
    {
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        public void Execute()
        {
            var spec = new UpdateMallSalesPeriodProductsSpec();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();

            criteria.SetMallSalesEnd();

            ErsRepository.UpdateSatisfying(spec, criteria);
        }
    }
}
