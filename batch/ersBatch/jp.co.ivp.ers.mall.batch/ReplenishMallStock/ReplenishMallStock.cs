﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall.batch.OperateMallProductImage.specification;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.merchandise.stock;

namespace jp.co.ivp.ers.mall.batch.ReplenishMallStock
{
    /// <summary>
    /// モール在庫補充 [Replenish mall stock]
    /// </summary>
    public class ReplenishMallStock
    {
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="dicArg">コマンド引数 [Command options]</param>
        public void Execute(IDictionary<string, object> dicArg)
        {
            int? from = (dicArg != null && dicArg.ContainsKey("from")) ? Convert.ToInt32(dicArg["from"]) : (int?)null;
            int? to = (dicArg != null && dicArg.ContainsKey("to")) ? Convert.ToInt32(dicArg["to"]) : (int?)null;

            int count = from.HasValue ? from.Value : 0;
            int divide = 5000;

            var listError = new List<string>();

            // 在庫補充データ件数 [The data count for replenish stock quantity]
            long total = this.ObtainStockForReplenishCount();

            while (true)
            {
                int start = count * divide;
                int end = (count + 1) * divide;

                try
                {
                    // 在庫補充データ取得 [Get the data for replenish stock quantity]
                    var listTarget = this.ObtainStockForReplenish(start, divide);

                    if (listTarget.Count == 0)
                    {
                        return;
                    }

                    // モール在庫更新パラメータリスト取得 [Get the list of parameter for update mall stock]
                    var listParam = this.ObtainUpdateStockParamList(listTarget);

                    // モール在庫更新 [Update mall stock]
                    this.UpdateMallStock(listParam);
                }
                catch (Exception e)
                {
                    listError.Add(string.Format("[{0} - {1}]\r\n{2}", start, end, e.ToString()));
                }

                if (end >= total)
                {
                    break;
                }

                if (to.HasValue && count >= to.Value)
                {
                    break;
                }

                count++;
            }

            if (listError.Count > 0)
            {
                throw new Exception(String.Join(Environment.NewLine, listError));
            }
        }

        #region 在庫補充データ取得 [Get the data for replenish stock quantity]
        /// <summary>
        /// 在庫補充データ件数取得 [Get the data count for replenish stock quantity]
        /// </summary>
        /// <returns>在庫補充データ件数 [The data count for replenish stock quantity]</returns>
        protected virtual long ObtainStockForReplenishCount()
        {
            var spec = new SearchForReplenishMallStockSpec();
            var criteria = this.GetCriteria();

            return spec.GetCountData(criteria);
        }

        /// <summary>
        /// 在庫補充データ取得 [Get the data for replenish stock quantity]
        /// </summary>
        /// <param name="offset">オフセット [Offset]</param>
        /// <param name="limit">リミット [Limit]</param>
        /// <returns>在庫補充データ [The data for replenish stock quantity]</returns>
        protected virtual IList<ErsStock> ObtainStockForReplenish(int offset, int limit)
        {
            var factory = ErsFactory.ersMerchandiseFactory.GetErsStockFactory();
            var spec = new SearchForReplenishMallStockSpec();
            var criteria = this.GetCriteria();

            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

            criteria.OFFSET = offset;
            criteria.LIMIT = limit;


            var listFind = spec.GetSearchData(criteria);
            var listRet = new List<ErsStock>();

            foreach (var data in listFind)
            {
                var objStock = factory.GetErsStock();
                objStock.OverwriteWithParameter(data);
                listRet.Add(objStock);
            }

            return listRet;
        }

        /// <summary>
        /// クライテリアセット [Set the criteria]
        /// </summary>
        /// <returns>クライテリア [Criteria]</returns>
        protected virtual ErsStockCriteria GetCriteria()
        {
            var factory = ErsFactory.ersMerchandiseFactory.GetErsStockFactory();
            var criteria = factory.GetErsStockCriteria();
            var criteriaSku = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            var criteriaMall = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();

            criteriaSku.soldout_flg = EnumSoldoutFlg.DisableSoldout;
            criteria.Add(criteriaSku);

            criteriaMall.mall_flg = EnumOnOff.On;
            criteria.Add(criteriaMall);

            criteria.stock_less_than = ErsMallCommonConst.REPLENISH_MALL_STOCK_THRESHOLD;

            return criteria;
        }
        #endregion

        #region モール在庫更新パラメータリスト取得 [Get the list of parameter for update mall stock]
        /// <summary>
        /// モール在庫更新パラメータリスト取得 [Get the list of parameter for update mall stock]
        /// </summary>
        /// <param name="listTarget">対象リスト [The list of target]</param>
        /// <returns>モール在庫更新パラメータリスト [The list of parameter for update mall stock]</returns>
        protected virtual IList<UpdateStockParam> ObtainUpdateStockParamList(IList<ErsStock> listTarget)
        {
            var listRet = new List<UpdateStockParam>();

            foreach (var objStock in listTarget)
            {
                UpdateStockParam param = default(UpdateStockParam);

                param.productCode = objStock.scode;
                param.quantity = ErsMallCommonConst.REPLENISH_MALL_STOCK_QUANTITY;
                param.operation = EnumMallStockOperation.set;

                listRet.Add(param);
            }

            return listRet;
        }
        #endregion

        #region モール在庫更新 [Update mall stock]
        /// <summary>
        /// モール在庫更新 [Update mall stock]
        /// </summary>
        /// <param name="listParam">パラメータリスト [The list of parameters]</param>
        protected void UpdateMallStock(IList<UpdateStockParam> listParam)
        {
            // モール在庫更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);

            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }
        }
        #endregion
    }
}
