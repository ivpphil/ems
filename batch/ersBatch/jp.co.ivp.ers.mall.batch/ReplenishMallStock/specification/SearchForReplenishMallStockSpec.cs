﻿using System.Collections.Generic;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.batch.OperateMallProductImage.specification
{
    /// <summary>
    /// モール在庫補充商品取得 [Get the sku for replenish mall stock]
    /// </summary>
    public class SearchForReplenishMallStockSpec
         : SearchSpecificationBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return base.GetSearchData(criteria);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string GetSearchDataSql()
        {
            return "SELECT DISTINCT ON (stock_t.scode) stock_t.scode FROM stock_t "
                + "INNER JOIN s_master_t ON stock_t.scode = s_master_t.scode "
                + "INNER JOIN mall_s_master_t ON stock_t.scode = mall_s_master_t.scode";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countColumnAlias"></param>
        /// <returns></returns>
        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(DISTINCT stock_t.scode) AS " + countColumnAlias + " FROM stock_t "
                + "INNER JOIN s_master_t ON stock_t.scode = s_master_t.scode "
                + "INNER JOIN mall_s_master_t ON stock_t.scode = mall_s_master_t.scode";
        }
    }
}
