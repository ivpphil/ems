﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.stepmail;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.batch.util;

namespace jp.co.ivp.ers.batch.CreateListForStepMail
{
    public class CreateListForStepMailCommand
        : IErsBatchCommand
    {
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="args">コマンド引数 [Command line arguments]</param>
        /// <returns>実行結果 [Result]</returns>
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> dicArgs, DateTime? lastDate, string batchLocation)
        {
            CreateListForStepMail exec = new CreateListForStepMail();

            StepMailError error = new StepMailError(string.Empty, string.Empty);
            try
            {
                // 実行 [Execute]
                exec.Execute(dicArgs, ref error);
            }
            catch (Exception e)
            {
                error.error_message += e.ToString();
            }
            finally
            {
                // コマンドライン引数（リカバリ用）出力 [Put an argument of command line (for recovery)]
                if (!string.IsNullOrEmpty(error.command))
                {
                    this.OutputLog(batchName, executeDate, error.command);
                }

                if (!string.IsNullOrEmpty(error.error_message))
                {
                    throw new Exception(error.error_message);
                }
            }

            return;
        }

        /// <summary>
        /// ログ出力
        /// </summary>
        /// <param name="logDir">ログ出力ディレクトリ名</param>
        /// <param name="msg">ログ内容</param>
        /// <param name="prefix">ファイル名接頭語</param>
        public virtual void OutputLog(string batchName, DateTime? executeDate, string msg)
        {
            // ログファイルパス
            string logFilePath = ErsLogger.LogFilePath + batchName + "\\err\\" + executeDate.Value.ToString("yyyyMMddHHmmssfffffff") + "log.log";

            // ログ出力
            ErsFile.WriteAll(executeDate.Value.ToString() + Environment.NewLine + msg, logFilePath, false);
        }
    }
}
