﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.step_scenario;
using jp.co.ivp.ers.stepmail;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.target;

namespace jp.co.ivp.ers.batch.CreateListForStepMail
{
    class CreateListForStepMail
    {
        #region 構造体 [Structure]
        /// <summary>
        /// ステップメールパラメータ [Parameter for StepMail]
        /// </summary>
        protected struct StepMailParam
        {
            /// <summary>
            /// 指定ステップメールシナリオIDリスト [List of specified StepMailScenario id]
            /// </summary>
            public IList<int> listSpecifiedScenarioId { get; set; }

            /// <summary>
            /// 指定会員コードリスト [List of specified mcode]
            /// </summary>
            public IList<string> listSpecifiedMcode { get; set; }

            /// <summary>
            /// 指定日時 [Specified DateTime]
            /// </summary>
            public DateTime? dateSpecified { get; set; }

            /// <summary>
            /// 実行対象日時 [DateTime for Execute]
            /// </summary>
            public DateTime dateTarget { get; set; }
        }

        /// <summary>
        /// ステップメールコンテナ [Container of StepMail]
        /// </summary>
        protected struct StepMailContainer
        {
            /// <summary>
            /// ステップメールデータ [Data of StepMail]
            /// </summary>
            public Dictionary<string, object> dicStepMail { get; set; }

            /// <summary>
            /// ターゲット [ErsTarget]
            /// </summary>
            public ErsTarget objTarget { get; set; }

            /// <summary>
            /// ターゲット対象商品リスト [List of TargetItem]
            /// </summary>
            public IList<ErsTargetItem> listTargetItem { get; set; }

            /// <summary>
            /// ステップメールシナリオ [ErsStepScenario]
            /// </summary>
            public ErsStepScenario objStepScenario { get; set; }

            /// <summary>
            /// ステップメール [StepMail]
            /// </summary>
            public ErsStepMail objStepMail { get; set; }
        }
        #endregion

        #region プロパティ [Properties]
        /// <summary>
        /// 設定
        /// </summary>
        private SetupBatch setup
        {
            get
            {
                return ErsFactory.ersBatchFactory.getSetup();
            }
        }

        /// <summary>
        /// 送信数 [Count of send count]
        /// </summary>
        protected Dictionary<string, int> dicSendCount { get; set; }
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public CreateListForStepMail()
        {
            this.dicSendCount = new Dictionary<string, int>();
        }
        #endregion

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="dicArgs">コマンド引数 [Command line arguments]</param>
        /// <returns>エラー [Error]</returns>
        public void Execute(IDictionary<string, object> dicArgs, ref StepMailError error)
        {
            IList<StepMailError> listError = new List<StepMailError>();

            try
            {
                // 初期化 [Initialize]
                StepMailParam paramStepMail = this.Init(dicArgs);

                // ステップメールリスト取得 [Get List of StepMail]
                var listStepMail = this.ObtainStepMailList(paramStepMail.listSpecifiedScenarioId);

                if (listStepMail.Count == 0)
                {
                    return;
                }

                // ステップメール未配信リスト無効化 [Set non-active for non-delivery am_mailto_t]
                ErsFactory.ersStepMailFactory.GetSetNonActiveForNonDeliveryStepMailStgy().SetNonActive(listStepMail, paramStepMail.listSpecifiedScenarioId, paramStepMail.listSpecifiedMcode, paramStepMail.dateTarget);

                foreach (var data in listStepMail)
                {
                    // ステップメールコンテナ取得 [Get Container of StepMail]
                    var container = this.ObtainStepmailContainer(data);

                    // ステップメール実行 [Execute StepMail]
                    var listResult = this.ExecuteStepMail(container, paramStepMail);

                    if (listResult.Count > 0)
                    {
                        listError = listError.Concat(listResult).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                string error_message = ErsResources.GetMessage("65000", e.ToString());
                listError.Add(new StepMailError(error_message, null));
            }

            // エラー連結 [Join error]
            if (listError.Count > 0)
            {
                IList<string> listErrorMessage = new List<string>();
                IList<string> listCommand = new List<string>();

                foreach (var err in listError)
                {
                    listErrorMessage.Add(err.error_message);

                    if (!string.IsNullOrEmpty(err.command))
                    {
                        listCommand.Add(err.command);
                    }
                }

                error.error_message += String.Join(Environment.NewLine, listErrorMessage);

                if (listCommand.Count > 0)
                {
                    error.command += String.Join(Environment.NewLine, listCommand);
                }
            }

            return;
        }
        #endregion

        #region 初期化 [Initialize]
        /// <summary>
        /// 初期化 [Init]
        /// </summary>
        /// <param name="dicArgs">コマンド引数 [Command line arguments]</param>
        /// <returns>ステップメールパラメータ [Parameter for StepMail]</returns>
        protected StepMailParam Init(IDictionary<string, object> dicArgs)
        {
            StepMailParam paramStepMail = default(StepMailParam);

            // 指定シナリオID [Specified step_scenario_t.id]
            if (dicArgs.ContainsKey("scenario_id"))
            {
                paramStepMail.listSpecifiedScenarioId = new List<int>();

                string[] strSplitScenarioId = Convert.ToString(dicArgs["scenario_id"]).Split(',');

                foreach (var scenario_id in strSplitScenarioId)
                {
                    paramStepMail.listSpecifiedScenarioId.Add(Convert.ToInt32(scenario_id));
                }
            }

            // 指定会員コード [Specifield member_t.mcode]
            if (dicArgs.ContainsKey("mcode"))
            {
                paramStepMail.listSpecifiedMcode = new List<string>();

                string[] strSplitMcode = Convert.ToString(dicArgs["mcode"]).Split(',');

                foreach (var mcode in strSplitMcode)
                {
                    paramStepMail.listSpecifiedMcode.Add(mcode);
                }
            }

            // 指定日時 [Specified DateTime]
            if (dicArgs.ContainsKey("datetime"))
            {
                paramStepMail.dateSpecified = Convert.ToDateTime(dicArgs["datetime"]);
            }

            // シナリオIDと会員コードが指定されている場合は日時必須 [DateTime is required when scenario_id and mcode are specified]
            if ((dicArgs.ContainsKey("scenario_id") && dicArgs.ContainsKey("mcode")) && !dicArgs.ContainsKey("datetime"))
            {
                throw new Exception(ErsResources.GetMessage("65005"));
            }

            // 実行日時セット [Set DateTime for Execute]
            paramStepMail.dateTarget = paramStepMail.dateSpecified == null ? DateTime.Now : paramStepMail.dateSpecified.Value;

            return paramStepMail;
        }
        #endregion

        #region ステップメールリスト取得 [Get list of StepMail]
        /// <summary>
        /// ステップメールリスト取得 [ObtainStepMailList]
        /// </summary>
        /// <param name="listSpecifiedScenarioId">指定ステップメールシナリオIDリスト [List of specified StepMailScenario id]</param>
        /// <returns>ステップメールリスト [List of StepMail]</returns>
        protected IList<Dictionary<string, object>> ObtainStepMailList(IList<int> listSpecifiedScenarioId)
        {
            var spec = ErsFactory.ersStepScenarioFactory.GetStepMailSearchSpec();
            var criteria = this.GetCriteriaForStepMail(listSpecifiedScenarioId);

            return spec.GetSearchData(criteria);
        }

        /// <summary>
        /// クライテリア取得（ステップメール） [GetCriteriaForStepMail]
        /// </summary>
        /// <param name="listSpecifiedScenarioId">指定ステップメールシナリオIDリスト [List of specified StepMailScenario id]</param>
        /// <returns>ステップメールクライテリア [rsStepScenarioCriteria]</returns>
        protected ErsStepScenarioCriteria GetCriteriaForStepMail(IList<int> listSpecifiedScenarioId)
        {
            var criteria = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioCriteria();

            criteria.SetActiveOnly();

            // ID指定 [Specified id]
            if (listSpecifiedScenarioId != null)
            {
                criteria.id_in = listSpecifiedScenarioId;
            }

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByStepId(Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }
        #endregion

        #region ステップメール実行 [Execute StepMail]
        /// <summary>
        /// ステップメール実行 [ExecuteStepMail]
        /// </summary>
        /// <param name="container">ステップメールコンテナ [Container of StepMail]</param>
        /// <param name="paramStepMail">ステップメールパラメータ [Parameter for StepMail]</param>
        /// <returns>エラーリスト [List of error]</returns>
        protected IList<StepMailError> ExecuteStepMail(StepMailContainer container, StepMailParam paramStepMail)
        {
            IList<StepMailError> listError = new List<StepMailError>();

            try
            {
                // 対象会員リスト取得 [Get list of targt members]
                var listMember = ErsFactory.ersStepMailFactory.GetObtainTargetMemberListStgy().Obtain(container.objTarget, container.objStepScenario, container.objStepMail, container.listTargetItem, paramStepMail.dateTarget, paramStepMail.listSpecifiedMcode);

                if (listMember.Count == 0)
                {
                    return listError;
                }

                var stgyRegist = ErsFactory.ersStepMailFactory.GetRegistStepMailToStgy();

                using (var transaction = ErsDB_parent.BeginTransaction())
                {
                    // ステップメール登録 [Register StepMail]
                    var listResult = stgyRegist.RegistStepMail(listMember, container.objStepScenario, container.objStepMail, paramStepMail.dateTarget, this.dicSendCount,container.listTargetItem);

                    if (listResult.Count > 0)
                    {
                        listError = listError.Concat(listResult).ToList();
                    }

                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                string error_message = ErsResources.GetMessage("65001", container.objStepMail.scenario_id, container.objStepMail.id, e.ToString());
                string command = string.Format("scenario_id={0}", container.objStepMail.scenario_id);
                listError.Add(new StepMailError(error_message, command));
            }

            return listError;
        }
        #endregion

        #region ステップメールコンテナ取得 [Get container of StepMail]
        /// <summary>
        /// ステップメールコンテナ取得 [ObtainStepmailContainer]
        /// </summary>
        /// <param name="dicData">検索データ [Dictionary of search data]</param>
        /// <returns>ステップメールコンテナ [Container of StepMail]</returns>
        protected StepMailContainer ObtainStepmailContainer(Dictionary<string, object> dicData)
        {
            StepMailContainer container = default(StepMailContainer);

            container.dicStepMail = dicData;

            // ステップメールエンティティ取得 [Get Entity from search data]
            container.objStepScenario = this.ObtainStepScenarioFromSearchData(dicData);
            container.objStepMail = this.ObtainStepMailFromSearchData(dicData);

            var target_id = container.objStepScenario.target_id;
            container.objTarget = ErsFactory.ersTargetFactory.GetErsTargetWithId(target_id);

            // ステップメール対象商品リスト取得 [Get List of TargetItem]
            container.listTargetItem = ErsFactory.ersTargetFactory.GetObtainTargetItemListStgy().Obtain(target_id);

            return container;
        }
        #endregion

        #region ステップメールエンティティ取得 [Get entities of StepMail]
        /// <summary>
        /// ステップメールシナリオ取得 [ObtainStepScenarioFromSearchData]
        /// </summary>
        /// <param name="dicData">検索データ [Dictionary of search data]</param>
        /// <returns>ステップメールシナリオ [ErsStepScenario]</returns>
        protected ErsStepScenario ObtainStepScenarioFromSearchData(Dictionary<string, object> dicData)
        {
            var objStepScenario = ErsFactory.ersStepScenarioFactory.GetErsStepScenario();

            objStepScenario.OverwriteWithParameter(dicData);
            objStepScenario.id = Convert.ToInt32(dicData["scenario_id"]);

            return objStepScenario;
        }

        /// <summary>
        /// ステップメール取得 [ObtainStepMailFromSearchData]
        /// </summary>
        /// <param name="dicData">検索データ [Dictionary of search data]</param>
        /// <returns>ステップメール [ErsStepMailFromAddr]</returns>
        protected ErsStepMail ObtainStepMailFromSearchData(Dictionary<string, object> dicData)
        {
            var objStepMail = ErsFactory.ersStepMailFactory.GetErsStepMail();

            objStepMail.OverwriteWithParameter(dicData);

            return objStepMail;
        }
        #endregion
    }
}
