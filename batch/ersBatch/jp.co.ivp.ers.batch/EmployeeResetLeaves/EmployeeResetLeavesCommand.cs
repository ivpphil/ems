﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.batch.EmployeeResetLeaves
{
    public class EmployeeResetLeavesCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            this.EmployeeResetLeaves(executeDate);
        }


        internal void EmployeeResetLeaves(DateTime? executeDate)
        {
            using (var transaction = ErsDB_parent.BeginTransaction())
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                var empRepo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
                var empCri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
                empCri.SetOrderByEmployeeNo(Criteria.OrderBy.ORDER_BY_ASC);
                var list = empRepo.Find(empCri);

                foreach(var item in list)
                {
                    var leave_balance = this.setLeaveBalances(item.emp_no);
                    var leaveBalanceRepo = ErsFactory.ersRequestFactory.GetErsLeaveBalanceRepository();
                    var newLeaveBalance = ErsFactory.ersRequestFactory.GetErsLeaveBalance();

                    newLeaveBalance.emp_no = item.emp_no;
                    newLeaveBalance.last_vl = setup.vl_default;
                    newLeaveBalance.last_sl = setup.sl_default;

                    //for checking what leave must be carry over
                    if (setup.leave_carryover == EnumLeaveType.SickLeave.ToString())
                    {
                        newLeaveBalance.now_sl = Convert.ToDouble(leave_balance["sl_balance"]);
                    }
                    else if (setup.leave_carryover == EnumLeaveType.VacationLeave.ToString())
                    {
                        newLeaveBalance.now_vl = Convert.ToDouble(leave_balance["vl_balance"]);
                    }

                    newLeaveBalance.reason = ErsResources.GetMessage("from_batch");

                    leaveBalanceRepo.Insert(newLeaveBalance);
                }


                transaction.Commit();
            }
        }

        private Dictionary<string, double> setLeaveBalances(string emp_no)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            Dictionary<string, double> value = new Dictionary<string, double>();
            var repo = ErsFactory.ersRequestFactory.GetErsLeaveBalanceRepository();
            var cri = ErsFactory.ersRequestFactory.GetErsLeaveBalanceCriteria();
            cri.emp_no = emp_no;
            cri.AddOrderBy("intime", Criteria.OrderBy.ORDER_BY_DESC);
            cri.LIMIT = 1;

            var leave_bal = repo.FindSingle(cri);

            value["vl_balance"] = leave_bal != null ? Convert.ToDouble(leave_bal.total_vl) <= setup.carryover ? Convert.ToDouble(leave_bal.total_vl) : setup.carryover : 0;
            value["sl_balance"] = leave_bal != null ? Convert.ToDouble(leave_bal.total_sl) <= setup.carryover ? Convert.ToDouble(leave_bal.total_sl) : setup.carryover : 0;

            return value;
        }
    }
}
