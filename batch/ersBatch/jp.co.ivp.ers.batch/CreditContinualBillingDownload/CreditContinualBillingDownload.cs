﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.util;
using System.IO;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.batch.CreditContinualBillingDownload.model;
using jp.co.ivp.ers.Payment.continual_billing;

namespace jp.co.ivp.ers.batch.CreditContinualBillingDownload
{
    class CreditContinualBillingDownload
    {
        public List<string> listError = new List<string>();
        public List<string> listErrorForMail = new List<string>();

        private string tempFilePath;
        private DateTime currentDate;

        private string csvExtension = ".txt";
        private string tarZipExtension = ".tar.gz";
        private string uploadOkFileExtension = ".ok";

        public CreditContinualBillingDownload()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            this.tempFilePath = setup.log_path + setup.creditContinualBillingDownloadTempFilePath;
            ErsDirectory.CreateDirectories(this.tempFilePath);
        }

        internal void Execute(DateTime currentDate)
        {
            this.currentDate = currentDate;

            //最後に送信した継続課金ファイル名を取得する。
            var tarZipFileName = this.GetFileNameLastSequence();

            //サーバーからファイルDL
            if (!this.GetFileFromGmo(tarZipFileName))
            {
                //ファイルがない場合は終了
                return;
            }

            //DLファイル解凍処理
            var csvFileName = this.ExtractTarZip(tarZipFileName);

            //継続課金処理
            this.ContinualBilling(csvFileName);

            //CSVファイル削除(TarZIPはログとして残す)
            this.DeleteCsv(csvFileName, tarZipFileName);
            this.DeleteGmoFile(tarZipFileName);
        }

        #region "最後に送信した継続課金ファイル名を取得する。"
        /// <summary>
        /// 最後に送信した継続課金ファイル名を取得する。
        /// </summary>
        /// <returns></returns>
        private string GetFileNameLastSequence()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            var uploadTempFile = setup.log_path + setup.creditContinualBillingUploadTempFilePath;
            ErsDirectory.CreateDirectories(uploadTempFile);

            string[] files = null;
            string baseFileName = null;
            for (var targetDate = currentDate; targetDate >= currentDate.AddMonths(-1); targetDate = targetDate.AddMonths(-1))
            {
                baseFileName = "uri" + setup.gmo_shop_id + targetDate.ToString("yyyyMM");

                files = System.IO.Directory.GetFiles(uploadTempFile, baseFileName + "??" + tarZipExtension);

                if (files.Length != 0)
                {
                    break;
                }
            }
            if (files == null || files.Length == 0)
            {
                throw new Exception("継続課金ファイルが見つかりませんでした。");
            }

            var sequence = 0;
            foreach (var file in files)
            {
                var tempSequence = Convert.ToInt32(System.IO.Path.GetFileName(file).Replace(tarZipExtension, string.Empty).Replace(baseFileName, string.Empty));
                if (tempSequence > sequence)
                {
                    sequence = tempSequence;
                }
            }

            if (sequence != 0)
            {
                return "R" + baseFileName + VBStrings.Right("0" + sequence, 2) + this.tarZipExtension;
            }
            else
            {
                return string.Empty;
            }
        }
        #endregion

        #region "サーバーからファイルDL"
        /// <summary>
        /// サーバーからファイルDL
        /// </summary>
        private bool GetFileFromGmo(string tarZipFileName)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            using (var sftp = this.GetSftpConnection())
            {
                var localFilePath = this.tempFilePath + tarZipFileName;
                var ftpFilePath = setup.creditContinualBillingDownloadSftpUserSftpPutPath + tarZipFileName;

                //GMO側の終了フラグファイルがなければ終了
                if (!sftp.Exists(ftpFilePath + this.uploadOkFileExtension))
                {
                    return false;
                }

                //サーバーから対象ファイルDL
                sftp.GetFile(ftpFilePath, localFilePath);

                sftp.Close();
            }
            return true;
        }

        /// <summary>
        /// SFTPのコネクションを取得する
        /// </summary>
        /// <returns></returns>
        private SFTPClient GetSftpConnection()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            string upTempPath = null;
            if (setup.creditContinualBillingDownloadSftpSshKeyPath.HasValue())
            {
                upTempPath = setup.root_path + setup.creditContinualBillingDownloadSftpSshKeyPath;
            }
            
            return SFTPClient.Connect(
                            setup.creditContinualBillingDownloadSftpHost,
                            setup.creditContinualBillingDownloadSftpUser,
                            setup.creditContinualBillingDownloadSftpPass,
                            setup.creditContinualBillingDownloadSftpPort,
                            upTempPath,
                            setup.creditContinualBillingDownloadSftpSshPassPhrase);
        }
        #endregion

        #region "DLファイル解凍処理"
        /// <summary>
        /// DLファイル解凍処理
        /// </summary>
        /// <param name="tarZipFileName"></param>
        /// <returns></returns>
        private string ExtractTarZip(string tarZipFileName)
        {
            //圧縮ファイル作成
            var zipFileManager = ErsFactory.ersUtilityFactory.GetTarZipFileManager();
            zipFileManager.Extract(tempFilePath + tarZipFileName, tempFilePath);

            //ファイル名返却
            var CsvFileName = this.GetCsvFileName(tarZipFileName);
            return CsvFileName;
        }

        /// <summary>
        /// CSVファイル名取得
        /// </summary>
        /// <returns></returns>
        private string GetCsvFileName(string tarZipFileName)
        {
            return System.IO.Path.GetFileName(tarZipFileName).Replace(tarZipExtension, string.Empty) + csvExtension;
        }
        #endregion

        #region "継続課金処理"
        /// <summary>
        /// 継続課金処理
        /// </summary>
        /// <param name="csvFileName"></param>
        private void ContinualBilling(string csvFileName)
        {
            //CSVファイル読み込み
            var csvLoader = ErsFactory.ersUtilityFactory.GetErsCsvContainer<CsvDownloadRecord>();
            csvLoader.LoadPostedFile(this.tempFilePath + csvFileName);
            foreach (var model in csvLoader.GetValidatedModels(false))
            {
                if (!model.IsValid)
                {
                    string errMsg = DateTime.Now.ToString() + "," + csvFileName + "," + String.Join(Environment.NewLine, model.GetAllErrorMessageList()) + Environment.NewLine + Environment.NewLine;
                    this.listError.Add(errMsg);
                }
            }

            //結果にあった処理開始
            var importFactory = ErsFactory.ersOrderFactory.GetImportContinualBillingResultStgyFactory();

            using (var transaction = ErsDB_parent.BeginTransaction())
            {
                foreach (var model in csvLoader.GetValidModels())
                {
                    try//エラーは1件づつハンドリングする
                    {
                        //結果にあった処理開始
                        var importStgy = importFactory.GetImporter(model);
                        importStgy.Import(model);
                    }
                    catch (ImportContinualBillingException ex)
                    {
                        this.listErrorForMail.Add(model.credit_order_id + "\t" + ex.Message);
                    }
                    catch (Exception ex)
                    {
                        string errMsg = DateTime.Now.ToString() + "," + model.credit_order_id + "," + ex.ToString() + Environment.NewLine + Environment.NewLine;
                        this.listError.Add(errMsg);
                    }
                }

                transaction.Commit();
            }
        }
        #endregion

        #region "CSVファイル削除"
        /// <summary>
        /// CSVファイル削除
        /// </summary>
        /// <param name="csvFileName"></param>
        private void DeleteCsv(string csvFileName, string tarZipFileName)
        {
            File.Delete(this.tempFilePath + csvFileName);
        }
        #endregion

        #region "GMO側のファイルを削除する"
        /// <summary>
        /// GMO側のファイルを削除する
        /// </summary>
        /// <param name="tarZipFileName"></param>
        private void DeleteGmoFile(string tarZipFileName)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            using (var sftp = this.GetSftpConnection())
            {
                var ftpFilePath = setup.creditContinualBillingDownloadSftpUserSftpPutPath + tarZipFileName;

                //サーバーからファイルの削除
                sftp.DeleteFile(ftpFilePath + this.uploadOkFileExtension);
                sftp.DeleteFile(ftpFilePath);

                sftp.Close();
            }
        }
        #endregion
    }
}
