﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch.CreditContinualBillingDownload
{
    public class CreditContinualBillingDownloadCommand
          : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> argDictionary, DateTime? lastDate, string batchLocation)
        {
            CreditContinualBillingDownload exec = new CreditContinualBillingDownload();

            var currentDate = argDictionary.ContainsKey("date") ? Convert.ToDateTime(argDictionary["date"]) : DateTime.Now;

            string nowDate = currentDate.ToString("yyyy年MM月dd日");
            string exeName = System.IO.Path.GetFileName(System.Reflection.Assembly.GetCallingAssembly().Location);

            exec.Execute(currentDate);

            try
            {
                if (exec.listErrorForMail.Count != 0)
                {
                    var message = string.Join(Environment.NewLine, exec.listErrorForMail);
                    this.SendGmoError(message, nowDate, exeName);
                }
            }
            catch (Exception ex)
            {
                string errMsg = DateTime.Now.ToString() + ",<SENDMAIL>," + ex.ToString() + Environment.NewLine + Environment.NewLine;
                exec.listError.Add(errMsg);
            }

            //エラーが有った場合は例外
            if (exec.listError.Count != 0)
            {
                throw new Exception(string.Join(Environment.NewLine, exec.listError));
            }
        }

        private void SendGmoError(string message, string nowDate, string exeName)
        {
            // メール文言の修正がひつよう

            var setUp = ErsFactory.ersBatchFactory.getSetup();

            //管理者宛メール処理
            var sbMail_body = new StringBuilder();
            sbMail_body.AppendLine("ご担当者様");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("本日以下の伝票にて継続課金エラーが発生しております。");
            sbMail_body.AppendLine("請求処理のご対応を宜しくお願いいたします。");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("■{current_date} クレジットエラー発生");
            sbMail_body.AppendLine("{message}");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("※本メールはメール自動配信システムから送信しております。");

            var values = new Dictionary<string, object>() { { "current_date", nowDate }, { "message", message } };

            var mail_body = StringExtension.Format(sbMail_body.ToString(), values);

            var mail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();
            mail.MailSend(
                setUp.creditContinualBillingDownloadGmoErrMailTitle, mail_body,
                exeName,
                setUp.creditContinualBillingDownloadGmoErrMailTo, setUp.creditContinualBillingDownloadGmoErrMailFrom,
                setUp.creditContinualBillingDownloadGmoErrMailCc, setUp.creditContinualBillingDownloadGmoErrMailBcc);
        }
    }
}
