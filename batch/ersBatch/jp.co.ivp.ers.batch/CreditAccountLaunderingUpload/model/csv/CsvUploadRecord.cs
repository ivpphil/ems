﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;

namespace jp.co.ivp.ers.batch.CreditAccountLaunderingUpload.model.csv
{

    public class CsvUploadRecord : ErsBindableModel
    {
        [CsvField]
        [ErsSchemaValidation("member_card_t.mcode")]
        public virtual string mcode { get; set; }

        [CsvField]
        public virtual string card_sequence { get; set; }

        [CsvField]
        public virtual string remark { get { return string.Empty; } }
    }
}
