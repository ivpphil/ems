﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch.CreditAccountLaunderingUpload
{
    public class CreditAccountLaunderingUploadCommand
          : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> argDictinary, DateTime? lastDate, string batchLocation)
        {
            CreditAccountLaunderingUpload exec = new CreditAccountLaunderingUpload();

            exec.Execute(argDictinary);
        }
    }
}
