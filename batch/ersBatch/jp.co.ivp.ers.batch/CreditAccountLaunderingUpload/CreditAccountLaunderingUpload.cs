﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using System.IO;
using jp.co.ivp.ers.batch.CreditAccountLaunderingUpload.model.csv;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.batch.CreditAccountLaunderingUpload
{
    class CreditAccountLaunderingUpload
    {
        private string tempFilePath;
        private DateTime currentDate;

        private string csvExtension = ".txt";
        private string tarZipExtension = ".tar.gz";
        private string uploadOkFileExtension = ".ok";

        public CreditAccountLaunderingUpload()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            this.tempFilePath = setup.log_path + setup.creditAccountLaunderingUploadTempFilePath;
            ErsDirectory.CreateDirectories(this.tempFilePath);
        }

        internal void Execute(IDictionary<string, object> argDictinary)
        {
            this.currentDate = argDictinary.ContainsKey("date") ? Convert.ToDateTime(argDictinary["date"]) : DateTime.Now;

            //定期購読中会員のリストを取得
            Console.WriteLine("定期購読中会員のリストを取得...");
            var listMemberCard = this.GetRegularMemberList();

            //リストがなかったら終了
            if (listMemberCard.Count() == 0)
            {
                Console.WriteLine("会員がいないので終了:" + DateTime.Now.ToLongTimeString());
                return;
            }

            //CSVの生成
            Console.WriteLine("CSVの生成...");
            var csvFileName = this.CreateCsv(listMemberCard);

            //TarZIPファイルの生成
            var tarZipFileName = this.CreateTarZip(csvFileName);

            //SFTPにて、既定のディレクトリへ転送（PUT）
            Console.WriteLine("SFTPにて、既定のディレクトリへ転送...");
            this.PutTarZip(tarZipFileName);

            //CSVファイル削除(TarZIPはログとして残す)
            Console.WriteLine("CSVファイル削除...");
            this.DeleteCsv(csvFileName, tarZipFileName);
        }

        #region "定期購読中会員のリストを取得"
        /// <summary>
        /// 定期購読中会員のリストを取得
        /// </summary>
        /// <returns></returns>
        private IEnumerable<ErsMemberCard> GetRegularMemberList()
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
            criteria.active = EnumActive.Active;
            criteria.SetHasRegular();
            criteria.update_status = EnumCardUpdateStatus.Success;
            return repository.Find(criteria);
        }
        #endregion

        #region "CSVの生成"
        /// <summary>
        /// CSVの生成
        /// </summary>
        /// <param name="listArticles"></param>
        /// <returns></returns>
        private string CreateCsv(IEnumerable<ErsMemberCard> listMemberCard)
        {
            //ファイル名の取得
            var fileName = this.GetCsvFileName();

            //CSVファイルをtempフォルダへ作成
            this.CreateCsvFile(fileName, listMemberCard);

            //パスを返却
            return fileName;
        }

        /// <summary>
        /// ファイル名の取得
        /// </summary>
        /// <returns></returns>
        private string GetCsvFileName()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            var baseFileName = "arai" + setup.gmo_shop_id + currentDate.ToString("yyyyMM");

            //ファイル名の下二桁(月内連番)取得
            var sequence = this.GetFileNameSequence(baseFileName);

            //作成するCSVファイルは、その日の1回目、2回目でファイル名を変える
            return baseFileName + VBStrings.Right("0" + sequence, 2) + this.csvExtension;
        }

        /// <summary>
        /// ファイル名の下二桁(月内連番)取得
        /// </summary>
        /// <returns></returns>
        private int GetFileNameSequence(string baseFileName)
        {
            var files = System.IO.Directory.GetFiles(tempFilePath, baseFileName + "??" + tarZipExtension);
            var sequence = 0;
            foreach (var file in files)
            {
                var tempSequence = Convert.ToInt32(System.IO.Path.GetFileName(file).Replace(tarZipExtension, string.Empty).Replace(baseFileName, string.Empty));
                if (tempSequence > sequence)
                {
                    sequence = tempSequence;
                }
            }
            return sequence + 1;
        }

        /// <summary>
        /// CSVファイルをtempフォルダへ作成
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="listMember"></param>
        private void CreateCsvFile(string fileName, IEnumerable<ErsMemberCard> listMemberCard)
        {
            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            using (var writer = csvCreater.GetWriter(tempFilePath, fileName))
            {
                //this.WriteCsvHeader<CsvUploadRecord>(writer, value => SynergyCsvFormatter.Format(value));

                foreach (var memberCard in listMemberCard)
                {
                    var csvModel = new CsvUploadRecord();
                    csvModel.mcode = memberCard.card_mcode;

                    csvModel.card_sequence = memberCard.card_sequence;

                    csvCreater.WriteBody(csvModel, writer);
                }
            }
        }

        #endregion

        #region "TarZIPファイルの生成"
        /// <summary>
        /// TarZIPファイルの生成
        /// </summary>
        /// <param name="csvFileName"></param>
        /// <returns></returns>
        private string CreateTarZip(string csvFileName)
        {
            //ZIPファイル保存先取得
            var zipFileName = this.GetZipFileName(csvFileName);

            //圧縮ファイル作成
            var zipFileManager = ErsFactory.ersUtilityFactory.GetTarZipFileManager();
            zipFileManager.Compress(tempFilePath + csvFileName, tempFilePath + zipFileName);

            //Put完了ファイルを作成
            this.CreateUploadOkFile(zipFileName);

            //パスを返却
            return zipFileName;
        }

        /// <summary>
        /// ZIPファイル保存先取得
        /// </summary>
        /// <returns></returns>
        private string GetZipFileName(string csvFileName)
        {
            return System.IO.Path.GetFileNameWithoutExtension(csvFileName) + tarZipExtension;
        }

        /// <summary>
        /// Put完了ファイルを作成
        /// </summary>
        /// <param name="fileName"></param>
        private void CreateUploadOkFile(string zipFileName)
        {
            using (var hStream = System.IO.File.Create(tempFilePath + zipFileName + uploadOkFileExtension))
            {
                // 作成時に返される FileStream を利用して閉じる
                if (hStream != null)
                {
                    hStream.Close();
                }
            }
        }
        #endregion

        #region "SFTPにて、既定のディレクトリへ転送（PUT）"
        /// <summary>
        /// SFTPにて、既定のディレクトリへ転送（PUT）
        /// </summary>
        /// <param name="csvFileName"></param>
        private void PutTarZip(string tarZipFileName)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            string upTempPath = null;
            if (setup.creditAccountLaunderingUploadSftpSshKeyPath.HasValue())
            {
                upTempPath = setup.root_path + setup.creditAccountLaunderingUploadSftpSshKeyPath;
            }

            using (var sftp = SFTPClient.Connect(
                setup.creditAccountLaunderingUploadSftpHost,
                setup.creditAccountLaunderingUploadSftpUser,
                setup.creditAccountLaunderingUploadSftpPass,
                setup.creditAccountLaunderingUploadSftpPort,
                upTempPath,
                setup.creditAccountLaunderingUploadSftpSshPassPhrase))
            {
                var sourcePath = this.tempFilePath + tarZipFileName;
                var putPath = setup.creditAccountLaunderingUploadSftpUserSftpPutPath + tarZipFileName;

                //TarZipをput
                sftp.PutFile(sourcePath, putPath);

                //サーバーにアップ完了フラグファイルのアップ
                sftp.PutFile(sourcePath + uploadOkFileExtension, putPath + uploadOkFileExtension);

                sftp.Close();
            }
        }
        #endregion

        #region "Delete csv"
        /// <summary>
        /// CSVファイル削除
        /// </summary>
        /// <param name="csvFileName"></param>
        private void DeleteCsv(string csvFileName, string tarZipFileName)
        {
            File.Delete(this.tempFilePath + csvFileName);
            File.Delete(this.tempFilePath + tarZipFileName + uploadOkFileExtension);
        }
        #endregion
    }
}
