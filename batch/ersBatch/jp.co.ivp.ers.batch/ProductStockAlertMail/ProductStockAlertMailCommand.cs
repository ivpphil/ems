﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using jp.co.ivp.ers.sendmail.mass_send;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.batch.ProductStockAlertMail
{
    public class ProductStockAlertMailCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            var mailBatch = ErsFactory.ersBatchFactory.GetErsSendMailBatch();

            this.BindMailBody(mailBatch);

            if (!String.IsNullOrEmpty(mailBatch.body))
                this.sendMail(mailBatch);
        }


        internal void BindMailBody(ErsSendMailBatch mailBatch)
        {
            ProductStockAlert model = new ProductStockAlert();
            model.productStockAlertList = new List<Dictionary<string, object>>();

            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var skuCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();

            skuCriteria.greater_stock_alert = 0;

            var skuList = skuRepository.Find(skuCriteria);
            var listStrBody = new List<string>();
            bool HasStockAlert = false;

            StringBuilder bodyStrBuilder = new StringBuilder();

            
            listStrBody.Add("<ers:foreach src=\".Model.productStockAlertList\" variable=\"record\">");

            bodyStrBuilder.Append("<ers:formatString src=\"record.scode\" padRight=\"24\" fixed=\"24\" />");
            bodyStrBuilder.Append("｜<ers:formatString src=\"record.stock\" padLeft=\"18\" fixed=\"18\"/>");
            bodyStrBuilder.Append("｜<%=record.sname%>");
            bodyStrBuilder.Append("</ers:foreach>");


            listStrBody.Add(bodyStrBuilder.ToString());  

            foreach (var record in skuList)
            {
                var stock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(record.scode);

                if (stock != null)
                {
                    if (stock.stock < record.stock_alert_amount)
                    {
                        var productDic = record.GetPropertiesAsDictionary();
                        productDic["stock"] = stock.stock;
                        productDic["scode_length"] = ErsCommon.GetByteCount(record.scode) % 2;

                        model.productStockAlertList.Add(productDic);
                        HasStockAlert = true;
                    }
                }
            }

            if (!HasStockAlert)
                return;
            

            var strBody = String.Join(Environment.NewLine, listStrBody);

            mailBatch.Bind(model, strBody);

            listStrBody = new List<string>();
            listStrBody.Add("販売在庫が閾値を下回る商品を検知いたしました。");
            listStrBody.Add("--------------------------------------------------");
            listStrBody.Add("商品番号                ｜        販売可能数｜商品名");

            mailBatch.body = String.Join(Environment.NewLine, listStrBody) + mailBatch.body.Substring(1);
        }

        internal void sendMail(ErsSendMailBatch mailBatch)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            var ersSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSite();

            mailBatch.MailSend(
                setup.ProductStockAlertMail_mail_title,
                mailBatch.body,
                System.IO.Path.GetFileName(System.Reflection.Assembly.GetCallingAssembly().Location),
                setup.ProductStockAlertMail_email_address,
                ersSetup.f_email1,
                string.Empty,
                string.Empty);
        }
    }
}
