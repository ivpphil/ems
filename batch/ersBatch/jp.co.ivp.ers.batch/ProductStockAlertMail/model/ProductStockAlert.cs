﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail.mass_send;

namespace jp.co.ivp.ers.batch.ProductStockAlertMail
{
    class ProductStockAlert
        : ErsModelBase
    {
        public List<Dictionary<string, object>> productStockAlertList { get; set; }
    }
}
