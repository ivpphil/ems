﻿using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.batch.UploadEmpSchedule
{
    public class UploadEmpScheduleFile : IErsBatchCommand
    {
        int? batch_log_id = 0;
        Setup setup = ErsFactory.ersUtilityFactory.getSetup();


        

        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            try
            {              
                if (File.Exists(setup.download_path_folder))
                {
                    File.Delete(setup.download_path_folder);
                }

                var batchDetails = ErsFactory.ersRequestFactory.getErsScheduleBatchLogWithDate(DateTime.Today);
                if (batchDetails == null)
                {
                    InsertLog();
                }
                else
                {
                    batch_log_id = batchDetails.id;
                    UpdateLog(EnumBatchStatus.Executed);
                }

                
                ExportSchedules();
                ImportSchedules();

                UpdateLog(EnumBatchStatus.Success);
            }
            catch (Exception ex)
            {
                UpdateLog(EnumBatchStatus.Error,ex.Message,Convert.ToString(ex.InnerException));
            }
        }

        public void ExportSchedules()
        {
                 
            // Setup date range filter
            // Start of the week is always on Sunday
            DateTime startOfTheWeek = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Sunday);
            string DateFrom = String.Format("{0:MM/dd/yy}", startOfTheWeek);
            string DateTo = "";

            string today = DateTime.Now.DayOfWeek.ToString();

            if (setup.newSchedDays.Contains(today))               // NewSchedDays : Retrieve this weeks schedule and + a weeks onward. 
            {            
                 DateTo = String.Format("{0:MM/dd/yy}", startOfTheWeek.AddDays(setup.newSchedGetLength));
            }
            else if (setup.weekDays.Contains(today))        // WeekDays : Retrieve this weeks schedule only.
            {
                DateTo = String.Format("{0:MM/dd/yy}", startOfTheWeek.AddDays(setup.thisWeekSchedGetLength));
            }


            // --------------------------------- START OF NAVIGATING THE CHROME BROWSER ---------------------------------------------------------


            IWebDriver driver = new ChromeDriver(setup.chrome_driver_path);

            driver.Url = setup.desknet_login_page;

            driver.FindElement(By.Name(setup.desknet_username_element)).SendKeys(setup.desknet_username);
            driver.FindElement(By.Name(setup.desknet_password_element)).SendKeys(setup.desknet_password);
            driver.FindElement(By.ClassName(setup.login_button)).Click();

            // To get the Xpath 
            // Requirements :
            //  1. Firefox w/ Firebug.
            //  2. Inspect element and then copy the Xpath.

            // Clicking the Select button on Export Target User
            Thread.Sleep(10000);
            driver.Navigate().GoToUrl(setup.desknet_export_page);

            var dateFromElement = driver.FindElement(By.XPath(setup.xpath_date_from));
            var dateToElement = driver.FindElement(By.XPath(setup.xpath_date_to));

            Thread.Sleep(5000);

            dateFromElement.Clear();
            Thread.Sleep(1000);
            dateFromElement.SendKeys(DateFrom);             // Set Date value to Date From
            Thread.Sleep(1000);
            dateToElement.Clear();
            Thread.Sleep(1000);
            dateToElement.SendKeys(DateTo);                 // Set Date value to Date To

            // Click select button for exporting CSV file.
            Thread.Sleep(5000);
            driver.FindElement(By.XPath(setup.xpath_select_button)).Click();

            // Expanding the IVP Global Anchor
            Thread.Sleep(5000);
            driver.FindElement(By.XPath(setup.xpath_ivp_global_anchor_element)).Click();

            // Expanding the Development Division
            Thread.Sleep(5000);
            driver.FindElement(By.XPath(setup.xpath_development_division_anchor_element)).Click();

            // Selecting the ERS Team
            Thread.Sleep(5000);
            driver.FindElement(By.XPath(setup.xpath_ers_team_anchor_element)).Click();

            // Adding all members
            Thread.Sleep(5000);
            driver.FindElement(By.XPath(setup.xpath_add_all_members)).Click();

            // clickng the Ok button
            Thread.Sleep(5000);
            driver.FindElement(By.XPath(setup.xpath_ok_button)).Click();

            // Clicking the Ok Export for printing the Schedule into CSV Format
            Thread.Sleep(5000);
            driver.FindElement(By.XPath(setup.xpath_export_button)).Submit();

            Thread.Sleep(15000);
            driver.Quit();
        }
        public void ImportSchedules()
        {
            IWebDriver driver = new ChromeDriver(setup.chrome_driver_path);

            driver.Url = setup.ems_sched_import_page;

            Thread.Sleep(5000);
            driver.FindElement(By.Name(setup.csv_file)).SendKeys(setup.download_path_folder); ;

            Thread.Sleep(5000);
            driver.FindElement(By.Name(setup.btn_import_sched)).Submit();

            Thread.Sleep(15000);
            File.Delete(setup.download_path_folder);
            driver.Quit();
        }

        public void InsertLog()
        {
            var repository = ErsFactory.ersRequestFactory.GetErsScheduleBatchLogRepository();
            var entity = ErsFactory.ersRequestFactory.GetErsScheduleBatchLog();

            entity.details = setup.batch_executed;
            entity.status = EnumBatchStatus.Executed;

            repository.Insert(entity,true);

            batch_log_id = entity.id;

        }

        public void UpdateLog(EnumBatchStatus? BatchStatus, string ErrorMessage = null, string ErrorException = null)
        {
            var repository = ErsFactory.ersRequestFactory.GetErsScheduleBatchLogRepository();

            var old_log = ErsFactory.ersRequestFactory.getErsScheduleBatchLogWithID((int)batch_log_id);
            var new_log = ErsFactory.ersRequestFactory.getErsScheduleBatchLogWithParameter(old_log.GetPropertiesAsDictionary());

            if (BatchStatus == EnumBatchStatus.Success)
            {
                new_log.details = setup.batch_success;
                new_log.error_details = string.Empty;
                new_log.error_exception = string.Empty;
                new_log.status = EnumBatchStatus.Success;
            }
            else if (BatchStatus == EnumBatchStatus.Executed)
            {
                new_log.details = setup.batch_executed;
                new_log.error_details = string.Empty;
                new_log.error_exception = string.Empty;
                new_log.status = EnumBatchStatus.Executed;
            }
            else
            {
                new_log.details = setup.batch_error;
                new_log.error_details = ErrorMessage;
                new_log.error_exception = ErrorException;
                new_log.status = EnumBatchStatus.Error;
            }
      
            repository.Update(old_log, new_log);
        }

    }
}
