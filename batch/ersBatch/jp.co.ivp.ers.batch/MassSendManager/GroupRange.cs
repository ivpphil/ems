﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.batch.MassSendManager
{
    public class GroupRange
    {
        public int fromId { get; set; }
        public int toId { get; set; }
        public int? taskProcessId { get; set; }
        public int? process_Id { get; set; }
        public long? recordCount { get; set; }
        public DateTime? relaunchTime { get; set; }
    }
}
