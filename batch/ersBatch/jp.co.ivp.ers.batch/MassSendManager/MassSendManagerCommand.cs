﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch.MassSendManager
{
    public class MassSendManagerCommand
            : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            //実行日の算出
            executeDate = (options.ContainsKey("date")) ? Convert.ToDateTime(options["date"]) : executeDate ?? DateTime.Now;
            
            var massSendMgr = new MassSendManager();
            var CountDetail = massSendMgr.Execute(batchLocation, executeDate);
        }
    }
}
