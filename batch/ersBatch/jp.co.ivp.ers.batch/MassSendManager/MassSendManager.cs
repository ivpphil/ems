﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.atmail;
using System.Threading;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using System.Collections;
using jp.co.ivp.ers.db;
using System.Diagnostics;
using jp.co.ivp.ers;
using System.ComponentModel;
using System.Runtime.InteropServices;
using jp.co.ivp.ers.batch.util;
using System.Collections.Concurrent;

namespace jp.co.ivp.ers.batch.MassSendManager
{
    public class MassSendManager
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public int Execute(string batchLocation, DateTime? executeDateTime)
        {
            var massSendExePath = this.PrepareProcessExeFile(batchLocation, executeDateTime);

            var setupFromMail = ErsFactory.ErsAtMailFactory.GetErsAmSetupRepository();
            var setupFromMailResult = setupFromMail.Find(null);
            if (setupFromMailResult.Count() == 0)
            {
                throw new Exception("No record found in am_setup_t");
            }

            var mailFrom = setupFromMailResult[0].r_email.ToString();
            var replayTo = setupFromMailResult[0].p_email.ToString();

            ////対象リスト取得
            var dateList = this.findAmProcess(executeDateTime);

            var exceptions = new ConcurrentQueue<Exception>();
            System.Threading.Tasks.Parallel.ForEach(dateList.ToList(), (objOld) =>
            {
                try
                {
                    var setup = ErsFactory.ersBatchFactory.getSetup();

                    var batchDataContainer = this.GetBatchDataContainer(batchLocation, executeDateTime);
                    batchDataContainer.batchExePath = massSendExePath;
                    
                    //D4) Select records from am_mailto_t which are related to am_process_t 
                    var listResult = this.findMailTo(objOld.id, executeDateTime);
                    if (listResult.Count > 0)
                    {
                        var groupProces = this.getProcessGroupWithoutSendAtOnce(listResult, setup.MassSendManager_ProcessesNumber.Value);

                        //Update am_process_t into Delivering
                        this.updateAmProcessToSending(objOld.id.Value);

                        //Send to Mass
                        var processApps = this.sendToMassSend(groupProces, objOld, mailFrom, replayTo, batchDataContainer, executeDateTime);

                        //Monitor launch process
                        this.MonitorProcess(objOld, processApps, mailFrom, replayTo, batchDataContainer, executeDateTime); //CHECK IF ALL MAIL HAS BEEN SENT
                    }

                    this.updateAmProcessToSent(objOld.id.Value);
                }
                catch (Exception ex)
                {
                    // 例外追加 [Add the exception]
                    exceptions.Enqueue(ex);
                }
            });

            //1件でもエラーがあれば
            if (exceptions.Count != 0)
            {
                var insertErrArr = new List<string>();
                foreach (var ex in exceptions)
                {
                    insertErrArr.Add(DateTime.Now + ", " + ex.ToString());
                }

                throw new Exception(string.Join(Environment.NewLine, insertErrArr));
            }

            return dateList.Count();
        }

        /// <summary>
        /// Exeファイルを作成する
        /// </summary>
        /// <param name="batchLocation"></param>
        /// <param name="executeDateTime"></param>
        private string PrepareProcessExeFile(string batchLocation, DateTime? executeDateTime)
        {
            var batchDataContainer = this.GetBatchDataContainer(batchLocation, executeDateTime);

            ErsBatchProcess.PrepareProcessExeFile(batchDataContainer);

            return batchDataContainer.batchExePath;
        }

        /// <summary>
        /// GetDataContainer to Execute Process
        /// </summary>
        /// <param name="batchLocation"></param>
        /// <returns></returns>
        private BatchDataContainer GetBatchDataContainer(string batchLocation, DateTime? executeDateTime)
        {
            var massSendType = typeof(jp.co.ivp.ers.batch.MassSend.MassSendCommand);

            var batchDataContainer = new BatchDataContainer();
            batchDataContainer.executeDate = executeDateTime;
            batchDataContainer.batchId = massSendType.Name;
            batchDataContainer.batchName = massSendType.Name;
            batchDataContainer.class_name = massSendType.FullName;
            batchDataContainer.enableMutex = false;
            batchDataContainer.batchLocation = batchLocation;
            return batchDataContainer;
        }

        /// <summary>
        /// GO TO MASS SEND
        /// </summary>
        /// <param name="groupProces"></param>
        /// <param name="config"></param>
        private List<GroupRange> sendToMassSend(List<GroupRange> groupProces, ErsProcess ersProcess, string mailFrom, string replayTo, BatchDataContainer batchDataContainer, DateTime? executeDateTime)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var processApps = new List<GroupRange>();
            try
            {
                foreach (var groupList in groupProces)
                {
                    GroupRange processApp = new GroupRange();

                    //D4) Select records from am_mailto_t which are related to am_process_t 
                    processApp.recordCount = this.GetErsAtMailCountDeliver(groupList.fromId, groupList.toId, ersProcess.id, executeDateTime);

                    var process = this.launchProcess(groupList.fromId, groupList.toId, ersProcess, mailFrom, replayTo, batchDataContainer, executeDateTime);

                    processApp.fromId = groupList.fromId;
                    processApp.toId = groupList.toId;
                    processApp.taskProcessId = process.Id;
                    processApp.process_Id = ersProcess.id;
                    processApp.relaunchTime = DateTime.Now.AddMilliseconds(setup.MassSendManager_stopTime.Value);
                    processApps.Add(processApp);
                }
            }
            catch (Exception)
            {
                foreach (var list in processApps)
                {
                    killProcess((int)list.taskProcessId);
                }
                throw;
            }
            return processApps;
        }

        private Process launchProcess(int fromId, int toId, ErsProcess ersProcess, string mailFrom, string replayTo, BatchDataContainer batchDataContainer, DateTime? executeDateTime)
        {
            var from_email = mailFrom;
            var reply_email = replayTo;
            if (!string.IsNullOrEmpty(ersProcess.from_email))
            {
                if (!string.IsNullOrEmpty(ersProcess.from_name))
                {
                    from_email = string.Format("{0} <{1}>", ersProcess.from_name, ersProcess.from_email);
                }
                else
                {
                    from_email = ersProcess.from_email;
                }
            }

            if (!string.IsNullOrEmpty(ersProcess.reply_email))
            {
                reply_email = ersProcess.reply_email;
            }

            var listArgument = new List<string>();
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("processID", Convert.ToString(ersProcess.id), false));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("idFrom", Convert.ToString(fromId), false));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("idTo", Convert.ToString(toId), false));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("mailFrom", from_email, false));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("replayTo", reply_email, false));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("subject", Convert.ToString(ersProcess.subject), false));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("body", Convert.ToString(ersProcess.body), false));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("html_body", Convert.ToString(ersProcess.html_body), false));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("feature_body", Convert.ToString(ersProcess.feature_body), false));
            listArgument.Add(ErsObtainOption.GetOptionKeyValuePair("ExecuteDateTime", executeDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss.fffffff"), false));

            var options = string.Join(" ", listArgument);

            batchDataContainer.options = options;

            return ErsBatchProcess.ExecuteProcess(batchDataContainer, EnumBatchMode.ExecuteClass);
        }

        /// <summary>
        /// Monitor Process
        /// </summary>
        /// <param name="config"></param>
        /// <param name="environment"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="html_body"></param>
        /// <param name="future_body"></param>
        /// <returns></returns>
        private void MonitorProcess(ErsProcess ersProcess, List<GroupRange> processApps, string mailFrom, string replayTo, BatchDataContainer batchDataContainer, DateTime? executeDateTime)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            int contRelaunch = 0;
            int MAX_RELAUNCH_COUNT = processApps.Count * 2; //起動プロセス数*2回までリピート
            int interval = 10000;//10秒ごとにステータスチェック
            try
            {
                while (true)
                {
                    Thread.Sleep(interval);
                    List<GroupRange> processTemp = new List<GroupRange>();
                    foreach (var processApp in processApps)
                    {
                        processTemp.Add(processApp);
                    }

                    //Check if Process working
                    foreach (var process in processTemp)
                    {
                        var processId = process.process_Id;

                        //D5) Issue the following SQL regularly and check whether the number of non-delivery decreases.
                        long resultCount = this.GetErsAtMailCountDeliver(process.fromId, process.toId, process.process_Id, executeDateTime);
                        if (resultCount == 0)
                        {
                            //if all Mail Sent remove process in list
                            processApps.Remove(process);
                        }
                        else if (resultCount == process.recordCount && process.relaunchTime < DateTime.Now)
                        {
                            //check limit for retry
                            if (++contRelaunch > MAX_RELAUNCH_COUNT)
                            {
                                throw new Exception("Failed to send(retry times reach to " + MAX_RELAUNCH_COUNT + "). please check log for MassSendCommand.");
                            }

                            //D5) If the number of non-delivery does not decrease, judge it to have stopped. Then kill and restart. 
                            killProcess((int)process.taskProcessId);
                            var processNew = launchProcess(process.fromId, process.toId, ersProcess, mailFrom, replayTo, batchDataContainer, executeDateTime);
                            process.taskProcessId = processNew.Id;
                            process.relaunchTime = DateTime.Now.AddMilliseconds(setup.MassSendManager_stopTime.Value);
                        }
                        else
                        {
                            process.recordCount = resultCount;
                        }
                    }

                    //exit
                    if (processApps.Count == 0)
                    {
                        return;
                    }
                }
            }
            catch
            {
                foreach (var processApp in processApps)
                {
                    killProcess((int)processApp.taskProcessId);
                }

                throw;
            }
        }



        /// <summary>
        /// Less than Send at once
        /// </summary>
        /// <param name="listResult"></param>
        /// <param name="processNumber"></param>
        /// <returns></returns>
        private List<GroupRange> getProcessGroupWithoutSendAtOnce(IList<ErsMailTo> listResult, int processNumber)
        {
            int grp = listResult.Count() / processNumber;

            List<GroupRange> groupProcess = new List<GroupRange>();
            GroupRange groupRange = new GroupRange();
            int totalCount = listResult.Count();

            if (listResult.Count < processNumber)
            {
                groupRange.fromId = (int)listResult[0].id;
                groupRange.toId = (int)listResult[totalCount - 1].id;
                groupProcess.Add(groupRange);
                return groupProcess;
            }
            int? fromId = 0;
            int? toId = 0;
            int loopCount = 0;
            int? lastFrom = 0;
            for (int x = 0; x < listResult.Count; x++)
            {
                loopCount++;
                if (fromId == 0)
                {
                    fromId = listResult[x].id;
                }
                if (groupProcess.Count() < processNumber)
                {
                    if (loopCount == grp)
                    {
                        groupRange = new GroupRange();
                        groupRange.fromId = (int)fromId;
                        groupRange.toId = (int)listResult[x].id;
                        groupProcess.Add(groupRange);
                        lastFrom = fromId;
                        fromId = 0;
                        loopCount = 0;
                    }
                }
                toId = listResult[x].id;
            }

            if (fromId != 0)
            {
                groupProcess[groupProcess.Count() - 1].fromId = (int)lastFrom;
                groupProcess[groupProcess.Count() - 1].toId = (int)toId;
            }
            return groupProcess;

        }


        /// <summary>
        /// Kill Process
        /// </summary>
        private void killProcess(int processID)
        {
            int count = 0;
            while (true)
            {
                try
                {
                    Process localById = Process.GetProcessById(processID);
                    localById.Kill();
                }
                catch (Win32Exception win)
                {
                    Console.WriteLine(win.Message);
                }
                catch (NotSupportedException NotSupport)
                {
                    Console.WriteLine(NotSupport.Message);
                }
                catch (InvalidOperationException) // this is for Process.Kill
                {
                    break;
                }
                catch (ArgumentException) // this is for Process.GetProcessID
                {
                    break;
                }
                if (++count > 100)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Get Mail count deliver
        /// </summary>
        /// <param name="fromId"></param>
        /// <param name="toId"></param>
        /// <param name="process_Id"></param>
        /// <returns></returns>
        private long GetErsAtMailCountDeliver(int fromId, int toId, long? process_Id, DateTime? executeDateTime)
        {
            var ersMailToRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var ersMailToCriteria = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            ersMailToCriteria.idFrom = fromId;
            ersMailToCriteria.idTo = toId;
            ersMailToCriteria.process_id = process_Id;
            ersMailToCriteria.sent_flg = EnumSentFlg.NotSent;
            ersMailToCriteria.scheduled_date_less_or_null = executeDateTime;
            ersMailToCriteria.active = EnumActive.Active;
            return ersMailToRepo.GetRecordCount(ersMailToCriteria);
        }


        /// <summary>
        /// Find am_process_t
        /// </summary>
        /// <param name="dbCon"></param>
        /// <returns></returns>
        private IList<ErsProcess> findAmProcess(DateTime? executeDateTime)
        {
            var repo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();

            //D3 (Retrive a delivery management record of non-delivery))
            criteria.active = EnumActive.Active;
            criteria.status_in = new[] { EnumAmProcessStatus.NotSend, EnumAmProcessStatus.Sending };
            criteria.scheduled_date_to = executeDateTime;
            criteria.SetHasTargetMailto(executeDateTime);
            criteria.SetOrderByProcessID(Criteria.OrderBy.ORDER_BY_ASC);
            return repo.Find(criteria);
        }

        /// <summary>
        /// Update am_process_t
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dbCon"></param>
        /// <param name="status"></param>
        private void updateAmProcessToSending(int id)
        {

            var repoUpdate = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var oldProcess = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(id);
            var newProcess = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(id);

            newProcess.status = EnumAmProcessStatus.Sending;
            repoUpdate.Update(oldProcess, newProcess);
        }

        /// <summary>
        /// Update am_process_t
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dbCon"></param>
        /// <param name="status"></param>
        private void updateAmProcessToSent(int id)
        {
            var repoMailTo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var criteriaMailTo = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            criteriaMailTo.process_id = id;
            criteriaMailTo.sent_flg = EnumSentFlg.NotSent;
            criteriaMailTo.active = EnumActive.Active;
            if (repoMailTo.GetRecordCount(criteriaMailTo) > 0)
            {
                // Do not update if there are remaining mailto
                return;
            }

            var repoUpdate = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var oldProcess = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(id);
            var newProcess = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(id);

            newProcess.status = EnumAmProcessStatus.Sent;
            newProcess.sent_date = DateTime.Now;

            repoUpdate.Update(oldProcess, newProcess);
        }

        /// <summary>
        /// Find am_mailto_t
        /// </summary>
        /// <param name="process_id"></param>
        /// <param name="dbCon"></param>
        /// <returns></returns>
        private IList<ErsMailTo> findMailTo(int? process_id, DateTime? executeDateTime)
        {
            var repoMailTo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var criteriaMailTo = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();

            //D4) Select records from am_mailto_t which are related to am_process_t 
            criteriaMailTo.process_id = process_id;
            criteriaMailTo.sent_flg = EnumSentFlg.NotSent;
            criteriaMailTo.scheduled_date_less_or_null = executeDateTime;
            criteriaMailTo.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            criteriaMailTo.active = EnumActive.Active;
            return repoMailTo.Find(criteriaMailTo);
        }
    }
}