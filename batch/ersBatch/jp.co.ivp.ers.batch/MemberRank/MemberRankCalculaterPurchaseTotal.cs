﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.batch.MemberRank
{
    class MemberRankCalculaterPurchaseTotal
        : IMemberRankCalculater
    {
        public void Execute(DateTime recordDateFrom, DateTime recordDateTo, bool member_rank_centralization, int? site_id)
        {
            //付与下限を取得します。Gets lower limit for grant member rank
            var lowerLimit = ErsFactory.ersMemberFactory.GetRetrieveMemberRankGrantLowerLimitSpec().Retrieve(site_id);

            //下限以下で、メンバーランクを持つ会員のランクを剥奪
            // Do not remove the rank if the lower limit is 0;
            if (lowerLimit > 0)
            {
                var memberRankRepository = ErsFactory.ersMemberFactory.GetErsMemberRankRepository();
                var memberRankCriteria = ErsFactory.ersMemberFactory.GetErsMemberRankCriteria();
                memberRankCriteria.NonRankMemberPurchaseTotal(lowerLimit, recordDateFrom, recordDateTo, member_rank_centralization, site_id);
                memberRankCriteria.site_id = site_id;
                ErsFactory.ersMemberFactory.GetMemberRankDeleteSpec().Delete(memberRankCriteria);
            }

            //メンバーランクを取得
            var rankSetupRepository = ErsFactory.ersMemberFactory.GetErsMemberRankSetupRepository();
            var rankSetupCriteria = ErsFactory.ersMemberFactory.GetErsMemberRankSetupCriteria();
            rankSetupCriteria.active = EnumActive.Active;
            rankSetupCriteria.site_id = site_id;
            var listRankSetup = rankSetupRepository.Find(rankSetupCriteria);

            var stgy = ErsFactory.ersMemberFactory.GetMemberRankRegistSpec();
            foreach (var objRankSetup in listRankSetup)
            {
                //メンバーランクの付与
                var updateMemberRankCriteria = ErsFactory.ersMemberFactory.GetErsMemberRankCriteria();
                updateMemberRankCriteria.RankMemberPurchaseTotalUpdate(objRankSetup.value_from, objRankSetup.value_to, recordDateFrom, recordDateTo, member_rank_centralization, site_id);
                stgy.Update(updateMemberRankCriteria, objRankSetup.rank, site_id);

                var registMemberRankCriteria = ErsFactory.ersMemberFactory.GetErsMemberRankCriteria();
                registMemberRankCriteria.RankMemberPurchaseTotal(objRankSetup.value_from, objRankSetup.value_to, recordDateFrom, recordDateTo, member_rank_centralization, site_id);
                stgy.Regist(registMemberRankCriteria, objRankSetup.rank, site_id);
            }
        }
    }
}
