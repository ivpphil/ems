﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.batch.MemberRank
{
    public interface IMemberRankCalculater
    {
        void Execute(DateTime recordDateFrom, DateTime recordDateTo, bool member_rank_centralization, int? site_id);
    }
}
