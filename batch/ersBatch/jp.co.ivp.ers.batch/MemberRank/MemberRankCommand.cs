﻿using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.batch.MemberRank
{
    public class MemberRankCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            //基準日を取得
            var recordDate = new DateTime(executeDate.Value.Year, executeDate.Value.Month, executeDate.Value.Day);

            this.Execute(recordDate);
        }

        private void Execute(DateTime recordDate)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            if(setup.member_rank_centralization)
            {
                var objSetup = ErsFactory.ersUtilityFactory.GetErsSetupWithId(1);

                // Common configuration
                Execute(objSetup, recordDate, setup.member_rank_centralization, (int)EnumSiteId.COMMON_SITE_ID);
            }
            else
            {
                var siteRepository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
                var siteCriteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
                siteCriteria.mall_shop_kbn = EnumMallShopKbn.ERS;
                siteCriteria.SetActiveOnly();
                var listSite = siteRepository.Find(siteCriteria);

                foreach (var site in listSite)
                {
                    var objSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(site.id);

                    Execute(objSetup, recordDate, setup.member_rank_centralization, site.id);
                }
            }
        }

        private void Execute(ErsSetup objSetup, DateTime recordDate, bool member_rank_centralization, int? site_id)
        {
            IMemberRankCalculater calculater;
            switch (objSetup.member_rank_criterion)
            {
                case EnumMemberRankCriterion.PurchaseTimes:
                    calculater = new MemberRankCalculaterPurchaseTimes();
                    break;
                case EnumMemberRankCriterion.PuchaseTotal:
                    calculater = new MemberRankCalculaterPurchaseTotal();
                    break;
                default:
                    throw new Exception("subclass of IMemberRankCalculater for " + objSetup.member_rank_criterion.ToString() + " is not defined. ");
            }

            var recordDateFrom = recordDate.AddDays(-objSetup.member_rank_term.Value);

            calculater.Execute(recordDateFrom, recordDate, member_rank_centralization, site_id);
        }
    }
}
