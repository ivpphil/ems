﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.strategy;
using jp.co.ivp.ers.target;
using jp.co.ivp.ers.doc_bundle;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall;

namespace jp.co.ivp.ers.batch.BundleDocument
{
    public class BundleDocumentCommand
        : IErsBatchCommand
    {
        ErsOrderRepository orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
        ErsOrderRecordRepository orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
        ErsOrderMailRepository orderMailRepository = ErsFactory.ersOrderFactory.GetErsOrderMailRepository();
        ErsTargetRepository targetRepository = ErsFactory.ersTargetFactory.GetErsTargetRepository();
        ErsCampaignRepository campaignRepository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();
        ErsDocTargetRepository docTargetRepository = ErsFactory.ersDocBundleFactory.GetErsDocTargetRepository();
        ErsDocBundlingRepository docBundlingRepository = ErsFactory.ersDocBundleFactory.GetErsDocBundlingRepository();
        AddOrdinaryOrderStrategy addOrderStrategy = ErsFactory.ersOrderFactory.GetAddOrdinaryOrderStrategy();

        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            var listOrder = this.GetOrderList(executeDate);

            foreach (var objOrder in listOrder)
            {
                using (var tx = ErsDB_parent.BeginTransaction())
                {
                    this.BundleDocument(objOrder);

                    var newOrder = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(objOrder.GetPropertiesAsDictionary());
                    newOrder.doc_bundle_flg = EnumDocBundlingFlg.ON;
                    orderRepository.Update(objOrder, newOrder);

                    tx.Commit();
                }
            }
        }

        private IEnumerable<ErsOrder> GetOrderList(DateTime? executeDate)
        {
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.doc_bundle_flg = EnumDocBundlingFlg.OFF;
            criteria.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.ignoreMonitor();

            return orderRepository.Find(criteria);
        }

        private void BundleDocument(ErsOrder objOrder)
        {
            this.DeleteOldDocument(objOrder.d_no);

            var allOrderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(objOrder);
            var objMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objOrder.mcode, true);
            this.Bundle(allOrderRecords, objOrder, objMember);


            var sampleOrderRecord = allOrderRecords.FirstOrDefault((pair) => pair.Value.id != null);

            foreach (var orderRecord in allOrderRecords.Values)
            {
                if (orderRecord.id == null)
                {
                    if (sampleOrderRecord.Value != null && sampleOrderRecord.Value.id != null)
                    {
                        // オリジナルの明細と値をそろえる
                        orderRecord.deliv_method = sampleOrderRecord.Value.deliv_method;
                    }

                    orderRecordRepository.Insert(orderRecord, true);

                    // update item stock
                    ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetDecreaseStockStgy().Decrease(orderRecord.scode, orderRecord.amount.Value);



                    var newOrderMail = this.GetOrderMail(objOrder, orderRecord);
                    orderMailRepository.Insert(newOrderMail);

                    //set
                    ErsFactory.ersOrderFactory.GetRegistSetItemsStgy().UpRegist(orderRecord);

                }

            }

            this.UpdateMallStock(allOrderRecords);
        }

        /// <summary>
        /// モール在庫更新 [Update mall stock]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>パラメータリスト [The list of parameter]</returns>
        protected IList<UpdateStockParam> UpdateMallStock(Dictionary<string, ErsOrderRecord> allOrderRecords)
        {
            var listParam = new List<UpdateStockParam>();

            foreach (var record in allOrderRecords.Values)
            {
                if (record.doc_bundling_flg == EnumDocBundlingFlg.OFF)
                {
                    continue;
                }

                var ersMerchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(record.scode, null);
                if (ersMerchandise.h_mall_flg != EnumOnOff.On)
                {
                    continue;
                }

                if (record.set_flg != EnumSetFlg.IsNotSet)
                {
                    //子商品リスト取得
                    var DecreaseSetItemList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(record.scode);

                    foreach (var setProduct in DecreaseSetItemList)
                    {
                        var chiled_merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(setProduct.scode, null);

                        if (chiled_merchandise.h_mall_flg == EnumOnOff.Off)
                        {
                            continue;
                        }

                        UpdateStockParam param = default(UpdateStockParam);

                        param.productCode = setProduct.scode;
                        param.quantity = record.amount * setProduct.amount;
                        param.operation = EnumMallStockOperation.sub;

                        listParam.Add(param);
                    }
                }
                else
                {
                    UpdateStockParam param = default(UpdateStockParam);

                    param.productCode = record.scode;
                    param.quantity = record.amount;
                    param.operation = EnumMallStockOperation.sub;

                    listParam.Add(param);
                }
            }

            // モール在庫更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }

            return listParam;
        }

        private ErsOrderMail GetOrderMail(ErsOrder objOrder, ErsOrderRecord orderRecord)
        {
            var newOrderMail = ErsFactory.ersOrderFactory.GetErsOrderMail();
            newOrderMail.d_no = orderRecord.d_no;
            newOrderMail.ds_id = orderRecord.id;
            newOrderMail.purchase_mail = EnumSentFlg.Sent;
            return newOrderMail;
        }

        private void DeleteOldDocument(string d_no)
        {
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
            criteria.d_no = d_no;
            criteria.order_type = EnumOrderType.DocBundled;
            criteria.order_status = EnumOrderStatusType.NEW_ORDER;
            orderRecordRepository.Delete(criteria);
        }

        /// <summary>
        /// Bundles Documents to ErsOrder.
        /// </summary>
        /// <param name="ersOrder"></param>
        /// <param name="ersMember"></param>
        /// <returns>適用されたキャンペーンID</returns>
        public virtual IEnumerable<string> Bundle(IDictionary<string, ErsOrderRecord> allOrderRecords, ErsOrder ersOrder, ErsMember ersMember)
        {
            var orderRecords = allOrderRecords.Values;

            var resultList = new List<string>();

            //適用されるターゲットリスト
            var targetList = this.SearchErsTarget(ersMember, ersOrder.d_no, ersOrder.site_id);

            foreach (var target in targetList)
            {
                //ターゲットを持つキャンペーンリスト取得
                var campaignList = this.SearchErsCampaign(target.id, ersMember, orderRecords, ersOrder.site_id);

                //取得キャンペーン数ループ
                foreach (var campaign in campaignList)
                {
                    //キャンペーンコードを取得
                    resultList.Add(campaign.ccode);

                    //同梱物を明細に追加
                    var docBundlingList = this.GetBundleDocuments(ersMember, ersOrder.d_no, campaign.ccode, orderRecords, (int)ersOrder.site_id);

                    foreach (var doc in docBundlingList)
                    {
                        allOrderRecords.Add(doc);
                    }
                }
            }
            return resultList;
        }

        /// <summary>
        /// Gets instance of ErsTargetCriteria for document bundling.
        /// 現状から該当するターゲットリストを取得するクライテリアを作成
        /// </summary>
        /// <param name="ersMember"></param>
        /// <returns></returns>
        protected virtual IList<ErsTarget> SearchErsTarget(ErsMember objMember, string d_no, int? site_id)
        {
            var criteria = ErsFactory.ersTargetFactory.GetErsTargetCriteria();

            if (objMember == null)
            {
                criteria.recency_between = 0;
                criteria.frequency_between = 0;
                criteria.monetary_between = 0;
                criteria.site_id = site_id;
                return targetRepository.Find(criteria);
            }
            else
            {
                var memberRepository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
                var buildCriteriaStgy = ErsFactory.ersTargetFactory.GetBuildTargetMemberCriteriaStgy();

                criteria.recency_between_from_mcode = objMember.mcode;
                criteria.frequency_between_from_mcode = objMember.mcode;
                criteria.monetary_between_from_mcode = objMember.mcode;
                criteria.site_id = site_id;

                var list = targetRepository.Find(criteria);

                var listResult = new List<ErsTarget>();

                //件数を設定
                foreach (ErsTarget target in list)
                {
                    var listTargetItem = ErsFactory.ersTargetFactory.GetObtainTargetItemListStgy().Obtain(target.id);

                    var memberCriteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
                    memberCriteria.deleted = EnumDeleted.NotDeleted;
                    memberCriteria.active = EnumActive.Active;
                    memberCriteria.mcode = objMember.mcode;
                    buildCriteriaStgy.Build(target, memberCriteria, DateTime.Now, listTargetItem, d_no);

                    var d_count = memberRepository.GetRecordCount(memberCriteria);

                    if (d_count > 0)
                    {
                        listResult.Add(target);
                    }
                }

                return listResult;
            }
        }

        /// <summary>
        /// Search ErsCampaign
        /// ターゲットを持つキャンペーン取得
        /// </summary>
        /// <param name="ersMember"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        protected virtual IList<ErsCampaign> SearchErsCampaign(int? target_id, ErsMember objMember, IEnumerable<ErsOrderRecord> orderRecords, int? site_id)
        {
            var criteria = ErsFactory.ersDocBundleFactory.GetErsCampaignCriteria();

            criteria.target_id = target_id;
            criteria.SetTargetCondition(orderRecords);
            criteria.SetActiveOnly();
            criteria.site_id = site_id;

            return campaignRepository.Find(criteria);
        }

        /// <summary>
        /// Search ErsDocBundling
        /// キャンペーンの同梱オブジェクトリストを取得
        /// </summary>
        /// <param name="campaign"></param>
        /// <returns></returns>
        protected virtual IEnumerable<KeyValuePair<string, ErsOrderRecord>> GetBundleDocuments(ErsMember objMember, string d_no, string ccode, IEnumerable<ErsOrderRecord> baseOrderRecords, int site_id)
        {
            var orderRecords = baseOrderRecords.ToList();
            var criteria = ErsFactory.ersDocBundleFactory.GetErsDocBundlingCriteria();
            criteria.ccode = ccode;
            if (objMember != null)
            {
                criteria.SetDupplicate(objMember.mcode);
            }

            var docBundlingList = docBundlingRepository.Find(criteria);
            //docBundlingList に追加
            foreach (var docBundling in docBundlingList)
            {
                if (orderRecords.FirstOrDefault((orderRecord) => orderRecord.scode == docBundling.scode) != null)
                {
                    continue;
                }

                var merchandise = ErsFactory.ersBasketFactory.GetErsBaskRecordWithScode(null, docBundling.scode, null);
                if (merchandise == null)
                {
                    continue;
                }

                var tp_merchandise = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(merchandise.gcode);
                if (!tp_merchandise.site_id.Contains(site_id))
                {
                    continue;
                }
                merchandise.price = 0;
                merchandise.order_type = EnumOrderType.DocBundled;
                merchandise.doc_bundling_flg = EnumDocBundlingFlg.ON;

                //明細に追加
                yield return addOrderStrategy.GetOrder(d_no, merchandise, docBundling.amount.Value, null, null);
            }
        }
    }
}
