﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch.CreditAccountLaunderingDownload
{
    public class CreditAccountLaunderingDownloadCommand
          : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> argDictionary, DateTime? lastDate, string batchLocation)
        {
            CreditAccountLaunderingDownload exec = new CreditAccountLaunderingDownload();

            var currentDate = argDictionary.ContainsKey("date") ? Convert.ToDateTime(argDictionary["date"]) : DateTime.Now;

            exec.Execute(currentDate);
        }
    }
}
