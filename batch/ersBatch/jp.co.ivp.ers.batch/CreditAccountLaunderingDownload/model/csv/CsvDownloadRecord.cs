﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.Payment.account_laundering;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch.CreditAccountLaunderingDownload.model.csv
{
    public class CsvDownloadRecord : ErsBindableModel, IAccountLaunderingResult
    {
        [CsvField]
        [ErsSchemaValidation("member_card_t.card_mcode")]
        public virtual string card_mcode { get; set; }

        [CsvField]
        [ErsSchemaValidation("member_card_t.card_sequence")]
        public virtual string card_sequence { get; set; }

        [CsvField]
        public virtual string old_card_no { get; private set; }

        [CsvField]
        public virtual string old_expiration { get; private set; }

        [CsvField]
        public virtual string old_card_name { get; private set; }

        [CsvField]
        public virtual string old_card_code { get; private set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 3)]
        public virtual EnumAccountLaunderingResult? result { get; set; }

        [CsvField]
        public virtual string new_card_no { get; private set; }

        [CsvField]
        public virtual string new_expiration { get; private set; }

        [CsvField]
        public virtual string new_card_code { get; private set; }

        [CsvField]
        public virtual string execute_date { get; private set; }

        [CsvField]
        public virtual string remark { get; private set; }

        [CsvField]
        public virtual string process_number { get; private set; }

        /// <summary>
        /// 結果を保持しているか（退会済み会員のデータは結果がnullで戻ってくるため）
        /// </summary>
        public bool hasResult
        {
            get
            {
                return (!string.IsNullOrEmpty(this.card_mcode) && this.result != null);
            }
        }
    }
}
