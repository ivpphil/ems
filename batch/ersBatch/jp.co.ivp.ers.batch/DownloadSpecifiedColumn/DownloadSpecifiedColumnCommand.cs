﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.Collections.Concurrent;
using System.IO;
using jp.co.ivp.ers.batch.DownloadSpecifiedColumn.Model;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.batch.DownloadSpecifiedColumn
{
    public class DownloadSpecifiedColumnCommand
        : IErsBatchCommand
    {
        protected virtual string[] GetDelimiters()
        {
            return new[] { "," };
        }

        Setup setup;
        private const string resultPrefixExt = "-result-";

        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            setup = ErsFactory.ersUtilityFactory.getSetup();

            //Execute Process
            this.Execute();
        }

        protected void Execute()
        {
            //Automatically Created download source folder
            ErsDirectory.CreateDirectories(setup.downloadFilePath);
            ErsDirectory.CreateDirectories(setup.downloadFileResultPath);

            var uploadedFileList = Directory.EnumerateFiles(setup.downloadFilePath, "*.csv");

            //-- TODO: Temporary removed ==>>> Parallel Lambda Expression
            foreach (string filePath in uploadedFileList)
            {
                this.BindParallelOpenedFile(filePath);
            }
        }

        private void BindParallelOpenedFile(string filePath)
        {
            var exceptionsList = new ConcurrentQueue<Exception>();
            var resultPath = string.Empty;

            try
            {
                // ファイルパスの決定
                string pathFileName = Path.Combine(setup.downloadFileResultPath, Path.GetFileNameWithoutExtension(filePath) + resultPrefixExt);
                resultPath = ErsFile.WriteAllWithRandomName(pathFileName, "csv", string.Empty, ErsEncoding.ShiftJIS);

                //Set Locked Current File
                using (var fileStream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    this.BindAndDownloadCsvSchema(filePath, resultPath, fileStream, exceptionsList);
                }
            }
            catch (IOException ioEx)
            {
                //Identify if file is locked
                if (ErsFile.IsFileLocked(ioEx))
                    return;

                var exMessage = ioEx.Message;

                exMessage = exMessage.Replace(filePath, Path.GetFileName(filePath));
                exceptionsList.Enqueue(new Exception(exMessage));
            }
            catch (Exception ex)
            {
                var exMessage = ex.Message;

                exMessage = exMessage.Replace(filePath, Path.GetFileName(filePath));
                exceptionsList.Enqueue(new Exception(exMessage));
            }
            finally
            {
                //Delete Finished file
                this.DeleteFile(filePath, exceptionsList);

                this.SaveErrorLogs(resultPath, exceptionsList);
                this.SendAlertMail(Path.GetFileName(filePath), Path.GetFileName(resultPath), exceptionsList);
            }
        }

        //Bind Current File
        protected virtual void BindAndDownloadCsvSchema(string filePath, string resultFileName, Stream stream, ConcurrentQueue<Exception> exceptionsList)
        {
            try
            {
                var csvFileLoader = ErsFactory.ersUtilityFactory.GetErsCsvSchemaContainer<DownloadContainerModel>();
                csvFileLoader.LoadPostedFile(filePath, stream);

                var containerModel = this.GetDownloadContainerModel(csvFileLoader);

                this.ExecuteCopyData(containerModel, resultFileName);

            }
            catch (Exception ex)
            {
                string exMessage = string.Empty;

                if (ex.InnerException != null)
                    exMessage = ex.InnerException.Message;

                if (!exMessage.HasValue())
                    exMessage = ex.Message;

                exceptionsList.Enqueue(new Exception(exMessage));
            }

        }

        //Get ContainerModel
        public DownloadContainerModel GetDownloadContainerModel(ErsCsvSchemaContainer<DownloadContainerModel> csvFileLoader)
        {
            if (csvFileLoader.updatableSchemaList != null && csvFileLoader.updatableSchemaList.Count > 0)
            {
                var schemaDictionary = csvFileLoader.GetValidSchema(0);
                return (DownloadContainerModel)schemaDictionary["containerModel"];
            }

            var containerModel = new DownloadContainerModel();
            containerModel.tableName = csvFileLoader.tableName;
            containerModel.updatableSchemaList = csvFileLoader.updatableSchemaList;

            return containerModel;
        }

        public string CopySelectStatement(string tableName, IEnumerable<string> columnList, Encoding encoding)
        {
            var selectedColumn = "*";

            if (columnList != null && columnList.Count() > 0)
            {
                selectedColumn = String.Join(",", columnList);
            }

            return "COPY (SELECT " + selectedColumn + " FROM " + tableName + ") TO STDOUT " + this.WithParameters(encoding);
        }

        public string WithParameters(Encoding encoding)
        {
            return " WITH ENCODING '" + encoding.WebName + "' DELIMITER AS ',' NULL AS '' CSV HEADER ";
        }

        //Execute Copy Command
        protected virtual void ExecuteCopyData(DownloadContainerModel containerModel, string resultFileName, Encoding encoding = null)
        {
            if (!this.HasRecord(containerModel.tableName))
            {
                throw new ErsException("non_object_record");
            }

            if (encoding == null)
                encoding = ErsEncoding.ShiftJIS;

            string copyOutQuery = this.CopySelectStatement(containerModel.tableName, containerModel.updatableSchemaList, encoding);

            //New Instance of Csv Writer
            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();

            using (var writer = csvCreater.GetWriter(resultFileName, true, encoding))
            {
                //Get Copy data of database using Stream
                Stream copyOutputStream = ErsDatabaseCopyOut.GetCopyStream(copyOutQuery);

                //Get and Read Row List
                var rowList = ErsDatabaseCopyOut.GetRowListStream(copyOutputStream, this.GetDelimiters(), encoding);
                foreach (var row in rowList)
                {
                    var nativeRow = row.Select((rawvalue) => this.ConvertRawValueToNativeValue(rawvalue)).ToArray();

                    if (rowList.IndexOf(row) == 0)
                    {
                        //Writer Header in First line
                        csvCreater.WriteCsvHeader(nativeRow, writer);
                        continue;
                    }

                    //Write Body as Dictionary
                    Dictionary<string, object> dictionaryBody = Enumerable.Range(0, nativeRow.Length).ToDictionary(key => key.ToString(), value => (object)nativeRow[value]);
                    csvCreater.WriteBody(dictionaryBody, writer);
                }
            }
        }

        //Save Current File
        public void SaveErrorLogs(string resultFileName, ConcurrentQueue<Exception> exceptionsList)
        {
            if (this.HasError(exceptionsList))
            {
                var errorMessageList = exceptionsList.Select((err) => err.Message.ToString());
                var errorLogs = String.Join(Environment.NewLine, errorMessageList);

                ErsFile.WriteAll(errorLogs, resultFileName);
            }
        }


        protected bool HasRecord(string tableName)
        {
            var repository = ErsFactory.ersUpdateSpecifiedColumnFactory.GetErsUpdateSpecifiedColumnRepository(tableName);

            return (repository.GetRecordCount(null) > 0);
        }

        protected string ConvertRawValueToNativeValue(string value)
        {
            if (!value.HasValue() || value == "{}")
                return string.Empty;

            if (value.StartsWith("{") && value.EndsWith("}"))
            {
                string returnValue = value;
                returnValue = returnValue.Substring(1);
                returnValue = returnValue.Substring(0, returnValue.LastIndexOf("}"));
                return returnValue;
            }

            return value;
        }

        //Delete Current File
        private void DeleteFile(string filePath, ConcurrentQueue<Exception> exceptionsList)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                exceptionsList.Enqueue(new Exception(ex.Message));
            }
        }

        //Send mail after current file has done
        public void SendAlertMail(string updatedFileName, string resultFileName, ConcurrentQueue<Exception> exceptionsList)
        {
            if (!this.HasError(exceptionsList))
                resultFileName = string.Empty;

            var mailModel = new DownloadSpecifiedColumnMail(updatedFileName, resultFileName);
            mailModel.SendMail(mailModel.MailBody());
        }

        protected virtual bool HasError(ConcurrentQueue<Exception> exceptionsList)
        {
            return (exceptionsList != null && exceptionsList.Count > 0);
        }
    }
}
