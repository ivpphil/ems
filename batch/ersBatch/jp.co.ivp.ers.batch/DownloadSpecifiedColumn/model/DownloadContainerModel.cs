﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch.DownloadSpecifiedColumn.Model
{
    public class DownloadContainerModel
        : ErsBindableModel
    {
        public string messageTableKey { get { return "non_downloadable_table"; } }

        public virtual List<string> updatableSchemaList { get; set; }

        public virtual string parameterKey { get; set; }

        public string tableName { get; set; }
    }
}
