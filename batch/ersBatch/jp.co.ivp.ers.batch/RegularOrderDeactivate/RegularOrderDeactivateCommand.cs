﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.batch.RegularOrderDeactivate
{
    public class RegularOrderDeactivateCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            //実行日の算出
            var TargetExecuteDate = (options.ContainsKey("date")) ? Convert.ToDateTime(options["date"]) : executeDate ?? DateTime.Now;

            var listTarget = this.GetTargetList(TargetExecuteDate);

            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            foreach (var newRegularDetail in listTarget)
            {
                var oldRegularDetail = ErsFactory.ersOrderFactory.GetErsRegularOrderRecord(null);
                oldRegularDetail.OverwriteWithParameter(newRegularDetail.GetPropertiesAsDictionary());

                newRegularDetail.delete_date = newRegularDetail.next_date;

                repository.Update(oldRegularDetail, newRegularDetail);
            }
        }

        private IEnumerable<ErsRegularOrderRecord> GetTargetList(DateTime TargetExecuteDate)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
            criteria.SetActiveDetailsOnly();
            criteria.HasUpdateErrorCard();

            return repository.Find(criteria);
        }
    }
}
