﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace jp.co.ivp.ers.batch.DeleteTables
{
    class DeleteTablesSettingsModel
        : ErsBindableModel
    {
        /// <summary>
        /// テーブル設定
        /// </summary>
        [XmlField]
        [BindTable("connections")]
        public List<ConnectionSettings> connections { get; set; }
    }

    public class ConnectionSettings
        : ErsBindableModel
    {
        /// <summary>
        /// コネクションストリング
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.ConnectionString)]
        public virtual string connection_string { get; set; }

        /// <summary>
        /// テーブル設定
        /// </summary>
        [XmlField()]
        [BindTable("tables")]
        public List<TableSettings> tables { get; set; }
    }

    public class TableSettings
        : ErsBindableModel
    {
        /// <summary>
        /// テーブル名
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string table_name { get; set; }

        /// <summary>
        /// カラム名
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string column_name { get; set; }

        /// <summary>
        /// 間隔（Postgres "INTERVAL"）
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string interval { get; set; }
    }
}
