﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.mvc.model;
using System.Threading.Tasks;
using jp.co.ivp.ers.db;
using System.Collections.Concurrent;

namespace jp.co.ivp.ers.batch.DeleteTables
{
    public class DeleteTablesCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            this.Execute();
        }

        /// <summary>
        /// Execute
        /// </summary>
        public void Execute()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            DeleteTablesSettingsModel modelSettings = new DeleteTablesSettingsModel();

            // Get settings
            ErsXmlModelBinder.Bind(string.Format("{0}\\setup\\batch\\delete_tables.config", setup.root_path), modelSettings);

            var listErrorLog = new ConcurrentQueue<string>();

            // Execute delete
            Parallel.ForEach(modelSettings.connections, connection =>
            {
                try
                {
                    foreach (var table in connection.tables)
                    {
                        this.ExecuteDelete(connection.connection_string, table);
                    }
                }
                catch (Exception e)
                {
                    listErrorLog.Enqueue(e.ToString());
                }
            });

            if (listErrorLog.Count > 0)
            {
                throw new Exception(String.Join("\r\n", listErrorLog));
            }
        }

        /// <summary>
        /// ExecuteDelete
        /// </summary>
        /// <param name="table"></param>
        protected void ExecuteDelete(string connection_string, TableSettings table)
        {
            ErsDB_parent objDB = string.IsNullOrEmpty(connection_string) ? new ErsDB_parent(table.table_name) : new ErsDB_parent(table.table_name, ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(connection_string));

            objDB.gDelete(string.Format("WHERE {0} <= (now() - INTERVAL '{1}')", table.column_name, table.interval));
        }
    }
}
