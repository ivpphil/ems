﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.model;
using System.IO;

namespace jp.co.ivp.ers.batch.DeleteFiles
{
    public class DeleteFilesCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            this.Execute();
        }

        private void Execute()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var modelSettings = new DeleteFilesSettingsModel();

            // Get settings
            ErsXmlModelBinder.Bind(string.Format("{0}\\setup\\batch\\delete_files.config", setup.root_path), modelSettings);
            
            foreach (var file in modelSettings.files)
            {
                string[] files = System.IO.Directory.GetFiles(file.file_path, "*", System.IO.SearchOption.AllDirectories);

                /// 指定された日数以上前のファイルを削除
                foreach (string fdata in files)
                {
                    if (this.IsDeletable(fdata, file.period, file.extensions))
                    {
                        FileInfo cFileInfo = new FileInfo(fdata);
                        cFileInfo.Delete();
                    }
                }
            }
        }

        /// <summary>
        /// 削除対象ファイルかどうか
        /// </summary>
        /// <returns></returns>
        protected bool IsDeletable(string strPath, int intPeriod, string[] extensions)
        {
            // 拡張子
            if (extensions != null)
            {
                var extension = Path.GetExtension(strPath);
                if (!extension.HasValue())
                {
                    return false;
                }

                if (!extensions.Contains(extension.Substring(1)))
                {
                    return false;
                }
            }

            // 期間
            DateTime tdNow = DateTime.Today;
            DateTime dtCreate = System.IO.File.GetCreationTime(strPath);

            // 日単位
            tdNow = tdNow.AddDays(-intPeriod);

            return (dtCreate <= tdNow);
        }
    }
}
