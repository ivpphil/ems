﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.batch.DeleteFiles
{
    public class DeleteFilesSettingsModel
        : ErsBindableModel
    {
        /// <summary>
        /// ログ
        /// </summary>
        [XmlField]
        [BindTable("files")]
        public List<DeleteFilesSettings> files { get; set; }
    }

    public class DeleteFilesSettings
        : ErsBindableModel
    {
        /// <summary>
        /// ログパス
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string file_path { get; set; }

        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int period { get; set; }

        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All, isArray = true)]
        public string[] extensions { get; set; }
    }
}
