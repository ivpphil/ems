﻿using jp.co.ivp.ers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc;
using ersBatch.jp.co.ivp.ers.batch.EmployeeRegisterSendEmail;

namespace jp.co.ivp.ers.batch.EmployeeRegisterSendEmail
{
    public class EmployeeRegisterSendEmail
    {
        public void Execute()
        {
            var model = new EmployeeRegistSendEmailModel();
            var list = ErsFactory.ersEmployeeFactory.Getm_flg();

            var send_regist = ErsFactory.ersMailFactory.getErsSendmailEmp();

            var old = ErsFactory.ersEmployeeFactory.GetErsEmployee();
            var newlist = ErsFactory.ersEmployeeFactory.GetErsEmployee();

            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();

            if (list.Count > 0)
            {
                foreach(var l in list)
                {
                    old.OverwriteWithModel(l);
                    l.m_flg = 1;
                    l.utime = DateTime.Now;
                    newlist.OverwriteWithModel(l);
                    repo.Update(old, newlist);


                    model.email = l.email;
                    model.fname = l.fname;
                    model.lname = l.lname;
                    model.password=l.password;
                    send_regist.Send(model, model.email, EnumMformat.PC);
                }
            }


        }
    }
}
