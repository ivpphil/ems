﻿using jp.co.ivp.ers.batch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.batch.EmployeeRegisterSendEmail
{
    public class EmployeeRegisterSendEmailCommand
        :IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> argDictinary, DateTime? lastDate, string batchLocation)
        {
            new EmployeeRegisterSendEmail().Execute();
        }
    }
}
