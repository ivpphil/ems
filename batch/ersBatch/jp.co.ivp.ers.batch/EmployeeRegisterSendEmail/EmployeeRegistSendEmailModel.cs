﻿using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ersBatch.jp.co.ivp.ers.batch.EmployeeRegisterSendEmail
{
    class EmployeeRegistSendEmailModel
        :ErsModelBase
    {
        
        public string email { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string password { get; set; }
    }
}
