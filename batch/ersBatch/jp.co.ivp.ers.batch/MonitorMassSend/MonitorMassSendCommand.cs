﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.batch.MonitorMassSend
{
    public class MonitorMassSendCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            this.CheckNotSentMail(executeDate);
            this.CheckMassSendMonitorMail();
        }

        private void CheckNotSentMail(DateTime? executeDate)
        {
            var setUp = ErsFactory.ersBatchFactory.getSetup();

            var repo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();

            var executeDateTime = executeDate.Value.AddMinutes(-setUp.MonitorMassSendPastMinutes);

            //D3 (Retrive a delivery management record of non-delivery))
            criteria.active = EnumActive.Active;
            criteria.scheduled_date_to = executeDateTime;
            criteria.mailTo_scheduled_date_less_or_null = executeDateTime;
            criteria.mailTo_sent_flg = EnumSentFlg.NotSent;
            criteria.mailTo_active = EnumActive.Active;
            if (repo.GetRecordCountMailSend(criteria) > 0)
            {
                throw new Exception(string.Format("送信予定時間を{0}分経過したメール送信先を検知しました。", setUp.MonitorMassSendPastMinutes));
            }
        }

        private void CheckMassSendMonitorMail()
        {
            var setUp = ErsFactory.ersUtilityFactory.getSetup();

            var ersMailType = setUp.monitorMassSendMailServerReceptionType;
            using (var imailer = ErsFactory.ersUtilityFactory.GetErsImailer(ersMailType))
            {
                //接続
                string server = setUp.monitorMassSendMailServer;
                string mailServerAccount = setUp.monitorMassSendMailServerAccount;
                string mailServerPass = setUp.monitorMassSendMailServerPass;
                int mailServerPort = setUp.monitorMassSendMailServerPort;
                Boolean mailServerSslConnection = setUp.monitorMassSendMailServerSslConnection;

                if (!imailer.OpenConnection(server, mailServerAccount, mailServerPass, mailServerPort, mailServerSslConnection))
                {
                    throw new Exception("コネクションエラー");
                }

                var mailList = imailer.GetMailList();

                //メールセット時にエラーがれえば
                if (!string.IsNullOrEmpty(imailer.GetMailSetErrLog()))
                {
                    throw new Exception(imailer.GetMailSetErrLog());
                }

                bool isMailExist = false;
                foreach (var mail in mailList)
                {
                    if (!mail.ContainsKey("subject") || mail["subject"] == null)
                    {
                        continue;
                    }

                    var mail_title = Convert.ToString(mail["subject"]);

                    DateTime executeDate;
                    if (!DateTime.TryParse(mail_title, out executeDate))
                    {
                        continue;
                    }

                    //データオブジェクト取得
                    if (executeDate > DateTime.Now.AddMinutes(-15))
                    {
                        isMailExist = true;

                        //メール移動(削除)
                        imailer.MailMoveDelete(setUp.monitorMassSendReadMailDirName, new[] { Convert.ToString(mail["uid"]) });
                    }

                }

                if (!isMailExist)
                {
                    throw new Exception("＠メール監視用メールが送信されていませんでした。");
                }
            }
        }
    }
}
