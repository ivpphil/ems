﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.incomingmail;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.site;

namespace jp.co.ivp.ers.batch.ContactMailReception
{
    public class ContactMailReception
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (setup.Multiple_sites)
            {
                // サイトリスト取得 [Get list of site]
                var repository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
                var criteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();

                criteria.mall_shop_kbn = EnumMallShopKbn.ERS;

                var list = repository.Find(criteria);

                foreach (var site in list)
                {
                    this.Execute(site);
                }
            }
            else
            {
                this.Execute(null);
            }
        }

        private void Execute(ErsSite site)
        {
            var setUp = ErsFactory.ersBatchFactory.getSetup();

            //メール取得用オブジェト生成
            var ersMailType = site == null ? setUp.contactMailReceptionMailServerReceptionType : setUp.multiple_contactMailReceptionMailServerReceptionType(site.id);
            using (var imailer = ErsFactory.ersUtilityFactory.GetErsImailer(ersMailType))
            {
                List<string> errArr = new List<string>();
                List<string> deleteMailArr = new List<string>();

                //接続
                string server = site == null ? setUp.contactMailReceptionMailServer : setUp.multiple_contactMailReceptionMailServer(site.id);
                string mailServerAccount = site == null ? setUp.contactMailReceptionMailServerAccount : setUp.multiple_contactMailReceptionMailServerAccount(site.id);
                string mailServerPass = site == null ? setUp.contactMailReceptionMailServerPass : setUp.multiple_contactMailReceptionMailServerPass(site.id);
                int mailServerPort = site == null ? setUp.contactMailReceptionMailServerPort : setUp.multiple_contactMailReceptionMailServerPort(site.id);
                Boolean mailServerSslConnection = site == null ? setUp.contactMailReceptionMailServerSslConnection : setUp.multiple_contactMailReceptionMailServerSslConnection(site.id);

                if (!imailer.OpenConnection(server, mailServerAccount, mailServerPass, mailServerPort, mailServerSslConnection))
                {
                    throw new Exception("コネクションエラー");
                }

                var mailList = imailer.GetMailList();

                //メールセット時にエラーがれえば
                if (!string.IsNullOrEmpty(imailer.GetMailSetErrLog()))
                    errArr.Add(imailer.GetMailSetErrLog());

                var incomingMailRepository = ErsFactory.ersCtsInquiryFactory.GetErsCtsIncomingMailRepository();
                foreach (var mail in mailList)
                {
                    try
                    {
                        using (var transaction = ErsDB_parent.BeginTransaction())
                        {
                            //データオブジェクト取得
                            var mailReception = this.GetErsCtsIncomingMailWithReceptionData(mail);

                            //DBデータとの重複チェック
                            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsIncomingMailCriteria();
                            criteria.message_id = mailReception.message_id;
                            criteria.u_id = mailReception.u_id;

                            //重複していれば取り込みはスキップ
                            if (incomingMailRepository.GetRecordCount(criteria) == 0)
                            {
                                mailReception.active = EnumActive.Active;
                                mailReception.utime = DateTime.Now;
                                mailReception.site_id = site == null ? (int)EnumSiteId.COMMON_SITE_ID : site.id;

                                incomingMailRepository.Insert(mailReception, true);

                                //サーバー側移動(削除)対象メールID
                                deleteMailArr.Add(mailReception.u_id);
                            }

                            //移動(削除)対象とする
                            deleteMailArr.Add(mailReception.u_id);

                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        errArr.Add(DateTime.Now + ", " + ex.ToString());
                    }
                }

                try
                {
                    var dirName = site == null ? setUp.contactMailReceptionReadMailDirName : setUp.multiple_contactMailReceptionReadMailDirName(site.id);
                    //メール移動(削除)
                    imailer.MailMoveDelete(dirName, deleteMailArr);
                }
                catch (Exception ex)
                {
                    errArr.Add(ex.ToString());
                }

                //1件でもエラーがあれば
                if (errArr.Count != 0)
                {
                    throw new Exception(string.Join(",", errArr));
                }
            }
        }


        public virtual ErsCtsIncomingMail GetErsCtsIncomingMailWithReceptionData(IDictionary<string, object> dic)
        {
            var setUp = ErsFactory.ersBatchFactory.getSetup();

            var ersCtsMailReception = ErsFactory.ersCtsInquiryFactory.GetErsCtsIncomingMail();

            //メールID
            if (dic.ContainsKey("message-id") && dic["message-id"] != null)
                ersCtsMailReception.message_id = Convert.ToString(dic["message-id"]);

            if (dic.ContainsKey("uid") && dic["uid"] != null)
                ersCtsMailReception.u_id = Convert.ToString(dic["uid"]);

            //FROM
            if (dic.ContainsKey("from") && dic["from"] != null)
            {
                ersCtsMailReception.mail_from = this.TrimAddressName(Convert.ToString(dic["from"]));
                ersCtsMailReception.mail_from_name = this.TrimAddressEmail(Convert.ToString(dic["from"]));
            }

            //TO
            if (dic.ContainsKey("to") && dic["to"] != null)
                ersCtsMailReception.mail_to = this.GetMailAddressArr(Convert.ToString(dic["to"]));

            //CC
            if (dic.ContainsKey("cc") && dic["cc"] != null)
                ersCtsMailReception.mail_cc = this.GetMailAddressArr(Convert.ToString(dic["cc"]));

            //BCC
            if (dic.ContainsKey("bcc") && dic["bcc"] != null)
                ersCtsMailReception.mail_bcc = this.GetMailAddressArr(Convert.ToString(dic["bcc"]));

            //メール内容
            if (dic.ContainsKey("subject") && (Convert.ToString(dic["subject"])).HasValue())
            {
                ersCtsMailReception.mail_title = Convert.ToString(dic["subject"]);
            }
            else
            {
                ersCtsMailReception.mail_title = setUp.defaultCaseInquiryMailTitle;
            }
                

            //メール本文
            if (dic.ContainsKey("body") && dic["body"] != null)
                ersCtsMailReception.mail_body = Convert.ToString(dic["body"]);

            //mcode
            //メアドに紐づく会員コード取得
            ersCtsMailReception.mcode = ErsFactory.ersMemberFactory.GetMemberCodeByEmail(ersCtsMailReception.mail_from);

            //メール受信(サーバー時間)
            if (dic.ContainsKey("date") && dic["date"] != null)
                ersCtsMailReception.received_date = this.GetCorrectDateString(Convert.ToString(dic["date"]));

            ersCtsMailReception.active = EnumActive.NonActive;

            return ersCtsMailReception;
        }

        /// <summary>
        /// to, cc, bcc　カンマ区切りのアドレスを配列で返す
        /// </summary>
        /// <param name="Mailboxes"></param>
        /// <returns></returns>
        public string[] GetMailAddressArr(String address)
        {
            string[] ret;

            if (address.IndexOf("@") < 0)
            {
                ret = new string[1];
                ret[0] = null;
            }
            else if (address.IndexOf(",") > 0)
            {
                //`@以下で,があれば複数のメールアドレスと判断
                char[] delimiterChars = { ',' };
                ret = address.Replace(" ", "").Split(delimiterChars);

                for (int cnt = 0; cnt < ret.Length; cnt++)
                {
                    ret[cnt] = this.TrimAddressName(ret[cnt]);
                }
            }
            else
            {
                ret = new string[1];
                ret[0] = this.TrimAddressName(address);
            }

            return ret;
        }

        /// <summary>
        /// メアドに名前設定がしてある場合に名前をトリムしてメアドのみを返却
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string TrimAddressName(string email)
        {
            //<>の中の文字列がメアドとなる
            if (email.IndexOf("<") > 0 && email.IndexOf(">") > 0)
            {
                email = email.Substring((email.IndexOf("<") + 1), (email.IndexOf(">") - email.IndexOf("<") - 1));
            }
            return email;
        }

        /// <summary>
        /// メアドに名前設定がしてある場合にメアドをトリムして名前のみ返却
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string TrimAddressEmail(string email)
        {
            //<>の中の文字列が名前となる
            if (email.IndexOf("<") > 0 && email.IndexOf(">") > 0)
            {
                email = email.Substring(0, (email.LastIndexOf("<") - 1));
            }
            else
            {
                email = null;
            }
            return email;
        }

        /// <summary>
        /// 文字列形式の時刻をdate型で返却
        /// </summary>
        /// <returns></returns>
        public DateTime? GetCorrectDateString(string tpDate)
        {
            if (tpDate != null)
            {
                string[] expectedFormats = {
                    "ddd, d MMM yyyy HH':'mm':'ss zzz",
                    "ddd, d MMM yyyy H':'m':'s zzz", "d MMM yyyy HH':'mm':'ss zzz",
                    "ddd, d MMM yyyy HH':'mm':'ss", "ddd, d MMM yyyy H':'m':'s",
                    "d MMM yyyy HH':'mm':'ss","ddd, d MMM yyyy HH':'mm':'ss zzz"
                };

                // (JST) などを取り払う
                int i = tpDate.LastIndexOf("(");
                if (i > -1)
                {
                    tpDate = tpDate.Substring(0, i).TrimEnd().Trim();
                }

                // +0900　などを取り払う
                i = tpDate.LastIndexOf("+");
                if (i > -1)
                {
                    tpDate = tpDate.Substring(0, i).TrimEnd().Trim();
                }

                //スペースが２つ続きのものがある場合がある
                tpDate = tpDate.Replace("  ", " ");

                DateTime dt2 = System.DateTime.ParseExact(tpDate,
                            expectedFormats,
                            System.Globalization.DateTimeFormatInfo.InvariantInfo,
                            System.Globalization.DateTimeStyles.None);
                return dt2;
            }

            return null;

        }
    }
}
