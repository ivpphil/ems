﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.batch.SendShippedMail.model;

namespace jp.co.ivp.ers.batch.SendShippedMail
{
    public class SendShippedMailCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            var listError = new List<string>();

            // 対象伝票を取得
            var listOrder = this.GetListOrder();

            var repository = ErsFactory.ersOrderFactory.GetErsOrderMailRepository();

            foreach (var newOrder in listOrder)
            {
                var newOrderRecordList = this.GetOrderRecords(newOrder);

                // 配送番号ごとに分類
                var dicShipped = new Dictionary<string, List<ErsOrderRecord>>();
                foreach (var orderRecord in newOrderRecordList)
                {
                    var sendno = orderRecord.sendno ?? string.Empty;

                    if (!dicShipped.ContainsKey(sendno))
                    {
                        dicShipped[sendno] = new List<ErsOrderRecord>();
                    }

                    dicShipped[sendno].Add(orderRecord);
                }

                //メール送信処理
                foreach (var sendno in dicShipped.Keys)
                {
                    try
                    {
                        using (var tx = ErsDB_parent.BeginTransaction())
                        {
                            var recordList = dicShipped[sendno];
                            var shipdate = recordList.First().shipdate;

                            foreach (var record in recordList)
                            {
                                // メールフラグを先に更新
                                var newOrderMail = this.GetOrderMail(record);
                                var oldOrderMail = ErsFactory.ersOrderFactory.GetErsOrderMail();
                                oldOrderMail.OverwriteWithParameter(newOrderMail.GetPropertiesAsDictionary());

                                newOrderMail.shipped_mail = EnumSentFlg.Sent;

                                repository.Update(oldOrderMail, newOrderMail);
                            }

                            var mailRecord = recordList.Where(orderRecord => orderRecord.doc_bundling_flg == EnumDocBundlingFlg.OFF);

                            if (mailRecord.Count() > 0)
                            {
                                var model = this.LoadMailData(newOrder, sendno, shipdate, mailRecord);

                                this.SendMail(newOrder, model);
                            }

                            tx.Commit();
                        }
                    }
                    catch (Exception e)
                    {
                        listError.Add(e.ToString());
                    }
                }
            }

            if (listError.Count > 0)
            {
                throw new Exception(string.Join(Environment.NewLine, listError));
            }
        }

        private ErsOrderMail GetOrderMail(ErsOrderRecord record)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderMailRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderMailCriteria();
            criteria.d_no = record.d_no;
            criteria.ds_id = record.id;
            criteria.active = EnumActive.Active;
            return repository.FindSingle(criteria);
        }

        /// <summary>
        /// 明細の一覧を取得
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private IList<ErsOrderRecord> GetOrderRecords(ErsOrder order)
        {
            // 該当伝票番号
            // メール未送信
            // キャンセル、返品明細以外
            var orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var orderRecordCriteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
            orderRecordCriteria.d_no = order.d_no;
            orderRecordCriteria.order_status_not_in = ErsOrderCriteria.CANCEL_STATUS_ARRAY;
            orderRecordCriteria.shipped_mail = EnumSentFlg.NotSent;
            return orderRecordRepository.Find(orderRecordCriteria);
        }

        /// <summary>
        /// 対象伝票を取得
        /// </summary>
        /// <returns></returns>
        protected virtual IList<ErsOrder> GetListOrder()
        {
            // -ERS伝票
            // -配送済み

            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.mall_shop_kbn = EnumMallShopKbn.ERS;
            criteria.order_status_in = new[] { EnumOrderStatusType.DELIVERED };
            criteria.shipped_mail = EnumSentFlg.NotSent;

            return repository.Find(criteria);
        }

        /// <summary>
        /// メール送信用にデータを読み込みます
        /// </summary>
        /// <param name="order"></param>
        /// <param name="sendno"></param>
        /// <param name="shipdate"></param>
        /// <param name="orderRecordList"></param>
        /// <param name="command"></param>
        private ShippedMailModel LoadMailData(ErsOrder order, string sendno, DateTime? shipdate, IEnumerable<ErsOrderRecord> orderRecordList)
        {
            var model = new ShippedMailModel();

            model.d_no = order.d_no;
            model.mall_d_no = order.mall_d_no;
            model.sendno = sendno;
            model.senddate = order.senddate;
            model.shipdate = shipdate;
            model.lname = order.lname;
            model.fname = order.fname;
            model.intime = order.intime;
            model.sendtime = ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId(order.sendtime);
            model.amounttotal = orderRecordList.Count();
            model.subtotal = order.subtotal;
            model.carriage = order.carriage;
            model.p_service = order.p_service;
            model.coupon_discount = order.coupon_discount;
            model.tax = order.tax;
            model.total = order.total;
            model.orderRecords = orderRecordList;

            var setup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(order.site_id.Value);

            if (orderRecordList.Any(e => e.deliv_method == EnumDelvMethod.Mail))
            {
                model.delv_name = setup.mail_delivery;
                model.delv_url = setup.mail_url;
            }
            else
            {
                model.delv_name = setup.delivery;
                model.delv_url = setup.url;
            }

            model.w_pay = ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(order.pay);
            model.w_etc = ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetEtcNameFromId(order.pay);
            model.etc = order.etc;

            return model;
        }

        /// <summary>
        /// 配送完了メールを送信します
        /// </summary>
        /// <param name="item"></param>
        protected virtual void SendMail(ErsOrder order, ShippedMailModel command)
        {
            if (order.email == null)
            {
                //メールアドレスがない場合はメール送信をスキップ
                return;
            }

            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(order.mcode, true);
            var mformat = (member != null) ? member.mformat : EnumMformat.PC;

            var setup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(order.site_id.Value);

            var sendMail = ErsFactory.ersMailFactory.GetErsSendMailDelivered(order.site_id);

            sendMail.mail_from = setup.r_email;

            sendMail.Send(order.d_no, order.mcode, order.email, mformat, command);
       }
    }
}