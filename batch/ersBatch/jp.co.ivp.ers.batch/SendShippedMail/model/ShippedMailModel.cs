﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch.SendShippedMail.model
{
    public class ShippedMailModel
        : ErsModelBase
    {
        /// <summary>
        /// values for send mail
        /// </summary>
        public string d_no { get; set; }

        public string mall_d_no { get; set; }

        public IEnumerable<ErsOrderRecord> orderRecords { get; set; }

        public string sendno { get; set; }

        public DateTime? senddate { get; set; }

        public DateTime? shipdate { get; set; }

        public DateTime? intime { get; set; }

        public string w_pay { get; set; }

        public string sendtime { get; set; }

        public int amounttotal { get; set; }

        public int subtotal { get; set; }

        public int carriage { get; set; }

        public int p_service { get; set; }

        public int coupon_discount { get; set; }

        public int tax { get; set; }

        public int total { get; set; }

        public string lname { get; set; }

        public string fname { get; set; }

        public string w_etc { get; set; }

        public int etc { get; set; }

        public string delv_name { get; set; }

        public string delv_url { get; set; }
    }
}
