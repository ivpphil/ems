﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.batch.SendRegularOrderMail.model
{
    class RegularOrderMailModel
        : ErsModelBase
    {
        public ErsOrder objOrder { get; set; }

        public IEnumerable<ErsOrderRecord> listOrderRecords { get; set; }

        public string email { get; set; }

        public string d_no { get; set; }

        public string mall_d_no { get; set; }

        public string lname { get; set; }

        public string fname { get; set; }

        public string lnamek { get; set; }

        public string fnamek { get; set; }

        public string compname { get; set; }

        public string compnamek { get; set; }

        public string division { get; set; }

        public string divisionk { get; set; }

        public string zip { get; set; }

        public int? pref { get; set; }

        public string address { get; set; }

        public string taddress { get; set; }

        public string maddress { get; set; }

        public string tel { get; set; }

        public string fax { get; set; }

        public string add_lname { get; set; }

        public string add_fname { get; set; }

        public string add_lnamek { get; set; }

        public string add_fnamek { get; set; }

        public string add_compname { get; set; }

        public string add_compnamek { get; set; }

        public string add_zip { get; set; }

        public int? add_pref { get; set; }

        public string add_address { get; set; }

        public string add_taddress { get; set; }

        public string add_maddress { get; set; }

        public string add_tel { get; set; }

        public string add_fax { get; set; }

        public EnumOrderPaymentStatusType? order_payment_status { get; set; }

        public EnumSendTo? send { get; set; }

        public DateTime? senddate { get; set; }

        public int? sendtime { get; set; }

        public EnumWrap? wrap { get; set; }

        public string memo2 { get; set; }

        public string memo { get; set; }

        public string memo3 { get; set; }

        public string usr_memo { get; set; }

        public EnumPmFlg? pm_flg { get; set; }

        public int? carriage { get; set; }

        public int? etc { get; set; }

        public int? total { get; set; }

        public int? p_service { get; set; }

        public int? rakuten_p_service { get; set; }

        public string coupon_code { get; set; }

        public int coupon_discount { get; set; }

        public EnumOrderStatusType? order_status { get; set; }

        public string erp_d_no { get; set; }

        public int? adjust_price { get; set; }

        public EnumPaymentType? pay { get; set; }

        public string mcode
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return objOrder.mcode;
            }
        }

        public string c_req_no
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return objOrder.c_req_no;
            }
        }

        public int subtotal
        {
            get
            {
                if (objOrder == null)
                    return 0;

                return objOrder.subtotal;
            }
        }

        public int current_total
        {
            get
            {
                if (objOrder == null)
                    return 0;

                return objOrder.total;
            }
        }

        public int tax
        {
            get
            {
                if (objOrder == null)
                    return 0;

                return objOrder.tax;
            }
        }

        public DateTime? intime
        {
            get
            {
                if (objOrder == null)
                    return null;

                return objOrder.intime;
            }
        }

        public DateTime? utime
        {
            get
            {
                if (objOrder == null)
                    return null;

                return objOrder.utime;
            }
        }

        public DateTime? paid_date
        {
            get
            {
                if (objOrder == null)
                    return null;

                return objOrder.paid_date;
            }
        }


        public int? paid_price
        {
            get
            {
                if (objOrder == null)
                    return 0;

                return objOrder.paid_price;
            }
        }

        public string etc_name
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetEtcNameFromId(objOrder.pay);
            }
        }

        public string pay_name
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(objOrder.pay);
            }
        }

        public int amounttotal
        {
            get
            {
                if (objOrder == null)
                    return 0;

                return ErsFactory.ersOrderFactory.GetObtainAmountTotalStgy().Obtain(this.listOrderRecords);
            }
        }

        public string w_order_type
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                var orderType = ErsFactory.ersOrderFactory.GetObtainOrderTypeStgy().Obtain(this.listOrderRecords);
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.OrderType, EnumCommonNameColumnName.namename, (int)orderType);
            }
        }

        public string w_pm_flg
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.PmFlg, EnumCommonNameColumnName.namename, (int)objOrder.pm_flg);
            }
        }

        public string ccode
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return objOrder.ccode;
            }
        }

        /// <summary>
        /// コンビニ名
        /// </summary>
        public string w_ccode
        {
            get
            {
                if (objOrder == null)
                    return null;

                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ConvCode, EnumCommonNameColumnName.namename, (int?)objOrder.conv_code);
            }
        }

        /// <summary>
        /// コンビニ確認番号
        /// </summary>
        public string conv_conf_no
        {
            get
            {
                if (objOrder == null)
                    return null;

                return objOrder.conv_conf_no;
            }
        }

        /// <summary>
        /// コンビニ受付番号
        /// </summary>
        public string conv_receipt_no
        {
            get
            {
                if (objOrder == null)
                    return null;

                return objOrder.conv_receipt_no;
            }
        }

        /// <summary>
        /// コンビニ支払期限日時
        /// </summary>
        public DateTime? conv_payment_term
        {
            get
            {
                if (objOrder == null)
                    return null;

                return objOrder.conv_payment_term;
            }
        }

        public string w_pref
        {
            get
            {
                if (this.pref == null)
                    return string.Empty;

                return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(this.pref);
            }
        }

        #region for purchase mail.

        public string purchasedmail_fullname
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return string.Format("{0} {1}", this.lname, this.fname);

                return string.Format("{0} {1}", this.add_lname, this.add_fname);
            }
        }

        public string purchasedmail_zip
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return this.zip;

                return this.add_zip;
            }
        }

        public string purchasedmail_pref_name
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return this.w_pref;

                if (this.add_pref == null)
                    return string.Empty;

                return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(this.add_pref);
            }
        }

        public string purchasedmail_address
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return this.address;

                return this.add_address;
            }
        }

        public string purchasedmail_taddress
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return this.taddress;

                return this.add_taddress;
            }
        }

        public string purchasedmail_maddress
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return this.maddress;

                return this.add_maddress;
            }
        }

        #endregion

    }
}
