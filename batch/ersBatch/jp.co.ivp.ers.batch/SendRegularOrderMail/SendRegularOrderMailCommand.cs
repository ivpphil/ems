﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.batch.SendRegularOrderMail.model;

namespace jp.co.ivp.ers.batch.SendRegularOrderMail
{
    public class SendRegularOrderMailCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            this.ExecuteSendMail(executeDate.Value);
        }

        internal void ExecuteSendMail(DateTime executeDate)
        {
            var orderList = this.GetListOrder(executeDate);

            var repository = ErsFactory.ersOrderFactory.GetErsOrderMailRepository();
            foreach (var objOrder in orderList)
            {
                using (var tx = ErsDB_parent.BeginTransaction())
                {
                    var recordList = this.GetOrderRecords(objOrder);

                    foreach (var record in recordList)
                    {
                        // メールフラグを先に更新
                        var newOrderMail = this.GetOrderMail(record);
                        var oldOrderMail = ErsFactory.ersOrderFactory.GetErsOrderMail();
                        oldOrderMail.OverwriteWithParameter(newOrderMail.GetPropertiesAsDictionary());

                        newOrderMail.purchase_mail = EnumSentFlg.Sent;

                        repository.Update(oldOrderMail, newOrderMail);
                    }

                    var mailRecord = recordList.Where(orderRecord => orderRecord.doc_bundling_flg == EnumDocBundlingFlg.OFF);

                    var memberOrderModel = this.GetMailModel(objOrder, mailRecord);

                    EnumMformat mformat = this.GetMformat(objOrder);

                    var sendmail = ErsFactory.ersMailFactory.GetErsSendMailRegularOrder(objOrder.site_id);
                    sendmail.SendMail(objOrder.d_no, objOrder.mcode, objOrder.email, mformat, memberOrderModel);

                    tx.Commit();
                }
            }
        }

        private ErsOrderMail GetOrderMail(ErsOrderRecord record)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderMailRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderMailCriteria();
            criteria.d_no = record.d_no;
            criteria.ds_id = record.id;
            criteria.active = EnumActive.Active;
            return repository.FindSingle(criteria);
        }

        private EnumMformat GetMformat(ErsOrder objOrder)
        {
            var ersMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objOrder.mcode);

            if (ersMember != null && ersMember.mformat.HasValue)
            {
                return ersMember.mformat.Value;
            }
            else
            {
                return ErsFactory.ErsAtMailFactory.GetDetermineMformatStgy().WithDomain(objOrder.email);
            }
        }

        /// <summary>
        /// メール送信用のモデルを取得
        /// </summary>
        /// <param name="objOrder"></param>
        /// <param name="mailRecord"></param>
        /// <returns></returns>
        private RegularOrderMailModel GetMailModel(ErsOrder objOrder, IEnumerable<ErsOrderRecord> mailRecord)
        {
            var memberOrderModel = new RegularOrderMailModel();
            memberOrderModel.OverwriteWithParameter(objOrder.GetPropertiesAsDictionary());
            memberOrderModel.objOrder = objOrder;
            memberOrderModel.listOrderRecords = mailRecord;
            return memberOrderModel;
        }

        /// <summary>
        /// 対象伝票を取得
        /// </summary>
        /// <returns></returns>
        protected virtual IList<ErsOrder> GetListOrder(DateTime executeDate)
        {
            // -ERS伝票
            // -配送済み

            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.mall_shop_kbn = EnumMallShopKbn.ERS;
            criteria.pm_flg = EnumPmFlg.REGULAR;
            criteria.purchase_mail = EnumSentFlg.NotSent;
            criteria.email_not_equal = null;

            return repository.Find(criteria);
        }

        /// <summary>
        /// 明細の一覧を取得
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private IList<ErsOrderRecord> GetOrderRecords(ErsOrder order)
        {
            // 該当伝票番号
            // メール未送信
            var orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var orderRecordCriteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
            orderRecordCriteria.d_no = order.d_no;
            orderRecordCriteria.purchase_mail = EnumSentFlg.NotSent;
            return orderRecordRepository.Find(orderRecordCriteria);
        }
    }
}
