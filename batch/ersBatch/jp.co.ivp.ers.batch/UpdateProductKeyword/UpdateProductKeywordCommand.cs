﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.batch.UpdateProductKeyword
{
    public class UpdateProductKeywordCommand
         : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            var registKeywordStgy = ErsFactory.ersMerchandiseFactory.GetRegistKeywordStgy();
            var keywordConstracter = ErsFactory.ersMerchandiseFactory.GetProductKeywordConstracterStgy();

            DateTime? lastSuccessDate = DateTime.MinValue;

            if (options != null && options.ContainsKey("last_success_date") && options["last_success_date"] != null)
            {
                lastSuccessDate = (DateTime?)Convert.ToDateTime(options["last_success_date"]);
            }

            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            groupCriteria.ChangedIn();
            var listMerchandise = groupRepository.FindGroupBaseItemList(groupCriteria);

            foreach (var merchandise in listMerchandise)
            {
                using (var tx = ErsDB_parent.BeginTransaction())
                {
                    var objGroup = ErsFactory.ersMerchandiseFactory.GetErsGroupWithParameter(merchandise.GetPropertiesAsDictionary());
                    var keywords = keywordConstracter.CreateGroupKeyword(objGroup).Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    registKeywordStgy.UpdateGroupKeyword(merchandise.gcode, keywords);

                    registKeywordStgy.UpdateDetailKeyword(merchandise.gcode);

                    registKeywordStgy.UpdateGroupKeywordUpdated(merchandise.gcode, executeDate);
                    
                    registKeywordStgy.UpdateDetailKeywordUpdated(merchandise.gcode, executeDate);

                    tx.Commit();
                }
            }

            registKeywordStgy.DeleteIsolated();
        }
    }
}
