﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch.UpdatePriceForSearch
{
    public class UpdatePriceForSearchCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            ErsFactory.ersMerchandiseFactory.GetUpdatePriceForSearchStgy().Update();
        }
    }
}
