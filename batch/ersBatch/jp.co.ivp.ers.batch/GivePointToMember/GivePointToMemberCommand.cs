﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.batch.GivePointToMember
{
    public class GivePointToMemberCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            var regularExecuteDate = (options.ContainsKey("date")) ? Convert.ToDateTime(options["date"]) : executeDate ?? DateTime.Now;
            this.UpdateMemberPoint(regularExecuteDate);
        }


        /// <summary>
        /// update member point according to the order details.
        /// </summary>
        internal void UpdateMemberPoint(DateTime executeDate)
        {

            //Search target orders.
            var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();

            var criteria = this.GetCriteria(executeDate);

            var list = orderRepository.Find(criteria);

            foreach (var order in list)
            {
                using (var tx = ErsDB_parent.BeginTransaction())
                {
                    //point計算
                    int givePoint = this.GetGivePoint(order);

                    //give point to member
                    if (givePoint > 0)
                    {
                        var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(order.mcode);

                        if (member != null)
                        {
                            var reason = ErsResources.GetFieldName("point_reason_give_point");
                            ErsFactory.ersOrderFactory.GetUpdateMemberPointStgy().Increase(member.mcode, givePoint, reason, order.d_no, order.intime, (int)order.site_id);
                        }
                    }

                    order.point_ck = 1;

                    //update d_master_t.point_ck
                    var old_order = ErsFactory.ersOrderFactory.GetOrderWithD_no(order.d_no);
                    if (old_order == null)
                    {
                        throw new ErsException("30104", order.d_no);
                    }

                    orderRepository.Update(old_order, order);

                    tx.Commit();
                }
            }
        }

        /// <summary>
        /// Get give point.
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        private int GetGivePoint(ErsOrder order)
        {
            var orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var criteriaForRecordSearch = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
            criteriaForRecordSearch.d_no = order.d_no;
            criteriaForRecordSearch.order_status_in = new[] { EnumOrderStatusType.DELIVERED };
            criteriaForRecordSearch.SetOrderByPoint(Criteria.OrderBy.ORDER_BY_ASC);
            criteriaForRecordSearch.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
            var orderRecords = orderRecordRepository.Find(order, criteriaForRecordSearch);

            var usePoint = order.p_service;
            var couponDiscount = order.coupon_discount;
            var discount = usePoint + couponDiscount;
            var givePoint = 0;
            foreach (var orderRecord in orderRecords.Values)
            {
                int recordTotal = orderRecord.GetAmount() * orderRecord.price.Value;

                if (discount <= recordTotal)
                {
                    recordTotal = recordTotal - discount;
                    discount = 0;
                }
                else
                {
                    discount = discount - recordTotal;
                    recordTotal = 0;
                }

                givePoint += (int)Math.Round(recordTotal * (((double)orderRecord.point.Value) / 100) * (((double)(order.point_magnification) / 100)));
            }
            return givePoint;
        }

        /// <summary>
        /// Get Criteria instance for search order.
        /// </summary>
        /// <param name="baseDate"></param>
        /// <returns></returns>
        private ErsOrderCriteria GetCriteria(DateTime baseDate)
        {
            //基準日
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

            criteria.shipdate = baseDate.AddDays(-1 * setup.daysGivePoint);
            criteria.order_status_in = new[] { EnumOrderStatusType.DELIVERED };
            // Changed not to consider the payment status
            //criteria.order_payment_status_in = new[] { EnumOrderPaymentStatusType.PAID };
            criteria.point_ck = 0;
            criteria.mcode_not_equal = null;

            criteria.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);
            return criteria;
        }
    }
}
