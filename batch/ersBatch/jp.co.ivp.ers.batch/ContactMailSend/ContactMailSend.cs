﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.batch.ContactMailSend
{
    public class ContactMailSend
         : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            this.Execute();
        }

        private void Execute()
        {
            List<string> insertErrArr = new List<string>();

            //対象リスト取得
            var repo = ErsFactory.ersCtsInquiryFactory.GetErsCtsSendEmailRepository();
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsSendEmailCriteria();

            criteria.email_status  = EnumEnqEmailStatus.EmailRegist;

            var dateList = repo.Find(criteria);

            foreach (var objOld in dateList)
            {
                try
                {
                    using (var transaction = ErsDB_parent.BeginTransaction())
                    {
                        string mailTitle = objOld.email_title;
                        string mailBody = objOld.email_header + Environment.NewLine;
                        mailBody += objOld.email_body + Environment.NewLine;
                        mailBody += objOld.email_fotter + Environment.NewLine;

                        string mail_from = objOld.email_from;
                        string mail_to = objOld.email_to[0];

                        string cc_email = string.Empty;
                        if (objOld.email_cc != null && objOld.email_cc.Length > 0)
                            cc_email = string.Join(",", objOld.email_cc);

                        string bcc_email = string.Empty;
                        if (objOld.email_bcc !=null && objOld.email_bcc.Length > 0)
                            bcc_email = string.Join(",", objOld.email_bcc);

                        //メール処理
                        var mail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();
                        mail.MailSend(
                            mailTitle,
                            mailBody,
                            System.IO.Path.GetFileName(System.Reflection.Assembly.GetCallingAssembly().Location),
                            mail_to,
                            mail_from,
                            cc_email,
                            bcc_email);

                        //DB更新
                        var objNew = ErsFactory.ersCtsInquiryFactory.GetErsCtsSendEmailWithId(objOld.id);

                        objNew.id = objOld.id;
                        objNew.email_status = EnumEnqEmailStatus.SendFinish;
                        objNew.utime = DateTime.Now;
                        repo.Update(objOld, objNew);

                        transaction.Commit();
                    }
                }
                catch (Exception ex)
                {
                    insertErrArr.Add(DateTime.Now + ", " + ex.ToString());
                }
            }

            //1件でもエラーがあれば
            if (insertErrArr.Count != 0)
            {
                throw new Exception(string.Join(",", insertErrArr));
            }
        }
    }
}
