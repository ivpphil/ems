﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch.UpdateSpecifiedColumn;

namespace jp.co.ivp.ers.batch.DeleteSpecifiedColumn
{
    public class DeleteSpecifiedColumnMail
        : UpdateSpecifiedColumnMail
    {
        public DeleteSpecifiedColumnMail(string _fileName, string _errorFileName)
            : base(_fileName, _errorFileName)
        {
        }

        public override void SendMail(string body)
        {
            var sendmail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();

            sendmail.Bind(this, body);

            string title = setup.deleteSpecifiedColumnAlertMailTitle;
            string mailTo = setup.deleteSpecifiedColumnAlertMailTo;
            string mailFrom = ErsFactory.ersUtilityFactory.GetErsSetupOfSite().f_email1;

            sendmail.MailSend(
                title,
                sendmail.body,
                System.IO.Path.GetFileName(System.Reflection.Assembly.GetCallingAssembly().Location),
                mailTo,
                mailFrom,
                string.Empty,
                string.Empty);
        }

        public override string MailBody()
        {
            string defaultBody = string.Empty;

            defaultBody += "ご担当者様";
            defaultBody += Environment.NewLine + Environment.NewLine;
            defaultBody += "DB削除を完了いたしましたので、ご報告申し上げます。";
            defaultBody += Environment.NewLine;
            defaultBody += "ファイル名：<%=.Model.fileName%>";

            defaultBody += Environment.NewLine + Environment.NewLine;

            if (errorFileName.HasValue())
            {
                defaultBody += "エラーを以下のファイルに保存いたしました。";
                defaultBody += Environment.NewLine;
                defaultBody += "ファイル名：<%=.Model.errorFileName%>";
            }
            else
            {
                defaultBody += "全件を正常にDELETEいたしました。";
            }

            defaultBody += Environment.NewLine;

            return defaultBody;
        }
    }
}
