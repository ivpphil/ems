﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.IO;
using System.Collections.Concurrent;
using jp.co.ivp.ers.batch.DeleteSpecifiedColumn.Model;
using jp.co.ivp.ers.batch.UpdateSpecifiedColumn.Model;
using jp.co.ivp.ers.update_specified_column;

namespace jp.co.ivp.ers.batch.DeleteSpecifiedColumn
{
    public class DeleteSpecifiedColumnCommand
        : IErsBatchCommand
    {
        Setup setup;
        ErsCsvCreater csvCreater;
        private const string backupPrefixExt = "-backup-";
        private const string errorPrefixExt = "-error-";
        private bool HeaderExist = false;

        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            setup = ErsFactory.ersUtilityFactory.getSetup();

            //Execute Process
            this.Execute();
        }

        protected virtual void Execute()
        {
            ErsDirectory.CreateDirectories(setup.deleteFilePath);
            var uploadedFileList = Directory.EnumerateFiles(setup.deleteFilePath, "*.csv");

            //-- TODO: Temporary removed ==>>> Parallel Lambda Expression
            foreach (string filePath in uploadedFileList)
            {
                BindParallelOpenedFile(filePath);
            }
        }

        private void BindParallelOpenedFile(string filePath)
        {
            var exceptionsList = new ConcurrentQueue<Exception>();

            try
            {
                string backupFileName = Path.Combine(setup.deleteFileBackupPath, Path.GetFileNameWithoutExtension(filePath) + backupPrefixExt);
                var backupFilePath = ErsFile.WriteAllWithRandomName(backupFileName, "csv", string.Empty, ErsEncoding.ShiftJIS);

                //Set Locked Current File
                using (var fileStream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    this.BindAndDeleteCsvSchema(filePath, fileStream, backupFilePath, exceptionsList);
                }
            }
            catch (IOException ioEx)
            {
                //Identify if file is locked
                if (ErsFile.IsFileLocked(ioEx))
                    return;

                var exMessage = ioEx.Message;

                exMessage = exMessage.Replace(filePath, Path.GetFileName(filePath));
                exceptionsList.Enqueue(new Exception(exMessage));
            }
            catch (Exception ex)
            {
                var exMessage = ex.Message;

                exMessage = exMessage.Replace(filePath, Path.GetFileName(filePath));
                exceptionsList.Enqueue(new Exception(exMessage));
            }
            finally
            {
                //Delete Finished file
                this.DeleteFile(filePath, exceptionsList);

                var errorFilePath = this.SaveErrorLogs(filePath, exceptionsList);
                this.SendAlertMail(Path.GetFileName(filePath), Path.GetFileName(errorFilePath), exceptionsList);
            }
        }

        //Bind Current File
        protected virtual void BindAndDeleteCsvSchema(string filePath, Stream stream, string backupFilePath, ConcurrentQueue<Exception> exceptionsList)
        {
            try
            {
                //Backup class writer
                csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();

                var csvFileLoader = ErsFactory.ersUtilityFactory.GetErsCsvSchemaContainer<DeleteContainerModel>();
                csvFileLoader.LoadPostedFile(filePath, stream);

                if (csvFileLoader.validIndexes.Count() > 0)
                {
                    var validSchemaList = csvFileLoader.GetValidatedSchema();

                    //Create file backup
                    using (var writer = csvCreater.GetWriter(backupFilePath))
                    {
                        foreach (var schemaDictionary in validSchemaList)
                        {
                            var containerModel = (DeleteContainerModel)schemaDictionary["containerModel"];

                            if (containerModel.IsValid)
                            {
                                this.ExecuteDelete(containerModel, schemaDictionary, csvFileLoader.tableName, csvFileLoader.parameterKey, writer, exceptionsList);
                                continue;
                            }

                            csvFileLoader.MarkRecordAsInvalid(schemaDictionary);
                            exceptionsList.Enqueue(new Exception(String.Join(Environment.NewLine, containerModel.GetAllErrorMessageList())));
                        }
                    }

                    if (csvFileLoader.validIndexes.Count() <= 0)
                    {
                        //Delete Backup when there's no valid data
                        this.DeleteFile(backupFilePath, exceptionsList);
                    }
                }

            }
            catch (Exception ex)
            {
                exceptionsList.Enqueue(new Exception(ex.Message));
            }

        }

        //Execute Delete of Current File
        protected void ExecuteDelete(DeleteContainerModel containerModel, Dictionary<string, object> objDic, string tableName, List<string> parameterKey, StreamWriter writer, ConcurrentQueue<Exception> exceptionsList)
        {
            try
            {
                var repository = ErsFactory.ersUpdateSpecifiedColumnFactory.GetErsUpdateSpecifiedColumnRepository(tableName);
                var criteria = ErsFactory.ersUpdateSpecifiedColumnFactory.GetErsUpdateSpecifiedColumnCritera();
                for (int i = 0; i < parameterKey.Count(); i++)
                {
                    criteria[tableName, parameterKey[i], db.Criteria.Operation.EQUAL] = objDic[parameterKey[i]];
                }

                var oldObjDataList = repository.Find(criteria);

                if (oldObjDataList.Count > 0)
                {

                    //Write Header of backup
                    if (!this.HeaderExist)
                    {
                        IEnumerable<string> csv_header = oldObjDataList.FirstOrDefault().Keys;
                        csvCreater.WriteCsvHeader(csv_header, writer);
                        HeaderExist = true;
                    }


                    foreach (var oldObjEntity in oldObjDataList)
                    {
                        //Backup data
                        csvCreater.WriteBody(oldObjEntity.GetPropertiesAsDictionary(), writer);

                        //Delete after been backup the data
                        repository.Delete(oldObjEntity);
                    }
                }
                else
                {
                    containerModel.AddInvalidField(string.Empty, ErsResources.GetMessage("non_object_record"));
                    exceptionsList.Enqueue(new Exception(String.Join(Environment.NewLine, containerModel.GetAllErrorMessageList())));
                }
            }
            catch (Exception ex)
            {
                string exMessage = string.Empty;

                if (ex.InnerException != null)
                    exMessage = ex.InnerException.Message;

                if (!exMessage.HasValue())
                    exMessage = ex.Message;

                containerModel.AddInvalidField(string.Empty, exMessage);
                exceptionsList.Enqueue(new Exception(String.Join(Environment.NewLine, containerModel.GetAllErrorMessageList())));
            }
        }

        //Delete Current File
        private void DeleteFile(string filePath, ConcurrentQueue<Exception> exceptionsList)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                exceptionsList.Enqueue(new Exception(ex.Message));
            }
        }

        //Save Current File
        public string SaveErrorLogs(string originPath, ConcurrentQueue<Exception> exceptionsList)
        {
            if (this.HasError(exceptionsList))
            {
                var errorMessageList = exceptionsList.Select((err) => err.Message);
                var errorLogs = String.Join(Environment.NewLine, errorMessageList);

                string pathFileName = Path.Combine(setup.deleteFileErrorPath, Path.GetFileNameWithoutExtension(originPath) + errorPrefixExt);

                ErsDirectory.CreateDirectories(setup.deleteFileErrorPath);
                return ErsFile.WriteAllWithRandomName(pathFileName, "csv", errorLogs);
            }

            return null;
        }

        //Send mail after current file has done
        public void SendAlertMail(string deletedFileName, string errorFileName, ConcurrentQueue<Exception> exceptionsList)
        {
            if (!this.HasError(exceptionsList))
                errorFileName = string.Empty;

            var mailModel = new DeleteSpecifiedColumnMail(deletedFileName, errorFileName);
            mailModel.SendMail(mailModel.MailBody());
        }

        protected virtual bool HasError(ConcurrentQueue<Exception> exceptionsList)
        {
            return (exceptionsList != null && exceptionsList.Count > 0);
        }
    }
}
