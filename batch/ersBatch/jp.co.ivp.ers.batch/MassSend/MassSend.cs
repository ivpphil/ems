﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.mvc;
using System.Threading;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.batch.MassSend
{
    public class MassSend
    {
        internal void Execute(Configuration config)
        {
            var insertErrArr = new List<string>();

            var lstResult = this.findMailTo(config);

            var setup = ErsFactory.ersBatchFactory.getSetup();

            int setAtOnceCount = 0;
            foreach (var objOld in lstResult)
            {
                try
                {
                    using (var tx = ErsDB_parent.BeginTransaction())
                    {
                        if(checkMailStatus(config, objOld.id))
                        {
                            var sent_flg = this.sendMail(config, objOld);

                            this.updateMailTo(config, objOld.id, sent_flg);

                            tx.Commit();
                        }
                    }
                }
                catch (Exception ex)
                {
                    insertErrArr.Add(DateTime.Now + ", " + ex.ToString());
                }
                finally
                {
                    if (++setAtOnceCount == setup.MassSend_sendAtOnce)
                    {
                        setAtOnceCount = 0;
                        Thread.Sleep(setup.MassSend_restInterval.Value);
                    }
                }
            }

            //1件でもエラーがあれば
            if (insertErrArr.Count != 0)
            {
                throw new Exception(string.Join(Environment.NewLine, insertErrArr));
            }
        }

        /// <summary>
        /// Find am_mailto_t
        /// </summary>
        /// <param name="dbCon"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        private IList<ErsMailTo> findMailTo(Configuration config)
        {

            var repo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();

            //D2) By the following condition, get the list of an assigned mailto address from a am_mailto_t.
            criteria.process_id = config.processID;
            criteria.idFrom = config.idFrom;
            criteria.idTo = config.idTo;
            criteria.sent_flg = EnumSentFlg.NotSent;
            criteria.scheduled_date_less_or_null = config.ExecuteDateTime;
            criteria.active = EnumActive.Active;

            return repo.Find(criteria);
        }

        /// <summary>
        /// Send Mail
        /// </summary>
        private EnumSentFlg sendMail(Configuration config, ErsMailTo objMailTo)
        {
            if (!objMailTo.email.HasValue())
            {
                return EnumSentFlg.DataError;
            }

            var sendmail = ErsFactory.ersMailFactory.GetErsSendMailAtMail();

            sendmail.mail_to = objMailTo.email;
            sendmail.mail_from = config.mailFrom;
            sendmail.reply_to = config.replayTo;
            sendmail.subject = config.subject;

            var mailTo = ErsFactory.ErsAtMailFactory.GetEmailToModel();
            mailTo.OverwriteWithParameter(objMailTo.GetPropertiesAsDictionary());
            var mformat = objMailTo.mformat.Value;

            //D3) Bind the value of am_mailto_t to the body of the email.
            if (EnumMformat.PC == mformat | string.IsNullOrEmpty(config.feature_body))
            {
                sendmail.Bind(mailTo, config.body, config.html_body, config.processID + "PC");
            }
            else
            {
                sendmail.Bind(mailTo, config.feature_body, config.processID + "Mobile");
            }

            var setup = ErsFactory.ersBatchFactory.getSetup();
            
            //D3) Use the method of ErsSmtp SendSynchronous class to send an email.
            sendmail.SendMail(setup.MassSend_hostname, setup.MassSend_port.Value);

            return EnumSentFlg.Sent;
        }

        private bool checkMailStatus(Configuration config, int? id)
        {
            var repoCheck = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var mailTo = ErsFactory.ErsAtMailFactory.GetErsAtMailProcessIdWithRecordLock(id);
            return (mailTo.sent_flg == EnumSentFlg.NotSent);
        }

        /// <summary>
        /// Update am_mailto_t
        /// </summary>
        private void updateMailTo(Configuration config, int? id, EnumSentFlg sent_flg)
        {
            //D3) When the transmission succeeds at each transmission, update the record of the list of mail delivery.																					

            var repoUpdate = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var oldMailTo = ErsFactory.ErsAtMailFactory.GetErsAtMailProcessId(id);
            var newMailTo = ErsFactory.ErsAtMailFactory.GetErsAtMailProcessId(id);

            newMailTo.sent_flg = sent_flg;
            newMailTo.sent_date = DateTime.Now;
            repoUpdate.Update(oldMailTo, newMailTo);
        }
    }
}
