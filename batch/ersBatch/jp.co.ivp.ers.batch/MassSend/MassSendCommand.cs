﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch.util;

namespace jp.co.ivp.ers.batch.MassSend
{
    public class MassSendCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            Configuration config = new Configuration();

            var listError = new List<string>();

            //D1) All the value which used by MassSend are received from MassSendManager.
            config.processID = ErsObtainOption.GetInt32Value(options, "processID", listError);
            config.idFrom = ErsObtainOption.GetInt32Value(options, "idFrom", listError);
            config.idTo = ErsObtainOption.GetInt32Value(options, "idTo", listError);
            config.mailFrom = ErsObtainOption.GetStringValue(options, "mailFrom", listError);
            config.replayTo = ErsObtainOption.GetStringValue(options, "replayTo", listError);
            config.subject = ErsObtainOption.GetStringValue(options, "subject", listError);
            config.body = ErsObtainOption.GetStringValue(options, "body", listError);
            config.html_body = ErsObtainOption.GetStringValue(options, "html_body", listError);
            config.feature_body = ErsObtainOption.GetStringValue(options, "feature_body", listError);
            config.ExecuteDateTime = ErsObtainOption.GetDateTimeValue(options, "ExecuteDateTime", listError);

            if (listError.Count > 0)
            {
                throw new Exception(string.Join(Environment.NewLine, listError));
            }

            var mSend = new MassSend();
            mSend.Execute(config);
        }
    }
}
