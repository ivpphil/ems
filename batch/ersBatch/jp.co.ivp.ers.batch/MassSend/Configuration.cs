﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Reflection;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch.MassSend
{
    public class Configuration
    {
        public long? processID { get; set; }
        public int? idFrom { get; set; }
        public int? idTo { get; set; }

        public string mailFrom { get; set; }
        public string replayTo { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string html_body { get; set; }
        public string feature_body { get; set; }

        public DateTime? ExecuteDateTime { get; set; }
    }
}
