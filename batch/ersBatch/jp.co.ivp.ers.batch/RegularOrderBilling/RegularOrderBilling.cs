﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using System.Collections;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.merchandise.strategy;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.order.strategy;

namespace jp.co.ivp.ers.batch.RegularOrderBilling
{
    public class RegularOrderBilling : ErsRepository
    {
        internal List<string> insertErrArr = new List<string>();
        internal List<string> gmoErrArr = new List<string>();
        internal List<string> stockErrArr = new List<string>();
        internal List<string> issuedOrderNo = new List<string>();

        ErsRegularLogRepository regularLogRepository = ErsFactory.ersOrderFactory.GetErsRegularLogRepository();
        ErsRegularOrderRecordRepository orderRecRep = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
        ErsRegularErrLogRepository regularErrLogRepository = ErsFactory.ersOrderFactory.GetErsRegularErrLogRepository();
        ObatainTargetRegularOrderRecordKeysSpec obatainTargetRegularOrderRecordKeysSpec = ErsFactory.ersOrderFactory.GetObatainTargetRegularOrderRecordKeysSpec();
        PutRegularDateForwardStgy putRegularDateForwardStgy = ErsFactory.ersOrderFactory.GetPutRegularDateForwardStgy();

        public class MemberNotFoundGmoException
            : GmoException
        {
            private string _message = string.Empty;

            public MemberNotFoundGmoException(string message)
                : base()
            {
                this._message = message;
            }

            public override string errInfoMsg
            {
                get
                {
                    return this._message;
                }
            }
        }

        public int Execute(DateTime executeDate)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //対象の明細を取得する。
            var criteria = this.SetCriteriaSelectMcode(executeDate);
            var obatainTargetRegularOrderSpec = ErsFactory.ersOrderFactory.GetObatainTargetRegularOrderSpec();
            var serchResultList = obatainTargetRegularOrderSpec.Obtain(criteria);

            var memberRepository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var orderMailRepository = ErsFactory.ersOrderFactory.GetErsOrderMailRepository();

            foreach (var serchResulItem in serchResultList)
            {
                List<int> serchIdResultList = null;
                try
                {
                    //対象会員に紐づく定期明細取得
                    var regCriteria = this.SetCriteriaSelectDetailId(serchResulItem);
                    serchIdResultList = obatainTargetRegularOrderRecordKeysSpec.ObtainRecordKeys(regCriteria);

                    //定期ヘッダはあるが有効定期明細が無い場合は処理ぬける
                    if (serchIdResultList.Count == 0)
                    {
                        continue;
                    }

                    //会員に関する情報取得   
                    var mcode = Convert.ToString(serchResulItem["mcode"]);
                    var ersMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);

                    if (ersMember == null)
                    {
                        throw new Exception(string.Format("会員データが見つかりません。(会員番号:{0})", mcode));
                    }

                    //定期クラス生成と初期値のセット
                    var regularRegister = ErsFactory.ersOrderFactory.GetErsRegularRegister();
                    regularRegister.LoadValues(serchResulItem, ersMember);

                    //1-4-4)クレジットカード決済で、使用カードの「カード洗い替えステータス」が「エラー」の場合、エラーにせず、正常終了する（次の伝票へ）
                    if (regularRegister.pay == EnumPaymentType.CREDIT_CARD)
                    {
                        var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                        var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
                        memberCardCriteria.active = EnumActive.Active;
                        memberCardCriteria.mcode = regularRegister.mcode;
                        memberCardCriteria.id = regularRegister.member_card_id;
                        var ersMemberCard = memberCardRepository.FindSingle(memberCardCriteria);
                        if (ersMemberCard.update_status == EnumCardUpdateStatus.Error)
                        {
                            continue;
                        }
                    }

                    var ersOrder = this.GetNewOrder(regularRegister);

                    //新たな伝票詳細を作成
                    var orderRecords = this.GetNewOrderDetails(serchIdResultList, ersOrder);

                    using (var tx = ErsDB_parent.BeginTransaction())
                    {
                        foreach (int id in serchIdResultList)
                        {
                            this.UpdateRegularOrderDetail(id);
                        }

                        if (orderRecords.Count == 0)
                        {
                            //明細が全てスキップされた場合は、定期明細への修正のみをコミットして次の発行へすすむ
                            tx.Commit();
                            continue;
                        }

                        //伝票番号
                        ErsFactory.ersOrderFactory.GetSetD_noStgy().SetNext(ersOrder, orderRecords.Values);
                        ersOrder.base_d_no = ersOrder.d_no;
                        ersOrder.site_id = Convert.ToInt32(serchResulItem["site_id"]);

                        int sequence_subd_no = 1;
                        var d_no = ersOrder.d_no + "-" + (sequence_subd_no.ToString("D2"));
                        ErsFactory.ersOrderFactory.GetSetD_noStgy().Set(ersOrder, orderRecords.Values, d_no);

                        ersOrder.ransu = ErsFactory.ersSessionStateFactory.GetObtainRansuStgy().CreateNewRansu();
                        ersOrder.utime = null;

                        //金額算出
                        var clacService = ErsFactory.ersOrderFactory.GetErsCalcService();
                        clacService.calcOrder(ersOrder, orderRecords.Values, 0);

                        //顧客購入合計金額更新
                        ErsFactory.ersOrderFactory.GetUpdateMemberDataStgy().Update(ersMember, ersOrder.total, 1, DateTime.Now);

                        var oldMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);
                        memberRepository.Update(oldMember, ersMember);

                        if (!string.IsNullOrEmpty(ersOrder.mcode))
                        {
                            var site_id = setup.member_rank_centralization ? (int)EnumSiteId.COMMON_SITE_ID : ersOrder.site_id;
                            var member_rank = ErsFactory.ersMemberFactory.GetErsMemberRankSetupSearchSpecification().SelectAsList(ersOrder.mcode, site_id);

                            if (member_rank.Count > 0)
                            {
                                ersOrder.point_magnification = (int)member_rank[0]["point_magnification"];
                            }
                            else
                            {
                                ersOrder.point_magnification = 100;
                            }
                        }

                        //支払い方法がカードの場合
                        if (ersOrder.pay == EnumPaymentType.CREDIT_CARD && ersOrder.card_info == null)
                        {
                            throw new MemberNotFoundGmoException(string.Format("カード情報が登録されていません。(会員番号:{0} / カード預け番号:{1})", regularRegister.mcode, regularRegister.member_card_id));
                        }

                        if (ErsFactory.ersOrderFactory.GetErsPayment(ersOrder.pay) is ErsGmoCard
                            && ersOrder.total <= setup.gmo_compensable_total)
                        {
                            // GMOのカード決済の場合に、3万円以上の場合のみ与信を取得するように修正する。（3万円未満の場合は、与信を取得しない）
                            // 与信を取得しなかった場合、入金ステータスを「与信未取得」にセットする
                            ersOrder.order_payment_status = EnumOrderPaymentStatusType.NO_AUTH;
                            // 与信を取得しなかった場合でも、GMO用の決済番号は発行しておく
                            ersOrder.credit_order_id = ErsFactory.ersOrderFactory.GetErsOrderRepository().GetNextCreditOrderId(ersOrder.d_no);
                            ersOrder.sent_continual_billing = EnumSentContinualBillingFlg.Waiting;
                        }
                        else
                        {
                            var payment = ErsFactory.ersOrderFactory.GetErsPayment(ersOrder.pay);
                            payment.SendAuth(ersOrder, ersMember, false);
                        }

                        //伝票生成
                        var orderRep = ErsFactory.ersOrderFactory.GetErsOrderRepository();
                        orderRep.Insert(ersOrder);

                        foreach (var orderRecord in orderRecords.Values)
                        {
                            orderRecord.site_id = Convert.ToInt32(serchResulItem["site_id"]);
                            orderRecordRepository.Insert(orderRecord, true);

                            var newOrderMail = this.GetOrderMail(orderRecord);
                            orderMailRepository.Insert(newOrderMail);
                        }

                        //セット商品があれば
                        ErsFactory.ersOrderFactory.GetRegistSetItemsStgy().RegistRegularBatch(orderRecords.Values);

                        //在庫減算
                        var decreaseStockStgy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetDecreaseStockStgy();
                        foreach (var record in orderRecords.Values)
                        {
                            decreaseStockStgy.Decrease(record.scode, record.amount.Value);
                        }

                        // モール在庫更新 [Update mall stock]
                        this.UpdateMallStock(orderRecords);

                        tx.Commit();

                        this.issuedOrderNo.Add(ersOrder.d_no);
                    }
                }
                //在庫エラー
                catch (ErsRegularOutOfStockException stockEx)
                {
                    //エラーログをインサートする
                    this.InsertErrorLog(serchResulItem, stockEx.errMsg, EnumRegularErrLogDispFlg.StockError, serchIdResultList);

                    stockErrArr.Add(stockEx.errMsg);
                }
                catch (GmoException gmoException)
                {
                    //エラーログをインサートする
                    this.InsertErrorLog(serchResulItem, gmoException.errInfoMsg, EnumRegularErrLogDispFlg.PaymentError, serchIdResultList);

                    var errMsg = DateTime.Now.ToString() + ", mcode=" + serchResulItem["mcode"] + ", " + gmoException.errInfoMsg + Environment.NewLine;
                    gmoErrArr.Add(errMsg);
                }
                catch (Exception ex)
                {
                    //エラーログをインサートする
                    this.InsertErrorLog(serchResulItem, ex.ToString(), EnumRegularErrLogDispFlg.SystemError, serchIdResultList);

                    var errMsg = "mcode=" + serchResulItem["mcode"]+", err=" + ex.ToString() + Environment.NewLine + Environment.NewLine;
                    insertErrArr.Add(errMsg);
                }
            }

            return 0;
        }

        /// <summary>
        /// メール送信判定レコード取得
        /// </summary>
        /// <param name="objOrder"></param>
        /// <param name="orderRecord"></param>
        /// <returns></returns>
        private ErsOrderMail GetOrderMail(ErsOrderRecord orderRecord)
        {
            var newOrderMail = ErsFactory.ersOrderFactory.GetErsOrderMail();
            newOrderMail.d_no = orderRecord.d_no;
            newOrderMail.ds_id = orderRecord.id;
            return newOrderMail;
        }

        /// <summary>
        /// モール在庫更新 [Update mall stock]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        protected void UpdateMallStock(IDictionary<string, ErsOrderRecord> orderRecords)
        {
            var listParam = new List<UpdateStockParam>();

            foreach (var record in orderRecords.Values)
            {
                var objSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(record.scode);

                if (objSku != null && objSku.h_mall_flg == EnumOnOff.On)
                {
                    UpdateStockParam param = default(UpdateStockParam);

                    param.productCode = record.scode;
                    param.quantity = record.amount;
                    param.operation = EnumMallStockOperation.sub;

                    listParam.Add(param);
                }
            }

            // モール在庫更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }
        }

        private void UpdateRegularOrderDetail(int id)
        {
            //定期明細取得
            var detail = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordWithId(id);
            var oldDetail = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordWithId(id);

            //ログ処理
            var regularLog = ErsFactory.ersOrderFactory.GetErsRegularLogWithParameters(oldDetail.GetPropertiesAsDictionary());
            regularLog.regular_detail_id = oldDetail.id;
            regularLog.intime = null;
            regularLog.utime = null;
            regularLogRepository.Insert(regularLog, true);

            putRegularDateForwardStgy.Execute(detail, true);

            //DB更新
            orderRecRep.Update(oldDetail, detail);
        }

        #region "エラーログをインサートする"
        /// <summary>
        /// エラーログをインサートする
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="errMsg"></param>
        /// <param name="disp_flg"></param>
        /// <param name="regularDetailIdArr"></param>
        private void InsertErrorLog(Dictionary<string, object> dic, string errMsg, EnumRegularErrLogDispFlg disp_flg, IEnumerable<int> regularDetailIdArr)
        {
            var regularErrLog = ErsFactory.ersOrderFactory.GetErsRegularErrLog();
            regularErrLog.OverwriteWithParameter(dic);
            regularErrLog.error_description = "定期伝票発行：" + errMsg;
            regularErrLog.disp_flg = disp_flg;
            regularErrLog.regular_detail_id = regularDetailIdArr.ToArray();
            regularErrLog.active = EnumActive.Active;
            regularErrLog.occured_date = DateTime.Now;
            regularErrLogRepository.Insert(regularErrLog, true);

            if (disp_flg == EnumRegularErrLogDispFlg.PaymentError)
            {
                this.InsertCtsEnquiry(regularErrLog.mcode, errMsg);
            }
        }

        private void InsertCtsEnquiry(string mcode, string description)
        {
            var ctsEnquiryRepository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();

            var ctsEnquiry = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiry();

            ctsEnquiry.mcode = mcode;
            ctsEnquiry.intime = DateTime.Now;
            ctsEnquiry.enq_casename = "定期伝票発行：" + description;
            ctsEnquiry.card_error_flg = EnumCardErrFlg.Err;

            ctsEnquiryRepository.Insert(ctsEnquiry, true);

            var ctsEnquiryDetailRepository = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetailRepository();
            var ctsEnquiryDetail = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetail();
            ctsEnquiryDetail.case_no = ctsEnquiry.case_no;
            ctsEnquiryDetail.enq_detail = "定期伝票発行：" + description;

            ctsEnquiryDetailRepository.Insert(ctsEnquiryDetail, true);
        }

        #endregion

        private ErsOrder GetNewOrder(RegularRegister regularRegister)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            // 伝票ヘッダにパラメータセット
            var inputParameters = regularRegister.GetPropertiesAsDictionary();
            var ersOrder = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(inputParameters);
            var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(ersOrder.pay);
            objPayment.SetPaymentMethod(ersOrder, regularRegister);

            //購入経路
            ersOrder.pm_flg = EnumPmFlg.REGULAR;//定期
            ersOrder.site_id = setup.site_id; // サイトID

            return ersOrder;
        }

        private IDictionary<string, ErsOrderRecord> GetNewOrderDetails(List<int> serchIdResultList, ErsOrder ersOrder)
        {
            IDictionary<string, ErsOrderRecord> valReturn = new Dictionary<string, ErsOrderRecord>();

            //伝票生成対象があれば
            foreach (int id in serchIdResultList)
            {
                //定期明細取得
                var detail = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordWithId(id);

                //在庫、販売期間、商品が販売対象かチェック 1商品でも購入不可なら伝票は生成しない
                if (!ErsFactory.ersOrderFactory.GetCheckRegularBatchIsPurchaseStgy().Check(detail.scode))
                {
                    throw new ErsRegularOutOfStockException(detail.scode, detail.mcode);
                }

                //スキップ対象なら
                if (detail.skip_date != detail.next_date)
                {
                    //伝票明細処理
                    var record = ErsFactory.ersOrderFactory.GetAddBatchOrderStrategy().GetOrderRecord(ersOrder.d_no, detail);
                    if (valReturn.ContainsKey(record.Key))
                    {
                        //数量および定期IDを加算
                        valReturn[record.Key].amount += record.Value.amount;
                        valReturn[record.Key].total += record.Value.total;

                        var regular_detail_id = valReturn[record.Key].regular_detail_id.ToList();
                        regular_detail_id.AddRange(record.Value.regular_detail_id);
                        valReturn[record.Key].regular_detail_id = regular_detail_id.ToArray();
                    }
                    else
                    {
                        valReturn.Add(record);
                    }
                    ersOrder.ccode = detail.ccode;
                }
            }

            return valReturn;
        }

        /// <summary>
        /// 伝票生成対象会員用条件セット
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private ErsRegularOrderCriteria SetCriteriaSelectMcode(DateTime executeDate)
        {
            ErsRegularOrderCriteria tpRegCriteria = ErsFactory.ersOrderFactory.GetErsRegularOrderCriteria();

            tpRegCriteria.AddGroupBy("regular_detail_t.mcode");
            tpRegCriteria.AddGroupBy("regular_detail_t.pay");
            tpRegCriteria.AddGroupBy("regular_detail_t.member_card_id");
            tpRegCriteria.AddGroupBy("regular_detail_t.mixed_group_code");
            tpRegCriteria.AddGroupBy("regular_detail_t.member_add_id");
            tpRegCriteria.AddGroupBy("regular_detail_t.next_date");
            tpRegCriteria.AddGroupBy("regular_detail_t.next_sendtime");
            tpRegCriteria.AddGroupBy("regular_detail_t.weekend_operation");
            tpRegCriteria.AddGroupBy("regular_detail_t.conv_code");
            tpRegCriteria.AddGroupBy("regular_detail_t.site_id");
            tpRegCriteria.AddGroupBy("member_card_t.card_mcode");



            tpRegCriteria.next_date_less_equal = executeDate.AddDays(ErsFactory.ersBatchFactory.getSetup().create_regular_order_days);
            tpRegCriteria.SetActiveOnly();
            tpRegCriteria.AddOrderBy("mcode", Criteria.OrderBy.ORDER_BY_ASC);

            return tpRegCriteria;
        }

        /// <summary>
        /// 定期明細取得用クライテリア条件セット
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private ErsRegularOrderCriteria SetCriteriaSelectDetailId(Dictionary<string, object> serchResulItem)
        {
            ErsRegularOrderCriteria tpRegCriteria = ErsFactory.ersOrderFactory.GetErsRegularOrderCriteria();

            tpRegCriteria.mcode = Convert.ToString(serchResulItem["mcode"]);

            tpRegCriteria.detail_pay = Convert.ToInt32(serchResulItem["pay"]);

            if (serchResulItem["member_card_id"] != null)
            {
                tpRegCriteria.member_card_id = Convert.ToInt32(serchResulItem["member_card_id"]);
            }
            else
            {
                tpRegCriteria.member_card_id = null;
            }

            if (serchResulItem["mixed_group_code"] != null)
            {
                tpRegCriteria.mixed_group_code = Convert.ToString(serchResulItem["mixed_group_code"]);
            }
            else
            {
                tpRegCriteria.mixed_group_code = null;
            }

            tpRegCriteria.member_add_id = Convert.ToInt32(serchResulItem["member_add_id"]);

            tpRegCriteria.next_date = Convert.ToDateTime(serchResulItem["next_date"]);

            tpRegCriteria.next_sendtime = Convert.ToInt32(serchResulItem["next_sendtime"]);

            tpRegCriteria.weekend_operation = (EnumWeekendOperation)Convert.ToInt32(serchResulItem["weekend_operation"]);

            if (serchResulItem["conv_code"] != null)
            {
                tpRegCriteria.conv_code = (EnumConvCode)Convert.ToInt32(serchResulItem["conv_code"]);
            }
            else
            {
                tpRegCriteria.conv_code = null;
            }

            tpRegCriteria.SetActiveOnly();
            tpRegCriteria.AddOrderBy("id", Criteria.OrderBy.ORDER_BY_ASC);

            return tpRegCriteria;
        }

    }
}
