﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.batch;

namespace jp.co.ivp.ers.batch.RegularOrderBilling
{
    public class RegularOrderBillingCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            //実行日の算出
            var regularExecuteDate = (options.ContainsKey("date")) ? Convert.ToDateTime(options["date"]) : executeDate ?? DateTime.Now;

            var exec = new RegularOrderBilling();
            exec.Execute(regularExecuteDate);

            string nowDate = regularExecuteDate.ToString("yyyy年MM月dd日");
            string exeName = System.IO.Path.GetFileName(System.Reflection.Assembly.GetCallingAssembly().Location);

            //結果の送信
            if (exec.issuedOrderNo.Count > 0)
            {
                this.SendIssuedResult(exec.issuedOrderNo, nowDate, exeName);
            }

            //GMOエラーがあった場合は、メール処理
            if (exec.gmoErrArr.Count != 0)
            {
                this.SendGmoError(exec.gmoErrArr.Count, nowDate, exeName);
            }

            //在庫エラーがあった場合は、メール処理
            if (exec.stockErrArr.Count != 0)
            {
                //管理者宛メール処理
                this.regBatchStockErrMailSend(exec.stockErrArr, nowDate, exeName);
            }

            //エラーが1件でもあれば
            if (exec.insertErrArr.Count != 0)
            {
                throw new Exception(string.Join(Environment.NewLine, exec.insertErrArr));
            }
        }

        private void SendIssuedResult(List<string> issuedOrderNo, string nowDate, string exeName)
        {
            var setUp = ErsFactory.ersBatchFactory.getSetup();

            var sbMail_body = new StringBuilder();
            sbMail_body.AppendLine("ご担当者様");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("{current_date}の定期伝票発行件数は{d_no_count}件です。");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("対象伝票番号は以下になります。");
            foreach (var d_no in issuedOrderNo)
            {
                sbMail_body.AppendLine(d_no);
            }
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("※本メールはメール自動配信システムから送信しております。");

            var values = new Dictionary<string, object>() { { "current_date", nowDate }, { "d_no_count", issuedOrderNo.Count } };

            var mail_body = StringExtension.Format(sbMail_body.ToString(), values);

            var mail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();
            mail.MailSend(
                setUp.regularOrderBillingResultMailTitle, mail_body,
                exeName,
                setUp.regularOrderBillingResultMailTo, setUp.regularOrderBillingResultMailFrom,
                setUp.regularOrderBillingResultMailCc, setUp.regularOrderBillingResultMailBcc);
        }

        private void SendGmoError(int errorCount, string nowDate, string exeName)
        {
            var setUp = ErsFactory.ersBatchFactory.getSetup();

            //管理者宛メール処理
            var sbMail_body = new StringBuilder();
            sbMail_body.AppendLine("ご担当者様");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("本日以下の件数のクレジットエラーが");
            sbMail_body.AppendLine("発生しておりますのでお手数ですがご対応を宜しくお願いいたします。");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("■{current_date} クレジットエラー発生");
            sbMail_body.AppendLine("{errorCount}件");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("※本メールはメール自動配信システムから送信しております。");

            var values = new Dictionary<string, object>() { { "current_date", nowDate }, { "errorCount", errorCount } };

            var mail_body = StringExtension.Format(sbMail_body.ToString(), values);

            var mail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();
            mail.MailSend(
                setUp.regularOrderBillingGmoErrMailTitle, mail_body,
                exeName,
                setUp.regularOrderBillingGmoErrMailTo, setUp.regularOrderBillingGmoErrMailFrom,
                setUp.regularOrderBillingGmoErrMailCc, setUp.regularOrderBillingGmoErrMailBcc);
        }

        /// <summary>
        /// 定期バッチで在庫エラーの際のメール送信
        /// </summary>
        /// <param name="mailTo"></param>
        /// <param name="mailFrom"></param>
        /// <param name="mailCc"></param>
        /// <param name="errCnt"></param>
        public virtual void regBatchStockErrMailSend(List<string> stockErrArr, string nowDate, string exeName)
        {
            var setUp = ErsFactory.ersBatchFactory.getSetup();

            var sbMail_body = new StringBuilder();
            sbMail_body.AppendLine("ご担当者様");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("本日以下の件数の在庫エラーが");
            sbMail_body.AppendLine("発生しておりますのでお手数ですがご対応を宜しくお願いいたします。");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("■{current_date} 在庫エラー発生");
            foreach (var err in stockErrArr)
            {
                sbMail_body.AppendLine(err);
            }
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("※本メールはメール自動配信システムから送信しております。");

            var values = new Dictionary<string, object>() { { "current_date", nowDate } };

            var mail_body = StringExtension.Format(sbMail_body.ToString(), values);

            var mail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();
            mail.MailSend(
                setUp.regularOrderBillingStockErrMailTitle, mail_body,
                exeName,
                setUp.regularOrderBillingStockErrMailTo, setUp.regularOrderBillingStockErrMailFrom,
                setUp.regularOrderBillingStockErrMailCc, setUp.regularOrderBillingStockErrMailBcc);
        }
    }
}
