﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.batch.DeleteExpiredPoint
{
    public class DeleteExpiredPointCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            var regularExecuteDate = (options.ContainsKey("date")) ? Convert.ToDateTime(options["date"]) : executeDate ?? DateTime.Now;
            this.DeleteExpiredPoint(regularExecuteDate);
        }


        /// <summary>
        /// Delete expired point from member point.
        /// </summary>
        internal void DeleteExpiredPoint(DateTime executeDate)
        {

            var setup = ErsFactory.ersBatchFactory.getSetup();
            int    site_id = setup.site_id;
            var sites = setup.ersSiteId;

            if (setup.Multiple_sites)
            {
                foreach (var site in sites)
                {
                    //基準日
                    var expirationDate = executeDate.AddMonths(-1 * setup.monthsExpirationPoint);

                    var repository = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryRepository();
                    var criteria = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryCriteria();

                    criteria.GetLatestRecord(int.Parse(site));
                    criteria.dt_to = expirationDate;
                    criteria.DontHaveRecentOrder(expirationDate);

                    var list = repository.Find(criteria);

                    foreach (var item in list)
                    {
                        var reason = ErsResources.GetFieldName("point_reason_expired_point");
                        ErsFactory.ersOrderFactory.GetUpdateMemberPointStgy().Decrease(item.mcode, item.total_p, reason, null, null,int.Parse(site));
                    }
                }
            }
            else
            {
                    //基準日
                    var expirationDate = executeDate.AddMonths(-1 * setup.monthsExpirationPoint);

                    var repository = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryRepository();
                    var criteria = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryCriteria();

                    criteria.GetLatestRecord();
                    criteria.dt_to = expirationDate;
                    criteria.DontHaveRecentOrder(expirationDate);

                    var list = repository.Find(criteria);

                    foreach (var item in list)
                    {
                        var reason = ErsResources.GetFieldName("point_reason_expired_point");
                        ErsFactory.ersOrderFactory.GetUpdateMemberPointStgy().Decrease(item.mcode, item.total_p, reason, null, null,site_id);
                    }
                }
            }
        }
    
}
