﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mall;

namespace jp.co.ivp.ers.batch.CreditCapturePayment
{
    public class CreditCapturePaymentCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            this.UpdateCreditCapturePayment();
        }


        /// <summary>
        /// update order_payment_status for the order.
        /// </summary>
        internal void UpdateCreditCapturePayment()
        {
            List<string> insertErrArr = new List<string>();

            //Search target orders.
            var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();

            var criteria = this.GetCriteria();

            var list = orderRepository.Find(criteria);

            foreach (var order in list)
            {
                try
                {
                    using (var tx = ErsDB_parent.BeginTransaction())
                    {
                        // update order payment status
                        var old_order = ErsFactory.ersOrderFactory.GetOrderWithD_no(order.d_no);
                        if (old_order == null)
                        {
                            throw new ErsException("30104", order.d_no);
                        }

                        order.order_payment_status = EnumOrderPaymentStatusType.PAID;
                        order.paid_date = DateTime.Now;
                        order.paid_price = order.total;

                        orderRepository.Update(old_order, order);

                        this.DoCapture(order);

                        tx.Commit();
                    }
                }
                catch (Exception ex)
                {
                    insertErrArr.Add(DateTime.Now + ", " + ex.ToString());
                }
            }

            //1件でもエラーがあれば
            if (insertErrArr.Count != 0)
            {
                throw new Exception(string.Join(",", insertErrArr));
            }
        }

        /// <summary>
        /// Send capture
        /// </summary>
        /// <param name="order"></param>
        protected virtual void DoCapture(ErsOrder order)
        {
            var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(order.pay);
            objPayment.SendCapture(order, order.total);
        }

        /// <summary>
        /// Get Criteria instance for search order.
        /// </summary>
        /// <param name="baseDate"></param>
        /// <returns></returns>
        protected virtual ErsOrderCriteria GetCriteria()
        {
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

            criteria.pay_in = new[] { EnumPaymentType.CREDIT_CARD, EnumPaymentType.PAYPAL };
            criteria.order_status_in = new[] { EnumOrderStatusType.DELIVERED };
            criteria.order_payment_status_in = new[] { EnumOrderPaymentStatusType.NOT_PAID };
            //ERS
            criteria.mall_shop_kbn = EnumMallShopKbn.ERS;

            criteria.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);
            return criteria;
        }
    }
}
