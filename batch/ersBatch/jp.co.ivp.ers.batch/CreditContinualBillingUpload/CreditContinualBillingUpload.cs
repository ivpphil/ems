﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using System.IO;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.batch.CreditContinualBillingUpload.model;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.batch.CreditContinualBillingUpload
{
    class CreditContinualBillingUpload
    {
        private string tempFilePath;
        private DateTime currentDate;

        private string csvExtension = ".txt";
        private string tarZipExtension = ".tar.gz";
        private string uploadOkFileExtension = ".ok";

        public CreditContinualBillingUpload()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            this.tempFilePath = setup.log_path + setup.creditContinualBillingUploadTempFilePath;
            ErsDirectory.CreateDirectories(this.tempFilePath);
        }

        internal void Execute(IDictionary<string, object> argDictinary)
        {
            this.currentDate = argDictinary.ContainsKey("date") ? Convert.ToDateTime(argDictinary["date"]) : DateTime.Now;

            //継続課金伝票のリストを取得
            var listOrder = this.GetOrderList();

            //リストがなかったら終了
            if (listOrder.Count() == 0)
            {
                return;
            }

            //CSVの生成
            var csvFileName = this.CreateCsv(listOrder);

            //TarZIPファイルの生成
            var tarZipFileName = this.CreateTarZip(csvFileName);

            using (var tx = ErsDB_parent.BeginTransaction())
            {
                // 継続課金送信済みに更新
                this.UpdateSentOrder(listOrder);

                //SFTPにて、既定のディレクトリへ転送（PUT）
                this.PutTarZip(tarZipFileName);

                tx.Commit();
            }

            //CSVファイル削除(TarZIPはログとして残す)
            this.DeleteCsv(csvFileName, tarZipFileName);
        }

        #region "継続課金伝票のリストを取得"
        /// <summary>
        /// 継続課金伝票のリストを取得
        /// </summary>
        /// <returns></returns>
        private IEnumerable<ErsOrder> GetOrderList()
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.intime_to = new DateTime(this.currentDate.Year, this.currentDate.Month, 1, 23, 59, 59).AddDays(-1);
            criteria.order_payment_status_in = new[] { EnumOrderPaymentStatusType.NO_AUTH };
            criteria.sent_continual_billing = EnumSentContinualBillingFlg.Waiting;
            criteria.HasDeliveredOrderRecord();
            return repository.Find(criteria);
        }
        #endregion

        #region "CSVの生成"
        /// <summary>
        /// CSVの生成
        /// </summary>
        /// <param name="listArticles"></param>
        /// <returns></returns>
        private string CreateCsv(IEnumerable<ErsOrder> listOrder)
        {
            //ファイル名の取得
            var fileName = this.GetCsvFileName();

            //CSVファイルをtempフォルダへ作成
            this.CreateCsvFile(fileName, listOrder);

            //パスを返却
            return fileName;
        }

        /// <summary>
        /// ファイル名の取得
        /// </summary>
        /// <returns></returns>
        private string GetCsvFileName()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            var baseFileName = "uri" + setup.gmo_shop_id + currentDate.ToString("yyyyMM");

            //ファイル名の下二桁(月内連番)取得
            var sequence = this.GetFileNameSequence(baseFileName);

            //作成するCSVファイルは、その日の1回目、2回目でファイル名を変える
            return baseFileName + VBStrings.Right("0" + sequence, 2) + this.csvExtension;
        }

        /// <summary>
        /// ファイル名の下二桁(月内連番)取得
        /// </summary>
        /// <returns></returns>
        private int GetFileNameSequence(string baseFileName)
        {
            var files = System.IO.Directory.GetFiles(tempFilePath, baseFileName + "??" + tarZipExtension);
            var sequence = 0;
            foreach (var file in files)
            {
                var tempSequence = Convert.ToInt32(System.IO.Path.GetFileName(file).Replace(tarZipExtension, string.Empty).Replace(baseFileName, string.Empty));
                if (tempSequence > sequence)
                {
                    sequence = tempSequence;
                }
            }
            return sequence + 1;
        }

        /// <summary>
        /// CSVファイルをtempフォルダへ作成
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="listMember"></param>
        private void CreateCsvFile(string fileName, IEnumerable<ErsOrder> listOrder)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            using (var writer = csvCreater.GetWriter(tempFilePath, fileName))
            {
                //this.WriteCsvHeader<CsvUploadRecord>(writer, value => SynergyCsvFormatter.Format(value));

                var process_sequence = 0;
                foreach (var order in listOrder)
                {
                    process_sequence++;
                    if (process_sequence > 99999)
                    {
                        process_sequence = 1;
                    }

                    var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                    var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
                    memberCardCriteria.id = order.member_card_id;
                    var memberCard = memberCardRepository.FindSingle(memberCardCriteria);

                    var csvModel = new CsvUploadRecord();

                    csvModel.gmo_shop_id = setup.gmo_shop_id;
                    csvModel.card_mcode = memberCard.card_mcode;
                    csvModel.card_sequence = memberCard.card_sequence;
                    csvModel.transaction_code = 0; // 0:売上
                    csvModel.intime = order.intime.Value.ToString("yyyyMMdd");
                    csvModel.credit_order_id = order.credit_order_id;
                    csvModel.total = order.total;
                    csvModel.pay = 1; //1:一括
                    csvModel.process_sequence = string.Format("{0:D5}", process_sequence);
                    csvModel.remarks = order.site_id.ToString();

                    csvCreater.WriteBody(csvModel, writer);
                }
            }
        }

        #endregion

        #region "TarZIPファイルの生成"
        /// <summary>
        /// TarZIPファイルの生成
        /// </summary>
        /// <param name="csvFileName"></param>
        /// <returns></returns>
        private string CreateTarZip(string csvFileName)
        {
            //ZIPファイル保存先取得
            var zipFileName = this.GetZipFileName(csvFileName);

            //圧縮ファイル作成
            var zipFileManager = ErsFactory.ersUtilityFactory.GetTarZipFileManager();
            zipFileManager.Compress(tempFilePath + csvFileName, tempFilePath + zipFileName);

            //Put完了ファイルを作成
            this.CreateUploadOkFile(zipFileName);

            //パスを返却
            return zipFileName;
        }

        /// <summary>
        /// ZIPファイル保存先取得
        /// </summary>
        /// <returns></returns>
        private string GetZipFileName(string csvFileName)
        {
            return System.IO.Path.GetFileNameWithoutExtension(csvFileName) + tarZipExtension;
        }

        /// <summary>
        /// Put完了ファイルを作成
        /// </summary>
        /// <param name="fileName"></param>
        private void CreateUploadOkFile(string zipFileName)
        {
            using (var hStream = System.IO.File.Create(tempFilePath + zipFileName + uploadOkFileExtension))
            {
                // 作成時に返される FileStream を利用して閉じる
                if (hStream != null)
                {
                    hStream.Close();
                }
            }
        }
        #endregion

        #region　"継続課金送信済みに更新"
        private void UpdateSentOrder(IEnumerable<ErsOrder> listOrder)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();

            // トランザクションは呼び出し元で開始
            foreach (var order in listOrder)
            {
                var oldOrder = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(order.GetPropertiesAsDictionary());
                order.sent_continual_billing = EnumSentContinualBillingFlg.Sent;

                repository.Update(oldOrder, order);
            }
        }
        #endregion

        #region "SFTPにて、既定のディレクトリへ転送（PUT）"
        /// <summary>
        /// SFTPにて、既定のディレクトリへ転送（PUT）
        /// </summary>
        /// <param name="csvFileName"></param>
        private void PutTarZip(string tarZipFileName)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            string upTempPath = null;
            if (setup.creditContinualBillingUploadSftpSshKeyPath.HasValue())
            {
                upTempPath = setup.root_path + setup.creditContinualBillingUploadSftpSshKeyPath;
            }

            using (var sftp = SFTPClient.Connect(
                setup.creditContinualBillingUploadSftpHost,
                setup.creditContinualBillingUploadSftpUser,
                setup.creditContinualBillingUploadSftpPass,
                setup.creditContinualBillingUploadSftpPort,
                upTempPath,
                setup.creditContinualBillingUploadSftpSshPassPhrase))
            {
                var sourcePath = this.tempFilePath + tarZipFileName;
                var putPath = setup.creditContinualBillingUploadSftpUserSftpPutPath + tarZipFileName;

                //TarZipをput
                sftp.PutFile(sourcePath, putPath);

                //サーバーにアップ完了フラグファイルのアップ
                sftp.PutFile(sourcePath + uploadOkFileExtension, putPath + uploadOkFileExtension);

                sftp.Close();
            }
        }
        #endregion

        #region "Delete csv"
        /// <summary>
        /// CSVファイル削除
        /// </summary>
        /// <param name="csvFileName"></param>
        private void DeleteCsv(string csvFileName, string tarZipFileName)
        {
            File.Delete(this.tempFilePath + csvFileName);
            File.Delete(this.tempFilePath + tarZipFileName + uploadOkFileExtension);
        }
        #endregion
    }
}
