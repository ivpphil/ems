﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch.CreditContinualBillingUpload.model
{
    public class CsvUploadRecord : ErsBindableModel
    {
        /// <summary>
        /// ショップID
        /// </summary>
        [CsvField]
        public virtual string gmo_shop_id { get; set; }

        /// <summary>
        /// 会員ID
        /// </summary>
        [CsvField]
        public virtual string card_mcode { get; set; }

        /// <summary>
        /// カード登録連番
        /// </summary>
        [CsvField]
        public virtual string card_sequence { get; set; }

        /// <summary>
        /// 取引コード
        /// </summary>
        [CsvField]
        public virtual int? transaction_code { get; set; }

        /// <summary>
        /// 利用年月日
        /// </summary>
        [CsvField]
        public virtual string intime { get; set; }

        /// <summary>
        /// オーダーID
        /// </summary>
        [CsvField]
        public virtual string credit_order_id { get; set; }

        /// <summary>
        /// 商品コード
        /// </summary>
        [CsvField]
        public virtual string scode { get; set; }

        /// <summary>
        /// 利用金額
        /// </summary>
        [CsvField]
        public virtual int? total { get; set; }

        /// <summary>
        /// 税送料
        /// </summary>
        [CsvField]
        public virtual string tax { get; set; }

        /// <summary>
        /// 支払方法
        /// </summary>
        [CsvField]
        public virtual int pay { get; set; }

        /// <summary>
        /// 支払回数
        /// </summary>
        [CsvField]
        public virtual string pay_times { get; set; }

        /// <summary>
        /// ボーナス回数
        /// </summary>
        [CsvField]
        public virtual string bonus_times { get; set; }

        /// <summary>
        /// ボーナス金額
        /// </summary>
        [CsvField]
        public virtual string bonus_total { get; set; }

        /// <summary>
        /// 端末処理通番
        /// </summary>
        [CsvField]
        public virtual string process_sequence { get; set; }

        /// <summary>
        /// 加盟店自由項目
        /// </summary>
        [CsvField]
        public virtual string remarks { get; set; }

        /// <summary>
        /// 処理番号
        /// </summary>
        [CsvField]
        public virtual string process_id { get; set; }

        /// <summary>
        /// 処理結果
        /// </summary>
        [CsvField]
        public virtual string process_result { get; set; }

        /// <summary>
        /// 仕向先コード
        /// </summary>
        [CsvField]
        public virtual string card_code { get; set; }

        /// <summary>
        /// オーソリ結果
        /// </summary>
        [CsvField]
        public virtual string authory_result { get; set; }
    }
}
