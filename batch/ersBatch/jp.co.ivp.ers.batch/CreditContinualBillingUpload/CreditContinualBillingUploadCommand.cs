﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.batch.CreditContinualBillingUpload
{
    public class CreditContinualBillingUploadCommand
          : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> argDictionary, DateTime? lastDate, string batchLocation)
        {
            CreditContinualBillingUpload exec = new CreditContinualBillingUpload();

            exec.Execute(argDictionary);
        }
    }
}
