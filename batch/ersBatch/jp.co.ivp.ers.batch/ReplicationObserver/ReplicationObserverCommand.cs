﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.model;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.batch.ReplicationObserver
{
    public class ReplicationObserverCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            this.Execute();
        }

        public void Execute()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var modelSettings = new ReplicationObserverSettingsModel();

            // Get settings
            ErsXmlModelBinder.Bind(string.Format("{0}setup\\batch\\replication_observer.config", setup.root_path), modelSettings);

            var listErrorLog = new ConcurrentQueue<string>();

            // Execute replication
            Parallel.ForEach(modelSettings.replications, replication =>
            {
                try
                {
                    this.ExecuteReplication(replication);
                }
                catch (ReplicationObserverException e)
                {
                    listErrorLog.Enqueue(e.Message);
                }
                catch (Exception e)
                {
                    listErrorLog.Enqueue(e.ToString());
                }
            });

            if (listErrorLog.Count > 0)
            {
                throw new Exception(String.Join("\r\n", listErrorLog));
            }
        }

        /// <summary>
        /// ExecuteReplication
        /// </summary>
        /// <param name="replication"></param>
        protected void ExecuteReplication(ReplicationSettings replication)
        {
            // Get master
            var objDB = new ErsDB_parent(replication.update_table_name, ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(replication.connection_string_master));

            var criteria = new Criteria();
            criteria.Add(Criteria.GetCriterion(string.Format("{0}.{1}", replication.update_table_name, replication.update_primary_key_name), replication.update_primary_key, Criteria.Operation.EQUAL));

            // Select from master
            var listDic = objDB.gSelect(replication.update_column_names, criteria);

            if (listDic.Count != 1)
            {
                throw new ReplicationObserverException(string.Format("[{0}]\r\n更新対象が見付かりませんでした。（マスター） {1} = \"{2}\"",
                                                            replication.connection_name_master,
                                                            replication.update_primary_key_name,
                                                            replication.update_primary_key));
            }

            var dicMaster = listDic.First();
            var dicUpdate = new Dictionary<string, object>(dicMaster);

            this.SetUpdateData(dicMaster, dicUpdate);

            // Update master
            objDB.gUpdateColumn(replication.update_column_names, dicUpdate, criteria);

            // Wait
            for (var i = 0; i < replication.update_wait_seconds; i++)
            {
                Console.WriteLine("Waiting for replication...{0}", i);
                System.Threading.Thread.Sleep(1000);
            }

            // Re-Select after update
            dicUpdate = objDB.gSelect(replication.update_column_names, criteria).First();

            var listMessage = new List<string>();

            foreach (var connection_setting in replication.connection_slave_settings)
            {
                try
                {
                    // Get slave
                    var objDBSlave = new ErsDB_parent(replication.update_table_name, ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(connection_setting.connection_string));

                    // Select from slave
                    var listDicSlave = objDBSlave.gSelect(replication.update_column_names, criteria);

                    if (listDicSlave.Count != 1)
                    {
                        listMessage.Add(string.Format("[{0}]\r\n更新対象が見付かりませんでした。（スレーブ） {1} = \"{2}\"",
                                            connection_setting.connection_name,
                                            replication.update_primary_key_name,
                                            replication.update_primary_key));
                        continue;
                    }

                    var dicSlave = listDicSlave.First();

                    var listDiffMessage = new List<string>();

                    // Check Diff
                    foreach (var data in dicUpdate)
                    {
                        if (!dicSlave.ContainsKey(data.Key))
                        {
                            listDiffMessage.Add(string.Format("カラムが見付かりませんでした。 \"{0}\"", data.Key));
                            continue;
                        }

                        if (this.IsDiffData(data.Value, dicSlave[data.Key]))
                        {
                            listDiffMessage.Add(string.Format("内容に違いがあります。 \"{0}\" : {1} <> {2}", data.Key, data.Value, dicSlave[data.Key]));
                        }
                    }

                    if (listDiffMessage.Count() > 0)
                    {
                        listMessage.Add(string.Format("[{0}]\r\n{1}\r\n",
                                            connection_setting.connection_name,
                                            String.Join("\r\n", listDiffMessage)));
                    }
                }
                catch (Exception e)
                {
                    listMessage.Add(string.Format("[{0}]\r\n{1}\r\n",
                                        connection_setting.connection_name,
                                        e.ToString()));
                }
            }

            if (listMessage.Count() > 0)
            {
                throw new ReplicationObserverException(String.Join("\r\n", listMessage));
            }
        }

        /// <summary>
        /// SetUpdateData
        /// </summary>
        /// <param name="dicMaster"></param>
        /// <param name="dicUpdate"></param>
        protected void SetUpdateData(Dictionary<string, object> dicMaster, Dictionary<string, object> dicUpdate)
        {
            foreach (var data in dicMaster)
            {
                if (data.Value is DateTime)
                {
                    dicUpdate[data.Key] = DateTime.Now;
                }
                else if (data.Value == null)
                {
                    dicUpdate[data.Key] = 0;
                }
                else if (string.IsNullOrEmpty(data.Value.ToString()))
                {
                    dicUpdate[data.Key] = 0;
                }
                else
                {
                    dicUpdate[data.Key] = 0x01 & ~Convert.ToInt32(data.Value.ToString());
                }
            }
        }

        /// <summary>
        /// IsDiffData
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        protected bool IsDiffData(object obj1, object obj2)
        {
            if (obj1 is DateTime)
            {
                if (((DateTime)obj1).ToString("yyyy/MM/dd HH:mm:ss") != ((DateTime)obj2).ToString("yyyy/MM/dd HH:mm:ss"))
                {
                    return true;
                }
            }
            else if (obj1.ToString() != obj2.ToString())
            {
                return true;
            }

            return false;
        }
    }
}
