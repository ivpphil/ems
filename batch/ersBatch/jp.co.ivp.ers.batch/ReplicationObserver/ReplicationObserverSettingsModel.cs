﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;

namespace jp.co.ivp.ers.batch.ReplicationObserver
{
    /// <summary>
    /// レプリケーション監視設定モデル
    /// </summary>
    class ReplicationObserverSettingsModel
        : ErsBindableModel
    {
        /// <summary>
        /// レプリケーション設定
        /// </summary>
        [XmlField]
        [BindTable("replications")]
        public List<ReplicationSettings> replications { get; set; }
    }

    /// <summary>
    /// レプリケーション設定
    /// </summary>
    public class ReplicationSettings
        : ErsBindableModel
    {
        /// <summary>
        /// コネクション名（マスター）
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string connection_name_master { get; set; }

        /// <summary>
        /// コネクションストリング（マスター）
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.ConnectionString)]
        public virtual string connection_string_master { get; set; }

        /// <summary>
        /// コネクションストリング（スレーブ）設定
        /// </summary>
        [XmlField]
        [BindTable("connection_slave_settings")]
        public virtual List<ConnectionSlaveSettings> connection_slave_settings { get; set; }

        /// <summary>
        /// 更新テーブル名
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string update_table_name { get; set; }

        /// <summary>
        /// 更新カラム名
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All, isArray = true)]
        public virtual string[] update_column_names { get; set; }

        /// <summary>
        /// 更新主キー名
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string update_primary_key_name { get; set; }

        /// <summary>
        /// 更新主キー
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string update_primary_key { get; set; }

        /// <summary>
        /// 更新待ち時間（秒）
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int update_wait_seconds { get; set; }
    }

    /// <summary>
    /// コネクション（スレーブ）設定
    /// </summary>
    public class ConnectionSlaveSettings
        : ErsBindableModel
    {
        /// <summary>
        /// コネクション名
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string connection_name { get; set; }

        /// <summary>
        /// コネクションストリング
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.ConnectionString)]
        public virtual string connection_string { get; set; }
    }
}
