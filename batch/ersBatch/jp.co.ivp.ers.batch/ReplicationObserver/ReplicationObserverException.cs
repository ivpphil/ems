﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch.ReplicationObserver
{
    public class ReplicationObserverException
        : Exception
    {
        public ReplicationObserverException(string errorMessage)
            : base(errorMessage)
        {
            this.errorMessage = errorMessage;
        }

        public string errorMessage { get; set; }
    }
}
