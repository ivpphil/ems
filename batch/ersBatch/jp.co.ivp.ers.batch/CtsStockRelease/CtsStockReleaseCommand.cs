﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall;

namespace jp.co.ivp.ers.batch.CtsStockRelease
{
    public class CtsStockReleaseCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            this.ExecuteReleaseStockCts();
        }

        internal void ExecuteReleaseStockCts()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var spec = ErsFactory.ersCtsOrderFactory.GetSearchCtsStockReleaseSpec();
            var repository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            var criteria = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderCriteria();

            criteria.stock_reservation = EnumStockReservation.Reserve;
            criteria.SetCoalesceIntimeAndUtime(DateTime.Now.AddMinutes(-setup.stock_reservation_period));

            //Stock Reserved List
            var ctsOrderList = spec.GetSearchData(criteria);

            var listParam = new List<UpdateStockParam>();
            foreach (var cts_order_record in ctsOrderList)
            {
                using (var transasction = ErsDB_parent.BeginTransaction())
                {
                    //Obtain current bask record
                    var old_bask = ErsFactory.ersBasketFactory.GetErsBaskRecordWithId(Convert.ToInt32(cts_order_record["bask_id"]));
                    var new_bask = ErsFactory.ersBasketFactory.GetErsBaskRecordWithParameter(old_bask.GetPropertiesAsDictionary());

                    //Release reserved stock from ds_master_t record
                    ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetIncreaseStockStgy().Increase(old_bask.scode, old_bask.amount);

                    //Release reserved stock from bask_t record
                    new_bask.stock_reservation = EnumStockReservation.NonReserve;
                    repository.Update(old_bask, new_bask);

                    if (new_bask.h_mall_flg == EnumOnOff.On)
                    {
                        UpdateStockParam param = default(UpdateStockParam);

                        param.productCode = old_bask.scode;
                        param.quantity = old_bask.amount;
                        param.operation = EnumMallStockOperation.add;

                        listParam.Add(param);
                    }

                    transasction.Commit();
                }
            }

            // モール在庫更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }
        }
    }
}
