﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.batch.ConvertProductKeyword
{
    public class ConvertProductKeywordCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            var keywordConstracter = ErsFactory.ersMerchandiseFactory.GetProductKeywordConstracterStgy();
            
            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();

            // keywordがまだコンバートされてなくて(null)、keywords_tにデータがあるg_master_tのみ更新
            groupCriteria.keyword = null;
            groupCriteria.Add(Criteria.GetUniversalCriterion("EXISTS(SELECT 1 FROM keywords_relation_t INNER JOIN keywords_t ON keywords_t.id = keywords_relation_t.keyword_id WHERE keywords_relation_t.gcode = g_master_t.gcode AND keyword_type = 0)"));
            var listGroup = groupRepository.FindBy(groupCriteria);

            foreach (var newGroup in listGroup)
            {
                var oldGroup = ErsFactory.ersMerchandiseFactory.GetErsGroupWithParameter(newGroup.GetPropertiesAsDictionary());

                // キーワードをセットする。
                // disp_keyword = keyword入力値
                // keyword = 検索用
                newGroup.disp_keyword = this.GetKeyword(newGroup);
                newGroup.keyword = ErsKanaConverter.String_conversion(keywordConstracter.CreateGroupKeyword(newGroup));
                groupRepository.Update(oldGroup, newGroup);
            }

            // 商品明細のkeyword (フロントでの検索用)
            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var skuCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            //keywordがまだコンバートされてない(null)
            skuCriteria.keyword = null;
            var listSku = skuRepository.FindBy(skuCriteria);

            foreach (var objSku in listSku)
            {
                var oldSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithParameter(objSku.GetPropertiesAsDictionary());
                objSku.keyword = ErsKanaConverter.String_conversion(keywordConstracter.CreateSkuKeyword(objSku));
                skuRepository.Update(oldSku, objSku);
            }
        }

        /// <summary>
        /// グループのdisp_keywordを取得する
        /// </summary>
        /// <param name="objMappable"></param>
        private string[] GetKeyword(ErsGroup objGroup)
        {
            var keywordRepository = ErsFactory.ersMerchandiseFactory.GetErsKeywordsRepository();
            var keywordRelationCriteria = ErsFactory.ersMerchandiseFactory.GetErsKeywordsRelationCriteria();
            keywordRelationCriteria.gcode = objGroup.gcode;
            keywordRelationCriteria.keyword_type = EnumKeywordType.Group;
            keywordRelationCriteria.active = EnumActive.Active;
            keywordRelationCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            // 商品グループ名と商品名は表示用キーワードには含めない
            keywordRelationCriteria.Add(Criteria.GetCriterion("disp_keyword", objGroup.gcode, Criteria.Operation.NOT_EQUAL));
            keywordRelationCriteria.Add(Criteria.GetCriterion("disp_keyword", objGroup.gname, Criteria.Operation.NOT_EQUAL));
            var listKeyword = keywordRepository.FindKeyword(keywordRelationCriteria);
            if (listKeyword.Count == 0)
            {
                return null;
            }

            return listKeyword.Select((objKeywords) => objKeywords.disp_keyword).ToArray();
        }
    }
}
