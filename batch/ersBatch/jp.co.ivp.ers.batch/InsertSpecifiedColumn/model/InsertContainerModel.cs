﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch.InsertSpecifiedColumn.model
{
    public class InsertContainerModel
        : ErsBindableModel
    {
        public virtual List<string> updatableSchemaList { get; set; }

        public virtual List<string> parameterKey { get; set; }

        public string tableName { get; set; }
    }
}
