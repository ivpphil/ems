﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.Collections.Concurrent;
using System.IO;
using jp.co.ivp.ers.batch.InsertSpecifiedColumn.model;
using jp.co.ivp.ers.update_specified_column;
using jp.co.ivp.ers.db;
using System.Data.Common;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.batch.InsertSpecifiedColumn
{
    public class InsertSpecifiedColumnCommand
        : IErsBatchCommand
    {
        Setup setup;
        private const string backupPrefixExt = "-backup-";
        private const string errorPrefixExt = "-error-";
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            setup = ErsFactory.ersUtilityFactory.getSetup();

            //Execute Process
            this.Execute();
        }

        protected virtual void Execute()
        {
            ErsDirectory.CreateDirectories(setup.insertTableFilePath);
            ErsDirectory.CreateDirectories(setup.insertTableFileBackupPath);

            var uploadedFileList = Directory.EnumerateFiles(setup.insertTableFilePath, "*.csv");

            //-- TODO: Temporary removed ==>>> Parallel Lambda Expression
            //foreach (string filePath in uploadedFileList)
            //{
            //    BindParallelOpenedFile(filePath);
            //}


            // Parallel Lambda Expression
            Parallel.ForEach(uploadedFileList.OrderByDescending(d => d), filePath =>
            {
                BindParallelOpenedFile(filePath);
            });
        }

        private void BindParallelOpenedFile(string filePath)
        {
            var exceptionsList = new ConcurrentQueue<Exception>();

            try
            {
                //Set Locked Current File
                using (var fileStream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    string backupFileName = Path.Combine(setup.insertTableFileBackupPath, Path.GetFileNameWithoutExtension(filePath) + backupPrefixExt);
                    var backupFilePath = ErsFile.WriteAllWithRandomName(backupFileName, "csv", fileStream, ErsEncoding.ShiftJIS);

                    fileStream.Position = 0;
                    this.BindAndInsertCsvSchema(filePath, fileStream, exceptionsList);
                }
            }
            catch (IOException ioEx)
            {
                //Identify if file is locked
                if (ErsFile.IsFileLocked(ioEx))
                    return;

                var exMessage = ioEx.Message;

                exMessage = exMessage.Replace(filePath, Path.GetFileName(filePath));
                exceptionsList.Enqueue(new Exception(exMessage));
            }
            catch (Exception ex)
            {
                var exMessage = ex.Message;

                exMessage = exMessage.Replace(filePath, Path.GetFileName(filePath));
                exceptionsList.Enqueue(new Exception(exMessage));
            }
            finally
            {
                //Delete Finished file
                this.DeleteFile(filePath, exceptionsList);

                var errorFilePath = this.SaveErrorLogs(filePath, exceptionsList);
                this.SendAlertMail(Path.GetFileName(filePath), Path.GetFileName(errorFilePath), exceptionsList);
            }
        }

        //Bind Current File
        protected virtual void BindAndInsertCsvSchema(string filePath, Stream stream, ConcurrentQueue<Exception> exceptionsList)
        {
            var copyInCmd = new ErsDatabaseCopyInCommand();

            var csvCreater = ErsFactory.ersUtilityFactory.GetErsMemoryCsvCreater();

            try
            {
                var csvFileLoader = ErsFactory.ersUtilityFactory.GetErsCsvSchemaContainer<InsertContainerModel>();
                csvFileLoader.LoadPostedFile(filePath, stream);

                if (csvFileLoader.validIndexes.Count() > 0)
                {
                    var validSchemaList = csvFileLoader.GetValidatedSchema();

                    string sqlCommandText = "COPY " + csvFileLoader.tableName + "(" + String.Join(",", this.GetInsertableColumns(csvFileLoader.tableName, csvFileLoader.updatableSchemaList)) + ") FROM STDIN DELIMITER AS '\t' NULL AS '\\N' CSV HEADER";

                    using (var memoryStream = new MemoryStream())
                    {
                        //Start writing data in stream
                        using (var writer = csvCreater.GetWriter(memoryStream))
                        {
                            foreach (var schemaDictionary in validSchemaList)
                            {
                                var containerModel = (InsertContainerModel)schemaDictionary["containerModel"];

                                if (containerModel.IsValid)
                                {
                                    this.ExecuteInsert(containerModel, schemaDictionary, csvFileLoader.tableName, exceptionsList, csvCreater, writer);
                                    continue;
                                }

                                csvFileLoader.MarkRecordAsInvalid(schemaDictionary);
                                exceptionsList.Enqueue(new Exception(String.Join(Environment.NewLine, containerModel.GetAllErrorMessageList())));
                            }

                            //Flush after write all data
                            writer.Flush();

                            //Start copy from stream
                            copyInCmd.CopyStartFromStream(sqlCommandText, memoryStream);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                var message = copyInCmd.GetNpgsqlExceptionMessage(ex);
                exceptionsList.Enqueue(new Exception(message));
            }

        }

        protected void ExecuteInsert(InsertContainerModel containerModel, Dictionary<string, object> objDic, 
            string tableName, ConcurrentQueue<Exception> exceptionsList,
            ErsMemoryCsvCreater csvCreater, StreamWriter writer)
        {
            try
            {
                var repository = ErsFactory.ersUpdateSpecifiedColumnFactory.GetErsUpdateSpecifiedColumnRepository(tableName);
                var entity = ErsFactory.ersUpdateSpecifiedColumnFactory.GetErsUpdateSpecifiedColumnWithParameters(objDic);

                var insertable_entity = entity.GetConcreteValuePropertiesAsDictionary(repository.GetTableSchema());
                var entity_type = entity.GetPropertiesTypeAsDictionary(repository.GetTableSchema());


                var concreteDic = insertable_entity.Select((dic) => new { dic.Key, dic.Value }).ToDictionary(dic => dic.Key, dic => ErsCopySerializer.GetConcreteValue(entity_type[dic.Key], dic.Value, false));
                csvCreater.WriteBody(concreteDic, writer);

            }
            catch (Exception ex)
            {
                string exMessage = string.Empty;

                if (ex.InnerException != null)
                    exMessage = ex.InnerException.Message;

                if (!exMessage.HasValue())
                    exMessage = ex.Message;

                containerModel.AddInvalidField(string.Empty, exMessage);
                exceptionsList.Enqueue(new Exception(String.Join(Environment.NewLine, containerModel.GetAllErrorMessageList())));
            }
        }

        //Delete Current File
        private void DeleteFile(string filePath, ConcurrentQueue<Exception> exceptionsList)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                exceptionsList.Enqueue(new Exception(ex.Message));
            }
        }

        //Save Current File
        public string SaveErrorLogs(string originPath, ConcurrentQueue<Exception> exceptionsList)
        {
            if (this.HasError(exceptionsList))
            {
                var errorMessageList = exceptionsList.Select((err) => err.Message.ToString());
                var errorLogs = String.Join(Environment.NewLine, errorMessageList);

                string pathFileName = Path.Combine(setup.insertTableFileErrorPath, Path.GetFileNameWithoutExtension(originPath) + errorPrefixExt);

                ErsDirectory.CreateDirectories(setup.insertTableFileErrorPath);
                return ErsFile.WriteAllWithRandomName(pathFileName, "csv", errorLogs);
            }

            return null;
        }


        //Send mail after current file has done
        public void SendAlertMail(string updatedFileName, string errorFileName, ConcurrentQueue<Exception> exceptionsList)
        {
            if (!this.HasError(exceptionsList))
                errorFileName = string.Empty;

            var mailModel = new InsertSpecifiedColumnMail(updatedFileName, errorFileName);
            mailModel.SendMail(mailModel.MailBody());
        }

        protected virtual bool HasError(ConcurrentQueue<Exception> exceptionsList)
        {
            return (exceptionsList != null && exceptionsList.Count > 0);
        }

        protected List<string> GetInsertableColumns(string tableName, IEnumerable<string> updatableSchemaList)
        {
            var list = new List<string>();
            var repository = ErsFactory.ersUpdateSpecifiedColumnFactory.GetErsUpdateSpecifiedColumnRepository(tableName);

            foreach (System.Data.DataColumn column in repository.GetTableSchema())
            {
                if (updatableSchemaList.Contains(column.ColumnName))
                    list.Add(column.ColumnName);
            }

            return list;
        }
    }
}
