﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.batch.MonitorMassSendPrepare
{
    public class MonitorMassSendPrepareCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            using (var tx = ErsDB_parent.BeginTransaction())
            {
                this.RegistInstructionToSend(executeDate);
                tx.Commit();
            }
        }

        /// <summary>
        /// Regist Instruction To Send 
        /// </summary>
        /// <param name="executeDate"></param>
        private void RegistInstructionToSend(DateTime? executeDate)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //Update Process
            var processRepository = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var processCriteria = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();
            processCriteria.id = setup.monitorMassSendProcessID;
            var listProcess = processRepository.Find(processCriteria);
            if (listProcess.Count == 0)
            {
                this.InsertNewProcess(executeDate);
            }
            else
            {
                this.UpdateProcess(listProcess.First(), executeDate);
            }

            //Update MailTo
            var mailToRepositroy = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var mailtoCriteria = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            mailtoCriteria.process_id = setup.monitorMassSendProcessID;
            var listMailTo = mailToRepositroy.Find(mailtoCriteria);
            if (listMailTo.Count == 0)
            {
                this.InsertNewMailTo();
            }
            else
            {
                this.UpdateMailTo(listMailTo.First());
            }
        }

        /// <summary>
        /// Update Instraction for monitoring
        /// </summary>
        /// <param name="oldProcess"></param>
        /// <param name="executeDate"></param>
        private void UpdateProcess(ErsProcess oldProcess, DateTime? executeDate)
        {
            var processRepository = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var newProcess = ErsFactory.ErsAtMailFactory.GetErsProcessWithParameters(oldProcess.GetPropertiesAsDictionary());
            this.ResetProcessValues(newProcess, executeDate);
            processRepository.Update(oldProcess, newProcess);
        }

        /// <summary>
        /// Insert new Instrasction for monitoring
        /// </summary>
        /// <param name="executeDate"></param>
        private void InsertNewProcess(DateTime? executeDate)
        {
            var processRepository = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var objProcess = ErsFactory.ErsAtMailFactory.GetErsProcess();
            this.ResetProcessValues(objProcess, executeDate);
            processRepository.Insert(objProcess);
        }

        /// <summary>
        /// Reset values of process
        /// </summary>
        /// <param name="objProcess"></param>
        /// <param name="executeDate"></param>
        private void ResetProcessValues(ErsProcess objProcess, DateTime? executeDate)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var messageDateTimeText = string.Format("{0:yyyy/MM/dd HH:mm:dd}", executeDate);
            var messageBodyText = string.Format("@メール監視[{0}]", messageDateTimeText);

            objProcess.id = setup.monitorMassSendProcessID;
            objProcess.up_kind = EnumUpKind.MONITOR;
            objProcess.status = EnumAmProcessStatus.NotSend;
            objProcess.subject = messageDateTimeText;
            objProcess.body = messageBodyText;
            objProcess.html_body = messageBodyText;
            objProcess.feature_body = messageBodyText;
            objProcess.scheduled_date = executeDate;
            objProcess.sent_date = null;
            objProcess.active = EnumActive.Active;
        }

        /// <summary>
        /// Update Mailto
        /// </summary>
        private void UpdateMailTo(ErsMailTo oldMailTo)
        {
            var mailToRepositroy = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();

            var newMailTo = ErsFactory.ErsAtMailFactory.GetErsMailToWithParameters(oldMailTo.GetPropertiesAsDictionary());
            this.ResetMailToValues(newMailTo);
            mailToRepositroy.Update(oldMailTo, newMailTo);
        }

        private void InsertNewMailTo()
        {
            var mailToRepositroy = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();

            var objMailTo = ErsFactory.ErsAtMailFactory.GetErsMailTo();
            this.ResetMailToValues(objMailTo);
            mailToRepositroy.Insert(objMailTo);
        }

        /// <summary>
        /// Reset values of mailto
        /// </summary>
        /// <param name="objMailTo"></param>
        private void ResetMailToValues(ErsMailTo objMailTo)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var memberRepository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var memberCriteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            memberCriteria.lname = setup.monitorSecondLname;
            memberCriteria.fname = setup.monitorSecondFname;
            var listMember = memberRepository.Find(memberCriteria);
            if(listMember.Count == 0)
            {
                throw new Exception("A member for monitorSecondLname is not registerd in member_t.");
            }
            var objMember = listMember.First();

            objMailTo.process_id = setup.monitorMassSendProcessID;
            objMailTo.mcode = objMember.mcode;
            objMailTo.email = setup.monitorMassSendMailAddress;
            objMailTo.lname = objMember.lname;
            objMailTo.fname = objMember.fname;
            objMailTo.mformat = EnumMformat.PC;
            objMailTo.sent_flg = EnumSentFlg.NotSent;
            objMailTo.scheduled_date = null;
            objMailTo.active = EnumActive.Active;
        }

    }
}
