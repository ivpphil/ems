﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.batch.ProductRanking
{
    public class ProductRankingCommand
        : IErsBatchCommand
    {
        public void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation)
        {
            this.InsertProductRanking(executeDate);
        }


        internal void InsertProductRanking(DateTime? executeDate)
        {
            using (var transaction = ErsDB_parent.BeginTransaction())
            {
                //Update All Old Product Ranking into NonAcive
                ErsFactory.ersRankingFactory.GetUpdateProductRankingStgy().NonActive(EnumActive.Active, EnumRankType.Daily);


                //Update Insert new Product Ranking
                var setup = ErsFactory.ersBatchFactory.getSetup();
                var spec = ErsFactory.ersOrderFactory.GetProductRankingSpec();
                var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

                criteria.SetBetweenDSMIntimeByRankingTerm(setup.rankingTerm);
                criteria.AddOrderBySumResult(Criteria.OrderBy.ORDER_BY_DESC);
                criteria.order_status_not_in = criteria.CancelStatusArray;
                criteria.ignoreMonitor();
                criteria.site_id_equal = (int)EnumSiteId.SITE_1;
                //Criteria for Filtering Product
                criteria.Add(this.GetFilteredCriteria(executeDate));

                criteria.AddGroupBy("ds_master_t.gcode");
                criteria.AddGroupBy("d_master_t.site_id");
                criteria.LIMIT = setup.rankingLimit;
                var list = spec.GetSearchData(criteria);
                
                criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

                criteria.SetBetweenDSMIntimeByRankingTerm(setup.rankingTerm);
                criteria.AddOrderBySumResult(Criteria.OrderBy.ORDER_BY_DESC);
                criteria.order_status_not_in = criteria.CancelStatusArray;
                criteria.ignoreMonitor();
                criteria.site_id_equal = (int)EnumSiteId.SITE_5;
                //Criteria for Filtering Product
                criteria.Add(this.GetFilteredCriteria(executeDate));

                criteria.AddGroupBy("ds_master_t.gcode");
                criteria.AddGroupBy("d_master_t.site_id");
                criteria.LIMIT = setup.rankingLimit;

                var list2 = spec.GetSearchData(criteria);

                list.AddRange(list2);

                foreach (var record in list)
                {
                    var repository = ErsFactory.ersRankingFactory.GetErsRankingRepository();

                    var ranking = ErsFactory.ersRankingFactory.GetErsRankingWithParameter(record);
                    ranking.sum_date = DateTime.Now;
                    ranking.rank_type = EnumRankType.Daily;

                    repository.Insert(ranking);
                }


                transaction.Commit();
            }
        }

        /// <summary>
        /// Criteria for Filtering Product
        /// </summary>
        /// <param name="executeDate"></param>
        /// <returns>string[]</returns>
        internal Criteria GetFilteredCriteria(DateTime? executeDate)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var groupCri = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();

            groupCri.disp_list_flg = EnumDisp_list_flg.Visible;
            groupCri.ignore_gcode = setup.IgnoreGcode;
            groupCri.g_active = EnumActive.Active;
            groupCri.s_active = EnumActive.Active;
            groupCri.doc_bundling_flg_not_equal = EnumDocBundlingFlg.ON;
            groupCri.SetDateFromTo(executeDate);

            return groupCri;
        }

    }
}
