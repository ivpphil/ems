﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using System.IO;

namespace jp.co.ivp.ers.batch.UpdateSpecifiedColumn
{
    public class UpdateSpecifiedColumnMail
        : ErsModelBase
    {
        protected SetupBatch setup = ErsFactory.ersBatchFactory.getSetup();
        public UpdateSpecifiedColumnMail() { }

        public UpdateSpecifiedColumnMail(string _fileName, string _errorFileName)
        {
            this._fileName = _fileName;
            this._errorFileName = _errorFileName;
        }

        private string _fileName;
        private string _errorFileName;

        public string fileName
        {
            get
            {
                return Path.GetFileName(_fileName);
            }
            set
            {
                this._fileName = value;
            }
        }

        public string errorFileName
        {
            get
            {
                return Path.GetFileName(_errorFileName);
            }
            set
            {
                this._errorFileName = value;
            }
        }

        public virtual void SendMail(string body)
        {
            var sendmail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();

            sendmail.Bind(this, body);

            string title = setup.updateSpecifiedColumnAlertMailTitle;
            string mailTo = setup.updateSpecifiedColumnAlertMailTo;
            string mailFrom = ErsFactory.ersUtilityFactory.GetErsSetupOfSite().f_email1;

            sendmail.MailSend(
                title,
                sendmail.body,
                System.IO.Path.GetFileName(System.Reflection.Assembly.GetCallingAssembly().Location),
                mailTo,
                mailFrom,
                string.Empty,
                string.Empty);
        }

        public virtual string MailBody()
        {
            string defaultBody = string.Empty;

            defaultBody += "ご担当者様";
            defaultBody += Environment.NewLine + Environment.NewLine;
            defaultBody += "DB更新を完了いたしましたので、ご報告申し上げます。";
            defaultBody += Environment.NewLine;
            defaultBody += "ファイル名：<%=.Model.fileName%>";

            defaultBody += Environment.NewLine + Environment.NewLine;

            if (errorFileName.HasValue())
            {
                defaultBody += "エラーを以下のファイルに保存いたしました。";
                defaultBody += Environment.NewLine;
                defaultBody += "ファイル名：<%=.Model.errorFileName%>";
            }
            else
            {
                defaultBody += "全件を正常にUPDATEいたしました。";
            }

            defaultBody += Environment.NewLine;

            return defaultBody;
        }
    }
}
