﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using jp.co.ivp.ers.batch;
using System.Threading.Tasks;
using NCrontab;
using jp.co.ivp.ers.batch.util;
using System.Diagnostics;

namespace TaskScheduler
{
    class TaskScheduler
    {
        static readonly string programName = "タスクスケジューラ";

        public void Execute(DateTime executeDate, IDictionary<string, object> options)
        {
            try
            {
                using (var mutex = new ErsMutex(System.Reflection.Assembly.GetCallingAssembly().Location))
                {
                    if (mutex.isStarted)
                    {
                        return;
                    }

                    ErsLogger.Initialize(ConfigurationManager.AppSettings["logFilePath"]);

                    var log4netconfigPath = new Uri(new Uri(ConfigurationManager.AppSettings["ersRoot"]), ConfigurationManager.AppSettings["log4netconfig"]).LocalPath;
                    ErsLog4netLogger.InitLogger(log4netconfigPath);

                    this.ExecuteMain(executeDate);
                }
            }
            catch (Exception ex)
            {
                //バッチスケジューラ自体の例外を補足
                //ErsLogger.OutputOperateLog(executeDate, programName + "の実行が異常終了しました。ログを出力しました。");
                ErsLog4netLogger.OutputOperateLog(executeDate, programName + "の実行が異常終了しました。ログを出力しました。");

                //エラーログを出力
                ErsLogger.OutputErrorLog(executeDate, programName, string.Empty, ex);
            }
        }

        /// <summary>
        /// メイン処理実行
        /// </summary>
        /// <param name="executeDate"></param>
        private void ExecuteMain(DateTime executeDate)
        {
            var ersRoot = ConfigurationManager.AppSettings["ersRoot"];
            var ersBinPath = ConfigurationManager.AppSettings["ersBinPath"];
            var binPath = new Uri(new Uri(ersRoot), ersBinPath).LocalPath;
            var ersMershaller = new ErsMershaller(binPath);

            //Ersを初期化する
            var ersBatchEnvironmentClassName = ConfigurationManager.AppSettings["ErsBatchEnvironmentClassName"];
            var objErsBatchEnvironment = ersMershaller.GetInstance<IErsBatchEnvironment>(ersBatchEnvironmentClassName);
            objErsBatchEnvironment.Initialization(ersRoot);

            var ersMallBatchEnvironmentClassName = ConfigurationManager.AppSettings["ErsMallBatchEnvironmentClassName"];
            var objErsMallBatchEnvironment = ersMershaller.GetInstance<IErsBatchEnvironment>(ersMallBatchEnvironmentClassName);
            objErsMallBatchEnvironment.Initialization(ersRoot);

            var ersBatchExecuterClassName = ConfigurationManager.AppSettings["ErsBatchExecuterClassName"];
            var objErsBatchExecuter = ersMershaller.GetInstance<IErsBatchExecuter>(ersBatchExecuterClassName);

            //バッチの一覧を取得
            var listBatch = objErsBatchExecuter.GetBatchDataList(executeDate);

            //実行対象のバッチを取得
            var targetBatchList = this.GetTargetBatchList(objErsBatchExecuter, executeDate, listBatch);

            //1件もない場合は、実行ログを落として終了
            if (targetBatchList.Count == 0)
            {
                //ログを出力
                //ErsLogger.OutputOperateLog(executeDate, "対象はありませんでした。");
                ErsLog4netLogger.OutputOperateLog(executeDate, "対象はありませんでした。");

                return;
            }

            //対象のバッチを非同期に実行する
            this.ExecuteAllBatch(executeDate, ersRoot, targetBatchList);
        }

        /// <summary>
        /// 実行対象のバッチを取得
        /// </summary>
        /// <param name="listBatch"></param>
        /// <returns></returns>
        private List<BatchDataContainer> GetTargetBatchList(IErsBatchExecuter objErsBatchExecuter, DateTime executeDate, IList<BatchDataContainer> listBatch)
        {
            var targetBatchList = new List<BatchDataContainer>();
            foreach (var batchContainer in listBatch)
            {
                //実行対象かどうかをチェック
                bool isTarget = this.CheckIfTarget(objErsBatchExecuter, executeDate, batchContainer);

                if (isTarget)
                {
                    //対象の場合、リストへ追加
                    targetBatchList.Add(batchContainer);
                }
            }
            return targetBatchList;
        }

        /// <summary>
        /// 実行対象かどうかをチェック
        /// </summary>
        /// <param name="executeDate"></param>
        /// <param name="batchContainer"></param>
        /// <returns></returns>
        private bool CheckIfTarget(IErsBatchExecuter objErsBatchExecuter, DateTime executeDate, BatchDataContainer batchContainer)
        {
            //実行スケジュール内かどうかをチェック(crontab形式で指定される)
            var contab = CrontabSchedule.Parse(batchContainer.schedule);
            var nextDate = contab.GetNextOccurrence(batchContainer.lastDate.Value);
            nextDate = nextDate.AddSeconds(-nextDate.Second);
            if (nextDate < executeDate)
            {
                return true;
            }

            //即時実行テーブルにレコードがあるかをチェック
            if (objErsBatchExecuter.CheckExistsImmediate(executeDate, batchContainer.batchId))
            {
                batchContainer.batchImmediate = true;
                return true;
            }

            return false;
        }

        /// <summary>
        /// 対象のバッチのプロセスを実行する
        /// </summary>
        /// <param name="targetBatchList"></param>
        private void ExecuteAllBatch(DateTime executeDate, string ersRoot, List<BatchDataContainer> targetBatchList)
        {
            foreach (var targetBatch in targetBatchList)
            {
                targetBatch.executeDate = executeDate;
                targetBatch.batchLocation = System.Reflection.Assembly.GetCallingAssembly().Location;

                //ログを出力
                //ErsLogger.OutputOperateLog(executeDate, targetBatch.batchName + "を起動します。");
                ErsLog4netLogger.OutputOperateLog(executeDate, targetBatch.batchName + "を起動します。");

                if (!ErsBatchProcess.PrepareProcessExeFile(targetBatch))
                {
                    //ErsLogger.OutputOperateLog(targetBatch.executeDate, targetBatch.batchName + "は既に実行中の為、終了しました。");
                    ErsLog4netLogger.OutputOperateLog(targetBatch.executeDate, targetBatch.batchName + "は既に実行中の為、終了しました。");
                    continue;
                }
                ErsBatchProcess.ExecuteProcess(targetBatch, EnumBatchMode.Execute);
            }
        }
    }
}
