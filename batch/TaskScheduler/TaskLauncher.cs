﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.batch.util;

namespace TaskScheduler
{
    class TaskLauncher
    {
        static readonly string programName = "タスクスケジューラ・子プロセス";

        /// <summary>
        /// 子プロセスの実行
        /// </summary>
        /// <param name="batchExecuteDate"></param>
        /// <param name="options"></param>
        internal void Execute(DateTime batchExecuteDate, IDictionary<string, object> options)
        {
            BatchDataContainer batchContainer = new BatchDataContainer();

            try
            {
                ErsLogger.Initialize(ConfigurationManager.AppSettings["logFilePath"]);
                var log4netconfigPath = new Uri(new Uri(ConfigurationManager.AppSettings["ersRoot"]), ConfigurationManager.AppSettings["log4netconfig"]).LocalPath;
                ErsLog4netLogger.InitLogger(log4netconfigPath);

                var batchMode = batchContainer.LoadOptions(options);

                using (var mutex = new ErsMutex(System.Reflection.Assembly.GetCallingAssembly().Location))
                {
                    //enableMutexで明示的にfalseを指定された場合は複数起動可能
                    if (mutex.isStarted && batchContainer.enableMutex != false)
                    {
                        if (batchMode == EnumBatchMode.Execute)
                        {
                            //ErsLogger.OutputOperateLog(batchContainer.executeDate, batchContainer.batchName + "は既に実行中の為、終了しました。");
                            ErsLog4netLogger.OutputOperateLog(batchContainer.executeDate, batchContainer.batchName + "は既に実行中の為、終了しました。");
                        }

                        return;
                    }

                    this.ExecuteMain(batchContainer, batchMode);
                }
            }
            catch (ErsBatchStepException ex)
            {
                //バッチの例外を補足
                //ErsLogger.OutputOperateLog(batchContainer.executeDate, batchContainer.batchName + "の実行が異常終了しました。ログを出力しました。");
                ErsLog4netLogger.OutputOperateLog(batchContainer.executeDate, batchContainer.batchName + "の実行が異常終了しました。ログを出力しました。");

                //エラーログを出力
                ErsLogger.OutputErrorLog(batchContainer.executeDate, batchContainer.batchId, ex.Message, ex.InnerException);
            }
            catch (Exception ex)
            {
                var executeDate = (batchContainer.executeDate ?? batchExecuteDate);
                var batchName = (batchContainer.batchName ?? programName);
                var batchId = (batchContainer.batchId ?? programName);

                //バッチの例外を補足
                //ErsLogger.OutputOperateLog(executeDate, batchName + "の実行が異常終了しました。ログを出力しました。");
                ErsLog4netLogger.OutputOperateLog(executeDate, batchName + "の実行が異常終了しました。ログを出力しました。");

                //エラーログを出力
                ErsLogger.OutputErrorLog(executeDate, batchId, string.Empty, ex);
            }

            var executeDateForLog = (batchContainer.executeDate ?? batchExecuteDate);
            //終了ログを出力
            ErsLogger.OutputEndLog(executeDateForLog, batchContainer.batchId);
        }

        /// <summary>
        /// 実行メイン処理
        /// </summary>
        /// <param name="options"></param>
        /// <param name="batchContainer"></param>
        private void ExecuteMain(BatchDataContainer batchContainer, EnumBatchMode batchMode)
        {
            var ersRoot = ConfigurationManager.AppSettings["ersRoot"];
            var ersBinPath = ConfigurationManager.AppSettings["ersBinPath"];
            var binPath = new Uri(new Uri(ersRoot), ersBinPath).LocalPath;
            var ersMershaller = new ErsMershaller(binPath);

            //Ersを初期化する
            var ersBatchEnvironmentClassName = ConfigurationManager.AppSettings["ErsBatchEnvironmentClassName"];
            var objErsBatchEnvironment = ersMershaller.GetInstance<IErsBatchEnvironment>(ersBatchEnvironmentClassName);
            objErsBatchEnvironment.Initialization(ersRoot);

            var ersMallBatchEnvironmentClassName = ConfigurationManager.AppSettings["ErsMallBatchEnvironmentClassName"];
            var objMallErsBatchEnvironment = ersMershaller.GetInstance<IErsBatchEnvironment>(ersMallBatchEnvironmentClassName);
            objMallErsBatchEnvironment.Initialization(ersRoot);

            //バッチを実行する
            var ersBatchExecuterClassName = ConfigurationManager.AppSettings["ErsBatchExecuterClassName"];
            var objErsBatchExecuter = ersMershaller.GetInstance<IErsBatchExecuter>(ersBatchExecuterClassName);
            if (batchMode == EnumBatchMode.Execute)
            {
                objErsBatchExecuter.Execute(batchContainer);

                //ErsLogger.OutputOperateLog(batchContainer.executeDate, batchContainer.batchName + "の実行が正常終了しました。");
                ErsLog4netLogger.OutputOperateLog(batchContainer.executeDate, batchContainer.batchName + "の実行が正常終了しました。");
            }
            else
            {
                objErsBatchExecuter.ExecuteClass(batchContainer);
            }
        }
    }
}
