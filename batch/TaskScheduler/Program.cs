﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.batch.util;
using System.Configuration;
using jp.co.ivp.ers.batch;

namespace TaskScheduler
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = ErsObtainOption.Obtain(args, true);
            var executeDate = DateTime.Now;

            if (options.ContainsKey("-execute"))
            {
                //バッチを実行する（別プロセスとして起動される）
                var objTaskLauncher = new TaskLauncher();
                objTaskLauncher.Execute(executeDate, options);
            }
            else
            {
                //バッチのリストを取得し、各バッチを起動する
                var objTaskScheduler = new TaskScheduler();
                objTaskScheduler.Execute(executeDate, options);
            }
        }
    }
}
