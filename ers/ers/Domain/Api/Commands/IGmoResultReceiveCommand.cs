﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using jp.co.ivp.ers.Payment;

namespace ers.Domain.Api.Commands
{
    public interface IGmoResultReceiveCommand
        : ICommand, IGmoResultReceive
    {
    }
}