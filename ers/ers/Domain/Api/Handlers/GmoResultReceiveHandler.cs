﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Api.Commands;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db;
using System.Text;
using jp.co.ivp.ers.util;

namespace ers.Domain.Api.Handlers
{
    public class GmoResultReceiveHandler
        : ICommandHandler<IGmoResultReceiveCommand>
    {
        private const string exeName = "コンビニ決済入金処理";
        private const int CancelErr = 1;
        private const int MultipleErr = 2;

        public ICommandResult Submit(IGmoResultReceiveCommand command)
        {
            this.SaveGmoLog();

            this.ImportResult(command);

            return new CommandResult(true);
        }

        private void ImportResult(IGmoResultReceiveCommand command)
        {
            //コンビニ以外は受信しない
            if (command.PayType != EnumGmoPayType.Convenience || command.Status != EnumGmoResultStatus.PAYSUCCESS)
            {
                return;
            }

            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.access_id = command.AccessID;
            criteria.credit_order_id = command.OrderID;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            //criteria.order_payment_status_in = new[] { EnumOrderPaymentStatusType.NOT_PAID };
            
            var listOrder = repository.Find(criteria);
            var newOrder = listOrder.First();

            bool multipleFlg = false;

            var oldOrder = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(newOrder.GetPropertiesAsDictionary());
            newOrder.order_payment_status = EnumOrderPaymentStatusType.PAID;
            newOrder.paid_date = DateTime.ParseExact(command.FinishDate, "yyyyMMddHHmmss", null);
            newOrder.paid_price = command.Amount.Value;

            //入金済みのものに対して入金があった場合、伝票は更新せず、エラー通知のみ行う
            if (oldOrder.order_payment_status == EnumOrderPaymentStatusType.NOT_PAID)
            {
                repository.Update(oldOrder, newOrder);
            }
            else
            {
                multipleFlg = true;
            }


            //キャンセル伝票、多重入金の場合、エラーメールを送信する
            //多重入金
            if (multipleFlg)
            {
                SendCancelError(newOrder, MultipleErr);
            }
            else
            {
                //キャンセル判定
                var orderRecords = GetOrderRecords(newOrder.d_no);
                var order_status = ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy().ObtainFrom(orderRecords);
                if (order_status == EnumOrderStatusType.CANCELED || order_status == EnumOrderStatusType.CANCELED_AFTER_DELIVER)
                {
                    SendCancelError(newOrder, CancelErr);
                }
            }
        }

        private void SaveGmoLog()
        {
            var retDic = new Dictionary<string, object>();
            foreach (string key in HttpContext.Current.Request.Form.Keys)
            {
                retDic.Add(Convert.ToString(key), HttpContext.Current.Request.Form[key]);
            }
            ErsGmoBase.OutputGmoLog(null, retDic);
        }

        private IEnumerable<ErsOrderRecord> GetOrderRecords(string d_no)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
            criteria.d_no = d_no;
            criteria.doc_bundling_flg = EnumDocBundlingFlg.OFF;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            return repository.Find(criteria);
        }

        private void SendCancelError(ErsOrder order, int ErrorType)
        {
            Setup setUp = ErsFactory.ersUtilityFactory.getSetup();
 
            //管理者宛メール処理
            var sbMail_body = new StringBuilder();
            sbMail_body.AppendLine("ご担当者様");
            sbMail_body.AppendLine();
            if (ErrorType == CancelErr)
            {
                sbMail_body.AppendLine("キャンセル伝票に対し、入金された旨を検知いたしました。");
            }
            else
            {
                sbMail_body.AppendLine("１つの伝票に対し、複数回入金された旨を検知いたしました。");
            }
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("今回入金額：\\{paid_price}");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("決済方法：コンビニ決済");
            sbMail_body.AppendLine("伝票番号：{d_no}");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("返金処理の手配をお願いいたします。");
            sbMail_body.AppendLine();
            sbMail_body.AppendLine("※本メールはメール自動配信システムから送信しております。");

            var values = new Dictionary<string, object>() { { "paid_price", order.paid_price }, { "d_no", order.d_no } };

            var mail_body = StringExtension.Format(sbMail_body.ToString(), values);

            var mail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();
            mail.MailSend(
                setUp.conveniencePaidGmoErrMailTitle, mail_body,
                exeName,
                setUp.conveniencePaidGmoErrMailTo, setUp.conveniencePaidGmoErrMailFrom,
                setUp.conveniencePaidGmoErrMailCc, setUp.conveniencePaidGmoErrMailBcc);
        }
    }
}