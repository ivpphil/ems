﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Api.Commands;
using System.ComponentModel.DataAnnotations;

namespace ers.Domain.Api.Handlers
{
    public class ValidateGetZip
        : IValidationHandler<IGetZipCommand>
    {
        public IEnumerable<ValidationResult> Validate(IGetZipCommand command)
        {
            yield return command.CheckRequired("zip");
        }
    }
}