﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Api.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.Payment;

namespace ers.Domain.Api.Handlers
{
    public class ValidateGmoResultReceive
        : IValidationHandler<IGmoResultReceiveCommand>
    {
        public IEnumerable<ValidationResult> Validate(IGmoResultReceiveCommand command)
        {
            if (command.JobCd == EnumGmoJobCd.CHECK || command.Status != EnumGmoResultStatus.PAYSUCCESS)
            {
                //有効性チェックと、決済完了の場合以外は処理しない
                yield break;
            }

            foreach (var result in this.CheckRequired(command))
            {
                yield return result;
            }

            foreach (var result in this.CheckValues(command))
            {
                yield return result;
            }
        }

        private IEnumerable<ValidationResult> CheckValues(IGmoResultReceiveCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            if (command.ShopID != setup.gmo_shop_id)
            {
                yield return new ValidationResult(ErsResources.GetMessage("10025", ErsResources.GetFieldName("ShopID")), new[] { "ShopID" });
            }
            if (command.ShopPass != "**********")
            {
                yield return new ValidationResult(ErsResources.GetMessage("10025", ErsResources.GetFieldName("ShopPass")), new[] { "ShopPass" });
            }
            if (command.AccessPass != "********************************")
            {
                yield return new ValidationResult(ErsResources.GetMessage("10025", ErsResources.GetFieldName("AccessPass")), new[] { "AccessPass" });
            }

            //エラー処理
            if (command.ErrCode.HasValue() || command.ErrInfo.HasValue())
            {
                yield return new ValidationResult(new GmoException(new GmoRetryableException(command.ErrCode, command.ErrInfo).ErrorInfoList).Message, new[] { "ErrCode", "ErrInfo" });
            }

            //以下伝票データのチェック
            if (!command.IsValidField("AccessID", "OrderID"))
            {
                yield break;
            }
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.access_id = command.AccessID;
            criteria.credit_order_id = command.OrderID;
            var listOrder = repository.Find(criteria);
            if (listOrder.Count != 1)
            {
                yield return new ValidationResult(ErsResources.GetMessage("53303", command.AccessID, command.OrderID), new[] { "AccessID", "OrderID" });
                yield break;
            }

            var objOrder = listOrder.First();

            if (command.PayType == EnumGmoPayType.Convenience)
            {
                if (objOrder.pay != EnumPaymentType.CONVENIENCE_STORE)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("53304", ErsResources.GetFieldName("PayType"), command.PayType), new[] { "PayType" });
                }
                if (objOrder.conv_conf_no != command.CvsConfNo)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("53304", ErsResources.GetFieldName("CvsConfNo"), command.CvsCode), new[] { "CvsConfNo" });
                }
                if (objOrder.conv_receipt_no != command.CvsReceiptNo)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("53304", ErsResources.GetFieldName("CvsReceiptNo"), command.CvsCode), new[] { "CvsReceiptNo" });
                }
            }
        }

        private IEnumerable<ValidationResult> CheckRequired(IGmoResultReceiveCommand command)
        {
            yield return command.CheckRequired("ShopID");
            yield return command.CheckRequired("ShopPass");
            yield return command.CheckRequired("AccessID");
            yield return command.CheckRequired("AccessPass");
            yield return command.CheckRequired("OrderID");
            yield return command.CheckRequired("Status");
            yield return command.CheckRequired("Amount");
            yield return command.CheckRequired("Tax");
            yield return command.CheckRequired("Currency");
            yield return command.CheckRequired("TranDate");
            yield return command.CheckRequired("PayType");

            // カード・PayPal決済以外
            if (command.PayType != EnumGmoPayType.Credit &&
                command.PayType != EnumGmoPayType.PayPal)
            {
                yield return command.CheckRequired("PaymentTerm");
            }

            // カード・PayPal・iDネット
            if (command.PayType == EnumGmoPayType.Credit ||
               command.PayType == EnumGmoPayType.PayPal ||
               command.PayType == EnumGmoPayType.iDnet)
            {
                yield return command.CheckRequired("JobCd");
            }

            //カード・ｉＤネット
            if (command.PayType == EnumGmoPayType.Credit ||
                command.PayType == EnumGmoPayType.iDnet)
            {
                yield return command.CheckRequired("Forward");
                yield return command.CheckRequired("Method");
                yield return command.CheckRequired("Approve");
            }

            //カード・コンビニ・Pay-easy・PayPal・ｉＤネット
            if (command.PayType == EnumGmoPayType.Credit ||
                command.PayType == EnumGmoPayType.Convenience ||
                command.PayType == EnumGmoPayType.PayEasy ||
                command.PayType == EnumGmoPayType.PayPal ||
                command.PayType == EnumGmoPayType.iDnet)
            {
                yield return command.CheckRequired("TranID");
            }

            //コンビニ
            if (command.PayType == EnumGmoPayType.Convenience)
            {
                yield return command.CheckRequired("CvsCode");
                yield return command.CheckRequired("CvsConfNo");
                yield return command.CheckRequired("CvsReceiptNo");
            }

            //Edy
            if (command.PayType == EnumGmoPayType.Edy)
            {
                yield return command.CheckRequired("EdyReceiptNo");
                yield return command.CheckRequired("EdyOrderNo");
            }

            //Suica
            if (command.PayType == EnumGmoPayType.Suica)
            {
                yield return command.CheckRequired("SuicaReceiptNo");
                yield return command.CheckRequired("SuicaOrderNo");
            }

            //Pay-easy
            if (command.PayType == EnumGmoPayType.PayEasy)
            {
                yield return command.CheckRequired("CustID");
                yield return command.CheckRequired("BkCode");
                yield return command.CheckRequired("ConfNo");
                yield return command.CheckRequired("EncryptReceiptNo");
            }

            //モバイルSuica・Edy・コンビニ・Pay-easy
            if (command.PayType == EnumGmoPayType.Suica ||
                command.PayType == EnumGmoPayType.Edy ||
                command.PayType == EnumGmoPayType.Convenience ||
                command.PayType == EnumGmoPayType.PayEasy)
            {
                yield return command.CheckRequired("FinishDate");
                yield return command.CheckRequired("ReceiptDate");
            }

            //WebMoney
            if (command.PayType == EnumGmoPayType.WebMoney)
            {
                yield return command.CheckRequired("WebMoneyManagementNo");
                yield return command.CheckRequired("WebMoneySettleCode");
            }
        }
    }
}