﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Api.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace ers.Domain.Api.Mappers
{
    public class KeywordsSuggestMapper
        : IMapper<IKeywordsSuggestMappable>
    {
        public void Map(IKeywordsSuggestMappable objMappable)
        {
            if (!objMappable.keyword.HasValue())
            {
                return;
            }

            var keyword = ErsKanaConverter.String_conversion(objMappable.keyword);
            objMappable.ListSuggest = this.CreateSuggestList(keyword);
        }

        private IEnumerable<string> CreateSuggestList(string keyword)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsKeywordsRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsKeywordsCriteria();
            criteria.keyword_prefix_search = keyword;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.SetOrderByKeywordOctetLength(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);
            var listKeyword = repository.Find(criteria);
            if (listKeyword.Count == 0)
            {
                return null;
            }

            return listKeyword.Select((objKeyword) => objKeyword.disp_keyword);
        }
    }
}