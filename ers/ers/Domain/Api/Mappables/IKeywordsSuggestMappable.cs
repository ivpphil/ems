﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ers.Domain.Api.Mappables
{
    public interface IKeywordsSuggestMappable
        : IMappable
    {
        string keyword { get; set; }

        IEnumerable<string> ListSuggest { get; set; }
    }
}