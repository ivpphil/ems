﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Redirect.Commands
{
    public interface IRedirectCommand
        : ICommand
    {
        int? id { get; set; }

        string ptn { get; set; }
    }
}