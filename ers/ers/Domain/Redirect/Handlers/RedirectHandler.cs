﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Redirect.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Redirect.Handlers
{
    public class RedirectHandler
        : ICommandHandler<IRedirectCommand>
    {
        public ICommandResult Submit(IRedirectCommand command)
        {
            // クリック数をカウントアップする
            if (!this.AlreadyRegistered(command.id, command.ptn))
            {
                //count up
                this.InsertMailUrlClickCount(command.id, command.ptn);
            }
            else
            {
                //create new record
                this.UpdateMailUrlClickCount(command.id, command.ptn);
            }

            return new CommandResult(true);
        }

        /// <summary>
        /// check if the value is already registered
        /// </summary>
        /// <param name="process_id"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        private bool AlreadyRegistered(int? process_id, string url)
        {
            var repository = ErsFactory.ersStepMailFactory.GetErsMailUrlClickCounterRepository();
            var criteria = ErsFactory.ersStepMailFactory.GetErsMailUrlClickCounterCriteria();
            criteria.process_id = process_id;
            criteria.url = url;
            var recordList = repository.Find(criteria);
            return (recordList.Count > 0);
        }

        /// <summary>
        /// Count up the click count
        /// </summary>
        /// <param name="ersMailUrlClickCounter"></param>
        private void UpdateMailUrlClickCount(int? process_id, string url)
        {
            ErsFactory.ersStepMailFactory.GetCountUpMailUrlClickStgy().CountUp(process_id, url);
        }

        /// <summary>
        /// Insert new record of mail_url_click_counter_t
        /// </summary>
        private void InsertMailUrlClickCount(int? process_id, string url)
        {
            var process = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(process_id);

            var repository = ErsFactory.ersStepMailFactory.GetErsMailUrlClickCounterRepository();

            var record = ErsFactory.ersStepMailFactory.GetErsMailUrlClickCounter();
            record.process_id = process_id;
            record.step_mail_id = process.step_mail_id;
            record.url = url;
            record.click_count = 1;
            record.intime = DateTime.Now;
            record.active = EnumActive.Active;

            repository.Insert(record, true);
        }
    }
}