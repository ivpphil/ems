﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Redirect.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Redirect.Handlers
{
    public class ValidateRedirect
        : IValidationHandler<IRedirectCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IRedirectCommand command)
        {
            yield return command.CheckRequired("id");
            //idが正しいかを検証
            if (command.IsValidField("id") && command.id != null)
            {
                yield return ErsFactory.ErsAtMailFactory.GetCheckProcessExistStgy().CheckProcessExist(command.id);
            }

            //リダイレクト先URLが正しいかを検証
            yield return command.CheckRequired("ptn");
            if (command.IsValidField("ptn") && !string.IsNullOrEmpty(command.ptn))
            {
                yield return ErsFactory.ErsAtMailFactory.GetCheckRedirectUrlStgy().CheckUrl(command.ptn, "stepRedirect.ptn");
            }

            yield break;
        }
    }
}