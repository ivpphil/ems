﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;

namespace ers.Domain.Images.Commands
{
    public interface IStepSpacerCommand
        : ICommand
    {
        int? id { get; }

        string add { get; }
    }
}