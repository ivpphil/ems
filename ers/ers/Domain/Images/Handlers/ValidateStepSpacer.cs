﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Images.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Images.Handlers
{
    public class ValidateStepSpacer
        : IValidationHandler<IStepSpacerCommand>
    {
        public IEnumerable<ValidationResult> Validate(IStepSpacerCommand command)
        {
            //idのチェック
            yield return command.CheckRequired("id");
            //idが正しいかを検証
            if (command.IsValidField("id") && command.id != null)
            {
                yield return ErsFactory.ErsAtMailFactory.GetCheckProcessExistStgy().CheckProcessExist(command.id);
            }

            //リダイレクト先URLが正しいかを検証
            yield return command.CheckRequired("add");
            if (command.IsValidField("add") && !string.IsNullOrEmpty(command.add))
            {
                yield return ErsFactory.ErsAtMailFactory.GetCheckEncodedMailAddressStgy().CheckEncodedMailAddress(command.add, "stepSpacer.add");
            }

            yield break;
        }
    }
}