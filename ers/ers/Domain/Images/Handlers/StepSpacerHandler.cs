﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Images.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Images.Handlers
{
    public class StepSpacerHandler
        : ICommandHandler<IStepSpacerCommand>
    {
        public ICommandResult Submit(IStepSpacerCommand command)
        {
            this.RegistCount(command);
            return new CommandResult(true);
        }

        internal void RegistCount(IStepSpacerCommand command)
        {
            //stepmail idを取得
            var process = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(command.id.Value);

            //メールアドレスを復号化
            var decripter = ErsFactory.ersUtilityFactory.getErsEncryption();
            var email = decripter.HexDecode(command.add);

            if (!this.AlreadyRegistered(command.id, email))
            {
                this.InsertNewRecord(command.id, process.step_mail_id, email);
            }
        }

        private bool AlreadyRegistered(int? process_id, string email)
        {
            var repository = ErsFactory.ersStepMailFactory.GetErsMailOpenCounterRepository();
            var criteria = ErsFactory.ersStepMailFactory.GetErsMailOpenCounterCriteria();
            criteria.process_id = process_id;
            criteria.mail_addr = email;

            return repository.GetRecordCount(criteria) > 0;
        }

        private void InsertNewRecord(int? process_id, int? step_mail_id, string email)
        {
            var repository = ErsFactory.ersStepMailFactory.GetErsMailOpenCounterRepository();
            var record = ErsFactory.ersStepMailFactory.GetErsMailOpenCounter();
            record.process_id = process_id;
            record.step_mail_id = step_mail_id;
            record.mail_addr = email;
            record.open_count = 1;
            record.intime = DateTime.Now;
            record.active = EnumActive.Active;
            repository.Insert(record, true);
        }
    }
}