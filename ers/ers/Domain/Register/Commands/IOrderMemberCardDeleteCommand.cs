﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers;

namespace ers.Domain.Register.Commands
{
    public interface IOrderMemberCardDeleteCommand
        : ICommand
    {
        ErsMember member { get; set; }

        int?[] del_card_id { get; set; }

        EnumMemberEntryMode? k_flg { get; set; }

        EnumPaymentType? pay { get; set; }
    }
}