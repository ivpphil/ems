﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Cart.Commands;

namespace ers.Domain.Register.Commands
{
    public interface IRegisterCartCommand
        : ICartCommand
    {
    }
}