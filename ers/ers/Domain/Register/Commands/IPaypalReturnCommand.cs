﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Register.Commands
{
    public interface IPaypalReturnCommand
        : ICommand
    {
        string val { get; set; }

        string token { get; set; }

        string PayerID { get; set; }
    }
}