﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.order.strategy;

namespace ers.Domain.Register.Commands
{
    public interface IOrderRegistCommand
        : ICommand, IIsMonitorSpecDatasource, IPaymentInfoGmoInputContainer, IRegistRegularOrderDataSource, IPaymentInfoGmoConvenienceContainer
    {
        EnumMemberEntryMode? k_flg { get; }

        ErsOrderIntegrated order { get; }

        ErsMember member { get; }

        ICartCommand cart { get; }

        IEnumerable<ErsBaskRecord> orderRecords { get; }

        bool entry_submit { get; }

        bool entry2_submit { get; }

        bool entry3_submit { get; }

        bool back_to_input { get; }

        bool point_flg { get; set; }

        bool coupon_flg { get; }

        int? pri_chk { get; }

        string ransu { get; }

        EnumDelvMethod? deliv_method { get; set; }

        EnumPaymentType? pay { get; set; }

        DateTime? senddate { get; }

        int? sendtime { get; }

        int? regular_sendtime { get; }

        EnumFirstTimeOrdinary? firstTimeOrdinary { get; }

        EnumSendTo? send { get; set; }

        int point { get; }

        int? ent_point { get; }

        int? p_service { get; set; }

        string coupon_code { get; set; }

        string ent_coupon_code { get; set; }

        int? member_add_id { get; set; }

        EnumPmFlg pm_flg { get; }

        DateTime? intime { set; }

        bool IsNonNeededPaymentSpec { get; }

        bool ValidateMappedOrder { get; set; }

        string mcode { get; }

        int? coupon_discount { get; }

        int? order_total { get; }

        bool IsValidateRegularOrder { get; }

        int? pref { get; set; }

        int? add_pref { get; set; }

        string validated_ent_coupon_code { get; set; }
    }
}