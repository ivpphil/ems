﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.Payment;

namespace ers.Domain.Register.Commands
{
    public interface IOrderMemberRegistCommand
        : ICommand, IPaymentInfoContainer
    {
        ErsOrderIntegrated order { get; }

        ErsMember member { get; }

        ICartCommand cart { get; }

        EnumMemberEntryMode? k_flg { get; }

        bool entry_submit { get; }

        bool entry2_submit { get; }

        bool entry3_submit { get; }

        string mcode { get; }

        string email { get; }

        string email_confirm { get; }

        EnumMformat? mformat { get; }

        string passwd { get; }

        string passwd_confirm { get; }

        string lname { get; }

        string fname { get; }

        string lnamek { get; }

        string fnamek { get; }

        int? pref { get; }

        int? birthday_y { get; }

        int? birthday_m { get; }

        int? birthday_d { get; }

        EnumSex? sex { get; }

        int? job { get; }

        int? ques { get; }

        EnumPmFlg pm_flg { get; }

        bool needPersonalData { get; }

        EnumSendTo? send { get; set; }

        int? member_add_id { get; set; }

        string add_lname { get; }

        string add_fname { get; }

        string add_lnamek { get; }

        string add_fnamek { get; }

        int? add_pref { get; }

        EnumAddressAdd address_add { get; }

        EnumCardSave card_save { get; }

        int? card_id { get; set; }

        int? validity_y { get; }

        int? validity_m { get; }

        EnumPaymentType? pay { get; }

        int? p_service { get; set; }

        string compname { get; set; }

        string compnamek { get; set; }

        string division { get; set; }

        string divisionk { get; set; }

        string tel { get; set; }

        string fax { get; set; }

        string zip { get; set; }

        string address { get; set; }

        string taddress { get; set; }

        string maddress { get; set; }

        bool IsNonNeededPaymentSpec { get; }

        int?[] del_card_id { get; set; }

        string add_zip { get; set; }
    }
}