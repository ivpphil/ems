﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.order;

namespace ers.Domain.Register.Commands
{
    public interface IMemberEntryCommand : ICommand
    {
        string mcode { get; set; }
        string lname { get; }
        string fname { get; }
        string lnamek { get; }
        string fnamek { get; }
        string compname { get; }
        string compnamek { get; }
        string division { get; }
        string divisionk { get; }
        string tlname { get; }
        string tfname { get; }
        string tlnamek { get; }
        string tfnamek { get; }
        string zip { get; }
        int? pref { get; }
        string address { get; }
        string taddress { get; }
        string maddress { get; }
        string tel { get; }
        string fax { get; }
        string email { get; }
        string email_confirm { get; }
        EnumMformat? mformat { get; }
        string passwd { get; }
        string passwd_confirm { get; }
        int? ques { get; }
        string ans { get; }
        int? job { get; }
        EnumSex? sex { get; }
        int? birthday_y { get; }
        int? birthday_m { get; }
        int? birthday_d { get; }
        DateTime? birth { get; }
        EnumMFlg? m_flg { get; }
        int? pri_chk { get; }
    }
}