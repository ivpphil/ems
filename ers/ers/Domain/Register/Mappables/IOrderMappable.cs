﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.Payment;

namespace ers.Domain.Register.Mappables
{
    public interface IOrderMappable
        : IMappable, IOrderIntegratedDateContainer, IPaymentInfoGmoInputContainer, IPaymentInfoPayPalContainer
    {
        ErsMember member { get; }

        ICartCommand cart { get; }

        EnumWeekendOperation weekend_operation { get; }

        ErsOrderIntegrated order { set; }

        EnumFirstTimeOrdinary? firstTimeOrdinary { get; set; }

        int? ent_point { set; }

        CreditCardInfo savedCardInfo { set; }

        bool entry_submit { get; }

        EnumSendTo? send { set; }

        EnumMemberEntryMode? k_flg { get; }

        string mcode { get; }

        string address_name { set; }

        string add_lname { set; }

        string add_fname { set; }

        string add_lnamek { set; }

        string add_fnamek { set; }

        string add_zip { set; }

        string add_tel { set; }

        string add_fax { set; }

        int? add_pref { set; }

        string add_address { set; }

        string add_taddress { set; }

        string add_maddress { set; }

        EnumDelvMethod? deliv_method { get; set; }

        short DisplayedDelvMethod { get; }
    }
}