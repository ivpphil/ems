﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order.related;

namespace ers.Domain.Register.Mappables
{
    public interface IMemberOrderMappable
           : IMappable
    {
        bool IsConfirmationPage { get; }

        EnumMemberEntryMode? k_flg { get; }

        ErsMember member { set; }

        IList<Dictionary<string, object>> ListCreditCardInfo { get; set; }

        IList<CreditCardInfo> ListDelCreditCardInfo { get; set; }

        IList<Dictionary<string, object>> addressList { set; }

        string email { set; }

        string email_confirm { set; }

        int? birthday_y { set; }

        int? birthday_m { set; }

        int? birthday_d { set; }

        string memo { set; }

        EnumMFlg? m_flg { set; }

        int?[] del_card_id { get; set; }

        int? point_magnification { get; set; }

    }
}