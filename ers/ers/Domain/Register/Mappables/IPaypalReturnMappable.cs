﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ers.Domain.Register.Mappables
{
    public interface IPaypalReturnMappable
        : IMappable
    {
        ers.Models.Register register { get; set; }

        ers.Models.Cart cart { get; set; }

        string ssl_ransu { get; set; }

        string mcode { get; set; }

        string token { get; set; }

        string PayerID { get; set; }

        int? monitor_flg { get; set; }
    }
}