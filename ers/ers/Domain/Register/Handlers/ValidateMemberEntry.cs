﻿using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using ers.Domain.Register.Commands;
using System.ComponentModel.DataAnnotations;
using ers.Models;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers;

namespace ers.Domain.Register.Handlers
{

    public class ValidateMemberEntry : IValidationHandler<IMemberEntryCommand>
    {

        public virtual IEnumerable<ValidationResult> Validate(IMemberEntryCommand command)
        {
            yield return command.CheckRequired("lname");
            yield return command.CheckRequired("fname");
            yield return command.CheckRequired("lnamek");
            yield return command.CheckRequired("fnamek");

            yield return command.CheckRequired("email");
            yield return ErsFactory.ersMemberFactory.GetCheckEmailConfirmStgy().CheckEmailConfirm(command.email, command.email_confirm);
            if (!string.IsNullOrEmpty(command.email))
            {
                yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailStgy().CheckDuplicate(ErsMember.DEFAUTL_MCODE, command.email);
            }

            yield return command.CheckRequired("mformat");
            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("mformat", EnumCommonNameType.MFormat, (int?)command.mformat);

            yield return command.CheckRequired("tel");

            yield return command.CheckRequired("zip");
            yield return command.CheckRequired("pref");
            foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("pref", command.pref))
            {
                yield return result;
            }
            if (command.IsValidField("zip", "pref"))
            {
                yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("zip", command.zip, "pref", command.pref);
            }
            yield return command.CheckRequired("taddress");
            yield return command.CheckRequired("address");

            yield return command.CheckRequired("birthday_y");
            yield return command.CheckRequired("birthday_m");
            yield return command.CheckRequired("birthday_d");
            foreach (var result in ErsFactory.ersMemberFactory.GetValidateBirthdayStgy().Validate(command.birthday_y, command.birthday_m, command.birthday_d))
            {
                yield return result;
            }

            yield return command.CheckRequired("sex");
            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("sex", EnumCommonNameType.Sex, (int?)command.sex);

            yield return command.CheckRequired("passwd");

            yield return ErsFactory.ersMemberFactory.GetCheckPasswdConfirmStgy().CheckPasswdConfirm(command.passwd, command.passwd_confirm);

            yield return command.CheckRequired("ques");
            foreach (var result in ErsFactory.ersMemberFactory.GetValidateQuesStgy().Validate(command.ques))
            {
                yield return result;
            }
           
            yield return command.CheckRequired("ans");
            

            foreach (var result in ErsFactory.ersMemberFactory.GetValidateJobStgy().Validate(command.job))
            {
                yield return result;
            }

            

            if (command.pri_chk == null)
                yield return new ValidationResult(ErsResources.GetMessage("20101"), new[] { "pri_chk" });

        }
    }
}
