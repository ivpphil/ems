﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Register.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.state;
using ers.Models.cart;
using System.Collections;

namespace ers.Domain.Register.Handlers
{
    public class ValidateOrderRegist
        : IValidationHandler<IOrderRegistCommand>
    {
        public IEnumerable<ValidationResult> Validate(IOrderRegistCommand command)
        {
            if (command.IsValidateRegularOrder)
            {
                if (command.k_flg == EnumMemberEntryMode.ANONYMOUS)
                    this.ValidateRegularOrder(command);

                return null;
            }

            if (command.ValidateMappedOrder)
            {
                return this.ValidateGeneratedOrder(command);
            }
            else
            {
                return this.ValidateInputValues(command);
            }


        }

        public virtual IEnumerable<ValidationResult> ValidateGeneratedOrder(IOrderRegistCommand command)
        {

            yield return ErsFactory.ersCouponFactory.GetErsValidateCouponStgy().
                Validate(command.mcode, command.ent_coupon_code, command.order.subtotal, null, false, ErsFactory.ersUtilityFactory.getSetup().site_id);

            //For Recomputation of invalid coupon
            if (!command.IsValidField("ent_coupon_code"))
            {
                command.validated_ent_coupon_code = command.ent_coupon_code;
                command.ent_coupon_code = null;
            }

            //フロアリミット金額チェック
            if (command.pay != null)
            {
                yield return ErsFactory.ersOrderFactory.GetCheckFloorLimitStgy().Validate(command.pay, command.order_total.Value, command.pm_flg);
            }

            if(command.cart != null)
            {
                IEnumerable<string> basket_list = (command.cart.basketItems.Select(x => x.scode) as IEnumerable<string>).Cast<string>().ToList();
                IEnumerable<string> regular_basket_list = (command.cart.regularBasketItems.Select(x => x.scode) as IEnumerable<string>).Cast<string>().ToList();
                yield return ErsFactory.ersOrderFactory.ValidateOrderRegistDeliveryMethod().Validate(command.deliv_method, basket_list, regular_basket_list);
            }

        }

        public virtual IEnumerable<ValidationResult> ValidateInputValues(IOrderRegistCommand command)
        {
            //入力クーポンコードを大文字にする
            if (!string.IsNullOrEmpty(command.ent_coupon_code))
            {
                command.ent_coupon_code = command.ent_coupon_code.ToUpper();
            }

            if (!command.entry_submit && !command.cart.recompute && !command.cart.regular_recompute)
            {
                yield return command.CheckRequired("send");
                yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("send", EnumCommonNameType.SendTo, (int?)command.send);

                //For All item has total of 0 and bask_t.carriageFree == EnumCarriageFreeStatus.CARRIAGE_FREE
                if (!command.IsNonNeededPaymentSpec)
                {
                    yield return command.CheckRequired("pay");
                    if (command.IsValidField("pay"))
                    {
                        yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().Validate("pay", command.pay, command.cart != null && command.cart.regularBasketItems != null && command.cart.regularBasketItems.Count() > 0);
                        var shipping_pref = (command.send == EnumSendTo.MEMBER_ADDRESS ? command.pref : command.add_pref);
                        yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().ValidateOverseas("pay", command.pay, shipping_pref);
                        if (command.pay == EnumPaymentType.NON_NEEDED_PAYMENT)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName("pay")), new[] { "pay" });
                        }
                    }

                    //コンビニ用Validation
                    if (command.pay == EnumPaymentType.CONVENIENCE_STORE)
                    {
                        yield return command.CheckRequired("conv_code");
                        if (command.IsValidField("conv_code"))
                        {
                            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("conv_code", EnumCommonNameType.ConvCode, (int?)command.conv_code);
                        }
                    }
                }

                if (command.cart.basketItemCount != 0)
                {
                    yield return command.CheckRequired("firstTimeOrdinary");
                }

                //check Senddate
                if (command.firstTimeOrdinary == EnumFirstTimeOrdinary.Specify)
                {
                    yield return command.CheckRequired("senddate");

                    foreach (var result in ErsFactory.ersOrderFactory.GetValidateSenddateStgy().Validate(DateTime.Now, command.senddate))
                    {
                        yield return result;
                    }
                }

                foreach (var result in ErsFactory.ersOrderFactory.GetValidateSendtimeStgy().Validate(command.sendtime))
                {
                    yield return result;
                }


                if (((IPaymentInfoGmoInputContainer)command).card_id.HasValue)
                {

                    //modelに預け情報をセット
                    var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(ErsContext.sessionState.Get("mcode"));
                    var cardInfo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD)).ObtainMemberCardInfo(member, ((IPaymentInfoGmoInputContainer)command).card_id);
                    if (cardInfo == null)
                        yield return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName("card_id")), new[] { "card_id" });
                }

                //entry2の遷移時チェック
                if (command.entry2_submit || command.entry3_submit)
                {
                    yield return command.CheckRequired("deliv_method");

                    if (command.pay != null)
                    {
                        //メール便が選択された場合代引き選択不可
                        yield return ErsFactory.ersOrderFactory.GetCheckMailDeliveryStgy().Validate(command.deliv_method, command.pay);
                    }
                }

                //pointチェック
                if (command.k_flg == EnumMemberEntryMode.MEMBER && !command.back_to_input)
                {
                    var pointCheckResult = ErsFactory.ersOrderFactory.GetCheckPointsStgy().CheckPoints(command.point, command.ent_point, command.p_service, command.point_flg);
                    if (pointCheckResult != null)
                    {
                        command.p_service = 0;
                        yield return pointCheckResult;
                    }
                    else
                    {
                        if (command.IsValidField("ent_point"))
                        {
                            //入力されたポイントはp_serviceへセット
                            command.p_service = command.ent_point;
                        }
                    }
                }

                ///非会員購入では購入できないように修正します。
                ///if non-member will not be able to use coupon
                if (!string.IsNullOrEmpty(command.coupon_code) && command.k_flg == EnumMemberEntryMode.ANONYMOUS)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("20244"), new[] { "coupon_code" });
                }

                if (command.k_flg != EnumMemberEntryMode.ANONYMOUS)
                {
                    //クーポンチェック
                    ////再計算ボタン未押下チェック
                    if (command.coupon_code != command.ent_coupon_code && !command.coupon_flg)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("20212"), new[] { "ent_coupon_code" });
                    }

                    //クーポン再計算時
                    if (command.coupon_flg)
                    {
                        if (command.IsValidField("ent_coupon_code"))
                        {
                            //入力されたポイントはp_serviceへセット
                            command.coupon_code = command.ent_coupon_code;
                        }
                    }
                }

                if (command.cart != null && command.cart.regularBasketItems != null && command.cart.basketItemCount > 0)
                {
                    foreach (var model in command.cart.regularBasketItems)
                    {
                        if (command.senddate == model.next_date && command.sendtime != command.regular_sendtime)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("20222"), new[] { "sendtime" });
                        }
                    }
                }
            }

            if (command.pri_chk == null)
                yield return new ValidationResult(ErsResources.GetMessage("20100"), new[] { "pri_chk" });


            //カートが全てメール便の場合は配送日・配送時刻指定不可
            //メール便が選択された場合は配送日・配送時刻指定不可
            if (command.firstTimeOrdinary == EnumFirstTimeOrdinary.Specify || (command.sendtime != null && command.sendtime != 0))
            {
                yield return ErsFactory.ersOrderFactory.GetCheckMailDeliveryStgy().CkSendDateTimeBasketMailDelivery(command.deliv_method, command.senddate, command.sendtime);
            }
        }

        public virtual void ValidateRegularOrder(IOrderRegistCommand command)
        {
            if (command.cart != null && command.cart.regularBasketItems != null && command.cart.regularBasketItems.Count() > 0)
            {
                throw new ErsException("20241");
            }
        }
    }
}