﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Register.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ers.Domain.Register.Handlers
{
    public class ValidatePaypalReturn
        : IValidationHandler<IPaypalReturnCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IPaypalReturnCommand command)
        {
            if (!string.IsNullOrEmpty(command.val))
            {
                var dic = ErsCommon.GetDictionaryWithHttpString(command.val, "$", "=");
                dic["token"] = command.token;
                dic["PayerID"] = command.PayerID;

                ErsBindModel.ModelBind(command, dic, false);
            }

            yield return command.CheckRequired("token");
            yield return command.CheckRequired("ssl_ransu");
            yield return command.CheckRequired("mcode");

            yield break;
        }
    }
}