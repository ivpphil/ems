﻿
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Models.Home;
using System;
using System.Collections.Generic;
using ers.Domain.Register.Commands;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;

namespace ers.Domain.Register.Handlers
{
    public class MemberEntryHandler : ICommandHandler<IMemberEntryCommand>
    {
        public ICommandResult Submit(IMemberEntryCommand command)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();

            var objMember = ErsFactory.ersMemberFactory.GetErsMember();
            objMember.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            objMember.pm_flg = this.EnvPM_flg();
            var age = ErsFactory.ersMemberFactory.GetGetAgeStgy().GetAge(objMember.birth);
            objMember.age_code = ErsFactory.ersMemberFactory.GetGetAgeCodeStgy().GetAgeCode(age);
            objMember.dm_flg = EnumDmFlg.Need;
            objMember.out_bound_flg = EnumOutBoundFlg.Need;
            objMember.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();
            repository.Insert(objMember, true);

            command.mcode = objMember.mcode;

            return new CommandResult(true);
        }

        /// <summary>
        /// 環境依存のデータセット
        /// </summary>
        public virtual EnumPmFlg EnvPM_flg()
        {
            return EnumPmFlg.PC;  //0:PC
        }
    }

}
