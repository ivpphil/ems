﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Cart.Handlers;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.basket;

namespace ers.Domain.Register.Handlers
{
    public class RegisterCartHandler
        : CartHandler
    {
        protected override void Refresh(ICartCommand command, ErsBasket basket, int? member_rank)
        {
            if (!string.IsNullOrEmpty(command.del_key))
            {
                basket.Remove(command.del_key);
            }
            else if (command.recompute)
            {
                if (basket.objBasketRecord.Count > 0)
                {
                    this.ReCompute(command, basket);
                }
            }
            else if (!string.IsNullOrEmpty(command.del_regular_key))
            {
                basket.RemoveRegular(command.del_regular_key);

            }
            else if (command.regular_recompute)
            {
                if (basket.objRegularBasketRecord.Count > 0)
                {
                    this.ReComputeRegular(command, basket, member_rank);
                }
            }
        }
    }
}