﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Register.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;

namespace ers.Domain.Register.Handlers
{
    public class ValidateOrderMemberRegist
        : IValidationHandler<IOrderMemberRegistCommand>
    {
        public IEnumerable<ValidationResult> Validate(IOrderMemberRegistCommand command)
        {
            yield return command.CheckRequired("k_flg");
            if (!command.IsValidField("k_flg"))
            {
                throw new ErsException("E01220001");
            }

            yield return command.CheckRequired("lname");
            yield return command.CheckRequired("fname");
            yield return command.CheckRequired("lnamek");
            yield return command.CheckRequired("fnamek");

            yield return command.CheckRequired("email");
            yield return command.CheckRequired("email_confirm");

            yield return command.CheckRequired("mformat");
            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("mformat", EnumCommonNameType.MFormat, (int?)command.mformat);

            yield return command.CheckRequired("tel");

            yield return command.CheckRequired("zip");
            yield return command.CheckRequired("pref");
            foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("pref", command.pref))
            {
                yield return result;
            }
            if (command.IsValidField("zip", "pref"))
            {
                yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("zip", command.zip, "pref", command.pref);
            }

            yield return command.CheckRequired("taddress");
            yield return command.CheckRequired("address");



            if (command.k_flg == EnumMemberEntryMode.MEMBER || command.k_flg == EnumMemberEntryMode.ANONYMOUS)
            {
                yield return ErsFactory.ersMemberFactory.GetCheckEmailConfirmStgy().CheckEmailConfirm(command.email, command.email_confirm);
            }

            //会員登録時
            if (command.needPersonalData)
            {
                yield return ErsFactory.ersMemberFactory.GetCheckEmailConfirmStgy().CheckEmailConfirm(command.email, command.email_confirm);

                yield return command.CheckRequired("birthday_y");
                yield return command.CheckRequired("birthday_m");
                yield return command.CheckRequired("birthday_d");

                foreach (var result in ErsFactory.ersMemberFactory.GetValidateBirthdayStgy().Validate(command.birthday_y, command.birthday_m, command.birthday_d))
                {
                    yield return result;
                }

                yield return command.CheckRequired("sex");
                yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("sex", EnumCommonNameType.Sex, (int?)command.sex);

                yield return command.CheckRequired("passwd");

                yield return ErsFactory.ersMemberFactory.GetCheckPasswdConfirmStgy().CheckPasswdConfirm(command.passwd, command.passwd_confirm);


                yield return command.CheckRequired("ques");
                foreach (var result in ErsFactory.ersMemberFactory.GetValidateQuesStgy().Validate(command.ques))
                {
                    yield return result;
                }
                yield return command.CheckRequired("ans");

                foreach (var result in ErsFactory.ersMemberFactory.GetValidateJobStgy().Validate(command.job))
                {
                    yield return result;
                }




            }

            if (command.k_flg != EnumMemberEntryMode.ANONYMOUS)
            {
                if (!string.IsNullOrEmpty(command.email))
                {
                    yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailStgy().CheckDuplicate(ErsContext.sessionState.Get("mcode"), command.email);
                }
                //yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailAndNameStgy().CheckDuplicate(ErsContext.sessionState.Get("mcode"), email, lname, fname);

                //yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailAndPasswordStgy().CheckDuplicate(ErsContext.sessionState.Get("mcode"), email, passwd);
            }
            if (!command.entry_submit && !command.cart.recompute && !command.cart.regular_recompute && !command.cart.del_key.HasValue() && !command.cart.del_regular_key.HasValue())
            {
                //For All item has total of 0 and bask_t.carriageFree == EnumCarriageFreeStatus.CARRIAGE_FREE

                //カード情報
                if (command.pay == EnumPaymentType.CREDIT_CARD && !command.IsNonNeededPaymentSpec)
                {
                    var member = ErsFactory.ersMemberFactory.getErsMemberWithRansu(ErsContext.sessionState);

                    if (!command.card_id.HasValue)
                    {
                        //クレジットカード入力時のチェック

                        //yield return command.CheckRequired("card_holder_name");
                        yield return command.CheckRequired("card");
                        yield return command.CheckRequired("cardno");
                        yield return command.CheckRequired("validity_y");
                        yield return command.CheckRequired("validity_m");

                        yield return ErsFactory.ersOrderFactory.GetCheckValidityStgy().Check(command.validity_y, command.validity_m);

                        if (command.card_save == EnumCardSave.Save)
                        {
                            if (member != null)
                            {
                                yield return ErsFactory.ersMemberFactory.GetValidateCardSaveCountStgy().Validate(member, command.del_card_id, new[] { "card_save" });
                            }
                        }
                    }

                    //使用クレジットカード選択時の入力チェック

                    yield return ErsFactory.ersMemberFactory.GetValidateDeleteCardStgy().Validate(member, command.card_id, command.card_save, command.del_card_id, new[] { "del_card_id" });
                }
                               
                //配送番号
                if (command.send == EnumSendTo.ANOTHER_ADDRESS)
                {
                    yield return command.CheckRequired("add_lname");
                    yield return command.CheckRequired("add_fname");
                    //yield return command.CheckRequired("add_lnamek"); //海外配送対応
                    //yield return command.CheckRequired("add_fnamek"); //海外配送対応
                    yield return command.CheckRequired("add_tel");
                    yield return command.CheckRequired("add_zip");
                    yield return command.CheckRequired("add_pref");
                    foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("add_pref", command.add_pref))
                    {
                        yield return result;
                    }
                    if (command.IsValidField("add_zip", "add_pref"))
                    {
                        yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("add_zip", command.add_zip, "add_pref", command.add_pref);
                    }

                    yield return command.CheckRequired("add_address");
                    yield return command.CheckRequired("add_taddress");
                    if (command.address_add == EnumAddressAdd.Add)
                    {
                        yield return command.CheckRequired("address_name");
                    }

                    //別住所でカード以外は不可
                    if(command.pay == EnumPaymentType.CREDIT_CARD)
                    { 
                        yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().ValidateRecipientName(
                        "pay", command.pay, command.send,
                        command.lname, command.add_lname,
                        command.fname, command.add_fname,
                        command.lnamek, command.add_lnamek,
                        command.fnamek, command.add_fnamek
                        );
                    }

                    if (command.pay != EnumPaymentType.CREDIT_CARD)
                    {
                        yield return command.CheckRequired("add_lnamek");
                        yield return command.CheckRequired("add_fnamek");                        
                    }

                    yield return ErsFactory.ersOrderFactory.GetValidateSendStgy().ValidateAnotherAddressSave("address_add", command.send, command.address_add, command.member_add_id, ErsContext.sessionState.Get("ransu"));
                }
                //20121115:定期商品を含みクレジット支払い、カード未保存はエラー
                yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().ValidateCardSave(command.pay, command.card_save, command.card_id, ErsContext.sessionState.Get("ransu"));

            }
        }
    }
}