﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Register.Commands;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ers.Domain.Register.Handlers
{
    public class OrderMemberCardDeleteHandler
        : ICommandHandler<IOrderMemberCardDeleteCommand>
    {
        public ICommandResult Submit(IOrderMemberCardDeleteCommand command)
        {
            this.Delete(command);

            return new CommandResult(true);
        }

        private void Delete(IOrderMemberCardDeleteCommand command)
        {
            if (command.k_flg != EnumMemberEntryMode.MEMBER || command.pay != EnumPaymentType.CREDIT_CARD || command.del_card_id == null)
            {
                return;
            }

            var ersGmo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD));

            foreach (var card_id in command.del_card_id)
            {
                try
                {
                    using (var tx = ErsDB_parent.BeginTransaction())
                    {
                        var cardInfo = ersGmo.ObtainMemberCardInfo(command.member, card_id);

                        //GMO側保持カードの削除　今回DBに登録する情報取得
                        var newErsMemberCard = ersGmo.deleteCardInfo(cardInfo, command.member);

                        //削除対象
                        var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                        var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
                        memberCardCriteria.active = EnumActive.Active;
                        memberCardCriteria.mcode = command.member.mcode;
                        memberCardCriteria.id = card_id;
                        var oldErsMemberCard = memberCardRepository.Find(memberCardCriteria).First();

                        //IVP側カード処理
                        memberCardRepository.Update(oldErsMemberCard, newErsMemberCard);

                        tx.Commit();
                    }
                }
                catch (Exception e)
                {
                    //カード削除はエラー表示させない。
                    //ログに落とす。
                    ErsCommon.loggingException(e.ToString());

                    if (ErsFactory.ersUtilityFactory.getSetup().debug)
                        throw;
                }
            }
        }
    }
}