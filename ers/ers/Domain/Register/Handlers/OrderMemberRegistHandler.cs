﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Register.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.state;

namespace ers.Domain.Register.Handlers
{
    public class OrderMemberRegistHandler
         : ICommandHandler<IOrderMemberRegistCommand>
    {
        public ICommandResult Submit(IOrderMemberRegistCommand command)
        {
            this.Insert(command);

            return new CommandResult(true);
        }

        private void Insert(IOrderMemberRegistCommand command)
        {
            var order = command.order;
            var member = command.member;

            //Update member data.
            if (command.k_flg == EnumMemberEntryMode.MEMBER)
            {
                member.lname = command.lname;
                member.fname = command.fname;
                member.lnamek = command.lnamek;
                member.fnamek = command.fnamek;
                member.compname = command.compname;
                member.compnamek = command.compnamek;
                member.division = command.division;
                member.divisionk = command.divisionk;
                member.email = command.email;
                member.mformat = command.mformat;
                member.tel = command.tel;
                member.fax = command.fax;
                member.zip = command.zip;
                member.pref = command.pref;
                member.address = command.address;
                member.taddress = command.taddress;
                member.maddress = command.maddress;

                var oldMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(member.mcode);
                var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
                repository.Update(oldMember, member);

                //クッキーの更新
                var nickName = command.member.lname + command.member.fname;
                ErsContext.sessionState.Add(ErsSessionState.nickNameKey, HttpUtility.UrlEncode(nickName, HttpContext.Current.Response.ContentEncoding));

                //emailをクッキー書き込み
                var objc = new OtherCookie();
                string cookie_email = objc.GetCoookieEmail("login_email");
                if (cookie_email.HasValue())
                {
                    objc.SetCookieEmail("login_email", command.member.email, ErsFactory.ersUtilityFactory.getSetup().cookieTimer);
                }

            }
            else if (command.k_flg == EnumMemberEntryMode.NO_MEMBER)
            {
                member.OverwriteWithParameter(command.GetPropertiesAsDictionary());

                member.intime = DateTime.Now;
                member.pm_flg = command.pm_flg;
                member.dm_flg = EnumDmFlg.Need;
                member.out_bound_flg = EnumOutBoundFlg.Need;

                var age = ErsFactory.ersMemberFactory.GetGetAgeStgy().GetAge(member.birth);
                member.age_code = ErsFactory.ersMemberFactory.GetGetAgeCodeStgy().GetAgeCode(age);

                var memberRepository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
                member.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();
                memberRepository.Insert(member, true);

                ErsContext.sessionState.Add("mcode", member.mcode);
                order.mcode = command.mcode;
            }
            else if (command.k_flg == EnumMemberEntryMode.ANONYMOUS)
            {
                member.OverwriteWithParameter(command.GetPropertiesAsDictionary());
            }

            //Regist Address Info
            if (command.address_add == EnumAddressAdd.Add && command.send == EnumSendTo.ANOTHER_ADDRESS)
            {
                var addressInfoRepository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
                var newAddressInfo = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithParameter(command.GetPropertiesAsDictionary());
                newAddressInfo.mcode = member.mcode;
                newAddressInfo.site_id = member.site_id;
                addressInfoRepository.Insert(newAddressInfo, true);

                command.member_add_id = newAddressInfo.id;
            }
            else if (command.member_add_id != null)
            {
                this.UpdateAnotherAddressInfo(command);
            }

            if (command.card_save == EnumCardSave.Save && !command.card_id.HasValue && command.pay == EnumPaymentType.CREDIT_CARD)
            {
                if (order.card_info == null)
                {
                    var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(command.pay);
                    objPayment.SetPaymentMethod(order, command);
                }

                var memberCard = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD)).SaveMemberCardInfo(order.card_info, member);
                var oldMemberCard = ErsFactory.ersMemberFactory.GetRetrieveAlreadyStoredMemberCardStgy().Retrieve(memberCard.card_mcode, memberCard.card_sequence);
                if (oldMemberCard == null)
                {
                    var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                    memberCardRepository.Insert(memberCard, true);
                }
                else
                {
                    memberCard = oldMemberCard;

                    //GMO側でカードシーケンスの再利用が発生した場合で、（すでに登録済みのカード番号が指定されたときなど）
                    //その番号が削除対象になっている場合は削除させない
                    if (command.del_card_id != null && command.del_card_id.Contains(memberCard.id))
                    {
                        command.del_card_id = command.del_card_id.Where((value) => value != memberCard.id).ToArray();
                    }
                }


                command.card_id = memberCard.id;

                //定期明細で使用しているカードを削除した場合に、カードシーケンスを新しく登録したカード番号で更新する。
                if (command.del_card_id != null)
                {
                    this.UpdateRegularOrderCardSequence(command, memberCard);
                }
            }
        }

        /// <summary>
        /// 定期明細で使用しているカードを削除した場合に、カードシーケンスを新しく登録したカード番号で更新する。
        /// </summary>
        /// <param name="command"></param>
        /// <param name="memberCard"></param>
        private void UpdateRegularOrderCardSequence(IOrderMemberRegistCommand command, global::jp.co.ivp.ers.member.ErsMemberCard memberCard)
        {
            var regularOrderRepository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            foreach (var card_id in command.del_card_id)
            {
                var regularOrderCriteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
                regularOrderCriteria.member_card_id = card_id;
                var listOrderRecord = regularOrderRepository.Find(regularOrderCriteria);
                foreach (var record in listOrderRecord)
                {
                    var oldRecord = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordWithId(record.id);
                    record.member_card_id = memberCard.id;
                    regularOrderRepository.Update(oldRecord, record);
                }
            }
        }

        /// <summary>
        /// Update shipping another address info.
        /// </summary>
        private void UpdateAnotherAddressInfo(IOrderMemberRegistCommand command)
        {
            var old_addressInfo = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(command.member_add_id.Value, command.mcode);
            var addressInfo = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(command.member_add_id.Value, command.mcode);

            addressInfo.OverwriteWithModel(command);
            addressInfo.address_name = old_addressInfo.address_name;
            addressInfo.intime = old_addressInfo.intime;

            var addressInfoRepository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            addressInfoRepository.Update(old_addressInfo, addressInfo);
        }
    }
}