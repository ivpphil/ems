﻿using System;
using System.Collections.Generic;
using System.Linq;
using ers.Domain.Register.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mall;

namespace ers.Domain.Register.Handlers
{
    public class OrderRegistHandler
        : ICommandHandler<IOrderRegistCommand>
    {
        public ICommandResult Submit(IOrderRegistCommand command)
        {
            ErsFactory.ersOrderFactory.GetSetD_noStgy().SetNext(command.order, null);

            this.Insert(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// 会員情報をインサート
        /// </summary>
        internal virtual void Insert(IOrderRegistCommand command)
        {
            var order = command.order;

            order.SetMemberAddId(command.member_add_id);

            order.SetMemberCardId(((IPaymentInfoGmoInputContainer)command).card_id);

            //Regist regular purchase order.
            if (command.cart.basket.objRegularBasketRecord.Count > 0)
            {
                ErsFactory.ersOrderFactory.GetRegistRegularOrderStgy().Regist(command, command.cart.basket, order, command.regular_sendtime);
            }

            var isMonitor = ErsFactory.ersOrderFactory.GetIsMonitorSpec().IsSpecified(command.cart.basket, command);

            var ccode = this.getCcode(command);

            //Prepares bills from order instance
            foreach (var innerOrder in order.GetListOrder())
            {
                this.PrepareBills(command, innerOrder.OrderHeader, innerOrder.OrderRecords, ccode);
            }

            //Update Member data
            if (command.k_flg != EnumMemberEntryMode.ANONYMOUS)
            {
                this.UpdateMember(command.member, order.subtotal);
            }

            //Decrease stock.
            var decreaseStockStgy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetDecreaseStockStgy();

            foreach (var baskRecord in command.orderRecords)
            {
                decreaseStockStgy.Decrease(baskRecord.scode, baskRecord.amount);
            }

            //Delete basket data
            ErsFactory.ersBasketFactory.GetEmptyBasketStrategy().EmptyBasket(ErsContext.sessionState.Get("ransu"));

            // モール在庫更新 [Update mall stock]
            var listParam = this.UpdateMallStock(command);

            try
            {
                //Executes payment with order instance
                //if (command.order_total != 0)
                //{
                foreach (var innerOrder in order.GetListOrder())
                {
                    this.ExecutePayment(command, innerOrder.OrderHeader, isMonitor);
                }
                //}
            }
            catch
            {
                if (listParam.Count > 0)
                {
                    try
                    {
                        // モール連携在庫リカバリ登録 [Register mall stock recovery]
                        ErsMallFactory.ersMallStockRecoveryFactory.GetRegisterMallStockRecoveryStgy().Register(listParam);
                    }
                    catch (Exception eTmp)
                    {
                        ErsCommon.loggingException(string.Format("Failed to register mall stock recovery.\r\n{0}", eTmp.ToString()));
                    }
                }

                throw;
            }
        }

        /// <summary>
        /// モール在庫更新 [Update mall stock]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>パラメータリスト [The list of parameter]</returns>
        protected IList<UpdateStockParam> UpdateMallStock(IOrderRegistCommand command)
        {
            var listParam = new List<UpdateStockParam>();

            foreach (var record in command.orderRecords)
            {
                if (record.h_mall_flg != EnumOnOff.On)
                {
                    continue;
                }

                if (record.set_flg != EnumSetFlg.IsNotSet)
                {
                    //子商品リスト取得
                    var DecreaseSetItemList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(record.scode);

                    foreach (var setProduct in DecreaseSetItemList)
                    {
                        var chiled_merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(setProduct.scode, null);

                        if (chiled_merchandise.h_mall_flg == EnumOnOff.Off)
                        {
                            continue;
                        }

                        UpdateStockParam param = default(UpdateStockParam);

                        param.productCode = setProduct.scode;
                        param.quantity = record.amount * setProduct.amount;
                        param.operation = EnumMallStockOperation.sub;

                        listParam.Add(param);
                    }
                }
                else
                {
                    UpdateStockParam param = default(UpdateStockParam);

                    param.productCode = record.scode;
                    param.quantity = record.amount;
                    param.operation = EnumMallStockOperation.sub;

                    listParam.Add(param);
                }
            }

            // モール在庫更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }

            return listParam;
        }

        /// <summary>
        /// 会員の購入情報を更新する。
        /// </summary>
        /// <param name="mcode"></param>
        /// <param name="total"></param>
        /// <param name="intime"></param>
        private void UpdateMember(ErsMember member, int subtotal)
        {
            var oldMember = ErsFactory.ersMemberFactory.getErsMemberWithParameter(member.GetPropertiesAsDictionary());
            
            //sale値を加算の為追加
            ErsFactory.ersOrderFactory.GetUpdateMemberDataStgy().Update(member, subtotal, 1, DateTime.Now);

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            repository.Update(oldMember, member);
        }

        /// <summary>
        /// 決済を行う。
        /// </summary>
        public void ExecutePayment(IOrderRegistCommand command, ErsOrder order, bool isMonitor)
        {
            if (command.order_total != 0)
            {
                var oldOrder = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(order.GetPropertiesAsDictionary());

                var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(order.pay);
                objPayment.SetPaymentMethod(order, command);

                objPayment.SendAuth(order, command.member, isMonitor);

                var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
                repository.Update(oldOrder, order);
            }
            //Use points
            if (command.k_flg == EnumMemberEntryMode.MEMBER && order.p_service > 0)
            {
                var reason = ErsResources.GetFieldName("point_reason_purchase");

                Setup setup = ErsFactory.ersUtilityFactory.getSetup();
                if (!setup.member_centralization)
                {
                    ErsFactory.ersOrderFactory.GetUpdateMemberPointStgy().Decrease(command.member.mcode, order.p_service, reason, order.d_no, order.intime, (int)order.site_id);
                }
                else
                {
                    var consumablePoints = ErsFactory.ersMemberFactory.GetObtainMemberPointStgy().GetPointHistory(ErsContext.sessionState.Get("mcode"),(int)order.site_id);
                    ErsFactory.ersOrderFactory.GetUpdateMemberPointStgy().Decrease(command.member.mcode, order.p_service, reason, order.d_no, order.intime, (int)consumablePoints.site_id);
                }
            }
        }

        public string getCcode(IOrderRegistCommand command)
        {
            if (command.cart.basket != null)
            {
                foreach (var rec in command.cart.basket.objBasketRecord.Values)
                {
                    if (rec.ccode != null)
                    {
                        return rec.ccode;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        protected void PrepareBills(IOrderRegistCommand command, ErsOrder order, IDictionary<string, ErsOrderRecord> orderRecords, string ccode)
        {
            // set payment to paid.
            if (order.total == 0)
            {
                order.order_payment_status = EnumOrderPaymentStatusType.PAID;
                order.pay = EnumPaymentType.NON_NEEDED_PAYMENT;
                order.paid_date = DateTime.Now;
            }
            else
            {
                order.order_payment_status = EnumOrderPaymentStatusType.NOT_PAID;
            }

            order.intime = DateTime.Now;
            command.intime = order.intime;
            order.pm_flg = command.pm_flg;

            order.mcode = command.member.mcode;

            order.ccode = ccode;

            //伝票
            var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            orderRepository.Insert(order, true);

            var orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var orderMailRepository = ErsFactory.ersOrderFactory.GetErsOrderMailRepository();

            foreach (var orderRecord in orderRecords.Values)
            {
                orderRecord.deliv_method = command.deliv_method;

                orderRecordRepository.Insert(orderRecord, true);

                var newOrderMail = this.GetOrderMail(orderRecord);
                orderMailRepository.Insert(newOrderMail);
            }

            //set items
            ErsFactory.ersOrderFactory.GetRegistSetItemsStgy().Regist(orderRecords.Values);

        }

        /// <summary>
        /// メール送信判定レコード取得
        /// </summary>
        /// <param name="objOrder"></param>
        /// <param name="orderRecord"></param>
        /// <returns></returns>
        private ErsOrderMail GetOrderMail(ErsOrderRecord orderRecord)
        {
            var newOrderMail = ErsFactory.ersOrderFactory.GetErsOrderMail();
            newOrderMail.d_no = orderRecord.d_no;
            newOrderMail.ds_id = orderRecord.id;
            newOrderMail.purchase_mail = EnumSentFlg.Sent;
            return newOrderMail;
        }
    }
}