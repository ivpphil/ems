﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Register.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order.related;

namespace ers.Domain.Register.Mappers
{
    public class OrderMapper
        : IMapper<IOrderMappable>
    {
        public void Map(IOrderMappable objMappable)
        {
            EnumFirstTimeOrdinary? firstTimeOrdinary = objMappable.firstTimeOrdinary;
            //If there is not ordinary order then sets the value which is the shortest date of regular order into the senddate.
            if (objMappable.cart.basket.objBasketRecord.Count == 0)
            {
                firstTimeOrdinary = EnumFirstTimeOrdinary.WithRegular;
                objMappable.firstTimeOrdinary = firstTimeOrdinary;
            }

            switch (firstTimeOrdinary)
            {
                case EnumFirstTimeOrdinary.Shortest:
                    objMappable.senddate = null;
                    break;
                case EnumFirstTimeOrdinary.WithRegular:
                    objMappable.senddate = objMappable.cart.basket.GetRegularShortestDate();
                    break;
            }

            objMappable.cart.basket.CalcRegularFirstDate(objMappable.weekend_operation);

            //reset card_sequence
            var order = ErsFactory.ersOrderFactory.GetErsOrderIntegrated();
            order.OverwriteWithParameter(objMappable.GetPropertiesAsDictionary());
            order.GenerateOrder(objMappable, objMappable.cart.basket, objMappable.deliv_method);

            var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(order.pay);
            objPayment.SetPaymentMethod(order, objMappable);
            
            objMappable.order = order;
            objMappable.ent_point = order.p_service;
            objMappable.p_service = order.p_service;

            //Set Delv Method
            //this.SetDefaultDelvMethod(objMappable);

            //Get CardInfo
            if (objMappable.card_id.HasValue)
            {
                objMappable.savedCardInfo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD)).ObtainMemberCardInfo(objMappable.member, objMappable.card_id);
            }

            if (!objMappable.entry_submit && !objMappable.cart.recompute && !objMappable.cart.regular_recompute)
            {
                this.SendSet(objMappable);
            }
        }

        /// <summary>
        ///  sendを再判定設定する
        /// </summary>
        protected virtual void SendSet(IOrderMappable objMappable)
        {
            //PCではスルーする
        }

        protected virtual void SetDefaultDelvMethod(IOrderMappable objMappable)
        {
            if (!objMappable.deliv_method.HasValue && objMappable.DisplayedDelvMethod == (short)EnumDelvMethod.Mail)
                objMappable.deliv_method = EnumDelvMethod.Mail;
            else if (!objMappable.deliv_method.HasValue && objMappable.DisplayedDelvMethod != (short)EnumDelvMethod.Mail)
                objMappable.deliv_method = EnumDelvMethod.Express;
        }
    }
}