﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Register.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers;

namespace ers.Domain.Register.Mappers
{
    public class PaypalReturnMapper
        : IMapper<IPaypalReturnMappable>
    {
        public void Map(IPaypalReturnMappable objMappable)
        {
            ErsContext.sessionState.Add("ssl_ransu", objMappable.ssl_ransu);
            ErsContext.sessionState.Add("mcode", objMappable.mcode);

            var paypal = (ErsPayPal)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.PAYPAL);

            paypal.GetExpressCheckout(new ErsModelBase[] { objMappable.register, objMappable.cart }, objMappable.token, objMappable.monitor_flg == 1);

            objMappable.register.token = objMappable.token;
            objMappable.register.PayerID = objMappable.PayerID;
            objMappable.register.paypalReturn = true;

            if (objMappable.register.k_flg == EnumMemberEntryMode.MEMBER)
            {
                ErsContext.SessionCheck();
            }
        }
    }
}