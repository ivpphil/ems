﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Register.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order.related;

namespace ers.Domain.Register.Mappers
{
    public class MemberOrderMapper
          : IMapper<IMemberOrderMappable>
    {
        public void Map(IMemberOrderMappable objMappable)
        {
            if (objMappable.IsConfirmationPage)
            {
                this.LoadDefaultData(objMappable);
            }
            else
            {
                this.LoadMemberData(objMappable);
            }
        }

        private void LoadMemberData(IMemberOrderMappable objMappable)
        {
            if (objMappable.k_flg == EnumMemberEntryMode.MEMBER)
            {
                //会員の場合は、modelに初期値を詰め込む
                var member = ErsFactory.ersMemberFactory.getErsMemberWithRansu(ErsContext.sessionState);
                objMappable.member = member;
                objMappable.OverwriteWithParameter(member.GetPropertiesAsDictionary());
                objMappable.email_confirm = member.email;
                if (member.birth != null)
                {
                    objMappable.birthday_y = member.birth.Value.Year;
                    objMappable.birthday_m = member.birth.Value.Month;
                    objMappable.birthday_d = member.birth.Value.Day;
                }

                //メモは初期化
                objMappable.memo = null;
            }
        }

        private void LoadDefaultData(IMemberOrderMappable objMappable)
        {
            ErsMember member;
            if (objMappable.k_flg == EnumMemberEntryMode.MEMBER)
            {
                member = ErsFactory.ersMemberFactory.getErsMemberWithRansu(ErsContext.sessionState);
                var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().Retrieve(member.mcode);

                var Result = ErsFactory.ersMemberFactory.GetErsMemberRankSetupSearchSpecification().SelectAsList(member.mcode);
                if (Result.Count > 0)
                {
                    objMappable.point_magnification = (int)Result[0]["point_magnification"];
                }
                else
                {
                    objMappable.point_magnification = 100;
                }
                var paymentCredit = (IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD);
                var listCreditCardInfo = paymentCredit.ObtainMemberCardInfo(member);
                objMappable.ListCreditCardInfo = new List<Dictionary<string, object>>();
                var hasRegularOrderSpec = ErsFactory.ersMemberFactory.GetIsCardHasRegularOrderSpec();
                foreach (var cardInfo in listCreditCardInfo)
                {
                    var dictionary = cardInfo.GetPropertiesAsDictionary();
                    dictionary["hasRegularOrder"] = hasRegularOrderSpec.Has(member.mcode, cardInfo.card_id);
                    objMappable.ListCreditCardInfo.Add(dictionary);
                }

                var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
                var addCri = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

                //検索条件をクライテリアに保存
                addCri.mcode = member.mcode;
                addCri.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);

                var addList = repository.Find(addCri);

                var addressList = new List<Dictionary<string, object>>();
                foreach (var address in addList)
                {
                    addressList.Add(address.GetPropertiesAsDictionary());
                }
                objMappable.addressList = addressList;

                //Get CardInfo
                if (objMappable.del_card_id != null)
                {
                    var listDelCardInfo = new List<CreditCardInfo>();
                    foreach (var card_id in objMappable.del_card_id)
                    {
                        var CardInfo = paymentCredit.ObtainMemberCardInfo(member, card_id);
                         if (CardInfo != null)
                         {
                             listDelCardInfo.Add(CardInfo);
                         }
                    }
                    objMappable.ListDelCreditCardInfo = listDelCardInfo;
                }
            }
            else
            {
                member = ErsFactory.ersMemberFactory.GetErsMember();

                if (objMappable.k_flg == EnumMemberEntryMode.NO_MEMBER)
                {
                    //default point_magnification
                    objMappable.point_magnification = 100;
                }
            }
            objMappable.member = member;
        }
    }
}