﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ers.Domain.Cart.Commands
{
    public interface ILPUpSellCartCommand
        : ILPCartCommand
    {
        int? upsell_amount { get; }

        bool IsAddCombinationUpSell { get; set; }
    }
}