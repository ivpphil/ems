﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.order;
using ers.Models.cart;
using jp.co.ivp.ers;

namespace ers.Domain.Cart.Commands
{
    public interface ICartCommand
         : ICommand, IManageRegularPatternDatasource
    {
        ErsBasket basket { get; set; }

        int? firstTime { get; }

        bool recompute { get; }

        bool regular_recompute { get; }

        int basketItemCount { get; }

        int? amount { get; }

        string ccode { get; }

        bool regular_basket_in { get; }

        string del_key { get; }

        string del_regular_key { get; }

        IEnumerable<Cart_items> basketItems { get; }

        IEnumerable<Cart_regular_items> regularBasketItems { get; }

        int subtotal { get; }

        int total_all_item { get; }

        short? DisplayedDelvMethod { get; set; }

        EnumMemberEntryMode? k_flg { get; set; }
    }
}