﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.lp;

namespace ers.Domain.Cart.Commands
{
    public interface ILPCartCommand
        : ICartCommand
    {        
        ErsLpPageManage lp_page_manage { get; }

        int? amount { get; }

        bool IsOrdinaryOrder { get; }

        bool IsRegularOrder { get; }

        string ransu { get; }

        bool HasConfirmPage { get; }

    }
}