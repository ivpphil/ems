﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using jp.co.ivp.ers.basket;

namespace ers.Domain.Cart.Commands
{
    public interface ICartRegularBasketRecordCommand
        : ICommand
    {
        EnumSendPtn? send_ptn { get; set; }
    }
}