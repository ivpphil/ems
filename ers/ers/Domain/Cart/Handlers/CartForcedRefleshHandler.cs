﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers;

namespace ers.Domain.Cart.Handlers
{
    public class CartForcedRefleshHandler
         : CartHandler, ICommandHandler<ICartForcedRefleshCommand>
    {
        public ICommandResult Submit(ICartForcedRefleshCommand command)
        {
            return base.Submit(command);
        }

        protected override void Refresh(ICartCommand command, ErsBasket basket, int? member_rank)
        {
            if (basket.objBasketRecord.Count > 0)
            {
                this.ReCompute(command, basket);
            }

            if (basket.objRegularBasketRecord.Count > 0)
            {
                this.ReComputeRegular(command, basket, member_rank);
            }
        }
    }
}