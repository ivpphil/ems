﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers;
using System.ComponentModel.DataAnnotations;

namespace ers.Domain.Cart.Handlers
{
    public class ValidateLPCart
        : IValidationHandler<ILPCartCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ILPCartCommand command)
        {
            this.ValidateSalesOrderType(command);

            if (command.regular_basket_in)
            {
                yield return command.CheckRequired("send_ptn");

                if (command.send_ptn == EnumSendPtn.DAY_INTERVALS)
                {
                    yield return command.CheckRequired("firstTime");

                    if (command.next_date == null & command.firstTime == 1)
                    {
                        yield return new ValidationResult(string.Format(ErsResources.GetMessage("10000"), ErsResources.GetFieldName("senddate")), new[] { "senddate" });
                    }
                }

                if (command.send_ptn.HasValue && command.send_ptn != EnumSendPtn.NORMAL)
                {
                    var validateRegularService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(command.send_ptn.Value);

                    foreach (var result in validateRegularService.Validate(command))
                    {
                        yield return result;
                    }
                }
            }
        }

        protected void ValidateSalesOrderType(ILPCartCommand command)
        {
            var result = true;

            if (!command.IsOrdinaryOrder && !command.IsRegularOrder)
            {
                result = false;
            }
            else if (!command.regular_basket_in && !command.IsOrdinaryOrder)
            {
                result = false;
            }
            else if (command.regular_basket_in && !command.IsRegularOrder)
            {
                result = false;
            }


            if (!result)
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
        }
    }
}