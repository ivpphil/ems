﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;

namespace ers.Domain.Cart.Handlers
{
    public class WishlistHandler
        : ICommandHandler<IWishlistCommand>
    {
        public ICommandResult Submit(IWishlistCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (!string.IsNullOrEmpty(command.scode))
            {
                //追加
                var repository = ErsFactory.ersMemberFactory.GetErsWishlistRepository();
                var criteria = ErsFactory.ersMemberFactory.GetErsWishlistCriteria();
                criteria.mcode = ((ErsSessionState)ErsContext.sessionState).GetDecodedMcode();
                criteria.scode = command.scode;
                if (repository.GetRecordCount(criteria) != 0)
                {
                    return new CommandResult(true);
                }

                var em = ErsFactory.ersMemberFactory.GetErsWishlistWithParameter(command.GetPropertiesAsDictionary());
                em.mcode = ((ErsSessionState)ErsContext.sessionState).GetDecodedMcode();
                em.site_id = setup.site_id;
                repository.Insert(em);
            }
            else if (!string.IsNullOrEmpty(command.del_scode))
            {
                //削除
                var repository = ErsFactory.ersMemberFactory.GetErsWishlistRepository();
                var criteria = ErsFactory.ersMemberFactory.GetErsWishlistCriteria();
                criteria.mcode = ((ErsSessionState)ErsContext.sessionState).GetDecodedMcode();
                criteria.scode = command.del_scode;
                criteria.site_id = setup.site_id;
                repository.Delete(criteria);
            }

            return new CommandResult(true);
        }
    }
}