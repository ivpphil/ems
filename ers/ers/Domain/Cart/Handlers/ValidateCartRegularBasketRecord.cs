﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ers.Domain.Cart.Handlers
{
    public class ValidateCartRegularBasketRecord
        : IValidationHandler<ICartRegularBasketRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICartRegularBasketRecordCommand command)
        {
            yield return command.CheckRequired("regular_key");
            yield return command.CheckRequired("amount");

            yield return command.CheckRequired("send_ptn");

            if (command.send_ptn != EnumSendPtn.NORMAL && command.IsValidField("send_ptn"))
            {
                var regularPatternService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(command.send_ptn.Value);

                foreach (var result in regularPatternService.Validate(command))
                {
                    yield return result;
                }
            }
        }
    }
}