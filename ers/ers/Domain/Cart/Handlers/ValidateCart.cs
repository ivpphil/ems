﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Cart.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Cart.Handlers
{
    public class ValidateCart
           : IValidationHandler<ICartCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICartCommand command)
        {
            if (command.scode != null)
            {
                //Check scode if existing
                var merchandise = ErsFactory.ersMerchandiseFactory.GetActiveErsMerchandiseWithScode(command.scode, null, ErsFactory.ersUtilityFactory.getSetup().site_id);
                if (merchandise == null)
                {
                    throw new ErsException("20000", command.scode);
                }
            }

            yield return this.ValidateDelivMethodCart(command);

            var controller = command.controller;
            if (command.basketItems != null)
            {
                foreach (var model in command.basketItems)
                {
                    model.AddInvalidField(controller.commandBus.Validate<ICartBasketRecordCommand>(model));

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "basketItems" });
                        }
                    }
                }
            }

            if (command.regularBasketItems != null)
            {
                foreach (var model in command.regularBasketItems)
                {
                    model.AddInvalidField(controller.commandBus.Validate<ICartRegularBasketRecordCommand>(model));

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "regularBasketItems" });
                        }
                    }
                }
            }

            if (command.regular_basket_in)
            {
                yield return command.CheckRequired("send_ptn");

                if (command.send_ptn == EnumSendPtn.DAY_INTERVALS)
                {
                    yield return command.CheckRequired("firstTime");

                    if (command.next_date == null & command.firstTime == 1)
                    {
                        yield return new ValidationResult(string.Format(ErsResources.GetMessage("10000"), ErsResources.GetFieldName("senddate")), new[] { "senddate" });
                    }
                }

                if (command.send_ptn.HasValue && command.send_ptn != EnumSendPtn.NORMAL)
                {
                    var validateRegularService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(command.send_ptn.Value);

                    foreach (var result in validateRegularService.Validate(command))
                    {
                        yield return result;
                    }
                }
            }

            if (command.ccode != null)
            {
                ErsFactory.ersOrderFactory.GetCheckCampaignAlreadyPurchasedStgy().ValidateBasket(ErsContext.sessionState.Get("ransu"), command.ccode);
            }

            if (command.basket.objBasketRecord.Count > 0)
            {
                if (command.k_flg == EnumMemberEntryMode.ANONYMOUS)
                {
                    if (command.basket.objBasketRecord.Any(p => p.Value.plural_order_type == EnumPluralOrderType.Once))
                        throw new ErsException("BT00200");
                }
            }

            yield break;
        }

        /// <summary>
        /// Validate Deliv Method of adding product
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public ValidationResult ValidateDelivMethodCart(ICartCommand command)
        {
            bool result = true;

            bool IsDeleteItem = !command.regular_basket_in && command.del_key.HasValue();


            if (!IsDeleteItem && command.scode.HasValue())
            {
                ErsFactory.ersBasketFactory.GetErsBasket().PrepareSession();
                string ransu = ErsContext.sessionState.Get("ransu");
                if (ErsFactory.ersBasketFactory.GetBasketDelivMethodMailMaxPurchaseSpec().IsSpecifiedByExistedMail(ransu, command.scode, command.regular_basket_in))
                    result = false;
                else
                {
                    var merchandise = ErsFactory.ersMerchandiseFactory.GetActiveErsMerchandiseWithScode(command.scode, null);
                    if (ErsFactory.ersBasketFactory.GetBasketDelivMethodMailMaxPurchaseSpec().IsSpecifiedByDelvMethodType(ransu, merchandise))
                        result = false;
                }

                //displays error message if one of two or more items has a delivery method of mail shipping (deliv_method=2)
                //remarks: can't be put in the basket with another item
                if (result == false)
                {
                    //get bask_t.scode with delivery method "Mail"
                    var specifiedMailScode = ErsFactory.ersBasketFactory.GetBasketDelivMethodMailMaxPurchaseSpec().scodeSpecifiedByExistedMail(ransu, command.scode, command.regular_basket_in);

                    //if scode is empty, set to given scode
                    if (specifiedMailScode == string.Empty)
                        specifiedMailScode = command.scode;
                    
                    
                    //get s_master_t record by scode
                    var sku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(specifiedMailScode);
                    
                    //get message with sname (product name)
                    return new ValidationResult(ErsResources.GetMessage("BT00100", sku.sname));
                }
            }
            
            return null;
        }
    }
}