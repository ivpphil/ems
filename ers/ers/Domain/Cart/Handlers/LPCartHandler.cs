﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.db;

namespace ers.Domain.Cart.Handlers
{
    public class LPCartHandler
        : ICommandHandler<ILPCartCommand>
    {
        public ICommandResult Submit(ILPCartCommand command)
        {
            var mcode = ErsContext.sessionState.Get("mcode");
            var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().Retrieve(mcode);

            var basket = ErsFactory.ersBasketFactory.GetErsLPBasket();

            basket.Init(mcode, command.ransu);
            this.Refresh(command, basket, member_rank);

            this.SetNonConfirmBasket(command, basket);

            return new CommandResult(true);
        }

        protected virtual void SetNonConfirmBasket(ILPCartCommand command, ErsBasket basket)
        {
            if (!command.HasConfirmPage)
                command.basket = basket;
        }

        protected virtual void Refresh(ILPCartCommand command, ErsBasket basket, int? member_rank)
        {
            string scode = command.lp_page_manage.sell_scode;

            this.ClearNonLandingPageBasket(basket, command.ransu);

            if (!string.IsNullOrEmpty(scode))
            {
                ErsBaskRecord merchandise;
                if (command.regular_basket_in)
                {
                    var service = ErsFactory.ersOrderFactory.GetManageRegularPatternService(command.send_ptn.Value);
                    merchandise = service.GetMerchandise(basket.ransu, scode, command, member_rank);
                }
                else
                {
                    merchandise = ErsFactory.ersBasketFactory.GetErsBaskRecordWithScode(basket.ransu, scode, member_rank);
                    if (merchandise != null)
                    {
                        merchandise.send_ptn = EnumSendPtn.NORMAL;
                    }
                }

                if (merchandise != null)
                {
                    this.SetMerchandisePrice(command, merchandise);
                    basket.Add(merchandise, command.amount.Value);
                }

               
            }
        }

        internal virtual bool CheckLPMaxBasketCount(ILPCartCommand command, ErsBasket basket)
        {
            return (basket.objBasketRecord.Count != 1 && basket.objBasketRecord.Count != 0
                || basket.objRegularBasketRecord.Count != 1 && basket.objRegularBasketRecord.Count != 0);
        }

        internal virtual bool CheckMerchandiseKeys(ILPCartCommand command, ErsBasket basket, IEnumerable<ErsBaskRecord> merchandiseList)
        {
            bool result = true;

            foreach (var merchandise in merchandiseList)
            {
                if (merchandise != null)
                {
                    if (this.CheckIsNewItem(merchandise, basket))
                        result = false;
                }
            }

            return result;
        }

        internal virtual void SetMerchandisePrice(ILPCartCommand command, ErsBaskRecord merchandise)
        {
            merchandise.ccode = command.lp_page_manage.ccode;

            if (command.regular_basket_in)
            {
                merchandise.price = command.lp_page_manage.sell_first_regular_price;
                merchandise.regular_price = command.lp_page_manage.sell_regular_price;

                var regularPatternService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(merchandise.send_ptn.Value);
                merchandise.next_date = regularPatternService.CalculateFirstTime(merchandise, DateTime.Now);
            }
            else
            {
                if (command.lp_page_manage.sell_discount_flg == EnumUse.Use && command.amount >= command.lp_page_manage.sell_discount_amount
                    && command.lp_page_manage.sell_discount_price < command.lp_page_manage.sell_price)
                {
                    merchandise.price = command.lp_page_manage.sell_discount_price;
                }
                else
                {
                    merchandise.price = command.lp_page_manage.sell_price;
                }
            }
        }

        /// <summary>
        /// recompute basket items
        /// </summary>
        protected virtual void ReCompute(ILPCartCommand command, ErsBasket basket, string merchandiseKey, int amount)
        {
            //recompute ordinary order
            var up_amount_list = new Dictionary<string, int?>();

            up_amount_list.Add(merchandiseKey, amount);

            if (command.regular_basket_in)
            {
                if (basket.objRegularBasketRecord.Count > 0)
                {
                    basket.ReComputeRegularRecord(command, merchandiseKey, amount, null, null);
                }
            }
            else
            {
                var mcode = ErsContext.sessionState.Get("mcode");
                basket.ReCompute(mcode, up_amount_list);
            }
        }

        internal bool CheckIsNewItem(ErsBaskRecord merchandise, ErsBasket basket)
        {
            if (basket.objBasketRecord.ContainsKey(ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(merchandise))
                && basket.objBasketRecord.Count > 0)
            {
                return false;
            }

            if (basket.objRegularBasketRecord.ContainsKey(ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(merchandise))
                && basket.objRegularBasketRecord.Count > 0)
            {
                return false;
            }

            return true;

        }

        protected virtual void ClearNonLandingPageBasket(ErsBasket basket, string ransu)
        {
            ErsFactory.ersBasketFactory.GetEmptyBasketStrategy().EmptyBasket(ransu);
            basket.objRegularBasketRecord.Clear();
            basket.objBasketRecord.Clear();

        }

        protected virtual void RemoveBaskRecordByKey(ErsBaskRecord merchandise, ErsBasket basket)
        {
            var merchandiseKey = ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(merchandise);

            if (basket.objBasketRecord.ContainsKey(merchandiseKey))
            {
                basket.Remove(merchandiseKey);
            }

            if (basket.objRegularBasketRecord.ContainsKey(merchandiseKey))
            {
                basket.RemoveRegular(merchandiseKey);
            }
        }
    }
}