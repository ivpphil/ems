﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers;
using ers.Models.cart;
using ers.Domain.Cart.Mappables;

namespace ers.Domain.Cart.Handlers
{
    public class CartHandler
         : ICommandHandler<ICartCommand>
    {
        public ICommandResult Submit(ICartCommand command)
        {
            var mcode = ErsContext.sessionState.Get("mcode");
            var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().Retrieve(mcode);

            var basket =ErsFactory.ersBasketFactory.GetErsBasket();

            basket.PrepareSession();
            basket.Init(mcode, ErsContext.sessionState.Get("ransu"));

            this.Refresh(command, basket, member_rank);

            return new CommandResult(true);
        }

        protected virtual void Refresh(ICartCommand command, ErsBasket basket, int? member_rank)
        {
            if (!string.IsNullOrEmpty(command.scode))
            {
                ErsBaskRecord merchandise;
                if (command.regular_basket_in)
                {
                    var service = ErsFactory.ersOrderFactory.GetManageRegularPatternService(command.send_ptn.Value);
                    merchandise = service.GetMerchandise(basket.ransu, command.scode, command, member_rank);
                }
                else
                {
                    merchandise = ErsFactory.ersBasketFactory.GetErsBaskRecordWithScode(basket.ransu, command.scode, member_rank);
                    if (merchandise != null)
                    {
                        merchandise.send_ptn = EnumSendPtn.NORMAL;
                    }
                }
                if (merchandise != null)
                {
                    if (command.ccode != null)
                    {
                        var ccode = command.ccode.ToUpper();
                        var cp_price =  this.getCampaignPrice(ccode, merchandise.scode);
                        merchandise.price = cp_price.price;
                        merchandise.price2 = cp_price.price2;
                        merchandise.ccode = ccode;
                    }

                    // カートへ商品を投入 [Insert product to cart]
                    this.InsertProductToCart(command, merchandise, basket, member_rank);
                }
            }
            else if (!string.IsNullOrEmpty(command.del_key))
            {
                basket.Remove(command.del_key);
            }
            else if (command.recompute)
            {
                if (basket.objBasketRecord.Count > 0)
                {
                    this.ReCompute(command, basket);
                }
            }
            else if (!string.IsNullOrEmpty(command.del_regular_key))
            {
                basket.RemoveRegular(command.del_regular_key);
            }
            else if (command.regular_recompute)
            {
                if (basket.objRegularBasketRecord.Count > 0)
                {
                    this.ReComputeRegular(command, basket, member_rank);
                }
            }
        }

        /// <summary>
        /// recompute basket items
        /// </summary>
        protected void ReCompute(ICartCommand command, ErsBasket basket)
        {
            //recompute ordinary order
            var up_amount_list = new Dictionary<string, int?>();
            if (command.basketItems != null)
            {
                foreach (var item in command.basketItems)
                {
                    up_amount_list.Add(item.key, item.amount.Value);
                }
            }

            var mcode = ErsContext.sessionState.Get("mcode");
            basket.ReCompute(mcode, up_amount_list);
        }

        /// <summary>
        /// recompute basket items
        /// </summary>
        protected void ReComputeRegular(ICartCommand command, ErsBasket basket, int? member_rank)
        {
            //recompute regular order
            basket.ReComputeRegular(command.regularBasketItems, member_rank);
        }

        /// <summary>
        /// キャンペーン価格取得
        /// </summary>
        /// <param name="ccode"></param>
        /// <param name="scode"></param>
        /// <returns></returns>
        private ErsPrice getCampaignPrice(string ccode, string scode)
        {
            var campaign_price = ErsFactory.ersMerchandiseFactory.GetErsPrice();
            var cri = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
            var rep = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();
            cri.ccode = ccode;
            cri.scode = scode;
            var campaign_price_list = rep.Find(cri);
            if (campaign_price_list.Count == 1)
            {
                campaign_price = campaign_price_list[0];
            }

            return campaign_price;
        }

        /// <summary>
        /// カートへ商品を投入 [Insert product to cart]
        /// </summary>
        /// <param name="command">ICartCommand</param>
        /// <param name="merchandise">ErsBaskRecord</param>
        /// <param name="basket">ErsBasket</param>
        protected virtual void InsertProductToCart(ICartCommand command, ErsBaskRecord merchandise, ErsBasket basket, int? member_rank)
        {
            if (merchandise != null)
            {
                var amount = command.amount == null ? 1 : command.amount;
                var key = ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(merchandise);

                if (basket.objBasketRecord.ContainsKey(key))
                {
                    // カート商品数量更新 [Update cart product amount]
                    this.UpdateCartProductAmount(command, merchandise, amount, basket);
                }
                else if (basket.objRegularBasketRecord.Any(e => 
                    e.Value.scode == merchandise.scode &&
                    e.Value.price == merchandise.price &&
                    e.Value.send_ptn == merchandise.send_ptn &&
                    e.Value.ptn_interval_month == merchandise.ptn_interval_month &&
                    e.Value.ptn_interval_week == merchandise.ptn_interval_week &&
                    e.Value.ptn_interval_day == merchandise.ptn_interval_day &&
                    e.Value.ptn_day == merchandise.ptn_day &&
                    e.Value.ptn_weekday == merchandise.ptn_weekday))
                {
                    // カート定期商品数量更新 [Update cart regular product amount]
                    this.UpdateCartRegularProductAmount(command, merchandise, amount, basket, member_rank);
                }
                else
                {
                    basket.Add(merchandise, amount.Value);
                }
            }
        }

        /// <summary>
        /// カート商品数量更新 [Update cart product amount]
        /// </summary>
        /// <param name="command">ICartCommand</param>
        /// <param name="merchandise">ErsBaskRecord</param>
        /// <param name="amount">int?</param>
        /// <param name="basket">ErsBasket</param>
        protected virtual void UpdateCartProductAmount(ICartCommand command, ErsBaskRecord merchandise, int? amount, ErsBasket basket)
        {
            var controller = command.controller;
            var basketItems = new List<Cart_items>();
            foreach (var itemData in basket.objBasketRecord.Values)
            {
                var cartItem = new Cart_items();
                cartItem.baskRecord = itemData;
                cartItem.containerModel = command;
                controller.mapperBus.Map<ICartBasketRecordMappable>(cartItem);
                if (cartItem.scode.HasValue())
                {
                    basketItems.Add(cartItem);
                }
            }

            var up_amount_list = new Dictionary<string, int?>();

            if (basketItems != null)
            {
                foreach (var item in basketItems)
                {
                    if (item.scode == merchandise.scode)
                    {
                        up_amount_list.Add(item.key, item.amount.Value + amount.Value);
                    }
                }
            }

            var mcode = ErsContext.sessionState.Get("mcode");
            basket.ReCompute(mcode, up_amount_list, null);
        }

        /// <summary>
        /// カート定期商品数量更新 [Update cart regular product amount]
        /// </summary>
        /// <param name="command">ICartCommand</param>
        /// <param name="merchandise">ErsBaskRecord</param>
        /// <param name="amount">int?</param>
        /// <param name="basket">ErsBasket</param>
        protected virtual void UpdateCartRegularProductAmount(ICartCommand command, ErsBaskRecord merchandise, int? amount, ErsBasket basket, int? member_rank)
        {
            var controller = command.controller;
            var basketItems = new List<Cart_regular_items>();
            foreach (var itemData in basket.objRegularBasketRecord.Values)
            {
                var cartItem = new Cart_regular_items();
                cartItem.baskRecord = itemData;
                cartItem.containerModel = command;
                controller.mapperBus.Map<ICartRegularBasketRecordMappable>(cartItem);

                if (cartItem.scode == merchandise.scode &&
                    cartItem.price == merchandise.price &&
                    cartItem.send_ptn == merchandise.send_ptn &&
                    cartItem.ptn_interval_month == merchandise.ptn_interval_month &&
                    cartItem.ptn_interval_week == merchandise.ptn_interval_week &&
                    cartItem.ptn_interval_day == merchandise.ptn_interval_day &&
                    cartItem.ptn_day == merchandise.ptn_day &&
                    cartItem.ptn_weekday == merchandise.ptn_weekday)
                {
                    cartItem.amount += amount;
                }

                basketItems.Add(cartItem);
            }

            basket.ReComputeRegular(basketItems, member_rank);
        }
    }
}