﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ers.Domain.Cart.Handlers
{
    public class LPUpSellCartHandler
        : LPCartHandler, ICommandHandler<ILPUpSellCartCommand>
    {
        public ICommandResult Submit(ILPUpSellCartCommand command)
        {
            return base.Submit(command);
        }

        protected override void Refresh(ILPCartCommand command, ErsBasket basket, int? member_rank)
        {
            ErsBaskRecord lp_merchandise = null;
            var lp_upsell_cart_command = command as ILPUpSellCartCommand;

            string scode = lp_upsell_cart_command.lp_page_manage.upsell_scode;

            this.ClearNonLandingPageBasket(basket, command.ransu);

            var upsell_merchandise = this.GetBaskMerchandise(lp_upsell_cart_command, basket, member_rank, scode);

            if (lp_upsell_cart_command.lp_page_manage.upsell_stgy_kbn == EnumLpUpsellStgy.Combination)
            {
                lp_upsell_cart_command.IsAddCombinationUpSell = true;
                lp_merchandise = this.GetBaskMerchandise(lp_upsell_cart_command, basket, member_rank, lp_upsell_cart_command.lp_page_manage.sell_scode);
            }


            this.RefreshByMerchandise(lp_upsell_cart_command, basket, upsell_merchandise, lp_upsell_cart_command.upsell_amount);

            if (lp_upsell_cart_command.lp_page_manage.upsell_stgy_kbn == EnumLpUpsellStgy.Combination)
                this.RefreshByMerchandise(lp_upsell_cart_command, basket, lp_merchandise, lp_upsell_cart_command.amount);
        }

        protected ErsBaskRecord GetBaskMerchandise(ILPUpSellCartCommand command, ErsBasket basket, int? member_rank, string scode)
        {
            ErsBaskRecord merchandise = null;

            if (!string.IsNullOrEmpty(scode))
            {
                if (command.regular_basket_in)
                {
                    var service = ErsFactory.ersOrderFactory.GetManageRegularPatternService(command.send_ptn.Value);
                    merchandise = service.GetMerchandise(basket.ransu, scode, command, member_rank);
                }
                else
                {
                    merchandise = ErsFactory.ersBasketFactory.GetErsBaskRecordWithScode(basket.ransu, scode, member_rank);
                    if (merchandise != null)
                    {
                        merchandise.send_ptn = EnumSendPtn.NORMAL;
                    }
                }

                if (merchandise != null)
                {
                    this.SetMerchandisePrice(command, merchandise);
                }
            }

            return merchandise;
        }

        protected void RefreshByMerchandise(ILPUpSellCartCommand command, ErsBasket basket, ErsBaskRecord merchandise, int? amount)
        {
            if (merchandise != null)
            {
                basket.Add(merchandise, amount.Value);
            }
        }

        internal override void SetMerchandisePrice(ILPCartCommand command, ErsBaskRecord merchandise)
        {
            var lp_upsell_cart_command = command as ILPUpSellCartCommand;
            merchandise.ccode = command.lp_page_manage.ccode;

            if (command.regular_basket_in)
            {
                this.SetMerchandiseRegularPrice(lp_upsell_cart_command, merchandise);
            }
            else
            {
                this.SetOrdinaryPrice(lp_upsell_cart_command, merchandise);
            }
        }

        internal virtual void SetMerchandiseRegularPrice(ILPUpSellCartCommand command, ErsBaskRecord merchandise)
        {
            this.SetRegularPrice(command, merchandise);

            var regularPatternService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(merchandise.send_ptn.Value);
            merchandise.next_date = regularPatternService.CalculateFirstTime(merchandise, DateTime.Now);
        }

        internal virtual void SetRegularPrice(ILPUpSellCartCommand command, ErsBaskRecord merchandise)
        {
            if (!command.IsAddCombinationUpSell)
            {
                merchandise.price = command.lp_page_manage.upsell_first_regular_price;
                merchandise.regular_price = command.lp_page_manage.upsell_regular_price;
            }
            else
            {
                merchandise.price = command.lp_page_manage.sell_first_regular_price;
                merchandise.regular_price = command.lp_page_manage.sell_regular_price;
            }
        }

        internal virtual void SetOrdinaryPrice(ILPUpSellCartCommand command, ErsBaskRecord merchandise)
        {
            if (!command.IsAddCombinationUpSell)
            {
                if (this.IsDiscountedPrice(command.lp_page_manage.upsell_discount_flg, command.upsell_amount, command.lp_page_manage.upsell_discount_amount)
                    && command.lp_page_manage.upsell_discount_price < command.lp_page_manage.upsell_price)
                {
                    merchandise.price = command.lp_page_manage.upsell_discount_price;
                }
                else
                {
                    merchandise.price = command.lp_page_manage.upsell_price;
                }
            }
            else
            {
                if (this.IsDiscountedPrice(command.lp_page_manage.sell_discount_flg, command.amount, command.lp_page_manage.sell_discount_amount)
                    && command.lp_page_manage.sell_upsell_price < command.lp_page_manage.sell_price)
                {
                    merchandise.price = command.lp_page_manage.sell_upsell_price;
                }
                else
                {
                    merchandise.price = command.lp_page_manage.sell_price;
                }   
            }
        }

        internal override bool CheckLPMaxBasketCount(ILPCartCommand command, ErsBasket basket)
        {
            if (command.lp_page_manage.upsell_stgy_kbn == EnumLpUpsellStgy.Combination)
            {
                return (basket.objRegularBasketRecord.Count != 2 && basket.objRegularBasketRecord.Count != 0
                    || basket.objBasketRecord.Count != 2 && basket.objBasketRecord.Count != 0);
            }

            return base.CheckLPMaxBasketCount(command, basket);

        }

        internal virtual bool IsDiscountedPrice(EnumUse? discount_flg, int? amount, int? sell_discount_amount)
        {
            return (discount_flg == EnumUse.Use && amount >= sell_discount_amount);
        }
    }
}