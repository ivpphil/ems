﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Cart.Handlers
{
    public class ValidateCartBasketRecord
        : IValidationHandler<ICartBasketRecordCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ICartBasketRecordCommand command)
        {
            yield return command.CheckRequired("key");
            yield return command.CheckRequired("amount");
        }
    }
}