﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ers.Domain.Cart.Mappables
{
    public interface IWishlistMappable
        : IMappable
    {
        List<Dictionary<string, object>> listItem { set; }

        ErsPagerModel pager { get; set; }

        long recordCount { get; set; }
    }
}