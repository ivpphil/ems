﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ers.Models.cart;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers;

namespace ers.Domain.Cart.Mappables
{
    public interface ICartRegularBasketRecordMappable
         : ICartBasketRecordMappable, IMappable
    {
        string regular_key { get; set; }

        EnumSendPtn? send_ptn { get; set; }
    }
}