﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ers.Models.cart;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers;

namespace ers.Domain.Cart.Mappables
{
    public interface ICartBasketRecordMappable
         : IMappable
    {
        ErsBaskRecord baskRecord { get; set; }

        string scode { get; set; }

        int? amount { get; set; }

        string key { get; set; }

        string sname { get; set; }

        string m_sname { get; set; }

        string attribute1 { get; set; }

        string attribute2 { get; set; }

        int? price { get; set; }

        int? total { get; set; }

        EnumCarriageCostType? carriage_cost_type { get; set; }

        EnumPluralOrderType? plural_order_type { get; set; }

        string ccode { get; set; }

        int? max_purchase_count { get; set; }
    }
}