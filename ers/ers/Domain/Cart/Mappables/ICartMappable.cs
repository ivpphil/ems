﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ers.Models.cart;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers;

namespace ers.Domain.Cart.Mappables
{
    public interface ICartMappable
         : IMappable
    {
        IEnumerable<Cart_items> basketItems { get; set; }

        IEnumerable<Cart_regular_items> regularBasketItems { get; set; }

        int total_all_item { get; }

        int amounttotal_all_item { get; }

        string scode { get; }

        List<Dictionary<string, object>> recommendItems { set; }

        ErsBasket basket { set; }

        bool IsRegister { get; set; }

        short? DisplayedDelvMethod { get; set; }
    }
}