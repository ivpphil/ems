﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ers.Domain.Cart.Mappables
{
    public interface ILPCartMappable
        : ICartMappable
    {
        string ransu { get; }

        EnumUse? lp_carriage_free_flg { get; }

        int? lp_carriage_free_price { get; }
    }
}