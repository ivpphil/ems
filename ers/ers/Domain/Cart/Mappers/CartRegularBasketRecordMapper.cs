﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Cart.Mappables;
using jp.co.ivp.ers.mvc;
using ers.Models.cart;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers;

namespace ers.Domain.Cart.Mappers
{
    public class CartRegularBasketRecordMapper
        : CartBasketRecordMapper, IMapper<ICartRegularBasketRecordMappable>
    {
        public void Map(ICartRegularBasketRecordMappable objMappable)
        {
            this.LoadDefaultValue(objMappable);
        }

        protected virtual void LoadDefaultValue(ICartRegularBasketRecordMappable objMappable)
        {
            var merchandise = objMappable.baskRecord;

            objMappable.OverwriteWithParameter(merchandise.GetPropertiesAsDictionary());

            objMappable.regular_key = ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(merchandise);
        }

        protected virtual void LoadDisplayValue(ICartRegularBasketRecordMappable objMappable)
        {
            var merchandise = objMappable.baskRecord;

            base.LoadDisplayValue(objMappable);

            objMappable.send_ptn = merchandise.send_ptn;
        }
    }
}