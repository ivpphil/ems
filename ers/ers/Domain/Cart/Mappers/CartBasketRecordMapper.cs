﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Cart.Mappables;
using jp.co.ivp.ers.mvc;
using ers.Models.cart;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ers.Domain.Cart.Mappers
{
    public class CartBasketRecordMapper
        : IMapper<ICartBasketRecordMappable>
    {
        public void Map(ICartBasketRecordMappable objMappable)
        {
            this.LoadDefaultValue(objMappable);
        }

        protected virtual void LoadDefaultValue(ICartBasketRecordMappable objMappable)
        {
            var merchandise = objMappable.baskRecord;

            var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().RetrieveWithSession();

            //if from register entry message will not be displayed
            if (objMappable.controller.ToString().Contains("cart"))
            {
                var merchandiseRec = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(merchandise.scode, member_rank);
                if (merchandiseRec == null)
                {
                    return;
                }

                if (!ErsFactory.ersMerchandiseFactory.GetChecksActivenessOfMerchandiseSpec().Check(merchandiseRec))
                {
                    var setup = ErsFactory.ersUtilityFactory.getSetup();
                    objMappable.controller.AddInformation(ErsResources.GetMessage("20006", merchandise.scode) + ErsResources.GetMessage("20008"));
                }
            }

            this.LoadDisplayValue(objMappable);

            objMappable.scode = merchandise.scode;
            objMappable.amount = merchandise.amount;
            objMappable.max_purchase_count = merchandise.max_purchase_count;

            objMappable.key = ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(merchandise);
        }

        protected virtual void LoadDisplayValue(ICartBasketRecordMappable objMappable)
        {
            var merchandise = objMappable.baskRecord;

            objMappable.sname = merchandise.sname;
            if (merchandise.m_sname != null)
            {
                objMappable.m_sname = VBStrings.HalfKana(merchandise.m_sname);
            }
            else
            {
                objMappable.m_sname = VBStrings.HalfKana(merchandise.sname);
            }
            objMappable.attribute1 = merchandise.attribute1;
            objMappable.attribute2 = merchandise.attribute2;

            objMappable.price = merchandise.price;
            objMappable.total = merchandise.total;
            objMappable.carriage_cost_type = merchandise.carriage_cost_type;
            objMappable.plural_order_type = merchandise.plural_order_type;
            objMappable.ccode = merchandise.ccode;
        }
    }
}