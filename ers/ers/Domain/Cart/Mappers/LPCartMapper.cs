﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Cart.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.basket;

namespace ers.Domain.Cart.Mappers
{
    public class LPCartMapper
        : CartMapper, IMapper<ILPCartMappable>
    {
        public void Map(ILPCartMappable objMappable)
        {
            var mcode = ErsContext.sessionState.Get("mcode");
            var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().Retrieve(mcode);


            var basket = ErsFactory.ersBasketFactory.GetErsLPBasket();
            this.SetCarriageFree(objMappable, basket);
            objMappable.basket = basket;

            //basket.PrepareSession();
            basket.Init(mcode, objMappable.ransu);
            if (objMappable.IsRegister)
            {
                basket.CheckBasketItems(false);
            }


            this.RefreshView(objMappable, basket);
            this.SetRecommend(objMappable, basket, member_rank);
            //this.SetBasketDataToCookie(objMappable);
        }

        protected virtual void SetCarriageFree(ILPCartMappable objMappable, ErsLPBasket basket)
        {
            basket.carriage_free_flg = objMappable.lp_carriage_free_flg;
            basket.lp_carriage_free = objMappable.lp_carriage_free_price;
        }
    }
}