﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Cart.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;

namespace ers.Domain.Cart.Mappers
{
    public class WishlistMapper
        : IMapper<IWishlistMappable>
    {
        public void Map(IWishlistMappable objMappable)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsWishlistRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsWishlistCriteria();
            criteria.mcode = ((ErsSessionState)ErsContext.sessionState).GetDecodedMcode();
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            objMappable.recordCount = repository.GetRecordCount(criteria);
           
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);

            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            var list = repository.FindWishListItemList(criteria);


            
            var listItem = new List<Dictionary<string, object>>();
            foreach (var merchandise in list)
            {
                var viewDictionary = merchandise.GetPropertiesAsDictionary();

                var objstock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(merchandise.scode);
                merchandise.stock = objstock.stock;

                viewDictionary["StockStatus"] = ErsFactory.ersMerchandiseFactory.GetStockStatusSpecification().GetStockStatusOfMerchandise(merchandise);

                if ((merchandise.s_sale_ptn == EnumSalePatternType.REGULAR) || (merchandise.s_sale_ptn == EnumSalePatternType.ALL))
                {
                    viewDictionary["sale_regular_flg"] = true;
                    viewDictionary["non_member_regular_first_price"] = merchandise.regular_first_price;
                    viewDictionary["non_member_regular_price"] = merchandise.regular_price;

                }
                else
                {
                    viewDictionary["sale_regular_flg"] = false;
                }
                if (merchandise.price != null)
                {
                    viewDictionary["price"] = merchandise.price;
                }
                else
                {
                    viewDictionary["price"] = merchandise.regular_first_price;
                }                
                viewDictionary["price2"] = merchandise.price2;

                if (ErsFactory.ersMerchandiseFactory.GetOnCampaignSpecification().IsSatisfiedBy(merchandise.p_date_from, merchandise.p_date_to))
                {
                    viewDictionary["price"] = merchandise.sale_price;
                    viewDictionary["default_price"] = merchandise.price;
                    viewDictionary["onCampaign"] = true;
                }

                listItem.Add(viewDictionary);
            }
            
            objMappable.listItem = listItem;
        }
    }
}