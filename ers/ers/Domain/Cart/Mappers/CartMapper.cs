﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Cart.Mappables;
using jp.co.ivp.ers.mvc;
using ers.Models.cart;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers;

namespace ers.Domain.Cart.Mappers
{
    public class CartMapper
        : IMapper<ICartMappable>
    {
        public void Map(ICartMappable objMappable)
        {
            var mcode = ErsContext.sessionState.Get("mcode");
            var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().Retrieve(mcode);


            var basket = ErsFactory.ersBasketFactory.GetErsBasket();
            objMappable.basket = basket;

            basket.PrepareSession();
            basket.Init(mcode, ErsContext.sessionState.Get("ransu"));
            if (objMappable.IsRegister)
            {
                basket.CheckBasketItems(false);
            }

            this.RefreshView(objMappable, basket);
            this.SetRecommend(objMappable, basket, member_rank);
            this.SetBasketDataToCookie(objMappable);
            this.SetDisplayedDelvMethod(objMappable);
        }

        /// <summary>
        /// refresh the values of basket items
        /// </summary>
        protected virtual void RefreshView(ICartMappable objMappable, ErsBasket basket)
        {
            objMappable.basketItems = LoadDefaultValue(objMappable, basket.objBasketRecord.Values);
            objMappable.regularBasketItems = LoadRegularDefaultValue(objMappable, basket.objRegularBasketRecord.Values);
        }

        /// <summary>
        /// load default value of basket itmes
        /// </summary>
        /// <returns></returns>
        protected IEnumerable<Cart_items> LoadDefaultValue(ICartMappable objMappable, IEnumerable<ErsBaskRecord> records)
        {
            var controller = objMappable.controller;
            var basketItems = new List<Cart_items>();
            foreach (var itemData in records)
            {
                var cartItem = new Cart_items();
                cartItem.baskRecord = itemData;
                cartItem.containerModel = objMappable;
                controller.mapperBus.Map<ICartBasketRecordMappable>(cartItem);

                if (cartItem.scode.HasValue())
                {
                    basketItems.Add(cartItem);
                }
            }

            return basketItems;
        }

        /// <summary>
        /// load default value of basket itmes
        /// </summary>
        /// <returns></returns>
        protected IEnumerable<Cart_regular_items> LoadRegularDefaultValue(ICartMappable objMappable, IEnumerable<ErsBaskRecord> records)
        {
            var controller = objMappable.controller;
            var basketItems = new List<Cart_regular_items>();
            foreach (var itemData in records)
            {
                var cartItem = new Cart_regular_items();
                cartItem.baskRecord = itemData;

                controller.mapperBus.Map<ICartRegularBasketRecordMappable>(cartItem);

                basketItems.Add(cartItem);
            }

            return basketItems;
        }

        /// <summary>
        /// リコメンド取得
        /// </summary>
        protected void SetRecommend(ICartMappable objMappable, ErsBasket basket, int? member_rank)
        {
            var scode = this.GetRecommendBaseMerchandise(objMappable, basket);

            var merchandise = ErsFactory.ersMerchandiseFactory.GetActiveErsMerchandiseWithScode(scode, member_rank);
            if (merchandise == null)
            {
                return;
            }

            objMappable.recommendItems = ErsFactory.ersViewServiceFactory.GetErsViewRecommendService().GetRecommend(merchandise, member_rank);
        }

        /// <summary>
        /// リコメンド取得条件となる商品を取得
        /// </summary>
        /// <returns></returns>
        protected string GetRecommendBaseMerchandise(ICartMappable objMappable, ErsBasket basket)
        {
            //recomenndを選択
            if (!string.IsNullOrEmpty(objMappable.scode))
            {
                return objMappable.scode;
            }
            else if (basket.objBasketRecord.Count > 0)
            {
                //最初のレコード取得
                return basket.objBasketRecord.Values.Last().scode;
            }
            else if (basket.objRegularBasketRecord.Count > 0)
            {
                //最初のレコード取得
                return basket.objRegularBasketRecord.Values.Last().scode;
            }

            return null;
        }

        /// <summary>
        /// write basket data to cookie.
        /// </summary>
        protected virtual void SetBasketDataToCookie(ICartMappable objMappable)
        {
            ErsContext.sessionState.Add("basket_total", Convert.ToString(objMappable.total_all_item));
            ErsContext.sessionState.Add("basket_count", Convert.ToString(objMappable.amounttotal_all_item));
        }

        /// <summary>
        /// Display options Deliv Mehotd by above conditions
        /// </summary>
        /// <param name="objMappable"></param>
        protected virtual void SetDisplayedDelvMethod(ICartMappable objMappable)
        {
            if (this.FindItemDelivMethod(objMappable, EnumDelvMethod.Mail))
                objMappable.DisplayedDelvMethod = (short)EnumDelvMethod.Mail;
            else if (this.FindItemDelivMethod(objMappable, EnumDelvMethod.ExpressAndMail))
            {
                objMappable.DisplayedDelvMethod = (short)EnumDelvMethod.Express;

                if (objMappable.amounttotal_all_item == 1)
                    objMappable.DisplayedDelvMethod = (short)EnumDelvMethod.ExpressAndMail;
            }
            else
            {
                objMappable.DisplayedDelvMethod = (short)EnumDelvMethod.Express;
            }
        }

        protected virtual bool FindItemDelivMethod(ICartMappable objMappable, EnumDelvMethod deliv_method)
        {
            var result = false;

            if (objMappable.basketItems != null && objMappable.basketItems.Count() > 0)
            {
                result = objMappable.basketItems.Where((record) => record.baskRecord.deliv_method == deliv_method).Count() > 0;
            }

            if (result == false)
            {
                if (objMappable.regularBasketItems != null && objMappable.regularBasketItems.Count() > 0)
                {
                    result = objMappable.regularBasketItems.Where((record) => record.baskRecord.deliv_method == deliv_method).Count() > 0;
                }
            }

            return result;
        }
    }
}