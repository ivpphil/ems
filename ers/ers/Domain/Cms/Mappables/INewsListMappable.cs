﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;

namespace ers.Domain.Cms.Mappables
{
    public interface INewsListMappable
        : IMappable
    {
        ErsPagerModel pager { get; }

        string contents_code { get; }

        string contents_name { set; }

        EnumRequired? req_login_flg { set; }

        long recordCount { set; }

        List<Dictionary<string, object>> newsList { set; }
    }
}