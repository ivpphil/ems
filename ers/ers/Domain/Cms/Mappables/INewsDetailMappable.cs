﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ers.Domain.Cms.Mappables
{
    public interface INewsDetailMappable
        : IMappable
    {
        string template_code { get; }

        string article_code { get; }

        EnumRequired? req_login_flg { set; }

        List<Dictionary<string, object>> linkList { set; }

        List<Dictionary<string, object>> fileList { set; }

        int linkListCount { set; }

        int fileListCount { set; }

        string img_file_name1 { set; }

        string img_file_name2 { set; }

        string img_file_name3 { set; }

        string img_file_name4 { set; }

        string img_file_name5 { set; }

        string img_file_name6 { set; }

        string img_file_name7 { set; }
    }
}