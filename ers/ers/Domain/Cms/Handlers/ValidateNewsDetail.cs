﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Cms.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.contents;
using System.Net;

namespace ers.Domain.Cms.Handlers
{
    public class ValidateNewsDetail
        : IValidationHandler<INewsDetailCommand>
    {
        public virtual IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(INewsDetailCommand command)
        {
            yield return command.CheckRequired("article_code");

            var repository = ErsFactory.ersContentsFactory.GetErsNewsArticleRepository();
            var criteria = this.GetCriteria(command);

            var retList = repository.Find(criteria);
            if (retList.Count == 0)
            {
                throw new HttpException((int)HttpStatusCode.NotFound, "HTTP/1.1 404 Not Found");
            }
        }

        protected virtual ErsNewsArticleCriteria GetCriteria(INewsDetailCommand command)
        {
            var criteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
            criteria.article_code = command.article_code;
            criteria.active = EnumActive.Active;
            criteria.period_from_less = DateTime.Now;
            criteria.period_to_than = DateTime.Now;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            return criteria;
        }
    }
}