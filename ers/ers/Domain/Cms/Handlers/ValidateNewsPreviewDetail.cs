﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Cms.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.contents;
using System.Net;
using System.ComponentModel.DataAnnotations;

namespace ers.Domain.Cms.Handlers
{
    public class ValidateNewsPreviewDetail
        : ValidateNewsDetail, IValidationHandler<INewsDetailCommand>
    {
        public override IEnumerable<ValidationResult> Validate(INewsDetailCommand command)
        {
            foreach (var result in base.Validate(command))
            {
                yield return result;
            }

            yield return command.CheckRequired("admin_ransu");
            yield return command.CheckRequired("admin_ssl_ransu");
            yield return command.CheckRequired("user_cd");

            if (!string.IsNullOrEmpty(command.admin_ransu) && !string.IsNullOrEmpty(command.admin_ssl_ransu) && !string.IsNullOrEmpty(command.user_cd))
            {
                var enc = ErsFactory.ersUtilityFactory.getErsEncryption();
                var decode_admin_ransu = enc.HexDecode(command.admin_ransu);
                var decode_admin_ssl_ransu = enc.HexDecode(command.admin_ssl_ransu);
                var decode_user_cd = enc.HexDecode(command.user_cd);

                var state = ErsFactory.ersSessionStateFactory.GetUserStateAdminSpecification().getUserState(decode_admin_ransu, decode_admin_ssl_ransu, decode_user_cd);

                if (state != EnumUserState.LOGIN)
                {
                    throw new HttpException((int)HttpStatusCode.NotFound, "HTTP/1.1 404 Not Found");
                }
            }
        }

        protected override ErsNewsArticleCriteria GetCriteria(INewsDetailCommand command)
        {
            var criteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
            criteria.article_code = command.article_code;
            return criteria;
        }
    }
}