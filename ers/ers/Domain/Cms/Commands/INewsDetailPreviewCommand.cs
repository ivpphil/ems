﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Cms.Commands
{
    public interface INewsDetailPreviewCommand
        : ICommand, INewsDetailCommand
    {
    }
}