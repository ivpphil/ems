﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Cms.Commands
{
    public interface INewsDetailCommand
        : ICommand
    {
        string admin_ransu { get; set; }

        string admin_ssl_ransu { get; set; }

        string user_cd { get; set; }

        string article_code { get; set; }
    }
}