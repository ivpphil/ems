﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Cms.Mappables;
using jp.co.ivp.ers.mvc;
using System.Net;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers;

namespace ers.Domain.Cms.Mappers
{
    public class NewsDetailPreviewMapper
        : NewsDetailMapper, IMapper<INewsDetailPreviewMappable>
    {
        public void Map(INewsDetailPreviewMappable objMappable)
        {
            base.Map(objMappable);
        }

        protected override ErsNewsArticleCriteria GetCriteria(INewsDetailMappable objMappable)
        {
            var criteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
            criteria.article_code = objMappable.article_code;
            return criteria;
        }
    }
}