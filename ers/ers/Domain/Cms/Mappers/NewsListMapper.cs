﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Cms.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ers.Domain.Cms.Mappers
{
    public class NewsListMapper
        : IMapper<INewsListMappable>
    {
        public void Map(INewsListMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var contentsRepository = ErsFactory.ersContentsFactory.GetErsCmsContentsRepository();
            var contentsCriteria = ErsFactory.ersContentsFactory.GetErsCmsContentsCriteria();
            contentsCriteria.contents_code = objMappable.contents_code;
            contentsCriteria.site_id = setup.site_id;

            IList<ErsCmsContents> contentsList = contentsRepository.Find(contentsCriteria);
            if (contentsList.Count > 0)
            {
                objMappable.contents_name = contentsList[0].contents_name;
                objMappable.req_login_flg = contentsList[0].req_login_flg;
            }
            else
            {
                objMappable.contents_name = null;
            }

            var repository = ErsFactory.ersContentsFactory.GetErsNewsArticleRepository();
            var criteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
            criteria.contents_code = objMappable.contents_code;
            criteria.active = EnumActive.Active;
            criteria.period_from_less = DateTime.Now;
            criteria.period_to_than = DateTime.Now;
            criteria.site_id = setup.site_id;
            criteria.available_template();

            var recordCount = repository.GetRecordCount(criteria);
            objMappable.recordCount = recordCount;

            objMappable.pager.SetLimitAndOffsetToCriteria(criteria, recordCount);
            criteria.SetOrderByPosted_date(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);

            IList<ErsNewsArticle> list = repository.Find(criteria);
            var ersList = new List<Dictionary<string, object>>();
            foreach (var item in list)
            {
                var dictionary = item.GetPropertiesAsDictionary();
                ersList.Add(dictionary);
            }
            objMappable.newsList = ersList;
        }
    }
}