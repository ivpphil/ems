﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Cms.Mappables;
using jp.co.ivp.ers.mvc;
using System.Net;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers;

namespace ers.Domain.Cms.Mappers
{
    public class NewsDetailMapper
        : IMapper<INewsDetailMappable>
    {
        public void Map(INewsDetailMappable objMappable)
        {
            this.SetNewsArticleWithID(objMappable);
        }

        internal void SetNewsArticleWithID(INewsDetailMappable objMappable)
        {
            var repository = ErsFactory.ersContentsFactory.GetErsNewsArticleRepository();
            var criteria = this.GetCriteria(objMappable);
            IList<ErsNewsArticle> retList = repository.Find(criteria);

            var objArticle = retList.First();

            objMappable.OverwriteWithParameter(objArticle.GetPropertiesAsDictionary());

            var linkList = new List<Dictionary<string, object>>();
            objMappable.linkList = linkList;
            if (objArticle.link_string_1 != null && objArticle.link_url_1 != null)
            {
                var dic = new Dictionary<string, object>();
                dic["link"] = objArticle.link_url_1;
                dic["linkString"] = objArticle.link_string_1;
                linkList.Add(dic);
            }
            if (objArticle.link_string_2 != null && objArticle.link_url_2 != null)
            {
                var dic = new Dictionary<string, object>();
                dic["link"] = objArticle.link_url_2;
                dic["linkString"] = objArticle.link_string_2;
                linkList.Add(dic);
            }
            if (objArticle.link_string_3 != null && objArticle.link_url_3 != null)
            {
                var dic = new Dictionary<string, object>();
                dic["link"] = objArticle.link_url_3;
                dic["linkString"] = objArticle.link_string_3;
                linkList.Add(dic);
            }
            if (objArticle.link_string_4 != null && objArticle.link_url_4 != null)
            {
                var dic = new Dictionary<string, object>();
                dic["link"] = objArticle.link_url_4;
                dic["linkString"] = objArticle.link_string_4;
                linkList.Add(dic);
            }
            if (objArticle.link_string_5 != null && objArticle.link_url_5 != null)
            {
                var dic = new Dictionary<string, object>();
                dic["link"] = objArticle.link_url_5;
                dic["linkString"] = objArticle.link_string_5;
                linkList.Add(dic);
            }

            var fileList = new List<Dictionary<string, object>>();
            objMappable.fileList = fileList;
            if (objArticle.file_string_1 != null && objArticle.file_real_name_1 != null)
            {
                var dic = new Dictionary<string, object>();
                dic["file"] = objArticle.file_real_name_1;
                dic["fileString"] = objArticle.file_string_1;
                fileList.Add(dic);
            }
            if (objArticle.file_string_2 != null && objArticle.file_real_name_2 != null)
            {
                var dic = new Dictionary<string, object>();
                dic["file"] = objArticle.file_real_name_2;
                dic["fileString"] = objArticle.file_string_2;
                fileList.Add(dic);
            }
            if (objArticle.file_string_3 != null && objArticle.file_real_name_3 != null)
            {
                var dic = new Dictionary<string, object>();
                dic["file"] = objArticle.file_real_name_3;
                dic["fileString"] = objArticle.file_string_3;
                fileList.Add(dic);
            }
            if (objArticle.file_string_4 != null && objArticle.file_real_name_4 != null)
            {
                var dic = new Dictionary<string, object>();
                dic["file"] = objArticle.file_real_name_4;
                dic["fileString"] = objArticle.file_string_4;
                fileList.Add(dic);
            }
            if (objArticle.file_string_5 != null && objArticle.file_real_name_5 != null)
            {
                var dic = new Dictionary<string, object>();
                dic["file"] = objArticle.file_real_name_5;
                dic["fileString"] = objArticle.file_string_5;
                fileList.Add(dic);
            }

            objMappable.linkListCount = linkList.Count;
            objMappable.fileListCount = fileList.Count;

            objMappable.img_file_name1 = this.GetFileName(objArticle.img_file_name, 0);
            objMappable.img_file_name2 = this.GetFileName(objArticle.img_file_name, 1);
            objMappable.img_file_name3 = this.GetFileName(objArticle.img_file_name, 2);
            objMappable.img_file_name4 = this.GetFileName(objArticle.img_file_name, 3);
            objMappable.img_file_name5 = this.GetFileName(objArticle.img_file_name, 4);
            objMappable.img_file_name6 = this.GetFileName(objArticle.img_file_name, 5);
            objMappable.img_file_name7 = this.GetFileName(objArticle.img_file_name, 6);

            var contentsRepository = ErsFactory.ersContentsFactory.GetErsCmsContentsRepository();
            var contentsCriteria = ErsFactory.ersContentsFactory.GetErsCmsContentsCriteria();
            contentsCriteria.contents_code = objArticle.contents_code;
            IList<ErsCmsContents> contentsList = contentsRepository.Find(contentsCriteria);
            if (contentsList.Count > 0)
            {
                objMappable.req_login_flg = contentsList[0].req_login_flg;
            }
        }

        protected virtual ErsNewsArticleCriteria GetCriteria(INewsDetailMappable objMappable)
        {
            var criteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
            criteria.article_code = objMappable.article_code;
            criteria.active = EnumActive.Active;
            criteria.period_from_less = DateTime.Now;
            criteria.period_to_than = DateTime.Now;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            return criteria;
        }

        private string GetFileName(string[] img_file_name, int index)
        {
            if (img_file_name == null)
            {
                return null;
            }

            if (img_file_name.Length <= index)
            {
                return null;
            }

            return img_file_name[index];
        }
    }
}