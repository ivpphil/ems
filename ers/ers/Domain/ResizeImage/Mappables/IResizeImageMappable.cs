﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ers.Domain.ResizeImage.Mappables
{
    public interface IResizeImageMappable
        : IMappable
    {
        string filePath { get; set; }

        string fileName { get; set; }

        string scode { get; set; }


        int width { get; set; }

        string contentType { get; set; }

        byte[] bytes { get; set; }

        EnumImageDir? dirName { get; set; }
    }
}