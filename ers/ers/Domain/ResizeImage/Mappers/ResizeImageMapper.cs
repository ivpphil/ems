﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.ResizeImage.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System.IO;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.ResizeImage.Mappers
{
    public class ResizeImageMapper
        : IMapper<IResizeImageMappable>
    {
        public void Map(IResizeImageMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var dirName = objMappable.dirName;

            string skuFileName = objMappable.scode;

            if(string.IsNullOrEmpty(Path.GetExtension(skuFileName)))
            {
                skuFileName = "S" + skuFileName + "_01" + Path.GetExtension(objMappable.fileName);
            }
            
            string fileName;

            string image_directory = "";
            if (setup.Multiple_sites)
            {
                image_directory = ErsFactory.ersUtilityFactory.getSetup().multiple_image_directory(setup.site_id);
            }
            else
            {
                image_directory = ErsFactory.ersUtilityFactory.getSetup().image_directory;
            }

            //指定のファイルがあるかどうか
            var exist = this.CheckFilePath(image_directory, dirName, objMappable.fileName);
            if (!exist && dirName == EnumImageDir.simg)
            {
                //simgの場合、simgになければbimgから取得
                dirName = EnumImageDir.bimg;
                exist = this.CheckFilePath(image_directory, dirName, objMappable.fileName);
            }
            fileName = objMappable.fileName;

            //なければscodeで画像取得
            if (!exist)
            {
                dirName = objMappable.dirName;
                exist = this.CheckFilePath(image_directory, dirName, skuFileName);
                if (!exist && dirName == EnumImageDir.simg)
                {
                    //simgの場合、simgになければbimgから取得
                    dirName = EnumImageDir.bimg;
                    exist = this.CheckFilePath(image_directory, dirName, skuFileName);
                }
                if (exist)
                {
                    fileName = skuFileName;
                }
            }


            if (!exist)
            {
                //throw new HttpException(404, "HTTP/1.1 404 Not Found");
                objMappable.bytes = new byte[500]; objMappable.contentType = "image/jpeg";
                objMappable.controller.Response.StatusCode = (int)System.Net.HttpStatusCode.NotFound; return;
            }

            var filePath = image_directory + dirName + "\\" + fileName;
            var tempFilePath = setup.image_temp_directory + dirName + "\\";
            
            var converter = ErsFactory.ersUtilityFactory.getErsResizeImage();
            converter.InitSize(objMappable.width);
            var bytes = converter.CreateResizedImageWithCache(filePath, tempFilePath);
            objMappable.contentType = converter.ContentType;
            objMappable.bytes = bytes;
        }

        private bool CheckFilePath(string image_directory, EnumImageDir? dirName, string fileName)
        {
            var filePath = image_directory + dirName + "\\";

            filePath += fileName;

            return File.Exists(filePath);
        }
    }
}