﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Detail.Commands
{
    public interface IDetailViewHistoryCommand
        : ICommand
    {
    }
}