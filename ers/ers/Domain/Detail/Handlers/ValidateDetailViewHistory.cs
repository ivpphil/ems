﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using ers.Domain.Detail.Commands;

namespace ers.Domain.Detail.Handlers
{
    public class ValidateDetailViewHistory
        : IValidationHandler<IDetailViewHistoryCommand>
    {
        public IEnumerable<ValidationResult> Validate(IDetailViewHistoryCommand command)
        {
            yield return command.CheckRequired("history_gcode");
        }
    }
}