﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Detail.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ers.Domain.Detail.Handlers
{
    public class ValidateMerchandiseDetail
        : IValidationHandler<IMerchandiseDetailCommand>
    {
        public IEnumerable<ValidationResult> Validate(IMerchandiseDetailCommand command)
        {
            if (string.IsNullOrEmpty(command.gcode) && string.IsNullOrEmpty(command.scode))
            {
                yield return new ValidationResult(ErsResources.GetMessage("20001"), new[] { "gcode", "scode" });
            }
        }
    }
}