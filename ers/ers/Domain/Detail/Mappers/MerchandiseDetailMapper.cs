﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Detail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.db;
using System.Collections;
using jp.co.ivp.ers;

namespace ers.Domain.Detail.Mappers
{
    public class MerchandiseDetailMapper
        : IMapper<IMerchandiseDetailMappable>
    {
        public const int ThumnailCount = 3;

        public void Map(IMerchandiseDetailMappable objMappable)
        {
            var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().RetrieveWithSession();

            var merchandise = this.GetMerchandise(objMappable, member_rank);
            objMappable.merchandise = merchandise;
            objMappable.listMerchandise = this.GetListMerchandise(merchandise, member_rank);

            objMappable.OverwriteWithParameter(merchandise.GetPropertiesAsDictionary());
            objMappable.link_url = GetLinkUrl(merchandise.link_url);

            if (ErsFactory.ersMerchandiseFactory.GetOnCampaignSpecification().IsSatisfiedBy(merchandise.p_date_from, merchandise.p_date_to))
            {
                objMappable.price = merchandise.sale_price;
                objMappable.default_price = merchandise.price;
                objMappable.onCampaign = true;
            }

            if (!merchandise.disp_send_ptn.HasValue())
            {
                merchandise.disp_send_ptn = "0";
            }
            var disp_send_ptn = merchandise.disp_send_ptn.PadLeft(3, '0');
            
            objMappable.disp_send_ptn_month_intervals = (Convert.ToInt32(disp_send_ptn.Substring(0, 1)) == 1);
            objMappable.disp_send_ptn_week_intervals = (Convert.ToInt32(disp_send_ptn.Substring(1, 1)) == 1);
            objMappable.disp_send_ptn_month_day_intervals = (Convert.ToInt32(disp_send_ptn.Substring(2, 1)) == 1);

            objMappable.imageFiles = this.GetImageFiles(merchandise);
            objMappable.countImageFiles = objMappable.imageFiles.Count;

            this.SetGcodeCookies(objMappable);
        }

        private ErsMerchandise GetMerchandise(IMerchandiseDetailMappable objMappable, int? member_rank)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (!string.IsNullOrEmpty(objMappable.gcode))
            {
                var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
                criteria.gcode = objMappable.gcode;
                criteria.g_active = EnumActive.Active;
                criteria.s_active = EnumActive.Active;
                criteria.site_id = setup.site_id;
                criteria.SetOrderByDisp_order(Criteria.OrderBy.ORDER_BY_ASC);
                criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
                var listMerchandise = repository.FindSkuBaseItemList(criteria, member_rank);

                if (listMerchandise.Count == 0)
                {
                    throw new ErsException("20000", objMappable.gcode);
                }

                return listMerchandise.First();
            }
            else
            {
                var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
                criteria.scode = objMappable.scode;
                criteria.g_active = EnumActive.Active;
                criteria.s_active = EnumActive.Active;
                criteria.site_id = setup.site_id;
                var merchandise = repository.FindSkuBaseItemList(criteria, member_rank);

                if (merchandise.Count == 0)
                {
                    throw new ErsException("20000", objMappable.scode);
                }

                return merchandise.First();
            }
        }

        /// <summary>
        /// Get a list of merchandise in this group.
        /// </summary>
        public IEnumerable<Dictionary<string, object>> GetListMerchandise(ErsMerchandise merchandise, int? member_rank)
        {
            if (merchandise == null)
            {
                return null;
            }

            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            groupCriteria.gcode = merchandise.gcode;
            groupCriteria.g_active = EnumActive.Active;
            groupCriteria.s_active = EnumActive.Active;
            groupCriteria.SetOrderByDisp_order(Criteria.OrderBy.ORDER_BY_ASC);
            groupCriteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
            var setup =  ErsFactory.ersUtilityFactory.getSetup();
            groupCriteria.site_id = setup.site_id;
            var listEm = groupRepository.FindSkuBaseItemList(groupCriteria, member_rank);
            var retList = new List<Dictionary<string, object>>();
            foreach (var item in listEm)
            {
                var dictionary = item.GetPropertiesAsDictionary();
                dictionary["StockStatus"] = ErsFactory.ersMerchandiseFactory.GetStockStatusSpecification().GetStockStatusOfMerchandise(item);
                retList.Add(dictionary);
            }

            return retList;
        }

        private void SetGcodeCookies(IMerchandiseDetailMappable objMappable)
        {

            List<string> newGcodeCookiesLst = GetNewGcodeCookies(objMappable);

            HttpCookie cookie = new HttpCookie("history_gcode");
            cookie.Value = String.Join(",", newGcodeCookiesLst.ToArray());
            objMappable.controller.Response.Cookies.Add(cookie);
        }

        private List<string> GetNewGcodeCookies(IMerchandiseDetailMappable objMappable)
        {
            var gcode = HttpUtility.UrlEncode(objMappable.gcode);

            var retNewGcodeCookies = new List<string>();

            if (objMappable.controller.HttpContext.Request.Cookies.Get("history_gcode") == null)
            {
                retNewGcodeCookies.Insert(0, gcode);
                return retNewGcodeCookies;
            }

            retNewGcodeCookies = objMappable.controller.HttpContext.Request.Cookies.Get("history_gcode").Value.Split(',').ToList();

            if (retNewGcodeCookies.Contains(gcode))
            {
                retNewGcodeCookies.Remove(gcode);
            }

            retNewGcodeCookies.Insert(0, gcode);

            return retNewGcodeCookies;
        }

        private List<string> GetImageFiles(ErsMerchandise merchandise)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            string image_directory = "";
            if (setup.Multiple_sites)
            {
                image_directory = ErsFactory.ersUtilityFactory.getSetup().multiple_image_directory(setup.site_id);
            }
            else
            {
                image_directory = ErsFactory.ersUtilityFactory.getSetup().image_directory;
            }
                var ret = new List<string>();
            for (int i = 0; i < ThumnailCount; i++)
            {
                var fileName = string.Format("S{0}_{1:00}.jpg", merchandise.scode, i + 1);
                if (System.IO.File.Exists(image_directory + "\\" + EnumImageDir.simg + "\\" + fileName) ||
                    System.IO.File.Exists(image_directory + "\\" + EnumImageDir.bimg + "\\" + fileName))
                {
                    ret.Add(fileName);
                }
            }
            return ret;
        }

        /// <summary>
        /// Sets link_url. If link_url is not a valid uri, default site uri will be added.
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        private string GetLinkUrl(string uri)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (Uri.IsWellFormedUriString(uri, UriKind.Absolute))
            {
                return uri;
            }
            else
            {
                return setup.sec_url + uri;
            }
        }
    }
}