﻿using System.Collections.Generic;
using System.Linq;
using ers.Domain.Detail.Mappables;
using ers.Models.detail;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ers.Domain.Detail.Mappers
{
    public class PriceDetailRankRecordMapper : IMapper<IPriceDetailRankRecordMappable>
    {
        public void Map(IPriceDetailRankRecordMappable objMappable)
        {
            var listRecord = new List<price_detail_rank_record>();

            var repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();

            var rankRepository = ErsFactory.ersMemberFactory.GetErsMemberRankSetupRepository();
            var rankCriteria = ErsFactory.ersMemberFactory.GetErsMemberRankSetupCriteria();
            rankCriteria.active = EnumActive.Active;

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (setup.member_rank_centralization)
            {
                rankCriteria.site_id_for_admin = (int)EnumSiteId.COMMON_SITE_ID;
            }
            else
            {
                rankCriteria.site_id_for_admin = setup.site_id;
            }

            rankCriteria.SetOrderByRank(Criteria.OrderBy.ORDER_BY_ASC);
            var listRank = rankRepository.Find(rankCriteria);
            foreach (var rank in listRank)
            {
                var record = new price_detail_rank_record();

                var criteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
                criteria.scode = objMappable.scode;
                criteria.member_rank = rank.rank;
                var listPrice = repository.FindPriceMemberRankItemList(criteria);

                if (listPrice.Count > 0)
                {
                    var price = listPrice.First();
                    record.OverwriteWithParameter(price.GetPropertiesAsDictionary());
                }
                else
                {
                    record.member_rank = rank.rank;
                }
                listRecord.Add(record);
            }

            objMappable.all_price_empty = listRecord.All(p => p.price == null);
            objMappable.all_regular_price_empty = listRecord.All(p => p.regular_price == null);
            objMappable.all_regular_first_price_empty = listRecord.All(p => p.regular_first_price == null);

            objMappable.listPriceDetailRankRecord = listRecord;
            

            this.SetRankName(objMappable);
        }


        internal void SetRankName(IPriceDetailRankRecordMappable objMappable)
        {
            var site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            foreach (var record in objMappable.listPriceDetailRankRecord)
            {
                var objRankSetup = ErsFactory.ersMemberFactory.GetErsMemberRankSetupWithRank(record.member_rank, site_id);
                if (objRankSetup != null)
                {
                    record.member_rank_name = objRankSetup.rank_name;
                }
            }
        }

    }
}