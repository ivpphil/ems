﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Detail.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Detail.Mappers
{
    public class MerchandiseRecommendMapper
        : IMapper<IMerchandiseRecommendMappable>
    {
        public void Map(IMerchandiseRecommendMappable objMappable)
        {
            var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().RetrieveWithSession();

            objMappable.recommendItems = ErsFactory.ersViewServiceFactory.GetErsViewRecommendService().GetRecommend(objMappable.merchandise, member_rank);
        }
    }
}