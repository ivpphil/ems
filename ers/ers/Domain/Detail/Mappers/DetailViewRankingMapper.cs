﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Detail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ers.Domain.Detail.Mappers
{
    public class DetailViewRankingMapper
        : IMapper<IDetailViewRankingMappable>
    {
        public void Map(IDetailViewRankingMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var searchspec = ErsFactory.ersMerchandiseFactory.GetSearchDailyRankingListSpec();
            var criteria = ErsFactory.ersRankingFactory.GetErsRankingCriteria();
            criteria.rank_active = EnumActive.Active;
            criteria.rank_type = EnumRankType.Daily;
            criteria.g_site_id = setup.site_id;
            criteria.site_id = setup.site_id;
            criteria.SetOrderBySum_result(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.LIMIT = setup.FrontRankingDispCount;

            var list = searchspec.GetSearchData(criteria);

            if (list.Count == 0)
            {
                return;
            }

            var arrGcode = list.Select((dictionary) => Convert.ToString(dictionary["gcode"])).ToArray();

            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var group_criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            group_criteria.ignore_gcode = setup.IgnoreGcode;
            group_criteria.disp_list_flg = EnumDisp_list_flg.Visible;
            group_criteria.g_active = EnumActive.Active;
            group_criteria.s_active = EnumActive.Active;
            group_criteria.SetDateFromTo(DateTime.Now);
            group_criteria.gcode_in = arrGcode;
            group_criteria.site_id = setup.site_id;
            group_criteria.AddOrderByArrayIndex("g_master_t.gcode", arrGcode, Criteria.OrderBy.ORDER_BY_ASC);

            objMappable.viewRankingList = repository.FindGroupBaseItemList(group_criteria);
        }
    }
}