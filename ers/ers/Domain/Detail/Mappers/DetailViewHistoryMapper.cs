﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Detail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ers.Domain.Detail.Mappers
{
    public class DetailViewHistoryMapper
        : IMapper<IDetailViewHistoryMappable>
    {
        public void Map(IDetailViewHistoryMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

            criteria.gcode_in = objMappable.history_gcode;
            if (objMappable.ignore_gcode.HasValue())
            {
                criteria.ignore_gcode = new[] { objMappable.ignore_gcode };
            }
            criteria.ignore_gcode = setup.IgnoreGcode;
            criteria.disp_list_flg = EnumDisp_list_flg.Visible;
            criteria.g_active = EnumActive.Active;
            criteria.s_active = EnumActive.Active;
            criteria.SetDateFromTo(DateTime.Now);
            criteria.site_id = setup.site_id;
            criteria.AddOrderByArrayIndex("g_master_t.gcode", objMappable.history_gcode, Criteria.OrderBy.ORDER_BY_ASC);
            criteria.LIMIT = 10;
            objMappable.viewHistoryList = repository.FindGroupBaseItemList(criteria);
        }
    }
}