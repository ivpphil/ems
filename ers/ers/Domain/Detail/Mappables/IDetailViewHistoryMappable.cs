﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.merchandise;

namespace ers.Domain.Detail.Mappables
{
    public interface IDetailViewHistoryMappable
        : IMappable
    {
        IList<ErsMerchandise> viewHistoryList { set; }

        string ignore_gcode { get; }

        string[] history_gcode { get; set; }
    }
}