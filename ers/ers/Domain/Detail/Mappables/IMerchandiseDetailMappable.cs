﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.merchandise;

namespace ers.Domain.Detail.Mappables
{
    public interface IMerchandiseDetailMappable
        : IMappable
    {
        ErsMerchandise merchandise { get; set; }

        string gcode { get; set; }

        string scode { get; set; }

        bool disp_send_ptn_month_intervals { get; set; }

        bool disp_send_ptn_week_intervals { get; set; }

        bool disp_send_ptn_month_day_intervals { get; set; }

        IEnumerable<Dictionary<string, object>> listMerchandise { get; set; }

        int? default_price { get; set; }

        int? price2 { get; set; }

        int? price { get; set; }

        bool onCampaign { get; set; }

        List<string> imageFiles { get; set; }

        int? countImageFiles { get; set; }

        string link_url { set; }
    }
}
