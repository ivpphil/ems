﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.merchandise;

namespace ers.Domain.Detail.Mappables
{
    public interface IMerchandiseRecommendMappable
        : IMappable
    {
        ErsMerchandise merchandise { get; set; }

        List<Dictionary<string, object>> recommendItems { get; set; }
    }
}
