﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.merchandise;

namespace ers.Domain.Detail.Mappables
{
    public interface IDetailViewRankingMappable
        : IMappable
    {
        IList<ErsMerchandise> viewRankingList { set; }
    }
}