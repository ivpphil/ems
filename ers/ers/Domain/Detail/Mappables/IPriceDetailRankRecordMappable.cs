﻿using System.Collections.Generic;
using ers.Models.detail;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ers.Domain.Detail.Mappables
{
    public interface IPriceDetailRankRecordMappable : IMappable
    {
        string scode { get; }

        bool all_price_empty { set; }

        bool all_regular_price_empty { set; }

        bool all_regular_first_price_empty { set; }

        List<price_detail_rank_record> listPriceDetailRankRecord { get; set; }

    }
}
