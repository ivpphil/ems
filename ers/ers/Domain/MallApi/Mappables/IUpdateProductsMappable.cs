﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ers.Domain.MallApi.Mappables
{
    public interface IUpdateProductsMappable
        : IMappable
    {
        /// <summary>
        /// エラーメッセージ [Error message]
        /// </summary>
        string ErrorMessage { get; }
    }
}