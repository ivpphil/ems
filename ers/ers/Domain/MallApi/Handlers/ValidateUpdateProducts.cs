﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ers.Domain.MallApi.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.MallApi.Handlers
{
    public class ValidateUpdateProducts
        : IValidationHandler<IUpdateProductsCommand>
    {
        /// <summary>
        /// バリデート [Validate]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>結果 [Results]</returns>
        public IEnumerable<ValidationResult> Validate(IUpdateProductsCommand command)
        {
            yield return command.CheckRequired("sys_id");
            yield return command.CheckRequired("sys_pass");

            if (command.IsValidField("sys_id", "sys_pass"))
            {
                var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();

                string user = mallSetup.GetErsAccountUser();
                string pass = mallSetup.GetErsAccountPass();

                // 認証チェック [Check authentication]
                if (command.sys_id != user || command.sys_pass != pass)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("102000"), new[] { "authentication" });
                }
            }

            // 商品情報 [Product information]
            if (command.products != null)
            {
                foreach (var product in command.products)
                {
                    product.AddInvalidField(command.controller.commandBus.Validate<IProductModelCommand>(product));

                    if (!product.IsValid)
                    {
                        yield return new ValidationResult(product.ErrorMessage, new[] { "update_products" });
                    }
                }
            }
        }
    }
}