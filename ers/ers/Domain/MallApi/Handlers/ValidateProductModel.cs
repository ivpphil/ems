﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ers.Domain.MallApi.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.MallApi.Handlers
{
    public class ValidateProductModel
        : IValidationHandler<IProductModelCommand>
    {
        /// <summary>
        /// バリデート [Validate]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>結果 [Results]</returns>
        public IEnumerable<ValidationResult> Validate(IProductModelCommand command)
        {
            yield return command.CheckRequired("product_code");
            yield return command.CheckRequired("stock");

            if (command.IsValidField("product_code"))
            {
                // 商品存在チェック [Is exists sku]
                if (!this.IsExistsSku(command.product_code))
                {
                    yield return new ValidationResult(ErsResources.GetMessage("102001", command.product_code));
                }
            }
        }

        /// <summary>
        /// 商品存在チェック [Is exists sku]
        /// </summary>
        /// <param name="scode">商品コード [Scode]</param>
        /// <returns>true : 存在している [Exists] ／ false : 存在していない [Not exists]</returns>
        protected bool IsExistsSku(string scode)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            var repository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();

            criteria.scode = ErsMallCommonService.GetErsSkuFromMall(scode);

            return repository.GetRecordCount(criteria) == 0 ? false : true;
        }
    }
}