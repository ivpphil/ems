﻿using System;
using System.Linq;
using ers.Domain.MallApi.Commands;
using ers.Models;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mall.stock;
using jp.co.ivp.ers.db;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.api.stock;

namespace ers.Domain.MallApi.Handlers
{
    public class UpdateProductsHandler
         : ICommandHandler<IUpdateProductsCommand>
    {
        /// <summary>
        /// サブミット [Submit]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>結果 [Result]</returns>Command
        public ICommandResult Submit(IUpdateProductsCommand command)
        {
            var listParam = new List<UpdateStockParam>();

            //モール在庫マスタ用
            var objDB = ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase();

            using (var tx = ErsDB_parent.BeginTransaction())
            {
                foreach (var product in command.products)
                {
                    if (!product.IsValid)
                    {
                        continue;
                    }

                    this.Import(command, product, listParam, objDB);
                }

                // アラートメール送信 [Send an alert mail]
                this.SendAlertMail(command);

                tx.Commit();
            }

            // モール在庫更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }

            return new CommandResult(true);
        }

        /// <summary>
        /// セット内訳を考慮して減算対象となる商品を減算する。
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        private void Import(IUpdateProductsCommand command, ProductModel product, List<UpdateStockParam> listParam, ErsDatabase objDB)
        {
            //商品を減算
            var diffStock = this.ImportMallStock(command, product, objDB);

            var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(product.ers_scode, null);

            if (merchandise.set_flg != EnumSetFlg.IsNotSet)
            {
                if (diffStock == 0)
                {
                    return;
                }
                
                //モール在庫更新
                ErsMallFactory.ersMallStockFactory.GetUpdateErsMallStockStgy().Update(objDB, product.ers_scode, EnumMallStockOperation.add, diffStock);

                //子商品リスト取得
                var DecreaseSetItemList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(merchandise.scode);

                foreach (var setProduct in DecreaseSetItemList)
                {
                    var chiled_merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(setProduct.scode, null);

                    if (chiled_merchandise.h_mall_flg == EnumOnOff.Off)
                    {
                        continue;
                    }

                    var targetProduct = new ProductModel();
                    targetProduct.product_code = setProduct.scode;
                    targetProduct.price = setProduct.price;
                    targetProduct.stock = product.stock * setProduct.amount;

                    //セット商品内訳を減算
                    var chiled_diffStock = this.ImportMallStock(command, targetProduct, objDB);
                    if (chiled_diffStock != 0)
                    {
                        ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetIncreaseStockStgy().Increase(targetProduct.ers_scode, chiled_diffStock);

                        UpdateStockParam param = default(UpdateStockParam);

                        param.productCode = targetProduct.ers_scode;
                        param.quantity = chiled_diffStock;
                        param.operation = EnumMallStockOperation.add;

                        listParam.Add(param);
                    }
                }
            }
            else
            {
                if (diffStock != 0)
                {
                    //ERS在庫更新
                    ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetIncreaseStockStgy().Increase(product.ers_scode, diffStock);

                    //モール在庫更新
                    ErsMallFactory.ersMallStockFactory.GetUpdateErsMallStockStgy().Update(objDB, product.ers_scode, EnumMallStockOperation.add, diffStock);
                }

                //親在庫の再計算
                var DecreaseParentItemList = ErsMallFactory.ersMallStockFactory.GetSearchParentMallStockDiffSpec().GetSearchData(merchandise.scode);
                foreach (var parentItem in DecreaseParentItemList)
                {
                    var parent_diffStock = Convert.ToInt32(parentItem["diffstock"]);
                    var parent_scode = Convert.ToString(parentItem["parent_scode"]);

                    var parent_merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(parent_scode, null);

                    if (parent_merchandise.h_mall_flg == EnumOnOff.Off)
                    {
                        continue;
                    }


                    var listOldParam = listParam.FindAll((existParam) => existParam.productCode == parent_scode);
                    if (listOldParam.Count == 0)
                    {
                        var param = default(UpdateStockParam);

                        param.productCode = parent_scode;
                        param.quantity = parent_diffStock;
                        param.operation = EnumMallStockOperation.add;
                        listParam.Add(param);
                    }
                    else
                    {
                        var param = listOldParam.First();
                        param.quantity = parent_diffStock;
                    }
                }
            }
        }

        /// <summary>
        /// モールの在庫を取得します。
        /// </summary>
        /// <param name="command"></param>
        /// <param name="product"></param>
        private int ImportMallStock(IUpdateProductsCommand command, ProductModel product, ErsDatabase objDB)
        {
            // 商品情報チェック [Validate product information]
            if (!this.ValidateProduct(command, product))
            {
                return 0;
            }

            var objMallStock = this.GetMallStock(objDB, product.ers_scode);

            return product.stock.Value - objMallStock.stock.Value;
        }

        #region 商品情報チェック [Validate product information]
        /// <summary>
        /// 商品情報チェック [Validate product information]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <param name="product">商品情報 [Product information]</param>
        /// <returns>true : 正常 [Valid] ／ false : 異常 [Invalid]</returns>
        protected bool ValidateProduct(IUpdateProductsCommand command, ProductModel product)
        {
            // 在庫数チェック [Check the stock quantity]
            if (product.stock < 0)
            {
                // アラートメールログ追加 [Add the log for alert mail]
                command.listStockLevelingAlertLog.Add(ErsResources.GetMessage("102007", product.ers_scode, product.stock));
            }

            return true;
        }
        #endregion

        #region アラートメール送信 [Send an alert mail]
        /// <summary>
        /// アラートメール送信 [Send an alert mail]
        /// <param name="command">コマンド [Command]</param>
        /// </summary>
        protected void SendAlertMail(IUpdateProductsCommand command)
        {
            if (command.listStockLevelingAlertLog.Count == 0)
            {
                return;
            }

            var setup = ErsMallFactory.ersMallUtilityFactory.getSetup();

            var mailTo = setup.mallStockLevelingAlertMailTo;
            var mailFrom = setup.mallStockLevelingAlertMailFrom;

            if (string.IsNullOrEmpty(setup.mallStockLevelingAlertMailTo) || string.IsNullOrEmpty(setup.mallStockLevelingAlertMailFrom))
            {
                return;
            }

            var mailCc = setup.mallStockLevelingAlertMailCc;
            var mailBcc = setup.mallStockLevelingAlertMailBcc;
            var mailTitle = setup.mallStockLevelingAlertMailTitle;

            var objMail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();

            var mailText = string.Format("{0:yyyy/MM/dd HH:mm:ss}\r\n{1}", DateTime.Now, ErsResources.GetMessage("102006", String.Join(string.Empty, command.listStockLevelingAlertLog)));

            // メール送信 [Send mail]
            objMail.MailSend(mailTitle, mailText, string.Empty, mailTo, mailFrom, mailCc, mailBcc);
        }
        #endregion

        #region 在庫更新 [Update stock]
        private ErsMallStock GetMallStock(ErsDatabase objDB, string scode)
        {
            var repostiroy = ErsMallFactory.ersMallStockFactory.GetErsMallStockRepository(objDB);
            var criteria = ErsMallFactory.ersMallStockFactory.GetErsMallStockCriteria();
            criteria.scode = scode;
            criteria.ForUpdate = true;
            return repostiroy.FindSingle(criteria);
        }
        #endregion
    }
}