﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.MallApi.Commands
{
    public interface IProductModelCommand
        : ICommand
    {
        /// <summary>
        /// 商品コード [Scode]
        /// </summary>
        string product_code { get; set; }

        /// <summary>
        /// 在庫数 [Stock]
        /// </summary>
        int? stock { get; set; }

        /// <summary>
        /// 価格 [Price]
        /// </summary>
        int? price { get; set; }
    }
}