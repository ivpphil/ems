﻿using System.Collections.Generic;
using ers.Models;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.MallApi.Commands
{
    public interface IUpdateProductsCommand
        : ICommand
    {
        /// <summary>
        /// 在庫平準化アラートログ [Alert for stock leveling]
        /// </summary>
        IList<string> listStockLevelingAlertLog { get; set; }


        /// <summary>
        /// 認証ID [Authentication id]
        /// </summary>
        string sys_id { get; }

        /// <summary>
        /// 認証パス [Authentication password]
        /// </summary>
        string sys_pass { get; }

        /// <summary>
        /// 商品情報 [Product information]
        /// </summary>
        IEnumerable<ProductModel> products { get; set; }
    }
}