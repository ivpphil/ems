﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Quest.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ers.Domain.Quest.Handlers
{
    public class ValidateQuest
        : IValidationHandler<IQuestCommand>
    {
        public IEnumerable<ValidationResult> Validate(IQuestCommand command)
        {
            yield return command.CheckRequired("lname");
            yield return command.CheckRequired("fname");
            yield return command.CheckRequired("email");
            yield return command.CheckRequired("email_confirm");
            yield return command.CheckRequired("quest_ptn");
            yield return command.CheckRequired("quest_text");

            if (command.email != command.email_confirm)
            {
                yield return new ValidationResult(string.Format(ErsResources.GetMessage("10102"), new[] { ErsResources.GetFieldName("quest_email"), ErsResources.GetFieldName("quest_email_confirm") }), new[] { "email", "email_confirm" });
            }

            if (command.pri_chk == null)
                yield return new ValidationResult(ErsResources.GetMessage("20102"), new[] { "pri_chk" });
        }
    }
}