﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Quest.Commands
{
    public interface IQuestCommand
        : ICommand
    {
        string email { get; set; }

        string email_confirm { get; set; }

        int? pri_chk { get; set; }
    }
}