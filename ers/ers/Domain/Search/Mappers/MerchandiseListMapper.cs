﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Search.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.strategy;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ers.Domain.Search.Mappers
{
    public class MerchandiseListMapper
        : IMapper<IMerchandiseListMappable>
    {
        ErsGroupRepository repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

        ObtainMerchandisePriceStgy stgy = ErsFactory.ersMerchandiseFactory.GetObtainMerchandisePriceStgy();

        public void Map(IMerchandiseListMappable objMappable)
        {
            //検索条件をクライテリアに保存
            var emCri = this.GetCriteria(objMappable);

            objMappable.recordCount = repository.GetRecordCountGroupBase(emCri);

            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("20002"));
                return;
            }

            //アドレスリスト取得
            objMappable.merchandiseList = this.GetFindList(objMappable, emCri);
        }

        //検索条件をクライテリアにセット
        public ErsGroupCriteria GetCriteria(IMerchandiseListMappable objMappable)
        {
            var emCri = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();

            //クライテリアにパラメタを渡す

            var setup = ErsFactory.ersUtilityFactory.getSetup();
            //除外するgcode
            emCri.ignore_gcode = setup.IgnoreGcode;

            if (objMappable.s_cate1 != null)
                emCri.cate1 = objMappable.s_cate1.Value;

            if (objMappable.s_cate2 != null)
                emCri.cate2 = objMappable.s_cate2.Value;

            if (objMappable.s_cate3 != null)
                emCri.cate3 = objMappable.s_cate3.Value;

            if (objMappable.s_cate4 != null)
                emCri.cate4 = objMappable.s_cate4.Value;

            if (objMappable.s_cate5 != null)
                emCri.cate5 = objMappable.s_cate5.Value;

            if (!string.IsNullOrEmpty(objMappable.s_sname))
                emCri.sname_and_gname = objMappable.s_sname;

            if (!string.IsNullOrEmpty(objMappable.s_scode))
                emCri.scode_and_gcode = objMappable.s_scode;

            if (!string.IsNullOrEmpty(objMappable.s_keyword))
            {
                var s_keyword = ErsKanaConverter.String_conversion(objMappable.s_keyword);
                emCri.keyword_prefix_search = s_keyword;
            }

            emCri.SetPriceSearch(objMappable.s_price1, objMappable.s_price2);

            
            if (objMappable.s_outstock == false)
            {
                emCri.SetExcludeOutOfStock();
            }

            emCri.disp_list_flg = EnumDisp_list_flg.Visible;
            emCri.g_active = EnumActive.Active;
            emCri.s_active = EnumActive.Active;
            //同梱商品は除外
            emCri.doc_bundling_flg_not_equal = EnumDocBundlingFlg.ON;

            emCri.site_id = setup.site_id;

            return emCri;
        }


        internal List<Dictionary<string, object>> GetFindList(IMerchandiseListMappable objMappable, ErsGroupCriteria emCri)
        {
            var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().RetrieveWithSession();

            this.SetSortToCriteria(objMappable, emCri);

            var objlist = repository.FindGroupBaseItemList(emCri);

            var viewList = new List<Dictionary<string, object>>();
            var itemCount = 1;
            foreach (var merchandise in objlist)
            {
                var viewDictionary = merchandise.GetPropertiesAsDictionary();

                this.GetMerchandiseValue(merchandise, viewDictionary, member_rank);

                viewDictionary["startLine"] = ((itemCount - 1) % objMappable.maxLineCount == 0);
                viewDictionary["endLine"] = (itemCount % objMappable.maxLineCount == 0 || itemCount == objlist.Count);
                viewDictionary["sale_regular_flg"] = this.GetSaleRegularFlg(merchandise);

                viewList.Add(viewDictionary);

                itemCount++;
            }

            return viewList;
        }

        protected virtual void GetMerchandiseValue(ErsMerchandise merchandise, Dictionary<string, object> viewDictionary, int? member_rank)
        {
            stgy.LoadMaximumAndMinimamPriceOfMerchandise(merchandise.gcode, member_rank);
            viewDictionary["price"] = stgy.min_price;
            viewDictionary["max_price"] = stgy.max_price;
            viewDictionary["default_price"] = stgy.min_default_price;
            viewDictionary["price2"] = stgy.min_price2;
            viewDictionary["non_member_price"] = stgy.min_non_member_price;
            viewDictionary["non_member_regular_price"] = stgy.min_non_member_regular_price;

            viewDictionary["regular_price"] = stgy.min_regular_price;
            viewDictionary["max_regular_price"] = stgy.max_regular_price;

            viewDictionary["onCampaign"] = stgy.isCampaign;
        }
        /// <summary>
        /// set sort to criteria
        /// </summary>
        /// <param name="emCri"></param>
        private void SetSortToCriteria(IMerchandiseListMappable objMappable, ErsGroupCriteria emCri)
        {
            if (objMappable.sort != null)
            {
                switch (objMappable.sort)
                {
                    case 1://価格が安い順
                        emCri.SetOrderByPrice_for_search(Criteria.OrderBy.ORDER_BY_ASC);
                        break;
                    case 2://発売日の新しい順
                        emCri.SetOrderByDateFrom(Criteria.OrderBy.ORDER_BY_DESC);
                        break;
                }
            }

            //検索SQLにLIMIT と OFFSETを加える
            objMappable.pager.SetLimitAndOffsetToCriteria(emCri);

            //商品出力結果
            emCri.SetOrderBySort(Criteria.OrderBy.ORDER_BY_ASC);
            emCri.SetOrderByDateFrom(Criteria.OrderBy.ORDER_BY_DESC);
            emCri.SetOrderByGcode(Criteria.OrderBy.ORDER_BY_ASC);
            emCri.SetOrderByDisp_order(Criteria.OrderBy.ORDER_BY_ASC);
        }

        //定期フラグ
        public bool GetSaleRegularFlg(ErsMerchandise merchandise)
        {
            if (merchandise.disp_send_ptn == "000")
            {
                return false;
            }

            switch (merchandise.s_sale_ptn)
            {
                case EnumSalePatternType.NORMAL:
                    return false;
                case EnumSalePatternType.REGULAR:
                case EnumSalePatternType.ALL:
                    return true;
            }

            return false;
        }

    }
}