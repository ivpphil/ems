﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Search.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ers.Domain.Search.Handlers
{
    public class ValidateMerchandiseList
        : IValidationHandler<IMerchandiseListCommand>
    {
        public IEnumerable<ValidationResult> Validate(IMerchandiseListCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (!string.IsNullOrEmpty(command.s_keyword) && ErsKanaConverter.String_conversion(command.s_keyword).Split(new[] { " ", "　" }, StringSplitOptions.RemoveEmptyEntries).Length > setup.Keyword_num)
            {
                yield return new ValidationResult(ErsResources.GetMessage("20005", Convert.ToString(setup.Keyword_num)), new[] { "s_keyword" });
            }
        }

 
    }
}