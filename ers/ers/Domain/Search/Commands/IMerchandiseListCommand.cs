﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Search.Commands
{
    public interface IMerchandiseListCommand
        : ICommand
    {
        string s_keyword { get; }
    }
}