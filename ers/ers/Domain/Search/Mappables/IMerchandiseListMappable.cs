﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc;

namespace ers.Domain.Search.Mappables
{
    public interface IMerchandiseListMappable
        : IMappable, IErsModelBase
    {
        ErsPagerModel pager { get; }

        List<Dictionary<string, object>> merchandiseList { get; set; }

        long recordCount { get; set; }

        long maxLineCount { get; }

        int? s_cate1 { get; set; }

        int? s_cate2 { get; set; }

        int? s_cate3 { get; set; }

        int? s_cate4 { get; set; }

        int? s_cate5 { get; set; }

        string s_sname { get; set; }

        string s_scode { get; set; }

        string s_keyword { get; set; }

        long? s_price1 { get; set; }

        long? s_price2 { get; set; }

        int? sort { get; set; }

        bool s_outstock { get; set; }
    }
}
