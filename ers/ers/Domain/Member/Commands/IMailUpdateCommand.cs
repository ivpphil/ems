﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Commands
{
    public interface IMailUpdateCommand
        : ICommand
    {
        bool submit_btn1 { get; set; }

        bool submit_btn2 { get; set; }

        EnumMFlg? m_flg { get; set; }
    }
}