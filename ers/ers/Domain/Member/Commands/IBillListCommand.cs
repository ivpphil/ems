﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Member.Commands
{
    public interface IBillListCommand
        : ICommand
    {
        DateTime? s_date1 { get; }
        DateTime? s_date2 { get; }
    }
}