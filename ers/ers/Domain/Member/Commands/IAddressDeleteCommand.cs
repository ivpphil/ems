﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Member.Commands
{
    public interface IAddressDeleteCommand
        : ICommand
    {
        int? id { get; set; }
    }
}