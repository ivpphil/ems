﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Member.Commands
{
    public interface IPassrimCommand
        : ICommand
    {
        string email { get; set; }

        string lname { get; set; }

        string fname { get; set; }

        bool submit_btn { get; set; }
    }
}