﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Member.Commands
{
    public interface IPasschangeAnswerCommand
        : ICommand
    {
        string enc_mcode { get; set; }

        string enc_ransu { get; set; }

        bool submit_ans { get; set; }

        bool submit_passwd { get; set; }

        string ans { get; set; }

        string passwd { get; set; }

        string passwd_confirm { get; set; }
    }
}