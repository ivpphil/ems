﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Member.Commands
{
    public interface IAddressUpdateCommand
        : ICommand
    {
        int? add_pref { get; set; }

        int? id { get; set; }

        bool submit_btn1 { get; set; }

        bool submit_btn2 { get; set; }

        string add_zip { get; set; }
    }
}