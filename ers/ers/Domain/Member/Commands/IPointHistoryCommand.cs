﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Member.Commands
{
    public interface IPointHistoryCommand
        : ICommand
    {
        DateTime? s_date1 { get; set; }
        DateTime? s_date2 { get; set; }
    }
}