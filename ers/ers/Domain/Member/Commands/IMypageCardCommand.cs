﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Models.member;

namespace ers.Domain.Member.Commands
{
    public interface IMypageCardCommand
        : ICommand, IMypageCardRecordCommand
    {
        List<MypageCardRecord> listCardInfo { get; set; }

        bool entry_submit { get; set; }

        bool delete_submit { get; set; }

        bool modify_submit { get; set; }
    }
}