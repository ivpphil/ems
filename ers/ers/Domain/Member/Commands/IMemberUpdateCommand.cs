﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;

namespace ers.Domain.Member.Commands
{
    public interface IMemberUpdateCommand
        : ICommand
    {
        ErsMember member { get; }

        int? pref { get; }

        EnumMformat? mformat { get; }

        int? ques { get; }

        string passwd { get; }

        string passwd_confirm { get; }

        int? job { get; }

        EnumSex? sex { get; }

        int? birthday_y { get; }

        int? birthday_m { get; }

        int? birthday_d { get; }

        string email { get; }

        string email_confirm { get; }

        string zip { get; set; }
    }
}