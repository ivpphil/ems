﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Member.Commands
{
    public interface IMypageCardRecordCommand
        : ICommand
    {
        int? validity_y { get; set; }

        int? validity_m { get; set; }

        int? card_id { get; set; }

        string card_holder_name { get; set; }

        int? card_type { get; set; }

        string card_no { get; set; }
    }
}