﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class PasschangeHandler
        : ICommandHandler<IPasschangeCommand>
    {
        public ICommandResult Submit(IPasschangeCommand command)
        {
            var encObj = ErsFactory.ersUtilityFactory.getErsEncryption();
            var mcode = encObj.HexDecode(command.enc_mcode);
            var ransu = encObj.HexDecode(command.enc_ransu);

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();

            //現在の会員情報取得
            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);
            var old_member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);

            //入力値取得
            member.passwd = command.passwd;

            //更新
            repository.Update(old_member, member);

            //pass_randu処理
            var passrimService = ErsFactory.ersMemberFactory.GetErsPasswordReminderService();
            passrimService.InsertPassRansu(ransu, mcode);

            return new CommandResult(true);
        }
    }
}