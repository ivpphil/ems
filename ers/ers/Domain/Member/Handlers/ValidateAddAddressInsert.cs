﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class ValidateAddAddressInsert
        : IValidationHandler<IAddAddressInsertCommand>
    {
        public IEnumerable<ValidationResult> Validate(IAddAddressInsertCommand command)
        {
            //入力ページ以外では必須チェック
            yield return command.CheckRequired("address_name");
            yield return command.CheckRequired("add_lname");
            yield return command.CheckRequired("add_fname");
            // yield return command.CheckRequired("add_lnamek"); //海外配送対応
            // yield return command.CheckRequired("add_fnamek"); //海外配送対応
            yield return command.CheckRequired("add_zip");
            yield return command.CheckRequired("add_pref");
            foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("add_pref", command.add_pref))
            {
                yield return result;
            }
            if (command.IsValidField("add_zip", "add_pref"))
            {
                yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("add_zip", command.add_zip, "add_pref", command.add_pref);
            }

            yield return command.CheckRequired("add_address");
            yield return command.CheckRequired("add_taddress");
            yield return command.CheckRequired("add_tel");
        }
    }
}