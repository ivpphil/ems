﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ers.Domain.Member.Handlers
{
    public class ValidatePasschange
        : IValidationHandler<IPasschangeCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPasschangeCommand command)
        {
            yield return command.CheckRequired("passwd");

            //パスワード一致チェック
            if (command.passwd != null)
            {
                yield return ErsFactory.ersMemberFactory.GetCheckPasswdConfirmStgy().CheckPasswdConfirm(command.passwd, command.passwd_confirm);

                //yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailAndPasswordStgy().CheckDuplicate(this.mcode, this.passwd);
            }
        }
    }
}