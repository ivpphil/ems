﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Member.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class ValidateAddressDelete
        : IValidationHandler<IAddressDeleteCommand>
    {
        public IEnumerable<ValidationResult> Validate(IAddressDeleteCommand command)
        {
            //稼動中の定期データがあれば削除はできない
            var list = this.GetRegularDetailData(command);

            foreach (var regulerOrder in list)
            {
                foreach (ErsRegularOrderRecord regularOrderRecord in regulerOrder.regularOrderRecords)
                {
                    throw new ErsException("20303");
                }
            }

            //入力ページ以外では必須チェック
            yield return command.CheckRequired("id");
        }

        /// <summary>
        /// 別お届け先指定の定期情報取得
        /// </summary>
        public virtual IList<ErsRegularOrder> GetRegularDetailData(IAddressDeleteCommand command)
        {

            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderCriteria();
            criteria.mcode = ErsContext.sessionState.Get("mcode");
            criteria.member_add_id = command.id;
            criteria.SetActiveOnly();
            var site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetMemberSiteId(ErsContext.sessionState.Get("mcode"));
            if (site_id != (int)EnumSiteId.COMMON_SITE_ID)
            {
                criteria.site_id = site_id;
            }
            criteria.SetIntimeOrderBy(Criteria.OrderBy.ORDER_BY_DESC);

            return repository.Find(criteria);
        }
    }
}