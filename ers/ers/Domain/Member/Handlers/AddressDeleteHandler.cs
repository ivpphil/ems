﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class AddressDeleteHandler
        : ICommandHandler<IAddressDeleteCommand>
    {
        public ICommandResult Submit(IAddressDeleteCommand command)
        {
            var mcode = ErsContext.sessionState.Get("mcode");
            var addressInfo = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(command.id, mcode);

            var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            repository.Delete(addressInfo);

            return new CommandResult(true);
        }
    }
}