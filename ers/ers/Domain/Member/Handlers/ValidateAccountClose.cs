﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class ValidateAccountClose
        : IValidationHandler<IAccountCloseCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IAccountCloseCommand command)
        {
            var mcode = ErsContext.sessionState.Get("mcode");
            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);

            //退会済み
            if (member == null)
                throw new ErsException("20300");

            var specification = ErsFactory.ersMemberFactory.GetCancelMembershipSpecification();
            specification.mcode = member.mcode;
            specification.site_id = member.site_id;

            //新着注文および未配送注文が存在するため退会できません
            if (specification.isSatisfiedBy() == false
                || ErsFactory.ersMemberFactory.GetHasNotYetSaledIssuedRegularOrderSpec().Has(mcode, member.site_id))
            {
                throw new ErsException("20301");
            }

            yield break;
        }
    }
}