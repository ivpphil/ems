﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class MailUpdateHandler
        : ICommandHandler<IMailUpdateCommand>
    {
        public ICommandResult Submit(IMailUpdateCommand command)
        {
            this.MailMagaUpdateInsert(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// 会員情報、メルマガ配信を更新
        /// </summary>
        internal void MailMagaUpdateInsert(IMailUpdateCommand command)
        {
            //更新処理
            var new_member = ErsFactory.ersMemberFactory.getErsMemberWithRansu(ErsContext.sessionState);
            var old_member = ErsFactory.ersMemberFactory.getErsMemberWithRansu(ErsContext.sessionState);

            //会員マスタ更新
            new_member.m_flg = command.m_flg;
            var memberRepository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            memberRepository.Update(old_member, new_member);
        }
    }
}