﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class AddAddressInsertHandler
        : ICommandHandler<IAddAddressInsertCommand>
    {
        public ICommandResult Submit(IAddAddressInsertCommand command)
        {            
            this.Insert(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// 別お届け先をインサート
        /// </summary>
        internal void Insert(IAddAddressInsertCommand command)
        {
            var em = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithParameter(command.GetPropertiesAsDictionary());

            em.mcode = ErsContext.sessionState.Get("mcode");
            var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            repository.Insert(em, true);
        }
    }
}