﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class ValidatePointHistory
        : IValidationHandler<IPointHistoryCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPointHistoryCommand command)
        {
            yield return command.CheckRequired("s_date1");
            yield return command.CheckRequired("s_date2");

            if (command.s_date1.HasValue && command.s_date2.HasValue)
            {
                DateTime dt_from = command.s_date1.Value;
                DateTime dt_to = command.s_date2.Value;
                if (dt_from > dt_to)
                {
                    yield return new ValidationResult(
                        ErsResources.GetMessage("10045",
                            ErsResources.GetFieldName("s_date2"),
                            ErsResources.GetFieldName("s_date1")),
                        new[] { "s_date1", "s_date2" });
                }
            }
        }
    }
}