﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ers.Domain.Member.Handlers
{
    public class ValidatePasschangeAnswer
        : IValidationHandler<IPasschangeAnswerCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPasschangeAnswerCommand command)
        {
            yield return command.CheckRequired("ans");

            if (command.IsValidField("ans"))
            {
                var encObj = ErsFactory.ersUtilityFactory.getErsEncryption();
                var mcode = encObj.HexDecode(command.enc_mcode);
                var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);

                if (member == null)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10210"), new[] { "enc_mcode", "enc_ransu" });
                    yield break;
                }

                //セキュリティアンサー一致チェック
                if (command.ans != null)
                {
                    yield return ErsFactory.ersMemberFactory.GetCheckQuesConfirmStgy().CheckQuesConfirm(mcode, command.ans, member.ques);
                }
            }
        }
    }
}