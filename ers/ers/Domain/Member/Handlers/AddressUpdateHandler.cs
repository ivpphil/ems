﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class AddressUpdateHandler
        : ICommandHandler<IAddressUpdateCommand>
    {
        public ICommandResult Submit(IAddressUpdateCommand command)
        {
            
            this.Update(command);
            
            return new CommandResult(true);
        }

        /// <summary>
        /// 別お届け先をアップデート
        /// </summary>
        internal void Update(IAddressUpdateCommand command)
        {
            var mcode = ErsContext.sessionState.Get("mcode");
            var em = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(command.id.Value, mcode);
            em.OverwriteWithModel(command);

            var old_em = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(command.id.Value, mcode);

            var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            repository.Update(old_em, em);

        }
    }
}