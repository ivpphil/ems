﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using System.Text;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class MemberUpdateHandler
        : ICommandHandler<IMemberUpdateCommand>
    {
        public ICommandResult Submit(IMemberUpdateCommand command)
        {
            ErsMemberRepository repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();

            var old_em = ErsFactory.ersMemberFactory.getErsMemberWithRansu(ErsContext.sessionState);

            repository.Update(old_em, command.member);

            var nickName = command.member.lname + command.member.fname;

            ErsContext.sessionState.Add(ErsSessionState.nickNameKey, HttpUtility.UrlEncode(nickName, HttpContext.Current.Response.ContentEncoding));

            //emailをクッキー書き込み
            var objc = new OtherCookie();
            string cookie_email = objc.GetCoookieEmail("login_email");
            if (cookie_email.HasValue())
            {
                objc.SetCookieEmail("login_email", command.member.email, ErsFactory.ersUtilityFactory.getSetup().cookieTimer);
            }

            return new CommandResult(true);
        }
    }
}