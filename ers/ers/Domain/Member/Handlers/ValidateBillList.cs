﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class ValidateBillList
        : IValidationHandler<IBillListCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IBillListCommand command)
        {
            yield return command.CheckRequired("s_date1");
            yield return command.CheckRequired("s_date2");

            if (command.s_date1 != null && command.s_date1 > DateTime.Now)
                yield return new ValidationResult(
                    ErsResources.GetMessage("10046",
                        ErsResources.GetFieldName("s_date1"), ErsResources.GetFieldName("current_date")),
                    new[] { "s_date1" });

            if (command.s_date1 != null && command.s_date2 != null && command.s_date1 > command.s_date2)
                yield return new ValidationResult(
                    ErsResources.GetMessage("10045",
                        ErsResources.GetFieldName("s_date2"),
                        ErsResources.GetFieldName("s_date1")),
                    new[] { "s_date2" });
        }
    }
}