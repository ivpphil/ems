﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using System.ComponentModel.DataAnnotations;

namespace ers.Domain.Member.Handlers
{
    public class ValidateMypageCard
        : IValidationHandler<IMypageCardCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IMypageCardCommand command)
        {
            if (!command.entry_submit && !command.modify_submit && !command.delete_submit)
            {
                yield return new ValidationResult(ErsResources.GetMessage("10211"), new[] { "card_id" });
            }

            var mcode = ErsContext.sessionState.Get("mcode");
            
            if (command.entry_submit)
            {
                foreach (var result in command.controller.commandBus.Validate<IMypageCardRecordCommand>(command))
                {
                    yield return result;
                }
            }

            if (command.modify_submit)
            {
                yield return command.CheckRequired("card_id");
                if (ErsFactory.ersMemberFactory.GetCheckCardAlreadyStoredSpec().Check(mcode, command.card_id))
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10025", ErsResources.GetFieldName("card_id")), new[] { "card_id" });
                }

                if (command.listCardInfo == null || command.listCardInfo.Count == 0)
                {
                    throw new ErsException("10043", ErsFactory.ersUtilityFactory.getSetup().sec_url + ErsFactory.ersUtilityFactory.getSetup().mypage_url);
                }

                foreach (var model in command.listCardInfo)
                {
                    if (ErsFactory.ersMemberFactory.GetCheckCardAlreadyStoredSpec().Check(mcode, model.card_id))
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("10025", ErsResources.GetFieldName("card_id")), new[] { "card_id" });
                    }

                    if (command.card_id == model.card_id)
                    {
                        model.AddInvalidField(command.controller.commandBus.Validate<IMypageCardRecordCommand>(model));

                        if (!model.IsValid)
                        {
                            foreach (var errorMessage in model.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "listCardInfo" });
                            }
                        }
                    }
                    else
                    {
                        model.ClearInvalidFields();
                    }
                }
            }

            if (command.delete_submit)
            {
                //定期で稼働中のカードは削除できない
                yield return command.CheckRequired("card_id");

                if (command.IsValidField("card_id", "mcode"))
                {
                    yield return ErsFactory.ersMemberFactory.GetCheckInOperationCardRegularOrder().CheckInOperationCard(mcode, command.card_id);
                }
                if (command.listCardInfo == null || command.listCardInfo.Count == 0)
                {
                    throw new ErsException("10043", ErsFactory.ersUtilityFactory.getSetup().sec_url + ErsFactory.ersUtilityFactory.getSetup().mypage_url);
                }
                else
                {
                    foreach (var model in command.listCardInfo)
                    {
                        model.ClearInvalidField("card_id");//JSで使用されてしまうため、不正な値を削除。
                    }
                }
            }
            else
            {
                //カード預け枚数チェック
                var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);
                yield return ErsFactory.ersMemberFactory.GetValidateCardSaveCountStgy().Validate(member, new[] { command.card_id }, new[] { "card_id" });
            }
        }
    }
}