﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc;

namespace ers.Domain.Member.Handlers
{
    public class ValidateMailUpdate
        : IValidationHandler<IMailUpdateCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IMailUpdateCommand command)
        {
            if (command.submit_btn1 || command.submit_btn2)
            {
                yield return command.CheckRequired("m_flg");
            }
        }
    }
}