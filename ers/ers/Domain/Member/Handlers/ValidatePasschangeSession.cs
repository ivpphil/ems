﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ers.Domain.Member.Handlers
{
    public class ValidatePasschangeSession
        : IValidationHandler<IPasschangeSessionCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPasschangeSessionCommand command)
        {
            yield return command.CheckRequired("enc_mcode");
            yield return command.CheckRequired("enc_ransu");

            if (command.enc_mcode == null || command.enc_ransu == null)
                yield break;

            var encObj = ErsFactory.ersUtilityFactory.getErsEncryption();
            var mcode = encObj.HexDecode(command.enc_mcode);
            var ransu = encObj.HexDecode(command.enc_ransu);

            var passrimService = ErsFactory.ersMemberFactory.GetErsPasswordReminderService();

            // パスワード変更済み（完了したらインサートされる）
            if (!passrimService.isValidPassRimRansu(ransu))
            {
                throw new ErsException("10042");
            }

            // 乱数が不正もしくは期限切れ
            if (!passrimService.isValidRansu(ransu))
            {
                throw new ErsException("10210");
            }
        }
    }
}