﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class ValidatePassrim
        : IValidationHandler<IPassrimCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPassrimCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //必須チェック
            yield return command.CheckRequired("lname");
            yield return command.CheckRequired("fname");
            yield return command.CheckRequired("email");

            //会員存在チェック
            if (command.IsValidField("lname", "fname", "email"))
            {
                yield return ErsFactory.ersMemberFactory.GetCheckExitMemberWithEmailStgy().CheckExitMember(command.email, command.lname, command.fname, setup.site_id, null);
            }
        }
    }
}