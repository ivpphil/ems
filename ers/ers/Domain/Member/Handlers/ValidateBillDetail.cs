﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Member.Commands;

namespace ers.Domain.Member.Handlers
{
    public class ValidateBillDetail
        : IValidationHandler<IBillDetailCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IBillDetailCommand command)
        {
            yield return command.CheckRequired("d_no");
        }
    }
}