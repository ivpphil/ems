﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class AccountCloseHandler
        : ICommandHandler<IAccountCloseCommand>
    {
        public ICommandResult Submit(IAccountCloseCommand command)
        {
            var mcode = ErsContext.sessionState.Get("mcode");
            //トランザクション開始
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();

            //会員削除
            var oldMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);
            var objMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);
            objMember.deleted = EnumDeleted.Deleted;
            repository.Update(oldMember, objMember);

            return new CommandResult(true);
        }
    }
}