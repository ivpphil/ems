﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using System.ComponentModel.DataAnnotations;

namespace ers.Domain.Member.Handlers
{
    public class ValidateMypageCardRecord
        : IValidationHandler<IMypageCardRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IMypageCardRecordCommand command)
        {
            //yield return CheckRequired("card_holder_name");
            yield return command.CheckRequired("card_type");
            yield return command.CheckRequired("card_no");
            yield return command.CheckRequired("validity_y");
            yield return command.CheckRequired("validity_m");

            yield return ErsFactory.ersOrderFactory.GetCheckValidityStgy().Check(command.validity_y, command.validity_m);
        }
    }
}