﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Handlers
{
    public class ValidateMemberUpdate
        : IValidationHandler<IMemberUpdateCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IMemberUpdateCommand command)
        {
            yield return command.CheckRequired("lname");
            yield return command.CheckRequired("fname");
            yield return command.CheckRequired("lnamek");
            yield return command.CheckRequired("fnamek");
            yield return command.CheckRequired("zip");
            yield return command.CheckRequired("pref");
            foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("pref", command.pref))
            {
                yield return result;
            }
            if (command.IsValidField("zip", "pref"))
            {
                yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("zip", command.zip, "pref", command.pref);
            }

            yield return command.CheckRequired("address");
            yield return command.CheckRequired("taddress");
            yield return command.CheckRequired("tel");
            yield return command.CheckRequired("email");
            yield return command.CheckRequired("mformat");
            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("mformat", EnumCommonNameType.MFormat, (int?)command.mformat);

            yield return command.CheckRequired("ques");
            foreach (var result in ErsFactory.ersMemberFactory.GetValidateQuesStgy().Validate(command.ques))
            {
                yield return result;
            }
            yield return command.CheckRequired("ans");

            if (!string.IsNullOrEmpty(command.passwd) || !string.IsNullOrEmpty(command.passwd_confirm))
            {
                yield return ErsFactory.ersMemberFactory.GetCheckPasswdConfirmStgy().CheckPasswdConfirm(command.passwd, command.passwd_confirm);
            }

            foreach (var result in ErsFactory.ersMemberFactory.GetValidateJobStgy().Validate(command.job))
            {
                yield return result;
            }

            yield return command.CheckRequired("sex");
            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("sex", EnumCommonNameType.Sex, (int?)command.sex);

            yield return command.CheckRequired("birthday_y");
            yield return command.CheckRequired("birthday_m");
            yield return command.CheckRequired("birthday_d");

            foreach (var result in ErsFactory.ersMemberFactory.GetValidateBirthdayStgy().Validate(command.birthday_y, command.birthday_m, command.birthday_d))
            {
                yield return result;
            }

            yield return ErsFactory.ersMemberFactory.GetCheckEmailConfirmStgy().CheckEmailConfirm(command.email, command.email_confirm);

            if (!string.IsNullOrEmpty(command.email))
            {
                yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailStgy().CheckDuplicate(ErsContext.sessionState.Get("mcode"), command.email);
            }

            //yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailAndNameStgy().CheckDuplicate(ErsContext.sessionState.Get("mcode"), email, lname, fname);

            //yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailAndPasswordStgy().CheckDuplicate(ErsContext.sessionState.Get("mcode"), email, passwd);

        }
    }
}