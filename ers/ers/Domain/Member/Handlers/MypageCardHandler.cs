﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order.strategy;

namespace ers.Domain.Member.Handlers
{
    public class MypageCardHandler
        : ICommandHandler<IMypageCardCommand>
    {
        IErsPaymentCreditBase ersGmo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD));
        PutRegularDateForwardStgy putRegularDateForwardStgy = ErsFactory.ersOrderFactory.GetPutRegularDateForwardStgy();

        public ICommandResult Submit(IMypageCardCommand command)
        {
            var mcode = ErsContext.sessionState.Get("mcode");
            var ersMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);

            //登録の場合
            if (command.entry_submit)
            {
                this.Insert(command, ersMember);
            }
            else if (command.delete_submit)
            {
                this.Delete(command, ersMember);
            }
            else if (command.modify_submit)
            {
                this.Update(command, ersMember);
            }

            return new CommandResult(true);
        }

        private void Update(IMypageCardCommand command, ErsMember ersMember)
        {
            ErsMemberCard newErsMemberCard;
            using (var tx = ErsDB_parent.BeginTransaction())
            {
                //新規カード預けを行う
                var updateCardInfo = command.listCardInfo.First((objCardInfo) => objCardInfo.card_id == command.card_id);

                var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
                memberCardCriteria.id = command.card_id;
                var oldMemberCard = memberCardRepository.FindSingle(memberCardCriteria);

                CreditCardInfo creditCardInfo = new CreditCardInfo(
                    ersMember.mcode, oldMemberCard.card_mcode, oldMemberCard.id, null, updateCardInfo.card_holder_name, updateCardInfo.card_type, updateCardInfo.card_no, updateCardInfo.validity_y, updateCardInfo.validity_m, null);

                newErsMemberCard = this.InsertCardData(ersMember, creditCardInfo);

                //定期伝票更新
                this.UpdateRegularOrderCardSequence(command.card_id, newErsMemberCard);

                tx.Commit();
            }

            //預けたカードのIDが削除対象のIDと同一の場合は、削除はしない
            if (command.card_id == newErsMemberCard.id)
            {
                return;
            }

            try
            {
                this.Delete(command, ersMember);
            }
            catch (Exception e)
            {
                //カード削除はエラー表示させない。
                //ログに落とす。
                ErsCommon.loggingException(e.ToString());

                if (ErsFactory.ersUtilityFactory.getSetup().debug)
                    throw;
            }
        }

        /// <summary>
        /// 定期明細で使用しているカードを削除した場合に、カードシーケンスを新しく登録したカード番号で更新する。
        /// </summary>
        /// <param name="command"></param>
        /// <param name="memberCard"></param>
        private void UpdateRegularOrderCardSequence(int? card_id, ErsMemberCard memberCard)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var regularOrderRepository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            var regularOrderCriteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
            regularOrderCriteria.member_card_id = card_id;

            var listOrderRecord = regularOrderRepository.Find(regularOrderCriteria);
            foreach (var record in listOrderRecord)
            {
                var oldRecord = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordWithId(record.id);
                record.member_card_id = memberCard.id;

                // put next day forward if today past the :next_date + create_regular_order_days.
                while (true)
                {
                    if ((record.next_date.Value.AddDays(-setup.create_regular_order_days)) > DateTime.Now.Date)
                    {
                        break;
                    }

                    putRegularDateForwardStgy.Execute(record, false);
                }
                regularOrderRepository.Update(oldRecord, record);
            }
        }

        /// <summary>
        /// カード預け処理を行う。
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ersMember"></param>
        /// <returns></returns>
        private void Insert(IMypageCardCommand command, ErsMember ersMember)
        {
            using (var tx = ErsDB_parent.BeginTransaction())
            {
                //新規登録の場合はcard_sequenceはNULLにする
                CreditCardInfo creditCardInfo = new CreditCardInfo(
                    ersMember.mcode, null, null, null, command.card_holder_name, command.card_type, command.card_no, command.validity_y, command.validity_m, null);

                this.InsertCardData(ersMember, creditCardInfo);

                tx.Commit();
            }
        }

        /// <summary>
        /// カード預けとDBInsertをおこなう
        /// </summary>
        /// <param name="ersMember"></param>
        /// <param name="creditCardInfo"></param>
        /// <returns></returns>
        private ErsMemberCard InsertCardData(ErsMember ersMember, CreditCardInfo creditCardInfo)
        {
            var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();

            //カード預け
            var newErsMemberCard = ersGmo.SaveMemberCardInfo(creditCardInfo, ersMember);

            var oldErsMemberCard = ErsFactory.ersMemberFactory.GetRetrieveAlreadyStoredMemberCardStgy().Retrieve(newErsMemberCard.card_mcode, newErsMemberCard.card_sequence);
            if (oldErsMemberCard != null)
            {
                newErsMemberCard = ErsFactory.ersMemberFactory.GetErsMemberCard();
                newErsMemberCard.OverwriteWithParameter(oldErsMemberCard.GetPropertiesAsDictionary());
                newErsMemberCard.update_status = EnumCardUpdateStatus.Success;

                memberCardRepository.Update(oldErsMemberCard, newErsMemberCard);

                return oldErsMemberCard;
            }

            //IVP側カード処理
            memberCardRepository.Insert(newErsMemberCard, true);

            return newErsMemberCard;
        }

        private void Delete(IMypageCardCommand command, ErsMember ersMember)
        {
            using (var tx = ErsDB_parent.BeginTransaction())
            {
                var cardInfo = ersGmo.ObtainMemberCardInfo(ersMember, command.card_id);

                //GMO側保持カードの削除　今回DBに登録する情報取得
                var newErsMemberCard = ersGmo.deleteCardInfo(cardInfo, ersMember);

                //削除対象
                var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
                memberCardCriteria.active = EnumActive.Active;
                memberCardCriteria.mcode = ersMember.mcode;
                memberCardCriteria.id = command.card_id;
                var oldErsMemberCard = memberCardRepository.Find(memberCardCriteria).First();

                //IVP側カード処理
                memberCardRepository.Update(oldErsMemberCard, newErsMemberCard);
                tx.Commit();
            }
        }
    }
}