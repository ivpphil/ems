﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ers.Domain.Member.Mappables
{
    public interface IAddressListMappable
        : IMappable
    {
        ErsPagerModel pager { get; set; }

        List<Dictionary<string, object>> list { get; set; }

        long recordCount { get; set; }
    }
}