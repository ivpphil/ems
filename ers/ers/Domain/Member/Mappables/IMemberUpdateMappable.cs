﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.member;

namespace ers.Domain.Member.Mappables
{
    public interface IMemberUpdateMappable
        : IMappable
    {
        bool IsConfirmationPage { get; }

        ErsMember member { get; set; }

        string email_confirm { get; set; }

        string email { get; }

        string passwd { get; set; }

        string passwd_confirm { get; set; }

        int? birthday_y { get; set; }

        int? birthday_m { get; set; }

        int? birthday_d { get; set; }
    }
}