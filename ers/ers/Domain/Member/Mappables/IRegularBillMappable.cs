﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ers.Domain.Member.Mappables
{
    public interface IRegularBillMappable
        : IMappable
    {
        IEnumerable<Dictionary<string, object>> orderRecordList { get; set; }
        bool nonActiveOnly { get; set; }

    }
}
