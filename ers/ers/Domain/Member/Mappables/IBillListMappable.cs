﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ers.Domain.Member.Mappables
{
    public interface IBillListMappable
        : IMappable
    {
        ErsPagerModel pager { get; }

        long recordCount { get; set; }

        IEnumerable<Dictionary<string, object>> list { get; set; }

        DateTime? s_date1 { get; set; }

        DateTime? s_date2 { get; set;  }
    }
}