﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ers.Models.member;

namespace ers.Domain.Member.Mappables
{
    public interface IMypageCardMappable
        : IMappable
    {
        List<MypageCardRecord> listCardInfo { get; set; }

        bool delete_submit { get; set; }

        int? card_id { get; set; }

        string card_holder_name { get; set; }

        int? card_type { get; set; }

        string card_no { get; set; }

        string w_cardno { get; set; }

        int? validity_y { get; set; }

        int? validity_m { get; set; }

        bool entry_submit { get; set; }

        bool modify_submit { get; set; }

        string confirm_card_holder_name { get; set; }

        string confirm_card_type { get; set; }

        string confirm_cardno { get; set; }

        int? confirm_validity_y { get; set; }

        int? confirm_validity_m { get; set; }
    }
}