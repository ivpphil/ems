﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ers.Domain.Member.Mappables
{
    public interface IAddressDeleteMappable
        : IMappable
    {
        int? id { get; set; }
    }
}