﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Mappables
{
    public interface IPassrimMappable
        : IMappable
    {
        string mcode { get; set; }

        string email { get; set; }

        string lname { get; set; }

        string fname { get; set; }

        string changeUrl { get; set; }

        EnumMformat? mformat { get; set; }
    }
}