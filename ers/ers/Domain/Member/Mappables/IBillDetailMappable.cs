﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ers.Domain.Member.Mappables
{
    public interface IBillDetailMappable
        : IMappable
    {
        IEnumerable<Dictionary<string, object>> orderRecords { get; set; }

        string d_no { get; }

        string mcode { get; }

        DateTime? indate { set; }

        string zip { set; }

        string w_pref { set; }

        string address { set; }

        string taddress { set; }

        string maddress { set; }

        string tel { set; }

        string w_sendtime { set; }

        string w_payment_order_status { set; }

        string w_pay { set; }

        int amounttotal { set; }

        string w_etc { set; }

        string delivery { set; }

        string url { set; }

        string lname { set; }

        string fname { set; }

        string w_order_status { set; }

        string w_order_status2 { set; }

        IEnumerable<Dictionary<string, string>> sendnos { get; set; }

        string sendno { set; }

        int? p_service { set; }

        string memo2 { get; set; }
        string w_wrap { get; set; }
        EnumPaymentType? pay { get; set; }
        string w_conv_code { get; set; }
    }
}