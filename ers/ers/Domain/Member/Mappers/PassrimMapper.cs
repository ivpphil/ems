﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ers.Domain.Member.Mappers
{
    public class PassrimMapper
        : IMapper<IPassrimMappable>
    {
        public void Map(IPassrimMappable objMappable)
        {
            // mcode取得
            var member = this.GetMcodeWithEmail(objMappable.email, objMappable.lname, objMappable.fname);
            var mcode = member.mcode;
            objMappable.mcode = mcode;
            objMappable.mformat = member.mformat;

            ErsPasswodReminderService passrimService = ErsFactory.ersMemberFactory.GetErsPasswordReminderService();
            var ransu = passrimService.MakePassRansu(mcode);

            //エンコード
            var encObj = ErsFactory.ersUtilityFactory.getErsEncryption();
            var encMcode = encObj.HexEncode(mcode);
            var encRansu = encObj.HexEncode(ransu);

            //パラメーター生成
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            string param = "?enc_ransu=" + encRansu + "&enc_mcode=" + encMcode;
            objMappable.changeUrl = setup.sec_url + setup.change_url + param;
        }

        /// <summary>
        /// Eメールからmcodeを取得する。
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private ErsMember GetMcodeWithEmail(string email, string lname, string fname)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();

            criteria.email = email;
            criteria.lname = lname;
            criteria.fname = fname;
            criteria.deleted = EnumDeleted.NotDeleted;
            criteria.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();
            var list = repository.Find(criteria);

            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }
    }
}