﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace ers.Domain.Member.Mappers
{
    public class BillDetailMapper
        : IMapper<IBillDetailMappable>
    {
        private Setup setup { get; set; }

        public void Map(IBillDetailMappable objMappable)
        {
            this.setup = ErsFactory.ersUtilityFactory.getSetup();

            var objOrder = this.GetFindData(objMappable);

            objMappable.OverwriteWithParameter(objOrder.GetPropertiesAsDictionary());

            var orderRecords = this.GetOrderRecords(objOrder.d_no);
            objMappable.orderRecords = this.GetOrderRecordsForDisplay(orderRecords);

            objMappable.indate = objOrder.intime;

            objMappable.sendnos = this.GetSendnoList(orderRecords);


            var order_status = ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy().ObtainFrom(orderRecords);
            if (order_status != null)
            {
                EnumFrontOrderStatus front_status;
                switch (order_status)
                {
                    case EnumOrderStatusType.NEW_ORDER:
                    case EnumOrderStatusType.DELIVER_WAITING:
                    case EnumOrderStatusType.DELIVER_REQUEST:
                        front_status = EnumFrontOrderStatus.NEW_ORDER;
                        break;
                    case EnumOrderStatusType.DELIVERED:
                        front_status = EnumFrontOrderStatus.DELIVERED;
                        break;
                    case EnumOrderStatusType.CANCELED_AFTER_DELIVER:
                    case EnumOrderStatusType.CANCELED:
                        front_status = EnumFrontOrderStatus.CANCELED;
                        break;
                    default:
                        throw new Exception("undefined order status");
                }
                objMappable.w_order_status = ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().GetStringFromId(order_status);
                objMappable.w_order_status2 = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.FrontOrderStatus, EnumCommonNameColumnName.namename, (int)front_status);
            }


            var shippingAddress = ErsFactory.ersOrderFactory.GetShippingAddressInfo();
            shippingAddress.LoadShippingAddress(objOrder);
            objMappable.zip = shippingAddress.shipping_zip;
            objMappable.w_pref = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(shippingAddress.shipping_pref);
            objMappable.address =  shippingAddress.shipping_address;
            objMappable.taddress = shippingAddress.shipping_taddress;
            objMappable.maddress =  shippingAddress.shipping_maddress;
            objMappable.tel = shippingAddress.shipping_tel;
            objMappable.lname = shippingAddress.shipping_lname;
            objMappable.fname = shippingAddress.shipping_fname;

            objMappable.w_sendtime = ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId(objOrder.sendtime);
            objMappable.w_payment_order_status = ErsFactory.ersViewServiceFactory.GetErsViewOrderPaymentStatusService().GetStringFromId(objOrder.order_payment_status);
            objMappable.pay = objOrder.pay;
            objMappable.w_conv_code = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ConvCode, EnumCommonNameColumnName.namename, (int?)objOrder.conv_code);
            objMappable.w_pay = ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(objOrder.pay);
            objMappable.amounttotal = ErsFactory.ersOrderFactory.GetObtainAmountTotalStgy().Obtain(orderRecords);
            objMappable.w_etc = ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetEtcNameFromId(objOrder.pay);
            objMappable.w_wrap = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Wrap, EnumCommonNameColumnName.namename, (int?)objOrder.wrap);
            objMappable.memo2 = objOrder.memo2;

            objMappable.delivery = setup.delivery;
            objMappable.sendno = orderRecords.First<ErsOrderRecord>().sendno;
            if (orderRecords.First<ErsOrderRecord>().deliv_method == EnumDelvMethod.Express)
            {
                objMappable.url = setup.url;
            }
            else
            {
                objMappable.url = setup.mail_url;
            }
        }

        /// <summary>
        /// 伝票詳細の取得
        /// </summary>
        internal ErsOrder GetFindData(IBillDetailMappable objMappable)
        {

            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var addCri = this.SetOrderCriteria(objMappable);

            var ordList = repository.Find(addCri);
            if (ordList.Count != 1)
            {
                //対象データ無し
                throw new ErsException("10200");
            }
            var objOrder = ordList.ElementAt(0);

            return objOrder;

        }

        //検索条件をセット
        private ErsOrderCriteria SetOrderCriteria(IBillDetailMappable objMappable)
        {
            var OrderCriteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            //伝票№指定
            OrderCriteria.d_no = objMappable.d_no;
            OrderCriteria.mcode = objMappable.mcode;
            OrderCriteria.site_id = setup.site_id;
            return OrderCriteria;
        }

        private IEnumerable<ErsOrderRecord> GetOrderRecords(string d_no)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
            criteria.d_no = d_no;
            criteria.doc_bundling_flg = EnumDocBundlingFlg.OFF;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            return repository.Find(criteria);
        }

        public IEnumerable<Dictionary<string, object>> GetOrderRecordsForDisplay(IEnumerable<ErsOrderRecord> orderRecords)
        {
            foreach (var record in orderRecords)
            {
                var retDictionary = record.GetPropertiesAsDictionary();

                retDictionary["amount"] = record.GetAmount();
                retDictionary.Add("w_order_status", ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().GetStringFromId(record.order_status));
                if (record.order_status == EnumOrderStatusType.CANCELED)
                {
                    retDictionary.Add("cancelFlg", true);
                }
                else
                {
                    retDictionary.Add("cancelFlg", false);
                }

                if (record.order_status == EnumOrderStatusType.CANCELED_AFTER_DELIVER)
                {
                    retDictionary.Add("cancelAfterDeliverFlg", true);
                }
                else
                {
                    retDictionary.Add("cancelAfterDeliverFlg", false);
                }


                yield return retDictionary;
            }
        }

        public IEnumerable<Dictionary<string, string>> GetSendnoList(IEnumerable<ErsOrderRecord> orderRecords)
        {
            var retDictionary = new Dictionary<string, string>();
            foreach (var rec in orderRecords)
            {
                var dic = new Dictionary<string, string>();
                if (!string.IsNullOrEmpty(rec.sendno))
                {
                    if (!retDictionary.ContainsKey(rec.sendno))
                    {
                        retDictionary.Add(rec.sendno, rec.sendno);
                        dic.Add("sendno", rec.sendno);

                        yield return dic;
                    }
                }
            }
        }
    }
}