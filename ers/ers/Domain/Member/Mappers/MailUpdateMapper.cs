﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Mappers
{
    public class MailUpdateMapper
        : IMapper<IMailUpdateMappable>
    {
        public void Map(IMailUpdateMappable objMappable)
        {
            this.LoadMemberData(objMappable);
        }

        internal void LoadMemberData(IMailUpdateMappable objMappable)
        {
            //メンバー読み込み
            var member = ErsFactory.ersMemberFactory.getErsMemberWithRansu(ErsContext.sessionState);
            objMappable.m_flg = member.m_flg.Value;
        }
    }
}