﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;

namespace ers.Domain.Member.Mappers
{
    public class RegularBillMapper
        : IMapper<IRegularBillMappable>
    {
        public void Map(IRegularBillMappable objMappable)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderCriteria();
            criteria.mcode = ErsContext.sessionState.Get("mcode");
            if (objMappable.nonActiveOnly)
            {
                criteria.SetNonActiveOnly();
            }
            else
            {
                criteria.SetActiveOnly();
            }
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            criteria.SetIntimeOrderBy(Criteria.OrderBy.ORDER_BY_DESC);

            var orderList = repository.Find(criteria);

            if (orderList == null || orderList.Count == 0)
            {
                if (objMappable.nonActiveOnly)
                {
                    objMappable.controller.AddInformation(ErsResources.GetMessage("20307"));
                }
                else
                {
                    objMappable.controller.AddInformation(ErsResources.GetMessage("20306"));
                }
                return;
            }

            objMappable.orderRecordList = this.GetOrderRecordList(orderList, objMappable);
        }

        private IEnumerable<Dictionary<string, object>> GetOrderRecordList(IList<ErsRegularOrder> orderList, IRegularBillMappable objMappable)
        {
            var cnt = 0;
            foreach (var regularOrder in orderList)
            {
                foreach (var orderRecord in regularOrder.regularOrderRecords)
                {
                    if (targetRec(orderRecord, objMappable))
                    {
                        cnt++;

                        //前回お届け日が本日以降の場合は、前回お届け日を次回お届け日とする
                        if (orderRecord.last_date > DateTime.Now)
                        {
                            orderRecord.next5_date = orderRecord.next4_date;
                            orderRecord.next5_sendtime = orderRecord.next4_sendtime;
                            orderRecord.next4_date = orderRecord.next3_date;
                            orderRecord.next4_sendtime = orderRecord.next3_sendtime;
                            orderRecord.next3_date = orderRecord.next2_date;
                            orderRecord.next3_sendtime = orderRecord.next2_sendtime;
                            orderRecord.next2_date = orderRecord.next_date;
                            orderRecord.next2_sendtime = orderRecord.next_sendtime;
                            orderRecord.next_date = orderRecord.last_date;
                            orderRecord.next_sendtime = orderRecord.last_sendtime;
                        }

                        //会員に関する情報取得   
                        var ersMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(orderRecord.mcode);

                        var regularRegister = ErsFactory.ersOrderFactory.GetErsRegularRegister();
                        regularRegister.LoadValues(orderRecord.GetPropertiesAsDictionary(), ersMember);

                        var ersOrder = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(regularRegister.GetPropertiesAsDictionary());
                        var resultDictionary = this.GetDictionary(regularOrder, ersOrder, orderRecord);
                        resultDictionary.Add("bill_count", cnt);
                        var commonNameService = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService();
                        resultDictionary.Add("last_date_weekday", commonNameService.GetStringFromId(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename, (int)orderRecord.last_date.Value.DayOfWeek));
                        resultDictionary.Add("next_date_weekday", commonNameService.GetStringFromId(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename, (int)orderRecord.next_date.Value.DayOfWeek));
                        resultDictionary.Add("next2_date_weekday", commonNameService.GetStringFromId(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename, (int)orderRecord.next2_date.Value.DayOfWeek));
                        resultDictionary.Add("next3_date_weekday", commonNameService.GetStringFromId(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename, (int)orderRecord.next3_date.Value.DayOfWeek));
                        resultDictionary.Add("next4_date_weekday", commonNameService.GetStringFromId(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename, (int)orderRecord.next4_date.Value.DayOfWeek));
                        resultDictionary.Add("next5_date_weekday", commonNameService.GetStringFromId(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename, (int)orderRecord.next5_date.Value.DayOfWeek));
                        resultDictionary.Add("ptn_day_name", ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.PtnDay, EnumCommonNameColumnName.namename, orderRecord.ptn_day));
                        resultDictionary.Add("ptn_weekday_name", ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename, (int?)orderRecord.ptn_weekday));
                        resultDictionary.Add("ptn_interval_name", ErsFactory.ersViewServiceFactory.GetErsRegularOrderViewService().GetDeliveryTerm(orderRecord));

                        resultDictionary.Add("next_sendtime_name", ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId(orderRecord.next_sendtime_base));

                        //#19602 解約日表示判定フラグセット
                        if (orderRecord.delete_date == null)
                        {
                            resultDictionary.Add("deleteDateDispFlg", false);
                        }
                        else
                        {
                            resultDictionary.Add("deleteDateDispFlg", true);
                        }

                        //#19602 次々回表示判定フラグセット
                        if (orderRecord.next2_date > orderRecord.delete_date)
                        {
                            resultDictionary.Add("nextDispFlg", false);
                        }
                        else
                        {
                            resultDictionary.Add("nextDispFlg", true);
                        }

                        resultDictionary.Add("dateLabelFlg", (orderRecord.delete_date < DateTime.Now));

                        yield return resultDictionary;
                    }
                }
            }
        }

        private bool targetRec(ErsRegularOrderRecord orderRecord, IRegularBillMappable objMappable)
        {
            if (objMappable.nonActiveOnly)
            {
                if ((orderRecord.delete_date <= orderRecord.next_date && orderRecord.last_date <= DateTime.Now))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(orderRecord.delete_date.ToString()) || orderRecord.delete_date > orderRecord.next_date || orderRecord.last_date > DateTime.Now)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        private Dictionary<string, object> GetDictionary(ErsRegularOrder regularOrder, ErsOrder ersOrder, ErsRegularOrderRecord orderRecord)
        {
            var dicHeader = new Dictionary<string, object>();
            dicHeader["d_no"] = regularOrder.d_no;
            dicHeader["intime"] = regularOrder.intime;

            dicHeader["pay_name"] = ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(ersOrder.pay);

            var shippingAddress = ErsFactory.ersOrderFactory.GetShippingAddressInfo();
            shippingAddress.LoadShippingAddress(ersOrder);

            dicHeader["zip"] = shippingAddress.shipping_zip;
            dicHeader["pref_name"] = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(shippingAddress.shipping_pref);
            dicHeader["address"] = shippingAddress.shipping_address;
            dicHeader["maddress"] = shippingAddress.shipping_maddress;
            dicHeader["taddress"] = shippingAddress.shipping_taddress;
            dicHeader["tel"] = shippingAddress.shipping_tel;

            var dicRet = orderRecord.GetPropertiesAsDictionary();
            dicRet["header"] = dicHeader;
            return dicRet;
        }
    }
}