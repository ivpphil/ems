﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Mappers
{
    public class PasschangeMapper
        : IMapper<IPasschangeMappable>
    {
        public void Map(IPasschangeMappable objMappable)
        {
            var encObj = ErsFactory.ersUtilityFactory.getErsEncryption();
            var mcode = encObj.HexDecode(objMappable.enc_mcode);

            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);
            if (member != null)
            {
                objMappable.w_ques = ErsFactory.ersViewServiceFactory.GetErsViewQuesService().GetStringFromId(member.ques);
            }
            else
            {
                throw new ErsException("10210");
            }
        }
    }
}