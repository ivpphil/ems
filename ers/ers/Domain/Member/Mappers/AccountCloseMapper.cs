﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Mappers
{
    public class AccountCloseMapper
        : IMapper<IAccountCloseMappable>
    {
        public void Map(IAccountCloseMappable objMappable)
        {
            var mcode = ErsContext.sessionState.Get("mcode");
            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);

            objMappable.OverwriteWithParameter(member.GetPropertiesAsDictionary());
        }
    }
}