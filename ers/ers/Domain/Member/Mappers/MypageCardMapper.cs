﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order.related;
using ers.Models;
using ers.Models.member;

namespace ers.Domain.Member.Mappers
{
    public class MypageCardMapper
        : IMapper<IMypageCardMappable>
    {
        public void Map(IMypageCardMappable objMappable)
        {
            var listCardInfo = this.GetFindCardList(ErsContext.sessionState.Get("mcode"));

            //削除の場合
            if (objMappable.delete_submit)
            {
                objMappable.listCardInfo = listCardInfo;
                this.SetConfirmationForDelete(objMappable);
            }
            else if (objMappable.entry_submit)
            {
                objMappable.listCardInfo = this.OverwriteInputCardInfo(objMappable, listCardInfo);
                this.SetConfirmationForEntry(objMappable);
            }
            else if (objMappable.modify_submit)
            {
                objMappable.listCardInfo = this.OverwriteInputCardInfo(objMappable, listCardInfo);
                this.SetConfirmationForModify(objMappable);

                var mcode = ErsContext.sessionState.Get("mcode");
                var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);

                // 継続課金未送信の伝票がある場合は、新規登録となる旨のメッセージを表示する。
                if (ErsFactory.ersMemberFactory.GetHasNotYetSaledIssuedRegularOrderSpec().Has(mcode, objMappable.card_id, member.site_id))
                {
                    objMappable.controller.AddInformation(ErsResources.GetMessage("20308"));

                    if (ErsFactory.ersMemberFactory.GetIsCardHasRegularOrderSpec().Has(mcode, objMappable.card_id))
                    {
                        objMappable.controller.AddInformation(ErsResources.GetMessage("20309"));
                    }
                }
            }
            else
            {
                objMappable.listCardInfo = listCardInfo;
            }
        }

        private void SetConfirmationForModify(IMypageCardMappable objMappable)
        {
            if (!objMappable.IsValid)
            {
                return;
            }

            var modifyRecord = objMappable.listCardInfo.First(record => record.card_id == objMappable.card_id);
            objMappable.confirm_card_holder_name = modifyRecord.card_holder_name;
            objMappable.confirm_card_type = ErsFactory.ersViewServiceFactory.GetErsViewCardService().GetStringFromId(modifyRecord.card_type);
            objMappable.confirm_cardno = this.MaskString(modifyRecord.card_no, '*', 4);

            objMappable.confirm_validity_y = modifyRecord.validity_y;
            objMappable.confirm_validity_m = modifyRecord.validity_m;
        }

        private void SetConfirmationForEntry(IMypageCardMappable objMappable)
        {
            if (!objMappable.IsValid)
            {
                return;
            }

            objMappable.confirm_card_holder_name = objMappable.card_holder_name;
            objMappable.confirm_card_type = ErsFactory.ersViewServiceFactory.GetErsViewCardService().GetStringFromId(objMappable.card_type);
            objMappable.confirm_cardno = this.MaskString(objMappable.card_no, '*', 4);

            objMappable.confirm_validity_y = objMappable.validity_y;
            objMappable.confirm_validity_m = objMappable.validity_m;
        }

        //削除画面用個別データ取得
        public void SetConfirmationForDelete(IMypageCardMappable objMappable)
        {
            if (!objMappable.IsValid)
            {
                return;
            }

            var mcode = ErsContext.sessionState.Get("mcode");

            var creditCardInfo = this.GetFindCardData(mcode, objMappable.card_id);
            objMappable.confirm_card_holder_name = creditCardInfo.card_holder_name;
            objMappable.confirm_card_type = ErsFactory.ersViewServiceFactory.GetErsViewCardService().GetStringFromId(creditCardInfo.card_type);
            objMappable.confirm_cardno = this.MaskString(creditCardInfo.card_no, '*', 4);

            objMappable.confirm_validity_y = creditCardInfo.validity_y;
            objMappable.confirm_validity_m = creditCardInfo.validity_m;
        }

        //確認画面用個別データ取得
        public CreditCardInfo GetFindCardData(string mcode, int? card_id)
        {
            var ersMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);
            var ersGmo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD));

            var creditCardInfo = ersGmo.ObtainMemberCardInfo(ersMember, card_id);
            if (creditCardInfo == null)
            {
                //対象データ無し
                throw new ErsException("10200");
            }

            return creditCardInfo;
        }

        //一覧データ取得
        public List<MypageCardRecord> GetFindCardList(string mcode)
        {
            var ersMember = ErsFactory.ersMemberFactory.GetErsMember();
            var ersGmo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD));

            ersMember.mcode = mcode;

            var listCard = ersGmo.ObtainMemberCardInfo(ersMember);

            var listResult = new List<MypageCardRecord>();
            var lineNumber = 1;
            foreach (var record in listCard)
            {
                var cardRecord = new MypageCardRecord();
                cardRecord.card_id = record.card_id;
                cardRecord.saved_card_holder_name = record.card_holder_name;
                cardRecord.saved_card_name = record.card_name;
                cardRecord.saved_card_type = record.card_type;
                cardRecord.saved_card_no = record.card_no;
                cardRecord.saved_validity_y = record.validity_y;
                cardRecord.saved_validity_m = record.validity_m;
                cardRecord.lineNumber = lineNumber++;
                listResult.Add(cardRecord);
            }
            return listResult;
        }

        /// <summary>
        /// カード情報に入力情報を上書きする
        /// </summary>
        /// <param name="objMappable"></param>
        /// <param name="listCardInfo"></param>
        /// <returns></returns>
        public List<MypageCardRecord> OverwriteInputCardInfo(IMypageCardMappable objMappable, List<MypageCardRecord> listCardInfo)
        {
            if (objMappable.listCardInfo == null)
            {
                return listCardInfo;
            }

            foreach (var data in listCardInfo)
            {
                if (objMappable.card_id != data.card_id)
                {
                    continue;
                }
                try
                {
                    var modifyRecord = objMappable.listCardInfo.First(record => record.card_id == objMappable.card_id);

                    if (modifyRecord != null)
                    {
                        data.card_holder_name = modifyRecord.card_holder_name;
                        data.card_type = modifyRecord.card_type;
                        data.card_no = modifyRecord.card_no;
                        data.validity_y = modifyRecord.validity_y;
                        data.validity_m = modifyRecord.validity_m;
                    }
                }
                catch
                {
                    throw new ErsException("10043", ErsFactory.ersUtilityFactory.getSetup().sec_url + ErsFactory.ersUtilityFactory.getSetup().mypage_url);
                }
            }

            return listCardInfo;
        }

        /// <summary>
        /// 文字列をマスクする
        /// </summary>
        /// <param name="value">マスクしたい文字列</param>
        /// <param name="mask">マスクに使用する文字</param>
        /// <param name="remain">残す文字数</param>
        /// <returns>マスク後の文字列</returns>
        public string MaskString(string value, char mask, int remain)
        {
            if (!value.HasValue() || value.Length < remain)
            {
                return null;
            }

            return new String(mask, value.Length - remain) + VBStrings.Right(value, remain);
        }
    }
}