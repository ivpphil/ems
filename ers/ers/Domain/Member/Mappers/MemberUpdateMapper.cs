﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Mappers
{
    public class MemberUpdateMapper
        : IMapper<IMemberUpdateMappable>
    {
        public void Map(IMemberUpdateMappable objMappable)
        {
            if (!objMappable.IsConfirmationPage)
            {
                this.LoadDefaultData(objMappable);
            }
            else
            {
                this.LoadNewMemberData(objMappable);
            }
        }

        /// <summary>
        /// Load default member data
        /// </summary>
        internal void LoadDefaultData(IMemberUpdateMappable objMappable)
        {
            objMappable.member = ErsFactory.ersMemberFactory.getErsMemberWithRansu(ErsContext.sessionState);
            objMappable.OverwriteWithParameter(objMappable.member.GetPropertiesAsDictionary());
            objMappable.email_confirm = objMappable.email;
            objMappable.passwd = string.Empty;
            objMappable.passwd_confirm = string.Empty;

            if (objMappable.member.birth != null)
            {
                objMappable.birthday_y = objMappable.member.birth.Value.Year;
                objMappable.birthday_m = objMappable.member.birth.Value.Month;
                objMappable.birthday_d = objMappable.member.birth.Value.Day;
            }
        }

        /// <summary>
        /// Load a new ErsMember that overwritten by model.
        /// </summary>
        public void LoadNewMemberData(IMemberUpdateMappable objMappable)
        {
            objMappable.member = ErsFactory.ersMemberFactory.getErsMemberWithRansu(ErsContext.sessionState);
            objMappable.member.OverwriteWithModel(objMappable);
            var age = ErsFactory.ersMemberFactory.GetGetAgeStgy().GetAge(objMappable.member.birth);
            objMappable.member.age_code = ErsFactory.ersMemberFactory.GetGetAgeCodeStgy().GetAgeCode(age);
            if (string.IsNullOrEmpty(objMappable.passwd) && string.IsNullOrEmpty(objMappable.passwd_confirm))
            {
                //未入力の場合は、DBの値を使用する。
                var old_member = ErsFactory.ersMemberFactory.getErsMemberWithRansu(ErsContext.sessionState);
                objMappable.member.passwd = old_member.passwd;
            }
        }
    }
}