﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using ers.Models;

namespace ers.Domain.Member.Mappers
{
    public class PointHistoryMapper
        : IMapper<IPointHistoryMappable>
    {
        public void Map(IPointHistoryMappable objMappable)
        {
            //アドレスリスト取得
            objMappable.list = this.getPointBalanceOpe(objMappable);
        }

        /// <summary>
        /// 会員コードをキーに表示データ取得
        /// </summary>
        internal List<Dictionary<string, object>> getPointBalanceOpe(IPointHistoryMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //ポイント用リポジトリクラス　インスタンス化処理
            var repository = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryRepository();

            //ポイント用クライテリアクラス　インスタンス化処理
            var pointCri = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryCriteria();

            //検索条件をクライテリアへ
            var mcode = ErsContext.sessionState.Get("mcode");
            pointCri.mcode = mcode;
            pointCri.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetPointSiteId(setup.site_id);
            
            if (objMappable.s_date1.HasValue)
            {
                pointCri.dt_from = objMappable.s_date1.Value;
            }
            if (objMappable.s_date2 != null)
            {
                pointCri.dt_to = objMappable.s_date2.Value.GetEndForSearch();
            }

            //ページ送り用に全件取得
            objMappable.recordCount = repository.GetRecordCount(pointCri);

            //検索SQLにLIMIT と OFFSETを加える
            pointCri.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);

            objMappable.pager.SetLimitAndOffsetToCriteria(pointCri);

            //検索した結果を返す
            var list = repository.Find(pointCri);

            var listPoint = new List<Dictionary<string, object>>();
            foreach (var objPoint in list)
            {
                var dictionary = objPoint.GetPropertiesAsDictionary();

                dictionary["display_type"] = this.GetDisplayType(objPoint);
                if (objPoint.d_no.HasValue())
                {
                    var order = ErsFactory.ersOrderFactory.GetOrderWithD_no(objPoint.d_no, setup.site_id);
                    if (order != null)
                    {
                        dictionary["total"] = order.total;
                    }
                }

                listPoint.Add(dictionary);
            }

            return listPoint;
        }

        private EnumDisplayType GetDisplayType(global::jp.co.ivp.ers.member.ErsPointHistory objPoint)
        {
            if (objPoint.now_p < 0)
            {
                return EnumDisplayType.Use;
            }
            else
            {
                return EnumDisplayType.Add;
            }
        }

        public enum EnumDisplayType
        {
            /// <summary>
            /// ポイント使用
            /// </summary>
            Use,

            /// <summary>
            /// ポイント付与
            /// </summary>
            Add,
        }
    }
}