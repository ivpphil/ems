﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Mappers
{
    public class BillListMapper
        : IMapper<IBillListMappable>
    {
        public void Map(IBillListMappable objMappable)
        {
            var orderList = this.GetFindList(objMappable);

            var list = new List<Dictionary<string, object>>();
            var orderRec_list = new List<Dictionary<string, object>>();
            if (orderList == null)
            {
                return;
            }

            foreach (var order in orderList)
            {
                var orderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order);

                var dictionary = order.GetPropertiesAsDictionary();
                var order_status = ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy().ObtainFrom(orderRecords.Values);
                if (order_status != null)
                {
                    EnumFrontOrderStatus front_status;
                    switch (order_status)
                    {
                        case EnumOrderStatusType.NEW_ORDER:
                        case EnumOrderStatusType.DELIVER_WAITING:
                        case EnumOrderStatusType.DELIVER_REQUEST:
                            front_status = EnumFrontOrderStatus.NEW_ORDER;
                            break;
                        case EnumOrderStatusType.DELIVERED:
                            front_status = EnumFrontOrderStatus.DELIVERED;
                            break;
                        case EnumOrderStatusType.CANCELED_AFTER_DELIVER:
                        case EnumOrderStatusType.CANCELED:
                            front_status = EnumFrontOrderStatus.CANCELED;
                            break;
                        default:
                            throw new Exception("undefined order status");
                    }
                    dictionary["w_order_status"] = ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().GetStringFromId(order_status);
                    dictionary["w_order_status2"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.FrontOrderStatus, EnumCommonNameColumnName.namename, (int)front_status);                    
                }

                /// Get the product name 
                var orderRecs = this.GetOrderRecords(order.d_no);
                dictionary.Add("sname_list", orderRecs);

                list.Add(dictionary);
            }
            objMappable.list = list;
        }

        /// <summary>
        /// 伝票履歴の取得
        /// </summary>
        internal IList<ErsOrder> GetFindList(IBillListMappable objMappable)
        {
            ErsOrderRepository repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            ErsOrderCriteria addCri = this.SetOrderCriteria(objMappable);
            addCri.ds_doc_bundling_flg = EnumDocBundlingFlg.OFF;

            objMappable.recordCount = repository.GetRecordCount(addCri);

            //検索SQLにLIMIT と OFFSETを加える
            objMappable.pager.SetLimitAndOffsetToCriteria(addCri);

            addCri.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);

            return repository.Find(addCri);

        }

        //検索条件をセット
        private ErsOrderCriteria SetOrderCriteria(IBillListMappable objMappable)
        {
            ErsOrderCriteria OrderCriteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            OrderCriteria.intime_from = DateTime.Parse(objMappable.s_date1.Value.ToString("yyyy/MM/dd 00:00:00"));
            OrderCriteria.intime_to = DateTime.Parse(objMappable.s_date2.Value.ToString("yyyy/MM/dd 23:59:59"));

            var mcode = ErsContext.sessionState.Get("mcode");
            OrderCriteria.mcode = mcode;
            OrderCriteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            return OrderCriteria;

        }

        private IEnumerable<ErsOrderRecord> GetOrderRecords(string d_no)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
            criteria.d_no = d_no;
            criteria.doc_bundling_flg = EnumDocBundlingFlg.OFF;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            return repository.Find(criteria);
        }
    }
}