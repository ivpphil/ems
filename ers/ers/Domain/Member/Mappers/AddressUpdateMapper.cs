﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Mappers
{
    public class AddressUpdateMapper
        : IMapper<IAddressUpdateMappable>
    {
        public void Map(IAddressUpdateMappable objMappable)
        {
            this.Initilize(objMappable);
        }

        internal void Initilize(IAddressUpdateMappable objMappable)
        {
            //該当会員の住所取得
            if (objMappable.id != null)
            {
                var eai = this.GetFindData(objMappable.id.Value);
                objMappable.OverwriteWithParameter(eai.GetPropertiesAsDictionary());
            }
        }

        /// <summary>
        /// 別お届け先IDを元にデータ取得
        /// </summary>
        internal ErsAddressInfo GetFindData(int id)
        {
            ErsAddressInfoRepository repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            ErsAddressInfoCriteria addCri = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

            //検索条件をクライテリアに保存
            addCri.id = id;
            addCri.mcode = ErsContext.sessionState.Get("mcode");

            var list = repository.Find(addCri);
            if (list.Count == 0)
                throw new ErsException("10200");
            
            return list[0];

        }
    }
}