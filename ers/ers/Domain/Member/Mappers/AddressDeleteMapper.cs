﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Mappers
{
    public class AddressDeleteMapper
        : IMapper<IAddressDeleteMappable>
    {
        public void Map(IAddressDeleteMappable objMappable)
        {
            //該当会員の住所取得
            var mcode = ErsContext.sessionState.Get("mcode");
            var addressInfo = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(objMappable.id, mcode);
            if (addressInfo == null)
            {
                throw new ErsException("10200");
            }
            objMappable.OverwriteWithParameter(addressInfo.GetPropertiesAsDictionary());
        }
    }
}