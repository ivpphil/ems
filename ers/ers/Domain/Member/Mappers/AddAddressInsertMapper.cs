﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Mappers
{
    public class AddAddressInsertMapper
        : IMapper<IAddAddressInsertMappable>
    {
        public void Map(IAddAddressInsertMappable objMappable)
        {
            this.Initilize(objMappable);
        }

        internal void Initilize(IAddAddressInsertMappable objMappable)
        {
            //該当会員の住所取得
            if (objMappable.id != null)
            {
                var eai = this.GetFindData(objMappable.id.Value);
                objMappable.OverwriteWithParameter(eai.GetPropertiesAsDictionary());
            }
        }

        /// <summary>
        /// 別お届け先IDを元にデータ取得
        /// </summary>
        internal ErsAddressInfo GetFindData(int id)
        {
            ErsAddressInfoRepository repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            ErsAddressInfoCriteria addCri = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

            //検索条件をクライテリアに保存
            addCri.id = id;
            addCri.mcode = ErsContext.sessionState.Get("mcode");
            addCri.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();
            var list = repository.Find(addCri);

            return list[0];
        }
    }
}