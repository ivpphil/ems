﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ers.Domain.Member.Mappers
{
    public class AddressListMapper
        : IMapper<IAddressListMappable>
    {
        public void Map(IAddressListMappable objMappable)
        {
            //アドレスリスト取得
            objMappable.list = this.GetFindData(objMappable);
        }

        /// <summary>
        /// 会員コード元にデータ取得
        /// </summary>
        protected List<Dictionary<string, object>> GetFindData(IAddressListMappable objMappable)
        {
            var mcode = ErsContext.sessionState.Get("mcode");

            var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            var addCri = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

            //検索条件をクライテリアに保存
            addCri.mcode = mcode;

            objMappable.recordCount = repository.GetRecordCount(addCri);

            //検索SQLにLIMIT と OFFSETを加える
            addCri.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);

            objMappable.pager.SetLimitAndOffsetToCriteria(addCri);

            var list = repository.Find(addCri);

            var prefViewService = ErsFactory.ersViewServiceFactory.GetErsViewPrefService();
            var retList = new List<Dictionary<string, object>>();
            foreach (var record in list)
            {
                var dictionary = record.GetPropertiesAsDictionary();
                dictionary["w_add_pref"] = prefViewService.GetStringFromId(record.add_pref);
                retList.Add(dictionary);
            }
            return retList;

        }
    }
}