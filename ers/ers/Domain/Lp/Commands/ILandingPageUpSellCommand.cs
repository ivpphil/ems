﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ers.Domain.Lp.Commands
{
    public interface ILandingPageUpSellCommand
        : ILandingPageCommand
    {
        EnumLpUpsellStgy? upsell_stgy_kbn { get; }

        int? upsell_amount { get; }

        string upsell_scode { get; }

        int? upsell_max_stock { get; }

        int? upsell_max_amount { get; }
    }
}