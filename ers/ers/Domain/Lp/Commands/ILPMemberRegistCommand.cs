﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;

namespace ers.Domain.Lp.Commands
{
    public interface ILPMemberRegistCommand
        : ICommand
    {
        ErsMember member { get; set; }

        bool IsNotLogged { get; }

        bool IsNewMember { set; }

        string mcode { get; }

        ErsOrderIntegrated order { get; }

        Dictionary<string, object> item_code_name { get; }
        Dictionary<string, object> item_code_lname { get; }
        Dictionary<string, object> item_code_email { get; }
        Dictionary<string, object> item_code_email_confirm { get; }
        Dictionary<string, object> item_code_tel { get; }
        Dictionary<string, object> item_code_fax { get; }
        Dictionary<string, object> item_code_zip { get; }
        Dictionary<string, object> item_code_pref { get; }
        Dictionary<string, object> item_code_address { get; }
        Dictionary<string, object> item_code_taddress { get; }
        Dictionary<string, object> item_code_maddress { get; }
        Dictionary<string, object> item_code_birth { get; }
        Dictionary<string, object> item_code_monitor { get; }
        Dictionary<string, object> item_code_sex { get; }

        int? pref { get; }

        string email { get; }

        string email_confirm { get; }

        int? birthday_y { get; }

        int? birthday_m { get; }

        int? birthday_d { get; }

        string passwd { get; }

        string passwd_confirm { get; }

        int? ques { get; }

        EnumPmFlg pm_flg { get; }

        EnumCardSave card_save { get; }

        int? card_id { get; set; }

        int? validity_y { get; }

        int? validity_m { get; }

        EnumPaymentType? pay { get; }

        string lname { get; }

        string fname { get; }

        string lnamek { get; }

        string fnamek { get; }

        string zip { get; }

        string address { get; }

        string taddress { get; }

        string maddress { get; }

        string tel { get; }

        string fax { get; }

        string ans { get; }

        DateTime? birth { get; }

        int?[] del_card_id { get; }

        EnumSex? sex { get; }
    }
}