﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ers.Domain.Lp.Commands
{
    public interface IQuestionnaireDetailCommand
        : ICommand
    {
        Dictionary<string, object> lp_questionnaire { get; set; }

        string item_name { get; }
    }
}