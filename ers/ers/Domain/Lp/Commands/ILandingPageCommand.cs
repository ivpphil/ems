﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Models.lp;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.basket;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.order.strategy;
using ers.Domain.Register.Commands;

namespace ers.Domain.Lp.Commands
{
    public interface ILandingPageCommand
        : IOrderRegistCommand
    {
        bool IsNotLogged { get; }

        bool ValidateMappedOrderData { get; }

        List<Questionnaire_Detail> questionnaireDetailItems { get; }

        int? amount { get; }

        EnumLpBuyLimit? buy_limit_kbn { get; }

        string ccode { get; set; }

        string lp_ccode { get; }

        string sell_scode { get; }

        int? sell_max_stock { get; }

        int? sell_max_amount { get; }

        int? page_id { get; }

        bool IsConfirmPage { get; }

        bool paypalReturn { get; }

        bool IsNonPaymentType { get; }

        bool IsNonPaymentTypeRegular { get; set; }

        bool IsNonPaymentTypeOrdinary { get; set; }

        int? pref { get; set; }

        bool HidePaymentArea { get; }

        int? sell_price { get; set; }
    }
}