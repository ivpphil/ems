﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Models.lp;
using jp.co.ivp.ers.order;

namespace ers.Domain.Lp.Commands
{
    public interface IDQuestionnaireRegistCommand
        : ICommand
    {
        ErsOrderIntegrated order { get; }

        List<Questionnaire_Detail> questionnaireDetailItems { get; }
    }
}