﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Lp.Mappables;
using jp.co.ivp.ers;
using ers.Models.lp;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;

namespace ers.Domain.Lp.Mappers
{
    public class LandingPageMapper
        : IMapper<ILandingPageMappable>
    {
        public void Map(ILandingPageMappable objMappable)
        {
            if (this.GetIsSetDefaultData(objMappable))
            {
                objMappable.page_id = this.GetLpPageId(objMappable);
                this.SetNewIssueRansu(objMappable);
            }

            if (objMappable.page_id != null)
            {
                this.SetLpPageManage(objMappable);
                this.SetHasUpSellRegistered(objMappable);
                //2-3-2-1)
                objMappable.lp_questionnaire_member_List = this.GetLpQuestionnaireList(objMappable, this.GetMemberLpCriteria(objMappable));
                //2-3-3-1)
                objMappable.lp_questionnaire_detail_List = this.GetLpQuestionnaireList(objMappable, this.GetDetailLpCriteria(objMappable));

                this.SetupItemCodeProperties(objMappable);

                if (this.GetIsSetDefaultData(objMappable))
                {
                    objMappable.questionnaireDetailItems = this.GetQuestionnaireDetailDefaultList(objMappable);
                }
                else
                {
                    this.SetQuestionnaireDetailList(objMappable);
                }

                objMappable.IsNonPaymentTypeOrdinary = this.IsNonPaymentTypeOrdinary(objMappable);
                objMappable.IsNonPaymentTypeRegular = this.IsNonPaymentTypeRegular(objMappable);

                bool nonPaymentTypValue = objMappable.IsNonPaymentTypeOrdinary;
                if (objMappable.cart != null && objMappable.cart.regular_basket_in)
                {
                    nonPaymentTypValue = nonPaymentTypValue && objMappable.IsNonPaymentTypeRegular;
                }

                objMappable.IsNonPaymentType = nonPaymentTypValue;

                if (objMappable.IsNonPaymentType)
                {
                    objMappable.pay = null;
                }

                //objMappable._canSelectMailDelv = this.canSelectMailDelv(objMappable);

                if (objMappable.cart != null)
                    this.SetDisplayedDelvMethod(objMappable);

                objMappable.IsOutOfStock = this.GetOutOfStockBool(objMappable, objMappable.sell_scode);
            }
        }

        private bool canSelectMailDelv(ILandingPageMappable objMappable)
        {
            var objMerchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(objMappable.lp_page_manage.sell_scode, null);
            if (objMerchandise != null)
            {
                return (objMerchandise.deliv_method == EnumDelvMethod.Mail);
            }

            return false;
        }

        internal bool GetIsSetDefaultData(ILandingPageMappable objMappable)
        {
            return !(objMappable.IsFromLoginPage || objMappable.IsConfirmPage);
        }

        protected virtual int? GetLpPageId(ILandingPageMappable objMappable)
        {
            var lp_page_manage = ErsFactory.ersLpFactory.GetRelatedRandomErsLpPageManageWithCcode(objMappable.ccode);

            if (lp_page_manage == null)
                throw new HttpException(404, "HTTP/1.1 404 Not Found");

            return lp_page_manage.id;
        }

        protected virtual void SetNewIssueRansu(ILandingPageMappable objMappable)
        {
            var ransuStgy = ErsFactory.ersSessionStateFactory.GetObtainRansuStgy();
            objMappable.ransu = ransuStgy.CreateNewRansu();
        }

        protected void SetHasUpSellRegistered(ILandingPageMappable objMappable)
        {
            if (objMappable.basic_stgy_kbn == EnumLpBasicStgy.UpSell)
            {
                var spec = ErsFactory.ersLpFactory.GetSearchLandingPageMangeHasUpSellSpec();
                var criteria = ErsFactory.ersLpFactory.GetErsLpPageManageCriteria();

                criteria.id = objMappable.page_id;
                criteria.basic_stgy_kbn = (int)objMappable.basic_stgy_kbn;
                criteria.active = (int)EnumActive.Active;

                criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
                objMappable.HasUpSellRegistered = spec.GetCountData(criteria) == 1;
            }


        }

        protected void SetLpPageManage(ILandingPageMappable objMappable)
        {
            objMappable.lp_page_manage = ErsFactory.ersLpFactory.GetErsLpPageManageWithIdAndCcode(objMappable.page_id, objMappable.ccode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objMappable"></param>
        protected List<Dictionary<string, object>> GetLpQuestionnaireList(ILandingPageMappable objMappable, Criteria criteria)
        {
            var spec = ErsFactory.ersLpFactory.GetSearchLpQuestionnaireWithSetupSpec();
            return spec.GetSearchData(criteria);
        }

        protected Criteria GetMemberLpCriteria(ILandingPageMappable objMappable)
        {
            var criteria = ErsFactory.ersLpFactory.GetErsLpQuestionnaireCriteria();
            criteria.lp_page_manage_id = objMappable.page_id;
            criteria.SetActiveLpQuestionnaireAndLpSetup();
            criteria.lp_setup_system_required_not_equal = EnumCmsFieldType.Hide;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        protected Criteria GetDetailLpCriteria(ILandingPageMappable objMappable)
        {
            var criteria = ErsFactory.ersLpFactory.GetErsLpQuestionnaireCriteria();
            criteria.lp_page_manage_id = objMappable.page_id;
            criteria.SetActiveLpQuestionnaireAndLpSetup();
            criteria.lp_setup_system_required = EnumCmsFieldType.Hide;
            criteria.is_used = EnumUse.Use;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        protected void SetupItemCodeProperties(ILandingPageMappable objMappable)
        {
            objMappable.item_code_name = this.GetLpQuestionnaireMemberByItemCode(objMappable, "name");
            objMappable.item_code_lname = this.GetLpQuestionnaireMemberByItemCode(objMappable, "lname");
            objMappable.item_code_email = this.GetLpQuestionnaireMemberByItemCode(objMappable, "email");
            objMappable.item_code_email_confirm = this.GetLpQuestionnaireMemberByItemCode(objMappable, "email_confirm");
            objMappable.item_code_tel = this.GetLpQuestionnaireMemberByItemCode(objMappable, "tel");
            objMappable.item_code_fax = this.GetLpQuestionnaireMemberByItemCode(objMappable, "fax");
            objMappable.item_code_zip = this.GetLpQuestionnaireMemberByItemCode(objMappable, "zip");
            objMappable.item_code_pref = this.GetLpQuestionnaireMemberByItemCode(objMappable, "pref");
            objMappable.item_code_address = this.GetLpQuestionnaireMemberByItemCode(objMappable, "address");
            objMappable.item_code_taddress = this.GetLpQuestionnaireMemberByItemCode(objMappable, "taddress");
            objMappable.item_code_maddress = this.GetLpQuestionnaireMemberByItemCode(objMappable, "maddress");
            objMappable.item_code_birth = this.GetLpQuestionnaireMemberByItemCode(objMappable, "birth");
            objMappable.item_code_monitor = this.GetLpQuestionnaireMemberByItemCode(objMappable, "monitor");
            objMappable.item_code_sex = this.GetLpQuestionnaireMemberByItemCode(objMappable, "sex");

        }

        internal Dictionary<string, object> GetLpQuestionnaireMemberByItemCode(ILandingPageMappable objMappable, string item_code)
        {
            return this.GetLpQuestionnaireByItemCode(objMappable.lp_questionnaire_member_List, item_code);
        }

        internal Dictionary<string, object> GetLpQuestionnaireDetailByItemCode(ILandingPageMappable objMappable, string item_code)
        {
            return this.GetLpQuestionnaireByItemCode(objMappable.lp_questionnaire_detail_List, item_code);
        }

        internal Dictionary<string, object> GetLpQuestionnaireByItemCode(List<Dictionary<string, object>> lp_questionnaireList, string item_code)
        {
            var lp_questionnaire_dic = lp_questionnaireList.Find((lp_q) => Convert.ToString(lp_q["item_code"]) == item_code);

            return lp_questionnaire_dic;
        }

        /// <summary>
        /// 2-3-3)
        /// </summary>
        /// <param name="objMappable"></param>
        protected List<Questionnaire_Detail> GetQuestionnaireDetailDefaultList(ILandingPageMappable objMappable)
        {
            var newDetailList = new List<Questionnaire_Detail>();

            if (objMappable.lp_questionnaire_detail_List != null)
            {
                foreach (var record in objMappable.lp_questionnaire_detail_List)
                {
                    var questionnaireDetai = new Questionnaire_Detail();
                    questionnaireDetai.OverwriteWithParameter(record);
                    questionnaireDetai.lp_questionnaire = record;
                    questionnaireDetai.lineNumber = newDetailList.Count + 1;
                    newDetailList.Add(questionnaireDetai);
                }
            }

            return newDetailList;
        }

        protected void SetQuestionnaireDetailList(ILandingPageMappable objMappable)
        {
            var defaultList = this.GetQuestionnaireDetailDefaultList(objMappable);

            if (defaultList != null)
            {
                foreach (var record in defaultList)
                {
                    if (objMappable.questionnaireDetailItems != null)
                    {
                        var lp_detail_table = objMappable.questionnaireDetailItems.Find((record_line) => record_line.lineNumber == record.lineNumber);

                        if (lp_detail_table != null)
                        {
                            lp_detail_table.OverwriteWithParameter(record.lp_questionnaire);
                            lp_detail_table.lp_questionnaire = record.lp_questionnaire;
                        }
                    }
                }
            }
        }


        protected virtual bool IsNonPaymentTypeOrdinary(ILandingPageMappable objMappable)
        {
            int? ordinary_price = this.GetOrdinaryProductPrice(objMappable);

            ordinary_price = ordinary_price - objMappable.lp_coupon_discount;

            if (objMappable.IsOrdinaryOrder)
            {
                var containerModel = objMappable.containerModel;

                if (objMappable.lp_page_manage.carriage_free_flg == EnumUse.NoUse && ErsFactory.ersBasketFactory.GetCarriageCostTypeFreeSpec().IsSatisfiedBy(objMappable.sell_scode))
                {
                    return true;
                }
                else if (objMappable.basic_stgy_kbn == EnumLpBasicStgy.FreeSample && objMappable.lp_page_manage.carriage_free_flg == EnumUse.Use)
                {
                    return true;
                }
            }
            
            return false;
        }

        protected virtual bool IsNonPaymentTypeRegular(ILandingPageMappable objMappable)
        {
            int? regular_first_price = this.GetRegularFirstProductPrice(objMappable);
            int? regular_price = this.GetRegularProductPrice(objMappable);

            int? total_regular_price = regular_first_price + regular_price;
            total_regular_price = total_regular_price - objMappable.lp_coupon_discount;

            if (objMappable.IsRegularOrder)
            {
                //if (objMappable.cart.basket.carriageFree == EnumCarriageFreeStatus.CARRIAGE_NOT_FREE)
                //{
                //    return false;
                //}
                //return total_regular_price <= 0;

                var containerModel = objMappable.containerModel;
                if (objMappable.lp_page_manage.carriage_free_flg == EnumUse.NoUse && objMappable.basic_stgy_kbn != EnumLpBasicStgy.FreeSample)
                {
                    return false;
                }
                else if (objMappable.basic_stgy_kbn == EnumLpBasicStgy.FreeSample || objMappable.lp_page_manage.carriage_free_flg == EnumUse.Use)
                {
                    return true;
                }
            }

            return false;
        }

        protected virtual int? GetOrdinaryProductPrice(ILandingPageMappable objMappable)
        {
            int? price = null;

            if (objMappable.lp_page_manage == null)
                return price;

            if (objMappable.lp_page_manage.sell_discount_flg == EnumUse.Use && objMappable.amount >= objMappable.lp_page_manage.sell_discount_amount
                && objMappable.lp_page_manage.sell_discount_price < objMappable.lp_page_manage.sell_price)
            {
                price = objMappable.lp_page_manage.sell_discount_price;
            }
            else
            {
                price = objMappable.lp_page_manage.sell_price;
            }

            return price;
        }

        protected virtual int? GetRegularFirstProductPrice(ILandingPageMappable objMappable)
        {
            if (objMappable.lp_page_manage == null)
                return null;

            return objMappable.lp_page_manage.sell_first_regular_price;
        }

        protected virtual int? GetRegularProductPrice(ILandingPageMappable objMappable)
        {
            if (objMappable.lp_page_manage == null)
                return null;

            return objMappable.lp_page_manage.sell_regular_price;
        }

        /// <summary>
        /// Display options Deliv Mehotd by above conditions
        /// </summary>
        /// <param name="objMappable"></param>
        protected virtual void SetDisplayedDelvMethod(ILandingPageMappable objMappable)
        {
            var list = GetLPMerchandise(objMappable);

            if (this.FindItemDelivMethod(list, EnumDelvMethod.Mail))
                objMappable.cart.DisplayedDelvMethod = (short)EnumDelvMethod.Mail;
            else if (this.FindItemDelivMethod(list, EnumDelvMethod.ExpressAndMail))
            {
                objMappable.cart.DisplayedDelvMethod = (short)EnumDelvMethod.Express;

                int? amount = GetAmountDelvMethod(objMappable);

                if (amount == 1 || amount == null || (!objMappable.IsLandingPage && amount == 0))
                    objMappable.cart.DisplayedDelvMethod = (short)EnumDelvMethod.ExpressAndMail;
            }
            else
            {
                objMappable.cart.DisplayedDelvMethod = (short)EnumDelvMethod.Express;
            }
        }

        public int? GetAmountDelvMethod(ILandingPageMappable objMappable)
        {
            if (objMappable.IsLandingPage)
            {
                return objMappable.amount;
            }

            if (objMappable.lp_page_manage.upsell_stgy_kbn == EnumLpUpsellStgy.Combination)
                return 2;

            int amount = objMappable.upsell_amount ?? 0;

            if (objMappable.amount.HasValue)
                amount += objMappable.amount.Value;

            return amount;
        }

        protected virtual IList<ErsMerchandise> GetLPMerchandise(ILandingPageMappable objMappable)
        {
            var list = new List<ErsMerchandise>();

            foreach (var scode in objMappable.LpScodes)
            {
                if (scode.HasValue())
                {
                    var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(scode, null);
                    if (merchandise != null)
                        list.Add(merchandise);
                }
            }

            return list;
        }

        protected virtual bool FindItemDelivMethod(IList<ErsMerchandise> list, EnumDelvMethod deliv_method)
        {
            var result = false;

            if (list.Count > 0)
            {
                result = list.Where((record) => record.deliv_method == deliv_method).Count() > 0;
            }

            return result;
        }

        protected virtual bool GetOutOfStockBool(ILandingPageMappable objMappable, string scode)
        {
            var result_outofstock = false;

            var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(scode, null);

            if (merchandise != null)
            {
                result_outofstock = ErsFactory.ersMerchandiseFactory.GetOutOfStockSpecification().IsSatisfiedBy(merchandise.set_flg, merchandise.scode, merchandise.stock, merchandise.soldout_flg);

                if (result_outofstock)
                    return result_outofstock;
            }

            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.scode = scode;
            criteria.ccode = objMappable.ccode;
            criteria.order_status_not_in = criteria.CancelStatusArray;

            var total_amount_ordered = ErsFactory.ersOrderFactory.GetObtainOrderSumAmount().ObtainOrderRecordSumAmount(criteria);

            if (total_amount_ordered >= objMappable.lp_page_manage.sell_max_stock)
                result_outofstock = true;

            return result_outofstock;
        }
    }
}