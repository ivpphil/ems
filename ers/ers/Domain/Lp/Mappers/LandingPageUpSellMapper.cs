﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Lp.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;
using ers.Models.cart;

namespace ers.Domain.Lp.Mappers
{
    public class LandingPageUpSellMapper
        : LandingPageMapper, IMapper<ILandingPageUpSellMappable>
    {
        public void Map(ILandingPageUpSellMappable objMappable)
        {
            base.Map(objMappable);

            this.SetLandingPageSname(objMappable);
        }

        protected override int? GetLpPageId(ILandingPageMappable objMappable)
        {
            return objMappable.page_id;
        }

        protected void SetLandingPageSname(ILandingPageUpSellMappable objMappable)
        {
            var upsell_sku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(objMappable.upsell_scode);

            if (upsell_sku != null)
                objMappable.upsell_sname = upsell_sku.sname;

            if (objMappable.upsell_stgy_kbn == EnumLpUpsellStgy.Combination)
            {
               var sku= ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(objMappable.sell_scode);

               if (sku != null)
                   objMappable.sname = sku.sname;
            }
        }

        protected override bool IsNonPaymentTypeOrdinary(ILandingPageMappable objMappable)
        {
            if (objMappable.lp_page_manage != null)
                return base.IsNonPaymentTypeOrdinary(objMappable);

            return false;
        }

        protected override bool IsNonPaymentTypeRegular(ILandingPageMappable objMappable)
        {
            if (objMappable.lp_page_manage != null)
                return base.IsNonPaymentTypeRegular(objMappable);

            return false;
        }

        protected override int? GetOrdinaryProductPrice(ILandingPageMappable objMappable)
        {
            int? price = null;

            var IsAddCombinationUpSell = objMappable.lp_page_manage.upsell_stgy_kbn == EnumLpUpsellStgy.Combination;

            var cart = objMappable.cart as LP_Cart;

            var car_upsell_amount = (cart != null) ? cart.upsell_amount : null;

            if (this.IsDiscountedPrice(objMappable.lp_page_manage.upsell_discount_flg, car_upsell_amount, objMappable.lp_page_manage.upsell_discount_amount)
                && objMappable.lp_page_manage.upsell_discount_price < objMappable.lp_page_manage.upsell_price)
            {
                price = objMappable.lp_page_manage.upsell_discount_price;
            }
            else
            {
                price = objMappable.lp_page_manage.upsell_price;
            }

            if (IsAddCombinationUpSell)
            {
                if (this.IsDiscountedPrice(objMappable.lp_page_manage.sell_discount_flg, objMappable.amount, objMappable.lp_page_manage.sell_discount_amount)
                    && objMappable.lp_page_manage.sell_upsell_price < objMappable.lp_page_manage.sell_price)
                {
                    price += objMappable.lp_page_manage.sell_upsell_price;
                }
                else
                {
                    price += objMappable.lp_page_manage.sell_price;
                }
            }

            return price;
        }

        protected override int? GetRegularProductPrice(ILandingPageMappable objMappable)
        {
            int? price = null;

            var IsAddCombinationUpSell = objMappable.lp_page_manage.upsell_stgy_kbn == EnumLpUpsellStgy.Combination;

            price = objMappable.lp_page_manage.upsell_first_regular_price;

            if (IsAddCombinationUpSell)
            {
                price += objMappable.lp_page_manage.sell_first_regular_price;
            }

            return price;
        }

        internal virtual bool IsDiscountedPrice(EnumUse? discount_flg, int? amount, int? sell_discount_amount)
        {
            return (discount_flg == EnumUse.Use && amount >= sell_discount_amount);
        }

        protected override bool GetOutOfStockBool(ILandingPageMappable objMappable, string scode)
        {
            var objUpSell = objMappable as ILandingPageUpSellMappable;

            var result_outofstock = base.GetOutOfStockBool(objMappable, objUpSell.upsell_scode);

            if (objUpSell.upsell_stgy_kbn == EnumLpUpsellStgy.Combination && !result_outofstock)
            {
                result_outofstock = base.GetOutOfStockBool(objMappable, scode);
            }

            return result_outofstock;
        }
    }
}