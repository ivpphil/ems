﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Lp.Mappables;
using jp.co.ivp.ers;

namespace ers.Domain.Lp.Mappers
{
    public class LandingPageTemplatePathMapper
        : IMapper<ILandingPageTemplatePathMappable>
    {
        public void Map(ILandingPageTemplatePathMappable objMappable)
        {
            this.SetLandingPageTemplate(objMappable);
        }

        protected virtual void SetLandingPageTemplate(ILandingPageTemplatePathMappable objMappable)
        {
            var spec = ErsFactory.ersLpFactory.GetSearchOnlyLandingPageWithManageSpec();
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageCriteria();
            criteria.page_type_code = objMappable.lp_page_type_code.ToString();
            criteria.lp_page_manage_id = objMappable.page_id;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            if (spec.GetCountData(criteria) != 1)
                return;

            var objLpPage = ErsFactory.ersLpFactory.GetErsLpPage();
            var list = spec.GetSearchData(criteria);
            objLpPage.OverwriteWithParameter(list.First());

            objMappable.objLpPage = objLpPage;
            objMappable.objLpTemplate = ErsFactory.ersLpFactory.GetErsLpTemplateWithTemplateCode(objLpPage.template_code);
        }
    }
}