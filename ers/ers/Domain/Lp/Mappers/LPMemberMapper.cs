﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Lp.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.related;

namespace ers.Domain.Lp.Mappers
{
    public class LPMemberMapper
        : IMapper<ILPMemberMappable>
    {
        public void Map(ILPMemberMappable objMappable)
        {
            string old_ccode = objMappable.ccode;

            ErsMember member = null;

            if (!objMappable.IsNotLogged && !objMappable.IsConfirmPage)
            {
                member = this.GetMemberDetail();

                if (member != null)
                {
                    objMappable.OverwriteWithParameter(member.GetPropertiesAsDictionary());

                    if (member.birth.HasValue)
                    {
                        objMappable.birthday_y = member.birth.Value.Year;
                        objMappable.birthday_m = member.birth.Value.Month;
                        objMappable.birthday_d = member.birth.Value.Day;

                    }

                    objMappable.email_confirm = member.email;
                }
            }

            this.SetMemberCard(objMappable, member);

            objMappable.ccode = old_ccode;
        }

        internal void SetMemberCard(ILPMemberMappable objMappable, ErsMember member)
        {
            member = (member == null) ? this.GetMemberDetail() : member;

            if (member != null && !objMappable.IsNotLogged)
            {
                objMappable.member = member;

                var paymentCredit = (IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD);
                var listCreditCardInfo = paymentCredit.ObtainMemberCardInfo(member);
                objMappable.ListCreditCardInfo = new List<Dictionary<string, object>>();
                var hasRegularOrderSpec = ErsFactory.ersMemberFactory.GetIsCardHasRegularOrderSpec();
                foreach (var cardInfo in listCreditCardInfo)
                {
                    var dictionary = cardInfo.GetPropertiesAsDictionary();
                    dictionary["hasRegularOrder"] = hasRegularOrderSpec.Has(member.mcode, cardInfo.card_id);
                    objMappable.ListCreditCardInfo.Add(dictionary);
                }

                //Get CardInfo
                if (objMappable.del_card_id != null)
                {
                    var listDelCardInfo = new List<CreditCardInfo>();
                    foreach (var card_id in objMappable.del_card_id)
                    {
                        var CardInfo = paymentCredit.ObtainMemberCardInfo(member, card_id);
                        if (CardInfo != null)
                        {
                            listDelCardInfo.Add(CardInfo);
                        }
                    }
                    objMappable.ListDelCreditCardInfo = listDelCardInfo;
                }
            }
        }

        protected ErsMember GetMemberDetail()
        {
            var mcode = ErsContext.sessionState.Get("mcode");
            return ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);
        }
    }
}