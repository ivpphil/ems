﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Lp.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.Payment;
using ers.Domain.Register.Mappers;

namespace ers.Domain.Lp.Mappers
{
    public class LPOrderMapper
        : OrderMapper, IMapper<ILPOrderMappable>
    {
        public void Map(ILPOrderMappable objMappable)
        {
            this.SetCouponCode(objMappable);
            base.Map(objMappable);
        }

        internal void SetCouponCode(ILPOrderMappable objMappable)
        {
            if (objMappable.coupon_flg)
                objMappable.ent_coupon_code = objMappable.lp_coupon_code;
        }
    }
}