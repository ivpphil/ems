﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Lp.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.Payment;

namespace ers.Domain.Lp.Handlers
{
    public class LPMemberHandler
        : ICommandHandler<ILPMemberRegistCommand>
    {
        public ICommandResult Submit(ILPMemberRegistCommand command)
        {
            if (command.IsNotLogged)
            {
                this.InsertMember(command);
            }
            else
            {
                this.UpdateMember(command);
            }

            this.SaveCard(command, command.member);

            return new CommandResult(true);
        }

        internal void InsertMember(ILPMemberRegistCommand command)
        {
            var order = command.order;

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var member = ErsFactory.ersMemberFactory.GetErsMember();
            member.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            member.dm_flg = EnumDmFlg.Need;
            member.out_bound_flg = EnumOutBoundFlg.Need;
            member.ccode = null;
            member.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();


            var age = ErsFactory.ersMemberFactory.GetGetAgeStgy().GetAge(member.birth);
            member.age_code = ErsFactory.ersMemberFactory.GetGetAgeCodeStgy().GetAgeCode(age);

            repository.Insert(member, true);

            ErsContext.sessionState.Add("mcode", member.mcode);
            order.mcode = command.mcode;
            command.member = member;
            command.IsNewMember = true;
        }

        internal void UpdateMember(ILPMemberRegistCommand command)
        {
            string mcode = command.mcode;
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var old_member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);
            var new_member = ErsFactory.ersMemberFactory.getErsMemberWithParameter(old_member.GetPropertiesAsDictionary());

            this.SetUpdateMemberData(command, new_member);

            repository.Update(old_member, new_member);
            command.member = new_member;
        }

        internal void SetUpdateMemberData(ILPMemberRegistCommand command, ErsMember member)
        {
            member.lname = command.lname;
            member.fname = command.fname;
            member.lnamek = command.lnamek;
            member.fnamek = command.fnamek;
            member.email = command.email;
            member.tel = command.tel;
            member.fax = command.fax;
            member.zip = command.zip;
            member.pref = command.pref;
            member.address = command.address;
            member.taddress = command.taddress;
            member.maddress = command.maddress;
            member.birth = command.birth;
            member.sex = command.sex;
        }

        internal void SaveCard(ILPMemberRegistCommand command, ErsMember member)
        {
            if (command.card_save == EnumCardSave.Save && !command.card_id.HasValue)
            {
                var memberCard = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD)).SaveMemberCardInfo(command.order.card_info, member);
                var oldMemberCard = ErsFactory.ersMemberFactory.GetRetrieveAlreadyStoredMemberCardStgy().Retrieve(memberCard.card_mcode, memberCard.card_sequence);
                if (oldMemberCard == null)
                {
                    var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                    memberCardRepository.Insert(memberCard, true);
                }

                command.card_id = memberCard.id;
            }
        }
    }
}
