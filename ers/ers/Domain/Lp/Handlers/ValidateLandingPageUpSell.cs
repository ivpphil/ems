﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Lp.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using System.ComponentModel.DataAnnotations;

namespace ers.Domain.Lp.Handlers
{
    public class ValidateLandingPageUpSell
        : ValidateLandingPage, IValidationHandler<ILandingPageUpSellCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ILandingPageUpSellCommand command)
        {
            return base.Validate(command);
        }

        public override IEnumerable<ValidationResult> ValidateAmount(ILandingPageCommand command)
        {
            var lp_upsell_command = command as ILandingPageUpSellCommand;

            yield return command.CheckRequired("upsell_amount");

            if (lp_upsell_command.upsell_stgy_kbn == EnumLpUpsellStgy.Combination)
                foreach (var base_result in base.ValidateAmount(command)) yield return base_result;

        }

        protected override void ValidateLandingPageCampaignStatus(ILandingPageCommand command)
        {
            var spec = ErsFactory.ersLpFactory.GetLandingPageStatusCampaignSpec();
            bool result = true;

            result = spec.HasSatisfiedCcodeAndPageId(command.ccode, command.page_id);

            if (result)
                result = this.HasUpSellRegistered(command);

            if (!result)
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
        }

        protected bool HasUpSellRegistered(ILandingPageCommand command)
        {
            var spec = ErsFactory.ersLpFactory.GetSearchLandingPageMangeHasUpSellSpec();
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageManageCriteria();

            criteria.id = command.page_id;
            criteria.basic_stgy_kbn = (int)EnumLpBasicStgy.UpSell;
            criteria.active = (int)EnumActive.Active;

            return spec.GetCountData(criteria) == 1;
        }

        protected override IEnumerable<ValidationResult> ValidateSaleableMaxStock(ILandingPageCommand command)
        {
            var lp_upsell_command = command as ILandingPageUpSellCommand;

            var spec = ErsFactory.ersOrderFactory.GetObtainOrderSumAmount();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.ccode = lp_upsell_command.lp_ccode;
            criteria.order_status_not_in = criteria.CancelStatusArray;
            criteria.scode = lp_upsell_command.upsell_scode;

            int? result = spec.ObtainOrderRecordSumAmount(criteria);

            if (result + lp_upsell_command.upsell_amount > lp_upsell_command.upsell_max_stock)
                yield return new ValidationResult(ErsResources.GetMessage("20205", lp_upsell_command.upsell_scode), new[] { "upsell_max_stock" });

            if (lp_upsell_command.upsell_stgy_kbn == EnumLpUpsellStgy.Combination)
                foreach (var base_result in base.ValidateSaleableMaxStock(command)) yield return base_result;
        }

        protected override IEnumerable<ValidationResult> ValidateSaleableMaxAmount(ILandingPageCommand command)
        {
            var lp_upsell_command = command as ILandingPageUpSellCommand;

            if (lp_upsell_command.upsell_amount > lp_upsell_command.upsell_max_amount)
                yield return new ValidationResult(ErsResources.GetMessage("20201", lp_upsell_command.upsell_scode, lp_upsell_command.upsell_max_amount), new[] { "upsell_max_amount" });

            if (lp_upsell_command.upsell_stgy_kbn == EnumLpUpsellStgy.Combination)
               foreach (var base_result in base.ValidateSaleableMaxAmount(command)) yield return base_result;
        }
    }
}