﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Lp.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;
using ers.Domain.Register.Handlers;
using ers.Domain.Register.Commands;
using jp.co.ivp.ers.Payment;

namespace ers.Domain.Lp.Handlers
{
    public class LPOrderRegistHandler
        : OrderRegistHandler, ICommandHandler<ILandingPageCommand>
    {
        public ICommandResult Submit(ILandingPageCommand command)
        {
            return base.Submit(command);
        }


        internal override void Insert(IOrderRegistCommand command)
        {
            var lp_page_command = command as ILandingPageCommand;

            //this.SetPaymentType(lp_page_command);

            var order = command.order;

            order.ccode = lp_page_command.lp_ccode;
            lp_page_command.ccode = lp_page_command.lp_ccode;

            order.SetMemberAddId(command.member_add_id);

            order.SetMemberCardId(((IPaymentInfoGmoInputContainer)lp_page_command).card_id);

            //Regist regular purchase order.
            if (command.cart.basket.objRegularBasketRecord.Count > 0)
            {
                ErsFactory.ersOrderFactory.GetRegistRegularOrderStgy().Regist(lp_page_command, command.cart.basket, order, lp_page_command.regular_sendtime);
            }

            var isMonitor = ErsFactory.ersOrderFactory.GetIsMonitorSpec().IsSpecified(command.cart.basket, lp_page_command);

            //Prepares bills from order instance
            foreach (var innerOrder in order.GetListOrder())
            {
                innerOrder.OrderHeader.page_id = lp_page_command.page_id;
                innerOrder.OrderHeader.pay = order.pay;
                base.PrepareBills(lp_page_command, innerOrder.OrderHeader, innerOrder.OrderRecords, lp_page_command.lp_ccode);
            }

            //Decrease stock.
            var decreaseStockStgy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetDecreaseStockStgy();

            foreach (var baskRecord in command.orderRecords)
            {
                decreaseStockStgy.Decrease(baskRecord.scode, baskRecord.amount);
            }

            //Delete basket data
            ErsFactory.ersBasketFactory.GetEmptyBasketStrategy().EmptyBasket(ErsContext.sessionState.Get("ransu"));

            //Executes payment with order instance (Credit will be executed at end of all.)
            foreach (var innerOrder in order.GetListOrder())
            {
                this.ExecutePayment(lp_page_command, innerOrder.OrderHeader, isMonitor);
            }
        }

        internal void SetPaymentType(ILandingPageCommand command)
        {
            if (command.order != null)
            {
                if (command.order.subtotal == 0)
                    command.order.pay = EnumPaymentType.NON_NEEDED_PAYMENT;
            }

        }
    }
}