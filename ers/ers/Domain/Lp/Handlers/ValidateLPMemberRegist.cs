﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Lp.Commands;
using jp.co.ivp.ers;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.member;

namespace ers.Domain.Lp.Handlers
{
    public class ValidateLPMemberRegist
        : IValidationHandler<ILPMemberRegistCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ILPMemberRegistCommand command)
        {
            yield return ValidateIsRequired(command, command.item_code_name, "lname", true);
            yield return ValidateIsRequired(command, command.item_code_name, "fname", true);
            yield return ValidateIsRequired(command, command.item_code_lname, "lnamek", true);
            yield return ValidateIsRequired(command, command.item_code_lname, "fnamek", true);
            yield return ValidateIsRequired(command, command.item_code_email, "email");
            yield return ValidateIsRequired(command, command.item_code_email_confirm, "email_confirm");
            yield return ValidateIsRequired(command, command.item_code_tel, "tel");
            yield return ValidateIsRequired(command, command.item_code_fax, "fax");
            yield return ValidateIsRequired(command, command.item_code_zip, "zip");
            yield return ValidateIsRequired(command, command.item_code_pref, "pref");
            foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("pref", command.pref))
            {
                yield return result;
            }
            if (command.IsValidField("zip", "pref"))
            {
                yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("zip", command.zip, "pref", command.pref);
            }

            yield return ValidateIsRequired(command, command.item_code_address, "address");
            yield return ValidateIsRequired(command, command.item_code_taddress, "taddress");
            yield return ValidateIsRequired(command, command.item_code_maddress, "maddress");
            yield return ValidateIsRequired(command, command.item_code_birth, "birth");

            yield return ValidateIsRequired(command, command.item_code_sex, "sex");
            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("sex", EnumCommonNameType.Sex, (int?)command.sex);

            yield return ErsFactory.ersMemberFactory.GetCheckEmailConfirmStgy().CheckEmailConfirm(command.email, command.email_confirm);

            if (command.IsValidField("email"))
            {
                var mcode = command.IsNotLogged ? ErsMember.DEFAUTL_MCODE : ErsContext.sessionState.Get("mcode");
                yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailStgy().CheckDuplicate(mcode, command.email);
            }

            foreach (var result in ErsFactory.ersMemberFactory.GetValidateBirthdayStgy().Validate(command.birthday_y, command.birthday_m, command.birthday_d))
            {
                yield return result;
            }

            if (command.IsNotLogged)
            {
                yield return command.CheckRequired("passwd");
                yield return ErsFactory.ersMemberFactory.GetCheckPasswdConfirmStgy().CheckPasswdConfirm(command.passwd, command.passwd_confirm);

                yield return command.CheckRequired("ques");
                foreach (var result in ErsFactory.ersMemberFactory.GetValidateQuesStgy().Validate(command.ques))
                {
                    yield return result;
                }
                yield return command.CheckRequired("ans");
            }

            if (command.pay == EnumPaymentType.CREDIT_CARD)
            {
                var member = ErsFactory.ersMemberFactory.getErsMemberWithRansu(ErsContext.sessionState);

                if (!command.card_id.HasValue)
                {
                    //クレジットカード入力時のチェック

                    //yield return command.CheckRequired("card_holder_name");
                    yield return command.CheckRequired("card");
                    yield return command.CheckRequired("cardno");
                    yield return command.CheckRequired("validity_y");
                    yield return command.CheckRequired("validity_m");

                    yield return ErsFactory.ersOrderFactory.GetCheckValidityStgy().Check(command.validity_y, command.validity_m);

                    if (command.card_save == EnumCardSave.Save)
                    {
                        if (member != null)
                        {
                            yield return ErsFactory.ersMemberFactory.GetValidateCardSaveCountStgy().Validate(member, command.del_card_id, new[] { "card_save" });
                        }
                    }
                }

                //使用クレジットカード選択時の入力チェック
                yield return ErsFactory.ersMemberFactory.GetValidateDeleteCardStgy().Validate(member, command.card_id, command.card_save, command.del_card_id, new[] { "del_card_id" });
            }

        }


        protected ValidationResult ValidateIsRequired(ILPMemberRegistCommand command, Dictionary<string, object> item_code, string propertyName, bool IsDefaultMessage = false)
        {
            if (!command.IsValidField(propertyName))
                return null;

            if ((EnumRequired)item_code["is_required"] == EnumRequired.Required)
            {
                if (IsDefaultMessage)
                {
                    return command.CheckRequired(propertyName);
                }

                if (command.CheckRequired(propertyName) != null)
                    return new ValidationResult(ErsResources.GetMessage("10000", Convert.ToString(item_code["item_name"])), new[] { propertyName });
            }

            return null;
        }
    }
}