﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Lp.Commands;
using jp.co.ivp.ers;
using System.ComponentModel.DataAnnotations;

namespace ers.Domain.Lp.Handlers
{
    public class ValidateQuestionnaireDetail
        : IValidationHandler<IQuestionnaireDetailCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IQuestionnaireDetailCommand command)
        {
            if (!command.IsValidField("value"))
                yield break;

            if ((EnumRequired)command.lp_questionnaire["is_required"] == EnumRequired.Required)
            {
                if (command.CheckRequired("value") != null)
                    yield return new ValidationResult(ErsResources.GetMessage("10000", command.item_name), new[] { "value" });
            }
        }
    }
}