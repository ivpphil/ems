﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Lp.Commands;
using jp.co.ivp.ers;

namespace ers.Domain.Lp.Handlers
{
    public class DQuestionnaireRegistHandler
        : ICommandHandler<IDQuestionnaireRegistCommand>
    {
        public ICommandResult Submit(IDQuestionnaireRegistCommand command)
        {
            this.Insert(command);
            return new CommandResult(true);
        }

        internal void Insert(IDQuestionnaireRegistCommand command)
        {
            if (command.questionnaireDetailItems != null)
            {
                foreach (var record in command.questionnaireDetailItems)
                {
                    var repository = ErsFactory.ersLpFactory.GetErsDQuestionnaireRepository();
                    var d_questionnaire = ErsFactory.ersLpFactory.GetErsDQuestionnaireWithParameter(command.GetPropertiesAsDictionary());

                    d_questionnaire.d_no = command.order.d_no;
                    d_questionnaire.item_code = record.item_code;
                    d_questionnaire.value = record.value;

                    repository.Insert(d_questionnaire);
                }
            }
        }
    }
}