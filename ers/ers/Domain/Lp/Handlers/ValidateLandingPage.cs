﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Lp.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.mvc;

namespace ers.Domain.Lp.Handlers
{
    public class ValidateLandingPage
        : IValidationHandler<ILandingPageCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ILandingPageCommand command)
        {
            if (!command.sell_price.HasValue)
            {
                command.sell_price = 0;
            }

            var controller = command.controller;

            this.ValidateLandingPageCampaignStatus(command);

            if (!command.IsConfirmPage || command.paypalReturn)
                yield break;

            if (command.questionnaireDetailItems != null && command.questionnaireDetailItems.Count != 0)
            {
                foreach (var model in command.questionnaireDetailItems)
                {
                    model.AddInvalidField(controller.commandBus.Validate<IQuestionnaireDetailCommand>(model));

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            string newErrorMessage = String.Format(errorMessage, model.item_name);
                            yield return new ValidationResult(newErrorMessage, new[] { "questionnaireDetailItems" });
                            model.error_msg = newErrorMessage;
                        }

                        model.RemoveInvalidField("value");
                    }
                }
            }

            foreach (var result in this.ValidateAmount(command))
            {
                yield return result;
            }

            yield return ValidateMemberOneTimePastOrder(command);

            foreach (var result in this.ValidateSaleableMaxStock(command))
                yield return result;

            foreach (var result in this.ValidateSaleableMaxAmount(command))
                yield return result;

            foreach (var result in ValidateInputValues(command))
            {
                yield return result;

            }

            yield return command.CheckRequired("pri_chk");

            yield return command.CheckRequired("deliv_method");

            if (!command.HidePaymentArea && !command.IsNonPaymentType)
            {
                yield return command.CheckRequired("pay");
            }
           
            if (command.ValidateMappedOrderData)
            {
                if (command.cart == null || (!command.cart.regular_basket_in && !command.IsNonPaymentTypeOrdinary) || (command.cart.regular_basket_in && !command.IsNonPaymentTypeRegular))
                {                    
                    //For All item has total of 0 and bask_t.carriageFree == EnumCarriageFreeStatus.CARRIAGE_FREE

                    if (command.IsValidField("pay"))
                    {
                        yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().Validate("pay", command.pay, command.cart != null && command.cart.regularBasketItems != null && command.cart.regularBasketItems.Count() > 0);
                        yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().ValidateOverseas("pay", command.pay, command.pref);
                        if (command.pay == EnumPaymentType.NON_NEEDED_PAYMENT)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName("pay")), new[] { "pay" });
                        }
                    }
                }

                if (command.pay != null)
                {
                    yield return ErsFactory.ersOrderFactory.GetCheckMailDeliveryStgy().Validate(command.deliv_method, command.pay);
                }

                foreach (var result in ValidateGeneratedOrder(command))
                {
                    yield return result;
                }
            }
        }

        public virtual IEnumerable<ValidationResult> ValidateAmount(ILandingPageCommand command)
        {
            yield return command.CheckRequired("amount");
        }

        public virtual IEnumerable<ValidationResult> ValidateGeneratedOrder(ILandingPageCommand command)
        {
            //フロアリミット金額チェック
            if (command.pay != null)
            {
                yield return ErsFactory.ersOrderFactory.GetCheckFloorLimitStgy().Validate(command.pay, command.order_total.Value, command.pm_flg);
            }
        }

        public virtual IEnumerable<ValidationResult> ValidateInputValues(ILandingPageCommand command)
        {

            if (!command.cart.recompute)
            {
                yield return command.CheckRequired("send");
                yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("send", EnumCommonNameType.SendTo, (int?)command.send);

                //コンビニ用Validation
                if (command.pay == EnumPaymentType.CONVENIENCE_STORE)
                {
                    yield return command.CheckRequired("conv_code");
                    if (command.IsValidField("conv_code"))
                    {
                        yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("conv_code", EnumCommonNameType.ConvCode, (int?)command.conv_code);
                    }
                }


                if (command.cart.basketItemCount != 0)
                {
                    yield return command.CheckRequired("firstTimeOrdinary");
                }

                //check Senddate
                if (command.firstTimeOrdinary == EnumFirstTimeOrdinary.Specify)
                {
                    yield return command.CheckRequired("senddate");

                    foreach (var result in ErsFactory.ersOrderFactory.GetValidateSenddateStgy().Validate(DateTime.Now, command.senddate))
                    {
                        yield return result;
                    }
                }

                foreach (var result in ErsFactory.ersOrderFactory.GetValidateSendtimeStgy().Validate(command.sendtime))
                {
                    yield return result;
                }


                if (((IPaymentInfoGmoInputContainer)command).card_id.HasValue)
                {
                    //modelに預け情報をセット
                    var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(ErsContext.sessionState.Get("mcode"));
                    var cardInfo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD)).ObtainMemberCardInfo(member, ((IPaymentInfoGmoInputContainer)command).card_id);
                    if (cardInfo == null)
                        yield return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName("card_id")), new[] { "card_id" });

                }
            }

            //カートが全てメール便の場合は配送日・配送時刻指定不可
            if (command.firstTimeOrdinary == EnumFirstTimeOrdinary.Specify || (command.sendtime != null && command.sendtime != 0))
            {
                yield return ErsFactory.ersOrderFactory.GetCheckMailDeliveryStgy().CkSendDateTimeBasketMailDelivery(command.deliv_method, command.senddate, command.sendtime);
            }
        }

        protected virtual void ValidateLandingPageCampaignStatus(ILandingPageCommand command)
        {
            var spec = ErsFactory.ersLpFactory.GetLandingPageStatusCampaignSpec();
            bool result = true;

            if (!command.IsConfirmPage)
            {
                result = spec.HasSatisfiedByCcode(command.ccode);
            }
            else
            {
                result = spec.HasSatisfiedCcodeAndPageId(command.ccode, command.page_id);
            }

            if (!result)
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
        }

        /// <summary>
        /// 2-7-2-2)
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        protected virtual ValidationResult ValidateMemberOneTimePastOrder(ILandingPageCommand command)
        {
            if (!command.IsNotLogged && command.buy_limit_kbn == EnumLpBuyLimit.OneTime)
            {

                var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

                criteria.mcode = command.mcode;
                criteria.ccode = command.lp_ccode;

                EnumOrderStatusType[] list;
                list = new EnumOrderStatusType[2] { EnumOrderStatusType.CANCELED, EnumOrderStatusType.CANCELED_AFTER_DELIVER };
                criteria.order_status_not_in = list;

                if (repository.GetRecordCount(criteria) > 0)
                {
                    return new ValidationResult(ErsResources.GetMessage("LP0002722"), new[] { "ccode" });
                }
            }

            return null;

        }

        /// <summary>
        /// 2-7-2-3)
        ///     2-7-2-3-1)
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        protected virtual IEnumerable<ValidationResult> ValidateSaleableMaxStock(ILandingPageCommand command)
        {
            var spec = ErsFactory.ersOrderFactory.GetObtainOrderSumAmount();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.ccode = command.lp_ccode;
            criteria.order_status_not_in = criteria.CancelStatusArray;
            criteria.scode = command.sell_scode;

            int? result = spec.ObtainOrderRecordSumAmount(criteria);

            if (result + command.amount > command.sell_max_stock)
                yield return new ValidationResult(ErsResources.GetMessage("20205", command.sell_scode), new[] { "sell_max_stock" });
        }

        /// <summary>
        /// 2-7-2-4)
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        protected virtual IEnumerable<ValidationResult> ValidateSaleableMaxAmount(ILandingPageCommand command)
        {
            if (command.amount > command.sell_max_amount)
                yield return new ValidationResult(ErsResources.GetMessage("20201", command.sell_scode, command.sell_max_amount), new[] { "sell_max_amount" });
        }
    }
}