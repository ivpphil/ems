﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.lp;
using ers.Models.lp;
using jp.co.ivp.ers;
using ers.Domain.Cart.Commands;

namespace ers.Domain.Lp.Mappables
{
    public interface ILandingPageMappable
        : IMappable
    {
        bool IsLandingPage { get; }

        ICartCommand cart { get; }

        ErsLpPageManage lp_page_manage { get; set; }

        List<Dictionary<string, object>> lp_questionnaire_member_List { get; set; }

        List<Dictionary<string, object>> lp_questionnaire_detail_List { get; set; }

        List<Questionnaire_Detail> questionnaireDetailItems { get; set; }

        string ccode { get; }

        int? page_id { get; set; }

        string ransu { get; set; }

        Dictionary<string, object> item_code_name { get; set; }
        Dictionary<string, object> item_code_lname { get; set; }
        Dictionary<string, object> item_code_email { get; set; }
        Dictionary<string, object> item_code_email_confirm { get; set; }
        Dictionary<string, object> item_code_tel { get; set; }
        Dictionary<string, object> item_code_fax { get; set; }
        Dictionary<string, object> item_code_zip { get; set; }
        Dictionary<string, object> item_code_pref { get; set; }
        Dictionary<string, object> item_code_address { get; set; }
        Dictionary<string, object> item_code_taddress { get; set; }
        Dictionary<string, object> item_code_maddress { get; set; }
        Dictionary<string, object> item_code_birth { get; set; }
        Dictionary<string, object> item_code_monitor { get; set; }
        Dictionary<string, object> item_code_sex { get; set; }

        EnumLpBasicStgy? basic_stgy_kbn { get; }

        bool IsFromLoginPage { get; }

        bool IsConfirmPage { get; }

        bool HasUpSellRegistered { set; }

        bool IsNotConfirmationTransaction { get; }

        bool IsOrdinaryOrder { get; }

        bool IsRegularOrder { get; }

        bool IsNonPaymentType { get; set; }

        string sell_scode { get; }

        int? upsell_amount { get; }

        int? amount { get; }

        bool IsNonPaymentTypeOrdinary { get; set; }

        bool IsNonPaymentTypeRegular { get; set; }

        bool _canSelectMailDelv { set; }

        EnumPaymentType? pay { get; set; }

        int lp_coupon_discount { get; }

        IEnumerable<string> LpScodes { get; }

        bool IsOutOfStock { get; set; } 
    }
}