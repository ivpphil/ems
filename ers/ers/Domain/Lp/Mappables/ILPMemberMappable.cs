﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.related;

namespace ers.Domain.Lp.Mappables
{
    public interface ILPMemberMappable
        : IMappable
    {
        string ccode { get; set; }

        bool IsNotLogged { get; }

        bool IsConfirmPage { get; }

        int? birthday_y { set; }

        int? birthday_m { set; }

        int? birthday_d { set; }

        string email_confirm { set; }

        ErsMember member { set; }

        IList<Dictionary<string, object>> ListCreditCardInfo { get; set; }

        IList<CreditCardInfo> ListDelCreditCardInfo { set; }

        int?[] del_card_id { get; }
    }
}