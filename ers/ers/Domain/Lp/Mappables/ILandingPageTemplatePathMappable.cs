﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers;

namespace ers.Domain.Lp.Mappables
{
    public interface ILandingPageTemplatePathMappable
        : IMappable
    {
        int? page_id { get; }

        ErsLpPage objLpPage { set; }

        ErsLpTemplate objLpTemplate { set; }

        EnumLpPageTypeCode lp_page_type_code { get; }
    }
}