﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using jp.co.ivp.ers.Payment;
using ers.Domain.Register.Mappables;

namespace ers.Domain.Lp.Mappables
{
    public interface ILPOrderMappable
        : IMappable, IOrderMappable
    {
        bool coupon_flg { get; }

        string lp_coupon_code { get; }
    }
}