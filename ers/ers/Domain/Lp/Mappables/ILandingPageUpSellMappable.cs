﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ers.Domain.Lp.Mappables
{
    public interface ILandingPageUpSellMappable
        : ILandingPageMappable
    {
        EnumLpUpsellStgy? upsell_stgy_kbn { get; }

        string upsell_sname { set; }

        string sname { set; }

        string upsell_scode { get; }
    }
}