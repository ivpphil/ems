﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Home.Mappables;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ers.Domain.Home.Mappers
{
    public class NewsListMapper
        : IMapper<INewsMappable>
    {
        public void Map(INewsMappable objMappable)
        {
            var repository = ErsFactory.ersContentsFactory.GetErsNewsArticleRepository();
            var criteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
            criteria.contents_code = objMappable.contents_code;
            criteria.period_from_less = DateTime.Now;
            criteria.period_to_than = DateTime.Now;
            criteria.SetOrderByPosted_date(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.active = EnumActive.Active;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            criteria.LIMIT = 5;
            var list = repository.Find(criteria);
            List<Dictionary<string, object>> ersList = new List<Dictionary<string, object>>();
            foreach (var item in list)
            {
                var dictionary = item.GetPropertiesAsDictionary();
                ersList.Add(dictionary);
            }

            objMappable.news_list = ersList;
        }

    }
}