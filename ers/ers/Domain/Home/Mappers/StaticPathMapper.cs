﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using ers.Domain.Home.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;

namespace ers.Domain.Home.Mappers
{
    public class StaticPathMapper
        : IMapper<IStaticPathMappable>
    {
        private static string[] staticTargetExtension = new[] { ".htm", ".html" };

        private static string defaultDocument = "index.html";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objMappable"></param>
        public void Map(IStaticPathMappable objMappable)
        {
            this.ValidateExtension(objMappable);

            //パスを取得
            var actualPath = ErsCommonContext.MapPath("~/Views/static/" + objMappable.staticPath);

            if (!File.Exists(actualPath))
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }

            objMappable.partialPath = "../static/" + objMappable.staticPath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objMappable"></param>
        private void ValidateExtension(IStaticPathMappable objMappable)
        {
            if (!objMappable.IsValidField("staticPath"))
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }

            var extension = string.Empty;

            try
            {
                extension = Path.GetExtension(objMappable.staticPath);
            }
            catch (ArgumentException)
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }

            if (!extension.HasValue())
            {
                objMappable.staticPath = Path.Combine(objMappable.staticPath, defaultDocument);
                extension = Path.GetExtension(objMappable.staticPath);
            }

            if (!staticTargetExtension.Contains(extension))
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }
        }
    }
}