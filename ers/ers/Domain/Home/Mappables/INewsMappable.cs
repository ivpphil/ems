﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ers.Domain.Home.Mappables
{
    public interface INewsMappable
        : IMappable
    {
        List<Dictionary<string, object>> news_list { get; set; }

        string contents_code { get; }
    }
}