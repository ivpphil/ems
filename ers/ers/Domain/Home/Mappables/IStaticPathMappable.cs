﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ers.Domain.Home.Mappables
{
    public interface IStaticPathMappable
        : IMappable
    {
        string staticPath { get; set; }

        string partialPath { get; set; }
    }
}
