﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ers.Domain.Login.Mappables
{
    public interface ILPLoginMappable
        : IMappable
    {
        string email { set; }

        string passwd { set; }

        string login_email { get; }

        string login_passwd { get; }
    }
}