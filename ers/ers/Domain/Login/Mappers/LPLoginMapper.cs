﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ers.Domain.Login.Mappables;

namespace ers.Domain.Login.Mappers
{
    public class LPLoginMapper
        : IMapper<ILPLoginMappable>
    {
        public void Map(ILPLoginMappable objMappable)
        {
            objMappable.email = objMappable.login_email;
            objMappable.passwd = objMappable.login_passwd;
        }
    }
}