﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Login.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;

namespace ers.Domain.Login.Handlers
{
    public class ValidateLogin
           : IValidationHandler<ILoginCommand>
    {
        public IEnumerable<ValidationResult> Validate(ILoginCommand command)
        {
            yield return command.CheckRequired("email");
            yield return command.CheckRequired("passwd");

            if (command.IsValidField("email", "passwd"))
            {
                var mcode = ((ISession)ErsContext.sessionState).GetUserCode(command.email, command.passwd);

                if (ErsFactory.ersMemberFactory.GetMemberAccountStatus(command.email) == EnumAccountStatus.Locked)
                {
                    command.account_status = EnumAccountStatus.Locked;
                    yield return new ValidationResult(ErsResources.GetMessage("memberlocked"));
                }
                else
                {
                    if (mcode == ErsMember.DEFAUTL_MCODE)
                    {
                        ErsFactory.ersMemberFactory.GetUpdateMemberLockTryCountStgy().Validate(command.email);
                        yield return new ValidationResult(ErsResources.GetMessage("10204"), new[] { "email", "passwd" });
                    }
                    else
                    {
                        ErsFactory.ersMemberFactory.GetUpdateMemberLockTryCountStgy().ResetMemberLoginTryCount(command.email);
                    }
                }
            }
            else
            {
                ErsFactory.ersMemberFactory.GetUpdateMemberLockTryCountStgy().Validate(command.email);
            }
        }

    
    }
}