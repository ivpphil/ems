﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Login.Commands;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using System.Text;
using jp.co.ivp.ers;

namespace ers.Domain.Login.Handlers
{
    public class LoginHandler
         : ICommandHandler<ILoginCommand>
    {
        public ICommandResult Submit(ILoginCommand command)
        {
            this.LoginAction(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// ログイン処理を実行する
        /// </summary>
        internal void LoginAction(ILoginCommand command)
        {
            var mcode = ((ISession)ErsContext.sessionState).GetUserCode(command.email, command.passwd);

            //前回ログインのセッション情報は一旦クリア
            ErsContext.sessionState.ClearLoginData();
            var objSession = ((ISession)ErsContext.sessionState);
            objSession.getNewSSLransu(mcode);

            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);

            var nickName = member.lname + member.fname;

            ErsContext.sessionState.Add(ErsSessionState.nickNameKey, HttpUtility.UrlEncode(nickName, HttpContext.Current.Response.ContentEncoding));

            //emailをクッキー書き込み
            var objc = new OtherCookie();
            if (command.email_ck)
            {
                objc.SetCookieEmail("login_email", command.email, ErsFactory.ersUtilityFactory.getSetup().cookieTimer);
            }
            else
            {
                objc.DeleteCookie("login_email");
            }
        }
    }
}