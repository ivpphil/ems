﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Login.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ers.Domain.Login.Handlers
{
    public class ValidateMemberRankPricingCommand : IValidationHandler<IMemberRankPricingCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IMemberRankPricingCommand command)
        {
            
            var mcode = ((ISession)ErsContext.sessionState).GetUserCode(command.email, command.passwd);

            var memberRank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().Retrieve(mcode);

            var basket = ErsFactory.ersBasketFactory.GetErsBasket();
            basket.PrepareSession();
            basket.Init(mcode, ErsContext.sessionState.Get("ransu"));

            // compare cart item price to what the price should be based on member account.
            // promt user to recalculate when price is incorrect.
            if (!ErsFactory.ersBasketFactory.GetValidateBasketRecordPriceStgy().ValidatePrice(memberRank, basket) || 
                !ErsFactory.ersBasketFactory.GetValidateRegularBasketRecordPriceStgy().ValidatePrice(memberRank, basket))
            {
                throw new ErsException("102300");
            }
                                    

            yield break;
        }       
    }
}