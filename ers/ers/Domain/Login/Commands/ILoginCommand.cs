﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ers.Domain.Login.Commands
{
    public interface ILoginCommand
        : ICommand
    {
        string email { get; }

        string passwd { get; }

        bool email_ck { get; }

        EnumAccountStatus account_status { get; set; }
    }
}