﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ers.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ers.Domain.Quest.Commands;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.state;

namespace ers.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [RedirectSite]

    [ErsSideMenu]
    [ErsLanguageMenu]
    public class questController
        : ErsControllerSecure
    {

        /// <summary>
        /// 入力画面
        /// </summary>
        /// <param name="quest"></param>
        /// <returns></returns>
        [ErsFrontProcessCompletionAttribute("quest_quest", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult quest1(Quest quest, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IQuestCommand>(quest), quest);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(quest);
            }

            return View("quest1", quest);
        }

        /// <summary>
        /// 確認画面
        /// </summary>
        /// <param name="quest"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsFrontProcessCompletionAttribute("quest_quest", mode = EnumHandlingMode.CHECK)]
        public virtual ActionResult quest2(Quest quest)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IQuestCommand>(quest), quest);
            if (!ModelState.IsValid)
            {
                return this.quest1(quest, EnumEck.Error);
            }

            quest.SetOutputHidden(true);

            return View("quest2", quest);
        }

        /// <summary>
        /// 完了画面
        /// </summary>
        /// <param name="quest"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsFrontProcessCompletionAttribute("quest_quest", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult quest3(Quest quest)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IQuestCommand>(quest), quest);
            if (!ModelState.IsValid)
            {
                return this.quest1(quest, EnumEck.Error);
            }

            // メール送信オブジェクト取得
            var sendmailQuest = ErsFactory.ersMailFactory.getErsSendMailQuest();
            sendmailQuest.Send(quest, quest.email);

            //Send Acceptance Mail
            var sendmailQuestAccepted = ErsFactory.ersMailFactory.GetErsSendMailQuestAccept();
            sendmailQuestAccepted.Send(quest, quest.email);

            return View("quest3", quest);
        }
    }
}
