﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using ers.Models;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.mvc.pager;
using ers.Domain.Search.Mappables;
using jp.co.ivp.ers;
using ers.Domain.Search.Commands;
using jp.co.ivp.ers.state;

namespace ers.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsLanguageMenu]
    [ErsSideMenu]
    public class SearchController
        : ErsControllerSecure

    {
        //
        // GET:

        public virtual ActionResult search(List list)
        {
            //初期値設定
            if (!list.search_item)
            {
                list.s_outstock = true;
            }

            //セッション情報を確認
            base.validateSession();

            return View("search", list);
        }

        /// <summary>
        /// LIST
        /// </summary>
        /// <param name="S_PageCnt">ページ数</param>
        /// <returns></returns>
        public virtual ActionResult list(List list)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IMerchandiseListCommand>(list), list);
            if (!ModelState.IsValid)
            {
                return search(list);
            }

            //初期値設定
            if (!list.search_item)
            {
                list.s_outstock = true;
            }

            //セッション情報を確認
            base.validateSession();

            //Pager設定
            list.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", list.pageCnt, list.maxItemCount);

            this.mapperBus.Map<IMerchandiseListMappable>(list);
            
            list.pager.LoadPageList(list.recordCount);

            //FormタグにHiddenを自動入力
            list.SetOutputHidden(true);
            list.SetOutputHidden("on_list", false);

            return View("list", list);
        }

    }
}
