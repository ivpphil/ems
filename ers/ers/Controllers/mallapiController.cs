﻿using System.Web.Mvc;
using ers.Domain.MallApi.Commands;
using ers.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ers.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    [ErsLanguageMenu]
    public class mallapiController
        : ErsControllerSecure
    {
        #region Override or New implementations
        /// <summary>
        /// エラーテンプレート名取得 [Get the name of template file for error page]
        /// </summary>
        /// <returns></returns>
        protected override string GetErrorTemplateName()
        {
            if (HttpContext.Request.IsSecureConnection)
            {
                return "error_mallapi_ssl";
            }
            else
            {
                return "error_mallapi";
            }
        }
        #endregion

        /// <summary>
        /// update_products
        /// 商品更新 [Update products]
        /// </summary>
        /// <param name="model">商品更新モデル [Model for update products]</param>
        /// <returns></returns>
        [NoNeedSession]
        public virtual ActionResult update_products(update_products model)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IUpdateProductsCommand>(model), model);

            if (model.IsValidField("authentication"))
            {
                this.commandBus.Submit<IUpdateProductsCommand>(model, EnumCommandTransaction.WithoutBeginTransaction);
            }

            return View("update_products", model);
        }
    }
}
