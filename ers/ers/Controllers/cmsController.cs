﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using ers.Models;
using jp.co.ivp.ers;
using ers.Domain.Cms.Mappables;
using ers.Domain.Cms.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Login.Commands;
using jp.co.ivp.ers.state;
using System.Web.Routing;
//using jp.co.ivp.ers.movie_article;
//using jp.co.ivp.ers.file_article;
//using jp.co.ivp.ers.util;
//using jp.co.ivp.ers.state;

namespace ers.Controllers
{
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    [ErsLanguageMenu]
    public class cmsController
        : ErsControllerSecure

    {
        [ErsRequireHttps(false)]
        public ActionResult news_list(news_list article)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }
            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", article.pageCnt, article.maxItemCount);

            article.pager = pager;
            this.mapperBus.Map<INewsListMappable>(article);

            if (article.req_login_flg == EnumRequired.Required && 
                !((ErsSessionState)ErsContext.sessionState).IsLoginButNotSecure())
            {
                //ログイン画面へリダイレクト
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                var news_login = new Dictionary<string, object>();
                news_login["contents_code"] = article.contents_code;

                return RedirectToAction(setup.sec_url, "login", "cms", new RouteValueDictionary(news_login));
            }

            pager.LoadPageList(article.recordCount);

            article.SetOutputHidden(true);
            return View("news_list", article);
        }

        [ErsRequireHttps(false)]
        public ActionResult news_detail(news_detail article)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<INewsDetailCommand>(article), article);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<INewsDetailMappable>(article);

            if (article.req_login_flg == EnumRequired.Required &&
                !((ErsSessionState)ErsContext.sessionState).IsLoginButNotSecure())
            {
                //ログイン画面へリダイレクト
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                var news_login = new Dictionary<string, object>();
                news_login["article_code"] = article.article_code;

                return RedirectToAction(setup.sec_url, "login", "cms", new RouteValueDictionary(news_login));
            }

            return View("news_detail", article);
        }

        [ErsRequireHttps(false)]
        public virtual ActionResult news_detail_preview(news_detail article)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<INewsDetailPreviewCommand>(article), article);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<INewsDetailPreviewMappable>(article);

            return View("news_detail", article);
        }

        [HttpGet]
        [ErsRequireHttps]
        public virtual ActionResult login(news_login article, EnumEck? eck = null)
        {
            if (!this.ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    ClearModelState(article);
                }
            }

            article.SetOutputHidden(true);

            return View("login", article);
        }

        [HttpPost]
        [ErsRequireHttps]
        public virtual ActionResult login(Login login, news_login article)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILoginCommand>(login), login);
            if (!ModelState.IsValid)
            {
                return this.login(article, EnumEck.Error);
            }

            this.commandBus.Submit<ILoginCommand>(login, EnumCommandTransaction.BeginTransaction);
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (!string.IsNullOrEmpty(article.article_code))
            {
                var detail = new Dictionary<string, object>();
                detail["article_code"] = article.article_code;

                return RedirectToAction(setup.nor_url, "news_detail", "cms", new RouteValueDictionary(detail));
            }

            var list = new Dictionary<string, object>();
            list["contents_code"] = article.contents_code;

            return RedirectToAction(setup.nor_url, "news_list", "cms", new RouteValueDictionary(list));
        }
    }
}