﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using ers.Models;
using jp.co.ivp.ers.db;
using ers.Domain.Api.Commands;
using ers.Domain.Api.Mappables;
using ers.Models.api;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ers.Controllers
{
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class apiController
        : ErsControllerSecure
    {

        /// <summary>
        /// 郵便番号検索１
        /// 非同期検索処理
        /// </summary>
        /// <returns></returns>
        [ErsRequireHttps(false)]
        [NoNeedSession]
        public virtual ActionResult get_zip(getZip get_zip)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IGetZipCommand>(get_zip), get_zip);

            if (this.ModelState.IsValid)
            {
                //Validatin結果はMapの中で判定する。
                this.mapperBus.Map<IGetZipMappable>(get_zip);
            }

            return View("get_zip", get_zip);
        }

        /// <summary>
        /// JavaScriptへ渡す定数を定義
        /// </summary>
        /// <param name="get_zip"></param>
        /// <returns></returns>
        [ErsRequireHttps(false)]
        [NoNeedSession]
        public virtual ActionResult get_const()
        {
            return View("get_const");
        }

        /// <summary>
        /// キーワードサジェスト
        /// </summary>
        /// <param name="keywords_suggest"></param>
        /// <returns></returns>
        [ErsRequireHttps(false)]
        public virtual ActionResult get_keywords_suggest(KeywordsSuggest keywords_suggest)
        {
            this.mapperBus.Map<IKeywordsSuggestMappable>(keywords_suggest);
            return View("get_keywords_suggest", keywords_suggest);
        }

        /// <summary>
        /// クッキーセッション接頭辞取得 [Get cookie session prefix]
        /// </summary>
        /// <returns>Json</returns>
        [ErsRequireHttps(false)]
        [NoNeedSession]
        public virtual ActionResult get_cookie_session_prefix()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            return Json(new { prefix = setup.cookieSessionPrefix }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// GMO決済結果取得
        /// </summary>
        /// <returns></returns>
        [NoNeedSession]
        [ErsRequireHttps(false)]
        public virtual ActionResult gmo_result_receive(GmoResultReceive gmoResultReceive)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IGmoResultReceiveCommand>(gmoResultReceive), gmoResultReceive);

            if (!this.ModelState.IsValid)
            {
                throw new Exception(string.Join(Environment.NewLine, gmoResultReceive.GetAllErrorMessageList()));
            }

            this.commandBus.Submit<IGmoResultReceiveCommand>(gmoResultReceive, EnumCommandTransaction.BeginTransaction);

            return Content("0");
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.RouteData.Values["action"].ToString() != "gmo_result_receive")
            {
                base.OnException(filterContext);
                return;
            }


            //GMO決済結果取得
            var exception = filterContext.Exception;

            var exceptionMessage = this.GetExceptionMessage(exception);

            //logging
            ErsCommon.loggingException(exceptionMessage);

            filterContext.Result = Content("1");
            filterContext.ExceptionHandled = true;
        }
    }
}
