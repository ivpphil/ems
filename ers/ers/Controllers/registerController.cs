﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using ers.Models;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mvc.CommandProcessor.Dispatcher;
using jp.co.ivp.ers.mvc.MapperProcessor.Dispatcher;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Register.Commands;
using jp.co.ivp.ers;
using ers.Domain.Cart.Mappables;
using ers.Domain.Cart.Commands;
using ers.Domain.Register.Mappables;
using jp.co.ivp.ers.Payment;
using ers.Domain.Login.Commands;

namespace ers.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [RedirectSite]

    [ErsSideMenu]
    [ErsLanguageMenu]
    public class registerController
        : ErsControllerSecure
    {
        [HttpPost]
        public virtual ActionResult login(Login login)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILoginCommand>(login), login);
            if (!ModelState.IsValid)
            {
                this.ClearModelState(login); // エラーをクリア

                if (login.account_status == EnumAccountStatus.Locked)
                {
                    this.ModelState.AddModelError("common", ErsResources.GetMessage("memberlocked"));　//共通エラー
                }
                else
                this.ModelState.AddModelError("common", ErsResources.GetMessage("10204"));　//共通エラー

                return GetErrorView();
            }

            //乱数発行

            try
            {
                this.ModelState.AddModelErrors(commandBus.Validate<IMemberRankPricingCommand>(login), login);
            }
            catch (ErsException ex)
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return GetErrorView(setup.multiple_pc_sec_url(setup.site_id) + "top/cart/asp/cart.asp", login, ex.Message);
            }
            finally
            {
                this.commandBus.Submit<ILoginCommand>(login, EnumCommandTransaction.BeginTransaction);
            }
            //registerに値をバインド。entryに引き継ぐ
            Register register = this.GetRegisterModel();

            //registerに値をバインド。entryに引き継ぐ
            Cart cart = this.GetRegisterCartModel();

            return entry(register, cart);
        }

        /// <summary>
        /// ログイン画面
        /// </summary>
        /// <returns></returns>
        protected virtual Register GetRegisterModel()
        {
            return this.GetBindedModel<Register>();
        }

        /// <summary>
        /// ログイン画面
        /// </summary>
        /// <returns></returns>
        protected virtual Cart GetRegisterCartModel()
        {
            return this.GetBindedModel<Cart>();
        }

        /// <summary>
        /// 購入情報入力画面
        /// </summary>
        /// <param name="register"></param>
        /// <param name="zip_search"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAuthorizationRegister]
        public virtual ActionResult entry(Register register, Cart cart, EnumEck? eck = null)
        {
            register.cart = cart;
            cart.IsRegister = true;
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderMemberRegistCommand>(register), register);
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderRegistCommand>(register), register);
            this.ModelState.AddModelErrors(commandBus.Validate<ICartCommand>(cart), cart);

            if (!this.IsErrorBack(eck))
            {
                if (!register.IsValidField("mcode"))
                {
                    var message = register.GetFieldErrorMessage("mcode");
                    ClearModelState(register, cart);//Modelのエラーを無視
                    throw new ErsException("universal", message);
                }

                ClearModelState(register, cart);//Modelのエラーを無視

                this.mapperBus.Map<IMemberOrderMappable>(register);

                this.commandBus.Submit<IRegisterCartCommand>(cart, EnumCommandTransaction.BeginTransaction);
                this.mapperBus.Map<ICartMappable>(cart);
                this.ModelState.AddModelErrors(commandBus.Validate<ICartCommand>(cart), cart);

                //会員ランク用の価格取得の為、カート再計算
                this.commandBus.Submit<ICartForcedRefleshCommand>(cart, EnumCommandTransaction.BeginTransaction);
            }
            else if (eck == EnumEck.BackPage)
            {
                //次画面から戻った場合
                ClearModelState(register, cart);
            }

            //Validate Regular Orders
            register.IsValidateRegularOrder = true;
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderRegistCommand>(register), register);

            register.SetOutputHidden("input_entry", true);
            register.SetOutputHidden("input_payment", true);
            register.SetOutputHidden("input_point", true);

            return View("entry", register);
        }

        /// <summary>
        /// 購入情報入力画面
        /// </summary>
        /// <param name="register"></param>
        /// <param name="zip_search"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAuthorizationRegister]
        public virtual ActionResult entry2(Register register, Cart cart, EnumEck? eck = null)
        {
            register.cart = cart;
            cart.IsRegister = true;
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderMemberRegistCommand>(register), register);
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderRegistCommand>(register), register);
            this.ModelState.AddModelErrors(commandBus.Validate<ICartCommand>(cart), cart);
            //次画面から戻った場合、取消ボタン時はエラークリア
            if (eck == EnumEck.BackPage || !string.IsNullOrEmpty(cart.del_key) || !string.IsNullOrEmpty(cart.del_regular_key))
            {
                //pointエラーのときは、entry3へ飛ばす
                if (!ModelState.IsValidField("ent_point") || !ModelState.IsValidField("ent_coupon_code"))
                {
                    return this.entry3(register, cart, EnumEck.Error);
                }

                ClearModelState(register, cart);//Modelのエラーを無視
            }
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    if (cart.recompute || cart.regular_recompute)
                    {
                        return GetErrorView();
                    }
                    else
                    {
                        return this.entry(register, cart, EnumEck.Error);
                    }
                }
            }

            register.IsConfirmationPage = true;
            this.mapperBus.Map<IMemberOrderMappable>(register);

            if (!this.IsErrorBack(eck))
            {
                //初期値設定
                if (register.k_flg == EnumMemberEntryMode.MEMBER && register.ListCreditCardInfo.Count() > 0 && register.card_id.HasValue)
                {
                    register.card_id = Convert.ToInt32(register.ListCreditCardInfo.First()["card_id"]);
                }
            }

            //受信データに問題がない場合は、データ更新
            this.commandBus.Submit<IRegisterCartCommand>(cart, EnumCommandTransaction.BeginTransaction);

            this.mapperBus.Map<ICartMappable>(cart);

            //Validate Regular Orders
            register.IsValidateRegularOrder = true;
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderRegistCommand>(register), register);

            this.ModelState.AddModelErrors(commandBus.Validate<ICartCommand>(cart), cart);

            register.SetOutputHidden("input_entry", true);
            register.SetOutputHidden("input_member", true);
            register.SetOutputHidden("input_point", true);

            this.AddModelToView("CartModel", cart);
           
            return View("entry2", register);
        }

        /// <summary>
        /// 購入情報確認画面
        /// </summary>
        /// <returns></returns>
        [ErsAuthorizationRegister]
        public virtual ActionResult entry3(Register register, Cart cart, EnumEck? eck = null)
        {
            register.cart = cart;
            cart.IsRegister = true;
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderMemberRegistCommand>(register), register);
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderRegistCommand>(register), register);
            this.ModelState.AddModelErrors(commandBus.Validate<ICartCommand>(cart), cart);
            if (cart.recompute || cart.regular_recompute || cart.del_regular_key.HasValue() || cart.del_key.HasValue())
            {
                return entry2(register, cart);
            }

            this.mapperBus.Map<ICartMappable>(cart);

            register.IsConfirmationPage = true;
            this.mapperBus.Map<IMemberOrderMappable>(register);

            //if payment type is paypal, Redirect to PayPal site
            if (register.paypalReturn == false && register.pay == EnumPaymentType.PAYPAL)
            {
                this.mapperBus.Map<IOrderMappable>(register);

                var isMonitor = ErsFactory.ersOrderFactory.GetIsMonitorSpec().IsSpecified(cart.basket, register);
                var paypal = (ErsPayPal)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.PAYPAL);
                return paypal.SetExpressCheckout(register, register.order, (int)register.k_flg, isMonitor,
                                                "register", "register", "paypal_return", "paypal_cancel");
            }

            if (!this.IsErrorBack(eck))
            {
                this.mapperBus.Map<IOrderMappable>(register);

                //Map後チェック
                register.ValidateMappedOrder = true;
                this.ModelState.AddModelErrors(commandBus.Validate<IOrderRegistCommand>(register), register);
                if (ModelState.IsValid)
                {
                    //会員ランク用の価格取得の為、カート再計算
                    this.mapperBus.Map<ICartMappable>(cart);
                    this.commandBus.Submit<ICartForcedRefleshCommand>(cart, EnumCommandTransaction.BeginTransaction);

                    //Validate Regular Orders
                    register.IsValidateRegularOrder = true;
                    this.ModelState.AddModelErrors(commandBus.Validate<IOrderRegistCommand>(register), register);
                }
                else
                {
                    ///Recompute invalid coupon
                    if (register.coupon_flg && !this.ModelState.IsValidField("ent_coupon_code"))
                    {
                        this.mapperBus.Map<IOrderMappable>(register);
                    }
                }
            }
            else
            {
                this.mapperBus.Map<IOrderMappable>(register);
            }

            //point入力時のエラーはentry3で表示する。
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    if (register.coupon_flg)
                    {
                        register.validated_ent_coupon_code = register.coupon_code;
                        register.coupon_code = null;
                        return entry3(register, cart, EnumEck.Error);
                    }
                    return entry2(register, cart, EnumEck.Error);
                }
            }

            register.SetOutputHidden("input_entry", true);
            register.SetOutputHidden("input_member", true);
            register.SetOutputHidden("input_payment", true);

            this.AddModelToView("CartModel", cart);

            return View("entry3", register);
        }

        /// <summary>
        /// PayPal戻り時
        /// </summary>
        /// <param name="val"></param>
        /// <param name="token"></param>
        /// <param name="PayerID"></param>
        /// <returns></returns>
        public virtual ActionResult paypal_return(Paypal_return paypal_return)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IPaypalReturnCommand>(paypal_return), paypal_return);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            paypal_return.register = this.GetRegisterModel();
            paypal_return.cart = this.GetRegisterCartModel();

            this.mapperBus.Map<IPaypalReturnMappable>(paypal_return);

            return entry3(paypal_return.register, paypal_return.cart);
        }

        /// <summary>
        /// PayPalキャンセル戻り時
        /// </summary>
        /// <param name="val"></param>
        /// <param name="token"></param>
        /// <param name="PayerID"></param>
        /// <returns></returns>
        public virtual ActionResult paypal_cancel(Paypal_return paypal_return)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IPaypalReturnCommand>(paypal_return), paypal_return);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            paypal_return.register = this.GetRegisterModel();
            paypal_return.cart = this.GetRegisterCartModel();

            this.mapperBus.Map<IPaypalReturnMappable>(paypal_return);

            return entry2(paypal_return.register, paypal_return.cart, EnumEck.Error);
        }

        [ErsAuthorizationRegister]
        public virtual ActionResult entry4(Register register, Cart cart)
        {
            register.cart = cart;
            cart.IsRegister = true;
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderMemberRegistCommand>(register), register);
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderRegistCommand>(register), register);
            this.ModelState.AddModelErrors(commandBus.Validate<ICartCommand>(cart), cart);
            if (!ModelState.IsValid)
            {
                return this.entry3(register, cart, EnumEck.Error);
            }
            
            //ポイント、クーポン利用時は、entry3を使用
            if (register.point_flg || register.coupon_flg )
            {
                return entry3(register, cart);
            }

            this.mapperBus.Map<ICartMappable>(cart);

            var isMonitor = ErsFactory.ersOrderFactory.GetIsMonitorSpec().IsSpecified(cart.basket, register);

            register.IsConfirmationPage = true;
            this.mapperBus.Map<IMemberOrderMappable>(register);

            this.mapperBus.Map<IOrderMappable>(register);

            //Map後チェック
            register.ValidateMappedOrder = true;
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderRegistCommand>(register), register);
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }
            register.coupon_code = register.ent_coupon_code;

            //Validate Regular Orders
            register.IsValidateRegularOrder = true;
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderRegistCommand>(register), register);

            using (var transaction = ErsDB_parent.BeginTransaction())
            {
                this.commandBus.Submit<IOrderMemberRegistCommand>(register, EnumCommandTransaction.WithoutBeginTransaction);
                this.commandBus.Submit<IOrderRegistCommand>(register, EnumCommandTransaction.WithoutBeginTransaction);

                transaction.Commit();
            }

            //内部でトランザクションを制御します
            this.commandBus.Submit<IOrderMemberCardDeleteCommand>(register, EnumCommandTransaction.WithoutBeginTransaction);

            register.setBasketCookie();

            try
            {
                if (register.k_flg == EnumMemberEntryMode.NO_MEMBER)
                {
                    //新規会員メール送信
                    var memberRegistered = ErsFactory.ersMailFactory.getErsSendMailMemberRegistered();
                    memberRegistered.Send(register.order.d_no, register.member.mcode, register.member.email, register.member.mformat, register, isMonitor);
                }

                //購入完了メール送信
                var sendmailThankyou = ErsFactory.ersMailFactory.getErsSendMailThankyou();
                sendmailThankyou.Send(register.order.d_no, register.member.mcode, register.member.email, register.member.mformat.Value, register, cart, isMonitor);
            }
            catch (Exception e)
            {
                //メール送信はエラー表示させない。
                //ログに落とす。
                ErsCommon.loggingException(e.ToString());

                if (ErsFactory.ersUtilityFactory.getSetup().debug)
                    throw;
            }

            return View("entry4", register);
        }

        /// <summary>
        /// 初回表示
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult member_entry(MemberEntry member, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMemberEntryCommand>(member), member);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(member);
                }
            }

            return View("member_entry1", member);
        }

        /// <summary>
        /// 確認画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult member_entry2(MemberEntry member)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMemberEntryCommand>(member), member);
            if (!ModelState.IsValid)
            {
                return this.member_entry(member, EnumEck.Error);
            }

            member.SetOutputHidden(true);

            return View("member_entry2", member);
        }

        /// <summary>
        /// 完了画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult member_entry3(MemberEntry member)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMemberEntryCommand>(member), member);
            if (!ModelState.IsValid)
            {
                return this.member_entry(member, EnumEck.Error);
            }

            //member regist
            commandBus.Submit((IMemberEntryCommand)member, EnumCommandTransaction.BeginTransaction);

            try
            {
                //新規会員メール送信
                var memberRegistered = ErsFactory.ersMailFactory.getErsSendMailMemberRegistered();
                memberRegistered.Send(null, member.mcode, member.email, member.mformat, member, false);
            }
            catch (Exception e)
            {
                //メール送信はエラー表示させない。
                //ログに落とす。
                ErsCommon.loggingException(e.ToString());

                if (ErsFactory.ersUtilityFactory.getSetup().debug)
                    throw;
            }

            return View("member_entry3", member);
        }
    }
}
