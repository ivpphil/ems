﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ers.Models.ResizeImage;
using jp.co.ivp.ers.mvc;
using ers.Domain.ResizeImage.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.state;

namespace ers.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [ErsSideMenu]
    public class ResizeImageController
        : ErsControllerBase
    {
        #region override
        protected override ErsState GetErsSessionState()
        {
            throw new NotImplementedException();
        }

        protected override void SetSessionToView()
        {
        }

        protected override ErsState GetErsState()
        {
            throw new NotImplementedException();
        }
        #endregion

        public ActionResult resizeImage(ResizeImage resizeImage)
        {
            if (!this.ModelState.IsValid)
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }

            this.mapperBus.Map<IResizeImageMappable>(resizeImage);

            return File(resizeImage.bytes, resizeImage.contentType);
        }
    }
}
