﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using ers.Models.images;
using ers.Domain.Images.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ers.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class imagesController
        : ErsControllerSecure
    {
        //
        // GET: /Detail/
        public virtual ActionResult spacer(stepSpacer objSpacer)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IStepSpacerCommand>(objSpacer), objSpacer);
            if (ModelState.IsValid)
            {
                //正常値の場合は、開封率をカウントアップ
                this.commandBus.Submit<IStepSpacerCommand>(objSpacer, EnumCommandTransaction.BeginTransaction);
            }

            return File(Server.MapPath("~/images/spacer.gif"), "image/gif");
        }
    }
}
