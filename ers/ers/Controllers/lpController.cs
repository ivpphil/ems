﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using System.Web.Mvc;
using ers.Models;
using jp.co.ivp.ers;
using ers.Domain.Login.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Web.Routing;
using jp.co.ivp.ers.state;
using ers.Domain.Lp.Mappables;
using ers.Domain.Lp.Commands;
using jp.co.ivp.ers.db;
using ers.Models.cart;
using ers.Domain.Cart.Commands;
using ers.Domain.Cart.Mappables;
using ers.Domain.Login.Mappables;
using ers.Domain.Register.Commands;
using jp.co.ivp.ers.Payment;
using ers.Domain.Register.Mappables;
using System.Collections;

namespace ers.Controllers
{
    [ValidateInput(false)]
    [ErsRequireHttps]
    [RedirectSite]
    [ErsSideMenu]
    [ErsLanguageMenu]
    public class lpController
        : ErsControllerSecure
    {
        [ErsFrontProcessCompletionAttribute("LPController_landing_page", mode = EnumHandlingMode.RESET)]
        public ActionResult lp(LandingPage lp_page, LP_Cart lp_cart, EnumEck? eck = null)
        {
            if (ModelState.ContainsKey(LandingPage.paypal_error_return_key)) ModelState.Remove(LandingPage.paypal_error_return_key);
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILandingPageCommand>(lp_page), lp_page);
            if (!IsErrorBack(eck))
            {
                ClearModelState(lp_page);
            }

            lp_page.cart = lp_cart;

            if (eck != EnumEck.Error)
            {
                lp_page.IsConfirmPage = (eck == EnumEck.BackPage);
                lp_page.IsNotConfirmationTransaction = true;
                this.mapperBus.Map<ILandingPageMappable>(lp_page);
            }

            this.mapperBus.Map<ILPMemberMappable>(lp_page);
            //Set Landing Page Template Path
            lp_page.lp_page_type_code = EnumLpPageTypeCode.Sell;
            this.mapperBus.Map<ILandingPageTemplatePathMappable>(lp_page);

            //Add info when out of stock
            if (lp_page.IsOutOfStock)
                this.AddInformation(ErsResources.GetMessage("LP0002724"));

            this.AddModelToView("CartModel", lp_cart);

            lp_page.SetOutputHidden("page_ids", true);
            lp_page.SetOutputHidden("detail_codes", true);

            return View("lp", lp_page);
        }

        [ErsFrontProcessCompletionAttribute("LPController_landing_page", mode = EnumHandlingMode.CHECK)]
        public ActionResult lp_confirm(LandingPage lp_page, LP_Cart lp_cart)
        {
            if (ModelState.ContainsKey(LandingPage.paypal_error_return_key))
            {
                ModelState.Remove(LandingPage.paypal_error_return_key);
            }
            lp_page.cart = lp_cart;
            lp_page.IsConfirmPage = true;

            //LandingPage Mapper
            this.mapperBus.Map<ILandingPageMappable>(lp_page);
            //LandingPage Validator
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILandingPageCommand>(lp_page), lp_page);

            //proceed complete for Non Confirm Page
            if (!lp_page.HasConfirmPage)
            {
                return this.lp_complete(lp_page, lp_cart);
            }

            //LandingPage Cart Validator
            lp_cart.IsOrdinaryOrder = lp_page.IsOrdinaryOrder;
            lp_cart.IsRegularOrder = lp_page.IsRegularOrder;
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILPCartCommand>(lp_cart), lp_cart);

            //LandingPage Member Validator
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILPMemberRegistCommand>(lp_page), lp_page);
            if (!ModelState.IsValid)
            {
                return this.lp(lp_page, lp_cart, EnumEck.Error);
            }

            lp_cart.lp_page_manage = lp_page.lp_page_manage;
            this.mapperBus.Map<ILPMemberMappable>(lp_page);
            this.commandBus.Submit<ILPCartCommand>(lp_cart, EnumCommandTransaction.BeginTransaction);
            this.mapperBus.Map<ILPCartMappable>(lp_cart);
            this.mapperBus.Map<ILPOrderMappable>(lp_page);

            lp_page.ValidateMappedOrderData = true;
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILandingPageCommand>(lp_page), lp_page);
            if (!ModelState.IsValid)
            {
                return this.lp(lp_page, lp_cart, EnumEck.Error);
            }

            //if payment type is paypal, Redirect to PayPal site
            if (lp_page.paypalReturn == false && lp_page.pay == EnumPaymentType.PAYPAL && ((!lp_cart.regular_basket_in && !lp_page.IsNonPaymentTypeOrdinary) || (lp_cart.regular_basket_in && !lp_page.IsNonPaymentTypeRegular)))
            {
                //Add proccess_completion due proccess check completion always error
                var proccess_completion = this.GetProccessCompletion();
                var additionalQuery = new Dictionary<string, object>();
                if (proccess_completion != null)
                {
                    var proccessRansu = proccess_completion["process_completion_check_ransu"] as String[];
                    additionalQuery.Add("process_completion_check_ransu", String.Join(",", proccessRansu));
                }

                this.mapperBus.Map<ILPOrderMappable>(lp_page);

                var isMonitor = ErsFactory.ersOrderFactory.GetIsMonitorSpec().IsSpecified(lp_cart.basket, lp_page);
                var paypal = (ErsPayPal)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.PAYPAL);
                return paypal.SetExpressCheckout(lp_page, lp_page.order, 0, isMonitor,
                                                "lp", "lp", "paypal_return", "paypal_cancel",
                                                additionalQuery);
            }

            //Set Landing Page Template Path
            lp_page.lp_page_type_code = EnumLpPageTypeCode.Confirm;
            this.mapperBus.Map<ILandingPageTemplatePathMappable>(lp_page);

            this.AddModelToView("CartModel", lp_cart);
            lp_cart.SetOutputHidden("lp_confirm", true);

            lp_page.SetOutputHidden("page_ids", true);
            lp_page.SetOutputHidden("lp_confirm", true);
            lp_page.SetOutputHidden("input_payment", true);

            return View("lp_confirm", lp_page);
        }

        [ErsFrontProcessCompletionAttribute("LPController_landing_page", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult lp_complete(LandingPage lp_page, LP_Cart lp_cart)
        {
            lp_page.cart = lp_cart;
            lp_page.IsConfirmPage = true;

            //LandingPage Mapper
            this.mapperBus.Map<ILandingPageMappable>(lp_page);
            //LandingPage Validator
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILandingPageCommand>(lp_page), lp_page);

            //LandingPage Cart Validator
            lp_cart.IsOrdinaryOrder = lp_page.IsOrdinaryOrder;
            lp_cart.IsRegularOrder = lp_page.IsRegularOrder;
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILPCartCommand>(lp_cart), lp_cart);

            //LandingPage Member Validator
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILPMemberRegistCommand>(lp_page), lp_page);
            if (!ModelState.IsValid)
            {
                return this.lp(lp_page, lp_cart, EnumEck.Error);
            }

            //Execute Cart Command Handler for Non Confirm Page
            lp_cart.lp_page_manage = lp_page.lp_page_manage;
            if (!lp_page.HasConfirmPage)
            {
                this.commandBus.Submit<ILPCartCommand>(lp_cart, EnumCommandTransaction.WithoutBeginTransaction);
            }
            else
            {
                this.mapperBus.Map<ILPCartMappable>(lp_cart);
            }

            this.mapperBus.Map<ILPOrderMappable>(lp_page);

            if (!lp_page.HasConfirmPage)
            {
                //if payment type is paypal, Redirect to PayPal site
                if (lp_page.paypalReturn == false && lp_page.pay == EnumPaymentType.PAYPAL && ((!lp_cart.regular_basket_in && !lp_page.IsNonPaymentTypeOrdinary) || (lp_cart.regular_basket_in && !lp_page.IsNonPaymentTypeRegular)))
                {
                    //Add proccess_completion due proccess check completion always error
                    var proccess_completion = this.GetProccessCompletion();
                    var additionalQuery = new Dictionary<string, object>();
                    if (proccess_completion != null)
                    {
                        var proccessRansu = proccess_completion["process_completion_check_ransu"] as String[];
                        additionalQuery.Add("process_completion_check_ransu", String.Join(",", proccessRansu));
                    }

                    this.mapperBus.Map<ILPOrderMappable>(lp_page);

                    var isMonitor = ErsFactory.ersOrderFactory.GetIsMonitorSpec().IsSpecified(lp_cart.basket, lp_page);
                    var paypal = (ErsPayPal)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.PAYPAL);

                    //To not able to remove process completion ransu
                    this.ModelState.AddModelError(LandingPage.paypal_error_return_key, string.Empty);
                    return paypal.SetExpressCheckout(lp_page, lp_page.order, 0, isMonitor,
                                                    "lp", "lp", "paypal_return_lp_complete", "paypal_cancel",
                                                    additionalQuery);
                }
            }

            lp_page.ValidateMappedOrderData = true;
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILandingPageCommand>(lp_page), lp_page);
            if (!ModelState.IsValid)
            {
                return this.lp(lp_page, lp_cart, EnumEck.Error);
            }

            using (var transaction = ErsDB_parent.BeginTransaction())
            {
                this.commandBus.Submit<ILPMemberRegistCommand>(lp_page, EnumCommandTransaction.WithoutBeginTransaction);
                this.commandBus.Submit<ILandingPageCommand>(lp_page, EnumCommandTransaction.WithoutBeginTransaction);
                this.commandBus.Submit<IDQuestionnaireRegistCommand>(lp_page, EnumCommandTransaction.WithoutBeginTransaction);

                transaction.Commit();
            }

            lp_page.k_flg = (!lp_page.IsNotLogged) ? EnumMemberEntryMode.MEMBER : (EnumMemberEntryMode?)null;
            //内部でトランザクションを制御します
            this.commandBus.Submit<IOrderMemberCardDeleteCommand>(lp_page, EnumCommandTransaction.WithoutBeginTransaction);

            //Empty Landing Page Basket
            ErsFactory.ersBasketFactory.GetEmptyBasketStrategy().EmptyBasket(lp_page.ransu);

            try
            {
                if (lp_page.IsNewMember)
                {
                    //新規会員メール送信
                    var memberRegistered = ErsFactory.ersMailFactory.getErsSendMailMemberRegistered();
                    memberRegistered.Send(lp_page.order.d_no, lp_page.member.mcode, lp_page.member.email, lp_page.member.mformat, lp_page, false);
                }

                //購入完了メール送信
                var sendmailThankyou = ErsFactory.ersMailFactory.getErsSendMailThankyou();
                sendmailThankyou.Send(lp_page.order.d_no, lp_page.member.mcode, lp_page.member.email, lp_page.member.mformat.Value, lp_page, lp_cart, false);
            }
            catch (Exception e)
            {
                //メール送信はエラー表示させない。
                //ログに落とす。
                ErsCommon.loggingException(e.ToString());

                if (ErsFactory.ersUtilityFactory.getSetup().debug)
                    throw;
            }

            //Set Landing Page Template Path
            lp_page.lp_page_type_code = EnumLpPageTypeCode.Complete;
            this.mapperBus.Map<ILandingPageTemplatePathMappable>(lp_page);

            return View("lp_complete", lp_page);
        }

        [ErsFrontProcessCompletionAttribute("LPController_upsell_landing_page", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult lp_upsell(LP_Upsell lp_upsell, LP_Cart lp_cart, EnumEck? eck = null)
        {
            if (ModelState.ContainsKey(LandingPage.paypal_error_return_key)) ModelState.Remove(LandingPage.paypal_error_return_key);
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILandingPageUpSellCommand>(lp_upsell), lp_upsell);
            if (!IsErrorBack(eck))
            {
                ClearModelState(lp_upsell);
            }

            lp_upsell.cart = lp_cart;

            if (eck != EnumEck.Error)
            {
                lp_upsell.IsConfirmPage = (eck == EnumEck.BackPage);
                lp_upsell.IsNotConfirmationTransaction = true;
                this.mapperBus.Map<ILandingPageUpSellMappable>(lp_upsell);
            }

            this.mapperBus.Map<ILPMemberMappable>(lp_upsell);
            //Set Landing Page Template Path
            lp_upsell.lp_page_type_code = EnumLpPageTypeCode.Upsell;
            this.mapperBus.Map<ILandingPageTemplatePathMappable>(lp_upsell);

            //Add info when out of stock
            if (lp_upsell.IsOutOfStock)
                this.AddInformation(ErsResources.GetMessage("LP0002724"));

            this.AddModelToView("CartModel", lp_cart);

            lp_upsell.SetOutputHidden("page_ids", true);
            lp_upsell.SetOutputHidden("detail_codes", true);

            return View("lp", lp_upsell);
        }

        [ErsFrontProcessCompletionAttribute("LPController_upsell_landing_page", mode = EnumHandlingMode.CHECK)]
        public ActionResult lp_upsell_confirm(LP_Upsell lp_upsell, LP_Cart lp_cart)
        {
            if (ModelState.ContainsKey(LandingPage.paypal_error_return_key)) ModelState.Remove(LandingPage.paypal_error_return_key);
            lp_upsell.cart = lp_cart;
            lp_upsell.IsConfirmPage = true;

            //LandingPage UpSell Mapper
            this.mapperBus.Map<ILandingPageUpSellMappable>(lp_upsell);
            //LandingPage UpSell Validator
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILandingPageUpSellCommand>(lp_upsell), lp_upsell);

            //proceed complete for Non Confirm Page
            if (!lp_upsell.HasConfirmPage)
            {
                return this.lp_upsell_complete(lp_upsell, lp_cart);
            }

            //LandingPage Cart Validator
            lp_cart.IsOrdinaryOrder = lp_upsell.IsOrdinaryOrder;
            lp_cart.IsRegularOrder = lp_upsell.IsRegularOrder;
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILPCartCommand>(lp_cart), lp_cart);

            //LandingPage Member Validator
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILPMemberRegistCommand>(lp_upsell), lp_upsell);
            if (!ModelState.IsValid)
            {
                return this.lp_upsell(lp_upsell, lp_cart, EnumEck.Error);
            }

            lp_cart.lp_page_manage = lp_upsell.lp_page_manage;
            this.mapperBus.Map<ILPMemberMappable>(lp_upsell);
            this.commandBus.Submit<ILPUpSellCartCommand>(lp_cart, EnumCommandTransaction.BeginTransaction);
            this.mapperBus.Map<ILPCartMappable>(lp_cart);
            this.mapperBus.Map<ILPOrderMappable>(lp_upsell);

            lp_upsell.ValidateMappedOrderData = true;
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILandingPageUpSellCommand>(lp_upsell), lp_upsell);
            if (!ModelState.IsValid)
            {
                return this.lp_upsell(lp_upsell, lp_cart, EnumEck.Error);
            }

            //if payment type is paypal, Redirect to PayPal site
            if (lp_upsell.paypalReturn == false && lp_upsell.pay == EnumPaymentType.PAYPAL && ((!lp_cart.regular_basket_in && !lp_upsell.IsNonPaymentTypeOrdinary) || (lp_cart.regular_basket_in && !lp_upsell.IsNonPaymentTypeRegular)))
            {
                //Add proccess_completion due proccess check completion always error
                var proccess_completion = this.GetProccessCompletion();

                var additionalQuery = new Dictionary<string, object>();
                if (proccess_completion != null)
                {
                    var proccessRansu = proccess_completion["process_completion_check_ransu"] as String[];
                    additionalQuery.Add("process_completion_check_ransu", String.Join(",", proccessRansu));
                }

                this.mapperBus.Map<ILPOrderMappable>(lp_upsell);

                var isMonitor = ErsFactory.ersOrderFactory.GetIsMonitorSpec().IsSpecified(lp_cart.basket, lp_upsell);
                var paypal = (ErsPayPal)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.PAYPAL);
                return paypal.SetExpressCheckout(lp_upsell, lp_upsell.order, 0, isMonitor,
                                                "lp", "lp", "paypal_return", "paypal_cancel",
                                                additionalQuery);
            }

            //Set Landing Page Template Path
            lp_upsell.lp_page_type_code = EnumLpPageTypeCode.Confirm;
            this.mapperBus.Map<ILandingPageTemplatePathMappable>(lp_upsell);

            this.AddModelToView("CartModel", lp_cart);
            lp_cart.SetOutputHidden("lp_confirm", true);

            lp_upsell.SetOutputHidden("page_ids", true);
            lp_upsell.SetOutputHidden("lp_confirm", true);
            lp_upsell.SetOutputHidden("input_payment", true);

            return View("lp_confirm", lp_upsell);
        }

        [ErsFrontProcessCompletionAttribute("LPController_upsell_landing_page", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult lp_upsell_complete(LP_Upsell lp_upsell, LP_Cart lp_cart)
        {
            lp_upsell.cart = lp_cart;
            lp_upsell.IsConfirmPage = true;

            //LandingPage UpSell Mapper
            this.mapperBus.Map<ILandingPageUpSellMappable>(lp_upsell);
            //LandingPage UpSell Validator
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILandingPageUpSellCommand>(lp_upsell), lp_upsell);

            //LandingPage Cart Validator
            lp_cart.IsOrdinaryOrder = lp_upsell.IsOrdinaryOrder;
            lp_cart.IsRegularOrder = lp_upsell.IsRegularOrder;
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILPCartCommand>(lp_cart), lp_cart);

            //LandingPage Member Validator
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILPMemberRegistCommand>(lp_upsell), lp_upsell);
            if (!ModelState.IsValid)
            {
                return this.lp_upsell(lp_upsell, lp_cart, EnumEck.Error);
            }

            //Execute Cart Command Handler for Non Confirm Page
            lp_cart.lp_page_manage = lp_upsell.lp_page_manage;
            if (!lp_upsell.HasConfirmPage)
                this.commandBus.Submit<ILPUpSellCartCommand>(lp_cart, EnumCommandTransaction.WithoutBeginTransaction);
            else
                this.mapperBus.Map<ILPCartMappable>(lp_cart);

            this.mapperBus.Map<ILPOrderMappable>(lp_upsell);

            if (!lp_upsell.HasConfirmPage)
            {
                //if payment type is paypal, Redirect to PayPal site
                if (lp_upsell.paypalReturn == false && lp_upsell.pay == EnumPaymentType.PAYPAL && ((!lp_cart.regular_basket_in && !lp_upsell.IsNonPaymentTypeOrdinary) || (lp_cart.regular_basket_in && !lp_upsell.IsNonPaymentTypeRegular)))
                {
                    
                    //Add proccess_completion due proccess check completion always error
                    var proccess_completion = this.GetProccessCompletion();

                    var additionalQuery = new Dictionary<string, object>();
                    if (proccess_completion != null)
                    {
                        var proccessRansu = proccess_completion["process_completion_check_ransu"] as String[];
                        additionalQuery.Add("process_completion_check_ransu", String.Join(",", proccessRansu));
                    }

                    this.mapperBus.Map<ILPOrderMappable>(lp_upsell);

                    var isMonitor = ErsFactory.ersOrderFactory.GetIsMonitorSpec().IsSpecified(lp_cart.basket, lp_upsell);
                    var paypal = (ErsPayPal)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.PAYPAL);

                    //To not able to remove process completion ransu
                    this.ModelState.AddModelError(LandingPage.paypal_error_return_key, string.Empty);
                    return paypal.SetExpressCheckout(lp_upsell, lp_upsell.order, 0, isMonitor,
                                                    "lp", "lp", "paypal_return_upsell_complete", "paypal_cancel",
                                                    additionalQuery);
                }
            }

            lp_upsell.ValidateMappedOrderData = true;
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILandingPageUpSellCommand>(lp_upsell), lp_upsell);
            if (!ModelState.IsValid)
            {
                return this.lp_upsell(lp_upsell, lp_cart, EnumEck.Error);
            }

            using (var transaction = ErsDB_parent.BeginTransaction())
            {
                this.commandBus.Submit<ILPMemberRegistCommand>(lp_upsell, EnumCommandTransaction.WithoutBeginTransaction);
                this.commandBus.Submit<ILandingPageCommand>(lp_upsell, EnumCommandTransaction.WithoutBeginTransaction);
                this.commandBus.Submit<IDQuestionnaireRegistCommand>(lp_upsell, EnumCommandTransaction.WithoutBeginTransaction);

                transaction.Commit();
            }

            lp_upsell.k_flg = (!lp_upsell.IsNotLogged) ? EnumMemberEntryMode.MEMBER : (EnumMemberEntryMode?)null;
            //内部でトランザクションを制御します
            this.commandBus.Submit<IOrderMemberCardDeleteCommand>(lp_upsell, EnumCommandTransaction.WithoutBeginTransaction);

            //Empty Landing Page Basket
            ErsFactory.ersBasketFactory.GetEmptyBasketStrategy().EmptyBasket(lp_upsell.ransu);

            try
            {
                if (lp_upsell.IsNewMember)
                {
                    //新規会員メール送信
                    var memberRegistered = ErsFactory.ersMailFactory.getErsSendMailMemberRegistered();
                    memberRegistered.Send(lp_upsell.order.d_no, lp_upsell.member.mcode, lp_upsell.member.email, lp_upsell.member.mformat, lp_upsell, false);
                }

                //購入完了メール送信
                var sendmailThankyou = ErsFactory.ersMailFactory.getErsSendMailThankyou();
                sendmailThankyou.Send(lp_upsell.order.d_no, lp_upsell.member.mcode, lp_upsell.member.email, lp_upsell.member.mformat.Value, lp_upsell, lp_cart, false);
            }
            catch (Exception e)
            {
                //メール送信はエラー表示させない。
                //ログに落とす。
                ErsCommon.loggingException(e.ToString());

                if (ErsFactory.ersUtilityFactory.getSetup().debug)
                    throw;
            }

            //Set Landing Page Template Path
            lp_upsell.lp_page_type_code = EnumLpPageTypeCode.Complete;
            this.mapperBus.Map<ILandingPageTemplatePathMappable>(lp_upsell);

            return View("lp_complete", lp_upsell);
        }

        [HttpPost]
        [ErsFrontProcessCompletionAttribute("LPController_landing_page", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult lp_login(LP_Login login)
        {
            //LandingPageに値をバインド。lp_pageに引き継ぐ
            LandingPage lp_page_model = this.GetLandingPageModel();

            //LandingPageUpSellに値をバインド。lp_pageに引き継ぐ
            LP_Upsell lp_upsell_model = this.GetLandingPageUpSellModel();

            this.mapperBus.Map<ILPLoginMappable>(login);
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILoginCommand>(login), login);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //乱数発行
            this.commandBus.Submit<ILoginCommand>(login, EnumCommandTransaction.BeginTransaction);

            lp_page_model.IsFromLoginPage = true;
            lp_upsell_model.IsFromLoginPage = true;
            LP_Cart lp_cart = this.GetLP_CartModel();

            if (!login.IsLandingPage)
                return lp_upsell(lp_upsell_model, lp_cart);

            //LandingPageに値をバインド。lp_pageに引き継ぐ
            return lp(lp_page_model, lp_cart);
        }

        /// <summary>
        /// PayPal戻り時
        /// </summary>
        /// <param name="val"></param>
        /// <param name="token"></param>
        /// <param name="PayerID"></param>
        /// <returns></returns>
        [ErsFrontProcessCompletionAttribute("LPController_landing_page", mode = EnumHandlingMode.CHECK)]
        public virtual ActionResult paypal_return(Paypal_return paypal_return)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IPaypalReturnCommand>(paypal_return), paypal_return);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //LandingPageに値をバインド。lp_pageに引き継ぐ
            LandingPage lp_page_model = this.GetLandingPageModel();

            //LandingPageUpSellに値をバインド。lp_pageに引き継ぐ
            LP_Upsell lp_upsell_model = this.GetLandingPageUpSellModel();


            var objLPModel = this.GetLandingPageModel();
            paypal_return.register = objLPModel;
            paypal_return.cart = this.GetLP_CartModel();
            this.mapperBus.Map<IPaypalReturnMappable>(paypal_return);

            paypal_return.cart = this.GetLP_CartModel();
            if (!objLPModel.IsLandingPage)
            {
                paypal_return.register = lp_upsell_model;
                this.mapperBus.Map<IPaypalReturnMappable>(paypal_return);

                return lp_upsell_confirm((LP_Upsell)paypal_return.register, (LP_Cart)paypal_return.cart);
            }

            paypal_return.register = lp_page_model;
            this.mapperBus.Map<IPaypalReturnMappable>(paypal_return);


            return lp_confirm((LandingPage)paypal_return.register, (LP_Cart)paypal_return.cart);
        }

        /// <summary>
        /// PayPal戻り時 Used this for Process Completion Attribute
        /// </summary>
        /// <param name="val"></param>
        /// <param name="token"></param>
        /// <param name="PayerID"></param>
        /// <returns></returns>
        [ErsFrontProcessCompletionAttribute("LPController_landing_page", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult paypal_return_lp_complete(Paypal_return paypal_return)
        {
            return this.paypal_return(paypal_return);
        }

        [ErsFrontProcessCompletionAttribute("LPController_upsell_landing_page", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult paypal_return_upsell_complete(Paypal_return paypal_return)
        {
            return this.paypal_return(paypal_return);
        }

        /// <summary>
        /// PayPalキャンセル戻り時
        /// </summary>
        /// <param name="val"></param>
        /// <param name="token"></param>
        /// <param name="PayerID"></param>
        /// <returns></returns>
        [ErsFrontProcessCompletionAttribute("LPController_landing_page", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult paypal_cancel(Paypal_return paypal_return)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IPaypalReturnCommand>(paypal_return), paypal_return);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //LandingPageに値をバインド。lp_pageに引き継ぐ
            LandingPage lp_page_model = this.GetLandingPageModel();

            //LandingPageUpSellに値をバインド。lp_pageに引き継ぐ
            LP_Upsell lp_upsell_model = this.GetLandingPageUpSellModel();

            var objLPModel = this.GetLandingPageModel();
            paypal_return.register = objLPModel;
            paypal_return.cart = this.GetLP_CartModel();
            this.mapperBus.Map<IPaypalReturnMappable>(paypal_return);

            paypal_return.cart = this.GetLP_CartModel();
            if (!objLPModel.IsLandingPage)
            {
                paypal_return.register = lp_upsell_model;
                this.mapperBus.Map<IPaypalReturnMappable>(paypal_return);paypal_return.register = lp_upsell_model;
                return lp_upsell((LP_Upsell)paypal_return.register, (LP_Cart)paypal_return.cart, EnumEck.BackPage);
            }

            paypal_return.register = lp_page_model;
            this.mapperBus.Map<IPaypalReturnMappable>(paypal_return);

            return lp((LandingPage)paypal_return.register, (LP_Cart)paypal_return.cart, EnumEck.BackPage);
        }

        /// <summary>
        /// ログイン画面
        /// </summary>
        /// <returns></returns>
        protected virtual LandingPage GetLandingPageModel()
        {
            return this.GetBindedModel<LandingPage>();
        }

        /// <summary>
        /// ログイン画面
        /// </summary>
        /// <returns></returns>
        protected virtual LP_Upsell GetLandingPageUpSellModel()
        {
            return this.GetBindedModel<LP_Upsell>();
        }

        /// <summary>
        /// ログイン画面
        /// </summary>
        /// <returns></returns>
        protected virtual LP_Cart GetLP_CartModel()
        {
            return this.GetBindedModel<LP_Cart>();
        }

        protected virtual Dictionary<string, object> GetProccessCompletion()
        {
            var model = this.GetAdditionalModel("process_completion_check_ransu");

            if (model == null)
                return null;

            return model.GetPropertiesAsDictionary();
        }
    }
}