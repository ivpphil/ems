﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using ers.Models;
using jp.co.ivp.ers.util;
using ers.Models.cart;
using ers.Models.redirect;
using ers.Domain.Redirect.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ers.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class redirectController
        : ErsControllerSecure
    {
        //
        // GET: /Detail/
        public virtual ActionResult redirect(stepRedirect objRedirect)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IRedirectCommand>(objRedirect), objRedirect);
            if (!ModelState.IsValid)
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }

            this.commandBus.Submit<IRedirectCommand>(objRedirect, EnumCommandTransaction.BeginTransaction);

            return Redirect(objRedirect.ptn);
        }
    }
}
