﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.merchandise;
using ers.Models;
using jp.co.ivp.ers.mvc;
using System.Web.Routing;
using ers.Models.cart;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Cart.Mappables;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;
using ers.Domain.Login.Commands;

namespace ers.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
	[ErsLanguageMenu]
    public class cartController
        : ErsControllerSecure
    {

        public virtual ActionResult cart(Cart cart, List list)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            this.ModelState.AddModelErrors(commandBus.Validate<ICartCommand>(cart), cart);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }
            //セッション情報を確認
            base.validateSession();

            //set login state
            if (((ISession)ErsContext.sessionState).getUserState() == EnumUserState.LOGIN)
            {
                cart.IsLoggedIn = true;
            }            

            var error = this.TempData["error_at_register"];
            this.ModelState.AddModelError("", Convert.ToString(error));

            this.commandBus.Submit<ICartCommand>(cart, EnumCommandTransaction.BeginTransaction);
            
            this.mapperBus.Map<ICartMappable>(cart);           

            list.SetOutputHidden(true);
            this.AddModelToView(list);

            return View("cart", cart);
        }

        public virtual ActionResult wishlist(Wishlist objWishList)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IWishlistCommand>(objWishList), objWishList);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            if (!((ErsSessionState)ErsContext.sessionState).IsLoginButNotSecure())
            {
                //ログイン画面へリダイレクト
                return this.login(objWishList);
            }

            //セッション情報を確認
            base.validateSession();

            //Pager設定
            objWishList.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", objWishList.pageCnt, objWishList.maxItemCount);

            this.commandBus.Submit<IWishlistCommand>(objWishList, EnumCommandTransaction.BeginTransaction);

            this.mapperBus.Map<IWishlistMappable>(objWishList);

            objWishList.pager.LoadPageList(objWishList.recordCount);
            return View("wishlist", objWishList);
        }

         ///<summary>
         /// ウィッシュリストへのログイン
         ///</summary>
         ///<param name="login"></param>
         ///<returns></returns>
        [HttpGet]
        public virtual ActionResult login(Wishlist objWishList, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IWishlistCommand>(objWishList), objWishList);
            if (!this.ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    ClearModelState(objWishList);
                }
            }

            objWishList.SetOutputHidden(true);

            return View("login", objWishList);
        }

         ///<summary>
         /// ウィッシュリストへのログイン
         ///</summary>
         ///<param name="login"></param>
         ///<returns></returns>
        [HttpPost]
        public virtual ActionResult login(Login login, Wishlist objWishList)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILoginCommand>(login), login);
            this.ModelState.AddModelErrors(this.commandBus.Validate<IWishlistCommand>(objWishList), objWishList);
            if (!ModelState.IsValid)
            {
                return this.login(objWishList, EnumEck.Error);
            }

            this.commandBus.Submit<ILoginCommand>(login, EnumCommandTransaction.BeginTransaction);

            //ウィッシュリストへリダイレクト
            return this.wishlist(objWishList);
        }

    }
}
