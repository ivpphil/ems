﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using ers.Models;
using jp.co.ivp.ers.util;
using System.Net;
using jp.co.ivp.ers.state;

namespace ers.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [ErsSideMenu]
    public class HttpErrorController
        : ErsControllerSecure
    {

        public virtual ActionResult Error(HttpErrorModel httpError)
        {
            return GetHttpErrorView(httpError);
        }

    }
}
