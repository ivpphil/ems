﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ers.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.util;
using ers.Models.member;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using ers.Domain.Member.Commands;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Login.Commands;
using ers.Domain.Home.Mappables;

namespace ers.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [RedirectSite]

    [ErsAuthorization]
    [ErsSideMenu]
    [ErsLanguageMenu]
    public class memberController
        : ErsControllerSecure
    {
        /// <summary>
        /// ログイン画面
        /// </summary>
        /// <returns></returns>
        [NoNeedSession]
        public virtual ActionResult index(Login login, EnumEck? eck = null)
        {
            if (!ModelState.IsValid)
            {
                this.ClearModelState(login); // エラーをクリア
                if (login.account_status == EnumAccountStatus.Locked)
                {
                    this.ModelState.AddModelError("common", ErsResources.GetMessage("memberlocked"));　//共通エラー
                }
                else
                this.ModelState.AddModelError("common", ErsResources.GetMessage("10204"));　//共通エラー

                if (!this.IsErrorBack(eck))
                {
                    return GetErrorView();
                }
            }
            else
            {
                //ログイン済みの場合はメニューへ
                if (((ISession)ErsContext.sessionState).getUserState() == EnumUserState.LOGIN)
                {
                    return user();
                }
            }
            ((ISession)ErsContext.sessionState).LogOut();
            return View("login");

        }

        [NoNeedSession]
        [HttpPost]
        public virtual ActionResult login(Login login)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ILoginCommand>(login), login);
            if (!ModelState.IsValid)
            {
                return index(login, EnumEck.Error);
            }

            this.commandBus.Submit<ILoginCommand>(login, EnumCommandTransaction.BeginTransaction);

            return this.user();
        }

        /// <summary>
        /// マイページメニュー
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult user()
        {
            User ModelUser = new User();

            this.mapperBus.Map<INewsMappable>(ModelUser);

            return View("user", ModelUser);
        }


        /// <summary>
        /// 初回表示
        /// </summary>
        /// <returns></returns>
        [ErsMypageProcessCompletion("mypage_member", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult member1(Member member, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMemberUpdateCommand>(member), member);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(member);
                this.mapperBus.Map<IMemberUpdateMappable>(member);
            }

            return View("member1", member);
        }

        /// <summary>
        /// 確認画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsMypageProcessCompletion("mypage_member", mode = EnumHandlingMode.CHECK)]
        public virtual ActionResult member2(Member member)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMemberUpdateCommand>(member), member);
            if (!ModelState.IsValid)
            {
                return this.member1(member, EnumEck.Error);
            }

            member.IsConfirmationPage = true;
            this.mapperBus.Map<IMemberUpdateMappable>(member);

            member.SetOutputHidden(true);

            return View("member2", member);
        }

        /// <summary>
        /// 完了画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsMypageProcessCompletion("mypage_member", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult member3(Member member)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMemberUpdateCommand>(member), member);
            if (!ModelState.IsValid)
            {
                return this.member1(member, EnumEck.Error);
            }

            member.IsConfirmationPage = true;
            this.mapperBus.Map<IMemberUpdateMappable>(member);

            commandBus.Submit((IMemberUpdateCommand)member, EnumCommandTransaction.BeginTransaction);

            return View("member3", member);
        }


        /// <summary>
        /// 購入履歴伝票一覧画面
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="pageSend"></param>
        /// <returns></returns>
        //[HttpPost]
        public virtual ActionResult bill2(bill_list bill_list, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IBillListCommand>(bill_list), bill_list);

            if (!this.IsErrorBack(eck))
            {
                this.ClearModelState(bill_list);
                //初期値入力
                if (bill_list.s_date1 == null)
                {
                    bill_list.s_date1 = DateTime.Now.AddYears(-1);
                }
                if (bill_list.s_date2 == null)
                {
                    bill_list.s_date2 = DateTime.Now;
                }
            }

            if (!ModelState.IsValid)
            {
                return this.bill2(bill_list, EnumEck.Error);
            }

            //Pager設定
            bill_list.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", bill_list.pageCnt, bill_list.maxItemCount);

            this.mapperBus.Map<IBillListMappable>(bill_list);
            this.mapperBus.Map<IMemberInfoMappable>(bill_list);

            bill_list.pager.LoadPageList(bill_list.recordCount);

            //FormタグにHiddenを自動入力
            bill_list.SetOutputHidden(true);

            return View("bill2", bill_list);
        }

        /// <summary>
        /// 購入履歴伝票詳細画面
        /// </summary>
        /// <param name="bill"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult bill3(Bill bill, bill_list bill_list)
        {
            
            if (!bill.d_no.HasValue())
            {
                ModelState.AddModelErrors(commandBus.Validate<IBillListCommand>(bill_list), bill_list);
            }
            ModelState.AddModelErrors(commandBus.Validate<IBillDetailCommand>(bill), bill);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //伝票リスト取得
            this.mapperBus.Map<IBillDetailMappable>(bill);

            //戻るボタン押下時bill_list 送信用
            bill_list.SetOutputHidden(true);
            this.AddModelToView(bill_list);

            return View("bill3", bill);
        }

        /// <summary>
        /// メール設定初回表示
        /// </summary>
        /// <returns></returns>
        [ErsMypageProcessCompletion("mypage_mailmodify", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult mail1(Mail mail, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailUpdateCommand>(mail), mail);
            if (!this.IsErrorBack(eck))
            {
                this.ClearModelState(mail);
            }

            //メンバー、メルマガ読み込み
            this.mapperBus.Map<IMemberInfoMappable>(mail);
            this.mapperBus.Map<IMailUpdateMappable>(mail);

            return View("mail1", mail);
        }

        /// <summary>
        /// メール設定 確認画面
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsMypageProcessCompletion("mypage_mailmodify", mode = EnumHandlingMode.CHECK)]
        public virtual ActionResult mail2(Mail mail)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailUpdateCommand>(mail), mail);
            if (!ModelState.IsValid)
            {
                return mail1(mail, EnumEck.Error);
            }

            this.mapperBus.Map<IMemberInfoMappable>(mail);

            mail.SetOutputHidden(true);
            return View("mail2", mail);
        }

        /// <summary>
        /// メール設定 完了画面
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsMypageProcessCompletion("mypage_mailmodify", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult mail3(Mail mail)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailUpdateCommand>(mail), mail);
            if (!ModelState.IsValid)
            {
                return mail1(mail, EnumEck.Error);
            }

            this.mapperBus.Map<IMemberInfoMappable>(mail);

            commandBus.Submit<IMailUpdateCommand>(mail, EnumCommandTransaction.BeginTransaction);

            return View("mail3", mail);
        }

        /// <summary>
        /// 別お届け先 リスト画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public virtual ActionResult address_list(AddressList addressList)
        {
            if (!ModelState.IsValid)
            {
                return user();
            }

            addressList.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", addressList.pageCnt, addressList.maxItemCount);

            this.mapperBus.Map<IAddressListMappable>(addressList);

            addressList.pager.LoadPageList(addressList.recordCount);

            addressList.SetOutputHidden(true);

            return View("address_list", addressList);

        }

        /// <summary>
        /// 別お届け先 初回表示
        /// </summary>
        /// <returns></returns>
        [ErsMypageProcessCompletion("mypage_addressinfo", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult address1(AddressInfo address, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAddressUpdateCommand>(address), address);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    this.ClearModelState(address);
            }

            if (!this.IsErrorBack(eck))
            {
                this.mapperBus.Map<IAddressUpdateMappable>(address);
            }

            return View("address1", address);
        }

        /// <summary>
        /// 別お届け先 確認画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsMypageProcessCompletion("mypage_addressinfo", mode = EnumHandlingMode.CHECK)]
        public virtual ActionResult address2(AddressInfo address)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAddressUpdateCommand>(address), address);
            if (!ModelState.IsValid)
            {
                return this.address1(address, EnumEck.Error);
            }

            address.SetOutputHidden(true);
            return View("address2", address);
        }

        /// <summary>
        /// 別お届け先 完了画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsMypageProcessCompletion("mypage_addressinfo", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult address3(AddressInfo address)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAddressUpdateCommand>(address), address);
            if (!ModelState.IsValid)
            {
                return this.address1(address, EnumEck.Error);
            }

            commandBus.Submit<IAddressUpdateCommand>(address, EnumCommandTransaction.BeginTransaction);

            return View("address3", address);
        }
        /// <summary>
        /// 追加する別お届け先 初回表示
        /// </summary>
        /// <returns></returns>
        [ErsMypageProcessCompletion("mypage_addressinfo", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult add_address1(Add_addressInfo address, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAddAddressInsertCommand>(address), address);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    this.ClearModelState(address);
            }

            if (!this.IsErrorBack(eck))
            {
                this.mapperBus.Map<IAddAddressInsertMappable>(address);
            }

            return View("add_address1", address);
        }

        /// <summary>
        /// 追加する別お届け先 確認画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsMypageProcessCompletion("mypage_addressinfo", mode = EnumHandlingMode.CHECK)]
        public virtual ActionResult add_address2(Add_addressInfo address)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAddAddressInsertCommand>(address), address);
            if (!ModelState.IsValid)
            {
                return this.add_address1(address, EnumEck.Error);
            }

            address.SetOutputHidden(true);
            return View("add_address2", address);
        }

        /// <summary>
        /// 追加する別お届け先 完了画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsMypageProcessCompletion("mypage_addressinfo", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult add_address3(Add_addressInfo address)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAddAddressInsertCommand>(address), address);
            if (!ModelState.IsValid)
            {
                return this.add_address1(address, EnumEck.Error);
            }

            commandBus.Submit<IAddAddressInsertCommand>(address, EnumCommandTransaction.BeginTransaction);

            return View("add_address3", address);
        }

        /// <summary>
        /// 別お届け先 削除確認画面
        /// </summary>
        /// <param name="member"></param
        /// <returns></returns>
        [ErsMypageProcessCompletion("mypage_address_delete", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult address_delete(AddressDelete address_delete)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAddressDeleteCommand>(address_delete), address_delete);
            if (!ModelState.IsValid)
            {
                AddressList addresslist = new AddressList();
                return this.address_list(addresslist);
            }

            //該当会員の住所取得
            this.mapperBus.Map<IAddressDeleteMappable>(address_delete);

            address_delete.SetOutputHidden(true);

            return View("address_delete", address_delete);
        }

        /// <summary>
        /// 別お届け先 削除完了画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsMypageProcessCompletion("mypage_address_delete", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult address_delete2(AddressDelete address_delete)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAddressDeleteCommand>(address_delete), address_delete);
            if (!ModelState.IsValid)
            {
                return this.address_delete(address_delete);
            }

            commandBus.Submit<IAddressDeleteCommand>(address_delete, EnumCommandTransaction.BeginTransaction);

            return View("address_delete2", address_delete);
        }


        /// <summary>
        /// 確認画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        //[HttpPost]
        public virtual ActionResult point2(PointHistory point, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPointHistoryCommand>(point), point);
            if (this.IsErrorBack(eck))
            {
                this.ClearModelState(point);
                point.s_date1 = Convert.ToDateTime(DateTime.Now.AddYears(-1).ToString("yyyy/MM/dd 00:00:00"));
                point.s_date2 = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd 23:59:59"));
            }

            if (!ModelState.IsValid)
            {
                return this.point2(point, EnumEck.Error);
            }

            //Pager設定
            point.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", point.pageCnt, point.maxItemCount);

            this.mapperBus.Map<IMemberInfoMappable>(point);
            this.mapperBus.Map<IPointHistoryMappable>(point);

            point.pager.LoadPageList(point.recordCount);

            //FormタグにHiddenを自動入力
            point.SetOutputHidden(true);

            return View("point2", point);

        }

        /// <summary>
        /// 退会の確認
        /// </summary>
        /// <returns></returns>
        [ErsMypageProcessCompletion("mypage_account_close", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult account_close1(Account_close account_close)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAccountCloseCommand>(account_close), account_close);
            if (!ModelState.IsValid)
            {
                return this.user();
            }

            //会員詳細取得
            this.mapperBus.Map<IAccountCloseMappable>(account_close);

            //HIDDEN情報
            account_close.SetOutputHidden(true);

            //ビューへセット
            return View("account_close1", account_close);
        }

        /// <summary>
        /// 退会の完了
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsMypageProcessCompletion("mypage_account_close", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult account_close2(Account_close account_close)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAccountCloseCommand>(account_close), account_close);
            //パラメータチェック
            if (!ModelState.IsValid)
            {
                this.account_close1(account_close);
            }

            //会員詳細取得
            this.mapperBus.Map<IAccountCloseMappable>(account_close);

            commandBus.Submit<IAccountCloseCommand>(account_close, EnumCommandTransaction.BeginTransaction);

            //セッション初期化
            ((ISession)ErsContext.sessionState).LogOut();

            //検索処理
            var mcode = ErsContext.sessionState.Get("mcode");
            var sendmailQuit = ErsFactory.ersMailFactory.getErsSendMailQuit();
            sendmailQuit.Send(account_close, account_close.email, account_close.mformat);

            //ビューへセット
            return View("account_close2", account_close);
        }

        /// <summary>
        /// パスワードリマインダー(メアド)入力画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [NoNeedSession]
        [ErsMypageProcessCompletion("mypage_passrim", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult passrim1(Passrim passrim, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPassrimCommand>(passrim), passrim);
            if (!this.IsErrorBack(eck))
            {
                this.ClearModelState(passrim);
            }

            passrim.SetOutputHidden(true);

            return View("passrim1", passrim);
        }

        /// <summary>
        /// パスワードリマインダー完了画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [NoNeedSession]
        [HttpPost]
        [ErsMypageProcessCompletion("mypage_passrim", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult passrim2(Passrim passrim)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPassrimCommand>(passrim), passrim);
            if (!ModelState.IsValid)
            {
                return this.passrim1(passrim, EnumEck.Error);
            }

            this.mapperBus.Map<IPassrimMappable>(passrim);

            var sendmailPassrim = ErsFactory.ersMailFactory.getErsSendMailPassrim();
            sendmailPassrim.Send(passrim, passrim.email, passrim.mformat);

            return View("passrim2", passrim);
        }

        /// <summary>
        /// セキュリティアンサー入力画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [NoNeedSession]
        [ErsMypageProcessCompletion("mypage_passchange", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult passchange1(Passchange passchange, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPasschangeSessionCommand>(passchange), passchange);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    passchange.SetOutputHidden("session", true);
                    return View("passchange1", passchange);
//                    return GetErrorView();
                }
            }

            this.mapperBus.Map<IPasschangeMappable>(passchange);

            passchange.SetOutputHidden("session", true);

            return View("passchange1", passchange);
        }


        /// <summary>
        /// 新パスワード入力画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [NoNeedSession]
        [ErsMypageProcessCompletion("mypage_passchange", mode = EnumHandlingMode.CHECK)]
        public virtual ActionResult passchange2(Passchange passchange, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPasschangeSessionCommand>(passchange), passchange);
            ModelState.AddModelErrors(commandBus.Validate<IPasschangeAnswerCommand>(passchange), passchange);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    return this.passchange1(passchange, EnumEck.Error);

                }
            }

            passchange.SetOutputHidden(true);

            return View("passchange2", passchange);
        }

        /// <summary>
        /// 新パスワード完了画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [NoNeedSession]
        [HttpPost]
        [ErsMypageProcessCompletion("mypage_passchange", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult passchange3(Passchange passchange)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPasschangeSessionCommand>(passchange), passchange);
            ModelState.AddModelErrors(commandBus.Validate<IPasschangeAnswerCommand>(passchange), passchange);
            ModelState.AddModelErrors(commandBus.Validate<IPasschangeCommand>(passchange), passchange);
            if (!ModelState.IsValid)
            {
                return this.passchange2(passchange, EnumEck.Error);
            }

            //パスワード更新
            commandBus.Submit<IPasschangeCommand>(passchange, EnumCommandTransaction.BeginTransaction);

            return View("passchange3", passchange);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult logout()
        {
            ((ISession)ErsContext.sessionState).LogOut();
            return View("login");
        }


        /// <summary>
        /// カード編集画面
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        [ErsMypageProcessCompletion("mypage_card", mode = EnumHandlingMode.RESET)]
        public virtual ActionResult card1(MypageCard myPageCard, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMypageCardCommand>(myPageCard), myPageCard);
            if (!IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    this.ClearModelState(myPageCard);
                }
            }

            this.mapperBus.Map<IMypageCardMappable>(myPageCard);

            myPageCard.SetOutputHidden("input", true);

            return View("card1", myPageCard);
        }


        ///// <summary>
        ///// カード編集確認画面
        ///// </summary>
        ///// <param name="member"></param>
        ///// <returns></returns>
        [ErsMypageProcessCompletion("mypage_card", mode = EnumHandlingMode.CHECK)]
        public virtual ActionResult card2(MypageCard myPageCard)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMypageCardCommand>(myPageCard), myPageCard);
            if (!ModelState.IsValid)
            {
                return this.card1(myPageCard, EnumEck.Error);
            }

            this.mapperBus.Map<IMypageCardMappable>(myPageCard);

            //FormタグにHiddenを自動入力
            myPageCard.SetOutputHidden(true);

            return View("card2", myPageCard);
        }


        ///// <summary>
        ///// カード編集確認画面
        ///// </summary>
        ///// <param name="member"></param>
        ///// <returns></returns>
        [ErsMypageProcessCompletion("mypage_card", mode = EnumHandlingMode.COMPLETION)]
        public virtual ActionResult card3(MypageCard myPageCard)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMypageCardCommand>(myPageCard), myPageCard);
            if (!ModelState.IsValid)
            {
                return this.card1(myPageCard, EnumEck.Error);
            }

            //トランザクションは内部で管理
            commandBus.Submit<IMypageCardCommand>(myPageCard, EnumCommandTransaction.WithoutBeginTransaction);

            return View("card3", myPageCard);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult regular_bill(Regular_bill regular_bill)
        {
            if (!ModelState.IsValid)
            {
                return user();
            }

            regular_bill.nonActiveOnly = false;
            this.mapperBus.Map<IRegularBillMappable>(regular_bill);

            return View("regular_bill", regular_bill);

        }

        public virtual ActionResult regular_bill_c(Regular_bill regular_bill)
        {
            if (!ModelState.IsValid)
            {
                return user();
            }
            regular_bill.nonActiveOnly = true;
            this.mapperBus.Map<IRegularBillMappable>(regular_bill);

            return View("regular_bill_c", regular_bill);

        }
    }
}
