﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using ers.Models;
using jp.co.ivp.ers.util;
using ers.Models.cart;
using ers.Domain.Detail.Mappables;
using jp.co.ivp.ers;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ers.Domain.Cart.Mappables;
using ers.Domain.Detail.Commands;
using jp.co.ivp.ers.state;
using System.Web.Routing;

namespace ers.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsLanguageMenu]
    [ErsSideMenu]
    public class DetailController
        : ErsControllerSecure
    {
        public virtual ActionResult detail(Detail detail, List list, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IMerchandiseDetailCommand>(detail), detail);
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    return GetErrorView();
                }
            }
            //セッション情報を確認
            base.validateSession();

            this.mapperBus.Map<IMerchandiseDetailMappable>(detail);
            this.mapperBus.Map<IMerchandiseRecommendMappable>(detail);
            this.mapperBus.Map<IPriceDetailRankRecordMappable>(detail);

            if (!this.IsErrorBack(eck))
            {
                //初期値設定
                if (detail.disp_send_ptn_month_intervals)
                {
                    detail.send_ptn = EnumSendPtn.MONTH_INTERVALS;
                }
                else if (detail.disp_send_ptn_week_intervals)
                {
                    detail.send_ptn = EnumSendPtn.WEEK_INTERVALS;
                }
                else if (detail.disp_send_ptn_month_day_intervals)
                {
                    detail.send_ptn = EnumSendPtn.DAY_INTERVALS;
                }
            }

            list.SetOutputHidden(true);
            this.AddModelToView(list);

            return View("detail", detail);
        }

        public virtual ActionResult cart(Cart cart, List list)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            this.ModelState.AddModelErrors(commandBus.Validate<ICartCommand>(cart), cart);
            if (!ModelState.IsValid)
            {
                var detail = GetDetailModel();
                return this.detail(detail, list, EnumEck.Error);
            }

            //セッション情報を確認
            base.validateSession();

            //受信データに問題がない場合は、データ更新
            this.commandBus.Submit<ICartCommand>(cart, EnumCommandTransaction.BeginTransaction);

            this.mapperBus.Map<ICartMappable>(cart);

            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var search_list = new Dictionary<string, object>();
            search_list["s_keyword"] = list.s_keyword;
            search_list["s_price1"] = list.s_price1;
            search_list["s_price2"] = list.s_price2;
            search_list["sort"] = list.sort;
            search_list["s_outstock"] = list.s_outstock;
            return this.RedirectToAction(setup.sec_url, "cart", "cart", new RouteValueDictionary(search_list));
        }

        protected virtual Detail GetDetailModel()
        {
            return this.GetBindedModel<Detail>();
        }

        public virtual ActionResult viewHistory(view_history view_history)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IDetailViewHistoryCommand>(view_history), view_history);
            if (!ModelState.IsValid)            
                return GetErrorView();


            mapperBus.Map<IDetailViewHistoryMappable>(view_history);
            return View("view_history", view_history);
        }

        public virtual ActionResult daily_ranking(daily_ranking daily_ranking)
        {
            if (!ModelState.IsValid)
                return GetErrorView();


            mapperBus.Map<IDetailViewRankingMappable>(daily_ranking);
            return View("daily_ranking", daily_ranking);
        }

    }
}
