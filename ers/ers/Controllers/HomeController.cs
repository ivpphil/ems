﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using jp.co.ivp.ers.mvc;
using ers.Models.Home;
using ers.Domain.Home.Mappables;
using jp.co.ivp.ers.state;

namespace ers.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    [ErsLanguageMenu]
    public class HomeController
        : ErsControllerSecure
    {
        //
        // GET: /Home/

        public virtual ActionResult Index(Index index)
        {
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }
            //セッション情報を確認
            base.validateSession();

            this.mapperBus.Map<INewsMappable>(index);

            return View(index);
        }

        //
        // GET: /Static/
        [ValidateInput(false)]
        public ActionResult staticRooting(staticRooting staticRooting)
        {
            this.mapperBus.Map<IStaticPathMappable>(staticRooting);

            return View("staticRooting", staticRooting);
        }
    }

}
