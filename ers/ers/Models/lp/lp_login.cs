﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Login.Mappables;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class LP_Login
        : Login, ILPLoginMappable
    {
        [ErsSchemaValidation("member_t.email")]
        [DisplayName("email")]
        public string login_email { get; set; }

        [ErsSchemaValidation("member_t.passwd")]
        [DisplayName("passwd")]
        public string login_passwd { get; set; }

        [HtmlSubmitButton]
        public bool IsLandingPage { get; set; }
    }
}