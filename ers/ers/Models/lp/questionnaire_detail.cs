﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.lp;
using ers.Domain.Lp.Mappables;
using ers.Domain.Lp.Commands;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;

namespace ers.Models.lp
{
    public class Questionnaire_Detail
        : ErsBindableModel, IQuestionnaireDetailCommand
    {

        public override string lineName
        {
            get
            {
                return "アンケートリストの";
            }
        }

        public Dictionary<string, object> lp_questionnaire { get; set; }

        [ErsOutputHidden("lp_confirm")]
        [ErsSchemaValidation("d_questionnaire_t.value")]
        public string value { get; set; }

        public string item_code { get; set; }

        public string template_name { get; set; }

        public string path_template_name
        {
            get
            {
                return String.Format("lp_input/{0}.htm", this.template_name);
            }
        }

        public string item_name
        {
            get
            {
                if (this.lp_questionnaire != null)
                    return Convert.ToString(lp_questionnaire["item_name"]);

                return string.Empty;
            }
        }

        public int? is_required
        {
            get
            {
                if (this.lp_questionnaire != null)
                    return Convert.ToInt32(lp_questionnaire["is_required"]);

                return null;
            }
        }

        public string item_note
        {
            get
            {
                if (this.lp_questionnaire != null)
                    return Convert.ToString(lp_questionnaire["item_note"]);

                return string.Empty;
            }
        }

        public string error_msg { get; set; }

        public bool isError
        {
            get
            {
                return this.error_msg.HasValue();
            }
        }
    }
}