﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Lp.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using ers.Domain.Lp.Mappables;
using System.ComponentModel;

namespace ers.Models
{
    public class LP_Upsell
        : LandingPage, ILandingPageUpSellCommand, ILandingPageUpSellMappable
    {
        public LP_Upsell()
        {
            this.IsLandingPage = false;
        }

        public override bool IsOrdinaryOrder
        {
            get
            {
                if (lp_page_manage != null)
                    return lp_page_manage.upsell_order_type == EnumOrderType.Usually
                        || lp_page_manage.upsell_order_type == EnumOrderType.BothUsuallyAndSubscription;

                return false;
            }
        }

        public override bool IsRegularOrder
        {
            get
            {
                if (lp_page_manage != null)
                    return lp_page_manage.upsell_order_type == EnumOrderType.Subscription
                        || lp_page_manage.upsell_order_type == EnumOrderType.BothUsuallyAndSubscription;

                return false;
            }
        }

        public string upsell_sname { get; set; }

        public string sname { get; set; }

        public EnumLpUpsellStgy? upsell_stgy_kbn
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.upsell_stgy_kbn;

                return null;
            }
        }

        public string upsell_scode
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.upsell_scode;

                return null;
            }
        }

        public int? upsell_max_stock
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.upsell_max_stock;

                return null;
            }
        }

        public int? upsell_max_amount
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.upsell_max_amount;

                return null;
            }
        }

        public virtual int? upsell_regular_first_price
        {
            get
            {

                if (this.lp_page_manage != null)
                    return this.lp_page_manage.upsell_first_regular_price;

                return null;
            }
        }

        public virtual int? upsell_regular_price
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.upsell_regular_price;

                return null;
            }
        }

        public override bool HidePaymentArea
        {
            get
            {
                return false;
            }
        }

        public override bool HidePaymentAreaFromConfirm
        {
            get
            {
                return false;
            }
        }

        public override bool HiddenSellPaymentAmount
        {
            get
            {
                return false;
            }
        }

        public override IEnumerable<string> LpScodes
        {
            get
            {
                yield return this.upsell_scode;
                yield return this.sell_scode;
            }
        }
    }
}