﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;
using jp.co.ivp.ers.lp;
using ers.Domain.Lp.Mappables;
using ers.Models.lp;
using ers.Domain.Lp.Commands;
using jp.co.ivp.ers.order;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.basket;
using System.ComponentModel;
using ers.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;

namespace ers.Models
{
    public class LandingPage
        : Register, ILandingPageCommand, ILPMemberRegistCommand, IDQuestionnaireRegistCommand,
        ILandingPageMappable, ILPMemberMappable, ILPOrderMappable, ILandingPageTemplatePathMappable
    {
        public const string paypal_error_return_key = "paypal_return_completion";

        public bool IsLandingPage { get; set; }
        public LandingPage()
        {
            this.IsLandingPage = true;
            this.firstTime = 0;
        }

        public bool IsFromLoginPage { get; internal set; }

        public bool IsConfirmPage { get; internal set; }

        public bool IsNotConfirmationTransaction { get; internal set; }

        public bool ValidateMappedOrderData { get; internal set; }

        public bool IsNewMember { get; set; }

        public bool IsValidModel
        {
            get
            {
                if (this.controller != null && this.controller.ModelState != null)
                    return this.controller.ModelState.IsValid;

                return true;
            }
        }

        public ErsLpPageManage lp_page_manage { get; set; }

        public ErsLpTemplate objLpTemplate { get; set; }

        public ErsLpPage objLpPage { get; set; }

        public List<Dictionary<string, object>> lp_questionnaire_member_List { get; set; }

        public List<Dictionary<string, object>> lp_questionnaire_detail_List { get; set; }

        public bool IsNotLogged
        {
            get
            {
                return ((ErsSessionState)ErsContext.sessionState).getUserState() != EnumUserState.LOGIN;
            }
        }

        public virtual bool IsOrdinaryOrder
        {
            get
            {
                if (lp_page_manage != null)
                    return lp_page_manage.sell_order_type == EnumOrderType.Usually
                        || lp_page_manage.sell_order_type == EnumOrderType.BothUsuallyAndSubscription;

                return false;
            }
        }

        public virtual bool IsRegularOrder
        {
            get
            {
                if (lp_page_manage != null)
                    return lp_page_manage.sell_order_type == EnumOrderType.Subscription
                        || lp_page_manage.sell_order_type == EnumOrderType.BothUsuallyAndSubscription;

                return false;
            }
        }

        public virtual bool HasConfirmPage
        {
            get
            {
                if (this.lp_page_manage != null)
                    return ErsFactory.ersLpFactory.GetLandingPageHasConfirmPageSpec().IsSatisfiedBy(lp_page_manage);

                return true;
            }
        }

        public bool HasUpSellRegistered { get; set; }

        public bool IsNonPaymentType { get; set; }

        public bool IsNonPaymentTypeOrdinary { get; set; }

        public bool IsNonPaymentTypeRegular { get; set; }

        [ErsOutputHidden("page_ids")]
        [ErsSchemaValidation("bask_t.ransu")]
        public override string ransu { get; set; }

        [ErsOutputHidden("page_ids")]
        [ErsSchemaValidation("lp_page_manage_t.ccode")]
        public string ccode { get; set; }

        [ErsOutputHidden("page_ids")]
        [ErsSchemaValidation("lp_page_manage_t.id")]
        public int? page_id { get; set; }

        #region Member Properties"

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.lname")]
        public override string lname { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.fname")]
        public override string fname { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.lnamek")]
        public override string lnamek { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.fnamek")]
        public override string fnamek { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.email")]
        public override string email { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.email")]
        public override string email_confirm { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.tel")]
        public override string tel { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.fax")]
        public override string fax { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.zip")]
        public override string zip { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.pref")]
        public override int? pref { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.address")]
        public override string address { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.taddress")]
        public override string taddress { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.maddress")]
        public override string maddress { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.passwd")]
        public override string passwd { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.passwd")]
        public override string passwd_confirm { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.ques")]
        public override int? ques { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.ans")]
        public override string ans { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = ErsViewBirthdayService.minimumOfSelectyear, rangeTo = ErsViewBirthdayService.maximumOfSelectyear)]
        public override int? birthday_y { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 12)]
        public override int? birthday_m { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 31)]
        public override int? birthday_d { get; set; }

        public override DateTime? birth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetBirthDay(this.birthday_y, this.birthday_m, this.birthday_d);
            }
        }

        #endregion

        #region "Pay And Amount Properties"

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.pay")]
        public override EnumPaymentType? pay { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 100)]
        public override string card_holder_name { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public override int? card { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(rangeFrom = 13, rangeTo = 16, type = CHK_TYPE.NumericString)]
        public override string cardno { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(rangeFrom = 3, rangeTo = 3, type = CHK_TYPE.NumericString)]
        public override string securityno { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(rangeFrom = 4, rangeTo = 4, type = CHK_TYPE.NumericString)]
        public override int? validity_y { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 12, type = CHK_TYPE.Numeric)]
        public override int? validity_m { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_card_t.id")]
        public override int? card_id { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public override EnumCardSave card_save { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.conv_code")]
        public override EnumConvCode? conv_code { get; set; }

        [ErsOutputHidden("lp_confirm")]
        [ErsSchemaValidation("bask_t.amount")]
        [DisplayName("upsell_amount")]
        public int? upsell_amount { get; set; }

        [ErsOutputHidden("lp_confirm")]
        [ErsSchemaValidation("bask_t.amount")]
        public virtual int? amount { get; set; }

        #endregion

        [BindTable("questionnaireDetailItems")]
        public List<Questionnaire_Detail> questionnaireDetailItems { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 1)]
        public override int? pri_chk { get; set; }

        [ErsOutputHidden("lp_confirm")]
        [ErsSchemaValidation("regular_detail_t.send_ptn")]
        public virtual EnumSendPtn? send_ptn { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public int? firstTime { get; set; }

        public Dictionary<string, object> item_code_name { get; set; }
        public Dictionary<string, object> item_code_lname { get; set; }
        public Dictionary<string, object> item_code_email { get; set; }
        public Dictionary<string, object> item_code_email_confirm { get; set; }
        public Dictionary<string, object> item_code_tel { get; set; }
        public Dictionary<string, object> item_code_fax { get; set; }
        public Dictionary<string, object> item_code_zip { get; set; }
        public Dictionary<string, object> item_code_pref { get; set; }
        public Dictionary<string, object> item_code_address { get; set; }
        public Dictionary<string, object> item_code_taddress { get; set; }
        public Dictionary<string, object> item_code_maddress { get; set; }
        public Dictionary<string, object> item_code_birth { get; set; }
        public Dictionary<string, object> item_code_monitor { get; set; }
        public Dictionary<string, object> item_code_sex { get;set; }

        public bool disp_send_ptn_month_intervals { get; set; }

        public bool disp_send_ptn_week_intervals { get; set; }

        public bool disp_send_ptn_month_day_intervals { get; set; }

        #region REGULAR ORDER INTERVAL LIST
        //fields for regular order
        public List<Dictionary<string, object>> ListPtnIntervalDay
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalDay, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ListPtnIntervalMonth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalMonth, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ListPtnDay
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnDay, EnumCommonNameColumnName.namename);
            }
        }

        public List<DateTime> ListRegularSenddateMonth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsRegularOrderViewService().GetListRegularSenddateMonth();
            }
        }

        public List<Dictionary<string, object>> ListPtnIntervalWeek
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalWeek, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ListPtnWeekday
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename);
            }
        }

        #endregion

        public string lp_ccode
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.ccode;

                return null;
            }
        }        

        public EnumUse? personal_info_kbn
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.personal_info_kbn;

                return null;
            }
        }

        public EnumLpBasicStgy? basic_stgy_kbn
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.basic_stgy_kbn;

                return null;
            }
        }

        public string personal_info_url
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.personal_info_url;

                return null;
            }
        }

        #region CARRIAGE FLG AND FREE PRICE
        public virtual EnumUse? lp_carriage_free_flg
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.carriage_free_flg;

                return null;
            }
        }

        public virtual bool carriage_free_flg
        {
            get
            {
                return this.lp_carriage_free_flg == EnumUse.Use;
            }
        }

        public int? lp_carriage_free_price
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.carriage_free_price;

                return 0;
            }
        }
        #endregion

        public EnumLpBuyLimit? buy_limit_kbn
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.buy_limit_kbn;

                return null;
            }
        }

        public string sell_scode
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.sell_scode;

                return string.Empty;
            }
        }

        public int? sell_max_stock
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.sell_max_stock;

                return null;
            }
        }

        public int? sell_max_amount
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.sell_max_amount;

                return null;
            }
        }
        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.sell_price")]
        public int? sell_price
        {
            get; set;
        }

            /// <summary>
            /// return upsell_max_amount for upsell item
            /// </summary>
            public int? upsellMax_amount
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.upsell_max_amount;

                return null;
            }
        }

        public virtual int? regular_first_price
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.sell_first_regular_price;

                return null;
            }
        }

        public virtual int? regular_price
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.sell_regular_price;

                return null;
            }
        }

        public string lp_coupon_code
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.coupon_code;

                return string.Empty;
            }
        }

        public override bool coupon_flg
        {
            get
            {
                if (this.lp_page_manage != null)
                    return (this.lp_page_manage.coupon_flg == EnumUse.Use);

                return base.coupon_flg;
            }
        }

        public int lp_coupon_discount
        {
            get
            {
                if (lp_coupon_code.HasValue() && coupon_flg)
                {
                    var couponObj = ErsFactory.ersCouponFactory.GetErsCouponWithCouponCode(lp_coupon_code);
                    if (couponObj != null)
                        return (int)couponObj.price;
                }

                return 0;
            }
        }

        public int total_all_item
        {
            get
            {
                if (this.cart != null)
                {
                    return this.cart.total_all_item;
                }

                return 0;
            }
        }

        #region Landing View Page Information

        public string page_title
        {
            get
            {
                if (this.lp_page_manage != null)
                    return lp_page_manage.page_title;

                return string.Empty;
            }
        }

        public string logo_image_file
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.logo_image_file;

                return string.Empty;
            }
        }

        public virtual string template_file_path
        {
            get
            {
                if (this.objLpTemplate != null)
                    return String.Format("lp_template/{0}", objLpTemplate.template_file_path);

                return string.Empty;
            }
        }

        public EnumLpPageTypeCode lp_page_type_code { get; set; }
        #endregion

        public override EnumSendTo? send
        {
            get
            {
                return EnumSendTo.MEMBER_ADDRESS;
            }
        }

        public override EnumMformat? mformat
        {
            get
            {
                return EnumMformat.PC;
            }
        }

        public override string w_pref
        {
            get
            {
                if (this.pref == null)
                {
                    return string.Empty;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(this.pref);
            }
        }

        public override bool canSelectMailDelv
        {
            get
            {
                return _canSelectMailDelv;
            }
        }
        public bool _canSelectMailDelv { internal get; set; }

        public override IEnumerable<Dictionary<string, object>> payList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPayService().SelectAsList(!this.IsOrdinaryOrder && this.IsRegularOrder); }
        }

        public virtual IEnumerable<string> LpScodes
        {
            get
            {
                yield return this.sell_scode;
            }
        }

        public bool HasQuestionnaires
        {
            get
            {
                return this.questionnaireDetailItems != null && this.questionnaireDetailItems.Count > 0;
            }
        }

        public virtual bool HidePaymentArea
        {
            get
            {
                return (this.IsNonPaymentType && this.sell_max_amount.HasValue && this.sell_max_amount == 1);
            }
        }

        public virtual bool HidePaymentAreaFromConfirm
        {
            get
            {
                return this.IsNonPaymentType && this.IsLandingPage;
            }
        }

        public virtual bool HiddenSellPaymentAmount
        {
            get
            {
                return (this.sell_max_amount.HasValue && this.sell_max_amount == 1);
            }
        }
       
        /// <summary>
        /// Return true if lp_page_manage_t.sell_max_amount or lp_page_manage_t.upsell_max_amount are greater than 1        
        /// </summary>
        public virtual bool Hidden_Sell_Upsell_PaymentAmount
        {
            get
            {
                return (this.sell_max_amount.HasValue && this.sell_max_amount != 1) || (this.upsellMax_amount.HasValue && this.upsellMax_amount != 1);
            }
        }
       
        /// <summary>
        /// Return true if lp_page_manage_t.upsell_max_amount is 1
        /// </summary>
        public virtual bool HiddenUpSellPaymentAmount
        {
            get
            {
                return (this.upsellMax_amount.HasValue && this.upsellMax_amount == 1);
            }
        }

        public bool IsOutOfStock { get; set; }


        public string upsell_button_1_file_name
        {
            get
            {
                if (this.objLpPage != null)
                    return this.objLpPage.upsell_button_1_file_name;
                return null;
            }
        }
        public string upsell_button_2_file_name
        {
            get
            {
                if (this.objLpPage != null)
                    return this.objLpPage.upsell_button_2_file_name;
                return null;
            }
        }
        public string upsell_button_3_file_name
        {
            get
            {
                if (this.objLpPage != null)
                    return this.objLpPage.upsell_button_3_file_name;
                return null;
            }
        }
    }
}