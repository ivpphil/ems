﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using ers.Domain.Member.Commands;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class Add_addressInfo : ErsFrontModelBase, IAddAddressInsertCommand, IAddAddressInsertMappable
    {
        [HtmlSubmitButton]
        public bool submit_btn1 { get; set; }

        [HtmlSubmitButton]
        public bool submit_btn2 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.id")]
        public int? id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.address_name")]
        public string address_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_lname")]
        public string add_lname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_fname")]
        public string add_fname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_lnamek")]
        public string add_lnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_fnamek")]
        public string add_fnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_compname")]
        public string add_compname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_compnamek")]
        public string add_compnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_zip")]
        public string add_zip { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_pref")]
        public int? add_pref { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_address")]
        public string add_address { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_taddress")]
        public string add_taddress { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_maddress")]
        public string add_maddress { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_tel")]
        public string add_tel { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_fax")]
        public string add_fax { get; set; }

        //都道府県プルダウン
        public List<Dictionary<string, object>> prefList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(); }
        }

        //都道府県表示用 
        public string w_add_pref
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(add_pref);
            }
        }
    }
}