﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using ers.Domain.Member.Mappables;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models.member
{
    public class Regular_bill
        : ErsFrontModelBase, IRegularBillMappable
    {
        public IEnumerable<Dictionary<string, object>> orderRecordList { get; set; }
        public bool nonActiveOnly { get; set; }
    }
}