﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.member;
using System.Web.Mvc;
using System.Data;
using jp.co.ivp.ers;
using ers.Domain.Member.Commands;
using ers.Domain.Member.Mappables;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class Mail
        : ErsFrontModelBase, IMailUpdateCommand, IMailUpdateMappable, IMemberInfoMappable
    {
        public string lname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.m_flg")]
        public EnumMFlg? m_flg { get; set; }

        [HtmlSubmitButton]
        public virtual bool submit_btn1 { get; set; }

        [HtmlSubmitButton]
        public virtual bool submit_btn2 { get; set; }

        public List<Dictionary<string, object>> m_flgList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MFlg, EnumCommonNameColumnName.namename); }
        }

        public string w_m_flg
        {
            get
            {
                if (!this.m_flg.HasValue)
                    return null;

                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.MFlg, EnumCommonNameColumnName.namename, (int)this.m_flg.Value);
            }
        }
    }
}