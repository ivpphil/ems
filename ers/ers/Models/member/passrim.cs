﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.sendmail;
using ers.Domain.Member.Commands;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class Passrim
        : ErsFrontModelBase, IPassrimCommand, IPassrimMappable
    {
        [ErsSchemaValidation("member_t.lname")]
        public string lname { get; set; }

        [ErsSchemaValidation("member_t.fname")]
        public string fname { get; set; }

        [ErsSchemaValidation("member_t.email")]
        public string email { get; set; }

        public EnumMformat? mformat { get; set; }

        public string mcode { get; set; }

        public string changeUrl { get; set; }

        //VEXフラグ
        public Boolean onVex { get {return ErsFactory.ersUtilityFactory.getSetup().onVEX; } }

        [HtmlSubmitButton]
        public virtual bool submit_btn { get; set; }
    }
}