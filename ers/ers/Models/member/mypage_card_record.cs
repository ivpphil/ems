﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using ers.Domain.Member.Commands;
using System.ComponentModel;

namespace ers.Models.member
{
    public class MypageCardRecord
        : ErsBindableModel, IMypageCardRecordCommand
    {
        public override string lineName
        {
            get
            {
                return string.Empty;
            }
        }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("member_card_t.id")]
        public int? card_id { get; set; }


        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 100)]
        public string saved_card_holder_name { get; set; }

        [DisplayName("common_undefined")]
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 100)]
        public string saved_card_name { get; set; }

        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 99, type = CHK_TYPE.Numeric)]
        public int? saved_card_type { get; set; }

        [ErsUniversalValidation(rangeFrom = 13, rangeTo = 16, type = CHK_TYPE.NumericString)]
        public string saved_card_no { get; set; }

        [ErsUniversalValidation(rangeFrom = 4, rangeTo = 4, type = CHK_TYPE.NumericString)]
        public int? saved_validity_y { get; set; }

        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 12, type = CHK_TYPE.Numeric)]
        public int? saved_validity_m { get; set; }


        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 100)]
        public string card_holder_name { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 99, type = CHK_TYPE.Numeric)]
        public int? card_type { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(rangeFrom = 13, rangeTo = 16, type = CHK_TYPE.NumericString)]
        public string card_no { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(rangeFrom = 4, rangeTo = 4, type = CHK_TYPE.NumericString)]
        public int? validity_y { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 12, type = CHK_TYPE.Numeric)]
        public int? validity_m { get; set; }
    }
}