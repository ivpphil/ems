﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.member.strategy;
using ers.Domain.Member.Commands;
using ers.Domain.Member.Mappables;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class Passchange
        : ErsFrontModelBase, IPasschangeCommand, IPasschangeMappable, IPasschangeAnswerCommand, IPasschangeSessionCommand
    {

        [HtmlSubmitButton]
        public bool submit_ans { get; set; }

        [HtmlSubmitButton]
        public bool submit_passwd { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.ans")]
        public string ans { get; set; }

        [ErsSchemaValidation("member_t.passwd")]
        public string passwd { get; set; }

        [ErsSchemaValidation("member_t.passwd")]
        public string passwd_confirm { get; set; }

        [ErsOutputHidden("session")]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 200)]
        public string enc_mcode { get; set; }

        [ErsOutputHidden("session")]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 200)]
        public string enc_ransu { get; set; }

        public string w_ques { get; set; }
    }
}