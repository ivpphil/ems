﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers;
using ers.Models.member;
using ers.Domain.Member.Commands;
using ers.Domain.Member.Mappables;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class Account_close
        : ErsFrontModelBase, IAccountCloseCommand, IAccountCloseMappable
    {

        // 性
        public string lname { get; set; }

        // 名
        public string fname { get; set; }

        // 性カナ
        public string lnamek { get; set; }

        // 名カナ
        public string fnamek { get; set; }

        // 会社名
        public string compname { get; set; }

        // 会社名カナ
        public string compnamek { get; set; }

        // 部署名
        public string division { get; set; }

        // 部署名カナ
        public string divisionk { get; set; }

        // 担当者名姓
        public string tlname { get; set; }

        // 担当者名名
        public string tfname { get; set; }

        // 担当者名姓カナ
        public string tlnamek { get; set; }

        // 担当者名名カナ
        public string tfnamek { get; set; }

        // メールアドレス
        public string email { get; set; }

        // メールアドレス
        public EnumMformat? mformat { get; set; }

        // 郵便番号
        public string zip { get; set; }

        // 都道府県
        public int? pref { get; set; }

        // 市区町村
        public string address { get; set; }

        // 番地、アパート、マンション名
        public string taddress { get; set; }

        // 上記以降の住所
        public string maddress { get; set; }

        // 電話番号
        public string tel { get; set; }

        // FAX番号
        public string fax { get; set; }

        // 職業
        public int? job { get; set; }

        // 性別
        public int? sex { get; set; }

        // 性別
        public string w_sex
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename, sex);
            }
        }

        //誕生日
        public DateTime? birth { get; set; }

        public string w_birth
        {
            get
            {
                if (this.birth.HasValue)
                {
                    return this.birth.Value.ToString("yyyy/MM/dd");
                }
                return string.Empty;
            }
        }

        public string w_pref
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(this.pref);
            }
        }

        public string w_job
        {
            get
            {
                    return ErsFactory.ersViewServiceFactory.GetErsViewJobService().GetStringFromId(this.job);
            }
        }

        [HtmlSubmitButton]
        public virtual bool close_btn { get; set; }
    }
}