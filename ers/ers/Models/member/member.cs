﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.member.strategy;
using jp.co.ivp.ers.state;
using System.Text;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers;
using ers.Domain.Member.Commands;
using ers.Domain.Member.Mappables;
using ers.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;

namespace ers.Models
{
    public class Member
        : ErsFrontModelBase, IMemberUpdateCommand, IMemberUpdateMappable
    {

        public ErsMember member { get; set; }

        public bool IsConfirmationPage { get; internal set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.email")]
        public virtual string email { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.email")]
        public virtual string email_confirm { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.mformat")]
        public EnumMformat? mformat { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.passwd")]
        public virtual string passwd { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.passwd")]
        public string passwd_confirm { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.lname")]
        public virtual string lname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fname")]
        public virtual string fname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.lnamek")]
        public virtual string lnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fnamek")]
        public virtual string fnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.compname")]
        public virtual string compname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.compnamek")]
        public virtual string compnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.division")]
        public virtual string division { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.divisionk")]
        public virtual string divisionk { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tlname")]
        public virtual string tlname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tfname")]
        public virtual string tfname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tlnamek")]
        public virtual string tlnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tfnamek")]
        public virtual string tfnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.zip")]
        public virtual string zip { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.pref")]
        public virtual int? pref { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.address")]
        public virtual string address { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.taddress")]
        public virtual string taddress { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.maddress")]
        public virtual string maddress { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = ErsViewBirthdayService.minimumOfSelectyear, rangeTo = ErsViewBirthdayService.maximumOfSelectyear)]
        public int? birthday_y { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 12)]
        public int? birthday_m { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 31)]
        public int? birthday_d { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.sex")]
        public virtual EnumSex? sex { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.job")]
        public virtual int? job { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tel")]
        public virtual string tel { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fax")]
        public virtual string fax { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.ques")]
        public virtual int? ques { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.ans")]
        public virtual string ans { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.memo")]
        public virtual string memo { get; set; }

        public DateTime? birth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetBirthDay(this.birthday_y, this.birthday_m, this.birthday_d);
            }
        }

        public string w_sex
        {
            get
            {
                if (!this.sex.HasValue)
                {
                    return null;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename, (int)this.sex.Value);
            }
        }

        public string w_job
        {
            get
            {
                if (member == null || member.job == null)
                    return string.Empty;
                else
                    return ErsFactory.ersViewServiceFactory.GetErsViewJobService().GetStringFromId(member.job);
            }
        }

        //秘密の質問表示表示用
        public string w_ques
        {
            get
            {
                if (member == null)
                    return string.Empty;
                else
                    return ErsFactory.ersViewServiceFactory.GetErsViewQuesService().GetStringFromId(member.ques);
            }
        }

        public string w_pref
        {
            get
            {
                if (member == null)
                    return string.Empty;

                return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(member.pref);
            }
        }

        public string w_mformat
        {
            get
            {
                if (!this.mformat.HasValue)
                    return null;

                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.MFormat, EnumCommonNameColumnName.namename, (int)this.mformat.Value);
            }
        }

        public int tgt_user_born
        {
            get { return ErsFactory.ersUtilityFactory.getSetup().tgt_user_born; }
        }


        public List<Dictionary<string, object>> prefList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> jobList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewJobService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> birthday_yList
        {
            get
            {
                var min_user_age = ErsFactory.ersUtilityFactory.getSetup().min_user_age;
                var max_user_age = ErsFactory.ersUtilityFactory.getSetup().max_user_age;
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListYear(max_user_age, min_user_age);
            }
        }

        public List<Dictionary<string, object>> birthday_mList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListMonth(); }
        }

        public List<Dictionary<string, object>> birthday_dList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListDay(); }
        }

        //セキュリティアンサープルダウン
        public List<Dictionary<string, object>> quesList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewQuesService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> cardList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCardService().SelectAsList(); }
        }

        public List<Dictionary<string,object>> mformatList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MFormat, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> sexList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> m_flgList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MFlg, EnumCommonNameColumnName.namename); }
        }
    }
}