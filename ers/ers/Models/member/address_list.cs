﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using ers.Domain.Member.Commands;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class AddressList
        : ErsFrontModelBase, IAddressListMappable
    {
        public ErsPagerModel pager { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }
        
        /// <summary>
        /// Get number of item on a page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        public long recordCount { get; set; }

        public List<Dictionary<string, object>> list { get; set; }
    }
}