﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.order;
using System.Web.Mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;
using ers.Domain.Member.Commands;
using ers.Domain.Member.Mappables;
using ers.Models.member;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class MypageCard
        : ErsFrontModelBase, IMypageCardCommand, IMypageCardMappable
    {
        [BindTable("listCardInfo")]
        public List<MypageCardRecord> listCardInfo { get; set; }

        public string mcode { get { return ErsContext.sessionState.Get("mcode"); } }

        //カード入力フォーム表示フラグ
        private int entryCardInfoCount { get { return ErsFactory.ersUtilityFactory.getSetup().entryCardInfoCount; } }

        /// 登録sumitがおされた場合
        /// </summary>
        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool entry_submit { get; set; }

        /// 登録sumitがおされた場合
        /// </summary>
        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool modify_submit { get; set; }

        /// 削除sumitがおされた場合
        /// </summary>
        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool delete_submit { get; set; }

        //カード預け番号
        [ErsOutputHidden]
        [ErsSchemaValidation("member_card_t.id")]
        public int? card_id { get; set; }

        //カード名義
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 100)]
        public string card_holder_name { get; set; }

        //カード種類名
        [ErsOutputHidden]
        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 99,  type = CHK_TYPE.Numeric)]
        public int? card_type { get; set; }

        //カード番号
        [ErsOutputHidden]
        [ErsUniversalValidation(rangeFrom = 13, rangeTo = 16, type = CHK_TYPE.NumericString)]
        public string card_no { get; set; }

        //カード番号　*表示用
        [ErsOutputHidden]
        [ErsUniversalValidation(rangeFrom = 13, rangeTo = 16, type = CHK_TYPE.All )]
        public string w_cardno { get; set; }

        //有効期限
        [ErsOutputHidden]
        [ErsUniversalValidation(rangeFrom = 4, rangeTo = 4, type = CHK_TYPE.NumericString)]
        public int? validity_y { get; set; }

        //有効期限
        [ErsOutputHidden]
        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 12, type = CHK_TYPE.Numeric)]
        public int? validity_m { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public virtual EnumCardSave? card_save { get; set; }

        //登録フォーム表示フラグ
        public bool newEntryFlg { get { return this.listCardInfo == null || (this.listCardInfo.Count < entryCardInfoCount); } }

        //カードプルダウンリスト生成
        public List<Dictionary<string, object>> cardList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCardService().SelectAsList(); }
        }


        public string confirm_card_holder_name { get; set; }

        public string confirm_card_type { get; set; }

        public string confirm_cardno { get; set; }

        public int? confirm_validity_y { get; set; }

        public int? confirm_validity_m { get; set; }
    }
}