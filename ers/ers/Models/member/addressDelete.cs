﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order;
using ers.Domain.Member.Commands;
using ers.Domain.Member.Mappables;
using jp.co.ivp.ers;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{

    public class AddressDelete
        : ErsFrontModelBase, IAddressDeleteCommand, IAddressDeleteMappable
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.id")]
        public int? id { get; set; }

        public string address_name { get; set; }

        public string add_lname { get; set; }

        public string add_fname { get; set; }

        public string add_lnamek { get; set; }

        public string add_fnamek { get; set; }

        public string add_compname { get; set; }

        public string add_compnamek { get; set; }

        public string add_zip { get; set; }

        public int? add_pref { get; set; }

        public string add_address { get; set; }

        public string add_taddress { get; set; }

        public string add_maddress { get; set; }

        public string add_tel { get; set; }

        public string add_fax { get; set; }

        [HtmlSubmitButton]
        public bool regist_submit { set; get; }

        //都道府県表示用 
        public string w_add_pref
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(add_pref);
            }
        }
    }
}