﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using ers.Domain.Member.Mappables;
using ers.Domain.Member.Commands;
using jp.co.ivp.ers;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class PointHistory
        : ErsFrontModelBase, IMemberInfoMappable, IPointHistoryCommand, IPointHistoryMappable
    {

        /// <summary>
        /// Get number of item on a page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        public ErsPagerModel pager { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? s_date1 { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? s_date2 { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation()]
        public bool pointMode { get { return true; } }

        public long recordCount { get; set; }

        public List<Dictionary<string, object>> list { get; set; }

        public string lname { get; set; }
    }
}
