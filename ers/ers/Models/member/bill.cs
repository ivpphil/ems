﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers;
using ers.Domain.Member.Mappables;
using ers.Domain.Member.Commands;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class Bill
        : ErsFrontModelBase, IBillDetailCommand, IBillDetailMappable
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("d_master_t.d_no")]
        public string d_no
        {
            get
            {
                return _d_no;
            }
            set
            {
                //Formatting the d_no value since a hyphen is removed at validation of DataAnnotation.
                if (!string.IsNullOrEmpty(value) && value.Length > 8 && !value.Contains('-'))
                {
                    _d_no = value.Substring(0, 8) + "-" + value.Substring(8);
                }
                else
                {
                    _d_no = value;
                }
            }
        }
        private string _d_no;

        public string mcode { get { return ErsContext.sessionState.Get("mcode"); } }

        public IEnumerable<Dictionary<string, object>> orderRecords { get; set; }

        public DateTime? indate { get; set; }

        public EnumSendTo send { get; set; }

        public string zip { get; set; }

        public string w_pref { get; set; }

        public string address { get; set; }

        public string taddress { get; set; }

        public string maddress { get; set; }

        public string tel { get; set; }

        public DateTime? senddate { get; set; }

        public string w_senddate_no_select
        {
            get
            {
                if (senddate == null)
                    return ErsResources.GetFieldName("senddate_no_select");

                return string.Empty;
            }
        }

        public string w_sendtime { get; set; }

        public string w_payment_order_status { get; set; }

        public EnumPaymentType? pay { get; set; }

        public string w_pay { get; set; }

        public string usr_memo { get; set; }

        public string delivery { get; set; }

        public string url { get; set; }

        public int amounttotal { get; set; }

        public int? subtotal { get; set; }

        public int? tax { get; set; }

        public int? carriage { get; set; }

        public int? etc { get; set; }

        public string w_etc { get; set; }

        public int? p_service { get; set; }

        public int? coupon_discount { get; set; }

        public int? total { get; set; }

        public string lname { get; set; }

        public string fname { get;  set; }

        public string w_order_status { get; set; }
        public string w_order_status2 { get; set; }
        public IEnumerable<Dictionary<string, string>> sendnos { get; set; }
        public string sendno { get; set; }

        public string w_wrap { get; set; }

        public string memo2 { get; set; }

        public string w_conv_code { get; set; }
    }
}