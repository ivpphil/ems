﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ers.Domain.Api.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;

namespace ers.Models.api
{
    public class GmoResultReceive
        : ErsModelBase, IGmoResultReceiveCommand
    {
        /// <summary>
        /// CHAR 13 ショップID
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 13)]
        public string ShopID { get; set; }

        /// <summary>
        /// CHAR 10 ショップパスワード “*” 10桁固定
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 10)]
        public string ShopPass { get; set; }

        /// <summary>
        /// CHAR 32 取引ID
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 32)]
        public string AccessID { get; set; }

        /// <summary>
        /// CHAR 32 取引パスワード “*” 32桁固定
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 32)]
        public string AccessPass { get; set; }

        /// <summary>
        /// CHAR 27 オーダーID
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 27)]
        public string OrderID { get; set; }

        /// <summary>
        /// CHAR - 現状態
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public EnumGmoResultStatus? Status { get; set; }

        /// <summary>
        /// CHAR - 処理区分
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public EnumGmoJobCd? JobCd { get; set; }

        /// <summary>
        /// NUMBER 10 利用金額
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? Amount { get; set; }

        /// <summary>
        /// NUMBER 10 税送料
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        [DisplayName("shipping_tax")]
        public int? Tax { get; set; }

        /// <summary>
        /// CHAR 3 通貨コード決済に利用された通貨を返却します。
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 3)]
        public string Currency { get; set; }

        /// <summary>
        /// CHAR 7 仕向先会社コードカード・ｉＤネット決済時のみ返却
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 7)]
        public string Forward { get; set; }

        /// <summary>
        /// CHAR 1 支払方法
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 1)]
        public EnumGmoMethod? Method { get; set; }

        /// <summary>
        /// NUMBER 2 支払回数カード・ｉＤネット決済時のみ返却
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? PayTimes { get; set; }

        /// <summary>
        /// CHAR 28 トランザクションID
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 28)]
        public string TranID { get; set; }

        /// <summary>
        /// CHAR 7 承認番号カード・ｉＤネット決済時のみ返却
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 7)]
        public string Approve { get; set; }

        /// <summary>
        /// CHAR 14 処理日付 yyyyMMddHHmmss書式
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 14)]
        public string TranDate { get; set; }

        /// <summary>
        /// CHAR 3 エラーコードエラー発生時のみ ※2
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 3)]
        public string ErrCode { get; set; }

        /// <summary>
        /// CHAR 9 エラー詳細コードエラー発生時のみ ※2
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 9)]
        public string ErrInfo { get; set; }

        /// <summary>
        /// CHAR 1 決済方法
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 1)]
        public EnumGmoPayType? PayType { get; set; }

        /// <summary>
        /// CHAR 5 支払先コンビニコード
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 5)]
        public EnumConvCode? CvsCode { get; set; }

        /// <summary>
        /// CHAR 20 コンビニ確認番号
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 20)]
        public string CvsConfNo { get; set; }

        /// <summary>
        /// CHAR 32 コンビニ確認番号
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 32)]
        public string CvsReceiptNo { get; set; }

        /// <summary>
        /// CHAR 16 Edy受付番号
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 16)]
        public string EdyReceiptNo { get; set; }

        /// <summary>
        /// CHAR 40 Edy注文番号
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 40)]
        public string EdyOrderNo { get; set; }

        /// <summary>
        /// CHAR 9 Suica受付番号
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 9)]
        public string SuicaReceiptNo { get; set; }

        /// <summary>
        /// CHAR 40 Suica注文番号
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 40)]
        public string SuicaOrderNo { get; set; }

        /// <summary>
        /// CHAR 11 Pay-easyお客様番号
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 11)]
        public string CustID { get; set; }

        /// <summary>
        /// CHAR 5 Pay-easy収納機関番号
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 5)]
        public string BkCode { get; set; }

        /// <summary>
        /// CHAR 20 Pay-easy確認番号
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 20)]
        public string ConfNo { get; set; }

        /// <summary>
        /// CHAR 14 Pay-easy支払期限日時(yyyyMMddHHmmss書式)
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 14)]
        public string PaymentTerm { get; set; }

        /// <summary>
        /// CHAR 128 Pay-easy暗号化決済番号
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 128)]
        public string EncryptReceiptNo { get; set; }

        /// <summary>
        /// CHAR 14 入金確定日時(yyyyMMddHHmmss書式)(モバイルSuica・Edy・コンビニ・Pay-easy)
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 14)]
        public string FinishDate { get; set; }

        /// <summary>
        /// CHAR 14 受付日時(yyyyMMddHHmmss書式)(モバイルSuica・Edy・コンビニ・Pay-easy)
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 14)]
        public string ReceiptDate { get; set; }

        /// <summary>
        /// CHAR 16 購入に使用されたWebMoneyの管理番号
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 16)]
        public string WebMoneyManagementNo { get; set; }

        /// <summary>
        /// CHAR 25 WebMoneyセンターが返却した決済コード
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 25)]
        public string WebMoneySettleCode { get; set; }
    }
}