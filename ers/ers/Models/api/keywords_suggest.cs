﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using ers.Domain.Api.Mappables;

namespace ers.Models.api
{
    public class KeywordsSuggest
        : ErsModelBase, IKeywordsSuggestMappable
    {
        [ErsSchemaValidation("keywords_t.keyword")]
        public string keyword { get; set; }

        public IEnumerable<string> ListSuggest { get; set; }
    }
}