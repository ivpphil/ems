﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.db;
using ers.Domain.Home.Mappables;
using ers.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Models
{
    public class User
        : ErsFrontModelBase, INewsMappable
    {
        public List<Dictionary<string, object>> news_list { get; set; }

        public string contents_code
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();

                if (setup.site_id == 1)
                {
                    return "10000002";
                }
                else
                {
                    return "20000002";
                }
            }
        }
    }
}