﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using ers.Domain.Home.Mappables;
using ers.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util.mobile;
using jp.co.ivp.ers;

namespace ers.Models.Home
{
    public class Index
        : ErsFrontModelBase, INewsMappable
    {
        public List<Dictionary<string, object>> news_list { get; set; }

        public virtual string contents_code
        {
            get
            {
                var site_type = new ErsMobileCommon().GetSiteType();
                if(site_type == EnumSiteType.SMARTPHONE)
                {
                    return "10000004";
                }


                return "10000001";
            }
        }
    }
}