﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Home.Mappables;
using ers.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ers.Models.Home
{
    public class staticRooting
        : ErsFrontModelBase, IStaticPathMappable
	{
        [ErsUniversalValidation(type = CHK_TYPE.WebAddress, rangeFrom = 0, rangeTo = 255)]
        public string staticPath { get; set; }

        public string partialPath { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.WebAddress, rangeFrom = 0, rangeTo = 255)]
        public virtual string staticPages { get; set; }
    }
}