﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise.category;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ers.Models
{
    public class sideMenu
        : ErsModelBase
    {
        //ログイン情報email（クッキー保存版）
        public string cookie_email { get; set; }
        //クッキー保存チェック
        public Boolean email_chk { get; set; }

        public List<Dictionary<string, object>> categoryList
        {
            get
            {
                var repository = ErsFactory.ersMerchandiseFactory.GetErsCategoryRepository(ErsCategory.minCategoryNumber);
                var c = ErsFactory.ersMerchandiseFactory.GetErsCategoryCriteria(ErsCategory.minCategoryNumber);
                c.active = EnumActive.Active;
                c.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
                c.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

                var categories = repository.Find(c);

                var retVal = new List<Dictionary<string, object>>();
                foreach (var category in categories)
                {
                    var cateItem = new Dictionary<string, object>();
                    cateItem["id"] = category.id;
                    cateItem["cate_name"] = category.cate_name;
                    retVal.Add(cateItem);
                }
                return retVal;
            }
        }
    }
}