﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.contents;
using ers.Domain.Cms.Mappables;
using ers.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ers.Domain.Cms.Commands;

namespace ers.Models
{
    public class news_login
        : ErsFrontModelBase
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("news_article_t.article_code")]
        public virtual string article_code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("news_article_t.contents_code")]
        public virtual string contents_code { get; set; }

        public virtual EnumRequired? req_login_flg { get; set; }
    }
}