﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ers.Domain.Cms.Mappables;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class news_list
        : news_login, INewsListMappable
    {
        public ErsPagerModel pager { get; internal set; }

        public long recordCount { get; set; }

        public List<Dictionary<string, object>> newsList { get; set; }

        public string contents_name { get; set; }

        public int id { get; protected set; }

        public string template_code { get; protected set; }

        public string title { get; protected set; }

        public string posted_date { get; protected set; }

        public virtual long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        public long maxRecCnt { get; protected set; }

        public long minRecCnt { get; protected set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }
    }
}