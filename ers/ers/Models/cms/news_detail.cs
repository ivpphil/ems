﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.contents;
using ers.Domain.Cms.Mappables;
using ers.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ers.Domain.Cms.Commands;

namespace ers.Models
{
    public class news_detail
        : news_login, INewsDetailMappable, INewsDetailPreviewMappable, INewsDetailCommand, INewsDetailPreviewCommand
    {
        public List<Dictionary<string, object>> linkList { get; set; }

        public List<Dictionary<string, object>> fileList { get; set; }

        public int linkListCount { get; set; }

        public int fileListCount { get; set; }

        public string img_file_name1 { get; set; }

        public string img_file_name2 { get; set; }

        public string img_file_name3 { get; set; }

        public string img_file_name4 { get; set; }

        public string img_file_name5 { get; set; }

        public string img_file_name6 { get; set; }

        public string img_file_name7 { get; set; }

        public int id { get; protected set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public string template_code { get; protected set; }

        public string title { get; protected set; }

        public string posted_date { get; protected set; }

        public string sub_title { get; protected set; }

        public string body { get; protected set; }

        public string add_body { get; protected set; }

        public string link_string_1 { get; protected set; }

        public string link_url_1 { get; protected set; }

        public string link_string_2 { get; protected set; }

        public string link_url_2 { get; protected set; }

        public string link_string_3 { get; protected set; }

        public string link_url_3 { get; protected set; }

        public string link_string_4 { get; protected set; }

        public string link_url_4 { get; protected set; }

        public string link_string_5 { get; protected set; }

        public string link_url_5 { get; protected set; }

        public string file_string_1 { get; protected set; }

        public string file_real_name_1 { get; protected set; }

        public string file_name_1 { get; protected set; }

        public string file_string_2 { get; protected set; }

        public string file_real_name_2 { get; protected set; }

        public string file_name_2 { get; protected set; }

        public string file_string_3 { get; protected set; }

        public string file_real_name_3 { get; protected set; }

        public string file_name_3 { get; protected set; }

        public string file_string_4 { get; protected set; }

        public string file_real_name_4 { get; protected set; }

        public string file_name_4 { get; protected set; }

        public string file_string_5 { get; protected set; }

        public string file_real_name_5 { get; protected set; }

        public string file_name_5 { get; protected set; }

        public string[] img_file_name { get; protected set; }

        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 255)]
        public string admin_ransu { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 255)]
        public string admin_ssl_ransu { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 255)]
        public string user_cd { get; set; }
    }
}