﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using System.IO;
using ers.Domain.ResizeImage.Mappables;
using jp.co.ivp.ers;

namespace ers.Models.ResizeImage
{
    public class ResizeImage
        : ErsModelBase, IResizeImageMappable
    {
        [ErsUniversalValidation]
        public string fileName { get; set; }

        [ErsUniversalValidation]
        public EnumImageDir? dirName { get; set; }

        public string contentType { get; set; }

        public byte[] bytes { get; set; }

        public string filePath { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.OneByteCharacter, rangeTo = 100)]
        public string scode { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int width { get; set; }
    }
}