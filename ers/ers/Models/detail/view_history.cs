﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ers.Domain.Detail.Mappables;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc.validation;
using ers.Domain.Detail.Commands;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class view_history
        : ErsFrontModelBase, IDetailViewHistoryMappable, IDetailViewHistoryCommand
    {
        [ErsSchemaValidation("g_master_t.gcode")]
        public string ignore_gcode { get; set; }

        [ErsSchemaValidation("g_master_t.gcode", isArray=true)]
        public string[] history_gcode { get; set; }

        public IList<ErsMerchandise> viewHistoryList { get; set; }
    }
}