﻿using System.ComponentModel;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ers.Models.detail
{
    public class price_detail_rank_record : ErsBindableModel
    {

        public override string lineName
        {
            get
            {
                return this.member_rank_name;
            }
        }

        [ErsSchemaValidation("price_t.member_rank")]
        public int? member_rank { get; set; }

        [ErsSchemaValidation("price_t.price")]
        [DisplayName("normal_price")]
        public int? price { get; set; }

        [ErsSchemaValidation("price_t.price")]
        [DisplayName("regular_price")]
        public int? regular_price { get; set; }

        [ErsSchemaValidation("price_t.price")]
        [DisplayName("regular_first_price")]
        public int? regular_first_price { get; set; }

        [ErsSchemaValidation("common_namecode_t.namename")]
        public string member_rank_name { get; set; }

    }
}