﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise.category;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.merchandise.specification;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.basket.specification;
using ers.Models.cart;
using jp.co.ivp.ers;
using ers.Domain.Detail.Mappables;
using jp.co.ivp.ers.db;
using ers.Domain.Detail.Commands;
using ers.jp.co.ivp.ers.mvc;
using ers.Models.detail;

namespace ers.Models
{
    public class Detail
        : ErsFrontModelBase, IMerchandiseDetailMappable, IMerchandiseRecommendMappable, IMerchandiseDetailCommand, IPriceDetailRankRecordMappable
    {
        public ErsMerchandise merchandise { get; set; }

        public Detail()
        {
            //初期値設定
            this.next_date_day = ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetRegularFromDate(DateTime.Now);
            this.firstTime = 0;
        }

        [ErsSchemaValidation("s_master_t.gcode")]
        public string gcode { get; set; }

        [ErsSchemaValidation("s_master_t.scode")]
        public string scode { get; set; }

        [ErsSchemaValidation("bask_t.amount")]
        public int amount { get; set; }

        [ErsSchemaValidation("bask_t.amount")]
        public int regular_amount { get; set; }

        //fields for regular order
        [ErsSchemaValidation("regular_detail_t.next_date")]
        public DateTime? next_date_day { get; set; }

        //fields for regular order
        [ErsSchemaValidation("regular_detail_t.next_date")]
        public DateTime? next_date_month { get; set; }

        //送料無料フラグ
        public Boolean carriageFreeFlg
        {
            get
            {
                if (this.carriage_cost_type == EnumCarriageCostType.Free)
                {
                    return true;
                }

                if (this.price != null)
                {
                    var isCarrigeFree = ErsFactory.ersBasketFactory.GetCarriageFreeSpecification().GetCarriageFreeStatus((int)this.price);
                    if (isCarrigeFree != EnumCarriageFreeStatus.CARRIAGE_NOT_FREE)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        [ErsSchemaValidation("regular_detail_t.ptn_interval_day")]
        public short? ptn_interval_day { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public short? ptn_monthly_interval_month { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public short? ptn_weekly_interval_month { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_day")]
        public short? ptn_day { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_interval_week")]
        public short? ptn_interval_week { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_weekday")]
        public DayOfWeek? ptn_weekday { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public int? firstTime { get; set; }

        [ErsSchemaValidation("regular_detail_t.send_ptn")]
        public virtual EnumSendPtn send_ptn { get; set; }

        public bool disp_send_ptn_month_intervals { get; set; }

        public bool disp_send_ptn_week_intervals { get; set; }

        public bool disp_send_ptn_month_day_intervals { get; set; }

        /// <summary>
        /// 商品詳細その他情報HTMLパス
        /// </summary>
        public string detail_info_Path { get { return "../static/items/" + this.gcode + ".html"; } }

        public string detail_info_m_Path { get { return "../static/items/" + this.gcode + "_m.html"; } }

        public string detail_info_u_Path { get { return "../static/items/" + this.gcode + "_u.html"; } }

        public EnumCarriageCostType? carriage_cost_type { get; set; }

        public EnumPluralOrderType? plural_order_type { get; set; }
        
        public string gname { get; set; }

        public DateTime? date_from { get; set; }

        public DateTime? date_to { get; set; }

        public EnumSalePatternType? s_sale_ptn { get; set; }

        //定期フラグ
        public bool sale_regular_flg
        {
            get
            {
                if (!this.disp_send_ptn_month_day_intervals && !this.disp_send_ptn_month_intervals && !this.disp_send_ptn_week_intervals)
                {
                    return false;
                }

                if (this.s_sale_ptn != null)
                {
                    if (this.s_sale_ptn == EnumSalePatternType.NORMAL)
                        return false;
                    else if (this.s_sale_ptn == EnumSalePatternType.REGULAR)
                        return true;
                    else if (this.s_sale_ptn == EnumSalePatternType.ALL)
                        return true;
                }
                return false;
            }
        }

        public EnumStockFlg? stock_flg { get; set; }

        public int? stock_set1 { get; set; }

        public int? stock_set2 { get; set; }

        public string metatitle { get; set; }

        public string w_metatitle
        {
            get
            {
                if (!string.IsNullOrEmpty(this.metatitle))
                    return this.metatitle;

                return this.gname;
            }
        }

        public string metadescription { get; set; }

        public string metawords { get; set; }

        public string link_url { get; set; }

        public string description { get; set; }

        public string jancode { get; set; }

        public string sname { get; set; }

        public int? price { get; set; }

        public int? default_price { get; set; }

        public int? regular_price { get; set; }

        public int? regular_first_price { get; set; }

        public int? non_member_price { get; set; }

        public int? non_member_regular_price { get; set; }

        public int? non_member_regular_first_price { get; set; }

        public int? price2 { get; set; }

        public int? point { get; set; }

        public int? stock
        {
            get
            {
                if (this.merchandise == null)
                {
                    return null;
                }
                return ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetObtainMerchandiseStockStgy().GetStock(merchandise.scode, merchandise.stock);
            }
        }

        public EnumSoldoutFlg? soldout_flg { get; set; }

        public string attribute1 { get; set; }

        public string attribute2 { get; set; }

        public DateTime? point_campaign_from { get; set; }

        public DateTime? point_campaign_to { get; set; }

        public int? campaign_point { get; set; }

        public string m_description { get; set; }

        public List<Dictionary<string, object>> recommendItems { get; set; }

        public int recommendItemCount
        {
            get
            {
                if (recommendItems == null)
                {
                    return 0;
                }
                return recommendItems.Count;
            }
        }

        public IEnumerable<ErsCategoryTree> categoryTree
        {
            get
            {
                if (this.merchandise == null)
                {
                    return null;
                }
                return ErsCategoryTree.GetTree(this.merchandise);
            }
        } 

        public short? category1_active { get { return ErsFactory.ersUtilityFactory.getSetup().cate1_active; } }
        public short? category2_active { get { return ErsFactory.ersUtilityFactory.getSetup().cate2_active; } }
        public short? category3_active { get { return ErsFactory.ersUtilityFactory.getSetup().cate3_active; } }
        public short? category4_active { get { return ErsFactory.ersUtilityFactory.getSetup().cate4_active; } }
        public short? category5_active { get { return ErsFactory.ersUtilityFactory.getSetup().cate5_active; } }

        public string category1_name { get { return ErsFactory.ersUtilityFactory.getSetup().cate1; } }
        public string category2_name { get { return ErsFactory.ersUtilityFactory.getSetup().cate2; } }
        public string category3_name { get { return ErsFactory.ersUtilityFactory.getSetup().cate3; } }
        public string category4_name { get { return ErsFactory.ersUtilityFactory.getSetup().cate4; } }
        public string category5_name { get { return ErsFactory.ersUtilityFactory.getSetup().cate5; } }

        public List<ErsCategory> category1 { get; set; }
        public List<ErsCategory> category2 { get; set; }
        public List<ErsCategory> category3 { get; set; }
        public List<ErsCategory> category4 { get; set; }
        public List<ErsCategory> category5 { get; set; }

        public int? category1_count
        {
            get
            {
                if (this.category1 == null)
                {
                    return null;
                }
                return this.category1.Count;
            }
        }
        public int? category2_count
        {
            get
            {
                if (this.category2 == null)
                {
                    return null;
                }
                return this.category2.Count;
            }
        }
        public int? category3_count
        {
            get
            {
                if (this.category3 == null)
                {
                    return null;
                }
                return this.category3.Count;
            }
        }
        public int? category4_count
        {
            get
            {
                if (this.category4 == null)
                {
                    return null;
                }
                return this.category4.Count;
            }
        }
        public int? category5_count
        {
            get
            {
                if (this.category5 == null)
                {
                    return null;
                }
                return this.category5.Count;
            }
        }

        //fields for regular order
        public List<Dictionary<string, object>> ListPtnIntervalDay
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalDay, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ListPtnIntervalMonth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalMonth, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ListPtnDay
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnDay, EnumCommonNameColumnName.namename);
            }
        }

        public List<DateTime> ListRegularSenddateMonth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsRegularOrderViewService().GetListRegularSenddateMonth();
            }
        }

        public List<Dictionary<string, object>> ListPtnIntervalWeek
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalWeek, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ListPtnWeekday
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename);
            }
        }

        public IEnumerable<Dictionary<string, object>> listMerchandise { get; set; }

        public int? countListMerchandise
        {
            get
            {
                if (listMerchandise == null)
                {
                    return 0;
                }

                return listMerchandise.Count();
            }
        }

        public bool? onPointCampaign
        {
            get
            {
                if (this.merchandise == null)
                {
                    return null;
                }
                return ErsFactory.ersMerchandiseFactory.GetOnPointCampaignSpecification().IsSatisfiedBy(merchandise.point_campaign_from, point_campaign_to, DateTime.Now);
            }
        }

        public bool onCampaign { get; set; }

        public EnumDatePeriod? DatePeriod
        {
            get
            {
                if (this.merchandise == null)
                {
                    return null;
                }
                return ErsFactory.ersMerchandiseFactory.GetOnSaleSpecification().GetDatePeriod(merchandise.date_from, merchandise.date_to, DateTime.Now);
            }
        }

        public EnumStockStatus? StockStatus
        {
            get
            {
                if (this.merchandise == null)
                {
                    return null;
                }
                return ErsFactory.ersMerchandiseFactory.GetStockStatusSpecification().GetStockStatusOfMerchandise(merchandise);
            }
        }

        public string encode_nor_url
        {
            get
            {
                return HttpUtility.UrlEncode(ErsFactory.ersUtilityFactory.getSetup().nor_url);
            }
        }

        /// <summary>
        /// Get filenames of this merchandise thumbnail.
        /// </summary>
        public List<string> imageFiles { get; set; }

        public int? countImageFiles { get; set; }

        public int? sendday
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.sendday;
            }
        }

        public int? sendday_count
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.sendday_count;
            }
        }

        public List<DateTime?> listHolidays
        {
            get
            {
                var repository = ErsFactory.ersOrderFactory.GetErsCalendarRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsCalendarCriteria();
                criteria.close_date_from = DateTime.Now.AddDays(this.sendday.Value);
                criteria.close_date_to = DateTime.Now.AddDays(this.sendday_count.Value);
                var listCalendar = repository.Find(criteria);
                var listHolidays = new List<DateTime>();
                return listCalendar.Select((calendar) => calendar.close_date).ToList();
            }
        }

        public bool show_selectable_calendar
        {
            get
            {
                return (this.listHolidays.Count <= this.sendday_count - this.sendday);
            }
        }

        public bool all_price_empty { get; set; }

        public bool all_regular_price_empty { get; set; }

        public bool all_regular_first_price_empty { get; set; }

        public List<price_detail_rank_record> listPriceDetailRankRecord { get; set; }

        public bool notSale
        {
            get
            {
                var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().RetrieveWithSession();
                if (!String.IsNullOrEmpty(gcode))
                {
                    var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
                    var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
                    criteria.gcode = gcode;
                    criteria.SetActiveOnly(DateTime.Now);
                    criteria.SetOrderByDisp_order(Criteria.OrderBy.ORDER_BY_ASC);
                    criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

                    var listMerchandise = repository.FindSkuBaseItemList(criteria, member_rank);
                    if (listMerchandise.Count == 0)
                    {
                        return true;
    }
                }
                else if (!string.IsNullOrEmpty(scode))
                {
                    var merchandise = ErsFactory.ersMerchandiseFactory.GetActiveErsMerchandiseWithScode(scode, member_rank);
                    if (merchandise == null)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public bool hasRankPrice
        {
            get
            {
                if (listPriceDetailRankRecord != null)
                    return listPriceDetailRankRecord.Count(p => p.price != null) > 0;
                else return false;
            }
        }
    }
}