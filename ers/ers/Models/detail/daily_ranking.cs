﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ers.Domain.Detail.Mappables;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc.validation;
using ers.Domain.Detail.Commands;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class daily_ranking
        : ErsFrontModelBase, IDetailViewRankingMappable
    {
        public IList<ErsMerchandise> viewRankingList { get; set; }
    }
}