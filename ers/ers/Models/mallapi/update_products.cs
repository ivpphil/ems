﻿using System;
using System.Collections.Generic;
using ers.Domain.MallApi.Commands;
using ers.Domain.MallApi.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ers.Models
{
    /// <summary>
    /// 商品更新モデル [Model for Update products]
    /// </summary>
    public class update_products
        : ErsModelBase, IUpdateProductsCommand, IUpdateProductsMappable
    {
        /// <summary>
        /// エラーメッセージ [Error message]
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return String.Join(Environment.NewLine, this.GetAllErrorMessageList());
            }
        }

        /// <summary>
        /// 在庫平準化アラートログ [Alert for stock leveling]
        /// </summary>
        public IList<string> listStockLevelingAlertLog { get; set; }


        /// <summary>
        /// 認証ID [Authentication id]
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string sys_id { get; set; }

        /// <summary>
        /// 認証パス [Authentication password]
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string sys_pass { get; set; }

        /// <summary>
        /// 商品情報 [Product information]
        /// </summary>
        [BindArrayObject("product")]
        public IEnumerable<ProductModel> products { get; set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public update_products()
        {
            this.listStockLevelingAlertLog = new List<string>();
        }
    }
}
