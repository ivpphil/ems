﻿using System;
using ers.Domain.MallApi.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ers.Models
{
    /// <summary>
    /// 商品情報モデル [Model of product infomation]
    /// </summary>
    public class ProductModel
        : ErsBindableModel, IProductModelCommand
    {
        /// <summary>
        /// ERS用商品コード [Product code for ERS]
        /// </summary>
        public string ers_scode
        {
            get
            {
                return this.product_code.HasValue() ? ErsMallCommonService.GetErsSkuFromMall(this.product_code) : this.product_code;
            }
        }

        /// <summary>
        /// エラーメッセージ [Error message]
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return String.Join(Environment.NewLine, this.GetAllErrorMessageList());
            }
        }

        /// <summary>
        /// 商品コード [Scode]
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string product_code { get; set; }

        /// <summary>
        /// 在庫数 [Stock]
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? stock { get; set; }

        /// <summary>
        /// 価格 [Price]
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? price { get; set; }
    }
}
