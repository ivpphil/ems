﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.state;
using System.Text;
using ers.Domain.Login.Commands;
using ers.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.Models
{
    public class Login
        : ErsFrontModelBase, ILoginCommand, IMemberRankPricingCommand
    {

        public Login()
        {
        }

        [ErsSchemaValidation("member_t.email")]
        public string email { get; set; }

        [ErsSchemaValidation("member_t.passwd")]
        public string passwd { get; set; }

        [HtmlSubmitButton]
        public bool email_ck { get; set; }

        public EnumAccountStatus account_status {get;set;}
    }
}