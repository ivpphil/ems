﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.basket;
using System.ComponentModel;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;
using ers.Domain.Cart.Commands;
using ers.Domain.Cart.Mappables;

namespace ers.Models.cart
{
    public class Cart_regular_items
        : Cart_items, IManageRegularPatternDatasource, ICartRegularBasketRecordCommand, ICartRegularBasketRecordMappable
    {
        public override string lineName
        {
            get
            {
                if (string.IsNullOrEmpty(this.scode))
                    return base.lineName;
                else
                    return ErsResources.GetMessage("line_name_scode", this.scode);
            }
        }

        [ErsUniversalValidation]
        public virtual string regular_key { get; set; }

        //fields for regular order
        [ErsSchemaValidation("regular_detail_t.send_ptn")]
        public EnumSendPtn? send_ptn { get; set; }

        [ErsSchemaValidation("d_master_t.senddate")]
        public DateTime? next_date { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_interval_day")]
        public short? ptn_interval_day { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public short? ptn_interval_month { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_day")]
        public short? ptn_day { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_interval_week")]
        public short? ptn_interval_week { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_weekday")]
        public DayOfWeek? ptn_weekday { get; set; }

        public string ptn_day_name
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.PtnDay, EnumCommonNameColumnName.namename, this.ptn_day);
            }
        }

        public string ptn_weekday_name
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename, (int?)this.ptn_weekday);
            }
        }

        public string ptn_interval_name
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsRegularOrderViewService().GetDeliveryTerm(this);
            }
        }
    }
}