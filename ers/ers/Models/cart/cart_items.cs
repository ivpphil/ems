﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers;
using ers.Domain.Cart.Commands;
using ers.Domain.Cart.Mappables;
using jp.co.ivp.ers.mvc;

namespace ers.Models.cart
{
    public class Cart_items
        : ErsBindableModel, ICartBasketRecordCommand, ICartBasketRecordMappable
    {
        public override string lineName
        {
            get
            {
                if (string.IsNullOrEmpty(this.scode))
                    return base.lineName;
                else
                    return ErsResources.GetMessage("line_name_scode", this.scode);
            }
        }

        [ErsUniversalValidation]
        public virtual string key { get; set; }

        [ErsSchemaValidation("bask_t.amount")]
        public virtual int? amount { get; set; }

        public virtual string scode { get; set; }
        public virtual string sname { get; set; }
        public virtual string m_sname { get; set; }
        public virtual string attribute2 { get; set; }
        public virtual string attribute1 { get; set; }
        public virtual int? price { get; set; }
        public virtual int? total { get; set; }
        public virtual EnumCarriageCostType? carriage_cost_type { get; set; }
        public virtual EnumPluralOrderType? plural_order_type { get; set; }
        public virtual int? regular_price { get; set; }
        public virtual int? regular_total
        {
            get
            {
                return regular_price * amount;
            }
        }

        public ErsBaskRecord baskRecord { get; set; }

        public virtual string ccode { get; set; }

        public virtual int? max_purchase_count { get; set; }
    }
}