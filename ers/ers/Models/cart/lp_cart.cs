﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Cart.Commands;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using ers.Domain.Cart.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.basket;
using System.ComponentModel;

namespace ers.Models.cart
{
    public class LP_Cart
        : Cart, ILPCartCommand, ILPUpSellCartCommand, ILPCartMappable
    {

        [ErsSchemaValidation("bask_t.ransu")]
        public string ransu { get; set; }

        [ErsOutputHidden("lp_confirm")]
        [HtmlSubmitButton]
        public override bool regular_basket_in { get; set; }

        public ErsLpPageManage lp_page_manage { get; set; }

        [ErsSchemaValidation("bask_t.amount")]
        public virtual int? amount { get; set; }

        [ErsSchemaValidation("bask_t.amount")]
        [DisplayName("upsell_amount")]
        public virtual int? upsell_amount { get; set; }

        public bool IsOrdinaryOrder { get; set; }

        public bool IsRegularOrder { get; set; }

        public bool IsAddCombinationUpSell { get; set; }

        public virtual bool HasConfirmPage
        {
            get
            {
                if (this.lp_page_manage != null)
                    return ErsFactory.ersLpFactory.GetLandingPageHasConfirmPageSpec().IsSatisfiedBy(lp_page_manage);

                return true;
            }
        }

        public virtual bool IsHiddenRegularTax
        {
            get
            {
                if (this.regular_tax == this.regular_tax_next)
                    return regular_tax == 0;

                return false;
            }
        }

        #region CARRIAGE FLG AND FREE PRICE
        public virtual EnumUse? lp_carriage_free_flg
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.carriage_free_flg;

                return null;
            }
        }

        public virtual bool carriage_free_flg
        {
            get
            {
                return this.lp_carriage_free_flg == EnumUse.Use;
            }
        }

        public int? lp_carriage_free_price
        {
            get
            {
                if (this.lp_page_manage != null)
                    return this.lp_page_manage.carriage_free_price;

                return 0;
            }
        }
        #endregion
    }
}