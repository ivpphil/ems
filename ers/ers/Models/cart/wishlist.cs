﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.state;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise.specification;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using ers.Domain.Cart.Commands;
using ers.Domain.Cart.Mappables;
using ers.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;

namespace ers.Models
{
    public class Wishlist
        : ErsFrontModelBase, IWishlistCommand, IWishlistMappable
    {
        public List<Dictionary<string, object>> listItem { get; set; }

        [ErsSchemaValidation("wishlist_t.scode")]
        [ErsOutputHidden]
        public string scode { get; set; }

        [ErsSchemaValidation("wishlist_t.scode")]
        public string del_scode { get; set; }

        public ErsPagerModel pager { get; set; }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        /// <summary>
        /// Get number of item on a page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }
    }
}