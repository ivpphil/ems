﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.Web.Mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.basket.specification;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.merchandise.strategy;
using ers.Models.cart;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;
using ers.Domain.Cart.Commands;
using ers.Domain.Cart.Mappables;
using ers.Domain.Register.Commands;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class Cart
        : ErsFrontModelBase, ICartCommand, ICartMappable, IRegisterCartCommand, ICartForcedRefleshCommand
    {
        public Cart()
        {
            basket = ErsFactory.ersBasketFactory.GetErsBasket();
        }

        public virtual ErsBasket basket { get; set; }

        public bool IsRegister { get; set; }

        [HtmlSubmitButton]
        public virtual bool regular_basket_in { get; set; }

        [HtmlSubmitButton]
        public bool normal_basket_in { get; set; }

        [ErsSchemaValidation("bask_t.scode")]
        public string scode { get; set; }

        [ErsSchemaValidation("bask_t.amount")]
        public int? amount { get; set; }

        [ErsUniversalValidation]
        public virtual string del_key { get; set; }

        [ErsUniversalValidation]
        public virtual string del_regular_key { get; set; }

        [BindTable("basketItems")]
        public IEnumerable<Cart_items> basketItems { get; set; }

        [HtmlSubmitButton]
        public bool recompute { get; set; }

        [HtmlSubmitButton]
        public bool regular_recompute { get; set; }

        [ErsSchemaValidation("bask_t.ccode")]
        public string ccode { get; set; }

        [DisplayName("delete_btn")]
        [HtmlSubmitButtonGroup("delete_ord")]
        public virtual string deleteButton { get; set; }

        [DisplayName("delete_btn")]
        [HtmlSubmitButtonGroup("delete_reg")]
        public virtual string deleteRegularButton { get; set; }

        /// <summary>
        /// ordinary order items count
        /// </summary>
        public int basketItemCount
        {
            get
            {
                if (basketItems == null)
                    return 0;
                else
                    return basketItems.Count();
            }
        }

        /// <summary>
        /// regular order items
        /// </summary>
        [BindTable("regularBasketItems")]
        public IEnumerable<Cart_regular_items> regularBasketItems { get; set; }

        /// <summary>
        /// regular order items count
        /// </summary>
        public int regularBasketItemCount
        {
            get
            {
                if (regularBasketItems == null)
                    return 0;
                else
                    return regularBasketItems.Count();
            }
        }

        public bool HasRegularOrder
        {
            get
            {
                return (regularBasketItemCount > 0);
            }
        }

        public bool existBasketItem
        {
            get
            {
                return (this.basketItemCount != 0 || this.regularBasketItemCount != 0);
            }
        }

        public virtual int regular_amounttotal { get { return basket.regular_amounttotal; } }

        public virtual int regular_subtotal { get { return basket.regular_subtotal; } }

        public virtual int regular_tax { get { return basket.regular_tax; } }

        public virtual int regular_total { get { return basket.regular_total; } }

        public virtual int regular_subtotal_next { get { return basket.regular_subtotal_next; } }

        public virtual int regular_tax_next { get { return basket.regular_tax_next; } }

        public virtual int regular_total_next { get { return basket.regular_total_next; } }

        public virtual int amounttotal { get { return basket.amounttotal; } }

        public virtual int subtotal { get { return basket.subtotal; } }

        public virtual int tax { get { return basket.tax; } }

        public virtual int total { get { return basket.total; } }

        public virtual int ordinary_total { get { return this.total; } }

        public virtual int amounttotal_all_item { get { return this.basket.amounttotal + this.basket.regular_amounttotal; } }

        public virtual int total_all_item { get { return this.basket.total + this.basket.regular_total; } }

        public List<Dictionary<string, object>> recommendItems { get; set; }

        public int recommendItemCount
        {
            get
            {
                if (recommendItems == null)
                {
                    return 0;
                }
                return recommendItems.Count;
            }
        }

        public EnumCarriageFreeStatus carriageFree
        {
            get
            {
                return basket.carriageFree;
            }
        }

        public int free { get { return basket.free; } }

        public int free_get { get { return basket.free_get; } }

        public string regi_memo
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.regi_memo;
            }
        }

        [ErsUniversalValidation(type=CHK_TYPE.Numeric, rangeFrom=1, rangeTo=3)]
        public virtual EnumSendPtn? send_ptn { get; set; }

        public DateTime? next_date
        {
            get
            {
                if (send_ptn == EnumSendPtn.DAY_INTERVALS && (this.firstTime != null && firstTime == 1))
                    return next_date_day;
                else if (send_ptn == EnumSendPtn.MONTH_INTERVALS)
                    return next_date_month;
                else
                    return null;
            }
        }

        //fields for regular order
        [ErsOutputHidden("lp_confirm")]
        [ErsSchemaValidation("regular_detail_t.next_date")]
        public DateTime? next_date_day { get; set; }

        //fields for regular order
        [ErsSchemaValidation("regular_detail_t.next_date")]
        public DateTime? next_date_month { get; set; }

        [ErsOutputHidden("lp_confirm")]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_day")]
        public short? ptn_interval_day { get; set; }

        public short? ptn_interval_month
        {
            get
            {
                if (send_ptn == EnumSendPtn.MONTH_INTERVALS)
                    return ptn_monthly_interval_month;
                else if (send_ptn == EnumSendPtn.WEEK_INTERVALS)
                    return ptn_weekly_interval_month;
                else
                    return null;
            }
        }

        [ErsOutputHidden("lp_confirm")]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public short? ptn_monthly_interval_month { get; set; }

        [ErsOutputHidden("lp_confirm")]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public short? ptn_weekly_interval_month { get; set; }

        [ErsOutputHidden("lp_confirm")]
        [ErsSchemaValidation("regular_detail_t.ptn_day")]
        public short? ptn_day { get; set; }

        [ErsOutputHidden("lp_confirm")]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_week")]
        public short? ptn_interval_week { get; set; }

        [ErsOutputHidden("lp_confirm")]
        [ErsSchemaValidation("regular_detail_t.ptn_weekday")]
        public DayOfWeek? ptn_weekday { get; set; }

        [ErsOutputHidden("lp_confirm")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public int? firstTime { get; set; }

        public short? DisplayedDelvMethod { get; set; }

        public int? sendday
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.sendday;
            }
        }

        public int? sendday_count
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.sendday_count;
            }
        }

        public List<DateTime?> listHolidays
        {
            get
            {
                var repository = ErsFactory.ersOrderFactory.GetErsCalendarRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsCalendarCriteria();
                criteria.close_date_from = DateTime.Now.AddDays(this.sendday.Value);
                criteria.close_date_to = DateTime.Now.AddDays(this.sendday_count.Value);
                var listCalendar = repository.Find(criteria);
                var listHolidays = new List<DateTime>();
                return listCalendar.Select((calendar) => calendar.close_date).ToList();
            }
        }

        public bool show_selectable_calendar
        {
            get
            {
                return (this.listHolidays.Count <= this.sendday_count - this.sendday);
            }
        }

        //fields for regular order
        public List<Dictionary<string, object>> ListPtnIntervalDay
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalDay, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ListPtnIntervalMonth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalMonth, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ListPtnDay
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnDay, EnumCommonNameColumnName.namename);
            }
        }

        public List<DateTime> ListRegularSenddateMonth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsRegularOrderViewService().GetListRegularSenddateMonth();
            }
        }

        public List<Dictionary<string, object>> ListPtnIntervalWeek
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalWeek, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ListPtnWeekday
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename);
            }
        }

        public bool IsLoggedIn { get; set; }

        public bool HideWithoutMembershipRegistration
        {
            get
            {
                if (this.basket.objBasketRecord.Count == 0)
                    return false;

                return this.basket.objBasketRecord.Any(p => p.Value.plural_order_type == EnumPluralOrderType.Once);
            }
        }


        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 3)]
        public EnumMemberEntryMode? k_flg { get; set; }

        /// <summary>
        /// True if the basket has only carriage free item.
        /// </summary>
        public bool BasketHasOnlyCarriageFreeItems
        {
            get
            {
                return this.basket.objBasketRecord.Count + this.basket.objRegularBasketRecord.Count > 0
                    && this.basket.objBasketRecord.Values.FirstOrDefault((record) => record.carriage_cost_type != EnumCarriageCostType.Free) == null
                    && this.basket.objRegularBasketRecord.Values.FirstOrDefault((record) => record.carriage_cost_type != EnumCarriageCostType.Free) == null;
            }
        }

        /// <summary>
        /// True if the total of normal items reached to carriage free price.
        /// </summary>
        public bool CarriageFreeOfNormalItems
        {
            get
            {
                return ErsFactory.ersBasketFactory.GetCarriageFreeSpecification().GetCarriageFreeStatus(basket.subtotal) == EnumCarriageFreeStatus.CARRIAGE_FREE;
            }
        }

        /// <summary>
        /// True if the basket has only carriage free item.
        /// </summary>
        public bool BasketHasOneMailDeliverItem
        {
            get
            {
                return this.basket.objBasketRecord.Count + this.basket.objRegularBasketRecord.Count == 1
                    && (this.basket.objBasketRecord.Values.FirstOrDefault((record) => record.deliv_method == EnumDelvMethod.Mail) != null
                        || this.basket.objRegularBasketRecord.Values.FirstOrDefault((record) => record.deliv_method == EnumDelvMethod.Mail) != null);
            }
        }
    }
}