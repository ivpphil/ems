﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.stepmail;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ers.Domain.Redirect.Commands;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models.redirect
{
    public class stepRedirect
        : ErsFrontModelBase, IRedirectCommand
    {
        /// <summary>
        /// ＠メールプロセスID
        /// </summary>
        [ErsSchemaValidation("mail_url_click_counter_t.process_id")]
        [DisplayName("stepRedirect.id")]
        public int? id { get; set; }

        /// <summary>
        /// リダイレクト先URL
        /// </summary>
        [ErsSchemaValidation("mail_url_click_counter_t.url")]
        [DisplayName("stepRedirect.ptn")]
        public string ptn { get; set; }
    }
}