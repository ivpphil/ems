﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers;
using ers.Domain.Quest.Commands;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models
{
    public class Quest
        : ErsFrontModelBase, IQuestCommand
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.lname")]
        public string lname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fname")]
        public string fname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.email")]
        public string email { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.email")]
        public string email_confirm { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 99)]
        public EnumQuestPtn? quest_ptn { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 10)]
        public string orderno { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 1000)]
        public string quest_text { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 1)]
        public int? pri_chk { get; set; }

        public List<Dictionary<string, object>> quest_ptntList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.QuestPtn, EnumCommonNameColumnName.namename);
            }
        }

        // お問い合わせ種別
        public string w_quest_ptn
        {
            get
            {
                if (this.quest_ptn == null)
                    return string.Empty;

                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.QuestPtn, EnumCommonNameColumnName.namename, (int?)this.quest_ptn);
            }
        }

        // 送信時間
        public string w_sendtime
        {
            get
            {
                return DateTime.Now.ToString();
            }
        }

        public virtual string pri_memo
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.pri_memo;
            }
        }

        public virtual DateTime current_time
        {
            get
            {
                return DateTime.Now;
            }
        }
    }
}