﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.strategy;
using System.ComponentModel;
using ers.Domain.Search.Mappables;
using jp.co.ivp.ers;
using ers.jp.co.ivp.ers.mvc;
using ers.Domain.Search.Commands;

namespace ers.Models
{
    public class List
        : ErsFrontModelBase, IMerchandiseListMappable, IMerchandiseListCommand
    {
        public List()
        {
            this.s_outstock = true;
        }

        public ErsPagerModel pager { get; internal set; }

        /// <summary>
        /// Get number of item on a merchandise list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        public long maxLineCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnOneLine; } }

        public long recordCount { get; set; }

        //商品検索結果
        public virtual List<Dictionary<string, object>> merchandiseList { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [ErsOutputHidden("on_list")]
        [HtmlSubmitButton]
        public bool search_item { get; set; }

        [ErsOutputHidden("on_list")]
        [ErsSchemaValidation("s_master_t.scode")]
        public string s_scode { get; set; }

        [ErsOutputHidden("on_list")]
        [ErsSchemaValidation("s_master_t.sname")]
        public string s_sname { get; set; }

        [ErsOutputHidden]
        [DisplayName("keyword")]
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 200)]
        public string s_keyword { get; set; }

        [ErsOutputHidden("on_list")]
        [ErsSchemaValidation("g_master_t.cate1")]
        public int? s_cate1 { get; set; }

        [ErsOutputHidden("on_list")]
        [ErsSchemaValidation("g_master_t.cate2")]
        public int? s_cate2 { get; set; }

        [ErsOutputHidden("on_list")]
        [ErsSchemaValidation("g_master_t.cate3")]
        public int? s_cate3 { get; set; }

        [ErsOutputHidden("on_list")]
        [ErsSchemaValidation("g_master_t.cate4")]
        public int? s_cate4 { get; set; }

        [ErsOutputHidden("on_list")]
        [ErsSchemaValidation("g_master_t.cate5")]
        public int? s_cate5 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.price_for_search")]
        [DisplayName("s_price")]
        public long? s_price1 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.price_for_search")]
        [DisplayName("s_price")]
        public long? s_price2 { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool s_outstock { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 3)]
        public int? sort { get; set; }

        [HtmlSubmitButton]
        public bool from_item_list { get; set; }

        public List<Dictionary<string, object>> CateList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().GetCategoryList(true,true,
                    this.s_cate1,
                    this.s_cate2,
                    this.s_cate3,
                    this.s_cate4,
                    this.s_cate5);
            }
        }
    }
}