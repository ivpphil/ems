﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.order;
using System.Web.Mvc;
using System.Collections;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.order.specification;
using ers.Models.cart;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers;
using ers.Domain.Register.Commands;
using ers.Domain.Cart.Commands;
using ers.Domain.Register.Mappables;
using ers.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;

namespace ers.Models
{
    public class Register
        : ErsFrontModelBase, IIsMonitorSpecDatasource, IOrderRegistCommand, IMemberOrderMappable, IOrderMappable, IOrderMemberRegistCommand, IOrderMemberCardDeleteCommand
    {
        public virtual string ransu { get { return ErsContext.sessionState.Get("ransu"); } set { } }

        public string mcode { get { return ErsContext.sessionState.Get("mcode"); } }

        public Register()
        {
            //初期値入力
            this.m_flg = EnumMFlg.Deliver;

            this.firstTimeOrdinary = EnumFirstTimeOrdinary.Shortest;
        }

        public bool IsConfirmationPage { get; internal set; }

        public ICartCommand cart { get; internal set; }

        public ErsMember member { get; set; }

        /// <summary>
        /// Gets main ErsRegularOrder that will be used in preparing bills
        /// </summary>
        public ErsOrderIntegrated order { get; set; }

        /// <summary>
        /// Holds credit card information for use in settlement
        /// </summary>
        public CreditCardInfo savedCardInfo { get; set; }

        public IList<Dictionary<string, object>> ListCreditCardInfo { get; set; }

        public IList<CreditCardInfo> ListDelCreditCardInfo { get; set; }

        public IList<Dictionary<string, object>> addressList { get; set; }

        public DateTime? intime { get; set; }

        /// <summary>
        /// paypalから戻ったときにTrue
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public bool paypalReturn { get; set; }

        /// entry1のsumitがおされた場合
        /// </summary>
        [HtmlSubmitButton]
        public bool entry_submit { get; set; }

        /// entry2のsumitがおされた場合
        [HtmlSubmitButton]
        public bool entry2_submit { get; set; }

        [HtmlSubmitButton]
        public bool entry3_submit { get; set; }

        /// <summary>
        /// 修正するボタン
        /// </summary>
        [HtmlSubmitButton]
        public bool back_to_input { get; set; }

        [HtmlSubmitButton]
        public virtual bool point_flg { get; set; }

        /// クーポン再計算がおされた場合
        [HtmlSubmitButton]
        public virtual bool coupon_flg { get; set; }

        public virtual bool IsValidateRegularOrder { get; internal set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string token { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string PayerID { get; set; }

        [ErsOutputHidden("input_entry")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 3)]
        public EnumMemberEntryMode? k_flg { get; set; }

        #region "inputs at entry1"

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.lname")]
        public virtual string lname { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.fname")]
        public virtual string fname { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.lnamek")]
        public virtual string lnamek { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.fnamek")]
        public virtual string fnamek { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.email")]
        public virtual string email { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.email")]
        public virtual string email_confirm { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.mformat")]
        public virtual EnumMformat? mformat { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.tel")]
        public virtual string tel { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.zip")]
        public virtual string zip { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.pref")]
        public virtual int? pref { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.address")]
        public virtual string address { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.taddress")]
        public virtual string taddress { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.maddress")]
        public virtual string maddress { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = ErsViewBirthdayService.minimumOfSelectyear, rangeTo = ErsViewBirthdayService.maximumOfSelectyear)]
        public virtual int? birthday_y { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 12)]
        public virtual int? birthday_m { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 31)]
        public virtual int? birthday_d { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.sex")]
        public EnumSex? sex { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.passwd")]
        public virtual string passwd { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.passwd")]
        public virtual string passwd_confirm { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.ques")]
        public virtual int? ques { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.ans")]
        public virtual string ans { get; set; }


        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.compname")]
        public virtual string compname { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.compnamek")]
        public virtual string compnamek { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.division")]
        public virtual string division { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.divisionk")]
        public virtual string divisionk { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.tlname")]
        public virtual string tlname { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.tfname")]
        public virtual string tfname { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.tlnamek")]
        public virtual string tlnamek { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.tfnamek")]
        public virtual string tfnamek { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsSchemaValidation("member_t.fax")]
        public virtual string fax { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.m_flg")]
        public virtual EnumMFlg? m_flg { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.job")]
        public int? job { get; set; }

        [ErsOutputHidden("input_member", "lp_confirm")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 1)]
        public virtual int? pri_chk { get; set; }

        #endregion

        #region "inputs at entry2"

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("ds_master_t.deliv_method")]
        public virtual EnumDelvMethod? deliv_method { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.pay")]
        public virtual EnumPaymentType? pay { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.send")]
        public virtual EnumSendTo? send { get; set; }

        [ErsOutputHidden("input_point")]
        [ErsSchemaValidation("d_master_t.p_service")]
        public virtual int? p_service { get; set; }

        [ErsOutputHidden("input_point")]
        [ErsSchemaValidation("d_master_t.p_service")]
        public virtual int? ent_point { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 2)]
        public virtual EnumFirstTimeOrdinary? firstTimeOrdinary { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.senddate")]
        public virtual DateTime? senddate { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.sendtime")]
        public virtual int? sendtime { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.sendtime")]
        public virtual int? regular_sendtime { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.memo")]
        public virtual string memo { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.point_magnification")]
        public virtual int? point_magnification { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? member_add_id { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(rangeFrom = 0, rangeTo = 1, type = CHK_TYPE.Numeric)]
        public virtual EnumAddressAdd address_add { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.address_name")]
        public virtual string address_name { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_lname")]
        public virtual string add_lname { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_fname")]
        public virtual string add_fname { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_lnamek")]
        public virtual string add_lnamek { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_fnamek")]
        public virtual string add_fnamek { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_compname")]
        public virtual string add_compname { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_compnamek")]
        public virtual string add_compnamek { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_zip")]
        public virtual string add_zip { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_pref")]
        public virtual int? add_pref { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_address")]
        public virtual string add_address { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_taddress")]
        public virtual string add_taddress { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_maddress")]
        public virtual string add_maddress { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_tel")]
        public virtual string add_tel { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.add_fax")]
        public virtual string add_fax { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.wrap")]
        public virtual EnumWrap wrap { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.memo2")]
        public virtual string memo2 { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 100)]
        public virtual string card_holder_name { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? card { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(rangeFrom = 13, rangeTo = 16, type = CHK_TYPE.NumericString)]
        public virtual string cardno { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(rangeFrom = 3, rangeTo = 3, type = CHK_TYPE.NumericString)]
        public virtual string securityno { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(rangeFrom = 4, rangeTo = 4, type = CHK_TYPE.NumericString)]
        public virtual int? validity_y { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 12, type = CHK_TYPE.Numeric)]
        public virtual int? validity_m { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_card_t.id")]
        public virtual int? card_id { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_card_t.id", isArray = true)]
        public virtual int?[] del_card_id { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public virtual EnumCardSave card_save { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.conv_code")]
        public virtual EnumConvCode? conv_code { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("regular_detail_t.next_sendtime")]
        public virtual int? next_sendtime { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("regular_detail_t.weekend_operation")]
        public virtual EnumWeekendOperation weekend_operation { get; set; }

        public short DisplayedDelvMethod
        {
            get
            {
                if (this.cart != null && this.cart.DisplayedDelvMethod.HasValue)
                {
                    return this.cart.DisplayedDelvMethod.Value;
                }

                return 3;
            }
        }

        //カード入力フォーム表示フラグ
        private int entryCardInfoCount { get { return ErsFactory.ersUtilityFactory.getSetup().entryCardInfoCount; } }

        #endregion

        #region "inputs at entry3"

        [ErsOutputHidden("input_point")]
        [ErsSchemaValidation("d_master_t.coupon_code")]
        public string coupon_code { get; set; }

        [ErsOutputHidden("input_point")]
        [ErsSchemaValidation("d_master_t.coupon_code")]
        public string ent_coupon_code { get; set; }

        string _invalid_coupon_code;
        public string validated_ent_coupon_code
        {
            get
            {
                if (_invalid_coupon_code.HasValue())
                {
                    return _invalid_coupon_code;
                }

                return ent_coupon_code;
            }
            set
            {
                _invalid_coupon_code = value;
            }
        }

        //クーポン割引額
        public int? coupon_discount
        {
            get
            {
                if (this.order == null)
                {
                    return null;
                }

                ///Return 0 if entered coupon code is invalid
                if (this._invalid_coupon_code.HasValue())
                {
                    return null;
                }

                return order.coupon_discount;
            }
        }

        #endregion

        #region "values used in view"

        public string d_no
        {
            get
            {
                if (this.order == null)
                {
                    return string.Empty;
                }

                return this.order.d_no;
            }
        }

        public string pay_memo
        {
            get
            {
                if (order == null)
                {
                    return string.Empty;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetPaymentMessageFromId(order.pay);
            }
        }

        //個人情報を入力するか否か
        public bool needPersonalData
        {
            get
            {
                return (k_flg == EnumMemberEntryMode.NO_MEMBER);
            }
        }

        public bool ExistCreditPay
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPayService().ExistValue(EnumPaymentType.CREDIT_CARD); }
        }

        public bool existCreditData
        {
            get
            {
                return (this.ListCreditCardInfo != null && this.ListCreditCardInfo.Count() != 0);
            }
        }

        //カード情報入力フォームの表示フラグ
        public Boolean newEntryFlg
        {
            get
            {

                return (this.ListCreditCardInfo==null || this.ListCreditCardInfo.Count() <= entryCardInfoCount);
            }
        }

        public virtual string w_card
        {
            get
            {
                if (order != null && order.card_info != null)
                {
                    return order.card_info.card_name;
                }

                return string.Empty;
            }
        }

        public virtual string cardno_u4
        {
            get
            {
                if (order != null && order.card_info != null)
                {
                    return order.card_info.card_no_u4;
                }

                return string.Empty;
            }
        }

        public virtual string w_card_save
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.CardSave, EnumCommonNameColumnName.namename, (int?)this.card_save);
            }
        }


        public virtual DateTime? birth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetBirthDay(this.birthday_y, this.birthday_m, this.birthday_d);
            }
        }

        public int point
        {
            get
            {
                Setup setup = ErsFactory.ersUtilityFactory.getSetup();
                int? site_id = null;
                //if (!setup.member_centralization) this condition was removed to retrieved the member point for each site (1 or 5)
                //{
                    site_id = setup.site_id;
                //}
                return ErsFactory.ersMemberFactory.GetObtainMemberPointStgy().GetPoint(ErsContext.sessionState.Get("mcode"), site_id);
            }
        }

        public string w_sex
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename, (int?)this.sex);
            }
        }

        public string w_job
        {
            get
            {
                if (this.job == null)
                {
                    return string.Empty;
                }
                else
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewJobService().GetStringFromId(this.job);
                }
            }
        }

        public string w_m_flg
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.MFlg, EnumCommonNameColumnName.namename, (int?)this.m_flg);
            }
        }

        //秘密の質問表示表示用
        public string w_ques
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewQuesService().GetStringFromId(this.ques);
            }
        }

        public virtual string w_pref
        {
            get
            {
                if (this.pref == null)
                {
                    return string.Empty;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(this.pref);
            }
        
        }

        public string w_add_pref
        {
            get
            {
                if (this.add_pref == null)
                {
                    return string.Empty;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(this.add_pref);
            }
        }

        public virtual string w_deliv_method
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.DelvMethod, EnumCommonNameColumnName.namename, (int?)this.deliv_method);
            }
        }

        public virtual bool canSelectMailDelv
        {
            get
            {
                return this.cart != null
                    && this.cart.basketItems != null
                    && this.cart.basketItems.Count() == 1
                    && this.cart.basketItems.First().baskRecord.deliv_method == EnumDelvMethod.Mail
                    && this.cart.basketItems.First().baskRecord.amount == 1
                    && (this.cart.regularBasketItems == null || this.cart.regularBasketItems.Count() == 0);
            }
        }

        public virtual string w_pay
        {
            get
            {
                if (order == null)
                {
                    return string.Empty;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(order.pay);
            }
        }

        public string w_send
        {
            get
            {
                if (order == null)
                {
                    return string.Empty;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.SendTo, EnumCommonNameColumnName.namename, (int)order.send.Value);
            }
        }

        public string w_senddate_no_select
        {
            get
            {
                if (this.senddate == null)
                {
                    return ErsResources.GetFieldName("senddate_no_select");
                }

                return string.Empty;
            }
        }

        public string w_sendtime
        {
            get
            {
                if (order != null && order.sendtime != null)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId(order.sendtime);
                }
                else
                {
                    return string.Empty;
                }

            }
        }

        public string w_regular_sendtime
        {
            get
            {
                if (this.regular_sendtime != null)
                    return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId((int)this.regular_sendtime);
                else
                    return string.Empty;

            }
        }

        public virtual string w_address_add
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.AddressAdd, EnumCommonNameColumnName.namename, (int?)this.address_add);
            }
        }

        public virtual string pri_memo
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.pri_memo;
            }
        }

        public string w_mformat
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.MFormat, EnumCommonNameColumnName.namename, (int?)this.mformat);
            }
        }

        public int tgt_user_born
        {
            get { return ErsFactory.ersUtilityFactory.getSetup().tgt_user_born; }
        }

        public string w_weekend_operation { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.WeekendOperation, EnumCommonNameColumnName.namename, (int?)this.weekend_operation); } }

        public virtual string w_conv_code
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ConvCode, EnumCommonNameColumnName.namename, (int?)this.conv_code); }
        }

        public List<Dictionary<string, object>> prefList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> jobList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewJobService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> birthday_yList
        {
            get
            {
                var min_user_age = ErsFactory.ersUtilityFactory.getSetup().min_user_age;
                var max_user_age = ErsFactory.ersUtilityFactory.getSetup().max_user_age;
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListYear(max_user_age, min_user_age);
        }
        }

        public List<Dictionary<string, object>> birthday_mList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListMonth(); }
        }

        public List<Dictionary<string, object>> birthday_dList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListDay(); }
        }

        //セキュリティアンサープルダウン
        public List<Dictionary<string, object>> quesList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewQuesService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> deliv_methodList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.DelvMethod, EnumCommonNameColumnName.namename); }
        }

        public virtual IEnumerable<Dictionary<string, object>> payList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPayService().SelectAsList(this.cart == null || this.cart.regularBasketItems == null || this.cart.regularBasketItems.Count() > 0); }
        }

        public List<Dictionary<string, object>> cardList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCardService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> sendtimeList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> senddateList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().SelectAsList(null, DateTime.Now.AddDays(ErsFactory.ersUtilityFactory.getSetup().sendday_count)); }
        }

        public List<Dictionary<string, object>> mformatList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MFormat, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> sexList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> m_flgList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MFlg, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> sendList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.SendTo, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> weekend_operationList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.WeekendOperation, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> convenienceList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ConvCode, EnumCommonNameColumnName.namename); }
        }

        public Dictionary<string, object> fromDate
        {
            get
            {
                var ret = new Dictionary<string, object>();

                var fromDate = ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetFromDate(DateTime.Now);

                if (fromDate.HasValue)
                {
                    ret["year"] = fromDate.Value.Year;
                    ret["month"] = fromDate.Value.Month;
                    ret["day"] = fromDate.Value.Day;
                }

                return ret;
            }
        }

        public int? order_subtotal
        {
            get
            {
                if (order == null)
                {
                    return null;
                }
                return order.subtotal;
            }
        }

        public int? order_tax
        {
            get
            {
                if (order == null)
                {
                    return null;
                }
                return order.tax;
            }
        }

        public int? disp_carriage
        {
            get
            {
                if (order == null)
                {
                    return null;
                }
                return order.carriage;
            }
        }

        public int? etc
        {
            get
            {
                if (order == null)
                {
                    return null;
                }
                return order.etc;
            }
        }

        public int? order_total
        {
            get
            {
                if (order == null)
                {
                    return null;
                }

                return order.total;
            }
        }

        public virtual string w_etc
        {
            get
            {
                if (order == null)
                {
                    return string.Empty;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetEtcNameFromId(order.pay);
            }
        }

        public string w_wrap
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Wrap, EnumCommonNameColumnName.namename, (int?)this.wrap);
            }
        }

        public IEnumerable<ErsBaskRecord> orderRecords
        {
            get
            {
                if(this.cart==null)
                {
                    yield break;
                }

                if (this.cart.basket == null)
                {
                    yield break;
                }

                foreach (var record in this.cart.basket.objBasketRecord.Values)
                {
                    record.sendtime = this.sendtime;
                    yield return record;
                }

                foreach (var record in this.cart.basket.objRegularBasketRecord.Values)
                {
                    record.sendtime = this.regular_sendtime;
                    yield return record;
                }
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public virtual void setBasketCookie()
        {
            //TODO:配置場所を考える
            ErsContext.sessionState.Add("basket_total", "0");
            ErsContext.sessionState.Add("basket_count", "0");
        }

        public string d_noList
        {
            get
            {
                if (this.order == null)
                {
                    return null;
                }

                var listResult = new List<string>();
                foreach (var innerOrder in this.order.GetListOrder())
                {
                    listResult.Add(innerOrder.OrderHeader.d_no);
                }

                return string.Join(",", listResult);
            }
        }

        public IEnumerable<Dictionary<string, object>> orderHeaderList
        {
            get
            {
                if (this.order == null)
                {
                    yield break;
                }

                var listResult = new List<string>();
                foreach (var innerOrder in this.order.GetListOrder())
                {
                    var dictionary = innerOrder.OrderHeader.GetPropertiesAsDictionary();
                    dictionary["w_ccode"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ConvCode, EnumCommonNameColumnName.namename, (int?)innerOrder.OrderHeader.conv_code);
                    yield return dictionary;
                }
            }
        }

        /// <summary>
        /// 環境依存のデータセット
        /// </summary>
        public virtual EnumPmFlg pm_flg
        {
            get
            {
                return EnumPmFlg.PC;  //0:PC
            }
        }


        public bool IsNonNeededPaymentSpec
        {
            get
            {
                return ErsFactory.ersOrderFactory.GetIsNonNeededPaymentSpec().IsSpecified(this.order_total);
            }
        }

        public bool IsAllBasketItemMailDelivery
        {
            get
            {
                return ErsFactory.ersOrderFactory.GetIsAllBasketItemMailDeliverySpec().Satisfy(ErsContext.sessionState.Get("ransu"));
            }
        }

        public string security_no { get; set; }

        public string c_req_no { get; set; }

        public int? sendday
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.sendday;
            }
        }

        public int? sendday_count
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.sendday_count;
            }
        }

        public List<DateTime?> listHolidays
        {
            get
            {
                var repository = ErsFactory.ersOrderFactory.GetErsCalendarRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsCalendarCriteria();
                criteria.close_date_from = DateTime.Now.AddDays(this.sendday.Value);
                criteria.close_date_to = DateTime.Now.AddDays(this.sendday_count.Value);
                var listCalendar = repository.Find(criteria);
                var listHolidays = new List<DateTime>();
                return listCalendar.Select((calendar) => calendar.close_date).ToList();
            }
        }

        public bool ValidateMappedOrder { get; set; }

        public bool overseas_shipping
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return (this.pref == (int)EnumPrefecture.OVERSEAS);
                }
                else
                {
                    return (this.add_pref == (int)EnumPrefecture.OVERSEAS);
                }
            }
        }


        #region for purchase mail.

        public string purchasedmail_fullname
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return string.Format("{0} {1}", this.lname, this.fname);
                }

                return string.Format("{0} {1}", this.add_lname, this.add_fname);
            }
        }

        public string purchasedmail_zip
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return this.zip;

                return this.add_zip;
            }
        }

        public string purchasedmail_pref_name
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return this.w_pref;

                return this.w_add_pref;
            }
        }

        public string purchasedmail_address
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return this.address;
                }

                return this.add_address;
            }
        }

        public string purchasedmail_taddress
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return this.taddress;
                }

                return this.add_taddress;
            }
        }

        public string purchasedmail_maddress
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return this.maddress;
                }

                return this.add_maddress;
            }
        }
        
        #endregion


    }
}