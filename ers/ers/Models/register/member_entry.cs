﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member.strategy;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers;
using ers.Domain.Register.Commands;
using ers.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;

namespace ers.Models
{
    public class MemberEntry
        : ErsFrontModelBase,
        //CASTのためにコマンドを定義
        IMemberEntryCommand
    {

        public MemberEntry()
        {
            //初期値入力
            m_flg = EnumMFlg.Deliver;
        }

        /// <summary>
        /// 会員番号
        /// </summary>
        public string mcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.lname")]
        public string lname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fname")]
        public string fname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.lnamek")]
        public string lnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fnamek")]
        public string fnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.email")]
        public string email { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.email")]
        public string email_confirm { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.mformat")]
        public EnumMformat? mformat { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tel")]
        public virtual string tel { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.zip")]
        public virtual string zip { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.pref")]
        public virtual int? pref { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.address")]
        public virtual string address { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.taddress")]
        public virtual string taddress { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.maddress")]
        public virtual string maddress { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = ErsViewBirthdayService.minimumOfSelectyear, rangeTo = ErsViewBirthdayService.maximumOfSelectyear)]
        public int? birthday_y { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 12)]
        public int? birthday_m { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 31)]
        public int? birthday_d { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.sex")]
        public EnumSex? sex { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.passwd")]
        public string passwd { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.passwd")]
        public string passwd_confirm { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.ques")]
        public int? ques { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.ans")]
        public string ans { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.compname")]
        public virtual string compname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.compnamek")]
        public virtual string compnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.division")]
        public virtual string division { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.divisionk")]
        public virtual string divisionk { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tlname")]
        public virtual string tlname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tfname")]
        public virtual string tfname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tlnamek")]
        public virtual string tlnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tfnamek")]
        public virtual string tfnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fax")]
        public virtual string fax { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.job")]
        public int? job { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.m_flg")]
        public EnumMFlg? m_flg { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 1)]
        public int? pri_chk { get; set; }

        public int tgt_user_born
        {
            get { return ErsFactory.ersUtilityFactory.getSetup().tgt_user_born; }
        }

        //セキュリティアンサープルダウン
        public List<Dictionary<string, object>> quesList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewQuesService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> mformatList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MFormat, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> sexList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> m_flgList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MFlg, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> prefList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> jobList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewJobService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> birthday_yList
        {
            get
            {
                var min_user_age = ErsFactory.ersUtilityFactory.getSetup().min_user_age;
                var max_user_age = ErsFactory.ersUtilityFactory.getSetup().max_user_age;
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListYear(max_user_age, min_user_age);
            }
        }

        public List<Dictionary<string, object>> birthday_mList
        {
             get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListMonth(); }
        }

        public List<Dictionary<string, object>> birthday_dList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListDay(); }
        }

        public string w_pref
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(this.pref);
            }
        }

        //秘密の質問表示表示用
        public string w_ques
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewQuesService().GetStringFromId(this.ques);
            }
        }

        public string w_mformat
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.MFormat, EnumCommonNameColumnName.namename, (int?)this.mformat);
            }
        }

        public string w_m_flg
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.MFlg, EnumCommonNameColumnName.namename, (int?)this.m_flg);
            }
        }

        public DateTime? birth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetBirthDay(this.birthday_y, this.birthday_m, this.birthday_d);
            }
        }

        public string w_sex
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename, (int?)this.sex);
            }
        }

        public string w_job
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewJobService().GetStringFromId(this.job);
            }
        }

        public virtual string pri_memo
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.pri_memo;
            }
        }
    }
}