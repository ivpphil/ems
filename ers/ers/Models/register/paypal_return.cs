﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.Payment;
using System.ComponentModel;
using jp.co.ivp.ers;
using ers.Domain.Register.Commands;
using ers.Domain.Register.Mappables;

namespace ers.Models
{
    public class Paypal_return
        : ErsBindableModel, IPaypalReturnCommand, IPaypalReturnMappable
    {
        public Register register { get; set; }

        public Cart cart { get; set; }

        [ErsUniversalValidation]
        [DisplayName("ransu")]
        public string val { get; set; }

        [ErsUniversalValidation]
        [DisplayName("ransu")]
        public string token { get; set; }

        [ErsUniversalValidation]
        [DisplayName("ransu")]
        public string PayerID { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 1)]
        [DisplayName("ransu")]
        public int? monitor_flg { get; set; }

        [ErsSchemaValidation("ransu_t.ssl_ransu")]
        [DisplayName("ransu")]
        public string ssl_ransu { get; set; }

        [ErsSchemaValidation("ransu_t.mcode")]
        [DisplayName("ransu")]
        public string mcode { get; set; }
    }
}