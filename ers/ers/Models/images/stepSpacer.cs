﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using ers.Domain.Images.Commands;
using ers.jp.co.ivp.ers.mvc;

namespace ers.Models.images
{
    public class stepSpacer
        : ErsFrontModelBase, IStepSpacerCommand
    {
        /// <summary>
        /// ＠メールプロセスID
        /// </summary>
        [ErsSchemaValidation("mail_url_click_counter_t.process_id")]
        [DisplayName("stepSpacer.id")]
        public int? id { get; set; }

        /// <summary>
        /// 暗号化メールアドレス
        /// </summary>
        [ErsUniversalValidation(type=CHK_TYPE.OneByteCharacter)]
        [DisplayName("stepSpacer.add")]
        public string add { get; set; }
    }
}