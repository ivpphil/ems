﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jp.co.ivp.ers
{
    public enum EnumMemberEntryMode
    {
        /// <summary>
        /// 1: 会員
        /// </summary>
        MEMBER = 1,

        /// <summary>
        /// 2: 非会員
        /// </summary>
        NO_MEMBER,

        /// <summary>
        /// 3: 会員登録なし
        /// </summary>
        ANONYMOUS
    }
}