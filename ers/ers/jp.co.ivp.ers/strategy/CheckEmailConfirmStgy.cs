﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.member.strategy
{
    public class CheckEmailConfirmStgy
    {
        /// <summary>
        /// EmailとEmail確認の入力が一緒か
        /// </summary>
        /// <returns></returns>
        public static ValidationResult CheckEmailConfirm(string email, string email_confirm)
        {
            if (email != email_confirm)
            {
                return new ValidationResult(string.Format(ErsResources.GetMessage("30033"), ErsResources.GetFieldName("email"), ErsResources.GetFieldName("email_confirm")), new[] { "email", "email_confirm" });
            }

            return null;
        }

    }
}
