﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member.strategy
{
    public class CheckExitMemberWithEmailStgy
    {
        /// <summary>
        /// メアドをキーに会員の存在チェック
        /// </summary>
        /// <returns></returns>
        public virtual ValidationResult CheckExitMember(string email)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();

            criteria.email = email;

            if (repository.GetRecordCount(criteria) != 1)
                return new ValidationResult(ErsResources.GetMessage("30031"), new[] { "email" });

            return null;
        }
    }
}
