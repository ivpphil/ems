﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.member.strategy
{
    public class ObtainMemberPointStgy
    {
        protected Dictionary<string, ErsPointHistory> pointCache
        {
            get
            {
                if (ErsCommonContext.GetPooledObject("pointCache") == null)
                    pointCache = new Dictionary<string, ErsPointHistory>();

                return (Dictionary<string, ErsPointHistory>)ErsCommonContext.GetPooledObject("pointCache");
            }
            set
            {
                ErsCommonContext.SetPooledObject("pointCache", value);
            }
        }

        public virtual int GetPoint(ErsMember member)
        {
            if (member.memberType == MemberType.NON_REGISTERD)
            {
                return 0;
            }

            var pointHistory = this.GetPointHistory(member);

            if (pointHistory != null)
                return pointHistory.total_p;
            else
                return 0;
        }

        public virtual ErsPointHistory GetPointHistory(ErsMember member)
        {

            if (!pointCache.ContainsKey(member.mcode))
            {
                //ポイント用リポジトリクラス　インスタンス化処理
                var repository = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryRepository();

                //ポイント用クライテリアクラス　インスタンス化処理
                var pointCri = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryCriteria();

                //検索条件をクライテリアへ
                pointCri.mcode = member.mcode;

                pointCri.SetOrderByDt(Criteria.OrderBy.ORDER_BY_DESC);

                pointCri.LIMIT = 1;

                IList<ErsPointHistory> list = repository.Find(pointCri);
                if (list.Count > 0)
                {
                    pointCache[member.mcode] = list[0];
                }
                else
                {
                    pointCache[member.mcode] = null;
                }
            }

            return pointCache[member.mcode];
        }
    }
}
