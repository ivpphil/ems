﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member.strategy
{
    /// <summary>
    /// Checks Whether the member already exists. Uses member name and password.
    /// </summary>
    public class CheckDuplicateEmailAndPasswordStgy
    {
        /// <summary>
        /// Return true if the same member exists.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public static ValidationResult CheckDuplicate(string mcode, string email, string passwd)
        {
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();

            criteria.mcode_not_equal = mcode;
            criteria.email = email;
            criteria.passwd = passwd;

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();

            if (repository.GetRecordCount(criteria) > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("30034", ErsResources.GetFieldName("email"), ErsResources.GetFieldName("passwd")), new[] { "email", "email_confirm", "passwd", "passwd_confirm" });
            }

            return null;
        }

    }
}
