﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member.strategy
{
    public class CheckQuesConfirmStgy
    {
        /// <summary>
        /// セキュリティアンサー一致チェック
        /// </summary>
        public virtual ValidationResult CheckQuesConfirm(string mcode, string ans, int ques)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();

            criteria.mcode = mcode;
            criteria.ans = ans;
            criteria.ques = ques;

            if (repository.GetRecordCount(criteria) != 1)
            {
                return new ValidationResult(ErsResources.GetMessage("30032"), new[] { "ans" });
            }

            return null;

        }

    }
}
