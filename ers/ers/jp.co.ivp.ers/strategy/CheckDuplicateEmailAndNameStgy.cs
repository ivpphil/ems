﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member.strategy
{
    /// <summary>
    /// Checks Whether the member already exists. Uses member name and e-mail.
    /// </summary>
    public class CheckDuplicateEmailAndNameStgy
    {
        /// <summary>
        /// Return true if the same member exists.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public static ValidationResult CheckDuplicate(string mcode, string email, string lname, string fname)
        {
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();

            criteria.mcode_not_equal = mcode;
            criteria.email = email;
            criteria.lname = lname;
            criteria.fname = fname;

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();

            if (repository.GetRecordCount(criteria) > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("30035", ErsResources.GetFieldName("email"), ErsResources.GetFieldName("lname"), ErsResources.GetFieldName("fname")), new[] { "email", "email_confirm", "lname", "fname" });
            }

            return null;
        }

    }
}
