﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc
{
    public class ErsFrontProcessCompletionAttribute
        : ErsProcessCompletionAttributeBase
    {
        public ErsFrontProcessCompletionAttribute(string completedPageKeys)
            : base(completedPageKeys)
        {
        }

        public override void OnCheckCompletionError()
        {
            throw new ErsException("10042");
        }
    }
}