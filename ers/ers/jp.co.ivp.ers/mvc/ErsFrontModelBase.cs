﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ers.jp.co.ivp.ers.mvc
{
    public class ErsFrontModelBase
        : ErsModelBase
    {
        public bool enableTax
        {
            get
            {
                return (ErsFactory.ersUtilityFactory.getSetup().tax > 0);
            }
        }
    }
}