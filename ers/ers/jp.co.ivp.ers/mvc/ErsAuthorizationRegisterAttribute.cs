﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jp.co.ivp.ers.mvc
{
    public class ErsAuthorizationRegisterAttribute
        : ErsAuthorizationAttribute
    {
        /// <summary>
        /// 新規登録の場合はログイン状態を要求しない
        /// </summary>
        /// <param name="filterContext"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public override void SessionCheck(AuthorizationContext filterContext)
        {
            EnumMemberEntryMode k_flg;
            if (Enum.TryParse<EnumMemberEntryMode>(filterContext.HttpContext.Request["k_flg"], out k_flg))
            {
                if (k_flg != EnumMemberEntryMode.MEMBER)
                {
                    //会員でないときはセッション状態を常にクリアする。
                    ErsContext.sessionState.ClearLoginData();
                    return;
                }
            }

            base.SessionCheck(filterContext);
        }
    }
}