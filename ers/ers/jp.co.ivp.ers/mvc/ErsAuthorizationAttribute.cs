﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.state;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class ErsAuthorizationAttribute
        : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var controler = (ErsControllerSecure)filterContext.Controller;

            object[] obj = filterContext.ActionDescriptor.GetCustomAttributes(typeof(NoNeedSessionAttribute), false);
            if (obj.Length == 0)
            {
                this.SessionCheck(filterContext);
            }
        }

        /// <summary>
        /// セッションチェックを行うか否か。
        /// </summary>
        public virtual void SessionCheck(AuthorizationContext filterContext)
        {
            //Include language processing to view in error page.
            ErsLanguageMenuAttribute language= new ErsLanguageMenuAttribute();
            language.ProcessLanguage(filterContext);

            ErsContext.SessionCheck(filterContext);
        }
    }
}