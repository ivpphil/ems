﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc
{
    public class ErsMypageProcessCompletionAttribute
        : ErsProcessCompletionAttributeBase
    {
        public ErsMypageProcessCompletionAttribute(string completedPageKeys)
            : base(completedPageKeys)
        {
        }

        public override void OnCheckCompletionError()
        {
            throw new ErsException("10043", ErsFactory.ersUtilityFactory.getSetup().sec_url + ErsFactory.ersUtilityFactory.getSetup().mypage_url);
        }
    }
}