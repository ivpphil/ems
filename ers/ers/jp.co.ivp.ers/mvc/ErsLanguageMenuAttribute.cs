﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple=false)]
    public class ErsLanguageMenuAttribute
           : FilterAttribute, IActionFilter
    {
        
        public ErsLanguageMenuAttribute()
        {
            
        }

        #region IActionFilter メンバー

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            this.ProcessLanguage(filterContext);
        }

        public void ProcessLanguage(ControllerContext filterContext)
        {
            var controllerName = Convert.ToString(filterContext.RouteData.Values["controller"]);
            var actionName = Convert.ToString(filterContext.RouteData.Values["action"]);

            if (string.IsNullOrEmpty(controllerName))
            {
                return;
            }

            ErsLanguageMenuModel langModel = new ErsLanguageMenuModel(actionName, controllerName);

            if ((langModel.langList == null || langModel.langList.Count == 0))
            {
                throw new ErsException("63107");
            }

            var controller = (ErsControllerBase)filterContext.Controller;
            var model = controller.GetBindedModel<ErsLanguageMenuModel>();



            if (!controller.HasAdditionalModel("langModel"))
            {
                langModel.lang = model.lang;
                controller.AddModelToView("langModel", langModel);
            }
        }
            

        #endregion
    }
}