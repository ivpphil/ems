﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ers.Models;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class ErsSideMenuAttribute
        : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var controler = (ErsControllerBase)filterContext.Controller;

            var sideModel = new sideMenu();

            var objc = new OtherCookie();
            sideModel.cookie_email = objc.GetCoookieEmail("login_email");
            if (sideModel.cookie_email.HasValue())
            {
                sideModel.email_chk = true;
            }

            //サイドメニュー用のモデルを追加
            controler.AddModelToView("sideModel", sideModel);
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }
    }
}