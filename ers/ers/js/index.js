/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function(){
	var ersObj = ErsIndex();
	ersObj.simgChange();
});

/* ErsIndexオブジェクト生成コンストラクタ */
var ErsIndex = function(){

	var that = {};

	/* 商品画像切り替え処理
	 ---------------------------------------------------------------- */
	that.simgChange = function(){
		var domBimg = $("#ERS_keyvisual h2 img");	//大画像DOM
		var path = "";		//クリックされた画像パス
		var path1 = "";		//クリックされた画像パス

		$("#ERS_keyvisual h3 img").click(function(){
			$("#ERS_keyvisual h3 img").removeClass("bnr_on"); 

			$("#ERS_keyvisual h3 img").each(function(){　
				var newurl = $(this).attr('src').replace("tops_over","tops");
				$(this).attr('src', newurl);
			});

			$(this).addClass("bnr_on");
			path = $(this).attr("src");

			path1 = path.replace("tops","tops_over");
			$(this).attr("src",path1);

			path = path.replace("tops","topb");
			domBimg.fadeOut("fast",function(){
				domBimg.attr("src",path)
				$(this).fadeIn("fast");
			});
		});
	}

	return that;
}

