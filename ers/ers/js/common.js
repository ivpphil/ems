/*============================================================================
	Author and Copyright
		製作者: IVP CO,LTD（http://www.ivp.co.jp/）
		作成日: 2009-03-06
		修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function(){
	var ersObj = ErsCommon();
	ersObj.welcome();
	ersObj.basketDisplay();
	ersObj.imgChange();
	ersObj.imgChangeOpacity();
	ersObj.inputFocus();
	ersObj.smoothScroll();
	ersObj.DblClickCK();
//	ersObj.setJqueryUiDatePicker();
//	ersObj.autoEmailInput();
});


/* ErsCommonオブジェクト生成コンストラクタ */
var ErsCommon = function () {

	var that = {};

	/* ヘッダー部分・ようこそ・ログインボックス表示
	 ---------------------------------------------------------------- */
    that.welcome = function () {
		var objNicknameDiv;				//nicknameが入るdivタグオブジェクト
    var strNickname = "";
    var LoginCheck =
            setInterval(function(){
            	strNickname = ErsLib.getPageInfo()["strNickname"];
            	if (strNickname != "")
            	{

				//ログイン時に表示

		        if (ErsLib.isLogin()) {
		            //			objNicknameDiv = $("#ERS_header h2 span");
		            //			if(ErsLib.getPageInfo()["strFilename"] !=  "entry3.asp" && objNicknameDiv.size() != 0){
		            //				objNicknameDiv.html("ようこそ！　" + strNickname + "&nbsp;さん！");
		            //			}

		            $(".login_form h3.welcome").html("ようこそ！<br/><strong>" + strNickname + "</strong>&nbsp;様"); //ニックネーム表示
		            $(".login_form h3.welcome").css("display", "block"); //ようこそ～様
		            $(".login_form p.logout").css("display", "block"); //ログアウト
		            $(".login_form p.mypage").css("display", "block"); //マイページ
		            $(".login_form div").css("display", "none"); //ログアウト
			            $(".non_login_disp").css("display", "none"); //会員ログイン画像

		            $("article.logout").css("display", "block"); //ログアウト
		            $("article ul li.mypage").css("display", "none"); //マイページ
		            $("article ul li.logout").css("display", "block"); //マイページ

				}
				            		clearInterval(LoginCheck)
            	}
            },100);


	}

	/* カート内容表示
	 ---------------------------------------------------------------- */
    that.basketDisplay = function () {
        var host = location.host;
        var protocol = location.protocol;

        $.ajax({
            type: "GET",
            url: protocol + "//" + host + "/top/api/asp/get_cookie_session_prefix.asp",
            dataType: "json"
        }).done(function (retVal) {
            var prefix = retVal.prefix;
            var strTotalPrice = ErsLib.getPageInfo().objCookieList[prefix + "basket_total"]; 	//合計金額文字列
            var strTotalAmount = ErsLib.getPageInfo().objCookieList[prefix + "basket_count"]; 	//合計点数文字列

            if (!strTotalPrice) {
                strTotalPrice = "0"; //0を表示
            }

            while (strTotalPrice != (strTotalPrice = strTotalPrice.replace(/^(-?\d+)(\d{3})/, "$1,$2"))); //3桁カンマ区切り

            $("strong.price").html(strTotalPrice + "<span>&nbsp;円</span>"); //合計金額表示


            if (!strTotalAmount) {
                strTotalAmount = 0; //0を表示
            }
            $("strong.amount").html(strTotalAmount); //合計点数表示

        }).fail(function (retVal) {
            return false;
        });
    }

	/* ロールオーバーイメージ表示
	 ---------------------------------------------------------------- */
    that.imgChange = function () {
		var img_out; //mouseout時のsrc属性を格納
		var img_in; //mouseover時のsrc属性を格納
		var objChangeImg = $(".change");
		var hoverDom = {	//マウスオーバーされているDOM要素
            "tarDom": "",
            "img_out": ""
		};

        objChangeImg.hover(function () {
			//mouseover時の処理
			img_out = $(this).attr("src");
            img_in = img_out.replace("_off.gif", "_on.gif");
            $(this).attr("src", img_in);
			hoverDom = {
                "tarDom": $(this),
                "img_out": img_out
			};
		},
		function () {
			//mouseout時の処理
		    $(this).attr("src", img_out);
		});

		//ページ移動時にロールオーバーを戻す
        $(window).unload(function () {
			//ブラウザバックの時にエラーになるため、try～catch
            try {
                hoverDom["tarDom"].attr("src", hoverDom["img_out"]);
            } catch (e) {
				//何もしない
			}
		});
	}

	/* ロールオーバーイメージ表示処理パターン２
	 ---------------------------------------------------------------- */
    that.imgChangeOpacity = function () {
		var objChangeImg = $(".change_opacity");

        objChangeImg.hover(function () {
			//mouseover時の処理
            $(this).css({ opacity: "0.9" });
		},
		function () {
			//mouseout時の処理
		    $(this).css({ opacity: "1" });
		});
	}

	/* inputフォーカス時に背景色変更
	 ---------------------------------------------------------------- */
    that.inputFocus = function () {
		var bgColor = "#ffF4e1"
		var tarDom = $("input[type='text'],input[type='password'],textarea");

        tarDom.focus(function () {
            $(this).css("backgroundColor", bgColor);
		});

        tarDom.blur(function () {
            $(this).css("backgroundColor", "");
		});
	}


	/* email自動入力
	 ---------------------------------------------------------------- */
    /*	that.autoEmailInput = function(){
		var objCookie = ErsLib.getCookie();
		if(objCookie["erssession_email"] !== ""){
			$("#ERS_sidemenu2 #email_pc").val(objCookie["erssession_email"]);
		}
	}
    */

	/* スムーススクロール
	 ---------------------------------------------------------------- */
    that.smoothScroll = function () {
        var a_list = $("a[href^='#']").click(function (e) {
			var end = $(this).attr("href");
			ahead = end.substr(1);
			start_scroll(ahead);
			return false;
		});

        function start_scroll(end) {
			//出発地、到着地の要素取得
			var end_posi;
            try {
				end_posi = $("#" + end).get(0);
            } catch (e) {
                try {
					end_posi = $("@name=" + end).get(0);
                } catch (e) {
					return false;
				}
			}

			//出発地、到着地の座標取得処理
			var start_co;
			var screen_size;
			var doc_height = document.body.offsetHeight;
			var end_co = end_posi.offsetTop;

            if (ErsLib.isIE()) { // IE判別
				start_co = document.documentElement.scrollTop;
				screen_size = document.documentElement.clientHeight;
			}
            else {
				start_co = window.pageYOffset;
				screen_size = window.innerHeight;
			}

            if (doc_height - end_co < screen_size) {
				end_co = doc_height - screen_size;
			}

			var sabun = end_co - start_co;
            if (sabun == 0) return false;

			//スクロールスピードとかの設定
            var speed = Math.round(sabun / 10); //スクロールスピードの設定
            var riding = Math.round(sabun * 1 / 2); //着地の設定
			var scroll = window.scrollBy;
			var page_ck; //ページ下まで行った時のチェック用
			var doc_ele = document.documentElement;
			// マウス操作でスクロールストップ
            if (window.addEventListener) window.addEventListener("DOMMouseScroll", scroll_stop, false);
				window.onmousewheel = document.onmousewheel = scroll_stop;
            function scroll_stop() {
					window.clearInterval(scroll_trigger);
				}

			// スクロールスタートのトリガー
            if (sabun < 0) { // 下から上へ移動する場合
                if (ErsLib.isIE()) { // IE用処理
                    var scroll_trigger = setInterval(up_timer_ie, 2);
				}
                else {
                    var scroll_trigger = setInterval(up_timer, 2);
				}
			}
            else { // 上から下へ移動する場合
                if (ErsLib.isIE()) { // IE用処理
                    var scroll_trigger = setInterval(bottom_timer_ie, 2);
				}
                else {
					window.clearInterval(scroll_trigger);
                    var scroll_trigger = setInterval(bottom_timer, 2);
				}
			}

			// スクロールさせる関数
			// 下から上へスクロールさせる場合（InternetEXplorer用）
            function up_timer_ie() { // 
                if (end_co - doc_ele.scrollTop < riding) {
                    scroll(0, speed);
				}
                else if ((end_co - doc_ele.scrollTop) / 10 < -1) {
                    scroll(0, (end_co - doc_ele.scrollTop) / 10);
				}
                else {
                    scroll(0, -1);
                    if (end_co - doc_ele.scrollTop == 0) window.clearInterval(scroll_trigger);
				}
			}

			// 下から上へスクロールさせる場合（IE以外のモダンブラウザ用）
            function up_timer() {
                if (end_co - window.pageYOffset < riding) {
                    scroll(0, speed);
				}
                else if ((end_co - window.pageYOffset) / 10 < -1) {
                    scroll(0, (end_co - window.pageYOffset) / 10);
				}
                else {
                    scroll(0, -1);
                    if (end_co - window.pageYOffset == 0) window.clearInterval(scroll_trigger);
				}
			}

			// 上から下へスクロールさせる場合（InternetEXplorer用）
            function bottom_timer_ie() {
                if (end_co - doc_ele.scrollTop > riding) {
                    page_ck = end_co - doc_ele.scrollTop;
                    scroll(0, speed);
                    if (page_ck == end_co - doc_ele.scrollTop) window.clearInterval(scroll_trigger);
				}
                else if ((end_co - doc_ele.scrollTop) / 10 > 1) {
                    page_ck = end_co - doc_ele.scrollTop;
                    scroll(0, page_ck / 10);
                    if (page_ck == end_co - doc_ele.scrollTop) window.clearInterval(scroll_trigger);
				}
                else {
                    scroll(0, 1);
                    if (end_co - doc_ele.scrollTop == 0) window.clearInterval(scroll_trigger);
				}
			}

			// 上から下へスクロールさせる場合（IE以外のモダンブラウザ用）
            function bottom_timer() {
                if (end_co - window.pageYOffset > riding) {
                    page_ck = end_co - window.pageYOffset;
                    scroll(0, speed);
                    if (page_ck == end_co - window.pageYOffset) window.clearInterval(scroll_trigger);
				}
                else if ((end_co - window.pageYOffset) / 10 > 1) {
                    page_ck = end_co - window.pageYOffset;
                    scroll(0, page_ck / 10);
                    if (page_ck == end_co - window.pageYOffset) window.clearInterval(scroll_trigger);
				}
                else {
                    scroll(0, 1);
                    if (end_co - window.pageYOffset == 0) window.clearInterval(scroll_trigger);
				}
			}
		}
	}

	/* 二度押しチェック
	 ---------------------------------------------------------------- */
    that.DblClickCK = function () {
		var SbmBtn = $("input[type='image'],input[type='submit']");
		var dblflg = 0;
        SbmBtn.click(function (e) {
			//二度押し制限*****************************************
            if ($(this).data('dblflg') != 1) {
				var btn_id = $(this).attr("id");
				//郵便番号検索は何度でもOKにしないとAjax処理が一度しか押せなくなる。
                if ((btn_id != "zip_flg1") && (btn_id != "zip_flg2") && (btn_id != "zip_flg3")) {
					//一度目はフラグを立てる
                    $(this).data('dblflg', 1);
				}
            } else {
				//二度目はボタンを非活性(イメージは変更出来ない)
                $(this).attr("disabled", "true");
				//submit処理を実行飛ばさない
                e.preventDefault();
                e.stopPropagation()
				return false;
			}
		});
	}

	return that;

}

