// faq.js



$(document).ready(function(){


	// initial  ex. faq.html#q09-05-03
	if(window.location.hash != ""){
	
		var jump = "#" + window.location.hash; 
	
		var num_tab = window.location.hash.substr(2,2); 
		var num_dl = window.location.hash.substr(5,2); 
		var num_dd = window.location.hash.substr(8,2);
	
		num_tab--;
		num_dl--; 
		num_dd--; 
	
		num_tab_real = num_tab + 1;
		num_dl_real = num_dl + 1;
		num_dd_real = num_dd + 1;
		
		$(".tab").addClass('tab-hide');
		$(".tab").eq(num_tab).removeClass('tab-hide');
	
		$(".accordion dd").hide();
		var openAccordion = "#tab" + num_tab_real + " .accordion" + num_dl_real +" dd";
		$(openAccordion).eq(num_dd).slideDown();
		
	}
	
	
	// tab
	$(".faqlili li").click(function() {
		var num = $(".faqlili li").index(this);
		$(".tab").addClass('tab-hide');
		$(".tab").eq(num).removeClass('tab-hide');
		/*$(".faqlili li").removeClass('select');
		$(this).addClass('select');*/
	});
	
	
	// accordion
	$(".accordion dt").click(function(){
		var num_dt = $(".accordion dt").index(this);
		$(".accordion dd").slideUp();
		$(".accordion dd").eq(num_dt).slideDown();
	});
	
	
	// accordion all open
	$("a.open").click(function(){
		var parentId = $(this).parent().parent().attr("id");
		var openDd = "#" + parentId + " .accordion dd";
		$(openDd).slideDown();
	});
	
	
	// accordion all close
	$("a.close").click(function(){
		var parentId = $(this).parent().parent().attr("id");
		var closeDd = "#" + parentId + " .accordion dd";
		$(closeDd).slideUp();
	});


});
