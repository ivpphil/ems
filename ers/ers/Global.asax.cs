﻿using System.Web.Mvc;
using System.Web.Routing;
using jp.co.ivp.ers;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.common;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers.coupon;
using jp.co.ivp.ers.faq;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.stepmail;
using jp.co.ivp.ers.summary;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.warehouse;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers.target;
using jp.co.ivp.ers.doc_bundle;
using jp.co.ivp.ers.language;
using jp.co.ivp.ers.ranking;

namespace ers
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : ErsMvcApplication
    {
        protected override string[] GetNamespace()
        {
            return new[] { "ers.Controllers" };
        }

        protected override void SetFactory()
        {
            this.SetMallFactory();

            ErsFactory.ersBasketFactory = new ErsBasketFactory();

            ErsFactory.ersMerchandiseFactory = new ErsMerchandiseFactory();

            ErsFactory.ersMemberFactory = new ErsMemberFactory();

            ErsFactory.ersMailFactory = new ErsMailFactory();

            ErsFactory.ersSessionStateFactory = new ErsSessionStateFactory();

            ErsFactory.ersViewServiceFactory = new ErsViewServiceFactory();

            ErsFactory.ersUtilityFactory = new ErsUtilityFactory();

            ErsFactory.ersOrderFactory = new ErsOrderFactory();

            ErsFactory.ersAddressInfoFactory = new ErsAddressInfoFactory();

            ErsFactory.ersPointHistoryFactory = new ErsPointHistoryFactory();

            ErsFactory.ersAdministratorFactory = new ErsAdministratorFactory();

            ErsFactory.ersTargetFactory = new ErsTargetFactory();

            ErsFactory.ersDocBundleFactory = new ErsDocBundleFactory();

            ErsFactory.ersCouponFactory = new ErsCouponFactory();

            ErsFactory.ersContentsFactory = new ErsContentsFactory();

            ErsFactory.ErsAtMailFactory = new ErsAtMailFactory();

            ErsFactory.ersStepMailFactory = new ErsStepMailFactory();

            ErsFactory.ersCommonFactory = new ErsCommonFactory();

            ErsFactory.ersWarehouseFactory = new ErsWarehouseFactory();

            ErsFactory.ersSummaryFactory = new ErsSummaryFactory();

            ErsFactory.ersRankingFactory = new ErsRankingFactory();

            ErsFactory.ersLpFactory = new ErsLpFactory();

            ErsFactory.ersBatchFactory = new ErsBatchFactory();

            ErsFactory.ersLanguageFactory = new ErsLanguageFactory();
        }

        protected virtual void SetMallFactory()
        {
            ErsMallFactory.ersSiteFactory = new global::jp.co.ivp.ers.mall.site.ErsSiteFactory();

            ErsMallFactory.ersMallBatchFactory = new global::jp.co.ivp.ers.mall.batch.ErsMallBatchFactory();

            ErsMallFactory.ersMallUtilityFactory = new global::jp.co.ivp.ers.mall.util.ErsMallUtilityFactory();

            ErsMallFactory.ersMallShopFactory = new global::jp.co.ivp.ers.mall.shop.ErsMallShopFactory();

            ErsMallFactory.ersMallMailFactory = new global::jp.co.ivp.ers.mall.sendmail.ErsMallMailFactory();

            ErsMallFactory.ersMallViewServiceFactory = new global::jp.co.ivp.ers.mall.viewService.ErsMallViewServiceFactory();

            ErsMallFactory.ersMallAmazonFactory = new global::jp.co.ivp.ers.mall.amazon.ErsMallAmazonFactory();

            ErsMallFactory.ersMallAPIFactory = new global::jp.co.ivp.ers.mall.api.ErsMallAPIFactory();

            ErsMallFactory.ersMallCommonFactory = new global::jp.co.ivp.ers.mall.common.ErsMallCommonFactory();

            ErsMallFactory.ersMallStopTimeFactory = new global::jp.co.ivp.ers.mall.stop_time.ErsMallStopTimeFactory();

            ErsMallFactory.ersMallOrderFactory = new global::jp.co.ivp.ers.mall.mall_order.ErsMallOrderFactory();

            ErsMallFactory.ersMallProductFactory = new global::jp.co.ivp.ers.mall.product.ErsMallProductFactory();

            ErsMallFactory.ersMallStockErrorFactory = new global::jp.co.ivp.ers.mall.stock_error.ErsMallStockErrorFactory();

            ErsMallFactory.ersMallStockRecoveryFactory = new global::jp.co.ivp.ers.mall.stock_recovery.ErsMallStockRecoveryFactory();

            ErsMallFactory.ersMallStockFactory = new global::jp.co.ivp.ers.mall.stock.ErsMallStockFactory();
        }

        protected override void RegisterRoutes(RouteCollection routes)
        {
            routes.RouteExistingFiles = true;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //http error
            routes.MapRoute(
              "HttpError",
              "HttpError/{action}/{statusCode}",
              new { controller = "HttpError", action = "Error" },
              GetNamespace()
            );

            //handle images/spacer.gif before ignore files under images
            routes.MapRoute(
                "ErsStepMailOpenCounter", // Route name
                "images/spacer.gif", // URL with parameters
                new { controller = "images", action = "spacer" }, // Parameter defaults
                GetNamespace()
            );

            //画像変換
            routes.MapRoute(
               "BimgConvert", // Route name
               "images/bimg/{scode}/{*fileName}", // URL with parameters
               new { controller = "ResizeImage", action = "resizeImage", dirName = "bimg" }, // Parameter defaults
                  GetNamespace()
            );

            //画像変換
            routes.MapRoute(
               "SimgConvert", // Route name
               "images/simg/{scode}/{*fileName}", // URL with parameters
               new { controller = "ResizeImage", action = "resizeImage", dirName = "simg" }, // Parameter defaults
                  GetNamespace()
            );

            //画像変換
            routes.MapRoute(
               "BimgConvertNor", // Route name
               "images/bimg/{*fileName}", // URL with parameters
               new { controller = "ResizeImage", action = "resizeImage", dirName = "bimg" }, // Parameter defaults
                  GetNamespace()
            );

            //画像変換
            routes.MapRoute(
               "SimgConvertNor", // Route name
               "images/simg/{*fileName}", // URL with parameters
               new { controller = "ResizeImage", action = "resizeImage", dirName = "simg" }, // Parameter defaults
                  GetNamespace()
            );

            //ignore handle of files
            routes.IgnoreRoute("{*files}", new { files = @".*\.(" + ErsFactory.ersUtilityFactory.getSetup().ignore_file_extensions + ")(/.*)?" });
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.IgnoreRoute("{*allimage}", new { allimage = @".*\.(jpg|jpeg|gif|bmp|png|ping)(/.*)?" });

            routes.MapRoute(
                "ErsLegacy", // Route name
                "top/{controller}/asp/{action}.asp", // URL with parameters
                new { controller = "Home", action = "index" }, // Parameter defaults
                GetNamespace()
            );

            routes.MapRoute(
                "ErsLegacyRoot", // Route name
                "top/{controller}/asp/", // URL with parameters
                new { controller = "Home", action = "index" }, // Parameter defaults
                 GetNamespace()
            );

            routes.MapRoute(
              "DefaultIndex", // Route name
              "", // URL with parameters
              new { controller = "Home", action = "index" }, // Parameter defaults
                 GetNamespace()
           );

            routes.MapRoute(
             "Static", // Route name
             "{*staticPath}", // URL with parameters
             new { controller = "Home", action = "staticRooting" }, // Parameter defaults
                GetNamespace()
          );

        }

        protected override void SetResolver()
        {
            Bootstrapper.Initialise();
        }
    }
}