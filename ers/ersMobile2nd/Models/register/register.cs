﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using ersMobile2nd.Domain.Register.Commands;
using ersMobile2nd.Domain.Common.Commands;
using ersMobile2nd.Domain.Common.Mappables;
using ersMobile2nd.Domain.Register.Mappables;

namespace ersMobile2nd.Models
{
    /// <summary>
    /// モバイル用
    /// </summary>
    public class Register
        : ers.Models.Register, IOrderRegistCommand, IZipSearchCommand, IZipSearchMappable, IAnotherZipSearchCommand, IAnotherZipSearchMappable, IOrderMappable
    {
        [HtmlSubmitButton]
        public bool zip_flg1 { get; set; }

        public string zip_search_error { get; set; }

        [HtmlSubmitButton]
        public bool zip_flg2 { get; set; }

        public string add_zip_search_error { get; set; }

        /// <summary>
        /// 環境依存のデータセット
        /// </summary>
        /// <returns></returns>
        public override EnumPmFlg pm_flg
        {
            get
            {
                return EnumPmFlg.MOBILE;  //1:Mobile
            }
        }

        /// <summary>
        /// 戻り先ＵＲＬ
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.WebAddress)]
        public virtual string current_url { get; set; }

        /// <summary>
        /// 別住所レコードID
        /// </summary>
        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? add_address_id { get; set; }

        public override void setBasketCookie()
        {
            //base.setBasketCookie();
            //don't write this data to cookie in model.
        }
    }
}
