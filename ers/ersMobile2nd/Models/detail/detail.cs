﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace ersMobile2nd.Models
{
    public class Detail
        :ers.Models.Detail
    {
        public virtual string m_sname { get; internal set; }

        public virtual string disp_sname
        {
            get
            {
                if (string.IsNullOrEmpty(m_sname))
                {
                    return sname;
                }

                return m_sname;
            }
        }
    }

}
