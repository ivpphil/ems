﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersMobile2nd.Domain.Common.Commands;
using ersMobile2nd.Domain.Common.Mappables;
using jp.co.ivp.ers.mvc;

namespace ersMobile2nd.Models.member
{
    public class Member
        : ers.Models.Member, IZipSearchCommand, IZipSearchMappable
    {
        [HtmlSubmitButton]
        public bool zip_flg1 { get; set; }

        public string zip_search_error { get; set; }
    }
}
