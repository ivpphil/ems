﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersMobile2nd.Domain.Common.Commands;

namespace ersMobile2nd.Domain.Common.Handlers
{
    public class ValidateZipSearch
        : IValidationHandler<IZipSearchCommand>
    {
        public virtual IEnumerable<ValidationResult> Validate(IZipSearchCommand command)
        {
            yield return command.CheckRequired("zip");
        }
    }
}
