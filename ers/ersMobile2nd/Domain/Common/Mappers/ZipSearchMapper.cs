﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersMobile2nd.Domain.Common.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersMobile2nd.Domain.Common.Mappers
{
    public class ZipSearchMapper
         : IMapper<IZipSearchMappable>
    {
        public void Map(IZipSearchMappable member)
        {
            if (!member.IsValid)
            {
                member.zip_search_error = String.Join(Environment.NewLine, member.GetAllErrorMessageList());
                member.controller.ClearModelState(member);
            }
            else
            {
                var zipService = ErsFactory.ersViewServiceFactory.GetErsViewZipService();
                zipService.Search(member.zip);
                member.zip_search_error = zipService.error_message;
                if (string.IsNullOrEmpty(zipService.error_message))
                {
                    member.pref = zipService.pref;
                    member.address = zipService.address;
                }
            }
        }
    }
}
