﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc;

namespace ersMobile2nd.Domain.Common.Mappables
{
    public interface IAnotherZipSearchMappable
         : IMappable, IErsModelBase
   {
       string add_zip { get; }

       string add_zip_search_error { set; }

       int? add_pref { set; }

       string add_address { set; }
   }
}
