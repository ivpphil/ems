﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersMobile2nd.Domain.Register.Commands
{
    public interface IOrderRegistCommand
        : ers.Domain.Register.Commands.IOrderRegistCommand
    {
        int? add_address_id { get; set; }
    }
}
