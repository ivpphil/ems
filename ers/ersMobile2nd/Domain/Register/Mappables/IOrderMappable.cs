﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersMobile2nd.Domain.Register.Mappables
{
    public interface IOrderMappable
        : ers.Domain.Register.Mappables.IOrderMappable
    {
        int? add_address_id { get; }
    }
}
