﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using ersMobile2nd.Domain.Register.Mappables;

namespace ersMobile2nd.Domain.Register.Mappers
{
    public class OrderMapper
        : ers.Domain.Register.Mappers.OrderMapper
    {
        /// <summary>
        ///  sendを再判定設定する
        /// </summary>
        protected override void SendSet(ers.Domain.Register.Mappables.IOrderMappable objMappableParent)
        {
            var objMappable = (IOrderMappable)objMappableParent;

            if (objMappable.add_address_id != null && objMappable.add_address_id >= 0)
            {
                objMappable.send = EnumSendTo.ANOTHER_ADDRESS;
                if (objMappable.add_address_id != 0)
                {
                    if (objMappable.k_flg == EnumMemberEntryMode.MEMBER)
                    {
                        var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
                        var addCri = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

                        //検索条件をクライテリアに保存
                        addCri.mcode = objMappable.mcode;
                        addCri.id = (int)objMappable.add_address_id;

                        var addList = repository.Find(addCri);
                        if (addList.Count == 1)
                        {
                            foreach (var address in addList)
                            {
                                objMappable.address_name = address.address_name;
                                objMappable.add_lname = address.add_lname;
                                objMappable.add_fname = address.add_fname;
                                objMappable.add_lnamek = address.add_lnamek;
                                objMappable.add_fnamek = address.add_fnamek;
                                objMappable.add_zip = address.add_zip;
                                objMappable.add_tel = address.add_tel;
                                objMappable.add_fax = address.add_fax;
                                objMappable.add_pref = address.add_pref;
                                objMappable.add_address = address.add_address;
                                objMappable.add_taddress = address.add_taddress;
                                objMappable.add_maddress = address.add_maddress;
                            }
                        }
                    }

                }
            }
            else
            {
                objMappable.send = EnumSendTo.MEMBER_ADDRESS;
                this.ClearAddress(objMappable);
            }
        }

        public virtual void ClearAddress(IOrderMappable objMappable)
        {
            objMappable.address_name = string.Empty;
            objMappable.add_lname = string.Empty;
            objMappable.add_fname = string.Empty;
            objMappable.add_lnamek = string.Empty;
            objMappable.add_fnamek = string.Empty;
            objMappable.add_zip = string.Empty;
            objMappable.add_tel = string.Empty;
            objMappable.add_fax = string.Empty;
            objMappable.add_pref = null;
            objMappable.add_address = string.Empty;
            objMappable.add_taddress = string.Empty;
            objMappable.add_maddress = string.Empty;
        }

    }
}
