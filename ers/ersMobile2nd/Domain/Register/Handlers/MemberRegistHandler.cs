﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;

namespace ersMobile2nd.Domain.Register.Handlers
{
    public class MemberRegistHandler
        : ers.Domain.Register.Handlers.MemberEntryHandler
    {
        public override EnumPmFlg EnvPM_flg()
        {
            return EnumPmFlg.MOBILE;
        }
    }
}
