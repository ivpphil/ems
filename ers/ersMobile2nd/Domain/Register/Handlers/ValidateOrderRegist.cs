﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersMobile2nd.Domain.Register.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;

namespace ersMobile2nd.Domain.Register.Handlers
{
    public class ValidateOrderRegist
        : ers.Domain.Register.Handlers.ValidateOrderRegist
    {
        public override IEnumerable<ValidationResult> ValidateInputValues(ers.Domain.Register.Commands.IOrderRegistCommand parentCommand)
        {
            var command = (IOrderRegistCommand)parentCommand;

            //やむ終えず-1に設定したラジオボタンの値をNULLに戻す
            if (command.add_address_id == -1)
            {
                command.add_address_id = null;
            }

            return base.ValidateInputValues(command);
        }
    }
}
