﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ers.Models;
using jp.co.ivp.ers.mvc;

namespace ersMobile2nd.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class questController
        : ers.Controllers.questController
    {
    }
}
