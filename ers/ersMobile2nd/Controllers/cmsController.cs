﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;

namespace ersMobile2nd.Controllers
{
    /// <summary>
    /// Inherits CMS PC site
    /// </summary>
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class cmsController
        : ers.Controllers.cmsController
    {

    }

}
