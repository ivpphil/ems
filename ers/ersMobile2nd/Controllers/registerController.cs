﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
using ersMobile2nd.Models;
using ers.Domain.Register.Commands;
using ersMobile2nd.Domain.Common.Mappables;
using ersMobile2nd.Domain.Common.Commands;
using ersMobile2nd.Models.cart;
using jp.co.ivp.ers;

namespace ersMobile2nd.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class registerController
        : ers.Controllers.registerController
    {
        /// <summary>
        /// ログイン画面
        /// </summary>
        /// <returns></returns>
        protected override ers.Models.Register GetRegisterModel()
        {
            return this.GetBindedModel<Register>();
        }

        protected override ers.Models.Cart GetRegisterCartModel()
        {
            return this.GetBindedModel<Cart>();
        }

        #region entry
        [NonAction]
        public override ActionResult entry(ers.Models.Register register, ers.Models.Cart cart, EnumEck? eck = null)
        {
            return entry((Register)register, (Cart)cart, eck);
        }

        public virtual ActionResult entry(Register register, Cart cart, EnumEck? eck = null)
        {
            return base.entry(register, cart, eck);
        }
        #endregion

        #region entry2
        [NonAction]
        public override ActionResult entry2(ers.Models.Register register, ers.Models.Cart cart, EnumEck? eck = null)
        {
            return entry2((Register)register, (Cart)cart, eck);
        }

        public virtual ActionResult entry2(Register register, Cart cart, EnumEck? eck = null)
        {
            if (!register.zip_flg1)
            {
                register.current_url = "top/register/asp/entry2.asp";
                return base.entry2(register, cart, eck);
            }

            //郵便番号検索
            ModelState.AddModelErrors(commandBus.Validate<IZipSearchCommand>(register), register);

            this.mapperBus.Map<IZipSearchMappable>(register);

            return this.entry(register, cart, EnumEck.Error);
        }
        #endregion

        #region entry3
        [NonAction]
        public override ActionResult entry3(ers.Models.Register register, ers.Models.Cart cart, EnumEck? eck = null)
        {
            return this.entry3((Register)register, (Cart)cart, eck);
        }

        public virtual ActionResult entry3(Register register, Cart cart, EnumEck? eck = null)
        {
            if (!register.zip_flg2)
            {
                //削除ボタン
                if (cart.deleteButton != null || cart.deleteRegularButton != null)
                {
                    return entry2(register, cart, eck);
                }
                return base.entry3(register, cart, eck);
            }

            //郵便番号検索
            ModelState.AddModelErrors(commandBus.Validate<IAnotherZipSearchCommand>(register), register);

            this.mapperBus.Map<IAnotherZipSearchMappable>(register);

            return this.entry2(register, cart, EnumEck.BackPage);
        }
        #endregion

        #region entry4
        [NonAction]
        public override ActionResult entry4(ers.Models.Register register, ers.Models.Cart cart)
        {
            return this.entry4((Register)register, (Cart)cart);
        }

        public virtual ActionResult entry4(Register register, Cart cart)
        {
            return base.entry4(register, cart);
        }
        #endregion

        #region member_entry
        [NonAction]
        public override ActionResult member_entry2(ers.Models.MemberEntry member)
        {
            return this.member_entry2((MemberEntry)member);
        }

        public virtual ActionResult member_entry2(MemberEntry member)
        {
            if (!member.zip_flg1)
            {
                return base.member_entry2(member);
            }

            //郵便番号検索
            ModelState.AddModelErrors(commandBus.Validate<IZipSearchCommand>(member), member);

            this.mapperBus.Map<IZipSearchMappable>(member);

            return this.member_entry(member, EnumEck.Error);
        }
        #endregion
    }
}
