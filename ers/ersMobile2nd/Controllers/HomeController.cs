﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using jp.co.ivp.ers.mvc;
using ersMobile2nd.Models.Home;

namespace ersMobile2nd.Controllers
{
    /// <summary>
    /// Inherits PC site
    /// </summary>
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class HomeController
        : ers.Controllers.HomeController
    {
        /// <summary>
        /// return to pc site with override Index model
        /// </summary>
        /// <param name="index">mobile index</param>
        /// <returns></returns>
        public virtual ActionResult Index(Index index)
        {
            return base.Index(index);
        }

        /// <summary>
        /// override PC site
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        [NonAction]
        public override ActionResult Index(ers.Models.Home.Index index)
        {
            return Index((Index)index);
        }
    }
}
