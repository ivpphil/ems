﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersSmartPhone.Models.Home
{
    public class Index
        : ers.Models.Home.Index
    {
        /// <summary>
        /// get content code for smartphone
        /// </summary>
        public override string contents_code
        {
            get { return "10000004"; }
        }
    }
}