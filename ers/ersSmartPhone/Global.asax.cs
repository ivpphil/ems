﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.CodeDom.Compiler;
using System.IO;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;

namespace ersSmartPhone
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : ers.MvcApplication
    {
        protected override string[] GetNamespace()
        {
            return new[] { "ersSmartPhone.Controllers" };
        }

        protected override void SetFactory()
        {
            base.SetFactory();
        }

        protected override void RegisterRoutes(RouteCollection routes)
        {
            routes.RouteExistingFiles = true;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //http error
            routes.MapRoute(
              "HttpError",
              "HttpError/{action}/{statusCode}",
              new { controller = "HttpError", action = "Error" },
              GetNamespace()
            );

            //handle images/spacer.gif before ignore files under images
            routes.MapRoute(
                "ErsStepMailOpenCounter", // Route name
                "images/spacer.gif", // URL with parameters
                new { controller = "images", action = "spacer" }, // Parameter defaults
                GetNamespace()
            );

            //画像変換
            routes.MapRoute(
               "BimgConvert", // Route name
               "images/bimg/{*fileName}", // URL with parameters
               new { controller = "ResizeImage", action = "resizeImage", dirName = "bimg" }, // Parameter defaults
                  GetNamespace()
            );

            //画像変換
            routes.MapRoute(
               "SimgConvert", // Route name
               "images/simg/{*fileName}", // URL with parameters
               new { controller = "ResizeImage", action = "resizeImage", dirName = "simg" }, // Parameter defaults
                  GetNamespace()
            );

            //ignore handle of files
            routes.IgnoreRoute("{*files}", new { files = @".*\.(" + ErsFactory.ersUtilityFactory.getSetup().ignore_file_extensions + ")(/.*)?" });
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.IgnoreRoute("{*allimage}", new { allimage = @".*\.(jpg|jpeg|gif|bmp|png|ping)(/.*)?" });

            routes.MapRoute(
                "ErsLegacy", // Route name
                "top/{controller}/asp/{action}.asp", // URL with parameters
                new { controller = "Home", action = "index" }, // Parameter defaults
                GetNamespace()
            );

            routes.MapRoute(
                "ErsLegacyRoot", // Route name
                "top/{controller}/asp/", // URL with parameters
                new { controller = "Home", action = "index" }, // Parameter defaults
                 GetNamespace()
            );

            routes.MapRoute(
              "DefaultIndex", // Route name
              "", // URL with parameters
              new { controller = "Home", action = "index" }, // Parameter defaults
                 GetNamespace()
           );

            routes.MapRoute(
             "Static", // Route name
             "{*staticPath}", // URL with parameters
             new { controller = "Home", action = "staticRooting" }, // Parameter defaults
                GetNamespace()
          );

        }

        protected override void SetResolver()
        {
            Bootstrapper.Initialise();
        }
    }
}