﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersSmartPhone.Controllers
{
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class cmsController
        : ers.Controllers.cmsController
    {
        
    }
}