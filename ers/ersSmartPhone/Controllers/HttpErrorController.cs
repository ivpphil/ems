﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.Net;

namespace ersSmartPhone.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [ErsSideMenu]
    public class HttpErrorController
        : ers.Controllers.HttpErrorController
    {

    }
}
