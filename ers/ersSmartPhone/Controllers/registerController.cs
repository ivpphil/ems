﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using ersSmartPhone.Models;


namespace ersSmartPhone.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class registerController
        : ers.Controllers.registerController
    {
        /// <summary>
        /// ログイン画面
        /// </summary>
        /// <returns></returns>
        protected override ers.Models.Register GetRegisterModel()
        {
            return this.GetBindedModel<Register>();
        }
        
        #region entry
        [NonAction]
        public override ActionResult entry(ers.Models.Register register, ers.Models.Cart cart, EnumEck? eck = null)
        {
            return entry((Register)register, cart, eck);
        }

        public virtual ActionResult entry(Register register, ers.Models.Cart cart, EnumEck? eck = null)
        {
            return base.entry(register, cart, eck);
        }
        #endregion

        #region entry2
        [NonAction]
        public override ActionResult entry2(ers.Models.Register register, ers.Models.Cart cart, EnumEck? eck = null)
        {
            return entry2((Register)register, cart, eck);
        }

        public virtual ActionResult entry2(Register register, ers.Models.Cart cart, EnumEck? eck = null)
        {
            return base.entry2(register, cart, eck);
        }
        #endregion

        #region entry3
        [NonAction]
        public override ActionResult entry3(ers.Models.Register register, ers.Models.Cart cart, EnumEck? eck = null)
        {
            return this.entry3((Register)register, cart, eck);
        }

        public virtual ActionResult entry3(Register register, ers.Models.Cart cart, EnumEck? eck = null)
        {
            return base.entry3(register, cart, eck);
        }
        #endregion

        #region entry4
        [NonAction]
        public override ActionResult entry4(ers.Models.Register register, ers.Models.Cart cart)
        {
            return this.entry4((Register)register, cart);
        }

        public virtual ActionResult entry4(Register register, ers.Models.Cart cart)
        {
            return base.entry4(register, cart);
        }
        #endregion
    }
}
