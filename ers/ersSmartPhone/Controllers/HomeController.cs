﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ersSmartPhone.Models.Home;
using ers.Domain.Home.Mappables;
using jp.co.ivp.ers.mvc;

namespace ersSmartPhone.Controllers
{
    /// <summary>
    /// Inherits ers HomeController
    /// </summary>
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class HomeController
        : ers.Controllers.HomeController
    {
        /// <summary>
        /// return to pc site with override Index model
        /// </summary>
        /// <param name="index">smartphone index</param>
        /// <returns></returns>
        public virtual ActionResult Index(Index index)
        {
            return base.Index(index); 
        }

        /// <summary>
        /// override PC site
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        [NonAction]
        public override ActionResult Index(ers.Models.Home.Index index)
        {
            return Index((Index)index);
        }

    }
}