/*============================================================================
	Author and Copyright
		製作者: IVP CO,LTD（http://www.ivp.co.jp/）
		作成日: 2009-03-06
		修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function(){
	var ersObj = ErsCart();
	ersObj.BindRecompute();
	ersObj.ChangeRegularPtn();
});

/* ErsDetailオブジェクト生成コンストラクタ */
var ErsCart = function () {

    var that = {};

    /* 郵便番号から住所自動入力
    ---------------------------------------------------------------- */
    that.BindRecompute = function () {
        var inner = {
            AddHidden: function (button, name, value) {
                var amounts = $(".input_amount");
                var amount_length = amounts.length;
                var max_amounts = $(".max_amount");
                var snames = $(".input_sname");
                var is_error = false;

                for (i = 0; i < amount_length; i++) {
                    if ($.isNumeric(amounts[i].value)) {
                        var actual_max_amounts = parseInt(max_amounts[i].value);
                        var actual_amounts = amounts[i].value;
                        if (actual_max_amounts != 0 && actual_amounts > actual_max_amounts) {
                            is_error = true;
                            alert(snames[i].value + "は、" + actual_max_amounts + "個までしか購入できません。");
                            //submit活性化
                            $(button).data('dblflg', 0);
                            $(button).prop("disabled", false);
                            break;
                        }
                    }
                }

                if (!is_error) {
                    var objForm = $("form[name='entry_form']");
                    objForm.append("<input type='hidden' name='" + name + "' value='" + value + "' />");
                    return true;
                }
                else {
                    $(".change_opacity").removeAttr("disabled");
                    $(".change_opacity").css({ opacity: "1" });
                    return false;
                }

            }
        }
        //clickイベントをバインド
        $("#recompute").click(function () {
            return inner.AddHidden(this, "recompute", "1");
        });

        //clickイベントをバインド
        $("#regular_recompute").click(function () {
            return inner.AddHidden(this, "regular_recompute", "1");
        });
    }

    /* 配送パターン変更処理
    ---------------------------------------------------------------- */
    that.ChangeRegularPtn = function () {
        var hiddenNextDate = "";
        var hiddenPtnIntervalDay = "";
        var domParent = "";

        //個別商品変更
        $(".regular_day_modify").click(function () {
            //対象hiddenのname取得
            domParent = $(this).parent().parent();
            var domInterval = $("input[name$='ptn_interval_day']", domParent);
            var domNextDate = $("input[name$='next_date']", domParent);

            hiddenPtnIntervalDay = domInterval.attr("name");
            hiddenNextDate = domNextDate.attr("name");
            //[Firefox] Jquery lib v1.6+ set attribute 'checked/selected' supported by using prop() prototype
            $("#regular_day_dialog option[value=" + domInterval.val() + "]").prop("selected", true);
            if (domNextDate.val() !== "") {
                //Since radio button automatically change all other same value of property name
                //$("#regular_day_dialog #shortest").prop("checked", false);

                //[Firefox] Jquery lib v1.6+ set attribute 'checked/selected' supported by using prop() prototype
                $("#regular_day_dialog #fixed").prop("checked", true);
                $("#regular_day_dialog #datepicker2").val(domNextDate.val().substr(0, 10));
            } else {
                //Fixed when no selected domNextDate.val()
                $("#regular_day_dialog #shortest").prop("checked", true);
                $("#regular_day_dialog #datepicker2").val('');
            }

            $("#regular_day_dialog").dialog("open");
            return false;
        });

        $("#regular_day_dialog").dialog({
            autoOpen: false,
            height: 320,
            width: 395,
            modal: true,
            buttons: {
                "設定する": function () {
                    var valPtn = $("#change_ptn_interval_day option:selected").val();
                    //配送パターンの設定
                    $("input[name='" + hiddenPtnIntervalDay + "']").val(valPtn);
                    //配送日の設定
                    var valNextDate = "";
                    var valFirstTime = $("input[name='change_first_time']:checked").val();
                    if (valFirstTime == 1) {
                        valNextDate = $("input[name='change_next_date']").val();
                    }
                    $("input[name='" + hiddenNextDate + "']").val(valNextDate);

                    //再計算用hidden生成してsubmit
                    $(".ui-widget-overlay").after('<p class="loading_alert">変更を適用しています</p>')
                    $("#entry_form").append('<input type="hidden" value="1" name="regular_recompute">').submit();
                    $(".ui-dialog").hide();
                    //$( this ).dialog( "close" );
                }
            },
            close: function () {
                hiddenNextDate = "";
                hiddenPtnIntervalDay = "";
                domParent = "";
            }
        });

        var hiddenPtnIntervalMonth = "";
        var hiddenPtnIntervalWeek = "";
        var hiddenPtnWeekday = "";

        //個別商品変更
        $(".regular_week_modify").click(function () {
            //対象hiddenのname取得
            domParent = $(this).parent().parent();
            var domPtnIntervalMonth = $("input[name$='ptn_interval_month']", domParent);
            var domPtnIntervalWeek = $("input[name$='ptn_interval_week']", domParent);
            var domPtnWeekday = $("input[name$='ptn_weekday']", domParent);

            hiddenPtnIntervalMonth = domPtnIntervalMonth.attr("name");
            hiddenPtnIntervalWeek = domPtnIntervalWeek.attr("name");
            hiddenPtnWeekday = domPtnWeekday.attr("name");
            $("#regular_week_dialog #change_ptn_week_interval_month option[value=" + domPtnIntervalMonth.val() + "]").prop("selected", true);
            $("#regular_week_dialog #change_ptn_interval_week option[value=" + domPtnIntervalWeek.val() + "]").prop("selected", true);
            $("#regular_week_dialog #change_ptn_weekday option[value=" + domPtnWeekday.val() + "]").prop("selected", true);

            $("#regular_week_dialog").dialog("open");
            return false;
        });
        $("#regular_week_dialog").dialog({
            autoOpen: false,
            height: 320,
            width: 395,
            modal: true,
            buttons: {
                "設定する": function () {
                    var valPtnIntervalMonth = $("#change_ptn_week_interval_month option:selected").val();
                    //配送パターンの設定
                    $("input[name='" + hiddenPtnIntervalMonth + "']").val(valPtnIntervalMonth);

                    var varPtnIntervalWeek = $("#change_ptn_interval_week option:selected").val();
                    //配送パターンの設定
                    $("input[name='" + hiddenPtnIntervalWeek + "']").val(varPtnIntervalWeek);

                    var valPtnIntervalWeekday = $("#change_ptn_weekday option:selected").val();
                    //配送パターンの設定
                    $("input[name='" + hiddenPtnWeekday + "']").val(valPtnIntervalWeekday);

                    //再計算用hidden生成してsubmit
                    $(".ui-widget-overlay").after('<p class="loading_alert">変更を適用しています</p>')
                    $("#entry_form").append('<input type="hidden" value="1" name="regular_recompute">').submit();
                    $(".ui-dialog").hide();
                    //$( this ).dialog( "close" );
                }
            },
            close: function () {
                hiddenNextDate = "";
                hiddenPtnIntervalDay = "";
                domParent = "";
            }
        });

        var hiddenPtnDay = "";

        //個別商品変更
        $(".regular_month_modify").click(function () {
            //対象hiddenのname取得
            domParent = $(this).parent().parent();
            var domPtnIntervalMonth = $("input[name$='ptn_interval_month']", domParent);
            var domPtnDay = $("input[name$='ptn_day']", domParent);
            var domNextDate = $("input[name$='next_date']", domParent);

            hiddenPtnIntervalMonth = domPtnIntervalMonth.attr("name");
            hiddenPtnDay = domPtnDay.attr("name");
            hiddenNextDate = domNextDate.attr("name");



            //配送パターンの設定
            var valNextDate = domNextDate.val();
            var arrNextDate = valNextDate.split('/');

            $("#regular_month_dialog #change_ptn_month_interval_month option[value=" + domPtnIntervalMonth.val() + "]").prop("selected", true);
            $("#regular_month_dialog #change_ptn_day option[value=" + domPtnDay.val() + "]").prop("selected", true);
            $("#regular_month_dialog #change_next_date option[value^='" + arrNextDate[0] + "/" + arrNextDate[1] + "']").prop("selected", true);

            $("#regular_month_dialog").dialog("open");
            return false;
        });
        $("#regular_month_dialog").dialog({
            autoOpen: false,
            height: 320,
            width: 395,
            modal: true,
            buttons: {
                "設定する": function () {
                    var valPtnIntervalMonth = $("#change_ptn_month_interval_month option:selected").val();
                    //配送パターンの設定
                    $("input[name='" + hiddenPtnIntervalMonth + "']").val(valPtnIntervalMonth);

                    var varPtnDay = $("#change_ptn_day option:selected").val();
                    //配送パターンの設定
                    $("input[name='" + hiddenPtnDay + "']").val(varPtnDay);

                    var valNextDate = $("#change_next_date option:selected").val();
                    //配送パターンの設定
                    $("input[name='" + hiddenNextDate + "']").val(valNextDate);

                    //再計算用hidden生成してsubmit
                    $(".ui-widget-overlay").after('<p class="loading_alert">変更を適用しています</p>')
                    $("#entry_form").append('<input type="hidden" value="1" name="regular_recompute">').submit();
                    $(".ui-dialog").hide();
                    //$( this ).dialog( "close" );
                }
            },
            close: function () {
                hiddenNextDate = "";
                hiddenPtnIntervalDay = "";
                domParent = "";
            }
        });
    }


    return that;
}

