﻿/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function(){
	var ersObj = ErsSearch();
	ersObj.searchCate();
});

/* ErsSearchオブジェクト生成コンストラクタ */
var ErsSearch = function () {

    var that = {};

    /* 商品検索画面でのカテゴリ表示
    ---------------------------------------------------------------- */
    that.searchCate = function () {
        var form_change = function (cate_num) {
            var cate_checked = new Array(5);
            var formObj = new Array(5);
            var j; 	//汎用

            if (cate_num == "") {
                cate_num = 5;
            }

            //選択されているカテゴリを取得。
            for (cnt = 1; cnt <= 5; cnt++) {

                //selectのオブジェクトを生成。
                formObj[cnt] = eval("document.form1.s_cate" + cnt)
                if (!!formObj[cnt]) {

                    //javascript稼動時
                    if (!form_selected[cnt]) {
                        for (i = (formObj[cnt].length + 1); i >= 0; i--) {
                            if (formObj[cnt].options[i] && formObj[cnt].options[i].selected && cate_num >= cnt) {
                                cate_checked[cnt] = formObj[cnt].options[i].value
                            }
                        }

                        //全画面からpostされた値でselected
                    } else {
                        cate_checked[cnt] = form_selected[cnt];
                    }
                }
            }

            //フォームを一旦すべてリセット。
            for (cnt = 1; cnt <= 5; cnt++) {
                if (!!formObj[cnt]) {
                    for (i = (formObj[cnt].length + 1); i >= 0; i--) {
                        formObj[cnt].options[i] = null;
                    }
                }
            }

            //カテゴリを順に表示
            for (cnt = 1; cnt <= 5; cnt++) {

                //フォーム自体が出現していないと、処理しない。
                if (formObj[cnt]) {

                    var isRoot = $(formObj[cnt]).hasClass('root_element');

                    //値を初期化
                    j = 1;

                    //フォームの一番上を作っておく。
                    formObj[cnt].options[0] = new Option("全て", "");

                    for (i = 0; i < form_array_cate[cnt].length; i++) {

                        //全表示する場合
                        if ((isRoot || cate_relation_level < cnt) && form_array_cate[cnt][i]) {
                            formObj[cnt].options[j] = new Option(form_array_cate[cnt][i].value, form_array_cate[cnt][i].id);
                            j++;

                            //親カテゴリによって、表示を絞る場合。
                        } else if (form_array_cate[cnt][i] && form_array_cate[cnt][i].parent_id === cate_checked[cnt - 1] && cate_checked[cnt - 1] != 0) {
                            formObj[cnt].options[j] = new Option(form_array_cate[cnt][i].value, form_array_cate[cnt][i].id);
                            j++;

                        }
                        //選択済み
                        if (j > 0 && formObj[cnt].options[j - 1] && formObj[cnt].options[j - 1].value == cate_checked[cnt]) {
                            formObj[cnt].options[j - 1].selected = true
                        }
                    }

                    //アイテムが出現しなければ、非活性
                    if (j == 1 && !isRoot) {
                        formObj[cnt].style.backgroundcolor = "#DDDDDD";
                        formObj[cnt].disabled = true;
                    } else {
                        formObj[cnt].disabled = false;
                    }
                }
            }
        };

        //onchangeイベントをバインド
        $('select[id^="s_cate"]').change(function () {
            form_change($(this).attr('id').slice(-1));
        }).change();
    }

    return that;
}

