/*============================================================================
	Author and Copyright
		製作者: IVP CO,LTD（http://www.ivp.co.jp/）
		作成日: 2010-09-29
		修正日: 2010-09-29
============================================================================*/

var init = function () {

    var prvVar = {}; //プライベート変数格納
    var that = {}; 	//戻り値用オブジェクト

    prvVar.strFullPath = window.location; 	//ドメイン以下
    prvVar.strPath = prvVar.strFullPath.pathname; 	//ドメイン以下
    prvVar.objCookieList = {}; 		//cookieをkey・valueごとに格納用
    prvVar.strBrowser = ""; 		//ブラウザ種別を格納（IE6・IE7・IE8・FX・Opera・Safari・Chrome・Otherのいずれか）
    prvVar.strNickname = ""; 		//nickname情報を格納

    //UserAgent取得処理。ブラウザ種別をstrBrowserにセット
    (function () {
        var userAgent = window.navigator.userAgent.toLowerCase();
        var appVersion = window.navigator.appVersion.toLowerCase();
        if (userAgent.indexOf("msie") > -1) {
            if (appVersion.indexOf("msie 6.0") > -1) {
                prvVar.strBrowser = "IE6";
            }
            else if (appVersion.indexOf("msie 7.0") > -1) {
                prvVar.strBrowser = "IE7";
            }
            else if (appVersion.indexOf("msie 8.0") > -1) {
                prvVar.strBrowser = "IE8";
            }
            else {
                prvVar.strBrowser = "Other";
            }
        }
        else if (userAgent.indexOf("firefox") > -1) {
            prvVar.strBrowser = "FX";
        }
        else if (userAgent.indexOf("opera") > -1) {
            prvVar.strBrowser = "Opera";
        }
        else if (userAgent.indexOf("chrome") > -1) {
            prvVar.strBrowser = "Chrome";
        }
        else if (userAgent.indexOf("safari") > -1) {
            prvVar.strBrowser = "Safari";
        }
        else {
            prvVar.strBrowser = "Other";
        }
    })();


    //cookie取得処理。objCookieListにcookieのハッシュを格納
    (function () {
        var strCookie; 			//cookie文字列
        var aryTmp; 				//汎用配列
        var aryTmp2; 			//汎用配列
        var aryTmp3; 			//汎用配列
        var tmpKey; 				//key一時格納用
        var tmpVal; 				//value一時格納用

        strCookie = decodeURIComponent(document.cookie);
        aryTmp = strCookie.split("; ");
        for (var i = 0; i < aryTmp.length; i++) {
            //valueが単一の場合
            if (aryTmp[i].indexOf("&") === -1) {
                aryTmp2 = aryTmp[i].split("=");
                tmpKey = aryTmp2[0];
                tmpVal = aryTmp2[1];
            } else {
                //valueが複数の場合
                tmpVal = {};
                aryTmp2 = aryTmp[i].split("&");
                for (j = 0; j < aryTmp2.length; j++) {
                    aryTmp3 = aryTmp2[j].split("=");
                    //イコールが2つある場合は先頭をkeyとみなす
                    if (aryTmp3.length === 3) {
                        tmpKey = aryTmp3[0];
                        tmpVal[aryTmp3[1]] = aryTmp3[2];
                    } else if (aryTmp3.length === 2) {
                        tmpVal[aryTmp3[0]] = aryTmp3[1];
                    }
                }
            }
            prvVar.objCookieList[tmpKey] = tmpVal;
        }
    })();

    //Nickname取得処理。実行キューを一旦空にするためDOM構築完了後に実行
    //空にしないと定数の取得がprvVarに反映されないため
    $(document).ready(function () {
        var host = location.host;
        var protocol = location.protocol;

        $.ajax({
            type: "GET",
            url: protocol + "//" + host + "/top/api/asp/get_cookie_session_prefix.asp",
            dataType: "json",
            success: function (retVal) {
                var prefix = retVal["prefix"];

                //cookieから取得した内容を文字列に変換
                if (prvVar.objCookieList[prefix + "nickname"] != null) {
                    var tmpNickname = prvVar.objCookieList[prefix + "nickname"];
                    prvVar.strNickname = tmpNickname;
                }
            },
            ajaxError: function () {
                return false;
            },
            timeout: 3000
        });
    });
    /* メソッドの実装
    ---------------------------------------------------------------- */
    //##################################################################
    //#関数名　：dispChange
    //#概要　　：選択項目によって表示・非表示を切り替える
    //#引数　　：オブジェクト（以下フォーマット）
    //　　　　　　strSelector:値取得対象DOMのセレクタ
    //　　　　　　ckVal:表示するケースの値
    //　　　　　　dispDom:表示するDOMオブジェクト
    //#戻り値　：なし
    //#備考　　：entry.aspで使用
    //##################################################################
    that.dispChange = function (obj) {
        //表示・非表示フラグ
        var blnDispFlg = false;
        var ckVal = $(obj["strSelector"]).val();

        //値が適合すればtrueに変更
        for (i in obj["ckVal"]) {
            if (ckVal === obj["ckVal"][i]) {
                blnDispFlg = true;
                break;
            }
        }

        //表示処理
        if (blnDispFlg) {
            obj["domDisp"].slideDown("fast");
        } else {
            obj["domDisp"].slideUp("fast");
        }
    }


    //##################################################################
    //#関数名　：zipSearch
    //#概要　　：get_zip.aspを叩いて郵便番号情報を取得、設定する
    //#引数　　：オブジェクト（以下フォーマット）
    //　　　　　　domZip:郵便番号1のDOM,
    //　　　　　　domPref:都道府県のDOM,
    //　　　　　　domAddress:住所1のDOM,
    //#戻り値　：なし
    //#備考　　：entry.aspで使用
    //##################################################################
    that.zipSearch = function (obj) {

        $.ajax({
            type: "GET",
            url: "../../api/asp/get_zip.asp?zip=" + obj["domZip"].val(),
            dataType: "json",
            success: function (retVal) {
                if (retVal["error_message"]) {
                    //0件、2件以上ヒット時、エラーメッセージを表示する。
                    obj["zip_search_error"].css("display", "");
                    obj["zip_search_error"].html(retVal["error_message"]);
                    if (obj["domPref"]) obj["domPref"].val(retVal["pref"]);
                    if (obj["domAddress"]) obj["domAddress"].val(retVal["address"]);
                    if (obj["domAddress2"]) obj["domAddress2"].val(retVal["address2"]);
                    if (obj["domAddress3"]) obj["domAddress3"].val("");

                } else {
                    if (obj["zip_search_error"]) obj["zip_search_error"].css("display", "none");
                    if (obj["domPref"]) obj["domPref"].val(retVal["pref"]);
                    if (obj["domAddress"]) obj["domAddress"].val(retVal["address"]);
                    if (obj["domAddress2"]) obj["domAddress2"].val(retVal["address2"]);
                }
            },
            ajaxError: function () {
                return false;
            },
            timeout: 3000
        });
    }

    //##################################################################
    //#関数名　：getPageInfo
    //#概要　　：ページ情報のgetterメソッド
    //#引数　　：なし
    //#戻り値　：prvVarオブジェクト
    //#備考　　：
    //##################################################################
    that.getPageInfo = function () {
        return prvVar;
    }


    //##################################################################
    //#関数名　：isLogin
    //#概要　　：ログイン状態判別メソッド
    //#引数　　：なし
    //#戻り値　：真偽値。cookieにnicknameの設定があればTrueを返す。なければFalse。
    //#備考　　：
    //##################################################################
    that.isLogin = function () {
        if (prvVar.strNickname === "") {
            return false
        } else {
            return true
        }
    }


    //##################################################################
    //#関数名　：isIE
    //#概要　　：IE判別メソッド
    //#引数　　：なし
    //#戻り値　：真偽値。ブラウザ種別がIE6・IE7・IE8のいずれかの場合True、それ以外はFalse
    //#備考　　：
    //##################################################################
    that.isIE = function () {
        var pattern = /IE[6-8]/i;
        return pattern.test(prvVar.strBrowser);
    }


    //##################################################################
    //#関数名　：getCookie
    //#概要　　：cookieオブジェクトのgetterメソッド
    //#引数　　：なし
    //#戻り値　：cookieのハッシュ
    //#備考　　：
    //##################################################################
    that.getCookie = function () {
        return prvVar.objCookieList;
    }

    //##################################################################
    //#関数名　：escapeHTML
    //#概要　　：Escape value 
    //#引数　　：string
    //#戻り値　：escaped string
    //#備考　　：
    //##################################################################
    that.escapeHTML = function (val) {
        return $("<div />").text(val).html();
    };

    //オブジェクトを戻す
    return that;
};

var ErsLib = init();