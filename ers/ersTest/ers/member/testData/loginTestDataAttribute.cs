﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersTestLibrary.common;
using ersTestLibrary.library;
using ersTestLibrary.common.db;
using jp.co.ivp.ers.db;

namespace ersTest.ers.member.testData
{
    public class loginTestDataAttribute
        : ActionTestDataCreatorBaseAttribute, IActionTestData
    {
        [ThreadStatic]
        public static ErsTestMember objMember;

        public void CreateTestData(NUnit.Framework.TestDetails testDetails)
        {
            using (var transaction = ErsDB_parent.BeginTransaction())
            {
                this.CreateErsTestMember();
                transaction.Commit();
            }
        }

        public void DeleteCreatedData(NUnit.Framework.TestDetails testDetails)
        {
            loginTestDataAttribute.objMember.DeleteWhenHasRecord("member_t");
        }

        #region "Create ErsTestMember"
        public void CreateErsTestMember()
        {
            var repository = new DapperRepository("member_t");
            var entity = new ErsTestMember();
            entity.mcode = "99999999";
            entity.lname = "てすとTest";
            entity.fname = "太郎Test";
            entity.lnamek = "テスト";
            entity.fnamek = "タロウ";
            entity.email = "test-member@ivp.co.jp";
            entity.passwd = "aaaaaaaa";

            repository.Insert(entity);

            loginTestDataAttribute.objMember = entity;
        }
        #endregion
    }
}
