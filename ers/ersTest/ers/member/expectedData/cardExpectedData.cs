﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ersTest.ers.member.expectedData
{
    public class cardExpectedData
    {
        public static Dictionary<string, object> GetCardExpectedData()
        {

            var dic = new Dictionary<string, object>();

            dic["card_type"] = "2";
            dic["card_no"] = "5500000000000189";
            dic["validity_y"] = "2020";
            dic["validity_m"] = "1";

            return dic;
        }

        public static Dictionary<string, object> GetCardExpectedDataUpdate()
        {

            var dic = new Dictionary<string, object>();

            dic["record_key_0_card_type"] = "2";
            dic["record_key_0_card_no"] = "5500000000000189";
            dic["record_key_0_validity_y"] = "2022";
            dic["record_key_0_validity_m"] = "2";
            return dic;
        }

    }
}
