﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ersTest.ers.member.expectedData
{
    public class loginExpectedData
    {
        public static Dictionary<string, object> GetloginExpectedData()
        {

            var dic = new Dictionary<string, object>();

            dic["email"] = "test-member@ivp.co.jp";
            dic["passwd"] = "aaaaaaaa";

            return dic;
        }

        public static Dictionary<string, object> GetloginWithErrorLastExpectedData()
        {
            var dic = new Dictionary<string,object>();
            dic["email"] = "";
            dic["passwd"] = "";
            return dic;
        }
    }
}
