﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ersTestLibrary.common;
using ersTestLibrary;
using ersTestLibrary.common.db;
using ersTestLibrary.common.extension;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;
using ersTestLibrary.ers;
using FluentAssertions;
using jp.co.ivp.ers;
using ersTestLibrary.library;
using ersTest.ers.member.testData;
using ersTest.ers.member.testElements;
using ersTest.ers.member.expectedData;

namespace ersTest.ers.member
{
    [TestFixture]
    [ActionTestData]
    public class card
         : ErsTestCommon
    {
        #region "Text Case"

        /// <summary>
        /// TEST CASE INPUT - CASE #1
        /// </summary>
        ///<remarks>
        ///Page Information
        ///     top/member/asp/card.asp
        ///     
        /// Case Description
        ///     Add new Card and Delete new Added Card
        ///
        /// </remarks>
        [TestCaseSource("Drivers")]
        [loginTestData]
        public void TestCase_1(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndFront;

                // == Move to destination test page == //
                var url = setup.pc_sec_url;
                CommonSeleniumUtil.ProceedToTargetPage(url + "top/member/asp/", driver, pageEnd,20); 

                //this.ProcessStartPage(driver, wait, pageEnd);                    

                // ## Initilize elements in loginElements container ## //
                loginElements login = new loginElements();
                PageFactory.InitElements(driver, login);

                // send keys for login
                driver.SendKeysByDictionary(loginExpectedData.GetloginExpectedData());
                login.submit_login.Click();

                //## Initilize element in cardElements container ##//
                cardElements card = new cardElements();
                PageFactory.InitElements(driver, card);
                card.card_ref.Click();

                // send keys for card
                driver.SendKeysByDictionary(cardExpectedData.GetCardExpectedData());

                // == Move to comfirmation page == //
                card.card_confirm.Click();

                // == Move to complete page == //
                card.card_complete.Click();

                // == Move to card page == //
                CommonSeleniumUtil.ProceedToTargetPage(url + "top/member/asp/card1.asp", driver, pageEnd, 20);

                // == Move to delete page == //
                card.button_delete.Click();

                // == Move to delete complete page == //
                card.button_delete_complete.Click();

                // == Move to card page == //
                CommonSeleniumUtil.ProceedToTargetPage(url + "top/member/asp/card1.asp", driver, pageEnd, 20);


            }
        }


        /// <summary>
        /// TEST CASE INPUT - CASE #1
        /// </summary>
        ///<remarks>
        ///Page Information
        ///     top/member/asp/card.asp
        ///     
        /// Case Description
        ///     Add new Card, update and Delete new Added Card
        ///
        /// </remarks>
        [TestCaseSource("Drivers")]
        [loginTestData]
        public void TestCase_2(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndFront;

                // == Move to destination test page == //
                var url = setup.pc_sec_url;
                CommonSeleniumUtil.ProceedToTargetPage(url + "top/member/asp/", driver, pageEnd, 20);

                //this.ProcessStartPage(driver, wait, pageEnd);                    

                // ## Initilize elements in loginElements container ## //
                loginElements login = new loginElements();
                PageFactory.InitElements(driver, login);

                // send keys for login
                driver.SendKeysByDictionary(loginExpectedData.GetloginExpectedData());
                login.submit_login.Click();

                //## Initilize element in cardElements container ##//
                cardElements card = new cardElements();
                PageFactory.InitElements(driver, card);

                #region add
                card.card_ref.Click();

                // send keys for card
                driver.SendKeysByDictionary(cardExpectedData.GetCardExpectedData());

                // == Move to comfirmation page == //
                card.card_confirm.Click();

                // == Move to complete page == //
                card.card_complete.Click();
                #endregion
                // == Move to card page == //
                CommonSeleniumUtil.ProceedToTargetPage(url + "top/member/asp/card1.asp", driver, pageEnd, 20);

                #region update
                // ==Update Card == //
                card.button_update.Click();

                // send keys for card update
                driver.SendKeysByDictionary(cardExpectedData.GetCardExpectedDataUpdate());

                // == Move to card page update confirm== //
                card.button_update_confirm.Click();

                // == Move to card page update complete== //
                card.button_update_complete.Click();
                #endregion

                // == Move to card page == //
                CommonSeleniumUtil.ProceedToTargetPage(url + "top/member/asp/card1.asp", driver, pageEnd, 20);

                #region delete
                // == Move to delete page == //
                card.button_delete.Click();

                // == Move to delete complete page == //
                card.button_delete_complete.Click();

                // == Move to card page == //
                CommonSeleniumUtil.ProceedToTargetPage(url + "top/member/asp/card1.asp", driver, pageEnd, 20);

                #endregion

            }
        }
        #endregion
    }
}
