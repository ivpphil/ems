﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ersLibraryTest.Site.ers.createData;
using ersTestLibrary.common;
using OpenQA.Selenium;
using ersLibraryTest.Site.ers;
using ersTestLibrary.ers.member;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using FluentAssertions;
using ersTestLibrary.common.extension;
using jp.co.ivp.ers;
using ersTestLibrary.Site.ers.createData;
using ersTestLibrary.Site.ers;
using ersTestLibrary.ers;

namespace ersTest.ers.member
{
    [TestFixture, MemberTestData, AddressTestData(countData = 1, non_test_data = new[] { "Test1_4_1", "Test1_4_2", "Test1_6_1" })]
    public class address_SpecNumber
        : ErsTestCommon
    {
        #region "Text Case"

        /// <summary>
        /// TEST 1-3
        /// </summary>
        /// <remarks>
        /// Check If addressbook values is displayed correctly in address modify page
        /// </remarks>
        [TestCaseSource("Drivers")]
        public void Test1_3(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndFront;

                //Processiding to My Page
                this.ProceedToMyPage(driver, wait, pageEnd);
                //Processiding to Modify Page
                this.ProceedToModifyPage(driver, wait, pageEnd);

                // == TEST == //
                // [TEST 1_3]
                // Check If addressbook values is displayed correctly in address modify page as initial value

                var address1 = new address1();
                PageFactory.InitElements(driver, address1);

                //[Expected values]
                //AddressTestDataAttribute.objAddress

                //[Assertion]
                address1.address_name.GetValue().Should().Be(AddressTestDataAttribute.objAddress.address_name);
                address1.add_lname.GetValue().Should().Be(AddressTestDataAttribute.objAddress.add_lname);
                address1.add_fname.GetValue().Should().Be(AddressTestDataAttribute.objAddress.add_fname);
                address1.add_lnamek.GetValue().Should().Be(AddressTestDataAttribute.objAddress.add_lnamek);
                address1.add_fnamek.GetValue().Should().Be(AddressTestDataAttribute.objAddress.add_fnamek);
                address1.add_zip.GetValue().Should().Be(AddressTestDataAttribute.objAddress.add_zip);
                address1.add_pref.GetSelectedValue().Should().Be(Convert.ToString(AddressTestDataAttribute.objAddress.add_pref));
                address1.add_address.GetValue().Should().Be(AddressTestDataAttribute.objAddress.add_address);
                address1.add_taddress.GetValue().Should().Be(AddressTestDataAttribute.objAddress.add_taddress);
                address1.add_maddress.GetValue().Should().Be(AddressTestDataAttribute.objAddress.add_maddress);
                address1.add_tel.GetValue().Should().Be(AddressTestDataAttribute.objAddress.add_tel);
                address1.add_fax.GetValue().Should().Be(AddressTestDataAttribute.objAddress.add_fax);
            }
        }

        /// <summary>
        /// TEST 1-4-1 Check Required Validation
        /// </summary>
        /// <remarks>
        /// Check if addressbook the composite validation in check required has been carried out correctly.
        /// </remarks>
        [TestCaseSource("Drivers")]
        public void Test1_4_1(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndFront;

                //Processiding to My Page
                this.ProceedToMyPage(driver, wait, pageEnd);
                //Processiding to Registration Page
                this.ProceedToRegistPage(driver, wait, pageEnd);


                // == TEST == //
                // [TEST 1_4_1]
                // Check if addressbook the composite validation in check required has been carried out correctly.

                var address1 = new address1();
                PageFactory.InitElements(driver, address1);

                this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address1.asp", pageEnd);

                address1.submit.Click();
                wait.Until(ExpectedConditions.ElementExists(pageEnd));

                var validation_message = new ValidationMessage();
                PageFactory.InitElements(driver, validation_message);

                //[Expected Error]
                var errorList = new List<string>();
                errorList.Add(ErsResources.GetMessage("10000", "別お届け先 呼称"));
                errorList.Add(ErsResources.GetMessage("10000", "別お届け先 お名前（姓）"));
                errorList.Add(ErsResources.GetMessage("10000", "別お届け先 郵便番号"));
                errorList.Add(ErsResources.GetMessage("10000", "別お届け先 都道府県"));
                errorList.Add(ErsResources.GetMessage("10000", "別お届け先 市区町村"));
                errorList.Add(ErsResources.GetMessage("10000", "別お届け先 番地、アパート、マンション名"));
                errorList.Add(ErsResources.GetMessage("10000", "別お届け先 電話番号"));

                //[Assertion]
                errorList.Should().ContainInOrder(validation_message.error_messages);
            }
        }

        /// <summary>
        /// TEST 1-4-2 Check Add Zip Validation
        /// </summary>
        /// <remarks>
        /// Check if addressbook the composite validation in check add zip validation has been carried out correctly.
        /// </remarks>
        [TestCaseSource("Drivers")]
        public void Test1_4_2(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndFront;

                //Processiding to My Page
                this.ProceedToMyPage(driver, wait, pageEnd);
                //Processiding to Registration Page
                this.ProceedToRegistPage(driver, wait, pageEnd);


                // == TEST == //
                // [TEST 1_4_2]
                // Check if addressbook the composite validation in check add zip validation has been carried out correctly.

                var address1 = new address1();
                PageFactory.InitElements(driver, address1);

                this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address1.asp", pageEnd);

                this.InputModifyValues(address1);

                address1.add_zip.SendKeys("-");

                //[Expected Error]
                var validate_message = ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("add_zip", address1.add_zip.GetValue(), "add_pref", 28);

                address1.submit.Click();
                wait.Until(ExpectedConditions.ElementExists(pageEnd));

                var validation_message = new ValidationMessage();
                PageFactory.InitElements(driver, validation_message);

                //[Assertion]
                validation_message.error_messages.Should().Contain(validate_message.ErrorMessage);

            }
        }

        /// <summary>
        /// TEST 1-5
        /// </summary>
        /// <remarks>
        /// Check If addressbook values is displayed correctly in address modify confirmation page
        /// </remarks>
        [TestCaseSource("Drivers")]
        public void Test1_5(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndFront;

                //Processiding to My Page
                this.ProceedToMyPage(driver, wait, pageEnd);

                //Processiding to Modification Page
                this.ProceedToModifyPage(driver, wait, pageEnd);


                // == TEST == //
                // [TEST 1_5]
                // Check If addressbook values is displayed correctly in address modify confirmation page

                var address1 = new address1();
                PageFactory.InitElements(driver, address1);

                this.InputModifyValues(address1);

                address1.submit.Click();
                wait.Until(ExpectedConditions.ElementExists(pageEnd));

                var address2 = new address2();
                PageFactory.InitElements(driver, address2);

                //[Expected values]
                var address_name = string.Format("{0}変更", AddressTestDataAttribute.objAddress.address_name);
                var add_name = string.Format("{0}変更 {1}変更", AddressTestDataAttribute.objAddress.add_lname, AddressTestDataAttribute.objAddress.add_fname);
                var add_namek = string.Format("{0}ヘンコウ {1}ヘンコウ", AddressTestDataAttribute.objAddress.add_lnamek, AddressTestDataAttribute.objAddress.add_fnamek);
                var add_zip = "651-0084";
                var add_pref = "兵庫県";
                var add_address = "神戸市中央区磯辺通 3丁目2-11 三宮ファーストビル3F";
                var add_tel = "33333333333";
                var add_fax = "44444444444";

                //[Assertion]
                address2.address_name.Text.Should().Be(address_name);
                address2.add_name.Text.Should().Be(add_name);
                address2.add_namek.Text.Should().Be(add_namek);
                address2.add_zip.Text.Should().Be(add_zip);
                address2.add_pref.Text.Should().Be(add_pref);
                address2.add_address.Text.Should().Be(add_address);
                address2.add_tel.Text.Should().Be(add_tel);
                address2.add_fax.Text.Should().Be(add_fax);
            }
        }

        /// <summary>
        /// TEST 1-6-1 Registration
        /// </summary>
        /// <remarks>
        /// Check If addressbook values is registered correctly in completion page
        /// </remarks>
        [TestCaseSource("Drivers")]
        public void Test1_6_1(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndFront;

                //Processiding to My Page
                this.ProceedToMyPage(driver, wait, pageEnd);
                //Processiding to Registration Page
                this.ProceedToRegistPage(driver, wait, pageEnd);

                var address1 = new address1();
                PageFactory.InitElements(driver, address1);

                this.InputModifyValues(address1);

                address1.submit.Click();
                this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address2.asp", pageEnd);


                // == TEST == //
                //[TEST 1_6_1] 
                // Assert Check If addressbook values is new registered correctly in completion page

                var address2 = new address2();
                PageFactory.InitElements(driver, address2);

                address2.submit.Click();
                this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address3.asp", pageEnd);

                var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
                var last_seq_id = ErsFactory.ersTableSequenceFactory.GetLastValueBySequenceName("addressbook_t_id_seq");

                var criteria = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();
                criteria.id = (int?)last_seq_id;
                var objAddress = repository.FindSingle(criteria);

                //Deleted new registered
                repository.Delete(objAddress);

                //[Expected values]
                var address_name = "変更";
                var add_lname = "変更";
                var add_fname = "変更";
                var add_lnamek = "ヘンコウ";
                var add_fnamek = "ヘンコウ";
                var add_zip = "651-0084";
                var add_pref = 28;
                var add_address = "神戸市中央区磯辺通";
                var add_taddress = "3丁目2-11";
                var add_maddress = "三宮ファーストビル3F";
                var add_tel = "33333333333";
                var add_fax = "44444444444";

                //[Assertion]
                objAddress.address_name.Should().Be(address_name);
                objAddress.add_lname.Should().Be(add_lname);
                objAddress.add_fname.Should().Be(add_fname);
                objAddress.add_lnamek.Should().Be(add_lnamek);
                objAddress.add_fnamek.Should().Be(add_fnamek);
                objAddress.add_zip.Should().Be(add_zip);
                objAddress.add_pref.Should().Be(add_pref);
                objAddress.add_address.Should().Be(add_address);
                objAddress.add_taddress.Should().Be(add_taddress);
                objAddress.add_maddress.Should().Be(add_maddress);
                objAddress.add_tel.Should().Be(add_tel);
                objAddress.add_fax.Should().Be(add_fax);
            }
        }


        /// <summary>
        /// TEST 1-6-2 Modification
        /// </summary>
        /// <remarks>
        /// Check If addressbook values is registered correctly in completion page
        /// </remarks>
        [TestCaseSource("Drivers")]
        public void Test1_6_2(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndFront;

                //Processiding to My Page
                this.ProceedToMyPage(driver, wait, pageEnd);
                //Processiding to Modification Page
                this.ProceedToModifyPage(driver, wait, pageEnd);

                var address1 = new address1();
                PageFactory.InitElements(driver, address1);

                this.InputModifyValues(address1);

                address1.submit.Click();
                this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address2.asp", pageEnd);


                // == TEST == //
                //[TEST 1_6_2] 
                // Assert Check If addressbook values is registered correctly in completion page

                var address2 = new address2();
                PageFactory.InitElements(driver, address2);

                address2.submit.Click();
                this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address3.asp", pageEnd);

                var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
                var criteria = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();
                criteria.id = AddressTestDataAttribute.objAddress.id;
                var objAddress = repository.FindSingle(criteria);

                //[Expected values]
                var address_name = string.Format("{0}変更", AddressTestDataAttribute.objAddress.address_name);
                var add_lname = string.Format("{0}変更", AddressTestDataAttribute.objAddress.add_lname);
                var add_fname = string.Format("{0}変更", AddressTestDataAttribute.objAddress.add_fname);
                var add_lnamek = string.Format("{0}ヘンコウ", AddressTestDataAttribute.objAddress.add_lnamek);
                var add_fnamek = string.Format("{0}ヘンコウ", AddressTestDataAttribute.objAddress.add_fnamek);
                var add_zip = "651-0084";
                var add_pref = 28;
                var add_address = "神戸市中央区磯辺通";
                var add_taddress = "3丁目2-11";
                var add_maddress = "三宮ファーストビル3F";
                var add_tel = "33333333333";
                var add_fax = "44444444444";

                //[Assertion]
                objAddress.address_name.Should().Be(address_name);
                objAddress.add_lname.Should().Be(add_lname);
                objAddress.add_fname.Should().Be(add_fname);
                objAddress.add_lnamek.Should().Be(add_lnamek);
                objAddress.add_fnamek.Should().Be(add_fnamek);
                objAddress.add_zip.Should().Be(add_zip);
                objAddress.add_pref.Should().Be(add_pref);
                objAddress.add_address.Should().Be(add_address);
                objAddress.add_taddress.Should().Be(add_taddress);
                objAddress.add_maddress.Should().Be(add_maddress);
                objAddress.add_tel.Should().Be(add_tel);
                objAddress.add_fax.Should().Be(add_fax);
            }
        }

        /// <summary>
        /// TEST 1-6-3 Deletion
        /// </summary>
        /// <remarks>
        /// Check If addressbook is correctly deleted in completion page
        /// </remarks>
        [TestCaseSource("Drivers")]
        public void Test1_6_3(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndFront;

                //Processiding to My Page
                this.ProceedToMyPage(driver, wait, pageEnd);
                //Processiding to deletion Page
                this.ProceedToDeletePage(driver, wait, pageEnd);


                // == TEST == //
                //[TEST 1_6_3] 
                // Assert Check If addressbook is correctly deleted in completion page

                var address2 = new address2();
                PageFactory.InitElements(driver, address2);

                address2.delete_submit.Click();
                this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address_delete2.asp", pageEnd);

                var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
                var criteria = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();
                criteria.id = AddressTestDataAttribute.objAddress.id;
                long recordCount = repository.GetRecordCount(criteria);

                recordCount.Should().Be(0);
            }
        }
        #endregion

        #region "Helper function"

        /// <summary>
        /// Proceed to test target page
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="wait"></param>
        /// <param name="pageEnd"></param>
        private void ProceedToMyPage(IWebDriver driver, WebDriverWait wait, By pageEnd)
        {
            //Login to member site
            LoginToMember.Login(driver, MemberTestDataAttribute.objMember.email, MemberTestDataAttribute.objMember.passwd);

            //Proceed to addresslist page
            var user = new user();
            PageFactory.InitElements(driver, user);
            //Wait until into next page and next page end
            this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/login.asp", pageEnd);
            user.address_list_link.Click();
            wait.Until(ExpectedConditions.ElementExists(pageEnd));
        }

        /// <summary>
        /// Proceed to test target page
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="wait"></param>
        /// <param name="pageEnd"></param>
        private void ProceedToModifyPage(IWebDriver driver, WebDriverWait wait, By pageEnd)
        {
            //Proceed to address modify page
            var address_list = new address_list();
            PageFactory.InitElements(driver, address_list);
            //Wait until into next page and next page end
            this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address_list.asp", pageEnd);
            address_list.first_row_submit.Click();
            wait.Until(ExpectedConditions.ElementExists(pageEnd));

            this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address1.asp", pageEnd);
        }

        /// <summary>
        /// Proceed to test target page
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="wait"></param>
        /// <param name="pageEnd"></param>
        private void ProceedToRegistPage(IWebDriver driver, WebDriverWait wait, By pageEnd)
        {
            //Proceed to address modify page
            var address_list = new address_list();
            PageFactory.InitElements(driver, address_list);
            //Wait until into next page and next page end
            this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address_list.asp", pageEnd);
            address_list.regist_submit.Click();
            wait.Until(ExpectedConditions.ElementExists(pageEnd));

            this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address1.asp", pageEnd);
        }

        /// <summary>
        /// Proceed to test target page
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="wait"></param>
        /// <param name="pageEnd"></param>
        private void ProceedToDeletePage(IWebDriver driver, WebDriverWait wait, By pageEnd)
        {
            //Proceed to address modify page
            var address_list = new address_list();
            PageFactory.InitElements(driver, address_list);
            //Wait until into next page and next page end
            this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address_list.asp", pageEnd);
            address_list.delete_submit.Click();
            wait.Until(ExpectedConditions.ElementExists(pageEnd));

            this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address_delete.asp", pageEnd);
        }

        /// <summary>
        /// Inputs modified value
        /// </summary>
        /// <param name="address1"></param>
        private void InputModifyValues(address1 address1)
        {
            address1.add_zip.Clear();
            address1.add_zip.SendKeys("651-0084");
            address1.search_zip.Click();

            address1.address_name.SendKeys("変更");
            address1.add_lname.SendKeys("変更");
            address1.add_fname.SendKeys("変更");
            address1.add_lnamek.SendKeys("ヘンコウ");
            address1.add_fnamek.SendKeys("ヘンコウ");
            address1.add_taddress.Clear();
            address1.add_taddress.SendKeys("3丁目2-11");
            address1.add_maddress.Clear();
            address1.add_maddress.SendKeys("三宮ファーストビル3F");
            address1.add_tel.Clear();
            address1.add_tel.SendKeys("33333333333");
            address1.add_fax.Clear();
            address1.add_fax.SendKeys("44444444444");
        }

        #endregion
    }
}
