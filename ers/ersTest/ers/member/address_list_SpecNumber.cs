﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Firefox;
using ersTestLibrary.common;
using jp.co.ivp.ers;
using ersTestLibrary.ers.member;
using FluentAssertions;
using ersTestLibrary.Site.ers.createData;
using ersTestLibrary.Site.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;

namespace ersTest.ers.member
{
    /// <summary>
    /// Basically, the number of TestFixture related to a specification file is only one.
    /// This class have to inherits TestCommon class.
    /// </summary>
    [TestFixture, MemberTestData, AddressTestData(countData = 44, single_list = new[] { "Test1_1" })]
    public class address_list_SpecNumber
        : ErsTestCommon
    {
        #region "Text Case"
        /// <summary>
        /// TEST 1-1, 1-2 SORTING and CHECK DISPLAY
        /// </summary>
        /// <remarks>
        /// Check If addressbook values is displayed correctly in address list
        /// </remarks>
        [TestCaseSource("Drivers")]
        public void Test1_1(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndFront;

                this.ProceedToTargetPage(driver, wait, pageEnd);


                // == TEST == //
                //[TEST 1-1]
                // if the table contains only header or contains correct rows.
                var address_list = new address_list();
                PageFactory.InitElements(driver, address_list);
                var allRows = address_list.table.FindElements(By.TagName("tr"));

                //[Assertion]
                allRows.Should().HaveCount((c) => c == 2, "table doesn't contains correct rows.");

                for (var rowNumber = 1; rowNumber < allRows.Count; rowNumber++) //Skip header
                {
                    var row = allRows[rowNumber];

                    //Get cell texts.
                    var cells = row.FindElements(By.TagName("td"));
                    var cellValues = cells.Select((cell) => cell.Text);

                    //[Expected values]
                    var address_name = AddressTestDataAttribute.objAddress.address_name;
                    var name = string.Format("{0} {1}", AddressTestDataAttribute.objAddress.add_lname, AddressTestDataAttribute.objAddress.add_fname);
                    var address = string.Format("{0} {1} {2} {3}",
                        ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(AddressTestDataAttribute.objAddress.add_pref),
                        AddressTestDataAttribute.objAddress.add_address,
                        AddressTestDataAttribute.objAddress.add_taddress,
                        AddressTestDataAttribute.objAddress.add_maddress);

                    //[Assertion]
                    cellValues.Should().ContainInOrder(new[] { address_name, name, address }, "table row doesn't contains correct values.");
                }
            }
        }
        #endregion

        #region "Text Case"
        /// <summary>
        /// TEST 1-1, 1-2 PAGER
        /// </summary>
        /// <remarks>
        /// Check If addressbook values is displayed correctly in address list with pager
        /// </remarks>
        [TestCaseSource("Drivers")]
        public void Test1_2(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndFront;

                this.ProceedToTargetPage(driver, wait, pageEnd);


                // == TEST == //
                //[TEST 1-2]
                // if the table contains only header or contains correct rows.
                var address_list = new address_list();
                PageFactory.InitElements(driver, address_list);

                var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
                var criteria = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

                //検索条件をクライテリアに保存
                criteria.mcode = MemberTestDataAttribute.objMember.mcode;

                long recordCount = repository.GetRecordCount(criteria);
                long LastPage = address_list.CountAllPage(recordCount);

                //検索SQLにLIMIT と OFFSETを加える
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);

                for (var pageCnt = 1; pageCnt <= LastPage; pageCnt++)
                {
                    address_list.SetLimitAndOffsetToCriteria(pageCnt, criteria);

                    var list = repository.Find(criteria);

                    wait.Until(ExpectedConditions.ElementExists(address_list.tableBy));

                    this.RowListChecker(address_list, list);

                    if (pageCnt != LastPage)
                    {
                        driver.FindElement(By.Name("pagesend_cnt" + (pageCnt + 1))).Submit();
                        wait.Until(ExpectedConditions.ElementExists(pageEnd));
                        wait.Until(ExpectedConditions.ElementExists(address_list.ByPagerValue(pageCnt + 1)));
                    }
                }
            }
        }
        #endregion

        #region "Helper function"

        protected void RowListChecker(address_list address_list, IList<ErsAddressInfo> objAddressList)
        {
            var allRows = address_list.table.FindElements(By.TagName("tr"));

            //[Assertion] With Header(objAddressList.Count + 1)
            allRows.Should().HaveCount((c) => c == objAddressList.Count + 1, "table doesn't contains correct rows.");

            for (var rowNumber = 1; rowNumber < allRows.Count; rowNumber++) //Skip header
            {
                var objAddress = objAddressList[rowNumber - 1];
                var row = allRows[rowNumber];

                //Get cell texts.
                var cells = row.FindElements(By.TagName("td"));
                var cellValues = cells.Select((cell) => cell.Text);

                //[Expected values]
                var address_name = objAddress.address_name;
                var name = string.Format("{0} {1}", objAddress.add_lname, objAddress.add_fname);
                var address = string.Format("{0} {1} {2} {3}",
                    ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(objAddress.add_pref),
                    objAddress.add_address,
                    objAddress.add_taddress,
                    objAddress.add_maddress);

                //[Assertion]
                cellValues.Should().ContainInOrder(new[] { address_name, name, address }, "table row doesn't contains correct values.");
            }
        }

        private void ProceedToTargetPage(IWebDriver driver, WebDriverWait wait, By pageEnd)
        {
            LoginToMember.Login(driver, MemberTestDataAttribute.objMember.email, MemberTestDataAttribute.objMember.passwd);

            //Proceed to addresslist page
            var user = new user();
            PageFactory.InitElements(driver, user);

            //Wait until into next page and next page end
            this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/login.asp");
            wait.Until(ExpectedConditions.ElementExists(pageEnd));

            user.address_list_link.Click();
            wait.Until(ExpectedConditions.ElementExists(pageEnd));

            this.WaitUntilIntoContainsNextPage(driver, wait, "top/member/asp/address_list.asp");
        }

        #endregion
    }
}



