﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ersLibraryTest.Site.ers.createData;
using ersTestLibrary.common;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ersTestLibrary.ers.member;
using FluentAssertions;
using OpenQA.Selenium.Support.PageObjects;
using jp.co.ivp.ers;
using ersTestLibrary.Site.ers.createData;
using ersTestLibrary.Site.ers;

namespace ersTest.ers.member
{
    /// <summary>
    /// Basically, the number of TestFixture related to a specification file is only one.
    /// This class have to inherits TestCommon class.
    /// </summary>
    [TestFixture, MemberTestData, OrderTestData]
    public class order_list_SpecNumber
        : ErsTestCommon
    {
         #region "Text Case"
        /// <summary>
        /// TEST 1-1, 1-2
        /// </summary>
        /// <remarks>
        /// Check If Member Order values is displayed correctly in Bill list
        /// </remarks>
        [TestCaseSource("Drivers")]
        public void Test1_1(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndFront;

                this.ProceedToTargetPage(driver, wait, pageEnd);


                // == TEST == //
                //[TEST 1-1]
                // if the table contains only header or contains correct rows.
                var bill_list = new bill2();
                PageFactory.InitElements(driver, bill_list);
                var allRows = bill_list.table.FindElements(By.TagName("tr"));

                //[Assertion]
                allRows.Should().HaveCount((c) => c == 2, "table doesn't contains correct rows.");

                for (var rowNumber = 1; rowNumber < allRows.Count; rowNumber++) //Skip header
                {
                    var row = allRows[rowNumber];

                    //Get cell texts.
                    var cells = row.FindElements(By.TagName("td"));
                    var cellValues = cells.Select((cell) => cell.Text);

                    //[Expected values]
                    string d_no = OrderTestDataAttribute.objOrder.d_no;
                    string intime = OrderTestDataAttribute.objOrder.intime.Value.ToString("yyyy/MM/dd");
                    string total = "¥" + String.Format("{0:#,0}", OrderTestDataAttribute.objOrder.total);
                    string w_order_status2 = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.FrontOrderStatus, EnumCommonNameColumnName.namename, (int)OrderTestDataAttribute.GetFrontOrderStatus());

                    //[Assertion]
                    cellValues.Should().ContainInOrder(new[] { d_no, intime, total, w_order_status2 }, "table row doesn't contains correct values.");
                }
            }
        }
        #endregion

        #region "Helper function"
        private void ProceedToTargetPage(IWebDriver driver, WebDriverWait wait, By pageEnd)
        {
            LoginToMember.Login(driver, MemberTestDataAttribute.objMember.email, MemberTestDataAttribute.objMember.passwd);

            //Proceed to addresslist page
            var user = new user();
            PageFactory.InitElements(driver, user);
            user.bill1_link.Click();
            wait.Until(ExpectedConditions.ElementExists(pageEnd));

            var bill1 = new bill1();
            PageFactory.InitElements(driver, bill1);
            bill1.search.Click();
            wait.Until(ExpectedConditions.ElementExists(pageEnd));
        }
        #endregion

    }
}
