﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersTestLibrary.common;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;


namespace ersTest.ers.member.testElements
{
    public class cardElements
         : ErsElementContainerControlBase
    {

        /// <summary>
        /// LINK お客様情報を変更する
        /// </summary>
        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article[1]/ul[5]/li/a")]
        public IWebElement card_ref { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='contents']/form[2]/div/p[2]/input")]
        public IWebElement card_confirm { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='contents']/div[2]/form[1]/p/input")]
        public IWebElement card_complete { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/form/div[1]/div/p[2]/input")]
        public IWebElement button_delete { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/div[2]/form[1]/p/input")]
        public IWebElement button_delete_complete { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/form/div[1]/div/p[1]/input")]
        public IWebElement button_update { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/form/div[2]/div/p/input")]
        public IWebElement button_update_confirm { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='contents']/div[2]/form[1]/p/input")]
        public IWebElement button_update_complete { get; set; }
        
        [FindsBy(How = How.Name, Using = "card_type")]
        [ErsAssertElement(AssertionType.TextShouldEqual)]
        public IWebElement card_type { get; set; }

        [FindsBy(How = How.Name, Using = "card_no")]
        [ErsAssertElement(AssertionType.TextShouldEqual)]
        public IWebElement card_no { get; set; }

        [FindsBy(How = How.Name, Using = "validity_y")]
        [ErsAssertElement(AssertionType.TextShouldEqual)]
        public IWebElement validity_y { get; set; }

        [FindsBy(How = How.Name, Using = "validity_m")]
        [ErsAssertElement(AssertionType.TextShouldEqual)]
        public IWebElement validity_m { get; set; }

    }
}
