﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersTestLibrary.common;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;


namespace ersTest.ers.member.testElements
{
    public class loginElements
         : ErsElementContainerControlBase
    {
        [FindsBy(How = How.Name, Using = "email")]
        [ErsAssertElement(AssertionType.TextShouldEqual)]
        public IWebElement email { get; set; }

        [FindsBy(How = How.Name, Using = "passwd")]
        [ErsAssertElement(AssertionType.TextShouldEqual)]
        public IWebElement password { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/form/div[1]/div/p[2]/input")]
        public IWebElement submit_login { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article[1]/ul[2]/li/a")]
        public IWebElement wishlist_href { get; set; }

        /// <summary>
        /// LINK お客様情報を変更する
        /// </summary>
        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article[1]/ul[4]/li[1]/a")]
        public IWebElement member1_ref { get; set; }

    }
}
