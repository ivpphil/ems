﻿1) Prepare for test

	1-1) Prepare for test

		Need to configurate IE security settings: show below
		http://code.google.com/p/selenium/wiki/InternetExplorerDriver#Required_Configuration

	1-2) Setting of site

		Off the BASIC Authentication since Selenium can't test over it.
		Tests on http site even if it is secure page because Selenium can't test over Self-signed certificate SSL

	1-3) Execute the test cases

		Begin debug with specifying ersTest project in VWD.
		Or, Load test with Nunit with specifying ersTest.dll.

2) Creation of test data

	2-1)Creation of test data
		Create test data for test at a class that implementes ITestAction interface.
		★Create the class in ersTestLibrary\ers\createData\。

		*ITestAction.Targets = ActionTargets.Suite → The creation executed once for each TestFixture.
		*ITestAction.Targets = ActionTargets.Test →The creation executed once for each TestCase.

	2-2)Screen class
		Please create Screen classes that are used from WebDeveloper in under ers directory.
		e.g) 
		A screen class for Mypage menu（member/asp/user.asp）is created as
		ersTestLibrary\ers\member\user.cs

	2-3)Test class
		★The name of the test class must be 
			<Page name>_<Number of spec>.cs

		★A granularity for each factors is:
			TextFixture(Class) => Specification file
			TextCase(Test method) => Test case number

		* Note to don't write many tests in one test case.
		（Divid tests）

		* Get entity using Selenium.Support.PageObjects classes.
		* Get entity in the Screen using By.ID instead of By.Xpath as much as possible.

3) Structure of test case project

	3-1) Frameworks

		・NUnit
		・Selenium WebDriver
			- A driver is needed by IE and Chrome
				http://code.google.com/p/selenium/downloads/list
		・FluentAssertions
			https://fluentassertions.codeplex.com/documentation
			To use this framework, please write
			using FluentAssertions;
			On the top of souce-code。

	3-2) ersTest
		
		・Tests are written in this project
	
	3-3) ersTestLibrary
		
		・Define classes that can be used in each test cases in ersTest

4) Creation of test cases.

	4-0) Guideline
		4-0-1) Displayed data of each pages.
			   Checks if the data output dynamically by ERS dynamically are displayed correctly.
			   If a visible data would be changed by the state of data, test the all branching.

		4-0-2) Confirmation pages.
			   Check if a data modified in a input page are displayed correctly.

		4-0-3) Completion pages.
			   Check if a data defined in specification are Updated/Inserted/Updated correctly.
			   If a data would be changed by the state of data, test the all branching.


	4-1) Inputing search fileter page.
		4-1-1) Create Test Data into DB for the Test.

		4-1-2) Input all filter condition, then click search bottun, then confirm if the test data is displayed in search result.

		4-1-3) Change one filter condition from input in 4-1-2, Then confirm if the test data become not to be displayed in search result.
		　　　 And repeat the step for each filter condition.


	4-2) Search result screen.（Test displaying data.）
		4-2-1) Create Test Data into DB for the Test.

		4-2-2) Check if followings are displayed correctly.
				- The order of columns are correctly.
				- The data in column are displayed correctly.


	4-3) Search result screen.（Test pager）
		4-1-1) Create some Test Data into DB for the pager Test.

		4-3-2) Click pager link, then check if the paging is done correctly.
		
		4-3-3) Check if the order of datas are sorted correctly.


	4-4) Search result screen.（Test CSV download）
		4-4-1) Click the csv download button in screen of 4-3

		4-4-2) Check if The number of row is correct amount.

		4-4-3) Check if the order of datas are sorted correctly.


	4-5) Data Input/modify/delete page.
		4-5-1) Transit to input page.（By emulate the user operation with selenium.）

		4-5-2) Checks if the data output dynamically by ERS dynamically are displayed correctly.

		4-5-3) Modify all value in the input page.

		4-5-4) Check if the composite validation check has been carried out correctly.

		4-5-5) Check if a data modified in a input page are displayed correctly in confirmation page.

		4-5-6) Transit to completion page, Then check if all data are inserted/modifyed/deleted correctly by select from database.
			   If several tables in db are manipulated, check all data in these tables.

		4-5-7) Repeat from 4-5-2 to 4-5-6 if a data would be changed by the state of data
