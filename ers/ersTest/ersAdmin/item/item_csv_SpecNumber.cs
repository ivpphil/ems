﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ersTestLibrary.ersAdmin.createData;
using ersTestLibrary.common;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ersTestLibrary.ersAdmin;
using jp.co.ivp.ers;
using ersTestLibrary.ersAdmin.item;
using OpenQA.Selenium.Support.PageObjects;

namespace ersTest.ersAdmin.item
{
    [TestFixture, AdministratorTestData]
    public class item_csv_SpecNumber
        : ErsTestCommon
    {
        #region "Text Case"
        /// <summary>
        /// TEST 1-1
        /// </summary>
        /// <remarks>
        /// Test for csv download
        /// </remarks>
        [TestCaseSource("Drivers")]
        public void Test1_1(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndAdmin;

                this.ProceedToSearchResult(driver, wait, pageEnd);

                var item_csv = new item_csv();
                PageFactory.InitElements(driver, item_csv);

                // Load ersTest/upload/ersAdmin/item/cus_list_SpecNumber_1_1.csv
                item_csv.csv_file.SendKeys(CommonVariables.UploadFilePath + @"\ersAdmin\item\cus_list_SpecNumber_1_1.csv");

                item_csv.submit_csv_file.Click();

                item_csv.submit_csv_file_dialog.Click();
                wait.Until(ExpectedConditions.ElementExists(pageEnd));

                // == Prepare == //
                // Plase test the result here.
            }
        }
        #endregion

        #region "Helper function"
        /// <summary>
        /// Proceed to test target page
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="wait"></param>
        /// <param name="pageEnd"></param>
        private void ProceedToSearchResult(IWebDriver driver, WebDriverWait wait, By pageEnd)
        {
            //Login to member site
            LoginToAdmin.Login(driver, AdministratorTestData.objAdministrator.user_login_id, AdministratorTestData.objAdministrator.passwd);

            //Proceed to addresslist page
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            driver.Url = setup.admin_sec_url + "top/item/asp/item_csv.asp";
            wait.Until(ExpectedConditions.ElementExists(pageEnd));
        }
        #endregion
    }
}
