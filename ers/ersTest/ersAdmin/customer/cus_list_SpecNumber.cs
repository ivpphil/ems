﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ersTestLibrary.ersAdmin.createData;
using OpenQA.Selenium;
using ersTestLibrary.common;
using OpenQA.Selenium.Support.UI;
using ersTestLibrary.ersAdmin;
using ersTestLibrary.ersAdmin.home;
using OpenQA.Selenium.Support.PageObjects;
using ersTestLibrary.ersAdmin.customer;
using ersTestLibrary.Site.ers.createData;
using jp.co.ivp.ers;
using ersTestLibrary.ersAdmin.customer.csv;
using FluentAssertions;

namespace ersTest.ersAdmin.customer
{
    [TestFixture, AdministratorTestData, MemberTestData]
    public class cus_list_SpecNumber
        : ErsTestCommon
    {
        #region "Text Case"
        /// <summary>
        /// TEST 1-3
        /// </summary>
        /// <remarks>
        /// Test for csv download
        /// </remarks>
        [TestCaseSource("Drivers")]
        public void Test1_1(EnumBrowser browserName, Func<IWebDriver> driverInstantiation)
        {
            using (var driver = driverInstantiation())
            {
                // == Prepare == //
                var wait = CommonVariables.GetDefaultWait(driver);
                var pageEnd = CommonVariables.PageEndAdmin;

                this.ProceedToSearchResult(driver, wait, pageEnd);

                var cus_list = new cus_list();
                PageFactory.InitElements(driver, cus_list);

                // Load CSV
                var csvLoader = ErsFactory.ersUtilityFactory.GetErsCsvContainer<customer_csv>();
                using (var testFileLoader = TestFileLoader.Begin(browserName))
                {
                    cus_list.downlad_page.Click();

                    csvLoader.LoadPostedFile(testFileLoader.GetDownloadedFile());
                }

                foreach (var model in csvLoader.GetValidatedModels(false))
                {
                    if (!model.IsValid)
                    {
                        // check validation error
                        String.Join(Environment.NewLine, model.GetAllErrorMessageList()).Should().BeNullOrEmpty();
                    }

                    // == TEST == //
                    //check values ****This is sample. Please check value in more detail.****
                    model.lname.Should().NotBeNullOrEmpty();
                    model.fname.Should().NotBeNullOrEmpty();
                    // ....
                    model.intime.Should().HaveValue();
                }
            }
        }
        #endregion

        #region "Helper function"
        /// <summary>
        /// Proceed to test target page
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="wait"></param>
        /// <param name="pageEnd"></param>
        private void ProceedToSearchResult(IWebDriver driver, WebDriverWait wait, By pageEnd)
        {
            //Login to member site
            LoginToAdmin.Login(driver, AdministratorTestData.objAdministrator.user_login_id, AdministratorTestData.objAdministrator.passwd);

            //Proceed to addresslist page
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            driver.Url = setup.admin_sec_url + "top/customer/asp/cus_search.asp";
            wait.Until(ExpectedConditions.ElementExists(pageEnd));

            //Proceed to address modify page
            var cus_search = new cus_search();
            PageFactory.InitElements(driver, cus_search);
            cus_search.submit.Click();
            wait.Until(ExpectedConditions.ElementExists(pageEnd));
        }
        #endregion
    }
}
