﻿1) テストの準備

	1-1) テストの準備

		IEの設定変更が必要です。
		http://code.google.com/p/selenium/wiki/InternetExplorerDriver#Required_Configuration

	1-2) サイトの設定

		BASIC認証を超えれないので、OFFにする。
		自己証明SSLの場合、エラー画面をはさんでしまうので、HTTPでテストする。

	1-3) テストの実行

		VWDで、ersTestプロジェクトを指定して、デバッグ実行する。
		または、NunitにersTest.dllを指定して、テストを起動する。

2) テストデータの作成

	2-1)テストデータ作成
		テストデータの作成はITestActionを実装したクラスにて行う。
		★ersTestLibrary\ers\createData\内に作成する。

		*ITestAction.Targets = ActionTargets.Suite →TestFixtureごとに1回実行される
		*ITestAction.Targets = ActionTargets.Test →TestCaseごとに1回実行される

	2-2)画面クラス
		WebDeveloperが使用する画面クラスは、
		★ers以下に作成する。
		e.g) 
		マイページメニュー（member/asp/user.asp）の画面クラスは
		ersTestLibrary\ers\member\user.cs に作成する。

	2-3)テストクラス
		★クラス名は<ページ名>_<仕様書番号>.csとする

		★以下の単位で作成する
			TextFixture(Class) => Specification file
			TextCase(Test method) => Test case number

		* 1つのテストケースに、多くのテストは記載しない。
		（テストケースを分ける）

		* 画面の要素はSelenium.Support.PageObjectsクラスを使用して取得する
		* 画面の要素を取得する場合は、できるだけXpathではなくてIDで取得するようにする。(By.Id)

3) テスト用プロジェクトの構成

	3-1) 使用フレームワーク

		・NUnit
		・Selenium WebDriver
			- IEとChromeはDriverが必要
				http://code.google.com/p/selenium/downloads/list
		・FluentAssertions
			https://fluentassertions.codeplex.com/documentation
			使用するために、書くクラスで
			using FluentAssertions;
			を記述する。

	3-2) ersTest
		
		・テストを記述する。
	
	3-3) ersTestLibrary
		
		・ersTestの各テストで共通で使用できるクラスを定義する。

4)テストの作成

	4-0)基本指針
		4-0-1)初期表示
			動的に表示される全てのデータについて初期表示が正しいかをチェックする
			データによって分岐する項目がある場合は、その分岐が機能しているかをテストする

		4-0-2)確認画面
			複合的な入力チェックが正しく行われているかを確認する。
			動的に表示される全てのデータについて入力の変更が正しく反映されるかをチェックする。

		4-0-3)完了画面
			複合的な入力チェックが正しく行われているかを確認する。
			動的に表示される全てのデータについてDBの更新が正しく行われているかをチェックする。
			データによって分岐する項目がある場合は、その分岐が機能しているかをテストする


	4-1)検索ページ
		4-1-1)検索対象のテストデータを登録する。

		4-1-2)検索条件すべてを入力し、テストデータが表示されるかを確認する。

		4-1-3)4-1-2の検索条件から、1項目づつデータを変更する。
		　　　　変更した結果、テストデータが検索にかからなくなることを確認する。


	4-2)検索結果一覧（表示データチェック）
		4-2-1)表示データチェック用のデータを登録する。

		4-2-2)表示データについて、カラムの並び順・表示されているデータが正しいかを確認する。


	4-3)検索結果一覧（ページャーチェック）
		4-3-1)ページャー確認用のテストデータを登録する

		4-3-2)ページ送りをクリックし、正常に遷移することを確認する。
		
		4-3-3)ソート順が、仕様どおりのソートになっていることを確認する。


	4-4)検索結果一覧（CSVダウンロード）
		4-4-1)CSVダウンロードをクリックする。

		4-4-2)取得件数が正しいかどうかを確認する。

		4-4-3)データの並びが正しく出力されているかを確認する。


	4-5)詳細ページ
		4-5-1)詳細ページに遷移する。（該当ページまで、ユーザの操作をエミュレートして遷移する。）

		4-5-2)初期表示データが正しく表示されているかを確認する。

		4-5-3)全項目のデータを変更し、確認画面へ遷移する

		4-5-4)複合的な入力チェックが正しく行われているかを確認する。

		4-5-5)全項目のデータの変更が確認画面で表示されていることを確認する。

		4-5-6)完了画面へ遷移し、DBをSELECTして変更データがすべて適用されていることを確認する。
		　（複数テーブルが更新される場合は、全てのデータをチェックする）

		4-5-7) 4-5-2 ～ 4-5-6 について、データによる分岐がある場合は分岐か機能しているかを確認する。
