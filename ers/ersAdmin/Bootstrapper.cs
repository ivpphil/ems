using System.Web.Mvc;
using System.Reflection;
using jp.co.ivp.ers.mvc.CommandProcessor.Dispatcher;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.member;

namespace ersAdmin
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            //command-handler定義 同名対応の為、フルパスで記載し、usingは使わない

            //Please sort codes for RegisterType by the controller.


            /* Api Controller */
            //Api
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Api.Commands.IGetZipCommand>, ersAdmin.Domain.Api.Handlers.ValidateGetZip>();
            container.RegisterType<IMapper<ersAdmin.Domain.Api.Mappables.IGetZipMappable>, ersAdmin.Domain.Api.Mappers.GetZipMapper>();
            container.RegisterType<IMapper<ersAdmin.Domain.Api.Mappables.IFileUploaderBaseMappable>, ersAdmin.Domain.Api.Mappers.FileUploaderBaseMapper>();

            /* AtMail Controller */
            //AtMail Rgist
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Atmail.Commands.IMailtemplateRegistCommand>, ersAdmin.Domain.Atmail.Handlers.ValidateMailtemplateRegist>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Atmail.Commands.IMailtemplateRegistCommand>, ersAdmin.Domain.Atmail.Handlers.MailtemplateRegistHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Atmail.Mappables.IMailtemplateRegistMappable>, ersAdmin.Domain.Atmail.Mappers.MailtemplateRegistMapper>();

            //AtMail Modify
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Atmail.Commands.IMailtemplateModifyCommand>, ersAdmin.Domain.Atmail.Handlers.ValidateMailtemplateModify>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Atmail.Commands.IMailtemplateModifyCommand>, ersAdmin.Domain.Atmail.Handlers.MailtemplateModifyHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Atmail.Mappables.IMailtemplateModifyMappable>, ersAdmin.Domain.Atmail.Mappers.MailtemplateModifyMapper>();
            
           //AtMail Template list
            container.RegisterType<IMapper<ersAdmin.Domain.Atmail.Mappables.IMailTemplateMappable>, ersAdmin.Domain.Atmail.Mappers.MailTemplateMapper>();

            //AtMail Delete
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Atmail.Commands.IMailtemplateDeleteCommand>, ersAdmin.Domain.Atmail.Handlers.MailtemplateDeleteHandler>();

            //AtMail Info body
            container.RegisterType<IMapper<ersAdmin.Domain.Atmail.Mappables.IMailinfoBodyMappable>, ersAdmin.Domain.Atmail.Mappers.MailinfoBodyMapper>();


            //News list copy
            container.RegisterType<IMapper<ersAdmin.Domain.Atmail.Mappables.IMailinglistMappable>, ersAdmin.Domain.Atmail.Mappers.MailinglistMapper>();

            //Total
            container.RegisterType<IMapper<ersAdmin.Domain.Atmail.Mappables.IMailinglistTotalMappable>, ersAdmin.Domain.Atmail.Mappers.MailinglistTotalMapper>();

            //News list copy confirm
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Atmail.Commands.IMailinfoNewListCommand>, ersAdmin.Domain.Atmail.Handlers.ValidateMailinfoNewList>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Atmail.Commands.IMailinfoNewListCommand>, ersAdmin.Domain.Atmail.Handlers.MailinfoNewListHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Atmail.Mappables.IMailinfoNewListMappable>, ersAdmin.Domain.Atmail.Mappers.MailinfoNewListMapper>();

            //News file del
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Atmail.Commands.IMailinfoDeleteCommand>, ersAdmin.Domain.Atmail.Handlers.ValidateMailinfoDelete>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Atmail.Commands.IMailinfoDeleteCommand>, ersAdmin.Domain.Atmail.Handlers.MailinfoDeleteHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Atmail.Mappables.IMailinfoDeleteMappable>, ersAdmin.Domain.Atmail.Mappers.MailinfoDeleteMapper>();

            //MailTo Csv Upload
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Atmail.Commands.IMailtoCSVUploadCommand>, ersAdmin.Domain.Atmail.Handlers.ValidateMailtoCSVUpload>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Atmail.Commands.IMailtoCSVUploadCommand>, ersAdmin.Domain.Atmail.Handlers.MailtoCSVUploadHandler>();

            //MailSetup
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Atmail.Commands.IMailsetupCommand>, ersAdmin.Domain.Atmail.Handlers.ValidateMailsetup>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Atmail.Commands.IMailsetupCommand>, ersAdmin.Domain.Atmail.Handlers.MailsetupHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Atmail.Mappables.IMailsetupMappable>, ersAdmin.Domain.Atmail.Mappers.MailsetupMapper>();

            //MailTo Download
            container.RegisterType<IMapper<ersAdmin.Domain.Atmail.Mappables.IMailinfoDownloadMappable>, ersAdmin.Domain.Atmail.Mappers.MailinfoDownloadMapper>();            


            //Mail Info(tmail)
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Atmail.Commands.IMailinfoCommand>, ersAdmin.Domain.Atmail.Handlers.ValidateMailinfo>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Atmail.Commands.IMailinfoCommand>, ersAdmin.Domain.Atmail.Handlers.MailinfoHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Atmail.Mappables.IMailinfoMappable>, ersAdmin.Domain.Atmail.Mappers.MailinfoMapper>();

            //TestMail
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Atmail.Commands.ITestMailCommand>, ersAdmin.Domain.Atmail.Handlers.ValidationTestMail>();
            container.RegisterType<IMapper<ersAdmin.Domain.Atmail.Mappables.ITestMailMappable>, ersAdmin.Domain.Atmail.Mappers.TestMailMapper>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Atmail.Commands.ITestMailCommand>, ersAdmin.Domain.Atmail.Handlers.TestMailHandler>();


            /* CMS */
            container.RegisterType<IMapper<ersAdmin.Domain.Cms.Mappables.IContentsListMappable>, ersAdmin.Domain.Cms.Mappers.ContentsListMapper>();

            container.RegisterType<IMapper<ersAdmin.Domain.Cms.Mappables.IContentsModifyMappable>, ersAdmin.Domain.Cms.Mappers.ContentsModifyMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IContentsModifyCommand>, ersAdmin.Domain.Cms.Handlers.ValidateContentsModify>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Cms.Commands.IContentsModifyCommand>, ersAdmin.Domain.Cms.Handlers.ContentsModifyHandler>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IContentsDeleteCommand>, ersAdmin.Domain.Cms.Handlers.ValidateContentsDelete>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Cms.Commands.IContentsDeleteCommand>, ersAdmin.Domain.Cms.Handlers.ContentsDeleteHandler>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IContentsRegistCommand>, ersAdmin.Domain.Cms.Handlers.ValidateContentsRegist>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Cms.Commands.IContentsRegistCommand>, ersAdmin.Domain.Cms.Handlers.ContentsRegistHandler>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IFreeListCommand>, ersAdmin.Domain.Cms.Handlers.ValidateFreeList>();
            container.RegisterType<IMapper<ersAdmin.Domain.Cms.Mappables.IFreeListMappable>, ersAdmin.Domain.Cms.Mappers.FreeListMapper>();

            container.RegisterType<IMapper<ersAdmin.Domain.Cms.Mappables.IFreeIndexMappable>, ersAdmin.Domain.Cms.Mappers.FreeIndexMapper>();

            container.RegisterType<IMapper<ersAdmin.Domain.Cms.Mappables.IFreeModifyMappable>, ersAdmin.Domain.Cms.Mappers.FreeModifyMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IFreeModifyCommand>, ersAdmin.Domain.Cms.Handlers.ValidateFreeModify>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Cms.Commands.IFreeModifyCommand>, ersAdmin.Domain.Cms.Handlers.FreeModifyHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IFreeImgGroupRecordCommand>, ersAdmin.Domain.Cms.Handlers.ValidateFreeImgGroupRecord>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IFreeDeleteCommand>, ersAdmin.Domain.Cms.Handlers.ValidateFreeDelete>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Cms.Commands.IFreeDeleteCommand>, ersAdmin.Domain.Cms.Handlers.FreeDeleteHandler>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IFreeTemplateCommand>, ersAdmin.Domain.Cms.Handlers.ValidateFreeTemplate>();
            container.RegisterType<IMapper<ersAdmin.Domain.Cms.Mappables.IFreeTemplateMappable>, ersAdmin.Domain.Cms.Mappers.FreeTemplateMapper>();
            
            container.RegisterType<IMapper<ersAdmin.Domain.Cms.Mappables.IFreeRegistMappable>, ersAdmin.Domain.Cms.Mappers.FreeRegistMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IFreeRegistCommand>, ersAdmin.Domain.Cms.Handlers.ValidateFreeRegist>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Cms.Commands.IFreeRegistCommand>, ersAdmin.Domain.Cms.Handlers.FreeRegistHandler>();

            container.RegisterType<IMapper<ersAdmin.Domain.Cms.Mappables.IInsertTableMappable>, ersAdmin.Domain.Cms.Mappers.InsertTableMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IInsertTableCommand>, ersAdmin.Domain.Cms.Handlers.ValidateInsertTable>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Cms.Commands.IInsertTableCommand>, ersAdmin.Domain.Cms.Handlers.InsertTableHandler>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IDeleteTableCommand>, ersAdmin.Domain.Cms.Handlers.ValidateDeleteTable>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Cms.Commands.IDeleteTableCommand>, ersAdmin.Domain.Cms.Handlers.DeleteTableHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Cms.Mappables.IDeleteTableMappable>, ersAdmin.Domain.Cms.Mappers.DeleteTableMapper>();


            /* Item Controller */
            //Item
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemCSVCommand>, ersAdmin.Domain.Item.Handlers.ValidateItemCSV>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemCSVRecordCommand>, ersAdmin.Domain.Item.Handlers.ValidateItemCSVRecord>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Item.Commands.IItemCSVCommand>, ersAdmin.Domain.Item.Handlers.ItemCSVHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IItemCSVMappable>, ersAdmin.Domain.Item.Mappers.ItemCSVMapper>();
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IItemCSVRecordMappable>, ersAdmin.Domain.Item.Mappers.ItemCSVRecordMapper>();

            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.ISetItemListCSVMappable>, ersAdmin.Domain.Item.Mappers.SetItemListCSVMapper>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemRegistCommand>, ersAdmin.Domain.Item.Handlers.ValidateItemRegist>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Item.Commands.IItemRegistCommand>, ersAdmin.Domain.Item.Handlers.ItemRegistHandler>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemModifyCommand>, ersAdmin.Domain.Item.Handlers.ValidateItemModify>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Item.Commands.IItemModifyCommand>, ersAdmin.Domain.Item.Handlers.ItemModifyHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IItemModifyMappable>, ersAdmin.Domain.Item.Mappers.ItemModifyMapper>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemModifyDetailListRecordCommand>, ersAdmin.Domain.Item.Handlers.ValidateItemModifyDetailListRecord>();
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IItemModifyDetailListRecordMappable>, ersAdmin.Domain.Item.Mappers.ItemModifyDetailListRecordMapper>();

            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IItemListMappable>, ersAdmin.Domain.Item.Mappers.ItemListMapper>();

            //  Item_modify_mall_detail
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemModifyMallAmazonDetailListRecordCommand>, ersAdmin.Domain.Item.Handlers.ValidateItemModifyMallAmazonDetailListRecord>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemModifyMallYahooDetailListRecordCommand>, ersAdmin.Domain.Item.Handlers.ValidateItemModifyMallYahooDetailListRecord>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemModifyMallRakutenDetailListRecordCommand>, ersAdmin.Domain.Item.Handlers.ValidateItemModifyMallRakutenDetailListRecord>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IMallCSVCommand>, ersAdmin.Domain.Item.Handlers.ValidateMallCSV>();
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IMallTSVMappable>, ersAdmin.Domain.Item.Mappers.MallTSVMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemTSVRecordCommand>, ersAdmin.Domain.Item.Handlers.ValidateItemTSVRecord>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Item.Commands.IMallCSVCommand>, ersAdmin.Domain.Item.Handlers.MallCSVHandler>();

            //CateRegist
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Item.Commands.ICateRegistCommand>, ersAdmin.Domain.Item.Handlers.CateRegistHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.ICateRegistCommand>, ersAdmin.Domain.Item.Handlers.ValidateCateRegist>();
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.ICateRegistMappable>, ersAdmin.Domain.Item.Mappers.CateRegistMapper>();
            //CateHeader
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.ICateHeaderListRecordCommand>, ersAdmin.Domain.Item.Handlers.ValidateCateHeaderListRecord>();
            //CateItem
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.ICateItemListRecordCommand>, ersAdmin.Domain.Item.Handlers.ValidateCateItemListRecord>();


            //ItemCoexistence
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemCoexistenceCommand>, ersAdmin.Domain.Item.Handlers.ValidateItemCoexistence>();
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IItemCoexistenceMappable>, ersAdmin.Domain.Item.Mappers.ItemCoexistenceMapper>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Item.Commands.IItemCoexistenceCommand>, ersAdmin.Domain.Item.Handlers.ItemCoexistenceHandler>();
            //ItemCoexistence detail
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemCoexistenceDetailListRecordCommand>, ersAdmin.Domain.Item.Handlers.ValidateItemCoexistenceDetailListRecord>();

            //Item List Csv Download
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IItemListCSVMappable>, ersAdmin.Domain.Item.Mappers.ItemListCSVMapper>();

            //Item TSV Rokuten
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IItemRakutenListCSVMappable>, ersAdmin.Domain.Item.Mappers.ItemRakutenListCSVMapper>();
            //Item TSV Yahoo
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IItemYahooListCSVMappable>, ersAdmin.Domain.Item.Mappers.ItemYahooListCSVMapper>();
            //Item TSV Amazon
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IItemAmazonListCSVMappable>, ersAdmin.Domain.Item.Mappers.ItemAmazonListCSVMapper>();

            //Set Item Csv
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.ISetItemCSVCommand>, ersAdmin.Domain.Item.Handlers.ValidateSetItemCSV>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Item.Commands.ISetItemCSVCommand>, ersAdmin.Domain.Item.Handlers.SetItemCSVHandler>();
            //Set Item Csv Record
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.ISetItemCSVRecordCommand>, ersAdmin.Domain.Item.Handlers.ValidateSetItemCSVRecord>();

            //Set Item
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.ISetItemSearchMappable>, ersAdmin.Domain.Item.Mappers.SetItemSearchMapper>();
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.ISetItemModifyMappable>, ersAdmin.Domain.Item.Mappers.SetItemModifyMapper>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Item.Commands.ISetItemModifyCommand>, ersAdmin.Domain.Item.Handlers.SetItemModifyHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.ISetItemModifyCommand>, ersAdmin.Domain.Item.Handlers.ValidateSetItemModify>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IChildScodeListCommand>, ersAdmin.Domain.Item.Handlers.ValidateChildSscodeListData>();

            //Common Item List
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.ICommonItemListMappable>, ersAdmin.Domain.Item.Mappers.CommonItemListMapper>();

            //Cate Tree View
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.ICateTreeViewMappable>, ersAdmin.Domain.Item.Mappers.CateTreeViewMapper>();

            // Set Item All Csv
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.ISetItemAllCSVCommand>, ersAdmin.Domain.Item.Handlers.ValidateSetItemAllCSV>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Item.Commands.ISetItemAllCSVCommand>, ersAdmin.Domain.Item.Handlers.SetItemAllCSVHandler>();


            // Item CSV Delete
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemCSVDeleteCommand>, ersAdmin.Domain.Item.Handlers.ValidationItemCSVDelete>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Item.Commands.IItemCSVDeleteCommand>, ersAdmin.Domain.Item.Handlers.ItemCSVDeleteHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IItemCSVDeleteMappable>, ersAdmin.Domain.Item.Mappers.ItemCSVDeleteMapper>();

            //Item CSS Delete Record
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Item.Commands.IItemCSVDeleteRecordCommand>, ersAdmin.Domain.Item.Handlers.ValidationItemCSVDeleteRecord>();
            container.RegisterType<IMapper<ersAdmin.Domain.Item.Mappables.IItemCSVDeleteRecordMappable>, ersAdmin.Domain.Item.Mappers.ItemCSVDeleteRecordMapper>();


            /* Store Controller */
            //Store
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreAdminCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreAdmin>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreAdminListRecordCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreAdminListRecord>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IStoreAdminCommand>, ersAdmin.Domain.Store.Handlers.StoreAdminHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStoreAdminMappable>, ersAdmin.Domain.Store.Mappers.StoreAdminMapper>();

            //Role
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IRoleMappable>, ersAdmin.Domain.Store.Mappers.RoleMapper>();

            //Role Regist
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IRoleRegistCommand>, ersAdmin.Domain.Store.Handlers.ValidateRoleRegist>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IRoleRegistCommand>, ersAdmin.Domain.Store.Handlers.RoleRegistHandler>();

            //Role Modify
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IRoleModifyCommand>, ersAdmin.Domain.Store.Handlers.ValidateRoleModify>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IRoleModifyCommand>, ersAdmin.Domain.Store.Handlers.RoleModifyHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IRoleModifyMappable>, ersAdmin.Domain.Store.Mappers.RoleModifyMapper>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IRoleDeleteCommand>, ersAdmin.Domain.Store.Handlers.RoleDeleteHandler>();

            //common name
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.ICommonNameRegistMappable>, ersAdmin.Domain.Store.Mappers.CommonNameRegistMapper>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.ICommonNameCommand>, ersAdmin.Domain.Store.Handlers.CommonNameHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.ICommonNameCommand>, ersAdmin.Domain.Store.Handlers.ValidateCommonName>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.ICommonNameRecordCommand>, ersAdmin.Domain.Store.Handlers.ValidateCommonNameRecord>();
            
            
            /* Campaign Controller */
            // Target Regist
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Campaign.Commands.ITargetRegistCommand>, ersAdmin.Domain.Campaign.Handlers.ValidateTargetRegist>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Campaign.Commands.ITargetRegistCommand>, ersAdmin.Domain.Campaign.Handlers.TargetRegistHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Campaign.Commands.IScenarioItemListRecordCommand>, ersAdmin.Domain.Campaign.Handlers.ValidateScenarioItemListRecord>();

            //target search
            container.RegisterType<IMapper<ersAdmin.Domain.Campaign.Mappables.ITargetSearchMappable>, ersAdmin.Domain.Campaign.Mappers.TargetSearchMapper>();

            //target csv download
            container.RegisterType<IMapper<ersAdmin.Domain.Campaign.Mappables.ITargetCsvDownloadMappable>, ersAdmin.Domain.Campaign.Mappers.TargetCsvDownloadMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Campaign.Commands.ITargetCsvDownloadCommand>, ersAdmin.Domain.Campaign.Handlers.ValidateTargetCsvDownload>();

            //Target Modify
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Campaign.Commands.ITargetModifyCommand>, ersAdmin.Domain.Campaign.Handlers.ValidateTargetModify>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Campaign.Commands.ITargetModifyCommand>, ersAdmin.Domain.Campaign.Handlers.TargetModifyHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Campaign.Mappables.ITargetModifyMappable>, ersAdmin.Domain.Campaign.Mappers.TargetModifyMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Campaign.Commands.ICampaignModifyTargetListRecordCommand>, ersAdmin.Domain.Campaign.Handlers.ValidateCampaignModifyTargetListRecord>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Campaign.Commands.ITargetDeleteCommand>, ersAdmin.Domain.Campaign.Handlers.ValidateTargetDelete>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Campaign.Commands.ITargetDeleteCommand>, ersAdmin.Domain.Campaign.Handlers.TargetDeleteHandler>();

            //campaign regist
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Campaign.Commands.ICampaignRegistCommand>, ersAdmin.Domain.Campaign.Handlers.ValidateCampaignRegist>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Campaign.Commands.ICampaignRegistCommand>, ersAdmin.Domain.Campaign.Handlers.CampaignRegistHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Campaign.Mappables.ICampaignRegistMappable>, ersAdmin.Domain.Campaign.Mappers.CampaignRegistMapper>();

            //campaign modify
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Campaign.Commands.ICampaignModifyDetailListRecordCommand>, ersAdmin.Domain.Campaign.Handlers.ValidateCampaignModifyDetailListRecord>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Campaign.Commands.ICampaignModifyTargetListRecordCommand>, ersAdmin.Domain.Campaign.Handlers.ValidateCampaignModifyTargetListRecord>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Campaign.Commands.ICampaignModifyCommand>, ersAdmin.Domain.Campaign.Handlers.ValidateCampaignModify>();            
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Campaign.Commands.ICampaignModifyCommand>, ersAdmin.Domain.Campaign.Handlers.CampaignModifyHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Campaign.Mappables.ICampaignModifyMappable>, ersAdmin.Domain.Campaign.Mappers.CampaignModifyMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Campaign.Commands.ICampaignDeleteCommand>, ersAdmin.Domain.Campaign.Handlers.ValidateCampaignDelete>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Campaign.Commands.ICampaignDeleteCommand>, ersAdmin.Domain.Campaign.Handlers.CampaignDeleteHandler>();

            //campaign common scode list
            container.RegisterType<IMapper<ersAdmin.Domain.Campaign.Mappables.ICommonScodeListMappable>, ersAdmin.Domain.Campaign.Mappers.CommonScodeListMapper>();
            
            //Campaign search/list
            container.RegisterType<IMapper<ersAdmin.Domain.Campaign.Mappables.ICampaignSearchMappable>, ersAdmin.Domain.Campaign.Mappers.CampaignSearchMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Customer.Commands.ICustomerSearchCommand>, ersAdmin.Domain.Customer.Handlers.ValidateCustomerSearch>();

            /* Customer Controller */
            //customer_search
            container.RegisterType<IMapper<ersAdmin.Domain.Customer.Mappables.ICustomerSearchMappable>, ersAdmin.Domain.Customer.Mappers.CustomerSearchMapper>();

            //cus_list, cus_detail, cus_complete
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Customer.Commands.ICustomerCommand>, ersAdmin.Domain.Customer.Handlers.ValidateCustomer>();
            container.RegisterType<IMapper<ersAdmin.Domain.Customer.Mappables.ICustomerMappable>, ersAdmin.Domain.Customer.Mappers.CustomerMapper>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Customer.Commands.ICustomerCommand>, ersAdmin.Domain.Customer.Handlers.CustomerHandler>();

            //cus_point_search, cus_point_search_c
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Customer.Commands.IPointSearchCommand>, ersAdmin.Domain.Customer.Handlers.ValidatePointSearch>();
            container.RegisterType<IMapper<ersAdmin.Domain.Customer.Mappables.IPointSearchMappable>, ersAdmin.Domain.Customer.Mappers.PointSearchMapper>();

            //cus_point_add
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Customer.Commands.IPointAddCommand>, ersAdmin.Domain.Customer.Handlers.ValidatePointAdd>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Customer.Commands.IPointAddCommand>, ersAdmin.Domain.Customer.Handlers.PointAddHandler>();

            //cus_mail, cus_mail_complete
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Customer.Commands.ICustomerMailCommand>, ersAdmin.Domain.Customer.Handlers.ValidateCustomerMail>();
            container.RegisterType<IMapper<ersAdmin.Domain.Customer.Mappables.ICustomerMailMappable>, ersAdmin.Domain.Customer.Mappers.CustomerMailMapper>();

            //download_csv_all, download_csv_page
            container.RegisterType<IMapper<ersAdmin.Domain.Customer.Mappables.ICustomerCSVMappable>, ersAdmin.Domain.Customer.Mappers.CustomerCSVMapper>();

            //cus_crderr_search, cus_crderr_list
            container.RegisterType<IMapper<ersAdmin.Domain.Customer.Mappables.ICusCrderrSearchMappable>, ersAdmin.Domain.Customer.Mappers.CusCrderrSearchMapper>();

            //cus_crderr_dl_csv_all, cus_crderr_dl_csv_page
            container.RegisterType<IMapper<ersAdmin.Domain.Customer.Mappables.ICusCrderrCSVMappable>, ersAdmin.Domain.Customer.Mappers.CusCrderrCSVMapper>();

            //cus_mail_all_complete
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Customer.Commands.ICusMailAllCommand>, ersAdmin.Domain.Customer.Handlers.ValidateCusMailAll>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Customer.Commands.ICusMailAllCommand>, ersAdmin.Domain.Customer.Handlers.CusMailAllHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Customer.Mappables.ICusMailAllMappable>, ersAdmin.Domain.Customer.Mappers.CusMailAllMapper>();

            //member_rank
            container.RegisterType<IMapper<ersAdmin.Domain.Customer.Mappables.IMemberRankSetupMappable>, ersAdmin.Domain.Customer.Mappers.MemberRankSetupMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Customer.Commands.IMemberRankSetupCommand>, ersAdmin.Domain.Customer.Handlers.ValidateMemberRankSetup>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Customer.Commands.IMemberRankSetupDetailCommand>, ersAdmin.Domain.Customer.Handlers.ValidateMemberRankSetupDetail>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Customer.Commands.IMemberRankSetupCommand>, ersAdmin.Domain.Customer.Handlers.MemberRankSetupHandler>();

            /* Home Controller */
            //cus_detail
            container.RegisterType<IMapper<ersAdmin.Domain.Home.Mappables.IIndexMappable>, ersAdmin.Domain.Home.Mappers.IndexMapper>();

            /* Login Controller */
            //Login
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Login.Commands.ILoginCommand>, ersAdmin.Domain.Login.Handlers.ValidateLogin>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Login.Commands.ILoginCommand>, ersAdmin.Domain.Login.Handlers.LoginHandler>();

            //Login Change Password
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Login.Commands.IPassChangeCommand>, ersAdmin.Domain.Login.Handlers.PassChangeHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Login.Commands.IPassChangeCommand>, ersAdmin.Domain.Login.Handlers.ValidatePassChange>();
            container.RegisterType<IMapper<ersAdmin.Domain.Login.Mappables.IPassChangeMappable>, ersAdmin.Domain.Login.Mappers.PassChangeMapper>();

            /* store Controller */

            //storeCarriage
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IStoreCarriageCommand>, ersAdmin.Domain.Store.Handlers.StoreCarriageHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreCarriageCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreCarriage>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStoreCarriageMappable>, ersAdmin.Domain.Store.Mappers.StoreCarriageMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreCarriageRecordCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreCarriageRecord>();

            //storeAlert
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IStoreAlertCommand>, ersAdmin.Domain.Store.Handlers.StoreAlertHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreAlertCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreAlert>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStoreAlertMappable>, ersAdmin.Domain.Store.Mappers.StoreAlertMapper>();

            //Store_payment_table
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStorePaymentListRecordCommand>, ersAdmin.Domain.Store.Handlers.ValidateStorePaymentListRecord>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStorePaymentListRecordMappable>, ersAdmin.Domain.Store.Mappers.StorePaymentListRecordMapper>();

            //store_etc
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IStoreEtcCommand>, ersAdmin.Domain.Store.Handlers.StoreEtcHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreEtcCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreEtc>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStoreEtcMappable>, ersAdmin.Domain.Store.Mappers.StoreEtcMapper>();

            //store_etc_table
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreEtcListRecordCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreEtcListRecord>();
 
            //Store_card_table
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreCardListRecordCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreCardListRecord>();

            //store_payment
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IStorePaymentCommand>, ersAdmin.Domain.Store.Handlers.StorePaymentHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStorePaymentCommand>, ersAdmin.Domain.Store.Handlers.ValidateStorePayment>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStorePaymentMappable>, ersAdmin.Domain.Store.Mappers.StorePaymentMapper>();


            //Store_delivery_day
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IStoreDeliveryDayCommand>, ersAdmin.Domain.Store.Handlers.StoreDeliveryDayHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreDeliveryDayCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreDeliveryDay>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStoreDeliveryDayMappable>, ersAdmin.Domain.Store.Mappers.StoreDeliveryDayMapper>();

            //Store_delivery_day_delete
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IDeleteDeliveryDayCommand>, ersAdmin.Domain.Store.Handlers.DeleteDeliveryDayHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IDeleteDeliveryDayCommand>, ersAdmin.Domain.Store.Handlers.ValidateDeleteDeliveryDay>();

            //store_calendar
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IStoreCalendarCommand>, ersAdmin.Domain.Store.Handlers.StoreCalendarHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreCalendarCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreCalendar>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStoreCalendarMappable>, ersAdmin.Domain.Store.Mappers.StoreCalendarMapper>();

            //Store_mail_text
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IStoreMailTextCommand>, ersAdmin.Domain.Store.Handlers.StoreMailTextHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreMailTextCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreMailText>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStoreMailTextMappable>, ersAdmin.Domain.Store.Mappers.StoreMailTextMapper>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IStoreMallMailTextCommand>, ersAdmin.Domain.Store.Handlers.StoreMallMailTextHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreMallMailTextCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreMallMailText>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStoreMallMailTextMappable>, ersAdmin.Domain.Store.Mappers.StoreMallMailTextMapper>();

            //Store_tax
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IStoreTaxCommand>, ersAdmin.Domain.Store.Handlers.StoreTaxHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreTaxCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreTax>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStoreTaxMappable>, ersAdmin.Domain.Store.Mappers.StoreTaxMapper>();

            //Store_tax_payment_table
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreTaxPaymentListRecordCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreTaxPaymentListRecord>();

            ////Store_delivery
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IStoreDeliveryCommand>, ersAdmin.Domain.Store.Handlers.StoreDeliveryHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreDeliveryCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreDelivery>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStoreDeliveryMappable>, ersAdmin.Domain.Store.Mappers.StoreDeliveryMapper>();

            // store_mall_setting
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Store.Commands.IStoreMallSettingCommand>, ersAdmin.Domain.Store.Handlers.StoreMallSettingHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Store.Commands.IStoreMallSettingCommand>, ersAdmin.Domain.Store.Handlers.ValidateStoreMallSetting>();
            container.RegisterType<IMapper<ersAdmin.Domain.Store.Mappables.IStoreMallSettingMappable>, ersAdmin.Domain.Store.Mappers.StoreMallSettingMapper>();


            /* regular Controller */
            //bill search
            container.RegisterType<IMapper<ersAdmin.Domain.Regular.Mappables.IBillSearchMappable>, ersAdmin.Domain.Regular.Mappers.BillSearchMapper>();
            container.RegisterType<IMapper<ersAdmin.Domain.Regular.Mappables.IBillSearchCSVMappable>, ersAdmin.Domain.Regular.Mappers.BillSearchCSVMapper>();
 
            //regist_income
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Regular.Commands.IRegistIncomeCommand>, ersAdmin.Domain.Regular.Handlers.RegistIncomeHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Regular.Commands.IRegistIncomeCommand>, ersAdmin.Domain.Regular.Handlers.ValidateRegistIncome>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Regular.Commands.IRegistIncomeDetailListRecordCommand>, ersAdmin.Domain.Regular.Handlers.ValidateRegistIncomeDetailListRecord>();
            container.RegisterType<IMapper<ersAdmin.Domain.Regular.Mappables.IRegistIncomeMappable>, ersAdmin.Domain.Regular.Mappers.RegistIncomeMapper>();
 
            //Shipping_csv_upload
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Regular.Commands.IShippingUploadCSVCommand>, ersAdmin.Domain.Regular.Handlers.ShippingUploadCSVHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Regular.Commands.IShippingUploadCSVCommand>, ersAdmin.Domain.Regular.Handlers.ValidateShippingUploadCSV>();

            //stock_update_all
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Regular.Commands.IStockUpdateAllCSVCommand>, ersAdmin.Domain.Regular.Handlers.StockUpdateAllCSVHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Regular.Commands.IStockUpdateAllCSVCommand>, ersAdmin.Domain.Regular.Handlers.ValidateStockUpdateAllCSV>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Regular.Commands.IStockUpdateCSVRecordCommand>, ersAdmin.Domain.Regular.Handlers.ValidateStockUpdateCSVRecord>();

            //stock_update
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Regular.Commands.IStockUpdateCSVCommand>, ersAdmin.Domain.Regular.Handlers.StockUpdateCSVHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Regular.Commands.IStockUpdateCSVCommand>, ersAdmin.Domain.Regular.Handlers.ValidateStockUpdateCSV>();
 
            ///cvs record
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Regular.Commands.IShippingUploadCSVRecordCommand>, ersAdmin.Domain.Regular.Handlers.ValidateShippingUploadCSVRecord>();

            //delivery income csv            
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Regular.Commands.IDeliveryIncomeCsvCommand>, ersAdmin.Domain.Regular.Handlers.DeliveryIncomeCsvHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Regular.Commands.IDeliveryIncomeCsvCommand>, ersAdmin.Domain.Regular.Handlers.ValidateDeliveryIncomeCsv>();           
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Regular.Commands.IDeliveryIncomeCSVRecordCommand>, ersAdmin.Domain.Regular.Handlers.ValidateDeliveryIncomeCSVRecord>();

            //bill_detail
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Regular.Commands.IBillModifyCommand>, ersAdmin.Domain.Regular.Handlers.BillModifyHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Regular.Commands.IBillModifyCommand>, ersAdmin.Domain.Regular.Handlers.ValidateBillModify>();
            container.RegisterType<IMapper<ersAdmin.Domain.Regular.Mappables.IBillModifyMappable>, ersAdmin.Domain.Regular.Mappers.BillModifyMapper>();
 
            //bill_mail
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Regular.Commands.IBillMailCommand>, ersAdmin.Domain.Regular.Handlers.ValidateBillMail>();
            container.RegisterType<IMapper<ersAdmin.Domain.Regular.Mappables.IBillMailMappable>, ersAdmin.Domain.Regular.Mappers.BillMailMapper>();

			/* promotionController */
            //coupon_regist, coupon_regist_complete
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Promotion.Commands.ICouponRegistCommand>, ersAdmin.Domain.Promotion.Handlers.CouponRegistHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Promotion.Commands.ICouponRegistCommand>, ersAdmin.Domain.Promotion.Handlers.ValidateCouponRegist>();

            //coupon_search, coupon_list
            container.RegisterType<IMapper<ersAdmin.Domain.Promotion.Mappables.ICouponSearchMappable>, ersAdmin.Domain.Promotion.Mappers.CouponSearchMapper>();

            //coupon_modify, coupon_modify_complete
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Promotion.Commands.ICouponModifyCommand>, ersAdmin.Domain.Promotion.Handlers.CouponModifyHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Promotion.Commands.ICouponModifyCommand>, ersAdmin.Domain.Promotion.Handlers.ValidateCouponModify>();
            container.RegisterType<IMapper<ersAdmin.Domain.Promotion.Mappables.ICouponModifyMappable>, ersAdmin.Domain.Promotion.Mappers.CouponModifyMapper>();

            //coupon_modify_delete
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Promotion.Commands.ICouponDeleteCommand>, ersAdmin.Domain.Promotion.Handlers.CouponDeleteHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Promotion.Commands.ICouponDeleteCommand>, ersAdmin.Domain.Promotion.Handlers.ValidateCouponDelete>();

            //coupon_download_csv_all, coupon_download_csv_page
            container.RegisterType<IMapper<ersAdmin.Domain.Promotion.Mappables.ICouponListCSVMappable>, ersAdmin.Domain.Promotion.Mappers.CouponListCSVMapper>();
            
            //Coupon Upload
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Promotion.Commands.ICouponCSVCommand>, ersAdmin.Domain.Promotion.Handlers.ValidateCouponCSV>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Promotion.Commands.ICouponCSVRecordCommand>, ersAdmin.Domain.Promotion.Handlers.ValidateCouponCSVRecord>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Promotion.Commands.ICouponCSVCommand>, ersAdmin.Domain.Promotion.Handlers.CouponCSVHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Promotion.Mappables.ICouponCSVMappable>, ersAdmin.Domain.Promotion.Mappers.CouponCSVMapper>();
            container.RegisterType<IMapper<ersAdmin.Domain.Promotion.Mappables.ICouponCSVRecordMappable>, ersAdmin.Domain.Promotion.Mappers.CouponCSVRecordMapper>();

            //Shipment List
            container.RegisterType<IMapper<ersAdmin.Domain.Regular.Mappables.IShipmentListMappable>, ersAdmin.Domain.Regular.Mappers.ShipmentListMapper>();

            //Shipment List Csv
            container.RegisterType<IMapper<ersAdmin.Domain.Regular.Mappables.IShipmentListCSVMappable>, ersAdmin.Domain.Regular.Mappers.ShipmentListCSVMapper>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Regular.Commands.IShipmentListCSVCommand>, ersAdmin.Domain.Regular.Handlers.ShipmentListCSVHandler>();


            /* StepMailController */
            //stepmail_list
            container.RegisterType<IMapper<ersAdmin.Domain.StepMail.Mappables.IStepmailListMappable>, ersAdmin.Domain.StepMail.Mappers.StepmailListMapper>();

            //stepmail_regist, stepmail_regist_complete
            container.RegisterType<ICommandHandler<ersAdmin.Domain.StepMail.Commands.IStepmailRegistCommand>, ersAdmin.Domain.StepMail.Handlers.StepmailRegistHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.StepMail.Commands.IStepmailRegistCommand>, ersAdmin.Domain.StepMail.Handlers.ValidateStepmailRegist>();
            container.RegisterType<IMapper<ersAdmin.Domain.StepMail.Mappables.IStepmailRegistMappable>, ersAdmin.Domain.StepMail.Mappers.StepmailRegistMapper>();

            //stepmail_regist, stepmail_regist_complete
            container.RegisterType<ICommandHandler<ersAdmin.Domain.StepMail.Commands.IStepmailModifyCommand>, ersAdmin.Domain.StepMail.Handlers.StepmailModifyHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.StepMail.Commands.IStepmailModifyCommand>, ersAdmin.Domain.StepMail.Handlers.ValidateStepmailModify>();
            container.RegisterType<IMapper<ersAdmin.Domain.StepMail.Mappables.IStepmailModifyMappable>, ersAdmin.Domain.StepMail.Mappers.StepmailModifyMapper>();

            //stepmail_delete_complete
            container.RegisterType<ICommandHandler<ersAdmin.Domain.StepMail.Commands.IStepmailDeleteCommand>, ersAdmin.Domain.StepMail.Handlers.StepmailDeleteHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.StepMail.Commands.IStepmailDeleteCommand>, ersAdmin.Domain.StepMail.Handlers.ValidateStepmailDelete>();

            //scenario_list
            container.RegisterType<IMapper<ersAdmin.Domain.StepMail.Mappables.IScenarioListMappable>, ersAdmin.Domain.StepMail.Mappers.ScenarioListMapper>();

            //scenario_regist, scenario_regist_complete
            container.RegisterType<ICommandHandler<ersAdmin.Domain.StepMail.Commands.IScenarioRegistCommand>, ersAdmin.Domain.StepMail.Handlers.ScenarioRegistHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.StepMail.Commands.IScenarioRegistCommand>, ersAdmin.Domain.StepMail.Handlers.ValidateScenarioRegist>();
            container.RegisterType<IMapper<ersAdmin.Domain.StepMail.Mappables.IScenarioRegistMappable>, ersAdmin.Domain.StepMail.Mappers.ScenarioRegistMapper>();

            //delvy_actual_list
            container.RegisterType<IMapper<ersAdmin.Domain.StepMail.Mappables.IDelvyActualListMappable>, ersAdmin.Domain.StepMail.Mappers.DelvyActualListMapper>();


            //delvy_detail 
            container.RegisterType<IValidationHandler<ersAdmin.Domain.StepMail.Commands.IDelvyDetailCommand>, ersAdmin.Domain.StepMail.Handlers.ValidateDelvyDetail>();
            container.RegisterType<IMapper<ersAdmin.Domain.StepMail.Mappables.IDelvyDetailMappable>, ersAdmin.Domain.StepMail.Mappers.DelvyDetailMapper>();

            //delvy_summary            
            container.RegisterType<IValidationHandler<ersAdmin.Domain.StepMail.Commands.IDelvySummaryCommand>, ersAdmin.Domain.StepMail.Handlers.ValidateDelvySummary>();
            container.RegisterType<IMapper<ersAdmin.Domain.StepMail.Mappables.IDelvySummaryMappable>, ersAdmin.Domain.StepMail.Mappers.DelvySummaryMapper>();

            //item_search
            container.RegisterType<IMapper<ersAdmin.Domain.StepMail.Mappables.IItemSearchMappable>, ersAdmin.Domain.StepMail.Mappers.ItemSearchMapper>();


            //get_itemdetail
            container.RegisterType<IMapper<ersAdmin.Domain.StepMail.Mappables.IGetItemDetailMappable>, ersAdmin.Domain.StepMail.Mappers.GetItemDetailMapper>();
            



            //scenario_modify, scenario_modify_complete
            container.RegisterType<ICommandHandler<ersAdmin.Domain.StepMail.Commands.IScenarioModifyCommand>, ersAdmin.Domain.StepMail.Handlers.ScenarioModifyHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.StepMail.Commands.IScenarioModifyCommand>, ersAdmin.Domain.StepMail.Handlers.ValidateScenarioModify>();
            container.RegisterType<IMapper<ersAdmin.Domain.StepMail.Mappables.IScenarioModifyMappable>, ersAdmin.Domain.StepMail.Mappers.ScenarioModifyMapper>();

            //scenario_delete_complete
            container.RegisterType<ICommandHandler<ersAdmin.Domain.StepMail.Commands.IScenarioDeleteCommand>, ersAdmin.Domain.StepMail.Handlers.ScenarioDeleteHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.StepMail.Commands.IScenarioDeleteCommand>, ersAdmin.Domain.StepMail.Handlers.ValidateScenarioDelete>();


            /* priceController */
            //price entry
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Price.Commands.IPriceListCommand>, ersAdmin.Domain.Price.Handlers.ValidatePriceList>();
            container.RegisterType<IMapper<ersAdmin.Domain.Price.Mappables.IPriceListMappable>, ersAdmin.Domain.Price.Mappers.PriceListMapper>();

            container.RegisterType<ICommandHandler<ersAdmin.Domain.Price.Commands.IPriceDetailCommand>, ersAdmin.Domain.Price.Handlers.PriceDetailHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Price.Commands.IPriceDetailCommand>, ersAdmin.Domain.Price.Handlers.ValidatePriceDetail>();
            container.RegisterType<IMapper<ersAdmin.Domain.Price.Mappables.IPriceDetailMappable>, ersAdmin.Domain.Price.Mappers.PriceDetailMapper>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Price.Commands.IPriceDetailListCommand>, ersAdmin.Domain.Price.Handlers.ValidatePriceDetailList>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Price.Commands.IPriceDetailListCommand>, ersAdmin.Domain.Price.Handlers.PriceDetailListHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Price.Mappables.IPriceDetailListMappable>, ersAdmin.Domain.Price.Mappers.PriceDetailListMapper>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Price.Commands.IPriceMemberRankCommand>, ersAdmin.Domain.Price.Handlers.ValidatePriceMemberRank>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Price.Commands.IPriceMemberRankCommand>, ersAdmin.Domain.Price.Handlers.PriceMemberRankHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Price.Mappables.IPriceMemberRankMappable>, ersAdmin.Domain.Price.Mappers.PriceMemberRankMapper>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Price.Commands.IPriceMemberRankRecordCommand>, ersAdmin.Domain.Price.Handlers.ValidatePriceMemberRankRecord>();

            //Price table record
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Price.Commands.IPriceDetailListRecordCommand>, ersAdmin.Domain.Price.Handlers.ValidatePriceDetailListRecord>();

            //Price CSv Upload
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Price.Commands.IPriceCsvCommand>, ersAdmin.Domain.Price.Handlers.ValidatePriceCsv>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Price.Commands.IPriceCsvRecordCommand>, ersAdmin.Domain.Price.Handlers.ValidatePriceCsvRecord>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Price.Commands.IPriceCsvCommand>, ersAdmin.Domain.Price.Handlers.PriceCsvHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Price.Mappables.IPriceCsvMappable>, ersAdmin.Domain.Price.Mappers.PriceCsvMapper>();

            //Price Csv Download
            container.RegisterType<IMapper<ersAdmin.Domain.Price.Mappables.IPriceListCSVMappable>, ersAdmin.Domain.Price.Mappers.PriceListCSVMapper>();

            //Warehouse
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IOrderListCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateOrderList>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IOrderListMappable>, ersAdmin.Domain.Warehouse.Mappers.OrderListMapper>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IOrderRegistCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateOrderRegist>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IOrderRegistRecordCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateOrderRegistRecord>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Warehouse.Commands.IOrderRegistCommand>, ersAdmin.Domain.Warehouse.Handlers.OrderRegistHandler>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IOrderListPDFCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateOrderListPDF>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IOrderListPDFMappable>, ersAdmin.Domain.Warehouse.Mappers.OrderListPDFMapper>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IPastOrderListMappable>, ersAdmin.Domain.Warehouse.Mappers.PastOrderListMapper>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.ISupplierRegisterCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateSupplierRegister>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Warehouse.Commands.ISupplierRegisterCommand>, ersAdmin.Domain.Warehouse.Handlers.SupplierRegisterHandler>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.ISupplierListCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateSupplierList>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.ISupplierListMappable>, ersAdmin.Domain.Warehouse.Mappers.SupplierListMapper>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.ISupplierModifyCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateSupplierModify>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Warehouse.Commands.ISupplierModifyCommand>, ersAdmin.Domain.Warehouse.Handlers.SupplierModifyHandler>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.ISupplierModifyMappable>, ersAdmin.Domain.Warehouse.Mappers.RoleModifyMapper>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IPastOrderListCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidatePastOrderList>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IPastOrderListMappable>, ersAdmin.Domain.Warehouse.Mappers.PastOrderListMapper>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IPastOrderListCsvMappable>, ersAdmin.Domain.Warehouse.Mappers.PastOrderListCsvMapper>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IPastOrderListPDFMappable>, ersAdmin.Domain.Warehouse.Mappers.PastOrderListPDFMapper>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IPastOrderCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidatePastOrder>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IPastOrderMappable>, ersAdmin.Domain.Warehouse.Mappers.PastOrderMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IPastOrderModifyCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidatePastOrderModify>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Warehouse.Commands.IPastOrderModifyCommand>, ersAdmin.Domain.Warehouse.Handlers.PastOrderModifyHandler>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IStorageCsvUploadRecordCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateStorageCsvUploadRecord>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IStorageCsvUploadCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateStorageCsvUpload>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Warehouse.Commands.IStorageCsvUploadCommand>, ersAdmin.Domain.Warehouse.Handlers.StorageCsvUploadHandler>();
             
            //  move_csv
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IMoveCsvUploadCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateMoveCsvUpload>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Warehouse.Commands.IMoveCsvUploadCommand>, ersAdmin.Domain.Warehouse.Handlers.MoveCsvUploadHandler>();

            //  move_csv_record
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IMoveCsvUploadRecordCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateMoveCsvUploadRecord>();

            //move
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IMoveHistoryListMappable>, ersAdmin.Domain.Warehouse.Mappers.MoveHistoryMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IMoveHistoryListCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateMoveHistoryList>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IMoveHistorySearchCSVMappable>, ersAdmin.Domain.Warehouse.Mappers.MoveHistorySearchCSVMapper>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IMoveHistorySearchCSVMappable>, ersAdmin.Domain.Warehouse.Mappers.MoveHistorySearchCSVMapper>();
            
            //storage_download
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IStorageDownloadCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateStorageDownload>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IStorageDownloadMappable>, ersAdmin.Domain.Warehouse.Mappers.StorageDownloadMapper>();

            //stock
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IStockListMappable>, ersAdmin.Domain.Warehouse.Mappers.StockListMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IStockListCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateStockList>();
            container.RegisterType<IMapper<ersAdmin.Domain.Warehouse.Mappables.IStockListCSVMappable>, ersAdmin.Domain.Warehouse.Mappers.StockListCSVMapper>();

            //  stock_taking
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IStockTakingCsvUploadCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateStockTakingCsvUpload>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Warehouse.Commands.IStockTakingCsvUploadCommand>, ersAdmin.Domain.Warehouse.Handlers.StockTakingCsvUploadHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Warehouse.Commands.IStockTakingCsvUploadRecordCommand>, ersAdmin.Domain.Warehouse.Handlers.ValidateStockTakingCsvUploadRecord>();



            //Summary
            container.RegisterType<IMapper<ersAdmin.Domain.Summary.Mappables.ISummaryListMappable>, ersAdmin.Domain.Summary.Mappers.SummaryListMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Summary.Commands.ISummaryResultCommand>, ersAdmin.Domain.Summary.Handlers.ValidateSummaryResult>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Summary.Commands.ISummaryConditionCommand>, ersAdmin.Domain.Summary.Handlers.ValidateSummaryCondition>();
            container.RegisterType<IMapper<ersAdmin.Domain.Summary.Mappables.ISummaryResultMappable>, ersAdmin.Domain.Summary.Mappers.SummaryResultMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Summary.Commands.ISummaryResultCSVCommand>, ersAdmin.Domain.Summary.Handlers.ValidateSummaryResultCSV>();
            container.RegisterType<IMapper<ersAdmin.Domain.Summary.Mappables.ISummaryResultCSVMappable>, ersAdmin.Domain.Summary.Mappers.SummaryResultCSVMapper>();

            //Lp
            container.RegisterType<IMapper<ersAdmin.Domain.Lp.Mappables.ILandingListMappable>, ersAdmin.Domain.Lp.Mappers.LandingListMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Lp.Commands.ILandingListCopyCommand>, ersAdmin.Domain.Lp.Handlers.ValidateLandingListCopy>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Lp.Commands.ILandingListCopyCommand>, ersAdmin.Domain.Lp.Handlers.LandingListCopyHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Lp.Commands.IDownloadCsvLpDQuestCommand>, ersAdmin.Domain.Lp.Handlers.ValidateDownloadCsvLpDQuest>();
            container.RegisterType<IMapper<ersAdmin.Domain.Lp.Mappables.IDownloadCsvLpDQuestMappable>, ersAdmin.Domain.Lp.Mappers.DownloadCsvLpDQuestMapper>();

            container.RegisterType<IMapper<ersAdmin.Domain.Lp.Mappables.ILpRegistMappable>, ersAdmin.Domain.Lp.Mappers.LpRegistMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Lp.Commands.ILpRegistCommand>, ersAdmin.Domain.Lp.Handlers.ValidateLpRegist>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Lp.Commands.ILpRegistQuestionnaireRecordCommand>, ersAdmin.Domain.Lp.Handlers.ValidateLpRegistQuestionnaireRecord>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Lp.Commands.ILpRegistCommand>, ersAdmin.Domain.Lp.Handlers.LpRegistHandler>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Lp.Commands.ILpModifyCommand>, ersAdmin.Domain.Lp.Handlers.ValidateLpModify>();
            container.RegisterType<IMapper<ersAdmin.Domain.Lp.Mappables.ILpModifyMappable>, ersAdmin.Domain.Lp.Mappers.LpModifyMapper>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Lp.Commands.ILpModifyCommand>, ersAdmin.Domain.Lp.Handlers.LpModifyHandler>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Lp.Commands.ILpDeleteCommand>, ersAdmin.Domain.Lp.Handlers.ValidateLpDelete>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Lp.Commands.ILpDeleteCommand>, ersAdmin.Domain.Lp.Handlers.LpDeleteHandler>();

            container.RegisterType<IMapper<ersAdmin.Domain.Lp.Mappables.ILpPageRegistMappable>, ersAdmin.Domain.Lp.Mappers.LpPageRegistMapper>();
            container.RegisterType<IMapper<ersAdmin.Domain.Lp.Mappables.ILpPageRegistTemplateMappable>, ersAdmin.Domain.Lp.Mappers.LpPageRegistTemplateMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Lp.Commands.ILpPageRegistCommand>, ersAdmin.Domain.Lp.Handlers.ValidateLpPageRegist>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Lp.Commands.ILpPageRegistCommand>, ersAdmin.Domain.Lp.Handlers.LpPageRegistHandler>();

            container.RegisterType<IValidationHandler<ersAdmin.Domain.Lp.Commands.ILpPageDeleteCommand>, ersAdmin.Domain.Lp.Handlers.ValidateLpPageDelete>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Lp.Commands.ILpPageDeleteCommand>, ersAdmin.Domain.Lp.Handlers.LpPageDeleteHandler>();

            container.RegisterType<IMapper<ersAdmin.Domain.Lp.Mappables.ILpPreviewPageMappable>, ersAdmin.Domain.Lp.Mappers.LpPreviewPageMapper>();



            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IUpdateTableCommand>, ersAdmin.Domain.Cms.Handlers.ValidateUpdateTable>();
            container.RegisterType<IMapper<ersAdmin.Domain.Cms.Mappables.IUpdateTableMappable>, ersAdmin.Domain.Cms.Mappers.UpdateTableMapper>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Cms.Commands.IUpdateTableCommand>, ersAdmin.Domain.Cms.Handlers.UpdateTableHandler>();
			
			container.RegisterType<IMapper<ersAdmin.Domain.Cms.Mappables.IDownloadTableMappable>, ersAdmin.Domain.Cms.Mappers.DownloadTableMapper>();
            container.RegisterType<IValidationHandler<ersAdmin.Domain.Cms.Commands.IDownloadTableCommand>, ersAdmin.Domain.Cms.Handlers.ValidateDownloadTable>();
            container.RegisterType<ICommandHandler<ersAdmin.Domain.Cms.Commands.IDownloadTableCommand>, ersAdmin.Domain.Cms.Handlers.DownloadTableHandler>();

            return container;
        }
    }
}