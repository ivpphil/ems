(function() {
   tinymce.PluginManager.requireLangPack("ers"); 
   tinymce.create("tinymce.plugins.ersPlugin", 
   {
      init : function(a, b) {
         a.addCommand("mceers", 
         function() {
            a.windowManager.open( {
               file : b + "/dialog.htm", 
               width : 400 + parseInt(a.getLang("ers.delta_width", 
               0)), height : 110 + parseInt(a.getLang("ers.delta_height", 0)), inline : 1}
            , {
               plugin_url : b, 
               some_custom_arg : "Insert URL here"}
            )}
         ); a.addButton("ers", 
         {
            title : "ers.desc", 
            cmd : "mceers", 
            image : b + "/img/ers.gif"}
         ); a.onNodeChange.add(function(d, 
         c, e) {
            c.setActive("ers", 
            e.nodeName == "IMG")}
         )}
      , createControl : function(b, a) {
         return null}
      , getInfo : function() {
         return {
            longname : "ers plugin", 
            author : "Some author", 
            authorurl : "http://tinymce.moxiecode.com", 
            infourl : "http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/ers", 
            version : "1.0"}
         }
      }
   ); 
   tinymce.PluginManager.add("ers", tinymce.plugins.ersPlugin)}
)(); 