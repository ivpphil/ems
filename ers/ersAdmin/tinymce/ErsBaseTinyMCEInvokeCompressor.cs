﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using jp.co.ivp.ers.util;
using Moxiecode.Manager.Utils;
using jp.co.ivp.ers;

namespace ersAdmin.tinymce
{
    public class ErsBaseTinyMCEInvokeCompressor
    {
        private Assembly _assembly;
        private object objCompressor;

        public ErsBaseTinyMCEInvokeCompressor(string compressorClass)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            _assembly = System.Reflection.Assembly.LoadFrom(setup.root_path + @"ersAdmin\tinymce\jscripts\tiny_mce\plugins\imagemanager\bin\MCManager.dll");
            objCompressor = _assembly.CreateInstance(compressorClass);
        }

        public void AddContent(string content)
        {
            var objMethod = objCompressor.GetType().GetMethod("AddContent", new Type[] { content.GetType() });
            this.InvokeMethod(objMethod, new[] { content });
        }

        public void AddContent(string content, bool remove_whitespace)
        {
            var objMethod = objCompressor.GetType().GetMethod("AddContent", new Type[] { content.GetType(), remove_whitespace.GetType() });
            this.InvokeMethod(objMethod, new object[] { content, remove_whitespace });
        }

        public void AddFile(string file)
        {
            var objMethod = objCompressor.GetType().GetMethod("AddFile", new Type[] { file.GetType() });
            this.InvokeMethod(objMethod, new object[] { file });
        }

        public void AddFile(string file, bool remove_whitespace)
        {
            var objMethod = objCompressor.GetType().GetMethod("AddFile", new Type[] { file.GetType(), remove_whitespace.GetType() });
            this.InvokeMethod(objMethod, new object[] { file, remove_whitespace });
        }

        public void Compress(HttpRequest request, HttpResponse response)
        {
            var objMethod = objCompressor.GetType().GetMethod("Compress", new Type[] { request.GetType(), response.GetType() });
            this.InvokeMethod(objMethod, new object[] { request, response });
        }

        public void InvokeMethod(MethodInfo objMethod, object[] parameters)
        {
            object result = null;

            result = objMethod.Invoke(objCompressor, parameters);

            if (result != null)
                throw new Exception(result.ToString());
        }

        public virtual void SetProperties(Dictionary<string, object> dic, string bindTargetName = null)
        {
            ErsReflection.SetPropertyAll(objCompressor, dic, bindTargetName);
        }

        public virtual void SetProperty(string propertyName, object value)
        {
            this.SetProperties(new Dictionary<string, object>() { { propertyName, value } });
        }
    }
}