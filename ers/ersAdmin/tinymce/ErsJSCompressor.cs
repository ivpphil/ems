﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersAdmin.tinymce
{
    public class ErsJSCompressor
        : ErsBaseTinyMCEInvokeCompressor
    {
        public ErsJSCompressor()
            : base("Moxiecode.Manager.Utils.JSCompressor") { }
    }
}