﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersAdmin.tinymce
{
    public class ErsCSSCompressor
        : ErsBaseTinyMCEInvokeCompressor
    {
        public ErsCSSCompressor()
            : base("Moxiecode.Manager.Utils.CSSCompressor") { }
    }
}