﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc.pager;
using ersAdmin.Domain.Warehouse.Mappables;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers.util;
using System.ComponentModel;

namespace ersAdmin.Models.warehouse
{
    public class move_history_list : ErsModelBase, IMoveHistoryListMappable, IMoveHistoryListCommand, IMoveHistorySearchCSVMappable
    {


        public move_history_list()
        {
            this.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
        }

        public ErsPagerModel pager { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        public long recordCount { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_move_t.intime")]
        [DisplayName("s_movedate_from")]
        public DateTime? s_movedate_from { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_move_t.intime")]
        [DisplayName("s_movedate_to")]
        public DateTime? s_movedate_to { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_move_t.move_type")]
        public EnumWhMoveType? s_move_type { get; set; }

        public List<Dictionary<string, object>> moveTypeList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.WhMoveType, EnumCommonNameColumnName.namename); }
        }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.supplier_code")]
        public string s_supplier_code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.supplier_name")]
        public string s_supplier_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]        
        public string s_scode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.maker_scode")]    
        public string s_maker_scode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.sname")]
        [DisplayName("s_sname")]
        public string s_sname { get; set; }

        public bool IsSearchPage { get; internal set; }

        [HtmlSubmitButton]
        public bool search { get; set; }

        public IEnumerable<move_history_record> listMoveHistory { get; set; }

        public ErsCsvCreater csvCreater { get; set; }
    }
}