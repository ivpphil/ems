﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using System.ComponentModel;

namespace ersAdmin.Models.warehouse.csv
{
    public class past_order_record_csv
        : ErsBindableModel
    {
        [CsvField]
        public DateTime? intime { get; set; }

        [CsvField]
        public string order_no { get; set; }

        [CsvField]
        public string supplier_code { get; set; }

        [CsvField]
        public string supplier_name { get; set; }

        public EnumWhOrderStatus? wh_order_status { get; set; }

        [CsvField]
        [DisplayName("wh_order_status")]
        public string w_wh_order_status
        {
            get; set;
        }

        [CsvField]
        public string scode { get; set; }

        [CsvField]
        public string maker_scode { get; set; }

        [CsvField]
        public string sname { get; set; }

        [CsvField]
        public int? cost_price { get; set; }

        [CsvField]
        public int? amount { get; set; }

        [CsvField]
        public int? shelf001 { get; set; }

        [CsvField]
        public int? shelf002 { get; set; }

        [CsvField]
        public int? shelf003 { get; set; }

        [CsvField]
        public DateTime? schedule_date { get; set; }

        [CsvField]
        public DateTime? first_intime { get; set; }

        [CsvField]
        public DateTime? last_intime { get; set; }

        public EnumWhUpStock? up_stock { get; set; }

        [CsvField]
        [DisplayName("up_stock")]
        public string w_up_stock
        {
            get; set;
        }

        public EnumWhOrderType? wh_order_type { get; set; }

        public string w_wh_order_type
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.WhOrderType, EnumCommonNameColumnName.namename, (int?)this.wh_order_type);
            }
        }
    }
}