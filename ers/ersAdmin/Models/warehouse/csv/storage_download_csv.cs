﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models.warehouse.csv
{
    public class storage_download_csv : ErsBindableModel
    {

        [ErsSchemaValidation("wh_order_t.schedule_date")]
        public DateTime? schedule_date { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_order_t.schedule_date")]
        public string w_schedule_date 
        {
            get
            {
                if (!this.schedule_date.HasValue)
                    return null;

                return this.schedule_date.Value.ToString("yyyy/MM/dd");
            }
        }

        [CsvField]
        [ErsSchemaValidation("wh_order_t.order_no")]
        public string order_no { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_supplier_t.supplier_code", requireAlphabet = true)]
        public string supplier_code { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_supplier_t.supplier_name")]
        public string supplier_name { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]
        public string scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.maker_scode")]
        public string maker_scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.sname")]
        public string sname { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_order_t.amount")]
        public int amount { get; set; }

    }
}