﻿using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace ersAdmin.Models.csv
{
    public class stock_taking_csv_upload_record : ErsBindableModel, IStockTakingCsvUploadRecordCommand
    {

        [CsvField]
        [ErsSchemaValidation("wh_move_t.scode", requireAlphabet = true)]
        public string scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_move_t.maker_scode")]
        public string maker_scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_move_t.amount")]
        public int? shelf001 { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_move_t.amount")]
        public int? shelf002 { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_move_t.amount")]
        public int? shelf003 { get; set; }

    }
}