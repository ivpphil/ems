﻿using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace ersAdmin.Models.csv
{
    public class storage_csv_upload_record : ErsBindableModel, IStorageCsvUploadRecordCommand
    {
        [CsvField]
        [ErsSchemaValidation("wh_storage_t.order_no")]
        public string order_no { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_storage_t.scode", requireAlphabet = true)]
        public string scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_storage_t.maker_scode")]
        public string maker_scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_storage_t.shelf001")]
        public int? shelf001 { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_storage_t.shelf002")]
        public int? shelf002 { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_storage_t.shelf003")]
        public int? shelf003 { get; set; }

    }
}