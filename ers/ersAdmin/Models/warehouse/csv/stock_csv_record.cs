﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using System.ComponentModel;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Models.csv
{
    public class stock_csv_record : ErsModelBase
    {
        [CsvField]
        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]
        public string scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.sname")]
        public string sname { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.price")]
        public int? price { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.cost_price")]
        public int? cost_price { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_stock_t.shelf001")]
        public int? shelf001 { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_stock_t.shelf002")]
        public int? shelf002 { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_stock_t.shelf003")]
        public int? shelf003 { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_order_t.amount")]
        public int? amount { get; set; }

        [CsvField]
        [ErsSchemaValidation("stock_t.stock")]
        public int? stock { get; set; }
    }
}