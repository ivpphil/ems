﻿using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace ersAdmin.Models.csv
{
    public class move_csv_upload_record : ErsBindableModel, IMoveCsvUploadRecordCommand
    {

        [CsvField]
        [ErsSchemaValidation("wh_move_t.scode", requireAlphabet = true)]
        public string scode { get; set; }


        [CsvField]
        [ErsSchemaValidation("wh_move_t.maker_scode")]
        public string maker_scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_move_t.reason")]
        public string reason { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_move_t.shelf_from")]
        public EnumShelfNumber? shelf_from { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_move_t.shelf_to")]
        public EnumShelfNumber? shelf_to { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_move_t.amount")]
        public int? amount { get; set; }



    }
}