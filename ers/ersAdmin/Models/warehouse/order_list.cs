﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Warehouse.Commands;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.pager;


namespace ersAdmin.Models.warehouse
{
    public class OrderList
        : ErsModelBase, IOrderListCommand, IOrderListMappable, IOrderRegistCommand, IPastOrderListMappable
    {
        public OrderList()
        {
            var today = DateTime.Now;
            this.s_intime_less_than = new DateTime(today.Year, today.Month,today.Day,23,59,59);
        }

        [ErsOutputHidden("search")]
        [ErsSchemaValidation("d_master_t.intime")]
        public DateTime? s_intime_less_than { get; set; }

        [ErsOutputHidden("search")]
        [ErsSchemaValidation("s_master_t.supplier_code")]
        public string s_supplier_code { get; set; }

        [ErsOutputHidden("search")]
        [ErsSchemaValidation("s_master_t.scode")]
        public string s_scode { get; set; }

        [ErsOutputHidden("search")]
        [ErsSchemaValidation("s_master_t.sname")]
        public string s_sname { get; set; }

        [ErsOutputHidden("search")]
        [ErsSchemaValidation("s_master_t.wh_order_type")]
        public EnumWhOrderType? s_wh_order_type { get; set; }

        [BindTable("orderRegistRecords")]
        public IEnumerable<OrderRegistRecord> orderRegistRecords { get; set; }

        public IEnumerable<Dictionary<string, object>> listWhOrderType
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.WhOrderType, EnumCommonNameColumnName.namename);
            }
        }

        public DateTime? s_intime { get; set; }

        public IEnumerable<past_order_record> listPastOrder { get; set; }

        public DateTime? s_orderdate_from { get; set; }

        public DateTime? s_orderdate_to { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        [ErsOutputHidden("input")]
        public long recordCount { get; set; }

        public ErsPagerModel pager { get; set; }

        public string s_supplier_name { get; set; }

        public string s_order_no { get; set; }

        public EnumWhOrderStatus? s_wh_order_status { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        /// <summary>
        /// Get number of item on a merchandise list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().OrderItemNumberOnPage; } }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        [ErsOutputHidden("input")]
        public int pageCurrentCnt { get; set; }
    }
}