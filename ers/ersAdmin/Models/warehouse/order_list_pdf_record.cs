﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Models.warehouse
{
    public class order_list_pdf_record
        : ErsBindableModel
    {
        /// <summary>
        /// ヘッダーを表示する場合にTrue
        /// </summary>
        public bool IsDispHeader { get; set; }

        /// <summary>
        /// ヘッダーの前に改行を挿入する場合にTrue
        /// </summary>
        public bool IsNewPage { get; set; }

        /// <summary>
        /// フッターを表示する場合にTrue
        /// </summary>
        public bool IsDispFooter { get; set; }

        public string scode { get; set; }

        public string sname { get; set; }

        public int? cost_price { get; set; }

        public int? amount { get; set; }

        public int? total { get; set; }

        public string remarks { get; set; }

        /// <summary>
        /// ヘッダー情報
        /// </summary>
        public string order_no { get; set; }

        public string supplier_name { get; set; }

        public string zip { get; set; }

        public string w_pref { get; set; }

        public string address { get; set; }

        public string tel { get; set; }

        public string fax { get; set; }

        public DateTime? intime { get; set; }

        public int header_total { get; set; }

        public string supplier_code { get; set; }

        public string outsourcer { get; set; }
    }
}