﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using ersAdmin.Domain.Warehouse.Mappables;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers.util;
using System.ComponentModel;

namespace ersAdmin.Models.warehouse
{
    public class stock_list : ErsModelBase, IStockListMappable, IStockListCommand, IStockListCSVMappable
    {
        public ErsPagerModel pager { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        public long recordCount { get; set; }

        #region search fields

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_stock_t.intime")]
        [DisplayName("s_warehousing_from")]
        public DateTime? s_warehousing_from { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_stock_t.intime")]
        [DisplayName("s_warehousing_to")]
        public DateTime? s_warehousing_to { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.supplier_code")]
        public string s_supplier_code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.supplier_name")]
        public string s_supplier_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode")]
        public string s_scode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.sname")]
        public string s_sname { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type=CHK_TYPE.Numeric,rangeFrom = 1,rangeTo = 3)]
        public int? s_stock_type { get; set; }

        public List<Dictionary<string, object>> stockTypeList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.StockStatusType, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> list { get; set; }

        #endregion

        public bool IsSearchPage { get; internal set; }

        [HtmlSubmitButton]
        public bool search { get; set; }

        #region csv
        
        public ErsCsvCreater csvCreater { get; set; }
        public stock_list()
        {
            this.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
        }

        #endregion
    }
}