﻿using System.Linq;
using ersAdmin.Domain.Warehouse.Commands;
using ersAdmin.Models.csv;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Models
{
    public class storage_csv_upload : ErsModelBase, IStorageCsvUploadCommand
    {

        public virtual string resultMsg
        {
            get
            {
                if (this.csv_file == null)
                    return string.Empty;

                return ErsResources.GetMessage("30000", this.csv_file.validIndexes.Count());
            }
        }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public virtual bool chk_find { get; set; }

        [ErsOutputHidden]
        [BindCsvFile]
        public ErsCsvContainer<storage_csv_upload_record> csv_file { get; set; }

        [HtmlSubmitButton]
        public virtual bool regist { get; set; }

        public bool allCsvFieldsInvalid
        {
            get
            {
                if (this.csv_file == null)
                    return true;

                return (this.csv_file.validIndexes.Count() == 0);
            }
        }

    }
}