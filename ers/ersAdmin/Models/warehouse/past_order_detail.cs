﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Warehouse.Commands;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Models.warehouse
{
    public class PastOrderDetail
        : ErsModelBase, IPastOrderCommand, IPastOrderMappable, IPastOrderModifyCommand
    {
        public bool IsInitialize { get; set; }

        public bool IsDenyModify { get; set; }

        public bool IsUnderStock { get; set; }

        public DateTime? intime { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_order_info_t.order_no")]
        public string order_no { get; set; }

        public string supplier_code { get; set; }

        public string supplier_name { get; set; }

        [ErsSchemaValidation("wh_order_info_t.wh_order_status")]
        public EnumWhOrderStatus? wh_order_status { get; set; }

        [ErsSchemaValidation("wh_order_info_t.remarks")]
        public string remarks { get; set; }

        public IEnumerable<past_order_record> listPastOrder { get; set; }

        public List<Dictionary<string, object>> wh_order_statusList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.WhOrderStatus, EnumCommonNameColumnName.namename); }
        }

        public string w_wh_order_status
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.WhOrderStatus, EnumCommonNameColumnName.namename, (int?)this.wh_order_status); }
        }

        [HtmlSubmitButton]
        public bool is_under_stock { get; set; }
    }
}