﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Models.warehouse
{
    public class SupplierRegist
        : ErsModelBase, ISupplierRegisterCommand
    {

        [ErsSchemaValidation("wh_supplier_t.supplier_code", requireAlphabet = true)]
        public string supplier_code { get; set; }

        [ErsSchemaValidation("wh_supplier_t.supplier_name")]
        public string supplier_name { get; set; }

        [ErsSchemaValidation("wh_supplier_t.zip")]
        public string zip { get; set; }

        [ErsSchemaValidation("wh_supplier_t.pref")]
        public int? pref { get; set; }

        [ErsSchemaValidation("wh_supplier_t.address")]
        public string address { get; set; }

        [ErsSchemaValidation("wh_supplier_t.tel")]
        public string tel { get; set; }

        [ErsSchemaValidation("wh_supplier_t.fax")]
        public string fax { get; set; }

        [ErsSchemaValidation("wh_supplier_t.email")]
        public string email { get; set; }

        [ErsSchemaValidation("wh_supplier_t.intime")]
        public DateTime? intime { get; set; }

        public List<Dictionary<string, object>> prefList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(false, (int)EnumSiteId.COMMON_SITE_ID); }
        }
    }
}