﻿using System;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using ersAdmin.Domain.Warehouse.Mappables;

namespace ersAdmin.Models.warehouse
{
    public class storage_download : ErsModelBase, IStorageDownloadCommand, IStorageDownloadMappable
    {

        [ErsSchemaValidation("wh_order_t.schedule_date")]
        public DateTime? s_date_from { get; set; }

        [ErsSchemaValidation("wh_order_t.schedule_date")]
        public DateTime? s_date_to { get; set; }

        public ErsCsvCreater csvCreater { get; set; }

        public bool CreateCsvFile { get; set; }

        public bool hasRecord { get; set; }

        public storage_download()
        {
            this.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
        }


    }
}