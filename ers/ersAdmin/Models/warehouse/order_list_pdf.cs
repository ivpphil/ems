﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Warehouse.Commands;
using ersAdmin.Domain.Warehouse.Mappables;
using System.IO;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;

namespace ersAdmin.Models.warehouse
{
    public class OrderListPDF
        : ErsModelBase, IOrderListPDFCommand, IOrderListPDFMappable
    {
        public Byte[] pdf { get; set; }

        public DateTime? intime { get; set; }

        public IEnumerable<order_list_pdf_record> listOrder { get; set; }

        [ErsSchemaValidation("wh_order_t.order_no", requireAlphabet = true)]
        public string order_no { get; set; }
        
        #region 別ページで使用する検索条件
        public ErsPagerModel pager { get; set; }

        public long recordCount { get; set; }

        public DateTime? s_intime { get; set; }

        public DateTime? s_orderdate_from { get; set; }

        public DateTime? s_orderdate_to { get; set; }

        public string s_order_no { get; set; }

        public string s_supplier_code { get; set; }

        public string s_supplier_name { get; set; }

        public string s_scode { get; set; }

        public string s_sname { get; set; }

        public IEnumerable<past_order_record> listPastOrder { get; set; }

        public EnumWhOrderStatus? s_wh_order_status { get; set; }
        #endregion
    }
}