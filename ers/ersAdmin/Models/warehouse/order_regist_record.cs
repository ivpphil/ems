﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models.warehouse
{
    public class OrderRegistRecord
        : ErsBindableModel, IOrderRegistRecordCommand
    {
        public override string lineName
        {
            get
            {
                return string.Format("{0} {1}[{2}]", base.lineName, ErsResources.GetFieldName("scode"), this.scode);
            }
        }

        [ErsOutputHidden("input", "confirm")]
        [ErsSchemaValidation("wh_supplier_t.supplier_code", requireAlphabet = true)]
        public string supplier_code { get; set; }

        [ErsOutputHidden("input", "confirm")]
        [ErsSchemaValidation("wh_supplier_t.supplier_name")]
        public string supplier_name { get; set; }

        [ErsOutputHidden("input", "confirm")]
        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]
        public string scode { get; set; }

        [ErsOutputHidden("input", "confirm")]
        [ErsSchemaValidation("s_master_t.maker_scode")]
        public string maker_scode { get; set; }

        [ErsOutputHidden("input", "confirm")]
        [ErsSchemaValidation("s_master_t.sname")]
        public string sname { get; set; }

        [ErsOutputHidden("input", "confirm")]
        [ErsSchemaValidation("price_t.price")]
        public int? price { get; set; }

        [ErsOutputHidden("input", "confirm")]
        [ErsSchemaValidation("price_t.cost_price")]
        public int? cost_price { get; set; }

        [ErsOutputHidden("input", "confirm")]
        [ErsSchemaValidation("wh_order_t.wh_order_type")]
        public EnumWhOrderType? wh_order_type { get; set; }

        public string w_wh_order_type
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.WhOrderType, EnumCommonNameColumnName.namename, (int?)this.wh_order_type);
            }
        }

        [ErsOutputHidden("input", "confirm")]
        [ErsSchemaValidation("stock_t.stock")]
        public int? stock { get; set; }

        [ErsOutputHidden("input", "confirm")]
        [ErsSchemaValidation("wh_stock_t.stock")]
        public int? wh_stock { get; set; }

        [ErsOutputHidden("input", "confirm")]
        [ErsSchemaValidation("wh_order_t.amount")]
        public int? base_amount { get; set; }

        [ErsOutputHidden("input", "confirm")]
        [ErsSchemaValidation("s_master_t.stock_alert_amount")]
        public int? stock_alert_amount { get; set; }

        [ErsOutputHidden("confirm")]
        [ErsSchemaValidation("wh_order_t.amount")]
        public int? amount { get; set; }

        public int order_amount { get { return amount ?? 0; } }

        public int wh_ordered_amount { get; set; }

        [ErsOutputHidden("confirm")]
        [ErsSchemaValidation("wh_order_t.up_stock")]
        public EnumWhUpStock up_stock { get; set; }

        public string w_up_stock
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.WhUpStock, EnumCommonNameColumnName.namename, (int)this.up_stock);
            }
        }

        [ErsOutputHidden("confirm")]
        [ErsUniversalValidation(type=CHK_TYPE.Numeric, rangeFrom=1, rangeTo=1)]
        public EnumOnOff? check_order { get; set; }

        [ErsOutputHidden("confirm")]
        [ErsSchemaValidation("wh_order_t.schedule_date")]
        public DateTime? schedule_date { get; set; }
    }
}