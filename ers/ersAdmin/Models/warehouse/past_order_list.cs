﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Warehouse.Commands;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.util;
using System.ComponentModel;

namespace ersAdmin.Models.warehouse
{
    public class PastOrderList
        : ErsModelBase, IPastOrderListCommand, IPastOrderListMappable, IPastOrderListCsvMappable, IPastOrderListPDFMappable
    {
        public ErsCsvCreater csvCreater { get; set; }

        public PastOrderList()
        {
            this.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
        }

        public ErsPagerModel pager { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        public long recordCount { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("d_master_t.intime")]
        [DisplayName("s_orderdate_from")]
        public DateTime? s_orderdate_from { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("d_master_t.intime")]
        [DisplayName("s_orderdate_to")]
        public DateTime? s_orderdate_to { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_order_t.order_no")]
        public string s_order_no { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.supplier_code")]
        public string s_supplier_code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.supplier_name")]
        public string s_supplier_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode")]
        public string s_scode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.sname")]
        public string s_sname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("d_master_t.intime")]
        public DateTime? s_intime { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_order_info_t.wh_order_status")]
        public EnumWhOrderStatus? s_wh_order_status { get; set; }

        public IEnumerable<past_order_record> listPastOrder { get; set; }

        public List<Dictionary<string, object>> wh_order_statusList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.WhOrderStatus, EnumCommonNameColumnName.namename); }
        }

        #region PDF出力
        public byte[] pdf { get; set; }

        public IEnumerable<order_list_pdf_record> listOrder { get; set; }

        public string order_no { get; set; }
        #endregion
    }
}