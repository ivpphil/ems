﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using System.ComponentModel;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models.warehouse
{
    public class move_history_record: ErsModelBase
    {
        public int id { get; set; }

        [ErsSchemaValidation("wh_move_t.move_type")]
        public EnumWhMoveType? move_type { get; set; }

        [CsvField]
        [DisplayName("move_type")]
        public string w_move_type
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.WhMoveType, EnumCommonNameColumnName.namename, (int?)this.move_type);
            }
        }

        [CsvField]
        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]
        public string scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.maker_scode")]        
        public string maker_scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.sname")]        
        public string sname { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_supplier_t.supplier_code", requireAlphabet = true)] 
        public string supplier_code { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_supplier_t.supplier_name")] 
        public string supplier_name { get; set; }

        [ErsSchemaValidation("wh_move_t.shelf_from")] 
        public EnumShelfNumber? shelf_from { get; set; }

        [CsvField]
        [DisplayName("shelf_from")]
        public string w_shelf_from
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ShelfNumber, EnumCommonNameColumnName.namename, (int?)this.shelf_from);
            }
        }

        [CsvField]
        [ErsSchemaValidation("wh_move_t.shelf_to")] 
        public EnumShelfNumber? shelf_to { get; set; }

        [CsvField]
        [DisplayName("shelf_to")]
        public string w_shelf_to
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ShelfNumber, EnumCommonNameColumnName.namename, (int?)this.shelf_to);
            }
        }

        [CsvField]
        [ErsSchemaValidation("wh_move_t.amount")] 
        public int? amount { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_move_t.reason")] 
        public string reason { get; set; }

        [CsvField]
        [ErsSchemaValidation("wh_move_t.intime")] 
        public DateTime? intime { get; set; }
    }
}