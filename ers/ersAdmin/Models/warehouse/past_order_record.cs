﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Models.warehouse
{
    public class past_order_record
        : ErsBindableModel
    {
        public int id { get; set; }

        public string order_no { get; set; }

        public string supplier_code { get; set; }

        public string supplier_name { get; set; }

        public string scode { get; set; }

        public string sname { get; set; }

        public int? cost_price { get; set; }

        public EnumWhOrderType? wh_order_type { get; set; }

        public string w_wh_order_type
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.WhOrderType, EnumCommonNameColumnName.namename, (int?)this.wh_order_type);
            }
        }

        public int? amount { get; set; }

        public EnumWhUpStock up_stock { get; set; }

        public string w_up_stock
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.WhUpStock, EnumCommonNameColumnName.namename, (int?)this.up_stock);
            }
        }

        public DateTime? intime { get; set; }

        public EnumWhOrderStatus? wh_order_status { get; set; }

        public string w_wh_order_status
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.WhOrderStatus, EnumCommonNameColumnName.namename, (int?)this.wh_order_status);
            }
        }

        public string maker_scode { get; set; }

        public int? shelf001 { get; set; }

        public int? shelf002 { get; set; }

        public int? shelf003 { get; set; }

        public DateTime? schedule_date { get; set; }

        public DateTime? first_intime { get; set; }

        public DateTime? last_intime { get; set; }
    }
}