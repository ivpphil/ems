﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Warehouse.Commands;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Models.warehouse
{
    public class SupplierList
        : ErsModelBase, ISupplierListCommand, ISupplierListMappable
    {
        public ErsPagerModel pager { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        public long recordCount { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.supplier_code")]
        public string s_supplier_code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.supplier_name")]
        public string s_supplier_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.zip")]
        public string s_zip { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.pref")]
        public int? s_pref { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.address")]
        public string s_address { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.tel")]
        public string s_tel { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.fax")]
        public string s_fax { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("wh_supplier_t.email")]
        public string s_email { get; set; }

        //Supplier List result
        public List<Dictionary<string, object>> list { get; set; }

        public List<Dictionary<string, object>> prefList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(false, (int)EnumSiteId.COMMON_SITE_ID); }
        }
    }
}