﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Summary.Mappables;
using ersAdmin.Domain.Summary.Commands;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ersAdmin.Models.summary
{
    public class SummaryList
        : ErsModelBase, ISummaryListMappable, ISummaryResultCommand, ISummaryResultMappable, ISummaryResultCSVCommand, ISummaryResultCSVMappable
    {
          public ErsCsvCreater csvCreater { get; set; }
          public SummaryList()
        {
            this.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
        }

      public bool IsErrorBack { get; set; }

        public List<Dictionary<string, object>> GroupList { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("summary_group_t.group_code")]
        public string group_code { get; set; }

        [BindTable("summary_conditions")]
        public IList<summary_condition> summary_conditions { get; set; }

        public string group_name { get; set; }

        public IList<summary_result> summary_results { get; set; }

        [ErsSchemaValidation("summary_t.summary_code")]
        public string summary_code { get; set; }
    }
}