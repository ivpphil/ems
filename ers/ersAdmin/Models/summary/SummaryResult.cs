﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Models.summary
{
    public class summary_result
        : ErsBindableModel
    {
        public string summary_code { get; set; }
        public EnumSummaryType? summary_type { get; set; }
        public string summary_name { get; set; }
        public EnumOnOff? display_title { get; set; }
        public EnumOnOff? display_table { get; set; }
        public EnumOnOff? display_csv_dl { get; set; }

        public EnumOnOff? display_graph { get; set; }
        public EnumSummaryGraphType? graph_type { get; set; }

        /// <summary>
        /// テーブル用データリスト
        /// </summary>
        public IEnumerable<Dictionary<string, object>> listResult { get; set; }

        /// <summary>
        /// グラフ用データリスト
        /// </summary>
        public IEnumerable<IEnumerable<KeyValuePair<string, object>>> listResultValues { get; set; }

        /// <summary>
        /// テーブル用カラムリスト
        /// </summary>
        public Dictionary<string, object> FirstResult { get; set; }

        /// <summary>
        /// グラフ用カラムリスト
        /// </summary>
        public IEnumerable<KeyValuePair<string, object>> FirstResultValues { get; set; }

        public int FirstResultValuesCount
        {
            get
            {
                if (this.FirstResultValues == null)
                {
                    return 0;
                }
                return this.FirstResultValues.Count();
            }
        }
    }
}