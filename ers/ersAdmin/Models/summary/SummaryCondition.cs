﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;
using ersAdmin.Domain.Summary.Commands;
using jp.co.ivp.ers.summary.strategy;

namespace ersAdmin.Models.summary
{
    public class summary_condition
        : ErsBindableModel, ISummaryConditionCommand, ISummaryConditionValue
    {
        public override string lineName
        {
            get
            {
                return string.Empty;
            }
        }

        public bool onInitialize { get; set; }

        public string group_code { get; set; }

        public string item_name { get; set; }

        public string column_name { get; set; }

        public string template_name { get; set; }

        public string template_path
        {
            get
            {
                return string.Format("summary/condition/{0}.htm", this.template_name);
            }
        }

        [ErsOutputHidden]
        [ErsSchemaValidation("summary_condition_t.id")]
        public int? id { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type=CHK_TYPE.All)]
        public string value { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type=CHK_TYPE.All)]
        public string value_from { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type=CHK_TYPE.All)]
        public string value_to { get; set; }
    }
}