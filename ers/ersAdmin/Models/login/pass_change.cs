﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.administrator;
using ersAdmin.Domain.Login.Commands;
using ersAdmin.Domain.Login.Mappables;

namespace ersAdmin.Models.login
{
    public class Pass_change
        : ErsModelBase, IPassChangeCommand, IPassChangeMappable
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("administrator_t.passwd")]
        public string passwd { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("administrator_t.passwd")]
        [DisplayName("passwd_confirm")]
        public string passwd_confirm { get; set; }

    }
}