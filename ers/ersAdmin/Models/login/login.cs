﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.state;
using ersAdmin.Domain.Login.Commands;

namespace ersAdmin.Models
{
    public class Login
        : ErsModelBase, ILoginCommand
    {
        [HtmlSubmitButton]
        public bool sessionError { get; set; }

        [ErsSchemaValidation("administrator_t.user_login_id")]
        public string user_login_id { get; set; }

        [ErsSchemaValidation("administrator_t.passwd")]
        public string passwd { get; set; }

        //mcode
        public string user_cd { get; set; }

    }
}