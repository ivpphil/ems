﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using ersAdmin.Domain.Home.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class index
        : ErsModelBase, IIndexMappable
    {

        //ディクショナリの設定
        public Dictionary<string, Dictionary<string, string>> dict = new Dictionary<string, Dictionary<string, string>>();

        //コンストラクタ
        public index()
        {   

        }

        //現在日時(速報値)
        public string CurDate { get; set; }

        // ---------------------------------------------------
        //売上(本日)
        public Dictionary<string, int> TodaySales { get; set; }

        //売上(今週)
        public Dictionary<string, int> ThisWeekSales { get; set; }

        //売上(先週)
        public Dictionary<string, int> LastWeekSales { get; set; }

        //売上(今月)
        public Dictionary<string, int> ThisMonthSales { get; set; }

        //売上(先月)
        public Dictionary<string, int> LastMonthSales { get; set; }

        //売上(今年)
        public Dictionary<string, int> ThisYearSales { get; set; }

        //売上(昨年)
        public Dictionary<string, int> LastYearSales { get; set; }
        // ---------------------------------------------------
        //受注(本日)
        public Dictionary<string, int> TodayOrders { get; set; }

        //受注(今週)
        public Dictionary<string, int> ThisWeekOrders { get; set; }

        //受注(先週)
        public Dictionary<string, int> LastWeekOrders { get; set; }

        //受注(今月)
        public Dictionary<string, int> ThisMonthOrders { get; set; }

        //受注(先月)
        public Dictionary<string, int> LastMonthOrders { get; set; }

        //受注(今年)
        public Dictionary<string, int> ThisYearOrders { get; set; }

        //受注(昨年)
        public Dictionary<string, int> LastYearOrders { get; set; }
        // ---------------------------------------------------
        //キャンセル(本日)
        public int TodayCancelNum { get; set; }

        //キャンセル(今週)
        public int ThisWeekCancelNum { get; set; }

        //キャンセル(先週)
        public int LastWeekCancelNum { get; set; }

        //キャンセル(今月)
        public int ThisMonthCancelNum { get; set; }

        //キャンセル(先月)
        public int LastMonthCancelNum { get; set; }

        //キャンセル(今年)
        public int ThisYearCancelNum { get; set; }

        //キャンセル(昨年)
        public int LastYearCancelNum { get; set; }
        // ---------------------------------------------------

        //累計会員数
        public long MemberAllCount { get; set; }

        //累計ポイント発行数
        public long AllPoint { get; set; }

        //使用ポイント数
        public long UsedPoint { get; set; }

        //売上トップ5
        public List<Dictionary<string, object>> GetTop { get; set; }

        // Display Member Rank
        public string DisplayRank { get; set; }

        //Display Previous Update
        public string PreviousUpdate { get; set; }

        //Display Next Update
        public string NextUpdate { get; set; }
    }
}