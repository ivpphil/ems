﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class Del_ransu
        : ErsModelBase
    {

        internal void Delete()
        {
            var stgy = ErsFactory.ersSessionStateFactory.GetRansuDeleteStgy();

            stgy.MakeIgnorRansuList();
            stgy.DeleteExpiredRansu();
            stgy.DeleteExpiredAdminRansu();
            stgy.DeleteExpiredContactRansu();
        }
    }
}