﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using ersAdmin.Domain.Cms.Commands;

namespace ersAdmin.Models.cms
{
    public class free_img_group_record
        : ErsBindableModel, IFreeImgGroupRecordCommand
    {
        [ErsOutputHidden("required")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string img_item_name { get; set; }

        [BindTable("free_img_records")]
        public IList<free_img_record> free_img_records { get; set; }
    }
}