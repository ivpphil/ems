﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.merchandise;
using System.IO;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using ersAdmin.Domain.Cms.Commands;
using ersAdmin.Models.cms;
using ersAdmin.Domain.Cms.Mappables;

namespace ersAdmin.Models
{
    public class free_modify
        : ErsModelBase, IFreeModifyCommand, IFreeModifyMappable, IFreeDeleteCommand
    {
        public string user_cd { get { return ErsContext.sessionState.Get("user_cd"); } }

        public bool IsConfirmation { get; set; }

        public bool IsInitialize { get; set; }

        public free_modify()
        {
            this.posted_date = DateTime.Now;
        }

        [ErsOutputHidden("required")]
        [ErsSchemaValidation("news_article_t.article_code")]
        public string article_code { get; set; }

        public string contents_name { get; set; }

        public string contents_name_admin { get; set; }

        public string title_name { get; set; }

        public string sub_title_name { get; set; }

        public string body_name { get; set; }

        public string add_body_name { get; set; }

        public string code_name { get; set; }

        public string period_name { get; set; }

        public string template_file_path { get; set; }

        public EnumCmsFieldType? title_use_flg { get; set; }

        public EnumCmsFieldType? sub_title_use_flg { get; set; }

        public EnumCmsFieldType? body_use_flg { get; set; }

        public EnumCmsFieldType? add_body_use_flg { get; set; }

        public EnumCmsFieldType? code_use_flg { get; set; }

        //検索結果
        [BindTable("free_img_group_records")]
        public IList<free_img_group_record> free_img_group_records { get; set; }

        [BindTable("free_link_records")]
        public IList<free_link_record> free_link_records { get; set; }

        [BindTable("free_file_records")]
        public IList<free_file_record> free_file_records { get; set; }

        [ErsOutputHidden("required")]
        [ErsSchemaValidation("news_article_t.contents_code")]
        public string contents_code { get; set; }

        [ErsOutputHidden("required")]
        [ErsSchemaValidation("news_article_t.template_code")]
        public string template_code { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("news_article_t.scode", requireAlphabet = true)]
        public string scode { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("news_article_t.title")]
        public string title { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("news_article_t.sub_title")]
        public string sub_title { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("news_article_t.body")]
        public string body { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("news_article_t.add_body")]
        public string add_body { get; set; }

        [ErsOutputHidden("input")]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? posted_date { get; set; }

        [ErsOutputHidden("input")]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? period_from { get; set; }

        [ErsOutputHidden("input")]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? period_to { get; set; }

        [ErsOutputHidden("required")]
        [ErsSchemaValidation("news_article_t.intime")]
        public DateTime? intime { get; set; }

        [ErsOutputHidden("required")]
        [ErsSchemaValidation("news_article_t.utime")]
        public DateTime? utime { get; set; }

        [ErsOutputHidden("required")]
        [ErsSchemaValidation("news_article_t.create_user_id")]
        public int? create_user_id { get; set; }

        [ErsOutputHidden("required")]
        [ErsSchemaValidation("news_article_t.upd_user_id")]
        public int? upd_user_id { get; set; }

        public string create_name { get; set; }

        public string update_user { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("news_article_t.active")]
        public EnumActive? active { get; set; }

        [HtmlSubmitButton]
        public bool delete { get; set; }

        public string active_name
        {
            get
            {
                var active_value = this.active == null ? EnumActive.NonActive : this.active;
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ArticleState, EnumCommonNameColumnName.namename, (int?)active_value);
            }
        }
    }
}