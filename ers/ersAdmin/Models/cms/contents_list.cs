﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers;
using ersAdmin.Domain.Cms.Mappables;

namespace ersAdmin.Models
{
    public class contents_list
        : ErsModelBase, IContentsListMappable
    {
        public ErsPagerModel pager { get; set; }

        public virtual string noSearchResultMessage { get { return ErsResources.GetMessage("10200"); } }

        public virtual long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long recordCount { get; set; }

        //検索結果
        public List<Dictionary<string, object>> list { get; set; }
    }
}