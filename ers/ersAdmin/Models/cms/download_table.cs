﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using ersAdmin.Domain.Cms.Commands;
using ersAdmin.Domain.Cms.Mappables;

namespace ersAdmin.Models
{
    public class download_table : update_table, IDownloadTableCommand, IDownloadTableMappable
    {
        public download_table()
        {            
            this.isFromDownload = (this.isDownLoad) ? true:false;
        }
    }
}