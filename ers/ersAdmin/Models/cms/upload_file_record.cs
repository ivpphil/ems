﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models.cms
{
    public class upload_file_record : ErsBindableModel
    {
        [ErsOutputHidden("required")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string file_name { get; set; }

        //[ErsOutputHidden("required")]
        //[ErsUniversalValidation(type = CHK_TYPE.All)]
        //public string full_path_file { get; set; }

        [ErsOutputHidden("required")]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? created_date { get; set; }
    }
}