﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers;
using ersAdmin.Domain.Cms.Commands;

namespace ersAdmin.Models
{
    public class contents_regist
        : contents_modify, IContentsRegistCommand
    {
    }
}