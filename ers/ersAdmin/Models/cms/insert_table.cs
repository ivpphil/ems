﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Cms.Mappables;
using ersAdmin.Models.cms;
using jp.co.ivp.ers;
using System.IO;
using ersAdmin.Domain.Cms.Commands;

namespace ersAdmin.Models
{
    public class insert_table
        : update_table, IInsertTableMappable, IInsertTableCommand
    {
        [BindTable("insert_file_record")]
        public IList<insert_file_record> insert_file_record { get; set; }

        [BindTable("insert_file_err_record")]
        public IList<insert_file_record> insert_file_err_record { get; set; }

        public bool isCompleted { get; set; }

        public virtual string download_path
        {
            get
            {
                if (this.download_err_folder_path.HasValue() && this.downloadFile.HasValue())
                    return Path.Combine(this.download_err_folder_path, this.downloadFile);

                return null;
            }
        }

        public virtual string download_err_folder_path
        {
            get
            {
                return ErsFactory.ersUtilityFactory.getSetup().insertTableFileErrorPath;
            }
        }
    }
}