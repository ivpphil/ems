﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;

namespace ersAdmin.Models.cms
{
    public class free_link_record
        : ErsBindableModel
    {
        [ErsOutputHidden("required")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? number { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("news_article_t.link_string_1")]
        [DisplayName("link_string")]
        public string link_string { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("news_article_t.link_url_1")]
        [DisplayName("link_url")]
        public string link_url { get; set; }
    }
}