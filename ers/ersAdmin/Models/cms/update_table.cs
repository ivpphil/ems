﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.cms;
using ersAdmin.Domain.Cms.Mappables;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models
{
    public class update_table : ErsModelBase, IUpdateTableMappable, IUpdateTableCommand
    {
        [BindTable("upload_file_record")]
        public IList<upload_file_record> upload_file_record { get; set; }

        [BindTable("upload_file_err_record")]
        public IList<upload_file_record> upload_file_err_record { get; set; }

        [BindFile]
        public HttpPostedFileBase file_name { get; set; }

        [HtmlSubmitButton]
        public bool isSave { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string downloadFile { get; set; }

        [HtmlSubmitButton]
        public bool isDownLoad { get; set; }

        [HtmlSubmitButton]
        public bool isFromDownload { get; set; }
    }
}