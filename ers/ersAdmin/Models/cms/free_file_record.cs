﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers;

namespace ersAdmin.Models.cms
{
    public class free_file_record
        : ErsBindableModel
    {
        #region Image uploader property
        public virtual string file_identifier
        {
            get
            {
                return ErsContext.sessionState.Get("admin_ransu") + "news_pdf" + number;
            }
        }

        public virtual string news_pdf_temp_folder
        {
            get
            {
                return "news_pdf";
            }
        }

        public bool IsInitialize { get; set; }

        #endregion

        [ErsOutputHidden("required")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? number { get; set; }

        [ErsOutputHidden("required")]
        [ErsSchemaValidation("news_article_t.file_real_name_1")]
        [DisplayName("file_real_name")]
        public string file_real_name { get; set; }

        [ErsOutputHidden("required")]
        [ErsSchemaValidation("news_article_t.file_name_1")]
        [DisplayName("file_name")]
        public string file_name { get; set; }

        [BindFile(10 * 1024 * 1024)]
        public HttpPostedFileBase news_pdf { get; set; }

        [BindTable("news_pdf_detail")]
        public IList<news_pdf_detail> news_pdf_detail { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("news_article_t.file_string_1")]
        [DisplayName("file_string")]
        public string file_string { get; set; }

        [ErsOutputHidden("input")]
        [HtmlSubmitButton]
        public bool file_delete { get; set; }

        [ErsOutputHidden("required")]
        [ErsUniversalValidation(type = CHK_TYPE.OneByteCharacter)]
        public string temp_file_name { get; set; }

        [ErsOutputHidden("required")]
        [ErsSchemaValidation("news_article_t.file_real_name_1")]
        [DisplayName("file_real_name")]
        public string old_file_real_name { get; set; }

        [ErsOutputHidden("input")]
        [HtmlSubmitButton]
        public bool disabled_file_uploader { get; set; }

        public bool HasRecordNewsPdfDetail
        {
            get
            {
                if (news_pdf_detail != null && news_pdf_detail.Count > 0 && news_pdf_detail.First().news_pdf_file_name.HasValue())
                    return true;

                return false;
            }
        }

        public string news_pdf_detail_filename
        {
            get
            {
                if (HasRecordNewsPdfDetail)
                    return this.news_pdf_detail.First().news_pdf_file_name;

                return string.Empty;
            }
        }
    }
}