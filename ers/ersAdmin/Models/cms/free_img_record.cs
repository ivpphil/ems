﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;

namespace ersAdmin.Models.cms
{
    public class free_img_record
        : ErsBindableModel
    {
        [ErsOutputHidden("required")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? img_width { get; set; }

        [ErsOutputHidden("required")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? img_count { get; set; }

        [ErsOutputHidden("required")]
        [ErsSchemaValidation("news_article_t.img_file_name")]
        public string img_file_name { get; set; }

        [BindPicture(EnumPictureType.JPEG, EnumPictureType.PNG, EnumPictureType.GIF)]
        public HttpPostedFileBase news_image { get; set; }

        [ErsOutputHidden("input")]
        [HtmlSubmitButton]
        public bool file_delete { get; set; }

        [ErsOutputHidden("required")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string temp_image_file_name { get; set; }
    }
}