﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers;
using ersAdmin.Domain.Cms.Mappables;
using ersAdmin.Domain.Cms.Commands;

namespace ersAdmin.Models
{
    public class free_template
        : ErsModelBase, IFreeTemplateMappable, IFreeTemplateCommand
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("news_article_t.contents_code")]
        public string contents_code { get; set; }

        public string[] available_template { get; set; }

        //検索結果
        public List<ErsCmsTemplate> template { get; set; }
    }
}