﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.merchandise;
using System.IO;
using jp.co.ivp.ers;
using ersAdmin.Domain.Cms.Commands;
using ersAdmin.Domain.Cms.Mappables;

namespace ersAdmin.Models
{
    public class free_regist
        : free_modify, IFreeRegistCommand, IFreeRegistMappable
    {
        public string login_user { get; set; }

        public free_regist()
        {
            var admin = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithUserCode(this.user_cd);
            this.login_user = admin.user_name;
            active = EnumActive.Active;
        }
    }
}