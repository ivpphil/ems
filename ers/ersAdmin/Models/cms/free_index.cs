﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using ersAdmin.Domain.Cms.Mappables;

namespace ersAdmin.Models
{
    public class free_index
        : ErsModelBase, IFreeIndexMappable
    {
        public IList<ErsCmsContents> free_list { get; set; }
    }
}