﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using System.IO;
using ersAdmin.Models.cms;
using ersAdmin.Domain.Cms.Commands;
using ersAdmin.Domain.Cms.Mappables;

namespace ersAdmin.Models
{
    public class delete_table
        : update_table, IDeleteTableCommand, IDeleteTableMappable
    {
        [BindTable("delete_file_record")]
        public IList<delete_file_record> delete_file_record { get; set; }

        [BindTable("delete_file_err_record")]
        public IList<delete_file_record> delete_file_err_record { get; set; }

        public bool isCompleted { get; set; }

        public virtual string download_path
        {
            get
            {
                if (this.download_err_folder_path.HasValue() && this.downloadFile.HasValue())
                    return Path.Combine(this.download_err_folder_path, this.downloadFile);

                return null;
            }
        }

        public virtual string download_err_folder_path
        {
            get
            {
                return ErsFactory.ersUtilityFactory.getSetup().deleteFileErrorPath;
            }
        }
    }
}