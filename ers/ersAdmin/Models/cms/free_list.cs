﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers.administrator;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using ersAdmin.Domain.Cms.Commands;
using ersAdmin.Domain.Cms.Mappables;

namespace ersAdmin.Models
{
    public class free_list
        : ErsModelBase, IFreeListCommand, IFreeListMappable
    {
        public ErsPagerModel pager { get; set; }

        public virtual string noSearchResultMessage { get { return ErsResources.GetMessage("10200"); } }

        public virtual long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long recordCount { get; set; }

        [ErsOutputHidden("non_search_condition")]
        [ErsSchemaValidation("news_article_t.contents_code")]
        public string contents_code { get; set; }

        public string contents_name { get; set; }

        public string contents_name_admin { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("news_article_t.posted_date")]
        public DateTime? posted_date_from { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("news_article_t.posted_date")]
        public DateTime? posted_date_to { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 255)]
        public string keyword { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool containsActive { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool containsBefore { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool containsAfter { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool containsNonActive { get; set; }

        public int? site_id { get; set; }

        public string preview_site_url {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.multiple_pc_sec_url((int)this.site_id);

            }
        }

        private ErsEncryption enc { get { return ErsFactory.ersUtilityFactory.getErsEncryption(); } }

        public string admin_ransu { get { return this.enc.HexEncode(ErsContext.sessionState.Get("admin_ransu")); } }

        public string admin_ssl_ransu { get { return this.enc.HexEncode(ErsContext.sessionState.Get("admin_ssl_ransu")); } }

        public string user_cd { get { return this.enc.HexEncode(ErsContext.sessionState.Get("user_cd")); } }

        //検索結果
        public List<Dictionary<string, object>> itemList { get; set; }
    }
}