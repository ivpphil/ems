﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using System.IO;

namespace ersAdmin.Models.cms
{
    public class news_pdf_detail
        : ErsBindableModel
    {
        public override string lineName
        {
            get
            {
                return ErsResources.GetFieldName("news_pdf") + this.lineNumber;
            }
        }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("news_article_t.file_real_name_1")]
        [DisplayName("file_real_name")]
        public string news_pdf_file_name { get; set; }

        [ErsOutputHidden("input")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        [DisplayName("temp_file_name")]
        public string file_identifier { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All, isArray = true)]
        [DisplayName("news_pdf")]
        public string[] api_errorList { get; set; }

        public void SaveFromTemp(string tempDirectory, string outputDirectory)
        {
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();
            string tempfilename = file_identifier + news_pdf_file_name;

            if (File.Exists(tempDirectory + tempfilename))
            {
                uploadedFileHelper.SavePdfFile(tempDirectory, tempfilename, outputDirectory, news_pdf_file_name);
            }
        }
    }
}