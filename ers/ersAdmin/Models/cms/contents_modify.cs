﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers;
using ersAdmin.Domain.Cms.Commands;
using ersAdmin.Domain.Cms.Mappables;

namespace ersAdmin.Models
{
    public class contents_modify
        : ErsModelBase, IContentsModifyCommand, IContentsModifyMappable, IContentsDeleteCommand
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.id")]
        public int? id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.contents_code", rangeFrom = 8, rangeTo = 10)]
        public string contents_code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.contents_name", rangeTo = 50)]
        public string contents_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.available_template")]
        public string[] available_template { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.code_name", rangeTo = 50)]
        public string code_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.title_name", rangeTo = 50)]
        public string title_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.sub_title_name", rangeTo = 50)]
        public string sub_title_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.body_name", rangeTo = 50)]
        public string body_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.add_body_name", rangeTo = 50)]
        public string add_body_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.period_name", rangeTo = 50)]
        public string period_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.link_count", rangeFrom = 0, rangeTo = 5)]
        public int? link_count { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.file_count", rangeFrom = 0, rangeTo = 5)]
        public int? file_count { get; set; }

        [ErsSchemaValidation("cms_contents_t.intime")]
        public DateTime? intime { get; set; }

        [ErsSchemaValidation("cms_contents_t.utime")]
        public DateTime? utime { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cms_contents_t.active")]
        public EnumActive? active { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.FullString)]
        public string active_name
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Active, EnumCommonNameColumnName.namename, (int?)this.active);
            }
        }

        public List<Dictionary<string, object>> templateList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCmsTemplateService().SelectAsList(this.available_template); } }
    }
}