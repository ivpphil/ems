﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.order;
using ersAdmin.Models.csv;
using System.ComponentModel;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;
using ersAdmin.Domain.Regular.Mappables;

namespace ersAdmin.Models
{
    public class Bill_search
        : ErsSiteSearchModelBase, IBillSearchMappable, IBillSearchCSVMappable
    {
        public ErsCsvCreater csvCreater { get; set; }
        public Bill_search()
        {
            this.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
        }

        public ErsPagerModel pager { get; set; }

        public bool IsBillSearchPage { get; internal set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        /// <summary>
        /// Get number of item on a merchandise list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        public long recordCount { get; set; }

        /// <summary>
        /// 初期設定
        /// </summary>
        /// <param name="pager"></param> remove
        public void Init(ErsPagerModel pager)
        {
            this.pager = pager;
        }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("s_bill_intime")]
        public DateTime? s_bill_intime_from { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("s_bill_intime")]
        public DateTime? s_bill_intime_to { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("s_paid_date")]
        public DateTime? s_paid_date_from { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("s_paid_date")]
        public DateTime? s_paid_date_to { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("s_shipdate")]
        public DateTime? s_shipdate_from { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("s_shipdate")]
        public DateTime? s_shipdate_to { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("s_af_cancel_date")]
        public DateTime? s_af_cancel_date_from { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("s_af_cancel_date")]
        public DateTime? s_af_cancel_date_to { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("s_cancel_date")]
        public DateTime? s_cancel_date_from { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("s_cancel_date")]
        public DateTime? s_cancel_date_to { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("ds_master_t.scode", requireAlphabet = true)]
        [DisplayName("s_scode")]
        public string s_scode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("ds_master_t.order_type")]
        public EnumOrderType? s_order_type { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("ds_master_t.order_status")]
        public EnumOrderStatusType? s_order_status { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("d_master_t.order_payment_status")]
        public EnumOrderPaymentStatusType? s_order_payment_status { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("d_master_t.email")]
        public string s_email { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("d_master_t.d_no")]
        public string s_d_no { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("d_master_t.mall_d_no")]
        public string s_mall_d_no { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("d_master_t.pay")]
        public EnumPaymentType? s_pay { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("d_master_t.pm_flg")]
        public EnumPmFlg? s_pm_flg { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 255)]
        [DisplayName("lp_group_name")]
        public string s_ccode { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool s_continual_biling { get; set; }

        public List<Dictionary<string, object>> order_statusList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().SelectAsList(false); }
        }

        public List<Dictionary<string, object>> order_payment_statusList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewOrderPaymentStatusService().SelectAsList(false); }
        }

        public List<Dictionary<string, object>> payList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPayService().SelectAsList(false, false, (int)EnumSiteId.COMMON_SITE_ID); }
        }

        public List<Dictionary<string, object>> pm_flgList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PmFlg, EnumCommonNameColumnName.namename); }
        }

        public IEnumerable<Dictionary<string, object>> orderList { get; set; }

        public List<Dictionary<string, object>> orderTypeList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.OrderType, EnumCommonNameColumnName.namename, 1); } }

        public IList<Dictionary<string, object>> lpGroupNameList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsLpPageManageViewService().GetLpGroupName(); }
        }

        /// <summary>
        /// 初期値を設定する。
        /// </summary>  remove
        public virtual void SetDefaultData()
        {
            if (this.s_bill_intime_from == null)
            {
                s_bill_intime_from = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/01 00:00:00"));
            }
            if (this.s_bill_intime_to == null)
            {
                s_bill_intime_to = Convert.ToDateTime(DateTime.Now.AddMonths(1).ToString("yyyy/MM/01 23:59:59")).AddDays(-1);
            }
        }
    }
}