﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.order.strategy;
using jp.co.ivp.ers.member.strategy;
using ersAdmin.Models.regular;
using jp.co.ivp.ers;
using ersAdmin.Domain.Regular.Mappables;
using ersAdmin.Domain.Regular.Commands;
using jp.co.ivp.ers.mall;

namespace ersAdmin.Models
{
    public class BillModify
        : ErsSiteRegisterModelBase, IBillModifyCommand, IBillModifyMappable
    {
        #region "CTSログイン情報"
        public string admin_ransu { get { return ErsContext.sessionState.Get("admin_ransu"); } }

        public string admin_ssl_ransu { get { return ErsContext.sessionState.Get("admin_ssl_ransu"); } }

        public string user_cd { get { return ErsContext.sessionState.Get("user_cd"); } }

        public bool canDetailModify
        {
            get
            {
                var administrator = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithUserCode(this.user_cd);
                var agent = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(administrator.agent_id);
                return agent != null;
            }
        }
        #endregion

        public bool IsLoadDefaultValue { get; set; }

        public ErsOrderContainer objOrder { get; set; }

        [HtmlSubmitButton]
        public bool bill_change { get; set; }

        [HtmlSubmitButton]
        public bool bill_cancel { get; set; }

        [BindTable("orderRecords")]
        public List<bill_details> orderRecords { get; set; }

        [ErsOutputHidden("input_form")]
        [ErsSchemaValidation("d_master_t.d_no")]
        public string d_no
        {
            get
            {
                return _d_no;
            }
            set
            {
                //Formatting the d_no value since a hyphen is removed at validation of DataAnnotation.
                if (!string.IsNullOrEmpty(value) && value.Length > 8 && !value.Contains('-'))
                {
                    _d_no = value.Substring(0, 8) + "-" + value.Substring(8);
                }
                else
                {
                    _d_no = value;
                }
            }
        }
        private string _d_no;

        [ErsOutputHidden("input_form")]
        [ErsSchemaValidation("d_master_t.mall_d_no")]
        public string mall_d_no { get; set; }

        [ErsOutputHidden("input_form")]
        [ErsSchemaValidation("d_master_t.member_card_id")]
        public int? member_card_id { get; set; }


        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.email")]
        public string email { get; set; }

        [ErsSchemaValidation("d_master_t.email")]
        public string email_confirm { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.lname")]
        public string lname { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.fname")]
        public string fname { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.lnamek")]
        public string lnamek { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.fnamek")]
        public string fnamek { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.compname")]
        public string compname { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.compnamek")]
        public string compnamek { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.division")]
        public string division { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.divisionk")]
        public string divisionk { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.zip")]
        public string zip { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.pref")]
        public int? pref { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.address")]
        public string address { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.taddress")]
        public string taddress { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.maddress")]
        public string maddress { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.tel")]
        public string tel { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("member_t.fax")]
        public string fax { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_lname")]
        public string add_lname { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_fname")]
        public string add_fname { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_lnamek")]
        public string add_lnamek { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_fnamek")]
        public string add_fnamek { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_compname")]
        public string add_compname { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_compnamek")]
        public string add_compnamek { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_zip")]
        public string add_zip { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_pref")]
        public int? add_pref { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_address")]
        public string add_address { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_taddress")]
        public string add_taddress { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_maddress")]
        public string add_maddress { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_tel")]
        public string add_tel { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.add_fax")]
        public string add_fax { get; set; }

        [BindTarget("d_master_t")]
        [ErsOutputHidden("payment_not_changable")]
        [ErsSchemaValidation("d_master_t.order_payment_status")]
        public EnumOrderPaymentStatusType? order_payment_status { get; set; }

        [BindTarget("d_master_t")]
        [ErsOutputHidden("payment_not_changable")]
        [ErsSchemaValidation("d_master_t.paid_date")]
        public DateTime? paid_date { get; set; }

        [BindTarget("d_master_t")]
        [ErsOutputHidden("payment_not_changable")]
        [ErsSchemaValidation("d_master_t.paid_price")]
        public int? paid_price { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.send")]
        public EnumSendTo? send { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.senddate")]
        public DateTime? senddate { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.sendtime")]
        public int? sendtime { get; set; }

        [ErsSchemaValidation("ds_master_t.deliv_method")]
        public virtual EnumDelvMethod? deliv_method { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.wrap")]
        public EnumWrap? wrap { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.memo2")]
        public string memo2 { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.memo")]
        public string memo { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.memo3")]
        public string memo3 { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.usr_memo")]
        public string usr_memo { get; set; }

        public EnumPaymentType pay { get; set; }

        public EnumConvCode? conv_code { get; set; }

        public string w_conv_code { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ConvCode, EnumCommonNameColumnName.namename, (int?)conv_code); } }

        public string w_pm_flg
        {
            get
            {
                if (objOrder == null)
                {
                    return null;
                }
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.PmFlg, EnumCommonNameColumnName.namename, (int?)objOrder.OrderHeader.pm_flg);
            }
        }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.carriage")]
        public int? carriage { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.etc")]
        public int? etc { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.total")]
        public int? total { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.p_service")]
        public int? p_service { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.coupon_code")]
        public string coupon_code { get; set; }

        [BindTarget("d_master_t")]
        [ErsSchemaValidation("d_master_t.coupon_discount")]
        public int coupon_discount { get; set; }

        public EnumOrderStatusType? current_order_status
        {
            get
            {
                if (objOrder == null)
                    return null;

                return ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy().ObtainFrom(objOrder.OrderRecords.Values);
            }
        }

        public string mcode
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return objOrder.OrderHeader.mcode;
            }
        }

        public string c_req_no
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return ErsFactory.ersOrderFactory.GetErsPayment(objOrder.OrderHeader.pay).GetTransactionStringForView(objOrder.OrderHeader);
            }
        }

        public int subtotal
        {
            get
            {
                if (objOrder == null)
                    return 0;

                return objOrder.OrderHeader.subtotal;
            }
        }

        public int tax
        {
            get
            {
                if (objOrder == null)
                    return 0;

                return objOrder.OrderHeader.tax;
            }
        }

        public DateTime? intime
        {
            get
            {
                if (objOrder == null)
                    return null;

                return objOrder.OrderHeader.intime;
            }
        }

        public DateTime? utime
        {
            get
            {
                if (objOrder == null)
                    return null;

                return objOrder.OrderHeader.utime;
            }
        }

        public string ccode
        {
            get
            {
                if (objOrder == null)
                    return null;

                return objOrder.OrderHeader.ccode;
            }
        }

        public string etc_name
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetEtcNameFromId(objOrder.OrderHeader.pay, objOrder.OrderHeader.site_id);
            }
        }

        public string pay_name
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(objOrder.OrderHeader.pay, objOrder.OrderHeader.site_id);
            }
        }

        public int amounttotal
        {
            get
            {
                if (objOrder == null)
                    return 0;

                return ErsFactory.ersOrderFactory.GetObtainAmountTotalStgy().Obtain(objOrder.OrderRecords.Values, true);
            }
        }

        public string w_order_type
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                var orderType = ErsFactory.ersOrderFactory.GetObtainOrderTypeStgy().Obtain(objOrder.OrderRecords.Values);
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.OrderType, EnumCommonNameColumnName.namename, (int)orderType);
            }
        }

        public string w_order_payment_status
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return ErsFactory.ersViewServiceFactory.GetErsViewOrderPaymentStatusService().GetStringFromId(objOrder.OrderHeader.order_payment_status);
            }
        }

        public virtual string w_deliv_method
        {
            get
            {
                if (this.objOrder == null || this.objOrder.OrderRecords == null)
                {
                    return string.Empty;
                }
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().
                    GetStringFromId(EnumCommonNameType.DelvMethod, EnumCommonNameColumnName.namename,
                    this.objOrder.OrderRecords.Values.Min(e => (int)e.deliv_method));
            }
        }

        public virtual bool canSelectMailDelv
        {
            get
            {
                if (this.objOrder == null || this.objOrder.OrderRecords == null)
                {
                    return false;
                }

                var item = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(this.objOrder.OrderRecords.First().Value.gcode);

                return this.objOrder.OrderRecords.Count == 1
                    && this.objOrder.OrderRecords.First().Value.amount == 1
                    && this.objOrder.OrderRecords.First().Value.regular_detail_id == null
                    && item != null && item.deliv_method == EnumDelvMethod.Mail;
            }
        }

        public List<Dictionary<string, object>> deliv_methodList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.DelvMethod, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> prefList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(false, this.site_id_not_array); }
        }

        public List<Dictionary<string, object>> order_payment_statusList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewOrderPaymentStatusService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> sendtimeList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> sendList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.SendTo, EnumCommonNameColumnName.namename); }
        }

        public bool isBeforeDeliver { get; set; }

        public bool payment_changeable
        {
            get
            {
                if (this.objOrder == null)
                {
                    return false;
                }

                return ErsFactory.ersOrderFactory.GetIsPaymentInfoChangableSpec().IsChangable(this.objOrder.OrderHeader);
            }
        }

        public string contact_sec_url
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                if (setup.Multiple_sites && objOrder != null)
                {
                    return setup.multiple_contact_sec_url((int)objOrder.OrderHeader.site_id);
                }
                else
                {
                    return setup.contact_sec_url;
                }
            }
        }
    }
}