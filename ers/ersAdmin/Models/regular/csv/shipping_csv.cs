﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using System.ComponentModel;

namespace ersAdmin.Models.regular.csv
{
    public class Shipping_csv
        : ErsModelBase
    {
        internal virtual ErsOrder objOrder { get; set; }
        internal virtual ErsOrderRecord objOrderRecord { get; set; }

        [CsvField]
        public virtual int? id { get; set; }

        [CsvField]
        public virtual string d_no { get; set; }

        //[CsvField]
        //[DisplayName("order_status")]
        //public virtual string w_order_status { get { return ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().GetStringFromId(objOrderRecord.order_status); } }

        [CsvField]
        public virtual DateTime? intime { get { return objOrderRecord.intime; } }

        [CsvField]
        public virtual DateTime? utime { get { return objOrderRecord.utime; } }

        [CsvField]
        public virtual string mcode { get { return objOrder.mcode; } }

        [CsvField]
        [DisplayName("ship_csv_lname")]
        public virtual string lname { get { return objOrder.lname; } }

        [CsvField]
        [DisplayName("ship_csv_fname")]
        public virtual string fname { get { return objOrder.fname; } }

        [CsvField]
        [DisplayName("ship_csv_lnamek")]
        public virtual string lnamek { get { return objOrder.lnamek; } }

        [CsvField]
        [DisplayName("ship_csv_fnamek")]
        public virtual string fnamek { get { return objOrder.fnamek; } }

        [CsvField]
        [DisplayName("ship_csv_compname")]
        public virtual string compname { get { return objOrder.compname; } }

        [CsvField]
        [DisplayName("ship_csv_compnamek")]
        public virtual string compnamek { get { return objOrder.compnamek; } }

        [CsvField]
        [DisplayName("ship_csv_division")]
        public virtual string division { get { return objOrder.division; } }

        [CsvField]
        [DisplayName("ship_csv_divisionk")]
        public virtual string divisionk { get { return objOrder.divisionk; } }

        [CsvField]
        [DisplayName("ship_csv_zip")]
        public virtual string zip { get { return objOrder.zip; } }

        [CsvField]
        [DisplayName("ship_csv_pref")]
        public virtual string w_pref { get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(objOrder.pref, objOrder.site_id); } }

        [CsvField]
        [DisplayName("ship_csv_address")]
        public virtual string address { get { return objOrder.address; } }

        [CsvField]
        [DisplayName("ship_csv_taddress")]
        public virtual string taddress { get { return objOrder.taddress; } }

        [CsvField]
        [DisplayName("ship_csv_maddress")]
        public virtual string maddress { get { return objOrder.maddress; } }

        [CsvField]
        [DisplayName("ship_csv_tel")]
        public virtual string tel { get { return objOrder.tel; } }

        [CsvField]
        [DisplayName("ship_csv_fax")]
        public virtual string fax { get { return objOrder.fax; } }

        [CsvField]
        [DisplayName("ship_csv_email")]
        public virtual string email { get { return objOrder.email; } }

        //[CsvField]
        //[DisplayName("send")]
        //public virtual string w_send { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.SendTo, EnumCommonNameColumnName.namename, (int?)objOrder.send); } }

        [CsvField]
        [DisplayName("deliv_method")]
        public virtual string delv_method {
            get; set;
        }

        [CsvField]
        [DisplayName("ship_csv_add_lname")]
        public virtual string add_lname
        { 
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.lname;
                }
                return objOrder.add_lname; 
            } 
        }

        [CsvField]
        [DisplayName("ship_csv_add_fname")]
        public virtual string add_fname
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.fname;
                }
                return objOrder.add_fname;
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_lnamek")]
        public virtual string add_lnamek
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.lnamek;
                }
                return objOrder.add_lnamek;
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_fnamek")]
        public virtual string add_fnamek
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.fnamek;
                }
                return objOrder.add_fnamek;
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_compname")]
        public virtual string add_compname
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.compname;
                }
                return objOrder.add_compname;
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_compnamek")]
        public virtual string add_compnamek
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.compnamek;
                }
                return objOrder.add_compnamek;
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_division")]
        public virtual string add_division
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.division;
                }
                return objOrder.add_division;
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_divisionk")]
        public virtual string add_divisionk
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.divisionk;
                }
                return objOrder.add_divisionk;
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_zip")]
        public virtual string add_zip
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.zip;
                }
                return objOrder.add_zip;
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_pref")]
        public virtual string w_add_pref
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(objOrder.pref, objOrder.site_id); 
                }
                return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(objOrder.add_pref, objOrder.site_id); 
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_address")]
        public virtual string add_address
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.address;
                }
                return objOrder.add_address;
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_taddress")]
        public virtual string add_taddress
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.taddress;
                }
                return objOrder.add_taddress;
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_maddress")]
        public virtual string add_maddress
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.maddress;
                }
                return objOrder.add_maddress;
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_tel")]
        public virtual string add_tel
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.tel;
                }
                return objOrder.add_tel;
            }
        }

        [CsvField]
        [DisplayName("ship_csv_add_fax")]
        public virtual string add_fax
        {
            get
            {
                if (objOrder.send == EnumSendTo.MEMBER_ADDRESS)
                {
                    return objOrder.fax;
                }
                return objOrder.add_fax;
            }
        }

        [CsvField]
        public virtual string pay { get { return ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(objOrder.pay, objOrder.site_id); } }

        [CsvField]
        public virtual DateTime? senddate { get { return objOrder.senddate; } }

        [CsvField]
        [DisplayName("sendtime")]
        public virtual string w_sendtime { get { return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId(objOrder.sendtime); } }

        [CsvField]
        public virtual string scode { get { return objOrderRecord.scode; } }

        [CsvField]
        public virtual string sname { get { return (objOrderRecord.shipping_sname.HasValue()) ? objOrderRecord.shipping_sname : objOrderRecord.sname; } }

        [CsvField]
        public virtual int? price { get { return objOrderRecord.price; } }

        [CsvField]
        public virtual int? amount { get { return objOrderRecord.GetAmount(); } }

        [CsvField]
        public virtual int? ds_total { get { return objOrderRecord.total; } }

        [CsvField]
        public virtual int? subtotal { get { return objOrder.subtotal; } }

        [CsvField]
        public virtual int? tax { get { return objOrder.tax; } }

        [CsvField]
        public virtual int? carriage { get { return objOrder.carriage; } }

        [CsvField]
        public virtual int? etc { get { return objOrder.etc; } }

        [CsvField]
        public virtual int? p_service { get { return objOrder.p_service; } }

        [CsvField]
        public virtual int? coupon_discount { get { return objOrder.coupon_discount; } }

        [CsvField]
        public virtual int? total { get { return objOrder.total; } }

        [CsvField]
        public virtual string shipping_memo { get { return objOrderRecord.shipping_memo; } }
    }
}