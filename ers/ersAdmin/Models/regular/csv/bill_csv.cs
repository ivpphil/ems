﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System.ComponentModel;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;

namespace ersAdmin.Models.csv
{
    public class Bill_csv
        : ErsModelBase
    {
        internal virtual ErsOrder objOrder { get; set; }
        internal virtual ErsOrderRecord objOrderRecord { get; set; }

        [CsvField]
        public virtual string d_no { get { return objOrder.d_no; } }

        [CsvField]
        public virtual string mcode { get { return objOrder.mcode; } }

        [CsvField]
        public virtual string pay { get { return ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(objOrder.pay, objOrder.site_id); } }

        [CsvField]
        public virtual string send { get; set; }

        [CsvField]
        public virtual int? subtotal { get { return objOrder.subtotal; } }

        [CsvField]
        public virtual int? tax { get { return objOrder.tax; } }

        [CsvField]
        public virtual int? carriage { get { return objOrder.carriage; } }

        [CsvField]
        public virtual int? p_service { get { return objOrder.p_service; } }

        [CsvField]
        public virtual int? etc { get { return objOrder.etc; } }

        [CsvField]
        public virtual string coupon_code { get { return objOrder.coupon_code; } }

        [CsvField]
        public virtual int? coupon_discount { get { return objOrder.coupon_discount; } }

        [CsvField]
        public virtual int? total { get { return objOrder.total; } }

        [CsvField]
        public virtual string senddate { get; set; }

        [CsvField]
        public virtual string sendtime { get; set; }

        [CsvField]
        public virtual string memo { get { return objOrder.memo; } }

        [CsvField]
        public virtual string ransu { get { return objOrder.ransu; } }

        [CsvField]
        public virtual short? point_ck { get { return objOrder.point_ck; } }

        [CsvField]
        public virtual string lname { get { return objOrder.lname; } }

        [CsvField]
        public virtual string fname { get { return objOrder.fname; } }

        [CsvField]
        public virtual string lnamek { get { return objOrder.lnamek; } }

        [CsvField]
        public virtual string fnamek { get { return objOrder.fnamek; } }

        [CsvField]
        public virtual string compname { get { return objOrder.compname; } }

        [CsvField]
        public virtual string compnamek { get { return objOrder.compnamek; } }

        [CsvField]
        public virtual string division { get { return objOrder.division; } }

        [CsvField]
        public virtual string divisionk { get { return objOrder.divisionk; } }

        //[CsvField]
        public virtual string tlname { get { return objOrder.tlname; } }

        //[CsvField]
        public virtual string tfname { get { return objOrder.tfname; } }

        //[CsvField]
        public virtual string tlnamek { get { return objOrder.tlnamek; } }

        //[CsvField]
        public virtual string tfnamek { get { return objOrder.tfnamek; } }

        [CsvField]
        public virtual string zip { get { return objOrder.zip; } }

        [CsvField]
        public virtual string pref { get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(objOrder.pref, objOrder.site_id); } }

        [CsvField]
        public virtual string address { get { return objOrder.address; } }

        [CsvField]
        public virtual string taddress { get { return objOrder.taddress; } }

        [CsvField]
        public virtual string maddress { get { return objOrder.maddress; } }

        [CsvField]
        public virtual string tel { get { return objOrder.tel; } }

        [CsvField]
        public virtual string fax { get { return objOrder.fax; } }

        [CsvField]
        public virtual string email { get { return objOrder.email; } }

        [CsvField]
        public virtual string add_lname { get { return objOrder.add_lname; } }

        [CsvField]
        public virtual string add_fname { get { return objOrder.add_fname; } }

        [CsvField]
        public virtual string add_lnamek { get { return objOrder.add_lnamek; } }

        [CsvField]
        public virtual string add_fnamek { get { return objOrder.add_fnamek; } }

        //[CsvField]
        public virtual string add_compname { get { return objOrder.add_compname; } }

        //[CsvField]
        public virtual string add_compnamek { get { return objOrder.add_compnamek; } }

        [CsvField]
        public virtual string add_zip { get; set; }

        [CsvField]
        public virtual string add_pref { get; set; }

        [CsvField]
        public virtual string add_address { get; set; }

        [CsvField]
        public virtual string add_taddress { get { return objOrder.add_taddress; } }

        [CsvField]
        public virtual string add_maddress { get { return objOrder.add_maddress; } }

        [CsvField]
        public virtual string add_tel { get { return objOrder.add_tel; } }

        [CsvField]
        public virtual string add_fax { get { return objOrder.add_fax; } }

        [CsvField]
        public virtual string wrap { get; set; }

        [CsvField]
        public virtual string memo2 { get { return objOrder.memo2; } }

        [CsvField]
        public virtual string c_req_no { get; set; }

        [CsvField]
        public virtual string memo3 { get { return objOrder.memo3; } }

        [CsvField]
        public virtual DateTime? paid_date { get { return objOrder.paid_date; } }

        [CsvField]
        public virtual int? paid_price { get { return objOrder.paid_price; } }

        [CsvField]
        public virtual string pm_flg { 
            get; set;
        }

        [CsvField]
        public virtual string usr_memo { get { return objOrder.usr_memo; } }

        [CsvField]
        public virtual DateTime? intime { get { return objOrder.intime; } }

        [CsvField]
        [DisplayName("lp_page_manage_t.ccode")]
        public virtual string ccode { get { return objOrder.ccode; } }

        [CsvField]
        public virtual string scode { get { return objOrderRecord.scode; } }

        [CsvField]
        public virtual string jancode { get { return objOrderRecord.jancode; } }

        [CsvField]
        public virtual string sname { get { return objOrderRecord.sname; } }

        [CsvField]
        public virtual string m_sname { get { return objOrderRecord.m_sname; } }

        [CsvField]
        public virtual int? price { get { return objOrderRecord.price; } }

        [CsvField]
        public virtual int? amount { get { return objOrderRecord.GetAmount(); } }

        [CsvField]
        [DisplayName("total")]
        public virtual int? ds_total { get { return objOrderRecord.total; } }

        [CsvField]
        public virtual string point { get; set; }

        [CsvField]
        public virtual string order_status { 
            get; set;
        }

        [CsvField]
        public virtual string sendno { get { return objOrderRecord.sendno; } }

        [CsvField]
        public virtual DateTime? shipdate { get { return objOrderRecord.shipdate; } }

        [CsvField]
        public virtual string shipping_memo { get { return objOrderRecord.shipping_memo; } }

        [CsvField]
        public virtual string subd_no { get; set; }

        [CsvField]
        public virtual string gcode { get { return objOrderRecord.gcode; } }

        [CsvField]
        public virtual string gname { get { return objOrderRecord.gname; } }

        [CsvField]
        public virtual string order_type {
            get; set;
        }

        [CsvField]
        public virtual string attribute1 { get { return objOrderRecord.attribute1; } }

        [CsvField]
        public virtual string attribute2 { get { return objOrderRecord.attribute2; } }

        [CsvField]
        public virtual int[] regular_detail_id { get { return objOrderRecord.regular_detail_id; } }

        [CsvField]
        public virtual string mall_d_no { get { return objOrder.mall_d_no; } }

        [CsvField]
        public virtual DateTime? after_cancel_date { get { return objOrderRecord.after_cancel_date; } }

        [CsvField]
        public virtual DateTime? cancel_date { get { return objOrderRecord.cancel_date; } }

        [CsvField]
        public virtual int? cancel_price { get { return objOrderRecord.cancel_price; } }

        [CsvField]
        [DisplayName("store_site_name")]
        public virtual string site_name
        {
            get
            {
                if (objOrder.site_id == null || objOrder.site_id == 0)
                {
                    return "共通";
                }
                return ErsMallFactory.ersMallViewServiceFactory.GetErsViewSiteService().GetStringFromId(objOrder.site_id);
            }
        }
    }
}
