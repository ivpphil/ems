﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Regular.Commands;

namespace ersAdmin.Models.csv
{
    public class Stock_update_csv
        : ErsBindableModel, IStockUpdateCSVRecordCommand
    {
        [CsvField]
        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]
        public string scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("stock_t.stock")]
        public int? stock { get; set; }
    }
}