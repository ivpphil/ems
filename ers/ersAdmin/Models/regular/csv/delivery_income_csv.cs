﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using ersAdmin.Domain.Regular.Commands;

namespace ersAdmin.Models.csv
{
    /// <summary>
    /// 代金引換入金データCSVファイル定義モデル
    /// </summary>
    public class Delivery_income_csv
        : ErsBindableModel, IDeliveryIncomeCSVRecordCommand
    {
        [CsvField]
        [ErsSchemaValidation("d_master_t.d_no")]
        public string d_no
        {
            get
            {
                return _d_no;
            }
            set
            {
                //Formatting the d_no value since a hyphen is removed at validation of DataAnnotation.
                if (!string.IsNullOrEmpty(value) && value.Length > 8 && !value.Contains('-'))
                {
                    _d_no = value.Substring(0, 8) + "-" + value.Substring(8);
                }
                else
                {
                    _d_no = value;
                }
            }
        }
        private string _d_no;

        [CsvField]
        [ErsSchemaValidation("d_master_t.paid_price")]
        public int paid_price { get; set; }

        protected ErsOrder objOrder;
    }
}