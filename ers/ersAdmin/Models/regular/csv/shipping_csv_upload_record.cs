﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order.strategy.status;
using jp.co.ivp.ers;
using System.ComponentModel;
using ersAdmin.Domain.Regular.Commands;

namespace ersAdmin.Models.csv
{
    /// <summary>
    /// 出庫許可リストCSVファイル定義モデル
    /// </summary>
    public class Shipping_csv_upload_record
        : ErsBindableModel, IShippingUploadCSVRecordCommand
    {
        [CsvField]
        [ErsUniversalValidation(type=CHK_TYPE.All, rangeFrom=10, rangeTo=14)]
        [DisplayName("d_no")]
        public virtual string shipping_d_no
        {
            get
            {
                return this._shipping_d_no;
            }
            set
            {
                this._shipping_d_no = value;

                if (string.IsNullOrEmpty(value))
                {
                    return;
                }

               var arrShippingD_no = value.Split(new[] { '_' });

               if (!string.IsNullOrEmpty(arrShippingD_no[0]))
               {
                   if (arrShippingD_no[0].Length == 10)
                       this.d_no = arrShippingD_no[0].Substring(0, 8) + "-" + arrShippingD_no[0].Substring(8, 2);
                   else
                       this.d_no = arrShippingD_no[0];
               }

                if (arrShippingD_no.Length < 2)
                {
                    this.subd_no = string.Empty;
                }
                else
                {
                    this.subd_no = arrShippingD_no[1];
                }
            }
        }
        private string _shipping_d_no;

        [CsvField]
        [ErsSchemaValidation("ds_master_t.sendno")]
        public virtual string sendno { get; set; }

        [CsvField]
        [ErsSchemaValidation("ds_master_t.shipdate")]
        public virtual DateTime? shipdate { get; set; }

        public EnumOrderStatusType? order_status { get; internal set; }

        public virtual string d_no { get; set; }

        public virtual string subd_no { get; set; }

        
    }
}