﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc.pager;
using ersAdmin.Models.regular.csv;
using ersAdmin.Domain.Regular.Mappables;
using System.ComponentModel;
using jp.co.ivp.ers;
using ersAdmin.Domain.Regular.Commands;

namespace ersAdmin.Models
{
    public class Shipment_list
        : Bill_search, IShipmentListMappable, IShipmentListCSVMappable, IShipmentListCSVCommand
    {
        public IList<ErsOrderContainer> billList { get; set; }
    }
}