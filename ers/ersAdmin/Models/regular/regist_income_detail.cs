﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Regular.Commands;

namespace ersAdmin.Models
{
    public class Regist_income_detail
        : ErsBindableModel, IRegistIncomeDetailListRecordCommand
    {

        [HtmlSubmitButton]
        public virtual bool paid_check { get; set; }

        [ErsSchemaValidation("d_master_t.paid_price")]
        public virtual int paid_price { get; set; }

        [ErsSchemaValidation("d_master_t.paid_date")]
        public virtual DateTime? paid_date { get; set; }

        public virtual DateTime? intime { get; set; }
        public virtual string lname { get; set; }
        public virtual string fname { get; set; }
        public virtual int? total { get; set; }

        [ErsSchemaValidation("d_master_t.d_no")]
        public virtual string d_no
        {
            get
            {
                return _d_no;
            }
            set
            {
                //Formatting the d_no value since a hyphen is removed at validation of DataAnnotation.
                if (!string.IsNullOrEmpty(value) && value.Length > 8 && !value.Contains('-'))
                {
                    _d_no = value.Substring(0, 8) + "-" + value.Substring(8);
                }
                else
                {
                    _d_no = value;
                }
            }
        }
        private string _d_no;
        public virtual string mall_d_no { get; set; }

    }
}