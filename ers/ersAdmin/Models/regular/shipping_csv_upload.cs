﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.Web.Mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Regular.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    /// <summary>
    /// 出荷戻りデータCSVアップロード処理クラス
    /// </summary>
    public class Shipping_csv_upload
        : ErsModelBase, IShippingUploadCSVCommand
    {
        /// <summary>
        /// Gets display message on finish page.
        /// </summary>
        public virtual string resultMsg
        {
            get
            {
                if (this.csv_file == null)
                {
                    return string.Empty;
                }
                return ErsResources.GetMessage("30000", this.csv_file.validIndexes.Count());
            }
        }

        /// <summary>
        /// 1行目をスキップする場合はtrue
        /// <para>If you want to skip the first line, value must be true</para>
        /// </summary>
        [ErsOutputHidden]
        [HtmlSubmitButton]
        public virtual bool chk_find { get; set; }

        /// <summary>
        /// アップロードデータ
        /// <para>Upload data</para>
        /// </summary>
        [ErsOutputHidden]
        [BindCsvFile]
        public ErsCsvContainer<csv.Shipping_csv_upload_record> csv_file { get; set; }

        /// <summary>
        /// 登録ボタンの押下
        /// </summary>
        [HtmlSubmitButton]
        public virtual bool regist { get; set; }

        public bool allCsvFieldsInvalid
        {
            get
            {
                if (this.csv_file == null)
                    return true;

                return (this.csv_file.validIndexes.Count() == 0);
            }
        }


        /// <summary>
        /// values for send mail
        /// </summary>
        public string d_no { get; set; }

        public IList<ErsOrderRecord> orderRecords { get; set; }

        public string sendno { get; set; }

        public DateTime? shipdate { get; set; }

        public DateTime? intime { get; set; }

        public string sendtime { get; set; }

        public int amounttotal { get; set; }

        public int subtotal { get; set; }

        public int carriage { get; set; }

        public int p_service { get; set; }

        public int coupon_discount { get; set; }

        public int total { get; set; }

        public string lname { get; set; }

        public string fname { get; set; }

        public string w_etc { get; set; }

        public int etc { get; set; }
    }
}