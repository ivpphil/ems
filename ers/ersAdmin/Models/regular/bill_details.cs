﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.order.strategy.status;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Models.regular
{
    public class bill_details
        : ErsBindableModel
    {
        [BindTarget("confirm")]
        [ErsOutputHidden("input_form")]
        [ErsUniversalValidation(type = CHK_TYPE.OneByteCharacter)]
        public virtual string key { get; set; }

        [BindTarget("ds_master_t")]
        [ErsSchemaValidation("ds_master_t.shipping_memo")]
        public string shipping_memo { get; set; }

        [BindTarget("confirm")]
        public virtual int? amount { get; set; }

        [BindTarget("confirm")]
        public EnumOrderStatusType? order_status { get; set; }

        [BindTarget("confirm")]
        public string subd_no { get; set; }

        [BindTarget("confirm")]
        public string sendno { get; set; }

        [BindTarget("confirm")]
        public DateTime? shipdate { get; set; }

        [BindTarget("confirm")]
        public virtual string scode { get; set; }

        [BindTarget("confirm")]
        public virtual string sname { get; set; }

        [BindTarget("confirm")]
        public virtual int? price { get; set; }

        [BindTarget("confirm")]
        public virtual int? total { get; set; }

        public string w_order_status
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().GetStringFromId(this.order_status); }
        }

        public bool changeable
        {
            get
            {
                if(!this.order_status.HasValue)
                {
                    return false;
                }

                return (this.order_status == EnumOrderStatusType.NEW_ORDER || this.order_status == EnumOrderStatusType.DELIVER_WAITING);
            }
        }

        public virtual DateTime? after_cancel_date { get; set; }

        public virtual DateTime? cancel_date { get; set; }

        public virtual List<Dictionary<string, object>> lstSetItems { get; set; }
    }
}