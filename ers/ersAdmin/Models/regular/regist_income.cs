﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using ersAdmin.Domain.Regular.Commands;
using ersAdmin.Domain.Regular.Mappables;

namespace ersAdmin.Models
{
    public class Regist_income
        : ErsModelBase, IRegistIncomeCommand, IRegistIncomeMappable
    {
        public ErsPagerModel pager {get;set;}

        /// <summary>
        /// Get number of item on a merchandise list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        public long recordCount { get;  set; }

        [BindTable("order")]
        public List<Regist_income_detail> inputDetails { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public bool isErrorBack { get; set; }

    }
}