using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.sendmail;
using System.ComponentModel;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Regular.Commands;
using ersAdmin.Domain.Regular.Mappables;

namespace ersAdmin.Models
{
    public class Bill_mail
        : ErsModelBase, IBillMailCommand,IBillMailMappable
    {
        public ErsOrder objOrder { get; set; }

        /// <summary>
        /// ��ʍ���
        /// </summary>
        [ErsOutputHidden]
        [ErsSchemaValidation("d_master_t.d_no")]
        public string d_no
        {
            get
            {
                return _d_no;
            }
            set
            {
                //Formatting the d_no value since a hyphen is removed at validation of DataAnnotation.
                if (!string.IsNullOrEmpty(value) && value.Length > 8 && !value.Contains('-'))
                {
                    _d_no = value.Substring(0, 8) + "-" + value.Substring(8);
                }
                else
                {
                    _d_no = value;
                }
            }
        }
        private string _d_no;

        public string lname
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return objOrder.lname;
            }
        }

        public string fname
        {
            get
            {
                if (objOrder == null)
                    return string.Empty;

                return objOrder.fname;
            }
        }

        public int? site_id
        {
            get
            {
                if (this.objOrder == null)
                    return null;

                return this.objOrder.site_id;
            }
        }

        public EnumMallShopKbn? mall_shop_kbn
        {
            get
            {
                if (this.objOrder == null)
                    return null;

                return this.objOrder.mall_shop_kbn;
            }
        }

        public bool IsMallOrder
        {
            get
            {
                if (this.mall_shop_kbn == null)
                    return false;

                return this.mall_shop_kbn != EnumMallShopKbn.ERS;
            }
        }

        public bool IsFromEmailFixed
        {
            get
            {
                if (this.mall_shop_kbn == null)
                    return false;

                return this.mall_shop_kbn == EnumMallShopKbn.RAKUTEN || this.mall_shop_kbn == EnumMallShopKbn.AMAZON;
            }
        }

        [ErsUniversalValidation(type = CHK_TYPE.EMailAddr)]
        public string email { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.EMailAddr)]
        public string from_email { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Templates)]
        public string mail_title { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Templates)]
        public string mail_body { get; set; }

        [HtmlSubmitButton]
        public bool send_to_admin { get; set; }

        [HtmlSubmitButton]
        public bool send_btn { get; set; }

        public bool IsSendMail { get; set; }


        public bool IsLoadDefault { get; set; }
    }
}