﻿using System.Collections.Generic;
using System.Web.Mvc;
using ersAdmin.Domain.StepMail.Commands;
using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.step_scenario;

namespace ersAdmin.Models.stepmail
{
    public class stepmail_regist : ErsModelBase, IStepmailRegistCommand, IStepmailRegistMappable
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("step_mail_t.scenario_id")]
        public int? scenario_id { get; set; }

        public List<Dictionary<string, object>> elapsedNumList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewElapsedNumService().GetElapsedNumber();
            }
        }

        public List<Dictionary<string, object>> statusList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Status, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> elapsedList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Elapsed, EnumCommonNameColumnName.namename);
            }
        }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string strmail_ref_date_kbn { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string strstatus { get; set; }

        [ErsSchemaValidation("step_mail_t.step_mail_name")]
        public string step_mail_name { get; set; }

        [ErsSchemaValidation("step_mail_t.elapsed_num")]
        public int elapsed_num { get; set; }

        [ErsSchemaValidation("step_mail_t.elapsed_kbn")]
        public EnumElapsed? elapsed_kbn { get; set; }

        [ErsSchemaValidation("step_mail_t.mail_status_kbn")]
        public EnumStatus? mail_status_kbn { get; set; }

        [ErsSchemaValidation("step_mail_t.pc_mail_title")]
        public string pc_mail_title { get; set; }

        [AllowHtml]
        [ErsSchemaValidation("step_mail_t.pc_mail_body", rangeTo = 5000)]
        public string pc_mail_body { get; set; }

        [ErsSchemaValidation("step_mail_t.pc_mail_body_txt")]
        public string pc_mail_body_txt { get; set; }    

        [ErsSchemaValidation("step_mail_t.mobile_mail_body")]
        public string mobile_mail_body { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public EnumOrderPattern? order_ptn_kbn { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public EnumDeliveryTime? mail_delv_time_kbn { get; set; }

        public string scenario_name { get; set; }

        public int? target_id { get; set; }

        public string w_target { get; set; }

        public int? mail_delv_time_hh { get; set; }

        public int? mail_delv_time_mm { get; set; }

        public int? mail_delv_out_time_hh_from { get; set; }

        public int? mail_delv_out_time_mm_from { get; set; }

        public int? mail_delv_out_time_hh_to { get; set; }

        public int? mail_delv_out_time_mm_to { get; set; }
    }
}