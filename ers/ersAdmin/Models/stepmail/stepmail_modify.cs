﻿using ersAdmin.Domain.StepMail.Commands;
using ersAdmin.Domain.StepMail.Mappables;
using ersAdmin.Models.stepmail;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.stepmail;

namespace ersAdmin.Models.stepMail
{
    public class stepmail_modify : stepmail_regist, IStepmailModifyCommand, IStepmailModifyMappable, IStepmailDeleteCommand
    {

        public bool IsLoadDefaultData { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("step_mail_t.id")]
        public int? id { get; set; }
    }
}