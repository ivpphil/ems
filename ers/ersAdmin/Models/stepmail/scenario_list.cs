﻿using System.Collections.Generic;
using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models.stepmail
{
    public class scenario_list : ErsModelBase, IScenarioListMappable
    {

        public ErsPagerModel pager { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        public long maxLineCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnOneLine; } }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("step_scenario_t.scenario_name")]
        public string s_scenario_name { get; set; }			//input_field			

        [ErsOutputHidden]
        [ErsSchemaValidation("step_scenario_t.mail_status_kbn")]
        public EnumStatus? s_mail_status_kbn { get; set; }   //pulldown  	                    

        public List<Dictionary<string, object>> scenarioList { get; set; }

        public List<Dictionary<string, object>> statusList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Status, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> scenario_Erslist { get; set; }

    }
}