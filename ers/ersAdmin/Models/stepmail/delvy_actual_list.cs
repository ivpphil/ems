﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.atmail;
using ersAdmin.Domain.StepMail.Mappables;

namespace ersAdmin.Models.stepmail
{
    public class delvy_actual_list
        : ErsModelBase, IDelvyActualListMappable
    {
        public ErsPagerModel pager { get; set; }

        /// <summary>
        /// Get number of item on a merchandise list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        public long recordCount { get;  set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        /// <summary>
        /// input_field
        /// </summary>																												
        public List<Dictionary<string, object>> searched_items { get; set; }

        
    }
}