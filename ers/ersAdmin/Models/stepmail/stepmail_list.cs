﻿using System.Collections.Generic;
using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models.stepmail
{
    public class stepmail_list : ErsModelBase, IStepmailListMappable, IStepmailRegistMappable
    {

        public ErsPagerModel pager { get; set; }

        /// <summary>
        /// Get number of item on a list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        public long maxLineCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnOneLine; } }

        public long recordCount { get;  set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public List<Dictionary<string, object>> StepMailList { get;  set; }

        public string scenario_name { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string strmail_ref_date_kbn { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string strstatus { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public EnumDeliveryTime? mail_delv_time_kbn { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public EnumOrderPattern? order_ptn_kbn { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("step_scenario_t.id")]
        public int? scenario_id { get; set; }

        public string w_target { get; set; }

        public List<Dictionary<string, object>> elapsedList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Elapsed, EnumCommonNameColumnName.namename);
            }
        }

        public int? mail_delv_time_hh { get; set; }

        public int? mail_delv_time_mm { get; set; }

        public int? mail_delv_out_time_hh_from { get; set; }

        public int? mail_delv_out_time_mm_from { get; set; }

        public int? mail_delv_out_time_hh_to { get; set; }

        public int? mail_delv_out_time_mm_to { get; set; }
    }
}