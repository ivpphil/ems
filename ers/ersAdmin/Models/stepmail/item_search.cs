﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using System.ComponentModel;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using ersAdmin.Domain.StepMail.Mappables;

namespace ersAdmin.Models.stepmail
{
    public class item_search : ErsModelBase, IItemSearchMappable
    {

        public ErsPagerModel pager { get; set; }

        /// <summary>
        /// Get number of item on a merchandise list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [ErsOutputHidden("g_search_mode")]
        [HtmlSubmitButton]
        public bool g_search_mode { get; set; }

       
        [ErsSchemaValidation("s_master_t.sname")]
        public string s_sname { get; set; }

   
        [ErsSchemaValidation("s_master_t.scode")]
        public string s_scode { get; set; }

       
        [ErsSchemaValidation("s_master_t.gcode")]
        public string s_gcode { get; set; }

     
        [ErsSchemaValidation("g_master_t.cate1")]
        public int? s_cate1 { get; set; }

       
        [ErsSchemaValidation("g_master_t.cate2")]
        public int? s_cate2 { get; set; }

       
        [ErsSchemaValidation("g_master_t.cate3")]
        public int? s_cate3 { get; set; }

       
        [ErsSchemaValidation("g_master_t.cate4")]
        public int? s_cate4 { get; set; }

        
        [ErsSchemaValidation("g_master_t.cate5")]
        public int? s_cate5 { get; set; }


        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.sname")]
        public string hid_s_sname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode")]
        public string hid_s_scode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.gcode")]
        public string hid_s_gcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.cate1")]
        public int? hid_s_cate1 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.cate2")]
        public int? hid_s_cate2 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.cate3")]
        public int? hid_s_cate3 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.cate4")]
        public int? hid_s_cate4 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.cate5")]
        public int? hid_s_cate5 { get; set; }

        /// <summary>
        /// input_field
        /// </summary>																												
        public List<Dictionary<string, object>> searched_items { get; set; }
        
        /// <summary>
        /// input_field
        /// </summary>		 
        public List<Dictionary<string, object>> add_items { get; private set; }

        [DisplayName("search_btn")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string search_btn { get; set; }

        public List<Dictionary<string, object>> CateList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().GetCategoryList(false,false,
                    this.s_cate1,
                    this.s_cate2,
                    this.s_cate3,
                    this.s_cate4,
                    this.s_cate5);
            }
        }

        

        
    }
}