﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using ersAdmin.Domain.StepMail.Mappables;

namespace ersAdmin.Models.stepmail
{
    public class stepmail_getitemdetail : ErsModelBase, IGetItemDetailMappable
    {
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string code { get; set; }

        
        public string code_name { get; set; }
    }
}