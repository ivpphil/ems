﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.step_scenario;
using jp.co.ivp.ers.stepmail;
using jp.co.ivp.ers.atmail;
using ersAdmin.Domain.StepMail.Mappables;
using ersAdmin.Domain.StepMail.Commands;

namespace ersAdmin.Models.stepmail
{
    public class delvy_detail
        : ErsModelBase, IDelvyDetailMappable, IDelvyDetailCommand
    {
        private const int SCENARIO_ITEMS_FIRST_DISPLAY_COUNT = 2;

        [ErsSchemaValidation("am_process_t.id")]
        public int? process_id { get; set; }

        #region "シナリオ情報"
        /// <summary>
        /// シナリオ名称
        /// </summary>
        public string scenario_name { get; private set; }

        /// <summary>
        /// ターゲット
        /// </summary>
        public string w_target { get;  set; }

        /// <summary>
        /// 対象商品
        /// </summary>
        public IList<Dictionary<string, object>> scenario_items { get;   set; }

        /// <summary>
        /// 対象商品を表示するか否か
        /// </summary>
        public bool showScenarioItems
        {
            get
            {
                if (this.scenario_items == null)
                {
                    return false;
                }

                return (scenario_items.Count() > 0);
            }
        }

        /// <summary>
        /// 対象商品
        /// </summary>
        public IEnumerable<Dictionary<string, object>> scenarioItemsFirstDisplay
        {
            get
            {
                if (this.scenario_items == null)
                {
                    yield break;
                }

                for (var index = 0; index < SCENARIO_ITEMS_FIRST_DISPLAY_COUNT; index++)
                {
                    yield return scenario_items[index];
                }
            }
        }

        /// <summary>
        /// 対象商品
        /// </summary>
        public IEnumerable<Dictionary<string, object>> scenarioItemsRemainDisplay
        {
            get
            {
                if (this.scenario_items == null)
                {
                    yield break;
                }

                for (var index = SCENARIO_ITEMS_FIRST_DISPLAY_COUNT; index < scenario_items.Count; index++)
                {
                    yield return scenario_items[index];
                }
            }
        }

        /// <summary>
        /// 対象商品
        /// </summary>
        public bool showScenarioItemsRemain
        {
            get
            {
                return scenarioItemsRemainDisplay.Count() > 0;
            }
        }

        /// <summary>
        /// 配信基準日(文字列)
        /// </summary>
        public string w_mail_ref_date_kbn { get; set; }

        /// <summary>
        /// 配信時刻（時）
        /// </summary>
        public int? mail_delv_time_hh { get; private set; }

        /// <summary>
        /// 配信時刻（分）
        /// </summary>
        public int? mail_delv_time_mm { get; private set; }

        /// <summary>
        /// 配信除外時刻From（時）
        /// </summary>
        public int? mail_delv_out_time_hh_from { get; private set; }

        /// <summary>
        /// 配信除外時刻From（分）
        /// </summary>
        public int? mail_delv_out_time_mm_from { get; private set; }

        /// <summary>
        /// 配信除外時刻To（時）
        /// </summary>
        public int? mail_delv_out_time_hh_to { get; private set; }

        /// <summary>
        /// 配信除外時刻To（分）
        /// </summary>
        public int? mail_delv_out_time_mm_to { get; private set; }

        public EnumDeliveryTime mail_delv_time_kbn { get; private set; }

        public string w_mail_delv_time_kbn { get; set; }

        #endregion

        #region "ステップメール情報"

        /// <summary>
        /// ステップメール名称
        /// </summary>
        public string step_mail_name { get; private set; }

        /// <summary>
        /// 配信タイミング(日)
        /// </summary>
        public virtual int? elapsed_num { get; private set; }

        /// <summary>
        /// 配信タイミング(区分)(文字列)
        /// </summary>
        public virtual string w_elapsed_kbn { get; set; }

        #endregion

        #region "プロセス情報"

        /// <summary>
        /// タイトル
        /// </summary>
        public string subject { get; private set; }

        /// <summary>
        /// PC用本文（テキスト）
        /// </summary>
        public string body { get; private set; }

        /// <summary>
        /// PC用本文（HTML） 
        /// </summary>
        public string html_body { get; private set; }

        /// <summary>
        /// モバイル用本文
        /// </summary>
        public string feature_body { get; private set; }

        /// <summary>
        /// 配信数
        /// </summary>
        public long mail_count { get;   set; }

        #endregion

        

        
    }
}