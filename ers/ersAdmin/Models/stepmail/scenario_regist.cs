﻿using System.Collections.Generic;
using System.ComponentModel;
using ersAdmin.Domain.StepMail.Commands;
using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models.stepmail
{
    public class scenario_regist : ErsModelBase, IScenarioRegistCommand, IScenarioRegistMappable
    {

        public bool IsInitialization { get; set; }

        [ErsSchemaValidation("step_scenario_t.id")]
        public int? id { get; set; }

        [ErsSchemaValidation("step_scenario_t.scenario_name")]
        public string scenario_name { get; set; }

        [ErsSchemaValidation("step_scenario_t.target_id")]
        public int? target_id { get; set; }

        [ErsSchemaValidation("step_scenario_t.reg_elapsed_from")]
        public int? reg_elapsed_from { get; set; }

        [ErsSchemaValidation("step_scenario_t.reg_elapsed_to")]
        public int? reg_elapsed_to { get; set; }

        [ErsSchemaValidation("step_scenario_t.mail_ref_date_kbn")]
        public EnumReferenceDate? mail_ref_date_kbn { get; set; }

        [ErsSchemaValidation("step_scenario_t.mail_delv_time_kbn")]
        public EnumDeliveryTime? mail_delv_time_kbn { get; set; }

        [ErsSchemaValidation("step_scenario_t.mail_delv_time_hh")]
        public int? mail_delv_time_hh { get; set; }

        [ErsSchemaValidation("step_scenario_t.mail_delv_time_mm")]
        public int? mail_delv_time_mm { get; set; }

        [ErsSchemaValidation("step_scenario_t.mail_delv_out_time_hh_from")]
        public int? mail_delv_out_time_hh_from { get; set; }

        [ErsSchemaValidation("step_scenario_t.mail_delv_out_time_mm_from")]
        public int? mail_delv_out_time_mm_from { get; set; }

        [ErsSchemaValidation("step_scenario_t.mail_delv_out_time_hh_to")]
        public int? mail_delv_out_time_hh_to { get; set; }

        [ErsSchemaValidation("step_scenario_t.mail_delv_out_time_mm_to")]
        public int? mail_delv_out_time_mm_to { get; set; }

        [ErsSchemaValidation("step_scenario_t.mail_from_addr")]
        public string mail_from_addr { get; set; }

        [ErsSchemaValidation("step_scenario_t.mail_from_name")]
        public string mail_from_name { get; set; }

        [ErsSchemaValidation("step_scenario_t.mail_reply_addr")]
        public string mail_reply_addr { get; set; }

        [ErsSchemaValidation("step_scenario_t.mail_status_kbn")]
        public EnumStatus? mail_status_kbn { get; set; }

        List<Dictionary<string, object>> scenario_item_list { get; set; }

        List<Dictionary<string, object>> scenario_item_list_excluded { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("step_scenario_t.scenario_name")]
        public string s_scenario_name { get; set; }			//input_field			

        [ErsOutputHidden]
        [ErsSchemaValidation("step_scenario_t.mail_status_kbn")]
        public EnumStatus? s_mail_status_kbn { get; set; }			//pulldown

        public List<Dictionary<string, object>> mail_ref_date_kbn_list
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.RefDate, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> mail_status_kbn_list
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Status, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> mail_delv_time_hh_list
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewDeliveryTimeService().GetListHour();
            }
        }

        public List<Dictionary<string, object>> mail_delv_time_mm_list
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewDeliveryTimeService().GetListMin();
            }
        }

        public List<Dictionary<string, object>> mail_delv_list
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.DeliveryTime, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> target_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewTargetService().SelectAsList(); }
        }

        [HtmlSubmitButton]
        public bool submit_register { get; set; }

        [HtmlSubmitButton]
        public bool submit_delete { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All, requireAlphabet = true)]
        public string scode { get; set; }

        [HtmlSubmitButton]
        public bool submit_update { get; set; }
    }
}