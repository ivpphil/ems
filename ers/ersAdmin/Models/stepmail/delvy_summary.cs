﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.step_scenario;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.StepMail.Mappables;
using ersAdmin.Domain.StepMail.Commands;

namespace ersAdmin.Models.stepmail
{
    public class delvy_summary
        : ErsModelBase, IDelvySummaryMappable, IDelvySummaryCommand
    {

        [ErsSchemaValidation("am_process_t.id")]
        public int? process_id { get; set; }

        /// <summary>
        /// タイトル
        /// </summary>
        public string scenario_name { get; private set; }

        /// <summary>
        /// タイトル
        /// </summary>
        public string subject { get; private set; }

        /// <summary>
        /// 配信開始日時
        /// </summary>
        public DateTime? scheduled_date { get; private set; }

        /// <summary>
        /// 最終配信日時
        /// </summary>
        public DateTime? sent_date { get; private set; }

        /// <summary>
        /// 配信数
        /// </summary>
        public long mail_count { get;  set; }

        /// <summary>
        /// 開封数
        /// </summary>
        public long mail_open_count { get; set; }

        /// <summary>
        /// 開封数
        /// </summary>
        public long mail_not_open_count
        {
            get
            {
                return mail_count - mail_open_count;
            }
        }

        /// <summary>
        /// 開封率
        /// </summary>
        public int mail_open_percent
        {
            get
            {
                if (mail_count == 0)
                {
                    return 0;
                }

                if (mail_count < mail_open_count)
                {
                    return 100;
                }

                return Convert.ToInt32(Math.Round((mail_open_count * 1.0 / mail_count) * 100, 0));
            }
        }

        /// <summary>
        /// 開封率
        /// </summary>
        public long mail_not_open_percent
        {
            get
            {
                return 100 - mail_open_percent;
            }
        }

        /// <summary>
        /// クリックフィードバック
        /// </summary>
        public List<Dictionary<string, object>> listClickCount { get; set; }


        
    }
}