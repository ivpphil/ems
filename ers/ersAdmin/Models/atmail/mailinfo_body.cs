﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using ersAdmin.Domain.Atmail.Mappables;

namespace ersAdmin.Models
{
    public class mailinfo_body : ErsModelBase, IMailinfoBodyMappable
    {
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? id { get; set; }

        public virtual EnumAmProcessStatus? status { get; set; }

        public virtual string status_desc { get; set; }

        public virtual string subject { get; protected set; }

        public virtual string body { get; set; }

        public virtual string html_body { get; protected set; }

        public virtual string feature_body { get; set; }

        public virtual DateTime? utime { get; protected set; }

        public virtual DateTime? scheduled_date { get; protected set; }

    }
}