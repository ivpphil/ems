﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.db;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using ersAdmin.Domain.Atmail.Mappables;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    /// <summary>
    /// model use for setup table
    /// </summary>
    public class mailsetup
        : ErsSiteRegisterModelBase, IMailsetupCommand, IMailsetupMappable
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        //setup_t
        [ErsOutputHidden("mail")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? id { get; set; }

        [ErsOutputHidden("mail")]
        [ErsSchemaValidation("am_setup_t.admin_email")]
        public virtual string admin_email { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("am_setup_t.p_email")]
        public virtual string p_email { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("am_setup_t.r_email")]
        public virtual string r_email { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type=CHK_TYPE.All)]
        public virtual string p_from { get; set; }

        [HtmlSubmitButton]
        public bool confirm { get; set; }

    }
}