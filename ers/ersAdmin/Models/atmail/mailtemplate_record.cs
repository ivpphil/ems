﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Models.atmail
{
    public class mailtemplate_record
        : ErsBindableModel
    {
        public virtual int? id { get; set; }
        public virtual string subject { get; set; }
        public virtual string template_name { get; set; }
        public virtual string feature_body { get; set; }
        public virtual string body { get; set; }
        public virtual string html_body { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual EnumActive? active { get; set; }
    }
}