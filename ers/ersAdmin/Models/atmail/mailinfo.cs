﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.util;
using System.Text;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.sendmail;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Atmail.Commands;
using ersAdmin.Domain.Atmail.Mappables;
using ersAdmin.jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using System.ComponentModel;

namespace ersAdmin.Models
{
    /// <summary>
    /// model use for mail delivery
    /// </summary>
    public class mailinfo
        : ErsModelBase, IMailinfoCommand, IMailinfoMappable, ITestMailCommand, ITestMailMappable
    {

        public bool IsTemplatePage2 { get; internal set; }

        [ErsOutputHidden("mail1")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? id { get; set; }

        [ErsOutputHidden("mail1")]
        [ErsSchemaValidation("am_process_t.up_kind")]
        public virtual EnumUpKind? up_kind { get; set; }

        [ErsOutputHidden("mail2")]
        [ErsSchemaValidation("am_process_t.subject")]
        public virtual string subject { get; set; }

        [ErsOutputHidden("mail2")]
        [ErsSchemaValidation("am_process_t.body")]
        public virtual string body { get; set; }
        
        [ErsOutputHidden("mail2")]
        [ErsUniversalValidation(type = CHK_TYPE.HTMLTemplates)]
        public virtual string html_body { get; set; }

        [ErsOutputHidden("mail2")]
        [ErsSchemaValidation("am_process_t.feature_body")]
        public virtual string feature_body { get; set; }
        
        [ErsOutputHidden("mail1")]
        [ErsSchemaValidation("am_process_t.scheduled_date")]
        public virtual DateTime? scheduled_date { get; set; }

        [ErsOutputHidden("mail1")]
        [ErsSchemaValidation("am_process_t.sent_date")]
        public virtual DateTime? sent_date { get; set; }

        [ErsOutputHidden("mail1")]
        [ErsSchemaValidation("am_process_t.intime")]
        public virtual DateTime? intime { get; set; }

        [ErsOutputHidden("mail1")]
        [ErsSchemaValidation("am_process_t.utime")]
        public virtual DateTime? utime { get; set; }

        [ErsOutputHidden("mail1")]
        [ErsSchemaValidation("am_process_t.active")]
        public virtual EnumActive? active { get;  set; }

        [ErsOutputHidden("mail1")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual long? mailto_sent_cnt { get;  set; }

        [ErsOutputHidden("mail1")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual long? mailto_total_cnt { get;  set; }

        [ErsOutputHidden("mail1")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual long? mailto_notsent_cnt { get;  set; }

        //[ErsOutputHidden("mail1")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string template_id { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string pageName { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public EnumAmSSet? s_set { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public string s_date { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 23)]
        public string s_hour { get; set; }

        public List<Dictionary<string, object>> templateList { get { return ErsFactory.ersViewServiceFactory.GetErsViewAmTemplateService().templateList(); } }

        [ErsSchemaValidation("am_mailto_t.email")]
        public string default_send_testmail { get; set; }

        [DisplayName("mail_from_id")]
        [ErsOutputHidden("mail2")]
        [ErsSchemaValidation("am_setup_t.id")]
        public virtual int? setup_id { get; set; }

        public virtual string w_setup_id
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewAmSetupService().GetStringFromId(this.setup_id);
            }
        }

        public virtual List<Dictionary<string, object>> setup_list
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewAmSetupService().SelectAsList();
            }
        }
    }
}