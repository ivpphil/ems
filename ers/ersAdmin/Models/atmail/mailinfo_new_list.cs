﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;
using System.Text;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.db;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using ersAdmin.Domain.Atmail.Commands;
using ersAdmin.Domain.Atmail.Mappables;

namespace ersAdmin.Models
{
    /// <summary>
    /// model use for new list copy from process record
    /// </summary>
    public class mailinfo_new_list
        : ErsModelBase, IMailinfoNewListCommand, IMailinfoNewListMappable
    {

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? id { get; set; }

        public virtual long? mailto_total_cnt { get; set; }

        public virtual long? mailto_sent_cnt { get; protected set; }

        public virtual long? mailto_notsent_cnt { get; protected set; }

        public virtual DateTime? utime { get; protected set; }

        public virtual string subject { get; protected set; }
        
    }
}