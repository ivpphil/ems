﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using ersAdmin.Domain.Atmail.Mappables;

namespace ersAdmin.Models
{
    /// <summary>
    /// model use for downloading csv file
    /// </summary>
    public class mailinfo_download
        : ErsModelBase, IMailinfoDownloadMappable
    {

        public bool IsDownloadCompletionPage { get; internal set; }

        public ErsCsvCreater csvCreater { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? id { get; set; }

        public virtual EnumUpKind? up_kind { get; protected set; }

        public virtual EnumAmProcessStatus? status { get; protected set; }

        public virtual string subject { get; protected set; }

        public virtual string body { get; protected set; }

        public virtual string html_body { get; protected set; }

        public virtual string feature_body { get; protected set; }

        public virtual DateTime? scheduled_date { get; protected set; }

        public virtual DateTime? sent_date { get; protected set; }

        public virtual DateTime? intime { get; protected set; }

        public virtual DateTime? utime { get; protected set; }

        public virtual EnumActive? active { get; protected set; }

        public virtual long? mailto_sent_cnt { get; protected set; }

        public virtual long? mailto_total_cnt { get; set; }

        public virtual long? mailto_notsent_cnt { get; protected set; }
    }
}