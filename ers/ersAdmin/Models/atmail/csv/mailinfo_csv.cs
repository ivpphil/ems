﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.ComponentModel;

namespace ersAdmin.Models.csv
{
    /// <summary>
    /// use to save the record on download csv file
    /// </summary>
    public class mailinfo_csv : ErsModelBase
    {
        [CsvField]
        public virtual int id { get; set; }

        [CsvField]
        public virtual string mcode { get; set; }

        [CsvField]
        public virtual string email { get; set; }

        [CsvField]
        public virtual string lname { get; set; }

        [CsvField]
        public virtual string fname { get; set; }

        [CsvField]
        public virtual string etc1 { get; set; }

        [CsvField]
        public virtual string etc2 { get; set; }

    }
}
