﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.validation;
using ersAdmin.Domain.Atmail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace ersAdmin.Models.csv
{
    /// <summary>
    /// use for getting the information from csv file
    /// </summary>
    public class mailto_csv_upload
        : ErsBindableModel
    {
        [CsvField]
        [ErsSchemaValidation("am_mailto_t.mcode")]
        public virtual string mcode { get; set; }

        [CsvField]
        [ErsSchemaValidation("am_mailto_t.email")]
        public virtual string email { get; set; }

        [CsvField]
        [ErsSchemaValidation("am_mailto_t.lname")]
        public virtual string lname { get; set; }

        [CsvField]
        [ErsSchemaValidation("am_mailto_t.fname")]
        public virtual string fname { get; set; }

        [CsvField]
        [ErsSchemaValidation("am_mailto_t.etc1")]
        public virtual string etc1 { get; set; }

        [CsvField]
        [ErsSchemaValidation("am_mailto_t.etc2")]
        public virtual string etc2 { get; set; }
    }
}