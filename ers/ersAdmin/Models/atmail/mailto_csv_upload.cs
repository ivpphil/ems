﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using ersAdmin.Domain.Atmail.Commands;
using ersAdmin.Domain.Atmail.Mappables;

namespace ersAdmin.Models
{
    /// <summary>
    /// use for new list upload csv file
    /// </summary>
    public class mailto_csv_upload
        : ErsModelBase, IMailtoCSVUploadCommand
    {
        [HtmlSubmitButton]
        public bool upload { get; set; }

        [HtmlSubmitButton]
        public bool regist { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? id { get; set; }

        public bool chk_find { get; internal set; }

        public int upload_item_number
        {
            get
            {
                if (this.csv_file != null)
                    return this.csv_file.validIndexes.Count();

                return 0;
            }

        }

        /// <summary>
        /// アップロードデータ
        /// <para>Upload data</para>
        /// </summary>
        [ErsOutputHidden]
        [BindCsvFile]
        public ErsCsvContainer<csv.mailto_csv_upload> csv_file { get; set; }

    }
}