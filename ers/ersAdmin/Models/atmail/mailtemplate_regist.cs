﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using jp.co.ivp.ers.atmail;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Atmail.Mappables;
using ersAdmin.Domain.Atmail.Commands;

namespace ersAdmin.Models
{
    public class mailtemplate_regist
        : ErsModelBase, IMailtemplateRegistCommand, IMailtemplateRegistMappable
    {

        public bool IsCompletionPage { get; set; }

        //template_t
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? id { get; set; }

        [ErsSchemaValidation("am_template_t.subject")]
        public virtual string subject { get; set; }

        [ErsSchemaValidation("am_template_t.feature_body")]
        public virtual string feature_body { get; set; }

        [ErsSchemaValidation("am_template_t.template_name")]
        public virtual string template_name { get; set; }


        [ErsSchemaValidation("am_template_t.body")]
        public virtual string body { get; set; }

        [ErsSchemaValidation("am_template_t.html_body")]
        public virtual string html_body { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("am_template_t.intime")]
        public virtual DateTime? intime { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("am_template_t.utime")]
        public virtual DateTime? utime { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("am_template_t.active")]
        public virtual EnumActive? active { get; set; }

        [HtmlSubmitButton]
        public bool modify { get; set; }

        //update html body that uses <ers:redirectURL>
        public void updateHTMLbody(string strBody)
        {
            if (strBody != null)
            {
                string updateBody = string.Empty;
                updateBody = strBody.Replace("&lt;", "<");
                updateBody = updateBody.Replace("&gt;", ">");
                this.html_body = updateBody;
            }
        }
    }
}