﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.db;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using ersAdmin.Domain.Atmail.Commands;
using ersAdmin.Domain.Atmail.Mappables;

namespace ersAdmin.Models
{
    public class mailinfo_delete
        : ErsModelBase, IMailinfoDeleteCommand, IMailinfoDeleteMappable
    {

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? id { get; set; }

        public virtual DateTime? utime { get; protected set; }

        public virtual DateTime? scheduled_date { get; protected set; }

        public virtual long? mailto_sent_cnt { get; set; }

        public virtual long? mailto_total_cnt { get; set; }

        public virtual long? mailto_notsent_cnt { get; set; }
    }
}