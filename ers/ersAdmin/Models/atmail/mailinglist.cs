﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.atmail.process;
using jp.co.ivp.ers;
using ersAdmin.Domain.Atmail.Mappables;

namespace ersAdmin.Models
{
    /// <summary>
    /// use for loading the list of process records
    /// </summary>
    public class mailinglist
        : ErsModelBase, IMailinglistMappable, IMailinglistTotalMappable
    {

        public ErsPagerModel pager { get; internal set; }        

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        /// <summary>
        /// Get number of item on a merchandise list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        public long recordCount { get; set; }

        public List<Dictionary<string, object>> list { get; set; }

        public DateTime thisMnthTo { get; set; }
        public DateTime thisMnthFrm { get; set; }
        public DateTime lstMnthTo { get; set; }
        public DateTime lstMnthFrm { get; set; }
        public DateTime this25To { get; set; }
        public DateTime this25Frm { get; set; }
        public DateTime lst25To { get; set; }
        public DateTime lst25Frm { get; set; }

        public long thisMnthCnt { get; set; }
        public long thisMnthNotCnt { get; set; }
        public long lstMnthCnt { get; set; }
        public long lstMnthNotCnt { get; set; }
        public long this25Cnt { get; set; }
        public long this25NotCnt { get; set; }
        public long lst25Cnt { get; set; }
        public long lst25NotCnt { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? sort { get; set; }

        public int? next_sort
        {
            get
            {
                if (sort == 0)
                {
                    return 1;
                }

                return 0;
            }
        }
    }
}