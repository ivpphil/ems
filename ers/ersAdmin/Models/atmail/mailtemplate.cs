﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.db;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using ersAdmin.Models.atmail;
using ersAdmin.Domain.Atmail.Mappables;

namespace ersAdmin.Models
{
    public class mailtemplate : ErsModelBase, IMailTemplateMappable
    {
        public long recordCount { get; set; }

        public ErsPagerModel pager { get; internal set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        /// <summary>
        /// Get number of template list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        [BindTable("templateList")]
        public IList<mailtemplate_record> templateList { get; set; }   
       
    }
}