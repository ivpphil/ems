﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Lp.Mappables;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;
using ersAdmin.Domain.Lp.Commands;

namespace ersAdmin.Models.lp
{
    public class lp_page_regist
        : ErsSiteSearchModelBase, ILpPageRegistTemplateMappable, ILpPageRegistMappable, ILpPageRegistCommand, ILpPageDeleteCommand
    {
        public bool IsInitialize { get; set; }

        public bool hasRegistered { get; set; }

        [HtmlSubmitButton]
        public bool modAdd { get; set; }

        public string template_file_path { get; set; }

        public string template_img_file_path { get; set; }

        [ErsOutputHidden("common", "template")]
        [ErsSchemaValidation("lp_page_t.id")]
        public int? lp_page_id { get; set; }

        [ErsOutputHidden("common", "template")]
        [ErsSchemaValidation("lp_page_t.lp_page_manage_id")]
        public int? lp_page_manage_id { get; set; }

        [ErsOutputHidden("common", "template")]
        [ErsSchemaValidation("lp_page_type_t.page_type_code")]
        public EnumLpPageTypeCode page_type_code { get; set; }

        [ErsOutputHidden("template")]
        [ErsSchemaValidation("lp_template_t.template_code")]
        public string template_code { get; set; }

        public List<ErsLpTemplate> template { get; set; }

        [BindTable("listBlock")]
        public List<lp_page_regist_block> listBlock { get; set; }

        [BindPicture(new[] {EnumPictureType.JPEG,EnumPictureType.PNG})]
        public HttpPostedFileBase upsell_button_1_file { get; set; }

        [ErsOutputHidden("template")]
        [ErsSchemaValidation("lp_page_t.upsell_button_1_file_name")]
        public string upsell_button_1_file_name { get; set; }

        [BindPicture(new[] { EnumPictureType.JPEG, EnumPictureType.PNG })]
        public HttpPostedFileBase upsell_button_2_file { get; set; }

        [ErsOutputHidden("template")]
        [ErsSchemaValidation("lp_page_t.upsell_button_2_file_name")]
        public string upsell_button_2_file_name { get; set; }

        [BindPicture(new[] { EnumPictureType.JPEG, EnumPictureType.PNG })]
        public HttpPostedFileBase upsell_button_3_file { get; set; }

        [ErsOutputHidden("template")]
        [ErsSchemaValidation("lp_page_t.upsell_button_3_file_name")]
        public string upsell_button_3_file_name { get; set; }

        [ErsSchemaValidation("lp_template_t.upsell_button_1_use_flg")]
        public EnumCmsFieldType? upsell_button_1_use_flg { get; set; }

        [ErsSchemaValidation("lp_template_t.upsell_button_2_use_flg")]
        public EnumCmsFieldType? upsell_button_2_use_flg { get; set; }

        [ErsSchemaValidation("lp_template_t.upsell_button_3_use_flg")]
        public EnumCmsFieldType? upsell_button_3_use_flg { get; set; }

        [ErsUniversalValidation(type=CHK_TYPE.Numeric)]
        public int? del_upsell_button_2_flg { get; set; }

        [ErsUniversalValidation(type=CHK_TYPE.Numeric)]
        public int? del_upsell_button_3_flg { get; set; }


        public bool is_multiple_site
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.Multiple_sites;
            }
        }

        public override EnumMallShopKbn? s_mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }
    }
}