﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.lp;
using ersAdmin.Domain.Lp.Mappables;

namespace ersAdmin.Models.lp
{
    public class lp_search
        : ErsSiteSearchModelBase, ILandingListMappable
    {
        /// <summary>
        /// 検索用モールショップ区分 [Mall shop division for search]
        /// </summary>
        public override EnumMallShopKbn? s_mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        public ErsPagerModel pager { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        /// <summary>
        /// Get number of item on a merchandise list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        [ErsOutputHidden]
        [ErsUniversalValidation(rangeFrom = 0, rangeTo = 3, type = CHK_TYPE.Numeric)]
        public int? sort { get; set; }

        [ErsOutputHidden("input", "lpregist_base", "lpregist_detail", "lpregist_questionnaire")]
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 255)]
        public string s_freeword { get; set; }

        [ErsOutputHidden("input", "lpregist_base", "lpregist_detail", "lpregist_questionnaire")]
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 255)]
        public string s_group_name { get; set; }

        public IList<Dictionary<string, object>> list { get; set; }

        public long recordCount { get; set; }

        public IList<Dictionary<string, object>> lpGroupNameList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsLpPageManageViewService().GetLpGroupName(); }
        }
    }
}