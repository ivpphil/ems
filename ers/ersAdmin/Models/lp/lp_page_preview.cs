﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.lp;
using ersAdmin.Domain.Lp.Mappables;
using jp.co.ivp.ers;
using System.ComponentModel;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models.lp
{
    public class lp_page_preview
        : lp_page_regist, ILpPreviewPageMappable
    {
        public lp_regist objLpRegist { get; internal set; }

        public string partial_preview_page { get; set; }

        #region Landing Page Model Properties

        public string page_title
        {
            get
            {
                if (this.objLpRegist != null)
                    return this.objLpRegist.page_title;

                return string.Empty;
            }
        }

        public string logo_image_file { get; set; }

        public ErsLpPage objLpPage { get; set; }

        public Dictionary<string, object> item_code_name { get; set; }
        public Dictionary<string, object> item_code_lname { get; set; }
        public Dictionary<string, object> item_code_email { get; set; }
        public Dictionary<string, object> item_code_email_confirm { get; set; }
        public Dictionary<string, object> item_code_tel { get; set; }
        public Dictionary<string, object> item_code_fax { get; set; }
        public Dictionary<string, object> item_code_zip { get; set; }
        public Dictionary<string, object> item_code_pref { get; set; }
        public Dictionary<string, object> item_code_address { get; set; }
        public Dictionary<string, object> item_code_taddress { get; set; }
        public Dictionary<string, object> item_code_maddress { get; set; }
        public Dictionary<string, object> item_code_birth { get; set; }
        public Dictionary<string, object> item_code_monitor { get; set; }

        public List<QuestionnaireRecord> questionnaireDetailItems { get; set; }

        public string upsell_sname { get; set; }

        public string sname { get; set; }

        public bool IsLandingPage
        {
            get
            {
                return !(this.page_type_code == EnumLpPageTypeCode.Upsell);
            }
        }

        public EnumLpUpsellStgy? upsell_stgy_kbn
        {
            get
            {
                if (this.objLpRegist != null)
                    return objLpRegist.upsell_stgy_kbn;

                return null;
            }
        }

        public virtual string pri_memo
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return setup.pri_memo;
            }
        }

        public string personal_info_url
        {
            get
            {
                if (this.objLpRegist != null)
                    return this.objLpRegist.personal_info_url;

                return string.Empty;
            }
        }

        public EnumUse? personal_info_kbn
        {
            get
            {
                if (this.objLpRegist != null)
                    return (EnumUse)(int)this.objLpRegist.personal_info_kbn;

                return null;
            }
        }


        public bool HasUpSellRegistered { set; get; }


        #endregion


        public bool temp_upsell_button_1_file_name { set; get; }

        public bool temp_upsell_button_2_file_name { set; get; }

        public bool temp_upsell_button_3_file_name { set; get; }

        public bool temp_logo_image_file { get; set; }

        public EnumSiteType siteType 
        { 
            get 
            { 
                return EnumSiteType.ADMIN; 
            }
        }

        [DisplayName("site_t.id")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0)]
        public int? site_id_preview { get; set; }
        
    }
}