﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using ersAdmin.Domain.Lp.Commands;
using ersAdmin.Domain.Lp.Mappables;
using System.ComponentModel;

namespace ersAdmin.Models.lp
{
    public class download_csv_lp_dquest
        : ErsModelBase, IDownloadCsvLpDQuestCommand, IDownloadCsvLpDQuestMappable
    {
        public download_csv_lp_dquest()
        {
            this.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
        }

        public ErsCsvCreater csvCreater { get; set; }

        [DisplayName("lp_page_manage_t.id")]
        [ErsSchemaValidation("lp_page_manage_t.id")]
        public virtual int? page_id { get; set; }
    }
}