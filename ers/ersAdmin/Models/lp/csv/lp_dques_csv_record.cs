﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;

namespace ersAdmin.Models.csv
{
    public class lp_dques_csv_record
        : ErsBindableModel
    {
        [CsvField]
        [DisplayName("lp_page_manage_t.ccode")]
        public virtual string page_id { get; set; }

        [CsvField]
        public virtual string lp_group_name { get; set; }

        [CsvField]
        public virtual string d_no { get; set; }

        [CsvField]
        public virtual string item_name { get; set; }

        [CsvField]
        [DisplayName("d_questionnaire_t.value")]
        public virtual string value { get; set; }
    }
}