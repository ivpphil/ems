﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using ersAdmin.Domain.Lp.Commands;

namespace ersAdmin.Models.lp
{
    public class lp_page_regist_block
        : ErsBindableModel
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("lp_page_t.block_1_head")]
        [DisplayName("block_head")]
        public string head { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("lp_page_t.block_1_body")]
        [DisplayName("block_body")]
        public string body { get; set; }

        [ErsOutputHidden]
        [DisplayName("block_free")]
        [ErsSchemaValidation("lp_page_t.block_1_free")]
        public string free { get; set; }

        public string head_name { get; set; }

        public string body_name { get; set; }

        public string free_name { get; set; }

        public EnumCmsFieldType? head_use_flg { get; set; }

        public EnumCmsFieldType? body_use_flg { get; set; }

        public EnumCmsFieldType? free_use_flg { get; set; }
    }
}