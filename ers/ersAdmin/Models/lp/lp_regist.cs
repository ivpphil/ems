﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.mvc.pager;
using System.IO;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.Send;
using ersAdmin.Models.lp;
using jp.co.ivp.ers.merchandise;
using ersAdmin.Domain.Lp.Mappables;
using ersAdmin.Domain.Lp.Commands;



namespace ersAdmin.Models.lp
{
    public class lp_regist
        : ErsSiteRegisterModelBase, ILpRegistMappable, ILpRegistCommand, ILpModifyMappable, ILpModifyCommand, ILpDeleteCommand, ILandingListCopyCommand
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        public bool IsSaveTempImage { get; set; }

        [HtmlSubmitButton]
        public bool lpregist_base { get; set; }

        [HtmlSubmitButton]
        public bool lpregist_detail { get; set; }

        [HtmlSubmitButton]
        public bool lpregist_questionnaire { get; set; }

        [HtmlSubmitButton]
        public bool lpregist_confirm { get; set; }

        [HtmlSubmitButton]
        public bool IsConfirmationPage { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_detail", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.id")]
        public int? id { get; set; }

        [ErsOutputHidden("lpregist_detail", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.lp_group_name")]
        public string lp_group_name { get; set; }

        [ErsOutputHidden("lpregist_detail", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.ccode")]
        public string ccode { get; set; }

        [ErsOutputHidden("lpregist_detail", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.page_name")]
        public string page_name { get; set; }

        [ErsOutputHidden("lpregist_detail", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.page_title")]
        public string page_title { get; set; }

        [BindPicture(EnumPictureType.GIF, EnumPictureType.JPEG, EnumPictureType.PNG)]
        public HttpPostedFileBase logo_image { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_detail", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.logo_image_file")]
        public string logo_image_file { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_detail", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.logo_image_file")]
        public string old_logo_image_file { get; set; }

        [ErsOutputHidden("lpregist_detail", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.public_st_date")]
        public DateTime? public_st_date { get; set; }

        [ErsOutputHidden("lpregist_detail", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.public_ed_date")]
        public DateTime? public_ed_date { get; set; }

        [ErsOutputHidden("lpregist_detail", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.active")]
        public EnumActive? active { get; set; }


        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.basic_stgy_kbn")]
        public EnumLpBasicStgy? basic_stgy_kbn { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.upsell_stgy_kbn")]
        public EnumLpUpsellStgy? upsell_stgy_kbn { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.buy_limit_kbn")]
        public EnumLpBuyLimit? buy_limit_kbn { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.sell_scode")]
        public string sell_scode { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.sell_order_type")]
        public EnumOrderType? sell_order_type { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.sell_price")]
        public int? sell_price { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.sell_discount_flg")]
        public EnumUse sell_discount_flg { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.sell_discount_amount")]
        public int? sell_discount_amount { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.sell_discount_price")]
        public int? sell_discount_price { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.sell_upsell_price")]
        public int? sell_upsell_price { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.sell_first_regular_price")]
        public int? sell_first_regular_price { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.sell_regular_price")]
        public int? sell_regular_price { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.sell_max_amount")]
        public int? sell_max_amount { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.sell_max_stock")]
        public int? sell_max_stock { get; set; }


        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.upsell_scode")]
        public string upsell_scode { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.upsell_order_type")]
        public EnumOrderType? upsell_order_type { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.upsell_price")]
        public int? upsell_price { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.upsell_discount_flg")]
        public EnumUse upsell_discount_flg { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.upsell_discount_amount")]
        public int? upsell_discount_amount { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.upsell_discount_price")]
        public int? upsell_discount_price { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.upsell_first_regular_price")]
        public int? upsell_first_regular_price { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.upsell_regular_price")]
        public int? upsell_regular_price { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.upsell_max_amount")]
        public int? upsell_max_amount { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.upsell_max_stock")]
        public int? upsell_max_stock { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.coupon_flg")]
        public EnumUse coupon_flg { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.coupon_code")]
        public string coupon_code { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.carriage_free_flg")]
        public EnumUse carriage_free_flg { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_page_manage_t.carriage_free_price")]
        public int? carriage_free_price { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_detail")]
        [BindTable("listQuestionnaire")]
        public List<QuestionnaireRecord> listQuestionnaire { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_detail")]
        [ErsSchemaValidation("lp_page_manage_t.personal_info_kbn")]
        public EnumLpPersonalInfo? personal_info_kbn { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_detail")]
        [ErsSchemaValidation("lp_page_manage_t.personal_info_url")]
        public string personal_info_url { get; set; }

        public string w_basic_stgy_kbn { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.LpBasicStgy, EnumCommonNameColumnName.namename, (int?)this.basic_stgy_kbn); } }

        public string w_upsell_stgy_kbn { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.LpUpsellStgy, EnumCommonNameColumnName.namename, (int?)this.upsell_stgy_kbn); } }

        public string w_buy_limit_kbn { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.LpBuyLimit, EnumCommonNameColumnName.namename, (int?)this.buy_limit_kbn); } }

        public string w_sell_order_type { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.OrderType, EnumCommonNameColumnName.namename, (int?)this.sell_order_type); } }

        public string w_sell_discount_flg { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Use, EnumCommonNameColumnName.namename, (int?)this.sell_discount_flg); } }

        public string w_upsell_order_type { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.OrderType, EnumCommonNameColumnName.namename, (int?)this.upsell_order_type); } }

        public string w_upsell_discount_flg { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Use, EnumCommonNameColumnName.namename, (int?)this.upsell_discount_flg); } }

        public string w_coupon_flg { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Use, EnumCommonNameColumnName.namename, (int?)this.coupon_flg); } }

        public string w_carriage_free_flg { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Use, EnumCommonNameColumnName.namename, (int?)this.carriage_free_flg); } }

        public string w_personal_info_kbn { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.LpPersonalInfo, EnumCommonNameColumnName.namename, (int?)this.personal_info_kbn); } }

        public string w_active { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Active, EnumCommonNameColumnName.namename, (int?)this.active); } }

        public string confirm_active_name
        {
            get
            {
                var active_value = this.active == null ? EnumActive.NonActive : this.active;
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Active, EnumCommonNameColumnName.namename, (int?)active_value);
            }
        }

        public string sell_sname
        {
            get
            {
                var objSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(this.sell_scode);
                if (objSku == null)
                {
                    return null;
                }
                return objSku.sname;
            }
        }

        public string upsell_sname
        {
            get
            {
                var objSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(this.upsell_scode);
                if (objSku == null)
                {
                    return null;
                }
                return objSku.sname;
            }
        }

        public IList<Dictionary<string, object>> lp_group_name_list { get { return ErsFactory.ersViewServiceFactory.GetErsLpPageManageViewService().GetLpGroupName(); } }

        public IList<Dictionary<string, object>> basic_stgy_kbnList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.LpBasicStgy, EnumCommonNameColumnName.namename); } }

        public IList<Dictionary<string, object>> upsell_stgy_kbnList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.LpUpsellStgy, EnumCommonNameColumnName.namename); } }

        public IList<Dictionary<string, object>> buy_limit_kbnList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.LpBuyLimit, EnumCommonNameColumnName.namename); } }

        public IList<Dictionary<string, object>> salePtnList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.OrderType, EnumCommonNameColumnName.namename); } }

        public IList<Dictionary<string, object>> use_flgList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Use, EnumCommonNameColumnName.namename); } }

        public IList<Dictionary<string, object>> personal_info_kbnList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.LpPersonalInfo, EnumCommonNameColumnName.namename); } }

        public IList<Dictionary<string, object>> listLpPageType { get; set; }
   }
}

