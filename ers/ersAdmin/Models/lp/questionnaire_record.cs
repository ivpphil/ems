﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using ersAdmin.Domain.Lp.Commands;

namespace ersAdmin.Models.lp
{
    public class QuestionnaireRecord
        : ErsBindableModel, ILpRegistQuestionnaireRecordCommand
    {
        [BindTarget("confirm")]
        public string item_name { get; set; }

        [BindTarget("confirm")]
        public string template_name { get; set; }

        [BindTarget("confirm")]
        public string validation_comment { get; set; }

        [BindTarget("confirm")]
        public EnumCmsFieldType? is_system_required { get; set; }

        /// <summary>
        /// For Front Preview Page
        /// </summary>
        public string path_template_name { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_detail", "lpregist_questionnaire")]
        [ErsSchemaValidation("lp_questionnaire_t.item_code")]
        public string item_code { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_detail")]
        [ErsSchemaValidation("lp_questionnaire_t.is_used")]
        public EnumUse is_used { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_detail")]
        [ErsSchemaValidation("lp_questionnaire_t.is_required")]
        public EnumRequired is_required { get; set; }

        [ErsOutputHidden("lpregist_base", "lpregist_detail")]
        [ErsSchemaValidation("lp_questionnaire_t.item_note")]
        public string item_note { get; set; }

        public string w_is_used { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Use, EnumCommonNameColumnName.opt_chr1, (int?)this.is_used); } }

        public string w_is_required { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Required, EnumCommonNameColumnName.namename, (int?)this.is_required); } }
    }
}