﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System.ComponentModel;
using ersAdmin.Domain.Price.Commands;
using jp.co.ivp.ers;


namespace ersAdmin.Models.csv
{
    public class Price_csv_record
        : ErsBindableModel, IPriceCsvRecordCommand
    {
        [CsvField]
        [ErsSchemaValidation("price_t.gcode", requireAlphabet = true)]
        public virtual string gcode { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.scode", requireAlphabet = true)]
        public virtual string scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.price_kbn")]
        public virtual EnumPriceKbn? price_kbn { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.member_rank")]
        public virtual int? member_rank { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.date_from")]
        [DisplayName("price_t.date_from")]
        public virtual DateTime? date_from { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.date_to")]
        [DisplayName("price_t.date_to")]
        public virtual DateTime? date_to { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.price")]
        public virtual int? price { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.price2")]
        public virtual int? price2 { get; set; }
    }
}
