﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Price.Commands;
using ersAdmin.Domain.Price.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Models
{
    public class Price_csv
        : ErsModelBase, IPriceCsvCommand, IPriceCsvMappable
    {
        /// <summary>
        /// Gets display message on finish page.
        /// </summary>
        public virtual string resultMsg
        {
            get
            {
                if (this.csv_file == null)
                {
                    return string.Empty;
                }
                return ErsResources.GetMessage("30000", this.csv_file.validIndexes.Count());
            }
        }

        /// <summary>
        /// 1行目をスキップする場合はtrue
        /// <para>If you want to skip the first line, value must be true</para>
        /// </summary>
        [ErsOutputHidden]
        [HtmlSubmitButton]
        public virtual bool chk_find { get; set; }

        /// <summary>
        /// アップロードデータ
        /// <para>Upload data</para>
        /// </summary>
        [ErsOutputHidden]
        [BindCsvFile]
        public ErsCsvContainer<csv.Price_csv_record> csv_file { get; set; }

        /// <summary>
        /// 登録ボタンの押下
        /// </summary>
        [HtmlSubmitButton]
        public virtual bool regist { get; set; }

        public bool allCsvFieldsInvalid
        {
            get
            {
                if (this.csv_file == null)
                    return true;

                return (this.csv_file.validIndexes.Count() == 0);
            }
        }
    }
}