﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using ersAdmin.Domain.Price.Commands;
using ersAdmin.Domain.Price.Mappables;
using ersAdmin.jp.co.ivp.ers;
using jp.co.ivp.ers; 

namespace ersAdmin.Models{

    /// <summary>
    /// 価格クラス
    /// </summary>
    public class Price_list
        : ErsModelBase, IPriceListCommand, IPriceListMappable, IPriceListCSVMappable
    {
        public ErsCsvCreater csvCreater { get; set; }

        public ErsPagerModel pager { get; set; }

        public Price_list()
        {
            this.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
        }

        public virtual long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        [ErsOutputHidden]
        public long recordCount { get; set; }

        public IList<Dictionary<string, object>> price_list { get; set; }

        /// グループコード
        [ErsSchemaValidation("price_t.gcode")]
        [ErsOutputHidden]
        public string s_gcode { get; set; }

        /// 商品番号
        [ErsSchemaValidation("price_t.scode")]
        [ErsOutputHidden]
        public string s_scode { get; set; }

        /// インストアコード
        [ErsSchemaValidation("g_master_t.gname")]
        [ErsOutputHidden]
        public string s_gname { get; set; }

        //商品名
        [ErsSchemaValidation("s_master_t.sname")]
        [ErsOutputHidden]
        public string s_sname { get; set; }

        //商品名
        [ErsUniversalValidation(type=CHK_TYPE.OneByteCharacter)]
        [ErsOutputHidden]
        public EnumSearchType? price_search_type { get; set; }
    }

}