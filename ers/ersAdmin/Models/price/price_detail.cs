﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Price.Commands;
using ersAdmin.Domain.Price.Mappables;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers;
using ersAdmin.jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;

namespace ersAdmin.Models.price
{
    public class PriceDetail
        : ErsModelBase, 
        IPriceDetailCommand, IPriceDetailMappable,
        IPriceDetailListCommand, IPriceDetailListMappable,
        IPriceMemberRankCommand, IPriceMemberRankMappable
    {
        //商品名
        [ErsUniversalValidation(type=CHK_TYPE.OneByteCharacter)]
        [ErsOutputHidden]
        public EnumSearchType? price_search_type { get; set; }
        
        public string sname { get; set; }

        public string gname { get; set; }

        [ErsSchemaValidation("s_master_t.gcode", requireAlphabet = true)]
        [ErsOutputHidden("back_to_detail")]
        public string gcode { get; set; }

        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]
        [ErsOutputHidden("back_to_detail")]
        public string scode { get; set; }

        [ErsSchemaValidation("price_t.price")]
        [DisplayName("normal_price")]
        public int? price { get; set; }

        [ErsSchemaValidation("price_t.price2")]
        public int? price2 { get; set; }

        [ErsSchemaValidation("price_t.price")]
        [DisplayName("regular_price")]
        public int? regular_price { get; set; }

        [ErsSchemaValidation("price_t.price")]
        [DisplayName("regular_first_price")]
        public int? regular_first_price { get; set; }

        [ErsSchemaValidation("price_t.cost_price")]
        public int? cost_price { get; set; }

        [ErsSchemaValidation("s_master_t.supplier_code", requireAlphabet = true)]
        public string supplier_code { get; set; }

        [ErsSchemaValidation("s_master_t.wh_order_type")]
        public EnumWhOrderType wh_order_type { get; set; }

        [BindTable("member_rank_record")]
        public List<PriceDetailRankRecord> member_rank_record { get; set; }

        [BindTable("price_table")]
        public List<PriceDetailRecord> price_table { get; set; }

        [ErsSchemaValidation("g_master_t.s_sale_ptn")]
        [ErsOutputHidden]
        public EnumSalePatternType? s_sale_ptn { get; set; }

        public IList<ErsSku> productRelatedList { get; set; }

        public string w_sale_ptn { get { return ErsFactory.ersViewServiceFactory.GetErsViewSalePtnService().GetStringFromId(this.s_sale_ptn); } }
    }
}