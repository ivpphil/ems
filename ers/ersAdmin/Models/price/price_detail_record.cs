﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Price.Commands;
using System.ComponentModel;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Models.price
{
    public class PriceDetailRecord
        : ErsBindableModel, IPriceDetailListRecordCommand
    {
        public override string lineName
        {
            get
            {
                return ErsResources.GetFieldName("price_table") + " " + base.lineName;
            }
        }

        [HtmlSubmitButton]
        public bool delete { get; set; }

        [ErsSchemaValidation("price_t.id")]
        [ErsOutputHidden]
        public int? id { get; set; }

        [ErsSchemaValidation("price_t.gcode", requireAlphabet = true)]
        public string gcode { get; set; }

        [ErsSchemaValidation("price_t.scode", requireAlphabet = true)]
        public string scode { get; set; }

        [ErsSchemaValidation("price_t.date_from")]
        public DateTime? date_from { get; set; }

        [ErsSchemaValidation("price_t.date_to")]
        public DateTime? date_to { get; set; }

        [ErsSchemaValidation("price_t.price")]
        [DisplayName("sale_price")]
        public int? price { get; set; }

        #region REMOVE FIELDS REGARDS ON ADDING ROWS USING JAVASCRIPT

        /// Prefix rule remove_{property}
       
        [HtmlSubmitButton]
        public bool remove_delete { get; set; }

        #endregion
    }
}