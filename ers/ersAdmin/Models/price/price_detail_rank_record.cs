﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Price.Commands;
using ersAdmin.Domain.Price.Mappables;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System.ComponentModel;

namespace ersAdmin.Models.price
{
    public class PriceDetailRankRecord
        : ErsBindableModel, IPriceMemberRankRecordCommand
    {
        public override string lineName
        {
            get
            {
                return this.member_rank_name;
            }
        }

        [ErsSchemaValidation("price_t.member_rank")]
        [ErsOutputHidden]
        public int? member_rank { get; set; }

        [ErsSchemaValidation("price_t.price")]
        [DisplayName("normal_price")]
        public int? price { get; set; }

        [ErsSchemaValidation("price_t.price")]
        [DisplayName("regular_price")]
        public int? regular_price { get; set; }

        [ErsSchemaValidation("price_t.price")]
        [DisplayName("regular_first_price")]
        public int? regular_first_price { get; set; }

        [ErsSchemaValidation("common_namecode_t.namename")]
        [ErsOutputHidden]
        public string member_rank_name { get; set; }
    }
}