﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Models.customer
{
    public class Cus_Crderr_Search
        : ErsSiteSearchModelBase, ICusCrderrSearchMappable, ICusCrderrCSVMappable
    {
        /// <summary>
        /// ポイント検索用モールショップ区分 [Mall shop division for point search]
        /// </summary>
        public override EnumMallShopKbn? s_mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        public ErsPagerModel pager { get; set; }

        public ErsCsvCreater csvCreater { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        /// <summary>
        /// 顧客検索条件
        /// </summary>
        /// 
        [ErsOutputHidden]
        [ErsSchemaValidation("regular_error_t.occured_date")]
        public DateTime? src_occured_date_from { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("regular_error_t.occured_date")]
        public DateTime? src_occured_date_to { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.email")]
        public string src_email { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tel")]
        public string src_tel { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.lname")]
        public string src_lname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fname")]
        public string src_fname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.lnamek")]
        public string src_lnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fnamek")]
        public string src_fnamek { get; set; }

        [ErsOutputHidden]
        [DisplayName("regular_order_number")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? src_regular_order_number { get; set; }

        public string err_num { get; set; }

        public string err_mess { get; set; }

        /// <summary>
        /// 顧客リスト
        /// </summary>
        public List<Dictionary<string, object>> list { get; set; }

    }
}