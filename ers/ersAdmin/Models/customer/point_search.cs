﻿using System;
using System.Collections.Generic;
using ersAdmin.Domain.Customer.Commands;
using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using System.ComponentModel;
using jp.co.ivp.ers.mall;

namespace ersAdmin.Models
{
    public class Point_search
         : ErsModelBase, IPointSearchCommand, IPointSearchMappable
    {
        public bool IsCusPointSearch { get; internal set; }

        /// <summary>
        /// 画面項目
        /// </summary>
        [ErsOutputHidden("search")]
        [ErsSchemaValidation("point_t.mcode")]
        public string mcode { get; set; }

        /// <summary>
        /// 画面項目
        /// </summary>
        public EnumDeleted? deleted { get; set; }

        /// <summary>
        /// ポイント履歴検索用
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [ErsOutputHidden]
        public DateTime? s_date_f { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [ErsOutputHidden]
        public DateTime? s_date_t { get; set; }

        /// <summary>
        /// 顧客検索結果件数
        /// </summary>
        public long search_result_cnt { get; set; }

        /// <summary>
        /// ポイント履歴リスト
        /// </summary>
        public List<Dictionary<string, object>> PointHistoryList { get; set; }

        #region サイトIDポイント検索用 [Site ID for point search]

        /// <summary>
        /// ポイント検索用サイトID [Site ID for point search]
        /// </summary>
        [ErsOutputHidden]
        [DisplayName("site_t.id")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0)]
        public virtual int? sp_site_id { get; set; }

        /// <summary>
        /// ポイント検索用モールショップ区分 [Mall shop division for point search]
        /// </summary>
        public virtual EnumMallShopKbn? sp_mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        /// <summary>
        /// ポイント検索用サイト名 [Site name for point search]
        /// </summary>
        public virtual string sp_site_name
        {
            get
            {
                if (this.sp_site_id != null)
                {
                    return ErsMallFactory.ersMallViewServiceFactory.GetErsViewSiteService().GetStringFromId(this.sp_site_id);
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// ポイント検索用サイトリスト [List of site for point search]
        /// </summary>
        public virtual List<Dictionary<string, object>> sp_site_list
        {
            get
            {
                return ErsMallFactory.ersMallViewServiceFactory.GetErsViewSiteService().SelectAsList(this.sp_mall_shop_kbn);
            }
        }

        /// <summary>
        /// 会員サイトID [Site ID of selected member]
        /// </summary>
        public virtual int? member_site_id
        {
            get
            {
                if (!string.IsNullOrEmpty(this.mcode))
                {
                    var member = ErsFactory.ersMemberFactory.getErsMemberWithMcodeForAdmin(this.mcode, true);

                    if (member != null)
                    {
                        if (member.site_id != null && member.site_id != 0)
                        {
                            this.sp_site_id = member.site_id;
                        }

                        return member.site_id;
                    }
                }
                return null;
            }
        }

        #endregion
    }
}