﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Customer.Mappables;
using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class member_rank_setup
        : ErsSiteRegisterModelBase, IMemberRankSetupMappable, IMemberRankSetupCommand
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        public bool IsCompletePage { get; internal set; }

        [ErsSchemaValidation("setup_t.member_rank_criterion")]
        public EnumMemberRankCriterion? member_rank_criterion { get; set; }

        [ErsSchemaValidation("setup_t.member_rank_term")]
        public int? member_rank_term { get; set; }

        [ErsOutputHidden]
        [BindTable("detail_table")]
        public List<member_rank_setup_detail> detail_table { get; set; }

        public List<Dictionary<string, object>> member_rank_criterionList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MEMBER_RANK, EnumCommonNameColumnName.namename);
            }
        }
    }
}