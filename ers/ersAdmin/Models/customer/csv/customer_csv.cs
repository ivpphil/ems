﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;

namespace ersAdmin.Models.csv
{
    public class Customer_csv
        : ErsModelBase
    {

        [CsvField]
        public virtual int? id { get; set; }

        [CsvField]
        public virtual string mcode { get; set; }

        [CsvField]
        public virtual string email { get; set; }

        //[CsvField]
        public virtual string passwd { get; set; }

        [CsvField]
        public virtual string lname { get; set; }

        [CsvField]
        public virtual string fname { get; set; }

        [CsvField]
        public virtual string lnamek { get; set; }

        [CsvField]
        public virtual string fnamek { get; set; }

        [CsvField]
        public virtual string compname { get; set; }

        [CsvField]
        public virtual string compnamek { get; set; }

        [CsvField]
        public virtual string division { get; set; }

        [CsvField]
        public virtual string divisionk { get; set; }

        //[CsvField]
        public virtual string tlname { get; set; }

        //[CsvField]
        public virtual string tfname { get; set; }

        //[CsvField]
        public virtual string tlnamek { get; set; }

        //[CsvField]
        public virtual string tfnamek { get; set; }

        [CsvField]
        public virtual string zip { get; set; }

        [CsvField]
        [DisplayName("pref")]
        public virtual string disp_pref { get; set; }

        public virtual int? pref { get; set; }

        [CsvField]
        public virtual string address { get; set; }

        [CsvField]
        public virtual string taddress { get; set; }

        [CsvField]
        public virtual string maddress { get; set; }

        [CsvField]
        public virtual string birth { get; set; }

        [CsvField]
        [DisplayName("sex")]
        public virtual string disp_sex { get; set; }

        public virtual EnumSex? sex { get; set; }

        [CsvField]
        [DisplayName("job")]
        public virtual string disp_job { get; set; }

        public virtual int? job { get; set; }

        [CsvField]
        public virtual string tel { get; set; }

        [CsvField]
        public virtual string fax { get; set; }

        //[CsvField]
        [DisplayName("ques")]
        public virtual string disp_ques { get; set; }

        public virtual short? ques { get; set; }

        //[CsvField]
        public virtual string ans { get; set; }

        [CsvField]
        public virtual int? sale { get; set; }

        [CsvField]
        [DisplayName("m_flg")]
        public virtual string disp_m_flg { get; set; }

        public virtual EnumMFlg? m_flg { get; set; }

        [CsvField]
        [DisplayName("dm_flg")]
        public virtual string disp_dm_flg { get; set; }

        public virtual EnumDmFlg dm_flg { get; set; }

        [CsvField]
        [DisplayName("out_bound_flg")]
        public virtual string disp_out_bound_flg { get; set; }

        public virtual EnumOutBoundFlg out_bound_flg { get; set; }


        [CsvField]
        public virtual string memo { get; set; }

        [CsvField]
        public virtual DateTime? intime { get; set; }

        [CsvField]
        public virtual DateTime? utime { get; set; }

        [CsvField]
        [DisplayName("country")]
        public virtual string disp_country { get; set; }

        public virtual string country { get; set; }

        [CsvField]
        [DisplayName("age_code")]
        public virtual string disp_age_code { get; set; }

        public virtual int? age_code { get; set; }

        [CsvField]
        [DisplayName("member_rank")]
        public virtual string disp_member_rank { get; set; }

        public virtual int? rank { get; set; }

        public virtual int? site_id { get; set; }

        [CsvField]
        [DisplayName("store_site_name")]
        public virtual string site_name
        {
            get
            {
                if (this.site_id == null || this.site_id == 0)
                {
                    return "共通";
                }
                return ErsMallFactory.ersMallViewServiceFactory.GetErsViewSiteService().GetStringFromId(this.site_id);
            }
        }
    }
}
