﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.ComponentModel;

namespace ersAdmin.Models.csv
{
    public class Cus_crderr_csv
        : ErsModelBase
    {

        [CsvField]
        public virtual string mcode { get; set; }

        [CsvField]
        public virtual string lname { get; set; }

        [CsvField]
        public virtual string fname { get; set; }

        [CsvField]
        public virtual string lnamek { get; set; }

        [CsvField]
        public virtual string fnamek { get; set; }

        [CsvField]
        public virtual DateTime? occured_date { get; set; }

        [CsvField]
        [DisplayName("regular_order_number")]
        public virtual string regular_detail_id { get; set; }

        [CsvField]
        public virtual string tel { get; set; }

        [CsvField]
        public virtual string email { get; set; }

        [CsvField]
        public virtual string gmo_error_number { get; set; }

        [CsvField]
        public virtual string gmo_error_message { get; set; }
    }
}