﻿using System.Collections.Generic;
using ersAdmin.Domain.Customer.Commands;
using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models
{
    public class cus_mail_all
        : Customer_search, ICusMailAllCommand, ICusMailAllMappable
    {
        /// <summary>
        /// リストの新規か追加か
        /// </summary>
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 2)]
        public short? makeListRadio { set; get; }

        /// <summary>
        /// 追加の場合のリストＩＤ
        /// </summary>
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? sid { set; get; }

        public IList<ErsMember> memberList { get; set; }
    }
}
