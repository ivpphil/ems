﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;

namespace ersAdmin.Models
{
    public class member_rank_setup_detail
        : ErsBindableModel, IMemberRankSetupDetailCommand
    {
        [ErsSchemaValidation("member_rank_setup_t.id")]
        public int? id { get; set; }

        [ErsSchemaValidation("member_rank_setup_t.rank")]
        public int? rank { get; set; }

        [ErsSchemaValidation("member_rank_setup_t.rank_name")]
        public string rank_name { get; set; }

        [ErsSchemaValidation("member_rank_setup_t.value_from")]
        [DisplayName("rank_value_from")]
        public int? value_from { get; set; }

        [ErsSchemaValidation("member_rank_setup_t.value_to")]
        [DisplayName("rank_value_to")]
        public int? value_to { get; set; }

        [ErsUniversalValidation(type=CHK_TYPE.Numeric,rangeFrom =0)]
        public double? point_magnification { get; set; }
        
    }
}