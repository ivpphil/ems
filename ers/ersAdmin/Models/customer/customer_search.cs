﻿using System;
using System.Collections.Generic;
using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using ersAdmin.Domain.Customer.Commands;

namespace ersAdmin.Models
{
    public class Customer_search
        : ErsSiteSearchModelBase, ICustomerSearchMappable, ICustomerCSVMappable, ICustomerSearchCommand
    {
        /// <summary>
        /// 検索用モールショップ区分 [Mall shop division for search]
        /// </summary>
        public override EnumMallShopKbn? s_mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        public ErsPagerModel pager { get; set; }

        public ErsCsvCreater csvCreater { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        /// <summary>
        /// Get number of item on a merchandise list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        public long recordCount { get; set; }

        /// <summary>
        /// 顧客検索条件
        /// </summary>
        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.email")]
        public string src_email { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tel")]
        public string src_tel { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.lname")]
        public string src_lname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fname")]
        public string src_fname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.lnamek")]
        public string src_lnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fnamek")]
        public string src_fnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.compname")]
        public string src_compname { get; set; }

        //メール配信区分
        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.m_flg")]
        public EnumMFlg? src_mailtype { get; set; }

        /// <summary>
        /// 登録日from/to
        /// </summary>
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? src_regdate_f { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? src_regdate_t { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 200)]
        public int? src_age_f { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 200)]
        public int? src_age_t { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.age_code")]
        public int? src_age_code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.sex")]
        public EnumSex? src_sex { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? src_point_f { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? src_point_t { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.pref")]
        public int? src_pref { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.deleted")]
        public EnumDeleted? src_deleted { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.dm_flg")]
        public EnumDmFlg? src_dm_flg { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.out_bound_flg")]
        public EnumOutBoundFlg? src_out_bound_flg { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_rank_t.rank")]
        public int? src_member_rank { get; set; }

        /// <summary>
        /// 都道府県プルダウンリスト
        /// </summary>
        public List<Dictionary<string, object>> prefList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(false, (int)EnumSiteId.COMMON_SITE_ID); }
        }

        public List<Dictionary<string, object>> m_flgList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MFlg, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> sexList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> ageTypeList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ORDAGE, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> wrap_flgList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Wrap, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> member_rank_flgList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewMemberRankSetupService().SelectAsList(); }
        }

        /// <summary>
        /// 顧客リスト
        /// </summary>
        public List<Dictionary<string, object>> list { get; set; }

        public bool member_centralization
        {
            get
            {
                return ErsFactory.ersUtilityFactory.getSetup().member_centralization;
            }
        }
    }
}