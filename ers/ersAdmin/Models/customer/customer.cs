﻿using System;
using System.Collections.Generic;
using ersAdmin.Domain.Customer.Commands;
using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers.viewService;

namespace ersAdmin.Models
{
    /// <summary>
    /// 顧客クラス
    /// </summary>
    public class Customer
        : ErsSiteRegisterModelBase, ICustomerCommand, ICustomerMappable
    {

        public bool IsModifyCompletionPage { get; internal set; }

        public bool IsLoadDefaultData { get; set; }

        /// <summary>
        /// 会員情報
        /// </summary>
        public ErsMember objMember { get; set; }

        /// <summary>
        /// 画面項目
        /// </summary>
        [ErsOutputHidden("input_form", "complete")]
        [ErsSchemaValidation("member_t.mcode")]
        public string mcode { get; set; }

        /// <summary>
        /// 取得項目
        /// </summary>
        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.email")]
        public string email { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.mformat")]
        public EnumMformat? mformat { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tel")]
        public string tel { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.lname")]
        public string lname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fname")]
        public string fname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.lnamek")]
        public string lnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fnamek")]
        public string fnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.compname")]
        public string compname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.sex")]
        public EnumSex? sex { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.pref")]
        public virtual int? pref { get; set; }

        /// <summary>
        /// その他項目
        /// </summary>
        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.passwd")]
        public string passwd { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.compnamek")]
        public string compnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.division")]
        public string division { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.divisionk")]
        public string divisionk { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tlname")]
        public string tlname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tfname")]
        public string tfname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tlnamek")]
        public string tlnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.tfnamek")]
        public string tfnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.zip")]
        public string zip { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.address")]
        public string address { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.taddress")]
        public string taddress { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.maddress")]
        public string maddress { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.job")]
        public short job { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.fax")]
        public string fax { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.ques")]
        public int? ques { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.ans")]
        public string ans { get; set; }

        [ErsOutputHidden]
        [DisplayName("common_admin_note")]
        [ErsSchemaValidation("member_t.memo")]
        public string memo { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.email")]
        public string email_confirm { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.passwd")]
        public string passwd_confirm { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = ErsViewBirthdayService.minimumOfSelectyear, rangeTo = ErsViewBirthdayService.maximumOfSelectyear)]
        public int? birthday_y { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 12)]
        public int? birthday_m { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 31)]
        public int? birthday_d { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.m_flg")]
        public EnumMFlg? m_flg { get; set; }

        [ErsOutputHidden("input_form")]
        [ErsSchemaValidation("member_t.pm_flg")]
        public EnumPmFlg? pm_flg { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.account_status")]
        public EnumAccountStatus? account_status { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.login_try_count")]
        public int? login_try_count { get; set; }

        /// <summary>
        /// 表示項目
        /// </summary>
        public string w_sex
        {
            get
            {
                if (!this.sex.HasValue)
                {
                    return null;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename, (int)this.sex.Value);
            }
        }

        public DateTime? intime_disp
        {
            get
            {

                if (this.objMember == null)
                    return null;

                return this.objMember.intime;
            }
        }

        public DateTime? utime
        {
            get
            {
                if (this.objMember == null)
                    return null;

                return this.objMember.utime;
            }
        }

        /// <summary>
        /// 画面項目
        /// </summary>
        public EnumDeleted? deleted
        {
            get
            {
                if (this.objMember == null)
                    return null;

                return this.objMember.deleted;
            }
        }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 255)]
        public string point { get; set; }

        public DateTime? birth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetBirthDay(this.birthday_y, this.birthday_m, this.birthday_d);
            }
        }

        [ErsOutputHidden]
        [ErsSchemaValidation("member_rank_t.rank")]
        public int? rank { get; set; }

        [DisplayName("member_rank_t.rank")]
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All, rangeTo = 255)]
        public string disp_member_rank { get; set; }

        public List<Dictionary<string, object>> birthday_yList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListYear(); }
        }

        public List<Dictionary<string, object>> birthday_mList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListMonth(); }
        }

        public List<Dictionary<string, object>> birthday_dList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListDay(); }
        }

        /// <summary>
        /// 都道府県プルダウンリスト
        /// </summary>
        public List<Dictionary<string, object>> prefList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(false, this.site_id_not_array); }
        }

        /// <summary>
        /// 職業リスト
        /// </summary>
        public List<Dictionary<string, object>> jobList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewJobService().SelectAsList(); }
        }

        /// <summary>
        /// 質問リスト
        /// </summary>
        public List<Dictionary<string, object>> quesList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewQuesService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> mformatList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MFormat, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> sexList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> m_flgList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MFlg, EnumCommonNameColumnName.namename); }
        }
        
        /// <summary>
        /// 顧客検索結果件数
        /// </summary>
        [ErsOutputHidden]
        public int search_result_cnt { get; protected set; }

    }
}