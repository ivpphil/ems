﻿using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.ComponentModel;

namespace ersAdmin.Models.customer
{
    public class Point_add
        : ErsSiteRegisterModelBase, IPointAddCommand
    {

        public bool IsCusPointAdd { get; set; }

        /// <summary>
        /// 画面項目
        /// </summary>
        [ErsSchemaValidation("point_t.mcode")]
        public string mcode { get; set; }

        //付与ポイント
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeTo = Int32.MaxValue)]
        [DisplayName("cuspoint_points_granted")]
        public int? add_point { get; set; }

        //ポイント付与理由
        [ErsSchemaValidation("point_t.reason")]
        public string add_reason { get; set; }

    }
}