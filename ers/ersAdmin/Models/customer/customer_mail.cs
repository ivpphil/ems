﻿using ersAdmin.Domain.Customer.Commands;
using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.Collections.Generic;

namespace ersAdmin.Models
{
    public class Customer_mail
        : ErsModelBase, ICustomerMailCommand, ICustomerMailMappable
    {

        public ErsMember objMember { get; set; }

        public bool IsSendMail { get; set; }

        public bool IsLoadDefaultValue { get; set; }

        /// <summary>
        /// 画面項目
        /// </summary>
        [ErsOutputHidden]
        [ErsSchemaValidation("member_t.mcode")]
        public string mcode { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.EMailAddr)]
        public string email { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.EMailAddr)]
        public string from_email { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Templates)]
        public string mail_title { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Templates)]
        public string mail_body { get; set; }

        [HtmlSubmitButton]
        public bool send_to_admin { get; set; }

        [HtmlSubmitButton]
        public bool send_btn { get; set; }

        public string lname
        {
            get
            {
                if (objMember == null)
                    return string.Empty;

                return objMember.lname;
            }
        }

        public string fname
        {
            get
            {
                if (objMember == null)
                    return string.Empty;

                return objMember.fname;
            }
        }

        public virtual List<Dictionary<string, object>> template_list { get; set; }

        public virtual bool isCommon { get; set; }
    }
}