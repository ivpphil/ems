﻿using System;
using System.Collections.Generic;
using ersAdmin.Domain.Promotion.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace ersAdmin.Models
{
    public class coupon_search : coupon_s, ICouponSearchMappable, ICouponListCSVMappable
    {

        public bool IsSearchList { get; internal set; }

        #region Search

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.coupon_code")]
        public new string coupon_code { get; set; }

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.intime")]
        public DateTime? s_intime_from { get; set; }

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.intime")]
        public DateTime? s_intime_to { get; set; }

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.coupon_code")]
        public string s_coupon_code { get; set; }

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.coupon_type")]
        public EnumCouponType? s_coupon_type { get; set; }

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.price")]
        public int? s_price_from { get; set; }

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.price")]
        public int? s_price_to { get; set; }

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.price")]
        public int? s_base_price_from { get; set; }

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.price")]
        public int? s_base_price_to { get; set; }

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.start_date")]
        public DateTime? s_start_date_from { get; set; }

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.start_date")]
        public DateTime? s_start_date_to { get; set; }

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.end_date")]
        public DateTime? s_end_date_from { get; set; }

        [ErsOutputHidden("searchGroup")]
        [ErsSchemaValidation("coupon_t.end_date")]
        public DateTime? s_end_date_to { get; set; }

        #endregion
        public List<Dictionary<string, object>> list { get; set; }

        public long recordCount { get; set; }

        public ErsPagerModel pager { get; set; }

        public ErsCsvCreater csvCreater { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        /// <summary>
        /// Get number of template list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

    }


}