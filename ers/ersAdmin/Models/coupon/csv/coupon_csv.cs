﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;


namespace ersAdmin.Models.csv
{
    public class Coupon_csv
        : ErsBindableModel
    {
        [CsvField]
        public virtual string coupon_code { get; set; }

        [CsvField]
        public virtual EnumCouponType? coupon_type { get; set; }

        [CsvField]
        [DisplayName("coupon_t.price")]
        public virtual int? price { get; set; }

        [CsvField]
        public virtual int? base_price { get; set; }

        [CsvField]
        public virtual DateTime? start_date { get; set; }

        [CsvField]
        public virtual DateTime? end_date { get; set; }

        [CsvField]
        [DisplayName("coupon_t.active")]
        public virtual EnumActive active { get; set; }

        [CsvField]
        public virtual int[] site_id { get; set; }
    }
}
