﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.merchandise.strategy;
using System.ComponentModel;
using jp.co.ivp.ers;
using ersAdmin.Domain.Promotion.Commands;
using ersAdmin.Domain.Promotion.Mappables;

namespace ersAdmin.Models.csv
{
    public class Coupon_csv_record
        : ErsBindableModel, ICouponCSVRecordCommand, ICouponCSVRecordMappable
    {
        [CsvField]
        [ErsSchemaValidation("coupon_t.coupon_code")]
        public virtual string coupon_code { get; set; }

        [CsvField]
        [ErsSchemaValidation("coupon_t.coupon_type")]
        public virtual EnumCouponType? coupon_type { get; set; }

        [CsvField]
        [DisplayName("coupon_t.price")]
        [ErsSchemaValidation("coupon_t.price")]
        public virtual int? price { get; set; }

        [CsvField]
        [ErsSchemaValidation("coupon_t.base_price")]
        public virtual int? base_price { get; set; }

        [CsvField]
        [ErsSchemaValidation("coupon_t.start_date")]
        public virtual DateTime? start_date { get; set; }

        [CsvField]
        [ErsSchemaValidation("coupon_t.end_date")]
        public virtual DateTime? end_date { get; set; }

        [CsvField]
        [ErsSchemaValidation("coupon_t.active")]
        public virtual EnumActive? active { get; set; }

        [CsvField]
        [DisplayName("coupon_t.site_id")]
        public virtual int[] site_id { get; set; }
    }
}