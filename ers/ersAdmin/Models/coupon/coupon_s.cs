﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.coupon;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
namespace ersAdmin.Models
{
    public class coupon_s : ErsSiteSearchModelBase
    {
        /// <summary>
        /// 検索用モールショップ区分 [Mall shop division for search]
        /// </summary>
        public override EnumMallShopKbn? s_mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        [ErsOutputHidden("id")]
        [ErsSchemaValidation("coupon_t.id")]
        public int? id { get; set; }

        [ErsOutputHidden("coupon_code")]
        [ErsSchemaValidation("coupon_t.coupon_code")]
        public string coupon_code { get; set; }

        [ErsOutputHidden("coupon_type")]
        [ErsSchemaValidation("coupon_t.coupon_type")]
        public EnumCouponType? coupon_type { get; set; }

        [ErsOutputHidden("price")]
        [ErsSchemaValidation("coupon_t.price")]
        public int? price { get; set; }

        [ErsOutputHidden("base_price")]
        [ErsSchemaValidation("coupon_t.base_price")]
        public int? base_price { get; set; }

        [ErsOutputHidden("start_date")]
        [ErsSchemaValidation("coupon_t.start_date")]
        public DateTime? start_date { get; set; }

        [ErsOutputHidden("end_date")]
        [ErsSchemaValidation("coupon_t.end_date")]
        public DateTime? end_date { get; set; }

        [ErsOutputHidden("active")]
        [ErsSchemaValidation("coupon_t.active")]
        public EnumActive? active { get; set; }


        public List<Dictionary<string, object>> typeCode
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.CouponType, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> typeActive
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Active, EnumCommonNameColumnName.namename);
            }
        }



    }
}