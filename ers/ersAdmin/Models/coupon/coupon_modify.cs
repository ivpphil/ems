﻿using System;
using ersAdmin.Domain.Promotion.Commands;
using ersAdmin.Domain.Promotion.Mappables;
using jp.co.ivp.ers.coupon;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models
{
    public class coupon_modify : coupon, ICouponModifyCommand, ICouponModifyMappable, ICouponDeleteCommand
    {
        protected ErsCouponRepository couponRepo;

        [ErsOutputHidden("intime")]
        [ErsSchemaValidation("coupon_t.intime")]
        public DateTime intime { get; set; }

        [ErsOutputHidden("utime")]
        [ErsSchemaValidation("coupon_t.utime")]
        public DateTime? utime { get; set; }

    }
}