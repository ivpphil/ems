﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.administrator;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Models.store
{
    public class RoleModify
        : ErsModelBase, IRoleModifyCommand, IRoleDeleteCommand, IRoleModifyMappable
    {
        [ErsSchemaValidation("role_group_t.role_gname")]
        public string role_gname { get; set; }

        [ErsSchemaValidation("role_group_t.role_action")]
        public string[] role_action { get; set; }

        [ErsSchemaValidation("role_group_t.intime")]
        public DateTime intime { get; set; }

        [ErsSchemaValidation("role_group_t.utime")]
        public DateTime? utime { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.FullString)]
        public string PageTitle { get; set; }

        [HtmlSubmitButton]
        public bool submit_regist { get; set; }

        public virtual IList<Dictionary<string, object>> functionList { get { return ErsFactory.ersViewServiceFactory.GetErsViewFunctionGroupService().SelectAsList(this.role_action); } }

        [HtmlSubmitButton]
        public bool submit_change { get; set; }

        [HtmlSubmitButton]
        public bool submit_delete { get; set; }

        [ErsSchemaValidation("role_group_t.id")]
        public int id { get; set; }

        [ErsSchemaValidation("role_group_t.role_gname")]
        public string d_role_gname
        {
            get
            {
                var newsRoleGroup = ErsFactory.ersAdministratorFactory.GetErsRoleGroupWithId(this.id);
                if (newsRoleGroup == null)
                {
                    return string.Empty;
                }
                return newsRoleGroup.role_gname;
            }
        }
    }
}