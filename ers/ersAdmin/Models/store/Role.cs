﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using jp.co.ivp.ers.administrator.role_group;
using ersAdmin.Domain.Store.Mappables;
namespace ersAdmin.Models.store
{
    public class Role
        : ErsModelBase, IRoleMappable
    {
        public List<Dictionary<string, object>> Role_Group_List { get; set; }

        public long search_result_cnt { get; set; }

        [ErsSchemaValidation("news_role_group_t.role_gcode")]
        public string role_gcode {get;set;}

        [ErsSchemaValidation("news_role_group_t.role_gname")]
        public string role_gname {get;set;}

        [ErsSchemaValidation("news_role_group_t.role_action")]
        public string[] role_action{get;set;}

        [ErsSchemaValidation("news_role_group_t.intime")]
        public DateTime intime {get;set;}

        [ErsSchemaValidation("news_role_group_t.utime")]
        public DateTime utime {get;set;}


        [ErsUniversalValidation(type = CHK_TYPE.FullString)]
        public string PageTitle { get; set; }
    }
}