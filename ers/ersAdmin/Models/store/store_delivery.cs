﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.sendmail;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class Store_delivery : ErsSiteRegisterModelBase, IStoreDeliveryCommand, IStoreDeliveryMappable
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_delivery_btn { get; set; }
        public Store_delivery()
        {
        }

        /// 配送会社名
        [ErsSchemaValidation("setup_t.delivery")]
        public string delivery { get; set; }

        /// 問い合わせ先URL名
        [ErsSchemaValidation("setup_t.url")]
        public string url { get; set; }

        [ErsSchemaValidation("setup_t.mail_delivery")]
        public string mail_delivery { get; set; }

        /// 問い合わせ先URL名
        [ErsSchemaValidation("setup_t.mail_url")]
        public string mail_url { get; set; }
    }
}