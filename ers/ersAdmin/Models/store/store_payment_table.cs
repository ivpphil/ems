﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.Send;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using ersAdmin.Models.store;
using jp.co.ivp.ers;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;

namespace ersAdmin.Models
{
    public class Store_payment_table
        : ErsBindableModel, IStorePaymentListRecordCommand, IStorePaymentListRecordMappable

    {

        public override string lineName { get { return this.pay_name; } }

        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_payment_btn { get; set; }

        [ErsSchemaValidation("pay_t.id")]
        public EnumPaymentType? id { get; set; }

        [ErsSchemaValidation("pay_t.pay_name")]
        public string pay_name { get; set; }

        [ErsSchemaValidation("pay_t.active")]
        public EnumActive? active { get; set; }

        [ErsSchemaValidation("pay_t.etc_name")]
        public string etc_name { get; set; }

        [ErsSchemaValidation("pay_t.etc_flg")]
        public EnumOnOff? etc_flg { get; set; }

        [ErsSchemaValidation("pay_t.memo")]
        public string memo { get; set; }

        ///　支払い方法一覧リスト
        [BindTable("store_etc_table")]
        public List<store_etc_table> store_etc_table { get; set; }

        [ErsSchemaValidation("pay_t.enable_tax")]
        public EnumOnOff? enable_tax { get; set; }
        

    }
}