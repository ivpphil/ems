﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;
using ersAdmin.Domain.Store.Commands;

namespace ersAdmin.Models
{
    public class AdminListData
        : ErsBindableModel, IStoreAdminListRecordCommand
    {
        [ErsSchemaValidation("administrator_t.id")]
        public virtual int? id { get; set; }

        [ErsSchemaValidation("administrator_t.user_cd")]
        public virtual string user_cd { get; set; }

        [ErsSchemaValidation("administrator_t.user_login_id")]
        public virtual string user_login_id { get; set; }

        [ErsSchemaValidation("administrator_t.user_name")]
        public virtual string user_name { get; set; }

        [ErsSchemaValidation("administrator_t.passwd")]
        public virtual string passwd { get; set; }

        [ErsSchemaValidation("administrator_t.role_gcode")]
        public virtual string role_gcode { get; set; }

        [ErsSchemaValidation("administrator_t.agent_id")]
        public virtual int? agent_id { get; set; }

        [HtmlSubmitButton]
        public virtual bool delete { get; set; }

        public IList<Dictionary<string, object>> role_gcodeList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewRoleGroupService().SelectAsList(null);
            }
        }

        public List<Dictionary<string, object>> agent_idList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentService().SelectAsList(null);
            }
        }
    }
}