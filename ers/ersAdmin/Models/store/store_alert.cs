﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.sendmail;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;

namespace ersAdmin.Models
{
    public class Store_alert : ErsSiteRegisterModelBase, IStoreAlertCommand, IStoreAlertMappable
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_alert_btn { get; set; }

        [ErsSchemaValidation("setup_t.pri_memo")]
        public string pri_memo { get; set; }

        [ErsSchemaValidation("setup_t.regi_memo")]
        public string regi_memo { get; set; }

        [BindTable("store_payment_table")]
        public List<Store_payment_table> store_payment_table { get; set; }

        [ErsSchemaValidation("setup_t.error_message")]
        public string error_message { get; set; }

        [ErsSchemaValidation("setup_t.m_error_message")]
        public string m_error_message { get; set; }

        [ErsSchemaValidation("setup_t.wh_owner_description")]
        public string wh_owner_description { get; set; }

    }
}