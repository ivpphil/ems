﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.sendmail;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class Store_mail_text : ErsSiteRegisterModelBase, IStoreMailTextCommand, IStoreMailTextMappable
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_mail_template_btn { get; set; }

        /// 新規会員登録
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("entry_pc_title")]
        public string entry_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("entry_pc_body")]
        public string entry_pc_body { get; set; }

        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("entry_mobile_title")]
        public string entry_mobile_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("entry_mobile_body")]
        public string entry_mobile_body { get; set; }

        /// 注文完了
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("register_pc_title")]
        public string register_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("register_pc_body")]
        public string register_pc_body { get; set; }

        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("register_mobile_title")]
        public string register_mobile_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("register_mobile_body")]
        public string register_mobile_body { get; set; }

        /// 定期伝票発行完了
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("regular_order_pc_title")]
        public string regular_order_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("regular_order_pc_body")]
        public string regular_order_pc_body { get; set; }

        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("regular_order_mobile_title")]
        public string regular_order_mobile_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("regular_order_mobile_body")]
        public string regular_order_mobile_body { get; set; }

        /// 配送完了
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("delivery_pc_title")]
        public string delivery_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("delivery_pc_body")]
        public string delivery_pc_body { get; set; }

        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("delivery_mobile_title")]
        public string delivery_mobile_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("delivery_mobile_body")]
        public string delivery_mobile_body { get; set; }

        /// 退会完了
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("quit_pc_title")]
        public string quit_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("quit_pc_body")]
        public string quit_pc_body { get; set; }

        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("quit_mobile_title")]
        public string quit_mobile_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("quit_mobile_body")]
        public string quit_mobile_body { get; set; }

        //パスワードリマインダー
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("passrim_pc_title")]
        public string passrim_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("passrim_pc_body")]
        public string passrim_pc_body { get; set; }

        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("passrim_mobile_title")]
        public string passrim_mobile_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("passrim_mobile_body")]
        public string passrim_mobile_body { get; set; }

        //個別メール
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("individual_pc_title")]
        public string individual_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("individual_pc_body")]
        public string individual_pc_body { get; set; }

        //お問い合わせ
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("quest_pc_title")]
        public string quest_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("quest_pc_body")]
        public string quest_pc_body { get; set; }

        //お問い合わせの受付
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("quest_accept_pc_title")]
        public string quest_accept_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("quest_accept_pc_body")]
        public string quest_accept_pc_body { get; set; }

        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("quest_accept_mobile_title")]
        public string quest_accept_mobile_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("quest_accept_mobile_body")]
        public string quest_accept_mobile_body { get; set; }
    }
}