﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.sendmail;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class Store_admin
        : ErsSiteRegisterModelBase, IStoreAdminCommand, IStoreAdminMappable
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_admin_btn { get; set; }

        /// リプライニングメールアドレス
        [ErsSchemaValidation("setup_t.r_email")]
        public string replying_mail_addr { get; set; }

        /// 注文を受けたときにお知らせするメールアドレス
        [ErsSchemaValidation("setup_t.f_email1")]
        public string report_mail_addr1 { get; set; }

        /// 注文を受けたときにお知らせするメールアドレス
        [ErsSchemaValidation("setup_t.f_email2")]
        public string report_mail_addr2 { get; set; }

        /// 注文を受けたときにお知らせするメールアドレス
        [ErsSchemaValidation("setup_t.f_email3")]
        public string report_mail_addr3 { get; set; }

        /// お問い合わせを受けた時にお知らせするメールアドレス
        [ErsSchemaValidation("setup_t.quest_email_to")]
        public string quest_email_to { get; set; }

        //ユーザー利用権限一覧
        [BindTable("adminList")]
        public List<AdminListData> adminList { get; set; }
    }
}