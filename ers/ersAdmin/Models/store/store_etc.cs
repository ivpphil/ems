﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.Send;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class Store_etc
        : ErsSiteRegisterModelBase, IStoreEtcCommand, IStoreEtcMappable
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_etc_btn { get; set; }

        [BindTable("store_payment_table")]
        public List<Store_payment_table> store_payment_table { get; set; }
    }
}