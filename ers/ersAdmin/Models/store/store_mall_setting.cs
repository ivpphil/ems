﻿using System.Collections.Generic;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall.site;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models
{
    /// <summary>
    /// モール店舗設定モデル [Model for mall settings]
    /// </summary>
    public class store_mall_setting
        : ErsModelBase, IStoreMallSettingCommand, IStoreMallSettingMappable
    {
        #region システム利用 [Use for System]
        /// <summary>
        /// 初回表示 [First display]
        /// </summary>
        public bool isFirstDisplay { get; set; }
        #endregion

        #region ボタン [Buttons]
        /// <summary>
        /// "登録する"ボタン [Register button]
        /// </summary>
        [HtmlSubmitButton]
        public bool register_btn { get; set; }
        #endregion

        #region サイト [Site]
        /// <summary>
        /// サイトリスト [The list of site]
        /// </summary>
        public IList<ErsSite> listSite { get; set; }

        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        [ErsSchemaValidation("site_t.id")]
        public int? site_id { get; set; }

        /// <summary>
        /// モール店舗区分 [Mall shop type]
        /// </summary>
        public EnumMallShopKbn? mall_shop_kbn { get; set; }
        #endregion

        #region 楽天用 [For Rakuten]
        /// <summary>
        /// ショップ名 [Shop name]
        /// </summary>
        [ErsUniversalValidation(rangeTo=100, type = CHK_TYPE.All)]
        public string rakuten_shop_name { get; set; }

        /// <summary>
        /// 楽天店舗名 [Rakuten mall shop name]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.All)]
        public string rakuten_mall_shop_name { get; set; }

        /// <summary>
        /// 楽天店舗URL [Rakuten mall shop url]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string rakuten_mall_shop_url { get; set; }

        /// <summary>
        /// FTPホスト [FTP host]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.WebAddress)]
        public string rakuten_ftp_host { get; set; }

        /// <summary>
        /// FTPユーザ [FTP user]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string rakuten_ftp_user { get; set; }

        /// <summary>
        /// FTPパスワード [FTP password]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string rakuten_ftp_pass { get; set; }

        /// <summary>
        /// FTPパスワード最終更新日 [FTP password last updated date]
        /// </summary>
        public string rakuten_ftp_pass_utime { get; set; }

        /// <summary>
        /// WebAPI利用フラグ [Use WebAPI flag]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 1, type = CHK_TYPE.Numeric)]
        public int rakuten_use_webapi { get; set; }

        /// <summary>
        /// WebAPIユーザID [WebAPI user id]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string rakuten_webapi_user { get; set; }

        /// <summary>
        /// WebAPI店舗URL [WebAPI shop url]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string rakuten_webapi_url { get; set; }

        /// <summary>
        /// WebAPI認証キー [WebAPI authorization key]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string rakuten_webapi_auth_key { get; set; }

        /// <summary>
        /// WebAPI認証キー最終更新日 [WebAPI authorization key last updated date]
        /// </summary>
        public string rakuten_webapi_auth_key_utime { get; set; }

        /// <summary>
        /// R-Login ID [R-Login id
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string rakuten_rlogin_user { get; set; }

        /// <summary>
        /// R-Login パスワード [R-Login password]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string rakuten_rlogin_pass { get; set; }

        /// <summary>
        /// R-Login パスワード最終更新日 [R-Login password last updated date]
        /// </summary>
        public string rakuten_rlogin_pass_utime { get; set; }

        /// <summary>
        /// 楽天会員ID [Rakuten account id]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string rakuten_account_user { get; set; }

        /// <summary>
        /// 楽天会員パスワード [Rakuten account password]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string rakuten_account_pass { get; set; }

        /// <summary>
        /// 楽天メール送信者メールアドレス [From mail address for Rakuten mail]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.EMailAddr)]
        public string rakuten_mail_from { get; set; }

        /// <summary>
        /// 楽天メールサーバホスト [Mail server host for Rakuten mail]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.WebAddress)]
        public string rakuten_mail_host { get; set; }

        /// <summary>
        /// 楽天メールサーバポート [Mail server port for Rakuten mail]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 65535, type = CHK_TYPE.Numeric)]
        public string rakuten_mail_port { get; set; }

        /// <summary>
        /// 楽天メールサーバ認証ID [Mail server id for Rakuten mail]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string rakuten_mail_auth_id { get; set; }

        /// <summary>
        /// 楽天メールサーバ認証パスワード [Mail server password for Rakuten mail]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string rakuten_mail_auth_pass { get; set; }
        #endregion

        #region Yahoo!用 [For Yahoo!]
        /// <summary>
        /// ショップ名 [Shop name]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.All)]
        public string yahoo_shop_name { get; set; }

        /// <summary>
        /// Yahoo!店舗名 [Yahoo! mall shop name]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.All)]
        public string yahoo_mall_shop_name { get; set; }

        /// <summary>
        /// FTPホスト [FTP host]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.WebAddress)]
        public string yahoo_ftp_host { get; set; }

        /// <summary>
        /// FTPユーザ [FTP user]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string yahoo_ftp_user { get; set; }

        /// <summary>
        /// FTPパスワード [FTP password]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string yahoo_ftp_pass { get; set; }

        /// <summary>
        /// Yahoo!APIアカウント名 [API account]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string yahoo_api_account { get; set; }

        /// <summary>
        /// Yahoo!ビジネスID [Yahoo! business id]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string yahoo_business_user { get; set; }

        /// <summary>
        /// Yahoo!ビジネスパスワード [Yahoo! business password]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string yahoo_business_pass { get; set; }

        /// <summary>
        /// Yahoo!会員ID [Yahoo! account id]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string yahoo_account_user { get; set; }

        /// <summary>
        /// Yahoo!会員パスワード [Yahoo! account password]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string yahoo_account_pass { get; set; }

        /// <summary>
        /// Yahoo!決済データ存在フラグ [Yahoo! exists payment data]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 1, type = CHK_TYPE.Numeric)]
        public int yahoo_exists_payment_data { get; set; }

        /// <summary>
        /// Yahoo!メール送信者メールアドレス [From mail address for Yahoo! mail]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.EMailAddr)]
        public string yahoo_mail_from { get; set; }
        #endregion

        #region Amazon用 [For Amazon]
        /// <summary>
        /// ショップ名 [Shop name]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.All)]
        public string amazon_shop_name { get; set; }

        /// <summary>
        /// 出品者ID [Merchant id]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string amazon_merchant_id { get; set; }

        /// <summary>
        /// マーケットプレイスID [Marketplace id]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string amazon_marketplace_id { get; set; }

        /// <summary>
        /// アクセスキーID [Access key id]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string amazon_accesskey_id { get; set; }

        /// <summary>
        /// シークレットアクセスキー [Secret access key]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.OneByteCharacter)]
        public string amazon_secret_accesskey { get; set; }

        /// <summary>
        /// Amazonメール送信者メールアドレス [From mail address for Amazon mail]
        /// </summary>
        [ErsUniversalValidation(rangeTo = 100, type = CHK_TYPE.EMailAddr)]
        public string amazon_mail_from { get; set; }
        #endregion
    }
}