﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.sendmail;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.Send;
using System.ComponentModel;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;

namespace ersAdmin.Models
{
    public class Store_delivery_day : ErsModelBase, IStoreDeliveryDayCommand, IStoreDeliveryDayMappable, IDeleteDeliveryDayCommand 
    {
        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_delivery_day_btn { get; set; }

        ///　一覧リスト
        public List<Dictionary<string, object>> allList { get; set; }

        [ErsSchemaValidation("sendtime_t.id")]
        public int? delete_id { get; set;}

        /// 出荷指示日
        [ErsSchemaValidation("setup_t.shipping_csv_output_min_days")]
        public int shipping_csv_output_min_days { get; set; }

        /// 定期伝票作成日
        [ErsSchemaValidation("setup_t.create_regular_order_days")]
        public int create_regular_order_days { get; set; }

        /// 配送日（FROM）
        [ErsSchemaValidation("setup_t.sendday")]
        public int? sendday { get; set; }

        /// 配送日（TO）
        [ErsSchemaValidation("setup_t.sendday_count")]
        public int? sendday_count { get; set; }

        /// 追加
        [ErsSchemaValidation("sendtime_t.sendtime")]
        public string add_sendtime { get; set; }

        //
        [HtmlDictionary("sendtime")]
        [ErsSchemaValidation("sendtime_t.sendtime")]
        [DisplayName("add_sendtime")]
        public Dictionary<int?, string> sendtime { get; set; }
    }
}