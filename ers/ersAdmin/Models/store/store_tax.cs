﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.sendmail;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class Store_tax : ErsModelBase, IStoreTaxCommand, IStoreTaxMappable
    {
        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_tax_btn { get; set; }
        public Store_tax()
        {
        }

        /// 消費税
        [ErsSchemaValidation("setup_t.tax")]
        public int? tax { get; set; }

        [ErsSchemaValidation("setup_t.enable_carriage_tax")]
        public EnumOnOff?  enable_carriage_tax { get; set; }

        ///　支払い方法一覧リスト
        [BindTable("store_payment_table")]
        public IList<Store_tax_payment_table> store_payment_table { get; set; }
       

        
    }
}