﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.Send;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using ersAdmin.Models.store;
using jp.co.ivp.ers;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;

namespace ersAdmin.Models
{
    public class Store_tax_payment_table
        : ErsBindableModel, IStoreTaxPaymentListRecordCommand

    {

        public override string lineName { get { return this.pay_name; } }

        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_payment_btn { get; set; }

        [ErsSchemaValidation("pay_t.id")]
        public EnumPaymentType? id { get; set; }

        [ErsSchemaValidation("pay_t.pay_name")]
        public string pay_name { get; set; }

        [ErsSchemaValidation("pay_t.enable_tax")]
        public EnumOnOff? enable_tax { get; set; }
        

    }
}