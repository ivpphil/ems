﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Models.store
{
    public class Store_card_table
        : ErsBindableModel, IStoreCardListRecordCommand
    {
        [ErsSchemaValidation("card_t.id")]
        public int? id { get; set; }

        [ErsSchemaValidation("card_t.card_name")]
        public string card_name { get; set; }

        [ErsSchemaValidation("card_t.active")]
        public EnumActive? active { get; set; }


    }
}