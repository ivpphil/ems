﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using ersAdmin.Domain.Store.Commands;

namespace ersAdmin.Models.store
{
    public class common_name_detail : ErsBindableModel, ICommonNameRecordCommand
    {
        [ErsSchemaValidation("common_namecode_t.id")]
        public int? id { get; set; }

        [ErsSchemaValidation("common_namecode_t.type_code")]
        public EnumCommonNameType? type_code { get; set; }

        [ErsSchemaValidation("common_namecode_t.code")]
        public int? code { get; set; }

        [ErsSchemaValidation("common_namecode_t.namename")]
        public string namename { get; set; }

        [ErsSchemaValidation("common_namecode_t.opt_chr1")]
        public string opt_chr1 { get; set; }

        [ErsSchemaValidation("common_namecode_t.opt_chr2")]
        public string opt_chr2 { get; set; }

        [ErsSchemaValidation("common_namecode_t.opt_flg1")]
        public int opt_flg1 { get; set; }

        [ErsSchemaValidation("common_namecode_t.opt_flg2")]
        public int opt_flg2 { get; set; }

        [ErsSchemaValidation("common_namecode_t.opt_num1")]
        public int? opt_num1 { get; set; }

        [ErsSchemaValidation("common_namecode_t.opt_num2")]
        public int? opt_num2 { get; set; }

        [ErsSchemaValidation("common_namecode_t.disp_order")]
        public int? disp_order { get; set; } 
        
        [ErsSchemaValidation("common_namecode_t.active")]
        public EnumActive? active { get; set; }

        [ErsSchemaValidation("common_namecode_t.intime")]
        public DateTime? intime { get; set; }

        [ErsSchemaValidation("common_namecode_t.utime")]
        public DateTime? utime { get; set; }

        [HtmlSubmitButton()]
        public bool delete { get; set; }


    }
}