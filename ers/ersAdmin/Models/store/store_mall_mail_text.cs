﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.sendmail;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;

namespace ersAdmin.Models
{
    public class Store_mall_mail_text : ErsModelBase, IStoreMallMailTextCommand, IStoreMallMailTextMappable
    {
        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_mail_template_btn { get; set; }

        [ErsSchemaValidation("mail_template_t.site_id")]
        [DisplayName("site_id")]
        public int? site_id { get; set; }

        /// 注文完了
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("register_pc_title")]
        public string register_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("register_pc_body")]
        public string register_pc_body { get; set; }

        /// モールメール便エラーメール
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("mall_delv_error_pc_title")]
        public string mall_delv_error_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("mall_delv_error_pc_body")]
        public string mall_delv_error_pc_body { get; set; }

        /// 配送完了
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("delivery_pc_title")]
        public string delivery_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("delivery_pc_body")]
        public string delivery_pc_body { get; set; }

        // 個別メール
        [ErsSchemaValidation("tp_mail_t.subject")]
        [DisplayName("individual_pc_title")]
        public string individual_pc_title { get; set; }

        [ErsSchemaValidation("mail_template_t.value")]
        [DisplayName("individual_pc_body")]
        public string individual_pc_body { get; set; }

        public List<Dictionary<string, object>> siteList { get { return ErsMallFactory.ersMallViewServiceFactory.GetErsViewSiteService().SelectAsList((record) => (int)record["mall_shop_kbn"] != (int)EnumMallShopKbn.ERS); } }
    }
}