﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Store.Commands;

namespace ersAdmin.Models.store
{
    public class store_carriage_table
        : ErsBindableModel, IStoreCarriageRecordCommand
    {
        [ErsSchemaValidation("pref_t.id")]
        public int? id { get; set; }

        public string pref_name { get; set; }

        [ErsSchemaValidation("pref_t.carriage")]
        public int? carriage { get; set; }

        [HtmlSubmitButton]
        public bool top { get; set; }

        [HtmlSubmitButton]
        public bool end { get; set; }
    }
}