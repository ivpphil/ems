﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.Send;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using ersAdmin.Models.store;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class Store_payment : ErsSiteRegisterModelBase, IStorePaymentCommand, IStorePaymentMappable
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_payment_btn { get; set; }

        ///　支払い方法一覧リスト
        [BindTable("store_payment_table")]
        public IList<Store_payment_table> store_payment_table { get; set; }

        //カードブランドリスト
        [BindTable("store_card_table")]
        public IList<Store_card_table> store_card_table { get; set; }  
    }
}