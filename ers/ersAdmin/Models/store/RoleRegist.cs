﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.util;
using ersAdmin.Domain.Store.Commands;

namespace ersAdmin.Models.store
{
    public class RoleRegist : ErsModelBase,IRoleRegistCommand
    {
        [ErsSchemaValidation("role_group_t.role_gname")]
        public string role_gname { get; set; }

        [ErsSchemaValidation("role_group_t.role_action")]
        public string[] role_action { get; set; }

        [ErsSchemaValidation("role_group_t.intime")]
        public DateTime intime { get; set; }

        [ErsSchemaValidation("role_group_t.utime")]
        public DateTime? utime { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.FullString)]
        public string PageTitle { get; set; }

        [HtmlSubmitButton]
        public bool submit_regist { get; set; }

        public virtual IList<Dictionary<string, object>> functionList { get { return ErsFactory.ersViewServiceFactory.GetErsViewFunctionGroupService().SelectAsList(this.role_action); } }

    }
}