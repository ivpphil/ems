﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.common;
using ersAdmin.Domain.Store.Commands;

namespace ersAdmin.Models.store
{
    public class common_name_regist : ErsModelBase, ICommonNameRegistMappable, ICommonNameCommand
    {
        [BindTable("common_name_table")]
        public List<common_name_detail> common_name_table { get; set; }

        public IList<ErsCommonNameCode> Type_Code_List { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public EnumCommonNameType? type_code { get; set; }

        /// <summary>
        /// 登録ボタンが押されたか否か
        /// </summary>
        [HtmlSubmitButton]
        public bool regist { get; set; }

        /// <summary>
        /// ｵﾌﾟｼｮﾝ項目
        /// </summary>
        public string[] stArrayData { get; set; }

        /// <summary>
        /// ｵﾌﾟｼｮﾝ項目表示ﾌﾗｸﾞ
        /// </summary>
        public bool item_flg { get; set; }

        /// <summary>
        /// errorﾌﾗｸﾞ
        /// </summary>
        public bool error_flg { get; set; }


    }
}