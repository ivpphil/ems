﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Store.Commands;

namespace ersAdmin.Models.store
{
    public class store_etc_table
        : ErsBindableModel, IStoreEtcListRecordCommand
    {
        /// id
        [ErsSchemaValidation("etc_t.id")]
        public int? id { get; set; }

        /// 支払い方法
        [ErsSchemaValidation("etc_t.pay")]
        public EnumPaymentType? pay { get; set; }

        /// 以上
        [ErsSchemaValidation("etc_t.more")]
        public int? more { get; set; }

        /// 未満
        [ErsSchemaValidation("etc_t.down")]
        public int? down { get; set; }

        /// 手数料
        [ErsSchemaValidation("etc_t.price")]
        public int? price { get; set; }



       
    }
}