﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.sendmail;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.Send;
using ersAdmin.Models.store;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class Store_calendar : ErsModelBase, IStoreCalendarCommand,IStoreCalendarMappable
    {
        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_delivery_day_btn { get; set; }

        [HtmlSubmitButton]
        public bool butYearMonth { get; set; }
        
        // ここの変数をそのままDBには入れないので、ErsSchemaValidationでなく、
        // ErsUniversalValidationを使う

        /// 設定年
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? ddlYear { get; set; }

        /// 設定月
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? ddlMonth { get; set; }

        /// 追加
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public List<Dictionary<string, object>> dayList { get; set; }

        //
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, isArray = true)]
        public int[] checkDays { get; set; }

        public List<int> ListYear { get { return ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetYearList(DateTime.Now.AddYears(-1).Year, 3); } }

        public List<int> ListMonth { get { return ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetMonthList(1, 12); } }
        

        
    }
}