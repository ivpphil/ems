﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.sendmail;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.Send;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Store.Mappables;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Models.store;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class Store_carriage
        : ErsSiteRegisterModelBase, IStoreCarriageCommand, IStoreCarriageMappable
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        /// <summary>
        /// 修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool store_carriage_btn { get; set; }
        public Store_carriage()
        {
        }

        ///　一覧リスト
        [BindTable("allList")]
        public List<store_carriage_table> allList { get; set; }

        /// id
        [ErsSchemaValidation("pref_t.id")]
        public int? id { get; set; }

        /// 県名
        [ErsSchemaValidation("pref_t.pref_name")]
        public string pref_name { get; set; }

        /// 送料
        [ErsSchemaValidation("pref_t.carriage")]
        public int? carriage { get; set; }

        [HtmlDictionary("carriage")]
        [ErsSchemaValidation("pref_t.carriage")]
        public Dictionary<int?, int?> carriage_id { get; set; }
        
        /// サービス設定
        [ErsSchemaValidation("setup_t.free")]
        public int? free { get; set; }

        public EnumOnOff? enable_carriage_tax { get; set; }
    }
}