﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc.pager;
using System.ComponentModel;
using ersAdmin.Domain.Campaign.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class CommonScodeList
        : ErsModelBase, ICommonScodeListMappable
    {
        //ページ送り　現在ページ
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public bool hide_stock { get; set; }

        /// <summary>
        /// Get number of item on a merchandise list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        public long recordCount { get; set; }

        public ErsPagerModel pager { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [HtmlSubmitButton]
        public bool g_search_mode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.gcode")]
        public string s_gcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode")]
        public string s_scode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.sname")]
        public string s_sname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.cate1")]
        public int? s_cate1 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.cate2")]
        public int? s_cate2 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.cate3")]
        public int? s_cate3 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.cate4")]
        public int? s_cate4 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.cate5")]
        public int? s_cate5 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("g_master_t.doc_bundling_flg")]
        public EnumDocBundlingFlg s_doc_bundling_flg { get; set; }


        public List<Dictionary<string, object>> MeList { get; set; }

        public List<Dictionary<string, object>> CateList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().GetCategoryList(false, false,
                    this.s_cate1,
                    this.s_cate2,
                    this.s_cate3,
                    this.s_cate4,
                    this.s_cate5);
            }
        }

        public List<Dictionary<string, object>> CateValueList
        {
            get
            {
                return new List<Dictionary<string, object>> {
                    new Dictionary<string,object> { {"value", 0 } }, 
                    new Dictionary<string,object> { {"value", s_cate1.HasValue ? s_cate1.Value : 0 } }, 
                    new Dictionary<string,object> { {"value", s_cate2.HasValue ? s_cate2.Value : 0 } }, 
                    new Dictionary<string,object> { {"value", s_cate3.HasValue ? s_cate3.Value : 0 } }, 
                    new Dictionary<string,object> { {"value", s_cate4.HasValue ? s_cate4.Value : 0 } }, 
                    new Dictionary<string,object> { {"value", s_cate5.HasValue ? s_cate5.Value : 0 } }
                };
            }
        }
    }
}