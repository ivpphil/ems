﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using jp.co.ivp.ers.merchandise;
using ersAdmin.Models.campaign;
using jp.co.ivp.ers;
using ersAdmin.Domain.Campaign.Commands;
using ersAdmin.Domain.Campaign.Mappables;

namespace ersAdmin.Models
{
    public class target_modify
        : ErsSiteRegisterModelBase, ITargetModifyCommand, ITargetModifyMappable, ITargetDeleteCommand
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        public target_modify()
        {
            mode = "modify";
        }
        public virtual string mode { get;  set; }
        /// <summary>
        /// 戻り先URL
        /// </summary>
        public string returnUrl { get; internal set; }

        [ErsOutputHidden("modify")]
        [ErsSchemaValidation("target_t.id")]
        public int? id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.target_name")]
        public string target_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.recency_from")]
        public int? recency_from { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.recency_to")]
        public int? recency_to { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.frequency_from")]
        public int? frequency_from { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.frequency_to")]
        public int? frequency_to { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.monetary_from")]
        public int? monetary_from { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.monetary_to")]
        public int? monetary_to { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.order_ptn_kbn")]
        public EnumOrderPattern? order_ptn_kbn { get; set; }

        [ErsOutputHidden("modify")]
        [ErsSchemaValidation("target_t.intime")]
        public DateTime? intime { get; set; }

        [ErsOutputHidden("modify")]
        [ErsSchemaValidation("target_t.utime")]
        public DateTime? utime { get; set; }

        [ErsOutputHidden]
        [DisplayName("key")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? target_item_max { get; set; }

        [ErsOutputHidden]
        [DisplayName("key")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? targetexcluded_item_max { get; set; }

        [BindTable("scenario_item_table")]
        public List<scenario_item> scenario_item_table { get; set; }

        [BindTable("scenarioexcluded_item_table")]
        public List<scenario_item> scenarioexcluded_item_table { get; set; }

        public List<Dictionary<string, object>> order_ptn_kbn_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.OrderPattern, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> order_type_list
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.OrderType, EnumCommonNameColumnName.namename);
            }
        }

        public bool IsErrorBack { get; set; }
    }
}