﻿using System;
using System.ComponentModel;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Models.campaign
{
    public class scenario_item : ErsBindableModel, IScenarioItemListRecordCommand
    {

        [ErsOutputHidden]
        [ErsSchemaValidation("target_item_t.id")]
        [DisplayName("id")]
        public virtual int? id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_item_t.target_kbn")]
        [DisplayName("target_kbn")]
        public virtual EnumTargetKbn? target_kbn { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_item_t.gcode", requireAlphabet = true)]
        [DisplayName("gcode")]
        public virtual string gcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_item_t.scode", requireAlphabet = true)]
        [DisplayName("scode")]
        public virtual string scode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_item_t.order_type")]
        [DisplayName("order_type")]
        public virtual EnumOrderType? order_type { get; set; }

        public virtual EnumOrderType? disp_order_type { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        [DisplayName("sname")]
        public virtual string sname { get; set; }

        [DisplayName("Update_Time")]
        public virtual DateTime utime { get; set; }

    }
}