﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.order;
using ersAdmin.Models.csv;
using System.ComponentModel;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers;
using ersAdmin.Domain.Campaign.Mappables;

namespace ersAdmin.Models
{
    public class Campaign_search
        : ErsSiteSearchModelBase, ICampaignSearchMappable
    {
        /// <summary>
        /// 検索用モールショップ区分 [Mall shop division for search]
        /// </summary>
        public override EnumMallShopKbn? s_mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        public ErsPagerModel pager { get; internal set; }

        /// <summary>
        /// Get number of item on a merchandise list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("campaign_t.id")]
        public int? id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("campaign_t.ccode")]
        public string s_ccode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("campaign_t.active")]
        public EnumActive? s_active { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("campaign_t.campaign_name")]
        public string s_campaign_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("campaign_t.term_from")]
        public DateTime? s_term_from { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("campaign_t.term_to")]
        public DateTime? s_term_to { get; set; }

        /// <summary>
        ///  List 
        /// </summary>

        public List<Dictionary<string, object>> ActiveList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList( EnumCommonNameType.Active, EnumCommonNameColumnName.namename); }
        }

        //商品検索結果
        public List<Dictionary<string, object>> CampaignList { get; set; }

        public IList<ErsMember> mailToList { get; set; }

    }
}