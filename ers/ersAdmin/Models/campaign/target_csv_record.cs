﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using System.ComponentModel;
using jp.co.ivp.ers.mall;

namespace ersAdmin.Models.csv
{
    public class target_csv_record
        : ErsBindableModel
    {
        [CsvField]
        public virtual int? id { get; set; }

        [CsvField]
        public virtual string mcode { get; set; }

        [CsvField]
        public virtual string email { get; set; }

        //[CsvField]
        public virtual string passwd { get; set; }

        [CsvField]
        public virtual string lname { get; set; }

        [CsvField]
        public virtual string fname { get; set; }

        [CsvField]
        public virtual string lnamek { get; set; }

        [CsvField]
        public virtual string fnamek { get; set; }

        [CsvField]
        public virtual string compname { get; set; }

        [CsvField]
        public virtual string compnamek { get; set; }

        [CsvField]
        public virtual string division { get; set; }

        [CsvField]
        public virtual string divisionk { get; set; }

        //[CsvField]
        public virtual string tlname { get; set; }

        //[CsvField]
        public virtual string tfname { get; set; }

        //[CsvField]
        public virtual string tlnamek { get; set; }

        //[CsvField]
        public virtual string tfnamek { get; set; }

        [CsvField]
        public virtual string zip { get; set; }

        [CsvField]
        [DisplayName("pref")]
        public virtual string disp_pref
        {
            get
            {
                if (this.pref!=null)
                    return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(Convert.ToInt16(this.pref), (int)EnumSiteId.COMMON_SITE_ID);

                return "";
            }
        }

        public virtual int? pref { get; set; }

        [CsvField]
        public virtual string address { get; set; }

        [CsvField]
        public virtual string taddress { get; set; }

        [CsvField]
        public virtual string maddress { get; set; }

        [CsvField]
        public virtual string birth { get; set; }

        [CsvField]
        [DisplayName("sex")]
        public virtual string disp_sex
        {
            get {
                if (this.sex != null)
                {
                    var w_disp_sex = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename, (int)this.sex.Value);
                    if  (!string.IsNullOrEmpty(w_disp_sex))
                    {
                        return ErsResources.GetMessage(w_disp_sex);
                    }
                    
                }
                return "";
            }
        }

        public virtual EnumSex? sex { get; set; }

        [CsvField]
        [DisplayName("job")]
        public virtual string disp_job
        {
            get
            {
                if (this.job != null && this.job != 0)
                    return ErsFactory.ersViewServiceFactory.GetErsViewJobService().GetStringFromId(this.job);

                return "";
            }
        }

        public virtual int? job { get; set; }

        [CsvField]
        public virtual string tel { get; set; }

        [CsvField]
        public virtual string fax { get; set; }

        //[CsvField]
        [DisplayName("ques")]
        public virtual string disp_ques
        {
            get
            {
                if (this.ques != null)
                    return ErsFactory.ersViewServiceFactory.GetErsViewQuesService().GetStringFromId(Convert.ToInt16(this.ques));

                return "";
            }
        }

        public virtual short? ques { get; set; }

        //[CsvField]
        public virtual string ans { get; set; }

        [CsvField]
        public virtual int? sale { get; set; }

        [CsvField]
        [DisplayName("m_flg")]
        public virtual string disp_m_flg
        {
            get
            {
                if (this.m_flg != null)
                {
                    var w_m_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.MFlg, EnumCommonNameColumnName.namename, (int)this.m_flg.Value);
                    if (!string.IsNullOrEmpty(w_m_flg) )
                    {
                        return ErsResources.GetMessage(w_m_flg);
                    }
                }                    

                return "";
            }
        }

        public virtual EnumMFlg? m_flg { get; set; }

        [CsvField]
        [DisplayName("dm_flg")]
        public virtual string disp_dm_flg
        {
            get
            {
                var w_disp_dm_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.NEED, EnumCommonNameColumnName.namename, (int)this.dm_flg);
                if (!string.IsNullOrEmpty(w_disp_dm_flg)) {
                    return ErsResources.GetMessage(w_disp_dm_flg);
                }
                return string.Empty;
            }
        }

        public virtual EnumDmFlg dm_flg { get; set; }

        [CsvField]
        [DisplayName("out_bound_flg")]
        public virtual string disp_out_bound_flg
        {
            get
            {
                var w_disp_out_bound_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.NEED, EnumCommonNameColumnName.namename, (int)this.out_bound_flg);
                if (!string.IsNullOrEmpty(w_disp_out_bound_flg))
                {
                    return ErsResources.GetMessage(w_disp_out_bound_flg);
                }
                return string.Empty;
            }
        }

        public virtual EnumOutBoundFlg out_bound_flg { get; set; }


        [CsvField]
        public virtual string memo { get; set; }

        [CsvField]
        public virtual DateTime? intime { get; set; }

        [CsvField]
        public virtual DateTime? utime { get; set; }

        [CsvField]
        [DisplayName("country")]
        public virtual string disp_country
        {
            get
            {
                if (this.country != null)
                    return ErsFactory.ersViewServiceFactory.GetErsViewCountryService().GetStringFromId(Convert.ToInt16(this.country));

                return "";
            }
        }

        public virtual string country { get; set; }

        [CsvField]
        [DisplayName("age_code")]
        public virtual string disp_age_code
        {
            get
            {
                if (this.age_code != null)
                {
                    var w_age_code = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ORDAGE, EnumCommonNameColumnName.namename, this.age_code);
                    if (!string.IsNullOrEmpty(w_age_code))
                    {
                        return ErsResources.GetMessage(w_age_code);
                    }
                }
                return "";
            }
        }

        public virtual int? age_code { get; set; }

        [CsvField]
        [DisplayName("member_rank")]
        public virtual string disp_member_rank 
        {
            get
            {
                if (this.rank != null)
                {
                    var rank_name = ErsFactory.ersMemberFactory.GetErsMemberRankSetupWithRank(Convert.ToInt32(this.rank), this.site_id);

                    if (rank_name != null)
                    {
                        return rank_name.rank_name;
                    }
                }
                return "";

            }
        }

        public virtual int? rank { get; set; }

        public virtual int? site_id { get; set; }

        [CsvField]
        [DisplayName("store_site_name")]
        public virtual string site_name
        {
            get
            {
                if (this.site_id == null || this.site_id == 0)
                {
                    return "共通";
                }
                return ErsMallFactory.ersMallViewServiceFactory.GetErsViewSiteService().GetStringFromId(this.site_id);
            }
        }
    }
}