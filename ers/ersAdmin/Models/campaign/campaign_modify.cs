﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using jp.co.ivp.ers.merchandise;
using ersAdmin.Models.campaign;
using jp.co.ivp.ers;
using ersAdmin.Domain.Campaign.Commands;
using ersAdmin.Domain.Campaign.Mappables;

namespace ersAdmin.Models
{
    public class campaign_modify
        : ErsSiteRegisterModelBase, ICampaignModifyCommand, ICampaignModifyMappable, ICampaignDeleteCommand
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        public campaign_modify()
        {
            mode = "modify";
        }

        public virtual string mode { get; internal set; }
        /// <summary>
        /// 戻り先URL
        /// </summary>
        public string returnUrl { get; internal set; }

        [ErsSchemaValidation("campaign_t.id")]
        public int? id { get; set; }

        [ErsSchemaValidation("campaign_t.ccode")]
        public string ccode { get; set; }

        [ErsSchemaValidation("campaign_t.active")]
        public int? active { get; set; }

        [ErsSchemaValidation("campaign_t.campaign_name")]
        public string campaign_name { get; set; }

        [ErsSchemaValidation("campaign_t.term_from")]
        public DateTime? term_from { get; set; }

        [ErsSchemaValidation("campaign_t.term_to")]
        public DateTime? term_to { get; set; }

        [ErsSchemaValidation("campaign_t.scode", requireAlphabet = true)]
        public string scode { get; set; }

        [ErsSchemaValidation("campaign_t.advertise_name")]
        public string advertise_name { get; set; }

        [ErsSchemaValidation("campaign_t.advertise_code")]
        public string advertise_code { get; set; }

        [ErsSchemaValidation("campaign_t.advertise_cost")]
        public int? advertise_cost { get; set; }

        [ErsSchemaValidation("campaign_t.advertise_area")]
        public string advertise_area { get; set; }

        [ErsSchemaValidation("campaign_t.remarks")]
        public string remarks { get; set; }

        [ErsSchemaValidation("campaign_t.target_id")]
        public string target_id { get; set; }
        
        [ErsSchemaValidation("campaign_t.intime")]
        public DateTime? intime { get; set; }
        
        [ErsSchemaValidation("campaign_t.utime")]
        public DateTime? utime { get; set; }

        [BindTable("detail_table")]
        public List<Campaign_modify_detail> detail_table { get; set; }

        [BindTable("target_detail_table")]
        public List<Campaign_modify_target_s_list> target_detail_table { get; set; }

        [ErsSchemaValidation("s_master_t.sname")]
        public string sname { get; set; }

        [DisplayName("key")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? included_item_max { get; set; }

        [DisplayName("key")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? target_item_max { get; set; }

        [DisplayName("key")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? trigger_item_max { get; set; }

        public List<Dictionary<string, object>> ActiveList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Active, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> TargetList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewTargetService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> TriggerConditionList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.TriggerCondition, EnumCommonNameColumnName.namename); }
        }

        
        

    }
}