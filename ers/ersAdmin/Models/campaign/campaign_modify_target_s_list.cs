﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers.merchandise;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using ersAdmin.Domain.Campaign.Commands;
using ersAdmin.Domain.Campaign.Mappables;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Models.campaign
{
    /// <summary>
    /// 対象商品リストクラス
    /// </summary>
    public class Campaign_modify_target_s_list
    : ErsBindableModel, ICampaignModifyTargetListRecordCommand
    {
        /// <summary>
        /// target scode
        /// </summary>
        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]
        [DisplayName("scode")]
        public string target_scode { get; set; }

        /// <summary>
        /// target sname
        /// </summary>
        [ErsSchemaValidation("s_master_t.sname")]
        [DisplayName("sname")]
        public string target_sname { get; set; }

        /// <summary>
        /// target s_sale_ptn
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        [DisplayName("s_sale_ptn")]
        public EnumOrderType? target_order_type { get; set; }

        public string mode { get; set; }

        /// <summary>
        /// returns true if target_scode and target_sname are null
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            return (target_scode == null
                && target_sname == null
                && target_order_type == null
                );
        }

    }
}