﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.order;
using ersAdmin.Models.csv;
using System.ComponentModel;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using ersAdmin.Domain.Campaign.Mappables;

namespace ersAdmin.Models
{
    public class Target_search
        : ErsSiteSearchModelBase, ITargetSearchMappable
    {
        /// <summary>
        /// 検索用モールショップ区分 [Mall shop division for search]
        /// </summary>
        public override EnumMallShopKbn? s_mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        public ErsPagerModel pager { get; set; }

        /// <summary>
        /// Get number of item on a merchandise list page 
        /// </summary>
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("s_intime_from")]
        public DateTime? s_intime_from { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("s_intime_to")]
        public DateTime? s_intime_to { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.target_name")]
        public string s_target_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.recency_from")]
        public int? s_recency_from { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.recency_to")]
        public int? s_recency_to { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.frequency_from")]
        public int? s_frequency_from { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.frequency_to")]
        public int? s_frequency_to { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.monetary_from")]
        public int? s_monetary_from { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("target_t.monetary_to")]
        public int? s_monetary_to { get; set; }

        //ターゲット検索結果
        public List<Dictionary<string, object>> TargetList { get;  set; }


        
        

    }
}