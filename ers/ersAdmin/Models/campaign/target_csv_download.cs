﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using ersAdmin.Domain.Campaign.Mappables;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class target_csv_download
        : ErsModelBase, ITargetCsvDownloadMappable, ITargetCsvDownloadCommand
    {
        public target_csv_download()
        {
            this.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
        }

        public ErsCsvCreater csvCreater { get; set; }

        [ErsOutputHidden("modify")]
        [ErsSchemaValidation("target_t.id")]
        public int? id { get; set; }
    }
}