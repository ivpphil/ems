﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Models.campaign
{
    /// <summary>
    /// 同梱商品リストクラス
    /// </summary>
    public class Campaign_modify_detail
    : ErsBindableModel, ICampaignModifyDetailListRecordCommand
    {
        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]
        [DisplayName("scode")]
        public string inc_scode { get; set; }

        [ErsSchemaValidation("s_master_t.sname")]
        [DisplayName("sname")]
        public string inc_sname { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        [DisplayName("amount")]
        public int? inc_amount { get; set; }

        [ErsSchemaValidation("doc_bundling_t.duplicate")]
        [DisplayName("duplicate")]
        public EnumDocDuplicate? inc_duplicate { get; set; }

        public string mode { get; set; }

    }
}