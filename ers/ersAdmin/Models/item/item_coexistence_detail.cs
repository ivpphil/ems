﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using ersAdmin.Domain.Item.Commands;

namespace ersAdmin.Models
{
    public class Item_coexistence_detail
        : ErsBindableModel, IItemCoexistenceDetailListRecordCommand
    {
        [ErsSchemaValidation("mixed_group_t.id")]
        public virtual int? id { get; set; }

        [ErsSchemaValidation("mixed_group_t.mixed_group_code")]
        public virtual string mixed_group_code { get; set; }

        [ErsSchemaValidation("mixed_group_t.mixed_group_name")]
        public virtual string mixed_group_name { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual short? delete { get; set; }
    }
}
