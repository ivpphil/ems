﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.merchandise.strategy;
using jp.co.ivp.ers;
using System.ComponentModel;
using ersAdmin.Domain.Item.Commands;

namespace ersAdmin.Models.csv
{
    public class Set_Item_csv_record
        : ErsBindableModel, ISetItemCSVRecordCommand
    {
        [CsvField]
        [ErsSchemaValidation("set_master_t.parent_scode")]
        [DisplayName("set_master_t.parent_scode")]
        public virtual string parent_scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("set_master_t.scode", requireAlphabet = true)]
        [DisplayName("set_master_t.scode")]
        public virtual string scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("set_master_t.amount")]
        public virtual int? amount { get; set; }
    }
}