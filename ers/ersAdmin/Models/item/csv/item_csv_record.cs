﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.merchandise.strategy;
using System.ComponentModel;
using jp.co.ivp.ers;
using ersAdmin.Domain.Item.Commands;
using ersAdmin.Domain.Item.Mappables;

namespace ersAdmin.Models.csv
{
    public class Item_csv_record
        : ErsBindableModel, IItemCSVRecordCommand, IItemCSVRecordMappable
    {
        [CsvField]
        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]
        public virtual string scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.jancode", requireAlphabet = true)]
        public virtual string jancode { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.gcode", requireAlphabet = true)]
        public virtual string gcode { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.gname")]
        public virtual string gname { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.m_gname")]
        public virtual string m_gname { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.sname")]
        public virtual string sname { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.m_sname")]
        public virtual string m_sname { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.cts_sname")]
        public virtual string cts_sname { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.shipping_sname")]
        public virtual string shipping_sname { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.date_from")]
        public virtual DateTime? date_from { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.date_to")]
        public virtual DateTime? date_to { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.s_sale_ptn")]
        public virtual EnumSalePatternType? s_sale_ptn { get; set; }

        public virtual string w_s_sale_ptn { get { return ErsFactory.ersViewServiceFactory.GetErsViewSalePtnService().GetStringFromId(this.s_sale_ptn); } }

        [CsvField]
        [ErsSchemaValidation("g_master_t.stock_flg")]
        public virtual EnumStockFlg? stock_flg { get; set; }

        public virtual string w_stock_flg { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.StockFlg, EnumCommonNameColumnName.namename, (int?)stock_flg); } }

        [CsvField]
        [ErsSchemaValidation("g_master_t.stock_set1")]
        public virtual int? stock_set1 { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.stock_set2")]
        public virtual int? stock_set2 { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.soldout_flg")]
        public virtual EnumSoldoutFlg? soldout_flg { get; set; }

        public virtual string w_soldout_flg { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Active, EnumCommonNameColumnName.namename, (int?)soldout_flg); } }

        [CsvField]
        [DisplayName("cate1")]
        [ErsUniversalValidation(regExpPattern = @"^[\$0-9]*$", messageId = "10008")]
        public virtual string cate1 { get; set; }

        [CsvField]
        [DisplayName("cate2")]
        [ErsUniversalValidation(regExpPattern = @"^[\$0-9]*$", messageId = "10008")]
        public virtual string cate2 { get; set; }

        [CsvField]
        [DisplayName("cate3")]
        [ErsUniversalValidation(regExpPattern = @"^[\$0-9]*$", messageId = "10008")]
        public virtual string cate3 { get; set; }

        [CsvField]
        [DisplayName("cate4")]
        [ErsUniversalValidation(regExpPattern = @"^[\$0-9]*$", messageId = "10008")]
        public virtual string cate4 { get; set; }

        [CsvField]
        [DisplayName("cate5")]
        [ErsUniversalValidation(regExpPattern = @"^[\$0-9]*$", messageId = "10008")]
        public virtual string cate5 { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.recommend1")]
        public virtual string recommend1 { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.recommend2")]
        public virtual string recommend2 { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.recommend3")]
        public virtual string recommend3 { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.recommend4")]
        public virtual string recommend4 { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.recommend5")]
        public virtual string recommend5 { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.sort")]
        public virtual int? sort { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.disp_send_ptn")]
        public virtual string disp_send_ptn { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.disp_list_flg")]
        public virtual EnumDisp_list_flg? disp_list_flg { get; set; }

        public virtual string w_disp_list_flg { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.DispFlg, EnumCommonNameColumnName.namename, (int?)disp_list_flg); } }

        [CsvField]
        [ErsSchemaValidation("g_master_t.carriage_cost_type")]
        public virtual EnumCarriageCostType? carriage_cost_type { get; set; }

        public virtual string w_carriage_cost_type { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.CarriageCostType, EnumCommonNameColumnName.namename, (int?)carriage_cost_type); } }

        [CsvField]
        [ErsSchemaValidation("g_master_t.plural_order_type")]
        public virtual EnumPluralOrderType? plural_order_type { get; set; }

        public virtual string w_plural_order_type { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.PluralOrderType, EnumCommonNameColumnName.namename, (int?)plural_order_type); } }

        [CsvField]
        [ErsSchemaValidation("g_master_t.set_flg")]
        public virtual EnumSetFlg? set_flg { get; set; }

        public virtual string w_set_flg { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Active, EnumCommonNameColumnName.namename, (int?)set_flg); } }

        [CsvField]
        [ErsSchemaValidation("g_master_t.doc_bundling_flg")]
        public virtual EnumDocBundlingFlg? doc_bundling_flg { get; set; }

        public virtual string w_doc_bundling_flg { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Active, EnumCommonNameColumnName.namename, (int?)doc_bundling_flg); } }

        [CsvField]
        [ErsSchemaValidation("g_master_t.deliv_method")]
        public virtual EnumDelvMethod? deliv_method { get; set; }

        public virtual string w_deliv_method { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.DelvMethod, EnumCommonNameColumnName.namename, (int?)deliv_method); } }

        [CsvField]
        [DisplayName("g_master_t.active")]
        [ErsSchemaValidation("g_master_t.active")]
        public virtual EnumActive? active { get; set; }

        public virtual string w_active { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Active, EnumCommonNameColumnName.namename, (int?)active); } }

        [CsvField]
        [ErsSchemaValidation("s_master_t.active")]
        public virtual EnumActive? s_active { get; set; }

        public virtual string w_s_active { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Active, EnumCommonNameColumnName.namename, (int?)s_active); } }

        [CsvField]
        [ErsSchemaValidation("price_t.price")]
        public virtual int? price { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.price2")]
        public virtual int? price2 { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.price")]
        [DisplayName("regular_price")]
        public virtual int? regular_price { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.price")]
        [DisplayName("regular_first_price")]
        public virtual int? regular_first_price { get; set; }

        [CsvField]
        [ErsSchemaValidation("price_t.cost_price")]
        public int? cost_price { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.supplier_code", requireAlphabet = true)]
        public string supplier_code { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.maker_scode")]
        public string maker_scode { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.wh_order_type")]
        public EnumWhOrderType wh_order_type { get; set; }

        public virtual string w_wh_order_type { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.WhOrderType, EnumCommonNameColumnName.namename, (int?)wh_order_type); } }

        [CsvField]
        [ErsSchemaValidation("s_master_t.point")]
        public virtual int? point { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.attribute1")]
        public virtual string attribute1 { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.attribute2")]
        public virtual string attribute2 { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.disp_order")]
        public virtual int? disp_order { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.disp_keyword")]
        public virtual string[] disp_keyword { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.metatitle")]
        public virtual string metatitle { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.metadescription")]
        public virtual string metadescription { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.metawords")]
        public virtual string metawords { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.link_url")]
        public virtual string link_url { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.description")]
        public virtual string description { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.m_description")]
        public virtual string m_description { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.description_for_list")]
        public virtual string description_for_list { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.mixed_group_code")]
        public virtual string mixed_group_code { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.max_purchase_count")]
        public virtual int? max_purchase_count { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.point_campaign_from")]
        public virtual DateTime? point_campaign_from { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.point_campaign_to")]
        public virtual DateTime? point_campaign_to { get; set; }

        [CsvField]
        [ErsSchemaValidation("s_master_t.campaign_point")]
        public virtual int? campaign_point { get; set; }

        [CsvField]
        [ErsSchemaValidation("stock_t.stock")]
        public virtual int? stock { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.intime")]
        public virtual DateTime? intime { get; set; }

        [CsvField]
        [ErsSchemaValidation("g_master_t.site_id")]
        public virtual int[] site_id { get; set; }

        //csvリスト表示用
        public string cate1s { get { return (cate_s(cate1)); } }
        public string cate2s { get { return (cate_s(cate2)); } }
        public string cate3s { get { return (cate_s(cate3)); } }
        public string cate4s { get { return (cate_s(cate4)); } }
        public string cate5s { get { return (cate_s(cate5)); } }

        public string cate_s(string cate)
        {
            if (string.IsNullOrEmpty(cate))
            {
                return null;
            }

            var array = cate.Split('$');
            return String.Join(",", array);
        }

        /// <summary>
        /// disp_send_ptn
        /// </summary>
        /// <param name="send_ptn"></param>
        /// <returns></returns>
        public string disp_send_ptn_disp
        {
            get
            {
                string disp_send_ptn = "";
                if (this.disp_send_ptn != null)
                {
                    disp_send_ptn = VBStrings.Right("000" + this.disp_send_ptn, 3);
                }
                return disp_send_ptn;
            }
        }
    }
}