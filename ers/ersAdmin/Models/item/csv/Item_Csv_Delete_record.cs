﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Models.csv
{
    public class Item_Csv_Delete_record
        : ErsBindableModel, IItemCSVDeleteRecordCommand, IItemCSVDeleteRecordMappable
    {

        [CsvField]
        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]
        public virtual string scode { get; set; }

        [CsvField]
        public virtual string jancode { get; set; }

        [CsvField]
        public virtual string gcode { get; set; }

        [CsvField]
        public virtual string gname { get; set; }

        [CsvField]
        public virtual string m_gname { get; set; }

        [CsvField]
        public virtual string sname { get; set; }

        [CsvField]
        public virtual string m_sname { get; set; }

        [CsvField]
        public virtual string cts_sname { get; set; }

        [CsvField]
        public virtual string shipping_sname { get; set; }

        [CsvField]
        public virtual DateTime? date_from { get; set; }

        [CsvField]
        public virtual DateTime? date_to { get; set; }

        [CsvField]
        public virtual short? s_sale_ptn { get; set; }

        [CsvField]
        public virtual EnumStockFlg? stock_flg { get; set; }

        [CsvField]
        public virtual int? stock_set1 { get; set; }

        [CsvField]
        public virtual int? stock_set2 { get; set; }

        [CsvField]
        public virtual EnumSoldoutFlg? soldout_flg { get; set; }

        [CsvField]
        public virtual string cate1 { get; set; }

        [CsvField]
        public virtual string cate2 { get; set; }

        [CsvField]
        public virtual string cate3 { get; set; }

        [CsvField]
        public virtual string cate4 { get; set; }

        [CsvField]
        public virtual string cate5 { get; set; }

        [CsvField]
        public virtual string recommend1 { get; set; }

        [CsvField]
        public virtual string recommend2 { get; set; }

        [CsvField]
        public virtual string recommend3 { get; set; }

        [CsvField]
        public virtual string recommend4 { get; set; }

        [CsvField]
        public virtual string recommend5 { get; set; }

        [CsvField]
        public virtual int? sort { get; set; }

        [CsvField]
        public virtual string disp_send_ptn { get; set; }

        [CsvField]
        public virtual EnumDisp_list_flg? disp_list_flg { get; set; }

        [CsvField]
        public virtual EnumCarriageCostType? carriage_cost_type { get; set; }

        [CsvField]
        public virtual EnumPluralOrderType? plural_order_type { get; set; }

        [CsvField]
        public virtual EnumSetFlg? set_flg { get; set; }

        [CsvField]
        public virtual EnumDocBundlingFlg? doc_bundling_flg { get; set; }

        [CsvField]
        public virtual EnumDelvMethod? deliv_method { get; set; }

        [CsvField]
        public virtual EnumActive? active { get; set; }

        [CsvField]
        public virtual short? s_active { get; set; }

        [CsvField]
        public virtual int? price { get; set; }

        [CsvField]
        public virtual int? price2 { get; set; }

        [CsvField]
        public virtual int? regular_price { get; set; }

        [CsvField]
        public virtual int? regular_first_price { get; set; }

        [CsvField]
        public virtual int? price_ex_tax { get; set; }

        [CsvField]
        public virtual int? regular_price_ex_tax { get; set; }

        [CsvField]
        public virtual int? point { get; set; }

        [CsvField]
        public virtual string attribute1 { get; set; }

        [CsvField]
        public virtual string attribute2 { get; set; }

        [CsvField]
        public virtual int? disp_order { get; set; }

        [CsvField]
        public virtual string keyword { get; set; }

        [CsvField]
        public virtual string metatitle { get; set; }

        [CsvField]
        public virtual string metadescription { get; set; }

        [CsvField]
        public virtual string metawords { get; set; }

        [CsvField]
        public virtual string link_url { get; set; }

        [CsvField]
        public virtual string description { get; set; }

        [CsvField]
        public virtual string m_description { get; set; }

        [CsvField]
        public virtual string description_for_list { get; set; }

        [CsvField]
        public virtual string mixed_group_code { get; set; }

        [CsvField]
        public virtual int? max_purchase_count { get; set; }

        [CsvField]
        public virtual DateTime? point_campaign_from { get; set; }

        [CsvField]
        public virtual DateTime? point_campaign_to { get; set; }

        [CsvField]
        public virtual int? campaign_point { get; set; }

        [CsvField]
        public virtual int? stock { get; set; }

        [CsvField]
        public virtual DateTime? intime { get; set; }

        [CsvField]
        public virtual DateTime? utime { get; set; }

        //csvリスト表示用
        public string cate1s { get { return (cate_s(cate1)); } }
        public string cate2s { get { return (cate_s(cate2)); } }
        public string cate3s { get { return (cate_s(cate3)); } }
        public string cate4s { get { return (cate_s(cate4)); } }
        public string cate5s { get { return (cate_s(cate5)); } }

        public string cate_s(string cate)
        {
            if (string.IsNullOrEmpty(cate))
            {
                return null;
            }

            var array = cate.Split('$');
            return String.Join(",", array);
        }

        /// <summary>
        /// disp_send_ptn
        /// </summary>
        /// <param name="send_ptn"></param>
        /// <returns></returns>
        public string disp_send_ptn_disp
        {
            get
            {
                string disp_send_ptn = "";
                if (this.disp_send_ptn != null)
                {
                    disp_send_ptn = VBStrings.Right("000" + this.disp_send_ptn, 3);
                }
                return disp_send_ptn;
            }
        }

    }
}