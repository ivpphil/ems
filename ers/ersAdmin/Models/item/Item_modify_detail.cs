﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.Web;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers;
using ersAdmin.Domain.Item.Mappables;
using ersAdmin.Domain.Item.Commands;
using System.ComponentModel;
using ersAdmin.Models.item;

namespace ersAdmin.Models
{
    public class Item_modify_detail
        : ErsBindableModel, IItemModifyDetailListRecordMappable, IItemModifyDetailListRecordCommand
    {
        public const int MaxPurchaseCountViaMail = 1;

        public override string lineName
        {
            get
            {
                if (string.IsNullOrEmpty(this.scode))
                    return base.lineName;
                else
                    return base.lineName + " " + ErsResources.GetMessage("line_name_scode", this.scode);
            }
        }

        #region Image uploader property
        public virtual string image_identifier
        {
            get
            {
                return ErsContext.sessionState.Get("admin_ransu") + "item_bimg" + this.scode;
            }
        }

        public virtual string item_bimg_temp_folder
        {
            get
            {
                return "item_bimg";
            }
        }

        public bool IsInitialize { get; set; }

        #endregion

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]
        public virtual string old_scode { get; set; }

        public virtual string gcode { get; internal set; }

        [HtmlSubmitButton]
        public virtual bool delete { get; set; }

        [ErsSchemaValidation("s_master_t.scode")]
        public virtual string scode { get; set; }

        [ErsSchemaValidation("s_master_t.jancode", requireAlphabet = true)]
        public virtual string jancode { get; set; }

        [ErsSchemaValidation("s_master_t.sname")]
        public virtual string sname { get; set; }

        [ErsSchemaValidation("s_master_t.m_sname")]
        public virtual string m_sname { get; set; }

        [ErsSchemaValidation("price_t.price")]
        public virtual int? price { get; set; }

        [ErsSchemaValidation("price_t.price2")]
        public virtual int? price2 { get; set; }

        [ErsSchemaValidation("price_t.cost_price")]
        public virtual int? cost_price { get; set; }

        [ErsSchemaValidation("price_t.price")]
        [DisplayName("price_t.regular_price")]
        public virtual int? regular_price { get; set; }

        [ErsSchemaValidation("price_t.price")]
        [DisplayName("price_t.regular_first_price")]
        public virtual int? regular_first_price { get; set; }

        [ErsSchemaValidation("s_master_t.point")]
        public virtual int? point { get; set; }

        [ErsSchemaValidation("stock_t.stock")]
        public virtual int? stock { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("stock_t.stock")]
        [DisplayName("stock")]
        public int? old_stock { get; set; }

        [ErsSchemaValidation("s_master_t.soldout_flg")]
        public virtual EnumSoldoutFlg? soldout_flg { get; set; }

        [ErsSchemaValidation("s_master_t.attribute1")]
        public virtual string attribute1 { get; set; }

        [ErsSchemaValidation("s_master_t.attribute2")]
        public virtual string attribute2 { get; set; }

        [ErsSchemaValidation("s_master_t.supplier_code", requireAlphabet = true)]
        public virtual string supplier_code { get; set; }

        [ErsSchemaValidation("s_master_t.maker_scode")]
        public virtual string maker_scode { get; set; }

        [ErsSchemaValidation("s_master_t.wh_order_type")]
        public virtual EnumWhOrderType? wh_order_type { get; set; }
        
        [ErsSchemaValidation("s_master_t.disp_order")]
        public virtual int? disp_order { get; set; }

        [ErsSchemaValidation("s_master_t.mixed_group_code")]
        public virtual string mixed_group_code { get; set; }

        [ErsSchemaValidation("s_master_t.max_purchase_count")]
        public virtual int? max_purchase_count { get; set; }

        [ErsSchemaValidation("s_master_t.point_campaign_from")]
        public virtual DateTime? point_campaign_from { get; set; }

        [ErsSchemaValidation("s_master_t.point_campaign_to")]
        public virtual DateTime? point_campaign_to { get; set; }

        [ErsSchemaValidation("s_master_t.campaign_point")]
        public virtual int? campaign_point { get; set; }

        [ErsSchemaValidation("s_master_t.active")]
        public virtual EnumActive? active { get; set; }

        [ErsSchemaValidation("s_master_t.cts_sname")]
        public virtual string cts_sname { get; set; }

        [ErsSchemaValidation("s_master_t.shipping_sname")]
        public virtual string shipping_sname { get; set; }

        [ErsSchemaValidation("g_master_t.gcode")]
        public virtual string s_gcode { get; set; }


        [BindPicture(EnumPictureType.JPEG)]
        public HttpPostedFileBase bimg1 { get; set; }

        [BindPicture(EnumPictureType.JPEG)]
        public HttpPostedFileBase bimg2 { get; set; }

        [BindPicture(EnumPictureType.JPEG)]
        public HttpPostedFileBase bimg3 { get; set; }

        [BindPicture(EnumPictureType.JPEG)]
        public HttpPostedFileBase bimg4 { get; set; }

        [BindPicture(EnumPictureType.JPEG)]
        public HttpPostedFileBase bimg5 { get; set; }

        [HtmlSubmitButton]
        public bool disabled_file_uploader { get; set; }

        [ErsOutputHidden]
        [BindTable("item_bimg_detail")]
        public IList<item_bimg_detail> item_bimg_detail { get; set; }

        public bool IsEmpty()
        {
            return (scode == null
                && jancode == null
                && sname == null
                && m_sname == null
                && price == null
                && price2 == null
                && cost_price == null
                && point == null
                && stock == null
                && soldout_flg == null
                && attribute1 == null
                && attribute2 == null
                && disp_order == null
                && mixed_group_code == null
                && max_purchase_count == null
                && point_campaign_from == null
                && point_campaign_to == null
                && campaign_point == null
                && active == null
                && cts_sname == null
                && shipping_sname == null
                && wh_order_type == null
                && supplier_code == null
                );
        }

        public ErsSku objSku { get; set; }

    }
}
