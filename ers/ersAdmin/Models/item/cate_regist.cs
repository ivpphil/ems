﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.merchandise.category;
using jp.co.ivp.ers.db;
using ersAdmin.Models.item;
using jp.co.ivp.ers;
using ersAdmin.Domain.Item.Commands;
using ersAdmin.Domain.Item.Mappables;

namespace ersAdmin.Models
{
    public class CateRegist
        : ErsSiteRegisterModelBase, ICateRegistCommand, ICateRegistMappable
    {
        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        protected ErsViewCategoryService categoryService;

        public CateRegist()
        {
            this.complete_title = ErsResources.GetFieldName("complete_title_item_cateregist");
        }

        public string complete_title { get; private set; }

        public string complete_back_url { get; private set; }

        /// <summary>
        /// カテゴリヘッダの更新ボタンが押されたか否か
        /// </summary>
        [HtmlSubmitButton]
        public bool registCategory { get; set; }

        /// <summary>
        /// カテゴリアイテムの更新ボタンが押されたか否か
        /// </summary>
        [HtmlSubmitButton]
        public bool registCategoryItem { get; set; }

        [HtmlSubmitButton]
        public bool cate_edit_1 { get; set; }

        [HtmlSubmitButton]
        public bool cate_edit_2 { get; set; }

        [HtmlSubmitButton]
        public bool cate_edit_3 { get; set; }

        [HtmlSubmitButton]
        public bool cate_edit_4 { get; set; }

        [HtmlSubmitButton]
        public bool cate_edit_5 { get; set; }

        public string cateName 
        { 
            get
            {
                if (categoryNumber == null || this.site_id == null) { return null; } ;
                return ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().getCateName(categoryNumber, Convert.ToInt32(this.site_id)); 
            } 
        }

        /// <summary>
        /// カテゴリヘッダの表示用リスト
        /// </summary>
        [BindTable("cate_header_table")]
        public List<Cate_header> cate_header_table { get; set; }

        [BindTable("cate_body_table")]
        public List<Cate_item> cate_body_table { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? categoryNumber { get; set; }
    }
}