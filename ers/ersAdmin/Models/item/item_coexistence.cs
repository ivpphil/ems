﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.basket;
using System.Data;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Item.Commands;
using ersAdmin.Domain.Item.Mappables;

namespace ersAdmin.Models
{
    public class Item_coexistence
        : ErsModelBase, IItemCoexistenceCommand, IItemCoexistenceMappable
    {

        /// リスト
        [BindTable("mixed_group_table")]
        public List<Item_coexistence_detail> mixedGroupList { get; set; }

    }
}