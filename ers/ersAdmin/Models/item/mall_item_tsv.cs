﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.Web.Mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.merchandise.strategy;
using ersAdmin.Domain.Item.Commands;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers;
using System.ComponentModel;
using ersAdmin.Models.item.mall.rakuten;
using ersAdmin.Models.item.mall.yahoo;
using ersAdmin.Models.item.mall.amazon.health;
using ersAdmin.Models.item.tsv;

namespace ersAdmin.Models
{
    public class Mall_item_tsv
        : ErsModelBase, IMallCSVCommand, IMallTSVMappable
    {
        /// <summary>
        /// Gets display message on finish page.
        /// </summary>
        public virtual string resultMsg
        {
            get
            {
                int validUpdatedRecords = 0;
                if (this.tsv_rakuten == null && this.tsv_yahoo == null && this.tsv_amazon == null)
                {
                    return string.Empty;
                }

                if (this.tsv_rakuten != null && this.mall_shop_kbn == EnumMallShopKbn.RAKUTEN)
                    validUpdatedRecords = this.tsv_rakuten.validIndexes.Count();
                else if (this.tsv_yahoo != null && this.mall_shop_kbn == EnumMallShopKbn.YAHOO)
                    validUpdatedRecords = this.tsv_yahoo.validIndexes.Count();
                else if (this.tsv_amazon != null && this.mall_shop_kbn == EnumMallShopKbn.AMAZON)
                    validUpdatedRecords = this.tsv_amazon.validIndexes.Count();

                return ErsResources.GetMessage("30000", validUpdatedRecords);
            }
        }

        /// <summary>
        /// 1行目をスキップする場合はtrue
        /// <para>If you want to skip the first line, value must be true</para>
        /// </summary>
        [ErsOutputHidden]
        [HtmlSubmitButton]
        public virtual bool chk_find { get; set; }

        /// <summary>
        /// 登録ボタンの押下
        /// </summary>
        [HtmlSubmitButton]
        public virtual bool regist { get; set; }


        // <summary>
        /// アップロードデータ
        /// <para>Upload data</para>
        /// </summary>
        [ErsOutputHidden]
        [BindCsvFile]
        public ErsCsvContainer<CsvRakutenItemRecord> tsv_rakuten { get; set; }

        // <summary>
        /// アップロードデータ
        /// <para>Upload data</para>
        /// </summary>
        [ErsOutputHidden]
        [BindCsvFile]
        public ErsCsvContainer<CsvYahooItemRecord> tsv_yahoo { get; set; }

        // <summary>
        /// アップロードデータ
        /// <para>Upload data</para>
        /// </summary>
        [ErsOutputHidden]
        [BindCsvFile]
        public ErsCsvContainer<CsvAmazonItemRecord> tsv_amazon { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("mall_s_master_t.site_id")]
        [DisplayName("site_id")]
        public int? site_id { get; set; }

        /// <summary>
        /// Identifier Upload CSV
        /// </summary>
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeTo = 3)]
        [DisplayName("mall_flg")]
        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        public List<Item_tsv_mall_list> listMallList { get; set; }
    }
}