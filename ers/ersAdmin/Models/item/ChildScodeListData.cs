﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using ersAdmin.Domain.Item.Commands;

namespace ersAdmin.Models.item
{
    public class ChildScodeListData : ErsBindableModel, IChildScodeListCommand
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.id")]
        public int? id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("set_master_t.scode", requireAlphabet = true)]
        public string scode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.sname")]
        public string sname { get; set; }

        [ErsSchemaValidation("set_master_t.amount")]
        public int? amount { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool deleted { get; set; }
    }
}