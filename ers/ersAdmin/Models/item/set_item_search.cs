﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.util;

namespace ersAdmin.Models.item
{
    public class set_item_search
        : ErsModelBase, ISetItemSearchMappable, ISetItemListCSVMappable
    {
        public ErsPagerModel pager { get; set; }

        public virtual long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long recordCount { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode")]
        public string s_parent_scode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("set_master_t.scode")]
        public string s_child_scode { get; set; }

        public IEnumerable<ErsMerchandise> setItemList { get; set; }

        public ErsCsvCreater csvCreater { get; set; }
    }
}