﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Item.Commands;

namespace ersAdmin.Models.item.mall.amazon.health
{
    public class Item_modify_amazon_detail
        : Item_modify_mall_detail, IItemModifyMallAmazonDetailListRecordCommand
    {
        public override string lineName
        {
            get
            {
                mallTabName = "Amazon";
                return base.lineName;
            }
        }
        /// <summary>
        /// JANコード [JAN code]
        /// </summary>
        [DisplayName("amazon.jp.external_product_id")]
        [ErsSchemaValidation("s_master_t.jancode", requireAlphabet = true)]
        [ErsOutputHidden("input")]
        public override string jancode { get; set; }

        /// <summary>
        /// 商品コード [SKU code]
        /// </summary>
        [DisplayName("amazon.jp.item_sku")]
        [ErsSchemaValidation("s_master_t.scode")]
        [ErsOutputHidden("input")]
        public override string scode { get; set; }

        /// <summary>
        /// 商品タイプ [PURODUCT TYPE]
        /// </summary>
        [DisplayName("amazon.jp.feed_product_type")]
        [ErsSchemaValidation("mall_s_master_t.product_type")]
        public virtual string product_type { get; set; }

        /// <summary>
        /// 販売価格 [PRICE]
        /// </summary>
        [DisplayName("amazon.jp.standard_price")]
        [ErsSchemaValidation("mall_s_master_t.price")]
        public override int? price { get; set; }

        /// <summary>
        /// ブランド [SKU NAME]
        /// </summary>
        [DisplayName("amazon.jp.brand_name")]
        [ErsSchemaValidation("mall_s_master_t.brand")]
        public virtual string brand { get; set; }

        /// <summary>
        /// メーカー [MAKER NAME]
        /// </summary>
        [DisplayName("amazon.jp.manufacturer")]
        [ErsSchemaValidation("mall_s_master_t.maker_name")]
        public virtual string maker_name { get; set; }

        /// <summary>
        /// メーカー型番 [MANUFACTURE's PART NUMBER]
        /// </summary>
        [DisplayName("amazon.jp.part_number")]
        [ErsSchemaValidation("mall_s_master_t.model_number")]
        public virtual string model_number { get; set; }

        /// <summary>
        /// item_package_quantity : パッケージ商品数
        /// </summary>
        [DisplayName("amazon.jp.item_package_quantity")]
        [ErsSchemaValidation("mall_s_master_t.item_package_quantity")]
        public int? item_package_quantity { get; set; }

        /// <summary>
        /// product_description : 商品説明文
        /// </summary>
        [DisplayName("amazon.jp.product_description")]
        [ErsSchemaValidation("mall_s_master_t.product_description")]
        public string product_description { get; set; }

        /// <summary>
        /// condition_note : 商品のコンディション説明
        /// </summary>
        [DisplayName("amazon.jp.condition_note")]
        [ErsSchemaValidation("mall_s_master_t.condition_note")]
        public string condition_note { get; set; }

        /// <summary>
        /// sale_price : セール価格
        /// </summary>
        [DisplayName("amazon.jp.sale_price")]
        [ErsSchemaValidation("mall_s_master_t.sale_price")]
        public int? sale_price { get; set; }

        /// <summary>
        /// sale_from_date : セール開始日
        /// </summary>
        [DisplayName("amazon.jp.sale_from_date")]
        [ErsSchemaValidation("mall_s_master_t.sale_from_date")]
        public DateTime? sale_from_date { get; set; }

        /// <summary>
        /// sale_end_date : セール終了日
        /// </summary>
        [DisplayName("amazon.jp.sale_end_date")]
        [ErsSchemaValidation("mall_s_master_t.sale_end_date")]
        public DateTime? sale_end_date { get; set; }

        /// <summary>
        /// offering_can_be_gift_messaged : ギフトメッセージ
        /// </summary>
        [DisplayName("amazon.jp.offering_can_be_gift_messaged")]
        [ErsSchemaValidation("mall_s_master_t.offering_can_be_gift_messaged")]
        public virtual EnumOnOff offering_can_be_gift_messaged { get; set; }

        /// <summary>
        /// offering_can_be_giftwrapped : ギフト包装可
        /// </summary>
        [DisplayName("amazon.jp.offering_can_be_giftwrapped")]
        [ErsSchemaValidation("mall_s_master_t.offering_can_be_giftwrapped")]
        public virtual EnumOnOff offering_can_be_giftwrapped { get; set; }

        /// <summary>
        /// bullet_point1 : 商品説明の箇条書き1
        /// </summary>
        [DisplayName("amazon.jp.bullet_point1")]
        [ErsSchemaValidation("mall_s_master_t.bullet_point1")]
        public string bullet_point1 { get; set; }

        /// <summary>
        /// bullet_point2 : 商品説明の箇条書き2
        /// </summary>
        [DisplayName("amazon.jp.bullet_point2")]
        [ErsSchemaValidation("mall_s_master_t.bullet_point2")]
        public string bullet_point2 { get; set; }

        /// <summary>
        /// bullet_point3 : 商品説明の箇条書き3
        /// </summary>
        [DisplayName("amazon.jp.bullet_point3")]
        [ErsSchemaValidation("mall_s_master_t.bullet_point3")]
        public string bullet_point3 { get; set; }

        /// <summary>
        /// bullet_point4 : 商品説明の箇条書き4
        /// </summary>
        [DisplayName("amazon.jp.bullet_point4")]
        [ErsSchemaValidation("mall_s_master_t.bullet_point4")]
        public string bullet_point4 { get; set; }

        /// <summary>
        /// bullet_point5 : 商品説明の箇条書き5
        /// </summary>
        [DisplayName("amazon.jp.bullet_point5")]
        [ErsSchemaValidation("mall_s_master_t.bullet_point5")]
        public string bullet_point5 { get; set; }

        /// <summary>
        /// generic_keywords1 : 検索キーワード1
        /// </summary>
        [DisplayName("amazon.jp.generic_keywords1")]
        [ErsSchemaValidation("mall_s_master_t.generic_keywords1")]
        public string generic_keywords1 { get; set; }

        /// <summary>
        /// generic_keywords2 : 検索キーワード2
        /// </summary>
        [DisplayName("amazon.jp.generic_keywords2")]
        [ErsSchemaValidation("mall_s_master_t.generic_keywords2")]
        public string generic_keywords2 { get; set; }

        /// <summary>
        /// generic_keywords3 : 検索キーワード3
        /// </summary>
        [DisplayName("amazon.jp.generic_keywords3")]
        [ErsSchemaValidation("mall_s_master_t.generic_keywords3")]
        public string generic_keywords3 { get; set; }

        /// <summary>
        /// generic_keywords4 : 検索キーワード4
        /// </summary>
        [DisplayName("amazon.jp.generic_keywords4")]
        [ErsSchemaValidation("mall_s_master_t.generic_keywords4")]
        public string generic_keywords4 { get; set; }

        /// <summary>
        /// generic_keywords5 : 検索キーワード5
        /// </summary>
        [DisplayName("amazon.jp.generic_keywords5")]
        [ErsSchemaValidation("mall_s_master_t.generic_keywords5")]
        public string generic_keywords5 { get; set; }

        /// <summary>
        /// recommended_browse_nodes1 : 推奨ブラウズノード1
        /// </summary>
        [DisplayName("amazon.jp.recommended_browse_nodes1")]
        [ErsSchemaValidation("mall_s_master_t.recommended_browse_nodes1")]
        public virtual string recommended_browse_nodes1 { get; set; }

        /// <summary>
        /// recommended_browse_nodes2 : 推奨ブラウズノード2
        /// </summary>
        [DisplayName("amazon.jp.recommended_browse_nodes2")]
        [ErsSchemaValidation("mall_s_master_t.recommended_browse_nodes2")]
        public virtual string recommended_browse_nodes2 { get; set; }

        /// <summary>
        /// specific_uses_keywords1 : 用途1
        /// </summary>
        [DisplayName("amazon.jp.specific_uses_keywords1")]
        [ErsSchemaValidation("mall_s_master_t.specific_uses_keywords1")]
        public virtual string specific_uses_keywords1 { get; set; }

        /// <summary>
        /// specific_uses_keywords2 : 用途2
        /// </summary>
        [DisplayName("amazon.jp.specific_uses_keywords2")]
        [ErsSchemaValidation("mall_s_master_t.specific_uses_keywords2")]
        public virtual string specific_uses_keywords2 { get; set; }

        /// <summary>
        /// target_audience_keywords : 対象
        /// </summary>
        [DisplayName("amazon.jp.target_audience_keywords")]
        [ErsSchemaValidation("mall_s_master_t.target_audience_keywords")]
        public virtual string target_audience_keywords { get; set; }

        /// <summary>
        /// safety_warning : 警告
        /// </summary>
        [DisplayName("amazon.jp.safety_warning")]
        [ErsSchemaValidation("mall_s_master_t.safety_warning")]
        public virtual string safety_warning { get; set; }

        /// <summary>
        /// legal_disclaimer_description : 法規上の免責条項
        /// </summary>
        [DisplayName("amazon.jp.legal_disclaimer_description")]
        [ErsSchemaValidation("mall_s_master_t.legal_disclaimer_description")]
        public virtual string legal_disclaimer_description { get; set; }

        /// <summary>
        /// ingredients1 : 原材料・成分1
        /// </summary>
        [DisplayName("amazon.jp.ingredients1")]
        [ErsSchemaValidation("mall_s_master_t.ingredients1")]
        public virtual string ingredients1 { get; set; }

        /// <summary>
        /// ingredients2 : 原材料・成分2
        /// </summary>
        [DisplayName("amazon.jp.ingredients2")]
        [ErsSchemaValidation("mall_s_master_t.ingredients2")]
        public virtual string ingredients2 { get; set; }

        /// <summary>
        /// ingredients3 : 原材料・成分3
        /// </summary>
        [DisplayName("amazon.jp.ingredients3")]
        [ErsSchemaValidation("mall_s_master_t.ingredients3")]
        public virtual string ingredients3 { get; set; }

        /// <summary>
        /// special_ingredients : 特別成分
        /// </summary>
        [DisplayName("amazon.jp.special_ingredients")]
        [ErsSchemaValidation("mall_s_master_t.special_ingredients")]
        public virtual string special_ingredients { get; set; }

        /// <summary>
        /// indications : 使用上の注意
        /// </summary>
        [DisplayName("amazon.jp.indications")]
        [ErsSchemaValidation("mall_s_master_t.indications")]
        public virtual string indications { get; set; }

        /// <summary>
        /// directions : 商品の利用(調理)方法
        /// </summary>
        [DisplayName("amazon.jp.directions")]
        [ErsSchemaValidation("mall_s_master_t.directions")]
        public virtual string directions { get; set; }

        /// <summary>
        /// style_name : スタイル名
        /// </summary>
        [DisplayName("amazon.jp.style_name")]
        [ErsSchemaValidation("mall_s_master_t.style_name")]
        public string style_name { get; set; }

        /// <summary>
        /// flavor_name : フレーバー
        /// </summary>
        [DisplayName("amazon.jp.flavor_name")]
        [ErsSchemaValidation("mall_s_master_t.flavor_name")]
        public virtual string flavor_name { get; set; }

        /// <summary>
        /// size_name : サイズ
        /// </summary>
        [DisplayName("amazon.jp.size_name")]
        [ErsSchemaValidation("mall_s_master_t.size_name")]
        public virtual string size_name { get; set; }

        /// <summary>
        /// color_name : カラー
        /// </summary>
        [DisplayName("amazon.jp.color_name")]
        [ErsSchemaValidation("mall_s_master_t.color_name")]
        public virtual string color_name { get; set; }

        /// <summary>
        /// color_map : カラーマップ
        /// </summary>
        [DisplayName("amazon.jp.color_map")]
        [ErsSchemaValidation("mall_s_master_t.color_map")]
        public virtual string color_map { get; set; }

        /// <summary>
        /// scent_name : 香り
        /// </summary>
        [DisplayName("amazon.jp.scent_name")]
        [ErsSchemaValidation("mall_s_master_t.scent_name")]
        public virtual string scent_name { get; set; }

        /// <summary>
        /// item_form : 商品の形状
        /// </summary>
        [DisplayName("amazon.jp.item_form")]
        [ErsSchemaValidation("mall_s_master_t.item_form")]
        public virtual string item_form { get; set; }

        /// <summary>
        /// special_features1 : 特殊機能1
        /// </summary>
        [DisplayName("amazon.jp.special_features1")]
        [ErsSchemaValidation("mall_s_master_t.special_features1")]
        public virtual string special_features1 { get; set; }

        /// <summary>
        /// special_features2 : 特殊機能2
        /// </summary>
        [DisplayName("amazon.jp.special_features2")]
        [ErsSchemaValidation("mall_s_master_t.special_features2")]
        public virtual string special_features2 { get; set; }

        /// <summary>
        /// special_features3 : 特殊機能3
        /// </summary>
        [DisplayName("amazon.jp.special_features3")]
        [ErsSchemaValidation("mall_s_master_t.special_features3")]
        public virtual string special_features3 { get; set; }

        /// <summary>
        /// minimum_weight_recommendation : 推奨最小重量
        /// </summary>
        [DisplayName("amazon.jp.minimum_weight_recommendation")]
        [ErsSchemaValidation("mall_s_master_t.minimum_weight_recommendation")]
        public virtual string minimum_weight_recommendation { get; set; }

        /// <summary>
        /// maximum_weight_recommendation : 推奨最大重量
        /// </summary>
        [DisplayName("amazon.jp.maximum_weight_recommendation")]
        [ErsSchemaValidation("mall_s_master_t.maximum_weight_recommendation")]
        public virtual string maximum_weight_recommendation { get; set; }

        /// <summary>
        /// weight_recommendation_unit_of_measure : 推奨重量の単位
        /// </summary>
        [DisplayName("amazon.jp.weight_recommendation_unit_of_measure")]
        [ErsSchemaValidation("mall_s_master_t.weight_recommendation_unit_of_measure")]
        public virtual string weight_recommendation_unit_of_measure { get; set; }
    }
}