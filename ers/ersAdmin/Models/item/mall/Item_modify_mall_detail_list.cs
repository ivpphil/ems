﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;
using ersAdmin.Models.item.mall.rakuten;
using ersAdmin.Models.item.mall.yahoo;
using ersAdmin.Models.item.mall.amazon.health;

namespace ersAdmin.Models.item.mall
{
    public class Item_modify_mall_detail_list
        : ErsBindableModel
    {
        [ErsOutputHidden("input")]
        [ErsSchemaValidation("mall_s_master_t.site_id")]
        public int? site_id { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("site_t.site_name")]
        public string site_name { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("mall_s_master_t.mall_shop_kbn")]
        public EnumMallShopKbn? mall_shop_kbn { get; set; }

        [MallBindTable("listProduct")]
        public List<Item_modify_mall_detail> listProduct { get; set; }

        /// <summary>
        /// モール連携ONの商品がある場合はTrue
        /// </summary>
        public bool hasMallProduct
        {
            get
            {
                if (listProduct == null)
                {
                    return false;
                }

                return listProduct.Count((product) => product.mall_flg == EnumOnOff.On) > 0;
            }
        }
    }

    public class MallBindTableAttribute
        : BindTableAttribute
    {
        public MallBindTableAttribute(string table_name)
            : base(table_name)
        {
        }

        protected override ErsBindableModel GetModelInstance(Type fieldType, IErsModelBase container, Dictionary<string, object> requestedValue)
        {
            var model = container as Item_modify_mall_detail_list;

            if (model == null || !model.mall_shop_kbn.HasValue)
            {
                return base.GetModelInstance(fieldType, container, requestedValue);
            }

            switch (model.mall_shop_kbn)
            {
                case EnumMallShopKbn.RAKUTEN:
                    return new Item_modify_rakuten_detail();

                case EnumMallShopKbn.YAHOO:
                    return new Item_modify_yahoo_detail();

                case EnumMallShopKbn.AMAZON:
                    return new Item_modify_amazon_detail();
                default:
                    throw new Exception("Not excepted mall_shop_kbn");
            }
        }
    }
}