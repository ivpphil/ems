﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers;
using ersAdmin.Domain.Item.Commands;

namespace ersAdmin.Models.item.mall.yahoo
{
    public class CsvYahooItemRecord
        : ErsBindableModel, IItemModifyMallYahooDetailListRecordCommand
    {
        /// <summary>
        /// モールフラグ
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.mall_flg")]
        public virtual EnumOnOff mall_flg { get; set; }

        /// <summary>
        /// 非表示フラグ
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.deleted")]
        [DisplayName("mall_s_master_t.deleted")]
        public virtual EnumOnOff deleted { get; set; }

        /// <summary>
        /// 商品コード [SKU code]
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("yahoo.jp.code")]
        [ErsSchemaValidation("s_master_t.scode")]
        public virtual string scode { get; set; }

        /// <summary>
        /// 販売価格 [PRICE]
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("yahoo.jp.price")]
        [ErsSchemaValidation("mall_s_master_t.price")]
        public virtual int? price { get; set; }

        /// <summary>
        /// プロダクトカテゴリ [Manage ID]
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("yahoo.jp.path")]
        [ErsSchemaValidation("mall_s_master_t.manage_id")]
        public virtual int? manage_id { get; set; }

        /// <summary>
        /// headline : キャッチコピー
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.headline")]
        [DisplayName("yahoo.jp.headline")]
        public string headline { get; set; }

        /// <summary>
        /// caption : 商品説明
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.caption")]
        [DisplayName("yahoo.jp.caption")]
        public string caption { get; set; }

        /// <summary>
        /// abstract : ひと言コメント
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.abstract_")]
        [DisplayName("yahoo.jp.abstract")]
        public string abstract_ { get; set; }

        /// <summary>
        /// explanation : 商品情報
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.explanation")]
        [DisplayName("yahoo.jp.explanation")]
        public string explanation { get; set; }

        /// <summary>
        /// additional1 : フリースペース
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.additional1")]
        [DisplayName("yahoo.jp.additional1")]
        public string additional1 { get; set; }

        /// <summary>
        /// additional2 : フリースペース
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.additional2")]
        [DisplayName("yahoo.jp.additional2")]
        public string additional2 { get; set; }

        /// <summary>
        /// additional3 : フリースペース
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.additional3")]
        [DisplayName("yahoo.jp.additional3")]
        public string additional3 { get; set; }

        /// <summary>
        /// relevant-links : おすすめ商品
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.relevant_links")]
        [DisplayName("yahoo.jp.relevant_links")]
        public string relevant_links { get; set; }

        /// <summary>
        /// release-date : 発売日
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.release_date")]
        [DisplayName("yahoo.jp.release_date")]
        public string release_date { get; set; }

        /// <summary>
        /// point-code : ポイント倍率
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.point_code")]
        [DisplayName("yahoo.jp.point_code")]
        public int? point_code { get; set; }

        /// <summary>
        /// meta-key : META keywords
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.meta_key")]
        [DisplayName("yahoo.jp.meta_key")]
        public string meta_key { get; set; }

        /// <summary>
        /// meta-desc : META description
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.meta_desc")]
        [DisplayName("yahoo.jp.meta_desc")]
        public string meta_desc { get; set; }

        /// <summary>
        /// template : 使用中のテンプレート
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.template")]
        [DisplayName("yahoo.jp.template")]
        public string template { get; set; }

        /// <summary>
        /// sale_price : セール価格
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.sale_price")]
        [DisplayName("yahoo.jp.sale_price")]
        public int? sale_price { get; set; }

        /// <summary>
        /// sale-period-start : セール販売期間（開始日）
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.sale_period_start")]
        [DisplayName("yahoo.jp.sale_period_start")]
        public DateTime? sale_period_start { get; set; }

        /// <summary>
        /// sale-period-end : セール販売期間（終了日）
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.sale_period_end")]
        [DisplayName("yahoo.jp.sale_period_end")]
        public DateTime? sale_period_end { get; set; }

        /// <summary>
        /// sp-code : 販促コード
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.sp_code")]
        [DisplayName("yahoo.jp.sp_code")]
        public string sp_code { get; set; }

        /// <summary>
        /// brand-code : ブランドコード
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.brand_code")]
        [DisplayName("yahoo.jp.brand_code")]
        public string brand_code { get; set; }

        /// <summary>
        /// yahoo-product-code : Yahoo!ショッピング製品コード
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.yahoo_product_code")]
        [DisplayName("yahoo.jp.yahoo_product_code")]
        public string yahoo_product_code { get; set; }

        /// <summary>
        /// product-code : 製品コード
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.product_code")]
        [DisplayName("yahoo.jp.product_code")]
        public string product_code { get; set; }

        /// <summary>
        /// product-category : プロダクトカテゴリ
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.product_category")]
        [DisplayName("yahoo.jp.product_category")]
        public string product_category { get; set; }

        /// <summary>
        /// spec1 : スペック
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.spec1")]
        [DisplayName("yahoo.jp.spec1")]
        public string spec1 { get; set; }

        /// <summary>
        /// spec2 : スペック
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.spec2")]
        [DisplayName("yahoo.jp.spec2")]
        public string spec2 { get; set; }

        /// <summary>
        /// spec3 : スペック
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.spec3")]
        [DisplayName("yahoo.jp.spec3")]
        public string spec3 { get; set; }

        /// <summary>
        /// spec4 : スペック
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.spec4")]
        [DisplayName("yahoo.jp.spec4")]
        public string spec4 { get; set; }

        /// <summary>
        /// spec5 : スペック
        /// </summary>
        [CsvField(TsvFieldAttribute.NON_FIXED)]
        [ErsSchemaValidation("mall_s_master_t.spec5")]
        [DisplayName("yahoo.jp.spec5")]
        public string spec5 { get; set; }

        #region NON TSV FIELDS
        public EnumMallShopKbn current_mall_sales_flag
        {
            get
            {
                return EnumMallShopKbn.YAHOO;
            }
        }
        #endregion
    }
}