﻿using System;
using ersAdmin.Domain.Item.Commands;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using System.Collections.Generic;

namespace ersAdmin.Models.item.mall
{
    public class Item_modify_mall_detail
        : ErsBindableModel
    {
        protected string mallTabName = string.Empty;
        public override string lineName
        {
            get
            {
                if (string.IsNullOrEmpty(this.scode))
                    return base.lineName + String.Format(" {0}", mallTabName);
                else
                    return base.lineName + String.Format(" {0}" + ErsResources.GetMessage("line_name_scode", this.scode), mallTabName);
            }
        }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public EnumOnOff mall_flg { get; set; }

        /// <summary>
        /// 非表示フラグ
        /// </summary>
        [ErsSchemaValidation("mall_s_master_t.deleted")]
        public virtual EnumOnOff deleted { get; set; }

        [ErsSchemaValidation("mall_s_master_t.site_id")]
        public int? site_id { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("mall_s_master_t.gcode", requireAlphabet = true)]
        public virtual string gcode { get; set; }
                        
        [ErsOutputHidden("input")]
        [ErsSchemaValidation("mall_s_master_t.scode", requireAlphabet = true)]
        public virtual string scode { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("mall_s_master_t.jancode", requireAlphabet = true)]
        public virtual string jancode { get; set; }

        [ErsSchemaValidation("mall_s_master_t.price")]
        public virtual int? price { get; set; }

        [ErsSchemaValidation("mall_s_master_t.manage_id")]
        public virtual int? manage_id { get; set; }
    }
}
