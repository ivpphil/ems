﻿using System.ComponentModel;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc;
using System;

namespace ersAdmin.Models.item.mall.rakuten
{
    public class Item_modify_rakuten_detail
        : Item_modify_mall_detail, IItemModifyMallRakutenDetailListRecordCommand
    {
        public override string lineName
        {
            get
            {
                mallTabName = "楽天";
                return base.lineName;
            }
        }

        /// <summary>
        /// 商品コード [SKU code]
        /// </summary>
        [ErsOutputHidden("input")]
        [DisplayName("rakuten.product_number")]
        [ErsSchemaValidation("s_master_t.scode")]
        public override string scode { get; set; }

        /// <summary>
        /// 商品ディレクトリID [Manage ID]
        /// </summary>
        [ErsSchemaValidation("mall_s_master_t.manage_id")]
        [DisplayName("rakuten.all_products_dir_id")]
        public override int? manage_id { get; set; }

        /// <summary>
        /// 販売価格 [PRICE]
        /// </summary>
        [DisplayName("rakuten.selling_price")]
        [ErsSchemaValidation("mall_s_master_t.price")]
        public override int? price { get; set; }

        /// <summary>
        /// tag_id : タグＩＤ
        /// </summary>
        [DisplayName("rakuten.tag_id")]
        [ErsSchemaValidation("mall_s_master_t.tag_id")]
        public string tag_id { get; set; }

        /// <summary>
        /// pc_slogan : PC用キャッチコピー
        /// </summary>
        [DisplayName("rakuten.pc_slogan")]
        [ErsSchemaValidation("mall_s_master_t.pc_slogan")]
        public string pc_slogan { get; set; }

        /// <summary>
        /// mobile_slogan : モバイル用キャッチコピー
        /// </summary>
        [DisplayName("rakuten.mobile_slogan")]
        [ErsSchemaValidation("mall_s_master_t.mobile_slogan")]
        public string mobile_slogan { get; set; }

        /// <summary>
        /// product_layout : 商品情報レイアウト
        /// </summary>
        [DisplayName("rakuten.product_layout")]
        [ErsSchemaValidation("mall_s_master_t.product_layout")]
        public int? product_layout { get; set; }

        /// <summary>
        /// noshi : のし対応
        /// </summary>
        [DisplayName("rakuten.noshi")]
        [ErsSchemaValidation("mall_s_master_t.noshi")]
        public int? noshi { get; set; }

        /// <summary>
        /// pc_description : PC用商品説明文
        /// </summary>
        [DisplayName("rakuten.pc_description")]
        [ErsSchemaValidation("mall_s_master_t.pc_description")]
        public string pc_description { get; set; }

        /// <summary>
        /// mobile_description : モバイル用商品説明文
        /// </summary>
        [DisplayName("rakuten.mobile_description")]
        [ErsSchemaValidation("mall_s_master_t.mobile_description")]
        public string mobile_description { get; set; }

        /// <summary>
        /// smartphone_description : スマートフォン用商品説明文
        /// </summary>
        [DisplayName("rakuten.smartphone_description")]
        [ErsSchemaValidation("mall_s_master_t.smartphone_description")]
        public string smartphone_description { get; set; }

        /// <summary>
        /// pc_sale_description : PC用販売説明文
        /// </summary>
        [DisplayName("rakuten.pc_sale_description")]
        [ErsSchemaValidation("mall_s_master_t.pc_sale_description")]
        public string pc_sale_description { get; set; }

        /// <summary>
        /// movie : 動画
        /// </summary>
        [DisplayName("rakuten.movie")]
        [ErsSchemaValidation("mall_s_master_t.movie")]
        public string movie { get; set; }

        /// <summary>
        /// stock_display : 在庫数表示
        /// </summary>
        [DisplayName("rakuten.stock_display")]
        [ErsSchemaValidation("mall_s_master_t.stock_display")]
        public int? stock_display { get; set; }

        /// <summary>
        /// black_market_pass : 闇市パスワード
        /// </summary>
        [DisplayName("rakuten.black_market_pass")]
        [ErsSchemaValidation("mall_s_master_t.black_market_pass")]
        public string black_market_pass { get; set; }

        /// <summary>
        /// point_scale_rate : ポイント変倍率
        /// </summary>
        [DisplayName("rakuten.point_scale_rate")]
        [ErsSchemaValidation("mall_s_master_t.point_scale_rate")]
        public int? point_scale_rate { get; set; }

        /// <summary>
        /// point_scale_rate_period : ポイント変倍率適用期間
        /// </summary>
        [DisplayName("rakuten.point_scale_rate_period")]
        [ErsSchemaValidation("mall_s_master_t.point_scale_rate_period")]
        public DateTime? point_scale_rate_period { get; set; }

        /// <summary>
        /// header_footer_leftnavi : ヘッダー・フッター・レフトナビ
        /// </summary>
        [DisplayName("rakuten.header_footer_leftnavi")]
        [ErsSchemaValidation("mall_s_master_t.header_footer_leftnavi")]
        public string header_footer_leftnavi { get; set; }

        /// <summary>
        /// display_order : 表示項目の並び順
        /// </summary>
        [DisplayName("rakuten.display_order")]
        [ErsSchemaValidation("mall_s_master_t.display_order")]
        public string display_order { get; set; }

        /// <summary>
        /// common_description_small : 共通説明文（小）
        /// </summary>
        [DisplayName("rakuten.common_description_small")]
        [ErsSchemaValidation("mall_s_master_t.common_description_small")]
        public string common_description_small { get; set; }

        /// <summary>
        /// deature_product : 目玉商品
        /// </summary>
        [DisplayName("rakuten.deature_product")]
        [ErsSchemaValidation("mall_s_master_t.deature_product")]
        public string deature_product { get; set; }

        /// <summary>
        /// common_description_large : 共通説明文（大）
        /// </summary>
        [DisplayName("rakuten.common_description_large")]
        [ErsSchemaValidation("mall_s_master_t.common_description_large")]
        public string common_description_large { get; set; }

        /// <summary>
        /// display_review_test : レビュー本文表示
        /// </summary>
        [DisplayName("rakuten.display_review_test")]
        [ErsSchemaValidation("mall_s_master_t.display_review_test")]
        public int? display_review_test { get; set; }

        /// <summary>
        /// size_chart_link : サイズ表リンク
        /// </summary>
        [DisplayName("rakuten.size_chart_link")]
        [ErsSchemaValidation("mall_s_master_t.size_chart_link")]
        public string size_chart_link { get; set; }

        /// <summary>
        /// drug_description : 医薬品説明文
        /// </summary>
        [DisplayName("rakuten.drug_description")]
        [ErsSchemaValidation("mall_s_master_t.drug_description")]
        public string drug_description { get; set; }

        /// <summary>
        /// drug_notes : 医薬品注意事項
        /// </summary>
        [DisplayName("rakuten.drug_notes")]
        [ErsSchemaValidation("mall_s_master_t.drug_notes")]
        public string drug_notes { get; set; }

        /// <summary>
        /// control_number_dual_price_word : 二重価格文言管理番号
        /// </summary>
        [DisplayName("rakuten.control_number_dual_price_word")]
        [ErsSchemaValidation("mall_s_master_t.control_number_dual_price_word")]
        public string control_number_dual_price_word { get; set; }
    }
}