﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.merchandise.strategy;
using jp.co.ivp.ers;
using ersAdmin.Domain.Item.Commands;

namespace ersAdmin.Models
{
    public class ItemRegist
        : ItemModify, IItemRegistCommand
    {
        public override string mode
        {
            get
            {
                return "regist";
            }
        }

        /// <summary>
        /// 商品グループ修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool item_group_regist_btn { get; set; }
    }
}