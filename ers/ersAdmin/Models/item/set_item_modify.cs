﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Item.Commands;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.merchandise;

namespace ersAdmin.Models.item
{
    public class set_item_modify : ErsModelBase, ISetItemModifyCommand, ISetItemModifyMappable
    {
        public ErsPagerModel pager { get; internal set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        public long recordCount { get; set; }

        public bool LoadData { get; set; }

        public ErsSetMerchandise objSetItem { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.id")]
        public int? id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode", requireAlphabet = true)]
        public string parent_scode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.sname")]
        public string parent_sname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("price_t.price")]
        public int? price { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("price_t.price")]
        public int? regular_price { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("price_t.price")]
        public int? regular_first_price { get; set; }

        [HtmlSubmitButton]
        public bool delete { get; set; }

        [BindTable("ChildScodeList")]
        public List<ChildScodeListData> ChildScodeList { get; set; }

        [ErsSchemaValidation("set_master_t.scode", requireAlphabet = true)]
        public string append_scode { get; set; }
    }
}