﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.merchandise.category;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Models.item
{
    public class Cate_header
        : ErsBindableModel, ICateHeaderListRecordCommand
    {
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 5)]
        public int? categoryNumber { get; set; }

        /// <summary>
        /// setup_t.cate*受信用
        /// </summary>
        [ErsSchemaValidation("setup_t.cate1")]
        [DisplayName("cate_header_name")]
        public string cate_header_name { get; set; }

        /// <summary>
        /// setup_t.cate*_active受信用
        /// </summary>
        [ErsSchemaValidation("setup_t.cate1_active")]
        [DisplayName("cate_header_name")]
        public EnumActive? cate_header_active { get; set; }

    }
}