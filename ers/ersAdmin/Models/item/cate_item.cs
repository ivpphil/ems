﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.merchandise.category;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.Domain.Item.Commands;

namespace ersAdmin.Models.item
{
    public class Cate_item
        : ErsBindableModel, ICateItemListRecordCommand
    {
        [ErsSchemaValidation("cate2_t.id")]
        [DisplayName("cate_id")]
        public int? key { get; set; }

        [ErsSchemaValidation("cate2_t.id")]
        [DisplayName("cate_id")]
        public int? id { get; set; }

        [ErsSchemaValidation("cate2_t.cate_name")]
        [DisplayName("cate_name")]
        public string cate_name { get; set; }

        [ErsSchemaValidation("cate2_t.parent_id")]
        [DisplayName("cate_parent_id")]
        public int? parent_id { get; set; }

        [ErsSchemaValidation("cate2_t.active")]
        [DisplayName("cate_active")]
        public EnumActive? active { get; set; }

        [ErsSchemaValidation("cate2_t.disp_order")]
        [DisplayName("cate_disp_order")]
        public int? disp_order { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        [DisplayName("cate_delete")]
        public int? delete { get; set; }

        [HtmlSubmitButton]
        public bool hasParent { get; set; }

        public List<Dictionary<string, object>> listParent { get; internal set; }
    }
}