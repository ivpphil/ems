﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.viewService;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.merchandise.strategy;
using jp.co.ivp.ers;
using ersAdmin.Domain.Item.Commands;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mall;
using ersAdmin.Models.item.mall.yahoo;
using ersAdmin.Models.item.mall.rakuten;
using ersAdmin.Models.item.mall.amazon.health;
using ersAdmin.Models.item.mall;

namespace ersAdmin.Models
{
    public class ItemModify
        : ErsSiteRegisterModelBase, IItemModifyCommand, IItemModifyMappable
    {
        protected ErsViewCategoryService categoryService;
        protected Setup setup;

        public ItemModify()
        {
            //初期値入力
            date_from = DateTime.Now;
            date_to = DateTime.Now.AddYears(10);

            categoryService = ErsFactory.ersViewServiceFactory.GetErsViewCategoryService();
            this.setup = ErsFactory.ersUtilityFactory.getSetup();
        }

        #region Image uploader property
        public virtual string image_identifier
        {
            get
            {
                return ErsContext.sessionState.Get("admin_ransu") + "group_simg_detail";
            }
        }

        public virtual string group_simg_temp_folder
        {
            get
            {
                return "group_simg";
            }
        }

        public bool IsInitialize { get; set; }

        #endregion

        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public override EnumMallShopKbn? mall_shop_kbn { get { return EnumMallShopKbn.ERS; } }

        public virtual string mode
        {
            get
            {
                return "modify";
            }
        }

        [HtmlSubmitButton]
        public bool regist_complete { get; set; }

        /// <summary>
        /// 戻り先URL
        /// </summary>
        public string returnUrl { get; set; }

        /// <summary>
        /// 商品グループ修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool item_group_modify_btn { get; set; }

        /// <summary>
        /// 商品グループ修正押下時
        /// </summary>
        [HtmlSubmitButton]
        public bool item_group_delete_btn { get; set; }

        [ErsOutputHidden("complete")]
        [ErsSchemaValidation("g_master_t.gcode", requireAlphabet = true)]
        public string gcode { get; set; }

        [ErsOutputHidden("input")]
        [ErsSchemaValidation("g_master_t.gcode")]
        public string old_gcode { get; set; }

        [ErsSchemaValidation("g_master_t.gname")]
        public string gname { get; set; }

        [ErsSchemaValidation("g_master_t.m_gname")]
        public string m_gname { get; set; }

        [ErsSchemaValidation("g_master_t.date_from")]
        public DateTime? date_from { get; set; }

        [ErsSchemaValidation("g_master_t.date_to")]
        public DateTime? date_to { get; set; }

        [ErsSchemaValidation("g_master_t.s_sale_ptn")]
        public EnumSalePatternType? s_sale_ptn { get; set; }

        [ErsSchemaValidation("g_master_t.stock_flg")]
        public EnumStockFlg? stock_flg { get; set; }

        [ErsSchemaValidation("g_master_t.stock_set1")]
        public int? stock_set1 { get; set; }

        [ErsSchemaValidation("g_master_t.stock_set2")]
        public int? stock_set2 { get; set; }

        [ErsSchemaValidation("g_master_t.cate1")]
        public int[] cate1 { get; set; }

        [ErsSchemaValidation("g_master_t.cate2")]
        public int[] cate2 { get; set; }

        [ErsSchemaValidation("g_master_t.cate3")]
        public int[] cate3 { get; set; }

        [ErsSchemaValidation("g_master_t.cate4")]
        public int[] cate4 { get; set; }

        [ErsSchemaValidation("g_master_t.cate5")]
        public int[] cate5 { get; set; }

        [ErsSchemaValidation("g_master_t.recommend1")]
        public string recommend1 { get; set; }

        [ErsSchemaValidation("g_master_t.recommend2")]
        public string recommend2 { get; set; }

        [ErsSchemaValidation("g_master_t.recommend3")]
        public string recommend3 { get; set; }

        [ErsSchemaValidation("g_master_t.recommend4")]
        public string recommend4 { get; set; }

        [ErsSchemaValidation("g_master_t.recommend5")]
        public string recommend5 { get; set; }

        [ErsSchemaValidation("g_master_t.disp_keyword")]
        public string[] disp_keyword { get; set; }

        [ErsSchemaValidation("g_master_t.metatitle")]
        public string metatitle { get; set; }

        [ErsSchemaValidation("g_master_t.metadescription")]
        public string metadescription { get; set; }

        [ErsSchemaValidation("g_master_t.metawords")]
        public string metawords { get; set; }

        [ErsSchemaValidation("g_master_t.link_url")]
        public string link_url { get; set; }

        [ErsSchemaValidation("g_master_t.description")]
        public string description { get; set; }

        [ErsSchemaValidation("g_master_t.m_description")]
        public string m_description { get; set; }

        [ErsSchemaValidation("g_master_t.description_for_list")]
        public string description_for_list { get; set; }

        [ErsUniversalValidation(type =CHK_TYPE.Numeric, rangeFrom =0)]
        public int? sort { get; set; }

        [ErsSchemaValidation("g_master_t.disp_list_flg")]
        public EnumDisp_list_flg disp_list_flg { get; set; }

        [ErsSchemaValidation("g_master_t.active")]
        public EnumActive? active { get; set; }

        private EnumCarriageCostType? carriage_cost_type_value;
        [ErsSchemaValidation("g_master_t.carriage_cost_type")]
        public virtual EnumCarriageCostType? carriage_cost_type { 
            get 
            {
                return carriage_cost_type_value.GetValueOrDefault(EnumCarriageCostType.Normal);
            }
            set 
            {
                carriage_cost_type_value = value;
            }
        }

        [ErsSchemaValidation("g_master_t.plural_order_type")]
        public virtual EnumPluralOrderType? plural_order_type { get; set; }

        [ErsSchemaValidation("g_master_t.set_flg")]
        public virtual EnumSetFlg set_flg { get; set; }

        [ErsSchemaValidation("g_master_t.doc_bundling_flg")]
        public virtual EnumDocBundlingFlg doc_bundling_flg { get; set; }

        [ErsSchemaValidation("g_master_t.deliv_method")]
        public virtual EnumDelvMethod? deliv_method { get; set; }

        /// <summary>
        /// お届け周期パターン
        /// </summary>
        public string disp_send_ptn
        {
            get
            {
                return string.Empty + disp_send_ptn_month_intervals + disp_send_ptn_week_intervals + disp_send_ptn_month_day_intervals;
            }
        }

        /// <summary>
        /// お届け周期パターン(月毎)
        /// </summary>
        [ErsUniversalValidation(type=CHK_TYPE.Numeric, rangeFrom=0, rangeTo=1)]
        public int disp_send_ptn_month_intervals { get; set; }

        /// <summary>
        /// お届け周期パターン(週毎)
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public int disp_send_ptn_week_intervals { get; set; }

        /// <summary>
        /// お届け周期パターン(日毎)
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public int disp_send_ptn_month_day_intervals { get; set; }

        [BindPicture(EnumPictureType.JPEG)]
        public HttpPostedFileBase group_simg { get; set; }

        /// <summary>
        /// 最大購入可能数の入力可否ﾌﾗｸﾞ
        /// </summary>
        public bool deliv_method_flg
        {
            get
            {
                if (this.gcode != null)
                {
                    if (item_group_modify_btn == false)
                    {
                        var ObjGroup = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(this.gcode);
                        if (ObjGroup != null)
                        {
                            if (ObjGroup.deliv_method == EnumDelvMethod.Mail)
                            {
                                return true;
                            }
                        }
                    }
                    else if (deliv_method == EnumDelvMethod.Mail)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        [HtmlSubmitButton]
        public bool disabled_file_uploader { get; set; }

        [ErsOutputHidden]
        [BindTable("group_simg_detail")]
        public IList<group_simg_detail> group_simg_detail { get; set; }

        public bool IsGSimgExist
        {
            get
            {
                return System.IO.File.Exists(System.IO.Path.Combine(setup.image_directory + "simg", "G" + old_gcode + "_01.jpg"));
            }
        }

        public List<Dictionary<string, object>> cateList1 { get { return categoryService.GetCategory(1, cate1,setup.Multiple_sites); } }

        public List<Dictionary<string, object>> cateList2 { get { return categoryService.GetCategory(2, cate2, setup.Multiple_sites); } }

        public List<Dictionary<string, object>> cateList3 { get { return categoryService.GetCategory(3, cate3, setup.Multiple_sites); } }

        public List<Dictionary<string, object>> cateList4 { get { return categoryService.GetCategory(4, cate4, setup.Multiple_sites); } }

        public List<Dictionary<string, object>> cateList5 { get { return categoryService.GetCategory(5, cate5, setup.Multiple_sites); } }

        public List<Dictionary<string, object>> salePtnList { get { return ErsFactory.ersViewServiceFactory.GetErsViewSalePtnService().SelectAsList(); } }

        public List<Dictionary<string, object>> deliv_methodList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList( EnumCommonNameType.DelvMethod, EnumCommonNameColumnName.opt_chr1, null, null, false); } }

        public List<Dictionary<string, object>> carriage_cost_typeList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.CarriageCostType, EnumCommonNameColumnName.namename, null, null, false); } }

        public List<Dictionary<string, object>> plural_order_typeList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PluralOrderType, EnumCommonNameColumnName.namename, null, null, false); } }

        public string cateName1 { get { return ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().getCateNameView(1); } }
        public string cateName2 { get { return ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().getCateNameView(2); } }
        public string cateName3 { get { return ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().getCateNameView(3); } }
        public string cateName4 { get { return ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().getCateNameView(4); } }
        public string cateName5 { get { return ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().getCateNameView(5); } }

        [ErsOutputHidden]
        [BindTable("detail_table")]
        public List<Item_modify_detail> detail_table { get; set; }

        [BindTable("yahoo_detail_table")]
        public List<Item_modify_yahoo_detail> yahoo_detail_table { get; set; }


        [BindTable("rakuten_detail_table")]
        public List<Item_modify_rakuten_detail> rakuten_detail_table { get; set; }


        [BindTable("amazon_detail_table")]
        public List<Item_modify_amazon_detail> amazon_detail_table { get; set; }

        [BindTable("listMallList")]
        public List<Item_modify_mall_detail_list> listMallList { get; set; }

        public List<Dictionary<string, object>> listWh_order_type { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.WhOrderType, EnumCommonNameColumnName.namename); } }

        public List<Dictionary<string, object>> listAmazon_product_type { get { return ErsMallFactory.ersMallViewServiceFactory.GetErsViewMallAmazonProductTypeService().GetList(); } }

    }
}