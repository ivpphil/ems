﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;

namespace ersAdmin.Models
{
    public class group_simg_detail
        : ErsBindableModel
    {
        public override string lineName
        {
            get
            {
                return ErsResources.GetFieldName("group_simg") + this.lineNumber;
            }
        }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string group_simg { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        [DisplayName("temp_file_name")]
        public string file_identifier { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All, isArray = true)]
        [DisplayName("group_simg")]
        public string[] api_errorList { get; set; }
    }
}