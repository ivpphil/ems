﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.Models.item.mall.rakuten;
using ersAdmin.Models.item.mall.yahoo;
using ersAdmin.Models.item.mall.amazon.health;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;

namespace ersAdmin.Models.item.tsv
{
    public class Item_tsv_mall_list
        : ErsBindableModel
    {
        public string site_name { get; set; }

        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        public int? site_id { get; set; }
    }
}