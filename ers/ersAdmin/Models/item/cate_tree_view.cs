﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.viewService;
using ersAdmin.Models.item;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class CateTreeView
        : ErsSiteSearchModelBase, ICateTreeViewMappable
    {
        protected ErsViewCategoryService categoryService;
        public CateTreeView()
        {
            categoryService = ErsFactory.ersViewServiceFactory.GetErsViewCategoryService();
        }

        //メインツリー
        public List<Dictionary<string, object>> MainList { get; set; }

        public List<Dictionary<string, object>> TreeList { get; set; }
        public List<Dictionary<string, object>> cateList1 { get; set; }
        public List<Dictionary<string, object>> cateList2 { get; set; }
        public List<Dictionary<string, object>> cateList3 { get; set; }
        public List<Dictionary<string, object>> cateList4 { get; set; }
        public List<Dictionary<string, object>> cateList5 { get; set; }

        public EnumActive? cate_kind1_active { get; set; }
        public EnumActive? cate_kind2_active { get; set; }
        public EnumActive? cate_kind3_active { get; set; }
        public EnumActive? cate_kind4_active { get; set; }
        public EnumActive? cate_kind5_active { get; set; }

    }
}