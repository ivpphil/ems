﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using System.Web.Mvc;
using jp.co.ivp.ers.db;
using ersAdmin.Models.csv;
using jp.co.ivp.ers.merchandise;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Models
{
    public class Set_Item_all_csv
        : ErsModelBase, ISetItemAllCSVCommand
    {
        /// <summary>
        /// Gets display message on finish page.
        /// </summary>
        public virtual string resultMsg
        {
            get
            {
                if (this.csv_file == null)
                {
                    return string.Empty;
                }
                return ErsResources.GetMessage("30000", this.csv_file.validIndexes.Count());
            }
        }

        /// <summary>
        /// 1行目をスキップする場合はtrue
        /// <para>If you want to skip the first line, value must be true</para>
        /// </summary>
        [ErsOutputHidden]
        [HtmlSubmitButton]
        public virtual bool chk_find { get; set; }

        /// <summary>
        /// アップロードデータ
        /// <para>Upload data</para>
        /// </summary>
        [ErsOutputHidden]
        [BindCsvFile]
        public ErsCsvContainer<csv.Set_Item_csv_record> csv_file { get; set; }

        /// <summary>
        /// 登録ボタンの押下
        /// </summary>
        [HtmlSubmitButton]
        public virtual bool regist { get; set; }

        public bool allCsvFieldsInvalid
        {
            get
            {
                if (this.csv_file == null)
                    return true;

                return (this.csv_file.validIndexes.Count() == 0);
            }
        }

    }
}