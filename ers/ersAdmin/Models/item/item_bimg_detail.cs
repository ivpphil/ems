﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers;

namespace ersAdmin.Models.item
{
    public class item_bimg_detail
        : ErsBindableModel
    {
        public override string lineName
        {
            get
            {
                return ErsResources.GetFieldName("bimg") + this.lineNumber;
            }
        }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string bimg { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        [DisplayName("temp_file_name")]
        public string file_identifier { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All, isArray = true)]
        [DisplayName("bimg")]
        public string[] api_errorList { get; set; }
    }
}