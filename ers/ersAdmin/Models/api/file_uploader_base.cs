﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Api.Mappables;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;

namespace ersAdmin.Models.api
{
    public class file_uploader_base
        : ErsModelBase, IFileUploaderBaseMappable
    {
        public virtual HttpPostedFileBase file { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string temp_file_name { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string temp_folder { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        [DisplayName("temp_file_name")]
        public string name { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        [DisplayName("temp_file_name")]
        public string file_identifier { get; set; }
    }
}