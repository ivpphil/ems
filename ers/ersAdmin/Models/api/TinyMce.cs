﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models.api
{
    public class TinyMce
        : ErsModelBase
    {
        [HtmlSubmitButton]
        public bool enableErsButton { get; set; }

        [ErsUniversalValidation(type=CHK_TYPE.OneByteCharacter)]
        public string elements { get; set; }
    }
}