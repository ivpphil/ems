﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using System.ComponentModel;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Models.api
{
    public class file_uploader
        : file_uploader_base
    {
        [BindFile(10 * 1024 * 1024)]
        [DisplayName("img_file_name")]
        public override HttpPostedFileBase file { get; set; }
    }
}