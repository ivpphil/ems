﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.Domain.Api.Mappables;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;

namespace ersAdmin.Models.api
{
    public class image_uploader
        : file_uploader_base
    {
        [BindPicture]
        [DisplayName("img_file_name")]
        public override HttpPostedFileBase file { get; set; }
    }
}