﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using System.ComponentModel;
using jp.co.ivp.ers;

namespace ersAdmin.Models.api
{
    public class item_image_uploader
        : image_uploader
    {
        [BindPicture(EnumPictureType.JPEG)]
        [DisplayName("img_file_name")]
        public override HttpPostedFileBase file { get; set; }
    }
}