﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using ersAdmin.Models;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using ersAdmin.Domain.Price.Commands;
using ersAdmin.Domain.Price.Mappables;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.price;

namespace ersAdmin.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class priceController
        : ErsControllerSecureAdmin
    {

        /// <summary>
        /// トップ
        /// </summary>
        /// <returns></returns>
        public ActionResult index()
        {
            return View("index");
        }

        /// <summary>
        /// 検索画面
        /// </summary>
        /// <returns></returns>
        public ActionResult price_search(Price_list pricemodel, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPriceListCommand>(pricemodel), pricemodel);
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    this.ClearModelState(pricemodel);
                }
            }

            return View("price_search", pricemodel);
        }

        /// <summary>
        /// リスト画面
        /// </summary>
        /// <returns></returns>
        public ActionResult price_list(Price_list priceList)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPriceListCommand>(priceList), priceList);
            if (!ModelState.IsValid)
            {
                return this.price_search(priceList, EnumEck.Error);
            }

            priceList.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", priceList.pageCnt, priceList.maxItemCount);

            mapperBus.Map<IPriceListMappable>(priceList);

            priceList.pager.LoadPageList(priceList.recordCount);
            
            priceList.SetOutputHidden(true);

            return View("price_list", priceList);
        }

        /// <summary>
        /// リスト画面
        /// </summary>
        /// <returns></returns>
        public ActionResult price_detail(PriceDetail priceDetail, Price_list priceList, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPriceListCommand>(priceList), priceList);
            ModelState.AddModelErrors(commandBus.Validate<IPriceDetailCommand>(priceDetail), priceDetail);
            ModelState.AddModelErrors(commandBus.Validate<IPriceDetailListCommand>(priceDetail), priceDetail);
            ModelState.AddModelErrors(commandBus.Validate<IPriceMemberRankCommand>(priceDetail), priceDetail);
            this.ClearModelState(priceList);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    if (!priceDetail.IsValidField("scode", "gcode"))
                    {
                        return this.GetErrorView();
                    }

                    this.ClearModelState(priceDetail);
                }
            }

            if (!this.IsErrorBack(eck))
            {
                mapperBus.Map<IPriceDetailMappable>(priceDetail);
                mapperBus.Map<IPriceDetailListMappable>(priceDetail);
                mapperBus.Map<IPriceMemberRankMappable>(priceDetail);
            }

            priceDetail.SetOutputHidden(true);
            priceList.SetOutputHidden(true);
            this.AddModelToView(priceList);

            return View("price_detail", priceDetail);
        }

        /// <summary>
        /// 登録
        /// </summary>
        /// <returns></returns>
        public ActionResult price_complete(PriceDetail priceDetail, Price_list priceList)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPriceListCommand>(priceList), priceList);
            ModelState.AddModelErrors(commandBus.Validate<IPriceDetailCommand>(priceDetail), priceDetail);
            ModelState.AddModelErrors(commandBus.Validate<IPriceDetailListCommand>(priceDetail), priceDetail);
            ModelState.AddModelErrors(commandBus.Validate<IPriceMemberRankCommand>(priceDetail), priceDetail);
            this.ClearModelState(priceList);
            if (!ModelState.IsValid)
            {
                return this.price_detail(priceDetail, priceList, EnumEck.Error);
            }

            using (var tx = ErsDB_parent.BeginTransaction())
            {
                commandBus.Submit<IPriceDetailCommand>(priceDetail, EnumCommandTransaction.WithoutBeginTransaction);
                commandBus.Submit<IPriceDetailListCommand>(priceDetail, EnumCommandTransaction.WithoutBeginTransaction);
                commandBus.Submit<IPriceMemberRankCommand>(priceDetail, EnumCommandTransaction.WithoutBeginTransaction);
                tx.Commit();
            }

            priceDetail.SetOutputHidden("back_to_detail", true);
            priceList.SetOutputHidden(true);
            this.AddModelToView(priceList);

            return View("price_complete", priceDetail);
        }

        /// <summary>
        /// 価格マスタの一括登録
        /// </summary>
        public ActionResult price_csv()
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("price_csv");
        }

        /// <summary>
        /// 価格マスタの一括登録 確認画面
        /// </summary>
        public ActionResult price_csv_confirm(Price_csv price_csv)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPriceCsvCommand>(price_csv), price_csv);
            if (!ModelState.IsValid)
            {
                //エラーは確認画面に表示する。
                //return this.price_csv();
            }

            mapperBus.Map<IPriceCsvMappable>(price_csv);

            //モデル情報の引き渡し
            price_csv.SetOutputHidden(true);

            //テンプレート表示
            return View("price_csv_confirm", price_csv);
        }

        /// <summary>
        /// 価格マスタの一括登録 完了画面
        /// </summary>
        public ActionResult price_csv_complete(Price_csv price_csv)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPriceCsvCommand>(price_csv), price_csv);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //モデル情報の引き渡し
            price_csv.SetOutputHidden(true);

            mapperBus.Map<IPriceCsvMappable>(price_csv);

            commandBus.Submit<IPriceCsvCommand>(price_csv, EnumCommandTransaction.BeginTransaction);

            //テンプレート表示
            return View("price_csv_complete", price_csv);
        }

        /// <summary>
        /// CSVダウンロード（全件）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_csv_all(Price_list pricemodel)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<IPriceListCSVMappable>(pricemodel);
            return this.CsvFile(pricemodel.csvCreater.filePath);
        }

        /// <summary>
        /// CSVダウンロード（ページ）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_csv_page(Price_list pricemodel)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(pricemodel.pageCnt, pricemodel.maxItemCount);
            pricemodel.pager = pager;

            mapperBus.Map<IPriceListCSVMappable>(pricemodel);
            return this.CsvFile(pricemodel.csvCreater.filePath);
        }


    }
}