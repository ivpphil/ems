﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using ersAdmin.Models;
using ersAdmin.Models.login;
using jp.co.ivp.ers;
using ersAdmin.Domain.Login.Commands;
using ersAdmin.Domain.Login.Mappables;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;


namespace ersAdmin.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    public class loginController
        : ErsControllerSecureAdmin
    {
        public ActionResult login(Login login, EnumEck? eck = null)
        {
            if (!ModelState.IsValid && !login.sessionError)
            {
                this.ClearModelState(login); // エラーをクリア
                this.ModelState.AddModelError("common", ErsResources.GetMessage("10204"));　//共通エラー

                if (!this.IsErrorBack(eck))
                {
                    return GetErrorView();
                }
            }

            if (login.sessionError)
            {
                this.ModelState.AddModelError("error", ErsResources.GetMessage("10203"));
            }

            return View("login", login);
        }

        [HttpPost]
        public ActionResult loginaction(Login login)
        {
            ModelState.AddModelErrors(commandBus.Validate<ILoginCommand>(login), login);
            ((ISession)ErsContext.sessionState).LogOut();

            if (!ModelState.IsValid)
            {
                return this.login(login, EnumEck.Error);
            }

            commandBus.Submit<ILoginCommand>(login, EnumCommandTransaction.BeginTransaction);

            if (!((ISession)ErsContext.sessionState).CheckPasswordExpiration())
                return this.login_change();

            return View("loginaction");

            //return this.Redirect("/");
        }

        /// <summary>
        /// logout
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [ErsAuthorization]
        public ActionResult logoutaction()
        {
            ((ISession)ErsContext.sessionState).LogOut();

            return this.login(new Login());
        }

        /// <summary>
        /// logout
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [ErsAuthorization]
        public ActionResult login_change()
        {
            var pass_change = new Pass_change();

            mapperBus.Map<IPassChangeMappable>(pass_change);

            pass_change.SetOutputHidden(true);

            return View("login_change", pass_change);
        }

        /// <summary>
        /// logout
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [ErsAuthorization]
        public ActionResult pass_change(Pass_change pass_change, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPassChangeCommand>(pass_change), pass_change);
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    this.ClearModelState(pass_change);
                }
            }

            return View("pass_change", pass_change);
        }

        /// <summary>
        /// logout
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [ErsAuthorization]
        public ActionResult passchangeaction(Pass_change pass_change)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPassChangeCommand>(pass_change), pass_change);
            if (!ModelState.IsValid)
            {
                return this.pass_change(pass_change, EnumEck.Error);
            }

            commandBus.Submit<IPassChangeCommand>(pass_change, EnumCommandTransaction.BeginTransaction);

            return View("loginaction");
        }
    }
}
