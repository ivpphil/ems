﻿using System.Web.Mvc;
using ersAdmin.Models;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.store;
using jp.co.ivp.ers;
using ersAdmin.Domain.Store.Commands;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class StoreController
        : ErsControllerSecureAdmin
    {
        /// <summary>
        /// トップ
        /// </summary>
        /// <returns></returns>
        public ActionResult index()
        {
            return View("index");
        }

        /// <summary>
        /// 店舗管理者設定
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("store_store_admin", mode = EnumHandlingMode.RESET)]
        public ActionResult store_admin(Store_admin store_admin, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(this.commandBus.Validate<IStoreAdminCommand>(store_admin), store_admin);
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    return GetErrorView();
                }

                this.mapperBus.Map<IStoreAdminMappable>(store_admin);
            }

            return View("store_admin", store_admin);
        }

        [ErsAdminProcessCompletion("store_store_admin", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult store_admin2(Store_admin store_admin)
        {
            ModelState.AddModelErrors(this.commandBus.Validate<IStoreAdminCommand>(store_admin), store_admin);
            if (!ModelState.IsValid)
            {
                return this.store_admin(store_admin, EnumEck.Error);
            }

            this.commandBus.Submit<IStoreAdminCommand>(store_admin, EnumCommandTransaction.BeginTransaction);

            return View("store_admin2");
        }

        /// <summary>
        /// 各種注記設定
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("store_store_alert", mode = EnumHandlingMode.RESET)]
        public ActionResult store_alert(Store_alert store_alert, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreAlertCommand>(store_alert), store_alert);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    ClearModelState(store_alert); 
            }
            this.mapperBus.Map<IStoreAlertMappable>(store_alert);

            return View("store_alert", store_alert);
        }

        [ErsAdminProcessCompletion("store_store_alert", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult store_alert2(Store_alert store_alert)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreAlertCommand>(store_alert), store_alert);
            if (!ModelState.IsValid)
            {
                return this.store_alert(store_alert, EnumEck.Error);
            }

            //store_alert.InsertAlertData(store_alert);

            this.commandBus.Submit<IStoreAlertCommand>(store_alert, EnumCommandTransaction.BeginTransaction);

            return View("store_alert2");
        }

        /// <summary>
        /// 送料設定
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("store_store_carriage", mode = EnumHandlingMode.RESET)]
        public ActionResult store_carriage(Store_carriage store_carriage, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreCarriageCommand>(store_carriage), store_carriage);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    ClearModelState(store_carriage); 
                }
            }
            
            this.mapperBus.Map<IStoreCarriageMappable>(store_carriage);            

            return View("store_carriage", store_carriage);
        }

        [ErsAdminProcessCompletion("store_store_carriage", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult store_carriage2(Store_carriage store_carriage)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreCarriageCommand>(store_carriage), store_carriage);
            if (!ModelState.IsValid)
            {
                return this.store_carriage(store_carriage, EnumEck.Error);
            }

            commandBus.Submit<IStoreCarriageCommand>(store_carriage, EnumCommandTransaction.BeginTransaction);
            
            return View("store_carriage2");
        }
        /// <summary>
        /// 消費税設定
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("store_store_tax", mode = EnumHandlingMode.RESET)]
        public ActionResult store_tax(Store_tax store_tax, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreTaxCommand>(store_tax), store_tax);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    ClearModelState(store_tax); 
            }
            else
            {
                this.mapperBus.Map<IStoreTaxMappable>(store_tax);  
            }
            return View("store_tax", store_tax);
        }

        [ErsAdminProcessCompletion("store_store_tax", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult store_tax2(Store_tax store_tax)
        {
            store_tax.store_tax_btn = true;
            ModelState.AddModelErrors(commandBus.Validate<IStoreTaxCommand>(store_tax), store_tax);
            if (!ModelState.IsValid)
            {
                return this.store_tax(store_tax, EnumEck.Error);
            }
            commandBus.Submit<IStoreTaxCommand>(store_tax, EnumCommandTransaction.BeginTransaction);

            return View("store_tax2");
        }

        /// <summary>
        /// 配送業者設定
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("store_store_delivery", mode = EnumHandlingMode.RESET )]
        public ActionResult store_delivery(Store_delivery store_delivery,EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreDeliveryCommand>(store_delivery), store_delivery);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    ClearModelState(store_delivery); 
            }
            else
            {
                this.mapperBus.Map<IStoreDeliveryMappable>(store_delivery);
            }
            return View("store_delivery", store_delivery);
        }
        [ErsAdminProcessCompletion("store_store_delivery", mode = EnumHandlingMode.COMPLETION )]
        public ActionResult store_delivery2(Store_delivery store_delivery)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreDeliveryCommand>(store_delivery), store_delivery);
            if (!ModelState.IsValid)
            {
                return this.store_delivery(store_delivery, EnumEck.Error);
            }
            commandBus.Submit<IStoreDeliveryCommand>(store_delivery, EnumCommandTransaction.BeginTransaction);
            return View("store_delivery2");
        }

        /// <summary>
        /// 手数料設定
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("store_store_etc", mode = EnumHandlingMode.RESET)]
        public ActionResult store_etc(Store_etc etc, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreEtcCommand>(etc), etc);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    ClearModelState(etc);
            }
            else
            {
                this.mapperBus.Map<IStoreEtcMappable>(etc);
            }

            return View("store_etc", etc);
        }

        [ErsAdminProcessCompletion("store_store_etc", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult store_etc2(Store_etc etc)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreEtcCommand>(etc), etc);
            if (!ModelState.IsValid)
            {
                return this.store_etc(etc, EnumEck.Error);
            }

            commandBus.Submit<IStoreEtcCommand>(etc, EnumCommandTransaction.BeginTransaction);

            return View("store_etc2");
        }

        /// <summary>
        /// お支払い方法設定
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("store_store_payment", mode = EnumHandlingMode.RESET)]
        public ActionResult store_payment(Store_payment payment, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStorePaymentCommand>(payment), payment);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    ClearModelState(payment); 
            }
            else
            {
                this.mapperBus.Map<IStorePaymentMappable>(payment);  
            }
            

            return View("store_payment", payment);
        }

        [ErsAdminProcessCompletion("store_store_payment", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult store_payment2(Store_payment payment)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStorePaymentCommand>(payment), payment);
            if (!ModelState.IsValid)
            {
                return this.store_payment(payment, EnumEck.Error);
            }

            commandBus.Submit<IStorePaymentCommand>(payment, EnumCommandTransaction.BeginTransaction);

            return View("store_payment2", payment);
        }

        public ActionResult store_delivery_day(Store_delivery_day store_delivery_day,EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreDeliveryDayCommand>(store_delivery_day), store_delivery_day);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    ClearModelState(store_delivery_day);
            }

            this.mapperBus.Map<IStoreDeliveryDayMappable>(store_delivery_day);

            return View("store_delivery_day", store_delivery_day);
        }

        public ActionResult store_delivery_day2(Store_delivery_day store_delivery_day)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreDeliveryDayCommand>(store_delivery_day), store_delivery_day);
            if (!ModelState.IsValid)
            {
                return this.store_delivery_day(store_delivery_day, EnumEck.Error);
            }

            commandBus.Submit<IStoreDeliveryDayCommand>(store_delivery_day, EnumCommandTransaction.BeginTransaction);
            return View("store_delivery_day2");
        }

        public ActionResult store_sendtime_delete(Store_delivery_day store_delivery_day)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDeleteDeliveryDayCommand>(store_delivery_day), store_delivery_day);
            if (!ModelState.IsValid)
            {
                return this.store_delivery_day(store_delivery_day, EnumEck.Error);
            }

            commandBus.Submit<IDeleteDeliveryDayCommand>(store_delivery_day, EnumCommandTransaction.BeginTransaction);

            return this.store_delivery_day(store_delivery_day);
        }

        [ErsAdminProcessCompletion("store_store_calendar", mode = EnumHandlingMode.RESET )]
        public ActionResult store_calendar(Store_calendar store_calendar, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreCalendarCommand>(store_calendar), store_calendar);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    ClearModelState(store_calendar);
            }
            else
            {
                this.mapperBus.Map<IStoreCalendarMappable>(store_calendar);
            }
            return View("store_calendar", store_calendar);
        }
        [ErsAdminProcessCompletion("store_store_calendar", mode = EnumHandlingMode.COMPLETION )]
        public ActionResult store_calendar2(Store_calendar store_calendar)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreCalendarCommand>(store_calendar), store_calendar);
            if (!ModelState.IsValid)
            {
                return this.store_calendar(store_calendar, EnumEck.Error);
            }
            commandBus.Submit<IStoreCalendarCommand>(store_calendar, EnumCommandTransaction.BeginTransaction);
            return View("store_calendar2");
        }

        [ErsAdminProcessCompletion("store_store_mail_text", mode = EnumHandlingMode.RESET)]
        public ActionResult store_mail_text(Store_mail_text store_mail_text,EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreMailTextCommand>(store_mail_text), store_mail_text);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    ClearModelState(store_mail_text);
            }
            else
            {
                this.mapperBus.Map<IStoreMailTextMappable>(store_mail_text);
            }
            return View("store_mail_text", store_mail_text);
        }

        [ErsAdminProcessCompletion("store_store_mail_text", mode = EnumHandlingMode.COMPLETION )]
        public ActionResult store_mail_text2(Store_mail_text store_mail_text)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreMailTextCommand>(store_mail_text), store_mail_text);
            if (!ModelState.IsValid)
            {
                return this.store_mail_text(store_mail_text, EnumEck.Error);
            }
            commandBus.Submit<IStoreMailTextCommand>(store_mail_text, EnumCommandTransaction.BeginTransaction);
            return View("store_mail_text2");
        }

        [ErsAdminProcessCompletion("store_store_mall_mail_text", mode = EnumHandlingMode.RESET)]
        public ActionResult store_mall_mail_text(Store_mall_mail_text store_mail_text, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreMallMailTextCommand>(store_mail_text), store_mail_text);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    ClearModelState(store_mail_text);
            }
            else
            {
                this.mapperBus.Map<IStoreMallMailTextMappable>(store_mail_text);
            }
            return View("store_mall_mail_text", store_mail_text);
        }

        [ErsAdminProcessCompletion("store_store_mall_mail_text", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult store_mall_mail_text2(Store_mall_mail_text store_mail_text)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreMallMailTextCommand>(store_mail_text), store_mail_text);
            if (!ModelState.IsValid)
            {
                return this.store_mall_mail_text(store_mail_text, EnumEck.Error);
            }
            commandBus.Submit<IStoreMallMailTextCommand>(store_mail_text, EnumCommandTransaction.BeginTransaction);
            return View("store_mall_mail_text2");
        }

        /// <summary>
        /// 管理者権限設定一覧
        /// </summary>
        /// <returns></returns>
        public ActionResult role(Role List, EnumEck? eck = null)
        {
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    return GetErrorView();
            }
            else
            {
                mapperBus.Map<IRoleMappable>(List);
            }

            return View("role", List);
        }


        /// <summary>
        /// 管理者権限設定新規登録
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("store_role_regist", mode = EnumHandlingMode.RESET)]
        public ActionResult role_regist(RoleRegist Regist, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IRoleRegistCommand>(Regist), Regist);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    return GetErrorView();
                }
            }

            return View("role_regist", Regist);
        }

        /// <summary>
        /// 管理者権限設定新規登録完了
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ErsAdminProcessCompletion("store_role_regist", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult role_regist_complete(RoleRegist Regist)
        {
            ModelState.AddModelErrors(commandBus.Validate<IRoleRegistCommand>(Regist), Regist);
            if (!ModelState.IsValid)
            {
                return this.role_regist(Regist, EnumEck.Error);
            }

            this.commandBus.Submit<IRoleRegistCommand>(Regist, EnumCommandTransaction.BeginTransaction);

            return View("role_regist_complete");
        }

        /// <summary>
        /// 管理者権限設定詳細（更新・削除）
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("store_role_modify", mode = EnumHandlingMode.RESET)]
        public ActionResult role_detail(RoleModify Modify, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IRoleModifyCommand>(Modify), Modify);
            if (!Modify.IsValidField("id"))
            {
                return GetErrorView();
            }

            if (!this.IsErrorBack(eck))
            {
                //Modify.LoadObject();
                mapperBus.Map<IRoleModifyMappable>(Modify);
            }
            //Modify.Init();
            return View("role_detail", Modify);

        }

        /// <summary>
        /// 管理者権限設定詳細完了
        /// </summary>
        /// <param name="Modify"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsAdminProcessCompletion("store_role_modify", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult role_detail_complete(RoleModify Modify)
        {
            ModelState.AddModelErrors(commandBus.Validate<IRoleModifyCommand>(Modify), Modify);
            if (!ModelState.IsValid)
            {
                return this.role_detail(Modify, EnumEck.Error);
            }
            commandBus.Submit<IRoleModifyCommand>(Modify,EnumCommandTransaction.BeginTransaction);
            commandBus.Submit<IRoleDeleteCommand>(Modify, EnumCommandTransaction.BeginTransaction);

            return View("role_detail_complete", Modify);
        }

        /// <summary>
        /// モール店舗設定 [Mall settings]
        /// </summary>
        /// <param name="model">モデル [Model]</param>
        /// <param name="eck">戻りフラグ [Return falsg]</param>
        /// <returns>結果 [Result]</returns>
        [ErsAdminProcessCompletion("store_mall_setting", mode = EnumHandlingMode.RESET)]
        public ActionResult store_mall_setting(store_mall_setting model, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreMallSettingCommand>(model), model);

            if (!this.IsErrorBack(eck))
            {
                ClearModelState(model);
                model.isFirstDisplay = true;
            }

            this.mapperBus.Map<IStoreMallSettingMappable>(model);

            return View("store_mall_setting", model);
        }

        /// <summary>
        /// モール店舗設定完了 [Mall settings complete]
        /// </summary>
        /// <param name="model">モデル [Model]</param>
        /// <returns>結果 [Result]</returns>
        [ErsAdminProcessCompletion("store_mall_setting", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult store_mall_setting_complete(store_mall_setting model)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStoreMallSettingCommand>(model), model);

            if (!ModelState.IsValid)
            {
                return this.store_mall_setting(model, EnumEck.Error);
            }

            commandBus.Submit<IStoreMallSettingCommand>(model, EnumCommandTransaction.BeginTransaction);

            return View("store_mall_setting_complete");
        }


        /// <summary>
        /// 共通名称マスタ設定
        /// </summary>
        /// <param name="model">モデル [Model]</param>
        /// <param name="eck">戻りフラグ [Return falsg]</param>
        /// <returns>結果 [Result]</returns>
        [ErsAdminProcessCompletion("common_name_regist", mode = EnumHandlingMode.RESET)]
        public ActionResult common_name(common_name_regist model)
        {
            if (!ModelState.IsValid)
            {
                model.type_code = null;
            }
            ModelState.AddModelErrors(commandBus.Validate<ICommonNameCommand>(model), model);

            if (!ModelState.IsValid)
            {
                model.error_flg = true;
                this.mapperBus.Map<ICommonNameRegistMappable>(model);
                return View("common_name", model);
            }
            ClearModelState(model);
            if (!model.regist){
                this.mapperBus.Map<ICommonNameRegistMappable>(model);
                if (model.Type_Code_List.Count <= 0)
                {
                    return GetErrorView("対象データが存在しません");
                }
            }

            if (model.regist)
            {
                commandBus.Submit<ICommonNameCommand>(model, EnumCommandTransaction.BeginTransaction);
                return View("common_name_complete");
            }

            return View("common_name", model);
        }

    }
}
