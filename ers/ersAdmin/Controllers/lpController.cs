﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.db;
using ersAdmin.Models.lp;
using jp.co.ivp.ers;
using ersAdmin.Domain.Lp.Mappables;
using ersAdmin.Domain.Lp.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Reflection;

namespace ersAdmin.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class lpController
        : ErsControllerSecureAdmin
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lpSearch"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_landing_list_copy", mode = EnumHandlingMode.RESET)]
        public ActionResult landing_list(lp_search lpSearch, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    return GetErrorView();
                }
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", lpSearch.pageCnt, lpSearch.maxItemCount);
            lpSearch.pager = pager;

            this.mapperBus.Map<ILandingListMappable>(lpSearch);

            pager.LoadPageList(lpSearch.recordCount);

            lpSearch.SetOutputHidden(true);
            return View("landing_list", lpSearch);
        }

        public ActionResult download_csv_lp_dquest(download_csv_lp_dquest download_csv_lp_dquest)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IDownloadCsvLpDQuestCommand>(download_csv_lp_dquest), download_csv_lp_dquest);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<IDownloadCsvLpDQuestMappable>(download_csv_lp_dquest);

            return this.CsvFile(download_csv_lp_dquest.csvCreater.filePath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lpSearch"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_landing_list_copy", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult landing_list_copy(lp_regist regist, lp_search lpSearch, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<ILandingListCopyCommand>(regist), regist);
            if (!ModelState.IsValid)
            {
                this.lpregist_base(regist, lpSearch, EnumEck.Error);
            }

            this.commandBus.Submit<ILandingListCopyCommand>(regist, EnumCommandTransaction.BeginTransaction);

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            return View("lpregist_complete", regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lpregist", mode = EnumHandlingMode.RESET)]
        public ActionResult lpregist_base(lp_regist regist, lp_search lpSearch, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                ModelState.AddModelErrors(commandBus.Validate<ILpRegistCommand>(regist), regist);
                if (!ModelState.IsValid)
                {
                    this.ClearModelState(regist, lpSearch);//Modelのエラーを無視
                }
            }

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.SetOutputHidden(true);

            return View("lpregist_base", regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lpregist", mode = EnumHandlingMode.CHECK)]
        public ActionResult lpregist_detail(lp_regist regist, lp_search lpSearch, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                ModelState.AddModelErrors(commandBus.Validate<ILpRegistCommand>(regist), regist);
                if (!ModelState.IsValid)
                {
                    return this.lpregist_base(regist, lpSearch, EnumEck.Error);
                }

                regist.IsSaveTempImage = true;
                this.commandBus.Submit<ILpRegistCommand>(regist, EnumCommandTransaction.BeginTransaction);
            }

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.SetOutputHidden(true);

            return View("lpregist_detail", regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lpregist", mode = EnumHandlingMode.CHECK)]
        public ActionResult lpregist_questionnaire(lp_regist regist, lp_search lpSearch, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                ModelState.AddModelErrors(commandBus.Validate<ILpRegistCommand>(regist), regist);
                if (!ModelState.IsValid)
                {
                    return this.lpregist_detail(regist, lpSearch, EnumEck.Error);
                }
            }
            else
            {
                regist.IsConfirmationPage = true;
            }

            this.mapperBus.Map<ILpRegistMappable>(regist);

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.SetOutputHidden(true);

            return View("lpregist_questionnaire", regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lpregist", mode = EnumHandlingMode.CHECK)]
        public ActionResult lpregist_confirm(lp_regist regist, lp_search lpSearch)
        {
            ModelState.AddModelErrors(commandBus.Validate<ILpRegistCommand>(regist), regist);
            if (!ModelState.IsValid)
            {
                return this.lpregist_questionnaire(regist, lpSearch, EnumEck.Error);
            }

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.IsConfirmationPage = true;
            this.mapperBus.Map<ILpRegistMappable>(regist);

            regist.SetOutputHidden(true);

            return View("lpregist_confirm", regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lpregist", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult lpregist_complete(lp_regist regist, lp_search lpSearch)
        {
            ModelState.AddModelErrors(commandBus.Validate<ILpRegistCommand>(regist), regist);
            if (!ModelState.IsValid)
            {
                this.lpregist_base(regist, lpSearch, EnumEck.Error);
            }

            this.commandBus.Submit<ILpRegistCommand>(regist, EnumCommandTransaction.BeginTransaction);

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            return View("lpregist_complete", regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lpmodify", mode = EnumHandlingMode.RESET)]
        [ErsAdminProcessCompletion("lpController_lp_page_regist", mode = EnumHandlingMode.RESET)]
        public ActionResult lpmodify_confirm(lp_regist regist, lp_search lpSearch, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                ModelState.AddModelErrors(commandBus.Validate<ILpModifyCommand>(regist), regist);
                if (!ModelState.IsValid)
                {
                    if (regist.lpregist_base)
                    {
                        return this.lpmodify_base(regist, lpSearch, EnumEck.Error);
                    }
                    else if (regist.lpregist_detail)
                    {
                        return this.lpmodify_detail(regist, lpSearch, EnumEck.Error);
                    }
                    else if (regist.lpregist_questionnaire)
                    {
                        return this.lpmodify_questionnaire(regist, lpSearch, EnumEck.Error);
                    }
                    else
                    {
                        return this.landing_list(lpSearch, EnumEck.Error);
                    }
                }
            }

            if (this.IsErrorBack(eck) || regist.lpregist_base || regist.lpregist_detail || regist.lpregist_questionnaire || regist.lpregist_confirm)
            {
                regist.IsConfirmationPage = true;
            }
            this.mapperBus.Map<ILpModifyMappable>(regist);

            //テンポラリ画像保存
            if (regist.lpregist_base)
            {
                regist.IsSaveTempImage = true;
                this.commandBus.Submit<ILpRegistCommand>(regist, EnumCommandTransaction.BeginTransaction);
            }

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.SetOutputHidden(true);

            return View("lpmodify_confirm", regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lpmodify", mode = EnumHandlingMode.CHECK)]
        public ActionResult lpmodify_base(lp_regist regist, lp_search lpSearch, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                ModelState.AddModelErrors(commandBus.Validate<ILpModifyCommand>(regist), regist);
                if (!ModelState.IsValid)
                {
                    this.ClearModelState(regist, lpSearch);//Modelのエラーを無視
                }
            }

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.SetOutputHidden(true);

            return View("lpmodify_base", regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lpmodify", mode = EnumHandlingMode.CHECK)]
        public ActionResult lpmodify_detail(lp_regist regist, lp_search lpSearch, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                ModelState.AddModelErrors(commandBus.Validate<ILpModifyCommand>(regist), regist);
                if (!ModelState.IsValid)
                {
                    this.ClearModelState(regist, lpSearch);//Modelのエラーを無視
                }
            }

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.SetOutputHidden(true);

            return View("lpregist_detail", regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lpmodify", mode = EnumHandlingMode.CHECK)]
        public ActionResult lpmodify_questionnaire(lp_regist regist, lp_search lpSearch, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                ModelState.AddModelErrors(commandBus.Validate<ILpModifyCommand>(regist), regist);
                if (!ModelState.IsValid)
                {
                    this.ClearModelState(regist, lpSearch);//Modelのエラーを無視
                }
            }

            regist.IsConfirmationPage = true;
            this.mapperBus.Map<ILpModifyMappable>(regist);

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.SetOutputHidden(true);

            return View("lpregist_questionnaire", regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lpmodify", mode = EnumHandlingMode.COMPLETION)]
        [ErsAdminProcessCompletion("lpController_lp_page_regist", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult lpmodify_complete(lp_regist regist, lp_search lpSearch)
        {
            ModelState.AddModelErrors(commandBus.Validate<ILpModifyCommand>(regist), regist);
            if (!ModelState.IsValid)
            {
                return this.landing_list(lpSearch, EnumEck.Error);
            }

            this.commandBus.Submit<ILpModifyCommand>(regist, EnumCommandTransaction.BeginTransaction);

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            return View("lpregist_complete", regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lpmodify", mode = EnumHandlingMode.COMPLETION)]
        [ErsAdminProcessCompletion("lpController_lp_page_regist", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult lpdelete(lp_regist regist, lp_search lpSearch)
        {
            ModelState.AddModelErrors(commandBus.Validate<ILpDeleteCommand>(regist), regist);
            if (!ModelState.IsValid)
            {
                return this.landing_list(lpSearch, EnumEck.Error);
            }

            this.commandBus.Submit<ILpDeleteCommand>(regist, EnumCommandTransaction.BeginTransaction);

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            return View("lpdelete", regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lp_page_regist", mode = EnumHandlingMode.CHECK)]
        public ActionResult lp_page_regist_template(lp_page_regist page_regist, lp_regist regist, lp_search lpSearch, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    return this.lpmodify_confirm(regist, lpSearch, EnumEck.Error);
                }
            }

            this.mapperBus.Map<ILpPageRegistTemplateMappable>(page_regist);

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.SetOutputHidden(true);
            this.AddModelToView(regist);

            page_regist.SetOutputHidden("common", true);

            return View("lp_page_regist_template", page_regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lp_page_regist", mode = EnumHandlingMode.CHECK)]
        public ActionResult lp_page_regist(lp_page_regist page_regist, lp_regist regist, lp_search lpSearch, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                ModelState.AddModelErrors(commandBus.Validate<ILpPageRegistCommand>(page_regist), page_regist);
                if (!ModelState.IsValid)
                {
                    return this.lpmodify_confirm(regist, lpSearch, EnumEck.Error);
                }

                page_regist.IsInitialize = true;
            }

            this.mapperBus.Map<ILpPageRegistMappable>(page_regist);

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.SetOutputHidden(true);
            this.AddModelToView(regist);

            page_regist.SetOutputHidden("template", true);

            return View("lp_page_regist", page_regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lp_page_regist", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult lp_page_regist_complete(lp_page_regist page_regist, lp_regist regist, lp_search lpSearch)
        {
            ModelState.AddModelErrors(commandBus.Validate<ILpPageRegistCommand>(page_regist), page_regist);
            if (!ModelState.IsValid)
            {
                return this.lp_page_regist(page_regist, regist, lpSearch, EnumEck.Error);
            }

            this.commandBus.Submit<ILpPageRegistCommand>(page_regist, EnumCommandTransaction.BeginTransaction);

            //lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.SetOutputHidden(true);
            this.AddModelToView(regist);

            page_regist.SetOutputHidden(true);

            return View("lp_page_regist_complete", page_regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lp_page_regist", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult lp_page_delete(lp_page_regist page_regist, lp_regist regist, lp_search lpSearch)
        {
            ModelState.AddModelErrors(commandBus.Validate<ILpPageDeleteCommand>(page_regist), page_regist);
            if (!ModelState.IsValid)
            {
                return this.lp_page_regist(page_regist, regist, lpSearch, EnumEck.Error);
            }

            this.commandBus.Submit<ILpPageDeleteCommand>(page_regist, EnumCommandTransaction.BeginTransaction);

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.SetOutputHidden(true);
            this.AddModelToView(regist);

            page_regist.SetOutputHidden(true);

            return View("lp_page_regist_complete", page_regist);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regist"></param>
        /// <param name="lpSearch"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("lpController_lp_page_regist", mode = EnumHandlingMode.CHECK)]
        public ActionResult lp_page_preview(lp_page_preview page_preview, lp_regist regist, lp_search lpSearch)
        {
            page_preview.objLpRegist = regist;

            ModelState.AddModelErrors(commandBus.Validate<ILpPageDeleteCommand>(page_preview), page_preview);
            if (!ModelState.IsValid)
            {
                return this.lp_page_regist(page_preview, regist, lpSearch, EnumEck.Error);
            }

            regist.IsConfirmationPage = true;
            this.mapperBus.Map<ILpRegistMappable>(regist);
            this.mapperBus.Map<ILpPreviewPageMappable>(page_preview);

            lpSearch.SetOutputHidden(true);
            this.AddModelToView(lpSearch);

            regist.SetOutputHidden(true);
            this.AddModelToView(regist);

            page_preview.SetOutputHidden(true);

            return View("lp_page_preview", page_preview);
        }
    }
}
