﻿using System.Web.Mvc;
using ersAdmin.Domain.Customer.Commands;
using ersAdmin.Domain.Customer.Mappables;
using ersAdmin.Models;
using ersAdmin.Models.customer;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ersAdmin.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class customerController
        : ErsControllerSecureAdmin
    {
        /// <summary>
        /// トップ
        /// </summary>
        /// <returns></returns>
        public ActionResult index()
        {
            return View("index");
        }

        /// <summary>
        /// 顧客検索
        /// </summary>
        /// <returns></returns>
        public ActionResult cus_search(Customer_search customer_search, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                this.ClearModelState(customer_search);
            }
            return View("cus_search", customer_search);
        }
        
        /// <summary>
        /// 顧客検索リスト
        /// </summary>
        /// <returns></returns>
        public ActionResult cus_list(Customer_search customer_search)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<ICustomerSearchCommand>(customer_search), customer_search);
            if (!ModelState.IsValid)
            {
                return this.cus_search(customer_search, EnumEck.Error);
            }

            //Pager設定
            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", customer_search.pageCnt, customer_search.maxItemCount);

            customer_search.pager = pager;

            //顧客一覧検索
            mapperBus.Map<ICustomerSearchMappable>(customer_search);

            if (this.HasInformation)
            {
                return this.cus_search(customer_search, EnumEck.Error);
            }

            customer_search.SetOutputHidden(true);

            pager.LoadPageList(customer_search.recordCount);

            return View("cus_list", customer_search);
        }

        /// <summary>
        /// 顧客詳細
        /// </summary>
        /// <param name="customer_search"></param>
        /// <param name="customer"></param>
        /// <param name="zip_search"></param>
        /// <param name="error_back">他のActionから戻ってきた時はTrue</param>
        /// <returns></returns>
        public ActionResult cus_detail(Customer_search customer_search, Customer customer, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICustomerCommand>(customer), customer);
            if (!this.IsErrorBack(eck))
            {
                if (!customer.IsValidField("mcode"))
                {
                    var message = customer.GetFieldErrorMessage("mcode");
                    this.ClearModelState(customer_search, customer);
                    throw new ErsException("universal", message);
                }
                this.ClearModelState(customer_search, customer);
                //顧客詳細検索
                customer.IsLoadDefaultData = true;
            }

            mapperBus.Map<ICustomerMappable>(customer);
            customer.SetOutputHidden("input_form", true);

            //検索条件は常に保持
            customer_search.SetOutputHidden(true);
            this.AddModelToView(customer_search);

            return View("cus_detail", customer);
        }
       
        /// <summary>
        /// 顧客完了画面
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult cus_complete(Customer_search customer_search, Customer customer)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICustomerCommand>(customer), customer);
            if (!ModelState.IsValid)
            {
                return cus_detail(customer_search, customer, EnumEck.Error);
            }

            //顧客情報更新
            customer.IsModifyCompletionPage = true;
            this.mapperBus.Map<ICustomerMappable>(customer);
            commandBus.Submit((ICustomerCommand)customer, EnumCommandTransaction.BeginTransaction);

            //検索条件は常に保持
            customer_search.SetOutputHidden(true);
            this.AddModelToView(customer_search);

            customer.SetOutputHidden("complete", true);

            return View("cus_complete", customer);
        }

        /// <summary>
        /// ポイント履歴検索
        /// </summary>
        /// <returns></returns>
        public ActionResult cus_point_search(Customer_search customer_search, Point_search point_search, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                this.ClearModelState(customer_search, point_search);
            }

            //Model情報をhiddenに設定
            point_search.SetOutputHidden("search", true);

            //検索条件は常に保持
            customer_search.SetOutputHidden(true);
            this.AddModelToView(customer_search);

            return View("cus_point_search", point_search);
        }

        /// <summary>
        /// ポイント履歴検索チェック
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("customerController_cus_point_search", mode = EnumHandlingMode.RESET)]
        public ActionResult cus_point_search_c(Customer_search customer_search, Point_search point_search, Point_add point_add, EnumEck? eck = null)
        {
            this.ClearModelState(point_add);

            // ポイント履歴検索
            point_search.IsCusPointSearch = true;
            this.mapperBus.Map<IPointSearchMappable>(point_search);

            //Model情報をhiddenに設定
            point_search.SetOutputHidden("search", true);

            this.AddModelToView("pointModel", point_add);

            customer_search.SetOutputHidden(true);
            this.AddModelToView(customer_search);

            ModelState.AddModelErrors(commandBus.Validate<IPointSearchCommand>(point_search), point_search);
            if (!ModelState.IsValid)
            {
                return View("cus_point_search", point_search);
            }
            //Model情報をhiddenに設定
            point_search.SetOutputHidden(true);

            return View("cus_point_list", point_search);
        }

        /// <summary>
        /// ポイント履歴リスト
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("customerController_cus_point_search", mode = EnumHandlingMode.RESET)]
        public ActionResult cus_point_list(Customer_search customer_search, Point_search point_search, Point_add point_add, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPointAddCommand>(point_add), point_add);
            if (!this.IsErrorBack(eck))
            {
                this.ClearModelState(point_search, point_add);
            }
            else
            {
                this.ClearModelState(point_search);
            }

            // ポイント履歴検索
            point_search.IsCusPointSearch = true;
            this.mapperBus.Map<IPointSearchMappable>(point_search);


            //Model情報をhiddenに設定
            point_search.SetOutputHidden(true);

            this.AddModelToView("pointModel", point_add);

            customer_search.SetOutputHidden(true);
            this.AddModelToView(customer_search);

            return View("cus_point_list", point_search);
        }

        /// <summary>
        /// ポイント加算
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("customerController_cus_point_search", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult cus_point_add(Customer_search customer_search, Point_search point_search, Point_add point_add)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPointAddCommand>(point_add), point_add);
            if (!ModelState.IsValid)
            {
                return this.cus_point_list(customer_search, point_search, point_add, EnumEck.Error);
            }

            //ポイント付与処理
            point_add.IsCusPointAdd = true;
            commandBus.Submit<IPointAddCommand>(point_add, EnumCommandTransaction.BeginTransaction);

            // ポイント履歴検索
            point_search.IsCusPointSearch = true;
            this.mapperBus.Map<IPointSearchMappable>(point_search);

            point_search.SetOutputHidden(true);

            customer_search.SetOutputHidden(true);
            this.AddModelToView(customer_search);

            return RedirectToAction("cus_point_list", new { mcode = point_search.mcode, sp_site_id = point_search.sp_site_id });
        }

        /// <summary>
        /// 個別メール配信
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public ActionResult cus_mail(Customer_search customer_search, Customer_mail customer_mail, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICustomerMailCommand>(customer_mail), customer_mail);
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    return GetErrorView();
                }

                customer_mail.IsLoadDefaultValue = true;
            }

            mapperBus.Map<ICustomerMailMappable>(customer_mail);
            customer_mail.SetOutputHidden(true);

            //検索条件は常に保持
            customer_search.SetOutputHidden(true);
            this.AddModelToView(customer_search);

            return View("cus_mail", customer_mail);
        }

        /// <summary>
        /// 個別メール配信
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public ActionResult cus_mail_complete(Customer_search customer_search, Customer_mail customer_mail)
        {
            customer_mail.IsSendMail = true;
            ModelState.AddModelErrors(commandBus.Validate<ICustomerMailCommand>(customer_mail), customer_mail);
            if (!ModelState.IsValid)
            {
                return cus_mail(customer_search, customer_mail, EnumEck.Error);
            }

            this.mapperBus.Map<ICustomerMailMappable>(customer_mail);

            var sendMail = ErsFactory.ersMailFactory.getErsSendMailAdminIndividual();
            sendMail.SendMail(customer_mail, customer_mail.from_email, customer_mail.email, customer_mail.send_to_admin, customer_mail.mail_title, customer_mail.mail_body, EnumMformat.PC);

            //検索条件は常に保持
            customer_search.SetOutputHidden(true);
            this.AddModelToView(customer_search);

            customer_mail.SetOutputHidden(true);
            return View("cus_mail_complete", customer_mail);
        }

        /// <summary>
        /// CSVダウンロード（全件）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_csv_all(Customer_search customer_search)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<ICustomerCSVMappable>(customer_search);

            return this.CsvFile(customer_search.csvCreater.filePath);
        }

        /// <summary>
        /// CSVダウンロード（ページ）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_csv_page(Customer_search customer_search)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            customer_search.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(customer_search.pageCnt, customer_search.maxItemCount);

            mapperBus.Map<ICustomerCSVMappable>(customer_search);

            return this.CsvFile(customer_search.csvCreater.filePath);
        }

        ///******************************
        ///カードエラー
        ///******************************
        /// <summary>
        /// クレジットカードエラー顧客検索
        /// </summary>
        /// <returns></returns>
        public ActionResult cus_crderr_search(Cus_Crderr_Search cus_crderr)
        {
            //var cus_crderr = new Cus_Crderr_Search();
            return View("cus_crderr_search", cus_crderr);
        }

        /// <summary>
        /// クレジットカードエラー顧客リスト
        /// </summary>
        /// <returns></returns>
        public ActionResult cus_crderr_list(Cus_Crderr_Search cus_crderr)
        {
            if (!ModelState.IsValid)
            {
                return cus_crderr_search(cus_crderr);
            }
            
            //Pager設定
            cus_crderr.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", cus_crderr.pageCnt, cus_crderr.maxItemCount);

            cus_crderr.SetOutputHidden(true);
            
            mapperBus.Map<ICusCrderrSearchMappable>(cus_crderr);

            cus_crderr.pager.LoadPageList(cus_crderr.recordCount);

            return View("cus_crderr_list", cus_crderr);
        }

        /// <summary>
        /// クレジットカードエラーCSVダウンロード（全件）
        /// </summary>
        /// <returns></returns>
        public ActionResult cus_crderr_dl_csv_all(Cus_Crderr_Search customer_search)
        {

            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<ICusCrderrCSVMappable>(customer_search);

            return this.CsvFile(customer_search.csvCreater.filePath);
        }

        /// <summary>
        /// クレジットカードエラーCSVダウンロード（ページ）
        /// </summary>
        /// <returns></returns>
        public ActionResult cus_crderr_dl_csv_page(Cus_Crderr_Search customer_search)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            customer_search.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(customer_search.pageCnt, customer_search.maxItemCount);

            mapperBus.Map<ICusCrderrCSVMappable>(customer_search);

            return this.CsvFile(customer_search.csvCreater.filePath);
        }

        /// <summary>
        /// @メールリスト生成画面
        /// </summary>
        /// <returns></returns>
        public ActionResult cus_mail_all(Customer_search customer_search)
        {
            customer_search.SetOutputHidden(true);
            return View("cus_mail_all", customer_search);
        }

        /// <summary>
        /// @メールリスト生成完了画面
        /// </summary>
        /// <param name="customer_search"></param>
        /// <param name="atMailModel"></param>
        /// <returns></returns>
        public ActionResult cus_mail_all_complete(Customer_search customer_search, cus_mail_all atMailModel)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICusMailAllCommand>(atMailModel), atMailModel);
            if (!ModelState.IsValid)
            {
                return View("cus_mail_all");
            }

            mapperBus.Map<ICusMailAllMappable>(atMailModel);
            commandBus.Submit<ICusMailAllCommand>(atMailModel, EnumCommandTransaction.BeginTransaction);

            return View("cus_mail_all_complete", atMailModel);
        }

        public ActionResult member_rank(member_rank_setup member_rank_setup, EnumEck? eck = null)
        {
            if (!IsErrorBack(eck))
            {
                this.ClearModelState(member_rank_setup);
            }

            mapperBus.Map<IMemberRankSetupMappable>(member_rank_setup);

            return View("member_rank", member_rank_setup);
        }

        public ActionResult member_rank_complete(member_rank_setup member_rank_setup)
        {
            member_rank_setup.IsCompletePage = true;
            this.ModelState.AddModelErrors(commandBus.Validate<IMemberRankSetupCommand>(member_rank_setup), member_rank_setup);
            if (!ModelState.IsValid)
            {
                return this.member_rank(member_rank_setup, EnumEck.Error);
            }

            commandBus.Submit<IMemberRankSetupCommand>(member_rank_setup, EnumCommandTransaction.BeginTransaction);

            member_rank_setup.SetOutputHidden("input_form", true);
            return View("member_rank_complete", member_rank_setup);
        }
    
    }
}
