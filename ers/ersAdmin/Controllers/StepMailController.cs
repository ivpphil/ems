﻿using System.Web.Mvc;
using ersAdmin.Domain.StepMail.Commands;
using ersAdmin.Domain.StepMail.Mappables;
using ersAdmin.Models.stepmail;
using ersAdmin.Models.stepMail;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ersAdmin.Controllers
{
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class StepMailController : ErsControllerSecureAdmin
    {
        //stepmail

        public ActionResult index()
        {
            return View("index");
        }


        /// <summary>
        /// Search step mail list
        /// </summary>
        /// <param name="stepmail_list"></param>
        /// <returns></returns>
        public ActionResult stepmail_list(stepmail_list stepmail_list)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //Pager設定
            stepmail_list.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", stepmail_list.pageCnt, stepmail_list.maxItemCount);

            mapperBus.Map<IStepmailRegistMappable>(stepmail_list);
            mapperBus.Map<IStepmailListMappable>(stepmail_list);

            stepmail_list.pager.LoadPageList(stepmail_list.recordCount);

            //FormタグにHiddenを自動入力
            stepmail_list.SetOutputHidden(true);

            return View("stepmail_list", stepmail_list);
        }

        /// <summary>
        /// regist step mail
        /// </summary>
        /// <param name="stepMail_regist"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("stepmail_stepmail_regist", mode = EnumHandlingMode.RESET)]
        public ActionResult stepmail_regist(stepmail_regist stepMail_regist, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStepmailRegistCommand>(stepMail_regist), stepMail_regist);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(stepMail_regist);
            }

            mapperBus.Map<IStepmailRegistMappable>(stepMail_regist);

            stepMail_regist.SetOutputHidden(true);

            return View("stepmail_regist", stepMail_regist);
        }

        /// <summary>
        /// Completion screen for step mail regist 
        /// </summary>
        /// <param name="stepMail_regist"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("stepmail_stepmail_regist", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult stepmail_regist_complete(stepmail_regist stepMail_regist)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStepmailRegistCommand>(stepMail_regist), stepMail_regist);
            if (!ModelState.IsValid)
            {
                return this.stepmail_regist(stepMail_regist, EnumEck.Error);
            }

            commandBus.Submit<IStepmailRegistCommand>(stepMail_regist, EnumCommandTransaction.BeginTransaction);

            return View("stepmail_complete", stepMail_regist);
        }

        /// <summary>
        /// modify step mail record
        /// </summary>
        /// <param name="stepMail_regist"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("stepmail_stepmail_modify", mode = EnumHandlingMode.RESET)]
        public ActionResult stepmail_modify(stepmail_modify modify, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStepmailModifyCommand>(modify), modify);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(modify);
                modify.IsLoadDefaultData = true;
            }

            mapperBus.Map<IStepmailRegistMappable>(modify);
            mapperBus.Map<IStepmailModifyMappable>(modify);

            //FormタグにHiddenを自動入力
            modify.SetOutputHidden(true);

            return View("stepmail_detail", modify);
        }

        /// <summary>
        /// completion screen for step mail modification
        /// </summary>
        /// <param name="stepMail_regist"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("stepmail_stepmail_modify", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult stepmail_modify_complete(stepmail_modify modify)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStepmailModifyCommand>(modify), modify);
            if (!ModelState.IsValid)
            {
                return this.stepmail_modify(modify, EnumEck.Error);
            }

            commandBus.Submit<IStepmailModifyCommand>(modify, EnumCommandTransaction.BeginTransaction);

            return View("stepmail_complete", modify);
        }

        /// <summary>
        /// completion screen for step mail delete
        /// </summary>
        /// <param name="stepMail_regist"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("stepmail_stepmail_modify", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult stepmail_delete_complete(stepmail_modify modify)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStepmailDeleteCommand>(modify), modify);
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            commandBus.Submit<IStepmailDeleteCommand>(modify, EnumCommandTransaction.BeginTransaction);

            return View("stepmail_complete", modify);
        }


        /////scenario

        /// <summary>
        /// Loads list of step mail scenario
        /// </summary>
        /// <param name="list">model list</param>
        /// <returns>returns scenario_list view with corresponding model as reference</returns>
        public ActionResult scenario_list(scenario_list list)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ////Pager設定
            list.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", list.pageCnt, list.maxItemCount);

            list.SetOutputHidden(true);

            mapperBus.Map<IScenarioListMappable>(list);

            list.pager.LoadPageList(list.recordCount);

            return View("scenario_list", list);

        }

        /// <summary>
        /// Loads page where users input all necessary details of a step mail scenario
        /// </summary>
        /// <param name="regist">model regist</param>
        /// <param name="eck">error indicator</param>
        /// <returns>returns scenario_regist view with corresponding model as reference</returns>
        [ErsAdminProcessCompletion("stepmail_scenario_regist", mode = EnumHandlingMode.RESET)]
        public ActionResult scenario_regist(scenario_regist regist, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IScenarioRegistCommand>(regist), regist);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(regist);
                regist.IsInitialization = true;
            }

            mapperBus.Map<IScenarioRegistMappable>(regist);

            regist.SetOutputHidden(true);

            return View("scenario_regist", regist);
        }

        /// <summary>
        /// Loads if registration of step mail secenario is successful 
        /// </summary>
        /// <param name="regist">model regist</param>
        /// <returns>returns scenario_complete view when registration is successful</returns>
        [ErsAdminProcessCompletion("stepmail_scenario_regist", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult scenario_regist_complete(scenario_regist regist)
        {
            ModelState.AddModelErrors(commandBus.Validate<IScenarioRegistCommand>(regist), regist);
            if (!ModelState.IsValid)
            {
                if (regist.submit_register)
                    return scenario_regist(regist, EnumEck.Error);
            }

            commandBus.Submit<IScenarioRegistCommand>(regist, EnumCommandTransaction.BeginTransaction);

            regist.SetOutputHidden(true);

            return View("scenario_complete", regist);
        }

        /// <summary>
        /// Loads page where users can edit/update details of a step mail scenario
        /// </summary>
        /// <param name="detail">model detail</param>
        /// <param name="eck">error indicator</param>
        /// <returns>returns scenario_detail view with corresponding model as reference</returns>
        [ErsAdminProcessCompletion("stepmail_scenario_modify", mode = EnumHandlingMode.RESET)]
        public ActionResult scenario_modify(scenario_modify modify, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IScenarioModifyCommand>(modify), modify);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(modify);
                modify.IsLoadDefault = true;
            }
            
            mapperBus.Map<IScenarioModifyMappable>(modify);

            modify.SetOutputHidden(true);

            return View("scenario_detail", modify);
        }

        /// <summary>
        /// Loads if Update of step mail secenario is successful 
        /// </summary>
        /// <param name="model">model detail</param>
        /// <returns>returns scenario_complete view when update is successful</returns>
        [ErsAdminProcessCompletion("stepmail_scenario_modify", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult scenario_modify_complete(scenario_modify model)
        {
            ModelState.AddModelErrors(commandBus.Validate<IScenarioModifyCommand>(model), model);
            if (!ModelState.IsValid)
            {
                if (model.submit_update)
                    return scenario_modify(model, EnumEck.Error);
            }

            model.SetOutputHidden(true);

            commandBus.Submit<IScenarioModifyCommand>(model, EnumCommandTransaction.BeginTransaction);


            return View("scenario_complete", model);
        }

        /// <summary>
        /// Loads if deletion of step mail secenario is successful 
        /// </summary>
        /// <param name="model">model detail</param>
        /// <returns>returns scenario_complete view when deletion is successful</returns>
        [ErsAdminProcessCompletion("stepmail_scenario_modify", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult scenario_delete_complete(scenario_modify modify)
        {
            ModelState.AddModelErrors(commandBus.Validate<IScenarioDeleteCommand>(modify), modify);
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            commandBus.Submit<IScenarioDeleteCommand>(modify, EnumCommandTransaction.BeginTransaction);

            return View("scenario_complete");
        }

        /// <summary>
        /// 商品番号検索ページ
        /// </summary>
        /// <returns></returns>
        public ActionResult item_search(item_search ItemList)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ItemList.SetOutputHidden("g_search_mode", true);

            return View("item_search", ItemList);
        }

        /// <summary>
        /// 商品番号選択ページ
        /// </summary>
        /// <returns></returns>
        public ActionResult item_search_list(item_search ItemList)
        {

            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ItemList.pageCnt, ItemList.maxItemCount);

            ItemList.pager = pager;
            mapperBus.Map<IItemSearchMappable>(ItemList);

            //FormタグにHiddenを自動入力
            ItemList.SetOutputHidden(true);

            pager.LoadPageList(ItemList.recordCount);

            return View("item_search", ItemList);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string getItemDetail(stepmail_getitemdetail model)
        {
            if (model.code != null)
            {
                mapperBus.Map<IGetItemDetailMappable>(model);  
            }

            return model.code_name;
        }

        /// <summary>
        /// ステップメール配信実績一覧
        /// </summary>
        /// <returns></returns>
        public ActionResult delvy_actual(delvy_actual_list objDelvy_actual_list)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", objDelvy_actual_list.pageCnt, objDelvy_actual_list.maxItemCount);

            objDelvy_actual_list.pager = pager; 
            mapperBus.Map<IDelvyActualListMappable>(objDelvy_actual_list);

            //FormタグにHiddenを自動入力
            objDelvy_actual_list.SetOutputHidden(true);

            pager.LoadPageList(objDelvy_actual_list.recordCount);

            return View("delvy_actual", objDelvy_actual_list);

        }

        /// <summary>
        /// ステップメール配信実績詳細
        /// </summary>
        /// <returns></returns>
        public ActionResult delvy_detail(delvy_detail objDelvy_detail, delvy_actual_list objDelvy_actual_list)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDelvyDetailCommand>(objDelvy_detail), objDelvy_detail);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<IDelvyDetailMappable>(objDelvy_detail);

            //検索条件保持
            objDelvy_actual_list.SetOutputHidden(true);
            this.AddModelToView(objDelvy_actual_list);

            return View("delvy_detail", objDelvy_detail);
        }

        /// <summary>
        /// ステップメール配信実績詳細
        /// </summary>
        /// <returns></returns>
        public ActionResult delvy_summary(delvy_summary objDelvy_summary, delvy_actual_list objDelvy_actual_list)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDelvySummaryCommand>(objDelvy_summary), objDelvy_summary);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<IDelvySummaryMappable>(objDelvy_summary);

            //検索条件保持
            objDelvy_actual_list.SetOutputHidden(true);
            this.AddModelToView(objDelvy_actual_list);

            return View("delvy_summary", objDelvy_summary);
        }
    }
}
