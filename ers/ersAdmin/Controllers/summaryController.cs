﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.summary;
using ersAdmin.Domain.Summary.Mappables;
using ersAdmin.Domain.Summary.Commands;

namespace ersAdmin.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class summaryController
         : ErsControllerSecureAdmin
    {
        public ActionResult index(SummaryList summaryList)
        {
            return this.summary_list(summaryList);
        }

        public ActionResult summary_list(SummaryList summaryList, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                if (!this.ModelState.IsValid)
                {
                    return this.GetErrorView();
                }
            }
            else
            {
                //Mapper制御
                summaryList.IsErrorBack = true;
            }

            this.mapperBus.Map<ISummaryListMappable>(summaryList);

            return View("summary_list", summaryList);
        }

        public ActionResult summary_result(SummaryList summaryList)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ISummaryResultCommand>(summaryList), summaryList);
            if (!this.ModelState.IsValid)
            {
                return this.summary_list(summaryList, EnumEck.Error);
            }

            this.mapperBus.Map<ISummaryResultMappable>(summaryList);

            summaryList.SetOutputHidden(true);

            return View("summary_result", summaryList);
        }

        public ActionResult summary_csv_dl(SummaryList summaryList)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ISummaryResultCommand>(summaryList), summaryList);
            this.ModelState.AddModelErrors(this.commandBus.Validate<ISummaryResultCSVCommand>(summaryList), summaryList);
            if (!this.ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<ISummaryResultCSVMappable>(summaryList);
            return this.CsvFile(summaryList.csvCreater.filePath);
        }
    }
}