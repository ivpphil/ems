﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using System.Collections;
using ersAdmin.Models;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using ersAdmin.Domain.Atmail.Commands;
using ersAdmin.Domain.Atmail.Mappables;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class atmailController
        : ErsControllerSecureAdmin
    {
        //
        // GET: /atmail/
        /// <summary>
        /// load index for atmail
        /// </summary>
        public ActionResult index(mailinglist maillist)
        {
            return View("index");
        }
        /// <summary>
        /// option for upload csv file to be copy or copy file from process records
        /// </summary>
        public ActionResult new_list()
        {
            return View();
        }

        /// <summary>
        /// copy uploaded csv file
        /// </summary>
        #region uploaded data
        /// <summary>
        /// select csv file
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_upload_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult new_list_up()
        {
            var mailto_csv_upload = new mailto_csv_upload();
            mailto_csv_upload.SetOutputHidden(true);
            return View(mailto_csv_upload);
        }

        /// <summary>
        /// confirm page for selected csv file
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_upload_ransu", mode = EnumHandlingMode.CHECK)]
        public ActionResult new_list_up_confirm(mailto_csv_upload mailto_csv_upload)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailtoCSVUploadCommand>(mailto_csv_upload), mailto_csv_upload);
            if (!ModelState.IsValid)
            {
                //エラーは確認画面に表示する。
            }

            mailto_csv_upload.SetOutputHidden(true);

            return View("new_list_up_confirm", mailto_csv_upload);
        }

        /// <summary>
        /// completed page for csv file (records has been uploaded)
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_upload_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult new_list_up_complete(mailto_csv_upload mailto_csv_upload)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailtoCSVUploadCommand>(mailto_csv_upload), mailto_csv_upload);
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            commandBus.Submit<IMailtoCSVUploadCommand>(mailto_csv_upload, EnumCommandTransaction.BeginTransaction);

            return View("new_list_up_complete", mailto_csv_upload);
        }
        #endregion

        /// <summary>
        /// copy file from process records
        /// </summary>
        #region copy process file
        /// <summary>
        /// list of all process records
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_copy_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult new_list_copy(mailinglist maillist)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", maillist.pageCnt, maillist.maxItemCount);

            maillist.pager = pager;

            maillist.SetOutputHidden(true);

            mapperBus.Map<IMailinglistMappable>(maillist);

            pager.LoadPageList(maillist.recordCount);

            return View("new_list_copy", maillist);
        }

        /// <summary>
        /// confirmation page for selected process record
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_copy_ransu", mode = EnumHandlingMode.CHECK)]
        public ActionResult new_list_copy_confirm(mailinfo_new_list mailinfo_new_list, mailinglist maillist)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailinfoNewListCommand>(mailinfo_new_list), mailinfo_new_list);
            if (!ModelState.IsValid)
            {
                return this.new_list_copy(maillist);
            }

            mapperBus.Map<IMailinfoNewListMappable>(mailinfo_new_list);
            mailinfo_new_list.SetOutputHidden(true);

            return View("new_list_copy_confirm", mailinfo_new_list);
        }

        /// <summary>
        /// complete page and process record has been copied
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_copy_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult new_list_copy_complete(mailinfo_new_list mailinfo_new_list, mailinglist maillist)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailinfoNewListCommand>(mailinfo_new_list), mailinfo_new_list);
            if (!ModelState.IsValid)
            {
                return this.new_list_copy_confirm(mailinfo_new_list, maillist);
            }

            commandBus.Submit<IMailinfoNewListCommand>(mailinfo_new_list, EnumCommandTransaction.BeginTransaction);

            return View("new_list_copy_complete", mailinfo_new_list);
        }
        #endregion

        /// <summary>
        /// display all the records on process table
        /// </summary>
        public ActionResult process(mailinglist maillist)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", maillist.pageCnt, maillist.maxItemCount);

            maillist.pager = pager;

            maillist.SetOutputHidden(true);

            mapperBus.Map<IMailinglistMappable>(maillist);

            if (maillist.list.Count == 0)
            {
                this.AddInformation(ErsResources.GetMessage("10200"));
            }

            pager.LoadPageList(maillist.recordCount);

            return View("process",maillist);
        }

        /// <summary>
        /// deleting records on process table
        /// </summary>
        #region deleting process
        /// <summary>
        /// loading of information of selected process to be deleted
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_filedel_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult file_del1(mailinfo_delete mailinfo_delete)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailinfoDeleteCommand>(mailinfo_delete), mailinfo_delete);
            if (!ModelState.IsValid)
            {
                return View("process");
            }

            mapperBus.Map<IMailinfoDeleteMappable>(mailinfo_delete);
            mailinfo_delete.SetOutputHidden(true);

            return View("file_del1", mailinfo_delete);
        }
        /// <summary>
        /// complete page for deleted process record
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_filedel_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult file_del2(mailinfo_delete mailinfo_delete)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailinfoDeleteCommand>(mailinfo_delete), mailinfo_delete);
            if (!ModelState.IsValid)
            {
                return View("file_del1", mailinfo_delete);
            }

            commandBus.Submit<IMailinfoDeleteCommand>(mailinfo_delete, EnumCommandTransaction.BeginTransaction);
            return View("file_del2", mailinfo_delete);
        }
        #endregion
        /// <summary>
        /// load information of process record if subject has been selected on process page
        /// </summary>
        public ActionResult mail_body(mailinfo_body mailinfo_body)
        {
            if (!ModelState.IsValid)
            {
                return View("process");
            }

            mapperBus.Map<IMailinfoBodyMappable>(mailinfo_body);

            return View("mail_body", mailinfo_body);
        }

        //[HttpPost]
        //public ActionResult h_menu(mailinfo mailinfo)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View("process");
        //    }

        //    mailinfo.LoadInfo();
        //    mailinfo.SetOutputHidden(true);

        //    return View("h_menu",mailinfo);
        //}

        /// <summary>
        /// use for mail delivery
        /// </summary>
        #region mail delivery
        /// <summary>
        /// loading of selected process information
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_tmail_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult tmail1(mailinfo mailinfo)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailinfoCommand>(mailinfo), mailinfo);
            if (!ModelState.IsValid)
            {
                return View("process");
            }


            mapperBus.Map<IMailinfoMappable>(mailinfo);
            mailinfo.SetOutputHidden(true);

            return View("tmail1",mailinfo);
        }

        /// <summary>
        /// modify page for process record
        /// </summary>
        [ErsAdminProcessCompletion("mypage_atmail_tmail_ransu", mode = EnumHandlingMode.CHECK)]
        public ActionResult tmail2(mailinfo mailinfo, EnumEck? eck = null)
        {
            mailinfo.IsTemplatePage2 = true;

            if (!this.IsErrorBack(eck))
            {
                mapperBus.Map<IMailinfoMappable>(mailinfo);
            }

            mailinfo.SetOutputHidden("mail1",true);
            return View("tmail2",mailinfo);
        }

        /// <summary>
        /// confirmation page of edited process record
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_tmail_ransu", mode = EnumHandlingMode.CHECK)]
        public ActionResult tmail3(mailinfo mailinfo, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailinfoCommand>(mailinfo), mailinfo);
            if (!ModelState.IsValid && !this.IsErrorBack(eck))
            {
                return this.tmail2(mailinfo, EnumEck.Error);
            }

            if (!this.IsErrorBack(eck))
            {
                mapperBus.Map<IMailinfoMappable>(mailinfo);
                commandBus.Submit<IMailinfoCommand>(mailinfo, EnumCommandTransaction.BeginTransaction);
            }

            mailinfo.SetOutputHidden("mail1",true);
            mailinfo.SetOutputHidden("mail2", true);

            return View("tmail3",mailinfo);
        }
    
        /// <summary>
        /// complete page for edited process record and edit mailto records and process records
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_tmail_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult tmail4(mailinfo mailinfo)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailinfoCommand>(mailinfo), mailinfo);
            if (!ModelState.IsValid)
            {
                return this.tmail3(mailinfo, EnumEck.Error);
            }

            mapperBus.Map<IMailinfoMappable>(mailinfo);
            commandBus.Submit<IMailinfoCommand>(mailinfo, EnumCommandTransaction.BeginTransaction);

            mailinfo.SetOutputHidden("mail1", true);
            mailinfo.SetOutputHidden("mail2", true);

            return View("tmail4",mailinfo);
        }

        /// <summary>
        /// complete page for edited process record and edit mailto records and process records
        /// </summary>
        public ActionResult testmail(mailinfo mailinfo, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<ITestMailCommand>(mailinfo), mailinfo);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(mailinfo);
                mapperBus.Map<ITestMailMappable>(mailinfo);
            }

            mailinfo.SetOutputHidden("mail1", true);
            mailinfo.SetOutputHidden("mail2", true);

            return View("testmail", mailinfo);
        }

        public ActionResult testmail_complete(mailinfo mailinfo)
        {
            ModelState.AddModelErrors(commandBus.Validate<ITestMailCommand>(mailinfo), mailinfo);
            if (!ModelState.IsValid)
            {
                return this.testmail(mailinfo, EnumEck.Error);
            }

            mapperBus.Map<ITestMailMappable>(mailinfo);
            commandBus.Submit<ITestMailCommand>(mailinfo, EnumCommandTransaction.BeginTransaction);

            mailinfo.SetOutputHidden("mail1", true);
            mailinfo.SetOutputHidden("mail2", true);

            return View("testmail_complete", mailinfo);
        }

        #endregion

        /// <summary>
        /// download csv file of members per process
        /// </summary>
        #region download file
        /// <summary>
        /// load process information to be downloaded
        /// </summary>
        [HttpPost]
        public ActionResult download1(mailinfo_download mailinfo_download)
        {
            if (!ModelState.IsValid)
            {
                return View("process");
            }

            mapperBus.Map<IMailinfoDownloadMappable>(mailinfo_download);
            mailinfo_download.SetOutputHidden(true);

            return View("download1", mailinfo_download);
        }

        //[HttpPost]
        //public ActionResult download2(mailinfo_download mailinfo_download)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View("download1", mailinfo_download);
        //    }

        //    //mailinfo.LoadInfo();
        //    mailinfo_download.SetOutputHidden(true);

        //    return View("download2", mailinfo_download);
        //}
        /// <summary>
        /// downloaded selected process record
        /// </summary>
        public ActionResult download_csv(mailinfo_download mailinfo_download)
        {

            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mailinfo_download.IsDownloadCompletionPage = true;
            mapperBus.Map<IMailinfoDownloadMappable>(mailinfo_download);

            return this.CsvFile(mailinfo_download.csvCreater.filePath);
        }
        #endregion

        /// <summary>
        /// summarry/status of process records
        /// </summary>
        public ActionResult total(mailinglist mailinglist)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<IMailinglistTotalMappable>(mailinglist);

            return View("total",mailinglist);
        }

        #region setup
        /// <summary>
        /// menu page for setup and template
        /// </summary>
        public ActionResult s_menu()
        {
            return View("s_menu");
        }

        /// <summary>
        /// load setup of return/reply email address information
        /// </summary>
        [ErsAdminProcessCompletion("mypage_atmail_emailsetup_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult s_popmail1(mailsetup mailsetup, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailsetupCommand>(mailsetup), mailsetup);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(mailsetup);
                mapperBus.Map<IMailsetupMappable>(mailsetup);
            }

            mailsetup.SetOutputHidden("mail",true);

            return View("s_popmail1", mailsetup);
        }
        /// <summary>
        /// confirmation page of return/reply email address information
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_emailsetup_ransu", mode = EnumHandlingMode.CHECK)]
        public ActionResult s_popmail2(mailsetup mailsetup)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailsetupCommand>(mailsetup), mailsetup);
            if (!ModelState.IsValid)
            {
                return this.s_popmail1(mailsetup, EnumEck.Error);
            }

            mailsetup.SetOutputHidden(true);

            return View("s_popmail2", mailsetup);
        }
        /// <summary>
        /// complete page and inserting/updating return/reply email address information
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_emailsetup_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult s_popmail3(mailsetup mailsetup)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailsetupCommand>(mailsetup), mailsetup);
            if (!ModelState.IsValid)
            {
                return this.s_popmail1(mailsetup, EnumEck.Error);
            }

            commandBus.Submit<IMailsetupCommand>(mailsetup, EnumCommandTransaction.BeginTransaction);
            return View("s_popmail3", mailsetup);
        }
        #endregion

        #region template
        /// <summary>
        /// load template list records
        /// </summary>
        public ActionResult template_list(mailtemplate mailtemplate)
        {
            
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", mailtemplate.pageCnt, mailtemplate.maxItemCount);
            mailtemplate.pager = pager;

            mapperBus.Map<IMailTemplateMappable>(mailtemplate);

            pager.LoadPageList(mailtemplate.recordCount);

            return View("template_list", mailtemplate);
        }

        /// <summary>
        /// registration new template
        /// </summary>
        [ErsAdminProcessCompletion("mypage_atmail_template_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult template_regist(mailtemplate_regist mailtemplate_regist, EnumEck? eck = null)
        {
            //if (!ModelState.IsValid)
            //{
            //    return GetErrorView();
            //}

            ModelState.AddModelErrors(commandBus.Validate<IMailtemplateRegistCommand>(mailtemplate_regist), mailtemplate_regist);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(mailtemplate_regist);
               this.mapperBus.Map<IMailtemplateRegistMappable>(mailtemplate_regist);
            }

            mailtemplate_regist.SetOutputHidden(true);

            return View("template_regist", mailtemplate_regist);
        }

        /// <summary>
        /// registration template confirm page
        /// </summary>
        [ErsAdminProcessCompletion("mypage_atmail_template_ransu", mode = EnumHandlingMode.CHECK)]
        public ActionResult template_regist_confirm(mailtemplate_regist mailtemplate_regist, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailtemplateRegistCommand>(mailtemplate_regist), mailtemplate_regist);
            if (!ModelState.IsValid)
            {
                return this.template_regist(mailtemplate_regist, EnumEck.Error);
            }

            this.mapperBus.Map<IMailtemplateRegistMappable>(mailtemplate_regist);
            mailtemplate_regist.SetOutputHidden(true);
            return View("template_regist_confirm", mailtemplate_regist);
        }

        /// <summary>
        /// registration template complete page
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_template_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult template_regist_complete(mailtemplate_regist mailtemplate_regist)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailtemplateRegistCommand>(mailtemplate_regist), mailtemplate_regist);
            if (!ModelState.IsValid)
            {
                return this.template_regist(mailtemplate_regist, EnumEck.Error);
            }

            mailtemplate_regist.IsCompletionPage = true;
            this.mapperBus.Map<IMailtemplateRegistMappable>(mailtemplate_regist);
            commandBus.Submit((IMailtemplateRegistCommand)mailtemplate_regist, EnumCommandTransaction.BeginTransaction);

            return View("template_regist_complete", mailtemplate_regist);
        }

        /// <summary>
        /// Modify Teamplate
        /// </summary>
        [ErsAdminProcessCompletion("mypage_atmail_template_modify_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult template_update(mailtemplate_modify mailtemplate_modify, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailtemplateModifyCommand>(mailtemplate_modify), mailtemplate_modify);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(mailtemplate_modify);
                this.mapperBus.Map<IMailtemplateModifyMappable>(mailtemplate_modify);
            }

            mailtemplate_modify.SetOutputHidden(true);

            return View("template_update", mailtemplate_modify);
        }

        /// <summary>
        /// modify template confirm page
        /// </summary>
        [ErsAdminProcessCompletion("mypage_atmail_template_modify_ransu", mode = EnumHandlingMode.CHECK)]
        public ActionResult template_update_confirm(mailtemplate_modify mailtemplate_modify, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailtemplateModifyCommand>(mailtemplate_modify), mailtemplate_modify);
            if (!ModelState.IsValid)
            {
                return this.template_update(mailtemplate_modify, EnumEck.Error);
            }

            mailtemplate_modify.IsConfirmationPage = true;
            this.mapperBus.Map<IMailtemplateModifyMappable>(mailtemplate_modify);
            mailtemplate_modify.SetOutputHidden(true);
            return View("template_update_confirm", mailtemplate_modify);
        }

        /// <summary>
        /// modify template complete page
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_template_modify_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult template_update_complete(mailtemplate_modify mailtemplate_modify)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMailtemplateModifyCommand>(mailtemplate_modify), mailtemplate_modify);
            if (!ModelState.IsValid)
            {
                return this.template_update(mailtemplate_modify, EnumEck.Error);
            }

            mailtemplate_modify.IsModifyCompletionPage = true;
            this.mapperBus.Map<IMailtemplateModifyMappable>(mailtemplate_modify);
            commandBus.Submit((IMailtemplateModifyCommand)mailtemplate_modify, EnumCommandTransaction.BeginTransaction);

            return View("template_update_complete", mailtemplate_modify);
        }

        /// <summary>
        /// deleting template
        /// </summary>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_atmail_template_modify_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult template_delete(mailtemplate_modify mailtemplate_modify)
        {
            if (!ModelState.IsValid)
            {
                return this.template_update(mailtemplate_modify, EnumEck.Error);
            }

            mailtemplate_modify.IsDeleteCompletionPage = true;
            commandBus.Submit((IMailtemplateDeleteCommand)mailtemplate_modify, EnumCommandTransaction.BeginTransaction);

            return View("template_delete", mailtemplate_modify);
        }
        #endregion
    }
}
