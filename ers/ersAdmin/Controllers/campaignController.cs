﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using ersAdmin.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Campaign.Mappables;

namespace ersAdmin.Controllers
{
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class campaignController
        : ErsControllerSecureAdmin
    {
        /// <summary>
        /// トップ
        /// </summary>
        /// <returns></returns>
        public ActionResult index()
        {
            return View("index");
        }


        /// <summary>
        /// ターゲット新規登録
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("mypage_campaign_target_regist_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult target_regist(target_regist targetRegist, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<ITargetRegistCommand>(targetRegist), targetRegist);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(targetRegist);               
            }

            return View("target_modify", targetRegist);
        }

        /// <summary>
        /// ターゲット新規登録完了画面
        /// </summary>
        /// <param name="itemRegist"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_campaign_target_regist_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult target_regist_complete(target_regist targetRegist)
        {
            ModelState.AddModelErrors(commandBus.Validate<ITargetRegistCommand>(targetRegist), targetRegist);
            if (!ModelState.IsValid)
            {
                return target_regist(targetRegist, EnumEck.Error);
            }

            commandBus.Submit<ITargetRegistCommand>(targetRegist, EnumCommandTransaction.BeginTransaction);

            return View("target_complete", targetRegist);

        }

        /// <summary>
        /// ターゲット修正
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("mypage_campaign_target_modify_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult target_modify(target_modify targetModify, Target_search target_List, EnumEck? eck = null)
        {
            targetModify.IsErrorBack = this.IsErrorBack(eck);
            if (!this.IsErrorBack(eck))
            {
                ModelState.AddModelErrors(commandBus.Validate<ITargetModifyCommand>(targetModify), targetModify);

                ClearModelState(targetModify);
            }
            mapperBus.Map<ITargetModifyMappable>(targetModify);

            targetModify.SetOutputHidden(true);

            target_List.SetOutputHidden(true);
            this.AddModelToView(target_List);

            return View("target_modify", targetModify);
        }

        /// <summary>
        /// ターゲット修正完了画面
        /// </summary>
        /// <param name="itemRegist"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_campaign_target_modify_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult target_modify_complete(target_modify targetModify, Target_search target_List)
        {

            ModelState.AddModelErrors(commandBus.Validate<ITargetModifyCommand>(targetModify), targetModify);
            if (!ModelState.IsValid)
            {
                return target_modify(targetModify, target_List, EnumEck.Error);
            }
           
            commandBus.Submit<ITargetModifyCommand>(targetModify, EnumCommandTransaction.BeginTransaction);

            target_List.SetOutputHidden(true);
            this.AddModelToView(target_List);

            return View("target_complete", targetModify);

        }

        /// <summary>
        /// ターゲット削除完了画面
        /// </summary>
        /// <param name="itemRegist"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_campaign_target_modify_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult target_delete_complete(target_modify targetModify, Target_search target_List)
        {
            ModelState.AddModelErrors(commandBus.Validate<ITargetDeleteCommand>(targetModify), targetModify);
            if (!ModelState.IsValid)
            {
                return target_modify(targetModify, target_List, EnumEck.Error);
            }

            commandBus.Submit<ITargetDeleteCommand>(targetModify, EnumCommandTransaction.BeginTransaction);

            target_List.SetOutputHidden(true);
            this.AddModelToView(target_List);

            return View("target_delete_complete", targetModify);
        }



        /// <summary>
        /// ターゲット検索
        /// </summary>
        /// <returns></returns>
        public ActionResult target_search(Target_search target_search, EnumEck? eck)
        {
            if (!this.IsErrorBack(eck))
            {
                this.ClearModelState(target_search);
            }

            return View("target_search", target_search);
        }

        /// <summary>
        /// ターゲット検索リスト
        /// </summary>
        /// <param name="target_List"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        public ActionResult target_list(Target_search target_List)
        {
            if (!ModelState.IsValid)
            {
                return this.target_search(target_List, EnumEck.Error);
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", target_List.pageCnt, target_List.maxItemCount);

            target_List.pager = pager;
            mapperBus.Map<ITargetSearchMappable>(target_List);

            if (this.HasInformation)
            {
                return this.target_search(target_List, EnumEck.Error);
            }

            target_List.pager.LoadPageList(target_List.recordCount);

            //FormタグにHiddenを自動入力
            target_List.SetOutputHidden(true);

            return View("target_list", target_List);

        }

        /// <summary>
        /// ターゲット検索リスト
        /// </summary>
        /// <param name="target_List"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        public ActionResult target_csv_download(target_csv_download target_csv_download)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ITargetCsvDownloadCommand>(target_csv_download), target_csv_download);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<ITargetCsvDownloadMappable>(target_csv_download);

            return this.CsvFile(target_csv_download.csvCreater.filePath);
        }

        ///*********************************///
        ///キャンペーン
        ///*********************************///
        /// <summary>
        /// キャンペーン新規登録
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("mypage_campaign_template_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult campaign_regist(campaign_regist campaignRegist, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICampaignRegistCommand>(campaignRegist), campaignRegist);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(campaignRegist);
                this.mapperBus.Map<ICampaignRegistMappable>(campaignRegist);
            }

            return View("campaign_modify", campaignRegist);
        }

        /// <summary>
        /// キャンペーン新規登録完了画面
        /// </summary>
        /// <param name="campaignRegist"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_campaign_template_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult campaign_regist_complete(campaign_regist campaignRegist)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICampaignRegistCommand>(campaignRegist), campaignRegist);
            if (!ModelState.IsValid)
            {
                return campaign_regist(campaignRegist, EnumEck.Error);
            }

            //campaignRegist.InsertCampaign();
            commandBus.Submit((ICampaignRegistCommand)campaignRegist, EnumCommandTransaction.BeginTransaction);


            return View("campaign_complete", campaignRegist);

        }

        /// <summary>
        /// キャンペーン検索
        /// </summary>
        /// <returns></returns>
        public ActionResult campaign_search(Campaign_search campaign_search, EnumEck? eck)
        {
            if (!this.IsErrorBack(eck))
            {
                this.ClearModelState(campaign_search);
            }

            return View("campaign_search", campaign_search);
        }

        /// <summary>
        /// キャンペーン検索リスト
        /// </summary>
        /// <param name="campaign_List"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        public ActionResult campaign_list(Campaign_search campaign_List)
        {

            if (!ModelState.IsValid)
            {
                return this.campaign_search(campaign_List, EnumEck.Error);
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", campaign_List.pageCnt, campaign_List.maxItemCount);
            campaign_List.pager = pager;

            mapperBus.Map<ICampaignSearchMappable>(campaign_List);

            if (this.HasInformation)
            {
                return this.campaign_search(campaign_List, EnumEck.Error);
            }

            pager.LoadPageList(campaign_List.recordCount);

            //FormタグにHiddenを自動入力
            campaign_List.SetOutputHidden(true);

            return View("campaign_list", campaign_List);

        }

        /// <summary>
        /// キャンペーン修正
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("mypage_campaign_template_modify_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult campaign_modify(campaign_modify campaignModify, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICampaignModifyCommand>(campaignModify), campaignModify);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(campaignModify);
                this.mapperBus.Map<ICampaignModifyMappable>(campaignModify);
            }

            return View("campaign_modify", campaignModify);
        }

        /// <summary>
        /// キャンペーン修正完了画面
        /// </summary>
        /// <param name="itemRegist"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_campaign_template_modify_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult campaign_modify_complete(campaign_modify campaignModify)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICampaignModifyCommand>(campaignModify), campaignModify);
            if (!ModelState.IsValid)
            {
                return campaign_modify(campaignModify, EnumEck.Error);
            }

            //campaignModify.UpdateCampaign(campaignModify);

            commandBus.Submit((ICampaignModifyCommand)campaignModify, EnumCommandTransaction.BeginTransaction);

            return View("campaign_complete", campaignModify);

        }

        /// <summary>
        /// キャンペーン削除完了画面
        /// </summary>
        /// <param name="itemRegist"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsAdminProcessCompletion("mypage_campaign_template_modify_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult campaign_delete_complete(campaign_modify campaignModify)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICampaignDeleteCommand>(campaignModify), campaignModify);
            if (!ModelState.IsValid)
            {
                return campaign_modify(campaignModify, EnumEck.Error);
            }

            commandBus.Submit<ICampaignDeleteCommand>(campaignModify, EnumCommandTransaction.BeginTransaction);


            return View("campaign_delete_complete", campaignModify);
        }

        /// <summary>
        /// 商品番号検索ページ
        /// </summary>
        /// <returns></returns>
        public ActionResult common_scode_search(CommonScodeList commonItemList, EnumEck? eck = null)
        {

            if (!this.IsErrorBack(eck))
            {
                this.ClearModelState(commonItemList);
            }

            commonItemList.SetOutputHidden(true);

            return View("common_scode_search", commonItemList);
        }

        /// <summary>
        /// 商品番号選択ページ
        /// </summary>
        /// <returns></returns>
        public ActionResult common_scode_list(CommonScodeList commonItemList)
        {

            if (!ModelState.IsValid)
            {
                return this.common_scode_search(commonItemList, EnumEck.Error);
            }

            try
            {
                var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", commonItemList.pageCnt, commonItemList.maxItemCount);
                commonItemList.pager = pager;

                mapperBus.Map<ICommonScodeListMappable>(commonItemList);

                if (this.HasInformation)
                {
                    return this.common_scode_search(commonItemList, EnumEck.Error);
                }

                //FormタグにHiddenを自動入力
                commonItemList.SetOutputHidden(true);

                pager.LoadPageList(commonItemList.recordCount);

                return View("common_scode_search", commonItemList);
            }
            catch (Exception ex)
            {
                return GetErrorView("common_scode_search.asp", commonItemList,ex.Message);
            }
        }
    }
}