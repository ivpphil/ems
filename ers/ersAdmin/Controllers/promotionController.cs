﻿using System.Web.Mvc;
using ersAdmin.Domain.Promotion.Commands;
using ersAdmin.Domain.Promotion.Mappables;
using ersAdmin.Models;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ersAdmin.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class promotionController : ErsControllerSecureAdmin
    {
        //
        // GET: /promotion/

        #region Coupon
        /// <summary>
        /// Coupon Regist
        /// </summary>
        /// <param name="Coupon"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        public ActionResult coupon_regist(coupon_regist Coupon, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICouponRegistCommand>(Coupon), Coupon);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(Coupon);
            }
            return View("coupon_regist", Coupon);
        }

        /// <summary>
        /// Register Coupon Complete
        /// </summary>
        /// <param name="Coupon"></param>
        /// <returns></returns>
        public ActionResult coupon_regist_complete(coupon_regist Coupon)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICouponRegistCommand>(Coupon), Coupon);
            if (!ModelState.IsValid)
            {
                return this.coupon_regist(Coupon, EnumEck.Error);
            }

            Coupon.IsRegistCompletion = true;
            commandBus.Submit<ICouponRegistCommand>(Coupon, EnumCommandTransaction.BeginTransaction);

            return View("coupon_regist_complete", Coupon);
        }

        /// <summary>
        /// Search Coupon
        /// </summary>
        /// <param name="Coupon"></param>
        /// <returns></returns>
        public ActionResult coupon_search(coupon_search Coupon, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(Coupon);
            }
            return View("coupon_search", Coupon);
        }

        public ActionResult coupon_list(coupon_search Coupon, EnumEck? eck = null)
        {
            if (!ModelState.IsValid)
            {
                return this.coupon_search(Coupon, EnumEck.Error);
            }

            Coupon.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", Coupon.pageCnt, Coupon.maxItemCount);

            Coupon.IsSearchList = true;

            mapperBus.Map<ICouponSearchMappable>(Coupon);

            Coupon.pager.LoadPageList(Coupon.recordCount);

            if (this.HasInformation)
            {
                return this.coupon_search(Coupon, EnumEck.Error);
            }

            Coupon.SetOutputHidden("searchGroup", true);
            Coupon.SetOutputHidden("active", true);
            Coupon.SetOutputHidden("coupon_type", true);

            return View("coupon_list", Coupon);
        }

        /// <summary>
        /// Modify Coupon
        /// </summary>
        /// <param name="Coupon"></param>
        /// <param name="eck"></param>
        /// <returns></returns>
        public ActionResult coupon_modify(coupon_modify Coupon, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICouponModifyCommand>(Coupon), Coupon);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(Coupon);
            }
            
            mapperBus.Map<ICouponModifyMappable>(Coupon);

            Coupon.SetOutputHidden("id", true);
            Coupon.SetOutputHidden("intime", true);
            return View("coupon_modify", Coupon);
        }

        /// <summary>
        /// Delete Coupon
        /// </summary>
        /// <param name="Coupon"></param>
        /// <returns></returns>
        public ActionResult coupon_modify_complete(coupon_modify Coupon)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICouponModifyCommand>(Coupon), Coupon);
            if (!ModelState.IsValid)
            {
                return this.coupon_modify(Coupon, EnumEck.Error);
            }

            commandBus.Submit<ICouponModifyCommand>(Coupon, EnumCommandTransaction.BeginTransaction);

            return View("coupon_regist_complete", Coupon);
        }

        public ActionResult coupon_modify_delete(coupon_modify Coupon)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICouponDeleteCommand>(Coupon), Coupon);
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            commandBus.Submit<ICouponDeleteCommand>(Coupon, EnumCommandTransaction.BeginTransaction);

            return View("coupon_regist_complete", Coupon);
        }


        /// <summary>
        /// CSVダウンロード（全件）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_csv_all(coupon_search coupon_search)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<ICouponListCSVMappable>(coupon_search);

            return this.CsvFile(coupon_search.csvCreater.filePath);
        }

        /// <summary>
        /// CSVダウンロード（ページ）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_csv_page(coupon_search coupon_search)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            coupon_search.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(coupon_search.pageCnt, coupon_search.maxItemCount);

            mapperBus.Map<ICouponListCSVMappable>(coupon_search);

            return this.CsvFile(coupon_search.csvCreater.filePath);
        }

        /// <summary>
        /// coupon registration
        /// </summary>
        public ActionResult coupon_csv()
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var coupon_csv = new Coupon_csv();

            return View("coupon_csv", coupon_csv);
        }

        /// <summary>
        /// coupon registration confirm
        /// </summary>
        public ActionResult coupon_csv_confirm(Coupon_csv coupon_csv)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<ICouponCSVCommand>(coupon_csv), coupon_csv);
            if (!ModelState.IsValid)
            {
                //エラーは確認画面に表示する。 // display error to this confirmation page.
            }

            this.mapperBus.Map<ICouponCSVMappable>(coupon_csv);

            //モデル情報の引き渡し
            coupon_csv.SetOutputHidden(true);

            //テンプレート表示
            return View("coupon_csv_confirm", coupon_csv);
        }

        /// <summary>
        /// coupon registration complete
        /// </summary>
        public ActionResult coupon_csv_complete(Coupon_csv coupon_csv)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<ICouponCSVCommand>(coupon_csv), coupon_csv);
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            this.mapperBus.Map<ICouponCSVMappable>(coupon_csv);
            this.commandBus.Submit<ICouponCSVCommand>(coupon_csv, EnumCommandTransaction.WithoutBeginTransaction);

            //モデル情報の引き渡し
            coupon_csv.SetOutputHidden(true);

            //テンプレート表示
            return View("coupon_csv_complete", coupon_csv);
        }

        #endregion
    }
}
