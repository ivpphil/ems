﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ersAdmin.Domain.Regular.Commands;
using ersAdmin.Domain.Regular.Mappables;
using ersAdmin.Models;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.util;
 

namespace ersAdmin.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class RegularController
        : ErsControllerSecureAdmin
    {
        /// <summary>
        /// トップ
        /// </summary>
        /// <returns></returns>
        public ActionResult index()
        {
            return View("index");
        }

        /// <summary>
        /// 伝票検索
        /// </summary>
        /// <returns></returns>
        public ActionResult bill_search(Bill_search bill_search, EnumEck? eck)
        {
            if (!this.IsErrorBack(eck))
            {
                bill_search.IsBillSearchPage = true;
                mapperBus.Map<IBillSearchMappable>(bill_search);
                this.ClearModelState(bill_search);
            }

            return View("bill_search", bill_search);
        }

        /// <summary>
        /// 伝票リスト
        /// </summary>
        /// <returns></returns>
        public ActionResult bill_list(Bill_search bill_search)
        {
            if (!ModelState.IsValid)
            {
                return this.bill_search(bill_search, EnumEck.Error);
            }

            //Pager設定
            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", bill_search.pageCnt, bill_search.maxItemCount);

            bill_search.pager = pager;

            mapperBus.Map<IBillSearchMappable>(bill_search);

            if (this.HasInformation)
            {
                return this.bill_search(bill_search, EnumEck.Error);
            }

            bill_search.SetOutputHidden(true);

            pager.LoadPageList(bill_search.recordCount);

            return View("bill_list", bill_search);
        }

        /// <summary>
        /// 伝票詳細
        /// </summary>
        /// <returns></returns>
        public ActionResult bill_detail(BillModify bill_modify, Bill_search bill_search, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IBillModifyCommand>(bill_modify), bill_modify);
            if (!this.IsErrorBack(eck) || !bill_modify.IsValidField("d_no"))
            {
                if (!this.ModelState.IsValid)
                {
                    return this.GetErrorView();
                }

                this.ClearModelState(bill_modify);
                bill_modify.IsLoadDefaultValue = true;
            }

            mapperBus.Map<IBillModifyMappable>(bill_modify);

            bill_modify.SetOutputHidden("input_form", true);

            if (!bill_modify.payment_changeable)
            {
                bill_modify.SetOutputHidden("payment_not_changable", true);
            }

            //検索条件は常に保持
            bill_search.SetOutputHidden(true);
            this.AddModelToView(bill_search);

            return View("bill_detail", bill_modify);
        }

        /// <summary>
        /// 伝票詳細
        /// </summary>
        /// <returns></returns>
        public ActionResult bill_update(BillModify bill_modify, Bill_search bill_search)
        {
            ModelState.AddModelErrors(commandBus.Validate<IBillModifyCommand>(bill_modify), bill_modify);
            if (!this.ModelState.IsValid)
            {
                bool goBack = true;
                if (bill_modify.bill_cancel && bill_modify.IsValidField("d_no"))
                {
                    this.ClearModelState(bill_modify);
                    goBack = false;
                }

                if (goBack)
                {
                    return this.bill_detail(bill_modify, bill_search, EnumEck.Error);
                }
            }

            commandBus.Submit<IBillModifyCommand>(bill_modify, EnumCommandTransaction.WithoutBeginTransaction);

            bill_modify.SetOutputHidden("input_form", true);

            //検索条件は常に保持
            bill_search.SetOutputHidden(true);
            this.AddModelToView(bill_search);

            return View("bill_update", bill_modify);
        }

        /// <summary>
        /// CSVダウンロード（全件）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_csv_all(Bill_search bill_search)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //bill_search.CreateCsvFile();
            mapperBus.Map<IBillSearchCSVMappable>(bill_search);
            return this.CsvFile(bill_search.csvCreater.filePath);
        }

        /// <summary>
        /// CSVダウンロード（ページ）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_csv_page(Bill_search bill_search)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(bill_search.pageCnt, bill_search.maxItemCount);
            bill_search.pager = pager;

            //bill_search.CreateCsvFile(pager);
            mapperBus.Map<IBillSearchCSVMappable>(bill_search);
            return this.CsvFile(bill_search.csvCreater.filePath);
        }

        /// <summary>
        /// 出庫許可リスト
        /// </summary>
        /// <returns></returns>
        public ActionResult shipment_list(Shipment_list shipment_list)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }
            try
            {
                //Pager設定
                var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", shipment_list.pageCnt, shipment_list.maxItemCount);
                shipment_list.pager = pager;

                mapperBus.Map<IShipmentListMappable>(shipment_list);
                shipment_list.SetOutputHidden(true);

                shipment_list.pager.LoadPageList(shipment_list.recordCount);

                return View("shipment_list", shipment_list);
            }
            catch (Exception ex)
            {
                return GetErrorView("bill_list.asp", shipment_list, ex.Message);
            }
        }

        /// <summary>
        /// 出庫許可リストダウンロード（全件）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_shipment_csv_all(Shipment_list shipment_list)
        {

            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<IShipmentListCSVMappable>(shipment_list);
            commandBus.Submit<IShipmentListCSVCommand>(shipment_list, EnumCommandTransaction.BeginTransaction);
            return this.CsvFile(shipment_list.csvCreater.filePath);
        }

        /// <summary>
        /// 出庫許可リストダウンロード（ページ）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_shipment_csv_page(Shipment_list shipment_list)
        {

            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(shipment_list.pageCnt, shipment_list.maxItemCount);
            shipment_list.pager = pager;

            mapperBus.Map<IShipmentListCSVMappable>(shipment_list);
            commandBus.Submit<IShipmentListCSVCommand>(shipment_list, EnumCommandTransaction.BeginTransaction);
            
            return this.CsvFile(shipment_list.csvCreater.filePath);
        }


        /// <summary>
        /// 銀行振込入金登録
        /// </summary>
        /// <param name="regist_income"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("regist_income_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult regist_income(Regist_income regist_income, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IRegistIncomeCommand>(regist_income), regist_income);
            if (!this.IsErrorBack(eck))
            {
                //Pager設定
                var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", regist_income.pageCnt, regist_income.maxItemCount);
                regist_income.pager = pager;

                regist_income.isErrorBack = false;
                mapperBus.Map<IRegistIncomeMappable>(regist_income);

                pager.LoadPageList(regist_income.recordCount);
            }
            else
            {               
                regist_income.isErrorBack = true;
                mapperBus.Map<IRegistIncomeMappable>(regist_income);
            }

            return View("regist_income", regist_income);

        }


        /// <summary>
        /// 銀行振込入金登録 完了画面
        /// </summary>
        /// <param name="regist_income"></param>
        /// <returns></returns>
        [ErsAdminProcessCompletion("regist_income_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult regist_income_complete(Regist_income regist_income)
        {
            ModelState.AddModelErrors(commandBus.Validate<IRegistIncomeCommand>(regist_income), regist_income);
            if (!ModelState.IsValid)
            {
                return this.regist_income(regist_income, EnumEck.Error);
            }
     
            commandBus.Submit((IRegistIncomeCommand)regist_income, EnumCommandTransaction.BeginTransaction);

            return View("regist_income_complete", regist_income);
        }


        /// <summary>
        /// 販売可能数一括入れ替え
        /// </summary>
        [ErsAdminProcessCompletion("stock_update_all_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult stock_update_all()
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }
            return View("stock_update_all");
        }

        /// <summary>
        /// 販売可能数一括入れ替え 確認画面
        /// </summary>
        [ErsAdminProcessCompletion("stock_update_all_ransu", mode = EnumHandlingMode.CHECK)]
        public ActionResult stock_update_all_confirm(Stock_update_all stock_update_all)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStockUpdateAllCSVCommand>(stock_update_all), stock_update_all);
            if (!ModelState.IsValid)
            {
                //エラーは確認画面に表示する。
                //return this.stock_update_all(stock_update_all);
            }

            //モデル情報の引き渡し
            stock_update_all.SetOutputHidden(true);


            //テンプレート表示
            return View("stock_update_all_confirm", stock_update_all);
        }

        /// <summary>
        /// 販売可能数一括入れ替え 完了画面
        /// </summary>
        [ErsAdminProcessCompletion("stock_update_all_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult stock_update_all_complete(Stock_update_all stock_update_all)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStockUpdateAllCSVCommand>(stock_update_all), stock_update_all);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //モデル情報の引き渡し
            stock_update_all.SetOutputHidden(true);

            //update stock
            commandBus.Submit((IStockUpdateAllCSVCommand)stock_update_all, EnumCommandTransaction.BeginTransaction);

            //テンプレート表示
            return View("stock_update_all_complete", stock_update_all);
        }

        /// <summary>
        /// 販売可能数入れ替え
        /// </summary>
        [ErsAdminProcessCompletion("stock_update_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult stock_update()
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("stock_update");
        }

        /// <summary>
        /// 販売可能数入れ替え 確認画面
        /// </summary>
        [ErsAdminProcessCompletion("stock_update_ransu", mode = EnumHandlingMode.CHECK)]
        public ActionResult stock_update_confirm(Stock_update stock_update)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStockUpdateCSVCommand>(stock_update), stock_update);
            if (!ModelState.IsValid)
            {
                //エラーは確認画面に表示する。
                //return this.stock_update();
            }

            //モデル情報の引き渡し
            stock_update.SetOutputHidden(true);

            //テンプレート表示
            return View("stock_update_confirm", stock_update);

        }

        /// <summary>
        /// 販売可能数入れ替え 完了画面
        /// </summary>
        [ErsAdminProcessCompletion("stock_update_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult stock_update_complete(Stock_update stock_update)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStockUpdateCSVCommand>(stock_update), stock_update);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //モデル情報の引き渡し
            stock_update.SetOutputHidden(true);

            commandBus.Submit((IStockUpdateCSVCommand)stock_update, EnumCommandTransaction.BeginTransaction);

            //テンプレート表示
            return View("stock_update_complete", stock_update);
        }

        /// <summary>
        /// 代金引換入金データCSVアップロード
        /// </summary>
        [ErsAdminProcessCompletion("delivery_income_csv_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult delivery_income_csv()
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("delivery_income_csv");
        }

        /// <summary>
        /// 代金引換入金データCSVアップロード 確認画面
        /// </summary>
        [ErsAdminProcessCompletion("delivery_income_csv_ransu", mode = EnumHandlingMode.CHECK)]
        public ActionResult delivery_income_csv_confirm(Delivery_income_csv delivery_income_csv)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDeliveryIncomeCsvCommand>(delivery_income_csv), delivery_income_csv);
            if (!ModelState.IsValid)
            {
                //エラーは確認画面に表示する。
                //return this.delivery_income_csv();
            }

            //モデル情報の引き渡し
            delivery_income_csv.SetOutputHidden(true);

            //テンプレート表示
            return View("delivery_income_csv_confirm", delivery_income_csv);
        }

        /// <summary>
        /// 代金引換入金データCSVアップロード 完了画面
        /// </summary>
        [ErsAdminProcessCompletion("delivery_income_csv_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult delivery_income_csv_complete(Delivery_income_csv delivery_income_csv)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDeliveryIncomeCsvCommand>(delivery_income_csv), delivery_income_csv);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //モデル情報の引き渡し
            delivery_income_csv.SetOutputHidden(true);

            commandBus.Submit((IDeliveryIncomeCsvCommand)delivery_income_csv, EnumCommandTransaction.BeginTransaction);

            //テンプレート表示
            return View("delivery_income_csv_complete", delivery_income_csv);
        }

        /// <summary>
        /// 出荷戻りデータアップロード
        /// </summary>
        public ActionResult bill_csv_upload()
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("bill_csv_upload");
        }

        /// <summary>
        /// 出荷戻りデータアップロード 確認画面
        /// </summary>
        public ActionResult bill_csv_upload_confirm(Shipping_csv_upload bill_csv_upload)
        {
            ModelState.AddModelErrors(commandBus.Validate<IShippingUploadCSVCommand>(bill_csv_upload), bill_csv_upload);
            if (!ModelState.IsValid)
            {
                //エラーは確認画面に表示する。
                //return this.bill_csv_upload();
            }

            //モデル情報の引き渡し
            bill_csv_upload.SetOutputHidden(true);

            //テンプレート表示
            return View("bill_csv_upload_confirm", bill_csv_upload);
        }

        /// <summary>
        /// 出荷戻りデータアップロード 完了画面
        /// </summary>
        public ActionResult bill_csv_upload_complete(Shipping_csv_upload bill_csv_upload)
        {
            ModelState.AddModelErrors(commandBus.Validate<IShippingUploadCSVCommand>(bill_csv_upload), bill_csv_upload);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //モデル情報の引き渡し
            bill_csv_upload.SetOutputHidden(true);

            this.commandBus.Submit<IShippingUploadCSVCommand>(bill_csv_upload, EnumCommandTransaction.WithoutBeginTransaction);

            //テンプレート表示
            return View("bill_csv_upload_complete", bill_csv_upload);
        }

        /// <summary>
        /// 個別メール配信
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public ActionResult bill_mail(Bill_search bill_search, Bill_mail bill_mail, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IBillMailCommand>(bill_mail), bill_mail);
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    return GetErrorView();
                }
            }
            bill_mail.IsLoadDefault = !this.IsErrorBack(eck);

            mapperBus.Map<IBillMailMappable>(bill_mail);


            bill_mail.SetOutputHidden(true);

            //検索条件は常に保持
            bill_search.SetOutputHidden(true);
            this.AddModelToView(bill_search);

            return View("bill_mail", bill_mail);
        }

        /// <summary>
        /// 個別メール配信
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public ActionResult bill_mail_complete(Bill_search bill_search, Bill_mail bill_mail)
        {
            bill_mail.IsSendMail = true;
            ModelState.AddModelErrors(commandBus.Validate<IBillMailCommand>(bill_mail), bill_mail);
            if (!ModelState.IsValid)
            {
                return this.bill_mail(bill_search, bill_mail, EnumEck.Error);
            }

            this.mapperBus.Map<IBillMailMappable>(bill_mail);

            if (!bill_mail.IsMallOrder)
            {
                var sendMail = ErsFactory.ersMailFactory.getErsSendMailAdminIndividual();
                sendMail.SendMail(bill_mail, bill_mail.from_email, bill_mail.email, bill_mail.send_to_admin, bill_mail.mail_title, bill_mail.mail_body, EnumMformat.PC);
            }
            else
            {
                var stgy = ErsMallFactory.ersMallMailFactory.GetSendIndividualMailStgy();
                stgy.SendMail(bill_mail, bill_mail.from_email, bill_mail.email, bill_mail.send_to_admin, bill_mail.mail_title, bill_mail.mail_body, EnumMformat.PC, bill_mail.site_id);
            }

            //検索条件は常に保持
            bill_search.SetOutputHidden(true);
            this.AddModelToView(bill_search);

            bill_mail.SetOutputHidden(true);

            return View("bill_mail_complete", bill_mail);
        }

    }
}
