﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using ersAdmin.Models.warehouse;
using ersAdmin.Domain.Warehouse.Commands;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models;

namespace ersAdmin.Controllers
{
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class warehouseController
        : ErsControllerSecureAdmin
    {
        public ActionResult index()
        {
            return View("index");
        }

        public ActionResult whmanage_index()
        {
            return View("whmanage_index");
        }

        #region Order Search and List

        public ActionResult order_search(OrderList orderList, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IOrderListCommand>(orderList), orderList);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(orderList);
                }
            }

            return View("order_search", orderList);
        }

        [ErsAdminProcessCompletion("warehouse_order_search", mode = EnumHandlingMode.RESET)]
        public ActionResult order_list(OrderList orderList, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IOrderListCommand>(orderList), orderList);
            if (!IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    return this.order_search(orderList, EnumEck.Error);
                }

                orderList.pageCurrentCnt = orderList.pageCnt;
            }

            if ((eck == EnumEck.Error) && (orderList.pageCnt != 0)) orderList.pageCurrentCnt = orderList.pageCnt;

            //Pager設定            
            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", orderList.pageCurrentCnt, orderList.maxItemCount);
            orderList.pager = pager;

            if (!IsErrorBack(eck)) this.mapperBus.Map<IOrderListMappable>(orderList);

            if ((eck == EnumEck.Error) && (orderList.pageCnt != 0)) this.mapperBus.Map<IOrderListMappable>(orderList);

            pager.LoadPageList(orderList.recordCount);

            if (!IsErrorBack(eck))
            {
                if (orderList.orderRegistRecords.Count() == 0)
                {
                    this.AddInformation(ErsResources.GetMessage("10200"));
                    return this.order_search(orderList, EnumEck.Error);
                }
            }

            orderList.SetOutputHidden("search", true);
            orderList.SetOutputHidden("input", true);

            return View("order_list", orderList);
        }

        [ErsAdminProcessCompletion("warehouse_order_search", mode = EnumHandlingMode.CHECK)]
        public ActionResult order_list_confirm(OrderList orderList)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IOrderListCommand>(orderList), orderList);
            this.ModelState.AddModelErrors(this.commandBus.Validate<IOrderRegistCommand>(orderList), orderList);
            if (!ModelState.IsValid)
            {
                return this.order_list(orderList, EnumEck.Error);
            }

            orderList.SetOutputHidden("search", true);
            orderList.SetOutputHidden("input", true);
            orderList.SetOutputHidden("confirm", true);

            return View("order_list_confirm", orderList);
        }

        [ErsAdminProcessCompletion("warehouse_order_search", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult order_list_complete(OrderList orderList)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IOrderListCommand>(orderList), orderList);
            this.ModelState.AddModelErrors(this.commandBus.Validate<IOrderRegistCommand>(orderList), orderList);
            if (!ModelState.IsValid)
            {
                return this.order_list(orderList, EnumEck.Error);
            }

            this.commandBus.Submit<IOrderRegistCommand>(orderList, EnumCommandTransaction.BeginTransaction);

            this.mapperBus.Map<IPastOrderListMappable>(orderList);

            orderList.SetOutputHidden("search", true);

            return View("order_list_complete", orderList);
        }

        public ActionResult order_list_pdf(OrderListPDF orderListPDF)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IOrderListPDFCommand>(orderListPDF), orderListPDF);
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            this.mapperBus.Map<IOrderListPDFMappable>(orderListPDF);

            //return View("order_list_pdf", orderListPDF);
            return PdfFile(orderListPDF.pdf);
        }

        #endregion


        #region Supplier Registration, Search and Modification
        // GET: SupplierRegist/
        /// <summary>
        /// Register new Supplier
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("warehouse_supplier_regist", mode = EnumHandlingMode.RESET)]
        public ActionResult supplier_regist(SupplierRegist supplierRegist, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ISupplierRegisterCommand>(supplierRegist), supplierRegist);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(supplierRegist);
            }

            return View("supplier_regist", supplierRegist);
        }

        /// <summary>
        /// Supplier 完了画面
        /// </summary>
        /// <param name="itemRegist"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsAdminProcessCompletion("warehouse_supplier_regist", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult supplier_regist_complete(SupplierRegist supplierRegist)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ISupplierRegisterCommand>(supplierRegist), supplierRegist);
            if (!ModelState.IsValid)
            {
                return supplier_regist(supplierRegist, EnumEck.Error);
            }

            commandBus.Submit<ISupplierRegisterCommand>(supplierRegist, EnumCommandTransaction.BeginTransaction);

            return View("supplier_regist_complete", supplierRegist);
        }

        /// <summary>
        /// Search for Supplier
        /// </summary>
        /// <returns></returns>
        public ActionResult supplier_search(SupplierList supplierList, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ISupplierListCommand>(supplierList), supplierList);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(supplierList);
                }
            }

            return View("supplier_search", supplierList);
        }

        /// <summary>
        /// Supplier Search Results
        /// </summary>
        /// <returns></returns>
        public ActionResult supplier_list(SupplierList supplierList, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ISupplierListCommand>(supplierList), supplierList);
            if (!IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    return this.supplier_search(supplierList, EnumEck.Error);
                }

                //Pager設定
                var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", supplierList.pageCnt, supplierList.maxItemCount);
                supplierList.pager = pager;

                this.mapperBus.Map<ISupplierListMappable>(supplierList);

                if (supplierList.list.Count() == 0)
                {
                    this.AddInformation(ErsResources.GetMessage("10200"));
                    return this.supplier_search(supplierList, EnumEck.Error);
                }

                pager.LoadPageList(supplierList.recordCount);
            }

            supplierList.SetOutputHidden(true);

            return View("supplier_list", supplierList);
        }

        /// <summary>
        /// Modify existing Supplier 
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("warehouse_supplier_modify", mode = EnumHandlingMode.RESET)]
        public ActionResult supplier_modify(SupplierList supplierList, SupplierModify supplierModify, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ISupplierListCommand>(supplierList), supplierList);
            this.ModelState.AddModelErrors(this.commandBus.Validate<ISupplierModifyCommand>(supplierModify), supplierModify);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(supplierModify);
                this.mapperBus.Map<ISupplierModifyMappable>(supplierModify);
            }
            supplierModify.SetOutputHidden(true);

             //add search values to model
            supplierList.SetOutputHidden(true);
            this.AddModelToView(supplierList);

            return View("supplier_modify", supplierModify);
        }

        /// <summary>
        /// Modify Supplier completion page
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ErsAdminProcessCompletion("warehouse_supplier_modify", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult supplier_modify_complete(SupplierList supplierList,SupplierModify supplierModify)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ISupplierListCommand>(supplierList), supplierList);
            this.ModelState.AddModelErrors(this.commandBus.Validate<ISupplierModifyCommand>(supplierModify), supplierModify);
            if (!ModelState.IsValid)
            {
                return this.supplier_modify(supplierList,supplierModify, EnumEck.Error);
            }
            commandBus.Submit<ISupplierModifyCommand>(supplierModify, EnumCommandTransaction.BeginTransaction);

            //add search values to model
            supplierList.SetOutputHidden(true);
            this.AddModelToView(supplierList);

            return View("supplier_modify_complete", supplierModify);
        }

        #endregion


        #region Past Order Search and Cancellation

        public ActionResult past_order_search(PastOrderList pastOrderList, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IPastOrderListCommand>(pastOrderList), pastOrderList);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(pastOrderList);
                }
            }

            return View("past_order_search", pastOrderList);
        }
        
        public ActionResult past_order_list(PastOrderList pastOrderList, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IPastOrderListCommand>(pastOrderList), pastOrderList);
            if (!IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    return this.past_order_search(pastOrderList, EnumEck.Error);
                }

                //Pager設定
                var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", pastOrderList.pageCnt, pastOrderList.maxItemCount);
                pastOrderList.pager = pager;

                this.mapperBus.Map<IPastOrderListMappable>(pastOrderList);

                if (pastOrderList.listPastOrder.Count() == 0)
                {
                    this.AddInformation(ErsResources.GetMessage("10200"));
                    return this.past_order_search(pastOrderList, EnumEck.Error);
                }

                pager.LoadPageList(pastOrderList.recordCount);
            }

            pastOrderList.SetOutputHidden(true);

            return View("past_order_list", pastOrderList);
        }

        /// <summary>
        /// CSVダウンロード（全件）
        /// </summary>
        /// <returns></returns>
        public ActionResult past_order_download_all(PastOrderList pastOrderList)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IPastOrderListCommand>(pastOrderList), pastOrderList);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<IPastOrderListCsvMappable>(pastOrderList);

            return this.CsvFile(pastOrderList.csvCreater.filePath);
        }

        /// <summary>
        /// CSVダウンロード（ページ）
        /// </summary>
        /// <returns></returns>
        public ActionResult past_order_download_page(PastOrderList pastOrderList)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IPastOrderListCommand>(pastOrderList), pastOrderList);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(pastOrderList.pageCnt, pastOrderList.maxItemCount);
            pastOrderList.pager = pager;

            mapperBus.Map<IPastOrderListCsvMappable>(pastOrderList);

            return this.CsvFile(pastOrderList.csvCreater.filePath);

        }

        public ActionResult past_order_pdf_all(PastOrderList pastOrderList)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IPastOrderListCommand>(pastOrderList), pastOrderList);
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            this.mapperBus.Map<IPastOrderListPDFMappable>(pastOrderList);

            //return View("order_list_pdf", pastOrderList);
            return PdfFile(pastOrderList.pdf);
        }

        public ActionResult past_order_pdf_page(PastOrderList pastOrderList)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IPastOrderListCommand>(pastOrderList), pastOrderList);
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(pastOrderList.pageCnt, pastOrderList.maxItemCount);
            pastOrderList.pager = pager;

            this.mapperBus.Map<IPastOrderListPDFMappable>(pastOrderList);

            //return View("order_list_pdf", pastOrderList);
            return PdfFile(pastOrderList.pdf);
        }

        [ErsAdminProcessCompletion("warehouse_past_order_detail", mode = EnumHandlingMode.RESET)]
        public ActionResult past_order_detail(PastOrderList pastOrderList, PastOrderDetail pastOrderDetail, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IPastOrderListCommand>(pastOrderList), pastOrderList);
            this.ModelState.AddModelErrors(this.commandBus.Validate<IPastOrderCommand>(pastOrderDetail), pastOrderDetail);
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    return this.past_order_detail(pastOrderList, pastOrderDetail, EnumEck.Error);
                }

                ClearModelState(pastOrderDetail);
                pastOrderDetail.IsInitialize = true;
            }
            this.mapperBus.Map<IPastOrderMappable>(pastOrderDetail);
            pastOrderDetail.SetOutputHidden(true);

            //add search values to model
            pastOrderList.SetOutputHidden(true);
            this.AddModelToView(pastOrderList);

           

            return View("past_order_detail", pastOrderDetail);
        }

        [HttpPost]
        [ErsAdminProcessCompletion("warehouse_past_order_detail", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult past_order_modify(PastOrderList pastOrderList, PastOrderDetail pastOrderDetail)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IPastOrderListCommand>(pastOrderList), pastOrderList);
            this.ModelState.AddModelErrors(this.commandBus.Validate<IPastOrderModifyCommand>(pastOrderDetail), pastOrderDetail);
            if (!ModelState.IsValid)
            {
                return this.past_order_detail(pastOrderList, pastOrderDetail, EnumEck.Error);
            }
            this.mapperBus.Map<IPastOrderMappable>(pastOrderDetail);
            this.commandBus.Submit<IPastOrderModifyCommand>(pastOrderDetail, EnumCommandTransaction.BeginTransaction);

            pastOrderDetail.SetOutputHidden(true);

            pastOrderList.SetOutputHidden(true);
            this.AddModelToView(pastOrderList);

            return View("past_order_modify", pastOrderDetail);
        }
        #endregion

        #region storage

        public ActionResult storage_csv_upload()
        {
            if (!ModelState.IsValid)
                return this.GetErrorView();

            return View("storage_csv_upload");
        }

        public ActionResult storage_csv_upload_confirm(storage_csv_upload storage)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStorageCsvUploadCommand>(storage), storage);

            if (!ModelState.IsValid)
            {
                //エラーは確認画面に表示する。 // display error to this confirmation page.
            }

            storage.SetOutputHidden(true);

            return View("storage_csv_upload_confirm", storage);
        }

        public ActionResult storage_csv_upload_complete(storage_csv_upload storage)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStorageCsvUploadCommand>(storage), storage);

            if (!ModelState.IsValid)
                return this.GetErrorView();

            commandBus.Submit<IStorageCsvUploadCommand>(storage, EnumCommandTransaction.WithoutBeginTransaction);
            return View("storage_csv_upload_complete", storage);
        }

        public ActionResult storage_download(storage_download model, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IStorageDownloadCommand>(model), model);
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                    this.ClearModelState(model);
            }

            this.mapperBus.Map<IStorageDownloadMappable>(model);
            return View("storage_download", model);
        }

        public ActionResult storage_download_complete(storage_download model)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStorageDownloadCommand>(model), model);
            if (!ModelState.IsValid)
                return this.storage_download(model, EnumEck.Error);

            model.CreateCsvFile = true;
            this.mapperBus.Map<IStorageDownloadMappable>(model);

            if (!model.hasRecord)
            {
                this.AddInformation(ErsResources.GetMessage("10200"));
                return this.storage_download(model, EnumEck.Error);
            }

            return this.CsvFile(model.csvCreater.filePath);
        }

        #endregion

        #region move

        public ActionResult move_csv()
        {
            if (!ModelState.IsValid)
                return this.GetErrorView();

            return View("move_csv");
        }

        public ActionResult move_csv_confirm(move_csv_upload model)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMoveCsvUploadCommand>(model), model);
            if (!ModelState.IsValid)
                return this.move_csv();

            model.SetOutputHidden(true);

            return View("move_csv_confirm", model);
        }

        public ActionResult move_csv_complete(move_csv_upload model)
        {
            ModelState.AddModelErrors(commandBus.Validate<IMoveCsvUploadCommand>(model), model);
            if (!ModelState.IsValid)
                return this.GetErrorView();

            model.SetOutputHidden(true);

            this.commandBus.Submit<IMoveCsvUploadCommand>(model, EnumCommandTransaction.BeginTransaction);

            return View("move_csv_complete", model);
        }

        public ActionResult move_history_search(move_history_list model, EnumEck? eck = null)
        {
           
            if (!this.IsErrorBack(eck))
            {
                model.IsSearchPage = true;
                mapperBus.Map<IMoveHistoryListMappable>(model);
                this.ClearModelState(model);
            }

            return View("move_history_search", model);
        }

        public ActionResult move_history_list(move_history_list model, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IMoveHistoryListCommand>(model), model);
            if (!IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    return this.move_history_search(model, EnumEck.Error);
                }

                //Pager設定
                var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", model.pageCnt, model.maxItemCount);
                model.pager = pager;

                this.mapperBus.Map<IMoveHistoryListMappable>(model);

                if (model.listMoveHistory.Count() == 0)
                {
                    this.AddInformation(ErsResources.GetMessage("10200"));
                    return this.move_history_search(model, EnumEck.Error);
                }

                pager.LoadPageList(model.recordCount);
            }

            model.SetOutputHidden(true);

            return View("move_history_list", model);
        }

        /// <summary>
        /// CSVダウンロード（全件）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_move_csv_all(move_history_list model)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<IMoveHistorySearchCSVMappable>(model);

            return this.CsvFile(model.csvCreater.filePath);
        }

        /// <summary>
        /// CSVダウンロード（ページ）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_move_csv_page(move_history_list model)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(model.pageCnt, model.maxItemCount);
            model.pager = pager;

            mapperBus.Map<IMoveHistorySearchCSVMappable>(model);

            return this.CsvFile(model.csvCreater.filePath);

        }

        #endregion

        #region stock

        public ActionResult stock_search(stock_list model, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                model.IsSearchPage = true;
                mapperBus.Map<IStockListMappable>(model);
                this.ClearModelState(model);
            }
            return View("stock_search", model);
        }

        public ActionResult stock_list(stock_list model)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IStockListCommand>(model), model);
            if (!ModelState.IsValid)
            {
                return this.stock_search(model, EnumEck.Error);
            }

            //Pager設定
            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", model.pageCnt, model.maxItemCount);
            model.pager = pager;

            this.mapperBus.Map<IStockListMappable>(model);

            if (model.list.Count() == 0)
            {
                this.AddInformation(ErsResources.GetMessage("10200"));
                return this.stock_search(model, EnumEck.Error);
            }

            pager.LoadPageList(model.recordCount);

            model.SetOutputHidden(true);

            return View("stock_list", model);
        }

        public ActionResult download_stock_csv_all(stock_list model)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<IStockListCSVMappable>(model);

            return this.CsvFile(model.csvCreater.filePath);
        }

        public ActionResult download_stock_csv_page(stock_list model)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(model.pageCnt, model.maxItemCount);
            model.pager = pager;

            mapperBus.Map<IStockListCSVMappable>(model);

            return this.CsvFile(model.csvCreater.filePath);
        }

        #endregion

        #region  stock_taking

        public ActionResult stock_taking_csv_upload()
        {
            if (!ModelState.IsValid)
                return this.GetErrorView();

            return View("stock_taking_csv_upload");
        }

        public ActionResult stock_taking_csv_upload_confirm(stock_taking_csv_upload model)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStockTakingCsvUploadCommand>(model), model);
            if (!ModelState.IsValid)
                return this.stock_taking_csv_upload();

            model.SetOutputHidden(true);

            return View("stock_taking_csv_upload_confirm", model);
        }

        public ActionResult stock_taking_csv_upload_complete(stock_taking_csv_upload model)
        {
            ModelState.AddModelErrors(commandBus.Validate<IStockTakingCsvUploadCommand>(model), model);
            if (!ModelState.IsValid)
                return this.GetErrorView();

            model.SetOutputHidden(true);

            this.commandBus.Submit<IStockTakingCsvUploadCommand>(model, EnumCommandTransaction.WithoutBeginTransaction);

            return View("stock_taking_csv_upload_complete", model);
        }

        #endregion

    }
}