﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using ersAdmin.Models;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Mappables;
using ersAdmin.Models.item;
using System.Web.Routing;

namespace ersAdmin.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class ItemController
        : ErsControllerSecureAdmin
    {
        public ActionResult index()
        {
            return View("index");
        }

        // GET: /ItemRegist/
        /// <summary>
        /// 初回表示
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("ItemController_item_regist", mode = EnumHandlingMode.RESET)]
        public ActionResult item_regist(ItemRegist itemRegist, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IItemRegistCommand>(itemRegist), itemRegist);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(itemRegist);

                itemRegist.active = EnumActive.Active;
                itemRegist.disp_list_flg = EnumDisp_list_flg.Visible;
            }

            return View("item_modify", itemRegist);
        }

        /// <summary>
        /// 完了画面
        /// </summary>
        /// <param name="itemRegist"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsAdminProcessCompletion("ItemController_item_regist", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult item_regist_complete(ItemRegist itemRegist)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IItemRegistCommand>(itemRegist), itemRegist);
            if (!ModelState.IsValid)
            {
                return item_regist(itemRegist, EnumEck.Error);
            }

            this.commandBus.Submit<IItemRegistCommand>(itemRegist, EnumCommandTransaction.BeginTransaction);

            var dictionary = new RouteValueDictionary();
            dictionary.Add("gcode", itemRegist.gcode);
            dictionary.Add("regist_complete", true);

            return this.RedirectToAction("item_modify", dictionary);
        }

        /// <summary>
        /// 商品修正初回表示
        /// </summary>
        /// <returns></returns>
        [ErsAdminProcessCompletion("ItemController_item_modify", mode = EnumHandlingMode.RESET)]
        public ActionResult item_modify(ItemModify itemModify,ItemList itemList, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IItemModifyCommand>(itemModify), itemModify);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(itemModify);
                this.mapperBus.Map<IItemModifyMappable>(itemModify);
                itemModify.IsInitialize = true;
            }

            itemModify.SetOutputHidden("input", true);
            //商品検索条件保持用
            itemList.SetOutputHidden(true);
            this.AddModelToView(itemList);

            return View("item_modify", itemModify);
        }

        /// <summary>
        /// 商品修正完了画面
        /// </summary>
        /// <param name="itemModify"></param>
        /// <returns></returns>
        [HttpPost]
        [ErsAdminProcessCompletion("ItemController_item_modify", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult item_modify_complete(ItemModify itemModify,ItemList itemList)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IItemModifyCommand>(itemModify), itemModify);
            if (!ModelState.IsValid)
            {
                return this.item_modify(itemModify,itemList, EnumEck.Error);
            }

            //g_master_t更新
            this.commandBus.Submit<IItemModifyCommand>(itemModify, EnumCommandTransaction.BeginTransaction);

            itemModify.SetOutputHidden("complete", true);
            //商品検索条件保持用
            itemList.SetOutputHidden(true);
            this.AddModelToView(itemList);

            return View("item_modify_complete", itemModify);
        }

        /// <summary>
        /// 商品検索初回表示
        /// </summary>
        /// <returns></returns>
        public ActionResult item_search(ItemList itemSearch, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                this.ClearModelState(itemSearch);
            }

            itemSearch.isInitilize = true;
            this.mapperBus.Map<IItemListMappable>(itemSearch);

            return View("item_search", itemSearch);
        }

        public ActionResult item_list(ItemList itemList)
        {
            if (!ModelState.IsValid)
            {
                return this.item_search(itemList, EnumEck.Error);
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", itemList.pageCnt, itemList.maxItemCount);

            itemList.pager = pager;

            mapperBus.Map<IItemListMappable>(itemList);

            if (this.HasInformation)
            {
                return this.item_search(itemList, EnumEck.Error);
            }

            itemList.pager.LoadPageList(itemList.recordCount);

            //FormタグにHiddenを自動入力
            itemList.SetOutputHidden(true);

            return View("item_list", itemList);
        }

        /// <summary>
        /// 商品カテゴリの設定
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult cate_regist(CateRegist cateRegist)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICateRegistCommand>(cateRegist), cateRegist);
            if (!ModelState.IsValid)
            {
                if (cateRegist.registCategoryItem)
                {
                    mapperBus.Map<ICateRegistMappable>(cateRegist);
                }
                return View("cate_regist", cateRegist);
            }


            mapperBus.Map<ICateRegistMappable>(cateRegist);

            if (cateRegist.registCategory || cateRegist.registCategoryItem)
            {
                //カテゴリのヘッダを更新する。
                commandBus.Submit<ICateRegistCommand>(cateRegist, EnumCommandTransaction.BeginTransaction);
                return View("cate_regist_complete", cateRegist);
            }

            return View("cate_regist", cateRegist);
        }


        /// <summary>
        /// カート混在許可グループ設定
        /// </summary>
        public ActionResult item_coexistence(Item_coexistence item_coexistence, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IItemCoexistenceCommand>(item_coexistence), item_coexistence);
            if (!this.IsErrorBack(eck))
            {
                //データ表示
                mapperBus.Map<IItemCoexistenceMappable>(item_coexistence);
            }

            return View("item_coexistence", item_coexistence);
        }

        /// <summary>
        /// カート混在許可グループ設定 完了
        /// </summary>
        public ActionResult item_coexistence_complete(Item_coexistence item_coexistence)
        {
            ModelState.AddModelErrors(commandBus.Validate<IItemCoexistenceCommand>(item_coexistence), item_coexistence);
            if (!ModelState.IsValid)
            {
                return this.item_coexistence(item_coexistence, EnumEck.Error);
            }

            //データの操作
            commandBus.Submit<IItemCoexistenceCommand>(item_coexistence, EnumCommandTransaction.BeginTransaction);

            return View("item_coexistence_complete", item_coexistence);
        }

        /// <summary>
        /// 新規商品の一括登録
        /// </summary>
        public ActionResult item_csv()
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var item_tsv = new Mall_item_tsv();
            this.mapperBus.Map<IMallTSVMappable>(item_tsv);

            return View("item_csv", item_tsv);
        }

        /// <summary>
        /// 新規商品の一括登録 確認画面
        /// </summary>
        public ActionResult item_csv_confirm(Item_csv item_csv)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IItemCSVCommand>(item_csv), item_csv);
            if (!ModelState.IsValid)
            {
                //エラーは確認画面に表示する。 // display error to this confirmation page.
            }

            this.mapperBus.Map<IItemCSVMappable>(item_csv);

            //モデル情報の引き渡し
            item_csv.SetOutputHidden(true);

            //テンプレート表示
            return View("item_csv_confirm", item_csv);
        }

        /// <summary>
        /// 新規商品の一括登録 完了画面
        /// </summary>
        public ActionResult item_csv_complete(Item_csv item_csv)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IItemCSVCommand>(item_csv), item_csv);
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            this.mapperBus.Map<IItemCSVMappable>(item_csv);
            this.commandBus.Submit<IItemCSVCommand>(item_csv, EnumCommandTransaction.WithoutBeginTransaction);

            //モデル情報の引き渡し
            item_csv.SetOutputHidden(true);

            //テンプレート表示
            return View("item_csv_complete", item_csv);
        }

        /// <summary>
        /// モール商品の一括登録 確認画面
        /// </summary>
        public ActionResult mall_item_tsv_confirm(Mall_item_tsv item_tsv)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IMallCSVCommand>(item_tsv), item_tsv);
            if (!ModelState.IsValid)
            {
                //エラーは確認画面に表示する。 // display error to this confirmation page.
            }

            //モデル情報の引き渡し
            item_tsv.SetOutputHidden(true);

            //テンプレート表示
            return View("mall_item_tsv_confirm", item_tsv);
        }

        /// <summary>
        /// 新規商品の一括登録 完了画面
        /// </summary>
        public ActionResult mall_item_tsv_complete(Mall_item_tsv item_tsv)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IMallCSVCommand>(item_tsv), item_tsv);
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            this.commandBus.Submit<IMallCSVCommand>(item_tsv, EnumCommandTransaction.BeginTransaction);

            //モデル情報の引き渡し
            item_tsv.SetOutputHidden(true);

            //テンプレート表示
            return View("mall_item_tsv_complete", item_tsv);
        }

        /// <summary>
        /// CSVダウンロード（全件）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_csv_all(ItemList itemList)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<IItemListCSVMappable>(itemList);
            return this.CsvFile(itemList.csvCreater.filePath);
        }

        /// <summary>
        /// CSVダウンロード（ページ）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_csv_page(ItemList itemList)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(itemList.pageCnt, itemList.maxItemCount);

            itemList.pager = pager;
            mapperBus.Map<IItemListCSVMappable>(itemList);
            return this.CsvFile(itemList.csvCreater.filePath);
        }

        /// <summary>
        /// 商品番号検索ページ
        /// </summary>
        /// <returns></returns>
        public ActionResult common_item_search(CommonItemList commonItemList, EnumEck? eck = null)
        { 
            if (!this.IsErrorBack(eck))
            {
                this.ClearModelState(commonItemList);
            }

            commonItemList.SetOutputHidden("g_search_mode", true);

            return View("common_item_search", commonItemList);
        }

        /// <summary>
        /// 商品番号選択ページ
        /// </summary>
        /// <returns></returns>
        public ActionResult common_item_list(CommonItemList commonItemList)
        {
            if (!ModelState.IsValid)
            {
                return this.common_item_search(commonItemList, EnumEck.Error);
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", commonItemList.pageCnt, commonItemList.maxItemCount);

            commonItemList.pager = pager;

            mapperBus.Map<ICommonItemListMappable>(commonItemList);

            if (this.HasInformation)
            {
                return this.common_item_search(commonItemList, EnumEck.Error);
            }

            commonItemList.pager.LoadPageList(commonItemList.recordCount);

            //FormタグにHiddenを自動入力
            commonItemList.SetOutputHidden("g_search_mode", true);

            return View("common_item_search", commonItemList);
        }

        /// <summary>
        /// カテゴリツリー表示
        /// </summary>
        public ActionResult cate_tree_view(CateTreeView CateTreeView)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<ICateTreeViewMappable>(CateTreeView);
            return View("cate_tree_view", CateTreeView);
        }

        /// <summary>
        /// セット商品の一括登録
        /// </summary>        
        public ActionResult set_item_csv()
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("set_item_csv");
        }

        /// <summary>
        /// セット商品の一括登録 確認画面
        /// </summary>        
        public ActionResult set_item_csv_confirm(Set_Item_csv set_item_csv)
        {
            ModelState.AddModelErrors(commandBus.Validate<ISetItemCSVCommand>(set_item_csv), set_item_csv);
            if (!ModelState.IsValid)
            {
                //エラーは確認画面に表示する。
            }

            //モデル情報の引き渡し
            set_item_csv.SetOutputHidden(true);

            //テンプレート表示
            return View("set_item_csv_confirm", set_item_csv);
        }

        /// <summary>
        /// セット商品の一括登録 完了画面
        /// </summary>        
        public ActionResult set_item_csv_complete(Set_Item_csv set_item_csv)
        {

            ModelState.AddModelErrors(commandBus.Validate<ISetItemCSVCommand>(set_item_csv), set_item_csv);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //モデル情報の引き渡し
            set_item_csv.SetOutputHidden(true);

            commandBus.Submit<ISetItemCSVCommand>(set_item_csv, EnumCommandTransaction.BeginTransaction);

            //テンプレート表示
            return View("set_item_csv_complete", set_item_csv);
        }


        /// <summary>
        /// セット商品マスタの入替
        /// </summary>
        [ErsAdminProcessCompletion("set_item_all_csv_ransu", mode = EnumHandlingMode.RESET)]
        public ActionResult set_item_all_csv()
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("set_item_all_csv");
        }

        /// <summary>
        /// セット商品マスタの入替 確認画面
        /// </summary>
        [ErsAdminProcessCompletion("set_item_all_csv_ransu", mode = EnumHandlingMode.CHECK)]
        public ActionResult set_item_all_csv_confirm(Set_Item_all_csv set_item_all_csv)
        {
            ModelState.AddModelErrors(commandBus.Validate<ISetItemAllCSVCommand>(set_item_all_csv), set_item_all_csv);
            if (!ModelState.IsValid)
            {
                //エラーは確認画面に表示する。
            }

            //モデル情報の引き渡し
            set_item_all_csv.SetOutputHidden(true);

            //テンプレート表示
            return View("set_item_all_csv_confirm", set_item_all_csv);
        }

        /// <summary>
        /// セット商品マスタの入替 完了画面
        /// </summary>
         [ErsAdminProcessCompletion("set_item_all_csv_ransu", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult set_item_all_csv_complete(Set_Item_all_csv set_item_all_csv)
        {
            ModelState.AddModelErrors(commandBus.Validate<ISetItemAllCSVCommand>(set_item_all_csv), set_item_all_csv);
            if (!ModelState.IsValid)
            {
                //エラー行は無視して更新する。
            }

            //モデル情報の引き渡し
            set_item_all_csv.SetOutputHidden(true);
 
            // insert merchandise
            commandBus.Submit((ISetItemAllCSVCommand)set_item_all_csv, EnumCommandTransaction.BeginTransaction);

            //テンプレート表示
            return View("set_item_all_csv_complete", set_item_all_csv);
        }

        [ErsAdminProcessCompletion("item_csv_delete", mode = EnumHandlingMode.RESET)]
         public ActionResult item_csv_delete()
         {
             if (!ModelState.IsValid)
             {
                 return GetErrorView();
             }

             return View("item_csv_delete");
         }

        [ErsAdminProcessCompletion("item_csv_delete", mode = EnumHandlingMode.CHECK)]
        public ActionResult item_csv_delete_confirm(Item_Csv_Delete item_csv_delete)
        {
            ModelState.AddModelErrors(commandBus.Validate<IItemCSVDeleteCommand>(item_csv_delete), item_csv_delete);
            if (!ModelState.IsValid)
            {
                //エラー行は無視して更新する。
            }

            mapperBus.Map<IItemCSVDeleteMappable>(item_csv_delete);
            item_csv_delete.SetOutputHidden(true);

            return View("item_csv_delete_confirm", item_csv_delete);
        }

        public ActionResult item_csv_delete_complete(Item_Csv_Delete item_csv_delete)
        {
            ModelState.AddModelErrors(commandBus.Validate<IItemCSVDeleteCommand>(item_csv_delete), item_csv_delete);
            if (!ModelState.IsValid)
            {
                return GetErrorView();

            }

            mapperBus.Map<IItemCSVDeleteMappable>(item_csv_delete);
            commandBus.Submit<IItemCSVDeleteCommand>(item_csv_delete, EnumCommandTransaction.BeginTransaction);

            item_csv_delete.SetOutputHidden(true);

            return View("item_csv_delete_complete", item_csv_delete);
        }

        public ActionResult download_product_data_for_rakuten(ItemList itemList)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<IItemRakutenListCSVMappable>(itemList);
            return this.CsvFile(itemList.csvCreater.filePath);
        }

        public ActionResult download_product_data_for_yahoo(ItemList itemList)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<IItemYahooListCSVMappable>(itemList);
            return this.CsvFile(itemList.csvCreater.filePath);
        }

        public ActionResult download_product_data_for_amazon(ItemList itemList)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            mapperBus.Map<IItemAmazonListCSVMappable>(itemList);
            return this.CsvFile(itemList.csvCreater.filePath);
        }

        public ActionResult set_item_search(set_item_search set_item_search, EnumEck? eck = null)
        {
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    return GetErrorView();
                }
            }

            return View("set_item_search", set_item_search);
        }

        public ActionResult set_item_list(set_item_search item_list)
        {
            if (!ModelState.IsValid)
            {
                return set_item_search(item_list, EnumEck.Error);
            }

            item_list.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", item_list.pageCnt, item_list.maxItemCount);

            mapperBus.Map<ISetItemSearchMappable>(item_list);

            item_list.pager.LoadPageList(item_list.recordCount);

            item_list.SetOutputHidden(true);

            return View("set_item_list", item_list);
        }

        /// <summary>
        /// CSVダウンロード（全件）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_set_item_csv_all(set_item_search itemList)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<ISetItemListCSVMappable>(itemList);

            return this.CsvFile(itemList.csvCreater.filePath);
        }

        /// <summary>
        /// CSVダウンロード（ページ）
        /// </summary>
        /// <returns></returns>
        public ActionResult download_set_item_csv_page(set_item_search itemList)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(itemList.pageCnt, itemList.maxItemCount);

            itemList.pager = pager;
            this.mapperBus.Map<ISetItemListCSVMappable>(itemList);
            return this.CsvFile(itemList.csvCreater.filePath);
        }

        public ActionResult set_item_modify(set_item_modify set_item_modify, set_item_search set_item_searach, EnumEck? eck = null)
        {
            if (eck != EnumEck.Error)
            {
                if (!ModelState.IsValid)
                {
                    return GetErrorView();
                }

                //set_item_modify.LoadData = true;
                this.mapperBus.Map<ISetItemModifyMappable>(set_item_modify);
            }

            this.AddModelToView(set_item_searach);
            set_item_searach.SetOutputHidden(true);
            set_item_modify.SetOutputHidden(true);
            return View("set_item_modify", set_item_modify);
        }

        public ActionResult set_item_modify_complete(set_item_modify set_item_modify_complete, set_item_search set_item_search)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<ISetItemModifyCommand>(set_item_modify_complete), set_item_modify_complete);
            if (!ModelState.IsValid)
            {
                return this.set_item_modify(set_item_modify_complete, set_item_search, EnumEck.Error);
            }

            this.commandBus.Submit<ISetItemModifyCommand>(set_item_modify_complete, EnumCommandTransaction.BeginTransaction);
            this.AddModelToView(set_item_search);
            set_item_search.SetOutputHidden(true);
            set_item_modify_complete.SetOutputHidden(true);
            return View("set_item_modify_complete", set_item_modify_complete);
        }
    }
}
                                    