﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.administrator;
using ersAdmin.Models;
using jp.co.ivp.ers;
using ersAdmin.Domain.Home.Mappables;


namespace ersAdmin.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu(Order = 1)]
    [ErsNavigationMenu(RaiseRoleError = false, Order = 2)]
	[ErsAuthorization]
    public class HomeController
        : ErsControllerSecureAdmin
    {
        public ActionResult Index(index index)
        {
            EnumUserState state = ((ISession)ErsContext.sessionState).getUserState();

            mapperBus.Map<IIndexMappable>(index);

            return View("index", index);
        }
    }
}
