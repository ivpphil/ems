﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.administrator;
using ersAdmin.Models;
using jp.co.ivp.ers.contents;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers;
using ersAdmin.Domain.Cms.Mappables;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsAuthorization]
    [RoleCheck]
    [ErsLanguageMenu]
    [ErsNavigationMenu]
    public class cmsController : ErsControllerSecureAdmin
    {
        public ActionResult index()
        {
            return View("index");
        }

        public ActionResult contents_index()
        {
            return cms_content_list();
        }

        #region cms_contents_t
        public ActionResult cms_content_list()
        {
            if (!this.ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            var contentList = new contents_list();

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", contentList.pageCnt, contentList.maxItemCount);
            contentList.pager = pager;

            this.mapperBus.Map<IContentsListMappable>(contentList);

            pager.LoadPageList(contentList.recordCount);
            return View("cms_content_list", contentList);
        }

        public ActionResult cms_contents_regist(contents_regist contentsRegist, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IContentsRegistCommand>(contentsRegist), contentsRegist);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(contentsRegist);
                contentsRegist.active = EnumActive.Active;
            }

            return View("cms_contents_regist", contentsRegist);
        }

        public ActionResult cms_contents_regist_confirm(contents_regist contentsRegist)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IContentsRegistCommand>(contentsRegist), contentsRegist);
            if (!ModelState.IsValid)
            {
                return cms_contents_regist(contentsRegist, EnumEck.Error);
            }

            contentsRegist.SetOutputHidden(true);
            
            return View("cms_contents_regist_confirm", contentsRegist);
        }

        public ActionResult cms_contents_regist_complete(contents_regist contentsRegist)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IContentsRegistCommand>(contentsRegist), contentsRegist);
            if (!ModelState.IsValid)
            {
                return cms_contents_regist(contentsRegist, EnumEck.Error);
            }

            commandBus.Submit<IContentsRegistCommand>(contentsRegist, EnumCommandTransaction.BeginTransaction);

            return View("cms_contents_regist_complete", contentsRegist);
        }

        public ActionResult cms_contents_modify(contents_modify contentsModify, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IContentsModifyCommand>(contentsModify), contentsModify);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(contentsModify);
                this.mapperBus.Map<IContentsModifyMappable>(contentsModify);
            }

            return View("cms_contents_modify", contentsModify);
        }

        public ActionResult cms_contents_modify_confirm(contents_modify contentsModify)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IContentsModifyCommand>(contentsModify), contentsModify);
            if (!ModelState.IsValid)
            {
                return cms_contents_modify(contentsModify, EnumEck.Error);
            }

            contentsModify.SetOutputHidden(true);
            return View("cms_contents_modify_confirm", contentsModify);
        }

        public ActionResult cms_contents_modify_complete(contents_modify contentsModify)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IContentsModifyCommand>(contentsModify), contentsModify);
            if (!ModelState.IsValid)
            {
                return cms_contents_modify(contentsModify, EnumEck.Error);
            }

            commandBus.Submit<IContentsModifyCommand>(contentsModify, EnumCommandTransaction.BeginTransaction);

            return View("cms_contents_modify_complete", contentsModify);
        }

        public ActionResult cms_contents_del_complete(contents_modify contentsModify)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IContentsDeleteCommand>(contentsModify), contentsModify);
            if (!ModelState.IsValid)
            {
                return cms_contents_modify(contentsModify, EnumEck.Error);
            }

            commandBus.Submit<IContentsDeleteCommand>(contentsModify, EnumCommandTransaction.BeginTransaction);

            return View("cms_contents_del_complete", contentsModify);
        }
        #endregion

        #region CMS_FreeArea
        public ActionResult cms_free_index()
        {
            if (!this.ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            var cms_free_index = new free_index();

            this.mapperBus.Map<IFreeIndexMappable>(cms_free_index);

            return View("cms_free_index", cms_free_index);
        }

        public ActionResult cms_free_list(free_list freeList)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IFreeListCommand>(freeList), freeList);
            if (!freeList.IsValidField("contents_code"))
            {
                return this.GetErrorView();
            }

            if (ModelState.IsValid)
            {
                var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", freeList.pageCnt, freeList.maxItemCount);
                freeList.pager = pager;

                this.mapperBus.Map<IFreeListMappable>(freeList);

                pager.LoadPageList(freeList.recordCount);
            }

            freeList.SetOutputHidden(true);
            return View("cms_free_list", freeList);
        }

        public ActionResult cms_free_modify(free_modify freeModify, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IFreeModifyCommand>(freeModify), freeModify);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(freeModify);
                freeModify.IsInitialize = true;
            }

            this.mapperBus.Map<IFreeModifyMappable>(freeModify);


            freeModify.SetOutputHidden("required", true);

            return View("cms_free_modify", freeModify);
        }

        public ActionResult cms_free_modify_confirm(free_modify freeModify)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IFreeModifyCommand>(freeModify), freeModify);
            if (!ModelState.IsValid)
            {
                return cms_free_modify(freeModify, EnumEck.Error);
            }
            this.mapperBus.Map<IFreeModifyMappable>(freeModify);

            freeModify.IsConfirmation = true;
            commandBus.Submit<IFreeModifyCommand>(freeModify, EnumCommandTransaction.BeginTransaction);

            freeModify.SetOutputHidden("required", true);
            freeModify.SetOutputHidden("input", true);

            return View("cms_free_modify_confirm", freeModify);
        }

        public ActionResult cms_free_modify_complete(free_modify freeModify)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IFreeModifyCommand>(freeModify), freeModify);
            if (!ModelState.IsValid)
            {
                return cms_free_modify(freeModify, EnumEck.Error);
            }

            this.mapperBus.Map<IFreeModifyMappable>(freeModify);

            commandBus.Submit<IFreeModifyCommand>(freeModify, EnumCommandTransaction.BeginTransaction);

            freeModify.SetOutputHidden("required", true);

            return View("cms_free_modify_complete", freeModify);
        }

        public ActionResult cms_free_delete_complete(free_modify freeModify)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IFreeDeleteCommand>(freeModify), freeModify);
            if (!ModelState.IsValid)
            {
                return cms_free_modify(freeModify, EnumEck.Error);
            }

            this.mapperBus.Map<IFreeModifyMappable>(freeModify);

            commandBus.Submit<IFreeDeleteCommand>(freeModify, EnumCommandTransaction.BeginTransaction);
            
            return View("cms_free_delete_complete", freeModify);
        }

        public ActionResult cms_free_select_templete(free_template freeTemplate)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IFreeTemplateCommand>(freeTemplate), freeTemplate);
            if (!this.ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            this.mapperBus.Map<IFreeTemplateMappable>(freeTemplate);

            freeTemplate.SetOutputHidden(true);
            return View("cms_free_select_templete", freeTemplate);
        }

        public ActionResult cms_free_regist(free_regist freeRegist, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IFreeRegistCommand>(freeRegist), freeRegist);
            if (!this.IsErrorBack(eck))
            {
                ClearModelState(freeRegist);
                freeRegist.IsInitialize = true;
            }

            this.mapperBus.Map<IFreeRegistMappable>(freeRegist);

            freeRegist.SetOutputHidden("required", true);
            return View("cms_free_regist", freeRegist);
        }

        public ActionResult cms_free_regist_confirm(free_regist freeRegist)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IFreeRegistCommand>(freeRegist), freeRegist);
            if (!ModelState.IsValid)
            {
                return cms_free_regist(freeRegist, EnumEck.Error);
            }

            this.mapperBus.Map<IFreeRegistMappable>(freeRegist);

            freeRegist.IsConfirmation = true;
            commandBus.Submit<IFreeRegistCommand>(freeRegist, EnumCommandTransaction.BeginTransaction);

            freeRegist.SetOutputHidden("required", true);
            freeRegist.SetOutputHidden("input", true);
            return View("cms_free_regist_confirm", freeRegist);
        }

        public ActionResult cms_free_regist_complete(free_regist freeRegist)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IFreeRegistCommand>(freeRegist), freeRegist);
            if (!ModelState.IsValid)
            {
                return cms_free_regist(freeRegist, EnumEck.Error);
            }

            this.mapperBus.Map<IFreeRegistMappable>(freeRegist);

            commandBus.Submit<IFreeRegistCommand>(freeRegist, EnumCommandTransaction.BeginTransaction);

            return View("cms_free_regist_complete", freeRegist);
        }
        #endregion

        public ActionResult update_table(update_table model)
        {
            ModelState.AddModelErrors(commandBus.Validate<IUpdateTableCommand>(model), model);
            
            if (!ModelState.IsValid)
            {
                //return GetErrorView();
            }

            this.mapperBus.Map<IUpdateTableMappable>(model);

            commandBus.Submit<IUpdateTableCommand>(model, EnumCommandTransaction.BeginTransaction);

            this.mapperBus.Map<IUpdateTableMappable>(model);
            
            return View("update_table",model);
        }

        public ActionResult download_table(download_table model)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDownloadTableCommand>(model), model);

            if (!ModelState.IsValid)
            {
                //return GetErrorView();
            }

            this.mapperBus.Map<IDownloadTableMappable>(model);

            commandBus.Submit<IDownloadTableCommand>(model, EnumCommandTransaction.BeginTransaction);

            this.mapperBus.Map<IDownloadTableMappable>(model);

            return View("download_table",model);
        }

        public ActionResult downloadFile(update_table model)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var filePath = (!model.isFromDownload) ? setup.uploadedFileErrorPath : setup.downloadFileResultPath;

            var path = Path.Combine(filePath, model.downloadFile);

            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            string fileName = model.downloadFile;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

            //return this.CsvFile(setup.uploadedFileErrorPath, model.downloadFile);                       
        }

        public ActionResult insert_table(insert_table insert_table)
        {
            ModelState.AddModelErrors(commandBus.Validate<IInsertTableCommand>(insert_table), insert_table);

            if (!ModelState.IsValid)
            {
                //return GetErrorView();
            }

            if (ModelState.IsValid)
            {
                this.commandBus.Submit<IInsertTableCommand>(insert_table, EnumCommandTransaction.WithoutBeginTransaction);
            }

            this.mapperBus.Map<IInsertTableMappable>(insert_table);

            return View("insert_table", insert_table);
        }

        public ActionResult download_insert_err_file(insert_table insert_table)
        {
            return this.CsvFile(insert_table.download_path);
        }

        public ActionResult delete_table(delete_table delete_table)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDeleteTableCommand>(delete_table), delete_table);

            if (!ModelState.IsValid)
            {
                //return GetErrorView();
            }

            if (ModelState.IsValid)
            {
                this.commandBus.Submit<IDeleteTableCommand>(delete_table, EnumCommandTransaction.WithoutBeginTransaction);
            }

            this.mapperBus.Map<IDeleteTableMappable>(delete_table);

            return View("delete_table", delete_table);
        }

        public ActionResult download_delete_err_file(delete_table delete_table)
        {
            return this.CsvFile(delete_table.download_path);
        }
    }
}

