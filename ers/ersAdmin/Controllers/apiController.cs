﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using ersAdmin.Models;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Api.Commands;
using ersAdmin.Domain.Api.Mappables;
using ersAdmin.Models.api;

namespace ersAdmin.Controllers
{
    [ValidateInput(false)]
    public class apiController
        : ErsControllerSecureAdmin
    {

        /// <summary>
        /// 郵便番号検索１
        /// 非同期検索処理
        /// </summary>
        /// <returns></returns>
        public ActionResult get_zip(getZip get_zip)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IGetZipCommand>(get_zip), get_zip);

            //Validatin結果はMapの中で判定する。
            this.mapperBus.Map<IGetZipMappable>(get_zip);

            return View("get_zip", get_zip);
        }

        /// <summary>
        /// 郵便番号検索１
        /// 非同期検索処理
        /// </summary>
        /// <returns></returns>
        public ActionResult tiny_mce(TinyMce tinyMce)
        {
            Response.ContentType = "application/javascript";
            return View("tiny_mce", tinyMce);
        }

        /// <summary>
        /// Get Result
        /// </summary>
        /// <param name="file_uploader"></param>
        /// <returns></returns>
        [NonAction]
        public ActionResult get_result(file_uploader_base file_uploader)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<IFileUploaderBaseMappable>(file_uploader);

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Default action for images 
        /// </summary>
        /// <param name="image_uploader"></param>
        /// <returns></returns>
        public ActionResult image_upload(image_uploader image_uploader)
        {
            return this.get_result(image_uploader);
        }

        /// <summary>
        /// Default action for all files
        /// </summary>
        /// <param name="file_uploader"></param>
        /// <returns></returns>
        public ActionResult file_upload(file_uploader file_uploader)
        {
            return this.get_result(file_uploader);
        }

        /// <summary>
        /// action for item_modify page
        /// </summary>
        /// <param name="item_image_uploader"></param>
        /// <returns></returns>
        public ActionResult item_image_upload(item_image_uploader item_image_uploader)
        {
            return this.get_result(item_image_uploader);
        }
    }
}
