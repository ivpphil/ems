﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using ersAdmin.App_Start;
using jp.co.ivp.ers.util.HttpModule;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(CustomHeaderPackage), "PreStart")]


namespace ersAdmin.App_Start
{
    public class CustomHeaderPackage
    {
        public static void PreStart()
        {
            DynamicModuleUtility.RegisterModule(typeof(CustomHeaderModule));
        }
    }
}
