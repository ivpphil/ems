﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Api.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Api.Handlers
{
    public class ValidateGetZip
        : IValidationHandler<IGetZipCommand>
    {
        public IEnumerable<ValidationResult> Validate(IGetZipCommand command)
        {
            yield return command.CheckRequired("zip");
        }
    }
}