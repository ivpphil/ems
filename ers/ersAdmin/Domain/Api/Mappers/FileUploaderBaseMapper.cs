﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Api.Mappables;
using System.IO;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using System.Text.RegularExpressions;

namespace ersAdmin.Domain.Api.Mappers
{
    public class FileUploaderBaseMapper
        : IMapper<IFileUploaderBaseMappable>
    {
        public void Map(IFileUploaderBaseMappable objMappable)
        {
            if (objMappable.file != null)
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();

                if (!objMappable.temp_folder.EndsWith("\\"))
                    objMappable.temp_folder += "\\";

                var temp_path = setup.image_temp_directory + objMappable.temp_folder;

                string temp_file_name = this.GetNewTempFileName(objMappable);

                this.SaveImage(objMappable.file, temp_path, temp_file_name);
            }
        }

        private void SaveImage(HttpPostedFileBase imageField, string directoryPath, string fileName)
        {
            if (imageField != null)
            {
                ErsDirectory.CreateDirectories(directoryPath);
                imageField.SaveAs(directoryPath + "\\" + fileName);
            }
        }

        private string GetNewTempFileName(IFileUploaderBaseMappable objMappable)
        {
            var file_identifier = objMappable.file_identifier;
            var new_file_name = this.GetRemovedFileIdentifier(objMappable);

            return file_identifier + new_file_name;
        }

        private string GetRemovedFileIdentifier(IFileUploaderBaseMappable objMappable)
        {
            var file_name = objMappable.file.FileName;

            if (objMappable.file.FileName.ToLower() == "blob" && objMappable.name.HasValue())
                file_name = objMappable.name;

            file_name = Regex.Replace(Path.GetFileName(file_name), objMappable.file_identifier, string.Empty, RegexOptions.IgnoreCase);
            return file_name;
        }
    }
}