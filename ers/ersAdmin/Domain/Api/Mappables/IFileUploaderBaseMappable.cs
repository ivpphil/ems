﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Api.Mappables
{
    public interface IFileUploaderBaseMappable
        : IMappable
    {
        HttpPostedFileBase file { get; }

        string temp_file_name { get; }

        string temp_folder { get; set; }

        string name { get; }

        string file_identifier { get; }
    }
}