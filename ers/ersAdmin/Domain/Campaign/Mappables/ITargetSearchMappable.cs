﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.util;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Campaign.Mappables
{
    public interface ITargetSearchMappable : ISiteSearchBaseMappable, IMappable
    {
        long recordCount { get; set; }

        ErsPagerModel pager { get; set; }

        List<Dictionary<string, object>> TargetList { set; }

        DateTime? s_intime_from { get;  }

        DateTime? s_intime_to { get;  }

        string s_target_name { get;  }

        int? s_recency_from { get;  }

        int? s_recency_to { get;  }

        int? s_frequency_from { get; }

        int? s_frequency_to { get; }

        int? s_monetary_from { get;  }

        int? s_monetary_to { get;  }
    }
}