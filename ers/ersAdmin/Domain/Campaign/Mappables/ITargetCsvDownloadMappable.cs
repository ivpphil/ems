﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Campaign.Mappables
{
    public interface ITargetCsvDownloadMappable
        : IMappable
    {
        ErsCsvCreater csvCreater { get; }

        int? id { get; }
    }
}