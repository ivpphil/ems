﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.pager;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Campaign.Mappables
{
    public interface ICampaignSearchMappable : ISiteSearchBaseMappable, IMappable
    {
        long recordCount { get; set; }
        string s_ccode { get; }
        EnumActive? s_active { get; }
        string s_campaign_name { get;  }
        DateTime? s_term_from { get;  }
        DateTime? s_term_to { get;  }
        List<Dictionary<string, object>> ActiveList { get; }
        List<Dictionary<string, object>> CampaignList { get; set; }

        ErsPagerModel pager { get; }
    }
}