﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.campaign;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Campaign.Mappables
{
    public interface ITargetModifyMappable : ISiteRegisterBaseMappable, IMappable
    {
        int? id { get; set; }  

        EnumOrderPattern? order_ptn_kbn { get; }

        int? target_item_max { get; set; }

        int? targetexcluded_item_max { get; set; }

        List<scenario_item> scenario_item_table { get; set; }

        List<scenario_item> scenarioexcluded_item_table { get; set; }

        int? recency_to { get; set; }

        int? frequency_to { get; set; }

        int? monetary_to { get; set; }

        bool IsErrorBack { get; set; }
    }
}