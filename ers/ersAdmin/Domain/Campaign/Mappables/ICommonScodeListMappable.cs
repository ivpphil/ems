﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Campaign.Mappables
{
    public interface ICommonScodeListMappable : IMappable
    {
        long recordCount { get; set; }
        string s_sname { get; set; }
        string s_gcode { get; set; }
        string s_scode { get; set; }
        int? s_cate1 { get; set; }
        int? s_cate2 { get; set; }
        int? s_cate3 { get; set; }
        int? s_cate4 { get; set; }
        int? s_cate5 { get; set; }

        ErsPagerModel pager { get; }
        List<Dictionary<string, object>> MeList { set;}

        bool g_search_mode { get; set; }

        EnumDocBundlingFlg s_doc_bundling_flg { get; set; }
        
    }
}