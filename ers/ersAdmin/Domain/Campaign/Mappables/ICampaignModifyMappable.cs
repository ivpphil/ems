﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.campaign;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Campaign.Mappables
{
    public interface ICampaignModifyMappable : ISiteRegisterBaseMappable, IMappable
    {
        string ccode { get; }
        int? id { get; }
        string scode { get; }
        string sname { get; set; }
        List<Campaign_modify_detail> detail_table { get; set; }
        List<Campaign_modify_target_s_list> target_detail_table { get; set; }
        int? included_item_max { get; set;}
        int? target_item_max { get; set; }
        int? trigger_item_max { get; set; }
    }
}