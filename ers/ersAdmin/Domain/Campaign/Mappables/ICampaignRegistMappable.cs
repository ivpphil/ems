﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Campaign.Mappables
{
    public interface ICampaignRegistMappable : IMappable
    {
        int? included_item_max {  set; }
        int? target_item_max {  set; }
        int? trigger_item_max { set; }
    }
}