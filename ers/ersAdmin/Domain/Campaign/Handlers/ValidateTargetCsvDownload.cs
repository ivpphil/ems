﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Campaign.Commands;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class ValidateTargetCsvDownload
        : IValidationHandler<ITargetCsvDownloadCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ITargetCsvDownloadCommand command)
        {
            yield return command.CheckRequired("id");
        }
    }
}