﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class CampaignDeleteHandler
        : ICommandHandler<ICampaignDeleteCommand>
    {
        public ICommandResult Submit(ICampaignDeleteCommand command)
        {
            this.Execute(command);

            return new CommandResult(true);
        }

        public void Execute(ICampaignDeleteCommand command)
        {
            var repository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();
            var objCampaign = ErsFactory.ersDocBundleFactory.getErsCampaignWithId(command.id);
            repository.Delete(objCampaign);

            ///Delete Sub data from other tables
            this.DeleteCampaignInDoc(command);
            this.DeleteCampaignInTarget(command);
        }

        /// <summary>
        /// 同梱登削除
        /// </summary>
        [Obsolete("idベースでDELETEするように修正")]
        public void DeleteCampaignInDoc(ICampaignDeleteCommand command)
        {
            var Doc_Repo = ErsFactory.ersDocBundleFactory.GetErsDocBundlingRepository();

            var doc_criteria = ErsFactory.ersDocBundleFactory.GetErsDocBundlingCriteria();
            doc_criteria.ccode = command.ccode;
            var doc_boundlingList = Doc_Repo.Find(doc_criteria);

            foreach (var record in doc_boundlingList)
            {
                Doc_Repo.Delete(record);
            }
        }

        /// <summary>
        /// 対象商品削除
        /// </summary>
        [Obsolete("idベースでDELETEするように修正")]
        public void DeleteCampaignInTarget(ICampaignDeleteCommand command)
        {
            var Target_Repo = ErsFactory.ersDocBundleFactory.GetErsDocTargetRepository();
            var doc_target_criteria = ErsFactory.ersDocBundleFactory.GetErsDocTargetCriteria();
            doc_target_criteria.ccode = command.ccode;
            var doc_target_recList = Target_Repo.Find(doc_target_criteria);

            foreach (var record in doc_target_recList)
            {
                Target_Repo.Delete(record);
            }
        }
    }
}