﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.Models.campaign;
using ersAdmin.Domain.SiteBase.Handlers;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class CampaignRegistHandler : SiteRegisterBaseHandler, ICommandHandler<ICampaignRegistCommand>
    {
        public ICommandResult Submit(ICampaignRegistCommand command)
        {
            this.InsertCampaign(command);
            return new CommandResult(true);
        }

        /// <summary>
        /// 
        /// </summary>
        internal void InsertCampaign(ICampaignRegistCommand command)
        {
            var repository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();

            var campaign = ErsFactory.ersDocBundleFactory.GetErsCampaign();
            campaign.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            campaign.intime = DateTime.Now;
            campaign.campaign_type = 1;
            campaign.site_id = this.GetArrayOfSiteId(command);
            repository.Insert(campaign, true);
            //登録されたキャンペーン情報を取得
            command.id = campaign.id;
            //大文字化後のキャンペーンコードを取得
            command.ccode = campaign.ccode;
            //その他テーブル登録
            this.docInsert(command);
            this.trgInsert(command);
        }

        /// <summary>
        /// キャンペーン価格登録
        /// </summary>
        public void priceInsert(ICampaignRegistCommand command)
        {
            if (command.campaign_price != null)
            {
                var repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();

                var new_price = ErsFactory.ersMerchandiseFactory.GetErsPrice();
                new_price.gcode = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(command.scode).gcode;
                new_price.scode = command.scode;
                new_price.price = command.campaign_price;
                repository.Insert(new_price);
            }
        }

        /// <summary>
        /// 同梱物登録
        /// </summary>
        public void docInsert(ICampaignRegistCommand command)
        {
            var repository = ErsFactory.ersDocBundleFactory.GetErsDocBundlingRepository();

            foreach (Campaign_modify_detail dic in command.detail_table)
            {
                var newBundle = ErsFactory.ersDocBundleFactory.GetErsDocBundling();
                newBundle.ccode = command.ccode;
                newBundle.scode = dic.inc_scode;
                newBundle.amount = dic.inc_amount;
                newBundle.intime = DateTime.Now;
                newBundle.duplicate = dic.inc_duplicate;
                repository.Insert(newBundle);
            }
        }

        /// <summary>
        /// 対象商品登録
        /// </summary>
        public void trgInsert(ICampaignRegistCommand command)
        {
            var repository = ErsFactory.ersDocBundleFactory.GetErsDocTargetRepository();

            foreach (Campaign_modify_target_s_list dic2 in command.target_detail_table)
            {
                var newTarget = ErsFactory.ersDocBundleFactory.GetErsDocTarget();
                newTarget.ccode = command.ccode;
                newTarget.scode = dic2.target_scode;
                newTarget.order_type = dic2.target_order_type;
                newTarget.intime = DateTime.Now;
                repository.Insert(newTarget);
            }
        }
    }
}