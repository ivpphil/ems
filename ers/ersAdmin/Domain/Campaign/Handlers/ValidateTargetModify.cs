﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers;
using ersAdmin.Models.campaign;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class ValidateTargetModify : IValidationHandler<ITargetModifyCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ITargetModifyCommand command)
        {
            yield return command.CheckRequired("target_name");
            yield return command.CheckRequired("recency_from");
            yield return command.CheckRequired("frequency_from");
            yield return command.CheckRequired("monetary_from");

            yield return ErsFactory.ersTargetFactory.GetValidateOrderPtnKbnStgy().ValidateTarget(command.id, command.order_ptn_kbn, "order_ptn_kbn");

            var listScode = new List<string>();
            if (command.scenario_item_table != null)
            {
                foreach (scenario_item model in command.scenario_item_table)
                {
                    model.AddInvalidField(command.controller.commandBus.Validate<IScenarioItemListRecordCommand>(model));

                    if (command.order_ptn_kbn == EnumOrderPattern.PurchaseItem)
                    {
                        //Check if There are duplicate records in [Items]
                        if (listScode.Contains(model.scode))
                        {
                            model.AddInvalidField(new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("scode"), model.scode), new[] { "scode" }));
                        }
                        else
                        {
                            listScode.Add(model.scode);
                        }
                    }

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "scenario_item_table" });
                        }
                    }
                }
            }            

            var listExcludeScode = new List<string>();
            if (command.scenarioexcluded_item_table != null)
            {
                foreach (scenario_item model in command.scenarioexcluded_item_table)
                {
                    model.AddInvalidField(command.controller.commandBus.Validate<IScenarioItemListRecordCommand>(model));

                    if (command.order_ptn_kbn == EnumOrderPattern.PurchaseItem)
                    {
                        //Check if There are duplicate records in [Excluding Items]
                        if (listExcludeScode.Contains(model.scode))
                        {
                            model.AddInvalidField(new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("scode"), model.scode), new[] { "scode" }));
                        }
                        else
                        {
                            listExcludeScode.Add(model.scode);
                        }

                        //Check if There are same records in [Item] and [Excluding Items]
                        if (listScode.Contains(model.scode))
                        {
                            model.AddInvalidField(new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("scode"), model.scode), new[] { "scode" }));
                        }
                    }

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "scenarioexcluded_item_table" });
                        }
                    }
                }
            }

            //Check if user choose atleast one(1) product
            if (command.order_ptn_kbn == EnumOrderPattern.PurchaseItem)
            {
                if (command.scenario_item_table.Count == 0 && command.scenarioexcluded_item_table.Count == 0)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10206", ErsResources.GetFieldName("scode")), new[] { "scenario_item_table", "scenarioexcluded_item_table" });
                }
            }

            var checkNumericStgy = ErsFactory.ersCommonFactory.GetCheckNumericFromToStgy();
            foreach (var result in checkNumericStgy.Check("recency_from", command.recency_from, "recency_to", command.recency_to))
            {
                yield return result;
            }

            foreach (var result in checkNumericStgy.Check("frequency_from", command.frequency_from, "frequency_to", command.frequency_to))
            {
                yield return result;
            }

            foreach (var result in checkNumericStgy.Check("monetary_from", command.monetary_from, "monetary_to", command.monetary_to))
            {
                yield return result;
            }

            if (!command.IsValid)
            {
                if(command.scenarioexcluded_item_table != null)
                { 
                    command.targetexcluded_item_max = command.scenarioexcluded_item_table.Count() + 1;
                }
                if (command.scenario_item_table != null)
                {
                    command.target_item_max = command.scenario_item_table.Count() + 1;
                }
            }
        }
    }
}