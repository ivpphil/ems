﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Campaign.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class ValidateTargetDelete
        : IValidationHandler<ITargetDeleteCommand>
    {
        public IEnumerable<ValidationResult> Validate(ITargetDeleteCommand command)
        {
            yield return command.CheckRequired("id");

            if (command.IsValidField("id"))
            {
                foreach (var result in ErsFactory.ersTargetFactory.GetValidateTargetDeletable().Validate(command.id, "id"))
                {
                    yield return result;
                }
            }
        }
    }
}