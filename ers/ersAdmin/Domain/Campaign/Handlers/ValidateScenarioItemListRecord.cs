﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class ValidateScenarioItemListRecord : IValidationHandler<IScenarioItemListRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IScenarioItemListRecordCommand command)
        {
            yield return command.CheckRequired("scode");
            yield return command.CheckRequired("gcode");
            yield return command.CheckRequired("order_type");
        }
    }
}