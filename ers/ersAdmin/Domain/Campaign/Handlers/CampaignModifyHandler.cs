﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.Models.campaign;
using ersAdmin.Domain.SiteBase.Handlers;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class CampaignModifyHandler : SiteRegisterBaseHandler, ICommandHandler<ICampaignModifyCommand>
    {
        public ICommandResult Submit(ICampaignModifyCommand command)
        {
            this.UpdateCampaign(command);
            return new CommandResult(true);
        }

        /// <summary>
        /// キャンペーンテーブル更新
        /// </summary>
        /// <param name="campaignModify"></param>
        internal void UpdateCampaign(ICampaignModifyCommand command)
        {
            var repository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();

            //var new_campaign = ErsFactory.ersCampaignFactory.GetErsCampaign();

            var new_campaign = ErsFactory.ersDocBundleFactory.getErsCampaignWithId(command.id);
            var old_campaign = ErsFactory.ersDocBundleFactory.getErsCampaignWithId(command.id);

            new_campaign.OverwriteWithModel(command);
            new_campaign.utime = DateTime.Now;
            new_campaign.site_id = this.GetArrayOfSiteId(command);

            repository.Update(old_campaign, new_campaign);

            //その他テーブル登録更新
            this.docUpdate(command);
            this.trgUpdate(command);
        }

        /// <summary>
        /// 同梱登録更新
        /// </summary>
        [Obsolete("idベースでUPDATEするように修正")]
        public void docUpdate(ICampaignModifyCommand command)
        {
            var Doc_Repo = ErsFactory.ersDocBundleFactory.GetErsDocBundlingRepository();
            var doc_criteria = ErsFactory.ersDocBundleFactory.GetErsDocBundlingCriteria();
            doc_criteria.ccode = command.ccode;
            var old_doc_boundling = Doc_Repo.Find(doc_criteria);

            foreach (Campaign_modify_detail dic in command.detail_table)
            {
                bool Up_flg = false;
                foreach (var list in old_doc_boundling)
                {
                    if (dic.inc_scode == list.scode)
                    {
                        Up_flg = true;
                    }
                }
                if (Up_flg == true)
                {
                    var tempCriteria = ErsFactory.ersDocBundleFactory.GetErsDocBundlingCriteria();
                    tempCriteria.scode = dic.inc_scode;
                    tempCriteria.ccode = command.ccode;
                    var oldBundle = Doc_Repo.Find(tempCriteria)[0];

                    var newBundle = ErsFactory.ersDocBundleFactory.GetErsDocBundlingWithParameter(oldBundle.GetPropertiesAsDictionary());
                    newBundle.ccode = command.ccode;
                    newBundle.scode = dic.inc_scode;
                    newBundle.amount = dic.inc_amount;
                    newBundle.utime = DateTime.Now;
                    newBundle.duplicate = dic.inc_duplicate;
                    Doc_Repo.Update(oldBundle, newBundle);
                }
                else
                {
                    var newBundle = ErsFactory.ersDocBundleFactory.GetErsDocBundling();
                    newBundle.ccode = command.ccode;
                    newBundle.scode = dic.inc_scode;
                    newBundle.amount = dic.inc_amount;
                    newBundle.intime = DateTime.Now;
                    newBundle.duplicate = dic.inc_duplicate;
                    Doc_Repo.Insert(newBundle);
                }
            }

            this.docDelete(command, old_doc_boundling);
        }

        /// <summary>
        /// 対象商品登録更新
        /// </summary>
        [Obsolete("idベースでUPDATEするように修正")]
        public void trgUpdate(ICampaignModifyCommand command)
        {
            var repository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();
            var Target_Repo = ErsFactory.ersDocBundleFactory.GetErsDocTargetRepository();
            var doc_target_criteria = ErsFactory.ersDocBundleFactory.GetErsDocTargetCriteria();
            doc_target_criteria.ccode = command.ccode;
            var old_doc_target_rec = Target_Repo.Find(doc_target_criteria);

            foreach (Campaign_modify_target_s_list dic in command.target_detail_table)
            {
                bool Up_flg = false;
                foreach (var list in old_doc_target_rec)
                {
                    if (dic.target_scode == list.scode)
                    {
                        Up_flg = true;
                    }
                }
                if (Up_flg == true)
                {
                    var oldTarget = ErsFactory.ersDocBundleFactory.GetErsDocTarget();
                    oldTarget.scode = dic.target_scode;
                    oldTarget.ccode = command.ccode;

                    var newTarget = ErsFactory.ersDocBundleFactory.GetErsDocTarget();
                    newTarget.scode = dic.target_scode;
                    newTarget.ccode = command.ccode;
                    newTarget.order_type = dic.target_order_type;
                    newTarget.utime = DateTime.Now;

                    Target_Repo.Update(oldTarget, newTarget);
                }
                else
                {
                    var newTarget = ErsFactory.ersDocBundleFactory.GetErsDocTarget();
                    newTarget.scode = dic.target_scode;
                    newTarget.ccode = command.ccode;
                    newTarget.order_type = dic.target_order_type;
                    newTarget.intime = DateTime.Now;

                    Target_Repo.Insert(newTarget);
                }
            }

            this.trgDelete(command, old_doc_target_rec);
        }

        [Obsolete("idベースでDELETEするように修正")]
        public void docDelete(ICampaignModifyCommand command, IList<global::jp.co.ivp.ers.doc_bundle.ErsDocBundling> old_doc_boundling)
        {
            var Doc_Repo = ErsFactory.ersDocBundleFactory.GetErsDocBundlingRepository();

            foreach (var old_rec in old_doc_boundling)
            {
                var updatedList = command.detail_table.Where((record) => record.inc_scode == old_rec.scode);

                if (updatedList.Count() <= 0)
                {
                    Doc_Repo.Delete(old_rec);
                }
            }
        }

        [Obsolete("idベースでDELETEするように修正")]
        public void trgDelete(ICampaignModifyCommand command, IList<global::jp.co.ivp.ers.doc_bundle.ErsDocTarget> old_doc_target_rec)
        {
            var Target_Repo = ErsFactory.ersDocBundleFactory.GetErsDocTargetRepository();

            foreach (var old_rec in old_doc_target_rec)
            {
                var updatedList = command.target_detail_table.Where((record) => record.target_scode == old_rec.scode);

                if (updatedList.Count() <= 0)
                {
                    Target_Repo.Delete(old_rec);
                }
            }
        }
    }
}