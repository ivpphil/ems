﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class TargetDeleteHandler
        : ICommandHandler<ITargetDeleteCommand>
    {
        public ICommandResult Submit(ITargetDeleteCommand command)
        {
            var repository = ErsFactory.ersTargetFactory.GetErsTargetRepository();
            var criteria = ErsFactory.ersTargetFactory.GetErsTargetCriteria();
            criteria.id = command.id;

            repository.Delete(criteria);

            return new CommandResult(true);
        }
    }
}