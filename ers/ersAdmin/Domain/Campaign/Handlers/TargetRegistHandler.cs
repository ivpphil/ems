﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.Models.campaign;
using ersAdmin.Domain.SiteBase.Handlers;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class TargetRegistHandler
         : SiteRegisterBaseHandler, ICommandHandler<ITargetRegistCommand>
    {

        public ICommandResult Submit(ITargetRegistCommand command)
        {
            this.InsertTarget(command);
            return new CommandResult(true);
        }

        /// <summary>
        /// Insert target values in target_t table
        /// </summary>
        internal void InsertTarget(ITargetRegistCommand command)
        {
            if (command.order_ptn_kbn != EnumOrderPattern.PurchaseItem)
            {
                command.scenario_item_table.Clear();
                command.scenarioexcluded_item_table.Clear();
            }

            var repository = ErsFactory.ersTargetFactory.GetErsTargetRepository();

            var target = ErsFactory.ersTargetFactory.GetErsTarget();
            target.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            target.recency_to = command.recency_to ?? ushort.MaxValue;
            target.frequency_to = command.frequency_to ?? int.MaxValue;
            target.monetary_to = command.monetary_to ?? int.MaxValue;
            target.intime = DateTime.Now;
            target.site_id = this.GetArrayOfSiteId(command);

            repository.Insert(target, true);

            command.id = target.id;

            this.ScenarioItemInsert(command, target.id);

            this.ScenarioItemExcludedInsert(command, target.id);

        }

        /// <summary>
        /// create new step mail scenario item with step mail scenario id as reference/foreign key
        /// </summary>
        /// <param name="id">id scenario_id</param>
        private void ScenarioItemInsert(ITargetRegistCommand command, int? target_id)
        {
            var repository = ErsFactory.ersTargetFactory.GetErsTargetItemRepository();

            foreach (scenario_item dic2 in command.scenario_item_table)
            {
                var objScenario = ErsFactory.ersTargetFactory.GetErsTargetItem();
                objScenario.target_id = target_id;
                objScenario.scode = dic2.scode;
                objScenario.gcode = dic2.gcode;
                objScenario.target_kbn = EnumTargetKbn.Item;
                objScenario.order_type = dic2.order_type;
                objScenario.intime = DateTime.Now;
                repository.Insert(objScenario, true);
            }
        }

        /// <summary>
        /// create new step mail scenario item with step mail scenario id as reference/foreign key
        /// </summary>
        /// <param name="id">id scenario_id</param>
        private void ScenarioItemExcludedInsert(ITargetRegistCommand command, int? target_id)
        {
            var repository = ErsFactory.ersTargetFactory.GetErsTargetItemRepository();

            foreach (scenario_item dic2 in command.scenarioexcluded_item_table)
            {
                var objScenario = ErsFactory.ersTargetFactory.GetErsTargetItem();
                objScenario.target_id = target_id;
                objScenario.scode = dic2.scode;
                objScenario.gcode = dic2.gcode;
                objScenario.target_kbn = EnumTargetKbn.ExcludingItem;
                objScenario.order_type = dic2.order_type;
                objScenario.intime = DateTime.Now;
                repository.Insert(objScenario, true);
            }
        }
    }
}