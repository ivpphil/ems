﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Campaign.Commands;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class ValidateCampaignDelete
        : IValidationHandler<ICampaignDeleteCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ICampaignDeleteCommand command)
        {
            yield return command.CheckRequired("id");
            yield return command.CheckRequired("ccode");
        }
    }
}