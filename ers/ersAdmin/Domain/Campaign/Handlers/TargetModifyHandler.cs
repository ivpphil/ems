﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using ersAdmin.Models.campaign;
using jp.co.ivp.ers.target;
using ersAdmin.Domain.SiteBase.Handlers;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class TargetModifyHandler
        : SiteRegisterBaseHandler, ICommandHandler<ITargetModifyCommand>
    {


        public ICommandResult Submit(ITargetModifyCommand command)
        {
            this.UpdateTarget(command);
            return new CommandResult(true);
        }

        /// <summary>
        /// update values to target_t table
        /// </summary>
        /// <param name="command"></param>
        internal void UpdateTarget(ITargetModifyCommand command)
        {
            if (command.order_ptn_kbn != EnumOrderPattern.PurchaseItem)
            {
                command.scenario_item_table.Clear();
                command.scenarioexcluded_item_table.Clear();
            }

            var repository = ErsFactory.ersTargetFactory.GetErsTargetRepository();

            var new_target = ErsFactory.ersTargetFactory.GetErsTargetWithId(command.id);
            var old_target = ErsFactory.ersTargetFactory.GetErsTargetWithId(command.id);

            new_target.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            new_target.recency_to = command.recency_to ?? ushort.MaxValue;
            new_target.frequency_to = command.frequency_to ?? int.MaxValue;
            new_target.monetary_to = command.monetary_to ?? int.MaxValue;
            new_target.utime = DateTime.Now;
            new_target.site_id = this.GetArrayOfSiteId(command);

            repository.Update(old_target, new_target);

            this.UpdateScenarioItemIncluded(command);
            this.UpdateScenarioItemExcluded(command);

            this.RemoveItemIncludedDeleted(command, EnumTargetKbn.Item);
            this.RemoveItemIncludedDeleted(command, EnumTargetKbn.ExcludingItem);
        }

        /// <summary>
        /// Updates included items of step mail scenario
        /// </summary>
        internal void UpdateScenarioItemIncluded(ITargetModifyCommand command)
        {
            ErsTargetItemRepository repository = ErsFactory.ersTargetFactory.GetErsTargetItemRepository();
            ErsTargetItemCriteria criteria = ErsFactory.ersTargetFactory.GetErsTargetItemCriteria();
            criteria.target_id = command.id.Value;
            criteria.target_kbn = EnumTargetKbn.Item;

            var oldRec = repository.Find(criteria);

            foreach (scenario_item dic in command.scenario_item_table)
            {
                if (dic.id.HasValue)
                {
                    var oldObjEntity = ErsFactory.ersTargetFactory.GetErsTargetItemWithId(dic.id);
                    var newObjEntity = ErsFactory.ersTargetFactory.GetErsTargetItemWithId(dic.id);
                    newObjEntity.scode = dic.scode;
                    newObjEntity.gcode = dic.gcode;
                    newObjEntity.target_id = command.id;
                    newObjEntity.order_type = dic.order_type;
                    newObjEntity.target_kbn = dic.target_kbn;
                    newObjEntity.utime = DateTime.Now;
                    repository.Update(oldObjEntity, newObjEntity);
                }
                else
                {
                    var objScenario = ErsFactory.ersTargetFactory.GetErsTargetItem();
                    objScenario.target_id = command.id;
                    objScenario.scode = dic.scode;
                    objScenario.gcode = dic.gcode;
                    objScenario.order_type = dic.order_type;
                    objScenario.target_kbn = EnumTargetKbn.Item;
                    objScenario.intime = DateTime.Now;
                    repository.Insert(objScenario, true);
                }
            }
        }

        /// <summary>
        /// Updates included items of step mail scenario 
        /// </summary>
        internal void UpdateScenarioItemExcluded(ITargetModifyCommand command)
        {
            ErsTargetItemRepository repository = ErsFactory.ersTargetFactory.GetErsTargetItemRepository();
            ErsTargetItemCriteria criteria = ErsFactory.ersTargetFactory.GetErsTargetItemCriteria();
            criteria.target_id = command.id.Value;
            criteria.target_kbn = EnumTargetKbn.ExcludingItem;

            var oldRec = repository.Find(criteria);

            foreach (scenario_item dic in command.scenarioexcluded_item_table)
            {
                if (dic.id.HasValue)
                {
                    var oldObjEntity = ErsFactory.ersTargetFactory.GetErsTargetItemWithId(dic.id);
                    var newObjEntity = ErsFactory.ersTargetFactory.GetErsTargetItemWithId(dic.id);
                    newObjEntity.scode = dic.scode;
                    newObjEntity.gcode = dic.gcode;
                    newObjEntity.target_id = command.id;
                    newObjEntity.order_type = dic.order_type;
                    newObjEntity.target_kbn = dic.target_kbn;
                    newObjEntity.utime = DateTime.Now;
                    repository.Update(oldObjEntity, newObjEntity);
                }
                else
                {
                    var objScenario = ErsFactory.ersTargetFactory.GetErsTargetItem();
                    objScenario.target_id = command.id;
                    objScenario.scode = dic.scode;
                    objScenario.gcode = dic.gcode;
                    objScenario.order_type = dic.order_type;
                    objScenario.target_kbn = EnumTargetKbn.ExcludingItem;
                    objScenario.intime = DateTime.Now;
                    repository.Insert(objScenario, true);
                }
            }
        }

        /// <summary>
        /// To refresh the criteria so that the delete can delete all records that need to be deleted
        /// </summary>
        internal ErsTargetItemCriteria GetCriteria(ITargetModifyCommand command, EnumTargetKbn enumtarget)
        {
            ErsTargetItemCriteria criteria = ErsFactory.ersTargetFactory.GetErsTargetItemCriteria();
            criteria.target_id = command.id.Value;
            criteria.target_kbn = enumtarget;

            return criteria;
        }

        internal void RemoveItemIncludedDeleted(ITargetModifyCommand command, EnumTargetKbn enumtarget)
        {
            bool Up_flg = false;

            ErsTargetItemRepository repository = ErsFactory.ersTargetFactory.GetErsTargetItemRepository();
            var criteria = GetCriteria(command, enumtarget);

            var oldRec = repository.Find(criteria);

            Dictionary<string, object> objScenario = new Dictionary<string, object>();

            foreach (var list in oldRec)
            {
                Up_flg = false;

                if (enumtarget == EnumTargetKbn.Item)
                {
                    foreach (scenario_item dic in command.scenario_item_table)
                    {
                        if (dic.scode == list.scode)
                        {
                            Up_flg = true;
                        }
                    }
                }
                else if (enumtarget == EnumTargetKbn.ExcludingItem)
                {
                    foreach (scenario_item dic in command.scenarioexcluded_item_table)
                    {
                        if (dic.scode == list.scode)
                        {
                            Up_flg = true;
                        }
                    }
                }

                if (!Up_flg)
                {
                    criteria = GetCriteria(command, enumtarget);
                    criteria.id = list.id;
                    repository.Delete(criteria);
                }
            }
        }
    }
}