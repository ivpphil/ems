﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Campaign.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using ersAdmin.Models;
using jp.co.ivp.ers.mvc;
 

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class ValidateCampaignModifyDetailListRecord : IValidationHandler<ICampaignModifyDetailListRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICampaignModifyDetailListRecordCommand command)
        {
            yield return command.CheckRequired("inc_scode");
            yield return command.CheckRequired("inc_amount");
            yield return command.CheckRequired("inc_duplicate");
        }
    }
}