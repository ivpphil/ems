﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using ersAdmin.Models.campaign;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class ValidateCampaignModify : IValidationHandler<ICampaignModifyCommand>
    {
        /// <summary>
        /// フィールドチェック後の複合チェック
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ICampaignModifyCommand command)
        {
            if (command.detail_table != null)
            {
                var listScode = new List<string>();

                foreach (Campaign_modify_detail listDetail in command.detail_table)
                {
                    listDetail.lineNumber = command.detail_table.IndexOf(listDetail) + 1;
                    listDetail.mode = command.mode;
                    listDetail.AddInvalidField(command.controller.commandBus.Validate<ICampaignModifyDetailListRecordCommand>(listDetail));

                    //Check if There are duplicate records in [Items]
                    if (listScode.Contains(listDetail.inc_scode))
                    {
                        listDetail.AddInvalidField(new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("scode"), listDetail.inc_scode), new[] { "inc_scode" }));
                    }
                    else
                    {
                        listScode.Add(listDetail.inc_scode);
                    }

                    if (!listDetail.IsValid)
                    {
                        foreach (var errorMessage in listDetail.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "detail_table" });
                        }
                    }
                }
            }

            if (command.target_detail_table != null)
            {
                var listScode = new List<string>();

                foreach (Campaign_modify_target_s_list listtarget in command.target_detail_table)
                {
                    listtarget.lineNumber = command.target_detail_table.IndexOf(listtarget) + 1;

                    listtarget.AddInvalidField(command.controller.commandBus.Validate<ICampaignModifyTargetListRecordCommand>(listtarget));

                    //Check if There are duplicate records in [Items]
                    if (listScode.Contains(listtarget.target_scode))
                    {
                        listtarget.AddInvalidField(new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("scode"), listtarget.target_scode), new[] { "target_scode" }));
                    }
                    else
                    {
                        listScode.Add(listtarget.target_scode);
                    }

                    if (!listtarget.IsValid)
                    {
                        foreach (var errorMessage in listtarget.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "target_detail_table" });
                        }
                    }
                }
            }

            yield return command.CheckRequired("ccode");
            yield return command.CheckRequired("active");
            yield return command.CheckRequired("campaign_name");
            yield return command.CheckRequired("term_from");
            yield return command.CheckRequired("term_to");
            var checkDateFromToStgy = ErsFactory.ersCommonFactory.GetCheckDateFromToStgy();
            foreach (var result in checkDateFromToStgy.CheckDate("term_from", command.term_from, "term_to", command.term_to))
            {
                yield return result;
            }


            var repo = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();
            var criteria = ErsFactory.ersDocBundleFactory.GetErsCampaignCriteria();
            if (command.ccode != null)
            {
                criteria.ccode = command.ccode;

                if (command.mode == "modify")
                {
                    criteria.id_not_equal = command.id;
                }
            }
            if (repo.GetRecordCount(criteria) != 0)
            {
                yield return new ValidationResult(
                                ErsResources.GetMessage("10103", ErsResources.GetFieldName("ccode"), command.ccode),
                                new[] { "ccode" });
            }

            yield return command.CheckRequired("target_id");

            if (command.detail_table != null)
            {
                if (command.detail_table.Count == 0)
                {
                    yield return new ValidationResult(
                                    ErsResources.GetMessage("10206", ErsResources.GetFieldName("included_product")));
                }
            }
        }
    }
}