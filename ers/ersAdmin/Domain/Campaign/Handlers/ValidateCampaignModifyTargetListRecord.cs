﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Campaign.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Campaign.Handlers
{
    public class ValidateCampaignModifyTargetListRecord
        : IValidationHandler<ICampaignModifyTargetListRecordCommand>
    {
        /// <summary>
        /// validate target_scode and target_s_sale_ptn
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ICampaignModifyTargetListRecordCommand command)
        {
            if (command.IsEmpty())
            {
                yield break;
            }

            yield return command.CheckRequired("target_scode");
            yield return command.CheckRequired("target_order_type");
        }

    }
}