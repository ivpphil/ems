﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using ersAdmin.Models.campaign;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Campaign.Commands
{
    public interface ITargetRegistCommand : ISiteRegisterBaseCommand, ICommand
    {
        string target_name { get; }
        int? recency_from { get; }
        int? recency_to { get; }
        int? frequency_from { get; }
        int? frequency_to { get; }
        int? monetary_from { get; }
        int? monetary_to { get; }
        EnumOrderPattern? order_ptn_kbn { get; }
        string mode { get; set; }
        int? id { get; set; }

        List<scenario_item> scenario_item_table { get; set; }

        List<scenario_item> scenarioexcluded_item_table { get; set; }
        int? target_item_max { get; set; }
        int? targetexcluded_item_max { get; set; }
    }
}