﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.campaign;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Campaign.Commands
{
    public interface ICampaignRegistCommand : ISiteRegisterBaseCommand, ICommand
    {
        int? id { set; }

        string ccode { get; set; }

        string scode { get; }

        int? campaign_price { get; }

        List<Campaign_modify_detail> detail_table { get; }

        List<Campaign_modify_target_s_list> target_detail_table { get; }

        string mode { get; }

        DateTime? term_from { get; }
        DateTime? term_to { get; }
    }
}