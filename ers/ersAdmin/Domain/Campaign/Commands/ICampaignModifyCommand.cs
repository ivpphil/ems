﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.campaign;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Campaign.Commands
{
    public interface ICampaignModifyCommand : ISiteRegisterBaseCommand, ICommand
    {
        
        List<Campaign_modify_detail> detail_table { get; set; }

        List<Campaign_modify_target_s_list> target_detail_table { get; set; }

        string ccode { get; }

        string mode { get; }

        int? id { get; }

        string scode { get; }

        DateTime? utime { get; set; }

        DateTime? intime { get; set; }

        DateTime? term_from { get; }
        DateTime? term_to { get; }
    }
}