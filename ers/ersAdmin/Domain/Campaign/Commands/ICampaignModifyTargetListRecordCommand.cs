﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Campaign.Commands
{
    public interface ICampaignModifyTargetListRecordCommand : ICommand
    {
        bool IsEmpty();
        string mode { get; set; }
        string target_scode { get; set; }
        string target_sname { get; set; }
        EnumOrderType? target_order_type { get; set; }
    }
}