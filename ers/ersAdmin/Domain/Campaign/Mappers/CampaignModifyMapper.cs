﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Campaign.Mappables;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.campaign;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Campaign.Mappers
{
    public class CampaignModifyMapper : IMapper<ICampaignModifyMappable>
    {

        public void Map(ICampaignModifyMappable objMappable)
        {
            this.SetCampaignWithId(objMappable);
        }

        /// <summary>
        /// 表示時値セット
        /// </summary>
        internal void SetCampaignWithId(ICampaignModifyMappable objMappable)
        {
            var campaign = ErsFactory.ersDocBundleFactory.getErsCampaignWithId(objMappable.id);

            if (campaign == null)
                throw new ErsException("30300");

            objMappable.OverwriteWithParameter(campaign.GetPropertiesAsDictionary());
            objMappable.site_id = campaign.site_id;

            var marchandise = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(objMappable.scode);
            if (marchandise != null)
            {
                objMappable.sname = marchandise.sname;
            }

            var Doc_Repo = ErsFactory.ersDocBundleFactory.GetErsDocBundlingRepository();
            var doc_criteria = ErsFactory.ersDocBundleFactory.GetErsDocBundlingCriteria();
            doc_criteria.ccode = objMappable.ccode;
            doc_criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var doc_boundling = Doc_Repo.Find(doc_criteria);

            //同梱設定
            Campaign_modify_detail tmpdic = new Campaign_modify_detail();
            objMappable.detail_table = new List<Campaign_modify_detail>();
            objMappable.included_item_max = doc_boundling.Count;
            foreach (var list in doc_boundling)
            {
                tmpdic = new Campaign_modify_detail();
                tmpdic.inc_scode = list.scode;
                marchandise = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(tmpdic.inc_scode);
                if (marchandise != null)
                {
                    tmpdic.inc_sname = marchandise.sname;
                }
                tmpdic.inc_amount = Convert.ToInt16(list.amount);
                tmpdic.inc_duplicate = list.duplicate;
                objMappable.detail_table.Add(tmpdic);

            }
            //対象商品設定
            var Doc_target = ErsFactory.ersDocBundleFactory.GetErsDocTargetRepository();
            var doc_tg_criteria = ErsFactory.ersDocBundleFactory.GetErsDocTargetCriteria();
            doc_tg_criteria.ccode = objMappable.ccode;
            doc_tg_criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var doc_target = Doc_target.Find(doc_tg_criteria);

            Campaign_modify_target_s_list tmptgs = new Campaign_modify_target_s_list();
            objMappable.target_detail_table = new List<Campaign_modify_target_s_list>();
            objMappable.target_item_max = doc_target.Count;
            foreach (var list in doc_target)
            {
                tmptgs = new Campaign_modify_target_s_list();
                tmptgs.target_scode = list.scode;
                marchandise = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(tmptgs.target_scode);
                if (marchandise != null)
                {
                    tmptgs.target_sname = marchandise.sname;
                }
                tmptgs.target_order_type = list.order_type;
                objMappable.target_detail_table.Add(tmptgs);

            }
        }
    }
}