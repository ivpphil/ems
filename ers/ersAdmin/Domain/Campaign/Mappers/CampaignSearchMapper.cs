﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Campaign.Mappables;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using jp.co.ivp.ers.doc_bundle;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Campaign.Mappers
{
    public class CampaignSearchMapper : SiteSearchBaseMapper, IMapper<ICampaignSearchMappable>
    {
        public void Map(ICampaignSearchMappable objMappable)
        {
            Init(objMappable);
        }

        public virtual void Init(ICampaignSearchMappable objMappable)
        {
            ErsCampaignRepository repository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();

            //検索条件をクライテリアに保存
            var cpCri = this.SetCriteria(objMappable);

            //検索結果の総数を取得
            objMappable.recordCount = repository.GetRecordCount(cpCri);

            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            //検索SQLにLIMIT と OFFSETを加える
            objMappable.pager.SetLimitAndOffsetToCriteria(cpCri);

            cpCri.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);

            //出力結果
            var list = repository.Find(cpCri);

            //表示項目を設定
            foreach (ErsCampaign target in list)
            {
                target.w_active = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Active, EnumCommonNameColumnName.namename, Convert.ToInt32(target.active));
            }

            objMappable.CampaignList = ErsCommon.ConvertEntityListToDictionaryList(list);

        }

        /// <summary>
        /// 検索実体
        /// </summary>
        /// <returns></returns>
        protected virtual ErsCampaignCriteria SetCriteria(ICampaignSearchMappable objMappable)
        {
            ErsCampaignCriteria crtCampaign = ErsFactory.ersDocBundleFactory.GetErsCampaignCriteria();

            // 同梱番号
            if (!String.IsNullOrEmpty(objMappable.s_ccode))
            {
                crtCampaign.ccode = objMappable.s_ccode;
            }
            //ステータス
            if (objMappable.s_active != null)
            {
                crtCampaign.active = objMappable.s_active;
            }
            //同梱名
            if (!String.IsNullOrEmpty(objMappable.s_campaign_name))
            {
                crtCampaign.campaign_name_like = objMappable.s_campaign_name;
            }

            // 実施期間
            if (objMappable.s_term_from != null)
            {
                crtCampaign.term_from = DateTime.Parse(objMappable.s_term_from.Value.ToString("yyyy/MM/dd 00:00:00"));
            }
            if (objMappable.s_term_to != null)
            {
                crtCampaign.term_to = DateTime.Parse(objMappable.s_term_to.Value.ToString("yyyy/MM/dd 23:59:59"));
            }

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, crtCampaign, "campaign_t");

            return crtCampaign;

        }
    }
}