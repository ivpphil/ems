﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Campaign.Mappables;
using ersAdmin.Domain.Campaign.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.Models.campaign;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Campaign.Mappers
{
    public class TargetModifyMapper 
        : IMapper<ITargetModifyMappable>
    {

        public void Map(ITargetModifyMappable objMappable)
        {
            if (!objMappable.IsErrorBack)
            {
                SetTargetWithId(objMappable);
            }

            this.ReloadItems(objMappable);
        }

        /// <summary>
        /// 表示時値セット
        /// </summary>
        public void SetTargetWithId(ITargetModifyMappable objMappable)
        {
            var target = ErsFactory.ersTargetFactory.GetErsTargetWithId(objMappable.id);

            if (target == null)
                throw new ErsException("30300");

            objMappable.OverwriteWithParameter(target.GetPropertiesAsDictionary());
            objMappable.site_id = target.site_id;
            objMappable.recency_to = (target.recency_to == ushort.MaxValue) ? null : target.recency_to;
            objMappable.frequency_to = (target.frequency_to == int.MaxValue) ? null : target.frequency_to;
            objMappable.monetary_to = (target.monetary_to == int.MaxValue) ? null : target.monetary_to;

            this.LoadItemsIncluded(objMappable);
            this.LoadItemsExcluded(objMappable);

        }

        /// <summary>
        /// Loads included items of a step mail scenario
        /// </summary>
        internal void LoadItemsIncluded(ITargetModifyMappable objMappable)
        {
            //対象商品設定
            var target_item_repo = ErsFactory.ersTargetFactory.GetErsTargetItemRepository();
            var target_item_criteria = ErsFactory.ersTargetFactory.GetErsTargetItemCriteria();
            target_item_criteria.target_id = objMappable.id;
            target_item_criteria.target_kbn = EnumTargetKbn.Item;
            var record = target_item_repo.Find(target_item_criteria);

            scenario_item scenario_items = new scenario_item();
            objMappable.scenario_item_table = new List<scenario_item>();

            objMappable.target_item_max = record.Count();

            foreach (var list in record)
            {
                scenario_items = new scenario_item();
                scenario_items.id = list.id;
                scenario_items.scode = list.scode;
                scenario_items.gcode = list.gcode;
                scenario_items.target_kbn = list.target_kbn;
                scenario_items.order_type = list.order_type;
                scenario_items.sname = this.getSname(list.scode);

                objMappable.scenario_item_table.Add(scenario_items);
            }
        }

        /// <summary>
        /// Loads excluded items of a step mail scenario
        /// </summary>
        internal void LoadItemsExcluded(ITargetModifyMappable objMappable)
        {
            //対象商品設定
            var target_item_repo = ErsFactory.ersTargetFactory.GetErsTargetItemRepository();
            var target_item_criteria = ErsFactory.ersTargetFactory.GetErsTargetItemCriteria();
            target_item_criteria.target_id = objMappable.id.Value;
            target_item_criteria.target_kbn = EnumTargetKbn.ExcludingItem;
            var record = target_item_repo.Find(target_item_criteria);

            scenario_item scenario_items = new scenario_item();
            objMappable.scenarioexcluded_item_table = new List<scenario_item>();

            objMappable.targetexcluded_item_max = record.Count();

            foreach (var list in record)
            {
                scenario_items = new scenario_item();
                scenario_items.id = list.id;
                scenario_items.scode = list.scode;
                scenario_items.gcode = list.gcode;
                scenario_items.target_kbn = list.target_kbn;
                scenario_items.order_type = list.order_type;
                scenario_items.sname = this.getSname(list.scode);

                objMappable.scenarioexcluded_item_table.Add(scenario_items);
            }
        }

        /// <summary>
        /// Gets scode name
        /// </summary>
        /// <param name="value">scode of item</param>
        /// <returns>returns scode name</returns>
        internal string getSname(string scode)
        {
            var emRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var emCri = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            emCri.scode = scode;

            var list = emRepository.Find(emCri);
            if (list.Count > 0)
            {
                return list.First().sname;
            }

            return null;
        }

        internal void ReloadItems(ITargetModifyMappable objMappable)
        {
            if (objMappable.scenario_item_table != null)
            {
                foreach (var dr in objMappable.scenario_item_table)
                {
                    dr.disp_order_type = this.getOrderType(dr.scode);
                }
            }

            if (objMappable.scenarioexcluded_item_table != null)
            {
                foreach (var dr in objMappable.scenarioexcluded_item_table)
                {
                    dr.disp_order_type = this.getOrderType(dr.scode);
                }
            }
        }

        /// <summary>
        /// Gets order type
        /// </summary>
        /// <param name="value">scode of item</param>
        /// <returns>returns scode name</returns>
        internal EnumOrderType? getOrderType(string value)
        {
            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var groupCri = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            groupCri.scode = value;

            if (groupRepository.GetRecordCountSkuBase(groupCri) == 0)
            {
                return EnumOrderType.BothUsuallyAndSubscription;
            }

            var list = groupRepository.FindSkuBaseItemList(groupCri, null).First();
            if (list.s_sale_ptn == EnumSalePatternType.ALL)
            {
                return EnumOrderType.BothUsuallyAndSubscription;
            }

            return (EnumOrderType?)(int?)list.s_sale_ptn;
        }
    }
}