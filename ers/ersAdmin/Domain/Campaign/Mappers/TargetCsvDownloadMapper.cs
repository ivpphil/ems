﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Campaign.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Campaign.Mappers
{
    public class TargetCsvDownloadMapper
        : IMapper<ITargetCsvDownloadMappable>
    {
        public void Map(ITargetCsvDownloadMappable objMappable)
        {
            this.Init(objMappable);
        }

        protected virtual void Init(ITargetCsvDownloadMappable objMappable)
        {
            var target = ErsFactory.ersTargetFactory.GetErsTargetWithId(objMappable.id);

            if (target == null)
                throw new ErsException("10200");

            
            var memberRepository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var buildCriteriaStgy = ErsFactory.ersTargetFactory.GetBuildTargetMemberCriteriaStgy();

            var listTargetItem = ErsFactory.ersTargetFactory.GetObtainTargetItemListStgy().Obtain(target.id);

            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            criteria.deleted = EnumDeleted.NotDeleted;
            criteria.ignoreMonitor();
            criteria.active = EnumActive.Active;
            buildCriteriaStgy.Build(target, criteria, DateTime.Now, listTargetItem, null);

            criteria.SetOrderByMcode(Criteria.OrderBy.ORDER_BY_DESC);

            var memberList = memberRepository.Find(criteria);

            //Create download file
            this.CreateCsvFile(objMappable, memberList);

        }

        protected virtual void CreateCsvFile(ITargetCsvDownloadMappable objMappable, IList<ErsMember> memberList)
        {
            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            ersAdmin.Models.csv.target_csv_record item_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<ersAdmin.Models.csv.target_csv_record>(writer);
                foreach (var item in memberList)
                {
                    item_csv = new ersAdmin.Models.csv.target_csv_record();
                    item_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                    if (item.birth != null)
                    {
                        item_csv.birth = item.birth.Value.ToString("yyyy/MM/dd");
                    }
 
                    //購入金額合計設定
                    item_csv.sale = item.sale;

                    objMappable.csvCreater.WriteBody(item_csv, writer);
                }
            }
        }
    }
}