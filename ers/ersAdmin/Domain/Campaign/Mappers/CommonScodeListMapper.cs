﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Domain.Campaign.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Campaign.Mappers
{
    public class CommonScodeListMapper : IMapper<ICommonScodeListMappable>
    {
        public void Map(ICommonScodeListMappable objMappable)
        {
            Init(objMappable);
        }

        /// <summary>
        /// itemリストを初期化する。
        /// </summary>
        /// <param name="pager"></param>
        internal void Init(ICommonScodeListMappable objMappable)
        {
            //search s_master_t

            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

            //検索条件をクライテリアに保存
            var emCri = this.GetCriteria(objMappable);

            //検索結果の総数を取得
            objMappable.recordCount = groupRepository.GetRecordCountSkuBase(emCri);

            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            emCri.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
            emCri.SetOrderByGcode(Criteria.OrderBy.ORDER_BY_ASC);

            //検索SQLにLIMIT と OFFSETを加える
            objMappable.pager.SetLimitAndOffsetToCriteria(emCri);

            //商品出力結果
            var list = groupRepository.FindSkuBaseItemList(emCri,null);
            objMappable.MeList = ErsCommon.ConvertEntityListToDictionaryList(list);

        }

        /// <summary>
        /// 検索条件をクライテリアにセット
        /// </summary>
        /// <returns></returns>
        public ErsSkuCriteria GetCriteria(ICommonScodeListMappable objMappable)
        {
            var emCri = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //除外するgcode
            emCri.ignore_gcode = setup.IgnoreGcode;

            //set parameters
            if (!string.IsNullOrEmpty(objMappable.s_gcode))
                emCri.gcode = objMappable.s_gcode;

            if (!string.IsNullOrEmpty(objMappable.s_scode))
                emCri.scode = objMappable.s_scode;

            if (!string.IsNullOrEmpty(objMappable.s_sname))
                emCri.sname_and_gname = objMappable.s_sname;

            if (objMappable.s_cate1 != null)
                emCri.cate1 = objMappable.s_cate1.Value;

            if (objMappable.s_cate2 != null)
                emCri.cate2 = objMappable.s_cate2.Value;

            if (objMappable.s_cate3 != null)
                emCri.cate3 = objMappable.s_cate3.Value;

            if (objMappable.s_cate4 != null)
                emCri.cate4 = objMappable.s_cate4.Value;

            if (objMappable.s_cate5 != null)
                emCri.cate5 = objMappable.s_cate5.Value;

            emCri.doc_bundling_flg = objMappable.s_doc_bundling_flg;

            emCri.scode_not_equal = null;

            return emCri;
        }
    }
}