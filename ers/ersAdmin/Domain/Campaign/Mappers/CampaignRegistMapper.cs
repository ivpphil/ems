﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Campaign.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Campaign.Mappers
{
    public class CampaignRegistMapper : IMapper<ICampaignRegistMappable>
    {
        public void Map(ICampaignRegistMappable objMappable)
        {
            this.init(objMappable);
        }

        public void init(ICampaignRegistMappable objMappable)
        {
            objMappable.included_item_max = 0;
            objMappable.target_item_max = 0;
            objMappable.trigger_item_max = 0;
        }
    }
}