﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Campaign.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using ersAdmin.Models.csv;
using jp.co.ivp.ers.target;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Campaign.Mappers
{
    public class TargetSearchMapper : SiteSearchBaseMapper, IMapper<ITargetSearchMappable>
    {
        public void Map(ITargetSearchMappable objMappable)
        {
            this.Init(objMappable);
        }

        /// <summary>
        /// 検索実体
        /// </summary>
        /// <returns></returns>
        protected virtual ErsTargetCriteria SetCriteria(ITargetSearchMappable objMappable)
        {
            ErsTargetCriteria crtTarget = ErsFactory.ersTargetFactory.GetErsTargetCriteria();

            // 登録期間
            if (objMappable.s_intime_from != null)
            {
                crtTarget.intime_from = DateTime.Parse(objMappable.s_intime_from.Value.ToString("yyyy/MM/dd 00:00:00"));
            }
            if (objMappable.s_intime_to != null)
            {
                crtTarget.intime_to = DateTime.Parse(objMappable.s_intime_to.Value.ToString("yyyy/MM/dd 23:59:59"));
            }

            // ターゲット名称
            if (!String.IsNullOrEmpty(objMappable.s_target_name))
            {
                crtTarget.target_name_like = objMappable.s_target_name;
            }


            // 最新購買日
            if (objMappable.s_recency_from != null)
            {
                crtTarget.recency_from = Convert.ToInt32(objMappable.s_recency_from);
            }
            if (objMappable.s_recency_to != null)
            {
                crtTarget.recency_to = Convert.ToInt32(objMappable.s_recency_to);
            }

            // 累計購買数
            if (objMappable.s_frequency_from != null)
            {
                crtTarget.frequency_from = Convert.ToInt32(objMappable.s_frequency_from);
            }
            if (objMappable.s_frequency_to != null)
            {
                crtTarget.frequency_to = Convert.ToInt32(objMappable.s_frequency_to);
            }

            // 累計購買金額
            if (objMappable.s_monetary_from != null)
            {
                crtTarget.monetary_from = Convert.ToInt32(objMappable.s_monetary_from);
            }
            if (objMappable.s_monetary_to != null)
            {
                crtTarget.monetary_to = Convert.ToInt32(objMappable.s_monetary_to);
            }

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, crtTarget, "target_t");

            return crtTarget;

        }

        /// <summary>
        /// リストを初期化する。
        /// </summary>
        /// <param name="pager"></param>
        internal virtual void Init(ITargetSearchMappable objMappable)
        {
            ErsTargetRepository repository = ErsFactory.ersTargetFactory.GetErsTargetRepository();

            //検索条件をクライテリアに保存
            var tgCri = this.SetCriteria(objMappable);

            //検索結果の総数を取得
            objMappable.recordCount = repository.GetRecordCount(tgCri);

            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            //検索SQLにLIMIT と OFFSETを加える
            objMappable.pager.SetLimitAndOffsetToCriteria(tgCri);

            tgCri.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);

            //出力結果
            var list = repository.Find(tgCri);

            var memberRepository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var buildCriteriaStgy = ErsFactory.ersTargetFactory.GetBuildTargetMemberCriteriaStgy();

            var targetList = new List<Dictionary<string, object>>();
            //件数を設定
            foreach (ErsTarget target in list)
            {
                var dic = target.GetPropertiesAsDictionary();

                dic["w_purchase_course"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.OrderType, EnumCommonNameColumnName.namename, (int?)target.order_ptn_kbn);
                dic["recency_to"] = (target.recency_to == ushort.MaxValue) ? null : target.recency_to;
                dic["frequency_to"] = (target.frequency_to == int.MaxValue) ? null : target.frequency_to;
                dic["monetary_to"] = (target.monetary_to == int.MaxValue) ? null : target.monetary_to;

                var listTargetItem = ErsFactory.ersTargetFactory.GetObtainTargetItemListStgy().Obtain(target.id);

                var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
                criteria.deleted = EnumDeleted.NotDeleted;
                criteria.ignoreMonitor();
                criteria.active = EnumActive.Active;
                buildCriteriaStgy.Build(target, criteria, DateTime.Now, listTargetItem, null);

                var d_count = memberRepository.GetRecordCount(criteria);
                dic["list_count"] = d_count;

                targetList.Add(dic);
            }

            objMappable.TargetList = targetList;
        }
    }
}