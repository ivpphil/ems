﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Customer.Commands
{
    public interface IMemberRankSetupCommand
        : ISiteRegisterBaseCommand, ICommand
    {
        List<member_rank_setup_detail> detail_table { get; }

        EnumMemberRankCriterion? member_rank_criterion { get; }

        int? member_rank_term { get; }
    }
}