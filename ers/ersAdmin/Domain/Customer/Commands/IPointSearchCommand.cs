﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersAdmin.Domain.Customer.Commands
{
    public interface IPointSearchCommand : ICommand
    {

        string mcode { get; }
        DateTime? s_date_f { get; }
        DateTime? s_date_t { get; }
    }
}