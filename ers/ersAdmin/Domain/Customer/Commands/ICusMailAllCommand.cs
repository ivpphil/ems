﻿using System.Collections.Generic;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Customer.Commands
{
    public interface ICusMailAllCommand : ICommand
    {

        short? makeListRadio { set; get; }

        int? sid { set; get; }

        IList<ErsMember> memberList { get; }

    }
}