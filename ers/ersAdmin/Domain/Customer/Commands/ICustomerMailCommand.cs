﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Customer.Commands
{
    public interface ICustomerMailCommand : ICommand
    {

        bool IsSendMail { get; }

        bool IsLoadDefaultValue { get; }

    }
}