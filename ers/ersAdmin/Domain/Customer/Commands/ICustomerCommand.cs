﻿using System;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Customer.Commands
{
    public interface ICustomerCommand : ICommand
    {

        bool IsModifyCompletionPage { get; }

        int? pref { get; set; }

        EnumMformat? mformat { get; set; }

        int? ques { get; set; }

        short job { get; set; }

        EnumSex? sex { get; set; }

        int? birthday_y { get; set; }

        int? birthday_m { get; set; }

        int? birthday_d { get; set; }

        string passwd { get; set; }

        string passwd_confirm { get; set; }

        string email { get; set; }

        string email_confirm { get; set; }

        string mcode { get; set; }

        DateTime? birth { get; }

        EnumPmFlg? pm_flg { get; set; }

        string zip { get; set; }

        int? login_try_count { get; set; }
    }
}