﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Customer.Commands
{
    public interface ICustomerSearchCommand
        : ICommand
    {
        int? src_age_f { get; }
        int? src_age_t { get; }
        int? src_point_f { get; }
        int? src_point_t { get; }
        DateTime? src_regdate_f { get; }

        DateTime? src_regdate_t { get; }
    }
}