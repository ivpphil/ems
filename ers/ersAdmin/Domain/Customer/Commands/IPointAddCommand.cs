﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Customer.Commands
{
    public interface IPointAddCommand : ISiteRegisterBaseCommand, ICommand
    {

        bool IsCusPointAdd { get; }

        string mcode { get; }

        int? add_point { get; set; }
        
        string add_reason { get; set; }

    }
}