﻿using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Customer.Handlers
{
    public class CustomerHandler : ICommandHandler<ICustomerCommand>
    {

        public ICommandResult Submit(ICustomerCommand command)
        {
            if(command.IsModifyCompletionPage)
                this.Update(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// 顧客情報更新処理
        /// </summary>
        internal void Update(ICustomerCommand command)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();

            //Modelの情報をディクショナリに格納
            var new_member = ErsFactory.ersMemberFactory.getErsMemberWithMcodeForAdmin(command.mcode, true);
            new_member.OverwriteWithModel(command);
            var age = ErsFactory.ersMemberFactory.GetGetAgeStgy().GetAge(command.birth);
            new_member.age_code = ErsFactory.ersMemberFactory.GetGetAgeCodeStgy().GetAgeCode(age);
            new_member.ques = new_member.ques ?? 0;

            //変更前データ取得
            var old_member = ErsFactory.ersMemberFactory.getErsMemberWithMcodeForAdmin(command.mcode, true);

            if (ErsFactory.ersUtilityFactory.getSetup().member_centralization)
            {
                new_member.site_id = (int)EnumSiteId.COMMON_SITE_ID;
            }
            else
            {
                new_member.site_id = old_member.site_id;
            }

            if (string.IsNullOrEmpty(command.passwd) && string.IsNullOrEmpty(command.passwd_confirm))
            {
                //未入力の場合は、DBの値を使用する。
                new_member.passwd = old_member.passwd;
            }

            //顧客情報更新処理
            repository.Update(old_member, new_member);
        }

    }
}