﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Customer.Handlers
{
    public class ValidateCustomer : IValidationHandler<ICustomerCommand>
    {

        public IEnumerable<ValidationResult> Validate(ICustomerCommand command)
        {
            yield return command.CheckRequired("mcode");
            yield return command.CheckRequired("lname");
            yield return command.CheckRequired("fname");
            yield return command.CheckRequired("lnamek");
            yield return command.CheckRequired("fnamek");
            yield return command.CheckRequired("zip");
            yield return command.CheckRequired("pref");
            foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("pref", command.pref, false))
            {
                yield return result;
            }
            if (command.IsValidField("zip", "pref"))
            {
                yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("zip", command.zip, "pref", command.pref, false);
            }

            yield return command.CheckRequired("address");
            yield return command.CheckRequired("taddress");

            yield return command.CheckRequired("tel");

            //These are not required when the user registered at CTS
            if (command.pm_flg != EnumPmFlg.CTS)
            {
                yield return command.CheckRequired("email");
                yield return command.CheckRequired("ques");
                foreach (var result in ErsFactory.ersMemberFactory.GetValidateQuesStgy().Validate(command.ques))
                {
                    yield return result;
                }
                yield return command.CheckRequired("ans");
            }

            yield return command.CheckRequired("mformat");
            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("mformat", EnumCommonNameType.MFormat, (int?)command.mformat);

            foreach (var result in ErsFactory.ersMemberFactory.GetValidateJobStgy().Validate(command.job))
            {
                yield return result;
            }

            yield return command.CheckRequired("sex");
            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("sex", EnumCommonNameType.Sex, (int?)command.sex);

            yield return command.CheckRequired("birthday_y");
            yield return command.CheckRequired("birthday_m");
            yield return command.CheckRequired("birthday_d");

            foreach (var result in ErsFactory.ersMemberFactory.GetValidateBirthdayStgy().Validate(command.birthday_y, command.birthday_m, command.birthday_d))
            {
                yield return result;
            }

            if (!string.IsNullOrEmpty(command.passwd) || !string.IsNullOrEmpty(command.passwd_confirm))
            {
                yield return ErsFactory.ersMemberFactory.GetCheckPasswdConfirmStgy().CheckPasswdConfirm(command.passwd, command.passwd_confirm);
            }

            yield return ErsFactory.ersMemberFactory.GetCheckEmailConfirmStgy().CheckEmailConfirm(command.email, command.email_confirm);

            yield return ErsFactory.ersMemberFactory.GetCheckEmailRequiredStgy().Check(command.mcode, command.email);
            yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailStgy().CheckDuplicate(command.mcode, command.email);

            if (command.login_try_count < 0)
            {
                yield return new ValidationResult(ErsResources.GetMessage("cus_detail.negativenumber", ErsResources.GetFieldName("login_try_count")));
            }



        }

    }
}