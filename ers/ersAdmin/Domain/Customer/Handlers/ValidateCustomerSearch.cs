﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Customer.Handlers
{
    public class ValidateCustomerSearch
        : IValidationHandler<ICustomerSearchCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ICustomerSearchCommand command)
        {
            foreach (var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDateTime("src_regdate_f", command.src_regdate_f, "src_regdate_t", command.src_regdate_t))
            {
                yield return result;
            }
            foreach (var result in ErsFactory.ersCommonFactory.GetCheckNumericFromToStgy().Check("src_age_f", command.src_age_f, "src_age_t", command.src_age_t))
            {
                yield return result;
            }
            foreach (var result in ErsFactory.ersCommonFactory.GetCheckNumericFromToStgy().Check("src_point_f", command.src_point_f, "src_point_t", command.src_point_t))
            {
                yield return result;
            }
        }
    }
}