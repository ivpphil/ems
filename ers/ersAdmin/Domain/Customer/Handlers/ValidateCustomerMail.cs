﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Customer.Handlers
{
    public class ValidateCustomerMail : IValidationHandler<ICustomerMailCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICustomerMailCommand command)
        {
            yield return command.CheckRequired("mcode");
            if (command.IsSendMail)
            {
                yield return command.CheckRequired("email");
                yield return command.CheckRequired("from_email");
                yield return command.CheckRequired("mail_title");
                yield return command.CheckRequired("mail_body");
            }
        }
    }
}