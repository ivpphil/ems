﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Customer.Handlers
{
    public class ValidateCusMailAll : IValidationHandler<ICusMailAllCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICusMailAllCommand command)
        {
            //リスト追加なら
            if (command.makeListRadio == 2)
            {

                if (command.sid == null)
                {
                    yield return command.CheckRequired("sid");
                }
                else
                {
                    //processが存在していない
                    yield return ErsFactory.ErsAtMailFactory.GetCheckProcessExistStgy().CheckProcessExist(command.sid);
                }
            }
        }
    }
}