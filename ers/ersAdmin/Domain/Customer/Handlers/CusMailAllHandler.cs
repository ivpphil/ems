﻿using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Customer.Handlers
{
    public class CusMailAllHandler : ICommandHandler<ICusMailAllCommand>
    {

        public ICommandResult Submit(ICusMailAllCommand command)
        {
            var mailToRegist = ErsFactory.ErsAtMailFactory.GetRegistMailToStgy();

            if (command.makeListRadio == 1)
            {
                mailToRegist.InsertNewMailList(EnumUpKind.MEMBER, command.memberList);
            }
            else
            {
                mailToRegist.AddMailList(command.sid.Value, command.memberList);
            }

            return new CommandResult(true);
        }

    }
}