﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Customer.Handlers
{
    public class ValidatePointSearch : IValidationHandler<IPointSearchCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPointSearchCommand command)
        {
            yield return command.CheckRequired("mcode");
            if (ErsFactory.ersUtilityFactory.getSetup().Multiple_sites)
            {
                yield return command.CheckRequired("sp_site_id");
            }

            foreach(var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDate("s_date_f", command.s_date_f, "s_date_t", command.s_date_t))
            {
                yield return result;
            }
        }
    }
}