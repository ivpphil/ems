﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Customer.Commands;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Models;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Customer.Handlers
{
    public class ValidateMemberRankSetup
        : IValidationHandler<IMemberRankSetupCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IMemberRankSetupCommand command)
        {
            if (command.multiple_sites)
            {
                yield return command.CheckRequired("site_id");
            }
            yield return command.CheckRequired("member_rank_criterion");
            yield return command.CheckRequired("member_rank_term");

            if (command.detail_table != null)
            {
                foreach (var model in command.detail_table)
                {
                    model.AddInvalidField(command.controller.commandBus.Validate<IMemberRankSetupDetailCommand>(model));

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "detail_table" });
                        }
                    }
                }

                yield return CheckDuplicatedRange(command);
            }
        }


        internal ValidationResult CheckDuplicatedRange(IMemberRankSetupCommand command)
        {
            var listComparisor = new List<member_rank_setup_detail>();
            listComparisor.AddRange(command.detail_table);

            foreach (var model in command.detail_table)
            {
                foreach (var comparisor in listComparisor)
                {
                    if (model.rank != comparisor.rank)
                    {
                        if (comparisor.value_from.HasValue && comparisor.value_to.HasValue)
                        {
                            if (comparisor.value_from >= comparisor.value_to)
                                return new ValidationResult(ErsResources.GetMessage("MRS0001"), new[] { "detail_table" });

                            if (IsDuplicatedRange(comparisor.value_from.Value, comparisor.value_to.Value, model.value_from)
                                || IsDuplicatedRange(comparisor.value_from.Value, comparisor.value_to.Value, model.value_to))
                                return new ValidationResult(ErsResources.GetMessage("MRS0001"), new[] { "detail_table" });
                        }
                    }

                }
            }

            return null;
        }

        internal bool IsDuplicatedRange(int comparisor_from, int comparisor_to, int? value)
        {
            if (value >= comparisor_from && value <= comparisor_to)
                return true;

            return false;
        }
    }
}