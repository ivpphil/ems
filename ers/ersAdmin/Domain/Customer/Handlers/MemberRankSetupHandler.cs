﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Customer.Handlers
{
    public class MemberRankSetupHandler
        : ICommandHandler<IMemberRankSetupCommand>
    {
        public ICommandResult Submit(IMemberRankSetupCommand command)
        {
            this.UpdateMemberRankSetup(command);
            this.UpdateErsSetup(command);

            return new CommandResult(true);
        }

        internal void UpdateMemberRankSetup(IMemberRankSetupCommand command)
        {
            if (command.detail_table != null)
            {
                foreach (var detail in command.detail_table)
                {
                    var repository = ErsFactory.ersMemberFactory.GetErsMemberRankSetupRepository();
                    var old_member_rank_setup = ErsFactory.ersMemberFactory.GetErsMemberRankSetupWithId(detail.id);
                    var new_member_rank_setup = ErsFactory.ersMemberFactory.getErsMemberRankSetupWithParameter(old_member_rank_setup.GetPropertiesAsDictionary());

                    new_member_rank_setup.rank_name = detail.rank_name;
                    new_member_rank_setup.value_from = detail.value_from;
                    new_member_rank_setup.value_to = detail.value_to;

                    if (checkIfWholeNumber((double)detail.point_magnification))
                    {
                        new_member_rank_setup.point_magnification = (int)((double)detail.point_magnification * 100);
                    }
                    else
                    {
                        double point_magnification = (double)detail.point_magnification * 100;
                        double intPointMag = (int)(point_magnification);
                        double objDouble = intPointMag / 100;
                        if (detail.point_magnification != objDouble)
                        {
                            point_magnification++;
                        }
                        new_member_rank_setup.point_magnification = (int)(point_magnification);
                    }

                    if (old_member_rank_setup != null && new_member_rank_setup != null)
                    {
                        repository.Update(old_member_rank_setup, new_member_rank_setup);
                    }
                }
            }
        }

        internal bool checkIfWholeNumber(double val)
        {
            var obj1 = (int)(val);

            if (obj1 == val)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        internal void UpdateErsSetup(IMemberRankSetupCommand command)
        {
            var repository = ErsFactory.ersUtilityFactory.GetErsSetupRepository();
            var criteria = ErsFactory.ersUtilityFactory.GetErsSetupCriteria();

            if (!ErsFactory.ersUtilityFactory.getSetup().member_rank_centralization)
            {
                if (!command.multiple_sites)
                {
                    command.site_id = command.config_site_id;
                }
                criteria.site_id_for_admin = Convert.ToInt32(command.site_id);
            }

            var list = repository.Find(criteria);

            foreach (var new_setup in list)
            {
                var old_setup = ErsFactory.ersUtilityFactory.GetErsSetupWithId(new_setup.id.Value);

                new_setup.member_rank_criterion = command.member_rank_criterion;
                new_setup.member_rank_term = command.member_rank_term;

                repository.Update(old_setup, new_setup);
            }
        }
    }
}