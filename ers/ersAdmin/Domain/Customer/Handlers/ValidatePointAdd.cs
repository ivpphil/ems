﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using System;

namespace ersAdmin.Domain.Customer.Handlers
{
    public class ValidatePointAdd : IValidationHandler<IPointAddCommand>
    {

        public IEnumerable<ValidationResult> Validate(IPointAddCommand command)
        {
            if (command.multiple_sites)
            {
                yield return command.CheckRequired("site_id");
            }
            yield return command.CheckRequired("mcode");
            yield return command.CheckRequired("add_point");
            yield return command.CheckRequired("add_reason");

            if (command.add_point == 0)
                yield return new ValidationResult(ErsResources.GetMessage("10033", ErsResources.GetFieldName("cuspoint_points_granted"), 0));

            var total = ErsFactory.ersMemberFactory.GetObtainMemberPointStgy().GetPoint(command.mcode,
                ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetPointSiteId(command.site_id));

            if (command.add_point < 0)
            {
                if (Math.Abs((Convert.ToDecimal(command.add_point))) > total)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("20203", ErsResources.GetFieldName("cuspoint_points_granted"), 0));
                }
            }
            if (command.IsValidField("add_point"))
            {
                yield return ErsFactory.ersMemberFactory.GetObtainMemberPointStgy().ValidateMaxPoints(total, command.add_point.Value);
            }
        }

    }
}