﻿using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using System;

namespace ersAdmin.Domain.Customer.Handlers
{
    public class PointAddHandler : ICommandHandler<IPointAddCommand>
    {
        public ICommandResult Submit(IPointAddCommand command)
        {
            if (command.IsCusPointAdd)
                this.CusPointAdd(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// 顧客ポイント追加登録処理
        /// </summary>
        internal void CusPointAdd(IPointAddCommand command)
        {
            var rpsPointHistory = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryRepository();

            //事前検索(mcodeだけで検索させる)
            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcodeForAdmin(command.mcode, true);

            var objPointHistory = ErsFactory.ersPointHistoryFactory.GetErsPointHistory();
            objPointHistory.last_p = ErsFactory.ersMemberFactory.GetObtainMemberPointStgy().GetPointNotCache(member.mcode,
                ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetPointSiteId(command.site_id));


            //登録日
            objPointHistory.dt = System.DateTime.Now;
            //会員コード
            objPointHistory.mcode = command.mcode;
            //追加ポイント
            objPointHistory.now_p = command.add_point.Value;
            //理由
            objPointHistory.reason = command.add_reason;
            //合計結果ポイント
            objPointHistory.total_p = objPointHistory.last_p + objPointHistory.now_p;

            if (!command.multiple_sites)
            {
                command.site_id = command.config_site_id;
            }

            objPointHistory.site_id = Convert.ToInt32(command.site_id);

            //レコード追加処理
            rpsPointHistory.Insert(objPointHistory, true);

        }
    }
}