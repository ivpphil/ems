﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Customer.Commands;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Customer.Handlers
{
    public class ValidateMemberRankSetupDetail
        : IValidationHandler<IMemberRankSetupDetailCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IMemberRankSetupDetailCommand command)
        {
            yield return command.CheckRequired("id");
            yield return command.CheckRequired("rank");

            yield return command.CheckRequired("rank_name");
            yield return command.CheckRequired("value_from");
            yield return command.CheckRequired("value_to");
        }
    }
}