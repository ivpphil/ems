﻿using System;
using System.Collections.Generic;
using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Customer.Mappers
{
    public class PointSearchMapper : IMapper<IPointSearchMappable>
    {
        public void Map(IPointSearchMappable objMappable)
        {
            if (objMappable.IsCusPointSearch)
            {
                this.CusPointSearch(objMappable);
                this.InitializeMember(objMappable);
            }
        }

        /// <summary>
        /// 会員情報取得
        /// </summary>
        /// <param name="objMappable"></param>
        private void InitializeMember(IPointSearchMappable objMappable)
        {
            var objMember = ErsFactory.ersMemberFactory.getErsMemberWithMcodeForAdmin(objMappable.mcode);
            if (objMember == null)
            {
                return;
            }
            objMappable.deleted = objMember.deleted;
        }

        /// <summary>
        /// 顧客ポイントリスト取得
        /// </summary>
        internal void CusPointSearch(IPointSearchMappable objMappable)
        {
            var crtPointHistory = this.SetPointHistoryCriteria(objMappable);

            var rpsPointHistory = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryRepository();
            objMappable.search_result_cnt = rpsPointHistory.GetRecordCount(crtPointHistory);

            objMappable.PointHistoryList = this.GetPointList(crtPointHistory);
        }

        internal ErsPointHistoryCriteria SetPointHistoryCriteria(IPointSearchMappable objMappable)
        {
            ErsPointHistoryCriteria crtPointHistory = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryCriteria();
            // 会員コード
            crtPointHistory.mcode = objMappable.mcode;

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            crtPointHistory.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetPointSiteId(objMappable.sp_site_id);

            //登録日From to
            if (objMappable.s_date_f != null)
            {
                crtPointHistory.dt_from = Convert.ToDateTime(objMappable.s_date_f.Value.ToString("yyyy/MM/dd 00:00:00"));
            }
            if (objMappable.s_date_t != null)
            {
                crtPointHistory.dt_to = Convert.ToDateTime(objMappable.s_date_t.Value.ToString("yyyy/MM/dd 23:59:59"));
            }

            return crtPointHistory;
        }


        /// <summary>
        /// 会員リストを取得する
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        protected List<Dictionary<string, object>> GetPointList(ErsPointHistoryCriteria criteria)
        {
            criteria.SetOrderByDt(Criteria.OrderBy.ORDER_BY_DESC);

            var rpsPointHistory = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryRepository();

            //検索した結果を返す
            var list = rpsPointHistory.Find(criteria);

            var listPoint = new List<Dictionary<string, object>>();
            foreach (var objPoint in list)
            {
                var dictionary = objPoint.GetPropertiesAsDictionary();

                dictionary["display_type"] = this.GetDisplayType(objPoint);
                if (objPoint.d_no.HasValue())
                {
                    var order = ErsFactory.ersOrderFactory.GetOrderWithD_no(objPoint.d_no);
                    if (order != null)
                    {
                        dictionary["total"] = order.total;
                    }
                }

                listPoint.Add(dictionary);
            }

            return listPoint;
        }

        private EnumDisplayType GetDisplayType(global::jp.co.ivp.ers.member.ErsPointHistory objPoint)
        {
            if (objPoint.now_p < 0)
            {
                return EnumDisplayType.Use;
            }
            else
            {
                return EnumDisplayType.Add;
            }
        }

        public enum EnumDisplayType
        {
            /// <summary>
            /// ポイント使用
            /// </summary>
            Use,

            /// <summary>
            /// ポイント付与
            /// </summary>
            Add,
        }
    }
}