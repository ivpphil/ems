﻿using System;
using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersAdmin.Domain.Customer.Mappers
{
    public class CustomerCSVMapper
        : CustomerSearchMapper, IMapper<ICustomerCSVMappable>
    {
        public void Map(ICustomerCSVMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }

        /// <summary>
        /// CSVファイルを作成する。
        /// </summary>
        /// <returns></returns>
        internal virtual void CreateCsvFile(ICustomerCSVMappable objMappable)
        {

            objMappable.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();

            var rpsMember = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var crtMember = this.GetMemberCriteria(objMappable);

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(crtMember);
            }

            crtMember.SetOrderByMcode(Criteria.OrderBy.ORDER_BY_DESC);

            var listMember = rpsMember.Find(crtMember);

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            ersAdmin.Models.csv.Customer_csv customer_csv;

            var prefService = ErsFactory.ersViewServiceFactory.GetErsViewPrefService();
            var quesService = ErsFactory.ersViewServiceFactory.GetErsViewQuesService();
            var jobService = ErsFactory.ersViewServiceFactory.GetErsViewJobService();
            var countoryService = ErsFactory.ersViewServiceFactory.GetErsViewCountryService();
            var commonNameService = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService();

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<ersAdmin.Models.csv.Customer_csv>(writer);
                foreach (var item in listMember)
                {
                    customer_csv = new Models.csv.Customer_csv();
                    customer_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                    
                    if (customer_csv.pref != null)
                    {
                        customer_csv.disp_pref = prefService.GetStringFromId(Convert.ToInt16(customer_csv.pref), item.site_id);
                    }
                    if (customer_csv.sex != null && customer_csv.sex != 0 )
                    {
                        var msg_sex = commonNameService.GetStringFromId(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename, (int)customer_csv.sex.Value);                        
                        //if msg_sex not null, get resource message
                        if (msg_sex.HasValue())
                        {
                            customer_csv.disp_sex = ErsResources.GetMessage(msg_sex);
                        }
                    }
                    if (customer_csv.ques != null)
                    {
                        customer_csv.disp_ques = quesService.GetStringFromId(Convert.ToInt16(customer_csv.ques));
                    }
                    if (customer_csv.m_flg != null)
                    {
                        var msg_m_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.MFlg, EnumCommonNameColumnName.namename, (int)customer_csv.m_flg.Value);
                        //if msg_m_flg not null, get resource message
                        if (msg_m_flg.HasValue())
                        {
                            customer_csv.disp_m_flg = ErsResources.GetMessage(msg_m_flg);
                        }
                    }
                    if (customer_csv.job != null && customer_csv.job != 0)
                    {
                        customer_csv.disp_job = jobService.GetStringFromId(customer_csv.job);
                    }
                    if (customer_csv.country != null)
                    {
                        customer_csv.disp_country = countoryService.GetStringFromId(Convert.ToInt16(customer_csv.country));
                    }

                    if (customer_csv.rank != null)
                    {
                        customer_csv.disp_member_rank = ErsFactory.ersMemberFactory.GetObtainMemberRankNameStgy().Obtain(customer_csv.mcode, customer_csv.site_id.Value); 
                    }

                    if (customer_csv.dm_flg == EnumDmFlg.NoNeed || customer_csv.dm_flg == EnumDmFlg.Need ) {
                        var msg_dm_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.NEED, EnumCommonNameColumnName.namename, (int)customer_csv.dm_flg);
                        //if msg_dm_flg not null, get resource message
                        if (msg_dm_flg.HasValue())
                        {
                            customer_csv.disp_dm_flg = ErsResources.GetMessage(msg_dm_flg);
                        }
                    }

                    if (customer_csv.out_bound_flg == EnumOutBoundFlg.Need || customer_csv.out_bound_flg == EnumOutBoundFlg.NoNeed ) {
                        var msg_out_bound_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.NEED, EnumCommonNameColumnName.namename, (int)customer_csv.out_bound_flg);
                        //if msg_out_bound_flg not null, get resource message
                        if (msg_out_bound_flg.HasValue())
                        {
                            customer_csv.disp_out_bound_flg = ErsResources.GetMessage(msg_out_bound_flg);
                        }
                    }

                    customer_csv.age_code = 0;

                    if (item.birth != null)
                    {
                        customer_csv.birth = item.birth.Value.ToString("yyyy/MM/dd");

                        var age = ErsFactory.ersMemberFactory.GetGetAgeStgy().GetAge(Convert.ToDateTime(customer_csv.birth));
                        customer_csv.age_code = ErsFactory.ersMemberFactory.GetGetAgeCodeStgy().GetAgeCode(age);
                    }

                    var msg_age_code = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ORDAGE, EnumCommonNameColumnName.namename, customer_csv.age_code);
                    //if msg_age_code not null, get resource message
                    if (msg_age_code.HasValue())
                    {
                        customer_csv.disp_age_code = ErsResources.GetMessage(msg_age_code);
                    }
  
                    //購入金額合計設定
                    customer_csv.sale = item.sale;
                    
                    
                    objMappable.csvCreater.WriteBody(customer_csv, writer);
                }
            }
        }
    }
}