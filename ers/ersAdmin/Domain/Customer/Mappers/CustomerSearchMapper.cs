﻿using System;
using System.Collections.Generic;
using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Customer.Mappers
{
    public class CustomerSearchMapper : SiteSearchBaseMapper, IMapper<ICustomerSearchMappable>
    {

        public void Map(ICustomerSearchMappable objMappable)
        {
            this.SearchList(objMappable);
        }

        /// <summary>
        /// 伝票検索（通常）
        /// </summary>
        public void SearchList(ICustomerSearchMappable objMappable)
        {
            var memberRepo = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var memberCri = this.GetMemberCriteria(objMappable);

            //件数
            objMappable.recordCount = memberRepo.GetRecordCount(memberCri);
            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(memberCri);
            }

            memberCri.SetOrderByMcode(Criteria.OrderBy.ORDER_BY_DESC);

            //表示用リスト作成
            var listMember = memberRepo.Find(memberCri);

            var listCustomer = new List<Dictionary<string, object>>();

            foreach (var member in listMember)
            {
                var dictionary = member.GetPropertiesAsDictionary();
                dictionary["age"] = ErsFactory.ersMemberFactory.GetGetAgeStgy().GetAge(member.birth);
                dictionary["w_sex"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename, (int?)member.sex);
                listCustomer.Add(dictionary);
            }

            objMappable.list = listCustomer;

        }

        /// <summary>
        /// 顧客検索実体
        /// </summary>
        /// <returns></returns>
        protected ErsMemberCriteria GetMemberCriteria(ICustomerSearchMappable objMappable)
        {
            ErsMemberCriteria memberCri = ErsFactory.ersMemberFactory.GetErsMemberCriteria();

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, memberCri, "member_t");

            // メールアドレス
            if (!string.IsNullOrEmpty(objMappable.src_email))
            {
                memberCri.email = objMappable.src_email;
            }
            // 電話番号
            if (!string.IsNullOrEmpty(objMappable.src_tel))
            {
                memberCri.tel = objMappable.src_tel;
            }
            // 姓
            if (!string.IsNullOrEmpty(objMappable.src_lnamek))
            {
                memberCri.lnamek = objMappable.src_lnamek;
            }
            // 名
            if (!string.IsNullOrEmpty(objMappable.src_fnamek))
            {
                memberCri.fnamek = objMappable.src_fnamek;
            }
            // 企業名
            if (!string.IsNullOrEmpty(objMappable.src_compname))
            {
                memberCri.compname = objMappable.src_compname;
            }
            // メール配信区分
            if (objMappable.src_mailtype != null)
            {
                memberCri.m_flg = objMappable.src_mailtype.Value;
            }

            // 発行期間
            if (objMappable.src_regdate_f != null)
            {
                memberCri.regdate_f = DateTime.Parse(objMappable.src_regdate_f.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (objMappable.src_regdate_t != null)
            {
                memberCri.regdate_t = DateTime.Parse(objMappable.src_regdate_t.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            if (objMappable.src_age_f != null)
            {
                memberCri.age_f = objMappable.src_age_f.Value;
            }
            if (objMappable.src_age_t != null)
            {
                memberCri.age_t = objMappable.src_age_t.Value;
            }
            if (objMappable.src_sex != null)
            {
                memberCri.sex = objMappable.src_sex.Value;
            }
            if (objMappable.src_point_f != null && objMappable.src_point_t == null)
            {
                memberCri.point_from_for_admin = objMappable.src_point_f.Value;
            }
            else if (objMappable.src_point_f == null && objMappable.src_point_t != null)
            {
                memberCri.point_to_for_admin = objMappable.src_point_t.Value;
            }
            else if (objMappable.src_point_f != null && objMappable.src_point_t != null)
            {
                memberCri.point_between_for_admin(objMappable.src_point_f, objMappable.src_point_t);
            }

            if (objMappable.src_pref != null)
            {
                memberCri.pref = objMappable.src_pref.Value;
            }

            if (objMappable.src_age_code != null)
            {
                memberCri.age_code = objMappable.src_age_code.Value;
            }

            if (objMappable.src_dm_flg != null)
            {
                memberCri.dm_flg = objMappable.src_dm_flg.Value;
            }

            if (objMappable.src_out_bound_flg != null)
            {
                memberCri.out_bound_flg = objMappable.src_out_bound_flg.Value;
            }

            if (objMappable.src_deleted != EnumDeleted.Deleted)
            {
                memberCri.deleted = EnumDeleted.NotDeleted;
            }

            if (objMappable.src_member_rank != null)
            {
                memberCri.member_rank = objMappable.src_member_rank.Value;
            }

            memberCri.ignoreMonitor();

            return memberCri;
        }
    }
}