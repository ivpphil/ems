﻿using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Customer.Mappers
{
    public class CusMailAllMapper
        : CustomerSearchMapper, IMapper<ICusMailAllMappable>
    {

        public void Map(ICusMailAllMappable objMappable)
        {
            var memberCriteria = this.GetMemberCriteria(objMappable);
            var memberRepo = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            objMappable.memberList = memberRepo.Find(memberCriteria);
        }
    }
}