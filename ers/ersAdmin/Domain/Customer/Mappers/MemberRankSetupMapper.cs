﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.SiteBase.Mappers;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Customer.Mappers
{
    public class MemberRankSetupMapper
        : SiteRegisterBaseMapper, IMapper<IMemberRankSetupMappable>
    {
        public void Map(IMemberRankSetupMappable objMappable)
        {
            // 初期サイトIDセット [Set default site ID]
            if (!objMappable.multiple_sites)
            {
                objMappable.site_id = objMappable.config_site_id;
            }
            else
            {
                if (ErsFactory.ersUtilityFactory.getSetup().member_rank_centralization)
                {
                    objMappable.site_id = (int)EnumSiteId.COMMON_SITE_ID;
                }
                else
                {
                    this.SetDefaultSiteId(objMappable);
                }
            }

            if (objMappable.IsCompletePage)
            {
                this.LoadDefaultLabelDetail(objMappable);
                return;
            }

            this.LoadDefaultDataRank(objMappable);
            this.LoadDefaultDataDetail(objMappable);

        }

        internal void LoadDefaultDataRank(IMemberRankSetupMappable objMappable)
        {
            var repository = ErsFactory.ersUtilityFactory.GetErsSetupRepository();
            var setup = new ErsSetup();

            if (!ErsFactory.ersUtilityFactory.getSetup().member_rank_centralization)
            {
                setup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(objMappable.site_id));
            }
            else
            {
                setup = ErsFactory.ersUtilityFactory.GetErsSetupWithId(1);
            }

            if (setup == null)
            {
                throw new ErsException(ErsResources.GetMessage("10200"));
            }

            objMappable.OverwriteWithParameter(setup.GetPropertiesAsDictionary());
        }


        internal void LoadDefaultDataDetail(IMemberRankSetupMappable objMappable)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRankSetupRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberRankSetupCriteria();

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, criteria, "member_rank_setup_t");

            criteria.active = EnumActive.Active;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            var member_rank_setup_list = repository.Find(criteria);

            var new_detail_list = new List<member_rank_setup_detail>();
            //Load the details list.
            foreach (var detail in member_rank_setup_list)
            {
                var member_detail = new member_rank_setup_detail();
                member_detail.OverwriteWithParameter(detail.GetPropertiesAsDictionary());
                member_detail.point_magnification = member_detail.point_magnification / 100;
                new_detail_list.Add(member_detail);
            }

            objMappable.detail_table = new_detail_list;
        }

        internal void LoadDefaultLabelDetail(IMemberRankSetupMappable objMappable)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRankSetupRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberRankSetupCriteria();

            foreach (var detail in objMappable.detail_table)
            {
                if (detail.rank.HasValue)
                {
                 var member_rank_setup =  ErsFactory.ersMemberFactory.GetErsMemberRankSetupWithRank(detail.rank, Convert.ToInt32(objMappable.site_id));
                 if (member_rank_setup != null)
                     detail.id = member_rank_setup.id;
                }
            }

        }
    }
}