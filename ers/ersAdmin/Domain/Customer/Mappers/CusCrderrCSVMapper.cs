﻿using System;
using System.Collections.Generic;
using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Customer.Mappers
{
    public class CusCrderrCSVMapper
        : CusCrderrSearchMapper, IMapper<ICusCrderrCSVMappable>
    {

        public void Map(ICusCrderrCSVMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }

        /// <summary>
        /// CSVファイルを作成する。
        /// </summary>
        /// <returns></returns>
        internal void CreateCsvFile(ICusCrderrCSVMappable objMappable)
        {
            var memberRegularErrListSpec = ErsFactory.ersMemberFactory.GetMemberRegularErrListSpec();

            objMappable.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();

            var rpsRegularErrLog = ErsFactory.ersOrderFactory.GetErsRegularErrLogRepository();
            var crtRegularErrLog = this.SetCriteria(objMappable);

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(crtRegularErrLog);
            }
            crtRegularErrLog.SetOrderByOccured_date(Criteria.OrderBy.ORDER_BY_DESC);

            var listRegularErrLog = memberRegularErrListSpec.GetSearchData(crtRegularErrLog);

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            ersAdmin.Models.csv.Cus_crderr_csv customer_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<ersAdmin.Models.csv.Cus_crderr_csv>(writer);
                foreach (var item in listRegularErrLog)
                {
                    this.getErrorDetail(objMappable, item);
                    customer_csv = new ersAdmin.Models.csv.Cus_crderr_csv();
                    customer_csv.OverwriteWithParameter(item);
                    customer_csv.regular_detail_id = this.make_regular_detail_id_list((int[])item["regular_detail_id"]);
                    customer_csv.gmo_error_number = objMappable.err_num;
                    customer_csv.gmo_error_message = objMappable.err_mess;
                    objMappable.csvCreater.WriteBody(customer_csv, writer);
                }
            }

        }

        private void getErrorDetail(ICusCrderrCSVMappable objMappable, Dictionary<string, object> item)
        {
            int fstnum = 0;
            string tmp_err_num = "";
            string tmp_err_mess = "";
            string delim = "";
            objMappable.err_num = "";
            objMappable.err_mess = "";

            string[] sp_string = Convert.ToString(item["error_description"]).Split('$');
            if (sp_string.Length > 0)
            {
                foreach (string linedata in sp_string)
                {
                    tmp_err_num = "";
                    tmp_err_mess = "";
                    fstnum = linedata.IndexOf(']', 0);
                    if (fstnum >= 0)
                    {
                        tmp_err_num = VBStrings.Left(linedata, fstnum);
                        tmp_err_num = tmp_err_num.Replace('[', ' ').Trim();
                        tmp_err_mess = VBStrings.Right(linedata, linedata.Length - (fstnum + 1));
                    }
                    else
                    {
                        tmp_err_mess = linedata;
                    }
                    objMappable.err_num = objMappable.err_num + delim + tmp_err_num;
                    objMappable.err_mess = objMappable.err_mess + delim + tmp_err_mess;
                    delim = ",";
                }
            }
            else
            {
                objMappable.err_mess = Convert.ToString(item["error_description"]);
            }
        }

    }
}