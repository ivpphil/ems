﻿using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using System;
using System.Linq;
using System.Collections.Generic;

namespace ersAdmin.Domain.Customer.Mappers
{
    public class CustomerMailMapper : IMapper<ICustomerMailMappable>
    {

        public void Map(ICustomerMailMappable objMappable)
        {
            this.Init(objMappable);
            if (objMappable.IsLoadDefaultValue)
                this.LoadDefaultValue(objMappable);
        }


        /// <summary>
        /// 初期化を行う。
        /// </summary>
        internal void Init(ICustomerMailMappable objMappable)
        {
            objMappable.objMember = ErsFactory.ersMemberFactory.getErsMemberWithMcodeForAdmin(objMappable.mcode, true);

            objMappable.isCommon = objMappable.objMember.site_id == (int)EnumSiteId.COMMON_SITE_ID;
            var site_list = ErsMallFactory.ersMallViewServiceFactory.GetErsViewSiteService().SelectAsList(EnumMallShopKbn.ERS);
            var template_list = new List<Dictionary<string, object>>();

            foreach (var site in site_list)
            {
                if (objMappable.isCommon || objMappable.objMember.site_id == Convert.ToInt32(site["value"]))
                {
                    var add_dic = new Dictionary<string, object>();

                    add_dic["site_name"] = site["name"];
                    add_dic["site_id"] = site["value"];

                    var setup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(site["id"]));
                    add_dic["from_email"] = setup.r_email;

                    var objSendmail = ErsFactory.ersMailFactory.getErsSendMailAdminIndividual(Convert.ToInt32(site["id"]));
                    objSendmail.Init(objMappable, EnumMformat.PC);
                    add_dic["mail_body"] = objSendmail.body;
                    add_dic["mail_title"] = objSendmail.title;

                    template_list.Add(add_dic);
                }
            }

            objMappable.template_list = template_list;
        }

        /// <summary>
        /// 初期値を読み込む。
        /// </summary>
        internal void LoadDefaultValue(ICustomerMailMappable objMappable)
        {
            objMappable.email = objMappable.objMember.email;

            if (!objMappable.isCommon)
            {
                var template = objMappable.template_list.First();
                objMappable.from_email = Convert.ToString(template["from_email"]);
                objMappable.mail_body = Convert.ToString(template["mail_body"]);
                objMappable.mail_title = Convert.ToString(template["mail_title"]);
            }
        }
    }
}