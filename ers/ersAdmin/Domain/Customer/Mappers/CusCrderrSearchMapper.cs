﻿using System;
using System.Collections.Generic;
using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Customer.Mappers
{
    public class CusCrderrSearchMapper : SiteSearchBaseMapper, IMapper<ICusCrderrSearchMappable>
    {
        public void Map(ICusCrderrSearchMappable objMappable)
        {
            this.SearchList(objMappable);
        }

        /// <summary>
        /// エラー顧客検索
        /// </summary>
        internal void SearchList(ICusCrderrSearchMappable objMappable)
        {
            var crtRegularErrLog = this.SetCriteria(objMappable);

            objMappable.list = new List<Dictionary<string, object>>();

            var memberRegularErrListSpec = ErsFactory.ersMemberFactory.GetMemberRegularErrListSpec();

            //件数
            objMappable.recordCount = memberRegularErrListSpec.GetCountData(crtRegularErrLog);
            if (objMappable.recordCount == 0)
            {
                throw new ErsException("10200");
            }

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(crtRegularErrLog);
            }
            crtRegularErrLog.SetOrderByOccured_date(Criteria.OrderBy.ORDER_BY_DESC);

            //表示用リスト作成
            var listRegularErrLog = memberRegularErrListSpec.GetSearchData(crtRegularErrLog);
            foreach (var records in listRegularErrLog)
            {
                records["regular_detail_id_list"] = this.make_regular_detail_id_list((int[])records["regular_detail_id"]);
                objMappable.list.Add(records);
            }

        }


        /// <summary>
        /// 顧客検索実体
        /// </summary>
        /// <returns></returns>
        internal ErsRegularErrLogCriteria SetCriteria(ICusCrderrSearchMappable objMappable)
        {
            ErsRegularErrLogCriteria crtRegularErrLog = ErsFactory.ersOrderFactory.GetErsRegularErrLogCriteria();

            crtRegularErrLog.disp_flg = EnumOnOff.On;
            crtRegularErrLog.active = EnumActive.Active;
            crtRegularErrLog.deleted = EnumDeleted.NotDeleted;

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, crtRegularErrLog, "member_t");

            // メールアドレス
            if (!string.IsNullOrEmpty(objMappable.src_email))
            {
                crtRegularErrLog.email = objMappable.src_email;
            }
            // 電話番号
            if (!string.IsNullOrEmpty(objMappable.src_tel))
            {
                crtRegularErrLog.tel = objMappable.src_tel;
            }
            // 姓
            if (!string.IsNullOrEmpty(objMappable.src_lnamek))
            {
                crtRegularErrLog.lnamek = objMappable.src_lnamek;
            }

            // 名
            if (!string.IsNullOrEmpty(objMappable.src_fnamek))
            {
                crtRegularErrLog.fnamek = objMappable.src_fnamek;
            }

            // エラー発生日
            if (objMappable.src_occured_date_from != null && objMappable.src_occured_date_to != null)
            {
                var dateFrom =DateTime.Parse(objMappable.src_occured_date_from.Value.ToString("yyyy/MM/dd 00:00:00"));
                var dateTo =DateTime.Parse(objMappable.src_occured_date_to.Value.ToString("yyyy/MM/dd  23:59:59"));
                crtRegularErrLog.SetOccured_dateBetween(dateFrom, dateTo);
            }

            //定期受注番号
            if (objMappable.src_regular_order_number != null)
            {
                crtRegularErrLog.regular_order_number = objMappable.src_regular_order_number;
            }

            return crtRegularErrLog;
        }

        protected string make_regular_detail_id_list(Int32[] detail_id)
        {
            if (detail_id == null) { return null; }
            return String.Join(",", detail_id);
        }
    }
}