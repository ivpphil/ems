﻿using ersAdmin.Domain.Customer.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using System;
using System.Linq;
using jp.co.ivp.ers.db;
using System.Collections.Generic;

namespace ersAdmin.Domain.Customer.Mappers
{
    public class CustomerMapper : IMapper<ICustomerMappable>
    {

        public void Map(ICustomerMappable objMappable)
        {
            if (objMappable.IsLoadDefaultData)
            {
                this.LoadDefaultData(objMappable);
                return;
            }

            this.LoadMemberObject(objMappable);
        }

        /// <summary>
        /// Load default member data
        /// </summary>
        internal void LoadDefaultData(ICustomerMappable objMappable)
        {
            this.LoadMemberObject(objMappable);

            objMappable.OverwriteWithParameter(objMappable.objMember.GetPropertiesAsDictionary());
            objMappable.email_confirm = objMappable.email;
            objMappable.passwd = string.Empty;
            objMappable.passwd_confirm = string.Empty;
            objMappable.point = this.ObtainPointForDisplay(objMappable.objMember.mcode);
            objMappable.disp_member_rank = ErsFactory.ersMemberFactory.GetObtainMemberRankNameStgy().Obtain(objMappable.objMember.mcode, objMappable.objMember.site_id.Value);

            if (objMappable.objMember.birth != null)
            {
                objMappable.birthday_y = objMappable.objMember.birth.Value.Year;
                objMappable.birthday_m = objMappable.objMember.birth.Value.Month;
                objMappable.birthday_d = objMappable.objMember.birth.Value.Day;
            }
        }

        /// <summary>
        /// Load default member object
        /// </summary>
        internal void LoadMemberObject(ICustomerMappable objMappable)
        {
            objMappable.objMember = ErsFactory.ersMemberFactory.getErsMemberWithMcodeForAdmin(objMappable.mcode, true);
        }

        /// <summary>
        /// 表示用ポイント取得 [Obtain point for display]
        /// </summary>
        /// <param name="mcode">string</param>
        /// <returns>string</returns>
        protected virtual string ObtainPointForDisplay(string mcode)
        {
            var repository = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryRepository();
            if (!ErsFactory.ersUtilityFactory.getSetup().Multiple_sites)
            {
                var scrit = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryCriteria();
                scrit.mcode = mcode;
                scrit.site_id = (int)EnumSiteId.COMMON_SITE_ID;
                return repository.GetRecordCount(scrit) == 0 ? "0" : repository.GetRecordCount(scrit).ToString();
            }

            var site_list = ErsMallFactory.ersMallViewServiceFactory.GetErsViewSiteService().SelectAsList(EnumMallShopKbn.ERS);
            var point_list = new List<string>();

            foreach (var site in site_list)
            {
                var criteria = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryCriteria();
                criteria.mcode = mcode;
                criteria.site_id = Convert.ToInt32(site["value"]);

                if (repository.GetRecordCount(criteria) == 0)
                {
                    point_list.Add(Convert.ToString(site["name"]) + ":0");
                }
                else
                {
                    criteria.SetOrderByDt(Criteria.OrderBy.ORDER_BY_DESC);
                    point_list.Add(Convert.ToString(site["name"]) + ":" + repository.Find(criteria).First().total_p);
                }
            }
            return string.Join(", ", point_list);
        }
    }
}