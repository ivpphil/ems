﻿using System;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Customer.Mappables
{
    public interface ICustomerCSVMappable : ICustomerSearchMappable
    {
        ErsCsvCreater csvCreater { get; set; }
    }
}