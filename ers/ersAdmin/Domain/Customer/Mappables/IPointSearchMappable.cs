﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Customer.Mappables
{
    public interface IPointSearchMappable : IMappable
    {

        bool IsCusPointSearch { get; }

        string mcode { get; }

        DateTime? s_date_f { get; }

        DateTime? s_date_t { get; }

        long search_result_cnt { get; set; }

        List<Dictionary<string, object>> PointHistoryList { get; set; }

        EnumDeleted? deleted { get; set; }

        /// <summary>
        /// ポイント検索用サイトID [Site ID for point search]
        /// </summary>
        int? sp_site_id { get; set; }
    }
}