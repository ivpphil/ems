﻿using System;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Customer.Mappables
{
    public interface ICusCrderrCSVMappable
        : IMappable, ICusCrderrSearchMappable
    {
        ErsCsvCreater csvCreater { get; set; }

        string err_num { get; set; }

        string err_mess { get; set; }
    }
}