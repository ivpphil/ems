﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Customer.Mappables
{
    public interface IMemberRankSetupMappable
        : ISiteRegisterBaseMappable, IMappable
    {
        List<member_rank_setup_detail> detail_table { get; set; }

        bool IsCompletePage { get; }
    }
}