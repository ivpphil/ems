﻿using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System.Collections.Generic;

namespace ersAdmin.Domain.Customer.Mappables
{
    public interface ICustomerMailMappable : IMappable
    {

        string mcode { get; set; }

        ErsMember objMember { get; set; }

        bool IsLoadDefaultValue { get; }

        string mail_title { get; set; }

        string mail_body { get; set; }

        string from_email { get; set; }

        string email { get; set; }

        List<Dictionary<string, object>> template_list { get; set; }

        bool isCommon { get; set; }
    }
}