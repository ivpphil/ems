﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Customer.Mappables
{
    public interface ICusCrderrSearchMappable : ISiteSearchBaseMappable, IMappable
    {
        List<Dictionary<string, object>> list { get; set; }

        long recordCount { get; set; }

        ErsPagerModel pager { get; }

        string src_email { get; }

        string src_tel { get; }

        string src_lnamek { get; }

        string src_fnamek { get; }

        DateTime? src_occured_date_from { get; }

        DateTime? src_occured_date_to { get; }

        int? src_regular_order_number { get; }

    }
}