﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;
using System;
using jp.co.ivp.ers.member;

namespace ersAdmin.Domain.Customer.Mappables
{
    public interface ICustomerMappable : IMappable
    {

        bool IsLoadDefaultData { get; }

        string mcode { get; set; }

        string email { get; set; }

        EnumMformat? mformat { get; set; }

        string tel { get; set; }

        string lname { get; set; }

        string fname { get; set; }

        string lnamek { get; set; }

        string fnamek { get; set; }

        string compname { get; set; }

        EnumSex? sex { get; set; }

        int? pref { get; set; }

        string passwd { get; set; }

        string compnamek { get; set; }

        string division { get; set; }

        string divisionk { get; set; }

        string tlname { get; set; }

        string tfname { get; set; }

        string tlnamek { get; set; }

        string tfnamek { get; set; }

        string zip { get; set; }

        string address { get; set; }

        string taddress { get; set; }

        string maddress { get; set; }

        short job { get; set; }

        string fax { get; set; }

        int? ques { get; set; }

        string ans { get; set; }

        string memo { get; set; }

        string email_confirm { get; set; }

        string passwd_confirm { get; set; }

        int? birthday_y { get; set; }

        int? birthday_m { get; set; }

        int? birthday_d { get; set; }

        EnumMFlg? m_flg { get; set; }

        //DateTime? intime_disp { get; }

        //DateTime? utime { get; set; }

        //int point { get; set; }

        //DateTime? birth { get; set; }

        ErsMember objMember { get; set; }

        string point { get; set; }

        string disp_member_rank { get; set; }
    }
}