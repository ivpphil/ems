﻿using System.Collections.Generic;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Customer.Mappables
{
    public interface ICusMailAllMappable
        : IMappable, ICustomerSearchMappable
    {
        IList<ErsMember> memberList { get; set; }
    }
}