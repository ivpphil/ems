﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.util;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Customer.Mappables
{
    public interface ICustomerSearchMappable : ISiteSearchBaseMappable, IMappable
    {

        long recordCount { get; set; }

        ErsPagerModel pager { get; }

        List<Dictionary<string, object>> list { set; }

        string src_email { get; }

        string src_tel { get; }

        string src_lname { get; }

        string src_fname { get; }

        string src_lnamek { get; }

        string src_fnamek { get; }

        string src_compname { get; }

        EnumMFlg? src_mailtype { get; }

        DateTime? src_regdate_f { get; }

        DateTime? src_regdate_t { get; }

        int? src_age_f { get; }

        int? src_age_t { get; }

        int? src_age_code { get; }

        EnumSex? src_sex { get; }

        int? src_point_f { get; }

        int? src_point_t { get; }

        int? src_pref { get; }

        EnumDmFlg? src_dm_flg { get; set; }

        EnumOutBoundFlg? src_out_bound_flg { get; set; }

        int? src_member_rank { get; }

        EnumDeleted? src_deleted { get; set; }
    }
}