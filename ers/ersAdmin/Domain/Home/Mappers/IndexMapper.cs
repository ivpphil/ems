﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Home.Mappables;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.db;
using NCrontab;
using jp.co.ivp.ers.member;

namespace ersAdmin.Domain.Home.Mappers
{
    public class IndexMapper
        :IMapper<IIndexMappable>
    {
        public void Map(IIndexMappable objMappable)
        {
            objMappable.CurDate = this.getDateTime(DateTime.Now, true);

            this.setInformation(objMappable);

            this.getSaleData(objMappable);

            this.getOrderData(objMappable);

            this.getCancelData(objMappable);

            this.getAllMember(objMappable);

            this.getIssuePoint(objMappable);

            this.getUsePoint(objMappable);

            this.getSaleTop(objMappable);

            this.getDisplayRanks(objMappable);

            this.getPrevUpdate(objMappable);

            this.getNextUpdate(objMappable);
        }

        private void setInformation(IIndexMappable objMappable)
        {
            var setup = ErsMallFactory.ersMallUtilityFactory.getSetup();

            if (!setup.enableMall)
            {
                return;
            }

            // 楽天パスワード有効期限メッセージ取得 [Get the message for Rakuten password validity]
            var message = ErsMallFactory.ersSiteFactory.GetObtainRakutenPasswordValidityMessageStgy().Obtain();

            if (message.HasValue())
            {
                objMappable.controller.AddInformation(message);
            }
        }

        private Dictionary<string, int> loadSaleData(string s_date_from, string s_date_to)
        {

            NewSaleSpecification salespecification = ErsFactory.ersOrderFactory.GetNewSaleSpecification();
            salespecification.date_from = s_date_from;
            salespecification.date_to = s_date_to;
            salespecification.isSatisfiedBy();
            return salespecification.dict;

        }

        private Dictionary<string, int> loadOrderData(string s_date_from, string s_date_to)
        {

            NewOrderSpecification orderspecification = ErsFactory.ersOrderFactory.GetNewOrderSpecification();
            orderspecification.date_from = s_date_from;
            orderspecification.date_to = s_date_to;
            orderspecification.isSatisfiedBy();
            return orderspecification.dict;

        }

        private int loadCancelData(string s_date_from, string s_date_to)
        {
            int result;
            NewCancelNumSpecification canselspecification = ErsFactory.ersOrderFactory.GetNewCancelNumSpecification();
            canselspecification.date_from = s_date_from;
            canselspecification.date_to = s_date_to;
            result = canselspecification.isSatisfiedBy();
            return result;

        }

        internal void getSaleData(IIndexMappable objMappable)
        {
            //書式フォーマット
            const string format = "yyyy/MM/dd";

            //今月設定
            DateTime today = DateTime.Today;
            DateTime firstDay_s = today.AddDays(-today.Day + 1);
            DateTime endDay_s = firstDay_s.AddMonths(1).AddDays(-1);
            string firstDayText_s = firstDay_s.ToString(format);
            string endDayText_s = endDay_s.ToString(format);

            //先月設定
            DateTime today_e = DateTime.Today.AddMonths(-1);
            DateTime firstDay_e = today_e.AddDays(-today.Day + 1);
            DateTime endDay_e = firstDay_e.AddMonths(1).AddDays(-1);
            string firstDayText_e = firstDay_e.ToString(format);
            string endDayText_e = endDay_e.ToString(format);

            DateTime dtNow = DateTime.Now;
            string uWeekday = dtNow.DayOfWeek.ToString("d");

            /// 売上金額、売上件数

            /// 本日(売上)
            objMappable.TodaySales = loadSaleData(dtNow.ToShortDateString() + " 00:00:00", dtNow.ToShortDateString() + " 23:59:59");

            /// 今週(売上)
            objMappable.ThisWeekSales = loadSaleData(dtNow.AddDays(0 - int.Parse(uWeekday)).ToShortDateString() + " 00:00:00", dtNow.AddDays((0 - int.Parse(uWeekday)) + 6).ToShortDateString() + " 23:59:59");

            /// 先週(売上)
            objMappable.LastWeekSales = loadSaleData(dtNow.AddDays((0 - int.Parse(uWeekday)) - 7).ToShortDateString() + " 00:00:00", dtNow.AddDays(((0 - int.Parse(uWeekday)) + 6) - 7).ToShortDateString() + " 23:59:59");

            /// 今月(売上)
            objMappable.ThisMonthSales = loadSaleData(firstDayText_s + " 00:00:00", endDayText_s + " 23:59:59");

            /// 先月(売上)
            objMappable.LastMonthSales = loadSaleData(firstDayText_e + " 00:00:00", endDayText_e + " 23:59:59");

            /// 今年(売上)
            objMappable.ThisYearSales = loadSaleData(dtNow.Year + "/01/01 00:00:00", dtNow.Year + "/12/31 23:59:59");

            /// 昨年(売上)
            objMappable.LastYearSales = loadSaleData(dtNow.AddYears(-1).Year + "/01/01 00:00:00", dtNow.AddYears(-1).Year + "/12/31 23:59:59");

        }

        internal void getOrderData(IIndexMappable objMappable)
        {
            //書式フォーマット
            const string format = "yyyy/MM/dd";

            //今月設定
            DateTime today = DateTime.Today;
            DateTime firstDay_s = today.AddDays(-today.Day + 1);
            DateTime endDay_s = firstDay_s.AddMonths(1).AddDays(-1);
            string firstDayText_s = firstDay_s.ToString(format);
            string endDayText_s = endDay_s.ToString(format);

            //先月設定
            DateTime today_e = DateTime.Today.AddMonths(-1);
            DateTime firstDay_e = today_e.AddDays(-today.Day + 1);
            DateTime endDay_e = firstDay_e.AddMonths(1).AddDays(-1);
            string firstDayText_e = firstDay_e.ToString(format);
            string endDayText_e = endDay_e.ToString(format);

            DateTime dtNow = DateTime.Now;
            string uWeekday = dtNow.DayOfWeek.ToString("d");

            /// 受注金額、受注件数

            /// 本日(受注)
            objMappable.TodayOrders = loadOrderData(dtNow.ToShortDateString() + " 00:00:00", dtNow.ToShortDateString() + " 23:59:59");

            /// 今週(受注)
            objMappable.ThisWeekOrders = loadOrderData(dtNow.AddDays(0 - int.Parse(uWeekday)).ToShortDateString() + " 00:00:00", dtNow.AddDays((0 - int.Parse(uWeekday)) + 6).ToShortDateString() + " 23:59:59");

            /// 先週(受注)
            objMappable.LastWeekOrders = loadOrderData(dtNow.AddDays((0 - int.Parse(uWeekday)) - 7).ToShortDateString() + " 00:00:00", dtNow.AddDays(((0 - int.Parse(uWeekday)) + 6) - 7).ToShortDateString() + " 23:59:59");

            /// 今月(受注)
            objMappable.ThisMonthOrders = loadOrderData(firstDayText_s + " 00:00:00", endDayText_s + " 23:59:59");

            /// 先月(受注)
            objMappable.LastMonthOrders = loadOrderData(firstDayText_e + " 00:00:00", endDayText_e + " 23:59:59");

            /// 今年(受注)
            objMappable.ThisYearOrders = loadOrderData(dtNow.Year + "/01/01 00:00:00", dtNow.Year + "/12/31 23:59:59");

            /// 昨年(受注)
            objMappable.LastYearOrders = loadOrderData(dtNow.AddYears(-1).Year + "/01/01 00:00:00", dtNow.AddYears(-1).Year + "/12/31 23:59:59");

        }

        internal void getCancelData(IIndexMappable objMappable)
        {

            //書式フォーマット
            const string format = "yyyy/MM/dd";

            //今月設定
            DateTime today = DateTime.Today;
            DateTime firstDay_s = today.AddDays(-today.Day + 1);
            DateTime endDay_s = firstDay_s.AddMonths(1).AddDays(-1);
            string firstDayText_s = firstDay_s.ToString(format);
            string endDayText_s = endDay_s.ToString(format);

            //先月設定
            DateTime today_e = DateTime.Today.AddMonths(-1);
            DateTime firstDay_e = today_e.AddDays(-today.Day + 1);
            DateTime endDay_e = firstDay_e.AddMonths(1).AddDays(-1);
            string firstDayText_e = firstDay_e.ToString(format);
            string endDayText_e = endDay_e.ToString(format);

            DateTime dtNow = DateTime.Now;
            string uWeekday = dtNow.DayOfWeek.ToString("d");

            /// キャンセル数

            /// 本日(キャンセル数)
            objMappable.TodayCancelNum = loadCancelData(dtNow.ToShortDateString() + " 00:00:00", dtNow.ToShortDateString() + " 23:59:59");

            /// 今週(キャンセル数)
            objMappable.ThisWeekCancelNum = loadCancelData(dtNow.AddDays(0 - int.Parse(uWeekday)).ToShortDateString() + " 00:00:00", dtNow.AddDays((0 - int.Parse(uWeekday)) + 6).ToShortDateString() + " 23:59:59");

            /// 先週(キャンセル数)
            objMappable.LastWeekCancelNum = loadCancelData(dtNow.AddDays((0 - int.Parse(uWeekday)) - 7).ToShortDateString() + " 00:00:00", dtNow.AddDays(((0 - int.Parse(uWeekday)) + 6) - 7).ToShortDateString() + " 23:59:59");

            /// 今月(キャンセル数)
            objMappable.ThisMonthCancelNum = loadCancelData(firstDayText_s + " 00:00:00", endDayText_s + " 23:59:59");

            /// 先月(キャンセル数)
            objMappable.LastMonthCancelNum = loadCancelData(firstDayText_e + " 00:00:00", endDayText_e + " 23:59:59");

            /// 今年(キャンセル数)
            objMappable.ThisYearCancelNum = loadCancelData(dtNow.Year + "/01/01 00:00:00", dtNow.Year + "/12/31 23:59:59");

            /// 昨年(キャンセル数)
            objMappable.LastYearCancelNum = loadCancelData(dtNow.AddYears(-1).Year + "/01/01 00:00:00", dtNow.AddYears(-1).Year + "/12/31 23:59:59");

        }

        internal void getAllMember(IIndexMappable objMappable)
        {
            /// 累計会員数
            NewMemberAllCountSpecification allmempspecification = ErsFactory.ersMemberFactory.GetNewMemberAllCountSpecification();
            objMappable.MemberAllCount = allmempspecification.isSatisfiedBy();

        }

        internal void getIssuePoint(IIndexMappable objMappable)
        {
            /// 累計ﾎﾟｲﾝﾄ発行数
            NewPointSpecification allpspecification = ErsFactory.ersPointHistoryFactory.GetNewPointSpecification();
            allpspecification.Conditions = "all";
            objMappable.AllPoint = allpspecification.isSatisfiedBy();

        }

        internal void getUsePoint(IIndexMappable objMappable)
        {
            /// 使用ポイント数
            NewPointSpecification usepspecification = ErsFactory.ersPointHistoryFactory.GetNewPointSpecification();
            usepspecification.Conditions = "use";
            objMappable.UsedPoint = usepspecification.isSatisfiedBy();

        }

        internal void getSaleTop(IIndexMappable objMappable)
        {
            /// 今月の売上商品トップ5
            const string format = "yyyy/MM/dd";
            DateTime today = DateTime.Today;
            DateTime firstDay = today.AddDays(-today.Day + 1);
            DateTime endDay = firstDay.AddMonths(1).AddDays(-1);
            string firstDayText = firstDay.ToString(format);
            string endDayText = endDay.ToString(format);

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            NewTop5Specification topspecification = ErsFactory.ersOrderFactory.GetNewTop5Specification();
            topspecification.date_from = firstDayText + " 00:00:00";
            topspecification.date_to = endDayText + " 23:59:59";

            objMappable.GetTop = topspecification.isSatisfiedBy();
        }

        internal void getDisplayRanks(IIndexMappable objMappable)
        {
            if (ErsFactory.ersUtilityFactory.getSetup().member_rank_centralization)
            {
               objMappable.DisplayRank = this.getDisplayRanksWithSiteId((int)EnumSiteId.COMMON_SITE_ID);
            }
            else
            {
                var site_repository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
                var site_criteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
                site_criteria.mall_shop_kbn = EnumMallShopKbn.ERS;
                site_criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var site_list = site_repository.Find(site_criteria);
                var string_list = new List<string>();

                foreach (var site in site_list)
                {
                    string_list.Add(string.Format(@"【{0}】 {1}", site.site_name, this.getDisplayRanksWithSiteId(site.id)));
                }

                objMappable.DisplayRank = string.Join(Environment.NewLine, string_list);
            }
        }

        protected virtual string getDisplayRanksWithSiteId(int? site_id)
        {
            string RankDisplay = string.Empty;

            /// Displat Member Rank
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRankSetupRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberRankSetupCriteria();
            criteria.active = EnumActive.Active;
            criteria.site_id_for_admin = site_id;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var member_rank_setup_list = repository.Find(criteria);
            foreach (var item in member_rank_setup_list)
            {
                RankDisplay = getRankDisplay(RankDisplay,item,site_id);
            }
            return RankDisplay;
        }

        internal string getRankDisplay(string RankDisplay, ErsMemberRankSetup item, int? site_id)
        {
            var rep = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var cri = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            cri.member_rank = item.id;
            cri.deleted = EnumDeleted.NotDeleted;
            cri.ignoreMonitor();
            var rCount = rep.GetRecordCount(cri);
            RankDisplay = RankDisplay + item.rank_name + ":" + rCount + " " + ErsResources.GetFieldName("common_person") + " ";

            return RankDisplay;
        }

        internal void getPrevUpdate(IIndexMappable objMappable)
        {
            string PrevNextUpdate =string.Empty;
            var Result = ErsFactory.ersBatchFactory.GetErsBatchScheduleWithBatchId("MemberRank");
            if (Result.last_success_date != null)
            {
                objMappable.PreviousUpdate = getDateTime(Result.last_success_date.Value);
            }


        }
        internal void getNextUpdate(IIndexMappable objMappable)
        {
            string PrevNextUpdate = string.Empty;
            var Result = ErsFactory.ersBatchFactory.GetErsBatchScheduleWithBatchId("MemberRank");
            if (Result.last_success_date != null)
            {
                var contab = CrontabSchedule.Parse(Result.schedule);
                var nextDate = contab.GetNextOccurrence(Result.last_date.Value);
                nextDate = nextDate.AddSeconds(-nextDate.Second);

                objMappable.NextUpdate = getDateTime(nextDate);
            }
        }

        internal string getDateTime(DateTime dt, bool sec=false)
        {
            var tempDate = dt.ToString("yyyy") + ErsResources.GetMessage("year");
            tempDate += dt.ToString("MM") + ErsResources.GetMessage("months");
            tempDate += dt.ToString("dd") + ErsResources.GetMessage("day") + " ";
            tempDate += dt.ToString("HH") + ErsResources.GetMessage("hour");
            tempDate += dt.ToString("mm") + ErsResources.GetMessage("min");
            if (sec)
            {
                tempDate += dt.ToString("ss") + ErsResources.GetMessage("sec");
            }

            return tempDate;
        }
    }

}