﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Home.Mappables
{
    public interface IIndexMappable
        : IMappable
    {
        string CurDate { set; }

        /// <summary>
        /// Sales
        /// </summary>
        Dictionary<string, int> TodaySales { set; }

        Dictionary<string, int> ThisWeekSales { set; }

        Dictionary<string, int> LastWeekSales { set; }

        Dictionary<string, int> ThisMonthSales { set; }

        Dictionary<string, int> LastMonthSales { set; }

        Dictionary<string, int> ThisYearSales { set; }

        Dictionary<string, int> LastYearSales { set; }

        /// <summary>
        /// Order
        /// </summary>
        Dictionary<string, int> TodayOrders { set; }

        Dictionary<string, int> ThisWeekOrders { set; }

        Dictionary<string, int> LastWeekOrders { set; }

        Dictionary<string, int> ThisMonthOrders { set; }

        Dictionary<string, int> LastMonthOrders { set; }

        Dictionary<string, int> ThisYearOrders { set; }

        Dictionary<string, int> LastYearOrders { set; }

        /// <summary>
        /// Cancel
        /// </summary>
        int TodayCancelNum { set; }

        int ThisWeekCancelNum { set; }

        int LastWeekCancelNum { set; }

        int ThisMonthCancelNum { set; }

        int LastMonthCancelNum { set; }

        int ThisYearCancelNum { set; }

        int LastYearCancelNum { set; }

        /// <summary>
        /// Member
        /// </summary>
        long MemberAllCount { set; }

        /// <summary>
        /// All Point
        /// </summary>
        long AllPoint { set; }

        /// <summary>
        /// Used Point
        /// </summary>
        long UsedPoint { set; }

        /// <summary>
        /// Top
        /// </summary>
        List<Dictionary<string, object>> GetTop { set; }

        /// <summary>
        /// Member Rank Display
        /// </summary>
        string DisplayRank { set; }

        /// <summary>
        /// Display Previous Update
        /// </summary>
        string PreviousUpdate { set; }

        /// <summary>
        /// Display Next Update
        /// </summary>
        string NextUpdate { set; }
    }
}