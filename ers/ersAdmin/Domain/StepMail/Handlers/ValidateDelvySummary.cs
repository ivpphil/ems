﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.StepMail.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.StepMail.Handlers
{
    public class ValidateDelvySummary:IValidationHandler<IDelvySummaryCommand>
    {
        /// <summary>
        /// complex validation
        /// </summary>
        /// <param name="lineName"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(IDelvySummaryCommand command)
        {
            yield return command.CheckRequired("process_id");
            if (command.IsValidField("process_id") && command.process_id != null)
            {
                yield return ErsFactory.ErsAtMailFactory.GetCheckProcessExistStgy().CheckProcessAndStepmailExist(command.process_id);
            }
        }

    }
}