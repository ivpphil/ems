﻿using System;
using System.Collections.Generic;
using ersAdmin.Domain.StepMail.Commands;
using ersAdmin.Models.stepmail;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.step_scenario;

namespace ersAdmin.Domain.StepMail.Handlers
{
    public class ScenarioModifyHandler : ICommandHandler<IScenarioModifyCommand>
    {
        public ICommandResult Submit(IScenarioModifyCommand command)
        {
            this.Update(command);
            return new CommandResult(true);
        }

        /// <summary>
        /// Updates step mail scenario in step_scenario_t
        /// </summary>
        private void Update(IScenarioModifyCommand command)
        {
            if (command.submit_update)
            {
                ErsStepScenarioRepository repository = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioRepository();
                ErsStepScenario objOldEntity = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioByID(command.id.Value);
                ErsStepScenario objNewEntity = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioByID(command.id.Value);

                objNewEntity.OverwriteWithModel(command);
                objNewEntity.reg_elapsed_to = command.reg_elapsed_to ?? ushort.MaxValue;
                //mail_delv_time_mmがnotnullの為nullの場合はdefaultの0を渡す
                if (objNewEntity.mail_delv_time_mm == null)
                {
                    objNewEntity.mail_delv_time_mm = 0;
                }
                repository.Update(objOldEntity, objNewEntity);
            }
        }
    }
}