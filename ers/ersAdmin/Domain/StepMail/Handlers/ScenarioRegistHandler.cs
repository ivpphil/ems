﻿using System;
using ersAdmin.Domain.StepMail.Commands;
using ersAdmin.Models.stepmail;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.step_scenario;

namespace ersAdmin.Domain.StepMail.Handlers
{
    public class ScenarioRegistHandler : ICommandHandler<IScenarioRegistCommand>
    {
        public ICommandResult Submit(IScenarioRegistCommand command)
        {
            this.Insert(command);
            return new CommandResult(true); 
        }

        /// <summary>
        /// Creates new Step mail scenario
        /// </summary>
        private void Insert(IScenarioRegistCommand command)
        {
            ErsStepScenarioRepository repository = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioRepository();
            ErsStepScenario objErsStepScenario = ErsFactory.ersStepScenarioFactory.GetErsStepScenario();

            objErsStepScenario.OverwriteWithModel(command);
            objErsStepScenario.intime = DateTime.Now;
            objErsStepScenario.active = EnumActive.Active;

            objErsStepScenario.reg_elapsed_to = command.reg_elapsed_to ?? ushort.MaxValue;

            repository.Insert(objErsStepScenario, true);
        }
    }
}