﻿using ersAdmin.Domain.StepMail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.StepMail.Handlers
{
    public class StepmailRegistHandler : ICommandHandler<IStepmailRegistCommand>
    {
        public ICommandResult Submit(IStepmailRegistCommand command)
        {
            this.Insert(command);
            return new CommandResult(true);
        }

        /// <summary>
        /// for inserting records in the table
        /// </summary>
        private void Insert(IStepmailRegistCommand command)
        {
            var stepMailRepo = ErsFactory.ersStepMailFactory.GetErsStepMailRepository();
            var stepMail = ErsFactory.ersStepMailFactory.GetErsStepMail();

            stepMail.OverwriteWithModel(command);

            stepMailRepo.Insert(stepMail, true);
        }
    }
}