﻿using ersAdmin.Domain.StepMail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.StepMail.Handlers
{
    public class StepmailDeleteHandler : ICommandHandler<IStepmailDeleteCommand>
    {
        public ICommandResult Submit(IStepmailDeleteCommand command)
        {
            this.executeOperation(command);
            return new CommandResult(true);
        }

        private void executeOperation(IStepmailDeleteCommand command)
        {
            var stepMailRepo = ErsFactory.ersStepMailFactory.GetErsStepMailRepository();
            var stepMail = ErsFactory.ersStepMailFactory.GetErsStepMailWithId(command.id.Value);
            stepMailRepo.Delete(stepMail);
        }
    }
}