﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.StepMail.Commands;
using ersAdmin.Models.stepmail;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.StepMail.Handlers
{
    public class ValidateScenarioModify : IValidationHandler<IScenarioModifyCommand>
    {
        public IEnumerable<ValidationResult> Validate(IScenarioModifyCommand command)
        {
            if (command.submit_update)
            {
                yield return command.CheckRequired("scenario_name");
                yield return command.CheckRequired("target_id");
                yield return command.CheckRequired("mail_ref_date_kbn");

                if (command.mail_ref_date_kbn == EnumReferenceDate.LastPurchasedDate)
                {
                    yield return command.CheckRequired("mail_delv_time_kbn");
                }

                yield return ErsFactory.ersTargetFactory.GetValidateOrderPtnKbnStgy().ValidateScenario(command.target_id, command.mail_ref_date_kbn, "target_id");

                if (command.mail_ref_date_kbn == EnumReferenceDate.Birthdate || command.mail_ref_date_kbn == 0)
                {
                    yield return command.CheckRequired("mail_delv_time_hh");
                    yield return command.CheckRequired("mail_delv_time_mm");
                }
                else if (command.mail_ref_date_kbn == EnumReferenceDate.LastPurchasedDate && command.mail_delv_time_kbn == 0)
                {
                    yield return command.CheckRequired("mail_delv_time_hh");
                    yield return command.CheckRequired("mail_delv_time_mm");

                }
                else if (command.mail_ref_date_kbn == EnumReferenceDate.LastPurchasedDate && command.mail_delv_time_kbn == EnumDeliveryTime.NotDeliveryTime)
                {
                    yield return command.CheckRequired("mail_delv_out_time_hh_from");
                    yield return command.CheckRequired("mail_delv_out_time_mm_from");
                    yield return command.CheckRequired("mail_delv_out_time_hh_to");
                    yield return command.CheckRequired("mail_delv_out_time_mm_to");
                }

                yield return command.CheckRequired("mail_from_addr");
                yield return command.CheckRequired("mail_from_name");
                yield return command.CheckRequired("mail_reply_addr");
                yield return command.CheckRequired("mail_status_kbn");

                var checkNumericStgy = ErsFactory.ersCommonFactory.GetCheckNumericFromToStgy();
                foreach (var result in checkNumericStgy.Check("reg_elapsed_from", command.reg_elapsed_from, "reg_elapsed_to", command.reg_elapsed_to))
                {
                    yield return result;
                }
            }
        }
    }
}