﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.StepMail.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.StepMail.Handlers
{
    public class ValidateScenarioDelete : IValidationHandler<IScenarioDeleteCommand>
    {
        public IEnumerable<ValidationResult> Validate(IScenarioDeleteCommand command)
        {
            yield return command.CheckRequired("id");
        }
    }
}