﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.StepMail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.StepMail.Handlers
{
    public class ValidateStepmailRegist : IValidationHandler<IStepmailRegistCommand>
    {
        public IEnumerable<ValidationResult> Validate(IStepmailRegistCommand command)
        {
            yield return command.CheckRequired("scenario_id");
            yield return command.CheckRequired("elapsed_kbn");
            yield return command.CheckRequired("mail_status_kbn");
            yield return command.CheckRequired("step_mail_name");

            var checkDuplicate = ErsFactory.ersStepMailFactory.GetCheckDuplicateStepMail();

            foreach (var result in checkDuplicate.Check(command.step_mail_name))
            {
                yield return result;
            }

        }
    }
}