﻿using ersAdmin.Domain.StepMail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.StepMail.Handlers
{
    public class StepmailModifyHandler : ICommandHandler<IStepmailModifyCommand>
    {
        public ICommandResult Submit(IStepmailModifyCommand command)
        {
            this.Update(command);
            return new CommandResult(true);
        }

        private void Update(IStepmailModifyCommand command)
        {
            var repository = ErsFactory.ersStepMailFactory.GetErsStepMailRepository();
            var objOldEntity = ErsFactory.ersStepMailFactory.GetErsStepMailWithId(command.id.Value);
            var objNewEntity = ErsFactory.ersStepMailFactory.GetErsStepMailWithId(command.id.Value);

            objNewEntity.OverwriteWithModel(command);

            repository.Update(objOldEntity, objNewEntity);
        }
    }
}