﻿using ersAdmin.Domain.StepMail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.StepMail.Handlers
{
    public class ScenarioDeleteHandler : ICommandHandler<IScenarioDeleteCommand>
    {
        public ICommandResult Submit(IScenarioDeleteCommand command)
        {
            this.Delete(command);
            return new CommandResult(true);
        }

        /// <summary>
        /// Deletes step mail scenario in step_scenario_t
        /// </summary>
        private void Delete(IScenarioDeleteCommand command)
        {
            if (command.submit_delete)
            {
                var repository = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioRepository();
                var objOldEntity = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioByID(command.id.Value);

                repository.Delete(objOldEntity);

                this.DeleteStepMailItem(command);
            }
        }

        /// <summary>
        /// deletes step mail in step_mail_t
        /// </summary>
        internal void DeleteStepMailItem(IScenarioDeleteCommand command)
        {
            var repository = ErsFactory.ersStepMailFactory.GetErsStepMailRepository();
            var criteria = ErsFactory.ersStepMailFactory.GetErsStepMailCriteria();
            criteria.scenario_id = command.id.Value;

            repository.Delete(criteria);
        }
    }
}