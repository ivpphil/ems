﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.StepMail.Mappers
{
    public class DelvyActualListMapper : IMapper<IDelvyActualListMappable>
    {
        public void Map(IDelvyActualListMappable ObjMappable)
        {
            this.Init(ObjMappable);
        }
        /// <summary>
        /// itemリストを初期化する。
        /// </summary>
        /// <param name="pager"></param>
        internal void Init(IDelvyActualListMappable ObjMappable)
        {
            //search s_master_t
            var repository = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();
            criteria.step_mail_id_not_equal = null;
            criteria.active = EnumActive.Active;

            //検索結果の総数を取得
            ObjMappable.recordCount = repository.GetRecordCount(criteria);

            if (ObjMappable.recordCount > 0)
            {
                //検索SQLにLIMIT と OFFSETを加える
                ObjMappable.pager.SetLimitAndOffsetToCriteria(criteria);

                criteria.SetOrderByScheduleDate(Criteria.OrderBy.ORDER_BY_DESC);
                criteria.SetOrderByProcessID(Criteria.OrderBy.ORDER_BY_DESC);

                //商品出力結果
                var listResult = repository.Find(criteria);

                var searched_items = new List<Dictionary<string, object>>();

                var commonNameService = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService();
                foreach (var process in listResult)
                {
                    var dictionary = process.GetPropertiesAsDictionary();
                    dictionary["w_sent_status"] = commonNameService.GetStringFromId(EnumCommonNameType.AmProcessStatus, EnumCommonNameColumnName.namename, (int)process.status.Value);
                    searched_items.Add(dictionary);
                }

                ObjMappable.searched_items = searched_items;
            }
        }
    }
}