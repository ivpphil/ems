﻿using System;
using System.Collections.Generic;
using System.Linq;
using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.stepmail;

namespace ersAdmin.Domain.StepMail.Mappers
{
    public class StepmailListMapper : IMapper<IStepmailListMappable>
    {
        public void Map(IStepmailListMappable objMappable)
        {
            this.LoadStepMailList(objMappable);
        }

        /// <summary>
        /// Get the step mail records
        /// </summary>
        /// <returns></returns>
        private void LoadStepMailList(IStepmailListMappable objMappable)
        {
            var lstRet = new List<Dictionary<string, object>>();

            var repository = ErsFactory.ersStepMailFactory.GetErsStepMailRepository();
            var criteria = ErsFactory.ersStepMailFactory.GetErsStepMailCriteria();
            criteria.scenario_id = objMappable.scenario_id;

            objMappable.recordCount = repository.GetRecordCount(criteria);

            if (objMappable.recordCount > 0)
            {
                this.SetSortToCriteria(objMappable, criteria);

                var list = repository.Find(criteria);

                long ctr = 1;
                long pageNo = 0;

                foreach (var record in list)
                {
                    var dicView = record.GetPropertiesAsDictionary();

                    if (objMappable.pageCnt == 0) { pageNo = 1; } else { pageNo = objMappable.pageCnt; }
                    if (pageNo > objMappable.recordCount) { pageNo = objMappable.recordCount; }

                    dicView["status"] = ErsFactory.ersViewServiceFactory.GetErsViewStatusService().GetStringFromId(record.mail_status_kbn);
                    dicView["result_cnt"] = ((pageNo * objMappable.maxItemCount) - objMappable.maxItemCount) + ctr++;

                    lstRet.Add(dicView);
                }
            }
            objMappable.StepMailList = lstRet;
        }

        /// <summary>
        /// Sets sorting style of a list
        /// </summary>
        /// <param name="criteria">criteria ErsStepScenarioCriteria</param>
        internal void SetSortToCriteria(IStepmailListMappable objMappable, ErsStepMailCriteria criteria)
        {
            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            }

            criteria.SetOrderByDeliveryDate(Criteria.OrderBy.ORDER_BY_ASC);
        }
    }
}