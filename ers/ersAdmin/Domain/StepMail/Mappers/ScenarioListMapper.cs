﻿using System.Collections.Generic;
using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.step_scenario;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.StepMail.Mappers
{
    public class ScenarioListMapper : IMapper<IScenarioListMappable>
    {

        public void Map(IScenarioListMappable objMappable)
        {
            this.SearchList(objMappable);
        }

        public void SearchList(IScenarioListMappable objMappable)
        {
            this.LoadErsStepscenario(objMappable);
        }

        internal void LoadErsStepscenario(IScenarioListMappable objMappable)
        {
            var repository = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioRepository();

            var criteria = GetCriteria(objMappable);

            var lstRet = new List<Dictionary<string, object>>();

            objMappable.recordCount = repository.GetRecordCount(criteria);

            if (objMappable.recordCount > 0)
            {
                this.SetSortToCriteria(objMappable.pager, criteria);

                var list = repository.Find(criteria);

                objMappable.scenario_Erslist = ErsCommon.ConvertEntityListToDictionaryList(list);



                long ctr = 1;
                long pageNo = 0;

                foreach (var record in list)
                {
                    var dicView = record.GetPropertiesAsDictionary();

                    if (objMappable.pageCnt == 0) { pageNo = 1; } else { pageNo = objMappable.pageCnt; }
                    if (pageNo > objMappable.recordCount) { pageNo = objMappable.recordCount; }

                    dicView["result_cnt"] = ((pageNo * objMappable.maxItemCount) - objMappable.maxItemCount) + ctr++;
                    dicView["id"] = record.id;
                    dicView["scenario_name"] = record.scenario_name;
                    dicView["status"] = ErsFactory.ersViewServiceFactory.GetErsViewStatusService().GetStringFromId(record.mail_status_kbn);
                    dicView["intime"] = record.intime;
                    dicView["utime"] = record.utime;

                    lstRet.Add(dicView);
                }
            }

            objMappable.scenarioList = lstRet;
        }

        /// <summary>
        /// Gets criteria for searching
        /// </summary>
        /// <returns>returns ErsStepScenarioCriteria</returns>
        private ErsStepScenarioCriteria GetCriteria(IScenarioListMappable objMappable)
        {
            ErsStepScenarioCriteria stepScenCri = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioCriteria();

            //set parameters
            if (!string.IsNullOrEmpty(objMappable.s_scenario_name))
                stepScenCri.scenario_name_like = objMappable.s_scenario_name;

            if (objMappable.s_mail_status_kbn > 0)
                stepScenCri.mail_status_kbn = objMappable.s_mail_status_kbn;

            return stepScenCri;
        }

        /// <summary>
        /// Sets sorting style of a list
        /// </summary>
        /// <param name="criteria">criteria ErsStepScenarioCriteria</param>
        internal void SetSortToCriteria(ErsPagerModel pager, ErsStepScenarioCriteria criteria)
        {
            if (pager != null)
            {
                pager.SetLimitAndOffsetToCriteria(criteria);
            }

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
        }

    }
}