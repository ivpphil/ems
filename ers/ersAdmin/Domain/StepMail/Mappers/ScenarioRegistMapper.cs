﻿using System;
using System.Collections.Generic;
using System.Linq;
using ersAdmin.Domain.StepMail.Mappables;
using ersAdmin.Models.stepmail;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System.Text.RegularExpressions;

namespace ersAdmin.Domain.StepMail.Mappers
{
    public class ScenarioRegistMapper : IMapper<IScenarioRegistMappable>
    {
        public void Map(IScenarioRegistMappable objMappable)
        {
            if (objMappable.IsInitialization)
            {
                this.Initialize(objMappable);
            }
        }

        /// <summary>
        /// initialize value for flexible grids
        /// </summary>
        internal void Initialize(IScenarioRegistMappable objMappable)
        {
            var setMstRepo = ErsFactory.ErsAtMailFactory.GetErsAmSetupRepository();
            var listSetup = setMstRepo.Find(null);
            if (listSetup.Count == 0)
            {
                return;
            }
            var setup = listSetup.First();
            objMappable.mail_reply_addr = setup.p_email;
            var matchs = SplitMailPart(setup.r_email);
            if (matchs.Count != 0)
            {
                objMappable.mail_from_name = matchs[0].Groups[1].Value;
                objMappable.mail_from_addr = matchs[0].Groups[2].Value;
            }
        }

        //split part of email
        private static MatchCollection SplitMailPart(string mailAddress)
        {
            string operationTagStart = "^(.+)?<(.+)>$";
            var matchs = new Regex(operationTagStart, RegexOptions.IgnoreCase).Matches(mailAddress);
            return matchs;
        }
    }
}