﻿using System;
using System.Collections.Generic;
using System.Linq;
using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersAdmin.Domain.StepMail.Mappers
{
    public class StepmailRegistMapper : IMapper<IStepmailRegistMappable>
    {
        public void Map(IStepmailRegistMappable objMappable)
        {
            this.LoadScenario(objMappable);
        }

        private void LoadScenario(IStepmailRegistMappable objMappable)
        {
            ////get the step scenario by id
            var stepScenario = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioByID(objMappable.scenario_id);
            var commonNameService = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService();

            objMappable.scenario_name = stepScenario.scenario_name;
            objMappable.strstatus = commonNameService.GetStringFromId(EnumCommonNameType.Status, EnumCommonNameColumnName.namename, (int?)stepScenario.mail_status_kbn);
            objMappable.strmail_ref_date_kbn = commonNameService.GetStringFromId(EnumCommonNameType.RefDate, EnumCommonNameColumnName.namename, (int?)stepScenario.mail_ref_date_kbn);
            objMappable.mail_delv_time_kbn = stepScenario.mail_delv_time_kbn;

            objMappable.mail_delv_time_hh = stepScenario.mail_delv_time_hh;
            objMappable.mail_delv_time_mm = stepScenario.mail_delv_time_mm;

            objMappable.mail_delv_out_time_hh_from = stepScenario.mail_delv_out_time_hh_from;
            objMappable.mail_delv_out_time_mm_from = stepScenario.mail_delv_out_time_mm_from;
            objMappable.mail_delv_out_time_hh_to = stepScenario.mail_delv_out_time_hh_to;
            objMappable.mail_delv_out_time_mm_to = stepScenario.mail_delv_out_time_mm_to;

            objMappable.w_target = ErsFactory.ersViewServiceFactory.GetErsViewTargetService().GetStringFromId(stepScenario.target_id);
        }
    }
}