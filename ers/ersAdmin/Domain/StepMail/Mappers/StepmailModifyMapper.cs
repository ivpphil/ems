﻿using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.StepMail.Mappers
{
    public class StepmailModifyMapper : IMapper<IStepmailModifyMappable>
    {
        public void Map(IStepmailModifyMappable objMappable)
        {
            if(objMappable.IsLoadDefaultData)
                this.LoadDefaultData(objMappable);
        }

        public void LoadDefaultData(IStepmailModifyMappable objMappable)
        {
            var stepmail = ErsFactory.ersStepMailFactory.GetErsStepMail();

            var repository = ErsFactory.ersStepMailFactory.GetErsStepMailRepository();
            var criteria = ErsFactory.ersStepMailFactory.GetErsStepMailCriteria();

            criteria.id = objMappable.id.Value;

            if (repository.GetRecordCount(criteria) > 0)
                stepmail = repository.Find(criteria)[0];
            else 
                throw new ErsException("10200");

            objMappable.OverwriteWithParameter(stepmail.GetPropertiesAsDictionary());
        }
    }
}