﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.StepMail.Mappers
{
    public class ItemSearchMapper:IMapper<IItemSearchMappable>
    {

        public void Map(IItemSearchMappable ObjMappable)
        {
            this.Init(ObjMappable);
        }

        /// <summary>
        /// itemリストを初期化する。
        /// </summary>
        /// <param name="pager"></param>
        internal void Init(IItemSearchMappable ObjMappable)
        {
            if (ObjMappable.pageCnt > 0)
            {
                ObjMappable.s_cate1 = ObjMappable.hid_s_cate1;
                ObjMappable.s_cate2 = ObjMappable.hid_s_cate2;
                ObjMappable.s_cate3 = ObjMappable.hid_s_cate3;
                ObjMappable.s_cate4 = ObjMappable.hid_s_cate4;
                ObjMappable.s_cate5 = ObjMappable.hid_s_cate5;
                ObjMappable.s_sname = ObjMappable.hid_s_sname;
                ObjMappable.s_scode = ObjMappable.hid_s_scode;
                ObjMappable.s_gcode = ObjMappable.hid_s_gcode;
            }
            else
            {
                ObjMappable.hid_s_cate1 = ObjMappable.s_cate1;
                ObjMappable.hid_s_cate2 = ObjMappable.s_cate2;
                ObjMappable.hid_s_cate3 = ObjMappable.s_cate3;
                ObjMappable.hid_s_cate4 = ObjMappable.s_cate4;
                ObjMappable.hid_s_cate5 = ObjMappable.s_cate5;
                ObjMappable.hid_s_sname = ObjMappable.s_sname;
                ObjMappable.hid_s_scode = ObjMappable.s_scode;
                ObjMappable.hid_s_gcode = ObjMappable.s_gcode;
            }


            //search s_master_t
            var emRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

            //検索条件をクライテリアに保存
            var emCri = this.GetCriteria(ObjMappable);

            //検索結果の総数を取得
            ObjMappable.recordCount = emRepository.GetRecordCountSkuBase(emCri);

            if (ObjMappable.recordCount > 0)
            {
                //throw new ErsException("10200");
                emCri.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
                emCri.SetOrderByGcode(Criteria.OrderBy.ORDER_BY_ASC);

                //検索SQLにLIMIT と OFFSETを加える
                ObjMappable.pager.SetLimitAndOffsetToCriteria(emCri);

                //商品出力結果
                var list = emRepository.FindSkuBaseItemList(emCri,null);
                ObjMappable.searched_items = ErsCommon.ConvertEntityListToDictionaryList(list);
            }

        }

        /// <summary>
        /// Gets criteria for searching
        /// </summary>
        /// <returns>returns ErsMerchandiseCriteria</returns>
        public ErsSkuCriteria GetCriteria(IItemSearchMappable ObjMappable)
        {
            var emCri = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //除外するgcode
            emCri.ignore_gcode = setup.IgnoreGcode;
            emCri.s_active = EnumActive.Active;

            //set parameters

            if (!string.IsNullOrEmpty(ObjMappable.s_sname))
                emCri.sname_and_gname = ObjMappable.s_sname;

            if (!string.IsNullOrEmpty(ObjMappable.s_scode))
                emCri.scode = ObjMappable.s_scode;

            if (!string.IsNullOrEmpty(ObjMappable.s_gcode))
                emCri.gcode = ObjMappable.s_gcode;

            if (ObjMappable.s_cate1 != null)
                emCri.cate1 = ObjMappable.s_cate1.Value;

            if (ObjMappable.s_cate2 != null)
                emCri.cate2 = ObjMappable.s_cate2.Value;

            if (ObjMappable.s_cate3 != null)
                emCri.cate3 = ObjMappable.s_cate3.Value;

            if (ObjMappable.s_cate4 != null)
                emCri.cate4 = ObjMappable.s_cate4.Value;

            if (ObjMappable.s_cate5 != null)
                emCri.cate5 = ObjMappable.s_cate5.Value;

            return emCri;
        }
    }
}