﻿using System;
using System.Collections.Generic;
using System.Linq;
using ersAdmin.Domain.StepMail.Mappables;
using ersAdmin.Models.stepmail;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersAdmin.Domain.StepMail.Mappers
{
    public class ScenarioModifyMapper : IMapper<IScenarioModifyMappable>
    {
        public void Map(IScenarioModifyMappable objMappable)
        {
            if (objMappable.IsLoadDefault)
            {
                this.LoadDefault(objMappable);
            }
        }

        /// <summary>
        /// Loads Values of selected step mail scenario for editing
        /// </summary>
        private void LoadDefault(IScenarioModifyMappable objMappable)
        {
            var objStepmailScenario = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioByID(objMappable.id.Value);

            objMappable.OverwriteWithParameter(objStepmailScenario.GetPropertiesAsDictionary());

            objMappable.reg_elapsed_to = (objStepmailScenario.reg_elapsed_to == ushort.MaxValue) ? null :  objStepmailScenario.reg_elapsed_to;

            //DateTime time1 = Convert.ToDateTime(objMappable.mail_delv_out_time_hh_from + ":" + objMappable.mail_delv_out_time_mm_from);
            //DateTime time2 = Convert.ToDateTime(objMappable.mail_delv_out_time_hh_to + ":" + objMappable.mail_delv_out_time_mm_to);

        }
    }
}