﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.StepMail.Mappers
{
    public class GetItemDetailMapper:IMapper<IGetItemDetailMappable>
    {
        public void Map(IGetItemDetailMappable ObjMappable)
        {
           ObjMappable.code_name =  this.getDetail(ObjMappable);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string getDetail(IGetItemDetailMappable ObjMappable)
        {
            var emRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var emCri = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            emCri.scode = ObjMappable.code;

            var list = emRepository.FindSkuBaseItemList(emCri, null);
            if (list.Count() > 0)
            {
                var objMerchandise = list.First();
                return objMerchandise.gcode + "$" + objMerchandise.scode + "$" + objMerchandise.sname + "$" + objMerchandise.s_sale_ptn;
            }

            return null;
        }
    }
}