﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.step_scenario;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.StepMail.Mappers
{
    public class DelvySummaryMapper:IMapper<IDelvySummaryMappable>
    {
        public void Map(IDelvySummaryMappable ObjMappable)
        {
            this.LoadValues(ObjMappable);
        }


        public void LoadValues(IDelvySummaryMappable ObjMappable)
        {
            var objProcess = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(ObjMappable.process_id.Value);
            this.LoadProcessValues(ObjMappable,objProcess);

            var objStepmail = ErsFactory.ersStepMailFactory.GetErsStepMailWithId(objProcess.step_mail_id);

            var objScenario = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioByID(objStepmail.scenario_id);
            this.LoadScenarioValues(ObjMappable,objScenario);
        }

        /// <summary>
        /// Gets values of process
        /// </summary>
        /// <param name="objProcess"></param>
        private void LoadProcessValues(IDelvySummaryMappable ObjMappable,ErsProcess objProcess)
        {
            ObjMappable.OverwriteWithParameter(objProcess.GetPropertiesAsDictionary());
            ObjMappable.mail_count = this.GetMailCount(objProcess);
            ObjMappable.mail_open_count = this.GetMailOpenCount(objProcess);
            ObjMappable.listClickCount = this.GetListClickCount(objProcess);
        }

        /// <summary>
        /// クリックフィードバックのリストを取得する
        /// </summary>
        /// <param name="objProcess"></param>
        /// <returns></returns>
        private List<Dictionary<string, object>> GetListClickCount(ErsProcess objProcess)
        {
            var repository = ErsFactory.ersStepMailFactory.GetErsMailUrlClickCounterRepository();
            var criteria = ErsFactory.ersStepMailFactory.GetErsMailUrlClickCounterCriteria();
            criteria.process_id = objProcess.id;
            criteria.active = EnumActive.Active;
            criteria.SetOrderByClickCount(Criteria.OrderBy.ORDER_BY_DESC);
            var list = repository.Find(criteria);
            var listResult = new List<Dictionary<string, object>>();
            foreach (var item in list)
            {
                var dictionary = item.GetPropertiesAsDictionary();
                listResult.Add(dictionary);
            }
            return listResult;
        }

        /// <summary>
        /// 配信数を取得する
        /// </summary>
        /// <param name="objProcess"></param>
        /// <returns></returns>
        private long GetMailCount(ErsProcess objProcess)
        {
            var repository = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            criteria.process_id = objProcess.id;
            criteria.active = EnumActive.Active;
            return repository.GetRecordCount(criteria);
        }

        /// <summary>
        /// 配信数を取得する
        /// </summary>
        /// <param name="objProcess"></param>
        /// <returns></returns>
        private long GetMailOpenCount(ErsProcess objProcess)
        {
            var repository = ErsFactory.ersStepMailFactory.GetErsMailOpenCounterRepository();
            var criteria = ErsFactory.ersStepMailFactory.GetErsMailOpenCounterCriteria();
            criteria.process_id = objProcess.id;
            criteria.active = EnumActive.Active;
            return repository.GetRecordCount(criteria);
        }

        /// <summary>
        /// Get values of scenario
        /// </summary>
        private void LoadScenarioValues(IDelvySummaryMappable ObjMappable,ErsStepScenario objScenario)
        {
            ObjMappable.OverwriteWithParameter(objScenario.GetPropertiesAsDictionary());
        }
    }
}