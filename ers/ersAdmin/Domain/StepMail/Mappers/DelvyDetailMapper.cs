﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.StepMail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.stepmail;
using jp.co.ivp.ers;
using jp.co.ivp.ers.step_scenario;

namespace ersAdmin.Domain.StepMail.Mappers
{
    public class DelvyDetailMapper:IMapper<IDelvyDetailMappable>
    {
        public void Map(IDelvyDetailMappable ObjMappable)
        {
            this.LoadValues(ObjMappable);
        }
        public void LoadValues(IDelvyDetailMappable ObjMappable)
        {
            var objProcess = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(ObjMappable.process_id.Value);
            this.LoadProcessValues(ObjMappable,objProcess);

            var objStepmail = ErsFactory.ersStepMailFactory.GetErsStepMailWithId(objProcess.step_mail_id);
            this.LoadStepmailValues(ObjMappable, objStepmail);

            var objScenario = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioByID(objStepmail.scenario_id);
            this.LoadScenarioValues(ObjMappable, objScenario);
        }

        /// <summary>
        /// Gets values of process
        /// </summary>
        /// <param name="objProcess"></param>
        private void LoadProcessValues(IDelvyDetailMappable ObjMappable,ErsProcess objProcess)
        {
            ObjMappable.OverwriteWithParameter(objProcess.GetPropertiesAsDictionary());
            ObjMappable.mail_count = this.GetMailCount(objProcess);
        }

        /// <summary>
        /// 配信数を取得する
        /// </summary>
        /// <param name="objProcess"></param>
        /// <returns></returns>
        private long GetMailCount(ErsProcess objProcess)
        {
            var repository = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            criteria.process_id = objProcess.id;
            criteria.active = EnumActive.Active;
            return repository.GetRecordCount(criteria);
        }

        /// <summary>
        /// Get values of scenario
        /// </summary>
        private void LoadScenarioValues(IDelvyDetailMappable objMappable, ErsStepScenario objScenario)
        {
            objMappable.OverwriteWithParameter(objScenario.GetPropertiesAsDictionary());
            objMappable.w_mail_ref_date_kbn = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.RefDate, EnumCommonNameColumnName.namename, (int)objScenario.mail_ref_date_kbn.Value);
            objMappable.w_mail_delv_time_kbn = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.DeliveryTime, EnumCommonNameColumnName.namename, (int)objScenario.mail_delv_time_kbn);

            objMappable.w_target = ErsFactory.ersViewServiceFactory.GetErsViewTargetService().GetStringFromId(objScenario.target_id);
        }

        /// <summary>
        /// Get values of scenario
        /// </summary>
        private void LoadStepmailValues(IDelvyDetailMappable objMappable,ErsStepMail objStepmail)
        {
            objMappable.OverwriteWithParameter(objStepmail.GetPropertiesAsDictionary());
            objMappable.w_elapsed_kbn = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Elapsed, EnumCommonNameColumnName.namename, (int?)objStepmail.elapsed_kbn.Value);
        }
    }
}