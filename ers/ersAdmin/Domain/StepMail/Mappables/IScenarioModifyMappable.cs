﻿using System.Collections.Generic;
using ersAdmin.Models.stepmail;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.StepMail.Mappables
{
    public interface IScenarioModifyMappable : IMappable
    {
        int? id { get; }

        bool IsLoadDefault { get; }

        int? mail_delv_out_time_hh_from { get; }

        int? mail_delv_out_time_mm_from { get; }

        int? mail_delv_out_time_hh_to { get; }

        int? mail_delv_out_time_mm_to { get; }

        int? reg_elapsed_to { get; set; }

        int? target_id { get; set; }
    }
}