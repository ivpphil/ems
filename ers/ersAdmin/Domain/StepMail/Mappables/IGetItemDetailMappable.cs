﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.StepMail.Mappables
{
    public interface IGetItemDetailMappable:IMappable
    {
        string code { get; }

        string code_name { set; }
    }
}