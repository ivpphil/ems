﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.StepMail.Mappables
{
    public interface  IItemSearchMappable :IMappable
    {
        int pageCnt { get; }

        int? s_cate1 { get; set; }

        int? s_cate2 { get; set; }

        int? s_cate3 { get; set; }

        int? s_cate4 { get; set; }

        int? s_cate5 { get; set; }

        string s_sname { get; set; }

        string s_scode { get; set; }

        string s_gcode { get; set; }

        int? hid_s_cate1 { get; set; }

        int? hid_s_cate2 { get; set; }

        int? hid_s_cate3 { get; set; }

        int? hid_s_cate4 { get; set; }

        int? hid_s_cate5 { get; set; }

        string hid_s_gcode { get; set; }

        string hid_s_scode { get; set; }

        string hid_s_sname { get; set; }

        long recordCount { get; set; }

        ErsPagerModel pager { get; set; }

        List<Dictionary<string, object>> searched_items {  set; }
    }
}