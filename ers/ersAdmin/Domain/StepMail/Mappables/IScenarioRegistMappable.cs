﻿using System.Collections.Generic;
using ersAdmin.Models.stepmail;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.StepMail.Mappables
{
    public interface IScenarioRegistMappable : IMappable
    {

        bool IsInitialization { get; }

        string mail_from_addr { get; set; }

        string mail_from_name { get; set; }

        string mail_reply_addr { get; set; }
    }
}