﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.StepMail.Mappables
{
    public interface IStepmailModifyMappable : IMappable
    {

        int? id { get; }

        bool IsLoadDefaultData { get; }

    }
}