﻿using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.StepMail.Mappables
{
    public interface IScenarioListMappable : IMappable
    {

        string s_scenario_name { get; set; }

        EnumStatus? s_mail_status_kbn { get; set; }

        List<Dictionary<string, object>> scenarioList { get; set; }

        long recordCount { get; set; }

        List<Dictionary<string, object>> scenario_Erslist { get; set; }

        int pageCnt { get; set; }

        long maxItemCount { get; }

        ErsPagerModel pager { get; set; }

    }
}