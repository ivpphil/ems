﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.StepMail.Mappables
{
    public interface  IDelvyDetailMappable:IMappable
    {
        int? process_id { get; }

        long mail_count {   set; }

        string w_elapsed_kbn { set; }

        string w_mail_ref_date_kbn {  set; }

        string w_mail_delv_time_kbn { set; }

        string w_target { get; set; }
    }
}