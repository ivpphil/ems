﻿using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.StepMail.Mappables
{
    public interface IStepmailRegistMappable : IMappable
    {
        int? scenario_id { get; }

        string scenario_name { get; set; }

        string strstatus { get; set; }

        string strmail_ref_date_kbn { get; set; }

        EnumDeliveryTime? mail_delv_time_kbn { get; set; }

        int? mail_delv_time_hh { get; set; }

        int? mail_delv_time_mm { get; set; }

        int? mail_delv_out_time_hh_from { get; set; }

        int? mail_delv_out_time_mm_from { get; set; }

        int? mail_delv_out_time_hh_to { get; set; }

        int? mail_delv_out_time_mm_to { get; set; }

        string w_target { get; set; }
    }
}