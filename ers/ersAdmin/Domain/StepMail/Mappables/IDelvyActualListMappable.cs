﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.StepMail.Mappables
{
    public interface  IDelvyActualListMappable:IMappable
    {
        long recordCount { get; set; }

        ErsPagerModel pager { get; set; }

        List<Dictionary<string, object>> searched_items { get; set; }
    }
}

