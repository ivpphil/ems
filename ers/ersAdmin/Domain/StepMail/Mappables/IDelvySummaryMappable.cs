﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.StepMail.Mappables
{
    public interface  IDelvySummaryMappable:IMappable
    {
        int? process_id { get; }

        long mail_count {   set; }

        long mail_open_count { set; }

        List<Dictionary<string, object>> listClickCount {  set; }
    }
}