﻿using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.StepMail.Mappables
{
    public interface IStepmailListMappable : IMappable
    {
        ErsPagerModel pager { get; }

        int? scenario_id { get; }

        List<Dictionary<string, object>> StepMailList { get; set; }

        long recordCount { get; set; }

        int pageCnt { get; set; }

        long maxItemCount { get; }
    }
}