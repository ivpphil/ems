﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.StepMail.Commands
{
    public interface IStepmailRegistCommand : ICommand
    {
        string step_mail_name { get; }
    }
}