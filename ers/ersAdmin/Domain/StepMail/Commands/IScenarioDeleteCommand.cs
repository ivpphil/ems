﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.StepMail.Commands
{
    public interface IScenarioDeleteCommand : ICommand
    {

        int? id { get; }

        bool submit_delete { get; }

    }
}