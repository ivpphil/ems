﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.StepMail.Commands
{
    public interface IStepmailDeleteCommand : ICommand
    {

        int? id { get; }

    }
}