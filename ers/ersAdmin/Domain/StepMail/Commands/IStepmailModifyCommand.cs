﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.StepMail.Commands
{
    public interface IStepmailModifyCommand : ICommand
    {
        int? id { get; }

        string step_mail_name { get; set; }
    }
}