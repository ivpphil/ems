﻿using System.Collections.Generic;
using ersAdmin.Models.stepmail;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.StepMail.Commands
{
    public interface IScenarioRegistCommand : ICommand
    {
        int? id { get; }

        bool submit_register { get; set; }

        bool submit_update { get; set; }

        EnumReferenceDate? mail_ref_date_kbn { get; set; }

        EnumDeliveryTime? mail_delv_time_kbn { get; set; }

        int? reg_elapsed_from { get; set; }

        int? reg_elapsed_to { get; set; }

        int? mail_delv_out_time_hh_from { get; set; }

        int? mail_delv_out_time_mm_from { get; set; }

        int? mail_delv_out_time_hh_to { get; set; }

        int? mail_delv_out_time_mm_to { get; set; }

        int? target_id { get; set; }
    }
}