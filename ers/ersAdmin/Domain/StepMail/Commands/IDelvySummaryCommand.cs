﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.StepMail.Commands
{
    public interface  IDelvySummaryCommand:ICommand
    {
        int? process_id { get; }
    }
}