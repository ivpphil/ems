﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Lp.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.lp;

namespace ersAdmin.Domain.Lp.Mappers
{
    public class LpPageRegistTemplateMapper
        : IMapper<ILpPageRegistTemplateMappable>
    {
        public void Map(ILpPageRegistTemplateMappable objMappable)
        {
            this.CheckTemplate(objMappable);
        }

        public virtual void CheckTemplate(ILpPageRegistTemplateMappable objMappable)
        {
            var ContentsRepository = ErsFactory.ersLpFactory.GetErsLpPageTypeRepository();
            var ContentsCriteria = ErsFactory.ersLpFactory.GetErsLpPageTypeCriteria();
            ContentsCriteria.page_type_code = objMappable.page_type_code;
            var list = ContentsRepository.Find(ContentsCriteria);
            if (list.Count == 0)
            {
                return;
            }

            var available_template = list[0].available_template;

            var TemplateRepository = ErsFactory.ersLpFactory.GetErsLpTemplateRepository();
            var TemplateCriteria = ErsFactory.ersLpFactory.GetErsLpTemplateCriteria();
            TemplateCriteria.template_code_in = available_template;
            TemplateCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var retList = TemplateRepository.Find(TemplateCriteria);

            objMappable.template = new List<ErsLpTemplate>();
            for (int i = 0; i < available_template.Length; i++)
            {
                if (retList[i].active == EnumActive.Active)
                    objMappable.template.Add(retList[i]);
            }
        }
    }
}