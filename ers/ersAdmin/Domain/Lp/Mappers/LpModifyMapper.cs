﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Lp.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Lp.Mappers
{
    public class LpModifyMapper
        : LpRegistMapper, IMapper<ILpModifyMappable>
    {
        public void Map(ILpModifyMappable objMappable)
        {
            if (objMappable.IsConfirmationPage)
            {
                this.OverwriteListQuestionnaire(objMappable);
            }
            else
            {
                this.LoadErsLpPageManage(objMappable);

                this.LoadListQuestionnaire(objMappable);

                this.LoadRegisterdQuestionnaire(objMappable);
            }

            this.LoadLpPages(objMappable);
        }

        private void LoadErsLpPageManage(ILpModifyMappable objMappable)
        {
            var objErsLpPageManage = ErsFactory.ersLpFactory.GetErsLpPageManageWithIdForAdmin(objMappable.id);
            objMappable.OverwriteWithParameter(objErsLpPageManage.GetPropertiesAsDictionary());
            objMappable.site_id = objErsLpPageManage.site_id;
            objMappable.old_logo_image_file = objErsLpPageManage.logo_image_file;
        }

        private void LoadRegisterdQuestionnaire(ILpModifyMappable objMappable)
        {
            if (objMappable.listQuestionnaire != null)
            {
                foreach (var questionnaireRecord in objMappable.listQuestionnaire)
                {
                    var objLpQuestionnaire = ErsFactory.ersLpFactory.GetErsLpQuestionnaireWithItemCode(objMappable.id, questionnaireRecord.item_code);
                    if (objLpQuestionnaire == null)
                    {
                        continue;
                    }
                    questionnaireRecord.OverwriteWithParameter(objLpQuestionnaire.GetPropertiesAsDictionary());
                }
            }
        }

        private void LoadLpPages(ILpModifyMappable objMappable)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageTypeRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageTypeCriteria();
            criteria.active = EnumActive.Active;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listLpPageTypeRecords = repository.Find(criteria);

            var listLpPageType = new List<Dictionary<string, object>>();
            foreach (var objLpPageType in listLpPageTypeRecords)
            {
                if (objLpPageType.page_type_code == EnumLpPageTypeCode.Upsell.ToString() && 
                    objMappable.basic_stgy_kbn != EnumLpBasicStgy.UpSell)
                {
                    //Upsellの戦略じゃない場合はUpsellをスキップ
                    continue;
                }
                var dictionary = objLpPageType.GetPropertiesAsDictionary();
                dictionary["template_code"] = this.CheckLpPageRegistered(objMappable.id, objLpPageType.page_type_code);
                listLpPageType.Add(dictionary);
            }
            objMappable.listLpPageType = listLpPageType;
        }

        private string CheckLpPageRegistered(int? lp_page_manage_id, string page_type_code)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageRepository();

            var criteria = ErsFactory.ersLpFactory.GetErsLpPageCriteria();
            criteria.lp_page_manage_id = lp_page_manage_id;
            criteria.page_type_code = page_type_code;
            criteria.active = EnumActive.Active;

            var listRecord = repository.Find(criteria);
            if (listRecord.Count == 0)
            {
                return null;
            }

            return listRecord.First().template_code;
        }
    }
}