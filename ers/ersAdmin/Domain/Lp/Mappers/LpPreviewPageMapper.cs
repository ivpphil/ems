﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Lp.Mappables;
using jp.co.ivp.ers;
using System.Reflection;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.lp;
using ersAdmin.Models.lp;

namespace ersAdmin.Domain.Lp.Mappers
{
    public class LpPreviewPageMapper
        : IMapper<ILpPreviewPageMappable>
    {
        public void Map(ILpPreviewPageMappable objMappable)
        {
            objMappable.OverwriteWithParameter(objMappable.objLpRegist.GetPropertiesAsDictionary());
            
            this.SetPreviewProperties(objMappable);
            this.SetHasUpSellRegistered(objMappable);
            this.SaveTempImage(objMappable);
        }


        protected void SetPreviewProperties(ILpPreviewPageMappable objMappable)
        {
            var objLpTemplate = ErsFactory.ersLpFactory.GetErsLpTemplateWithTemplateCode(objMappable.template_code);
            objMappable.template_file_path = String.Format("lp_template/{0}", objLpTemplate.template_file_path);
            objMappable.partial_preview_page = this.GetPreviewViewPathPage(objMappable);

            this.SetupItemCodeProperties(objMappable);
            this.SetQuestionnaireDetailItems(objMappable);
            this.SetErsLpPage(objMappable);

            if (!objMappable.IsLandingPage)
                this.SetLandingPageSname(objMappable);

            objMappable.temp_logo_image_file = (objMappable.objLpRegist.old_logo_image_file != objMappable.objLpRegist.logo_image_file);

        }

        protected string GetPreviewViewPathPage(ILpPreviewPageMappable objMappable)
        {
            string view_path_format = @"../lp/{0}.htm";

            switch (objMappable.page_type_code)
            {
                case EnumLpPageTypeCode.Sell:
                case EnumLpPageTypeCode.Upsell:
                    return String.Format(view_path_format, "lp");
                case EnumLpPageTypeCode.Confirm:
                    return String.Format(view_path_format, "lp_confirm");
                case EnumLpPageTypeCode.Complete:
                    return String.Format(view_path_format, "lp_complete");
            }

            return string.Empty;
        }

        protected void SetupItemCodeProperties(ILpPreviewPageMappable objMappable)
        {
            objMappable.item_code_name = this.GetLpQuestionnaireByItemCode(objMappable, "name");
            objMappable.item_code_lname = this.GetLpQuestionnaireByItemCode(objMappable, "lname");
            objMappable.item_code_email = this.GetLpQuestionnaireByItemCode(objMappable, "email");
            objMappable.item_code_email_confirm = this.GetLpQuestionnaireByItemCode(objMappable, "email_confirm");
            objMappable.item_code_tel = this.GetLpQuestionnaireByItemCode(objMappable, "tel");
            objMappable.item_code_fax = this.GetLpQuestionnaireByItemCode(objMappable, "fax");
            objMappable.item_code_zip = this.GetLpQuestionnaireByItemCode(objMappable, "zip");
            objMappable.item_code_pref = this.GetLpQuestionnaireByItemCode(objMappable, "pref");
            objMappable.item_code_address = this.GetLpQuestionnaireByItemCode(objMappable, "address");
            objMappable.item_code_taddress = this.GetLpQuestionnaireByItemCode(objMappable, "taddress");
            objMappable.item_code_maddress = this.GetLpQuestionnaireByItemCode(objMappable, "maddress");
            objMappable.item_code_birth = this.GetLpQuestionnaireByItemCode(objMappable, "birth");
            objMappable.item_code_monitor = this.GetLpQuestionnaireByItemCode(objMappable, "monitor");

        }

        internal Dictionary<string, object> GetLpQuestionnaireByItemCode(ILpPreviewPageMappable objMappable, string item_code)
        {
            if (objMappable.objLpRegist != null && objMappable.objLpRegist.listQuestionnaire != null)
            {
                var recordList = objMappable.objLpRegist.listQuestionnaire;

                var objLpQuestionnaireDetail = recordList.Find((lp_q) => lp_q.item_code == item_code);

                return objLpQuestionnaireDetail.GetPropertiesAsDictionary();
            }

            return null;
        }

        protected void SetQuestionnaireDetailItems(ILpPreviewPageMappable objMappable)
        {
            var new_detail_items = new List<QuestionnaireRecord>();

            if (objMappable.objLpRegist != null && objMappable.objLpRegist.listQuestionnaire != null)
            {
                var recordList = objMappable.objLpRegist.listQuestionnaire;
                var questionnaireList = recordList.FindAll((lpRecord) => lpRecord.is_used == EnumUse.Use && lpRecord.is_system_required == EnumCmsFieldType.Hide);

                foreach (var record in questionnaireList)
                {
                    var recordClone = record.Clone() as QuestionnaireRecord;
                    recordClone.path_template_name = String.Format("lp_input/{0}.htm", record.template_name);

                    new_detail_items.Add(recordClone);
                }
            }

            objMappable.questionnaireDetailItems = new_detail_items;
        }

        protected Dictionary<string, object> GetDefaultLpQuestionnaire(ILpPreviewPageMappable objMappable, string item_code)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpQuestionnaireSetupRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpQuestionnaireSetupCriteria();

            if (repository.GetRecordCount(criteria) != 1)
                return null;

            return repository.FindSingle(criteria).GetPropertiesAsDictionary();
        }

        protected void SetErsLpPage(ILpPreviewPageMappable objMappable)
        {
            objMappable.objLpPage = ErsFactory.ersLpFactory.GetErsLpPage();

            foreach (var record in objMappable.listBlock)
            {
                ErsExpressionAccessor<ErsLpPage, string>.SetPropertyValue(objMappable.objLpPage, "block_" + record.lineNumber + "_head", record.head);
                ErsExpressionAccessor<ErsLpPage, string>.SetPropertyValue(objMappable.objLpPage, "block_" + record.lineNumber + "_body", record.body);
                ErsExpressionAccessor<ErsLpPage, string>.SetPropertyValue(objMappable.objLpPage, "block_" + record.lineNumber + "_free", record.free);

            }
        }

        protected void SetLandingPageSname(ILpPreviewPageMappable objMappable)
        {
            var objLpRegist = objMappable.objLpRegist;

            var upsell_sku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(objLpRegist.upsell_scode);

            if (upsell_sku != null)
                objMappable.upsell_sname = upsell_sku.sname;

            if (objMappable.upsell_stgy_kbn == EnumLpUpsellStgy.Combination)
            {
                var sku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(objLpRegist.sell_scode);

                if (sku != null)
                    objMappable.sname = sku.sname;
            }
        }

        protected void SetHasUpSellRegistered(ILpPreviewPageMappable objMappable)
        {
            if (objMappable.objLpRegist.basic_stgy_kbn == EnumLpBasicStgy.UpSell)
            {
                var spec = ErsFactory.ersLpFactory.GetSearchLandingPageMangeHasUpSellSpec();
                var criteria = ErsFactory.ersLpFactory.GetErsLpPageManageCriteria();

                criteria.id = objMappable.objLpRegist.id;
                criteria.basic_stgy_kbn = (int)objMappable.objLpRegist.basic_stgy_kbn;
                criteria.active = (int)EnumActive.Active;

                criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
                objMappable.HasUpSellRegistered = spec.GetCountData(criteria) == 1;
            }
        }

        protected void SaveTempImage(ILpPreviewPageMappable objMappable)
        {

            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();

            if (objMappable.upsell_button_1_file != null)
            {
                if (objMappable.upsell_button_1_file.ContentLength != 0)
                {
                    objMappable.upsell_button_1_file_name = GetImageFileName(objMappable.upsell_button_1_file);
                    objMappable.temp_upsell_button_1_file_name = true;
                    uploadedFileHelper.SaveTempFile(objMappable.upsell_button_1_file, setup.image_temp_directory, objMappable.upsell_button_1_file_name);
                }
            }
            if (objMappable.upsell_button_2_file != null)
            {
                if (objMappable.upsell_button_2_file.ContentLength != 0)
                {
                    objMappable.upsell_button_2_file_name = GetImageFileName(objMappable.upsell_button_2_file);
                    objMappable.temp_upsell_button_2_file_name = true;
                    uploadedFileHelper.SaveTempFile(objMappable.upsell_button_2_file, setup.image_temp_directory, objMappable.upsell_button_2_file_name);
                }
            }
            if (objMappable.upsell_button_3_file != null)
            {
                if (objMappable.upsell_button_3_file.ContentLength != 0)
                {
                    objMappable.upsell_button_3_file_name = GetImageFileName(objMappable.upsell_button_3_file);
                    objMappable.temp_upsell_button_3_file_name = true;
                    uploadedFileHelper.SaveTempFile(objMappable.upsell_button_3_file, setup.image_temp_directory, objMappable.upsell_button_3_file_name);
                }
            }

        }            

        protected string GetImageFileName(HttpPostedFileBase upsell_button_file)
        {
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();
            var fileName = uploadedFileHelper.GetTempFileName(upsell_button_file);
            return fileName;
        }

    }
}