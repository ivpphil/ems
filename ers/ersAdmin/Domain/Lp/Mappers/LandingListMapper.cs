﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Lp.Mappables;
using jp.co.ivp.ers.lp;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Lp.Mappers
{
    public class LandingListMapper
        : SiteSearchBaseMapper, IMapper<ILandingListMappable>
    {
        public void Map(ILandingListMappable objMappable)
        {
            this.SearchLp(objMappable);
        }

        public void SearchLp(ILandingListMappable objMappable)
        {
            var rlp = ErsFactory.ersLpFactory.GetErsLpPageManageRepository();
            var clp = SetManageCriteria(objMappable);
            objMappable.recordCount = rlp.GetRecordCount(clp);

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(clp);
            }

            if (objMappable.sort == 1)//◆case1: sort change by campaign name.
            {
                clp.SetOrderByLpGroupName(Criteria.OrderBy.ORDER_BY_ASC);
                clp.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);
                clp.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);
            }
            else if (objMappable.sort == 2)//◆case2: sort change by published start date
            {
                clp.SetOrderByPublicStartDate(Criteria.OrderBy.ORDER_BY_DESC);
                clp.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);
                clp.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);
            }
            else if (objMappable.sort == 3)//◆case1: sort change by published end date
            {
                clp.SetOrderByPublicEndDate(Criteria.OrderBy.ORDER_BY_DESC);
                clp.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);
                clp.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);
            }
            else
            {
                clp.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);
                clp.SetOrderByPageName(Criteria.OrderBy.ORDER_BY_ASC);
                clp.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            }

            var spec = ErsFactory.ersLpFactory.GetSearchDQuestionnaireWithRelatedLpSpec();

            var listManage = rlp.Find(clp);
            var list = new List<Dictionary<string, object>>();
            foreach (var objManage in listManage)
            {
                var dictionary = objManage.GetPropertiesAsDictionary();
                if (objManage.active == EnumActive.NonActive)
                {
                    dictionary["status"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ArticleState, EnumCommonNameColumnName.namename, (int)EnumArticleState.NonActive);
                }
                else if (objManage.public_st_date > DateTime.Now)
                {
                    dictionary["status"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ArticleState, EnumCommonNameColumnName.namename, (int)EnumArticleState.ActiveWait);
                }
                else if (objManage.public_ed_date < DateTime.Now)
                {
                    dictionary["status"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ArticleState, EnumCommonNameColumnName.namename, (int)EnumArticleState.OutsidePeriod);
                }
                else
                {
                    dictionary["status"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ArticleState, EnumCommonNameColumnName.namename, (int)EnumArticleState.Active);
                }

                var criteria = ErsFactory.ersLpFactory.GetErsDQuestionnaireCriteria();
                criteria.page_id = objManage.id;

                dictionary["hasDownload"] = spec.GetCountData(criteria) > 0;

                list.Add(dictionary);
            }
            objMappable.list = list;
        }

        public ErsLpPageManageCriteria SetManageCriteria(ILandingListMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var clp = ErsFactory.ersLpFactory.GetErsLpPageManageCriteria();

            clp.ignore_gcode = setup.IgnoreGcode;

            if (!string.IsNullOrEmpty(objMappable.s_freeword))
            {
                clp.JoinWithOrPageNameAndCcode = objMappable.s_freeword;
            }

            if (!string.IsNullOrEmpty(objMappable.s_group_name))
            {
                if (!string.IsNullOrEmpty(objMappable.s_group_name))
                {
                    clp.lp_group_name_like = objMappable.s_group_name;
                    clp.lp_group_name = objMappable.s_group_name;
                }
                
            }

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, clp, "lp_page_manage_t");

            return clp;
        }
    }
}