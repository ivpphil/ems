﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Lp.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Lp.Mappers
{
    public class DownloadCsvLpDQuestMapper
        : IMapper<IDownloadCsvLpDQuestMappable>
    {
        public void Map(IDownloadCsvLpDQuestMappable objMappable)
        {
            this.Init(objMappable);
        }

        protected virtual void Init(IDownloadCsvLpDQuestMappable objMappable)
        {
            var spec = ErsFactory.ersLpFactory.GetSearchDQuestionnaireWithRelatedLpSpec();
            var criteria = ErsFactory.ersLpFactory.GetErsDQuestionnaireCriteria();
            criteria.page_id = objMappable.page_id;
            criteria.value_not_eq = null;

            if (spec.GetCountData(criteria) == 0)
                throw new ErsException("10200");

            criteria.SetOrderByPageId(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByDno(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByLpQuestId(Criteria.OrderBy.ORDER_BY_ASC);

            var dQuestList = spec.GetSearchData(criteria);

            //Create download file
            this.CreateCsvFile(objMappable, dQuestList);

        }

        protected virtual void CreateCsvFile(IDownloadCsvLpDQuestMappable objMappable, IList<Dictionary<string, object>> dQuestList)
        {
            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            ersAdmin.Models.csv.lp_dques_csv_record item_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<ersAdmin.Models.csv.lp_dques_csv_record>(writer);
                foreach (var item in dQuestList)
                {
                    item_csv = new ersAdmin.Models.csv.lp_dques_csv_record();
                    item_csv.OverwriteWithParameter(item);
                    objMappable.csvCreater.WriteBody(item_csv, writer);
                }
            }
        }
    }
}