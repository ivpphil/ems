﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Lp.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.lp;
using ersAdmin.Models.lp;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Lp.Mappers
{
    public class LpPageRegistMapper
        : IMapper<ILpPageRegistMappable>
    {
        public const int blockCount = 10;

        public void Map(ILpPageRegistMappable objMappable)
        {
            if (objMappable.IsInitialize)
            {
                this.LoadInitialValue(objMappable);
            }

            this.CheckDisplay(objMappable);
        }

        internal void LoadInitialValue(ILpPageRegistMappable objMappable)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageCriteria();
            criteria.lp_page_manage_id = objMappable.lp_page_manage_id;
            criteria.page_type_code = objMappable.page_type_code.ToString();
            IList<ErsLpPage> retList = repository.Find(criteria);
            
            var listBlock = new List<lp_page_regist_block>();
            if (retList.Count == 0)
            {
                //空のブロックを作成して終了
                for (var i = 0; i < blockCount; i++)
                {
                    var newBlock = new lp_page_regist_block();
                    newBlock.lineNumber = i + 1;
                    listBlock.Add(newBlock);
                }
            }
            else
            {
                objMappable.OverwriteWithParameter(retList[0].GetPropertiesAsDictionary());
                objMappable.lp_page_id = retList[0].id;
                objMappable.hasRegistered = true;

                var objLpPage = retList.First();

                for (var i = 0; i < blockCount; i++)
                {
                    var newBlock = new lp_page_regist_block();
                    newBlock.lineNumber = i + 1;
                    newBlock.head = ErsExpressionAccessor<ErsLpPage, string>.GetPropertyValue(objLpPage, "block_" + newBlock.lineNumber + "_head");
                    newBlock.body = ErsExpressionAccessor<ErsLpPage, string>.GetPropertyValue(objLpPage, "block_" + newBlock.lineNumber + "_body");
                    newBlock.free = ErsExpressionAccessor<ErsLpPage, string>.GetPropertyValue(objLpPage, "block_" + newBlock.lineNumber + "_free");
                    listBlock.Add(newBlock);
                }
            }
            objMappable.listBlock = listBlock;
        }

        internal virtual void CheckDisplay(ILpPageRegistMappable objMappable)
        {
            var ContentsRepository = ErsFactory.ersLpFactory.GetErsLpPageTypeRepository();
            var ContentsCriteria = ErsFactory.ersLpFactory.GetErsLpPageTypeCriteria();
            ContentsCriteria.page_type_code = objMappable.page_type_code;
            IList<ErsLpPageType> clist = ContentsRepository.Find(ContentsCriteria);
            if (clist.Count > 0)
            {
                var objLpPageType = clist.First();

                for (var i = 0; i < blockCount; i++)
                {
                    var objBlock = objMappable.listBlock.First((record) => record.lineNumber == i + 1);
                    objBlock.head_name = ErsExpressionAccessor<ErsLpPageType, string>.GetPropertyValue(objLpPageType, "block_" + objBlock.lineNumber + "_head_name");
                    objBlock.body_name = ErsExpressionAccessor<ErsLpPageType, string>.GetPropertyValue(objLpPageType, "block_" + objBlock.lineNumber + "_body_name");
                    objBlock.free_name = ErsExpressionAccessor<ErsLpPageType, string>.GetPropertyValue(objLpPageType, "block_" + objBlock.lineNumber + "_free_name");
                }
            }

            var objLpTemplate = ErsFactory.ersLpFactory.GetErsLpTemplateWithTemplateCode(objMappable.template_code);
            if (objLpTemplate != null)
            {
                objMappable.template_file_path = objLpTemplate.template_file_path;
                objMappable.template_img_file_path = objLpTemplate.template_img_file_path;

                for (var i = 0; i < blockCount; i++)
                {
                    var objBlock = objMappable.listBlock.First((record) => record.lineNumber == i + 1);
                    objBlock.head_use_flg = ErsExpressionAccessor<ErsLpTemplate, EnumCmsFieldType?>.GetPropertyValue(objLpTemplate, "block_" + objBlock.lineNumber + "_head_use_flg");
                    objBlock.body_use_flg = ErsExpressionAccessor<ErsLpTemplate, EnumCmsFieldType?>.GetPropertyValue(objLpTemplate, "block_" + objBlock.lineNumber + "_body_use_flg");
                    objBlock.free_use_flg = ErsExpressionAccessor<ErsLpTemplate, EnumCmsFieldType?>.GetPropertyValue(objLpTemplate, "block_" + objBlock.lineNumber + "_free_use_flg");
                }
            }
        }

    }
}