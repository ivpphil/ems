﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Lp.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ersAdmin.Models.lp;

namespace ersAdmin.Domain.Lp.Mappers
{
    public class LpRegistMapper
        : IMapper<ILpRegistMappable>
    {
        public void Map(ILpRegistMappable objMappable)
        {
            if (objMappable.IsConfirmationPage)
            {
                this.OverwriteListQuestionnaire(objMappable);
            }
            else
            {
                this.LoadListQuestionnaire(objMappable);
            }
        }

        protected void OverwriteListQuestionnaire(ILpRegistMappable objMappable)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpQuestionnaireSetupRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpQuestionnaireSetupCriteria();
            criteria.active = EnumActive.Active;
            criteria.SetOrderByDisp_order(Criteria.OrderBy.ORDER_BY_ASC);

            var listQuestionnaireSetup = repository.Find(criteria);

            if (objMappable.listQuestionnaire != null)
            {
                foreach (var objQuestionnaireSetup in listQuestionnaireSetup)
                {
                    var listRecord = objMappable.listQuestionnaire.FindAll((pRecord) => pRecord.item_code == objQuestionnaireSetup.item_code);
                    if (listRecord.Count() != 1)
                    {
                        continue;
                    }
                    var record = listRecord.First();
                    record.OverwriteWithParameter(objQuestionnaireSetup.GetPropertiesAsDictionary(), "confirm");
                }
            }
        }

        protected void LoadListQuestionnaire(ILpRegistMappable objMappable)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpQuestionnaireSetupRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpQuestionnaireSetupCriteria();
            criteria.active = EnumActive.Active;
            criteria.SetOrderByDisp_order(Criteria.OrderBy.ORDER_BY_ASC);

            var listQuestionnaireSetup = repository.Find(criteria);

            var listQuestionnaire = new List<QuestionnaireRecord>();

            foreach (var objQuestionnaireSetup in listQuestionnaireSetup)
            {
                var record = new QuestionnaireRecord();
                record.OverwriteWithParameter(objQuestionnaireSetup.GetPropertiesAsDictionary());
                if (objQuestionnaireSetup.is_system_required != EnumCmsFieldType.Hide)
                {
                    record.is_used = EnumUse.Use;
                    if (objQuestionnaireSetup.is_system_required == EnumCmsFieldType.Required)
                    {
                        record.is_required = EnumRequired.Required;
                    }
                }
                listQuestionnaire.Add(record);
            }

            objMappable.listQuestionnaire = listQuestionnaire;
        }
    }
}