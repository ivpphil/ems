﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Lp.Commands
{
    public interface ILpModifyCommand
        : ICommand, ILpRegistCommand
    {
        int? id { get; set; }

        string old_logo_image_file { get; set; }

        EnumActive? active { get; set; }
    }
}