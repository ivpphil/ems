﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Lp.Commands
{
    public interface ILpPageDeleteCommand
        : ICommand
    {
        int? lp_page_manage_id { get; set; }

        EnumLpPageTypeCode page_type_code { get; set; }
    }
}