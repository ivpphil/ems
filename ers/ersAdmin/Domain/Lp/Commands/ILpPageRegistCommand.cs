﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.lp;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Lp.Commands
{
    public interface ILpPageRegistCommand
        : ICommand
    {
        List<lp_page_regist_block> listBlock { get; set; }

        string template_code { get; set; }

        int? lp_page_manage_id { get; set; }

        int? lp_page_id { get; set; }

        EnumLpPageTypeCode page_type_code { get; set; }

        HttpPostedFileBase upsell_button_1_file { get; set; }

        string upsell_button_1_file_name { get; set; }

        HttpPostedFileBase upsell_button_2_file { get; set; }

        string upsell_button_2_file_name { get; set; }

        HttpPostedFileBase upsell_button_3_file { get; set; }

        string upsell_button_3_file_name { get; set; }

        EnumCmsFieldType? upsell_button_1_use_flg { get; set; }

        EnumCmsFieldType? upsell_button_2_use_flg { get; set; }

        EnumCmsFieldType? upsell_button_3_use_flg { get; set; }

        
        int? del_upsell_button_2_flg { get; set; }
        
        int? del_upsell_button_3_flg { get; set; }


        bool IsInitialize { get; set; }

        bool modAdd { get; set; }
    }
}