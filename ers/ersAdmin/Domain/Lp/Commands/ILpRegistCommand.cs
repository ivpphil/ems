﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.lp;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Lp.Commands
{
    public interface ILpRegistCommand
        : ISiteRegisterBaseCommand, ICommand
    {
        bool lpregist_base { get; set; }

        bool lpregist_detail { get; set; }

        bool lpregist_questionnaire { get; set; }

        bool lpregist_confirm { get; set; }

        string ccode { get; set; }

        List<QuestionnaireRecord> listQuestionnaire { get; set; }

        DateTime? public_st_date { get; set; }

        DateTime? public_ed_date { get; set; }

        EnumLpBasicStgy? basic_stgy_kbn { get; set; }

        EnumUse sell_discount_flg { get; set; }

        EnumOrderType? sell_order_type { get; set; }

        EnumUse upsell_discount_flg { get; set; }

        EnumOrderType? upsell_order_type { get; set; }

        EnumUse coupon_flg { get; set; }

        string coupon_code { get; }

        EnumUse carriage_free_flg { get; set; }

        bool IsSaveTempImage { get; set; }

        HttpPostedFileBase logo_image { get; set; }

        string logo_image_file { get; set; }

        string lp_group_name { get; set; }

        EnumLpPersonalInfo? personal_info_kbn { get; set; }

        int? sell_price { get; set; }
    }
}