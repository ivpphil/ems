﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Lp.Mappables
{
    public interface IDownloadCsvLpDQuestMappable
        : IMappable
    {
        int? page_id { get; }

        ErsCsvCreater csvCreater { get; set; }
    }
}