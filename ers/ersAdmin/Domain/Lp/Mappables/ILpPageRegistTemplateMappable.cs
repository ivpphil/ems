﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Lp.Mappables
{
    public interface ILpPageRegistTemplateMappable
        : IMappable
    {
        EnumLpPageTypeCode page_type_code { get; set; }

        List<ErsLpTemplate> template { get; set; }
    }
}