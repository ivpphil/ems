﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Lp.Mappables
{
    public interface ILpModifyMappable
        : IMappable, ILpRegistMappable
    {
        int? id { get; set; }

        string old_logo_image_file { get; set; }

        EnumLpBasicStgy? basic_stgy_kbn { get; set; }

        IList<Dictionary<string, object>> listLpPageType { get; set; }
    }
}