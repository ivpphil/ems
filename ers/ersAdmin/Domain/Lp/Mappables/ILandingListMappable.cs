﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers.mvc.pager;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Lp.Mappables
{
    public interface ILandingListMappable
        : ISiteSearchBaseMappable, IMappable
    {
        ErsPagerModel pager { get; set; }

        long recordCount { get; set; }

        int? sort { get; set; }

        IList<Dictionary<string, object>> list { get; set; }

        string s_freeword { get; set; }

        string s_group_name { get; set; }
    }
}