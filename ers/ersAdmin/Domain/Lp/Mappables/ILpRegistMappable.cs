﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.lp;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Lp.Mappables
{
    public interface ILpRegistMappable
        : ISiteRegisterBaseMappable, IMappable
    {
        List<QuestionnaireRecord> listQuestionnaire { get; set; }

        bool IsConfirmationPage { get; set; }
    }
}