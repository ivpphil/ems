﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.lp;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Lp.Mappables
{
    public interface ILpPreviewPageMappable
        : IMappable
    {
        EnumLpPageTypeCode page_type_code { get; }

        string partial_preview_page { set; }

        lp_regist objLpRegist { get; }

        ErsLpPage objLpPage { get; set; }

        List<lp_page_regist_block> listBlock { get; }

        string template_file_path { set; }

        string template_code { get; }

        Dictionary<string, object> item_code_name { get; set; }
        Dictionary<string, object> item_code_lname { get; set; }
        Dictionary<string, object> item_code_email { get; set; }
        Dictionary<string, object> item_code_email_confirm { get; set; }
        Dictionary<string, object> item_code_tel { get; set; }
        Dictionary<string, object> item_code_fax { get; set; }
        Dictionary<string, object> item_code_zip { get; set; }
        Dictionary<string, object> item_code_pref { get; set; }
        Dictionary<string, object> item_code_address { get; set; }
        Dictionary<string, object> item_code_taddress { get; set; }
        Dictionary<string, object> item_code_maddress { get; set; }
        Dictionary<string, object> item_code_birth { get; set; }
        Dictionary<string, object> item_code_monitor { get; set; }

        List<QuestionnaireRecord> questionnaireDetailItems { get; set; }

        string upsell_sname { set; }

        string sname { set; }

        bool IsLandingPage { get; }

        EnumLpUpsellStgy? upsell_stgy_kbn { get; }

        bool HasUpSellRegistered { set; }

        HttpPostedFileBase upsell_button_3_file { get; set; }

        HttpPostedFileBase upsell_button_1_file { get; set; }

        HttpPostedFileBase upsell_button_2_file { get; set; }

        string upsell_button_1_file_name { get; set; }       

        string upsell_button_2_file_name { get; set; }

        string upsell_button_3_file_name { get; set; }

        bool temp_upsell_button_1_file_name { set; }

        bool temp_upsell_button_2_file_name { set; }

        bool temp_upsell_button_3_file_name { set; }

        bool temp_logo_image_file { set; }

    }
}