﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Lp.Mappables
{
    public interface ILpPageRegistMappable
        : IMappable
    {

        int? lp_page_manage_id { get; set; }

        EnumLpPageTypeCode page_type_code { get; set; }

        string template_code { get; set; }

        bool IsInitialize { get; set; }

        List<Models.lp.lp_page_regist_block> listBlock { get; set; }

        string template_file_path { get; set; }

        string template_img_file_path { get; set; }

        bool hasRegistered { get; set; }

        string upsell_button_1_file_name { get; set; }

        string upsell_button_2_file_name { get; set; }

        string upsell_button_3_file_name { get; set; }

        int? lp_page_id { get; set; }
    }
}