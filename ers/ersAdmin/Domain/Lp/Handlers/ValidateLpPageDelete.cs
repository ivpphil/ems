﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class ValidateLpPageDelete
        : IValidationHandler<ILpPageDeleteCommand>
    {
        public IEnumerable<ValidationResult> Validate(ILpPageDeleteCommand command)
        {
            yield return command.CheckRequired("lp_page_manage_id");
            yield return command.CheckRequired("page_type_code");
        }
    }
}