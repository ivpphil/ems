﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.lp;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class ValidateLpPageRegist
        : IValidationHandler<ILpPageRegistCommand>
    {
        public IEnumerable<ValidationResult> Validate(ILpPageRegistCommand command)
        {
            var objLpTemplate = ErsFactory.ersLpFactory.GetErsLpTemplateWithTemplateCode(command.template_code);

            if (objLpTemplate == null)
            {
                yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "template_code" });
                yield break;
            }

            command.upsell_button_1_use_flg = objLpTemplate.upsell_button_1_use_flg;
            command.upsell_button_2_use_flg = objLpTemplate.upsell_button_2_use_flg;
            command.upsell_button_3_use_flg = objLpTemplate.upsell_button_3_use_flg;

            if (command.modAdd)
            {
                if (command.upsell_button_1_use_flg == EnumCmsFieldType.Required)
                {
                    if (command.upsell_button_1_file_name == "" && command.upsell_button_1_file.FileName == "")
                    {
                        {
                            yield return command.CheckRequired("upsell_button_1_file_name");
                        }
                    }
                }
                if (command.upsell_button_2_use_flg == EnumCmsFieldType.Required)
                {
                    if (command.upsell_button_2_file_name == ""  && command.upsell_button_2_file.FileName == "")
                    {
                        {
                            yield return command.CheckRequired("upsell_button_2_file_name");
                        }
                    }
                }
                if (command.upsell_button_3_use_flg == EnumCmsFieldType.Required)
                {
                    if (command.upsell_button_3_file_name == ""  && command.upsell_button_3_file.FileName == "")
                    {
                        {
                            yield return command.CheckRequired("upsell_button_3_file_name");
                        }
                    }
                }
            }

            if (command.listBlock != null)
            {
                foreach (var record in command.listBlock)
                {
                    var head_use_flg = ErsExpressionAccessor<ErsLpTemplate, EnumCmsFieldType?>.GetPropertyValue(objLpTemplate, "block_" + record.lineNumber + "_head_use_flg");
                    var body_use_flg = ErsExpressionAccessor<ErsLpTemplate, EnumCmsFieldType?>.GetPropertyValue(objLpTemplate, "block_" + record.lineNumber + "_body_use_flg");
                    var free_use_flg = ErsExpressionAccessor<ErsLpTemplate, EnumCmsFieldType?>.GetPropertyValue(objLpTemplate, "block_" + record.lineNumber + "_free_use_flg");

                    if (head_use_flg == EnumCmsFieldType.Required)
                    {
                        record.AddInvalidField(record.CheckRequired("head"));
                    }
                    if (body_use_flg == EnumCmsFieldType.Required)
                    {
                        record.AddInvalidField(record.CheckRequired("body"));
                    }
                    if (free_use_flg == EnumCmsFieldType.Required)
                    {
                        record.AddInvalidField(record.CheckRequired("free"));
                    }

                    if (!record.IsValid)
                    {
                        foreach (var errorMessage in record.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "listBlock" });
                        }
                    }
                }
            }
        }
    }
}