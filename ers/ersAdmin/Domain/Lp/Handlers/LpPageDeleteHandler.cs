﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class LpPageDeleteHandler
        : ICommandHandler<ILpPageDeleteCommand>
    {
        public ICommandResult Submit(ILpPageDeleteCommand command)
        {
            this.DeleteLpPage(command);

            return new CommandResult(true);
        }

        private void DeleteLpPage(ILpPageDeleteCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageCriteria();
            criteria.lp_page_manage_id = command.lp_page_manage_id;
            criteria.page_type_code = command.page_type_code.ToString();
            repository.Delete(criteria);
        }
    }
}