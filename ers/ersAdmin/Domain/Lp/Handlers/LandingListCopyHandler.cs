﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class LandingListCopyHandler
        : ICommandHandler<ILandingListCopyCommand>
    {
        public ICommandResult Submit(ILandingListCopyCommand command)
        {
            var new_id = this.CopyLpPageManage(command);
            this.CopyQuestionnaire(new_id, command);
            this.CopyLpPage(new_id, command);

            command.id = new_id;

            return new CommandResult(true);
        }

        private int? CopyLpPageManage(ILandingListCopyCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageManageRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageManageCriteria();
            criteria.id = command.id;
            var listManage = repository.Find(criteria);

            if (listManage.Count == 0)
            {
                throw new ErsException("10200");
            }

            var objManage = listManage.First();
            objManage.id = null;
            objManage.page_name = ErsResources.GetMessage("lp_copy_prifix") + objManage.page_name;
            objManage.active = EnumActive.NonActive;
            objManage.intime = null;
            objManage.utime = null;
            repository.Insert(objManage, true);

            return objManage.id;
        }

        private void CopyQuestionnaire(int? new_id, ILandingListCopyCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpQuestionnaireRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpQuestionnaireCriteria();
            criteria.lp_page_manage_id = command.id;
            var listQuestionnaire = repository.Find(criteria);
            foreach (var objQuestionnaire in listQuestionnaire)
            {
                objQuestionnaire.id = null;
                objQuestionnaire.lp_page_manage_id = new_id;
                repository.Insert(objQuestionnaire);
            }
        }

        private void CopyLpPage(int? new_id, ILandingListCopyCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageCriteria();
            criteria.lp_page_manage_id = command.id;
            var listLpPage = repository.Find(criteria);
            foreach (var objLpPage in listLpPage)
            {
                objLpPage.id = null;
                objLpPage.lp_page_manage_id = new_id;
                repository.Insert(objLpPage);
            }
        }
    }
}