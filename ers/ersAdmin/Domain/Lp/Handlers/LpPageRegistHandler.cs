﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers.util;
using System.IO;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class LpPageRegistHandler
        : ICommandHandler<ILpPageRegistCommand>
    {
        public ICommandResult Submit(ILpPageRegistCommand command)
        {
            this.RegistLpPage(command);

            return new CommandResult(true);
        }

        private void RegistLpPage(ILpPageRegistCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageCriteria();
            criteria.lp_page_manage_id = command.lp_page_manage_id;
            criteria.page_type_code = command.page_type_code.ToString();
            var listLpPage = repository.Find(criteria);
            if (listLpPage.Count == 0)
            {
                this.InsertLpPage(command);
            }
            else
            {
                this.UpdateLpPage(command, listLpPage.First());
            }
        }

        private void InsertLpPage(ILpPageRegistCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var repository = ErsFactory.ersLpFactory.GetErsLpPageRepository();
            var ersLpPage = ErsFactory.ersLpFactory.GetErsLpPage();
            ersLpPage.lp_page_manage_id = command.lp_page_manage_id;
            ersLpPage.page_type_code = command.page_type_code.ToString();
            command.upsell_button_1_file_name = GetImageFileName(command.upsell_button_1_file);
            command.upsell_button_2_file_name = GetImageFileName(command.upsell_button_2_file);
            command.upsell_button_3_file_name = GetImageFileName(command.upsell_button_3_file);
            this.OverwriteValues(command, ersLpPage);
            repository.Insert(ersLpPage, true);

            //update Image file name
            this.UpdateLpPage(command, ersLpPage);
            
        }

        private void OverwriteValues(ILpPageRegistCommand command, ErsLpPage ersLpPage)
        {
            ersLpPage.template_code = command.template_code;
            foreach (var record in command.listBlock)
            {
                ErsExpressionAccessor<ErsLpPage, string>.SetPropertyValue(ersLpPage, "block_" + record.lineNumber + "_head", record.head);
                ErsExpressionAccessor<ErsLpPage, string>.SetPropertyValue(ersLpPage, "block_" + record.lineNumber + "_body", record.body);
                ErsExpressionAccessor<ErsLpPage, string>.SetPropertyValue(ersLpPage, "block_" + record.lineNumber + "_free", record.free);
            }
        }

        private void UpdateLpPage(ILpPageRegistCommand command, ErsLpPage oldLpPage)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var repository = ErsFactory.ersLpFactory.GetErsLpPageRepository();
            var newLpPage = ErsFactory.ersLpFactory.GetErsLpPage();
            newLpPage.OverwriteWithParameter(oldLpPage.GetPropertiesAsDictionary());
            if (command.upsell_button_1_file != null)
            {
                if (command.upsell_button_1_file.FileName != "")
                    SaveImage1(command, setup.lp_image_path, newLpPage);
            }
            if (command.upsell_button_2_file != null && command.del_upsell_button_2_flg == null)
            {
                if (command.upsell_button_2_file.FileName != "")
                    SaveImage2(command, setup.lp_image_path, newLpPage);
            }
            if (command.upsell_button_3_file != null && command.del_upsell_button_3_flg == null)
            {
                if(command.upsell_button_3_file.FileName != "")
                    SaveImage3(command, setup.lp_image_path, newLpPage);
            }
           
            if (command.del_upsell_button_2_flg.HasValue)
            {
                if (!String.IsNullOrEmpty(oldLpPage.upsell_button_2_file_name))
                {
                    DeleteImage(oldLpPage.upsell_button_2_file_name);
                    newLpPage.upsell_button_2_file_name = null;
                }
            }

            if (command.del_upsell_button_3_flg.HasValue)
            {
                if (!String.IsNullOrEmpty(oldLpPage.upsell_button_3_file_name))
                {
                    DeleteImage(oldLpPage.upsell_button_3_file_name);
                    newLpPage.upsell_button_3_file_name = null;
                }
            }

            this.OverwriteValues(command, newLpPage);
            repository.Update(oldLpPage, newLpPage);

           
        }

        protected string GetImageFileName(HttpPostedFileBase upsell_button_file)
        {
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();
            var fileName = uploadedFileHelper.GetTempFileName(upsell_button_file);
            return fileName;
        }
        //save picture 1
        protected void SaveImage1(ILpPageRegistCommand command, string directoryPath, ErsLpPage newLpPage)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();
            command.upsell_button_1_file_name = GetImageFileName(command.upsell_button_1_file);
            uploadedFileHelper.SaveTempFile(command.upsell_button_1_file, setup.image_temp_directory, command.upsell_button_1_file_name);

            var imgid = DateTime.Now.ToString("yyyyMMddHHmmss");

            var logo_image_file = newLpPage.id + "_1_" + imgid + Path.GetExtension(command.upsell_button_1_file_name);
            uploadedFileHelper.SaveResizedImage(setup.image_temp_directory, command.upsell_button_1_file_name, setup.lp_image_path, logo_image_file, 0);
            newLpPage.upsell_button_1_file_name = logo_image_file;
        }

        //save picture 2
        protected void SaveImage2(ILpPageRegistCommand command, string directoryPath, ErsLpPage newLpPage)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();
            command.upsell_button_2_file_name = GetImageFileName(command.upsell_button_2_file);
            uploadedFileHelper.SaveTempFile(command.upsell_button_2_file, setup.image_temp_directory, command.upsell_button_2_file_name);

            var imgid = DateTime.Now.ToString("yyyyMMddHHmmss");

            var logo_image_file = newLpPage.id + "_2_" + imgid + Path.GetExtension(command.upsell_button_2_file_name);
            uploadedFileHelper.SaveResizedImage(setup.image_temp_directory, command.upsell_button_2_file_name, setup.lp_image_path, logo_image_file, 0);
            newLpPage.upsell_button_2_file_name = logo_image_file;
        }

        //save picture 3
        protected void SaveImage3(ILpPageRegistCommand command, string directoryPath, ErsLpPage newLpPage)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();
            command.upsell_button_3_file_name = GetImageFileName(command.upsell_button_3_file);
            uploadedFileHelper.SaveTempFile(command.upsell_button_3_file, setup.image_temp_directory, command.upsell_button_3_file_name);

            var imgid = DateTime.Now.ToString("yyyyMMddHHmmss");

            var logo_image_file = newLpPage.id + "_3_" + imgid + Path.GetExtension(command.upsell_button_3_file_name);
            uploadedFileHelper.SaveResizedImage(setup.image_temp_directory, command.upsell_button_3_file_name, setup.lp_image_path, logo_image_file, 0);
            newLpPage.upsell_button_3_file_name = logo_image_file;
        }

        protected void DeleteImage(string filename)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var fileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();
            fileHelper.DeleteFile(setup.lp_image_path, filename);
            
        }

    }
}