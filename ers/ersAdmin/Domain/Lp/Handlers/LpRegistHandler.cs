﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.lp;
using System.IO;
using ersAdmin.Domain.SiteBase.Handlers;
using jp.co.ivp.ers.mall.site;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class LpRegistHandler
        : SiteRegisterBaseHandler, ICommandHandler<ILpRegistCommand>
    {
        public ICommandResult Submit(ILpRegistCommand command)
        {
            if (command.IsSaveTempImage)
            {
                this.SaveTempImage(command);
            }
            else
            {
                this.SaveImage(command);

                var objLpPageManage = this.RegistLpPageManage(command);

                this.RegistLpQuestionnaire(objLpPageManage, command);
            }

            return new CommandResult(true);
        }

        protected void SaveTempImage(ILpRegistCommand command)
        {
            if (command.logo_image.ContentLength == 0)
            {
                return;
            }

            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();

            command.logo_image_file = uploadedFileHelper.GetTempFileName(command.logo_image);
            uploadedFileHelper.SaveTempFile(command.logo_image, setup.news_image_temp_path, command.logo_image_file);
        }

        protected void SaveImage(ILpRegistCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var sites = GetErsListSite();
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();

            var imgid = DateTime.Now.ToString("yyyyMMddHHmmss");

            var logo_image_file = "lp_" + command.ccode + "_" + imgid + Path.GetExtension(command.logo_image_file);
            if (setup.Multiple_sites)
            {
                foreach (var site in sites)
                {
                    uploadedFileHelper.SaveResizedImage(setup.news_image_temp_path, command.logo_image_file, setup.multiple_news_image_path((int)site.id), logo_image_file, 0);
                }
            }
            else
            {

                uploadedFileHelper.SaveResizedImage(setup.news_image_temp_path, command.logo_image_file, setup.news_image_path, logo_image_file, 0);
            }
            command.logo_image_file = logo_image_file;
        }
        /// <summary>
        /// ERSサイトの一覧を取得します。
        /// </summary>
        /// <returns></returns>
        private IList<ErsSite> GetErsListSite()
        {
            var siteRepository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var siteCriteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
            siteCriteria.active = EnumActive.Active;
            siteCriteria.mall_shop_kbn = EnumMallShopKbn.ERS;
            siteCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listSite = siteRepository.Find(siteCriteria);
            return listSite;
        }

        private ErsLpPageManage RegistLpPageManage(ILpRegistCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageManageRepository();
            var objLpPageManage = ErsFactory.ersLpFactory.GetErsLpPageManage();
            objLpPageManage.OverwriteWithParameter(command.GetPropertiesAsDictionary());
            objLpPageManage.active = EnumActive.Active;
            objLpPageManage.site_id = this.GetArrayOfSiteId(command);

            if (objLpPageManage.basic_stgy_kbn == EnumLpBasicStgy.FreeSample)
            {
                objLpPageManage.sell_price = 0;
            }

            repository.Insert(objLpPageManage, true);
            return objLpPageManage;
        }

        private void RegistLpQuestionnaire(ErsLpPageManage objLpPageManage, ILpRegistCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpQuestionnaireRepository();

            foreach (var questionnaireRecord in command.listQuestionnaire)
            {
                var objLpQuestionnaire = ErsFactory.ersLpFactory.GetErsLpQuestionnaire();
                objLpQuestionnaire.OverwriteWithParameter(questionnaireRecord.GetPropertiesAsDictionary());
                objLpQuestionnaire.lp_page_manage_id = objLpPageManage.id;
                repository.Insert(objLpQuestionnaire);
            }
        }
    }
}