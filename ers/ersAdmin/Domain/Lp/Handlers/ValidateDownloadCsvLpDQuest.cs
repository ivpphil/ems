﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class ValidateDownloadCsvLpDQuest
        : IValidationHandler<IDownloadCsvLpDQuestCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IDownloadCsvLpDQuestCommand command)
        {
            yield return command.CheckRequired("page_id");
        }
    }
}