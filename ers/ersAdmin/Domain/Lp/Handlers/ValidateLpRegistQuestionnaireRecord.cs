﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class ValidateLpRegistQuestionnaireRecord
        : IValidationHandler<ILpRegistQuestionnaireRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(ILpRegistQuestionnaireRecordCommand command)
        {
            yield return command.CheckRequired("item_code");
        }
    }
}