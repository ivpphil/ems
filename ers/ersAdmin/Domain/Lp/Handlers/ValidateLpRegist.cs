﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class ValidateLpRegist
        : IValidationHandler<ILpRegistCommand>
    {
        public IEnumerable<ValidationResult> Validate(ILpRegistCommand command)
        {
            if (!command.sell_price.HasValue)
            {
                command.sell_price = 0;
            }

            if (command.lpregist_base
                || command.lpregist_detail
                || command.lpregist_questionnaire
                || command.lpregist_confirm)
            {
                foreach (var result in this.ValidateBaseSetting(command))
                {
                    yield return result;
                }
            }

            if (command.lpregist_detail
                || command.lpregist_questionnaire
                || command.lpregist_confirm)
            {
                foreach (var result in this.ValidateDetailSetting(command))
                {
                    yield return result;
                }
            }

            if (command.lpregist_questionnaire
                || command.lpregist_confirm)
            {
                foreach (var result in this.ValidateQuestionnaire(command))
                {
                    yield return result;
                }
            }
        }

        private IEnumerable<ValidationResult> ValidateBaseSetting(ILpRegistCommand command)
        {
            yield return command.CheckRequired("page_name");
            yield return command.CheckRequired("page_title");
            yield return command.CheckRequired("lp_group_name");
            if (command.logo_image == null || command.logo_image.ContentLength == 0)
            {
                yield return command.CheckRequired("logo_image_file");
            }
            else
            {
                yield return command.CheckRequired("logo_image");
            }
            yield return command.CheckRequired("ccode");
            yield return command.CheckRequired("public_st_date");
            yield return command.CheckRequired("public_ed_date");
            foreach (var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDateTime(
                "public_st_date", command.public_st_date,
                "public_ed_date", command.public_ed_date))
            {
                yield return result;
            }
        }

        private IEnumerable<ValidationResult> ValidateDetailSetting(ILpRegistCommand command)
        {
            yield return command.CheckRequired("basic_stgy_kbn");
            if (command.basic_stgy_kbn == EnumLpBasicStgy.UpSell)
            {
                yield return command.CheckRequired("upsell_stgy_kbn");
            }
            yield return command.CheckRequired("buy_limit_kbn");
            yield return command.CheckRequired("sell_scode");
            yield return command.CheckRequired("sell_order_type");
            yield return command.CheckRequired("sell_price");  
            yield return command.CheckRequired("sell_discount_flg");
            if (command.sell_discount_flg == EnumUse.Use)
            {
                yield return command.CheckRequired("sell_discount_amount");
                yield return command.CheckRequired("sell_discount_price");
                if (command.basic_stgy_kbn == EnumLpBasicStgy.UpSell)
                {
                    yield return command.CheckRequired("sell_upsell_price");
                }
            }
            if (command.sell_order_type != EnumOrderType.Usually)
            {
                yield return command.CheckRequired("sell_first_regular_price");
                yield return command.CheckRequired("sell_regular_price");
            }
            yield return command.CheckRequired("sell_max_amount");
            yield return command.CheckRequired("sell_max_stock");

            if (command.basic_stgy_kbn == EnumLpBasicStgy.UpSell)
            {
                yield return command.CheckRequired("upsell_scode");
                yield return command.CheckRequired("upsell_order_type");
                yield return command.CheckRequired("upsell_price");
                yield return command.CheckRequired("upsell_discount_flg");
                if (command.upsell_discount_flg == EnumUse.Use)
                {
                    yield return command.CheckRequired("upsell_discount_amount");
                    yield return command.CheckRequired("upsell_discount_price");
                }
                if (command.upsell_order_type != EnumOrderType.Usually)
                {
                    yield return command.CheckRequired("upsell_first_regular_price");
                    yield return command.CheckRequired("upsell_regular_price");
                }
                yield return command.CheckRequired("upsell_max_amount");
                yield return command.CheckRequired("upsell_max_stock");

                yield return ErsFactory.ersLpFactory.GetCheckUpsellOrderTypeStgy().Check(command.sell_order_type, command.upsell_order_type);
            }

            yield return command.CheckRequired("coupon_flg");
            if (command.coupon_flg == EnumUse.Use)
            {
                yield return command.CheckRequired("coupon_code");

                if (command.IsValidField("coupon_code"))
                {
                    var objCoupon = ErsFactory.ersCouponFactory.GetErsCouponWithCouponCode(command.coupon_code);
                    if (objCoupon == null || objCoupon.active != EnumActive.Active)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("20214"), new[] { "coupon_code" });
                    }
                }
            }

            yield return command.CheckRequired("carriage_free_flg");
            if (command.carriage_free_flg == EnumUse.Use)
            {
                yield return command.CheckRequired("carriage_free_price");
            }
        }

        private IEnumerable<ValidationResult> ValidateQuestionnaire(ILpRegistCommand command)
        {
            if (command.listQuestionnaire != null)
            {
                foreach (var record in command.listQuestionnaire)
                {
                    record.AddInvalidField(command.controller.commandBus.Validate<ILpRegistQuestionnaireRecordCommand>(record));

                    if (!record.IsValid)
                    {
                        foreach (var errorMessage in record.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "listQuestionnaire" });
                        }
                    }
                }
            }

            yield return command.CheckRequired("personal_info_kbn");
            if (command.personal_info_kbn == EnumLpPersonalInfo.UseUrl)
            {
                yield return command.CheckRequired("personal_info_url");
            }
        }
    }
}