﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class LpModifyHandler
        : LpRegistHandler, ICommandHandler<ILpModifyCommand>
    {
        public ICommandResult Submit(ILpModifyCommand command)
        {
            if (command.IsSaveTempImage)
            {
                this.SaveTempImage(command);
            }
            else
            {
                if (command.logo_image_file != command.old_logo_image_file)
                {
                    this.SaveImage(command);
                }

                this.UpdateLpPageManage(command);

                this.UpdateLpQuestionnaire(command);
            }

            return new CommandResult(true);
        }

        private void UpdateLpPageManage(ILpModifyCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageManageRepository();

            var oldLpPageManage = ErsFactory.ersLpFactory.GetErsLpPageManageWithIdForAdmin(command.id);
            var newLpPageManage = ErsFactory.ersLpFactory.GetErsLpPageManage();
            newLpPageManage.OverwriteWithParameter(oldLpPageManage.GetPropertiesAsDictionary());
            newLpPageManage.OverwriteWithParameter(command.GetPropertiesAsDictionary());
            newLpPageManage.active = command.active ?? EnumActive.NonActive;
            newLpPageManage.site_id = this.GetArrayOfSiteId(command);

            if(newLpPageManage.basic_stgy_kbn == EnumLpBasicStgy.FreeSample)
            {
                newLpPageManage.sell_price = 0;
            }

            repository.Update(oldLpPageManage, newLpPageManage);
        }

        private void UpdateLpQuestionnaire(ILpModifyCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpQuestionnaireRepository();

            foreach (var questionnaireRecord in command.listQuestionnaire)
            {
                var oldLpQuestionnaire = ErsFactory.ersLpFactory.GetErsLpQuestionnaireWithItemCode(command.id, questionnaireRecord.item_code);
                if (oldLpQuestionnaire == null)
                {
                    var newLpQuestionnaire = ErsFactory.ersLpFactory.GetErsLpQuestionnaire();
                    newLpQuestionnaire.OverwriteWithParameter(questionnaireRecord.GetPropertiesAsDictionary());
                    newLpQuestionnaire.id = null;
                    newLpQuestionnaire.lp_page_manage_id = command.id;
                    repository.Insert(newLpQuestionnaire);
                }
                else
                {
                    var newLpQuestionnaire = ErsFactory.ersLpFactory.GetErsLpQuestionnaire();
                    newLpQuestionnaire.OverwriteWithParameter(oldLpQuestionnaire.GetPropertiesAsDictionary());
                    newLpQuestionnaire.OverwriteWithParameter(questionnaireRecord.GetPropertiesAsDictionary());
                    repository.Update(oldLpQuestionnaire, newLpQuestionnaire);
                }
            }
        }
    }
}