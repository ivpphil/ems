﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class ValidateLpDelete
        : IValidationHandler<ILpDeleteCommand>
    {
        public IEnumerable<ValidationResult> Validate(ILpDeleteCommand command)
        {
            yield return command.CheckRequired("id");
        }
    }
}