﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class ValidateLpModify
        : ValidateLpRegist, IValidationHandler<ILpModifyCommand>
    {
        public IEnumerable<ValidationResult> Validate(ILpModifyCommand command)
        {
            yield return command.CheckRequired("id");

            foreach (var result in base.Validate(command))
            {
                yield return result;
            }
        }
    }
}