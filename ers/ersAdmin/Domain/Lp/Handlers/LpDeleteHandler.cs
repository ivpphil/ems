﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class LpDeleteHandler
        : ICommandHandler<ILpDeleteCommand>
    {
        public ICommandResult Submit(ILpDeleteCommand command)
        {
            this.DeleteLpPageManage(command);
            this.DeleteQuestionnaire(command);
            this.DeleteLpPage(command);

            return new CommandResult(true);
        }

        private void DeleteLpPageManage(ILpDeleteCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageManageRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageManageCriteria();
            criteria.id = command.id;
            repository.Delete(criteria);
        }

        private void DeleteQuestionnaire(ILpDeleteCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpQuestionnaireRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpQuestionnaireCriteria();
            criteria.lp_page_manage_id = command.id;
            repository.Delete(criteria);
        }

        private void DeleteLpPage(ILpDeleteCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageCriteria();
            criteria.lp_page_manage_id = command.id;
            repository.Delete(criteria);
        }

        private void DeleteLpButtonImagePage(ILpDeleteCommand command)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageCriteria();
            criteria.lp_page_manage_id = command.id;
            var result = repository.Find(criteria);
            foreach (var itm in result)
            {
                //itm.id
            }
            repository.Delete(criteria);
        }
    }
}