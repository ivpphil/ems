﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Lp.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Lp.Handlers
{
    public class ValidateLandingListCopy
        : IValidationHandler<ILandingListCopyCommand>
    {
        public IEnumerable<ValidationResult> Validate(ILandingListCopyCommand command)
        {
            yield return command.CheckRequired("id");
        }
    }
}