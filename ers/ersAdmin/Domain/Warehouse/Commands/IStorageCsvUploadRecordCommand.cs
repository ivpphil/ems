﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Warehouse.Commands
{
    public interface IStorageCsvUploadRecordCommand : ICommand
    {
        string order_no { get; }

        string scode { get; }

        string maker_scode { get; }

        int? shelf001 { get; }

        int? shelf002 { get; }

        int? shelf003 { get; }

    }
}
