﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Warehouse.Commands
{
    public interface IPastOrderListCommand
        : ICommand
    {
        DateTime? s_orderdate_from { get; set; }

        DateTime? s_orderdate_to { get; set; }
    }
}