﻿using ersAdmin.Models.csv;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Warehouse.Commands
{
    public interface IStorageCsvUploadCommand : ICommand
    {

        ErsCsvContainer<storage_csv_upload_record> csv_file { get; }

        bool chk_find { get; }

        bool regist { get; }

    }
}
