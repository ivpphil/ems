﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Warehouse.Commands
{
    public interface IStockTakingCsvUploadRecordCommand : ICommand
    {

        string scode { get; }

        int? shelf001 { get; }

        int? shelf002 { get; }

        int? shelf003 { get; }

    }
}
