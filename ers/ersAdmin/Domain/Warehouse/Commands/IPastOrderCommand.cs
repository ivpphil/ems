﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.warehouse;

namespace ersAdmin.Domain.Warehouse.Commands
{
    public interface IPastOrderCommand
        : ICommand
    {
        string order_no { get; set; }

        IEnumerable<past_order_record> listPastOrder { get; set; }
    }
}
