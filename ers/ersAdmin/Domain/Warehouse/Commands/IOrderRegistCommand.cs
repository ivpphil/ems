﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.warehouse;

namespace ersAdmin.Domain.Warehouse.Commands
{
    public interface IOrderRegistCommand
        : ICommand
    {
        IEnumerable<OrderRegistRecord> orderRegistRecords { get; set; }

        DateTime? s_intime { get; set; }
    }
}
