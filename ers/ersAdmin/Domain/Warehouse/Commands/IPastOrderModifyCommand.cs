﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using ersAdmin.Models.warehouse;

namespace ersAdmin.Domain.Warehouse.Commands
{
    public interface IPastOrderModifyCommand
        : ICommand
    {
        string order_no { get; set; }

        EnumWhOrderStatus? wh_order_status { get; set; }

        string remarks { get; set; }

        IEnumerable<past_order_record> listPastOrder { get; set; }

        bool is_under_stock { get; set; }
    }
}