﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.warehouse;

namespace ersAdmin.Domain.Warehouse.Commands
{
    public interface ISupplierRegisterCommand
        : ICommand
    {
        string supplier_code { get; set; }

        string supplier_name { get; set; }

        int? pref { get; set; }

        string zip { get; set; }

        string address { get; set; }

        string tel { get; set; }

        string fax { get; set; }

        string email { get; set; }
    }
}
