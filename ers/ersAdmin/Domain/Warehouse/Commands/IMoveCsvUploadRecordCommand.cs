﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Warehouse.Commands
{
    public interface IMoveCsvUploadRecordCommand : ICommand
    {

        string scode { get; }

        EnumShelfNumber? shelf_from { get; }

        EnumShelfNumber? shelf_to { get; }

        int? amount { get; }

    }
}
