﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Warehouse.Commands
{
    public interface IStockListCommand:ICommand
    {
        DateTime? s_warehousing_from { get; }

        DateTime? s_warehousing_to { get; }

        bool search { get; }
    }
}