﻿using System;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Warehouse.Commands
{
    public interface IStorageDownloadCommand : ICommand
    {

        DateTime? s_date_from { get; }
        DateTime? s_date_to { get; }

    }
}
