﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Warehouse.Commands
{
    public interface IMoveHistoryListCommand :ICommand
    {
        DateTime? s_movedate_from { get; set; }

        DateTime? s_movedate_to { get; set; }

        bool search { get; set; }
    }
}