﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateStockTakingCsvUploadRecord : IValidationHandler<IStockTakingCsvUploadRecordCommand>
    {

        public IEnumerable<ValidationResult> Validate(IStockTakingCsvUploadRecordCommand command)
        {

            yield return command.CheckRequired("scode");

            if (!string.IsNullOrEmpty(command.scode))
            {
                foreach (var result in ErsFactory.ersMerchandiseFactory.GetCheckMerchandiseExistStgy().CheckScodeExist(command.scode))
                    yield return result;
            }

        }

    }
}