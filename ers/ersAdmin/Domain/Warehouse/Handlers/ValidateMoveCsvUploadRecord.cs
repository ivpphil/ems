﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateMoveCsvUploadRecord : IValidationHandler<IMoveCsvUploadRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IMoveCsvUploadRecordCommand command)
        {

            yield return command.CheckRequired("scode");
            if(!string.IsNullOrEmpty(command.scode))
            {
                foreach (var result in ErsFactory.ersMerchandiseFactory.GetCheckMerchandiseExistStgy().CheckScodeExist(command.scode))
                    yield return result;
            }

            if (command.shelf_from == null && command.shelf_to == null)
                yield return new ValidationResult(ErsResources.GetMessage("MOVCSV0001"));

            if (command.shelf_from != null)
            {
                foreach (var result in ErsFactory.ersWarehouseFactory.GetValidateShelfNumberStgy().Validate(command.shelf_from))
                    yield return result;

                foreach(var result in ErsFactory.ersWarehouseFactory.GetValidateGoodsIssueStgy().Validate(command.scode, command.shelf_from, command.amount))
                    yield return result;
            }

            if (command.shelf_to != null)
            {
                foreach (var result in ErsFactory.ersWarehouseFactory.GetValidateShelfNumberStgy().Validate(command.shelf_to))
                    yield return result;
            }

        }

    }
}