﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateMoveCsvUpload : IValidationHandler<IMoveCsvUploadCommand>
    {

        public IEnumerable<ValidationResult> Validate(IMoveCsvUploadCommand command)
        {
            if (!command.regist)
            {
                if (command.csv_file.csv_file == null)
                    throw new ErsException("10202");
            }

            foreach (var model in command.csv_file.GetValidatedModels(command.chk_find))
            {
                model.AddInvalidField(command.controller.commandBus.Validate<IMoveCsvUploadRecordCommand>(model));

                if (!model.IsValid)
                {
                    if (!command.regist)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                            yield return new ValidationResult(errorMessage, new[] { "csv_file" });
                    }

                    command.csv_file.MarkRecordAsInvalid(model);
                }
            }

            //保持された登録情報が無い場合、エラーメッセージを表示。
            if (command.csv_file.validIndexes.Count() == 0)
            {
                //対象データが存在しません。確認してください。
                yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "csv_file" });
            }

        }

    }
}