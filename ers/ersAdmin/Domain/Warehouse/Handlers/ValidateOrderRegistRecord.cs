﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateOrderRegistRecord
        : IValidationHandler<IOrderRegistRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IOrderRegistRecordCommand command)
        {
            if (command.check_order == EnumOnOff.On)
            {
                yield return command.CheckRequired("amount");
                yield return command.CheckRequired("schedule_date");
            }
        }
    }
}