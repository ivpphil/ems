﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateStorageCsvUploadRecord : IValidationHandler<IStorageCsvUploadRecordCommand>
    {

        public IEnumerable<ValidationResult> Validate(IStorageCsvUploadRecordCommand command)
        {
            yield return command.CheckRequired("order_no");
            yield return command.CheckRequired("scode");
            yield return command.CheckRequired("shelf001");
            yield return command.CheckRequired("shelf002");
            yield return command.CheckRequired("shelf003");

            if ((command.shelf001 == 0) && (command.shelf002 == 1) && (command.shelf003 == 1))
            {
                yield return new ValidationResult(ErsResources.GetMessage("WHS0002", command.order_no, command.scode), new[] { "order_no", "scode" });
            }


            if (command.IsValidField("order_no", "scode"))
            {
                ////Confirmation of order number
                if ((!string.IsNullOrEmpty(command.scode)) && (!string.IsNullOrEmpty(command.order_no)))
                {
                    foreach (var result in ErsFactory.ersWarehouseFactory.GetCheckOrderNumberExistStgy().CheckOrderNumberExist(command.scode, command.order_no))
                        yield return result;
                }
            }

            if (command.IsValidField("order_no"))
            {
                ///Check the status of order
                if (!string.IsNullOrEmpty(command.order_no))
                {
                    foreach (var result in ErsFactory.ersWarehouseFactory.GetCheckStatusOrderStgy().CheckStatusOrder(command.order_no))
                        yield return result;
                }
            }

            if (command.IsValidField("order_no"))
            {
                ////Confirmation of the number of orders
                if ((!string.IsNullOrEmpty(command.scode)) && (!string.IsNullOrEmpty(command.order_no)))
                {
                    int? valShelOne = (command.shelf001 == null) ? 0 : command.shelf001;
                    int? valShelTwo = (command.shelf002 == null) ? 0 : command.shelf002;
                    int? valShelThree = (command.shelf003 == null) ? 0 : command.shelf003;

                    int? sumShelves = valShelOne + valShelTwo + valShelThree;

                    foreach (var result in ErsFactory.ersWarehouseFactory.GetValidateOrderCountStgy().Validate(command.scode, command.order_no, sumShelves.Value))
                        yield return result;
                }
            }
        }

    }
}