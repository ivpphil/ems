﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateSupplierModify
        : IValidationHandler<ISupplierModifyCommand>
    {
        public IEnumerable<ValidationResult> Validate(ISupplierModifyCommand command)
        {
            yield return command.CheckRequired("supplier_code");
            yield return command.CheckRequired("supplier_name");
            yield return command.CheckRequired("zip");
            yield return command.CheckRequired("pref");
            foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("pref", command.pref))
            {
                yield return result;
            }
            if (command.IsValidField("zip"))
            {
                yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("zip", command.zip);
            }

            yield return command.CheckRequired("address");
            yield return command.CheckRequired("tel");

            yield return ErsFactory.ersWarehouseFactory.GetCheckDuplicateSupplierCodeStgy().CheckDuplicate(command.id,command.supplier_code);
        }
    }
}