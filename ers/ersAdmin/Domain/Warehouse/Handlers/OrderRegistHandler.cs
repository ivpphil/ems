﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using ersAdmin.Models.warehouse;
using jp.co.ivp.ers;
using jp.co.ivp.ers.warehouse;
using jp.co.ivp.ers.merchandise.stock;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class OrderRegistHandler
        : ICommandHandler<IOrderRegistCommand>
    {
        ErsWhOrderRepository whOrderRepository = ErsFactory.ersWarehouseFactory.GetErsWhOrderRepository();
        ErsWhOrderInfoRepository whOrderInfoRepository = ErsFactory.ersWarehouseFactory.GetErsWhOrderInfoRepository();
        ErsWhStockRepository whStockRepository = ErsFactory.ersWarehouseFactory.GetErsWhStockRepository();
        ErsStockRepository stockRepositroy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockRepository();

        Dictionary<string, string> listOrderNo = new Dictionary<string, string>();

        public ICommandResult Submit(IOrderRegistCommand command)
        {
            command.s_intime = DateTime.Now;

            var listParam = new List<UpdateStockParam>();
            foreach (var model in command.orderRegistRecords)
            {
                if (model.check_order != EnumOnOff.On)
                {
                    continue;
                }

                this.AddOrder(command, model);

                if (model.up_stock == EnumWhUpStock.Up)
                {
                    this.IncreaseWebStock(model);

                    var objSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(model.scode);
                    // モール在庫用 [For mall stock]
                    if (objSku.h_mall_flg == EnumOnOff.On)
                    {
                        UpdateStockParam param = default(UpdateStockParam);

                        param.productCode = model.scode;
                        param.quantity = model.amount;
                        param.operation = EnumMallStockOperation.add;

                        listParam.Add(param);
                    }
                }
            }

            // モール在庫更新 [Update mall stock]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }

            return new CommandResult(true);
        }

        private void AddOrder(IOrderRegistCommand command, OrderRegistRecord model)
        {
            if (!listOrderNo.ContainsKey(model.supplier_code))
            {
                listOrderNo[model.supplier_code] = whOrderRepository.GetNextSequenceOrderNo();

                //発注情報インサート
                var whOrderInfo = ErsFactory.ersWarehouseFactory.GetErsWhOrderInfo();
                whOrderInfo.order_no = listOrderNo[model.supplier_code];
                whOrderInfo.supplier_code = model.supplier_code;
                whOrderInfo.wh_order_status = EnumWhOrderStatus.NotStorage;
                whOrderInfo.active = EnumActive.Active;
                whOrderInfoRepository.Insert(whOrderInfo);
            }

            var whOrder = ErsFactory.ersWarehouseFactory.GetErsWhOrder();
            whOrder.OverwriteWithParameter(model.GetPropertiesAsDictionary());
            whOrder.order_no = listOrderNo[model.supplier_code];
            whOrder.intime = command.s_intime;
            whOrderRepository.Insert(whOrder);
        }

        private void IncreaseWebStock(OrderRegistRecord model)
        {
            var increaseStockStgy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetIncreaseStockStgy();
            increaseStockStgy.Increase(model.scode, model.amount.Value);
        }
    }
}