﻿using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.warehouse;
using ersAdmin.Models.csv;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class StockTakingCsvUploadHandler : ICommandHandler<IStockTakingCsvUploadCommand>
    {
        public ICommandResult Submit(IStockTakingCsvUploadCommand command)
        {
            this.Process(command);
            return new CommandResult(true);
        }

        internal void Process(IStockTakingCsvUploadCommand command)
        {
            foreach (var item in command.csv_file.GetValidModels())
            {
                var repo = ErsFactory.ersWarehouseFactory.GetErsWhStockRepository();
                var criteria = this.GetWhStockCriteria(item.scode);
                if (repo.GetRecordCount(criteria) > 0)
                {
                    var wh_stock = repo.FindSingle(criteria);
                    using (var tx = ErsDB_parent.BeginTransaction())
                    {
                        this.CheckWhichInsertShouldPerform(item, wh_stock);

                        this.UpdateWhStock(item, repo, wh_stock);
                        tx.Commit();
                    }
                }
                else
                {
                    //実在庫がない場合はINSERTする
                    var wh_stock = ErsFactory.ersWarehouseFactory.GetErsWhStock();
                    wh_stock.active = EnumActive.Active;
                    wh_stock.scode = item.scode;
                    wh_stock.shelf001 = 0;
                    wh_stock.shelf002 = 0;
                    wh_stock.shelf003 = 0;
                    wh_stock.stock = 0;

                    var skuCriteria = this.GetSkuCriteria(item.scode);
                    var skuRepo = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
                    if (skuRepo.GetRecordCount(skuCriteria) > 0)
                    {
                        using (var tx = ErsDB_parent.BeginTransaction())
                        {
                            this.CheckWhichInsertShouldPerform(item, wh_stock);

                            this.InsertWhStock(item, repo, wh_stock);
                            tx.Commit();
                        }
                    }

                }
            }
        }

        private ErsWhStockCriteria GetWhStockCriteria(string scode)
        {
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhStockCriteria();
            criteria.scode = scode;

            return criteria;
        }

        private ErsSkuCriteria GetSkuCriteria(string scode)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.scode = scode;

            return criteria;
        }

        internal void CheckWhichInsertShouldPerform(stock_taking_csv_upload_record CSV, ErsWhStock wh_stock)
        {
            if ((wh_stock.shelf001 != CSV.shelf001) && (wh_stock.shelf001 > 0))
                this.InsertWhMoveOut(CSV, wh_stock, EnumShelfNumber.SHELF001, wh_stock.shelf001);
            
            if ((wh_stock.shelf001 != CSV.shelf001) && (CSV.shelf001 > 0))
                this.InsertWhMoveIn(CSV, wh_stock, EnumShelfNumber.SHELF001, CSV.shelf001);
            
            if ((wh_stock.shelf002 != CSV.shelf002) && (wh_stock.shelf002 > 0))
                this.InsertWhMoveOut(CSV, wh_stock, EnumShelfNumber.SHELF002, wh_stock.shelf002);
            
            if ((wh_stock.shelf002 != CSV.shelf002) && (CSV.shelf002 > 0))
                this.InsertWhMoveIn(CSV, wh_stock, EnumShelfNumber.SHELF002, CSV.shelf002);
            
            if ((wh_stock.shelf003 != CSV.shelf003) && (wh_stock.shelf003 > 0))
                this.InsertWhMoveOut(CSV, wh_stock, EnumShelfNumber.SHELF003, wh_stock.shelf003);
            
            if ((wh_stock.shelf003 != CSV.shelf003) && (CSV.shelf003 > 0))
                this.InsertWhMoveIn(CSV, wh_stock, EnumShelfNumber.SHELF003, CSV.shelf003);
        }

        internal void InsertWhMoveOut(stock_taking_csv_upload_record item, ErsWhStock wh_stock, EnumShelfNumber? shelf_from, int? amount)
        {
            var repo = ErsFactory.ersWarehouseFactory.GetErsWhMoveRepository();
            var wh_move = ErsFactory.ersWarehouseFactory.GetErsWhMove();
            wh_move.scode = item.scode;
            wh_move.maker_scode = item.maker_scode;
            wh_move.reason = "棚卸";
            wh_move.shelf_from = shelf_from;
            wh_move.amount = amount;
            wh_move.move_type = EnumWhMoveType.INVENTORY;

            repo.Insert(wh_move, true);
        }

        internal void InsertWhMoveIn(stock_taking_csv_upload_record item, ErsWhStock wh_stock, EnumShelfNumber? shelf_to, int? amount)
        {
            var repo = ErsFactory.ersWarehouseFactory.GetErsWhMoveRepository();
            var wh_move = ErsFactory.ersWarehouseFactory.GetErsWhMove();
            wh_move.scode = item.scode;
            wh_move.maker_scode = item.maker_scode;
            wh_move.reason = "棚卸";
            wh_move.shelf_to = shelf_to;
            wh_move.amount = amount;
            wh_move.move_type = EnumWhMoveType.INVENTORY;

            repo.Insert(wh_move, true);
        }

        internal void UpdateWhStock(stock_taking_csv_upload_record item, ErsWhStockRepository repo, ErsWhStock wh_stock)
        {
            var whStock = ErsFactory.ersWarehouseFactory.GetErsWhStock();
            whStock.OverwriteWithModel(wh_stock);
            whStock.shelf001 = (item.shelf001.HasValue) ? item.shelf001 : 0;
            whStock.shelf002 = (item.shelf002.HasValue) ? item.shelf002 : 0;
            whStock.shelf003 = (item.shelf003.HasValue) ? item.shelf003 : 0;
            whStock.stock = whStock.shelf001;
            repo.Update(wh_stock, whStock);
        }
        internal void InsertWhStock(stock_taking_csv_upload_record item, ErsWhStockRepository repo, ErsWhStock wh_stock)
        {
            var whStock = ErsFactory.ersWarehouseFactory.GetErsWhStock();
            whStock.OverwriteWithModel(wh_stock);
            whStock.shelf001 = (item.shelf001.HasValue) ? item.shelf001 : 0;
            whStock.shelf002 = (item.shelf002.HasValue) ? item.shelf002 : 0;
            whStock.shelf003 = (item.shelf003.HasValue) ? item.shelf003 : 0;
            whStock.stock = whStock.shelf001;
            repo.Insert(whStock);
        }
    }
}