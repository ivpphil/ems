﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateStorageUploadCSVRecord : IValidationHandler<IStorageUploadCSVRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IStorageUploadCSVRecordCommand command)
        {
            yield return command.CheckRequired("order_no");
            yield return command.CheckRequired("scode");
            yield return command.CheckRequired("maker_scode");
            yield return command.CheckRequired("shelf001");
            yield return command.CheckRequired("shelf002");
            yield return command.CheckRequired("shelf003");

            if (!string.IsNullOrEmpty(command.order_no) && !string.IsNullOrEmpty(command.scode))
            {
                foreach (var result in ErsFactory.ersWarehouseFactory.GetValidateOrderNumberStgy().Validate(command.scode, command.order_no))
                    yield return result;
            }

            if (!string.IsNullOrEmpty(command.order_no) && !string.IsNullOrEmpty(command.scode))
            {
                int? sumShelves = command.shelf001 + command.shelf002 + command.shelf003;
                foreach (var result in ErsFactory.ersWarehouseFactory.GetValidateOrderCountStgy().Validate(command.scode, command.order_no, sumShelves.Value))
                    yield return result;
            }
        }
    }
}