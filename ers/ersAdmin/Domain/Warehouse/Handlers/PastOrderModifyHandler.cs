﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using ersAdmin.Models.warehouse;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class PastOrderModifyHandler
        : ICommandHandler<IPastOrderModifyCommand>
    {
        public ICommandResult Submit(IPastOrderModifyCommand command)
        {
            this.UpdateWhOrderInfo(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// 発注情報更新
        /// </summary>
        /// <param name="command"></param>
        private void UpdateWhOrderInfo(IPastOrderModifyCommand command)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhOrderInfoRepository();
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhOrderInfoCriteria();
            criteria.order_no = command.order_no;
            criteria.active = EnumActive.Active;
            var oldWhOrderInfo = repository.FindSingle(criteria);

            var newWhOrderInfo = ErsFactory.ersWarehouseFactory.GetErsWhOrderInfo();
            newWhOrderInfo.OverwriteWithParameter(oldWhOrderInfo.GetPropertiesAsDictionary());

            var old_wh_order_status = newWhOrderInfo.wh_order_status;

            newWhOrderInfo.wh_order_status = command.wh_order_status;
            newWhOrderInfo.remarks = command.remarks;

            repository.Update(oldWhOrderInfo, newWhOrderInfo);

            if (old_wh_order_status != command.wh_order_status
                && command.wh_order_status == EnumWhOrderStatus.Canceled)
            {
                this.DecreaseWebStock(command, command.listPastOrder);
            }
        }

        /// <summary>
        /// WEB在庫減算
        /// </summary>
        /// <param name="orderRecordList"></param>
        private void DecreaseWebStock(IPastOrderModifyCommand command, IEnumerable<past_order_record> orderRecordList)
        {
            //Decrease stock.
            var decreaseStockStgy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetDecreaseStockStgy();
            var increaseStockStgy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetIncreaseStockStgy();

            foreach (var orderRecord in orderRecordList)
            {
                if (orderRecord.up_stock == EnumWhUpStock.Up)
                {
                    if (command.is_under_stock)
                    {
                        increaseStockStgy.Increase(orderRecord.scode, -orderRecord.amount.Value);
                    }
                    else
                    {
                        var objSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(orderRecord.scode);
                        if (objSku != null)
                        {
                            decreaseStockStgy.DecreaseNotSet(orderRecord.scode, orderRecord.amount.Value, objSku.soldout_flg.Value);
                        }
                    }
                }
            }
        }
    }
}