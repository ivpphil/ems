﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateMoveHistoryList:IValidationHandler<IMoveHistoryListCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IMoveHistoryListCommand command)
        {
            if (command.search)
            {
                if ((command.s_movedate_from) > (command.s_movedate_to))
                    yield return new ValidationResult(ErsResources.GetMessage("10045", ErsResources.GetFieldName("s_movedate_from"), ErsResources.GetFieldName("s_movedate_to")));
            }
        }
    }
}