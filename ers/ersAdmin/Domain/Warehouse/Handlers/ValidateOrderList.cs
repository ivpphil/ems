﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateOrderList
        : IValidationHandler<IOrderListCommand>
    {
        public IEnumerable<ValidationResult> Validate(IOrderListCommand command)
        {
            yield return command.CheckRequired("s_intime_less_than");
        }
    }
}