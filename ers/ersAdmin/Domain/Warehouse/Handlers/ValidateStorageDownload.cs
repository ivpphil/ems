﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateStorageDownload : IValidationHandler<IStorageDownloadCommand>
    {
        public IEnumerable<ValidationResult> Validate(IStorageDownloadCommand command)
        {
            yield return command.CheckRequired("s_date_from");
            yield return command.CheckRequired("s_date_to");

            if (command.s_date_from.HasValue && command.s_date_to.HasValue)
            {
                if ((command.s_date_from) > (command.s_date_to))
                    yield return new ValidationResult(ErsResources.GetMessage("10023", ErsResources.GetFieldName("schedule_date_from"), ErsResources.GetFieldName("schedule_date_to")));
            }
        }
    }
}