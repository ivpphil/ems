﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateOrderRegist
        : IValidationHandler<IOrderRegistCommand>
    {
        public IEnumerable<ValidationResult> Validate(IOrderRegistCommand command)
        {
            var existOrder = false;
            foreach (var model in command.orderRegistRecords)
            {
                existOrder = (existOrder || model.check_order == EnumOnOff.On);

                model.AddInvalidField(command.controller.commandBus.Validate<IOrderRegistRecordCommand>(model));
                if (!model.IsValid)
                {
                    foreach (var errorMessage in model.GetAllErrorMessageList())
                    {
                        yield return new ValidationResult(errorMessage, new[] { "orderRegistRecords" });
                    }
                }
            }

            if (!existOrder)
            {
                yield return new ValidationResult(ErsResources.GetMessage("10206", ErsResources.GetFieldName("check_order")), new[] { "orderRegistRecords" });
            }
        }
    }
}