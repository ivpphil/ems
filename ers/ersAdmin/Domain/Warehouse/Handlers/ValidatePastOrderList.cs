﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidatePastOrderList
        : IValidationHandler<IPastOrderListCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPastOrderListCommand command)
        {
            foreach (var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDateTime("s_orderdate_from", command.s_orderdate_from, "s_orderdate_to", command.s_orderdate_to))
            {
                yield return result;
            }
        }
    }
}