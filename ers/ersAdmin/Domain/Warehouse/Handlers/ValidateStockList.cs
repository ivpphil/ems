﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateStockList:IValidationHandler<IStockListCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IStockListCommand command)
        {
            if (command.search)
            {
                if ((command.s_warehousing_from) > (command.s_warehousing_to))
                    yield return new ValidationResult(ErsResources.GetMessage("10045", ErsResources.GetFieldName("s_warehousing_from"), ErsResources.GetFieldName("s_warehousing_to")));
            }
        }
    }
}