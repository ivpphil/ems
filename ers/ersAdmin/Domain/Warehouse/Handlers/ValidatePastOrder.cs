﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidatePastOrder
        : IValidationHandler<IPastOrderCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPastOrderCommand command)
        {
            yield return command.CheckRequired("order_no");
            yield break;
        }
    }
}