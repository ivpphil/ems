﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.warehouse;
using ersAdmin.Models;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class SupplierModifyHandler
        : ICommandHandler<ISupplierModifyCommand>
    {
        public ICommandResult Submit(ISupplierModifyCommand command)
        {
            this.UpdateSupplier(command);
            return new CommandResult(true);
        }

        /// <summary>
        /// Update record in wh_supplier_t
        /// </summary>
        internal void UpdateSupplier(ISupplierModifyCommand command)
        {
            //トランザクション開始
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhSupplierRepository();

            var new_supplier = ErsFactory.ersWarehouseFactory.GetErsWhSupplierWithId(command.id);
            new_supplier.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            var old_supplier = ErsFactory.ersWarehouseFactory.GetErsWhSupplierWithId(command.id);

            new_supplier.intime = old_supplier.intime;
            new_supplier.utime = DateTime.Now;

            repository.Update(old_supplier, new_supplier);
        }
    }
}