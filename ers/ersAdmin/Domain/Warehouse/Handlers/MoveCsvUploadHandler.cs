﻿using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.csv;
using System;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class MoveCsvUploadHandler : ICommandHandler<IMoveCsvUploadCommand>
    {
        public ICommandResult Submit(IMoveCsvUploadCommand command)
        {

            this.Process(command);
            return new CommandResult(true);

        }

        protected void Process(IMoveCsvUploadCommand command)
        {

            var repo = ErsFactory.ersWarehouseFactory.GetErsWhMoveRepository();
            foreach (var item in command.csv_file.GetValidModels())
            {
                this.InsertToWhMoveT(item);
                this.WhatMethodToBeCalledForWhStockT(item);
            }

        }

        protected void InsertToWhMoveT(move_csv_upload_record model)
        {
            var repo = ErsFactory.ersWarehouseFactory.GetErsWhMoveRepository();
            var whMove = ErsFactory.ersWarehouseFactory.GetErsWhMove();
            whMove.OverwriteWithModel(model);
            whMove.move_type = EnumWhMoveType.Move;

            repo.Insert(whMove, true);
        }

        protected void WhatMethodToBeCalledForWhStockT(move_csv_upload_record model)
        {
            if (model.shelf_from != null)
                this.DescreaseStock(model);

            if(model.shelf_to != null)
                this.IncreaseStock(model);
        }

        protected void DescreaseStock(move_csv_upload_record model)
        {
            var repo = ErsFactory.ersWarehouseFactory.GetErsWhStockRepository();
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhStockCriteria();
            criteria.scode = model.scode;

            if (repo.GetRecordCount(criteria) > 0)
            {
                ErsFactory.ersWarehouseFactory.GetDecreaseWhStockStgy().Decrease(model.scode, model.amount.Value, model.shelf_from);

                if (model.shelf_from == EnumShelfNumber.SHELF001)
                    ErsFactory.ersWarehouseFactory.GetDecreaseWhStockStgy().Decrease(model.scode, model.amount.Value,null);

            }
        }

        protected void IncreaseStock(move_csv_upload_record model)
        {
            var repo = ErsFactory.ersWarehouseFactory.GetErsWhStockRepository();
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhStockCriteria();
            criteria.scode = model.scode;

            if (repo.GetRecordCount(criteria) > 0)
            {
                ErsFactory.ersWarehouseFactory.GetIncreaseWhStockStgy().Increase(model.scode, model.amount.Value, model.shelf_to);

                if (model.shelf_to == EnumShelfNumber.SHELF001)
                    ErsFactory.ersWarehouseFactory.GetIncreaseWhStockStgy().Increase(model.scode, model.amount.Value,null);

            }
        }
        

    }
}