﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using ersAdmin.Models.warehouse;
using jp.co.ivp.ers;
using jp.co.ivp.ers.warehouse;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class SupplierRegisterHandler
        : ICommandHandler<ISupplierRegisterCommand>
    {
        public ICommandResult Submit(ISupplierRegisterCommand command)
        {
            this.InsertSupplier(command);
            return new CommandResult(true);
        }

        /// <summary>
        /// Insert supplier values in wh_supplier_t table
        /// </summary>
        internal void InsertSupplier(ISupplierRegisterCommand command)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhSupplierRepository();

            var supplier = ErsFactory.ersWarehouseFactory.GetErsWhSupplier();
            supplier.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            supplier.intime = DateTime.Now;
            supplier.active = EnumActive.Active;

            repository.Insert(supplier);
        }
    }
}