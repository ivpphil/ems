﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateOrderListPDF
        : IValidationHandler<IOrderListPDFCommand>
    {
        public IEnumerable<ValidationResult> Validate(IOrderListPDFCommand command)
        {
            yield return command.CheckRequired("order_no");
            yield break;
        }
    }
}