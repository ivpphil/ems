﻿using ersAdmin.Domain.Warehouse.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.warehouse;
using ersAdmin.Models.csv;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class StorageCsvUploadHandler : ICommandHandler<IStorageCsvUploadCommand>
    {
        private int? new_amount { get; set; }

        public ICommandResult Submit(IStorageCsvUploadCommand command)
        {
            this.Process(command);
            return new CommandResult(true);
        }

        internal void Process(IStorageCsvUploadCommand command)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhStorageRepository();

            foreach (var item in command.csv_file.GetValidModels())
            {
                ///insert storage
                this.InsertWhStorage(item);
                //update stock
                this.UpdateWhStock(item);
                //insert wh_move_t
                this.InsertWhMove(item);
                ///update order status
                this.UpdateOrderStatus(item);
            }
        }

        internal void InsertWhStorage(IStorageCsvUploadRecordCommand item)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhStorageRepository();
            var entity = ErsFactory.ersWarehouseFactory.GetErsWhStorage();
            entity.OverwriteWithModel(item);
            entity.active = EnumActive.Active;
            repository.Insert(entity);
        }

        internal void InsertWhMove(storage_csv_upload_record item)
        {
            foreach (EnumShelfNumber val in EnumShelfNumber.GetValues(typeof(EnumShelfNumber)))
            {
                if (this.CheckIfPassed(item,val))
                {
                    var repository = ErsFactory.ersWarehouseFactory.GetErsWhMoveRepository();
                    var entity = ErsFactory.ersWarehouseFactory.GetErsWhMove();
                    entity.OverwriteWithModel(item);
                    entity.reason = "入庫";
                    entity.shelf_from = null;

                    entity.shelf_to = val;
                    entity.amount = this.new_amount;

                    entity.move_type = EnumWhMoveType.Storage;
                    entity.active = EnumActive.Active;

                    repository.Insert(entity);
                }
            }
        }

        internal bool CheckIfPassed(storage_csv_upload_record item,EnumShelfNumber shelfNumber)
        {
            bool returnPass = true;

            if (shelfNumber == EnumShelfNumber.SHELF001)
            {
                if (item.shelf001 != 0)
                    this.new_amount = item.shelf001;
                else
                    return false;
            }
            else if (shelfNumber == EnumShelfNumber.SHELF002)
            {
                if (item.shelf002 != 0)
                    this.new_amount = item.shelf002;
                else
                    return false;
            }
            else if (shelfNumber == EnumShelfNumber.SHELF003)
            {
                if (item.shelf003 != 0)
                    this.new_amount = item.shelf003;
                else
                    return false;
            }

            return returnPass;
        }

        internal void UpdateWhStock(storage_csv_upload_record item)
        {
            var entity = ErsFactory.ersWarehouseFactory.GetErsWhStorage();
            entity.OverwriteWithModel(item);
            entity.shelf001 = (item.shelf001.HasValue) ? item.shelf001 : 0;
            entity.shelf002 = (item.shelf002.HasValue) ? item.shelf002 : 0;
            entity.shelf003 = (item.shelf003.HasValue) ? item.shelf003 : 0;

            var repository = ErsFactory.ersWarehouseFactory.GetErsWhStockRepository();
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhStockCriteria();
            criteria.scode = item.scode;

            if (repository.GetRecordCount(criteria) > 0)
            {
                var upStock = ErsFactory.ersWarehouseFactory.GetUpdateWhStockStgy();
                upStock.UpdateStock(entity);
            }
            else this.InsertWhStock(item, repository);

        }

        internal void InsertWhStock(storage_csv_upload_record item,ErsWhStockRepository repository)
        {
            var entity = ErsFactory.ersWarehouseFactory.GetErsWhStock();
            entity.OverwriteWithModel(item);
            entity.active = EnumActive.Active;
            entity.stock = item.shelf001;
            repository.Insert(entity);
        }


        internal void UpdateOrderStatus(storage_csv_upload_record item)
        {
            int shelves = ErsFactory.ersWarehouseFactory.GetGetSumShelvesStgy().GetSumShelves(item.scode, item.order_no, 0);

            int? orderAmount = ErsFactory.ersWarehouseFactory.GetGetOrderAmount().OrderAmount(item.scode, item.order_no);

            if (orderAmount == shelves)
            {
                var updateWhOrder = ErsFactory.ersWarehouseFactory.GetUpdateWhOrderInfoStgy();
                updateWhOrder.UpdateWhOrderInfo(item.order_no);
            }
        }
    }
}