﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidateSupplierList
        : IValidationHandler<ISupplierListCommand>
    {
        public IEnumerable<ValidationResult> Validate(ISupplierListCommand command)
        {
            foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("pref", command.s_pref))
            {
                yield return result;
            }
        }
    }
}