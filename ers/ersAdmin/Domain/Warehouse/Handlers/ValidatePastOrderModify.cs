﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Warehouse.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.warehouse;

namespace ersAdmin.Domain.Warehouse.Handlers
{
    public class ValidatePastOrderModify
        : IValidationHandler<IPastOrderModifyCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPastOrderModifyCommand command)
        {
            yield return command.CheckRequired("order_no");
            yield return command.CheckRequired("wh_order_status");

            ErsWhOrderInfo objWhOrderInfo = null;
            if (command.IsValidField("order_no"))
            {
                //order_no存在チェック
                var repository = ErsFactory.ersWarehouseFactory.GetErsWhOrderInfoRepository();
                var criteria = ErsFactory.ersWarehouseFactory.GetErsWhOrderInfoCriteria();
                criteria.order_no = command.order_no;
                criteria.active = EnumActive.Active;
                objWhOrderInfo = repository.FindSingle(criteria);
                if (objWhOrderInfo == null)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "order_no" });
                    yield break;
                }

                //入庫未完以外のステータスの場合はステータスを更新できない
                if (objWhOrderInfo.wh_order_status != EnumWhOrderStatus.NotStorage &&
                    objWhOrderInfo.wh_order_status != command.wh_order_status)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("30401"), new[] { "wh_order_status" });
                }
            }



            //一部入庫済みはキャンセルできない
            if (command.wh_order_status == EnumWhOrderStatus.Canceled)
            {
                var repository = ErsFactory.ersWarehouseFactory.GetErsWhStorageRepository();
                var criteria = ErsFactory.ersWarehouseFactory.GetErsWhStorageCriteria();
                criteria.active = EnumActive.Active;
                criteria.order_no = command.order_no;
                if (repository.GetRecordCount(criteria) > 0)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("30402"), new[] { "wh_order_status" });
                }
            }
        }
    }
}