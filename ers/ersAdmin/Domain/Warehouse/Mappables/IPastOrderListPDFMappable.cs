﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.warehouse;

namespace ersAdmin.Domain.Warehouse.Mappables
{
    public interface IPastOrderListPDFMappable
        : IMappable, IOrderListPDFMappable
    {
    }
}