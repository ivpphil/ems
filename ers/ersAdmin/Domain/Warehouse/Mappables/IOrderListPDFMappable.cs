﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System.IO;
using ersAdmin.Models.warehouse;

namespace ersAdmin.Domain.Warehouse.Mappables
{
    public interface IOrderListPDFMappable
        : IMappable, IPastOrderListMappable
    {
        Byte[] pdf { get; set; }

        IEnumerable<order_list_pdf_record> listOrder { get; set; }

        string order_no { get; set; }
    }
}
