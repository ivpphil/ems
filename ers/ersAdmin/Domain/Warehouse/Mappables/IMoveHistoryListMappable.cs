﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using ersAdmin.Models.warehouse;

namespace ersAdmin.Domain.Warehouse.Mappables
{
    public interface IMoveHistoryListMappable : IMappable
    {
        ErsPagerModel pager { get; set; }

        long recordCount { get; set; }

        IEnumerable<move_history_record> listMoveHistory { get; set; }

        DateTime? s_movedate_from { get; set; }

        DateTime? s_movedate_to { get; set; }

        EnumWhMoveType? s_move_type { get; set; }

        string s_supplier_code { get; set; }

        string s_supplier_name { get; set; }

        string s_scode { get; set; }

        string s_maker_scode { get; set; }

        string s_sname { get; set; }

        bool IsSearchPage { get; }
    }
}