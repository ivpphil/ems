﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Warehouse.Mappables
{
    public interface IPastOrderListMappable
        : IMappable
    {
        ErsPagerModel pager { get; set; }

        long recordCount { get; set; }

        DateTime? s_intime { get; set; }

        DateTime? s_orderdate_from { get; set; }

        DateTime? s_orderdate_to { get; set; }

        string s_order_no { get; set; }

        string s_supplier_code { get; set; }

        string s_supplier_name { get; set; }

        string s_scode { get; set; }

        string s_sname { get; set; }

        IEnumerable<Models.warehouse.past_order_record> listPastOrder { get; set; }

        EnumWhOrderStatus? s_wh_order_status { get; set; }
    }
}