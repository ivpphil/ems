﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.warehouse;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Warehouse.Mappables
{
    public interface IPastOrderMappable
        : IMappable
    {
        string order_no { get; set; }

        string supplier_code { get; set; }

        string supplier_name { get; set; }

        IEnumerable<past_order_record> listPastOrder { get; set; }

        EnumWhOrderStatus? wh_order_status { get; set; }

        string remarks { get; set; }

        DateTime? intime { get; set; }

        bool IsUnderStock { get; set; }

        bool IsInitialize { get; set; }

        bool IsDenyModify { get; set; }
    }
}