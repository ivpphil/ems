﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Warehouse.Mappables
{
    public interface ISupplierModifyMappable
        : IMappable
    {
        int id { get; set; }

        string supplier_code { get; set; }

        string supplier_name { get; set; }

        int? pref { get; set; }

        string zip { get; set; }

        string address { get; set; }

        string tel { get; set; }

        string fax { get; set; }

        string email { get; set; }
    }
}