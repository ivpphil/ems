﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Warehouse.Mappables
{
    public interface IPastOrderListCsvMappable
        : IMappable, IPastOrderListMappable
    {
        ErsCsvCreater csvCreater { get; }
    }
}