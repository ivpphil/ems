﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.warehouse;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.Warehouse.Mappables
{
    public interface IOrderListMappable
        : IMappable
    {
        IEnumerable<OrderRegistRecord> orderRegistRecords { get; set; }

        DateTime? s_intime_less_than { get; set; }

        string s_supplier_code { get; set; }

        string s_sname { get; set; }

        string s_scode { get; set; }

        EnumWhOrderType? s_wh_order_type { get; set; }

        ErsPagerModel pager { get; set; }

        long recordCount { get; set; }
    }
}
