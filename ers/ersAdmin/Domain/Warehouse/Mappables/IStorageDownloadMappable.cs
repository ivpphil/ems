﻿using System;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Warehouse.Mappables
{
    public interface IStorageDownloadMappable : IMappable
    {

        DateTime? s_date_from { get; set; }
        DateTime? s_date_to { get; set; }
        ErsCsvCreater csvCreater { get; set; }
        bool CreateCsvFile { get; set; }
        bool hasRecord { get; set; }

    }
}
