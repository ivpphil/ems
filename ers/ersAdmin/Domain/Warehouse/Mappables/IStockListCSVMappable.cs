﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.Warehouse.Mappables
{
    public interface IStockListCSVMappable:IMappable
    {
        ErsCsvCreater csvCreater { get; set; }

        ErsPagerModel pager { get; set; }

        DateTime? s_warehousing_from { get; set; }

        DateTime? s_warehousing_to { get; set; }

        string s_supplier_code { get; }

        string s_supplier_name { get; }

        string s_scode { get; }

        string s_sname { get; }

        int? s_stock_type { get; }
    }
}