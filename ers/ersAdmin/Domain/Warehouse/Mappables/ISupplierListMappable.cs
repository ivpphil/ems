﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.warehouse;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.Warehouse.Mappables
{
    public interface ISupplierListMappable
        : IMappable
    {
        ErsPagerModel pager { get; set; }

        long recordCount { get; set; }

        string s_supplier_code { get; set; }

        string s_supplier_name { get; set; }

        string s_zip { get; set; }

        int? s_pref { get; set; }

        string s_address { get; set; }

        string s_tel { get; set; }

        string s_fax { get; set; }

        string s_email { get; set; }

        List<Dictionary<string, object>> list { set; }
    }
}
