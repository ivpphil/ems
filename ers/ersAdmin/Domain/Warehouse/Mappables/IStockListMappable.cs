﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.Warehouse.Mappables
{
    public interface IStockListMappable:IMappable
    {
        ErsPagerModel pager { get; set; }

        long recordCount { get; set; }

        DateTime? s_warehousing_from { get; set; }

        DateTime? s_warehousing_to { get; set; }

        string s_supplier_code { get; }

        string s_supplier_name { get; }

        string s_scode { get; }

        string s_sname { get; }
        
        int? s_stock_type { get; }

        List<Dictionary<string, object>> list { set; }

        bool IsSearchPage { get; }
    }
}