﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers;
using ersAdmin.Models.warehouse.csv;
using ersAdmin.Models.warehouse;

namespace ersAdmin.Domain.Warehouse.Mappers
{
    public class PastOrderListCsvMapper
        : PastOrderListMapper, IMapper<IPastOrderListCsvMappable>
    {
        public void Map(IPastOrderListCsvMappable objMappable)
        {
            objMappable.listPastOrder = this.GetListPastOrder(objMappable);

            this.CreateCsvFile(objMappable);
        }

        protected override IEnumerable<past_order_record> GetListPastOrder(IPastOrderListMappable objMappable)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhOrderRepository();

            //検索条件をクライテリアに保存
            var criteria = this.GetCriteria(objMappable);

            if (objMappable.pager != null)
            {
                //検索SQLにLIMIT と OFFSETを加える
                objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

                criteria.SetOrderByOrderNo();

                var listWhOrderHeader = repository.FindPastOrderList(criteria);

                var arr_order_no = listWhOrderHeader.Select((dictionary) => Convert.ToString(dictionary["order_no"]));

                criteria = this.GetCriteria(objMappable);
                criteria.order_no_in = arr_order_no;
            }

            criteria.SetOrderByOrderNo();
            criteria.SetOrderBySScode();

            var listWhOrder = repository.FindPastList(criteria);

            var listPastOrder = new List<past_order_record>();
            foreach (var record in listWhOrder)
            {
                var pastOrder = new past_order_record();
                pastOrder.OverwriteWithParameter(record);
                listPastOrder.Add(pastOrder);
            }
            return listPastOrder;
        }

        public void CreateCsvFile(IPastOrderListCsvMappable objMappable)
        {
            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            past_order_record_csv item_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<past_order_record_csv>(writer);
                foreach (var item in objMappable.listPastOrder)
                {
                    var past_order_record = new past_order_record_csv();
                    past_order_record.OverwriteWithParameter(item.GetPropertiesAsDictionary());

                    item_csv = new past_order_record_csv();
                    item_csv.OverwriteWithParameter(past_order_record.GetPropertiesAsDictionary());
                    string w_w_up_stock = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.WhUpStock, EnumCommonNameColumnName.namename, (int?)item.up_stock);
                    if (past_order_record.w_up_stock.HasValue())
                    {
                        item_csv.w_up_stock = ErsResources.GetMessage(w_w_up_stock);
                    }
                    string w_w_wh_order_status = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.WhOrderStatus, EnumCommonNameColumnName.namename, (int?)item.wh_order_status);
                    if (past_order_record.w_wh_order_status.HasValue()) { 
                        item_csv.w_wh_order_status = ErsResources.GetMessage(w_w_wh_order_status);
                    }
                    objMappable.csvCreater.WriteBody(item_csv, writer);
                }
            }
        }
    }
}