﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.warehouse;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using ersAdmin.Models.csv;

namespace ersAdmin.Domain.Warehouse.Mappers
{
    public class StockListCSVMapper:IMapper<IStockListCSVMappable>
    {
        public void Map(IStockListCSVMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }
        public void CreateCsvFile(IStockListCSVMappable objMappable)
        {
            var spec = ErsFactory.ersWarehouseFactory.GetSearchStockSpec();
            var criteria = this.GetCriteria(objMappable);
            
            criteria.scode_not_equal = null;

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            }

            criteria.SetOrderBySID(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);

            var list = spec.GetSearchData(criteria);

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            ersAdmin.Models.csv.stock_csv_record item_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<ersAdmin.Models.csv.stock_csv_record>(writer);

                foreach (var item in list)
                {
                    var cvs_items = new stock_csv_record();
                    cvs_items.OverwriteWithParameter(item);

                    item_csv = new ersAdmin.Models.csv.stock_csv_record();
                    item_csv.OverwriteWithParameter(cvs_items.GetPropertiesAsDictionary());
                    objMappable.csvCreater.WriteBody(item_csv, writer);
                }
            }

        }
        private ErsWhStockCriteria GetCriteria(IStockListCSVMappable objMappable)
        {
            string strSQL = "";

            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhStockCriteria();

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //除外するgcode
            criteria.ignore_gcode = setup.IgnoreGcode;

            if (!string.IsNullOrEmpty(objMappable.s_supplier_code))
                criteria.supplier_code_prefix = objMappable.s_supplier_code;

            if (!string.IsNullOrEmpty(objMappable.s_supplier_name))
                criteria.supplier_name_ambi = objMappable.s_supplier_name;

            if (!string.IsNullOrEmpty(objMappable.s_scode))
                criteria.sm_scode = objMappable.s_scode;

            if (!string.IsNullOrEmpty(objMappable.s_sname))
                criteria.sname_ambi = objMappable.s_sname;

            if (objMappable.s_stock_type.HasValue)
            {
                strSQL = "";

                strSQL = (objMappable.s_stock_type == 1) ? "((wh_stock_t.shelf001+wh_order_t.amount) >0)" : (objMappable.s_stock_type == 2) ? "((wh_stock_t.shelf001+wh_order_t.amount) <=0)" : (objMappable.s_stock_type == 3) ? "((wh_stock_t.shelf001+wh_order_t.amount) <= s_master_t.wh_stock_alert_amount)" : "";

                var parameters = new Dictionary<string, object>();
                criteria.Add(Criteria.GetUniversalCriterion(strSQL, parameters));
            }

            //criteria.active = EnumActive.Active;

            if ((objMappable.s_warehousing_from != null) && (objMappable.s_warehousing_to != null))
            {
                strSQL = "";
                strSQL = "EXISTS(SELECT * FROM wh_storage_t "
                      + " inner join wh_order_t on wh_order_t.order_no = wh_storage_t.order_no and wh_order_t.scode = wh_storage_t.scode "
                      + " WHERE wh_order_t.order_no = wh_storage_t.order_no "
                      + " AND wh_storage_t.active = " + (int)EnumActive.Active + " AND wh_storage_t.intime BEtWEEN "
                      + " '" + objMappable.s_warehousing_from + "' and '" + objMappable.s_warehousing_to + "')";

                var parameters = new Dictionary<string, object>();
                criteria.Add(Criteria.GetUniversalCriterion(strSQL, parameters));
            }

            return criteria;
        }
    }
}