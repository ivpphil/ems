﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers.warehouse;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Warehouse.Mappers
{
    public class PastOrderListPDFMapper
        : OrderListPDFMapper, IMapper<IPastOrderListPDFMappable>
    {
        public void Map(IPastOrderListPDFMappable objMappable)
        {
            this.SetListOrder(objMappable);
            this.MapPdf(objMappable);
        }

        /// <summary>
        /// 検索条件をクライテリアにセット
        /// </summary>
        /// <returns></returns>
        protected override ErsWhOrderCriteria GetCriteria(IOrderListPDFMappable objMappable)
        {
            ErsWhOrderCriteria criteria = ErsFactory.ersWarehouseFactory.GetErsWhOrderCriteria();

            //クライテリアにパラメタを渡す
            if (objMappable.s_intime.HasValue)
            {
                criteria.intime = objMappable.s_intime;
            }

            if (objMappable.s_orderdate_from.HasValue)
            {
                criteria.intime_from = objMappable.s_orderdate_from.Value;
            }

            if (objMappable.s_orderdate_to.HasValue)
            {
                criteria.intime_to = objMappable.s_orderdate_to.Value;
            }

            if (!string.IsNullOrEmpty(objMappable.s_order_no))
                criteria.order_no = objMappable.s_order_no;

            if (!string.IsNullOrEmpty(objMappable.s_supplier_code))
                criteria.supplier_code_ambi = objMappable.s_supplier_code;

            if (!string.IsNullOrEmpty(objMappable.s_supplier_name))
                criteria.supplier_name_ambiguous = objMappable.s_supplier_name;

            if (!string.IsNullOrEmpty(objMappable.s_scode))
                criteria.scode_ambi = objMappable.s_scode;

            if (!string.IsNullOrEmpty(objMappable.s_sname))
                criteria.sname_ambiguous = objMappable.s_sname;

            if (objMappable.s_wh_order_status.HasValue)
                criteria.wh_order_status = objMappable.s_wh_order_status;

            criteria.active = EnumActive.Active;

            return criteria;
        }
    }
}