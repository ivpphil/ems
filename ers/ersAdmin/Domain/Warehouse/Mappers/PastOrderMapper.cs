﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Warehouse.Mappables;
using ersAdmin.Models.warehouse;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.warehouse;

namespace ersAdmin.Domain.Warehouse.Mappers
{
    public class PastOrderMapper : IMapper<IPastOrderMappable>
    {
        public void Map(IPastOrderMappable objMappable)
        {
            LoadObject(objMappable);
        }

        public void LoadObject(IPastOrderMappable objMappable)
        {
            this.SetOrderInfo(objMappable);
            this.SetListPastOrder(objMappable);
        }

        private void SetOrderInfo(IPastOrderMappable objMappable)
        {
            //発注情報をモデルにセット
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhOrderInfoRepository();
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhOrderInfoCriteria();
            criteria.active = EnumActive.Active;
            criteria.order_no = objMappable.order_no;
            var objOrderInfo = repository.FindSingle(criteria);
            if (objOrderInfo == null)
            {
                throw new ErsException(ErsResources.GetMessage("10200"));
            }
            objMappable.intime = objOrderInfo.intime;
            objMappable.IsDenyModify = objOrderInfo.wh_order_status != EnumWhOrderStatus.NotStorage;

            if (objMappable.IsInitialize)
            {
                //初期表示の場合のみセット
                objMappable.wh_order_status = objOrderInfo.wh_order_status;
                objMappable.remarks = objOrderInfo.remarks;
            }

            var objSupplier = ErsFactory.ersWarehouseFactory.GetErsWhSupplierWithSupplierCode(objOrderInfo.supplier_code);
            objMappable.supplier_code = objSupplier.supplier_code;
            objMappable.supplier_name = objSupplier.supplier_name;
        }
        
        /// <summary>
        /// 発注明細をモデルにセット
        /// </summary>
        /// <param name="objMappable"></param>
        public void SetListPastOrder(IPastOrderMappable objMappable)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhOrderRepository();

            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhOrderCriteria();
            criteria.order_no = objMappable.order_no;
            criteria.active = EnumActive.Active;

            criteria.SetOrderBySScode();

            var listWhOrder = repository.FindPastList(criteria);

            var listPastOrder = new List<past_order_record>();
            foreach (var record in listWhOrder)
            {
                var pastOrder = new past_order_record();
                pastOrder.OverwriteWithParameter(record);

                listPastOrder.Add(pastOrder);
            }

            objMappable.listPastOrder = listPastOrder;

            //Web在庫切れチェック
            var strategy = ErsFactory.ersWarehouseFactory.GetCheckOrderCancelStrategy();
            foreach (var objPastOrder in listPastOrder)
            {
                if (strategy.IsOverCancelAmount(objPastOrder.scode, objPastOrder.amount))
                {
                    objMappable.IsUnderStock = true;
                    break;
                }
            }
        }


    }
}