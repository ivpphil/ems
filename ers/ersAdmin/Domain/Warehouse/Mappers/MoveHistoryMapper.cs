﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.warehouse;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using ersAdmin.Models.warehouse;

namespace ersAdmin.Domain.Warehouse.Mappers
{
    public class MoveHistoryMapper:IMapper<IMoveHistoryListMappable>
    {

        public void Map(IMoveHistoryListMappable objMappable)
        {
            if (objMappable.IsSearchPage)
            {
                this.SetDefaultData(objMappable);
                return;
            }
            objMappable.listMoveHistory =  this.Init(objMappable);
        }

        private IEnumerable<move_history_record> Init(IMoveHistoryListMappable objMappable)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhMoveRepository();
            var listMoveRecord = new List<move_history_record>();

            var criteria = this.GetCriteria(objMappable);

            objMappable.recordCount = repository.GetWhMoveListCount(criteria);

            if (objMappable.recordCount == 0)
                return listMoveRecord;

            //検索SQLにLIMIT と OFFSETを加える
            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            criteria.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.SetOrderByID(Criteria.OrderBy.ORDER_BY_ASC);

            var list = repository.FindWhMoveList(criteria);

            foreach (var move in list)
            {
                var MoveHistoryRecord = new move_history_record();
                MoveHistoryRecord.OverwriteWithParameter(move);
                listMoveRecord.Add(MoveHistoryRecord);
            }

            return listMoveRecord;
        }

        /// <summary>
        /// 初期値を設定する。
        /// </summary>
        private void SetDefaultData(IMoveHistoryListMappable objMappable)
        {
            if (objMappable.s_movedate_from == null)
            {
                objMappable.s_movedate_from = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/01 00:00:00"));
            }
            if (objMappable.s_movedate_to == null)
            {
                objMappable.s_movedate_to = Convert.ToDateTime(DateTime.Now.AddMonths(1).ToString("yyyy/MM/01 23:59:59")).AddDays(-1);
            }
        }

        private ErsWhMoveCriteria GetCriteria(IMoveHistoryListMappable objMappable)
        {
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhMoveCriteria();

            if (objMappable.s_movedate_from != null)
                criteria.intime_from = DateTimeExtension.GetStartForSearch(objMappable.s_movedate_from.Value);

            if (objMappable.s_movedate_to != null)
                criteria.intime_to = DateTimeExtension.GetEndForSearch(objMappable.s_movedate_to.Value);

            if(objMappable.s_move_type.HasValue)
                criteria.move_type = objMappable.s_move_type;

            if (!string.IsNullOrEmpty(objMappable.s_supplier_code))
                criteria.supplier_code_ambi = objMappable.s_supplier_code;

            if (!string.IsNullOrEmpty(objMappable.s_supplier_name))
                criteria.supplier_name_ambi = objMappable.s_supplier_name;

            if (!string.IsNullOrEmpty(objMappable.s_scode))
                criteria.scode_ambi = objMappable.s_scode;

            if (!string.IsNullOrEmpty(objMappable.s_maker_scode))
                criteria.maker_scode_sm_prefix = objMappable.s_maker_scode;

            if (!string.IsNullOrEmpty(objMappable.s_sname))
                criteria.sname_ambi = objMappable.s_sname;

            criteria.active = EnumActive.Active;

            return criteria;
        }
    }
}