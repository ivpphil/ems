﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers.warehouse;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using ersAdmin.Models.warehouse;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Warehouse.Mappers
{
    public class MoveHistorySearchCSVMapper:IMapper<IMoveHistorySearchCSVMappable>
    {
        public void Map(IMoveHistorySearchCSVMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }

        public void CreateCsvFile(IMoveHistorySearchCSVMappable objMappable)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhMoveRepository();
            var criteria = this.GetCriteria(objMappable);

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            }

            criteria.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.SetOrderByID(Criteria.OrderBy.ORDER_BY_ASC);

            var itemList = repository.FindWhMoveList(criteria);

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            ersAdmin.Models.csv.move_history_csv item_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<ersAdmin.Models.csv.move_history_csv>(writer);
                foreach (var item in itemList)
                {
                    var MoveHistoryRecord = new move_history_record();
                    MoveHistoryRecord.OverwriteWithParameter(item);

                    item_csv = new ersAdmin.Models.csv.move_history_csv();
                    item_csv.OverwriteWithParameter(MoveHistoryRecord.GetPropertiesAsDictionary());
                    if (MoveHistoryRecord.w_move_type.HasValue()) { 
                        item_csv.w_move_type = ErsResources.GetMessage(MoveHistoryRecord.w_move_type);
                    }
                    if (MoveHistoryRecord.w_shelf_from.HasValue())
                    {
                        item_csv.w_shelf_from = ErsResources.GetMessage(MoveHistoryRecord.w_shelf_from);
                    }
                    if (MoveHistoryRecord.w_shelf_to.HasValue())
                    {
                        item_csv.w_shelf_to = ErsResources.GetMessage(MoveHistoryRecord.w_shelf_to);
                    } 
                    objMappable.csvCreater.WriteBody(item_csv, writer);
                }
            }
        }

        private ErsWhMoveCriteria GetCriteria(IMoveHistorySearchCSVMappable objMappable)
        {
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhMoveCriteria();

            if (objMappable.s_movedate_from != null)
                criteria.intime_from = DateTimeExtension.GetStartForSearch(objMappable.s_movedate_from.Value);

            if (objMappable.s_movedate_to != null)
                criteria.intime_to = DateTimeExtension.GetEndForSearch(objMappable.s_movedate_to.Value);

            if (objMappable.s_move_type.HasValue)
                criteria.move_type = objMappable.s_move_type;

            if (!string.IsNullOrEmpty(objMappable.s_supplier_code))
                criteria.supplier_code = objMappable.s_supplier_code;

            if (!string.IsNullOrEmpty(objMappable.s_supplier_name))
                criteria.supplier_name = objMappable.s_supplier_name;

            if (!string.IsNullOrEmpty(objMappable.s_scode))
                criteria.scode_sm = objMappable.s_scode;

            if (!string.IsNullOrEmpty(objMappable.s_maker_scode))
                criteria.maker_scode_sm_prefix = objMappable.s_maker_scode;

            if (!string.IsNullOrEmpty(objMappable.s_sname))
                criteria.sname = objMappable.s_sname;

            criteria.active = EnumActive.Active;

            return criteria;
        }
    }
}