﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.warehouse;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Warehouse.Mappers
{
    public class SupplierListMapper
        : IMapper<ISupplierListMappable>
    {
        public void Map(ISupplierListMappable objMappable)
        {
            objMappable.list = this.GetListSupplier(objMappable);
        }

        /// <summary>
        /// Supplier リストを初期化する。
        /// </summary>
        /// <param name="pager"></param>
        private List<Dictionary<string, object>> GetListSupplier(ISupplierListMappable objMappable)
        {
            ErsWhSupplierRepository supplierRepository = ErsFactory.ersWarehouseFactory.GetErsWhSupplierRepository();
            var listSupplier = new List<Dictionary<string, object>>();

            //検索条件をクライテリアに保存
            var emCri = this.GetCriteria(objMappable);

            //検索結果の総数を取得
            //objMappable.recordCount = supplierRepository.GetRecordCount(emCri);
            objMappable.recordCount = supplierRepository.GetRecordCount(emCri);

            if (objMappable.recordCount == 0) 
                return listSupplier;

            //検索SQLにLIMIT と OFFSETを加える
            objMappable.pager.SetLimitAndOffsetToCriteria(emCri);

            emCri.SetOrderBySupplierCode(Criteria.OrderBy.ORDER_BY_ASC);

            var list = supplierRepository.Find(emCri);

            foreach (var supplier in list)
            {
                var dictionary = supplier.GetPropertiesAsDictionary();
                dictionary["w_pref"] = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(supplier.pref.Value, (int)EnumSiteId.COMMON_SITE_ID);
                listSupplier.Add(dictionary);
            }

            return listSupplier;
        }


        /// <summary>
        /// 検索条件をクライテリアにセット
        /// </summary>
        /// <returns></returns>
        private ErsWhSupplierCriteria GetCriteria(ISupplierListMappable objMappable)
        {
            ErsWhSupplierCriteria emCri = ErsFactory.ersWarehouseFactory.GetErsWhSupplierCriteria();

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //クライテリアにパラメタを渡す
            if (!string.IsNullOrEmpty(objMappable.s_supplier_code))
                emCri.supplier_code_ambi = objMappable.s_supplier_code;

            if (!string.IsNullOrEmpty(objMappable.s_supplier_name))
                emCri.supplier_name_ambi = objMappable.s_supplier_name;

            if (!string.IsNullOrEmpty(objMappable.s_zip))
                emCri.zip = objMappable.s_zip;

            if (objMappable.s_pref.HasValue)
                emCri.pref = objMappable.s_pref;

            if (!string.IsNullOrEmpty(objMappable.s_address))
                emCri.address_ambi = objMappable.s_address;

            if (!string.IsNullOrEmpty(objMappable.s_tel))
                emCri.tel_prefix = objMappable.s_tel;

            if (!string.IsNullOrEmpty(objMappable.s_fax))
                emCri.fax_prefix = objMappable.s_fax;

            if (!string.IsNullOrEmpty(objMappable.s_email))
                emCri.email_prefix = objMappable.s_email;

            return emCri;
        }
    }
}