﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Warehouse.Mappables;
using System.IO;
using Rotativa;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using ersAdmin.Models.warehouse;
using jp.co.ivp.ers.warehouse;

namespace ersAdmin.Domain.Warehouse.Mappers
{
    public class OrderListPDFMapper
        : IMapper<IOrderListPDFMappable>
    {
        public void Map(IOrderListPDFMappable objMappable)
        {
            this.SetListOrder(objMappable);
            this.MapPdf(objMappable);
        }

        protected virtual void SetListOrder(IOrderListPDFMappable objMappable)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhOrderRepository();
            var criteria = this.GetCriteria(objMappable);

            if (objMappable.pager != null)
            {
                //検索SQLにLIMIT と OFFSETを加える
                objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

                criteria.SetOrderByOrderNo();

                var listWhOrderHeader = repository.FindPastOrderList(criteria);

                var arr_order_no = listWhOrderHeader.Select((dictionary) => Convert.ToString(dictionary["order_no"]));

                criteria = this.GetCriteria(objMappable);
                criteria.order_no_in = arr_order_no;
            }

            var listRecord = repository.FindPastList(criteria);

            order_list_pdf_record currentHeaderRecord = null;
            order_list_pdf_record lastRecord = null;
            int header_total = 0;
            
            var listResult = new List<order_list_pdf_record>();
            foreach (var record in listRecord)
            {
                var model = new order_list_pdf_record();
                model.OverwriteWithParameter(record);
                model.sname = this.GetSname(model.scode);
                model.total = model.cost_price * model.amount;

                if (currentHeaderRecord == null || model.order_no != currentHeaderRecord.order_no)
                {
                    //配送番号切り替わり時の処理（ヘッダ作成）
                    model.IsDispHeader = true;
                    if (currentHeaderRecord != null)
                    {
                        lastRecord.IsDispFooter = true;
                        lastRecord.IsNewPage = true;
                        lastRecord.header_total = header_total;
                        header_total = 0;
                    }

                    currentHeaderRecord = model;

                    var supplier = this.GetSupplier(objMappable, model.supplier_code);
                    if (supplier != null)
                    {
                        model.supplier_name = supplier.supplier_name;
                        model.zip = supplier.zip;
                        model.w_pref = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(supplier.pref, (int)EnumSiteId.COMMON_SITE_ID);
                        model.address = supplier.address;
                        model.tel = supplier.tel;
                        model.fax = supplier.fax;
                    }
                    var setup = ErsFactory.ersUtilityFactory.getSetup();
                    model.outsourcer = setup.wh_owner_description;

                }

                listResult.Add(model);

                header_total += model.total.Value;

                lastRecord = model;
            }

            //最終行の値セット
            lastRecord.IsDispFooter = true;
            lastRecord.header_total = header_total;

            objMappable.listOrder = listResult;
        }

        protected virtual ErsWhOrderCriteria GetCriteria(IOrderListPDFMappable objMappable)
        {
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhOrderCriteria();
            if (objMappable.order_no.HasValue())
            {
                criteria.order_no = objMappable.order_no;
            }
            criteria.active = EnumActive.Active;
            return criteria;
        }

        private string GetSname(string scode)
        {
            var objSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(scode);
            if (objSku == null)
            {
                return null;
            }
            return objSku.sname;
        }

        private ErsWhSupplier GetSupplier(IOrderListPDFMappable objMappable, string supplier_code)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhSupplierRepository();
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhSupplierCriteria();
            criteria.supplier_code = supplier_code;
            criteria.active = EnumActive.Active;
            var listResult = repository.Find(criteria);
            if (listResult.Count == 0)
            {
                return null;
            }

            return listResult.First();
        }

        protected void MapPdf(IOrderListPDFMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var pdfResult = new ViewAsPdf("order_list_pdf", objMappable)
            {
                WkhtmltopdfPath = ErsCommonContext.MapPath("~/../packages/Rotativa.1.6.1/content/Rotativa"),
                UserName = setup.BasicAuthUserName,
                Password = setup.BasicAuthPassword
            };

            try
            {
                objMappable.pdf = pdfResult.BuildPdf(objMappable.controller.ControllerContext);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    throw new Exception("Pdf builder could not be able to get authentication possibly. Please check BASIC authentication.", ex);
                }

                throw;
            }
        }
    }
}