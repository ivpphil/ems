﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Warehouse.Mappables;
using ersAdmin.Models.warehouse;
using jp.co.ivp.ers.warehouse;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Warehouse.Mappers
{
    public class OrderListMapper
        : IMapper<IOrderListMappable>
    {
        public void Map(IOrderListMappable objMappable)
        {
            objMappable.orderRegistRecords = this.GetListOrder(objMappable);
        }

        private IEnumerable<OrderRegistRecord> GetListOrder(IOrderListMappable objMappable)
        {
            var criteria = this.GetCriteria(objMappable);

            var spec = ErsFactory.ersWarehouseFactory.GetSearchOrderNeededSpec();

            //件数
            objMappable.recordCount = spec.GetCountData(criteria);

            criteria.SetOrderBySupplierCode();
            criteria.SetOrderBySScode();
            criteria.SetOrderByWhOrderType();
            criteria.SetOrderByCostPrice();

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            }

            var listOrder = spec.GetSearchData(criteria); 

            var listOrderRegistRecords = new List<OrderRegistRecord>();
            foreach (var record in listOrder)
            {
                var orderRegistRecord = new OrderRegistRecord();
                orderRegistRecord.OverwriteWithParameter(record);

                orderRegistRecord.base_amount = orderRegistRecord.amount ?? 0;

                //発注時状態変更時対応（在庫再割当て）
                this.ReallotWhStock(orderRegistRecord);

                //在庫品の場合は、WEB在庫更新をDEFAULTでチェック
                if (orderRegistRecord.wh_order_type == EnumWhOrderType.Stock)
                {
                    orderRegistRecord.up_stock =EnumWhUpStock.Up;
                }

                if (orderRegistRecord.stock_alert_amount < 0)
                {
                    orderRegistRecord.stock_alert_amount = null;
                }

                //発注閾値以下の場合は、発注をDEFAULTでチェック
                if ((orderRegistRecord.wh_order_type == EnumWhOrderType.Order && orderRegistRecord.amount > 0) || (orderRegistRecord.amount >= orderRegistRecord.stock_alert_amount))
                {
                    orderRegistRecord.check_order = EnumOnOff.On;
                }

                listOrderRegistRecords.Add(orderRegistRecord);
            }
            return listOrderRegistRecords;
        }

        Dictionary<string, int> listWhStock = new Dictionary<string, int>();
        private void ReallotWhStock(OrderRegistRecord orderRegistRecord)
        {
            if (!listWhStock.ContainsKey(orderRegistRecord.scode))
            {
                if (orderRegistRecord.amount <= orderRegistRecord.wh_stock)
                {
                    listWhStock[orderRegistRecord.scode] = orderRegistRecord.wh_stock.Value - orderRegistRecord.amount.Value;
                    orderRegistRecord.amount = null;
                }
                else
                {
                    listWhStock[orderRegistRecord.scode] = 0;
                    orderRegistRecord.amount -= orderRegistRecord.wh_stock;
                }
            }
            else
            {
                if (orderRegistRecord.amount <= listWhStock[orderRegistRecord.scode])
                {
                    listWhStock[orderRegistRecord.scode] = listWhStock[orderRegistRecord.scode] - orderRegistRecord.amount.Value;
                    orderRegistRecord.amount = null;
                }
                else
                {
                    orderRegistRecord.amount -= listWhStock[orderRegistRecord.scode];
                    listWhStock[orderRegistRecord.scode] = 0;
                }
            }
        }

        private ErsWhOrderCriteria GetCriteria(IOrderListMappable objMappable)
        {
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhOrderCriteria();
            criteria.intime_less_than = objMappable.s_intime_less_than;
            if (objMappable.s_supplier_code.HasValue())
            {
                criteria.supplier_code_ambi = objMappable.s_supplier_code;
            }

            if (objMappable.s_sname.HasValue())
            {
                criteria.sname_ambiguous = objMappable.s_sname;
            }

            if (objMappable.s_scode.HasValue())
            {
                criteria.s_scode = objMappable.s_scode;
            }

            if (objMappable.s_wh_order_type.HasValue)
            {
                criteria.wh_order_type = objMappable.s_wh_order_type;
            }

            return criteria;
        }
    }
}