﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.warehouse;

namespace ersAdmin.Domain.Warehouse.Mappers
{
    public class RoleModifyMapper : IMapper<ISupplierModifyMappable>
    {
        public void Map(ISupplierModifyMappable objMappable)
        {
            LoadObject(objMappable);
        }

        public void LoadObject(ISupplierModifyMappable objMappable)
        {
            this.SetObjectWithSupplierCode(objMappable);
        }

        public void SetObjectWithSupplierCode(ISupplierModifyMappable objMappable)
        {
            var supplier = (ErsWhSupplier)ErsFactory.ersWarehouseFactory.GetErsWhSupplierWithId(objMappable.id);

            if (supplier == null)
                throw new ErsException("30300");

            objMappable.OverwriteWithParameter(supplier.GetPropertiesAsDictionary());
        }
    }
}