﻿using System;
using ersAdmin.Domain.Warehouse.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using ersAdmin.Models.warehouse.csv;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Warehouse.Mappers
{
    public class StorageDownloadMapper : IMapper<IStorageDownloadMappable>
    {
        
        public void Map(IStorageDownloadMappable objMappable)
        {
            this.SetDefaultData(objMappable);

            if (objMappable.CreateCsvFile)
                this.DoCreateCsvFile(objMappable);
        }

        protected void SetDefaultData(IStorageDownloadMappable objMappable)
        {
            if (!objMappable.s_date_from.HasValue)
                objMappable.s_date_from = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/01"));

            if (!objMappable.s_date_to.HasValue)
                objMappable.s_date_to = Convert.ToDateTime(DateTime.Now.AddMonths(1).ToString("yyyy/MM/01")).AddDays(-1);
        }

        protected void DoCreateCsvFile(IStorageDownloadMappable objMappable)
        {
            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";
            
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhOrderCriteria();
            criteria.SetBetweenScheduleDate(objMappable.s_date_from.Value, objMappable.s_date_to.Value);
            criteria.amount_greater_than = 0;
            criteria.active = EnumActive.Active;

            if (ErsFactory.ersWarehouseFactory.GetStorageDownloadListSpec().GetCountData(criteria) == 0)
                objMappable.hasRecord = false;
            else
            {
                var storageList = ErsFactory.ersWarehouseFactory.GetStorageDownloadListSpec().GetSearchData(criteria);
                objMappable.hasRecord = true;
                using (var writer = objMappable.csvCreater.GetWriter(filename))
                {
                    objMappable.csvCreater.WriteCsvHeader<storage_download_csv>(writer);
                    foreach (var record in storageList)
                    {
                        var csvModel = new storage_download_csv();
                        csvModel.OverwriteWithParameter(record);
                        objMappable.csvCreater.WriteBody(csvModel, writer);
                    }
                }
            }
        }

    }
}