﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models;
using ersAdmin.Models.cms;

namespace ersAdmin.Domain.Cms.Mappables
{
    public interface IDownloadTableMappable:IMappable
    {
        IList<upload_file_record> upload_file_record { get; set; }

        IList<upload_file_record> upload_file_err_record { get; set; }
    }
}