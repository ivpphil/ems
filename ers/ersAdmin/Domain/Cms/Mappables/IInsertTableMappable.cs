﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.cms;

namespace ersAdmin.Domain.Cms.Mappables
{
    public interface IInsertTableMappable
        : IMappable
    {
        IList<insert_file_record> insert_file_record { set; }

        IList<insert_file_record> insert_file_err_record { set; }
    }
}