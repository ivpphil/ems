﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.cms;

namespace ersAdmin.Domain.Cms.Mappables
{
    public interface IDeleteTableMappable
        : IMappable
    {
        IList<delete_file_record> delete_file_record { set; }

        IList<delete_file_record> delete_file_err_record { set; }
    }
}