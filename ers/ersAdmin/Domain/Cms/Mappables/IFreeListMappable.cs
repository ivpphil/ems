﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.Cms.Mappables
{
    public interface IFreeListMappable
        : IMappable
    {
        ErsPagerModel pager { get; set; }

        long recordCount { get; set; }

        string contents_code { get; set; }

        string contents_name { get; set; }

        string contents_name_admin { get; set; }

        int? site_id { get; set; }

        DateTime? posted_date_from { get; set; }

        DateTime? posted_date_to { get; set; }

        string keyword { get; set; }

        bool containsActive { get; set; }

        bool containsBefore { get; set; }

        bool containsAfter { get; set; }

        bool containsNonActive { get; set; }

        List<Dictionary<string, object>> itemList { get; set; }
    }
}