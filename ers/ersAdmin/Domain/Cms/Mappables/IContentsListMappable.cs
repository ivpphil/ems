﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.Cms.Mappables
{
    public interface IContentsListMappable
        : IMappable
    {
        ErsPagerModel pager { get; set; }

        long recordCount { get; set; }

        List<Dictionary<string, object>> list { get; set; }
    }
}