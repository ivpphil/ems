﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Cms.Mappables
{
    public interface IContentsModifyMappable
        : IMappable
    {
        int? id { get; set; }
    }
}