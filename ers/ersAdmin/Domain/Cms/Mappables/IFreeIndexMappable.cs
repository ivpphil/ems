﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.contents;

namespace ersAdmin.Domain.Cms.Mappables
{
    public interface IFreeIndexMappable
        : IMappable
    {
        IList<ErsCmsContents> free_list { get; set; }
    }
}