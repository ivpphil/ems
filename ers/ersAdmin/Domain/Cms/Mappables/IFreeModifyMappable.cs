﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.cms;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Cms.Mappables
{
    public interface IFreeModifyMappable
        : IMappable
    {
        bool IsInitialize { get; set; }

        string article_code { get; set; }

        DateTime? posted_date { get; set; }

        DateTime? period_from { get; set; }

        DateTime? period_to { get; set; }

        string template_code { get; set; }

        IList<free_img_group_record> free_img_group_records { get; set; }

        int? create_user_id { get; set; }

        int? upd_user_id { get; set; }

        string create_name { get; set; }

        string update_user { get; set; }

        string contents_code { get; set; }

        string contents_name { get; set; }

        string contents_name_admin { get; set; }

        string title_name { get; set; }

        string sub_title_name { get; set; }

        string add_body_name { get; set; }

        string code_name { get; set; }

        string period_name { get; set; }

        string body_name { get; set; }

        string template_file_path { get; set; }

        EnumCmsFieldType? title_use_flg { get; set; }

        EnumCmsFieldType? sub_title_use_flg { get; set; }

        EnumCmsFieldType? body_use_flg { get; set; }

        EnumCmsFieldType? add_body_use_flg { get; set; }

        EnumCmsFieldType? code_use_flg { get; set; }

        IList<free_link_record> free_link_records { get; set; }

        IList<free_file_record> free_file_records { get; set; }
    }
}