﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Cms.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.contents;

namespace ersAdmin.Domain.Cms.Mappers
{
    public class FreeTemplateMapper
        : IMapper<IFreeTemplateMappable>
    {
        public void Map(IFreeTemplateMappable objMappable)
        {
            this.CheckTemplate(objMappable);
        }

        public virtual void CheckTemplate(IFreeTemplateMappable objMappable)
        {
            var ContentsRepository = ErsFactory.ersContentsFactory.GetErsCmsContentsRepository();
            var ContentsCriteria = ErsFactory.ersContentsFactory.GetErsCmsContentsCriteria();
            ContentsCriteria.contents_code = objMappable.contents_code;
            var list = ContentsRepository.Find(ContentsCriteria);
            if (list.Count == 0)
            {
                return;
            }

            var available_template = list[0].available_template;

            var TemplateRepository = ErsFactory.ersContentsFactory.GetErsCmsTemplateRepository();
            var TemplateCriteria = ErsFactory.ersContentsFactory.GetErsCmsTemplateCriteria();
            TemplateCriteria.template_code_in = available_template;
            TemplateCriteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
            TemplateCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var retList = TemplateRepository.Find(TemplateCriteria);

            objMappable.template = new List<ErsCmsTemplate>();
            for (int i = 0; i < available_template.Length; i++)
            {
                objMappable.template.Add(retList[i]);
            }
        }
    }
}