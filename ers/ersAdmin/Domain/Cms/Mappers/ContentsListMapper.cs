﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Cms.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Cms.Mappers
{
    public class ContentsListMapper
        : IMapper<IContentsListMappable>
    {
        public void Map(IContentsListMappable objMappable)
        {
            this.MapSearchResult(objMappable);
        }

        private void MapSearchResult(IContentsListMappable objMappable)
        {
            var repository = ErsFactory.ersContentsFactory.GetErsCmsContentsRepository();
            var criteria = ErsFactory.ersContentsFactory.GetErsCmsContentsCriteria();

            var recordCount = repository.GetRecordCount(criteria);
            objMappable.recordCount = recordCount;

            objMappable.pager.SetLimitAndOffsetToCriteria(criteria, recordCount);
            criteria.SetOrderByContentsCode(Criteria.OrderBy.ORDER_BY_ASC);

            var commonNameService = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService();
            IList<ErsCmsContents> list = repository.Find(criteria);

            var ersList = new List<Dictionary<string, object>>();
            foreach (var item in list)
            {
                var dictionary = item.GetPropertiesAsDictionary();

                dictionary["w_active"] = commonNameService.GetStringFromId(EnumCommonNameType.Active, EnumCommonNameColumnName.namename, (int?)item.active);
                
                ersList.Add(dictionary);
            }

            objMappable.list = ersList;
        }
    }
}