﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Cms.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Cms.Mappers
{
    public class ContentsModifyMapper
        : IMapper<IContentsModifyMappable>
    {
        public void Map(IContentsModifyMappable objMappable)
        {
            this.GetContentsWithID(objMappable);
        }

        internal void GetContentsWithID(IContentsModifyMappable objMappable)
        {
            var contents = ErsFactory.ersContentsFactory.GetErsCmsContentsWithId(objMappable.id);
            if (contents == null)
            {
                throw new ErsException("63072");
            }

            objMappable.OverwriteWithParameter(contents.GetPropertiesAsDictionary());
        }
    }
}