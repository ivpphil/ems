﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Cms.Mappables;

namespace ersAdmin.Domain.Cms.Mappers
{
    public class FreeRegistMapper
        : FreeModifyMapper, IMapper<IFreeRegistMappable>
    {
        public void Map(IFreeRegistMappable objMappable)
        {
            if (objMappable.IsInitialize)
            {
                this.InitImage(objMappable);
            }

            this.CheckDisplay(objMappable);
        }
    }
}