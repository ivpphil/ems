﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Cms.Mappables;

namespace ersAdmin.Domain.Cms.Mappers
{
    public class FreeIndexMapper
        : IMapper<IFreeIndexMappable>
    {
        public void Map(IFreeIndexMappable objMappable)
        {
            this.MapFreeList(objMappable);
        }

        internal void MapFreeList(IFreeIndexMappable objMappable)
        {
            var repository = ErsFactory.ersContentsFactory.GetErsCmsContentsRepository();
            var criteria = ErsFactory.ersContentsFactory.GetErsCmsContentsCriteria();

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetActiveOnly();
            if(!setup.Multiple_sites)
            {
                criteria.site_id = (int)EnumSiteId.COMMON_SITE_ID; 
            }
            objMappable.free_list = repository.Find(criteria);
        }
    }
}