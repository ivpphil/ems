﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Cms.Mappables;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using System.IO;
using ersAdmin.Models.cms;

namespace ersAdmin.Domain.Cms.Mappers
{
    public class DeleteTableMapper
        : IMapper<IDeleteTableMappable>
    {
        protected Setup setup;

        public void Map(IDeleteTableMappable objMappable)
        {
            setup = ErsFactory.ersUtilityFactory.getSetup();

            this.Init(objMappable);
        }

        protected virtual void Init(IDeleteTableMappable objMappable)
        {
            this.GetDeleteTableFileList(objMappable);

            this.GetDeleteTableErrorFileList(objMappable);
        }


        protected virtual void GetDeleteTableFileList(IDeleteTableMappable objMappable)
        {
            Directory.CreateDirectory(setup.deleteFilePath);

            var files = Directory.EnumerateFiles(setup.deleteFilePath);

            IList<delete_file_record> list = new List<delete_file_record>();

            foreach (var file in files)
            {
                var record = new delete_file_record();
                record.file_name = Path.GetFileName(file);
                record.created_date = File.GetCreationTime(file);

                list.Add(record);
            }

            objMappable.delete_file_record = list;
        }

        protected virtual void GetDeleteTableErrorFileList(IDeleteTableMappable objMappable)
        {
            Directory.CreateDirectory(setup.deleteFileErrorPath);

            var files = Directory.EnumerateFiles(setup.deleteFileErrorPath);

            IList<delete_file_record> list = new List<delete_file_record>();

            foreach (var file in files)
            {
                var record = new delete_file_record();
                record.file_name = Path.GetFileName(file);
                record.created_date = File.GetCreationTime(file);

                list.Add(record);
            }

            objMappable.delete_file_err_record = list;
        }
    }
}