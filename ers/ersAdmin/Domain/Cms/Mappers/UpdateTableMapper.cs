﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Cms.Mappables;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using System.IO;
using ersAdmin.Models.cms;

namespace ersAdmin.Domain.Cms.Mappers
{
    public class UpdateTableMapper:IMapper<IUpdateTableMappable>
    {
        private Setup setup;

        public void Map(IUpdateTableMappable objMappable)
        {
            setup = ErsFactory.ersUtilityFactory.getSetup();
            this.Init(objMappable);
        }

        protected void Init(IUpdateTableMappable objMappable)
        {
            this.GetUploadedFileList(objMappable);
            this.GetUploadedErrorFileList(objMappable);
        }

        protected void GetUploadedFileList(IUpdateTableMappable objMappable)
        {
            Directory.CreateDirectory(setup.uploadedFilePath);
            var files = Directory.GetFiles(setup.uploadedFilePath);

            IList<upload_file_record> uploadedFile = new List<upload_file_record>();

            foreach (var file in files)
            {
                var record = new upload_file_record();
                record.file_name = Path.GetFileName(file);
                record.created_date = File.GetCreationTime(file);

                uploadedFile.Add(record);
            }

            objMappable.upload_file_record = uploadedFile;
        }

        protected void GetUploadedErrorFileList(IUpdateTableMappable objMappable)
        {
            Directory.CreateDirectory(setup.uploadedFileErrorPath);
            var files = Directory.GetFiles(setup.uploadedFileErrorPath);

            IList<upload_file_record> uploadedFileError = new List<upload_file_record>();

            foreach (var file in files)
            {
                var record = new upload_file_record();
                record.file_name = Path.GetFileName(file);
                record.created_date = File.GetCreationTime(file);

                uploadedFileError.Add(record);
            }

            objMappable.upload_file_err_record = uploadedFileError;
        }

    }
}