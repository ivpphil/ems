﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Cms.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.cms;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Cms.Mappers
{
    public class FreeModifyMapper
        : IMapper<IFreeModifyMappable>
    {
        public void Map(IFreeModifyMappable objMappable)
        {
            if (objMappable.IsInitialize)
            {
                this.LoadInitialValue(objMappable);
                this.InitImage(objMappable);
                this.LoadImage(objMappable);
            }

            this.LoadAdministrator(objMappable);
            this.CheckDisplay(objMappable);
        }

        internal void LoadInitialValue(IFreeModifyMappable objMappable)
        {
            var repository = ErsFactory.ersContentsFactory.GetErsNewsArticleRepository();
            var criteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
            criteria.article_code = objMappable.article_code;
            IList<ErsNewsArticle> retList = repository.Find(criteria);

            if (retList.Count == 0)
            {
                throw new ErsException("63072");
            }

            var objArticle = retList.First();

            objMappable.OverwriteWithParameter(objArticle.GetPropertiesAsDictionary());
        }

        internal void InitImage(IFreeModifyMappable objMappable)
        {
            var TemplateRepository = ErsFactory.ersContentsFactory.GetErsCmsTemplateRepository();
            var TemplateCriteria = ErsFactory.ersContentsFactory.GetErsCmsTemplateCriteria();
            TemplateCriteria.template_code = objMappable.template_code;
            IList<ErsCmsTemplate> tlist = TemplateRepository.Find(TemplateCriteria);

            if (tlist.Count > 0)
            {
                this.LoadFreeImgGroupRecords(objMappable, tlist.First());
            }

            var ContentsRepository = ErsFactory.ersContentsFactory.GetErsCmsContentsRepository();
            var ContentsCriteria = ErsFactory.ersContentsFactory.GetErsCmsContentsCriteria();
            ContentsCriteria.contents_code = objMappable.contents_code;
            IList<ErsCmsContents> clist = ContentsRepository.Find(ContentsCriteria);
            if (clist.Count > 0)
            {
                this.LoadFreeFileRecords(objMappable, clist.First());
            }
        }

        private void LoadFreeImgGroupRecords(IFreeModifyMappable objMappable, ErsCmsTemplate template)
        {
            var img_item_name = template.img_item_name;
            var img_width = template.img_width;
            var img_group_count = template.img_group_count;

            if (img_width != null)
            {
                var img_count = 0;
                var img_group_records = new List<free_img_group_record>();
                for (var count = 0; count < img_item_name.Length; count++)
                {
                    var group_record = new free_img_group_record();
                    group_record.img_item_name = img_item_name[count];

                    group_record.free_img_records = new List<free_img_record>();
                    for (var item_count = 0; item_count < img_group_count[count]; item_count++)
                    {
                        var img_record = new free_img_record();
                        img_record.img_count = ++img_count;
                        img_record.img_width = img_width[item_count];
                        group_record.free_img_records.Add(img_record);
                    }
                    img_group_records.Add(group_record);
                }

                objMappable.free_img_group_records = img_group_records;
            }
        }

        private void LoadFreeFileRecords(IFreeModifyMappable objMappable, ErsCmsContents contents)
        {
            var link_count = contents.link_count;
            var file_count = contents.file_count;

            objMappable.free_link_records = new List<free_link_record>();
            for (var i = 0; i < link_count; i++)
            {
                var record = new free_link_record();
                record.number = i + 1;
                objMappable.free_link_records.Add(record);
            }

            objMappable.free_file_records = new List<free_file_record>();
            for (var i = 0; i < file_count; i++)
            {
                var record = new free_file_record();
                record.number = i + 1;
                objMappable.free_file_records.Add(record);
            }
        }

        internal virtual void LoadImage(IFreeModifyMappable objMappable)
        {
            var repository = ErsFactory.ersContentsFactory.GetErsNewsArticleRepository();
            var criteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
            criteria.article_code = objMappable.article_code;
            IList<ErsNewsArticle> retList = repository.Find(criteria);

            var objArticle = retList.First();

            if (objMappable.free_img_group_records != null)
            {
                foreach (var group_record in objMappable.free_img_group_records)
                {
                    foreach (var record in group_record.free_img_records)
                    {
                        if (objArticle.img_file_name.Length >= record.img_count.Value)
                        {
                            record.img_file_name = objArticle.img_file_name[record.img_count.Value - 1];
                        }
                    }
                }
            }

            var objContents = ErsFactory.ersContentsFactory.GetErsCmsContentsWithContentsCode(objMappable.contents_code);
            for (var i = 0; i < objContents.link_count; i++)
            {
                var record = objMappable.free_link_records[i];
                record.link_string = ErsExpressionAccessor<ErsNewsArticle, string>.GetPropertyValue(objArticle, "link_string_" + (i + 1));
                record.link_url = ErsExpressionAccessor<ErsNewsArticle, string>.GetPropertyValue(objArticle, "link_url_" + (i + 1));
            }

            for (var i = 0; i < objContents.file_count; i++)
            {
                var record = objMappable.free_file_records[i];
                record.file_real_name = ErsExpressionAccessor<ErsNewsArticle, string>.GetPropertyValue(objArticle, "file_real_name_" + (i + 1));
                record.file_string = ErsExpressionAccessor<ErsNewsArticle, string>.GetPropertyValue(objArticle, "file_string_" + (i + 1));
                record.old_file_real_name = record.file_real_name;
            }
        }

        internal virtual void LoadAdministrator(IFreeModifyMappable objMappable)
        {
            var creator = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithId(objMappable.create_user_id);
            var updater = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithId(objMappable.upd_user_id);
            if (creator != null)
            {
                objMappable.create_name = creator.user_name;
            }
            if (updater != null)
            {
                objMappable.update_user = updater.user_name;
            }
        }

        internal virtual void CheckDisplay(IFreeModifyMappable objMappable)
        {
            var ContentsRepository = ErsFactory.ersContentsFactory.GetErsCmsContentsRepository();
            var ContentsCriteria = ErsFactory.ersContentsFactory.GetErsCmsContentsCriteria();
            ContentsCriteria.contents_code = objMappable.contents_code;
            IList<ErsCmsContents> clist = ContentsRepository.Find(ContentsCriteria);
            if (clist.Count > 0)
            {
                objMappable.contents_name = clist[0].contents_name;
                objMappable.contents_name_admin = clist[0].contents_name_admin;
                objMappable.title_name = clist[0].title_name;
                objMappable.sub_title_name = clist[0].sub_title_name;
                objMappable.body_name = clist[0].body_name;
                objMappable.add_body_name = clist[0].add_body_name;
                objMappable.code_name = clist[0].code_name;
                objMappable.period_name = clist[0].period_name;
            }

            var TemplateRepository = ErsFactory.ersContentsFactory.GetErsCmsTemplateRepository();
            var TemplateCriteria = ErsFactory.ersContentsFactory.GetErsCmsTemplateCriteria();
            TemplateCriteria.template_code = objMappable.template_code;
            IList<ErsCmsTemplate> tlist = TemplateRepository.Find(TemplateCriteria);
            if (tlist.Count > 0)
            {
                objMappable.template_file_path = tlist[0].template_file_path;
                objMappable.title_use_flg = tlist[0].title_use_flg;
                objMappable.sub_title_use_flg = tlist[0].sub_title_use_flg;
                objMappable.body_use_flg = tlist[0].body_use_flg;
                objMappable.add_body_use_flg = tlist[0].add_body_use_flg;
                objMappable.code_use_flg = tlist[0].code_use_flg;
            }
        }
    }
}