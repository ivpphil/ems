﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Cms.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using System.IO;
using ersAdmin.Models;
using ersAdmin.Models.cms;

namespace ersAdmin.Domain.Cms.Mappers
{
    public class DownloadTableMapper:IMapper<IDownloadTableMappable>
    {
        private Setup setup;

        public void Map(IDownloadTableMappable objMappable)
        {
            setup = ErsFactory.ersUtilityFactory.getSetup();
            this.GetUploadedFileList(objMappable);
            this.GetUploadedErrorFileList(objMappable);
        }

        protected void GetUploadedFileList(IDownloadTableMappable objMappable)
        {
            Directory.CreateDirectory(setup.downloadFilePath);
            var files = Directory.GetFiles(setup.downloadFilePath);

            IList<upload_file_record> uploadedFile = new List<upload_file_record>();

            foreach (var file in files)
            {
                var record = new upload_file_record();
                record.file_name = Path.GetFileName(file);
                record.created_date = File.GetCreationTime(file);

                uploadedFile.Add(record);
            }

            objMappable.upload_file_record = uploadedFile;
        }

        protected void GetUploadedErrorFileList(IDownloadTableMappable objMappable)
        {
            Directory.CreateDirectory(setup.downloadFileResultPath);
            var files = Directory.GetFiles(setup.downloadFileResultPath);

            IList<upload_file_record> uploadedFileError = new List<upload_file_record>();

            foreach (var file in files)
            {
                var record = new upload_file_record();
                record.file_name = Path.GetFileName(file);
                record.created_date = File.GetCreationTime(file);

                uploadedFileError.Add(record);
            }

            objMappable.upload_file_err_record = uploadedFileError;
        }
    }
}