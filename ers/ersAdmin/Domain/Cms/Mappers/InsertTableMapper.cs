﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Cms.Mappables;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using System.IO;
using ersAdmin.Models.cms;

namespace ersAdmin.Domain.Cms.Mappers
{
    public class InsertTableMapper
        : IMapper<IInsertTableMappable>
    {
        protected Setup setup;

        public void Map(IInsertTableMappable objMappable)
        {
            setup = ErsFactory.ersUtilityFactory.getSetup();

            this.Init(objMappable);
        }

        protected virtual void Init(IInsertTableMappable objMappable)
        {
            this.GetInsertTableFileList(objMappable);

            this.GetInsertTableErrorFileList(objMappable);
        }


        protected virtual void GetInsertTableFileList(IInsertTableMappable objMappable)
        {
            Directory.CreateDirectory(setup.insertTableFilePath);

            var files = Directory.EnumerateFiles(setup.insertTableFilePath);

            IList<insert_file_record> list = new List<insert_file_record>();

            foreach (var file in files)
            {
                var record = new insert_file_record();
                record.file_name = Path.GetFileName(file);
                record.created_date = File.GetCreationTime(file);

                list.Add(record);
            }

            objMappable.insert_file_record = list;
        }

        protected virtual void GetInsertTableErrorFileList(IInsertTableMappable objMappable)
        {
            Directory.CreateDirectory(setup.insertTableFileErrorPath);

            var files = Directory.EnumerateFiles(setup.insertTableFileErrorPath);

            IList<insert_file_record> list = new List<insert_file_record>();

            foreach (var file in files)
            {
                var record = new insert_file_record();
                record.file_name = Path.GetFileName(file);
                record.created_date = File.GetCreationTime(file);

                list.Add(record);
            }

            objMappable.insert_file_err_record = list;
        }
    }
}