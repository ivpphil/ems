﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Cms.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.contents;

namespace ersAdmin.Domain.Cms.Mappers
{
    public class FreeListMapper
        : IMapper<IFreeListMappable>
    {
        public void Map(IFreeListMappable objMappable)
        {
            this.SearchList(objMappable);
        }

        private void SearchList(IFreeListMappable objMappable)
        {
            var contentsRepository = ErsFactory.ersContentsFactory.GetErsCmsContentsRepository();
            var contentsCriteria = ErsFactory.ersContentsFactory.GetErsCmsContentsCriteria();
            contentsCriteria.contents_code = objMappable.contents_code;
            var contentsList = contentsRepository.Find(contentsCriteria);

            if (contentsList.Count == 0)
            {
                return;
            }

            var objContents = contentsList.First();
            objMappable.contents_name = objContents.contents_name;
            objMappable.contents_name_admin = objContents.contents_name_admin;
            objMappable.site_id = objContents.site_id;

            var repository = ErsFactory.ersContentsFactory.GetErsNewsArticleRepository();
            var criteria = this.GetCriteria(objMappable);

            var recordCount = repository.GetRecordCount(criteria);
            objMappable.recordCount = recordCount;

            objMappable.pager.SetLimitAndOffsetToCriteria(criteria, recordCount);
            criteria.SetOrderByPosted_date(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.SetGroupById();

            var list = repository.Find(criteria);
            var ersList = new List<Dictionary<string, object>>();
            foreach (var item in list)
            {
                var dictionary = item.GetPropertiesAsDictionary();
                if (item.active == EnumActive.NonActive)
                {
                    dictionary["active"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ArticleState, EnumCommonNameColumnName.namename, (int)EnumArticleState.NonActive);
                }
                else if (item.period_from > DateTime.Now)
                {
                    dictionary["active"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ArticleState, EnumCommonNameColumnName.namename, (int)EnumArticleState.ActiveWait);
                }
                else if (item.period_to < DateTime.Now)
                {
                    dictionary["active"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ArticleState, EnumCommonNameColumnName.namename, (int)EnumArticleState.OutsidePeriod);
                }
                else
                {
                    dictionary["active"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ArticleState, EnumCommonNameColumnName.namename, (int)EnumArticleState.Active);
                }

                var objAdmin = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithId(item.upd_user_id);
                if (objAdmin != null)
                {
                    dictionary["user_name"] = objAdmin.user_name;
                }

                

                ersList.Add(dictionary);
            }
            objMappable.itemList = ersList;
        }

        public virtual ErsNewsArticleCriteria GetCriteria(IFreeListMappable objMappable)
        {
            ErsNewsArticleCriteria criteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();

            criteria.contents_code = objMappable.contents_code;

            if (!string.IsNullOrEmpty(objMappable.keyword))
            {
                criteria.keywords = objMappable.keyword;
            }

            if (objMappable.posted_date_from.HasValue)
            {
                criteria.posted_date_than_eq = objMappable.posted_date_from;
            }

            if (objMappable.posted_date_to.HasValue)
            {
                criteria.posted_date_less_eq = objMappable.posted_date_to;
            }

            var listCriteria = new List<Criteria>();

            if (objMappable.containsActive)
            {
                var orCriteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
                orCriteria.active = EnumActive.Active;
                orCriteria.period_from_less_eq = DateTime.Now;
                orCriteria.period_to_than_eq = DateTime.Now;
                listCriteria.Add(orCriteria);
            }

            if (objMappable.containsBefore)
            {
                var orCriteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
                orCriteria.active = EnumActive.Active;
                orCriteria.period_from_than = DateTime.Now;
                listCriteria.Add(orCriteria);
            }

            if (objMappable.containsAfter)
            {
                var orCriteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
                orCriteria.active = EnumActive.Active;
                orCriteria.period_to_less = DateTime.Now;
                listCriteria.Add(orCriteria);
            }

            if (objMappable.containsNonActive)
            {
                var orCriteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
                orCriteria.active = EnumActive.NonActive;
                listCriteria.Add(orCriteria);
            }

            if (listCriteria.Count > 0)
            {
                criteria.Add(Criteria.JoinWithOR(listCriteria));
            }

            return criteria;
        }
    }
}