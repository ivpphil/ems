﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.cms;

namespace ersAdmin.Domain.Cms.Commands
{
    public interface IInsertTableCommand
        : ICommand
    {
        HttpPostedFileBase file_name { get; set; }

        IList<upload_file_record> upload_file_record { get; set; }

        bool isSave { get; set; }

        bool isCompleted { set; }
    }
}