﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Cms.Commands
{
    public interface IFreeListCommand
        : ICommand
    {
        DateTime? posted_date_from { get; set; }
        DateTime? posted_date_to { get; set; }
    }
}