﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.cms;

namespace ersAdmin.Domain.Cms.Commands
{
    public interface IFreeImgGroupRecordCommand
        : ICommand
    {
        IList<free_img_record> free_img_records { get; set; }
    }
}