﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Cms.Commands
{
    public interface IContentsRegistCommand
        : ICommand
    {
        string contents_code { get; set; }

        string[] available_template { get; set; }
    }
}