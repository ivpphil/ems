﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.cms;

namespace ersAdmin.Domain.Cms.Commands
{
    public interface IDeleteTableCommand
        : ICommand
    {
        HttpPostedFileBase file_name { get; set; }

        IList<delete_file_record> delete_file_record { get; set; }

        bool isSave { get; set; }

        bool isCompleted { set; }
    }
}