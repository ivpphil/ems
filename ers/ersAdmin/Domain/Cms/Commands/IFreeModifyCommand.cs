﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.cms;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Cms.Commands
{
    public interface IFreeModifyCommand
        : ICommand
    {
        bool delete { get; set; }

        string article_code { get; set; }

        string contents_code { get; set; }

        string template_code { get; set; }

        DateTime? period_from { get; set; }

        DateTime? period_to { get; set; }

        DateTime? posted_date { get; set; }

        IList<free_img_group_record> free_img_group_records { get; set; }

        string user_cd { get; }

        bool IsConfirmation { get; set; }

        IList<free_link_record> free_link_records { get; set; }

        IList<free_file_record> free_file_records { get; set; }

        EnumActive? active { get; set; }
    }
}