﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Cms.Commands
{
    public interface IContentsModifyCommand
        : ICommand
    {
        int? id { get; set; }

        string contents_code { get; set; }

        string contents_name { get; set; }

        string code_name { get; set; }

        string title_name { get; set; }

        string sub_title_name { get; set; }

        string body_name { get; set; }

        string add_body_name { get; set; }

        string period_name { get; set; }

        int? link_count { get; set; }

        int? file_count { get; set; }

        string[] available_template { get; set; }

        EnumActive? active { get; set; }
    }
}