﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using System.IO;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class UpdateTableHandler : ICommandHandler<IUpdateTableCommand>
    {
        public ICommandResult Submit(IUpdateTableCommand command)
        {
            if ((command.isSave) && (command.IsValid))
                this.SaveFile(command);

            return new CommandResult(true);
        }

        internal void SaveFile(IUpdateTableCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var fileName = Path.GetFileName(command.file_name.FileName);

            var path = Path.Combine(setup.uploadedFilePath, fileName);

            command.file_name.SaveAs(path);

            command.isSave = false;
        }
    }
}