﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ContentsModifyHandler
        : ICommandHandler<IContentsModifyCommand>
    {
        public ICommandResult Submit(IContentsModifyCommand command)
        {
            this.UpdateContents(command);

            return new CommandResult(true);
        }

        internal void UpdateContents(IContentsModifyCommand command)
        {
            var repository = ErsFactory.ersContentsFactory.GetErsCmsContentsRepository();
            var criteria = ErsFactory.ersContentsFactory.GetErsCmsContentsCriteria();
            var newContents = ErsFactory.ersContentsFactory.GetErsCmsContentsWithId(command.id);

            newContents.contents_code = command.contents_code;
            newContents.contents_name = command.contents_name;
            newContents.code_name = command.code_name;
            newContents.title_name = command.title_name;
            newContents.sub_title_name = command.sub_title_name;
            newContents.body_name = command.body_name;
            newContents.add_body_name = command.add_body_name;
            newContents.period_name = command.period_name;
            newContents.link_count = command.link_count;
            newContents.file_count = command.file_count;
            newContents.active = command.active ?? EnumActive.NonActive;
            newContents.utime = DateTime.Now;
            newContents.available_template = command.available_template;

            var oldContents = ErsFactory.ersContentsFactory.GetErsCmsContentsWithId(command.id);

            repository.Update(oldContents, newContents);
        }
    }
}