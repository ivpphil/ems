﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ValidateFreeTemplate
        : IValidationHandler<IFreeTemplateCommand>
    {
        public IEnumerable<ValidationResult> Validate(IFreeTemplateCommand command)
        {
            yield return command.CheckRequired("contents_code");
        }
    }
}