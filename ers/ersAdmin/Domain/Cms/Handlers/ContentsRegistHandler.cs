﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.contents;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ContentsRegistHandler
        : ICommandHandler<IContentsRegistCommand>
    {
        public ICommandResult Submit(IContentsRegistCommand command)
        {
            this.InsertContents(command);

            return new CommandResult(true);
        }

        internal void InsertContents(IContentsRegistCommand command)
        {
            var repository = ErsFactory.ersContentsFactory.GetErsCmsContentsRepository();
            var newContents = ErsFactory.ersContentsFactory.GetErsCmsContentsWithModel(command);
            if (newContents.active == null)
            {
                newContents.active = EnumActive.NonActive;
            }
            newContents.available_template = command.available_template;
            newContents.intime = DateTime.Now;
            newContents.utime = newContents.intime;
            repository.Insert(newContents);
        }
    }
}