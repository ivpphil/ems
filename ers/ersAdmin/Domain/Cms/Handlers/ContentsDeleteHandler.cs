﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ContentsDeleteHandler
        : ICommandHandler<IContentsDeleteCommand>
    {
        public ICommandResult Submit(IContentsDeleteCommand command)
        {
            this.DeleteContents(command);

            return new CommandResult(true);
        }

        internal void DeleteContents(IContentsDeleteCommand command)
        {
            var repository = ErsFactory.ersContentsFactory.GetErsCmsContentsRepository();
            var criteria = ErsFactory.ersContentsFactory.GetErsCmsContentsCriteria();
            criteria.id = command.id;
            repository.Delete(criteria);
        }
    }
}