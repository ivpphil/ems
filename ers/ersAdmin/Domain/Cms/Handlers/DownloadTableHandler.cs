﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers;
using System.IO;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class DownloadTableHandler:ICommandHandler<IDownloadTableCommand>
    {
        public ICommandResult Submit(IDownloadTableCommand command)
        {
            if ((command.isSave) && (command.IsValid))
                this.SaveFile(command);

            return new CommandResult(true);
        }

        internal void SaveFile(IDownloadTableCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var fileName = Path.GetFileName(command.file_name.FileName);

            var path = Path.Combine(setup.downloadFilePath, fileName);

            command.file_name.SaveAs(path);

            command.isSave = false;
        }
    }
}