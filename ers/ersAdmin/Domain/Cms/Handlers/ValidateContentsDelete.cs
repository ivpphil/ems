﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ValidateContentsDelete
        : IValidationHandler<IContentsDeleteCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IContentsDeleteCommand command)
        {
            yield return command.CheckRequired("id");
        }
    }
}