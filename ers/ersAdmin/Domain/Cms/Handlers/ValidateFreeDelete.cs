﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ValidateFreeDelete
        : IValidationHandler<IFreeDeleteCommand>
    {
        public IEnumerable<ValidationResult> Validate(IFreeDeleteCommand command)
        {
            yield return command.CheckRequired("article_code");
        }
    }
}