﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers;
using System.IO;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class InsertTableHandler
        : ICommandHandler<IInsertTableCommand>
    {
        public ICommandResult Submit(IInsertTableCommand command)
        {
            if (command.isSave)
            {
                this.SaveFile(command);
            }

            return new CommandResult(true);
        }

        internal void SaveFile(IInsertTableCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var fileName = Path.GetFileName(command.file_name.FileName);

            var path = Path.Combine(setup.insertTableFilePath, fileName);

            command.file_name.SaveAs(path);
            command.isCompleted = true;
        }
    }
}