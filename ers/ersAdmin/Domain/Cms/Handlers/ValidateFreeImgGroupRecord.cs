﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ValidateFreeImgGroupRecord
        : IValidationHandler<IFreeImgGroupRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IFreeImgGroupRecordCommand command)
        {
            if (command.free_img_records != null)
            {
                foreach (var record in command.free_img_records)
                {
                    if (!record.IsValid)
                    {
                        foreach (var errorMessage in record.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "free_file_records" });
                        }
                    }
                }
            }
        }
    }
}