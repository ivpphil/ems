﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers;
using System.IO;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ValidateDeleteTable
        : IValidationHandler<IDeleteTableCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IDeleteTableCommand command)
        {
            if (command.isSave)
            {
                yield return command.CheckRequired("file_name");

                if (command.IsValidField("file_name"))
                {
                    ///check existing
                    var setup = ErsFactory.ersUtilityFactory.getSetup();
                    var path = Path.Combine(setup.deleteFilePath, command.file_name.FileName);
                    if (File.Exists(path))
                        yield return new ValidationResult(ErsResources.GetMessage("fileExist"));
                }
            }
        }
    }
}