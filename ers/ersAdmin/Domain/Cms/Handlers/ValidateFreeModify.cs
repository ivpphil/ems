﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using ersAdmin.Models.cms;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ValidateFreeModify
        : IValidationHandler<IFreeModifyCommand>
    {
        public IEnumerable<ValidationResult> Validate(IFreeModifyCommand command)
        {
            foreach (var result in this.ValidateModify(command))
            {
                yield return result;
            }

            foreach (var result in this.ValidateInputs(command))
            {
                yield return result;
            }
        }

        private IEnumerable<ValidationResult> ValidateModify(IFreeModifyCommand command)
        {
            yield return command.CheckRequired("article_code");

            if (command.delete)
                yield break;

            if (!command.IsValidField("article_code", "contents_code"))
            {
                yield break;
            }

            var objArticle = ErsFactory.ersContentsFactory.GetErsNewsArticleWithArticleCode(command.article_code);
            if (objArticle == null)
            {
                yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "article_code" });
                yield break;
            }

            command.template_code = objArticle.template_code;
        }

        protected IEnumerable<ValidationResult> ValidateInputs(IFreeModifyCommand command)
        {
            yield return command.CheckRequired("template_code");

            var objTemplate = ErsFactory.ersContentsFactory.GetErsCmsTemplateWithTemplateCode(command.template_code);
            if (objTemplate == null)
            {
                yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "template_code" });
                yield break;
            }

            yield return command.CheckRequired("posted_date");

            if (objTemplate.title_use_flg == EnumCmsFieldType.Required)
            {
                yield return command.CheckRequired("title");
            }
            if (objTemplate.sub_title_use_flg == EnumCmsFieldType.Required)
            {
                yield return command.CheckRequired("sub_title");
            }
            if (objTemplate.body_use_flg == EnumCmsFieldType.Required)
            {
                yield return command.CheckRequired("body");
            }
            if (objTemplate.add_body_use_flg == EnumCmsFieldType.Required)
            {
                yield return command.CheckRequired("add_body");
            }
            if (objTemplate.code_use_flg == EnumCmsFieldType.Required)
            {
                yield return command.CheckRequired("scode");
            }

            if (command.free_link_records != null)
            {
                foreach (var record in command.free_link_records)
                {
                    this.CheckListValues(record);

                    if (!record.IsValid)
                    {
                        foreach (var errorMessage in record.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "free_link_records" });
                        }
                    }
                }
            }

            if (command.free_file_records != null)
            {
                foreach (var record in command.free_file_records)
                {
                    this.CheckFileValues(record);

                    if (!record.IsValid)
                    {
                        foreach (var errorMessage in record.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "free_file_records" });
                        }
                    }
                }
            }

            if (command.free_img_group_records != null)
            {
                foreach (var record in command.free_img_group_records)
                {
                    record.AddInvalidField(command.controller.commandBus.Validate<IFreeImgGroupRecordCommand>(record));

                    if (!record.IsValid)
                    {
                        foreach (var errorMessage in record.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "free_img_group_records" });
                        }
                    }
                }
            }

            yield return command.CheckRequired("period_from");
            yield return command.CheckRequired("period_to");


            if (command.IsValidField("period_from", "period_to"))
            {
                if (command.period_from > command.period_to)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10045", ErsResources.GetFieldName("period_to"), ErsResources.GetFieldName("period_from")), new[] { "period_from", "period_to" });
                }
            }

            yield return command.CheckRequired("posted_date");
        }

        private void CheckFileValues(free_file_record free_file_record)
        {
            if (!free_file_record.disabled_file_uploader || (free_file_record.disabled_file_uploader && !free_file_record.old_file_real_name.HasValue()))
            {
                if (free_file_record.HasRecordNewsPdfDetail)
                {
                    if (!free_file_record.file_string.HasValue())
                    {
                        free_file_record.AddInvalidField(new ValidationResult(ErsResources.GetMessage("63076", new[] { ErsResources.GetFieldName("file_string"), ErsResources.GetFieldName("file_real_name") }), new[] { "file_string", "file_real_name" }));
                    }

                    if (!free_file_record.disabled_file_uploader)
                    {
                        if (free_file_record.news_pdf_detail.First().api_errorList != null)
                        {
                            foreach (var api_message in free_file_record.news_pdf_detail.First().api_errorList)
                                free_file_record.AddInvalidField(new ValidationResult(api_message));
                        }
                    }
                }

                if (free_file_record.file_string.HasValue())
                {
                    if (!free_file_record.HasRecordNewsPdfDetail)
                    {
                        free_file_record.AddInvalidField(new ValidationResult(ErsResources.GetMessage("63076", new[] { ErsResources.GetFieldName("file_real_name"), ErsResources.GetFieldName("file_string") }), new[] { "file_string", "file_real_name" }));
                    }
                }
            }

            if (free_file_record.disabled_file_uploader && free_file_record.old_file_real_name.HasValue())
            {
                if (!free_file_record.file_string.HasValue())
                {
                    free_file_record.AddInvalidField(new ValidationResult(ErsResources.GetMessage("63076", new[] { ErsResources.GetFieldName("file_string"), ErsResources.GetFieldName("file_real_name") }), new[] { "file_string", "file_real_name" }));
                }
            }
        }

        private void CheckListValues(free_link_record record)
        {
            if (string.IsNullOrEmpty(record.link_string) && !string.IsNullOrEmpty(record.link_url))
            {
                record.AddInvalidField(new ValidationResult(ErsResources.GetMessage("63076", new[] { ErsResources.GetFieldName("link_string"), ErsResources.GetFieldName("link_url") }), new[] { "link_string", "link_url" }));
            }
            if (string.IsNullOrEmpty(record.link_url) && !string.IsNullOrEmpty(record.link_string))
            {
                record.AddInvalidField(new ValidationResult(ErsResources.GetMessage("63076", new[] { ErsResources.GetFieldName("link_url"), ErsResources.GetFieldName("link_string") }), new[] { "link_string", "link_url" }));
            }
        }
    }
}