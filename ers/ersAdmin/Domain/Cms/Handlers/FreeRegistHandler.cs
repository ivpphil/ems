﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.contents;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class FreeRegistHandler
        : FreeModifyHandler, ICommandHandler<IFreeRegistCommand>
    {
        public ICommandResult Submit(IFreeRegistCommand command)
        {
            if (command.IsConfirmation)
            {
                this.SaveTmpFiles(command);
            }
            else
            {
                this.Insert(command);
            }

            return new CommandResult(true);
        }

        internal void Insert(IFreeRegistCommand command)
        {
            var repository = ErsFactory.ersContentsFactory.GetErsNewsArticleRepository();
            var newNewsArticle = ErsFactory.ersContentsFactory.GetErsNewsArticleWithModel(command);
            var criteria = ErsFactory.ersContentsFactory.GetErsNewsArticleCriteria();
            criteria.contents_code = newNewsArticle.contents_code;
            criteria.title = newNewsArticle.title;
            criteria.sub_title = newNewsArticle.sub_title;
            criteria.body = newNewsArticle.body;
            criteria.add_body = newNewsArticle.add_body;
            var retList = repository.Find(criteria);
            if (retList.Count > 0)
            {
                throw new ErsException("63068", ErsResources.GetFieldName("article_code"));
            }

            var id = repository.GetNextSequence();
            command.article_code = id.ToString();

            this.SaveFiles(command);

            newNewsArticle.id = id;
            newNewsArticle.article_code = command.article_code;
            var admin = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithUserCode(command.user_cd);
            newNewsArticle.create_user_id = admin.id;
            newNewsArticle.upd_user_id = admin.id;
            newNewsArticle.intime = DateTime.Now;
            newNewsArticle.utime = newNewsArticle.intime;
            newNewsArticle.active = command.active ?? EnumActive.NonActive;

            newNewsArticle.posted_date = command.posted_date;
            newNewsArticle.period_from = command.period_from;
            newNewsArticle.period_to = command.period_to;
            newNewsArticle.site_id = (int)EnumSiteId.COMMON_SITE_ID;

            var objContents = ErsFactory.ersContentsFactory.GetErsCmsContentsWithContentsCode(command.contents_code);
            for (var i = 0; i < objContents.link_count; i++)
            {
                var record = command.free_link_records[i];

                ErsExpressionAccessor<ErsNewsArticle, string>.SetPropertyValue(newNewsArticle, "link_string_" + (i + 1), record.link_string);
                ErsExpressionAccessor<ErsNewsArticle, string>.SetPropertyValue(newNewsArticle, "link_url_" + (i + 1), record.link_url);
            }

            for (var i = 0; i < objContents.file_count; i++)
            {
                var record = command.free_file_records[i];
                if (!string.IsNullOrEmpty(record.news_pdf_detail_filename))
                {
                    ErsExpressionAccessor<ErsNewsArticle, string>.SetPropertyValue(newNewsArticle, "file_real_name_" + (i + 1), record.news_pdf_detail_filename);
                }

                if (!string.IsNullOrEmpty(record.file_string))
                {
                    ErsExpressionAccessor<ErsNewsArticle, string>.SetPropertyValue(newNewsArticle, "file_string_" + (i + 1), record.file_string);
                }
            }

            //image
            var listImage = new List<string>();
            foreach (var group_record in command.free_img_group_records)
            {
                foreach (var record in group_record.free_img_records)
                {
                    if (!string.IsNullOrEmpty(record.img_file_name))
                    {
                        listImage.Add(record.img_file_name);
                    }
                    else
                    {
                        listImage.Add(string.Empty);
                    }
                }
            }
            newNewsArticle.img_file_name = listImage.ToArray();

            repository.Insert(newNewsArticle);
        }
    }
}