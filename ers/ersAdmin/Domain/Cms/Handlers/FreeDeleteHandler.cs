﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class FreeDeleteHandler
        : FreeModifyHandler, ICommandHandler<IFreeDeleteCommand>
    {
        public ICommandResult Submit(IFreeDeleteCommand command)
        {
            this.DeleteFiles(command);
            this.Delete(command);

            return new CommandResult(true);
        }

        private void Delete(IFreeDeleteCommand command)
        {
            //file delete
            foreach (var record in command.free_file_records)
            {
                record.file_delete = true;
            }

            //image delete
            foreach (var group_record in command.free_img_group_records)
            {
                foreach (var record in group_record.free_img_records)
                {
                    record.file_delete = true;
                }
            }

            var repository = ErsFactory.ersContentsFactory.GetErsNewsArticleRepository();
            var ErsNewsArticle = ErsFactory.ersContentsFactory.GetErsNewsArticleWithArticleCode(command.article_code);
            repository.Delete(ErsNewsArticle);
        }
    }
}