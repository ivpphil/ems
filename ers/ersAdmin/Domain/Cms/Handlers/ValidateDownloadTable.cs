﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers;
using System.IO;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ValidateDownloadTable:IValidationHandler<IDownloadTableCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IDownloadTableCommand command)
        {
            if (command.isSave)
            {
                if (command.file_name == null)
                    yield return command.CheckRequired("file_name");

                if (command.file_name != null)
                {
                    ///check existing
                    var setup = ErsFactory.ersUtilityFactory.getSetup();
                    var path = Path.Combine(setup.downloadFilePath, command.file_name.FileName);
                    if (File.Exists(path))
                        yield return new ValidationResult(ErsResources.GetMessage("fileExist"));
                }
            }
        }
    }
}