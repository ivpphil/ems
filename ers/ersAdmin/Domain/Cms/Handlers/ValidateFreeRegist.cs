﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ValidateFreeRegist
        : ValidateFreeModify, IValidationHandler<IFreeRegistCommand>
    {
        public IEnumerable<ValidationResult> Validate(IFreeRegistCommand command)
        {
            foreach (var result in this.ValidateInputs(command))
            {
                yield return result;
            }
        }
    }
}