﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ValidateFreeList
        : IValidationHandler<IFreeListCommand>
    {
        public IEnumerable<ValidationResult> Validate(IFreeListCommand command)
        {
            command.CheckRequired("contents_code");

            if (command.posted_date_from.HasValue && command.posted_date_to.HasValue)
            {
                if (command.posted_date_from > command.posted_date_to)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10045", ErsResources.GetFieldName("posted_date_to"), ErsResources.GetFieldName("posted_date_from")), new[] { "posted_date_from", "posted_date_to" });
                }
            }
        }
    }
}