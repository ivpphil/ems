﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using jp.co.ivp.ers;
using System.IO;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mall.site;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class FreeModifyHandler
        : ICommandHandler<IFreeModifyCommand>
    {
        public ICommandResult Submit(IFreeModifyCommand command)
        {
            if (command.IsConfirmation)
            {
                this.SaveTmpFiles(command);
            }
            else
            {
                this.DeleteFiles(command);
                this.SaveFiles(command);
                this.Update(command);
            }
            return new CommandResult(true);
        }

        protected void SaveTmpFiles(IFreeModifyCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();

            foreach (var group_record in command.free_img_group_records)
            {
                foreach (var record in group_record.free_img_records)
                {
                    if (record.news_image.ContentLength > 0)
                    {
                        record.temp_image_file_name = uploadedFileHelper.GetTempFileName(record.news_image);
                        uploadedFileHelper.SaveTempFile(record.news_image, setup.news_image_temp_path, record.temp_image_file_name);
                    }
                }
            }
        }

        protected void DeleteFiles(IFreeModifyCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();
            var sites = GetErsListSite();

            foreach (var group_record in command.free_img_group_records)
            {
                foreach (var record in group_record.free_img_records)
                {
                    if (!string.IsNullOrEmpty(record.img_file_name))
                    {
                        if (record.file_delete)
                        {
                            if (setup.Multiple_sites)
                            {
                                foreach (var site in sites)
                                {
                                    uploadedFileHelper.DeleteFile(setup.multiple_news_image_path((int)site.id), record.img_file_name);
                                }
                            }
                            else
                            {
                                uploadedFileHelper.DeleteFile(setup.news_image_path, record.img_file_name);
                            }
                                record.img_file_name = string.Empty;
                            
                        }
                    }
                }
            }

            foreach (var record in command.free_file_records)
            {
                //Only when browser is supported Drag and Drop Images runtime
                if (!record.disabled_file_uploader)
                {
                    if (record.old_file_real_name.HasValue())
                    {
                        if (File.Exists(setup.news_image_temp_path + record.old_file_real_name))
                            uploadedFileHelper.DeleteFile(setup.news_image_temp_path, record.old_file_real_name);
                    }
                }
            }
        }

        /// <summary>
        /// ERSサイトの一覧を取得します。
        /// </summary>
        /// <returns></returns>
        private IList<ErsSite> GetErsListSite()
        {
            var siteRepository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var siteCriteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
            siteCriteria.active = EnumActive.Active;
            siteCriteria.mall_shop_kbn = EnumMallShopKbn.ERS;
            siteCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listSite = siteRepository.Find(siteCriteria);
            return listSite;
        }

        protected void SaveFiles(IFreeModifyCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var sites = GetErsListSite();
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();

            var imgid = DateTime.Now.ToString("yyyyMMddHHmmss");

            foreach (var group_record in command.free_img_group_records)
            {
                foreach (var record in group_record.free_img_records)
                {
                    //image
                    if (!string.IsNullOrEmpty(record.temp_image_file_name))
                    {
                        var img_file_name = "news_" + command.article_code + "_" + record.img_count + "_" + imgid + Path.GetExtension(record.temp_image_file_name);
                        if (setup.Multiple_sites)
                        {
                            foreach (var site in sites)
                            {

                                uploadedFileHelper.SaveResizedImage(setup.news_image_temp_path, record.temp_image_file_name, setup.multiple_news_image_path((int)site.id), img_file_name, record.img_width.Value);
                            }
                        }
                        else
                        {
                            uploadedFileHelper.SaveResizedImage(setup.news_image_temp_path, record.temp_image_file_name, setup.news_image_path, img_file_name, record.img_width.Value);
                        }
                        record.img_file_name = img_file_name;
                    }
                }
            }

            foreach (var record in command.free_file_records)
            {
                //Only when browser is supported Drag and Drop Images runtime
                if (!record.disabled_file_uploader)
                {
                    if (record.HasRecordNewsPdfDetail)
                    {
                        foreach (var news_pdf_detail in record.news_pdf_detail)
                        {
                            string tempDirectory = setup.image_temp_directory + record.news_pdf_temp_folder + "\\";
                            if (setup.Multiple_sites)
                            {
                                foreach (var site in sites)
                                {
                                    news_pdf_detail.SaveFromTemp(tempDirectory, setup.multiple_news_image_path((int)site.id));
                                }
                            }
                            else
                            {
                                news_pdf_detail.SaveFromTemp(tempDirectory, setup.news_image_path);
                            }
                        }
                    }
                }
            }

            uploadedFileHelper.DeleteTempFile(setup.news_image_temp_path);
            uploadedFileHelper.DeleteTempFile(setup.image_temp_directory + "news_pdf\\");
        }

        protected void Update(IFreeModifyCommand command)
        {
            var repository = ErsFactory.ersContentsFactory.GetErsNewsArticleRepository();
            var newNewsArticle = ErsFactory.ersContentsFactory.GetErsNewsArticleWithArticleCode(command.article_code);
            newNewsArticle.OverwriteWithModel(command);

            var admin = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithUserCode(command.user_cd);
            newNewsArticle.upd_user_id = admin.id;
            newNewsArticle.utime = DateTime.Now;
            newNewsArticle.active = command.active ?? EnumActive.NonActive;

            newNewsArticle.posted_date = command.posted_date;
            newNewsArticle.period_from = command.period_from;
            newNewsArticle.period_to = command.period_to;
            newNewsArticle.site_id = (int)EnumSiteId.COMMON_SITE_ID;

            var objContents = ErsFactory.ersContentsFactory.GetErsCmsContentsWithContentsCode(command.contents_code);
            for (var i = 0; i < objContents.link_count; i++)
            {
                var record = command.free_link_records[i];

                ErsExpressionAccessor<ErsNewsArticle, string>.SetPropertyValue(newNewsArticle, "link_string_" + (i + 1), record.link_string);
                ErsExpressionAccessor<ErsNewsArticle, string>.SetPropertyValue(newNewsArticle, "link_url_" + (i + 1), record.link_url);
            }

            for (var i = 0; i < objContents.file_count; i++)
            {
                var record = command.free_file_records[i];

                //Only when browser is supported Drag and Drop Images runtime
                if (!record.disabled_file_uploader)
                {
                    ErsExpressionAccessor<ErsNewsArticle, string>.SetPropertyValue(newNewsArticle, "file_real_name_" + (i + 1), record.news_pdf_detail_filename);
                    ErsExpressionAccessor<ErsNewsArticle, string>.SetPropertyValue(newNewsArticle, "file_string_" + (i + 1), record.file_string);
                }
            }

            //image
            var listImage = new List<string>();
            foreach (var group_record in command.free_img_group_records)
            {
                foreach (var record in group_record.free_img_records)
                {
                    if (!string.IsNullOrEmpty(record.img_file_name))
                    {
                        listImage.Add(record.img_file_name);
                    }
                    else
                    {
                        listImage.Add(string.Empty);
                    }
                }
            }
            newNewsArticle.img_file_name = listImage.ToArray();

            var old_news = ErsFactory.ersContentsFactory.GetErsNewsArticleWithArticleCode(command.article_code);
            repository.Update(old_news, newNewsArticle);
        }
    }
}