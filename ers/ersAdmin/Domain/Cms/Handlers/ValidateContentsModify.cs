﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Cms.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.contents;

namespace ersAdmin.Domain.Cms.Handlers
{
    public class ValidateContentsModify
        : IValidationHandler<IContentsModifyCommand>
    {
        public IEnumerable<ValidationResult> Validate(IContentsModifyCommand command)
        {
            yield return command.CheckRequired("id");

            var repository = ErsFactory.ersContentsFactory.GetErsCmsContentsRepository();
            var criteria = ErsFactory.ersContentsFactory.GetErsCmsContentsCriteria();
            criteria.id_not_equal = command.id;
            criteria.contents_code = command.contents_code;
            IList<ErsCmsContents> list = repository.Find(criteria);
            if (list.Count > 0)
            {
                yield return new ValidationResult(ErsResources.GetMessage("63068", ErsResources.GetFieldName("contents_code")), new[] { "contents_code" });
            }
        }
    }
}