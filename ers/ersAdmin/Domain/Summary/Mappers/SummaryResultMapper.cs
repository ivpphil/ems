﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Summary.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.summary;
using ersAdmin.Models.summary;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Summary.Mappers
{
    public class SummaryResultMapper
        : IMapper<ISummaryResultMappable>
    {
        public void Map(ISummaryResultMappable objMappable)
        {
            objMappable.group_name = this.GetGroupName(objMappable);
            objMappable.summary_results = this.GetSummaryList(objMappable);
        }

        /// <summary>
        /// 集計グループ名を取得
        /// </summary>
        /// <param name="objMappable"></param>
        /// <returns></returns>
        private string GetGroupName(ISummaryResultMappable objMappable)
        {
            var repository = ErsFactory.ersSummaryFactory.GetErsSummaryGroupRepositroy();
            var criteria = ErsFactory.ersSummaryFactory.GetErsSummaryGroupCriteria();
            criteria.group_code = objMappable.group_code;
            criteria.active = EnumActive.Active;
            var listGroup = repository.Find(criteria);
            if (listGroup.Count == 0)
            {
                throw new ErsException("10200");
            }

            return listGroup.First().group_name;
        }

        /// <summary>
        /// 集計結果を取得します。
        /// </summary>
        /// <param name="objMappable"></param>
        /// <returns></returns>
        private List<summary_result> GetSummaryList(ISummaryResultMappable objMappable)
        {
            //表示する集計記述データのリストを取得
            var repository = ErsFactory.ersSummaryFactory.GetErsSummaryRepository();
            var criteria = ErsFactory.ersSummaryFactory.GetErsSummaryCriteria();
            criteria.group_code = objMappable.group_code;
            criteria.active = EnumActive.Active;
            criteria.SetOrderByDisp_order(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listSummayRecord = repository.Find(criteria);

            //集計の実行
            var listSummary = new List<summary_result>();
            foreach (var summary in listSummayRecord)
            {
                summary_result summary_result;
                switch (summary.summary_type)
                {
                    case EnumSummaryType.Aggregate:
                        //集計時
                        summary_result = this.CreateAggregateResult(objMappable, summary);
                        break;

                    case EnumSummaryType.Extract:
                        //データ週出
                        summary_result = this.CreateExtractResult(objMappable, summary);
                       break;

                    default:
                        continue;
                }
                listSummary.Add(summary_result);
            }

            return listSummary;
        }

        /// <summary>
        /// 抽出結果を作成します
        /// </summary>
        /// <param name="objMappable"></param>
        /// <param name="summary"></param>
        /// <returns></returns>
        private summary_result CreateExtractResult(ISummaryResultMappable objMappable, ErsSummary summary)
        {
            var createSummaryResultExtractStgy = ErsFactory.ersSummaryFactory.GetCreateSummaryResultExtractStgy();
            var listSummaryExtract = this.GetSummaryExtract(summary);

            var summary_result = new summary_result();
            summary_result.OverwriteWithParameter(summary.GetPropertiesAsDictionary());
            summary_result.listResult =  createSummaryResultExtractStgy.GetResultWithCache(summary, listSummaryExtract, objMappable.summary_conditions);
            if (summary_result.listResult.Count() > 0)
            {
                summary_result.FirstResult = summary_result.listResult.First();
            }

            return summary_result;
        }

        /// <summary>
        /// 抽出用のデータを取得します
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        protected IEnumerable<ErsSummaryExtract> GetSummaryExtract(ErsSummary summary)
        {
            var repository = ErsFactory.ersSummaryFactory.GetErsSummaryExtractRepository();
            var criteria = ErsFactory.ersSummaryFactory.GetErsSummaryExtractCriteria();
            criteria.group_code = summary.group_code;
            criteria.summary_code = summary.summary_code;
            criteria.active = EnumActive.Active;
            criteria.SetOrderByDisp_order(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listSummaryExtract = repository.Find(criteria);
            if (listSummaryExtract.Count == 0)
            {
                throw new Exception("Extract conditions are not found.Please define extract condition for summary_code '" + summary.summary_code + "' to summary_extract_t.");
            }
            return listSummaryExtract;
        }

        /// <summary>
        /// 集約結果を作成します。
        /// </summary>
        /// <param name="objMappable"></param>
        /// <param name="summary"></param>
        /// <param name="summary_result"></param>
        private summary_result CreateAggregateResult(ISummaryResultMappable objMappable, ErsSummary summary)
        {
            var createSummaryResultAggregateStgy = ErsFactory.ersSummaryFactory.GetCreateSummaryResultAggregateStgy();
            var summaryAggregate = this.GetSummaryAggregate(summary);

            var summary_result = new summary_result();
            summary_result.OverwriteWithParameter(summary.GetPropertiesAsDictionary());
            summary_result.display_graph = summaryAggregate.display_graph;
            summary_result.graph_type = summaryAggregate.graph_type;
            summary_result.listResult = createSummaryResultAggregateStgy.GetResultWithCache(summary, summaryAggregate, objMappable.summary_conditions);
            summary_result.listResultValues =  createSummaryResultAggregateStgy.GetResultOfGraph(summary_result.listResult);
            if (summary_result.listResult.Count() > 0)
            {
                summary_result.FirstResult = summary_result.listResult.First();
                summary_result.FirstResultValues = createSummaryResultAggregateStgy.GetFirstResultOfGraph(summary_result.FirstResult);
            }


            return summary_result;
        }

        /// <summary>
        /// 集約用のデータを取得します
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        protected ErsSummaryAggregate GetSummaryAggregate(ErsSummary summary)
        {
            var repository = ErsFactory.ersSummaryFactory.GetErsSummaryAggregateRepository();
            var criteria = ErsFactory.ersSummaryFactory.GetErsSummaryAggregateCriteria();
            criteria.group_code = summary.group_code;
            criteria.summary_code = summary.summary_code;
            criteria.active = EnumActive.Active;
            var listSummaryAggregate = repository.Find(criteria);
            if (listSummaryAggregate.Count == 0)
            {
                throw new Exception("Aggregate conditions are not found.Please define aggregate condition for summary_code '" + summary.summary_code + "' to summary_aggregate_t.");
            }
            var summaryAggregate = listSummaryAggregate.First();
            return summaryAggregate;
        }
    }
}