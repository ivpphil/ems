﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Summary.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Summary.Mappers
{
    public class SummaryResultCSVMapper
        : SummaryResultMapper, IMapper<ISummaryResultCSVMappable>
    {
        public void Map(ISummaryResultCSVMappable objMappable)
        {
            var repository = ErsFactory.ersSummaryFactory.GetErsSummaryRepository();
            var criteria = ErsFactory.ersSummaryFactory.GetErsSummaryCriteria();
            criteria.group_code = objMappable.group_code;
            criteria.summary_code = objMappable.summary_code;
            criteria.active = EnumActive.Active;
            var listSummayRecord = repository.Find(criteria);
            if (listSummayRecord.Count == 0)
            {
                throw new ErsException("10200");
            }

            var objSummary = listSummayRecord.First();

            IEnumerable<Dictionary<string, object>> listResult = null;
            switch (objSummary.summary_type)
            {
                case EnumSummaryType.Aggregate:
                    //集計時
                    var createSummaryResultAggregateStgy = ErsFactory.ersSummaryFactory.GetCreateSummaryResultAggregateStgy();
                    var summaryAggregate = this.GetSummaryAggregate(objSummary);
                    listResult =  createSummaryResultAggregateStgy.GetResult(objSummary, summaryAggregate, objMappable.summary_conditions);
                    break;
                case EnumSummaryType.Extract:
                    //データ週出
                    var createSummaryResultExtractStgy = ErsFactory.ersSummaryFactory.GetCreateSummaryResultExtractStgy();
                    var summaryExtract = this.GetSummaryExtract(objSummary);
                    listResult =  createSummaryResultExtractStgy.GetResult(objSummary, summaryExtract, objMappable.summary_conditions);
                    break;
            }

            var listColumns = listResult.First().Select((pair) => pair.Key).ToArray();

            //列名のヘッダーは出力しない
            if (objSummary.summary_type == EnumSummaryType.Aggregate)
            {
                listColumns[0] = string.Empty;
            }

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader(listColumns, writer);
                foreach (var item in listResult)
                {
                    objMappable.csvCreater.WriteBody(item, writer);
                }
            }
        }
    }
}