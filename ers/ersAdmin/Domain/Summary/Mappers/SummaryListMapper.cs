﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Summary.Mappables;
using jp.co.ivp.ers.db;
using ersAdmin.Models.summary;

namespace ersAdmin.Domain.Summary.Mappers
{
    public class SummaryListMapper
        : IMapper<ISummaryListMappable>
    {
        public void Map(ISummaryListMappable objMappable)
        {
            objMappable.GroupList = this.GetGroupList(objMappable);
        }

        private List<Dictionary<string, object>> GetGroupList(ISummaryListMappable objMappable)
        {
            var repository = ErsFactory.ersSummaryFactory.GetErsSummaryGroupRepositroy();
            var criteria = ErsFactory.ersSummaryFactory.GetErsSummaryGroupCriteria();
            criteria.active = EnumActive.Active;
            criteria.SetOrderByDisp_order(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listGroupRecord = repository.Find(criteria);
            if (listGroupRecord.Count == 0)
            {
                return null;
            }

            var listGroupList = new List<Dictionary<string, object>>();

            foreach (var record in listGroupRecord)
            {
                var dictionary = record.GetPropertiesAsDictionary();

                //グループのwhere句を取得
                if (objMappable.IsErrorBack && record.group_code == objMappable.group_code)
                {
                    //エラー戻り時
                    dictionary["summary_conditions"] = this.UpdateSummaryConditions(objMappable);
                }
                else
                {
                    dictionary["summary_conditions"] = this.GetSummaryConditions(record.group_code);
                }

                listGroupList.Add(dictionary);
            }

            //初期値設定
            if (!objMappable.IsErrorBack || !objMappable.group_code.HasValue())
            {
                objMappable.group_code = listGroupRecord.First().group_code;
            }

            return listGroupList;
        }

        private IList<summary_condition> UpdateSummaryConditions(ISummaryListMappable objMappable)
        {
            foreach (var condition in objMappable.summary_conditions)
            {
                if (!condition.IsValidField("id"))
                {
                    //IDが不正な場合は全てのレコードを再読込（不正な状態なので。）
                    return this.GetSummaryConditions(objMappable.group_code);
                }

                var summaryCondition = ErsFactory.ersSummaryFactory.GetErsSummaryConditionWithId(condition.id);
                condition.OverwriteWithParameter(summaryCondition.GetPropertiesAsDictionary());
                condition.onInitialize = false;
            }
            return objMappable.summary_conditions;
        }

        private IList<summary_condition> GetSummaryConditions(string group_code)
        {
            var repository = ErsFactory.ersSummaryFactory.GetErsSummaryConditionRepositroy();
            var criteria = ErsFactory.ersSummaryFactory.GetErsSummaryConditionCriteria();
            criteria.group_code = group_code;
            criteria.active = EnumActive.Active;
            criteria.SetOrderByDisp_order(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listConditionRecord = repository.Find(criteria);
            if (listConditionRecord.Count == 0)
            {
                return null;
            }

            var listConditionList = new List<summary_condition>();
            foreach (var record in listConditionRecord)
            {
                var condition = new summary_condition();
                condition.OverwriteWithParameter(record.GetPropertiesAsDictionary());
                condition.onInitialize = true;
                listConditionList.Add(condition);
            }
            return listConditionList;
        }
    }
}