﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.summary;

namespace ersAdmin.Domain.Summary.Commands
{
    public interface ISummaryResultCommand
        : ICommand
    {
        IList<summary_condition> summary_conditions { get; set; }
    }
}