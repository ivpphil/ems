﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Summary.Commands
{
    public interface ISummaryConditionCommand
        : ICommand
    {
        int? id { get; set; }

        string value { get; set; }

        string value_from { get; set; }

        string value_to { get; set; }
    }
}