﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Summary.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Summary.Handlers
{
    public class ValidateSummaryResult
        : IValidationHandler<ISummaryResultCommand>
    {
        public IEnumerable<ValidationResult> Validate(ISummaryResultCommand command)
        {
            yield return command.CheckRequired("group_code");
            
            if (command.summary_conditions != null)
            {
                foreach (var record in command.summary_conditions)
                {
                    record.AddInvalidField(command.controller.commandBus.Validate<ISummaryConditionCommand>(record));
                    if (!record.IsValid)
                    {
                        foreach (var errorMessage in record.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "summary_conditions" });
                        }
                    }
                }
            }
            else
            {
                yield return new ValidationResult(ErsResources.GetMessage("10000", ErsResources.GetFieldName("value")));
            }
        }
    }
}