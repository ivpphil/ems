﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Summary.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.summary;

namespace ersAdmin.Domain.Summary.Handlers
{
    public class ValidateSummaryCondition
        : IValidationHandler<ISummaryConditionCommand>
    {
        public IEnumerable<ValidationResult> Validate(ISummaryConditionCommand command)
        {
            yield return command.CheckRequired("id");
            if (!command.IsValidField("id"))
            {
                yield break;
            }

            var summaryCondition = ErsFactory.ersSummaryFactory.GetErsSummaryConditionWithId(command.id);
            if (summaryCondition == null)
            {
                yield return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName("id")), new[] { "id" });
                yield break;
            }

            switch (summaryCondition.where_oparation)
            {
                case EnumSummaryWhereOparation.BETWEEN_DATE:
                    foreach (var result in this.ValidateBetween(command, summaryCondition))
                    {
                        yield return result;
                    }
                    break;
                default:
                    foreach (var result in this.ValidateDefault(command, summaryCondition))
                    {
                        yield return result;
                    }
                    break;
            }
        }

        private IEnumerable<ValidationResult> ValidateDefault(ISummaryConditionCommand command, ErsSummaryCondition summaryCondition)
        {
            ValidationResult validationResult;
            command.value = this.ValidateValue(out validationResult, command, summaryCondition, command.value);
            yield return validationResult;

            if (summaryCondition.is_required == EnumOnOff.On)
            {
                //必須チェック
                yield return command.CheckRequired(summaryCondition.item_name, "value", command.value);
            }
        }

        private IEnumerable<ValidationResult> ValidateBetween(ISummaryConditionCommand command, ErsSummaryCondition summaryCondition)
        {
            ValidationResult validationResult;
            command.value_from = this.ValidateValue(out validationResult, command, summaryCondition, command.value_from);
            yield return validationResult;

            command.value_to = this.ValidateValue(out validationResult, command, summaryCondition, command.value_to);
            yield return validationResult;

            if (summaryCondition.is_required == EnumOnOff.On)
            {
                //必須チェック
                yield return command.CheckRequired(summaryCondition.item_name, "value_from", command.value_from);
                yield return command.CheckRequired(summaryCondition.item_name, "value_to", command.value_to);
            }
        }

        private string ValidateValue(out ValidationResult validationResult, ISummaryConditionCommand command, ErsSummaryCondition summaryCondition, string value)
        {
            if (value == null)
            {
                validationResult = null;
                return null;
            }

            var validator = new ErsSchemaValidationAttribute(summaryCondition.validation_comment);
            return validator.Validate(out validationResult, summaryCondition.item_name, summaryCondition.column_name, value);
        }
    }
}