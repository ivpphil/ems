﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Summary.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Summary.Handlers
{
    public class ValidateSummaryResultCSV
        : IValidationHandler<ISummaryResultCSVCommand>
    {
        public IEnumerable<ValidationResult> Validate(ISummaryResultCSVCommand command)
        {
            yield return command.CheckRequired("summary_code");
        }
    }
}