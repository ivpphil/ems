﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Summary.Mappables
{
    public interface ISummaryResultCSVMappable
        : IMappable, ISummaryResultMappable
    {
        ErsCsvCreater csvCreater { get; set; }

        string summary_code { get; set; }
    }
}