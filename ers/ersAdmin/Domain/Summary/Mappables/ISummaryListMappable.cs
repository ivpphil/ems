﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.summary;

namespace ersAdmin.Domain.Summary.Mappables
{
    public interface ISummaryListMappable
        : IMappable
    {
        bool IsErrorBack { get; set; }

        string group_code { get; set; }

        List<Dictionary<string, object>> GroupList { get; set; }

        IList<summary_condition> summary_conditions { get; set; }
    }
}