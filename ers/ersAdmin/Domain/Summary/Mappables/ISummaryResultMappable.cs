﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.summary;

namespace ersAdmin.Domain.Summary.Mappables
{
    public interface ISummaryResultMappable
        : IMappable
    {
        IList<summary_condition> summary_conditions { get; set; }

        string group_name { get; set; }

        string group_code { get; set; }

        IList<summary_result> summary_results { get; set; }
    }
}