﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface IItemAmazonListCSVMappable
        : IItemRakutenListCSVMappable, IItemListMappable
    {
    }
}