﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.csv;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface IItemCSVMappable
        : IMappable
    {
        ErsCsvContainer<Item_csv_record> csv_file { get; set; }
    }
}