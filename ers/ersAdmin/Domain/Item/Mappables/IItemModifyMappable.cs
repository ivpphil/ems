﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models;
using ersAdmin.Models.item.mall.yahoo;
using ersAdmin.Models.item.mall.rakuten;
using ersAdmin.Models.item.mall.amazon.health;
using ersAdmin.Models.item.mall;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface IItemModifyMappable
        : ISiteRegisterBaseMappable, IMappable
    {
        string gcode { get; set; }

        string[] disp_keyword { get; set; }

        int disp_send_ptn_month_intervals { get; set; }

        int disp_send_ptn_week_intervals { get; set; }

        int disp_send_ptn_month_day_intervals { get; set; }

        string old_gcode { get; set; }

        List<Item_modify_detail> detail_table { get; set; }

        [Obsolete]
        List<Item_modify_yahoo_detail> yahoo_detail_table { get; set; }

        [Obsolete]
        List<Item_modify_rakuten_detail> rakuten_detail_table { get; set; }

        [Obsolete]
        List<Item_modify_amazon_detail> amazon_detail_table { get; set; }

        List<Item_modify_mall_detail_list> listMallList { get; set; }
    }
}