﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface ICateTreeViewMappable
        : ISiteSearchBaseMappable, IMappable
    {
        List<Dictionary<string, object>> MainList { set; }

        List<Dictionary<string, object>> TreeList { get; set; }
        List<Dictionary<string, object>> cateList1 { get; set; }
        List<Dictionary<string, object>> cateList2 { get; set; }
        List<Dictionary<string, object>> cateList3 { get; set; }
        List<Dictionary<string, object>> cateList4 { get; set; }
        List<Dictionary<string, object>> cateList5 { get; set; }

        EnumActive? cate_kind1_active { get; set; }
        EnumActive? cate_kind2_active { get; set; }
        EnumActive? cate_kind3_active { get; set; }
        EnumActive? cate_kind4_active { get; set; }
        EnumActive? cate_kind5_active { get; set; }
    }
}