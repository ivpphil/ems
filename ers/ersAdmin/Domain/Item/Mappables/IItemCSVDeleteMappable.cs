﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface IItemCSVDeleteMappable
        : IMappable
    {
        ErsCsvContainer<ersAdmin.Models.csv.Item_Csv_Delete_record> csv_file { get; }

        IEnumerable<ersAdmin.Models.csv.Item_Csv_Delete_record> csv_file_validated { get; set; }
    }
}