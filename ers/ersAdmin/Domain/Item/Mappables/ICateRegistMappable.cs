﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.item;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface ICateRegistMappable
        : ISiteRegisterBaseMappable, IMappable
    {
        bool registCategoryItem { get; }

        bool registCategory { get; }

        int? categoryNumber { get; set; }

        List<Cate_item> cate_body_table { get; set; }

        List<Cate_header> cate_header_table { set; }

        bool cate_edit_1 { get; }

        bool cate_edit_2 { get; }

        bool cate_edit_3 { get; }

        bool cate_edit_4 { get; }

        bool cate_edit_5 { get; }
    }
}