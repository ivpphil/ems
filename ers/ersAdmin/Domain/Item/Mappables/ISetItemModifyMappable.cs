﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.pager;
using ersAdmin.Models.item;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.merchandise;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface ISetItemModifyMappable : IMappable
    {
        long recordCount { get; set; }

        ErsPagerModel pager { get; }

        bool LoadData { get; set; }

        int? id { get; set; }

        string parent_scode { get; set; }

        string parent_sname { get; set; }

        int? price { get; set; }

        int? regular_price { get; set; }

        int? regular_first_price { get; set; }

        bool delete { get; set; }

        List<ChildScodeListData> ChildScodeList { get; set; }

        string append_scode { get; set; }
    }
}
