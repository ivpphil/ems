﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.merchandise;

namespace ersAdmin.Domain.Item.Mappables 
{
    public interface ISetItemSearchMappable : IMappable
    {
        ErsPagerModel pager { get; set; }

        long recordCount { get; set; }

        string s_parent_scode { get; set; }

        string s_child_scode { get; set; }

        IEnumerable<ErsMerchandise> setItemList { get; set; }
    }
}
