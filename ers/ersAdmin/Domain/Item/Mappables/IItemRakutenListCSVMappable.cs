﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface IItemRakutenListCSVMappable
        : IMappable, IItemListMappable
    {
        ErsTsvCreater tsvCreater { get; }
        ErsCsvCreater csvCreater { get; set;}

        int? download_site_id { get; set; }
    }
}