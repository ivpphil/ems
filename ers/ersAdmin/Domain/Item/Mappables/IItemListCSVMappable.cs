﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface IItemListCSVMappable
        : IMappable, IItemListMappable
    {
        ErsCsvCreater csvCreater { get; }
    }
}