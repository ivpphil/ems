﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface IItemCSVRecordMappable
        : IMappable
    {
        EnumSoldoutFlg? soldout_flg { get; set; }

        EnumActive? active { get; set; }

        int? sort { get; set; }

        int? disp_order { get; set; }

        int? max_purchase_count { get; set; }

        int? point { get; set; }

        string disp_send_ptn { get; set; }

        DateTime? date_to { get; set; }

        DateTime? point_campaign_to { get; set; }

        DateTime? intime { get; set; }
    }
}