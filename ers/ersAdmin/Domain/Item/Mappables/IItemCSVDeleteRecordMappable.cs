﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface IItemCSVDeleteRecordMappable
        : IMappable
    {
        string scode { get; }
    }
}