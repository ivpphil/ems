﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall.site;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface IItemListMappable
        : ISiteSearchBaseMappable, IMappable
    {
        ErsPagerModel pager { get; }

        long recordCount { get; set; }

        List<Dictionary<string, object>> MeList { set; }

        string s_gcode { get; }

        string s_jancode { get; }

        string s_scode { get; }

        string s_sname { get; }

        int? s_cate1 { get; }

        int? s_cate2 { get; }

        int? s_cate3 { get; }

        int? s_cate4 { get; }

        int? s_cate5 { get; }

        int[] s_site_id_mall { get; set; }

        IList<ErsSite> listMallList { get; set; }

        bool isInitilize { get; set; }
    }
}