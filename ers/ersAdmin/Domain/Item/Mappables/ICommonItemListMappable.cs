﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface ICommonItemListMappable
        : IMappable
    {
        bool g_search_mode { get; }

        ErsPagerModel pager { get; }

        long recordCount { get; set; }

        List<Dictionary<string, object>> MeList { set; }

        string s_gcode { get; }

        string s_jancode { get; }

        string s_scode { get; }

        string s_sname { get; }

        int? s_cate1 { get; }

        int? s_cate2 { get; }

        int? s_cate3 { get; }

        int? s_cate4 { get; }

        int? s_cate5 { get; }
    }
}