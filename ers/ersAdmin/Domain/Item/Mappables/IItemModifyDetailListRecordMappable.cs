﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.merchandise;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface IItemModifyDetailListRecordMappable
        : IMappable
    {
        ErsSku objSku { get; set; }

        string scode { get; set; }

        string old_scode { get; set; }

        int? stock { get; set; }

        int? old_stock { get; set; }
    }
}