﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models;

namespace ersAdmin.Domain.Item.Mappables
{
    public interface IItemCoexistenceMappable
        : IMappable
    {
        List<Item_coexistence_detail> mixedGroupList { get; set; }
    }
}