﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Models.item;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Item.Mappers
{
    public class SetItemModifyMapper : IMapper<ISetItemModifyMappable>
    {

        public void Map(ISetItemModifyMappable objMappable)
        {
            this.Load(objMappable);
        }

        internal void Load(ISetItemModifyMappable objMappable)
        {
            if (objMappable.parent_scode != null)
            {
                var objMerchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(objMappable.parent_scode, null);

                objMappable.parent_sname = objMerchandise.sname;
                objMappable.price = objMerchandise.price;
                objMappable.regular_price = objMerchandise.regular_price;
                objMappable.regular_first_price = objMerchandise.regular_first_price;

            }

            if (objMappable.append_scode.HasValue())
            {
                this.AppendLine(objMappable);
            }
            else
            {
                objMappable.ChildScodeList = this.LoadObject(objMappable);
            }
        }

        private void AppendLine(ISetItemModifyMappable objMappable)
        {
            if (objMappable.ChildScodeList == null)
            {
                objMappable.ChildScodeList = new List<ChildScodeListData>();
            }

            if(objMappable.ChildScodeList.Exists((value) => value.scode == objMappable.append_scode))
            {
                return;
            }

            var content = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(objMappable.append_scode);
            var contentItem = new ChildScodeListData();
            contentItem.scode = content.scode;
            contentItem.sname = content.sname;
            objMappable.ChildScodeList.Add(contentItem);
        }

        private List<ChildScodeListData> LoadObject(ISetItemModifyMappable objMappable)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseRepository();
            var cri = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseCriteria();

            cri.parent_scode = objMappable.parent_scode;

            var setList = repository.Find(cri);

            var list = new List<ChildScodeListData>();

            foreach (var content in setList)
            {
                var contentItem = new ChildScodeListData();
                contentItem.OverwriteWithParameter(content.GetPropertiesAsDictionary());

                list.Add(contentItem);
            }

            return list;
        }
    }
}