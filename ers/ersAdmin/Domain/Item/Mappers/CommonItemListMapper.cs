﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Mappers
{
    public class CommonItemListMapper
        : IMapper<ICommonItemListMappable>
    {
        public void Map(ICommonItemListMappable objMappable)
        {
            Init(objMappable);
        }

        /// <summary>
        /// itemリストを初期化する。
        /// </summary>
        /// <param name="pager"></param>
        private void Init(ICommonItemListMappable objMappable)
        {
            //search s_master_t
            var emRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

            //商品出力結果
            IList<ErsMerchandise> list = null;
            if (objMappable.g_search_mode)
            {
                //検索条件をクライテリアに保存
                var emCri = this.GetGroupCriteria(objMappable);

                //search g_master_t
                //検索結果の総数を取得
                objMappable.recordCount = emRepository.GetRecordCountGroupBase(emCri);
                if (objMappable.recordCount == 0)
                {
                    objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                    return;
                }

                //検索SQLにLIMIT と OFFSETを加える
                objMappable.pager.SetLimitAndOffsetToCriteria(emCri);

                emCri.SetOrderByGcode(Criteria.OrderBy.ORDER_BY_ASC);

                list = emRepository.FindGroupBaseItemList(emCri);
            }
            else
            {
                //検索条件をクライテリアに保存
                var emCri = this.GetSkuCriteria(objMappable);

                emCri.scode_not_equal = null;

                //検索結果の総数を取得
                objMappable.recordCount = emRepository.GetRecordCountSkuBase(emCri);
                if (objMappable.recordCount == 0)
                {
                    objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                    return;
                }

                //検索SQLにLIMIT と OFFSETを加える
                objMappable.pager.SetLimitAndOffsetToCriteria(emCri);

                emCri.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

                list = emRepository.FindSkuBaseItemList(emCri, null);
            }

            objMappable.MeList = ErsCommon.ConvertEntityListToDictionaryList(list);
        }

        /// <summary>
        /// 検索条件をクライテリアにセット
        /// </summary>
        /// <returns></returns>
        private ErsGroupCriteria GetGroupCriteria(ICommonItemListMappable objMappable)
        {
            var emCri = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //除外するgcode
            emCri.ignore_gcode = setup.IgnoreGcode;

            //クライテリアにパラメタを渡す
            if (!string.IsNullOrEmpty(objMappable.s_gcode))
                emCri.gcode = objMappable.s_gcode;

            if (!string.IsNullOrEmpty(objMappable.s_jancode))
                emCri.jancode = objMappable.s_jancode;

            if (!string.IsNullOrEmpty(objMappable.s_scode))
                emCri.scode = objMappable.s_scode;

            if (!string.IsNullOrEmpty(objMappable.s_sname))
                emCri.sname_and_gname = objMappable.s_sname;

            if (objMappable.s_cate1 != null)
                emCri.cate1 = objMappable.s_cate1.Value;

            if (objMappable.s_cate2 != null)
                emCri.cate2 = objMappable.s_cate2.Value;

            if (objMappable.s_cate3 != null)
                emCri.cate3 = objMappable.s_cate3.Value;

            if (objMappable.s_cate4 != null)
                emCri.cate4 = objMappable.s_cate4.Value;

            if (objMappable.s_cate5 != null)
                emCri.cate5 = objMappable.s_cate5.Value;

            return emCri;
        }

        /// <summary>
        /// 検索条件をクライテリアにセット
        /// </summary>
        /// <returns></returns>
        private ErsSkuCriteria GetSkuCriteria(ICommonItemListMappable objMappable)
        {
            var emCri = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //除外するgcode
            emCri.ignore_gcode = setup.IgnoreGcode;

            //クライテリアにパラメタを渡す
            if (!string.IsNullOrEmpty(objMappable.s_gcode))
                emCri.gcode = objMappable.s_gcode;

            if (!string.IsNullOrEmpty(objMappable.s_jancode))
                emCri.jancode = objMappable.s_jancode;

            if (!string.IsNullOrEmpty(objMappable.s_scode))
                emCri.scode = objMappable.s_scode;

            if (!string.IsNullOrEmpty(objMappable.s_sname))
                emCri.sname_and_gname = objMappable.s_sname;

            if (objMappable.s_cate1 != null)
                emCri.cate1 = objMappable.s_cate1.Value;

            if (objMappable.s_cate2 != null)
                emCri.cate2 = objMappable.s_cate2.Value;

            if (objMappable.s_cate3 != null)
                emCri.cate3 = objMappable.s_cate3.Value;

            if (objMappable.s_cate4 != null)
                emCri.cate4 = objMappable.s_cate4.Value;

            if (objMappable.s_cate5 != null)
                emCri.cate5 = objMappable.s_cate5.Value;

            return emCri;
        }
    }
}