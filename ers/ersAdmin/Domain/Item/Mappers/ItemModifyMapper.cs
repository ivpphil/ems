﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using ersAdmin.Models;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.util;
using ersAdmin.Models.item.mall.yahoo;
using ersAdmin.Models.item.mall.amazon.health;
using ersAdmin.Models.item.mall.rakuten;
using ersAdmin.Models.item.mall;
using ersAdmin.Models.item;
using System.IO;
using jp.co.ivp.ers.mall.site;

namespace ersAdmin.Domain.Item.Mappers
{
    public class ItemModifyMapper
        : IMapper<IItemModifyMappable>
    {
        public void Map(IItemModifyMappable objMappable)
        {
            this.SetErsMerchandiseWithGcode(objMappable);
        }

        /// <summary>
        /// 
        /// </summary>
        internal void SetErsMerchandiseWithGcode(IItemModifyMappable objMappable)
        {
            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            groupCriteria.gcode = objMappable.gcode;
            var listGroup = groupRepository.Find(groupCriteria);

            if (listGroup.Count != 1)
            {
                throw new ErsException("20002");
            }

            var objGroup = listGroup.First();

            if (!objGroup.disp_send_ptn.HasValue())
            {
                objGroup.disp_send_ptn = "0";
            }
            var disp_send_ptn = objGroup.disp_send_ptn.PadLeft(3, '0');

            objMappable.OverwriteWithParameter(objGroup.GetPropertiesAsDictionary());
            objMappable.disp_send_ptn_month_intervals = Convert.ToInt32(disp_send_ptn.Substring(0, 1));
            objMappable.disp_send_ptn_week_intervals = Convert.ToInt32(disp_send_ptn.Substring(1, 1));
            objMappable.disp_send_ptn_month_day_intervals = Convert.ToInt32(disp_send_ptn.Substring(2, 1));
            objMappable.site_id = objGroup.site_id;

            objMappable.old_gcode = objMappable.gcode;//save gcode

            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var skuCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            skuCriteria.gcode = objMappable.gcode;
            skuCriteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
            var listSku = skuRepository.Find(skuCriteria);

            objMappable.detail_table = this.GetDetailTableData(objMappable, listSku);

            this.SetMallMerchandiseData(objMappable, objGroup, listSku);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objMappable"></param>
        internal List<Item_modify_detail> GetDetailTableData(IItemModifyMappable objMappable, IEnumerable<ErsSku> listSku)
        {
            var list = new List<Item_modify_detail>();
            foreach (var objSku in listSku)
            {
                var item = new Item_modify_detail();
                item.objSku = objSku;
                objMappable.controller.mapperBus.Map<IItemModifyDetailListRecordMappable>(item);

                this.SetBimgImageDetail(objMappable, item);

                list.Add(item);
            }

            this.AddEmptyRecord(list);

            return list;
        }



        internal void SetBimgImageDetail(IItemModifyMappable objMappable, Item_modify_detail item)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var list = new List<item_bimg_detail>();
 
            var image_files = Directory.EnumerateFiles(setup.image_directory + "bimg\\", "S" + item.old_scode + "_0?.jpg");

            if (image_files != null)
            {
                foreach (var image in image_files)
                {
                    var bimg_detail = new item_bimg_detail();
                    bimg_detail.bimg = Path.GetFileName(image);

                    list.Add(bimg_detail);
                }
            }

            item.item_bimg_detail = list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        protected void AddEmptyRecord<T>(List<T> list)
        {
            for (var i = 0; i < 5; i++)
            {
                list.Add(ErsExpressionInstantiator<T>.GetInstantiation());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objMappable"></param>
        /// <param name="current_group"></param>
        internal void SetMallMerchandiseData(IItemModifyMappable objMappable, ErsGroup objGroup, IEnumerable<ErsSku> listSku)
        {
            //サイトIDの一覧を取得
            var siteRepository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var siteCriteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
            siteCriteria.active = EnumActive.Active;
            siteCriteria.mall_shop_kbn_not_equal = EnumMallShopKbn.ERS;
            siteCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listSite = siteRepository.Find(siteCriteria);

            var mallRepository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();

            var listMallList = new List<Item_modify_mall_detail_list>();
            foreach (var objSite in listSite)
            {
                var container = new Item_modify_mall_detail_list();
                container.site_id = objSite.id;
                container.site_name = objSite.site_name;
                container.mall_shop_kbn = objSite.mall_shop_kbn;

                var mallCriteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();
                mallCriteria.gcode = objMappable.gcode;
                mallCriteria.site_id = objSite.id;
                mallCriteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
                var listMall = mallRepository.Find(mallCriteria);

                var record_mall_table = new List<Item_modify_mall_detail>();
                foreach (var objSku in listSku)
                {
                    Item_modify_mall_detail inner_mall_table_record;
                    switch (objSite.mall_shop_kbn)
                    {
                        case EnumMallShopKbn.RAKUTEN:
                            inner_mall_table_record = new Item_modify_rakuten_detail();
                            break;

                        case EnumMallShopKbn.YAHOO:
                            inner_mall_table_record = new Item_modify_yahoo_detail();
                            break;

                        case EnumMallShopKbn.AMAZON:
                            inner_mall_table_record = new Item_modify_amazon_detail();
                            break;
                        default:
                            throw new Exception("Not excepted mall_shop_kbn");
                    }

                    var objMall = listMall.FirstOrDefault((recMall) => recMall.scode == objSku.scode);
                    if (objMall != null)
                    {
                        inner_mall_table_record.OverwriteWithParameter(objMall.GetPropertiesAsDictionary());
                    }

                    inner_mall_table_record.site_id = objSite.id;
                    inner_mall_table_record.scode = objSku.scode;
                    inner_mall_table_record.gcode = objSku.gcode;

                    record_mall_table.Add(inner_mall_table_record);
                }

                this.AddEmptyRecord(record_mall_table);

                container.listProduct = record_mall_table;
                listMallList.Add(container);
            }

            objMappable.listMallList = listMallList;
        }
    }
}