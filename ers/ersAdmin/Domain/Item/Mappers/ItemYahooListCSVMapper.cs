﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mall;
using ersAdmin.Models.item.mall.yahoo;

namespace ersAdmin.Domain.Item.Mappers
{
    public class ItemYahooListCSVMapper
        : ItemListMapper, IMapper<IItemYahooListCSVMappable>
    {
        public void Map(IItemYahooListCSVMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }
        /// <summary>
        /// CSVファイルを作成する。
        /// </summary>
        /// <returns></returns>
        public virtual void CreateCsvFile(IItemYahooListCSVMappable objMappable)
        {
            var mallRepository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();
            var mallCriteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();
            this.GetCriteria(objMappable, mallCriteria);
            mallCriteria.site_id = objMappable.download_site_id;

            var yahooList = mallRepository.FindWithMerchandise(mallCriteria);

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            //var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            //objMappable.csvCreater = csvCreater;

            CsvYahooItemRecord set_item_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<CsvYahooItemRecord>(writer);
                foreach (var item in yahooList)
                {
                    var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(item.scode, null);
                    set_item_csv = new CsvYahooItemRecord();
                    set_item_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                    objMappable.csvCreater.WriteBody(set_item_csv, writer);
                }
            }
        }

        /// <summary>
        /// TSVファイルを作成する。
        /// </summary>
        /// <returns></returns>
        public void CreateTsvFile(IItemYahooListCSVMappable objMappable)
        {
            var mallRepository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();
            var mallCriteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();
            this.GetCriteria(objMappable, mallCriteria);
            mallCriteria.site_id = objMappable.download_site_id;

            var yahooList = mallRepository.FindWithMerchandise(mallCriteria);

            //var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".txt";
            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            CsvYahooItemRecord item_tsv;

            using (var writer = objMappable.tsvCreater.GetWriter(filename))
            {
                objMappable.tsvCreater.WriteCsvHeader<CsvYahooItemRecord>(writer);
                foreach (var item in yahooList)
                {
                    var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(item.scode, null);
                    item_tsv = new CsvYahooItemRecord();
                    item_tsv.OverwriteWithParameter(item.GetPropertiesAsDictionary());

                    objMappable.tsvCreater.WriteBody(item_tsv, writer);
                }
            }
        }
    }
}