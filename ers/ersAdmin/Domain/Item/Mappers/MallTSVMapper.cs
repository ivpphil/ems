﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ersAdmin.Models.item.tsv;

namespace ersAdmin.Domain.Item.Mappers
{
    public class MallTSVMapper
        : IMapper<IMallTSVMappable>
    {
        public void Map(IMallTSVMappable objMappable)
        {
            //サイトIDの一覧を取得
            var siteRepository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var siteCriteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
            siteCriteria.active = EnumActive.Active;
            siteCriteria.mall_shop_kbn_not_equal = EnumMallShopKbn.ERS;
            siteCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listSite = siteRepository.Find(siteCriteria);

            var mallRepository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();

            var listMallList = new List<Item_tsv_mall_list>();
            foreach (var objSite in listSite)
            {
                var objMall = new Item_tsv_mall_list();
                objMall.site_id = objSite.id;
                objMall.mall_shop_kbn = objSite.mall_shop_kbn;
                objMall.site_name = objSite.site_name;
                listMallList.Add(objMall);
            }
            objMappable.listMallList = listMallList;
        }
    }
}