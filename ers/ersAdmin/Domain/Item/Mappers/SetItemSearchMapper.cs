﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;

namespace ersAdmin.Domain.Item.Mappers
{
    public class SetItemSearchMapper
        : IMapper<ISetItemSearchMappable>
    {

        public void Map(ISetItemSearchMappable objMappable)
        {
            Init(objMappable);
        }

        public virtual void Init(ISetItemSearchMappable objMappable)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

            var criteria = this.SetCriteria(objMappable);

            objMappable.recordCount = repository.GetRecordCountSkuBase(criteria);

            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

            var recList = new List<Dictionary<string, object>>();
            objMappable.setItemList = repository.FindSkuBaseItemList(criteria, null);
        }

        public virtual ErsSkuCriteria SetCriteria(ISetItemSearchMappable obj)
        {
            ErsSkuCriteria cri = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();

            if (obj.s_parent_scode.HasValue())
            {
                cri.scode = obj.s_parent_scode;
            }
            if (obj.s_child_scode.HasValue())
            {
                cri.HasSetChild(obj.s_child_scode);
            }

            cri.set_flg = EnumSetFlg.IsSet;
            
            return cri;
        }
    }
}