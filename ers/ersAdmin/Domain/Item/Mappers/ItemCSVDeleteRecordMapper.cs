﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Mappers
{
    public class ItemCSVDeleteRecordMapper
        : IMapper<IItemCSVDeleteRecordMappable>
    {
        public void Map(IItemCSVDeleteRecordMappable objMappable)
        {
            if (objMappable.IsValid)
                LoadDefaultDataWithScode(objMappable);
        }

        internal void LoadDefaultDataWithScode(IItemCSVDeleteRecordMappable objMappable)
        {
            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            groupCriteria.scode = objMappable.scode;

            var itemList = groupRepository.FindSkuBaseItemList(groupCriteria,null);

            if (itemList.Count > 0)
                objMappable.OverwriteWithParameter(itemList.First().GetPropertiesAsDictionary());
        }
    }
}