﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall;

namespace ersAdmin.Domain.Item.Mappers
{
    public class CateTreeViewMapper
        : IMapper<ICateTreeViewMappable>
    {
        public void Map(ICateTreeViewMappable objMappable)
        {
            if(!objMappable.multiple_sites)
            {
                objMappable.s_site_id = new[] {objMappable.config_site_id };
            }
            if (objMappable.s_site_id == null || objMappable.s_site_id.Length == 0)
            {
                var site_list = ErsMallFactory.ersMallViewServiceFactory.GetErsViewSiteService().SelectAsList();
                objMappable.s_site_id = new int[1] { Convert.ToInt32(site_list.First()["value"]) };
            }

            objMappable.MainList = GetCateTreeList(objMappable);
        }

        /// <summary>
        /// ツリー構造を返す
        /// </summary>
        /// <returns>List</returns>
        public virtual List<Dictionary<string, object>> GetCateTreeList(ICateTreeViewMappable objMappable)
        {
            var TreeList = new List<Dictionary<string, object>>();
            var site_id = objMappable.s_site_id.First();

            objMappable.cateList1 = getCategoryWithParentID(1, site_id);
            objMappable.cateList2 = getCategoryWithParentID(2, site_id);
            objMappable.cateList3 = getCategoryWithParentID(3, site_id);
            objMappable.cateList4 = getCategoryWithParentID(4, site_id);
            objMappable.cateList5 = getCategoryWithParentID(5, site_id);

            //カテゴリ１
            int catenum = 1;
            var tree_line = new Dictionary<string, object>();
            tree_line.Add("id", catenum);
            tree_line.Add("cate_name", ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().getCateName(catenum));
            tree_line.Add("active", (int)ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().getCateActive(catenum));

            var CateList = new List<Dictionary<string, object>>();
            var dic_catelist = new List<Dictionary<string, object>>();
            foreach (var list_lines in objMappable.cateList1)
            {
                int child_cate = catenum + 1;
                var tmp_line = new Dictionary<string, object>();
                tmp_line.Add("id", Convert.ToString(list_lines["id"]));
                tmp_line.Add("cate_name", Convert.ToString(list_lines["cate_name"]));
                tmp_line.Add("parent_id", "");
                tmp_line.Add("disp_order", Convert.ToString(list_lines["disp_order"]));
                tmp_line.Add("active", Convert.ToString(list_lines["active"]));
                dic_catelist = makedic(child_cate, Convert.ToString(list_lines["id"]), objMappable);

                tmp_line.Add("child_dic", dic_catelist);
                tmp_line.Add("child_cnt", dic_catelist.Count);

                CateList.Add(tmp_line);

            }
            tree_line.Add("child_dic", CateList);
            tree_line.Add("child_cnt", objMappable.cateList1.Count);
            TreeList.Add(tree_line);

            //カテゴリ２以降
            for (catenum = 2; catenum <= 5; catenum++)
            {
                tree_line = new Dictionary<string, object>();
                tree_line.Add("id", catenum);
                tree_line.Add("cate_name", ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().getCateName(catenum, site_id));
                tree_line.Add("active", (int)ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().getCateActive(catenum, site_id));
                CateList = new List<Dictionary<string, object>>();
                var NoParentList = new List<Dictionary<string, object>>();
                NoParentList = ErsFactory.ersMerchandiseFactory.GetSearchCategoryListSpec().SelectAsListNoParent(catenum, false, site_id);
                foreach (var list_lines in NoParentList)
                {
                    int child_cate = catenum + 1;
                    var tmp_line = new Dictionary<string, object>();
                    tmp_line.Add("id", Convert.ToString(list_lines["id"]));
                    tmp_line.Add("cate_name", Convert.ToString(list_lines["cate_name"]));
                    tmp_line.Add("parent_id", Convert.ToString(list_lines["parent_id"]));
                    tmp_line.Add("disp_order", Convert.ToString(list_lines["disp_order"]));
                    tmp_line.Add("active", Convert.ToString(list_lines["active"]));
                    if (child_cate <= 5)
                    {
                        dic_catelist = makedic(child_cate, Convert.ToString(list_lines["id"]), objMappable);
                    }
                    tmp_line.Add("child_dic", dic_catelist);
                    tmp_line.Add("child_cnt", dic_catelist.Count);

                    CateList.Add(tmp_line);

                }

                tree_line.Add("child_dic", CateList);
                tree_line.Add("child_cnt", NoParentList.Count);
                TreeList.Add(tree_line);
            }


            return TreeList;

        }

        private List<Dictionary<string, object>> makedic(int catenum, string parent_id, ICateTreeViewMappable objMappable)
        {
            var tmp_list = new List<Dictionary<string, object>>();
            var dic_catelist = new List<Dictionary<string, object>>();
            var cateList = new List<Dictionary<string, object>>();
            EnumActive? cate_kind_active = EnumActive.NonActive;
            switch (catenum)
            {
                case 1:
                    cateList = objMappable.cateList1;
                    cate_kind_active = objMappable.cate_kind1_active;
                    break;
                case 2:
                    cateList = objMappable.cateList2;
                    cate_kind_active = objMappable.cate_kind2_active;
                    break;
                case 3:
                    cateList = objMappable.cateList3;
                    cate_kind_active = objMappable.cate_kind3_active;
                    break;
                case 4:
                    cateList = objMappable.cateList4;
                    cate_kind_active = objMappable.cate_kind4_active;
                    break;
                case 5:
                    cateList = objMappable.cateList5;
                    cate_kind_active = objMappable.cate_kind5_active;
                    break;
            }


            foreach (var list_lines in cateList)
            {
                int child_cate = catenum + 1;
                if (parent_id == Convert.ToString(list_lines["parent_id"]))
                {
                    var tmp_line = new Dictionary<string, object>();

                    tmp_line.Add("id", Convert.ToString(list_lines["id"]));
                    tmp_line.Add("cate_name", Convert.ToString(list_lines["cate_name"]));
                    tmp_line.Add("parent_id", Convert.ToString(list_lines["parent_id"]));
                    tmp_line.Add("disp_order", Convert.ToString(list_lines["disp_order"]));
                    tmp_line.Add("active", Convert.ToString(list_lines["active"]));
                    if (child_cate <= 5)
                    {
                        dic_catelist = makedic(child_cate, Convert.ToString(list_lines["id"]), objMappable);
                    }
                    tmp_line.Add("child_dic", dic_catelist);
                    tmp_line.Add("child_cnt", dic_catelist.Count);
                    tmp_list.Add(tmp_line);
                }

            }
            return tmp_list;
        }

        /// <summary>
        /// Get Categories with Parent ID
        /// </summary>
        /// <param name="categoryNumber">Category Number</param>
        /// <returns>List</returns>
        private List<Dictionary<string, object>> getCategoryWithParentID(int categoryNumber, int site_id)
        {
            var list = ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().GetCachedList(categoryNumber, site_id);

            list = this.GetOnlyActiveRecord(list);

            return list;
        }

        public virtual List<Dictionary<string, object>> GetOnlyActiveRecord(List<Dictionary<string, object>> list)
        {
            var retList = new List<Dictionary<string, object>>();

            foreach (var row in list)
            {
                if (row.ContainsKey("active") && (EnumActive)Convert.ToInt32(row["active"]) == EnumActive.NonActive)
                {
                    continue;
                }
                retList.Add(row);
            }

            return retList;
        }
    }
}