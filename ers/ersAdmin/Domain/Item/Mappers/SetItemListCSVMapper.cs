﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.csv;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;

namespace ersAdmin.Domain.Item.Mappers
{
    public class SetItemListCSVMapper
        : IMapper<ISetItemListCSVMappable>
    {
        public void Map(ISetItemListCSVMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }

        /// <summary>
        /// CSVファイルを作成する。
        /// </summary>
        /// <returns></returns>
        public virtual void CreateCsvFile(ISetItemListCSVMappable objMappable)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseRepository();

            var criteria = this.SetCriteria(objMappable);

            objMappable.recordCount = repository.GetRecordCount(criteria);

            //ページ毎出力の時に使用
            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            }

            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

            var itemList = repository.Find(criteria);

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            objMappable.csvCreater = csvCreater;

            Set_Item_csv_record set_item_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                csvCreater.WriteCsvHeader<Set_Item_csv_record>(writer);
                foreach (var item in itemList)
                {
                    if (item.scode.HasValue())
                    {
                        set_item_csv = new Set_Item_csv_record();
                        set_item_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                        csvCreater.WriteBody(set_item_csv, writer);
                    }
                }
            }
        }

        private ErsSetMerchandiseCriteria SetCriteria(ISetItemListCSVMappable objMappable)
        {
            var cri = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseCriteria();

            if (objMappable.s_parent_scode.HasValue())
            {
                cri.parent_scode = objMappable.s_parent_scode;
            }
            if (objMappable.s_child_scode.HasValue())
            {
                cri.scode = objMappable.s_child_scode;
            }

            cri.parent_set_flg = EnumSetFlg.IsSet;

            return cri;
        }
    }
}