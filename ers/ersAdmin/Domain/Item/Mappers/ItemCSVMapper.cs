﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersAdmin.Domain.Item.Mappers
{
    public class ItemCSVMapper
        : IMapper<IItemCSVMappable>
    {
        public void Map(IItemCSVMappable objMappable)
        {
            var controller = objMappable.controller;
            foreach (var model in objMappable.csv_file.GetValidModels())
            {
                controller.mapperBus.Map<IItemCSVRecordMappable>(model);
                objMappable.csv_file.SaveValue(model);
            }
        }
    }
}