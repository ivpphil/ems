﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Mappers
{
    public class ItemModifyDetailListRecordMapper
        : IMapper<IItemModifyDetailListRecordMappable>
    {
        public void Map(IItemModifyDetailListRecordMappable objMappable)
        {
            var objSku = objMappable.objSku;
            objMappable.OverwriteWithParameter(objSku.GetPropertiesAsDictionary());

            var objPrice = this.GetPrice(objSku.scode);
            if (objPrice != null)
            {
                objMappable.OverwriteWithParameter(objPrice.GetPropertiesAsDictionary());
            }

            var objStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(objSku.scode);
            if (objStock != null)
            {
                objMappable.OverwriteWithParameter(objStock.GetPropertiesAsDictionary());
            }

            objMappable.old_scode = objMappable.scode;
            objMappable.old_stock = objMappable.stock;
        }

        private ErsMerchandise GetPrice(string scode)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
            criteria.s_master_scode = scode;
            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
            var listPrice = repository.FindPriceSkuBaseItemList(criteria);
            if (listPrice.Count != 1)
            {
                return null;
            }
            return listPrice.First();
        }
    }
}