﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.mall;
using ersAdmin.Models.item.mall.rakuten;

namespace ersAdmin.Domain.Item.Mappers
{
    public class ItemRakutenListCSVMapper
        : ItemListMapper, IMapper<IItemRakutenListCSVMappable>
    {
        public void Map(IItemRakutenListCSVMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }
        /// <summary>
        /// CSVファイルを作成する。
        /// </summary>
        /// <returns></returns>
        public virtual void CreateCsvFile(IItemRakutenListCSVMappable objMappable)
        {
            var mallRepository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();
            var mallCriteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();
            this.GetCriteria(objMappable, mallCriteria);
            mallCriteria.site_id = objMappable.download_site_id;

            var rakutenList = mallRepository.FindWithMerchandise(mallCriteria);

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            CsvRakutenItemRecord set_item_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<CsvRakutenItemRecord>(writer);
                foreach (var item in rakutenList)
                {
                    var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(item.scode, null);
                    set_item_csv = new CsvRakutenItemRecord();
                    set_item_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                    objMappable.csvCreater.WriteBody(set_item_csv, writer);
                }
            }
        }

    }
}