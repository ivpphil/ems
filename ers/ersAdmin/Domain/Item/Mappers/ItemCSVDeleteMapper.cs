﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Item.Mappables;

namespace ersAdmin.Domain.Item.Mappers
{
    public class ItemCSVDeleteMapper
        : IMapper<IItemCSVDeleteMappable>
    {
        public void Map(IItemCSVDeleteMappable objMappable)
        {
            objMappable.csv_file_validated = objMappable.csv_file.GetValidModels();
            SetValidModels(objMappable);
        }

        internal void SetValidModels(IItemCSVDeleteMappable objMappable)
        {
            foreach (var validModel in objMappable.csv_file_validated)
            {
                validModel.controller.mapperBus.Map<IItemCSVDeleteRecordMappable>(validModel);
                objMappable.csv_file.SaveValue(validModel);
            }
        }
    }
}