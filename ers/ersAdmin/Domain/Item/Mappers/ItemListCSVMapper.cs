﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Item.Mappers
{
    public class ItemListCSVMapper
        : ItemListMapper, IMapper<IItemListCSVMappable>
    {
        public void Map(IItemListCSVMappable objMappable)
        {
            CreateCsvFile(objMappable);
        }

        /// <summary>
        /// CSVファイルを作成する。
        /// </summary>
        /// <returns></returns>
        public void CreateCsvFile(IItemListCSVMappable objMappable)
        {
            //検索結果をクライテリアにセット
            ErsGroupRepository groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            this.GetCriteria(objMappable, criteria);

            criteria.scode_not_equal = null;

            //ページ毎出力の時に使用
            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            }

            criteria.SetOrderByGcode(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

            var itemList = groupRepository.FindSkuBaseItemList(criteria, null);

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            ersAdmin.Models.csv.Item_csv_record item_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<ersAdmin.Models.csv.Item_csv_record>(writer);
                foreach (var item in itemList)
                {
                    item_csv = new ersAdmin.Models.csv.Item_csv_record();
                    item_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());

                    // 出力用カテゴリセット [Set category for output]
                    item_csv.cate1 = this.GetCategoryForOutput(item.cate1);
                    item_csv.cate2 = this.GetCategoryForOutput(item.cate2);
                    item_csv.cate3 = this.GetCategoryForOutput(item.cate3);
                    item_csv.cate4 = this.GetCategoryForOutput(item.cate4);
                    item_csv.cate5 = this.GetCategoryForOutput(item.cate5);

                    objMappable.csvCreater.WriteBody(item_csv, writer);
                }
            }
        }

        /// <summary>
        /// 出力用カテゴリ取得
        /// </summary>
        /// <param name="cate">int[]</param>
        /// <returns>string</returns>
        private string GetCategoryForOutput(int[] cate)
        {
            if (cate == null || cate.Length == 0)
            {
                return string.Empty;
            }
            return string.Join("$", cate);
        }
    }
}