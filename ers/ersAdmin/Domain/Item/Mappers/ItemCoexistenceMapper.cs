﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using ersAdmin.Models;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Mappers
{
    public class ItemCoexistenceMapper
        : IMapper<IItemCoexistenceMappable>
    {
        public void Map(IItemCoexistenceMappable objMappable)
        {
            getMixedGroupData(objMappable);
        }


        /// <summary>
        /// 混在グループデータセット
        /// </summary>
        internal void getMixedGroupData(IItemCoexistenceMappable objMappable)
        {
            var repository = ErsFactory.ersBasketFactory.GetErsMixGroupRepository();

            var criteria = ErsFactory.ersBasketFactory.GetErsMixGroupCriteria();
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            var list = repository.Find(criteria);

            objMappable.mixedGroupList = new List<Item_coexistence_detail>();
            foreach (var item in list)
            {
                var mixedGroup = new Item_coexistence_detail();
                mixedGroup.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                objMappable.mixedGroupList.Add(mixedGroup);
            }

            //追加用の空のフィールド追加
            for (var i = 0; i < 5; i++)
            {
                var mixedGroup = new Item_coexistence_detail();
               objMappable.mixedGroupList.Add(mixedGroup);
            }
        }
    }
}