﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall;
using ersAdmin.Models.item.mall.amazon.health;

namespace ersAdmin.Domain.Item.Mappers
{
    public class ItemAmazonListCSVMapper
        : ItemListMapper, IMapper<IItemAmazonListCSVMappable>
    {
        public void Map(IItemAmazonListCSVMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }
        /// <summary>
        /// CSVファイルを作成する。
        /// </summary>
        /// <returns></returns>
        public virtual void CreateCsvFile(IItemAmazonListCSVMappable objMappable)
        {
            var mallRepository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();
            var mallCriteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();
            this.GetCriteria(objMappable, mallCriteria);
            mallCriteria.site_id = objMappable.download_site_id;

            var amazonList = mallRepository.FindWithMerchandise(mallCriteria);

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";


            CsvAmazonItemRecord set_item_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<CsvAmazonItemRecord>(writer);
                foreach (var item in amazonList)
                {
                    var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(item.scode, null);
                    set_item_csv = new CsvAmazonItemRecord();
                    set_item_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                    objMappable.csvCreater.WriteBody(set_item_csv, writer);
                }
            }
        }
        
    }
}