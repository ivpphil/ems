﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.merchandise.category;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using ersAdmin.Models.item;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Item.Mappers
{
    public class CateRegistMapper
        : SiteRegisterBaseMapper, IMapper<ICateRegistMappable>
    {
        public void Map(ICateRegistMappable objMappable)
        {
            if (!objMappable.multiple_sites)
            {
                objMappable.site_id = objMappable.config_site_id;
            }
            if (objMappable.registCategoryItem)
            {
                SetCateItemParent(objMappable);
                LoadCategoryHeaderData(objMappable);
                return;
            }

            if (IsCate_edit_Cliecked(objMappable))
                LoadCateBodyList(objMappable);

            else
            {
                if (objMappable.registCategory)
                    return;

                LoadCategoryHeaderData(objMappable);
            }
            
        }


        private void SetCateItemParent(ICateRegistMappable objMappable)
        {
            if (objMappable.categoryNumber == null)
                return;

            var listParent = new List<Dictionary<string, object>>();
            listParent = null;
            //親カテゴリの設定
            if (objMappable.categoryNumber > ErsCategory.minCategoryNumber)
            {
                int parentcategoryNumber = objMappable.categoryNumber.Value - 1;
                listParent = ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().GetCategory(parentcategoryNumber, null, Convert.ToInt32(objMappable.site_id));
                foreach (var cate_body in objMappable.cate_body_table)
                {
                    if (listParent != null)
                    {
                        cate_body.listParent = listParent;
                        cate_body.hasParent = true;
                    }

                }
            }
        }

        /// <summary>
        /// load category header
        /// </summary>
        private void LoadCategoryHeaderData(ICateRegistMappable objMappable)
        {
            // 初期サイトIDセット [Set default site ID]
            this.SetDefaultSiteId(objMappable);

            var es = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(objMappable.site_id));

            var headerList = new List<Cate_header>();
            for (int categoryNumber = ErsCategory.minCategoryNumber; categoryNumber <= ErsCategory.maxCategoryNumber; categoryNumber++)
            {
                var header = new Cate_header();
                header.categoryNumber = categoryNumber;
                header.cate_header_name = (string)ErsReflection.GetProperty(es, "cate" + categoryNumber).GetValue(es, null);
                header.cate_header_active = (EnumActive)ErsReflection.GetProperty(es, "cate" + categoryNumber + "_active").GetValue(es, null);
                headerList.Add(header);
            }
            objMappable.cate_header_table = headerList;
        }

        /// <summary>
        /// カテゴリアイテムの表示用リスト
        /// </summary>
        public void LoadCateBodyList(ICateRegistMappable objMappable)
        {
            if (objMappable.cate_edit_1)
                objMappable.categoryNumber = 1;
            else if (objMappable.cate_edit_2)
                objMappable.categoryNumber = 2;
            else if (objMappable.cate_edit_3)
                objMappable.categoryNumber = 3;
            else if (objMappable.cate_edit_4)
                objMappable.categoryNumber = 4;
            else
                objMappable.categoryNumber = 5;

            objMappable.cate_body_table = GetCateItem(objMappable.categoryNumber.Value, Convert.ToInt32(objMappable.site_id));
            SetCateItemParent(objMappable);

        }

        private static List<Cate_item> GetCateItem(int categoryNumber, int site_id)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsCategoryRepository(categoryNumber);
            var c = ErsFactory.ersMerchandiseFactory.GetErsCategoryCriteria(categoryNumber);
            c.site_id = site_id;
            c.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
            c.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            var categories = repository.Find(c);

            var catelist = new List<Cate_item>();
            foreach (var category in categories)
            {
                var cateItem = new Cate_item();
                cateItem.key = category.id;
                cateItem.id = category.id;
                cateItem.cate_name = category.cate_name;
                cateItem.parent_id = category.parent_id;
                cateItem.active = category.active;
                cateItem.disp_order = category.disp_order;


                catelist.Add(cateItem);
            }

            //新規登録用に、５つの空のカテゴリを足しておく
            for (var i = 1; i <= 5; i++)
            {
                var cateItem = new Cate_item();
                cateItem.key = -1 * i;

                catelist.Add(cateItem);
            }
            return catelist;
        }

        internal bool IsCate_edit_Cliecked(ICateRegistMappable objMappable)
        {
            return (objMappable.cate_edit_1 || objMappable.cate_edit_2 || objMappable.cate_edit_3 || objMappable.cate_edit_4 || objMappable.cate_edit_5);
        }
    }
}