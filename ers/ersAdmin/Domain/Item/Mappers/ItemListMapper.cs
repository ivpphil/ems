﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Item.Mappables;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Item.Mappers
{
    public class ItemListMapper
        : SiteSearchBaseMapper, IMapper<IItemListMappable>
    {
        public void Map(IItemListMappable objMappable)
        {
            this.LoadMallList(objMappable);
            if (!objMappable.isInitilize)
            {
                this.LoadList(objMappable);
            }
        }

        private void LoadMallList(IItemListMappable objMappable)
        {
            //サイトIDの一覧を取得
            var siteRepository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var siteCriteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
            siteCriteria.active = EnumActive.Active;
            siteCriteria.mall_shop_kbn_not_equal = EnumMallShopKbn.ERS;
            siteCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            objMappable.listMallList = siteRepository.Find(siteCriteria);
        }

        /// <summary>
        /// itemリストを初期化する。
        /// </summary>
        /// <param name="pager"></param>
        private void LoadList(IItemListMappable objMappable)
        {
            ErsGroupRepository groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

            //検索条件をクライテリアに保存
            var emCri = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            this.GetCriteria(objMappable, emCri);

            emCri.scode_not_equal = null;

            //検索結果の総数を取得
            objMappable.recordCount = groupRepository.GetRecordCountGroupBase(emCri);

            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            //検索SQLにLIMIT と OFFSETを加える
            objMappable.pager.SetLimitAndOffsetToCriteria(emCri);

            emCri.SetOrderByGcode(Criteria.OrderBy.ORDER_BY_ASC);

            //商品出力結果
            var list = groupRepository.FindGroupBaseItemList(emCri);

            objMappable.MeList = ErsCommon.ConvertEntityListToDictionaryList(list);

        }


        /// <summary>
        /// 検索条件をクライテリアにセット
        /// </summary>
        /// <returns></returns>
        protected void GetCriteria(IItemListMappable objMappable, IItemSearchCriteria emCri)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //除外するgcode
            emCri.ignore_gcode = setup.IgnoreGcode;

            //クライテリアにパラメタを渡す
            if (!string.IsNullOrEmpty(objMappable.s_gcode))
                emCri.gcode = objMappable.s_gcode;

            if (!string.IsNullOrEmpty(objMappable.s_jancode))
                emCri.jancode = objMappable.s_jancode;

            if (!string.IsNullOrEmpty(objMappable.s_scode))
                emCri.scode = objMappable.s_scode;

            if (!string.IsNullOrEmpty(objMappable.s_sname))
                emCri.sname_and_gname = objMappable.s_sname;

            if (objMappable.s_cate1 != null)
                emCri.cate1 = objMappable.s_cate1.Value;

            if (objMappable.s_cate2 != null)
                emCri.cate2 = objMappable.s_cate2.Value;

            if (objMappable.s_cate3 != null)
                emCri.cate3 = objMappable.s_cate3.Value;

            if (objMappable.s_cate4 != null)
                emCri.cate4 = objMappable.s_cate4.Value;

            if (objMappable.s_cate5 != null)
                emCri.cate5 = objMappable.s_cate5.Value;

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, (Criteria)emCri, "g_master_t");

            if (objMappable.s_site_id_mall != null && objMappable.s_site_id_mall.Length > 0)
            {
                var criteriaList = new List<Criteria>();

                foreach (int site_id in objMappable.s_site_id_mall)
                {
                    var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();
                    criteria.mall_site_id(site_id);
                    criteriaList.Add(criteria);
                }

                emCri.Add(Criteria.JoinWithOR(criteriaList));
            }
        }
    }
}