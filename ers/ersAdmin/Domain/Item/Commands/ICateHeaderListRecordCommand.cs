﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Commands
{
    public interface ICateHeaderListRecordCommand
        : ICommand
    {
        EnumActive? cate_header_active { get; set; }
    }
}