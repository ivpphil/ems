﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Item.Commands
{
    public interface ISetItemCSVCommand
        : ICommand
    {
        ErsCsvContainer<ersAdmin.Models.csv.Set_Item_csv_record> csv_file { get; }

        bool chk_find { get; }

        bool regist { get; }
    }
}