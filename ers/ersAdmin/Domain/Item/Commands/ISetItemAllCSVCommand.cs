﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.csv;
using jp.co.ivp.ers.mvc;
namespace ersAdmin.Domain.Item.Commands
{
    public interface ISetItemAllCSVCommand : ICommand
    {
        ErsCsvContainer<Set_Item_csv_record> csv_file { get; }

        bool chk_find { get; }

        bool regist { get; }
    }
}