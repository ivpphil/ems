﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Item.Commands
{
    public interface IItemCSVCommand
        : ICommand
    {
        ErsCsvContainer<ersAdmin.Models.csv.Item_csv_record> csv_file { get; set; }

        bool chk_find { get; set; }

        bool regist { get; set; }

        List<string> listScode { get; set; }

        List<string> listJancode { get; set; }

        List<string> listAttribute { get; set; }
    }
}