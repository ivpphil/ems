﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using ersAdmin.Models;
using ersAdmin.Models.item.mall.yahoo;
using ersAdmin.Models.item.mall.rakuten;
using ersAdmin.Models.item.mall.amazon.health;
using ersAdmin.Models.item.mall;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Item.Commands
{
    public interface IItemModifyCommand
        : ISiteRegisterBaseCommand, ICommand
    {
        bool item_group_modify_btn { get; set; }

        bool item_group_delete_btn { get; set; }

        string gcode { get; set; }

        string old_gcode { get; set; }

        DateTime? date_from { get; set; }

        DateTime? date_to { get; set; }

        EnumStockFlg? stock_flg { get; set; }

        int? stock_set1 { get; set; }

        int? stock_set2 { get; set; }

        EnumSalePatternType? s_sale_ptn { get; set; }

        List<Item_modify_detail> detail_table { get; set; }

        List<Item_modify_mall_detail_list> listMallList { get; set; }

        string returnUrl { get; set; }

        HttpPostedFileBase group_simg { get; set; }

        int disp_send_ptn_month_intervals { get; set; }

        int disp_send_ptn_week_intervals { get; set; }

        int disp_send_ptn_month_day_intervals { get; set; }

        string[] disp_keyword { get; set; }

        IList<group_simg_detail> group_simg_detail { get; }

        string group_simg_temp_folder { get; }

        string image_identifier { get; }

        EnumDelvMethod? deliv_method { get; set; }

        bool disabled_file_uploader { get; }

        bool deliv_method_flg { get; }
    }
}