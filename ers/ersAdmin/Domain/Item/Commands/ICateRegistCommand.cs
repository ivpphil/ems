﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.item;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Item.Commands
{
    public interface ICateRegistCommand
        : ISiteRegisterBaseCommand, ICommand
    {
        List<Cate_header> cate_header_table { get; }

        List<Cate_item> cate_body_table { get; }

        bool registCategoryItem { get; }

        bool registCategory { get; }

        int? categoryNumber { get; }
    }
}