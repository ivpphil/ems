﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Item.Commands
{
    public interface IItemCoexistenceDetailListRecordCommand
        : ICommand
    {
        string mixed_group_code { get; }

        string mixed_group_name { get; }

        short? delete { get; set; }
    }
}