﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Item.Commands
{
    public interface IChildScodeListCommand : ICommand
    {
        int? id { get; set; }

        string scode { get; set; }

        string sname { get; set; }

        int? amount { get; set; }
    }
}
