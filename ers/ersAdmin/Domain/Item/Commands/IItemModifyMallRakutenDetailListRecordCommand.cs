﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Commands
{
    public interface IItemModifyMallRakutenDetailListRecordCommand
        : ICommand
    {
        string scode { get; set; }

        EnumOnOff mall_flg { get; set; }
    }
}