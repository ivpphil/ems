﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Item.Commands
{
    public interface IItemCSVDeleteRecordCommand
        : ICommand
    {
        string scode { get; }

    }
}