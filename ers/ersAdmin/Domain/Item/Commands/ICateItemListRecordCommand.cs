﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Commands
{
    public interface ICateItemListRecordCommand
        : ICommand
    {
        int? key { get; }

        int? id { get; }

        EnumActive? active { get; set; }

        string cate_name { get; }

        int? parent_id { get; }

        int? delete { get; }
    }
}