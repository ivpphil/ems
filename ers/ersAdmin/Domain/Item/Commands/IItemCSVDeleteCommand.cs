﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Item.Commands
{
    public interface IItemCSVDeleteCommand
        : ICommand
    {
        ErsCsvContainer<ersAdmin.Models.csv.Item_Csv_Delete_record> csv_file { get; }

        bool chk_find { get; }

        bool regist { get; }

        IEnumerable<ersAdmin.Models.csv.Item_Csv_Delete_record> csv_file_validated { get; }
    }
}