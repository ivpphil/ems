﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models;

namespace ersAdmin.Domain.Item.Commands
{
    public interface IItemCoexistenceCommand
        : ICommand
    {
        List<Item_coexistence_detail> mixedGroupList { get; }
    }
}