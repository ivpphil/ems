﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc.pager;
using ersAdmin.Models.item;

namespace ersAdmin.Domain.Item.Commands
{
    public interface ISetItemModifyCommand : ICommand
    {
        long recordCount { get; set; }

        ErsPagerModel pager { get; }

        bool LoadData { get; set; }

        int? id { get; set; }

        string parent_scode { get; set; }

        string parent_sname { get; set; }

        int? price { get; set; }

        int? regular_price { get; set; }

        int? regular_first_price { get; set; }

        bool delete { get; set; }

        List<ChildScodeListData> ChildScodeList { get; set; }
    }
}
