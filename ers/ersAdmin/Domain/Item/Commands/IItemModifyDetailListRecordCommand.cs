﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using ersAdmin.Models.item;

namespace ersAdmin.Domain.Item.Commands
{
    public interface IItemModifyDetailListRecordCommand
        : ICommand
    {
        bool IsEmpty();

        string s_gcode { get; set; }

        int? campaign_point { get; set; }

        EnumSoldoutFlg? soldout_flg { get; set; }

        EnumActive? active { get; set; }

        int? disp_order { get; set; }

        int? max_purchase_count { get; set; }

        int? point { get; set; }

        DateTime? point_campaign_to { get; set; }

        DateTime? point_campaign_from { get; set; }

        bool delete { get; set; }

        string old_scode { get; set; }

        IList<item_bimg_detail> item_bimg_detail { get; }
    }
}