﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Commands
{
    public interface IItemCSVRecordCommand
        : ICommand
    {
        string scode { get; set; }

        string jancode { get; set; }

        string gcode { get; set; }

        string attribute2 { get; set; }

        string attribute1 { get; set; }

        DateTime? date_from { get; set; }

        DateTime? date_to { get; set; }

        EnumStockFlg? stock_flg { get; set; }

        int? stock_set1 { get; set; }

        int? stock_set2 { get; set; }

        int? campaign_point { get; set; }

        DateTime? point_campaign_from { get; set; }

        DateTime? point_campaign_to { get; set; }

        EnumSalePatternType? s_sale_ptn { get; set; }

        int? regular_price { get; set; }

        EnumDelvMethod? deliv_method { get; set; }

        int? max_purchase_count { get; set; }

        string disp_send_ptn { get; set; }

        string cate1 { get; set; }

        string cate2 { get; set; }

        string cate3 { get; set; }

        string cate4 { get; set; }

        string cate5 { get; set; }
    }
}