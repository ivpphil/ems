﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models;
using ersAdmin.Models.item.mall.rakuten;
using ersAdmin.Models.item.mall.yahoo;
using ersAdmin.Models.item.mall.amazon.health;
using ersAdmin.Models.item.tsv;

namespace ersAdmin.Domain.Item.Commands
{
    public interface IMallCSVCommand
        : ICommand
    {
        ErsCsvContainer<CsvRakutenItemRecord> tsv_rakuten { get; set; }

        ErsCsvContainer<CsvYahooItemRecord> tsv_yahoo { get; set; }

        ErsCsvContainer<CsvAmazonItemRecord> tsv_amazon { get; set; }

        bool chk_find { get; }

        int? site_id { get; set; }

        EnumMallShopKbn? mall_shop_kbn { get; }

        bool regist { get; set; }

        List<Item_tsv_mall_list> listMallList { get; set; }
    }
}