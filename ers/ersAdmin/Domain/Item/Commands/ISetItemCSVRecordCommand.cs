﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Item.Commands
{
    public interface ISetItemCSVRecordCommand
        : ICommand
    {
        string parent_scode { get; }

        string scode { get; }
    }
}