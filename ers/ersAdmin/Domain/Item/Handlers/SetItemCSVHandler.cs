﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Handlers
{
    public class SetItemCSVHandler
        : ICommandHandler<ISetItemCSVCommand>
    {
        public ICommandResult Submit(ISetItemCSVCommand command)
        {
            Update(command);
            return new CommandResult(true);
        }

        private void Update(ISetItemCSVCommand command)
        {

            var repository = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseRepository();
            foreach (var item in command.csv_file.GetValidModels())
            {
                //親コードのレコードは一度削除する
                var set_master_criteria = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseCriteria();
                set_master_criteria.parent_scode = item.parent_scode;
                DeleteSetMerchandise(repository, item);
            }
            //レコード追加
            foreach (var item in command.csv_file.GetValidModels())
            {
                //追加
                InsertSetMerchandise(repository, item);
            }
        }

        /// <summary>
        /// Deleteする
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="item"></param>
        protected void DeleteSetMerchandise(ErsSetMerchandiseRepository repository, ersAdmin.Models.csv.Set_Item_csv_record item)
        {
            var setmerchandise = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseWithModel(item);
            ErsSetMerchandiseCriteria criteria = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseCriteria();
            criteria.parent_scode = setmerchandise.parent_scode;
            repository.Delete(criteria);
        }

        /// <summary>
        /// Insertする
        /// </summary>
        /// <param name="setupRepository"></param>
        /// <param name="item"></param>
        protected void InsertSetMerchandise(ErsSetMerchandiseRepository repository, ersAdmin.Models.csv.Set_Item_csv_record item)
        {
            var setmerchandise = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseWithModel(item);
            repository.Insert(setmerchandise);
        }
    }
}