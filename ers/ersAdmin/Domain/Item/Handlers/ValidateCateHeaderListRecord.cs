﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateCateHeaderListRecord
        : IValidationHandler<ICateHeaderListRecordCommand>
    {

        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ICateHeaderListRecordCommand command)
        {
            yield return command.CheckRequired("categoryNumber");
            yield return command.CheckRequired("cate_header_name");

            if (command.cate_header_active == null)
                command.cate_header_active = EnumActive.NonActive;
        }
    }
}