﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.util;
using System.IO;
using ersAdmin.Domain.SiteBase.Handlers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.site;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ItemRegistHandler
        : SiteRegisterBaseHandler, ICommandHandler<IItemRegistCommand>
    {
        public ICommandResult Submit(IItemRegistCommand command)
        {
            this.InsertMerchandiseGroup(command);

            //save picture
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var temp_folder = setup.image_temp_directory + command.group_simg_temp_folder + "\\";
            if (command.group_simg_detail != null)
            {
                if (command.group_simg_detail.Count > 0)
                {
                    var file = command.group_simg_detail.FirstOrDefault();
                    var file_temp = file.file_identifier + file.group_simg;

                    if (setup.Multiple_sites)
                    {
                        var sites = SetSiteList();
                        foreach (var site in sites)
                        {
                            CopyTempImages(Path.Combine(temp_folder, file_temp), Path.Combine(setup.multiple_image_directory((int)site.id) + "simg", "G" + command.gcode + "_01.jpg"));
                        }
                    }
                    else
                    {
                        CopyTempImages(Path.Combine(temp_folder, file_temp), Path.Combine(setup.image_directory + "simg", "G" + command.gcode + "_01.jpg"));
                    }

                }
            }

            this.DeleteTempFiles(command.image_identifier, temp_folder);

            return new CommandResult(true);
        }

        internal void InsertMerchandiseGroup(IItemRegistCommand command)
        {
            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

            var objGroup = ErsFactory.ersMerchandiseFactory.GetErsGroup();

            this.SetProperties(command, objGroup);

            var keywordConstracter = ErsFactory.ersMerchandiseFactory.GetProductKeywordConstracterStgy();
            objGroup.keyword = ErsKanaConverter.String_conversion(keywordConstracter.CreateGroupKeyword(objGroup));

            groupRepository.Insert(objGroup);
        }

        private void SetProperties(IItemRegistCommand command, ErsGroup objGroup)
        {
            objGroup.OverwriteWithParameter(command.GetPropertiesAsDictionary());
            objGroup.active = objGroup.active ?? EnumActive.NonActive;
            objGroup.disp_list_flg = objGroup.disp_list_flg ?? EnumDisp_list_flg.Invisible;
            objGroup.site_id = this.GetArrayOfSiteId(command);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageField"></param>
        /// <param name="directoryPath"></param>
        /// <param name="fileName"></param>
        private void SaveImage(HttpPostedFileBase imageField, string directoryPath, string fileName)
        {
            if (imageField != null)
            {
                imageField.SaveAs(directoryPath + "\\" + fileName);
            }
        }

        private void CopyTempImages(string tempFile, string destinationFile, bool overwrite = true)
        {
            if (File.Exists(tempFile))
            {
                File.Copy(tempFile, destinationFile, overwrite);
                File.Delete(tempFile);
            }
        }

        private void DeleteTempFiles(string identifier, string temp_path)
        {
            var temp_files = System.IO.Directory.GetFiles(temp_path, "*" + identifier + "*");

            foreach (var file in temp_files)
            {
                if (File.Exists(file))
                    File.Delete(file);
            }
        }

        /// <summary>
        /// サイトリストセット [Set site list]
        /// </summary>
        /// <param name="mappable">マッパブル [Mappable]</param>
        protected IList<ErsSite> SetSiteList()
        {
            var repository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var criteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();

            criteria.mall_shop_kbn = EnumMallShopKbn.ERS;
            criteria.SetActiveOnly();

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            return repository.Find(criteria);
        }
    }
}