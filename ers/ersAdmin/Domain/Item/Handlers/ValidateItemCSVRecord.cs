﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.merchandise;
using System.Text.RegularExpressions;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateItemCSVRecord
        : IValidationHandler<IItemCSVRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IItemCSVRecordCommand command)
        {
            foreach (var result in this.ValidateRecord(command))
            {
                yield return result;
            }

            foreach (var result in this.ValidateDupulication(command))
            {
                yield return result;
            }
        }

        private IEnumerable<ValidationResult> ValidateRecord(IItemCSVRecordCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            yield return command.CheckRequired("gcode");
            yield return command.CheckRequired("gname");
            yield return command.CheckRequired("s_sale_ptn");

            yield return command.CheckRequired("date_from");
            yield return command.CheckRequired("date_to");
            foreach (var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDateTime("date_from", command.date_from, "date_to", command.date_to))
            {
                yield return result;
            }

            yield return command.CheckRequired("stock_flg");
            if (command.stock_flg == EnumStockFlg.DisplaySymbol)
            {
                yield return command.CheckRequired("stock_set1");
                yield return command.CheckRequired("stock_set2");
            }
            foreach (var result in ErsFactory.ersMerchandiseFactory.GetCheckStock_flgStgy().Check(command.stock_flg, command.stock_set1, command.stock_set2))
            {
                yield return result;
            }

            yield return command.CheckRequired("sname");
            yield return command.CheckRequired("scode");
            yield return command.CheckRequired("stock");
            yield return command.CheckRequired("soldout_flg");
            yield return command.CheckRequired("disp_list_flg");
            yield return command.CheckRequired("active");
            yield return command.CheckRequired("s_active");

            yield return command.CheckRequired("carriage_cost_type");
            yield return command.CheckRequired("plural_order_type");
            yield return command.CheckRequired("set_flg");
            yield return command.CheckRequired("doc_bundling_flg");
            yield return command.CheckRequired("deliv_method");

            if ((command.campaign_point != null && command.campaign_point != 0) || command.point_campaign_from != null || command.point_campaign_to != null)
            {
                yield return command.CheckRequired("point_campaign_from");
                yield return command.CheckRequired("point_campaign_to");
                foreach (var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDateTime("point_campaign_from", command.point_campaign_from, "point_campaign_to", command.point_campaign_to))
                {
                    yield return result;
                }

                yield return command.CheckRequired("campaign_point");
            }

            foreach (var result in ErsFactory.ersMerchandiseFactory.GetCheckDuplicateMerchandiseStgy().CheckDuplicate(command.gcode, command.scode, command.jancode))
            {
                yield return result;
            }

            if (command.s_sale_ptn != null)
            {
                if (command.s_sale_ptn == EnumSalePatternType.NORMAL     //通常
                    || command.s_sale_ptn == EnumSalePatternType.ALL)     //通常と定期
                {
                    yield return command.CheckRequired("price");
                }

                if (command.s_sale_ptn == EnumSalePatternType.REGULAR     //定期
                    || command.s_sale_ptn == EnumSalePatternType.ALL)     //通常と定期
                {
                    yield return command.CheckRequired("regular_price");
                }
            }

            if (command.regular_price != null)
            {
                yield return command.CheckRequired("disp_send_ptn");
                if(command.IsValidField("disp_send_ptn"))
                {
                    yield return ErsFactory.ersMerchandiseFactory.GetValidateDispSendPtnStgy().Validate(command.disp_send_ptn);
                }
            }

            yield return ErsMallFactory.ersMallProductFactory.GetCheckMallFlgStgy().CheckGroupAndSku(command.gcode, command.scode, command.s_sale_ptn);
        }

        /// <summary>
        /// CheckDupulication
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> ValidateDupulication(IItemCSVRecordCommand command)
        {
            var parentModel = (IItemCSVCommand)command.containerModel;

            string checkScodeKey = command.scode;
            string checkJancodeKey = command.jancode;
            string checkAttributeKey = command.gcode + command.attribute2 + command.attribute1;

            ErsSkuRepository repository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.gcode = command.gcode;
            var s_master_t_list = repository.Find(criteria);
            var jancode_list = new List<string>();
            var attribute_list = new List<string>();

            foreach (var list in s_master_t_list)
            {
                if (list.scode != checkScodeKey)
                {
                    if (list.jancode != null)
                    {
                        jancode_list.Add(list.jancode);
                    }
                    if (list.attribute1 != null || list.attribute2 != null)
                    {
                        attribute_list.Add(list.gcode + list.attribute2 + list.attribute1);
                    }
                }
            }

            if (!string.IsNullOrEmpty(checkScodeKey) && parentModel.listScode.Contains(checkScodeKey))
            {
                yield return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("scode"), checkScodeKey), new[] { "scode" });
            }
            parentModel.listScode.Add(checkScodeKey);

            if (!string.IsNullOrEmpty(checkJancodeKey) && (parentModel.listJancode.Contains(checkJancodeKey) || jancode_list.Contains(checkJancodeKey)))
            {
                yield return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("jancode"), checkJancodeKey), new[] { "jancode" });
            }
            parentModel.listJancode.Add(checkJancodeKey);

            if (parentModel.listAttribute.Contains(checkAttributeKey) || attribute_list.Contains(checkAttributeKey))
            {
                yield return new ValidationResult(ErsResources.GetMessage("10105", command.scode, ErsResources.GetFieldName("attribute2"), ErsResources.GetFieldName("attribute1")), new[] { "attribute2", "attribute1" });
            }
            parentModel.listAttribute.Add(checkAttributeKey);
        }
    }
}