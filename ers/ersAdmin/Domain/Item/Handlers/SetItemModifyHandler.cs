﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using ersAdmin.Models.item;

namespace ersAdmin.Domain.Item.Handlers
{
    public class SetItemModifyHandler : ICommandHandler<ISetItemModifyCommand>
    {
        public ICommandResult Submit(ISetItemModifyCommand command)
        {
            this.UpdateSetItem(command);
            return new CommandResult(true);
        }

        private void UpdateSetItem(ISetItemModifyCommand command)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseRepository();

            foreach (var item in command.ChildScodeList)
            {
                if (item.deleted)
                {
                    if (item.id.HasValue)
                    {
                        this.DeleteContents(item);
                    }
                }
                else if (item.id.HasValue)
                {
                    this.UpdateContents(item);
                }
                else
                {
                    this.InsertContents(command, item);
                }
            }
        }

        private void InsertContents(ISetItemModifyCommand command, ChildScodeListData item)
        {
            var scodeListRepo = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseRepository();
            var scoderecord = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandise();
            scoderecord.OverwriteWithParameter(item.GetPropertiesAsDictionary());
            scoderecord.parent_scode = command.parent_scode;
            scodeListRepo.Insert(scoderecord);
        }

        private void DeleteContents(ChildScodeListData item)
        {
            var scodeListRepo = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseRepository();
            var scoderecord = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseWithID(item.id);
            scodeListRepo.Delete(scoderecord);
        }

        internal void UpdateContents(IChildScodeListCommand item)
        {
            var scodeListRepo = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseRepository();
            var newscoderecord = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseWithID(item.id);
            var oldscoderecord = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseWithParameters(newscoderecord.GetPropertiesAsDictionary());
            newscoderecord.amount = item.amount;

            scodeListRepo.Update(oldscoderecord, newscoderecord);
        }
    }
}