﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.mall.product.strategy;
using jp.co.ivp.ers.mall.site;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ItemCSVDeleteHandler
        : ICommandHandler<IItemCSVDeleteCommand>
    {
        public ICommandResult Submit(IItemCSVDeleteCommand command)
        {
            this.Delete(command);
            return new CommandResult(true);
        }

        private void Delete(IItemCSVDeleteCommand command)
        {
            //サイトIDの一覧を取得
            var listSite = this.GetListSite();

            var listHarcParam = new List<HarcProductTmp>();
            foreach (var csv in command.csv_file_validated)
            {
                var objSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(csv.scode);
                var objPrice = ErsFactory.ersMerchandiseFactory.GetErsDefaultPriceWithScode(csv.scode);

                //s_master_t
                var repo_sku = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
                repo_sku.Delete(objSku);

                //stock_t
                var repo_stock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockRepository();
                var cri_stock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockCriteria();
                cri_stock.scode = csv.scode;
                repo_stock.Delete(cri_stock);

                //wh_stock_t
                var repo_wh_stock = ErsFactory.ersWarehouseFactory.GetErsWhStockRepository();
                var cri_wh_stock = ErsFactory.ersWarehouseFactory.GetErsWhStockCriteria();
                cri_wh_stock.scode = csv.scode;
                repo_wh_stock.Delete(cri_wh_stock);

                //price_t
                var repo_price = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();
                var cri_price = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
                cri_price.scode = csv.scode;
                repo_price.Delete(cri_price);

                //モール更新 [Update Mall Merchandise]
                this.DeleteMallData(csv.scode);

                //グループの削除
                this.DeleteIsolatedGcode(objSku.gcode);

                if (objPrice != null)
                {
                    //harc更新
                    listHarcParam.Add(this.GetErsHarcProductTmp(objSku.scode, objSku.sname, objPrice.price, null, EnumMallShopKbn.ERS, EnumActive.NonActive));
                    foreach (var objSite in listSite)
                    {
                        listHarcParam.Add(this.GetErsHarcProductTmp(objSku.scode, objSku.sname, objPrice.price, objSite.id, objSite.mall_shop_kbn, EnumActive.NonActive));
                    }
                }
            }

            // Harc商品更新指示 [Update mall item]
            if (listHarcParam.Count > 0)
            {
                this.UpdateHarc(listHarcParam);
            }
        }

        private void DeleteIsolatedGcode(string gcode)
        {
            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.gcode = gcode;

            var registKeywordStgy = ErsFactory.ersMerchandiseFactory.GetRegistKeywordStgy();

            //When a group has no remaining item, then do deleting
            if (skuRepository.GetRecordCount(criteria) == 0)
            {
                var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
                var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
                groupCriteria.gcode = gcode;
                groupRepository.Delete(groupCriteria);

                registKeywordStgy.Delete(gcode, EnumKeywordType.Group);
            }

            registKeywordStgy.Delete(gcode, EnumKeywordType.Sku);
            registKeywordStgy.DeleteIsolated();
        }

        private HarcProductTmp GetErsHarcProductTmp(string scode, string sname, int? price, int? site_id, EnumMallShopKbn? mall_shop_kbn, EnumActive active)
        {
            HarcProductTmp harcParam = default(HarcProductTmp);
            harcParam.scode = scode;
            harcParam.sname = sname;
            harcParam.price = price;
            harcParam.active = active;
            harcParam.site_id = site_id;
            harcParam.mall_shop_kbn = mall_shop_kbn.Value;
            return harcParam;
        }

        /// <summary>
        /// Harc商品更新指示 [Update mall item]
        /// </summary>
        /// <param name="listParam"></param>
        private void UpdateHarc(List<HarcProductTmp> listParam)
        {
            // モール商品更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallProductFactory.GetOperateMallProductHarc().Execute(listParam);

            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102101", ret));
            }
        }

        /// <summary>
        /// モールデータに削除フラグセット
        /// </summary>
        /// <param name="objSku"></param>
        private void DeleteMallData(string scode)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();
            criteria.scode = scode;
            var listMallMerchandise = repository.Find(criteria);

            foreach (var oldObjRecord in listMallMerchandise)
            {
                var newObjReocrd = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandise();

                newObjReocrd.OverwriteWithModel(oldObjRecord);
                newObjReocrd.deleted = EnumDeleted.Deleted;
                repository.Update(oldObjRecord, newObjReocrd);
            }
        }

        private ErsMallMerchandise GetErsMallMerchandiseWithScodeAndMallShopId(string scode, int? site_id, EnumMallShopKbn? mall_shop_kbn)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();
            criteria.scode = scode;
            criteria.site_id = site_id;
            criteria.mall_shop_kbn = mall_shop_kbn;
            var listMallMerchandise = repository.Find(criteria);
            if (listMallMerchandise.Count != 1)
            {
                return null;
            }
            return listMallMerchandise.First();
        }

        /// <summary>
        /// サイトの一覧を取得します。
        /// </summary>
        /// <returns></returns>
        private IList<ErsSite> GetListSite()
        {
            var siteRepository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var siteCriteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
            siteCriteria.active = EnumActive.Active;
            siteCriteria.mall_shop_kbn_not_equal = EnumMallShopKbn.ERS;
            siteCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listSite = siteRepository.Find(siteCriteria);
            return listSite;
        }
    }
}