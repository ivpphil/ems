﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using ersAdmin.Models;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateItemCoexistence
        : IValidationHandler<IItemCoexistenceCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IItemCoexistenceCommand command)
        {
            if (command.mixedGroupList != null)
            {
                var listMixedGcode = new List<string>();
                foreach (var model in command.mixedGroupList)
                {
                    model.AddInvalidField(command.controller.commandBus.Validate<IItemCoexistenceDetailListRecordCommand>(model));

                    if (IsEmpty(model))
                    {
                        continue;
                    }

                    if (listMixedGcode.Contains(model.mixed_group_code))
                    {
                        model.AddInvalidField(new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("mixed_group_code"), model.mixed_group_code), new[] { "mixed_group_code" }));
                    }
                    else
                    {
                        listMixedGcode.Add(model.mixed_group_code);
                    }

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "mixedGroupList" });
                        }
                    }
                }
            }
        }

        public bool IsEmpty(Item_coexistence_detail detail)
        {
            if (detail.mixed_group_code == null
                && detail.mixed_group_name == null)
                return true;

            return false;
        }
    }
}