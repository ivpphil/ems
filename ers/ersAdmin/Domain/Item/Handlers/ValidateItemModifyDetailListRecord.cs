﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateItemModifyDetailListRecord
        : IValidationHandler<IItemModifyDetailListRecordCommand>
    {

        public IEnumerable<ValidationResult> Validate(IItemModifyDetailListRecordCommand command)
        {

            if (command.IsEmpty())
            {
                yield break;
            }

            if (command.delete)
            {
                yield break;
            }

            yield return command.CheckRequired("sname");
            yield return command.CheckRequired("scode");
            var objGroup = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(command.s_gcode);

            if (objGroup != null)
            {
                if (objGroup.s_sale_ptn == EnumSalePatternType.NORMAL     //通常
                    || objGroup.s_sale_ptn == EnumSalePatternType.ALL)     //通常と定期
                {
                    yield return command.CheckRequired("price");
                }

                if (objGroup.s_sale_ptn == EnumSalePatternType.REGULAR     //定期
                    || objGroup.s_sale_ptn == EnumSalePatternType.ALL)     //通常と定期
                {
                    yield return command.CheckRequired("regular_price");
                }
            }
            yield return command.CheckRequired("stock");
            if (command.old_scode.HasValue())
            {
                yield return command.CheckRequired("old_stock");
            }

            if ((command.campaign_point != null && command.campaign_point != 0) || command.point_campaign_from != null || command.point_campaign_to != null)
            {
                yield return command.CheckRequired("point_campaign_from");
                yield return command.CheckRequired("point_campaign_to");
                foreach (var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDateTime("point_campaign_from", command.point_campaign_from, "point_campaign_to", command.point_campaign_to))
                {
                    yield return result;
                }

                yield return command.CheckRequired("campaign_point");
            }

            if (command.soldout_flg == null)
                command.soldout_flg = EnumSoldoutFlg.DisableSoldout;
            yield return command.CheckRequired("soldout_flg");

            if (command.active == null)
                command.active = EnumActive.NonActive;

            if (command.disp_order == null)
                command.disp_order = 0;

            if (command.max_purchase_count == null)
                command.max_purchase_count = 0;

            if (command.point == null)
                command.point = 0;

            //point_campaign_toの時間に指定がない場合は、23:59:59
            if (command.point_campaign_to != null)
            {
                if (command.point_campaign_to.Value.Hour + command.point_campaign_to.Value.Minute + command.point_campaign_to.Value.Second == 0)
                    command.point_campaign_to = Convert.ToDateTime(command.point_campaign_to.Value.ToString("yyyy/MM/dd 23:59:59"));
            }

            //validate max_purchase with if old value of deliv_method of group item is mail
            var containerModel = command.containerModel as IItemModifyCommand;
            if (containerModel != null)
            {
                var groupSku = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(containerModel.old_gcode);
                if (groupSku != null && containerModel.deliv_method_flg)
                {
                    if (command.max_purchase_count > ersAdmin.Models.Item_modify_detail.MaxPurchaseCountViaMail)
                        yield return new ValidationResult(
                            ErsResources.GetMessage("10048", ErsResources.GetFieldName("max_purchase_count"), ersAdmin.Models.Item_modify_detail.MaxPurchaseCountViaMail),
                            new[] { "max_purchase_count" });
                }
            }

            if (command.item_bimg_detail != null)
            {
                foreach (var record in command.item_bimg_detail)
                {
                    if (record.api_errorList != null)
                    {
                        foreach (var err_message in record.api_errorList)
                        {
                            record.AddInvalidField(new ValidationResult(err_message, new[] { "item_bimg_detail" }));
                        }
                    }

                    if (!record.IsValid)
                    {
                        foreach (var errorMessage in record.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "item_bimg_detail" });
                        }
                    }
                }
            }
        }
    }
}