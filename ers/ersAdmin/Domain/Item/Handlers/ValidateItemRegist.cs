﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.Models;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateItemRegist
        : IValidationHandler<IItemRegistCommand>
    {
        public IEnumerable<ValidationResult> Validate(IItemRegistCommand command)
        {
            if (command.item_group_regist_btn)
            {
                yield return command.CheckRequired("gcode");
                yield return command.CheckRequired("gname");
                yield return command.CheckRequired("s_sale_ptn");

                yield return ErsFactory.ersMerchandiseFactory.GetCheckDuplicateMerchandiseGroupStgy().CheckDuplicate(command.gcode);

                yield return command.CheckRequired("date_from");
                yield return command.CheckRequired("date_to");
                foreach (var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDateTime("date_from", command.date_from, "date_to", command.date_to))
                {
                    yield return result;
                }

                yield return command.CheckRequired("stock_flg");
                if (command.stock_flg == EnumStockFlg.DisplaySymbol)
                {
                    yield return command.CheckRequired("stock_set1");
                    yield return command.CheckRequired("stock_set2");
                }
                foreach (var result in ErsFactory.ersMerchandiseFactory.GetCheckStock_flgStgy().Check(command.stock_flg, command.stock_set1, command.stock_set2))
                {
                    yield return result;
                }

                //yield return command.CheckRequired("carriage_cost_type"); refs #24368
                yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("carriage_cost_type", EnumCommonNameType.CarriageCostType, (int?)((ItemModify)command).carriage_cost_type);
                
                yield return command.CheckRequired("plural_order_type");
                yield return command.CheckRequired("set_flg");
                yield return command.CheckRequired("doc_bundling_flg");
                yield return command.CheckRequired("deliv_method");
                yield return command.CheckRequired("disp_list_flg");

                if (command.s_sale_ptn == EnumSalePatternType.ALL
                        || command.s_sale_ptn == EnumSalePatternType.REGULAR)
                {
                    if (command.disp_send_ptn_month_intervals + command.disp_send_ptn_week_intervals + command.disp_send_ptn_month_day_intervals == 0)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("10000", ErsResources.GetFieldName("disp_send_ptn")));
                    }
                }

                if (command.group_simg_detail != null)
                {
                    foreach (var record in command.group_simg_detail)
                    {
                        if (record.api_errorList != null)
                        {
                            foreach (var err_message in record.api_errorList)
                            {
                                record.AddInvalidField(new ValidationResult(err_message, new[] { "group_simg_detail" }));
                            }
                        }

                        if (!record.IsValid)
                        {
                            foreach (var errorMessage in record.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "group_simg_detail" });
                            }
                        }
                    }
                }

                if (command.disp_keyword != null)
                {
                    var tempkeys = string.Join("", command.disp_keyword);
                    var limit = ErsFactory.ersUtilityFactory.getSetup().merchandise_disp_keyword_limit;
                    if (tempkeys.Length >= limit)
                    {
                        yield return new ValidationResult(string.Format(ErsResources.GetMessage("10037", new[] { ErsResources.GetFieldName("disp_keyword"), limit.ToString() })));
                    }
                }
            }
        }
    }
}