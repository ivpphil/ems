﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateSetItemModify : IValidationHandler<ISetItemModifyCommand>
    {
        public IEnumerable<ValidationResult> Validate(ISetItemModifyCommand command)
        {
            yield return command.CheckRequired("parent_scode");

            if (command.ChildScodeList != null)
            {
                foreach (var det in command.ChildScodeList)
                {
                    det.AddInvalidField(command.controller.commandBus.Validate<IChildScodeListCommand>(det));

                    if (!det.IsValid)
                    {
                        foreach (var errorMessage in det.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "ChildScodeList" });
                        }
                    }
                }
            }
        }
    }
}