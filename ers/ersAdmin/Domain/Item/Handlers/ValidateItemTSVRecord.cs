﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateItemTSVRecord
        : IValidationHandler<IItemTSVRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IItemTSVRecordCommand command)
        {
            yield return command.CheckRequired("gcode");
            yield return command.CheckRequired("gname");
            yield return command.CheckRequired("scode");
            yield return command.CheckRequired("sname");
            yield return command.CheckRequired("price");

            if (command.scode.HasValue())
            {
                foreach (var result in ErsFactory.ersMerchandiseFactory.GetCheckMerchandiseExistStgy().CheckScodeExist(command.scode))
                {
                    yield return result;
                }
            }
        }
    }
}