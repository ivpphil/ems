﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.basket;
using ersAdmin.Models;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall.product.strategy;
using ersAdmin.Models.item.mall;
using jp.co.ivp.ers.mall.site;
using System.IO;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mall.util;
using ersAdmin.Domain.SiteBase.Handlers;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ItemModifyHandler
        : SiteRegisterBaseHandler, ICommandHandler<IItemModifyCommand>
    {
        public ICommandResult Submit(IItemModifyCommand command)
        {
            this.ExecuteOperation(command);
            return new CommandResult(true);
        }

        /// <summary>
        /// 
        /// </summary>
        internal void ExecuteOperation(IItemModifyCommand command)
        {
            if (command.item_group_delete_btn)
            {
                this.DeleteItem(command);
                command.returnUrl = "item_search";
            }
            else if (command.item_group_modify_btn)
            {
                this.UpdateGroup(command);
                command.returnUrl = "item_modify";
            }
            else
            {
                this.UpdateDetails(command);
                command.returnUrl = "item_modify";
            }

            ErsFactory.ersMerchandiseFactory.GetUpdatePriceForSearchStgy().Update(command.gcode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchandise"></param>
        private void UpdateDetails(IItemModifyCommand command)
        {
            var mallSetup = ErsMallFactory.ersMallUtilityFactory.getSetup();

            // don't overwrite value when detail update
            command.gcode = command.old_gcode;

            var gcode = command.old_gcode;

            var updateRelationalProductCodeStgy = ErsFactory.ersMerchandiseFactory.GetUpdateRelationalProductCodeStgy();

            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();

            var listParam = new List<UpdateStockParam>();
            var listHarcParam = new List<HarcProductTmp>();

            //サイトIDの一覧を取得
            var listSite = this.GetListSite();

            var keywordConstracter = ErsFactory.ersMerchandiseFactory.GetProductKeywordConstracterStgy();

            foreach (var item in command.detail_table)
            {
                if (!item.IsEmpty())
                {
                    if (item.delete)
                    {
                        //価格の削除
                        this.DeleteSku(item.old_scode);

                        //価格の削除
                        this.DeletePrice(item.old_scode);

                        //在庫の削除
                        this.DeleteStock(item.old_scode);

                        //実在庫の削除
                        this.DeleteWhStock(item.old_scode);

                        //モールデータの削除
                        this.DeleteMallProcess(listHarcParam, listSite, item);
                    }
                    else
                    {
                        var stockRepository = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockRepository();
                        var whStockRepository = ErsFactory.ersWarehouseFactory.GetErsWhStockRepository();

                        item.gcode = gcode;
                        var dicItem = item.GetPropertiesAsDictionary();

                        if (string.IsNullOrEmpty(item.old_scode))
                        {
                            // モールフラグONのデータがあるか
                            bool? isMallEnabled = this.GetMallEnabled(command, mallSetup);

                            //新規登録
                            var objSku = ErsFactory.ersMerchandiseFactory.GetErsSku();
                            objSku.OverwriteWithParameter(dicItem);
                            if (mallSetup.enableMall)
                            {
                                objSku.h_mall_flg = isMallEnabled.Value ? EnumOnOff.On : EnumOnOff.Off;
                            }
                            objSku.keyword = ErsKanaConverter.String_conversion(keywordConstracter.CreateSkuKeyword(objSku));

                            skuRepository.Insert(objSku);

                            //在庫登録
                            var objStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStock();
                            objStock.OverwriteWithParameter(dicItem);
                            stockRepository.Insert(objStock);

                            //実在庫登録
                            var objWhStock = ErsFactory.ersWarehouseFactory.GetErsWhStock();
                            objWhStock.OverwriteWithParameter(dicItem);
                            objWhStock.stock = 0;
                            objWhStock.active = EnumActive.Active;
                            whStockRepository.Insert(objWhStock);

                            //モール新規登録
                            this.InsertMallProcess(command, mallSetup, listParam, listHarcParam, listSite, item, isMallEnabled, objSku);
                        }
                        else
                        {
                            // モールフラグONのデータがあるか
                            bool? isMallEnabled = this.GetMallEnabled(command, mallSetup);

                            //Update set_master_t.scode before the s_master_t.scode has been updated in pgSQL IsolationLevel
                            updateRelationalProductCodeStgy.UpdateOtherScode(item.old_scode, item.scode);

                            //Update
                            var objOldSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(item.old_scode);
                            var objNewSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(item.old_scode);
                            objNewSku.OverwriteWithParameter(dicItem);
                            if (mallSetup.enableMall)
                            {
                                objNewSku.h_mall_flg = isMallEnabled.Value ? EnumOnOff.On : EnumOnOff.Off;
                            }
                            objNewSku.keyword = ErsKanaConverter.String_conversion(keywordConstracter.CreateSkuKeyword(objNewSku));

                            skuRepository.Update(objOldSku, objNewSku);

                            var diffStock = item.stock - item.old_stock;
                            if (diffStock != 0)
                            {
                                ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetIncreaseStockStgy().Increase(item.scode, diffStock.Value);
                            }

                            if (mallSetup.enableMall)
                            {
                                if (item.scode != item.old_scode)
                                {
                                    //旧データの削除
                                    this.DeleteMallProcess(listHarcParam, listSite, item);

                                    //新データの登録
                                    this.InsertMallProcess(command, mallSetup, listParam, listHarcParam, listSite, item, isMallEnabled, objNewSku);
                                }
                                else
                                {
                                    //モールの更新
                                    this.UpdateMallProcess(command, listParam, listHarcParam, listSite, item, isMallEnabled, objOldSku, objNewSku, diffStock);
                                }
                            }
                        }

                        //価格の登録
                        ErsFactory.ersMerchandiseFactory.GetRegistPriceStgy().Regist(item.scode, item.gcode, item.price, item.price2, item.cost_price, item.regular_price, item.regular_first_price);

                        //Only when browser is supported Drag and Drop Images runtime
                        if (!item.disabled_file_uploader)
                        {
                            //save picture
                            var setup = ErsFactory.ersUtilityFactory.getSetup();

                            var temp_folder = setup.image_temp_directory + item.item_bimg_temp_folder + "\\";
                            ErsDirectory.CreateDirectories(temp_folder);

                            var file_identifer_list = new List<string>();
                            var sites = GetErsListSite();
                            //Removed Old Images
                            foreach (var count in Enumerable.Range(1, 3))
                            {

                                if (setup.Multiple_sites)
                                {
                                    
                                    foreach (var site in sites)
                                    {
                                        var old_img_file = setup.multiple_image_directory((int)site.id) + "bimg\\S" + item.scode + "_" + String.Format("{0:D2}", count) + ".jpg";
                                        if (File.Exists(old_img_file))
                                        {
                                            File.Delete(old_img_file);
                                        }
                                    }
                                }
                                else
                                {

                                    var old_img_file = setup.image_directory + "bimg\\S" + item.scode + "_" + String.Format("{0:D2}", count) + ".jpg";
                                    if (File.Exists(old_img_file))
                                    {
                                        File.Delete(old_img_file);

                                    }
                                }
                            }

                            if (item.item_bimg_detail != null)
                            {
                                foreach (var file in item.item_bimg_detail)
                                {
                                    if (setup.Multiple_sites)
                                    {

                                        foreach (var site in sites)
                                        {
                                            var current_index = item.item_bimg_detail.IndexOf(file) + 1;

                                            var temp_file = file.file_identifier + file.bimg;
                                            var dest_file = setup.multiple_image_directory((int)site.id) + "bimg\\S" + item.scode + "_" + String.Format("{0:D2}", current_index) + ".jpg";

                                            var count_sites = sites.Count();

                                            file_identifer_list.Add(temp_file);
                                            if (current_index > 3)
                                            {
                                                break;
                                            }

                                            this.CopyTempImages(Path.Combine(temp_folder, temp_file), dest_file);
                                        }
                                    }
                                    else
                                    {
                                        var current_index = item.item_bimg_detail.IndexOf(file) + 1;

                                        var temp_file = file.file_identifier + file.bimg;
                                        var dest_file = setup.image_directory + "bimg\\S" + item.scode + "_" + String.Format("{0:D2}", current_index) + ".jpg";

                                        file_identifer_list.Add(temp_file);
                                        if (current_index > 3)
                                        {
                                            break;
                                        }

                                        this.CopyTempImages(Path.Combine(temp_folder, temp_file), dest_file);
                                        
                                    }
                                }
                            }

                            foreach (var identifer in file_identifer_list.Distinct())
                            {
                                this.DeleteTempFiles(identifer, temp_folder);
                            }
                        }

                        //Update Other Scode
                        updateRelationalProductCodeStgy.UpdateIsolationOtherScode(item.old_scode, item.scode);
                    }
                }
            }

            // Harc商品更新指示 [Update mall item]
            if (listHarcParam.Count > 0)
            {
                this.UpdateHarc(listHarcParam);
            }

            // モール在庫更新 [Update mall stock]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }
        }

        /// <summary>
        /// モールが有効かどうかを取得
        /// </summary>
        /// <param name="command"></param>
        /// <param name="mallSetup"></param>
        /// <returns></returns>
        private bool? GetMallEnabled(IItemModifyCommand command, SetupMall mallSetup)
        {
            if (!mallSetup.enableMall)
            {
                return null;
            }

            return command.listMallList.Count((listProducts) => listProducts.hasMallProduct) > 0;
        }

        private HarcProductTmp GetErsHarcProductTmp(string scode, string sname, int? price, int? site_id, EnumMallShopKbn? mall_shop_kbn, EnumActive active)
        {
            HarcProductTmp harcParam = default(HarcProductTmp);
            harcParam.scode = scode;
            harcParam.sname = sname;
            harcParam.price = price;
            harcParam.active = active;
            harcParam.site_id = site_id;
            harcParam.mall_shop_kbn = mall_shop_kbn.Value;
            return harcParam;
        }

        /// <summary>
        /// Harc商品更新指示 [Update mall item]
        /// </summary>
        /// <param name="listParam"></param>
        private void UpdateHarc(List<HarcProductTmp> listParam)
        {
            // モール商品更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallProductFactory.GetOperateMallProductHarc().Execute(listParam);

            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102101", ret));
            }
        }

        /// <summary>
        /// モール新規登録
        /// </summary>
        /// <param name="command"></param>
        /// <param name="mallSetup"></param>
        /// <param name="listParam"></param>
        /// <param name="listHarcParam"></param>
        /// <param name="listSite"></param>
        /// <param name="item"></param>
        /// <param name="isMallEnabled"></param>
        /// <param name="objSku"></param>
        private void InsertMallProcess(IItemModifyCommand command, SetupMall mallSetup, List<UpdateStockParam> listParam, List<HarcProductTmp> listHarcParam, IList<ErsSite> listSite, Item_modify_detail item, bool? isMallEnabled, ErsSku objSku)
        {
            if (mallSetup.enableMall)
            {
                //モール更新 [Update Mall Merchandise]
                this.InsertMallData(command, objSku, item.lineNumber);

                // モール在庫用 [For mall stock]
                if (isMallEnabled.Value)
                {
                    UpdateStockParam param = default(UpdateStockParam);

                    param.productCode = item.scode;
                    param.quantity = item.stock;
                    param.operation = EnumMallStockOperation.set;

                    listParam.Add(param);
                }

                //harc更新
                listHarcParam.Add(this.GetErsHarcProductTmp(item.scode, item.sname, item.price, null, EnumMallShopKbn.ERS, EnumActive.NonActive));
                foreach (var objSite in listSite)
                {
                    var mall_flgOn = command.listMallList.Count((listProducts) => listProducts.site_id == objSite.id && listProducts.hasMallProduct) > 0;

                    listHarcParam.Add(this.GetErsHarcProductTmp(item.scode, item.sname, item.price, objSite.id, objSite.mall_shop_kbn, mall_flgOn ? EnumActive.Active : EnumActive.NonActive));
                }
            }
        }

        /// <summary>
        /// 各モールデータINSERT
        /// </summary>
        /// <param name="command"></param>
        /// <param name="objSku"></param>
        /// <param name="lineNumber"></param>
        private void InsertMallData(IItemModifyCommand command, ErsSku objSku, int lineNumber)
        {
            var setup = ErsMallFactory.ersMallUtilityFactory.getSetup();
            if (!setup.enableMall)
            {
                return;
            }

            foreach (var objProductList in command.listMallList)
            {
                var mall_record = objProductList.listProduct.FirstOrDefault((mall) => mall.lineNumber == lineNumber);
                this.InsertMallMerchandise(objProductList.site_id, objProductList.mall_shop_kbn.Value, objSku, mall_record);
            }
        }

        /// <summary>
        /// モールデータINSERT
        /// </summary>
        /// <param name="mall_shop_kbn"></param>
        /// <param name="objSku"></param>
        /// <param name="current_record"></param>
        internal void InsertMallMerchandise(int? site_id, EnumMallShopKbn mall_shop_kbn, ErsSku objSku, Item_modify_mall_detail current_record)
        {
            var repo = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();

            var newObjReocrd = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandise();
            newObjReocrd.OverwriteWithParameter(objSku.GetPropertiesAsDictionary());
            if (current_record != null)
            {
                newObjReocrd.OverwriteWithParameter(current_record.GetPropertiesAsDictionary());
            }
            newObjReocrd.scode = objSku.scode;
            newObjReocrd.gcode = objSku.gcode;
            newObjReocrd.mall_shop_kbn = mall_shop_kbn;
            newObjReocrd.mall_flg = current_record.mall_flg;
            newObjReocrd.site_id = site_id;
            newObjReocrd.id = null;

            repo.Insert(newObjReocrd);
        }

        /// <summary>
        /// モールデータの更新
        /// </summary>
        /// <param name="command"></param>
        /// <param name="listParam"></param>
        /// <param name="listHarcParam"></param>
        /// <param name="listSite"></param>
        /// <param name="item"></param>
        /// <param name="isMallEnabled"></param>
        /// <param name="objOldSku"></param>
        /// <param name="objNewSku"></param>
        /// <param name="diffStock"></param>
        private void UpdateMallProcess(IItemModifyCommand command, List<UpdateStockParam> listParam, List<HarcProductTmp> listHarcParam, IList<ErsSite> listSite, Item_modify_detail item, bool? isMallEnabled, ErsSku objOldSku, ErsSku objNewSku, int? diffStock)
        {
            //モール更新 [Update Mall Merchandise]
            this.UpdateMallData(command, objNewSku, objOldSku.scode, item.lineNumber);

            // 旧データのモール連携がすべてOFFかどうか
            var isMallWasDisabled = (objOldSku.h_mall_flg == EnumOnOff.Off);

            // モール在庫用 [For mall stock]
            if ((diffStock != 0 || isMallWasDisabled) && isMallEnabled.Value)
            {
                UpdateStockParam param = default(UpdateStockParam);

                param.productCode = item.scode;
                if (isMallWasDisabled)
                {
                    //絶対数セット
                    param.quantity = item.stock;
                    param.operation = EnumMallStockOperation.set;
                }
                else
                {
                    //差分セット
                    param.quantity = diffStock;
                    param.operation = EnumMallStockOperation.add;
                }

                listParam.Add(param);
            }

            //harc更新
            listHarcParam.Add(this.GetErsHarcProductTmp(item.scode, item.sname, item.price, null, EnumMallShopKbn.ERS, EnumActive.NonActive));
            foreach (var objSite in listSite)
            {
                var mall_flgOn = command.listMallList.Count((listProducts) => listProducts.site_id == objSite.id && listProducts.hasMallProduct) > 0;

                listHarcParam.Add(this.GetErsHarcProductTmp(item.scode, item.sname, item.price, objSite.id, objSite.mall_shop_kbn, mall_flgOn ? EnumActive.Active : EnumActive.NonActive));
            }
        }

        /// <summary>
        /// 各モールデータUPDATE
        /// </summary>
        /// <param name="command"></param>
        /// <param name="sku"></param>
        /// <param name="old_scode"></param>
        internal void UpdateMallData(IItemModifyCommand command, ErsSku objSku, string old_scode, int lineNumber)
        {
            var setup = ErsMallFactory.ersMallUtilityFactory.getSetup();
            if (!setup.enableMall)
            {
                return;
            }

            foreach (var objProductList in command.listMallList)
            {
                var mall_record = objProductList.listProduct.FirstOrDefault((detail) => detail.scode == old_scode && detail.gcode == command.old_gcode);
                if (!this.UpdateMallMerchandise(objProductList.site_id, objProductList.mall_shop_kbn.Value, objSku, old_scode, mall_record))
                {
                    //更新対象がない場合はInsertする
                    mall_record = objProductList.listProduct.FirstOrDefault((mall) => mall.lineNumber == lineNumber);
                    this.InsertMallMerchandise(objProductList.site_id, objProductList.mall_shop_kbn.Value, objSku, mall_record);
                }
            }
        }

        /// <summary>
        /// モールデータUPDATE
        /// </summary>
        /// <param name="mall_shop_kbn"></param>
        /// <param name="old_scode"></param>
        /// <param name="new_scode"></param>
        /// <param name="current_record"></param>
        internal bool UpdateMallMerchandise(int? site_id, EnumMallShopKbn mall_shop_kbn, ErsSku objSku, string old_scode, Item_modify_mall_detail current_record)
        {
            if (current_record == null)
            {
                return false;
            }

            var repo = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();

            var oldObjRecord = this.GetErsMallMerchandiseWithScodeAndMallShopId(old_scode, site_id, mall_shop_kbn);
            if (oldObjRecord == null)
            {
                return false;
            }

            var newObjReocrd = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandise();
            newObjReocrd.OverwriteWithParameter(oldObjRecord.GetPropertiesAsDictionary());
            newObjReocrd.OverwriteWithParameter(objSku.GetPropertiesAsDictionary());
            if (current_record != null)
            {
                newObjReocrd.OverwriteWithParameter(current_record.GetPropertiesAsDictionary());
            }
            newObjReocrd.scode = objSku.scode;
            newObjReocrd.mall_shop_kbn = mall_shop_kbn;
            newObjReocrd.mall_flg = current_record.mall_flg;
            newObjReocrd.site_id = site_id;
            newObjReocrd.id = oldObjRecord.id;

            repo.Update(oldObjRecord, newObjReocrd);

            return true;
        }

        /// <summary>
        /// 商品情報取得
        /// </summary>
        /// <param name="mall_shop_kbn"></param>
        /// <param name="old_scode"></param>
        /// <param name="new_scode"></param>
        /// <param name="current_record"></param>
        private ErsMallMerchandise GetErsMallMerchandiseWithScodeAndMallShopId(string scode, int? site_id, EnumMallShopKbn? mall_shop_kbn)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();
            criteria.scode = scode;
            criteria.site_id = site_id;
            criteria.mall_shop_kbn = mall_shop_kbn;
            var listMallMerchandise = repository.Find(criteria);
            if (listMallMerchandise.Count != 1)
            {
                return null;
            }
            return listMallMerchandise.First();
        }

        /// <summary>
        /// グループ情報を更新する。
        /// </summary>
        /// <param name="merchandise"></param>
        private void UpdateGroup(IItemModifyCommand command)
        {
            var gcode = command.gcode;
            var old_gcode = command.old_gcode;

            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

            var oldObjGroup = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(old_gcode);
            var newObjGroup = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(old_gcode);

            this.SetProperties(command, newObjGroup);

            var keywordConstracter = ErsFactory.ersMerchandiseFactory.GetProductKeywordConstracterStgy();
            newObjGroup.keyword = ErsKanaConverter.String_conversion(keywordConstracter.CreateGroupKeyword(newObjGroup));

            groupRepository.Update(oldObjGroup, newObjGroup);


            //配送方法がメール便関連だった場合、最大購入可能数を1に更新する。
            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            if (command.deliv_method_flg)
            {
                foreach (var item in command.detail_table)
                {
                    if (!item.IsEmpty())
                    {
                        if(item.old_scode.HasValue())
                        {
                            var objOldSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(item.old_scode);
                            var objNewSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(item.old_scode);

                            objNewSku.max_purchase_count = 1;
                            skuRepository.Update(objOldSku, objNewSku);
                        }
                    }
                }
            }

            //Only when browser is supported Drag and Drop Images runtime
            if (!command.disabled_file_uploader)
            {
                //save picture
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                var sites = GetErsListSite();
                //SaveImage(command.group_simg, setup.image_directory + "\\simg", gcode + "_01.jpg");

                if (setup.Multiple_sites)
                {
                    foreach (var site in sites)
                    {
                        var old_img_file = Path.Combine(setup.multiple_image_directory((int)site.id) + "simg", "G" + gcode + "_01.jpg");
                        if (File.Exists(old_img_file))
                        {
                            File.Delete(old_img_file);
                        }
                    }
                }
                else
                {
                    var old_img_file = Path.Combine(setup.image_directory + "simg", "G" + gcode + "_01.jpg");
                    if (File.Exists(old_img_file))
                    {
                        File.Delete(old_img_file);
                    }
                }



                var temp_folder = setup.image_temp_directory + command.group_simg_temp_folder + "\\";
                ErsDirectory.CreateDirectories(temp_folder);

                if (command.group_simg_detail != null)
                {
                    if (command.group_simg_detail.Count > 0)
                    {
                        var file = command.group_simg_detail.FirstOrDefault();
                        var file_temp = file.file_identifier + file.group_simg;
                        if (setup.Multiple_sites)
                        {
                            foreach (var site in sites)
                            {
                                CopyTempImages(Path.Combine(temp_folder, file_temp), Path.Combine(setup.multiple_image_directory((int)site.id) + "simg", "G" + gcode + "_01.jpg"));
                            }
                        }
                        else
                        {
                            CopyTempImages(Path.Combine(temp_folder, file_temp), Path.Combine(setup.image_directory + "simg", "G" + gcode + "_01.jpg"));
                        }
                    }
                }

                this.DeleteTempFiles(command.image_identifier, temp_folder);
            }

            //Update Other Gcode
            ErsFactory.ersMerchandiseFactory.GetUpdateRelationalProductCodeStgy().UpdateOtherGcode(command.old_gcode, command.gcode);
        }

        /// <summary>
        /// チェックボックス用のNULL回避の為の値セット
        /// </summary>
        /// <param name="command"></param>
        /// <param name="objGroup"></param>
        private void SetProperties(IItemModifyCommand command, ErsGroup objGroup)
        {
            objGroup.OverwriteWithParameter(command.GetPropertiesAsDictionary());
            objGroup.active = objGroup.active ?? EnumActive.NonActive;
            objGroup.disp_list_flg = objGroup.disp_list_flg ?? EnumDisp_list_flg.Invisible;
            objGroup.site_id = this.GetArrayOfSiteId(command);
        }

        /// <summary>
        /// グループ画像の変更
        /// </summary>
        /// <param name="group_simg"></param>
        /// <param name="image_directory"></param>
        /// <param name="gcode"></param>
        private void SaveImage(HttpPostedFileBase imageField, string image_directory, string old_filename, string new_filename)
        {
            if (imageField != null)
            {
                imageField.SaveAs(Path.Combine(image_directory, new_filename));
            }
            else if (old_filename != new_filename)
            {
                this.CopyImage(image_directory, old_filename, new_filename);
            }
        }

        /// <summary>
        /// 登録画像を更新する
        /// </summary>
        /// <param name="group_simg"></param>
        /// <param name="old_fileName"></param>
        /// <param name="new_fileName"></param>
        private void CopyImage(string directoryPath, string old_fileName, string new_fileName)
        {
            var old_fullpath = Path.Combine(directoryPath, old_fileName);
            var new_fullpath = Path.Combine(directoryPath, new_fileName);

            //すでにある画像は上書きしない
            if (File.Exists(old_fullpath) && !File.Exists(new_fullpath))
            {
                File.Copy(old_fullpath, new_fullpath);
            }
        }

        private void CopyTempImages(string tempFile, string destinationFile, bool overwrite = true)
        {
            if (File.Exists(tempFile))
            {
                File.Copy(tempFile, destinationFile, overwrite);
            }
        }

        private void DeleteTempFiles(string identifier, string temp_path)
        {
            var temp_files = System.IO.Directory.GetFiles(temp_path, "*" + identifier + "*");
            
            foreach (var file in temp_files)
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
            }
        }

        /// <summary>
        /// 商品グループ・商品の全削除
        /// </summary>
        /// <param name="merchandise"></param>
        private void DeleteItem(IItemModifyCommand command)
        {
            var listSite = this.GetListSite();

            var gcode = command.gcode;

            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var skuCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            skuCriteria.gcode = gcode;
            var listSku = skuRepository.Find(skuCriteria);
            var listHarcParam = new List<HarcProductTmp>();
            foreach (var objSku in listSku)
            {
                var objPrice = ErsFactory.ersMerchandiseFactory.GetErsDefaultPriceWithScode(objSku.scode);

                //価格の削除
                this.DeleteSku(objSku.scode);

                //価格の削除
                this.DeletePrice(objSku.scode);

                //在庫の削除
                this.DeleteStock(objSku.scode);

                //実在庫の削除
                this.DeleteWhStock(objSku.scode);

                //モール更新 [Update Mall Merchandise]
                this.DeleteMallData(objSku.scode);

                //harc更新
                listHarcParam.Add(this.GetErsHarcProductTmp(objSku.scode, objSku.sname, objPrice.price, null, EnumMallShopKbn.ERS, EnumActive.NonActive));
                foreach (var objSite in listSite)
                {
                    listHarcParam.Add(this.GetErsHarcProductTmp(objSku.scode, objSku.sname, objPrice.price, objSite.id, objSite.mall_shop_kbn, EnumActive.NonActive));
                }
            }

            //グループの削除
            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            groupCriteria.gcode = gcode;
            groupRepository.Delete(groupCriteria);

            var registKeywordStgy = ErsFactory.ersMerchandiseFactory.GetRegistKeywordStgy();
            registKeywordStgy.Delete(gcode, EnumKeywordType.Group);
            registKeywordStgy.Delete(gcode, EnumKeywordType.Sku);
            registKeywordStgy.DeleteIsolated();

            // Harc商品更新指示 [Update mall item]
            if (listHarcParam.Count > 0)
            {
                this.UpdateHarc(listHarcParam);
            }
        }

        /// <summary>
        /// 商品の削除
        /// </summary>
        /// <param name="objSku"></param>
        private void DeleteSku(string scode)
        {
            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var skuCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            skuCriteria.scode = scode;
            skuRepository.Delete(skuCriteria);
        }

        /// <summary>
        /// 在庫の削除
        /// </summary>
        /// <param name="objSku"></param>
        private void DeleteStock(string scode)
        {
            var stockRepository = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockRepository();
            var stockCriteria = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockCriteria();
            stockCriteria.scode = scode;
            stockRepository.Delete(stockCriteria);
        }

        /// <summary>
        /// 実在庫の削除
        /// </summary>
        /// <param name="objSku"></param>
        private void DeleteWhStock(string scode)
        {
            var whStockRepository = ErsFactory.ersWarehouseFactory.GetErsWhStockRepository();
            var whStockCriteria = ErsFactory.ersWarehouseFactory.GetErsWhStockCriteria();
            whStockCriteria.scode = scode;
            whStockRepository.Delete(whStockCriteria);
        }

        /// <summary>
        /// 価格の削除
        /// </summary>
        /// <param name="objSku"></param>
        private void DeletePrice(string scode)
        {
            var priceRepository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();
            var priceCriteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
            priceCriteria.scode = scode;
            priceRepository.Delete(priceCriteria);
        }

        /// <summary>
        /// モール更新
        /// </summary>
        /// <param name="listHarcParam"></param>
        /// <param name="listSite"></param>
        /// <param name="item"></param>
        private void DeleteMallProcess(List<HarcProductTmp> listHarcParam, IList<ErsSite> listSite, Item_modify_detail item)
        {
            //モール更新 [Update Mall Merchandise]
            this.DeleteMallData(item.old_scode);

            //harc更新
            listHarcParam.Add(this.GetErsHarcProductTmp(item.old_scode, item.sname, item.price, null, EnumMallShopKbn.ERS, EnumActive.NonActive));
            foreach (var objSite in listSite)
            {
                listHarcParam.Add(this.GetErsHarcProductTmp(item.old_scode, item.sname, item.price, objSite.id, objSite.mall_shop_kbn, EnumActive.NonActive));
            }
        }

        /// <summary>
        /// モールデータに削除フラグセット
        /// </summary>
        /// <param name="objSku"></param>
        private void DeleteMallData(string scode)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();
            criteria.scode = scode;
            var listMallMerchandise = repository.Find(criteria);

            foreach (var oldObjRecord in listMallMerchandise)
            {
                var newObjReocrd = this.GetErsMallMerchandiseWithScodeAndMallShopId(oldObjRecord.scode, oldObjRecord.site_id, oldObjRecord.mall_shop_kbn);
                if (newObjReocrd == null)
                {
                    continue;
                }

                newObjReocrd.deleted = EnumDeleted.Deleted;

                repository.Update(oldObjRecord, newObjReocrd);
            }
        }

        /// <summary>
        /// サイトの一覧を取得します。
        /// </summary>
        /// <returns></returns>
        private IList<ErsSite> GetListSite()
        {
            var siteRepository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var siteCriteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
            siteCriteria.active = EnumActive.Active;
            siteCriteria.mall_shop_kbn_not_equal = EnumMallShopKbn.ERS;
            siteCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listSite = siteRepository.Find(siteCriteria);
            return listSite;
        }

        /// <summary>
        /// ERSサイトの一覧を取得します。
        /// </summary>
        /// <returns></returns>
        private IList<ErsSite> GetErsListSite()
        {
            var siteRepository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var siteCriteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
            siteCriteria.active = EnumActive.Active;
            siteCriteria.mall_shop_kbn = EnumMallShopKbn.ERS;
            siteCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listSite = siteRepository.Find(siteCriteria);
            return listSite;
        }
    }
}