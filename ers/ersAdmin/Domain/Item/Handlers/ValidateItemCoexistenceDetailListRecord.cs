﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateItemCoexistenceDetailListRecord
        : IValidationHandler<IItemCoexistenceDetailListRecordCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IItemCoexistenceDetailListRecordCommand command)
        {
            if (IsEmpty(command))
                yield break;

            yield return command.CheckRequired("mixed_group_code");
            yield return command.CheckRequired("mixed_group_name");

            if (command.delete == null)
                command.delete = 0;
        }

        public bool IsEmpty(IItemCoexistenceDetailListRecordCommand command)
        {
            if (command.mixed_group_code == null
                && command.mixed_group_name == null)
                return true;

            return false;
        }
    }
}