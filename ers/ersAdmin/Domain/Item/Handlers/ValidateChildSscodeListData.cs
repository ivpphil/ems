﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateChildSscodeListData : IValidationHandler<IChildScodeListCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IChildScodeListCommand command)
        {
            yield return command.CheckRequired("scode");
            yield return command.CheckRequired("amount");
            if (command.id.HasValue)
            {
                var repo = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseWithID(command.id);
                if (repo == null)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10200"));
                }
            }
        }
    }
}