﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using ersAdmin.Models;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateItemModify
        : IValidationHandler<IItemModifyCommand>
    {
        public IEnumerable<ValidationResult> Validate(IItemModifyCommand command)
        {
            yield return command.CheckRequired("gcode");

            //商品登録・商品修正
            if (command.item_group_modify_btn)
            {
                yield return command.CheckRequired("gcode");
                yield return command.CheckRequired("gname");
                yield return command.CheckRequired("s_sale_ptn");

                if (command.old_gcode != command.gcode)
                {
                    yield return ErsFactory.ersMerchandiseFactory.GetCheckDuplicateMerchandiseGroupStgy().CheckDuplicate(command.gcode);
                }

                yield return command.CheckRequired("date_from");
                yield return command.CheckRequired("date_to");
                foreach (var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDateTime("date_from", command.date_from, "date_to", command.date_to))
                {
                    yield return result;
                }

                yield return command.CheckRequired("stock_flg");
                if (command.stock_flg == EnumStockFlg.DisplaySymbol)
                {
                    yield return command.CheckRequired("stock_set1");
                    yield return command.CheckRequired("stock_set2");
                }
                foreach (var result in ErsFactory.ersMerchandiseFactory.GetCheckStock_flgStgy().Check(command.stock_flg, command.stock_set1, command.stock_set2))
                {
                    yield return result;
                }

                // yield return command.CheckRequired("carriage_cost_type"); refs #24368
                yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("carriage_cost_type", EnumCommonNameType.CarriageCostType, (int?)((ItemModify)command).carriage_cost_type);
                
                yield return command.CheckRequired("plural_order_type");
                yield return command.CheckRequired("set_flg");
                yield return command.CheckRequired("doc_bundling_flg");
                yield return command.CheckRequired("deliv_method");
                yield return command.CheckRequired("disp_list_flg");

                if (command.IsValidField("gcode", "s_sale_ptn"))
                {
                    foreach (var result in ErsFactory.ersMerchandiseFactory.GetCheckPriceForSalePtnRegisteredStgy().Validate(command.gcode, command.s_sale_ptn))
                    {
                        yield return result;
                    }

                    if (command.s_sale_ptn == EnumSalePatternType.ALL
                        || command.s_sale_ptn == EnumSalePatternType.REGULAR)
                    {
                        if (command.disp_send_ptn_month_intervals + command.disp_send_ptn_week_intervals + command.disp_send_ptn_month_day_intervals == 0)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("10000", ErsResources.GetFieldName("disp_send_ptn")));
                        }
                    }

                    yield return ErsMallFactory.ersMallProductFactory.GetCheckMallFlgStgy().CheckSku(command.old_gcode, command.s_sale_ptn);
                }
                // remove validation due to it will automatically set max purchase to 1.
                //yield return this.ValidateMaxPurchaseCount(command);
            }
            else if (command.item_group_delete_btn)
            {
            }
            else
            {
                var listScode = new List<string>();
                var listJancode = new List<string>();
                var listAttribute = new List<string>();

                if (command.detail_table != null)
                {
                    foreach (var detail in command.detail_table)
                    {
                        if (detail.IsEmpty())
                            continue;

                        detail.AddInvalidField(command.controller.commandBus.Validate<IItemModifyDetailListRecordCommand>(detail));

                        var checkScodeKey = detail.scode;
                        var checkJancodeKey = detail.jancode;
                        var checkAttributeKey = detail.attribute2 + detail.attribute1;

                        foreach (var result in
                            ErsFactory.ersMerchandiseFactory.GetCheckDuplicateMerchandiseStgy().CheckDuplicate(command.old_gcode, checkScodeKey, checkJancodeKey))
                        {
                            yield return result;
                        }

                        if (!string.IsNullOrEmpty(checkScodeKey) && listScode.Contains(checkScodeKey))
                        {
                            detail.AddInvalidField(new ValidationResult(
                                ErsResources.GetMessage("10103", ErsResources.GetFieldName("scode"), checkScodeKey),
                                new[] { "scode" }));
                        }
                        else
                        {
                            listScode.Add(checkScodeKey);
                        }

                        if (!string.IsNullOrEmpty(checkJancodeKey) && listJancode.Contains(checkJancodeKey))
                        {
                            detail.AddInvalidField(new ValidationResult(
                                ErsResources.GetMessage("10103", ErsResources.GetFieldName("jancode"), checkJancodeKey),
                                new[] { "jancode" }));
                        }
                        else
                        {
                            listJancode.Add(checkJancodeKey);
                        }

                        if (listAttribute.Contains(checkAttributeKey))
                        {
                            detail.AddInvalidField(new ValidationResult(
                                ErsResources.GetMessage("10105", detail.scode, ErsResources.GetFieldName("attribute2"), ErsResources.GetFieldName("attribute1")),
                                new[] { "attribute2", "attribute1" }));
                        }
                        else
                        {
                            listAttribute.Add(checkAttributeKey);
                        }

                        if (!detail.IsValid)
                        {
                            foreach (var errorMessage in detail.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "detail_table" });
                            }
                        }

                        //メール便等で最大購入可能数が[setup.max_purchase_count_mail_per_item]で無ければ
                        if (command.deliv_method_flg)
                        {
                            var setup = ErsFactory.ersUtilityFactory.getSetup();
                            if (setup.max_purchase_count_mail_per_item != detail.max_purchase_count && detail.max_purchase_count != 0)
                            {
                                //yield return new ValidationResult(ErsResources.GetMessage("GP0100", setup.max_purchase_count_mail_per_item));
                            }
                            else
                            {
                                detail.max_purchase_count = setup.max_purchase_count_mail_per_item;
                            }
                        }
                    }
                }

                if (command.listMallList != null)
                {
                    // 各モールの商品情報を検証
                    foreach (var listProductList in command.listMallList)
                    {
                        if (listProductList.listProduct != null)
                        {
                            // モールの各商品を検証
                            foreach (var detail in listProductList.listProduct)
                            {
                                switch (listProductList.mall_shop_kbn)
                                {
                                    case EnumMallShopKbn.RAKUTEN:
                                        detail.AddInvalidField(command.controller.commandBus.Validate((IItemModifyMallRakutenDetailListRecordCommand)detail));
                                        break;

                                    case EnumMallShopKbn.YAHOO:
                                        detail.AddInvalidField(command.controller.commandBus.Validate((IItemModifyMallYahooDetailListRecordCommand)detail));
                                        break;

                                    case EnumMallShopKbn.AMAZON:
                                        detail.AddInvalidField(command.controller.commandBus.Validate((IItemModifyMallAmazonDetailListRecordCommand)detail));
                                        break;
                                    default:
                                        throw new Exception("Not excepted mall_shop_kbn");
                                }

                                if (!detail.IsValid)
                                {
                                    foreach (var errorMessage in detail.GetAllErrorMessageList())
                                    {
                                        listProductList.AddInvalidField(new ValidationResult(errorMessage, new[] { "listProduct" }));
                                    }
                                }
                            }

                            if (!listProductList.IsValid)
                            {
                                foreach (var errorMessage in listProductList.GetAllErrorMessageList())
                                {
                                    yield return new ValidationResult(errorMessage, new[] { "listMallList" });
                                }
                            }
                        }
                    }
                }
            }

            //date_toの時間に指定がない場合は、23:59:59
            if (command.date_to != null)
            {
                if (command.date_to.Value.Hour + command.date_to.Value.Minute + command.date_to.Value.Second == 0)
                    command.date_to = Convert.ToDateTime(command.date_to.Value.ToString("yyyy/MM/dd 23:59:59"));
            }

            if (command.group_simg_detail != null)
            {
                foreach (var record in command.group_simg_detail)
                {
                    if (record.api_errorList != null)
                    {
                        foreach (var err_message in record.api_errorList)
                        {
                            record.AddInvalidField(new ValidationResult(err_message, new[] { "group_simg_detail" }));
                        }
                    }

                    if (!record.IsValid)
                    {
                        foreach (var errorMessage in record.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "group_simg_detail" });
                        }
                    }
                }
            }

            if(command.disp_keyword != null)
            {
                var tempkeys = string.Join("", command.disp_keyword);
                var limit = ErsFactory.ersUtilityFactory.getSetup().merchandise_disp_keyword_limit;
                if (tempkeys.Length >= limit)
                {
                    yield return new ValidationResult(string.Format(ErsResources.GetMessage("10037", new[] { ErsResources.GetFieldName("disp_keyword"),limit.ToString() })));
                }
            }
        }

        /// <summary>
        /// Validate Max Purchase Count per item when deliv method is not 1
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        protected virtual ValidationResult ValidateMaxPurchaseCount(IItemModifyCommand command)
        {
            if (command.deliv_method_flg)
            {
                if (command.detail_table != null)
                {
                    var setup = ErsFactory.ersUtilityFactory.getSetup();

                    var scodeList = new List<string>();
                    var repository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
                    var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();

                    foreach (var record in command.detail_table)
                    {
                        if (record.old_scode.HasValue())
                            scodeList.Add(record.old_scode);

                        if (!record.old_scode.HasValue() && record.scode.HasValue())
                            scodeList.Add(record.scode);
                    }

                    if (scodeList.Count() > 0)
                    {
                        criteria.scode_in = scodeList;
                        criteria.max_purchase_count_not_eq = setup.max_purchase_count_mail_per_item;

                        if (repository.GetRecordCount(criteria) > 0)
                        {
                            return new ValidationResult(ErsResources.GetMessage("GP0100", setup.max_purchase_count_mail_per_item));
                        }
                    }
                }
            }

            return null;
        }
    }
}