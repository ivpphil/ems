﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateSetItemCSVRecord
        : IValidationHandler<ISetItemCSVRecordCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ISetItemCSVRecordCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            yield return command.CheckRequired("parent_scode");
            yield return command.CheckRequired("scode");
            yield return command.CheckRequired("amount");

            var checkParentScodeKey = command.parent_scode;
            var checkScodeKey = command.scode;
            //子の商品コード存在チェック
            var skuCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            skuCriteria.scode = checkScodeKey;
            if (ErsFactory.ersMerchandiseFactory.GetErsSkuRepository().GetRecordCount(skuCriteria) == 0)
            {
                yield return new ValidationResult(ErsResources.GetMessage("20000", checkScodeKey), new[] { "scode" });
            }
            //親の商品コード存在チェック
            var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            groupCriteria.scode = checkParentScodeKey;
            var em = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository().FindSkuBaseItemList(groupCriteria, null);
            if (em.Count != 1)
            {
                yield return new ValidationResult(ErsResources.GetMessage("20000", checkParentScodeKey), new[] { "parent_scode" });
            }
            else
            {
                //親フラグ確認
                if (em[0].set_flg != EnumSetFlg.IsSet)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("20005", checkParentScodeKey), new[] { "parent_scode" });
                }
            }
        }
    }
}