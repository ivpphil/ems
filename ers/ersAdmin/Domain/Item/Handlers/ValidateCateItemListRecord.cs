﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateCateItemListRecord
        : IValidationHandler<ICateItemListRecordCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ICateItemListRecordCommand command)
        {
            if (command.delete != null)
                yield break;

            var containerModel = (CateRegist)command.containerModel;
            int categoryNumber = containerModel.categoryNumber.Value;

            var repository = ErsFactory.ersMerchandiseFactory.GetErsCategoryRepository(categoryNumber);

            if (this.IsEmpty(command))
            {
                var valueKey = command.key;

                if (valueKey >= 0)
                {
                    var c = ErsFactory.ersMerchandiseFactory.GetErsCategoryCriteria(categoryNumber);
                    c.id = valueKey.Value;
                    if (repository.Find(c).Count <= 0)
                        yield break;
                }

                if (valueKey < 0)
                    yield break;

            }

            yield return command.CheckRequired("key");
            yield return command.CheckRequired("id");
            yield return command.CheckRequired("cate_name");
            yield return command.CheckRequired("disp_order");

            if (command.active == null)
                command.active = EnumActive.NonActive;
        }

        public bool IsEmpty(ICateItemListRecordCommand command)
        {
            if (command.id == null
                && command.cate_name == null
                && command.parent_id == null
                && command.delete == null)
                return true;

            return false;
        }
    }
}