﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateItemCSV
        : IValidationHandler<IItemCSVCommand>
    {
        public IEnumerable<ValidationResult> Validate(IItemCSVCommand command)
        {
            var controller = command.controller;
            command.listScode = new List<string>();
            command.listJancode = new List<string>();
            command.listAttribute = new List<string>();

            if (!command.regist)
            {
                if (command.csv_file.csv_file == null)
                {
                    //アップロードファイルを指定してください。
                    //Specify the uploaded file
                    throw new ErsException("10202");
                }
            }

            var p_list = new List<ersAdmin.Models.csv.Item_csv_record>();

            foreach (var model in command.csv_file.GetValidatedModels(command.chk_find))
            {
                p_list.Add(model);
            }

            foreach(var model in p_list)
            {
                model.AddInvalidField(controller.commandBus.Validate<IItemCSVRecordCommand>(model));

                if (model.IsValidField("deliv_method"))
                {
                    model.AddInvalidField(this.ValidMaxPurchaseCount(model, p_list));
                }

                if (!model.IsValid)
                {
                    //完了時は、エラーは画面出力しない（エラー対象を省いて登録するため） // Error message is not displayed at complete screen.
                    if (!command.regist)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "csv_file" });
                        }
                    }
                    command.csv_file.MarkRecordAsInvalid(model);
                }
            }

            //保持された登録情報が無い場合、エラーメッセージを表示。
            if (command.csv_file.validIndexes.Count() == 0)
            {
                //対象データが存在しません。確認してください。
                yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "csv_file" });
            }
        }

        /// <summary>
        ///Validate max_purchase_count with if old value of deliv_method of group item is mail
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        protected ValidationResult ValidMaxPurchaseCount(IItemCSVRecordCommand command, List<ersAdmin.Models.csv.Item_csv_record> p_list)
        {
            bool isValidResult = true;
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (command.deliv_method == EnumDelvMethod.Mail)
            {
                if (command.max_purchase_count.HasValue && command.max_purchase_count > setup.max_purchase_count_mail_per_item)
                {
                    isValidResult = false;
                }
                else
                {
                    var repository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
                    var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
                    criteria.scode = command.scode;
                    criteria.max_purchase_count_not_eq = setup.max_purchase_count_mail_per_item;

                    //Check if existed product has max_purchase_count is != 1
                    if (!command.max_purchase_count.HasValue && repository.GetRecordCount(criteria) > 0)
                    {
                        isValidResult = false;
                    }
                    //Check if not existed product has max_purchase_count is > 1
                }
            }
            else
            {
                //ﾌｧｲﾙ内で不正なﾃﾞｰﾀがあればエラー
                if (command.max_purchase_count > setup.max_purchase_count_mail_per_item)
                {
                    if (p_list.Any(record => record.deliv_method == EnumDelvMethod.Mail && record.gcode == command.gcode))
                    {
                        isValidResult = false;
                    }
                }
            }

            if (!isValidResult)
            {
                return new ValidationResult(
                    ErsResources.GetMessage("GP0100", setup.max_purchase_count_mail_per_item),
                    new[] { "max_purchase_count" });
            }


            return null;
        }
    }
}