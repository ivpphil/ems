﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mall;
using ersAdmin.Models.item.mall;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateItemModifyMallYahooDetailListRecord
        : IValidationHandler<IItemModifyMallYahooDetailListRecordCommand>
    {

        public IEnumerable<ValidationResult> Validate(IItemModifyMallYahooDetailListRecordCommand command)
        {
            if (command.mall_flg == EnumOnOff.Off)
            {
                yield break;
            }

            yield return command.CheckRequired("lineNumber");
            if (command.scode.HasValue())
            {
                yield return ErsMallFactory.ersMallProductFactory.GetCheckMallFlgStgy().CheckGroup(command.scode);
                yield return command.CheckRequired("price");
                yield return command.CheckRequired("manage_id");
                yield return command.CheckRequired("template");
            }
        }
    }
}