﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ItemCoexistenceHandler
        : ICommandHandler<IItemCoexistenceCommand>
    {
        public ICommandResult Submit(IItemCoexistenceCommand command)
        {
            UpdateOrInsert(command);
            return new CommandResult(true);
        }

        /// <summary>
        ///  削除処理実行
        /// </summary>
        internal void UpdateOrInsert(IItemCoexistenceCommand command)
        {
            //リポジトリ インスタンス化
            var repository = ErsFactory.ersBasketFactory.GetErsMixGroupRepository();

            //行選択無し　及び　新規追加行以外はデータ更新
            foreach (var detail in command.mixedGroupList)
            {
                if (IsEmpty(detail))
                    continue;

                if (detail.delete == 1)
                {
                    var mixGroup = ErsFactory.ersBasketFactory.GetErsMixGroupWithId(detail.id.Value);
                    repository.Delete(mixGroup);
                }
                else if (detail.id == null)
                {
                    var mixGroup = ErsFactory.ersBasketFactory.GetErsMixGroupWithModel(detail);
                    repository.Insert(mixGroup);
                }
                else
                {
                    var mixGroup = ErsFactory.ersBasketFactory.GetErsMixGroupWithId(detail.id.Value);
                    var old_mixGroup = ErsFactory.ersBasketFactory.GetErsMixGroupWithId(detail.id.Value);
                    mixGroup.OverwriteWithModel(detail);
                    repository.Update(old_mixGroup, mixGroup);
                }
            }
        }

        public bool IsEmpty(Item_coexistence_detail detail)
        {
            if (detail.mixed_group_code == null
                && detail.mixed_group_name == null)
                return true;

            return false;
        }
    }
}