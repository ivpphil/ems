﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Handlers
{
    public class CateRegistHandler
        : ICommandHandler<ICateRegistCommand>
    {
        public ICommandResult Submit(ICateRegistCommand command)
        {
            if (command.registCategory)
            {
                if(!command.multiple_sites)
                {
                    command.site_id = command.config_site_id;
                }
                UpdateCategoryHeader(command);
                return new CommandResult(true);
            }
            if (command.registCategoryItem)
            {
                UpdateCategory(command);
            }

            return new CommandResult(true);
        }


        /// <summary>
        /// カテゴリのヘッダを更新する
        /// </summary>
        private void UpdateCategoryHeader(ICateRegistCommand command)
        {
            var es = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(command.site_id));

            var t = es.GetType();

            foreach (var header in command.cate_header_table)
            {
                var categoryNumber = header.categoryNumber;

                var propCate = t.GetProperty("cate" + categoryNumber);
                var propCateActive = t.GetProperty("cate" + categoryNumber + "_active");

                ErsReflection.SetProperty(propCate, es, header.cate_header_name, null);
                ErsReflection.SetProperty(propCateActive, es, header.cate_header_active, null);
            }

            var repository = ErsFactory.ersUtilityFactory.GetErsSetupRepository();
            var oldEs = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(command.site_id));
            repository.Update(oldEs, es);

        }

        private void UpdateCategory(ICateRegistCommand command)
        {
            int categoryNumber = command.categoryNumber.Value;
            var repository = ErsFactory.ersMerchandiseFactory.GetErsCategoryRepository(categoryNumber);

            foreach (var cate_body in command.cate_body_table)
            {
                var valueKey = cate_body.key;

                if (valueKey >= 0)
                {
                    var c = ErsFactory.ersMerchandiseFactory.GetErsCategoryCriteria(categoryNumber);
                    c.id = valueKey.Value;
                    var cate = repository.Find(c)[0];

                    if (cate_body.delete != null)
                    {
                        //Delete
                        repository.Delete(cate);
                    }
                    else
                    {
                        cate.OverwriteWithModel(cate_body);
                        cate.site_id = Convert.ToInt32(command.site_id);

                        if (cate.id == null && cate.cate_name == null)
                            return;

                        var oldCate = repository.Find(c)[0];
                        repository.Update(oldCate, cate);
                    }
                }
                else if (cate_body.id != null)
                {
                    //Insert
                    var cate = ErsFactory.ersMerchandiseFactory.GetErsCategoryWithModel(cate_body);

                    cate.categoryNumber = categoryNumber;
                    cate.site_id = Convert.ToInt32(command.site_id);

                    repository.Insert(cate);
                }
            }
        }
    }
}