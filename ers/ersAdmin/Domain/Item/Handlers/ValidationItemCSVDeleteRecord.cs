﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidationItemCSVDeleteRecord
        : IValidationHandler<IItemCSVDeleteRecordCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IItemCSVDeleteRecordCommand command)
        {
            yield return command.CheckRequired("scode");

            foreach (var result in ErsFactory.ersMerchandiseFactory.GetCheckMerchandiseExistStgy().CheckScodeExist(command.scode))
                yield return result;
        }
    }
}