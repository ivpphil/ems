﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using ersAdmin.Models.csv;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Handlers
{
    public class SetItemAllCSVHandler : ICommandHandler<ISetItemAllCSVCommand>
    {
        public ICommandResult Submit(ISetItemAllCSVCommand command)
        {
            AllUpdate(command);
            return new CommandResult(true);
        }

        internal void AllUpdate(ISetItemAllCSVCommand command)
        {
            int count = 0;

            var repository = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseRepository();

            //データを全削除
            repository.Delete();

            foreach (var item in command.csv_file.GetValidModels())
            {
                //レコード追加
                InsertSetMerchandise(repository, item);

                count++;
            }


            //保持された登録情報が無い場合、エラーメッセージを表示。
            if (count == 0)
            {
                //対象データが存在しません。確認してください。
                throw new ErsException("10200");
            }
        }

        /// <summary>
        /// insert csv values to merchandise
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="item"></param>
        protected void InsertSetMerchandise(ErsSetMerchandiseRepository repository, Set_Item_csv_record item)
        {
            var setmerchandise = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseWithModel(item);
            repository.Insert(setmerchandise);
        }
    }
}