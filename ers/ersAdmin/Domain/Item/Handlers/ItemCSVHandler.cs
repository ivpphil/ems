﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using ersAdmin.Models.csv;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mall.product.strategy;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall.site;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.strategy;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ItemCSVHandler
        : ICommandHandler<IItemCSVCommand>
    {
        ProductKeywordConstracterStgy keywordConstracter = ErsFactory.ersMerchandiseFactory.GetProductKeywordConstracterStgy();
        
        public ICommandResult Submit(IItemCSVCommand command)
        {
            this.Update(command);

            return new CommandResult(true);
        }

        internal void Update(IItemCSVCommand command)
        {
            var listHarcParam = new List<HarcProductTmp>();
            var listGcode = new List<string>();
            foreach (var item in command.csv_file.GetValidModels())
            {
                // 1行づつトランザクションを実行する
                try
                {
                    using (var tx = ErsDB_parent.BeginTransaction())
                    {
                        if (!listGcode.Contains(item.gcode))
                        {
                            listGcode.Add(item.gcode);
                        }

                        if (!this.CheckGcodeExist(item.gcode))
                        {
                            //追加
                            this.InsertGroup(item);
                        }
                        else
                        {
                            //更新
                            this.UpdateGroup(item);
                        }

                        //更新
                        UpdateMerchandise(item, listHarcParam);

                        tx.Commit();
                    }
                }
                catch (Exception exception)
                {
                    //エラーが発生した場合でも処理する
                    try
                    {
                        this.FinallyProcess(listHarcParam, listGcode);
                    }
                    catch(Exception innerException)
                    {
                        // catch内でのexceptionは、元のexceptionの後ろに記述する。
                        throw new Exception(exception.ToString() + Environment.NewLine + innerException.ToString());
                    }

                    throw;
                }
            }

            this.FinallyProcess(listHarcParam, listGcode);
        }

        /// <summary>
        /// エラーがおきた場合も必ず実行されるべき処理
        /// </summary>
        /// <param name="listHarcParam"></param>
        /// <param name="listGcode"></param>
        private void FinallyProcess(List<HarcProductTmp> listHarcParam, List<string> listGcode)
        {
            // Harc商品更新指示 [Update mall item]
            if (listHarcParam.Count > 0)
            {
                // モール商品更新 [Update stock for mall]
                var ret = ErsMallFactory.ersMallProductFactory.GetOperateMallProductHarc().Execute(listHarcParam);
                if (ret.HasValue())
                {
                    throw new Exception(ErsResources.GetMessage("102101", ret));
                }
            }

            ErsFactory.ersMerchandiseFactory.GetUpdatePriceForSearchStgy().Update();
        }

        /// <summary>
        /// すでに商品グループの登録があるか
        /// </summary>
        /// <param name="gcode"></param>
        /// <returns></returns>
        protected virtual bool CheckGcodeExist(string gcode)
        {
            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            
            var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            groupCriteria.gcode = gcode;

            return (0 < groupRepository.GetRecordCount(groupCriteria));
        }

        /// <summary>
        /// 商品グループをINSERTする
        /// </summary>
        /// <param name="setupRepository"></param>
        /// <param name="item"></param>
        protected void InsertGroup(Item_csv_record item)
        {
            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

            var objGroup = ErsFactory.ersMerchandiseFactory.GetErsGroupWithParameter(item.GetPropertiesAsDictionary());
            objGroup.site_id = item.site_id;

            // 入力用カテゴリセット [Set category for input]
            objGroup.cate1 = this.GetCategoryForInput(item.cate1);
            objGroup.cate2 = this.GetCategoryForInput(item.cate2);
            objGroup.cate3 = this.GetCategoryForInput(item.cate3);
            objGroup.cate4 = this.GetCategoryForInput(item.cate4);
            objGroup.cate5 = this.GetCategoryForInput(item.cate5);

            objGroup.keyword = ErsKanaConverter.String_conversion(keywordConstracter.CreateGroupKeyword(objGroup));

            groupRepository.Insert(objGroup);
        }

        /// <summary>
        /// 入力用カテゴリ取得
        /// </summary>
        /// <param name="cate">string</param>
        /// <returns>int[]</returns>
        private int[] GetCategoryForInput(string cate)
        {
            if (string.IsNullOrEmpty(cate))
            {
                return null;
            }
            return cate.Split('$').Select(e => Convert.ToInt32(e)).ToArray();
        }

        /// <summary>
        /// 商品グループをUPDATEする
        /// </summary>
        /// <param name="item"></param>
        private void UpdateGroup(Item_csv_record item)
        {
            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

            var oldGroup = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(item.gcode);
            var newGroup = ErsFactory.ersMerchandiseFactory.GetErsGroupWithParameter(oldGroup.GetPropertiesAsDictionary());
            newGroup.OverwriteWithParameter(item.GetPropertiesAsDictionary());
            newGroup.site_id = item.site_id;

            // 入力用カテゴリセット [Set category for input]
            newGroup.cate1 = this.GetCategoryForInput(item.cate1);
            newGroup.cate2 = this.GetCategoryForInput(item.cate2);
            newGroup.cate3 = this.GetCategoryForInput(item.cate3);
            newGroup.cate4 = this.GetCategoryForInput(item.cate4);
            newGroup.cate5 = this.GetCategoryForInput(item.cate5);

            newGroup.keyword = ErsKanaConverter.String_conversion(keywordConstracter.CreateGroupKeyword(newGroup));

            groupRepository.Update(oldGroup, newGroup);
        }

        /// <summary>
        /// 商品をUpdateする。
        /// </summary>
        /// <param name="setupRepository"></param>
        /// <param name="item"></param>
        protected void UpdateMerchandise(Item_csv_record item, List<HarcProductTmp> listHarcParam)
        {
            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            
            var stockRepository = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockRepository();
            var whStockRepository = ErsFactory.ersWarehouseFactory.GetErsWhStockRepository();

            item.gcode = item.gcode;
            var dicItem = item.GetPropertiesAsDictionary();

            if (!this.CheckScodeExist(item.scode))
            {
                //新規登録
                var objSku = ErsFactory.ersMerchandiseFactory.GetErsSku();
                objSku.OverwriteWithParameter(dicItem);
                objSku.keyword = ErsKanaConverter.String_conversion(keywordConstracter.CreateSkuKeyword(objSku));
                objSku.site_id = (int)EnumSiteId.COMMON_SITE_ID;
                skuRepository.Insert(objSku);

                //在庫登録
                var objStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStock();
                objStock.OverwriteWithParameter(dicItem);
                stockRepository.Insert(objStock);

                //実在庫登録
                var objWhStock = ErsFactory.ersWarehouseFactory.GetErsWhStock();
                objWhStock.OverwriteWithParameter(dicItem);
                objWhStock.stock = 0;
                objWhStock.active = EnumActive.Active;
                whStockRepository.Insert(objWhStock);

                //harc更新
                listHarcParam.Add(this.GetErsHarcProductTmp(item.scode, item.sname, item.price, EnumMallShopKbn.ERS, EnumActive.Active));

                //モール更新 [Update Mall Merchandise]
                this.InsertMallData(objSku);
            }
            else
            {
                //Update
                var objOldSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(item.scode);                
                var objNewSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(item.scode);
                objNewSku.OverwriteWithParameter(dicItem);
                objNewSku.keyword = ErsKanaConverter.String_conversion(keywordConstracter.CreateSkuKeyword(objNewSku));
                objNewSku.site_id = (int)EnumSiteId.COMMON_SITE_ID;
                skuRepository.Update(objOldSku, objNewSku);

                var objOldSkuWhStock = ErsFactory.ersWarehouseFactory.GetErsWhStockWithScode(item.scode);
                var objNewSkuWhStock = ErsFactory.ersWarehouseFactory.GetErsWhStockWithScode(item.scode);
                objNewSkuWhStock.OverwriteWithParameter(dicItem);
                whStockRepository.Update(objOldSkuWhStock, objNewSkuWhStock);

                var objOldSkuStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(item.scode);
                var objNewSkuStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(item.scode);
                objNewSkuStock.OverwriteWithParameter(dicItem);
                stockRepository.Update(objOldSkuStock, objNewSkuStock);

                //ERS在庫は更新しない

                //harc更新
                listHarcParam.Add(this.GetErsHarcProductTmp(item.scode, item.sname, item.price, EnumMallShopKbn.ERS, EnumActive.Active));

                //モール更新 [Update Mall Merchandise]
                this.UpdateMallData(objNewSku);
            }

            //価格の登録
            ErsFactory.ersMerchandiseFactory.GetRegistPriceStgy().Regist(item.scode, item.gcode, item.price, item.price2, item.cost_price, item.regular_price, item.regular_first_price);
        }

        /// <summary>
        /// すでに商品の登録があるか
        /// </summary>
        /// <param name="scode"></param>
        /// <returns></returns>
        protected virtual bool CheckScodeExist(string scode)
        {
            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            
            var skuCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            skuCriteria.scode = scode;

            return (0 < skuRepository.GetRecordCount(skuCriteria));
        }

        private HarcProductTmp GetErsHarcProductTmp(string scode, string sname, int? price, EnumMallShopKbn mall_shop_kbn, EnumActive active)
        {
            HarcProductTmp harcParam = default(HarcProductTmp);
            harcParam.scode = scode;
            harcParam.sname = sname;
            harcParam.price = price;
            harcParam.active = active;
            harcParam.mall_shop_kbn = mall_shop_kbn;
            return harcParam;
        }

        /// <summary>
        /// 各モールデータINSERT
        /// </summary>
        /// <param name="command"></param>
        /// <param name="objSku"></param>
        /// <param name="lineNumber"></param>
        private void InsertMallData(ErsSku objSku)
        {
            var setup = ErsMallFactory.ersMallUtilityFactory.getSetup();
            if (!setup.enableMall)
            {
                return;
            }

            var listSite = this.GetListSite();
            foreach (var objSite in listSite)
            {
                this.InsertMallMerchandise(objSite.id, objSite.mall_shop_kbn.Value, objSku);
            }
        }

        /// <summary>
        /// モールデータINSERT
        /// </summary>
        /// <param name="mall_shop_kbn"></param>
        /// <param name="objSku"></param>
        /// <param name="current_record"></param>
        internal void InsertMallMerchandise(int? site_id, EnumMallShopKbn mall_shop_kbn, ErsSku objSku)
        {
            var repo = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();

            var newObjReocrd = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandise();
            newObjReocrd.OverwriteWithParameter(objSku.GetPropertiesAsDictionary());
            newObjReocrd.id = null;
            newObjReocrd.scode = objSku.scode;
            newObjReocrd.mall_shop_kbn = mall_shop_kbn;
            newObjReocrd.site_id = site_id;

            repo.Insert(newObjReocrd);
        }

        /// <summary>
        /// 各モールデータUPDATE
        /// </summary>
        /// <param name="command"></param>
        /// <param name="sku"></param>
        /// <param name="old_scode"></param>
        internal void UpdateMallData(ErsSku objSku)
        {
            var setup = ErsMallFactory.ersMallUtilityFactory.getSetup();
            if (!setup.enableMall)
            {
                return;
            }

            //サイトIDの一覧を取得
            var listSite = this.GetListSite();
            foreach (var objSite in listSite)
            {
                if (!this.UpdateMallMerchandise(objSite.id, objSite.mall_shop_kbn.Value, objSku))
                {
                    //更新対象がない場合はInsertする
                    this.InsertMallMerchandise(objSite.id, objSite.mall_shop_kbn.Value, objSku);
                }
            }
        }

        /// <summary>
        /// サイトの一覧を取得します。
        /// </summary>
        /// <returns></returns>
        private IList<ErsSite> GetListSite()
        {
            var siteRepository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var siteCriteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
            siteCriteria.active = EnumActive.Active;
            siteCriteria.mall_shop_kbn_not_equal = EnumMallShopKbn.ERS;
            siteCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var listSite = siteRepository.Find(siteCriteria);
            return listSite;
        }

        /// <summary>
        /// モールデータUPDATE
        /// </summary>
        /// <param name="mall_shop_kbn"></param>
        /// <param name="old_scode"></param>
        /// <param name="new_scode"></param>
        /// <param name="current_record"></param>
        internal bool UpdateMallMerchandise(int? site_id, EnumMallShopKbn mall_shop_kbn, ErsSku objSku)
        {
            var repo = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();

            var oldObjRecord = this.GetErsMallMerchandiseWithScodeAndMallShopId(objSku.scode, site_id, mall_shop_kbn);
            if (oldObjRecord == null)
            {
                return false;
            }

            var newObjReocrd = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandise();
            newObjReocrd.OverwriteWithParameter(oldObjRecord.GetPropertiesAsDictionary());
            newObjReocrd.OverwriteWithParameter(objSku.GetPropertiesAsDictionary());
            newObjReocrd.id = oldObjRecord.id;
            newObjReocrd.scode = objSku.scode;
            newObjReocrd.mall_shop_kbn = mall_shop_kbn;

            repo.Update(oldObjRecord, newObjReocrd);

            return true;
        }

        /// <summary>
        /// 商品情報取得
        /// </summary>
        /// <param name="mall_shop_kbn"></param>
        /// <param name="old_scode"></param>
        /// <param name="new_scode"></param>
        /// <param name="current_record"></param>
        private ErsMallMerchandise GetErsMallMerchandiseWithScodeAndMallShopId(string scode, int? site_id, EnumMallShopKbn? mall_shop_kbn)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();
            criteria.scode = scode;
            criteria.site_id = site_id;
            criteria.mall_shop_kbn = mall_shop_kbn;
            var listMallMerchandise = repository.Find(criteria);
            if (listMallMerchandise.Count != 1)
            {
                return null;
            }
            return listMallMerchandise.First();
        }
    }
}