﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateCateRegist
        : IValidationHandler<ICateRegistCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ICateRegistCommand command)
        {
            if (command.registCategory)
            {
                if (command.multiple_sites)
                {
                    yield return command.CheckRequired("site_id");
                }
            }

            var controller = command.controller;
            if (command.cate_header_table != null)
            {
                foreach (var model in command.cate_header_table)
                {

                    model.AddInvalidField(controller.commandBus.Validate<ICateHeaderListRecordCommand>(model));

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "cate_header_table" });
                        }
                    }
                }
            }


            if (command.registCategoryItem)
            {
                yield return command.CheckRequired("site_id");

                var repository = ErsFactory.ersMerchandiseFactory.GetErsCategoryRepository(command.categoryNumber.Value);
                var c = ErsFactory.ersMerchandiseFactory.GetErsCategoryCriteria(command.categoryNumber.Value);
                c.site_id_not_equal = Convert.ToInt32(command.site_id);

                var categories = repository.Find(c);

                var listCateId = new List<int>();
                bool isParentIsSelected = false;

                if (command.cate_body_table != null)
                {
                    foreach (var cate_body in command.cate_body_table)
                    {
                        cate_body.AddInvalidField(controller.commandBus.Validate<ICateItemListRecordCommand>(cate_body));

                        //check duplicate
                        if (IsEmpty(cate_body))
                            continue;

                        if (cate_body.id != null)
                        {
                            if (listCateId.Contains(cate_body.id.Value) || categories.Any(e => e.id == cate_body.id))
                            {
                                cate_body.AddInvalidField(new ValidationResult(
                                   ErsResources.GetMessage("10103", ErsResources.GetFieldName("cate_id"), cate_body.id),
                                   new[] { "id" }));
                            }
                            else
                            {
                                listCateId.Add(cate_body.id.Value);
                            }
                        }

                        if (!cate_body.IsValid)
                        {
                            foreach (var errorMessage in cate_body.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "cate_body_table" });
                            }
                        }

                        if (cate_body.parent_id != null)
                            isParentIsSelected = true;
                    }
                }

                if (isParentIsSelected)
                {
                    foreach (var cate_body in command.cate_body_table)
                    {
                        if (!IsEmpty(cate_body))
                        {
                            cate_body.AddInvalidField(cate_body.CheckRequired(ErsResources.GetMessage("line_name", cate_body.lineNumber), "parent_id"));
                        }

                        if (!cate_body.IsValid)
                        {
                            foreach (var errorMessage in cate_body.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "cate_body_table" });
                            }
                        }
                    }
                }
            }
            yield break;
        }

        public bool IsEmpty(ICateItemListRecordCommand command)
        {
            if (command.id == null
                && command.cate_name == null
                && command.parent_id == null
                && command.delete == null)
                return true;

            return false;
        }
    }
}