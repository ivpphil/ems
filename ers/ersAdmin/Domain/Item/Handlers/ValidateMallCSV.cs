﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Item.Handlers
{
    public class ValidateMallCSV
        : IValidationHandler<IMallCSVCommand>
    {
        public IEnumerable<ValidationResult> Validate(IMallCSVCommand command)
        {
            var controller = command.controller;

            yield return command.CheckRequired("site_id");

            if (command.mall_shop_kbn == EnumMallShopKbn.RAKUTEN)
            {

                foreach (var model in command.tsv_rakuten.GetValidatedModels(command.chk_find))
                {
                    model.AddInvalidField(command.controller.commandBus.Validate<IItemModifyMallRakutenDetailListRecordCommand>(model));
                    if (!model.IsValid)
                    {
                        //完了時は、エラーは画面出力しない（エラー対象を省いて登録するため） // Error message is not displayed at complete screen.
                        if (!command.regist)
                        {
                            foreach (var errorMessage in model.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "tsv_file" });
                            }
                        }
                        command.tsv_rakuten.MarkRecordAsInvalid(model);
                    }
                }

                //保持された登録情報が無い場合、エラーメッセージを表示。
                if (command.tsv_rakuten.validIndexes.Count() == 0)
                {
                    //対象データが存在しません。確認してください。
                    yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "tsv_file" });
                }
            }
            else if (command.mall_shop_kbn == EnumMallShopKbn.YAHOO)
            {

                foreach (var model in command.tsv_yahoo.GetValidatedModels(command.chk_find))
                {
                    model.AddInvalidField(command.controller.commandBus.Validate<IItemModifyMallYahooDetailListRecordCommand>(model));
                    if (!model.IsValid)
                    {
                        //完了時は、エラーは画面出力しない（エラー対象を省いて登録するため） // Error message is not displayed at complete screen.
                        if (!command.regist)
                        {
                            foreach (var errorMessage in model.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "tsv_file" });
                            }
                        }
                        command.tsv_yahoo.MarkRecordAsInvalid(model);
                    }
                }

                //保持された登録情報が無い場合、エラーメッセージを表示。
                if (command.tsv_yahoo.validIndexes.Count() == 0)
                {
                    //対象データが存在しません。確認してください。
                    yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "tsv_file" });
                }
            }
            else if (command.mall_shop_kbn == EnumMallShopKbn.AMAZON)
            {

                foreach (var model in command.tsv_amazon.GetValidatedModels(command.chk_find))
                {
                    model.AddInvalidField(command.controller.commandBus.Validate<IItemModifyMallAmazonDetailListRecordCommand>(model));
                    if (!model.IsValid)
                    {
                        //完了時は、エラーは画面出力しない（エラー対象を省いて登録するため） // Error message is not displayed at complete screen.
                        if (!command.regist)
                        {
                            foreach (var errorMessage in model.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "tsv_file" });
                            }
                        }
                        command.tsv_amazon.MarkRecordAsInvalid(model);
                    }
                }

                //保持された登録情報が無い場合、エラーメッセージを表示。
                if (command.tsv_amazon.validIndexes.Count() == 0)
                {
                    //対象データが存在しません。確認してください。
                    yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "tsv_file" });
                }
            }
            else
            {
                yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "tsv_file" });
            }
        }
    }
}