﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Item.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using ersAdmin.Models;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall.product.strategy;

namespace ersAdmin.Domain.Item.Handlers
{
    public class MallCSVHandler
        : ICommandHandler<IMallCSVCommand>
    {
        protected List<UpdateStockParam> listParam { get; set; }
        protected List<HarcProductTmp> listHarcParam { get; set; }

        public ICommandResult Submit(IMallCSVCommand command)
        {
            this.listParam = new List<UpdateStockParam>();
            this.listHarcParam = new List<HarcProductTmp>();

            // 操作 [Operation]
            if (command.mall_shop_kbn == EnumMallShopKbn.RAKUTEN)
                this.OperateRakuten(command);

            if (command.mall_shop_kbn == EnumMallShopKbn.YAHOO)
                this.OperateYahoo(command);

            if (command.mall_shop_kbn == EnumMallShopKbn.AMAZON)
                this.OperateAmazon(command);

            // Harc商品更新指示 [Update mall item]
            if (this.listHarcParam.Count > 0)
            {
                // モール商品更新 [Update stock for mall]
                var ret = ErsMallFactory.ersMallProductFactory.GetOperateMallProductHarc().Execute(listHarcParam);
                if (ret.HasValue())
                {
                    throw new Exception(ErsResources.GetMessage("102101", ret));
                }
            }

            // モール在庫更新 [Update mall stock]
            if (this.listParam.Count > 0)
            {
                var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
                if (ret.HasValue())
                {
                    throw new Exception(ErsResources.GetMessage("102100", ret));
                }
            }

            return new CommandResult(true);
        }

        protected void OperateRakuten(IMallCSVCommand command)
        {
            if (command.tsv_rakuten != null)
            {
                foreach (var record in command.tsv_rakuten.GetValidModels())
                {
                    this.Operation(command, record.scode, record.mall_flg, record.GetPropertiesAsDictionary());
                }
            }
        }

        protected void OperateYahoo(IMallCSVCommand command)
        {
            if (command.tsv_yahoo != null)
            {
                foreach (var record in command.tsv_yahoo.GetValidModels())
                {
                    this.Operation(command, record.scode, record.mall_flg,record.GetPropertiesAsDictionary());
                }
            }
        }

        protected void OperateAmazon(IMallCSVCommand command)
        {
            if (command.tsv_amazon != null)
            {
                foreach (var record in command.tsv_amazon.GetValidModels())
                {
                    this.Operation(command, record.scode, record.mall_flg, record.GetPropertiesAsDictionary());
                }
            }
        }

        /// <summary>
        /// 操作 [Operation]
        /// </summary>
        /// <param name="command"></param>
        /// <param name="scode"></param>
        /// <param name="updatingData"></param>
        protected virtual void Operation(IMallCSVCommand command, string scode, EnumOnOff mall_flg, Dictionary<string, object> updatingData)
        {
            var mall_merchandise = this.GetErsMallMerchandiseWithScodeAndMallShopId(scode, command.site_id, command.mall_shop_kbn);

            // 在庫数連携
            if ((mall_merchandise == null || mall_merchandise.mall_flg == EnumOnOff.Off) && mall_flg == EnumOnOff.On)
            {
                var objStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(scode);

                UpdateStockParam param = default(UpdateStockParam);

                //絶対数セット
                param.productCode = scode;
                param.quantity = objStock.stock;
                param.operation = EnumMallStockOperation.set;

                this.listParam.Add(param);
            }

            // Harc商品連携
            var objMerchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(scode, null);
            this.listHarcParam.Add(this.GetErsHarcProductTmp(objMerchandise.scode, objMerchandise.sname, objMerchandise.price, command.site_id, command.mall_shop_kbn, mall_flg == EnumOnOff.On ? EnumActive.Active : EnumActive.NonActive));

            // ErsMallMerchandise更新 [Update ErsMallMerchandise]
            if (mall_merchandise != null)
            {
                this.UpdateMallMerchandise(mall_merchandise, updatingData);
            }
            else
            {
                this.InsertMallMerchandise(updatingData);
            }

            // s_master_t更新
            this.UpdateMerchandise(scode);
        }

        private void UpdateMerchandise(string scode)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.scode = scode;
            criteria.SetHasActiveMallFlg();
            var hasMallActive = (repository.GetRecordCount(criteria) > 0);

            var newSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(scode);
            var oldSku = ErsFactory.ersMerchandiseFactory.GetErsSku();
            oldSku.OverwriteWithParameter(newSku.GetPropertiesAsDictionary());
            newSku.h_mall_flg = hasMallActive ? EnumOnOff.On : EnumOnOff.Off;

            repository.Update(oldSku, newSku);
        }

        private HarcProductTmp GetErsHarcProductTmp(string scode, string sname, int? price, int? site_id, EnumMallShopKbn? mall_shop_kbn, EnumActive active)
        {
            HarcProductTmp harcParam = default(HarcProductTmp);
            harcParam.scode = scode;
            harcParam.sname = sname;
            harcParam.price = price;
            harcParam.active = active;
            harcParam.site_id = site_id;
            harcParam.mall_shop_kbn = mall_shop_kbn.Value;
            return harcParam;
        }

        private ErsMallMerchandise GetErsMallMerchandiseWithScodeAndMallShopId(string scode, int? site_id, EnumMallShopKbn? mall_shop_kbn)
        {
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();
            var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseCriteria();
            criteria.scode = scode;
            criteria.mall_shop_kbn = mall_shop_kbn;
            criteria.site_id = site_id;
            var listMallMerchandise = repository.Find(criteria);
            if (listMallMerchandise.Count != 1)
            {
                return null;
            }
            return listMallMerchandise.First();
        }
        
        /// <summary>
        /// ErsMallMerchandise更新 [Update ErsMallMerchandise]
        /// </summary>
        /// <param name="mall">ErsMallMerchandise</param>
        /// <param name="updatingData">Dictionary<string, object></param>
        protected virtual void UpdateMallMerchandise(ErsMallMerchandise mall, Dictionary<string, object> updatingData)
        {
            // Repository取得 [Get Repository]
            var repository = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();

            // ErsMallMerchandise取得 [Get ErsMallMerchandise]
            var new_mall = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandise();
            new_mall.OverwriteWithParameter(mall.GetPropertiesAsDictionary());

            new_mall.OverwriteWithParameter(updatingData);

            // 更新 [Update]
            repository.Update(mall, new_mall);
        }

        /// <summary>
        /// モールデータINSERT
        /// </summary>
        internal void InsertMallMerchandise(Dictionary<string, object> updatingData)
        {
            var repo = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandiseRepository();

            var newObjReocrd = ErsMallFactory.ersMallProductFactory.GetErsMallMerchandise();
            newObjReocrd.OverwriteWithParameter(updatingData);

            var objSKU = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(newObjReocrd.scode);
            newObjReocrd.gcode = objSKU.gcode;

            repo.Insert(newObjReocrd);
        }
    }
}