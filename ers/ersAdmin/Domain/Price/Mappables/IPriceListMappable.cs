﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc.pager;
using ersAdmin.jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Mappables
{
    public interface IPriceListMappable
        : IMappable
    {
        IList<Dictionary<string, object>> price_list { set; }

        long recordCount { get; set; }

        string s_gcode { get; }

        string s_scode { get; }

        string s_gname { get; }

        string s_sname { get; }

        ErsPagerModel pager { get; set; }

        EnumSearchType? price_search_type { get; set; }
    }
}