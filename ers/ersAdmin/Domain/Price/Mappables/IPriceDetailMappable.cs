﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;
using ersAdmin.jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Mappables
{
    public interface IPriceDetailMappable
        : IMappable
    {
        string scode { get; set; }

        string sname { get; set; }

        string gname { get; set; }

        string gcode { get; set; }

        int? price { get; set; }

        int? price2 { get; set; }

        int? regular_price { get; set; }

        int? regular_first_price { get; set; }

        int? cost_price { get; set; }

        EnumSalePatternType? s_sale_ptn { get; set; }

        IList<ErsSku> productRelatedList { get; set; }

        EnumSearchType? price_search_type { get; }
    }
}