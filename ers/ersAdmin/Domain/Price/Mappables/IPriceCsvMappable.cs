﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.csv;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Mappables
{
    public interface IPriceCsvMappable
        : IMappable
    {
        ErsCsvContainer<Price_csv_record> csv_file { get; }

        bool chk_find { get; set; }
    }
}