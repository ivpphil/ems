﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.Price.Mappables
{
    public interface IPriceListCSVMappable
        : IPriceListMappable
    {
        ErsCsvCreater csvCreater { get; }
    }
}