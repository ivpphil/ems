﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Price.Mappables
{
    public interface IPriceMemberRankMappable
        : IMappable
    {
        string scode { get; set; }

        List<Models.price.PriceDetailRankRecord> member_rank_record { get; set; }
    }
}