﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Price.Mappables
{
    public interface IPriceDetailListMappable
        : IMappable
    {
        List<ersAdmin.Models.price.PriceDetailRecord> price_table { set; }

        string scode { get; set; }
    }
}