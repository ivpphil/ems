﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Commands
{
    public interface IPriceCsvRecordCommand
        : ICommand
    {
        DateTime? date_to { get; set; }

        DateTime? date_from { get; set; }

        string gcode { get; }

        string scode { get; }

        EnumPriceKbn? price_kbn { get; set; }

        int? member_rank { get; set; }
    }
}