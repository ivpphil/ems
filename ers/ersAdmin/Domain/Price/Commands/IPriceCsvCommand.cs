﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.csv;

namespace ersAdmin.Domain.Price.Commands
{
    public interface IPriceCsvCommand
        : ICommand
    {
        ErsCsvContainer<Price_csv_record> csv_file { get; }

        bool chk_find { get; }

        bool regist { get; }
    }
}