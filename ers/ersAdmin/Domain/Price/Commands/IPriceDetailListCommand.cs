﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.jp.co.ivp.ers;
using ersAdmin.Models.price;

namespace ersAdmin.Domain.Price.Commands
{
    public interface IPriceDetailListCommand
        : ICommand
    {
        EnumSearchType? price_search_type { get; set; }

        List<PriceDetailRecord> price_table { get; }

        string scode { get; set; }

        string gcode { get; set; }
    }
}