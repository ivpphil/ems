﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.price;
using ersAdmin.jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Commands
{
    public interface IPriceMemberRankCommand
        : ICommand
    {
        List<PriceDetailRankRecord> member_rank_record { get; set; }

        string scode { get; set; }

        string gcode { get; set; }

        EnumSearchType? price_search_type { get; set; }
    }
}