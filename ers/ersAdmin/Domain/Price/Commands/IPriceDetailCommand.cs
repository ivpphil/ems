﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.price;
using ersAdmin.jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Commands
{
    public interface IPriceDetailCommand
        : ICommand
    {
        string scode { get; set; }

        string gcode { get; set; }

        List<PriceDetailRecord> price_table { get; }

        int? price { get; set; }

        int? price2 { get; set; }

        int? regular_price { get; set; }

        int? regular_first_price { get; set; }

        int? cost_price { get; set; }

        EnumSearchType? price_search_type { get; set; }
    }
}