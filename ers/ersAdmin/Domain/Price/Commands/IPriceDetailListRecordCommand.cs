﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Price.Commands
{
    public interface IPriceDetailListRecordCommand
        : ICommand
    {
        int? id { get; }

        string gcode { get; }

        string scode { get; }

        DateTime? date_from { get; }

        DateTime? date_to { set; get; }

        int? price { get; }

        bool delete { get; }
    }
}