﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Price.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;
using ersAdmin.jp.co.ivp.ers;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Mappers
{
    public class PriceListMapper
        : IMapper<IPriceListMappable>
    {
        public void Map(IPriceListMappable objMappable)
        {
            objMappable.price_list = this.SearchList(objMappable);
        }

        /// <summary>
        /// 新規の空入力行のみを作成
        /// </summary>
        /// <returns></returns>
        public List<ersAdmin.Models.price.PriceDetailRecord> NewList()
        {
            //表示用リスト作成
            List<ersAdmin.Models.price.PriceDetailRecord> retList = new List<ersAdmin.Models.price.PriceDetailRecord>();
            var lineNumber = 1;

            //新規登録用の行
            for (int i = 0; i < 5; i++)
            {
                var price = new ersAdmin.Models.price.PriceDetailRecord();
                price.lineNumber = lineNumber++;
                retList.Add(price);
            }

            return retList;
        }

        /// <summary>
        /// 検索結果
        /// </summary>
        public IList<Dictionary<string, object>> SearchList(IPriceListMappable objMappable)
        {

            var crtPrice = this.GetPriceCriteria(objMappable);
            var rpsPrice = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();

            //全件数
            if (objMappable.price_search_type == EnumSearchType.SKU_LIST)
            {
                objMappable.recordCount = rpsPrice.GetRecordCountPriceSkuBase(crtPrice);
            }
            else
            {
                objMappable.recordCount = rpsPrice.GetRecordCountPriceGroupBase(crtPrice);
            }

            if (objMappable.recordCount == 0)
                throw new ErsException("10200");

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(crtPrice);
            }

            crtPrice.SetOrderBySort(Criteria.OrderBy.ORDER_BY_ASC);
            crtPrice.SetOrderByGcode(Criteria.OrderBy.ORDER_BY_ASC);
            crtPrice.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

            //表示用リスト作成
            var listDicMerchandise = new List<Dictionary<string, object>>();

            IList<ErsMerchandise> listMerchandise;
            if (objMappable.price_search_type == EnumSearchType.SKU_LIST)
            {
                listMerchandise = rpsPrice.FindPriceSkuBaseItemList(crtPrice);
            }
            else
            {
                listMerchandise = rpsPrice.FindPriceGroupBaseItemList(crtPrice);
            }

            foreach (var merchandise in listMerchandise)
            {
                listDicMerchandise.Add(merchandise.GetPropertiesAsDictionary());
            }
            return listDicMerchandise;
        }

        /// <summary>
        /// 検索実体
        /// </summary>
        /// <returns></returns>
        public ErsPriceCriteria GetPriceCriteria(IPriceListMappable objMappable)
        {
            var crt = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();

            // グループコード
            if (!string.IsNullOrEmpty(objMappable.s_gcode))
            {
                crt.gcode_prefix = objMappable.s_gcode;
            }
            // 商品番号
            if (!string.IsNullOrEmpty(objMappable.s_scode))
            {
                crt.scode_prefix = objMappable.s_scode;
            }
            // グループ名
            if (!string.IsNullOrEmpty(objMappable.s_gname))
            {
                crt.gname_ambiguous = objMappable.s_gname;
            }
            // 商品名
            if (!string.IsNullOrEmpty(objMappable.s_sname))
            {
                crt.sname_ambiguous = objMappable.s_sname;
            }

            return crt;

        }
    }
}