﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Price.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers;
using ersAdmin.jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Price.Mappers
{
    public class PriceDetailMapper
        : IMapper<IPriceDetailMappable>
    {
        public void Map(IPriceDetailMappable objMappable)
        {
            this.MapMerchandise(objMappable);

            if (objMappable.price_search_type.HasValue && objMappable.price_search_type == EnumSearchType.GROUP_LIST)
            {
                this.MapRelatedListMerchandise(objMappable);
            }

            this.MapNormalPrice(objMappable);
        }

        private void MapNormalPrice(IPriceDetailMappable objMappable)
        {
            ErsMerchandise objPrice = ErsFactory.ersMerchandiseFactory.GetErsDefaultPriceWithScode(objMappable.scode);
            if (objPrice == null)
            {
                throw new ErsException("20002");
            }
            objMappable.price = objPrice.price;
            objMappable.price2 = objPrice.price2;
            objMappable.cost_price = objPrice.cost_price;
            objMappable.regular_price = objPrice.regular_price;
            objMappable.regular_first_price = objPrice.regular_first_price;
        }

        private void MapMerchandise(IPriceDetailMappable objMappable)
        {
            ErsMerchandise marchandise;
            if (string.IsNullOrEmpty(objMappable.scode))
            {
                marchandise = this.GetGroup(objMappable);
            }
            else
            {
                marchandise = this.GetSku(objMappable);
            }
            objMappable.gcode = marchandise.gcode;
            objMappable.gname = marchandise.gname;
            objMappable.sname = marchandise.sname;
            objMappable.s_sale_ptn = marchandise.s_sale_ptn;
        }

        private void MapRelatedListMerchandise(IPriceDetailMappable objMappable)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.gcode = objMappable.gcode;
            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            objMappable.productRelatedList = repository.Find(criteria);
        }

        private ErsMerchandise GetGroup(IPriceDetailMappable objMappable)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.scode = objMappable.scode;
            var listSku = repository.FindSkuBaseItemList(criteria, null);
            if (listSku.Count != 1)
            {
                throw new ErsException("20002");
            }
            return listSku.First();
        }

        private ErsMerchandise GetSku(IPriceDetailMappable objMappable)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.scode = objMappable.scode;
            var listSku = repository.FindSkuBaseItemList(criteria, null);
            if (listSku.Count != 1)
            {
                throw new ErsException("20002");
            }
            return listSku.First();
        }
    }
}