﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Price.Mappables;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using ersAdmin.jp.co.ivp.ers;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Mappers
{
    public class PriceListCSVMapper
        : PriceListMapper, IMapper<IPriceListCSVMappable>
    {
        public void Map(IPriceListCSVMappable objMappable)
        {
            CreateCsvFile(objMappable);
        }

        /// <summary>
        /// CSVファイルを作成する。
        /// </summary>
        /// <returns></returns>
        public virtual void CreateCsvFile(IPriceListCSVMappable objMappable)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();
             IEnumerable<ErsPrice> priceList;

            var criteria = this.GetPriceCriteria(objMappable);

            if (objMappable.price_search_type == EnumSearchType.SKU_LIST && objMappable.pager != null)
            {
                IList<Dictionary<string, object>> merchandiseList = this.SearchList(objMappable);
                var listScode = merchandiseList.Select((merchandise) => merchandise["scode"].ToString());
                criteria.scode_in = listScode;
                criteria.SetOrderByArrayIndex("s_master_t.scode", listScode.ToArray(), Criteria.OrderBy.ORDER_BY_ASC);
            }
            else if (objMappable.price_search_type == EnumSearchType.GROUP_LIST && objMappable.pager != null)
            {
                IList<Dictionary<string, object>> merchandiseList = this.SearchList(objMappable);
                var listGcode = merchandiseList.Select((merchandise) => merchandise["gcode"].ToString());
                criteria.gcode_in = listGcode;
                criteria.SetOrderByArrayIndex("g_master_t.gcode",listGcode.ToArray(),Criteria.OrderBy.ORDER_BY_ASC);
            }

            this.SetOrderBy(objMappable, criteria);

            //商品単位の一覧または全部をダウンロードのばあいは、商品単位で出力
            priceList = repository.Find(criteria);


            var filename = DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";

            ersAdmin.Models.csv.Price_csv_record price_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<ersAdmin.Models.csv.Price_csv_record>(writer);
                foreach (var price in priceList)
                {
                    price_csv = new ersAdmin.Models.csv.Price_csv_record();
                    price_csv.OverwriteWithParameter(price.GetPropertiesAsDictionary());
                    objMappable.csvCreater.WriteBody(price_csv, writer);
                }
            }

        }

        private void SetOrderBy(IPriceListCSVMappable objMappable, ErsPriceCriteria criteria)
        {
            criteria.SetOrderBySort(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByGcode(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByPriceKbn(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByMemberRank(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByDateFrom(Criteria.OrderBy.ORDER_BY_ASC);
        }
    }
}