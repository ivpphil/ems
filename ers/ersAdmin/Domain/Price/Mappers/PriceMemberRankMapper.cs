﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Price.Mappables;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.price;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Price.Mappers
{
    public class PriceMemberRankMapper
        : IMapper<IPriceMemberRankMappable>
    {
        public void Map(IPriceMemberRankMappable objMappable)
        {
            objMappable.member_rank_record = this.MapMemberRank(objMappable);

            //this.MapMemberRankDefaultData(objMappable);
        }

        private List<PriceDetailRankRecord> MapMemberRank(IPriceMemberRankMappable objMappable)
        {
            var listRecord = new List<PriceDetailRankRecord>();

            var repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();

            var rankRepository = ErsFactory.ersMemberFactory.GetErsMemberRankSetupRepository();
            var rankCriteria = ErsFactory.ersMemberFactory.GetErsMemberRankSetupCriteria();
            rankCriteria.active = EnumActive.Active;
            rankCriteria.SetOrderByRank(Criteria.OrderBy.ORDER_BY_ASC);

            if (ErsFactory.ersUtilityFactory.getSetup().member_rank_centralization)
            {
                rankCriteria.site_id_for_admin = (int)EnumSiteId.COMMON_SITE_ID;
            }
            else
            {
                rankCriteria.site_id_not_equal = (int)EnumSiteId.COMMON_SITE_ID;
            }

            var listRank = rankRepository.Find(rankCriteria);
            foreach (var rank in listRank)
            {
                var record = new PriceDetailRankRecord();

                var criteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
                criteria.scode = objMappable.scode;
                criteria.member_rank = rank.rank;
                var listPrice = repository.FindPriceMemberRankItemList(criteria);

                if (listPrice.Count > 0)
                {
                    var price = listPrice.First();
                    record.OverwriteWithParameter(price.GetPropertiesAsDictionary());
                }
                else
                {
                    record.member_rank = rank.rank;
                }

                record.member_rank_name = rank.rank_name;

                listRecord.Add(record);
            }

            return listRecord;
        }
    }
}