﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Price.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Mappers
{
    public class PriceDetailListMapper
        : IMapper<IPriceDetailListMappable>
    {
        public void Map(IPriceDetailListMappable objMappable)
        {
            objMappable.price_table = this.SearchList(objMappable);
        }

        /// <summary>
        /// 新規の空入力行のみを作成
        /// </summary>
        /// <returns></returns>
        public List<ersAdmin.Models.price.PriceDetailRecord> NewList()
        {
            //表示用リスト作成
            List<ersAdmin.Models.price.PriceDetailRecord> retList = new List<ersAdmin.Models.price.PriceDetailRecord>();
            var lineNumber = 1;

            //新規登録用の行
            for (int i = 0; i < 5; i++)
            {
                var price = new ersAdmin.Models.price.PriceDetailRecord();
                price.lineNumber = lineNumber++;
                retList.Add(price);
            }

            return retList;
        }

        /// <summary>
        /// 検索結果
        /// </summary>
        public List<ersAdmin.Models.price.PriceDetailRecord> SearchList(IPriceDetailListMappable objMappable)
        {

            var crtPrice = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
            crtPrice.price_kbn = EnumPriceKbn.SALE;
            crtPrice.scode = objMappable.scode;

            var rpsPrice = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();

            crtPrice.SetOrderBySort(Criteria.OrderBy.ORDER_BY_ASC);
            crtPrice.SetOrderByGcode(Criteria.OrderBy.ORDER_BY_ASC);
            crtPrice.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
            crtPrice.SetOrderByDateFrom(Criteria.OrderBy.ORDER_BY_ASC);

            //表示用リスト作成
            IList<ErsPrice> listPrice = rpsPrice.Find(crtPrice);

            List<ersAdmin.Models.price.PriceDetailRecord> retList = new List<ersAdmin.Models.price.PriceDetailRecord>();
            var lineNumber = 1;
            foreach (var priceData in listPrice)
            {
                var price = new ersAdmin.Models.price.PriceDetailRecord();
                price.OverwriteWithParameter(priceData.GetPropertiesAsDictionary());
                price.lineNumber = lineNumber++;
                retList.Add(price);
            }

            //新規登録用の行が必要かどうか
            for (int i = 0; i < 5; i++)
            {
                var price = new ersAdmin.Models.price.PriceDetailRecord();
                price.lineNumber = lineNumber++;
                retList.Add(price);
            }

            return retList;
        }
    }
}