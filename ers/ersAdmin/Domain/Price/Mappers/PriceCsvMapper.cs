﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Price.Mappables;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Mappers
{
    public class PriceCsvMapper
        : IMapper<IPriceCsvMappable>
    {
        public void Map(IPriceCsvMappable objMappable)
        {
            foreach (var model in objMappable.csv_file.GetValidModels())
            {
                if (model.price_kbn != EnumPriceKbn.RANK_NORMAL &&
                    model.price_kbn != EnumPriceKbn.RANK_REGULAR &&
                    model.price_kbn != EnumPriceKbn.RANK_REGULAR_FIRST)
                {
                    model.member_rank = null;
                }

                if (model.price_kbn == EnumPriceKbn.SALE)
                {
                    if (model.date_to.Value.Hour + model.date_to.Value.Minute +model.date_to.Value.Second == 0)
                    {
                        model.date_to = Convert.ToDateTime(model.date_to.Value.ToString("yyyy/MM/dd 23:59:59"));
                    }
                }
                else
                {
                    model.date_from = null;
                    model.date_to = null;
                }

                if (model.price_kbn != EnumPriceKbn.NORMAL)
                {
                    model.price2 = null;
                }

                objMappable.csv_file.SaveValue(model);
            }
        }
    }
}