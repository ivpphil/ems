﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Price.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Handlers
{
    public class PriceCsvHandler
        : ICommandHandler<IPriceCsvCommand>
    {
        public ICommandResult Submit(IPriceCsvCommand command)
        {
            Update(command);
            return new CommandResult(true);
        }

        internal void Update(IPriceCsvCommand command)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();

            foreach (var item in command.csv_file.GetValidModels())
            {
                //すでに商品の登録があるか
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
                criteria.scode = item.scode;
                criteria.price_kbn = item.price_kbn;
                switch (item.price_kbn)
                {
                    case EnumPriceKbn.NORMAL:
                    case EnumPriceKbn.REGULAR:
                    case EnumPriceKbn.REGULAR_FIRST:
                        break;
                    case EnumPriceKbn.RANK_NORMAL:
                    case EnumPriceKbn.RANK_REGULAR:
                    case EnumPriceKbn.RANK_REGULAR_FIRST:
                        criteria.member_rank = item.member_rank;
                        break;
                    case EnumPriceKbn.SALE:
                        criteria.date_from = item.date_from;
                        break;
                }
                var list = repository.Find(criteria);

                if (0 == list.Count)
                {
                    //追加
                    var price = ErsFactory.ersMerchandiseFactory.GetErsPrice();
                    price.OverwriteWithModel(item);
                    repository.Insert(price);
                }
                else
                {
                    var new_price = list.First();
                    new_price.OverwriteWithModel(item);

                    var old_price = repository.Find(criteria)[0];
                    //更新
                    repository.Update(old_price, new_price);
                }
            }

            ErsFactory.ersMerchandiseFactory.GetUpdatePriceForSearchStgy().Update();
        }
    }
}