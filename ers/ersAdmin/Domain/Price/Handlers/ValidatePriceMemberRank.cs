﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Price.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Price.Handlers
{
    public class ValidatePriceMemberRank
        : IValidationHandler<IPriceMemberRankCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPriceMemberRankCommand command)
        {
            if (command.member_rank_record != null)
            {
                foreach (var model in command.member_rank_record)
                {
                    model.AddInvalidField(command.controller.commandBus.Validate<IPriceMemberRankRecordCommand>(model));

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "member_rank_record" });
                        }
                    }
                }
            }
        }
    }
}