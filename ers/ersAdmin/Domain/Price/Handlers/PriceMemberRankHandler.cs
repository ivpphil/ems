﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Price.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers;
using ersAdmin.Models.price;
using ersAdmin.jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Handlers
{
    public class PriceMemberRankHandler
        : ICommandHandler<IPriceMemberRankCommand>
    {
        public ICommandResult Submit(IPriceMemberRankCommand command)
        {
            if (command.price_search_type == EnumSearchType.GROUP_LIST)
            {
                this.DeleteMemberRankPrices(command.gcode);
                this.RegistGroup(command);
            }
            else
            {
                this.RegistSku(command.scode, command.gcode, command.member_rank_record);
            }
            return new CommandResult(true);
        }

        /// <summary>
        /// 全てのセール価格を削除する
        /// </summary>
        /// <param name="gcode"></param>
        private void DeleteMemberRankPrices(string gcode)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
            criteria.gcode = gcode;
            criteria.price_kbn_in = new[] { EnumPriceKbn.RANK_NORMAL, EnumPriceKbn.RANK_REGULAR, EnumPriceKbn.RANK_REGULAR_FIRST };
            repository.Delete(criteria);
        }

        private void RegistGroup(IPriceMemberRankCommand command)
        {
            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var skuCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            skuCriteria.gcode = command.gcode;
            var listSku = skuRepository.Find(skuCriteria);

            foreach (var objSku in listSku)
            {
                this.RegistSku(objSku.scode, objSku.gcode, command.member_rank_record);
            }
        }

        private void RegistSku(string scode, string gcode, List<PriceDetailRankRecord> member_rank_record)
        {
            foreach (var record in member_rank_record)
            {
                ErsFactory.ersMerchandiseFactory.GetRegistPriceOfMemberRankStgy().Regist(scode, gcode, record.member_rank, record.price, record.regular_price, record.regular_first_price);
            }
        }
    }
}