﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Price.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Handlers
{
    public class ValidatePriceDetail
        : IValidationHandler<IPriceDetailCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPriceDetailCommand command)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            ErsMerchandise merchandise = null;
            if (string.IsNullOrEmpty(command.scode))
            {
                yield return command.CheckRequired("gcode");
                if (command.IsValidField("gcode"))
                {
                    var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
                    criteria.gcode = command.gcode;
                    var listMerchandise = repository.FindSkuBaseItemList(criteria, null);
                    if (listMerchandise.Count == 0)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("20002"));
                    }
                    else
                    {
                        merchandise = listMerchandise.First();
                    }
                }
            }
            else
            {
                yield return command.CheckRequired("scode");
                if (command.IsValidField("scode"))
                {
                    var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
                    criteria.scode = command.scode;
                    var listMerchandise = repository.FindSkuBaseItemList(criteria, null);
                    if (listMerchandise.Count == 0)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("20002"));
                    }
                    else
                    {
                        merchandise = listMerchandise.First();
                    }
                }
            }

            if (merchandise != null)
            {
                if (merchandise.s_sale_ptn != EnumSalePatternType.REGULAR)
                {
                    yield return command.CheckRequired("price");
                }
                if (merchandise.s_sale_ptn != EnumSalePatternType.NORMAL)
                {
                    yield return command.CheckRequired("regular_price");
                }

                command.gcode = merchandise.gcode;
                command.scode = merchandise.scode;
            }
        }
    }
}