﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Price.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;
using ersAdmin.jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Handlers
{
    public class PriceDetailHandler
        : ICommandHandler<IPriceDetailCommand>
    {
        public ICommandResult Submit(IPriceDetailCommand command)
        {
            if (command.price_search_type == EnumSearchType.SKU_LIST)
            {
                ErsFactory.ersMerchandiseFactory.GetRegistPriceStgy().Regist(command.scode, command.gcode, command.price, command.price2, command.cost_price, command.regular_price, command.regular_first_price);
            }
            else
            {
                this.RegistGroup(command);
            }

            return new CommandResult(true);
        }

        private void RegistGroup(IPriceDetailCommand command)
        {
            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var skuCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            skuCriteria.gcode = command.gcode;
            var listSku = skuRepository.Find(skuCriteria);
            foreach (var objSku in listSku)
            {
                ErsFactory.ersMerchandiseFactory.GetRegistPriceStgy().Regist(objSku.scode, objSku.gcode, command.price, command.price2, command.cost_price, command.regular_price, command.regular_first_price);
            }
        }

    }
}