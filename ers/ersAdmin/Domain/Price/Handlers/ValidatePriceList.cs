﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Price.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Price.Handlers
{
    public class ValidatePriceList
        : IValidationHandler<IPriceListCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IPriceListCommand command)
        {
            yield return command.CheckRequired("price_search_type");
        }
    }
}