﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Price.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Price.Handlers
{
    public class ValidatePriceDetailList
        : IValidationHandler<IPriceDetailListCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IPriceDetailListCommand command)
        {
            if (command.price_table != null)
            {
                foreach (var model in command.price_table)
                {
                    model.AddInvalidField(command.controller.commandBus.Validate<IPriceDetailListRecordCommand>(model));

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "price_table" });
                        }
                    }
                }
            }
        }
    }
}