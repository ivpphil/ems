﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Price.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;

namespace ersAdmin.Domain.Price.Handlers
{
    public class PriceDetailListHandler
        : ICommandHandler<IPriceDetailListCommand>
    {
        public ICommandResult Submit(IPriceDetailListCommand command)
        {
            if (command.price_search_type == EnumSearchType.GROUP_LIST)
            {
                this.DeleteSalePrices(command.gcode);
                this.RegistGroup(command);
            }
            else
            {
                this.RegistSku(command);
            }
            return new CommandResult(true);
        }

        /// <summary>
        /// 全てのセール価格を削除する
        /// </summary>
        /// <param name="gcode"></param>
        private void DeleteSalePrices(string gcode)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
            criteria.gcode = gcode;
            criteria.price_kbn = EnumPriceKbn.SALE;
            repository.Delete(criteria);
        }

        private void RegistGroup(IPriceDetailListCommand command)
        {
            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var skuCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            skuCriteria.gcode = command.gcode;
            var listSku = skuRepository.Find(skuCriteria);

            foreach (var price in command.price_table)
            {
                if (!IsEmpty(price) && !price.delete)
                {
                    foreach (var objSku in listSku)
                    {
                        ErsFactory.ersMerchandiseFactory.GetRegistPriceOfSaleStgy().Regist(null, objSku.scode, objSku.gcode, price.price, price.date_from, price.date_to);
                    }
                }
            }
        }

        /// <summary>
        /// 情報更新処理
        /// </summary>
        internal void RegistSku(IPriceDetailListCommand command)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();
            foreach (var price in command.price_table)
            {

                //空じゃなければ新規挿入
                if (!IsEmpty(price))
                {
                    if (price.delete)
                    {
                        var criteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
                        criteria.id = price.id;
                        criteria.scode = command.scode;

                        repository.Delete(criteria);
                    }
                    else
                    {
                        ErsFactory.ersMerchandiseFactory.GetRegistPriceOfSaleStgy().Regist(price.id, command.scode, command.gcode, price.price, price.date_from, price.date_to);
                    }
                }
            }

            ErsFactory.ersMerchandiseFactory.GetUpdatePriceForSearchStgy().Update();
        }

        public bool IsEmpty(ersAdmin.Models.price.PriceDetailRecord price)
        {
            if (price.gcode == null
                && price.scode == null
                && price.date_from == null
                && price.date_to == null
                && price.price == null)
                return true;

            return false;
        }
    }
}