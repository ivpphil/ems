﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Price.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Handlers
{
    public class ValidatePriceCsvRecord
        : IValidationHandler<IPriceCsvRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPriceCsvRecordCommand command)
        {
            yield return command.CheckRequired("gcode");
            yield return command.CheckRequired("scode");
            yield return command.CheckRequired("price_kbn");
            if (command.IsValidField("price_kbn"))
            {
                if (command.price_kbn == EnumPriceKbn.RANK_NORMAL||
                    command.price_kbn == EnumPriceKbn.RANK_REGULAR ||
                    command.price_kbn == EnumPriceKbn.RANK_REGULAR_FIRST)
                {
                    yield return command.CheckRequired("member_rank");
                }

                if (command.price_kbn == EnumPriceKbn.SALE)
                {
                    yield return command.CheckRequired("date_from");
                    yield return command.CheckRequired("date_to");

                    //日付チェック
                    if (command.IsValidField("date_from", "date_to"))
                    {
                        if (command.date_from > command.date_to)
                        {
                            yield return new ValidationResult(
                                ErsResources.GetMessage("10045", ErsResources.GetFieldName("price_t.date_to"), ErsResources.GetFieldName("price_t.date_from")),
                                new[] { "date_from", "date_to" });
                        }

                        if (command.IsValidField("scode"))
                        {
                            foreach (var result in ErsFactory.ersMerchandiseFactory.GetCheckDuplicatePriceStgy().CheckDuplicate(command.scode, command.date_from, command.date_to))
                            {
                                yield return result;
                            }
                        }
                    }
                }

                foreach (var result in this.CheckDuplicateCSV((Price_csv)command.containerModel, command))
                {
                    yield return result;
                }
            }
            
            
            var g_item = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(command.gcode);
            if (g_item != null)
            {
                if (g_item.s_sale_ptn != EnumSalePatternType.REGULAR)
                    yield return command.CheckRequired("price");
            }

            //DBデータチェック
            if (command.IsValidField("gcode", "scode"))
            {
                //存在チェック
                foreach (var result in
                    ErsFactory.ersMerchandiseFactory.GetCheckMerchandiseExistStgy().Check(command.gcode, command.scode))
                {
                    yield return result;
                }
            }
        }

        public IEnumerable<ValidationResult> CheckDuplicateCSV(Price_csv containerModel, IPriceCsvRecordCommand command)
        {
          for (var index = 0; index < command.lineNumber - 1; index++)
            {
                var record = containerModel.csv_file.GetValidModel(index);

                //既存一致条件での判定
                if (command.gcode == record.gcode &&
                    command.scode == record.scode &&
                    command.price_kbn == record.price_kbn)
                {
                    if (command.price_kbn == EnumPriceKbn.SALE)
                    {
                        if (command.date_from >= record.date_from && command.date_from <= record.date_to
                                || command.date_to >= record.date_from && command.date_to <= record.date_to)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("30201", command.scode), new[] { "scode", "date_from", "date_to" });
                        }
                    }

                    if (command.price_kbn == EnumPriceKbn.RANK_NORMAL||
                        command.price_kbn == EnumPriceKbn.RANK_REGULAR ||
                        command.price_kbn == EnumPriceKbn.RANK_REGULAR_FIRST)
                    {
                        if (command.price_kbn == record.price_kbn && command.member_rank == record.member_rank)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("10104", ErsResources.GetFieldName("scode"), ErsResources.GetFieldName("member_rank")), new[] { "scode", "member_rank" });
                        }
                    }

                    if (command.price_kbn == EnumPriceKbn.NORMAL||
                        command.price_kbn == EnumPriceKbn.REGULAR ||
                        command.price_kbn == EnumPriceKbn.REGULAR_FIRST)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("10104", ErsResources.GetFieldName("scode"), ErsResources.GetFieldName("price_kbn")), new[] { "scode", "price_kbn" });
                    }
                }
            }
        }


    }
}