﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Price.Commands;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Price.Handlers
{
    public class ValidatePriceDetailListRecord
        : IValidationHandler<IPriceDetailListRecordCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IPriceDetailListRecordCommand command)
        {
            if (this.IsEmpty(command))
                yield break;

            if (command.delete)
            {
                yield break;
            }

            yield return command.CheckRequired("date_from");
            yield return command.CheckRequired("date_to");
            yield return command.CheckRequired("price");

            //日付チェック
            foreach (var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDateTime("price_t.date_from", command.date_from, "price_t.date_to", command.date_to))
            {
                yield return result;
            }

            //date_toの時間に指定がない場合は、23:59:59
            if (command.IsValidField("date_to"))
            {
                if (command.date_to.Value.Hour + command.date_to.Value.Minute +command.date_to.Value.Second == 0)
                    command.date_to = Convert.ToDateTime(command.date_to.Value.ToString("yyyy/MM/dd 23:59:59"));
            }

            foreach (var result in this.CheckDuplicateTable((IPriceDetailListCommand)command.containerModel, command))
            {
                yield return result;
            }
        }

        public IEnumerable<ValidationResult> CheckDuplicateTable(IPriceDetailListCommand containerModel, IPriceDetailListRecordCommand command)
        {
            foreach (var record in containerModel.price_table)
            {
                if (record.lineNumber != command.lineNumber)
                {
                    //既存一致条件での判定
                    if (command.date_from >= record.date_from && command.date_from <= record.date_to
                                || command.date_to >= record.date_from && command.date_to <= record.date_to)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("30201", containerModel.scode), new[] { "scode", "date_from", "date_to" });
                    }
                }
            }
        }

        public bool IsEmpty(IPriceDetailListRecordCommand command)
        {
            if (command.gcode == null
                && command.scode == null
                && command.date_from == null
                && command.date_to == null
                && command.price == null)
                return true;

            return false;
        }
    }
}