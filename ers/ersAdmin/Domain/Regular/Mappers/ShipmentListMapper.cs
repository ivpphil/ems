﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Regular.Mappables;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using ersAdmin.Models;

namespace ersAdmin.Domain.Regular.Mappers
{
    public class ShipmentListMapper
        : BillSearchMapper, IMapper<IShipmentListMappable>
    {
        public void Map(IShipmentListMappable objMappable)
        {
            SearchList(objMappable);
        }

        /// <summary>
        /// 伝票検索（通常）
        /// </summary> remove
        public virtual void SearchList(IShipmentListMappable objMappable)
        {
            var crtOrder = SetOrderCriteria(objMappable);

            //件数
            var rpsOrder = ErsFactory.ersOrderFactory.GetErsOrderRepository();

            objMappable.recordCount = GetRecordCount(crtOrder, rpsOrder);

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(crtOrder);
            }

            objMappable.orderList = GetListForView(crtOrder, rpsOrder);
        }

        /// <summary>
        /// Get record count.
        /// </summary>
        /// <param name="crtOrder"></param>
        /// <param name="rpsOrder"></param>
        /// <returns></returns> remove
        protected virtual long GetRecordCount(ErsOrderCriteria crtOrder, ErsOrderRepository rpsOrder)
        {
            var count = rpsOrder.GetRecordCount(crtOrder);

            if (count == 0)
                throw new ErsException("10200");

            return count;
        }

                /// <summary>
        /// 伝票検索実体
        /// </summary>
        /// <returns></returns> remove
        protected virtual ErsOrderCriteria SetOrderCriteria(IShipmentListMappable objMappable)
        {
            var shipping_csv_output_min_days = ErsFactory.ersBatchFactory.getSetup().shipping_csv_output_min_days;
            var crtOrder = base.SetOrderCriteria(objMappable);

            crtOrder.senddate_greater_than = DateTime.Today.AddDays(shipping_csv_output_min_days);
            
            this.SetOrderCriteria(crtOrder);

            return crtOrder;
        }

        protected ErsOrderCriteria SetOrderCriteria(ErsOrderCriteria crtOrder)
        {
            
            //追加条件（キャンセル以外）
            crtOrder.SetConditionsForShipmentList(crtOrder);

            //同梱済みのみ
            crtOrder.doc_bundle_flg = EnumDocBundlingFlg.ON;

            return crtOrder;
        }
    }
}