﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Regular.Mappables;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.regular;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Regular.Mappers
{
    public class BillModifyMapper : IMapper<IBillModifyMappable>
    {
        public void Map(IBillModifyMappable objMappable)
        {
            this.Init(objMappable);
            if (objMappable.IsLoadDefaultValue)
            {
                this.LoadDefaultData(objMappable);
            }
            else
            {
                this.LoadConfirmData(objMappable);
            }

            this.LoadSetItems(objMappable);
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public void Init(IBillModifyMappable objMappable)
        {
            objMappable.objOrder = ErsFactory.ersOrderFactory.GetErsOrderContainer();
            // 伝票番号から伝票取得
            objMappable.objOrder.OrderHeader = ErsFactory.ersOrderFactory.GetOrderWithD_no(objMappable.d_no);
            if (objMappable.objOrder.OrderHeader == null)
            {
                throw new ErsException("30104", objMappable.d_no);
            }

            objMappable.objOrder.OrderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(objMappable.objOrder.OrderHeader);
        }

        private void LoadSetItems(IBillModifyMappable objMappable)
        {
            foreach (var orderrecord in objMappable.orderRecords)
            {
                var list = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(orderrecord.scode);

                orderrecord.lstSetItems = ErsCommon.ConvertEntityListToDictionaryList(list);
            }

            
        }

        /// <summary>
        /// 初期入力値を読み込む
        /// </summary>
        public void LoadDefaultData(IBillModifyMappable objMappable)
        {
            // プロパティ初期化
            objMappable.OverwriteWithParameter(objMappable.objOrder.OrderHeader.GetPropertiesAsDictionary());
            objMappable.email_confirm = objMappable.email;
            //明細情報
            objMappable.orderRecords = new List<bill_details>();

            var isBeforeDeliverSpec = ErsFactory.ersOrderFactory.GetIsBeforeDeliverSpec();
            var isBeforeDeliver = true;

            var createOrderRecordKeyStgy = ErsFactory.ersOrderFactory.GetCreateOrderRecordKeyStgy();
            foreach (var recordData in objMappable.objOrder.OrderRecords.Values)
            {
                var record = new bill_details();
                record.key = createOrderRecordKeyStgy.GetKey(recordData, objMappable.objOrder.OrderHeader.senddate);
                record.OverwriteWithParameter(recordData.GetPropertiesAsDictionary());
                record.amount = recordData.GetAmount();
                objMappable.orderRecords.Add(record);

                objMappable.deliv_method = recordData.deliv_method;

                if (!isBeforeDeliverSpec.Determine(recordData.order_status.Value, recordData.shipdate))
                {
                    isBeforeDeliver = false;
                }
            }

            objMappable.isBeforeDeliver = isBeforeDeliver;
        }

        /// <summary>
        /// 確認画面値を読み込む
        /// </summary>
        public void LoadConfirmData(IBillModifyMappable objMappable)
        {
            var isBeforeDeliverSpec = ErsFactory.ersOrderFactory.GetIsBeforeDeliverSpec();
            var isBeforeDeliver = true;

            var createOrderRecordKeyStgy = ErsFactory.ersOrderFactory.GetCreateOrderRecordKeyStgy();
            foreach (var recordData in objMappable.objOrder.OrderRecords.Values)
            {
                if (objMappable.orderRecords != null)
                {
                    var key = createOrderRecordKeyStgy.GetKey(recordData, objMappable.objOrder.OrderHeader.senddate);
                    var record = objMappable.orderRecords.FirstOrDefault((bill_detail) => bill_detail.key == key);
                    record.OverwriteWithParameter(recordData.GetPropertiesAsDictionary(), "confirm");
                    record.amount = recordData.GetAmount();
                }

                if (!isBeforeDeliverSpec.Determine(recordData.order_status.Value, recordData.shipdate))
                {
                    isBeforeDeliver = false;
                    break;
                }
            }

            objMappable.isBeforeDeliver = isBeforeDeliver;
        }
    }
}