﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Regular.Mappables;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.regular.csv;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Regular.Mappers
{
    public class ShipmentListCSVMapper
        : ShipmentListMapper, IMapper<IShipmentListCSVMappable>
    {
        public void Map(IShipmentListCSVMappable objMappable)
        {
            CreateCsvFile(objMappable);
        }

        /// <summary>
        /// CSVファイルを作成する。
        /// </summary>
        /// <returns></returns>
        public void CreateCsvFile(IShipmentListCSVMappable objMappable)
        {
            var crtOrder = SetOrderCriteria(objMappable);

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(crtOrder);
            }

            objMappable.billList = new List<ErsOrderContainer>();
            var rpsOrder = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            foreach (var order in rpsOrder.Find(crtOrder))
            {
                var orderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order);
                
                var container = ErsFactory.ersOrderFactory.GetErsOrderContainer();
                container.OrderHeader = order;
                container.OrderRecords = orderRecords;
                objMappable.billList.Add(container);
            }

            var filename = "shipment_" + DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + ".csv";

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<Shipping_csv>(writer);
                int record_id_counter = 1;
                foreach (var container in objMappable.billList)
                {
                    var subd_no = ErsFactory.ersOrderFactory.GetObtainNewSubd_noStgy().Obtain(container.OrderRecords.Values);

                    var shipping_csv = new Shipping_csv();

                    shipping_csv.objOrder = container.OrderHeader;

                    var old_records = ErsFactory.ersOrderFactory.GetErsOrderRecordList(container.OrderHeader).Select(e => e.Value);

                    foreach (var objOrderRecord in container.OrderRecords.Values)
                    {
                        if (ErsOrderCriteria.CANCEL_STATUS_ARRAY.Contains(objOrderRecord.order_status.Value))
                        {
                            continue;
                        }

                        objOrderRecord.subd_no = subd_no;

                        //output data.
                        shipping_csv.objOrderRecord = objOrderRecord;
                        shipping_csv.id = record_id_counter++;
                        shipping_csv.d_no = objOrderRecord.d_no.Replace("-", "");
                        if (shipping_csv.delv_method.HasValue())
                        {
                            shipping_csv.delv_method = ErsResources.GetMessage(shipping_csv.delv_method);
                        }
                        objMappable.csvCreater.WriteBody(shipping_csv, writer);
                    }
                }
            }
        }
    }
}