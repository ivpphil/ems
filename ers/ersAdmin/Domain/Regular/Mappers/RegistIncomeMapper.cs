﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Regular.Mappables;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ersAdmin.Models;

namespace ersAdmin.Domain.Regular.Mappers
{
    public class RegistIncomeMapper : IMapper<IRegistIncomeMappable>
    {
        public void Map(IRegistIncomeMappable objMappable)
        {
            if(!objMappable.isErrorBack)            
            SearchList(objMappable);
            else
                ReloadDetails(objMappable);
        }

        /// <summary>
        /// 入金可能伝票検索
        /// </summary>
        public void SearchList(IRegistIncomeMappable objMappable)
        {
            var crtOrder = SetOrderCriteria();

            var rpsOrder = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            //件数
            objMappable.recordCount = rpsOrder.GetRecordCount(crtOrder);

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(crtOrder);
            }

            //表示用リスト作成
            var listOrder = rpsOrder.Find(crtOrder);

            var ret = new List<Regist_income_detail>();

            foreach (var item in listOrder)
            {
                var model = new Regist_income_detail();
                model.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                ret.Add(model);
            }

            objMappable.inputDetails = ret;
        }

        /// <summary>
        /// 入金可能伝票検索実体
        /// </summary>
        /// <returns></returns>
        protected virtual ErsOrderCriteria SetOrderCriteria()
        {

            var crtOrder = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

            //カード、代引、コンビニ以外
            crtOrder.pay_not_in = new[] { EnumPaymentType.CREDIT_CARD, EnumPaymentType.CASH_ON_DELIVERY, EnumPaymentType.CONVENIENCE_STORE };
            //キャンセル以外
            crtOrder.order_status_not_in = crtOrder.CancelStatusArray;
            //入金済み以外
            crtOrder.order_payment_status_not_in = new[] { EnumOrderPaymentStatusType.PAID };


            //入金金額がnull
            var criteriaForNullPaidPrice = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteriaForNullPaidPrice.paid_price = null;

            //入金金額と合計金額が異なる
            var criteriaForComparingPrices = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteriaForComparingPrices.SetCompare_paid_price_and_total();

            //上の2条件をOrで結合
            var criteria = Criteria.JoinWithOR(new[] { criteriaForNullPaidPrice, criteriaForComparingPrices });


            //作成したOr条件を結合
            crtOrder.Add(criteria);

            crtOrder.ignoreMonitor();

            return crtOrder;

        }

        /// <summary>
        /// 入力値受信用のinputDetailsを、listDetailsにコンバート（エラー時の戻り用）
        /// </summary>
        internal void ReloadDetails(IRegistIncomeMappable objMappable)
        {
            foreach (var item in objMappable.inputDetails)
            {
                var order = ErsFactory.ersOrderFactory.GetOrderWithD_no(item.d_no);
                if (order == null)
                {
                    throw new ErsException("30104", item.d_no);
                }

                item.d_no = order.d_no;
                item.total = order.total;
                item.lname = order.lname;
                item.fname = order.fname;
            }
        }
    }
}