﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Regular.Mappables;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Regular.Mappers
{
    public class BillSearchMapper
        : SiteSearchBaseMapper, IMapper<IBillSearchMappable>
    {
        public void Map(IBillSearchMappable objMappable)
        {
            if (objMappable.IsBillSearchPage)
            {
                SetDefaultData(objMappable);
                return;
            }

            SearchList(objMappable);
        }

        /// <summary>
        /// 初期値を設定する。
        /// </summary>
        private void SetDefaultData(IBillSearchMappable objMappable)
        {
            if (objMappable.s_bill_intime_from == null)
            {
                objMappable.s_bill_intime_from = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/01 00:00:00"));
            }
            if (objMappable.s_bill_intime_to == null)
            {
                objMappable.s_bill_intime_to = Convert.ToDateTime(DateTime.Now.AddMonths(1).ToString("yyyy/MM/01 23:59:59")).AddDays(-1);
            }
        }

        /// <summary>
        /// 伝票検索（通常）
        /// </summary>
        private void SearchList(IBillSearchMappable objMappable)
        {
            var crtOrder = SetOrderCriteria(objMappable);

            //件数
            var rpsOrder = ErsFactory.ersOrderFactory.GetErsOrderRepository();

            objMappable.recordCount = rpsOrder.GetRecordCount(crtOrder);
            if(objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(crtOrder);
            }

            objMappable.orderList = GetListForView(crtOrder, rpsOrder);

        }

        /// <summary>
        /// 伝票検索実体
        /// </summary>
        /// <returns></returns>
        protected ErsOrderCriteria SetOrderCriteria(IBillSearchMappable objMappable)
        {
            ErsOrderCriteria crtOrder = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, crtOrder, "d_master_t");

            // 伝票番号
            if (!String.IsNullOrEmpty(objMappable.s_d_no))
            {
                var d_no = objMappable.s_d_no;
                if (d_no.Length > 8)
                {
                    d_no = d_no.Substring(0, 8) + "-" + d_no.Substring(8);
                }
                crtOrder.d_no_like = d_no;
            }

            // モール伝票番号
            if (!String.IsNullOrEmpty(objMappable.s_mall_d_no))
            {
                crtOrder.mall_d_no = objMappable.s_mall_d_no;
            }

            // 発行期間
            if (objMappable.s_bill_intime_from.HasValue)
            {
                objMappable.s_bill_intime_from = ErsCommon.SetDateTimeFormat(objMappable.s_bill_intime_from.Value, 0);
                crtOrder.intime_from = objMappable.s_bill_intime_from.Value;
            }
            if (objMappable.s_bill_intime_to.HasValue)
            {
                objMappable.s_bill_intime_to = ErsCommon.SetDateTimeFormat(objMappable.s_bill_intime_to.Value, 1);
                crtOrder.intime_to = objMappable.s_bill_intime_to.Value;
            }

            // 入金期間
            if (objMappable.s_paid_date_from.HasValue)
            {
                objMappable.s_paid_date_from = ErsCommon.SetDateTimeFormat(objMappable.s_paid_date_from.Value, 0);
                crtOrder.paid_date1 = objMappable.s_paid_date_from.Value;
            }
            if (objMappable.s_paid_date_to.HasValue)
            {
                objMappable.s_paid_date_to = ErsCommon.SetDateTimeFormat(objMappable.s_paid_date_to.Value, 1);
                crtOrder.paid_date2 = objMappable.s_paid_date_to.Value;
            }

            // 発送日
            if (objMappable.s_shipdate_from.HasValue)
            {
                objMappable.s_shipdate_from = ErsCommon.SetDateTimeFormat(objMappable.s_shipdate_from.Value, 0);
                crtOrder.shipdate1 = objMappable.s_shipdate_from.Value;
            }
            if (objMappable.s_shipdate_to.HasValue)
            {
                objMappable.s_shipdate_to = ErsCommon.SetDateTimeFormat(objMappable.s_shipdate_to.Value, 1);
                crtOrder.shipdate2 = objMappable.s_shipdate_to.Value;
            }

            // 返品日
            if (objMappable.s_af_cancel_date_from.HasValue)
            {
                objMappable.s_af_cancel_date_from = ErsCommon.SetDateTimeFormat(objMappable.s_af_cancel_date_from.Value, 0);
                crtOrder.af_cancel_date_from = objMappable.s_af_cancel_date_from.Value;
            }
            if (objMappable.s_af_cancel_date_to.HasValue)
            {
                objMappable.s_af_cancel_date_to = ErsCommon.SetDateTimeFormat(objMappable.s_af_cancel_date_to.Value, 1);
                crtOrder.af_cancel_date_to = objMappable.s_af_cancel_date_to.Value;
            }

            // キャンセル日
            if (objMappable.s_cancel_date_from.HasValue)
            {
                objMappable.s_cancel_date_from = ErsCommon.SetDateTimeFormat(objMappable.s_cancel_date_from.Value, 0);
                crtOrder.cancel_date_from = objMappable.s_cancel_date_from.Value;
            }
            if (objMappable.s_cancel_date_to.HasValue)
            {
                objMappable.s_cancel_date_to = ErsCommon.SetDateTimeFormat(objMappable.s_cancel_date_to.Value, 1);
                crtOrder.cancel_date_to = objMappable.s_cancel_date_to.Value;
            }

            // 商品番号
            if (!String.IsNullOrEmpty(objMappable.s_scode))
            {
                crtOrder.scode = objMappable.s_scode;
            }

            // 販売区分
            if (objMappable.s_order_type.HasValue)
            {
                crtOrder.order_type = objMappable.s_order_type;
            }

            // 対応区分
            if (objMappable.s_order_status.HasValue)
            {
                crtOrder.order_status_in = new[] { objMappable.s_order_status.Value };
            }

            // 入金区分
            if (objMappable.s_order_payment_status.HasValue)
            {
                crtOrder.order_payment_status_in = new[] { objMappable.s_order_payment_status.Value };
            }

            // メールアドレス
            if (!String.IsNullOrEmpty(objMappable.s_email))
            {
                crtOrder.email = objMappable.s_email;
            }

            // 支払い方法
            if (objMappable.s_pay.HasValue)
            {
                crtOrder.pay_in = new[] { objMappable.s_pay.Value };
            }

            // 購入サイト区分
            if (objMappable.s_pm_flg.HasValue)
            {
                if (objMappable.s_pm_flg == EnumPmFlg.CTS)
                {
                    crtOrder.SetPmFlgCts();
                }
                else
                {
                    crtOrder.pm_flg = objMappable.s_pm_flg.Value;
                }
            }

            if (objMappable.s_ccode.HasValue())
            {
                crtOrder.ccode = objMappable.s_ccode;
            }

            if (objMappable.s_continual_biling)
            {
                crtOrder.SetContinualBilingOnly();
            }

            crtOrder.ignoreMonitor();

            return crtOrder;

        }

        /// <summary>
        /// Get list for view
        /// </summary>
        /// <param name="crtOrder"></param>
        /// <param name="rpsOrder"></param>
        /// <returns></returns>
        protected IEnumerable<Dictionary<string, object>> GetListForView(ErsOrderCriteria crtOrder, ErsOrderRepository rpsOrder)
        {
            //表示用リスト作成
            var orderList = rpsOrder.Find(crtOrder);

            var obtainOrderStatusStgy = ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy();

            foreach (var order in orderList)
            {
                var orderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order);

                var dictionary = order.GetPropertiesAsDictionary();
                var order_status = obtainOrderStatusStgy.ObtainFrom(orderRecords.Values);

                if (order_status.HasValue)
                {
                    dictionary["w_deliv_method"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.DelvMethod, EnumCommonNameColumnName.namename, orderRecords.Values.Min(e => (int)e.deliv_method));
                    dictionary["w_order_status"] = ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().GetStringFromId(order_status);
                    dictionary["w_order_payment_status"] = ErsFactory.ersViewServiceFactory.GetErsViewOrderPaymentStatusService().GetStringFromId(order.order_payment_status);
                    dictionary["pay_name"] = ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(order.pay, order.site_id);
                    var orderType = ErsFactory.ersOrderFactory.GetObtainOrderTypeStgy().Obtain(orderRecords.Values);
                    dictionary["order_type_name"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.OrderType, EnumCommonNameColumnName.namename, (int)orderType);
                }
                yield return dictionary;
            }
        }
    }
}