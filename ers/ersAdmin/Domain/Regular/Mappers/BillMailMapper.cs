﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Regular.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersAdmin.Domain.Regular.Mappers
{
    public class BillMailMapper:IMapper<IBillMailMappable>
    {

        public void Map(IBillMailMappable ObjMappable)
        {
            this.Init(ObjMappable);
            if (ObjMappable.IsLoadDefault)
            {
                this.LoadDefaultValue(ObjMappable);
            }
        }

        /// <summary>
        /// 初期化を行う
        /// </summary>
        public void Init(IBillMailMappable ObjMappable)
        {
            ObjMappable.objOrder = ErsFactory.ersOrderFactory.GetOrderWithD_no(ObjMappable.d_no);
        }

        /// <summary>
        /// 初期値を読み込む。
        /// </summary>
        public void LoadDefaultValue(IBillMailMappable ObjMappable)
        {
            var objSendmail = ErsFactory.ersMailFactory.getErsSendMailAdminIndividual(ObjMappable.objOrder.site_id);
            objSendmail.Init(ObjMappable, EnumMformat.PC);

            ObjMappable.mail_title = objSendmail.title;
            ObjMappable.mail_body = objSendmail.body;
            ObjMappable.email = ObjMappable.objOrder.email;

            switch (ObjMappable.objOrder.mall_shop_kbn)
            {
                case EnumMallShopKbn.ERS:
                    {
                        var setup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(ObjMappable.objOrder.site_id.Value);
                        ObjMappable.from_email = setup.r_email;
                        break;
                    }

                case EnumMallShopKbn.YAHOO:
                    {
                        var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
                        ObjMappable.from_email = mallSetup.GetYahooMailFrom(ObjMappable.objOrder.site_id.Value);
                        break;
                    }

                case EnumMallShopKbn.RAKUTEN:
                    {
                        var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
                        ObjMappable.from_email = mallSetup.GetRakutenMailFrom(ObjMappable.objOrder.site_id.Value);
                        break;
                    }

                case EnumMallShopKbn.AMAZON:
                    {
                        var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
                        ObjMappable.from_email = mallSetup.GetAmazonMailFrom(ObjMappable.objOrder.site_id.Value);
                        break;
                    }
            }
        }
    }
}