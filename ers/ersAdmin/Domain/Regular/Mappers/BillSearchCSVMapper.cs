﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Regular.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.csv;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Regular.Mappers
{
    public class BillSearchCSVMapper
        : BillSearchMapper, IMapper<IBillSearchCSVMappable>
    {
        public void Map(IBillSearchCSVMappable objMappable)
        {
            CreateCsvFile(objMappable);
        }

        /// <summary>
        /// CSVファイルを作成する。
        /// </summary>
        /// <returns></returns>
        public virtual void CreateCsvFile(IBillSearchCSVMappable objMappable)
        {
            var crtOrder = SetOrderCriteria(objMappable);

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(crtOrder);
            }

            //crtOrder.ds_doc_bundling_flg = EnumDocBundlingFlg.OFF;
            var rpsOrder = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            //           var rpsOrder = ErsFactory.ersRegularOrderFactory.GetErsRegularOrderRepository();
            var billList = rpsOrder.Find(crtOrder);

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            Bill_csv bill_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<Bill_csv>(writer);
                foreach (var order in billList)
                {
                    var orderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order);

                    foreach (var orderRecord in orderRecords.Values)
                    {
                        bill_csv = new Bill_csv();
                        bill_csv.objOrder = order;
                        bill_csv.objOrderRecord = orderRecord;
                        string w_send = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.SendTo, EnumCommonNameColumnName.namename, (int)order.send.Value);
                        if (w_send.HasValue())
                        {
                            bill_csv.send = ErsResources.GetMessage(w_send);
                        }
                        bill_csv.senddate = Convert.ToString(order.senddate);
                        bill_csv.sendtime = ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId(order.sendtime);
                        bill_csv.c_req_no = ErsFactory.ersOrderFactory.GetErsPayment(order.pay).GetTransactionStringForView(order);
                        bill_csv.add_zip = order.add_zip;
                        bill_csv.add_pref = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(order.add_pref, order.site_id);
                        bill_csv.add_address = order.add_address;
                        bill_csv.subd_no = orderRecord.subd_no;
                        bill_csv.point = Convert.ToString(orderRecord.point);
                        bill_csv.wrap = Convert.ToString(order.memo2);
                        string w_order_type = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.OrderType, EnumCommonNameColumnName.namename, (int?)orderRecord.order_type);
                        if (w_order_type.HasValue())
                        {
                            bill_csv.order_type = ErsResources.GetMessage(w_order_type);
                        }
                        string w_order_status = ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().GetStringFromId(orderRecord.order_status);
                        if (w_order_status.HasValue())
                        {
                            bill_csv.order_status = ErsResources.GetMessage(w_order_status);
                        }
                        string w_pm_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.PmFlg, EnumCommonNameColumnName.namename, (int?)order.pm_flg);
                        if (w_pm_flg.HasValue())
                        {
                            bill_csv.pm_flg = ErsResources.GetMessage(w_pm_flg);
                        }
                        objMappable.csvCreater.WriteBody(bill_csv, writer);

                    }
                }
            }

        }
    }
}