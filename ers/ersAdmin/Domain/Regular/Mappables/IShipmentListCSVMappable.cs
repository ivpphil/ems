﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;

namespace ersAdmin.Domain.Regular.Mappables
{
    public interface IShipmentListCSVMappable
        : IMappable, IShipmentListMappable
    {
        ErsCsvCreater csvCreater { get; }

        IList<ErsOrderContainer> billList { set; get; }
    }
}