﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.order;
using ersAdmin.Models.regular;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Regular.Mappables
{
    public interface IBillModifyMappable : IMappable
    {
        ErsOrderContainer objOrder { get; set; }

        List<bill_details> orderRecords { get; set; }

        string d_no { get; set; }

        string email { get; set; }

        string email_confirm { get; set; }

        EnumDelvMethod? deliv_method { get; set; }

        bool IsLoadDefaultValue { get; set; }

        bool isBeforeDeliver { get; set; }
    }
}