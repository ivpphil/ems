﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Regular.Mappables
{
    public interface IBillSearchMappable
        : ISiteSearchBaseMappable, IMappable
    {
        bool IsBillSearchPage { get; }

        DateTime? s_bill_intime_from { get; set; }

        DateTime? s_bill_intime_to { get; set; }

        long recordCount { get;  set; }

        ErsPagerModel pager { get; }

        IEnumerable<Dictionary<string, object>> orderList { set; get; }

        string s_d_no { get; }

        DateTime? s_paid_date_from { get; set; }

        DateTime? s_paid_date_to { get; set; }

        DateTime? s_shipdate_from { get; set; }

        DateTime? s_shipdate_to { get; set; }

        DateTime? s_af_cancel_date_from { get; set; }

        DateTime? s_af_cancel_date_to { get; set; }

        DateTime? s_cancel_date_from { get; set; }

        DateTime? s_cancel_date_to { get; set; }

        string s_scode { get; }

        EnumOrderType? s_order_type { get; set; }

        EnumOrderStatusType? s_order_status { get; }

        EnumOrderPaymentStatusType? s_order_payment_status { get; }

        string s_email { get; }

        EnumPaymentType? s_pay { get; set;}

        EnumPmFlg? s_pm_flg { get; }

        string s_ccode { get; }

        string s_mall_d_no { get; set; }

        bool s_continual_biling { get; set; }
    }
}