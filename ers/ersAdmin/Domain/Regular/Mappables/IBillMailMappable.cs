﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Regular.Mappables
{
    public interface  IBillMailMappable:IMappable
    {
        ErsOrder objOrder { get; set; }

        string d_no { get; }

        string mail_title { set; }

        string mail_body {  set; }

        string from_email {   set; }


        bool IsLoadDefault { get; set; }

        string email { get; set; }

        bool IsFromEmailFixed { get; }
    }
}