﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using ersAdmin.Models;

namespace ersAdmin.Domain.Regular.Mappables
{
    public interface IRegistIncomeMappable : IMappable
    {
        long recordCount { get; set; }
        ErsPagerModel pager { get; set; }

        List<Regist_income_detail> inputDetails { get; set; }

        bool isErrorBack { get; set; }
    }
}