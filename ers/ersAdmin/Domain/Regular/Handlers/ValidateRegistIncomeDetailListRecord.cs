﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Regular.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class ValidateRegistIncomeDetailListRecord : IValidationHandler<IRegistIncomeDetailListRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IRegistIncomeDetailListRecordCommand command)
        {
            if (Convert.ToInt32(command.paid_check) == 1)
            {
                yield return command.CheckRequired("paid_price");
                yield return command.CheckRequired("paid_date");
            }
        }
    }
}