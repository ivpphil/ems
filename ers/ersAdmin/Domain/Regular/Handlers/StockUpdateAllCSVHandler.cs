﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Regular.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.csv;
using jp.co.ivp.ers.merchandise.stock;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class StockUpdateAllCSVHandler : ICommandHandler<IStockUpdateAllCSVCommand>
    {
        public ICommandResult Submit(IStockUpdateAllCSVCommand command)
        {
            //update stock
            Update(command);
            return new CommandResult(true);
        }

        /// <summary>
        /// アップロードデータの登録
        /// </summary>
        internal void Update(IStockUpdateAllCSVCommand command)
        {
            //リポジトリ インスタンス化
            var repository = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockRepository();

            //Integrate stock_t.stock and s_master_t.stock to s_master_t.stock.
            ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetStockIntegrationSpec().IntegrateStock();

            var listParam = new List<UpdateStockParam>();

            //アップロードデータ登録
            foreach (var model in command.csv_file.GetValidModels())
            {
                var objOldStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(model.scode);

                var objNewStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(model.scode);
                objNewStock.stock = Convert.ToInt32(model.stock);

                repository.Update(objOldStock, objNewStock);

                var objSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(model.scode);
                // モール在庫用 [For mall stock]
                if (objOldStock.stock != objNewStock.stock &&
                    (objSku.h_mall_flg == EnumOnOff.On))
                {
                    UpdateStockParam param = default(UpdateStockParam);

                    param.productCode = model.scode;
                    param.quantity = model.stock;
                    param.operation = EnumMallStockOperation.set;

                    listParam.Add(param);
                }
            }

            // モール在庫更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }
        }
    }
}