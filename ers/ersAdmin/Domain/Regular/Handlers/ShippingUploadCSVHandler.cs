﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Regular.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using ersAdmin.Models.csv;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.api.order_status;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class ShippingUploadCSVHandler : ICommandHandler<IShippingUploadCSVCommand>
    {
        public ICommandResult Submit(IShippingUploadCSVCommand command)
        {
            this.Update(command);

            return new CommandResult(true);
        }

        internal void Update(IShippingUploadCSVCommand command)
        {
            foreach (var item in command.csv_file.GetValidModels())
            {
                var order = ErsFactory.ersOrderFactory.GetOrderWithD_no(item.d_no);
                var newOrderRecordList = this.GetOrderRecords(item);

                //begin transaction in handler in order to handle the sending mail error.
                using (var tx = ErsDB_parent.BeginTransaction())
                {
                    this.UpdateStatus(newOrderRecordList, item.sendno, item.shipdate, order);

                    this.DecreaseWhStock(newOrderRecordList);

                    tx.Commit();
                }
            }
        }

        /// <summary>
        /// 倉庫在庫減算処理
        /// </summary>
        /// <param name="orderRecordList"></param>
        private void DecreaseWhStock(IList<ErsOrderRecord> orderRecordList)
        {
            var decreaseWhStockStgy = ErsFactory.ersWarehouseFactory.GetDecreaseWhStockStgy();
            foreach (var orderRecord in orderRecordList)
            {
                decreaseWhStockStgy.Decrease(orderRecord.scode, orderRecord.GetAmount(),null);
                decreaseWhStockStgy.Decrease(orderRecord.scode, orderRecord.GetAmount(), EnumShelfNumber.SHELF001);
                InsertWhMove(orderRecord);
            }
        }

        internal void InsertWhMove(ErsOrderRecord item)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhMoveRepository();
            var entity = ErsFactory.ersWarehouseFactory.GetErsWhMove();
            entity.scode = item.scode;
            entity.maker_scode = item.maker_scode;
            entity.reason = "出庫";
            entity.shelf_from = EnumShelfNumber.SHELF001;

            entity.shelf_to = null;
            entity.amount = item.amount;

            entity.move_type = EnumWhMoveType.Shipping;
            entity.active = EnumActive.Active;

            repository.Insert(entity);
        }

        /// <summary>
        /// 明細の一覧を取得
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private IList<ErsOrderRecord> GetOrderRecords(Shipping_csv_upload_record item)
        {
            var orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var orderRecordCriteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
            orderRecordCriteria.d_no= item.d_no;
            
            if(!string.IsNullOrEmpty(item.subd_no))
                orderRecordCriteria.subd_no = item.subd_no;

            orderRecordCriteria.order_status_not_in = ErsOrderCriteria.CANCEL_STATUS_ARRAY;
            return orderRecordRepository.Find(orderRecordCriteria);
        }

        /// <summary>
        /// 配送情報を更新する。
        /// </summary>
        /// <param name="setupRepository"></param>
        /// <param name="item"></param>
        protected void UpdateStatus(IList<ErsOrderRecord> orderRecordList, string send_no, DateTime? shipdate, ErsOrder order)
        {
            var orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var old_recors = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order).Select(e => e.Value);

            foreach (var orderRecord in orderRecordList)
            {
                ErsFactory.ersOrderFactory.GetOrderStatusSetDeliverdStgy().SetStatus(orderRecord, send_no, shipdate);

                var old_record = ErsFactory.ersOrderFactory.GetErsOrderRecordWithId(orderRecord.id);
                orderRecordRepository.Update(old_record, orderRecord);

                if (old_record.order_status != orderRecord.order_status)
                {
                    var new_records = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order).Select(e => e.Value);

                    // 受注ステータス更新履歴登録 [Regist order status history]
                    ErsFactory.ersOrderFactory.GetRegistOrderRecordStatusHistoryStgy().
                        Regist(old_record, orderRecord, order, old_recors, order, new_records);
                }
            }
        }
    }
}