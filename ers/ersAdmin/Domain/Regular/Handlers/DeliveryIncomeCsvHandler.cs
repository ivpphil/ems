﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Regular.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ersAdmin.Models.csv;
namespace ersAdmin.Domain.Regular.Handlers
{
    public class DeliveryIncomeCsvHandler : ICommandHandler<IDeliveryIncomeCsvCommand>
    {
        public ICommandResult Submit(IDeliveryIncomeCsvCommand command)
        {
            Update(command);
            return new CommandResult(true);
        }
       
        internal void Update(IDeliveryIncomeCsvCommand command)
        {
                foreach (var item in command.csv_file.GetValidModels())
                {
                    //更新                   
                    UpdateIncome(item);
                }
        }

        /// <summary>
        /// 入金情報を更新する。
        /// </summary>
        /// <param name="setupRepository"></param>
        /// <param name="item"></param>
        protected void UpdateIncome(Delivery_income_csv item)
        {
            var new_order = ErsFactory.ersOrderFactory.GetOrderWithD_no(item.d_no);
            if (new_order == null)
            {
                throw new ErsException("30104", item.d_no);
            }

            new_order.order_payment_status = EnumOrderPaymentStatusType.PAID;
            new_order.paid_date = DateTime.Now;
            new_order.paid_price = item.paid_price;

            var old_order = ErsFactory.ersOrderFactory.GetOrderWithD_no(item.d_no);

            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();

            repository.Update(old_order, new_order);
        }
    }
}