﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Regular.Commands;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class RegistIncomeHandler : ICommandHandler<IRegistIncomeCommand>
    {
        public ICommandResult Submit(IRegistIncomeCommand command)
        {
            //update regist_income
            Update(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// 伝票の入金情報更新
        /// </summary>
        internal void Update(IRegistIncomeCommand command)
        {
            foreach (var item in command.inputDetails)
            {
                if (item.paid_check)
                {

                    var order = ErsFactory.ersOrderFactory.GetOrderWithD_no(item.d_no);
                    if (order == null)
                    {
                        throw new ErsException("30104", item.d_no);
                    }

                    var old_order = ErsFactory.ersOrderFactory.GetOrderWithD_no(item.d_no);
                    if (old_order == null)
                    {
                        throw new ErsException("30104", item.d_no);
                    }

                    var orderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order);

                    //入金ステータスを変更せずに入金日、入金金額を更新するパターンがあるので、先に設定しておく
                    order.paid_date = item.paid_date;
                    order.paid_price = item.paid_price;

                    //ステータス変更が必要かどうかチェック
                    var otherModifyOrderPaymentStatusStrategy = ErsFactory.ersOrderFactory.GetOtherModifyOrderPaymentStatusStrategy();
                    otherModifyOrderPaymentStatusStrategy.SetStatus(order, orderRecords.Values, EnumOrderPaymentStatusType.PAID);

                    var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
                    orderRepository.Update(old_order, order);
                }
            }
        }
    }
}