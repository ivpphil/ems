﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Regular.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class ValidateBillMail:IValidationHandler<IBillMailCommand>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(IBillMailCommand command)
        {
            yield return command.CheckRequired("d_no");

            var objOrder = !string.IsNullOrEmpty(command.d_no) ? ErsFactory.ersOrderFactory.GetOrderWithD_no(command.d_no) : null;

            if (objOrder == null)
            {
                throw new ErsException("30104", command.d_no);
            }

            if (command.send_btn)
            {
                yield return command.CheckRequired("email");
                yield return command.CheckRequired("from_email");
                yield return command.CheckRequired("mail_title");
                yield return command.CheckRequired("mail_body");

                if (!string.IsNullOrEmpty(command.from_email) && objOrder != null)
                {
                    yield return ErsMallFactory.ersMallMailFactory.GetValidateMallMailFromStgy().Validate(objOrder.site_id, command.from_email, "from_email");
                }
            }
        }
    }
}