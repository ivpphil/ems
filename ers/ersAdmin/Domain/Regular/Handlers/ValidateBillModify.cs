﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Regular.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class ValidateBillModify : IValidationHandler<IBillModifyCommand>
    {
        /// <summary>
        /// フィールドチェック後の複合チェック
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(IBillModifyCommand command)
        {
            yield return command.CheckRequired("d_no");
            ErsOrder order = null;
            if (command.IsValidField("d_no"))
            {
                order = ErsFactory.ersOrderFactory.GetOrderWithD_no(command.d_no);
                if (order == null)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("29003", command.d_no), new[] { "d_no" });
                }
            }

            if (command.bill_change)
            {
                if (command.orderRecords != null)
                {
                    foreach (var model in command.orderRecords)
                    {
                        if (!model.IsValid)
                        {
                            foreach (var errorMessage in model.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "orderRecords" });
                            }
                        }
                    }
                }

                foreach (var result in this.ValidateBillChange(command))
                {
                    yield return result;
                }
            }

            if (command.bill_cancel)
            {
                if (order != null)
                {
                    var isBeforeDeliverSpec = ErsFactory.ersOrderFactory.GetIsBeforeDeliverSpec();
                    var orderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order);
                    foreach (var record in orderRecords.Values)
                    {
                        if (!isBeforeDeliverSpec.Determine(record.order_status.Value, record.shipdate))
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("63110"), new[] { "d_no" });
                            break;
                        }
                    }
                }
            }
        }

        protected virtual IEnumerable<ValidationResult> ValidateBillChange(IBillModifyCommand command)
        {
            var isMall = (!string.IsNullOrEmpty(command.mall_d_no));

            yield return command.CheckRequired("send");
            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("send", EnumCommonNameType.SendTo, (int?)command.send);

            if (!isMall || (string.IsNullOrEmpty(command.lname) && string.IsNullOrEmpty(command.fname)))
            {
                yield return command.CheckRequired("lname");
                yield return command.CheckRequired("fname");
            }
            if (!isMall || (string.IsNullOrEmpty(command.lnamek) && string.IsNullOrEmpty(command.fnamek)))
            {
                yield return command.CheckRequired("lnamek");
                yield return command.CheckRequired("fnamek");
            }

            yield return command.CheckRequired("zip");
            yield return command.CheckRequired("pref");
            foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("pref", command.pref, false))
            {
                yield return result;
            }
            if (command.IsValidField("zip", "pref"))
            {
                yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("zip", command.zip, "pref", command.pref, false);
            }

            yield return command.CheckRequired("address");

            if (!isMall)
            {
                yield return command.CheckRequired("taddress");
            }

            yield return command.CheckRequired("tel");
            yield return command.CheckRequired("deliv_method");

            if (command.IsValidField("d_no"))
            {
                var order = ErsFactory.ersOrderFactory.GetOrderWithD_no(command.d_no);
                if (!string.IsNullOrEmpty(order.mcode))
                {
                    var member = ErsFactory.ersMemberFactory.getErsMemberWithMcodeForAdmin(order.mcode);
                    if (member.pm_flg != EnumPmFlg.CTS)
                    {
                        yield return command.CheckRequired("email");
                    }
                }
            }
            yield return ErsFactory.ersMemberFactory.GetCheckEmailConfirmStgy().CheckEmailConfirm(command.email, command.email_confirm);

            foreach (var result in ErsFactory.ersOrderFactory.GetValidateSendtimeStgy().Validate(command.sendtime))
            {
                yield return result;
            }

            //配送番号
            if (command.send == EnumSendTo.ANOTHER_ADDRESS)
            {
                if (!isMall || (string.IsNullOrEmpty(command.add_lname) && string.IsNullOrEmpty(command.add_fname)))
                {
                    yield return command.CheckRequired("add_lname");
                    yield return command.CheckRequired("add_fname");
                }

                yield return command.CheckRequired("add_tel");
                yield return command.CheckRequired("add_zip");
                yield return command.CheckRequired("add_pref");
                foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("add_pref", command.add_pref, false))
                {
                    yield return result;
                }
                if (command.IsValidField("add_zip", "add_pref"))
                {
                    yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("add_zip", command.add_zip, "add_pref", command.add_pref, false);
                }
                
                yield return command.CheckRequired("add_address");

                if (!isMall)
                {
                    yield return command.CheckRequired("add_taddress");
                }
            }

            yield return command.CheckRequired("carriage");
            yield return command.CheckRequired("p_service");
            yield return command.CheckRequired("coupon_discount");
            yield return command.CheckRequired("etc");
            yield return command.CheckRequired("total");

            yield return command.CheckRequired("order_payment_status");
        }
    }
}