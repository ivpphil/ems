﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Regular.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class ShipmentListCSVHandler
        : ICommandHandler<IShipmentListCSVCommand>
    {
        public ICommandResult Submit(IShipmentListCSVCommand command)
        {
            Update(command);
            return new CommandResult(true);
        }


        private void Update(IShipmentListCSVCommand command)
        {
            var orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();

            foreach (var container in command.billList)
            {
                var old_records = ErsFactory.ersOrderFactory.GetErsOrderRecordList(container.OrderHeader).Select(e => e.Value);

                foreach (var objOrderRecord in container.OrderRecords.Values)
                {
                    //update status.
                    if (ErsOrderCriteria.CANCEL_STATUS_ARRAY.Contains(objOrderRecord.order_status.Value))
                    {
                        continue;
                    }

                    ErsFactory.ersOrderFactory.GetOrderStatusSetDeliverRequestStgy().SetStatus(objOrderRecord);

                    var old_record = ErsFactory.ersOrderFactory.GetErsOrderRecordWithId(objOrderRecord.id);

                    orderRecordRepository.Update(old_record, objOrderRecord);

                    if (old_record.order_status != objOrderRecord.order_status)
                    {
                        var new_records = ErsFactory.ersOrderFactory.GetErsOrderRecordList(container.OrderHeader).Select(e => e.Value);

                        // 受注ステータス更新履歴登録 [Regist order status history]
                        ErsFactory.ersOrderFactory.GetRegistOrderRecordStatusHistoryStgy().
                            Regist(old_record, objOrderRecord, container.OrderHeader, old_records, container.OrderHeader, new_records);
                    }
                }
            }
        }
    }
}