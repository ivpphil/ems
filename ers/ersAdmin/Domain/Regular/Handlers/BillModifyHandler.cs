﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Regular.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class BillModifyHandler
        : ICommandHandler<IBillModifyCommand>
    {
        public ICommandResult Submit(IBillModifyCommand command)
        {
            if (command.bill_change)
            {
                using (var tx = ErsDB_parent.BeginTransaction())
                {
                    this.Update(command);

                    tx.Commit();
                }
            }
            else if (command.bill_cancel)
            {
                this.Cancel(command);
            }

            return new CommandResult(true);
        }

        /// <summary>
        /// 伝票キャンセル
        /// </summary>
        /// <param name="command"></param>
        private void Cancel(IBillModifyCommand command)
        {
            var objOrder = ErsFactory.ersOrderFactory.GetOrderWithD_no(command.d_no);
            var objOldOrder = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(objOrder.GetPropertiesAsDictionary());
            var objOrderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(objOrder);
            var old_records = ErsFactory.ersOrderFactory.GetErsOrderRecordList(objOrder).Select(e => e.Value);

            using (var tx = ErsDB_parent.BeginTransaction())
            {
                //顧客の調整
                var ersMember = ErsFactory.ersMemberFactory.getErsMemberWithMcodeForAdmin(objOrder.mcode);
                if (ersMember != null && ersMember.deleted != EnumDeleted.Deleted)
                {
                    var oldMember = ErsFactory.ersMemberFactory.GetErsMember();
                    oldMember.OverwriteWithParameter(ersMember.GetPropertiesAsDictionary());

                    if (objOrder.p_service > 0)
                    {
                        //ポイントの返却
                        //Execute some operation if the money of this new order has changed.
                        var reason = ErsResources.GetFieldName("point_reason_bill_modify");
                        ErsFactory.ersOrderFactory.GetUpdateMemberPointStgy().Increase(objOrder.mcode, objOrder.p_service, reason, objOrder.d_no, objOrder.intime, (int)objOrder.site_id);
                    }

                    //購入情報変更
                    ErsFactory.ersOrderFactory.GetUpdateMemberDataStgy().Update(ersMember, -objOrder.subtotal, -1);

                    //顧客DB更新
                    var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
                    repository.Update(oldMember, ersMember);
                }

                //ヘッダの金額をすべて0円にする
                var dRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
                objOrder.total = 0;
                objOrder.subtotal = 0;
                objOrder.carriage = 0;
                objOrder.etc = 0;
                objOrder.wrap_cost = 0;
                objOrder.p_service = 0;
                objOrder.coupon_discount = 0;
                objOrder.tax = 0;
                dRepository.Update(objOldOrder, objOrder);

                //明細の調整
                var dsRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
                var orderCancelStrategy = ErsFactory.ersOrderFactory.GetOrderCancelStrategy();
                var increaseStockStgy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetIncreaseStockStgy();
                var listParam = new List<UpdateStockParam>();
                var cancelStatus = new[] { EnumOrderStatusType.CANCELED, EnumOrderStatusType.CANCELED_AFTER_DELIVER };
                foreach (var record in objOrderRecords.Values)
                {
                    if (cancelStatus.Contains(record.order_status.Value))
                    {
                        //キャンセル済みは処理しない
                        continue;
                    }

                    var oldRecord = ErsFactory.ersOrderFactory.GetErsOrderRecord();
                    oldRecord.OverwriteWithParameter(record.GetPropertiesAsDictionary());

                    var amount = record.GetAmount();
                    record.order_status = EnumOrderStatusType.CANCELED;

                    orderCancelStrategy.CancelRecord(record, amount, true);

                    //明細ＤＢ更新
                    dsRepository.Update(oldRecord, record);

                    if (oldRecord.order_status != record.order_status)
                    {
                        var new_records = ErsFactory.ersOrderFactory.GetErsOrderRecordList(objOrder).Select(e => e.Value);

                        // 受注ステータス更新履歴登録 [Regist order status history]
                        ErsFactory.ersOrderFactory.GetRegistOrderRecordStatusHistoryStgy().
                            Regist(oldRecord, record, objOldOrder, old_records, objOrder, new_records);
                    }

                    //refs #23910 return product must not update stock when canceled after delivery
                    if (!((record.order_status == EnumOrderStatusType.CANCELED_AFTER_DELIVER || record.order_status == EnumOrderStatusType.CANCELED) && (oldRecord.order_status == EnumOrderStatusType.DELIVER_REQUEST || oldRecord.order_status == EnumOrderStatusType.DELIVER_WAITING)))
                    {

                        //在庫返却処理
                        var setmerchandise_List = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(record.scode);
                        if (setmerchandise_List.Count > 0)
                        {
                            foreach (var list in setmerchandise_List)
                            {   //セット商品在庫返却
                                increaseStockStgy.Increase(list.scode, list.amount.Value * record.amount.Value);
                            }
                        }
                        else
                        {
                            //在庫返却
                            increaseStockStgy.Increase(record.scode, amount);
                        }

                        //モール更新
                        var objSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(record.scode);

                        if (objSku != null && objSku.h_mall_flg == EnumOnOff.On)
                        {
                            UpdateStockParam param = default(UpdateStockParam);

                            param.productCode = objSku.scode;
                            param.quantity = amount;
                            param.operation = EnumMallStockOperation.add;

                            listParam.Add(param);
                        }
                    }
                }

                // モール在庫更新 [Update stock for mall]
                var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
                if (ret.HasValue())
                {
                    throw new Exception(ErsResources.GetMessage("102100", ret));
                }

                tx.Commit();
            }

            //与信破棄を行う(入金済みの場合は処理しない)
            if (objOrder.order_payment_status == EnumOrderPaymentStatusType.NOT_PAID)
            {
                // 与信破棄 モールは与信破棄はモールでやってもらう
                if (objOrder.mall_shop_kbn == EnumMallShopKbn.ERS)
                {
                    try
                    {
                        var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(objOrder.pay);
                        objPayment.SetPaymentMethod(objOrder, objOrder);
                        objPayment.SendVoidAuth(objOrder);
                    }
                    catch (Exception eTmp)
                    {
                        ErsCommon.loggingException(string.Format("Failed to void auth of payment.\r\n{0}", eTmp.ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// 伝票情報更新
        /// </summary>
        internal void Update(IBillModifyCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            command.wrap = command.wrap ?? EnumWrap.NotWish;

            var oldOrder = ErsFactory.ersOrderFactory.GetOrderWithD_no(command.d_no);
            var oldOrderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(oldOrder);

            var order = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(oldOrder.GetPropertiesAsDictionary());
            order.OverwriteWithParameter(command.GetPropertiesAsDictionary("d_master_t"));
            var newOrderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order);

            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcodeForAdmin(order.mcode, true);

            if (member != null)
            {
                if (order.p_service != oldOrder.p_service)
                {
                    //Execute some operation if the money of this new order has changed.
                    var reason = ErsResources.GetFieldName("point_reason_bill_modify");

                    ErsFactory.ersOrderFactory.GetUpdateMemberPointStgy().Increase(member.mcode, oldOrder.p_service, reason, order.d_no, order.intime,(int)order.site_id);
                    ErsFactory.ersOrderFactory.GetUpdateMemberPointStgy().Decrease(member.mcode, order.p_service, reason, order.d_no, order.intime,(int)order.site_id);
                }

                ErsFactory.ersOrderFactory.GetUpdateMemberDataStgy().Update(member, -oldOrder.subtotal, 0);
                ErsFactory.ersOrderFactory.GetUpdateMemberDataStgy().Update(member, order.subtotal, 0);

                var old_member = ErsFactory.ersMemberFactory.getErsMemberWithMcodeForAdmin(order.mcode, true);
                var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
                repository.Update(old_member, member);
            }

            // 入金情報変更不可の場合は、古いデータで上書き
            if (!ErsFactory.ersOrderFactory.GetIsPaymentInfoChangableSpec().IsChangable(oldOrder))
            {
                order.order_payment_status = oldOrder.order_payment_status;
                order.paid_price = oldOrder.paid_price;
                order.paid_date = oldOrder.paid_date;
            }

            order.site_id = oldOrder.site_id;

            //伝票更新
            this.UpdateOrder(command, oldOrder, order);

            //入金ステータスが与信未取得の伝票（継続課金伝票）の修正が行われたときに与信を取得
            if (ErsFactory.ersOrderFactory.GetIsContinualBillingOrderSpec().Satisfy(oldOrder))
            {
                // ただし、配送済みで継続課金送信済みの場合は、与信の取得は行わない（継続課金を依頼しているため）
                var newOrderStatus = ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy().ObtainFrom(newOrderRecords.Values);
                if (oldOrder.sent_continual_billing != EnumSentContinualBillingFlg.Sent)
                {
                    // キャンセルの場合は与信を取得しない
                    // 30000万円以内の場合は与信を取得しない
                    if (!ErsOrderCriteria.CANCEL_STATUS_ARRAY.Contains(newOrderStatus.Value)
                            && setup.gmo_compensable_total < order.total)
                    {
                        this.ExecutePayment(order, member, command);
                    }
                }
                else
                {
                    //配送済み伝票の金額が変更になる場合は、完了画面に以下のメッセージを表示する
                    if (oldOrder.total != order.total)
                    {
                        command.controller.AddInformation(ErsResources.GetMessage("53305"));
                    }
                }
            }
            //金額が変更された場合は再与信
            else if (oldOrder.total != order.total)
            {
                /// 請求額変更
                var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(order.pay);
                objPayment.SendAlterAuth(order, order.total);

                //伝票更新
                var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
                orderRepository.Update(oldOrder, order);
            }
        }

        /// <summary>
        /// 伝票追加
        /// </summary>
        /// <param name="command"></param>
        /// <param name="oldOrder"></param>
        /// <param name="order"></param>
        private void UpdateOrder(IBillModifyCommand command, ErsOrder oldOrder, ErsOrder order)
        {
            var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            orderRepository.Update(oldOrder, order);

            var orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var listOrderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order);
            var old_records = listOrderRecords.Select(e => e.Value);

            var createOrderRecordKeyStgy = ErsFactory.ersOrderFactory.GetCreateOrderRecordKeyStgy();
            foreach (var orderRecord in listOrderRecords.Values)
            {
                var key = createOrderRecordKeyStgy.GetKey(orderRecord, order.senddate);
                var record = command.orderRecords.FirstOrDefault((bill_detail) => bill_detail.key == key);
                if (record == null)
                {
                    continue;
                }

                var newOrderRecord = ErsFactory.ersOrderFactory.GetErsOrderRecordWithParameter(orderRecord.GetPropertiesAsDictionary());
                newOrderRecord.OverwriteWithParameter(record.GetPropertiesAsDictionary("ds_master_t"));
                newOrderRecord.deliv_method = command.deliv_method;
                orderRecordRepository.Update(orderRecord, newOrderRecord);

                if (orderRecord.order_status != newOrderRecord.order_status)
                {
                    var new_records = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order).Select(e => e.Value);

                    // 受注ステータス更新履歴登録 [Regist order status history]
                    ErsFactory.ersOrderFactory.GetRegistOrderRecordStatusHistoryStgy().
                        Regist(orderRecord, newOrderRecord, oldOrder, old_records, order, new_records);
                }
            }
        }

        internal void ExecutePayment(ErsOrder order, ErsMember member, IBillModifyCommand command)
        {
            var oldOrder = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(order.GetPropertiesAsDictionary());

            var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(order.pay);
            objPayment.SetPaymentMethod(order, command);

            objPayment.SendAuth(order, member, false);

            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            repository.Update(oldOrder, order);
        }
    }
}