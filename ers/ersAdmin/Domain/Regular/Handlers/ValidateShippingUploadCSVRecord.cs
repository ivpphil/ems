﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Regular.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class ValidateShippingUploadCSVRecord:IValidationHandler<IShippingUploadCSVRecordCommand>
    {
        public virtual IEnumerable<ValidationResult> Validate(IShippingUploadCSVRecordCommand command)
        {
            yield return command.CheckRequired("d_no");
            yield return command.CheckRequired("sendno");
            yield return command.CheckRequired("shipdate");

            if (!string.IsNullOrEmpty(command.d_no) && !this.CehckD_no(command))
                yield return new ValidationResult(ErsResources.GetMessage("30104", command.shipping_d_no), new[] { "shipping_d_no" });
        }

        /// <summary>
        /// 伝票番号の存在チェック
        /// </summary>
        /// <returns></returns>
        private bool CehckD_no(IShippingUploadCSVRecordCommand command)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();

            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.d_no =  command.d_no;

            if (!string.IsNullOrEmpty(command.subd_no))
                criteria.subd_no = command.subd_no;

            criteria.order_status_in = new[] { EnumOrderStatusType.DELIVER_REQUEST };

            //({criteriaForPay} OR {criteriaForStatus}) AND {criteriaForDno}
            return repository.GetRecordCount(criteria) > 0;
        }
    }
}