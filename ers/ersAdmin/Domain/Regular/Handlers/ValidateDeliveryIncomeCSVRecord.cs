﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Regular.Commands;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class ValidateDeliveryIncomeCSVRecord : IValidationHandler<IDeliveryIncomeCSVRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IDeliveryIncomeCSVRecordCommand command)
        {

            yield return command.CheckRequired("d_no");
            yield return command.CheckRequired("paid_price");

            if (command.d_no.HasValue()) {
                var order = CehckD_no(command);

                if (order != null)
                {
                    if (order.total != command.paid_price)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("cod.payment.errmsg.paid.total.notequal.price"));
                    }                
                }
                else
                {
                    yield return new ValidationResult(ErsResources.GetMessage("30104", command.d_no), new[] { "d_no" });
                }
            }                          
        }

        /// <summary>
        /// 伝票番号の存在チェック
        /// </summary>
        /// <returns></returns>
        private ErsOrder CehckD_no(IDeliveryIncomeCSVRecordCommand command)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.d_no = command.d_no;
            criteria.order_status_in = new[] { EnumOrderStatusType.DELIVERED };
            criteria.order_payment_status_in = new[] { EnumOrderPaymentStatusType.NOT_PAID };
            if(repository.GetRecordCount(criteria) > 0)
            {
                return repository.Find(criteria).First();
            }
            return null;
        }
        
    }
}