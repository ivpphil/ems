﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Regular.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class ValidateStockUpdateCSVRecord : IValidationHandler<IStockUpdateCSVRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IStockUpdateCSVRecordCommand command)
        {
            yield return command.CheckRequired("scode");
            yield return command.CheckRequired("stock");

            yield return checkScode(command.scode);
        }

        /// <summary>
        /// 在庫数アップロード商品番号が存在するかチェック(検知)
        /// </summary>
        /// <param name="scode"></param>
        /// <returns></returns>
        protected ValidationResult checkScode(string scode)
        {

            //商品検索用リポジトリクラス　インスタンス化処理
            var repository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();

            //商品検索用クライテリアクラス　インスタンス化処理
            var merchandiseCri = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();

            //検索条件をクライテリアへ
            merchandiseCri.scode = scode;

            if (repository.GetRecordCount(merchandiseCri) == 0)
            {
                //商品が見つかりませんでした。\r\n商品番号[{0}]をご確認下さい。
                return new ValidationResult(ErsResources.GetMessage("20000", scode), new[] { "scode" });
            }

            return null;

        }
    }
}