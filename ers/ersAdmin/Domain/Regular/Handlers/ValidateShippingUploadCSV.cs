﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Regular.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class ValidateShippingUploadCSV:IValidationHandler<IShippingUploadCSVCommand>
    {
        public IEnumerable<ValidationResult> Validate(IShippingUploadCSVCommand command)
        {
            if (!command.regist)
            {
                if (command.csv_file.csv_file == null)
                {
                    //アップロードファイルを指定してください。
                    //Specify the uploaded file
                    throw new ErsException("10202");
                }
            }

            //完了時は、エラーは画面出力しない（エラー対象を省いて登録するため）
            List<string> validOrderNo = new List<string>();
            foreach (var model in command.csv_file.GetValidatedModels(command.chk_find))
            {
                if (validOrderNo.Where(o => o == model.d_no).Count() > 0)
                {
                    model.AddInvalidField(new ValidationResult(ErsResources.GetMessage("10103", new[] { ErsResources.GetFieldName("d_no"), model.d_no })));
                }
                else
                {
                    model.AddInvalidField(command.controller.commandBus.Validate<IShippingUploadCSVRecordCommand>(model));
                }
                validOrderNo.Add(model.d_no);
                

                if (!model.IsValid)
                {
                    if (!command.regist)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "csv_file" });
                        }
                    }

                    command.csv_file.MarkRecordAsInvalid(model);
                }
                
                
            }

            //保持された登録情報が無い場合、エラーメッセージを表示。
            if (command.csv_file.validIndexes.Count() == 0)
            {
                //対象データが存在しません。確認してください。
                yield return new ValidationResult(ErsResources.GetMessage("10200"));
            }
        }
    }
}