﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Regular.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class ValidateRegistIncome : IValidationHandler<IRegistIncomeCommand>
    {
        public IEnumerable<ValidationResult> Validate(IRegistIncomeCommand command)
        {
            if (command.inputDetails != null)
            {
                var isChecked = false;
                foreach (var model in command.inputDetails)
                {                    
                    model.AddInvalidField(command.controller.commandBus.Validate<IRegistIncomeDetailListRecordCommand>(model));

                    if (model.paid_check)
                    {
                        isChecked = true;
                    }

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "inputDetails" });
                        }
                    }
                }

                //入金処理にチェックを入れていない場合、エラーメッセージを表示。
                if (!isChecked)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("30103"), new[] { "inputDetails" });
                }
            }
        }
    }
}