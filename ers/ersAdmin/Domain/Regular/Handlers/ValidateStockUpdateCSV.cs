﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Regular.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Regular.Handlers
{
    public class ValidateStockUpdateCSV : IValidationHandler<IStockUpdateCSVCommand>
    {
        public IEnumerable<ValidationResult> Validate(IStockUpdateCSVCommand command)
        {
            if (!command.regist)
            {
                if (command.csv_file.csv_file == null)
                {
                    //アップロードファイルを指定してください。
                    //Specify the uploaded file
                    throw new ErsException("10202");
                }
            }

            //完了時は、エラーは画面出力しない（エラー対象を省いて登録するため）
            foreach (var model in command.csv_file.GetValidatedModels(command.chk_find))
            {
                model.AddInvalidField(command.controller.commandBus.Validate<IStockUpdateCSVRecordCommand>(model));
                if (!model.IsValid)
                {
                    if (!command.regist)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "csv_file" });
                        }
                    }

                    command.csv_file.MarkRecordAsInvalid(model);
                }
            }

            //保持された登録情報が無い場合、エラーメッセージを表示。
            if (command.csv_file.validIndexes.Count() == 0)
            {
                //対象データが存在しません。確認してください。
                yield return new ValidationResult(ErsResources.GetMessage("10200"));
            }
        }
    }
}