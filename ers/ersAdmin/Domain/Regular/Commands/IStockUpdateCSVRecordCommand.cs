﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Regular.Commands
{
    public interface IStockUpdateCSVRecordCommand : ICommand
    {
        string scode { get; set; }
        int? stock { get; set; }
    }
}