﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Regular.Commands
{
    public interface IShippingUploadCSVRecordCommand:ICommand
    {
        string d_no { get; }
        string shipping_d_no { get; }
        string subd_no { get; }
    }
}