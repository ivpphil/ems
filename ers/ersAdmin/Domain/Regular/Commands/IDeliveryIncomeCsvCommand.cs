﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using ersAdmin.Models.csv;

namespace ersAdmin.Domain.Regular.Commands
{
    public interface IDeliveryIncomeCsvCommand : ICommand
    {
        bool chk_find { get; set; }
        bool regist { get; set; }
        ErsCsvContainer<Delivery_income_csv> csv_file { get; set; }
    }
}