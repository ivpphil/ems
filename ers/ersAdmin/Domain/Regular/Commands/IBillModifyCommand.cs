﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.regular;
using jp.co.ivp.ers;
using jp.co.ivp.ers.Payment;

namespace ersAdmin.Domain.Regular.Commands
{
    public interface IBillModifyCommand : ICommand, IPaymentInfoGmoServerContainer
    {
        List<bill_details> orderRecords { get; set; }

        bool bill_change { get; set; }

        EnumSendTo? send { get; set; }

        int? pref { get; set; }

        int? sendtime { get; set; }

        int? add_pref { get; set; }

        string email { get; set; }

        string email_confirm { get; set; }

        string d_no { get; set; }

        string mall_d_no { get; set; }

        string lname { get; set; }

        string fname { get; set; }

        string lnamek { get; set; }

        string fnamek { get; set; }

        string compname { get; set; }

        string compnamek { get; set; }

        string division { get; set; }

        string divisionk { get; set; }

        string zip { get; set; }

        string address { get; set; }

        string taddress { get; set; }

        string maddress { get; set; }

        string tel { get; set; }

        string fax { get; set; }

        string add_lname { get; set; }

        string add_fname { get; set; }

        string add_lnamek { get; set; }

        string add_fnamek { get; set; }

        string add_compname { get; set; }

        string add_compnamek { get; set; }

        string add_zip { get; set; }

        string add_address { get; set; }

        string add_taddress { get; set; }

        string add_maddress { get; set; }

        string add_tel { get; set; }

        string add_fax { get; set; }

        EnumOrderPaymentStatusType? order_payment_status { get; set; }

        DateTime? senddate { get; set; }

        EnumWrap? wrap { get; set; }

        string memo2 { get; set; }

        string memo { get; set; }

        string memo3 { get; set; }

        string usr_memo { get; set; }

        int? carriage { get; set; }

        int? etc { get; set; }

        int? total { get; set; }

        int? p_service { get; set; }

        string coupon_code { get; set; }

        int coupon_discount { get; set; }

        EnumDelvMethod? deliv_method { get; set; }

        bool bill_cancel { get; set; }
    }
}