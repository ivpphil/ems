﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.Regular.Commands
{
    public interface IRegistIncomeDetailListRecordCommand : ICommand
    {
        bool paid_check { get; set; }
        int paid_price { get; set; }
        DateTime? paid_date { get; set; }
    }
}