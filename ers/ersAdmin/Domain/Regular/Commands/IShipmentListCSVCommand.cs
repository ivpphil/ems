﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;

namespace ersAdmin.Domain.Regular.Commands
{
    public interface IShipmentListCSVCommand
        : ICommand
    {
        IList<ErsOrderContainer> billList { get; }
    }
}