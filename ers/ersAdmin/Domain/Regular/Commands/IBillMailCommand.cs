﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Regular.Commands
{
    public interface  IBillMailCommand:ICommand
    {
        bool send_btn { get;  }

        string d_no { get; set; }

        string from_email { get; set; }
    }
}