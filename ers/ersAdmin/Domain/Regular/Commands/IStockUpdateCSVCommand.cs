﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.csv;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Regular.Commands
{
    public interface IStockUpdateCSVCommand : ICommand
    {
        bool chk_find { get; set; }
        bool regist { get; set; }
        ErsCsvContainer<Stock_update_csv> csv_file { get; set; }
    }
}