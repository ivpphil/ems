﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Regular.Commands
{
    public interface IDeliveryIncomeCSVRecordCommand : ICommand
    {
        string d_no { get; set; }
        int paid_price { get; set; }
    }
}