﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using ersAdmin.Models.csv;

namespace ersAdmin.Domain.Regular.Commands
{
    public interface IShippingUploadCSVCommand : ICommand
    {
        ErsCsvContainer<Shipping_csv_upload_record> csv_file { get; set; }

        bool chk_find { get; }

        bool regist { get; set; }
    }
}