﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models;

namespace ersAdmin.Domain.Regular.Commands
{
    public interface IRegistIncomeCommand : ICommand
    {
        List<Regist_income_detail> inputDetails { get; set; }        
    }
}