﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Login.Commands;
using jp.co.ivp.ers.administrator;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Login.Handlers
{
    public class ValidateLogin
        :IValidationHandler<ILoginCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ILoginCommand command)
        {
            yield return command.CheckRequired("user_login_id");
            yield return command.CheckRequired("passwd");

            if (command.IsValidField("user_login_id", "passwd"))
            {
                command.user_cd = ((ISession)ErsContext.sessionState).GetUserCode(command.user_login_id, command.passwd);

                if (command.user_cd == ErsAdministrator.DEFAULT_USER_CODE)
                    yield return new ValidationResult(ErsResources.GetMessage("10204"), new[] { "user_login_id", "passwd" });
            }
        }
    }
}