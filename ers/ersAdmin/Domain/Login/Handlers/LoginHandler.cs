﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Login.Commands;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Login.Handlers
{
    public class LoginHandler
        : ICommandHandler<ILoginCommand>
    {
        public ICommandResult Submit(ILoginCommand command)
        {
            ((ISession)ErsContext.sessionState).getNewSSLransu(command.user_cd);

            return new CommandResult(true);
        }
    }
}