﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Login.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Login.Handlers
{
    public class PassChangeHandler
        :ICommandHandler<IPassChangeCommand>
    {
        public ICommandResult Submit(IPassChangeCommand command)
        {
            UpdatePassword(command);
            return new CommandResult(true);
        }

        /// <summary>
        /// update admin password.
        /// </summary>
        private void UpdatePassword(IPassChangeCommand command)
        {
            var administrator = GetAdministrator();
            var old_administrator = GetAdministrator();

            administrator.passwd = command.passwd;
            administrator.pass_date = DateTime.Now;

            var adminRepository = ErsFactory.ersAdministratorFactory.GetErsAdministratorRepository();

            adminRepository.Update(old_administrator, administrator);
        }

        /// <summary>
        /// get ErsAdministrator instance
        /// </summary>
        /// <returns></returns>
        private ErsAdministrator GetAdministrator()
        {
            var user_cd = ErsContext.sessionState.Get("user_cd");
            return ErsFactory.ersAdministratorFactory.GetErsAdministratorWithUserCode(user_cd);
        }
    }
}