﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Login.Commands
{
    public interface ILoginCommand
        : ICommand
    {
        string user_cd { get; set; }

        string user_login_id { get; }

        string passwd { get; }
    }
}