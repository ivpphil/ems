﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Login.Commands
{
    public interface IPassChangeCommand
        : ICommand
    {
        string passwd { get; }

        string passwd_confirm { get; }
    }
}