﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Login.Mappables
{
    public interface IPassChangeMappable
        : IMappable
    {
        string passwd { set; }

        string passwd_confirm { set; }

    }
}