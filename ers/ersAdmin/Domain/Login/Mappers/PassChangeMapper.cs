﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Login.Mappables;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Login.Mappers
{
    public class PassChangeMapper
        : IMapper<IPassChangeMappable>
    {
        public void Map(IPassChangeMappable objMappable)
        {
            LoadPassword(objMappable);
        }

        /// <summary>
        /// load password to model
        /// </summary>
        private void LoadPassword(IPassChangeMappable objMappable)
        {
            var administrator = GetAdministrator();

            objMappable.passwd = administrator.passwd;
            objMappable.passwd_confirm = administrator.passwd;
        }

        /// <summary>
        /// get ErsAdministrator instance
        /// </summary>
        /// <returns></returns>
        private ErsAdministrator GetAdministrator()
        {
            var user_cd = ErsContext.sessionState.Get("user_cd");
            return ErsFactory.ersAdministratorFactory.GetErsAdministratorWithUserCode(user_cd);
        }
    }
}