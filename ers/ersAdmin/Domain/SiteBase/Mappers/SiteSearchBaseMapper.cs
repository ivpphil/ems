﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersAdmin.Domain.SiteBase.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.SiteBase.Mappers
{
    public class SiteSearchBaseMapper
        : IMapper<ISiteSearchBaseMappable>
    {
        public void Map(ISiteSearchBaseMappable objMappable)
        {
            
        }

        /// <summary>
        /// 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
        /// </summary>
        /// <param name="objMappable">ISiteSearchBaseMappable</param>
        /// <param name="criteria">Criteria</param>
        /// <param name="table_name">string</param>
        protected virtual void SetSearchPropertyOfSiteIdToCriteria(ISiteSearchBaseMappable objMappable, Criteria criteria, string table_name = null)
        {
            if (objMappable.s_site_id == null || objMappable.s_site_id.Length == 0)
            {
                return;
            }

            if (string.IsNullOrEmpty(table_name))
            {
                table_name = string.Empty;
            }
            else
            {
                table_name += ".";
            }

            // 0:全サイト追加 [Add "0:All site"]
            var site_id_list = objMappable.s_site_id.ToList();
            site_id_list.Add((int)EnumSiteId.COMMON_SITE_ID);

            criteria.Add(Criteria.GetInClauseCriterion(table_name + "site_id", site_id_list));
        }
    }
}
