﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersAdmin.Domain.SiteBase.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.db;
using System.Web.Services.Description;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.SiteBase.Mappers
{
    public class SiteRegisterBaseMapper
        : IMapper<ISiteRegisterBaseMappable>
    {
        public void Map(ISiteRegisterBaseMappable objMappable)
        {
            
        }

        /// <summary>
        /// 初期サイトIDセット [Set default site ID]
        /// </summary>
        /// <param name="objMappable">ISiteRegisterBaseMappable</param>
        protected virtual void SetDefaultSiteId(ISiteRegisterBaseMappable objMappable)
        {
            if (objMappable.site_id != null)
            {
                return;
            }

            var site_list = ErsMallFactory.ersMallViewServiceFactory.GetErsViewSiteService().SelectAsList();
            objMappable.site_id = Convert.ToInt32(site_list.First()["value"]);
        }

        /// <summary>
        /// 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
        /// </summary>
        /// <param name="objMappable">ISiteRegisterBaseMappable</param>
        /// <param name="criteria">Criteria</param>
        /// <param name="table_name">string</param>
        protected virtual void SetSearchPropertyOfSiteIdToCriteria(ISiteRegisterBaseMappable objMappable, Criteria criteria, string table_name = null)
        {
            if (objMappable.site_id == null)
            {
                return;
            }

            if (string.IsNullOrEmpty(table_name))
            {
                table_name = string.Empty;
            }
            else
            {
                table_name += ".";
            }

            if (objMappable.site_id.GetType() == typeof(int[]))
            {
                criteria.Add(Criteria.GetInClauseCriterion(table_name + "site_id", (int[])objMappable.site_id));
            }
            else
            {
                criteria.Add(Criteria.GetCriterion(table_name + "site_id", Convert.ToInt32(objMappable.site_id), Criteria.Operation.EQUAL));
            }
        }
    }
}
