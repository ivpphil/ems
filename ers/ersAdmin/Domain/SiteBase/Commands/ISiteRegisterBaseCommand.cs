﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.SiteBase.Command
{
    public interface ISiteRegisterBaseCommand
        : ICommand
    {
        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        object site_id { get; set; }

        bool multiple_sites { get; }

        int config_site_id { get; }
    }
}
