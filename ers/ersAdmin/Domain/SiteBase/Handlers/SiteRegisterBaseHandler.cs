﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.SiteBase.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;

namespace ersAdmin.Domain.SiteBase.Handlers
{
    public class SiteRegisterBaseHandler
        : ICommandHandler<ISiteRegisterBaseCommand>
    {
        public ICommandResult Submit(ISiteRegisterBaseCommand command)
        {
            return new CommandResult(true);
        }

        /// <summary>
        /// 配列型サイトID取得 [Get array of site ID]
        /// </summary>
        /// <param name="command">ISiteRegisterBaseCommand</param>
        /// <returns>int[]</returns>
        protected virtual int[] GetArrayOfSiteId(ISiteRegisterBaseCommand command)
        {
            if (command.site_id != null && command.site_id != "")
            {
                if (command.site_id.GetType() == typeof(int[]))
                {
                    int[] site_ids = (int[])command.site_id;
                    return site_ids.Distinct().ToArray();
                }

                return command.site_id.ToString().Split(',').Select(e => Convert.ToInt32(e)).Distinct().ToArray();
            }

            return null;
        }
    }
}