﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.SiteBase.Mappables
{
    public interface ISiteSearchBaseMappable
        : IMappable
    {
        /// <summary>
        /// 検索用サイトID [Site ID for search]
        /// </summary>
        int[] s_site_id { get; set; }

        bool multiple_sites { get; }

        int config_site_id { get; }
    }
}
