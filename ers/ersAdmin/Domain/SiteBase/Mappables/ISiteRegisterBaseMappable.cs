﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.SiteBase.Mappables
{
    public interface ISiteRegisterBaseMappable
        : IMappable
    {
        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        object site_id { get; set; }

        bool multiple_sites { get; }

        int config_site_id { get; }
    }
}
