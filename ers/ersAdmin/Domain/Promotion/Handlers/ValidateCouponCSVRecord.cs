﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.merchandise;
using ersAdmin.Domain.Promotion.Commands;

namespace ersAdmin.Domain.Promotion.Handlers
{
    public class ValidateCouponCSVRecord
        : IValidationHandler<ICouponCSVRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICouponCSVRecordCommand command)
        {
            foreach (var result in this.ValidateRecord(command))
            {
                yield return result;
            }
        }

        private IEnumerable<ValidationResult> ValidateRecord(ICouponCSVRecordCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            yield return command.CheckRequired("coupon_code");
            yield return command.CheckRequired("coupon_type");
            yield return command.CheckRequired("start_date");
            yield return command.CheckRequired("end_date");
            yield return command.CheckRequired("active");
            if (command.end_date < command.start_date)
            {
                yield return new ValidationResult(ErsResources.GetMessage("COUPON001"));
            }
        }
    }
}