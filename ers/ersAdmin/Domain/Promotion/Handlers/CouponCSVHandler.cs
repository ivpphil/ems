﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using ersAdmin.Models.csv;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mall.product.strategy;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall.site;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.strategy;
using ersAdmin.Domain.Promotion.Commands;

namespace ersAdmin.Domain.Promotion.Handlers
{
    public class CouponCSVHandler
        : ICommandHandler<ICouponCSVCommand>
    {
        ProductKeywordConstracterStgy keywordConstracter = ErsFactory.ersMerchandiseFactory.GetProductKeywordConstracterStgy();
        
        public ICommandResult Submit(ICouponCSVCommand command)
        {
            this.Execute(command);

            return new CommandResult(true);
        }

        internal void Execute(ICouponCSVCommand command)
        {
            var listGcode = new List<string>();
            foreach (var item in command.csv_file.GetValidModels())
            {
                // 1行づつトランザクションを実行する
                using (var tx = ErsDB_parent.BeginTransaction())
                {
                    if (!listGcode.Contains(item.coupon_code))
                    {
                        listGcode.Add(item.coupon_code);
                    }

                    if (!this.CheckCouponExist(item.coupon_code))
                    {
                        //追加
                        this.Insert(item);
                    }
                    else
                    {
                        //更新
                        this.Update(item);
                    }

                    //更新
                    tx.Commit();
                }
            }
        }


        /// <summary>
        /// Check Coupon Code exist
        /// </summary>
        /// <param name="gcode"></param>
        /// <returns></returns>
        protected virtual bool CheckCouponExist(string coupon_code)
        {
            var groupRepository = ErsFactory.ersCouponFactory.GetErsCouponRepository();
            var groupCriteria = ErsFactory.ersCouponFactory.GetErsCouponCriteria();
            groupCriteria.coupon_code = coupon_code;
            return (0 < groupRepository.GetRecordCount(groupCriteria));
        }

        /// <summary>
        /// Insert Coupon
        /// </summary>
        /// <param name="setupRepository"></param>
        /// <param name="item"></param>
        protected void Insert(Coupon_csv_record coupon)
        {
            var groupRepository = ErsFactory.ersCouponFactory.GetErsCouponRepository();
            var objCoupon = ErsFactory.ersCouponFactory.GetErsCouponWithParameter(coupon.GetPropertiesAsDictionary());
            objCoupon.site_id = coupon.site_id;
            groupRepository.Insert(objCoupon);
        }

        /// <summary>
        /// Update Coupon
        /// </summary>
        /// <param name="item"></param>
        private void Update(Coupon_csv_record coupon)
        {
            var groupRepository = ErsFactory.ersCouponFactory.GetErsCouponRepository();
            var oldCoupon = ErsFactory.ersCouponFactory.GetErsCouponWithCouponCode(coupon.coupon_code);
            var newCoupon = ErsFactory.ersCouponFactory.GetErsCouponWithParameter(oldCoupon.GetPropertiesAsDictionary());
            newCoupon.OverwriteWithParameter(coupon.GetPropertiesAsDictionary());
            newCoupon.site_id = coupon.site_id;
            groupRepository.Update(oldCoupon, newCoupon);
        }
    }
}