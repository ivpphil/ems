﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Promotion.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Promotion.Handlers
{
    public class ValidateCouponDelete : IValidationHandler<ICouponDeleteCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICouponDeleteCommand command)
        {
            yield return command.CheckRequired("id");
        }

    }
}