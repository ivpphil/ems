﻿using ersAdmin.Domain.Promotion.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using ersAdmin.Domain.Campaign.Handlers;

namespace ersAdmin.Domain.Promotion.Handlers
{
    public class CouponRegistHandler : CampaignModifyHandler, ICommandHandler<ICouponRegistCommand>
    {
        public ICommandResult Submit(ICouponRegistCommand command)
        {
            if (command.IsRegistCompletion)
                this.executeOperation(command);

            return new CommandResult(true);
        }

        internal void executeOperation(ICouponRegistCommand command)
        {

            var repo = ErsFactory.ersCouponFactory.GetErsCouponRepository();
            var criteria = ErsFactory.ersCouponFactory.GetErsCouponCriteria();
            var setCoupon = ErsFactory.ersCouponFactory.GetErsCoupon();
            setCoupon.OverwriteWithModel(command);
            setCoupon.site_id = this.GetArrayOfSiteId(command);
            repo.Insert(setCoupon, true);
        }
    }
}