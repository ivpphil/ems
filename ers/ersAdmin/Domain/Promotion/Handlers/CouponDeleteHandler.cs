﻿using ersAdmin.Domain.Promotion.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Promotion.Handlers
{
    public class CouponDeleteHandler : ICommandHandler<ICouponDeleteCommand>
    {
        public ICommandResult Submit(ICouponDeleteCommand command)
        {
            this.executeOperation(command);
            return new CommandResult(true);
        }

        private void executeOperation(ICouponDeleteCommand command)
        {
            var couponRepo = ErsFactory.ersCouponFactory.GetErsCouponRepository();
            var couponCriteria = ErsFactory.ersCouponFactory.GetErsCouponCriteria();
            couponCriteria.id = command.id;
            var result = couponRepo.Find(couponCriteria);

            if (result.Count != 0)
                couponRepo.Delete(result[0]);
        }

    }
}