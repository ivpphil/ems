﻿using ersAdmin.Domain.Promotion.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using ersAdmin.Domain.Campaign.Handlers;

namespace ersAdmin.Domain.Promotion.Handlers
{
    public class CouponModifyHandler : CampaignModifyHandler, ICommandHandler<ICouponModifyCommand>
    {
        public ICommandResult Submit(ICouponModifyCommand command)
        {
            this.executeOperation(command);
            return new CommandResult(true);
        }

        private void executeOperation(ICouponModifyCommand command)
        {
            var couponRepo = ErsFactory.ersCouponFactory.GetErsCouponRepository();
            var couponCriteria = ErsFactory.ersCouponFactory.GetErsCouponCriteria();
            couponCriteria.id = command.id;
            var result = couponRepo.Find(couponCriteria);

            var setCoupon = ErsFactory.ersCouponFactory.GetErsCoupon();
            setCoupon.OverwriteWithModel(command);
            setCoupon.site_id = this.GetArrayOfSiteId(command);
            couponRepo.Update(result[0], setCoupon);
        }
    }
}