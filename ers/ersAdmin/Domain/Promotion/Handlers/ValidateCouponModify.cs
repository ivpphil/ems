﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Promotion.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Promotion.Handlers
{
    public class ValidateCouponModify : IValidationHandler<ICouponModifyCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICouponModifyCommand command)
        {
            yield return command.CheckRequired("id");
            yield return command.CheckRequired("coupon_type");
            yield return command.CheckRequired("price");
            yield return command.CheckRequired("base_price");
            yield return command.CheckRequired("start_date");
            yield return command.CheckRequired("end_date");
            yield return command.CheckRequired("active");

            //set to End Time (23:59:59) if no inputted time in end_date
            if (command.end_date != null)
            {
                if (command.end_date.Value.Hour == 0 && command.end_date.Value.Minute == 0 && command.end_date.Value.Second == 0)
                    command.end_date = command.end_date.Value.GetEndForSearch();
            }

            foreach (var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDateTime("start_date", command.start_date, "end_date", command.end_date))
            {
                yield return result;
            }

            yield return command.CheckRequired("coupon_code");
            if (!string.IsNullOrEmpty(command.coupon_code))
            {
                if (!this.IsValidCoupon(command))
                    yield return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("coupon_code"), command.coupon_code), new[] { "coupon_code" });
            }
        }

        private bool IsValidCoupon(ICouponModifyCommand command)
        {
            var couponRepo = ErsFactory.ersCouponFactory.GetErsCouponRepository();
            var couponCri = ErsFactory.ersCouponFactory.GetErsCouponCriteria();
            couponCri.coupon_code = command.coupon_code;
            couponCri.id_not_equal = command.id;

            return (couponRepo.GetRecordCount(couponCri) == 0);
        }

    }
}