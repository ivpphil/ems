﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using ersAdmin.Domain.Promotion.Commands;

namespace ersAdmin.Domain.Promotion.Handlers
{
    public class ValidateCouponCSV
        : IValidationHandler<ICouponCSVCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICouponCSVCommand command)
        {
            var controller = command.controller;
            if (!command.regist)
            {
                if (command.csv_file.csv_file == null)
                {
                    //アップロードファイルを指定してください。
                    //Specify the uploaded file
                    throw new ErsException("10202");
                }
            }

            var p_list = new List<ersAdmin.Models.csv.Coupon_csv_record>();

            foreach (var model in command.csv_file.GetValidatedModels(command.chk_find))
            {
                p_list.Add(model);
            }

            foreach(var model in p_list)
            {
                model.AddInvalidField(controller.commandBus.Validate<ICouponCSVRecordCommand>(model));

                if (!model.IsValid)
                {
                    //完了時は、エラーは画面出力しない（エラー対象を省いて登録するため） // Error message is not displayed at complete screen.
                    if (!command.regist)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "csv_file" });
                        }
                    }
                    command.csv_file.MarkRecordAsInvalid(model);
                }
            }

            //保持された登録情報が無い場合、エラーメッセージを表示。
            if (command.csv_file.validIndexes.Count() == 0)
            {
                //対象データが存在しません。確認してください。
                yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "csv_file" });
            }
        }

    }
}