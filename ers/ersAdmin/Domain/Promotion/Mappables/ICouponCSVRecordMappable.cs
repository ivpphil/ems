﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Promotion.Mappables
{
    public interface ICouponCSVRecordMappable
        : IMappable
    {
        EnumActive? active { get; set; }
    }
}