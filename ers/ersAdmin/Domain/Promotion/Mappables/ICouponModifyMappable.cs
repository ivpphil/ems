﻿using System;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Promotion.Mappables
{
    public interface ICouponModifyMappable : ISiteRegisterBaseMappable, IMappable
    {

        int? id { get; }

        string coupon_code { get; }

        EnumCouponType? coupon_type { get; }

        int? price { get; }

        int? base_price { get; }

        DateTime? start_date { get; }

        DateTime? end_date { get; }

        EnumActive? active { get; }

        DateTime intime { get; }

        DateTime? utime { get; }

    }
}