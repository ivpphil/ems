﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.item.tsv;

namespace ersAdmin.Domain.Promotion.Mappables
{
    public interface ICouponUploadSVMappable
        : IMappable
    {
        List<Item_tsv_mall_list> listMallList { get; set; }
    }
}