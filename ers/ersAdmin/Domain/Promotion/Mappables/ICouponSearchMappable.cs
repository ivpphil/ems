﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Promotion.Mappables
{
    public interface ICouponSearchMappable : ISiteSearchBaseMappable, IMappable
    {

        bool IsSearchList { get; }

        List<Dictionary<string, object>> list { get; set; }

        long recordCount { get; set; }

        ErsPagerModel pager { get; }

        DateTime? s_intime_from { get; }

        DateTime? s_intime_to { get; }

        string s_coupon_code { get; set; }

        EnumCouponType? s_coupon_type { get; }

        int? s_price_from { get;}

        int? s_price_to { get;}

        int? s_base_price_from { get;}

        int? s_base_price_to { get;}

        DateTime? s_start_date_from { get;}

        DateTime? s_start_date_to { get;}

        DateTime? s_end_date_from { get;}

        DateTime? s_end_date_to { get;}

        EnumActive? active { get;}
    }
}