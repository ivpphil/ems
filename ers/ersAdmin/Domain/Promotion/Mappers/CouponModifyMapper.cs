﻿using ersAdmin.Domain.Promotion.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Promotion.Mappers
{
    public class CouponModifyMapper : IMapper<ICouponModifyMappable>
    {
        public void Map(ICouponModifyMappable objMappable)
        {
            this.loadCoupon(objMappable);
        }

        private void loadCoupon(ICouponModifyMappable objMappable)
        {
            var couponRepo = ErsFactory.ersCouponFactory.GetErsCouponRepository();
            var couponCriteria = ErsFactory.ersCouponFactory.GetErsCouponCriteria();
            couponCriteria.id = objMappable.id;

            var result = couponRepo.Find(couponCriteria);

            var coupon_rec = result[0];

            objMappable.OverwriteWithParameter(coupon_rec.GetPropertiesAsDictionary());
            objMappable.site_id = coupon_rec.site_id;
        }

    }
}