﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Promotion.Mappables;

namespace ersAdmin.Domain.Promotion.Mappers
{
    public class CouponCSVMapper
        : IMapper<ICouponCSVMappable>
    {
        public void Map(ICouponCSVMappable objMappable)
        {
            var controller = objMappable.controller;
            foreach (var model in objMappable.csv_file.GetValidModels())
            {
                controller.mapperBus.Map<ICouponCSVRecordMappable>(model);
                objMappable.csv_file.SaveValue(model);
            }
        }
    }
}