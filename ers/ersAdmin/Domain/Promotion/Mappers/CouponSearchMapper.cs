﻿using System;
using System.Collections.Generic;
using ersAdmin.Domain.Promotion.Mappables;
using jp.co.ivp.ers.coupon;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Promotion.Mappers
{
    public class CouponSearchMapper : SiteSearchBaseMapper, IMapper<ICouponSearchMappable>
    {
        public void Map(ICouponSearchMappable objMappable)
        {
            if (objMappable.IsSearchList)
                this.LoadList(objMappable);
        }

        public void LoadList(ICouponSearchMappable objMappable)
        {

            var couponCri = this.GetCouponCriteria(objMappable);

            var couponRepo = ErsFactory.ersCouponFactory.GetErsCouponRepository();
            objMappable.recordCount = couponRepo.GetRecordCount(couponCri);

            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            couponCri.SetOrderByProcessID(Criteria.OrderBy.ORDER_BY_DESC);

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(couponCri);
            }

            objMappable.list = this.GetList(couponRepo,couponCri);

        }

        /// <summary>
        /// Coupon search entity
        /// </summary>
        /// <returns></returns>
        protected ErsCouponCriteria GetCouponCriteria(ICouponSearchMappable objMappable)
        {
            var couponCri = ErsFactory.ersCouponFactory.GetErsCouponCriteria();

            //件数
            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, couponCri, "coupon_t");

            if (objMappable.s_intime_from.HasValue)
            {
                couponCri.isGreaterEqualIntime = objMappable.s_intime_from;
            }
            if (objMappable.s_intime_to.HasValue)
            {
                couponCri.isLessThanEqualIntime = objMappable.s_intime_to;
            }
            if (!string.IsNullOrEmpty(objMappable.s_coupon_code))
            {
                couponCri.coupon_code = objMappable.s_coupon_code;
            }
            //coupon type
            if (objMappable.s_coupon_type.HasValue)
            {
                couponCri.coupon_type = objMappable.s_coupon_type;
            }
            //Price
            if (objMappable.s_price_from.HasValue)
            {
                couponCri.isGreaterEqualPrice = objMappable.s_price_from;
            }
            if (objMappable.s_price_to.HasValue)
            {
                couponCri.isLessThanEqualPrice = objMappable.s_price_to;
            }
            //Base Price
            if (objMappable.s_base_price_from.HasValue)
            {
                couponCri.isGreaterThanEqualBasePrice = objMappable.s_base_price_from;
            }
            if (objMappable.s_base_price_to.HasValue)
            {
                couponCri.islessThanEqualBasePrice = objMappable.s_base_price_to;
            }
            //Start Date
            if (objMappable.s_start_date_from.HasValue)
            {
                couponCri.isGreaterThanEqualStartDate = objMappable.s_start_date_from;
            }
            if (objMappable.s_start_date_to.HasValue)
            {
                couponCri.islessThanEqualStartDate = objMappable.s_start_date_to;
            }
            //End Date
            if (objMappable.s_end_date_from.HasValue)
            {
                couponCri.isGreaterThanEqualEndDate = objMappable.s_end_date_from;
            }
            if (objMappable.s_end_date_to.HasValue)
            {
                couponCri.islessThanEqualEndDate = objMappable.s_end_date_to;
            }
            //active
            if (objMappable.active.HasValue)
            {
                couponCri.active = objMappable.active;
            }

            return couponCri;
        }


        internal List<Dictionary<string, object>> GetList(ErsCouponRepository couponRepo, ErsCouponCriteria criteria)
        {
            var listall = couponRepo.Find(criteria);
            var ret = ErsCommon.ConvertEntityListToDictionaryList(listall);

            return ret;
        }
    }
}