﻿using System;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Promotion.Mappers;
using ersAdmin.Domain.Promotion.Mappables;

namespace ersAdmin.Domain.Promotion.Mappers
{
    public class CouponListCSVMapper
        : CouponSearchMapper, IMapper<ICouponListCSVMappable>
    {
        public void Map(ICouponListCSVMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }

        /// <summary>
        /// CSVファイルを作成する。
        /// </summary>
        /// <returns></returns>
        internal virtual void CreateCsvFile(ICouponListCSVMappable objMappable)
        {
            objMappable.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();

            var rpsCoupon = ErsFactory.ersCouponFactory.GetErsCouponRepository();
            var crtCoupon = this.GetCouponCriteria(objMappable);

            crtCoupon.SetOrderByProcessID(Criteria.OrderBy.ORDER_BY_DESC);
            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(crtCoupon);
            }

            var listCoupon = rpsCoupon.Find(crtCoupon);

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            ersAdmin.Models.csv.Coupon_csv coupon_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<ersAdmin.Models.csv.Coupon_csv>(writer);
                foreach (var item in listCoupon)
                {
                    coupon_csv = new Models.csv.Coupon_csv();
                    coupon_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                    objMappable.csvCreater.WriteBody(coupon_csv, writer);
                }
            }
        }
    }
}