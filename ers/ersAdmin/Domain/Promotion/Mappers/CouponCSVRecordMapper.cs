﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using ersAdmin.Domain.Promotion.Mappables;

namespace ersAdmin.Domain.Promotion.Mappers
{
    public class CouponCSVRecordMapper
        : IMapper<ICouponCSVRecordMappable>
    {
        public void Map(ICouponCSVRecordMappable objMappable)
        {
            this.SetDefaultValue(objMappable);
        }

        private void SetDefaultValue(ICouponCSVRecordMappable objMappable)
        {
            //if (objMappable.soldout_flg == null)
            //    objMappable.soldout_flg = EnumSoldoutFlg.DisableSoldout;

            //if (objMappable.active == null)
            //    objMappable.active = EnumActive.Active;

            //if (objMappable.sort == null)
            //    objMappable.sort = 0;

            //if (objMappable.disp_order == null)
            //    objMappable.disp_order = 0;

            //if (objMappable.max_purchase_count == null)
            //    objMappable.max_purchase_count = 0;

            //if (objMappable.point == null)
            //    objMappable.point = 0;

            //if (!objMappable.disp_send_ptn.HasValue())
            //{
            //    objMappable.disp_send_ptn = "0";
            //}
            //objMappable.disp_send_ptn = objMappable.disp_send_ptn.PadLeft(3, '0');

            ////date_toの時間に指定がない場合は、23:59:59
            //if (objMappable.date_to != null)
            //{
            //    if (objMappable.date_to.Value.Hour + objMappable.date_to.Value.Minute + objMappable.date_to.Value.Second == 0)
            //        objMappable.date_to = Convert.ToDateTime(objMappable.date_to.Value.ToString("yyyy/MM/dd 23:59:59"));
            //}
            ////point_campaign_toの時間に指定がない場合は、23:59:59
            //if (objMappable.point_campaign_to != null)
            //{
            //    if (objMappable.point_campaign_to.Value.Hour + objMappable.point_campaign_to.Value.Minute + objMappable.point_campaign_to.Value.Second == 0)
            //        objMappable.point_campaign_to = Convert.ToDateTime(objMappable.point_campaign_to.Value.ToString("yyyy/MM/dd 23:59:59"));
            //}
        }
    }
}