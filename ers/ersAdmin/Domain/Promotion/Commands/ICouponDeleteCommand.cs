﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Promotion.Commands
{
    public interface ICouponDeleteCommand : ICommand
    {
        int? id { get; }
    }
}