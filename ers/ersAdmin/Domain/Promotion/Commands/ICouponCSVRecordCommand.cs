﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Promotion.Commands
{
    public interface ICouponCSVRecordCommand
        : ICommand
    {
        DateTime? start_date { get; }
        DateTime? end_date { get; }
    }
}