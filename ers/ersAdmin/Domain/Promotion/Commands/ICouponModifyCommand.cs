﻿using System;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Promotion.Commands
{
    public interface ICouponModifyCommand : ISiteRegisterBaseCommand, ICommand
    {

        int? id { get; }

        string coupon_code { get; }

        EnumCouponType? coupon_type { get; }

        int? price { get; }

        int? base_price { get; }

        DateTime? start_date { get; }

        DateTime? end_date { get; set; }

        EnumActive? active { get; }

    }
}