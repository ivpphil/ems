﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Promotion.Commands
{
    public interface ICouponCSVCommand
        : ICommand
    {
        ErsCsvContainer<ersAdmin.Models.csv.Coupon_csv_record> csv_file { get; set; }

        bool chk_find { get; set; }

        bool regist { get; set; }
    }
}