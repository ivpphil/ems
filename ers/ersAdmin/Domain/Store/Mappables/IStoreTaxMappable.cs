﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Models;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface  IStoreTaxMappable:IMappable
    {
        int? tax {  set; }

        IList<Store_tax_payment_table> store_payment_table { get; set; }

        EnumOnOff? enable_carriage_tax { get; set; }
    }
}