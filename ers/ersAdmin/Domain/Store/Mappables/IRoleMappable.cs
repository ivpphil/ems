﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface IRoleMappable : IMappable
    {
        long search_result_cnt { get; set; }
        List<Dictionary<string, object>> Role_Group_List { get; set; }
    }
}