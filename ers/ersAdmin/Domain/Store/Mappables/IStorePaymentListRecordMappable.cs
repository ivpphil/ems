﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;
using ersAdmin.Models.store;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface  IStorePaymentListRecordMappable:IMappable
    {
        EnumPaymentType? id { get; }

        List<store_etc_table> store_etc_table { get; set; }
    }
}