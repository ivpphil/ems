﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface IRoleModifyMappable : IMappable
    {
        int id { get; set; }
        string role_gname { get; set; }
        string[] role_action { get; set; }
        string d_role_gname { get; }

    }
}