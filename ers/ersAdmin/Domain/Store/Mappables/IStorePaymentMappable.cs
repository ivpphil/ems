﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models;
using ersAdmin.Models.store;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface IStorePaymentMappable : ISiteRegisterBaseMappable, IMappable
    {
        IList<Store_payment_table> store_payment_table { get; set; }

        IList<Store_card_table> store_card_table { get; set; }
    }
}