﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.store;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface  IStoreCalendarMappable:IMappable
    {
        int? ddlYear { get; set; }

        int? ddlMonth { get; set; }

        List<Dictionary<string, object>> dayList { get; set; }

        int[] checkDays { get; set; }

        bool butYearMonth { get; set; }

        bool store_delivery_day_btn { get; set; }
    }
}