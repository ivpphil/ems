﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface IStoreEtcMappable : ISiteRegisterBaseMappable, IMappable
    {
        List<Store_payment_table> store_payment_table { get; set; }
    }
}