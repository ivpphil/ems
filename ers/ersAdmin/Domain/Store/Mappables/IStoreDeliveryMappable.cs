﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface IStoreDeliveryMappable : ISiteRegisterBaseMappable, IMappable
    {
        string delivery { set; }

        string url { set; }

        string mail_delivery { set; }

        string mail_url { set; }

    }
}