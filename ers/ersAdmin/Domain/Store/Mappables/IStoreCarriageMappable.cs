﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.store;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface IStoreCarriageMappable : ISiteRegisterBaseMappable, IMappable
    {
        List<store_carriage_table> allList { get;  set; }
        int? free { get; set; }
        Dictionary<int?, int?> carriage_id { get; set; }
        EnumOnOff? enable_carriage_tax { get; set; }
    }
}