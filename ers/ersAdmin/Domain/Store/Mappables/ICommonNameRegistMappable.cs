﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using ersAdmin.Models.store;
using jp.co.ivp.ers.common;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface ICommonNameRegistMappable : IMappable
    {
        List<common_name_detail> common_name_table { get; set; }

        IList<ErsCommonNameCode> Type_Code_List { get; set; }

        EnumCommonNameType? type_code { get; set; }

        bool regist { get; }

        string[] stArrayData { get; set; }

        bool item_flg { get; set; }

        bool error_flg { get; set; }

    }
}