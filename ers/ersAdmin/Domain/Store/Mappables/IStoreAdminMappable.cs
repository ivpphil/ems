﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface IStoreAdminMappable
        : ISiteRegisterBaseMappable, IMappable
    {
        string replying_mail_addr { get; set; }

        string report_mail_addr1 { get; set; }

        string report_mail_addr2 { get; set; }

        string report_mail_addr3 { get; set; }

        string quest_email_to { get; set; }

        List<Models.AdminListData> adminList { get; set; }
    }
}