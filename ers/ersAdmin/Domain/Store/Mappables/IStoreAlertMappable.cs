﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface IStoreAlertMappable : ISiteRegisterBaseMappable, IMappable
    {
        string pri_memo {  set; }

        string regi_memo {  set; }

        string error_message {  set; }

        string m_error_message {  set; }

        string wh_owner_description { set; }

        List<Store_payment_table> store_payment_table { get; set; }
    }
}