﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Store.Mappables
{
    public interface IStoreDeliveryDayMappable:IMappable
    {
        int? sendday { set; }

        int? sendday_count { set; }

        List<Dictionary<string, object>> allList { get; set; }

        Dictionary<int?, string> sendtime { get; set; }

        int shipping_csv_output_min_days { get; set; }

        int create_regular_order_days { get; set; }
      }
}