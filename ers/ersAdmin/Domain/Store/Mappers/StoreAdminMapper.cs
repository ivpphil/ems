﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using ersAdmin.Models;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Store.Mappers
{
    public class StoreAdminMapper
        : SiteRegisterBaseMapper, IMapper<IStoreAdminMappable>
    {
        public void Map(IStoreAdminMappable objMappable)
        {
            this.FindData(objMappable);
        }

        /// <summary>
        /// DBから値を取得
        /// </summary>
        public void FindData(IStoreAdminMappable objMappable)
        {
            // 初期サイトIDセット [Set default site ID]
            this.SetDefaultSiteId(objMappable); 

            if(!objMappable.multiple_sites)
            {
                objMappable.site_id = objMappable.config_site_id;
            }

            //メールアドレス
            var setup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(objMappable.site_id));

            objMappable.replying_mail_addr = setup.r_email;
            objMappable.report_mail_addr1 = setup.f_email1;
            objMappable.report_mail_addr2 = setup.f_email2;
            objMappable.report_mail_addr3 = setup.f_email3;
            objMappable.quest_email_to = setup.quest_email_to;

            //管理者
            var administrator = ErsFactory.ersAdministratorFactory.GetErsAdministratorRepository();
            var criteria = ErsFactory.ersAdministratorFactory.GetErsAdministratorCriteria();
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var alldata = administrator.Find(criteria);

            var list = new List<AdminListData>();
            //Load the details list.
            foreach (var data in alldata)
            {
                var item = new AdminListData();
                item.OverwriteWithParameter(data.GetPropertiesAsDictionary());
                list.Add(item);
            }
            list.Add(new AdminListData());
            objMappable.adminList = list;
        }
    }
}