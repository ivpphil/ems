﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using ersAdmin.Models.store;
using jp.co.ivp.ers.Send;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Mappers
{
    public class StoreCalendarMapper : IMapper<IStoreCalendarMappable>
    {
        public void Map(IStoreCalendarMappable ObjMappable)
        {
            if (ObjMappable.IsValid)
            {
                this.SetDefaultValue(ObjMappable);
            }

            //日付からカレンダー生成
            this.MakeCalendar(ObjMappable);
        }

        private void SetDefaultValue(IStoreCalendarMappable ObjMappable)
        {
            if (!ObjMappable.butYearMonth)
            {
                DateTime now = DateTime.Now;
                ObjMappable.ddlYear = now.Year;
                ObjMappable.ddlMonth = now.Month;
            }

            if (!ObjMappable.store_delivery_day_btn)
            {
                var checkDays = new List<int>();
                ErsCalendarRepository calendarRepository = ErsFactory.ersOrderFactory.GetErsCalendarRepository();
                ErsCalendarCriteria criteria = ErsFactory.ersOrderFactory.GetErsCalendarCriteria();
                DateTime startMonth = DateTime.Parse(ObjMappable.ddlYear.ToString() + "/" + ObjMappable.ddlMonth.ToString() + "/1");
                DateTime endMonth = startMonth.AddMonths(1).AddDays(-1);
                criteria.close_date_from = startMonth;
                criteria.close_date_to = endMonth;
                var listDate = calendarRepository.Find(criteria);
                foreach (var date in listDate)
                {
                    checkDays.Add(date.close_date.Value.Day);
                }
                ObjMappable.checkDays = checkDays.ToArray();
            }
        }

        private void MakeCalendar(IStoreCalendarMappable ObjMappable)
        {
            //月初日取得
            DateTime startMonth = DateTime.Parse(ObjMappable.ddlYear.ToString() + "/" + ObjMappable.ddlMonth.ToString() + "/1");
            //月終日取得
            DateTime endMonth = startMonth.AddMonths(1).AddDays(-1);

            var dayList = new List<Dictionary<string, object>>();

            // まず1日までの空のデータを取得
            for (var weekday = (int)DayOfWeek.Sunday; weekday < (int)startMonth.DayOfWeek; weekday++)
            {
                var calenderDay = new Dictionary<string, object>();
                calenderDay["visible"] = false;
                calenderDay["weekday"] = (DayOfWeek)weekday;
                dayList.Add(calenderDay);
            }

            for (int day = 0; day < endMonth.Day; day++)
            {
                var certainDate = startMonth.AddDays(day);
                var calenderDay = new Dictionary<string, object>();
                calenderDay["visible"] = true;
                calenderDay["day"] = certainDate.Day;
                calenderDay["weekday"] = certainDate.DayOfWeek;
                calenderDay["check"] = ObjMappable.checkDays.Contains(certainDate.Day);
                dayList.Add(calenderDay);
            }

            // 末日までのデータ取得
            for (var weekday = (int)endMonth.DayOfWeek; weekday < (int)DayOfWeek.Saturday; weekday++)
            {
                var calenderDay = new Dictionary<string, object>();
                calenderDay["visible"] = false;
                calenderDay["weekday"] = (DayOfWeek)weekday + 1;
                dayList.Add(calenderDay);
            }

            ObjMappable.dayList = dayList;
        }
    }
}