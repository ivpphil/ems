﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Mappers
{
    public class RoleModifyMapper : IMapper<IRoleModifyMappable>
    {
        public void Map(IRoleModifyMappable objMappable)
        {
            LoadObject(objMappable);
        }

        public void LoadObject(IRoleModifyMappable objMappable)
        {
            objMappable.role_action = this.GetRoleAction(objMappable);
            objMappable.role_gname = objMappable.d_role_gname;
        }

        /// <summary>
        /// RoleActionデータ取得
        /// </summary>
        public string[] GetRoleAction(IRoleModifyMappable objMappable)
        {
            var repository = ErsFactory.ersAdministratorFactory.GetErsRoleGroupRepository();
            var criteria = ErsFactory.ersAdministratorFactory.GetErsRoleGroupCriteria();
            criteria.id = objMappable.id;

            if (repository.GetRecordCount(criteria) == 0)
                throw new ErsException("10200");

            return ErsFactory.ersAdministratorFactory.GetErsRoleGroupWithId(objMappable.id).role_action;
        }
    }
}