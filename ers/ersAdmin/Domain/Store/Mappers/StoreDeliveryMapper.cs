﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Store.Mappers
{
    public class StoreDeliveryMapper : SiteRegisterBaseMapper, IMapper<IStoreDeliveryMappable>
    {
        public void Map(IStoreDeliveryMappable objMappable)
        {
            this.FindData(objMappable);
        }

        /// <summary>
        /// DBから値を取得
        /// </summary>
        public void FindData(IStoreDeliveryMappable objMappable)
        {
            // 初期サイトIDセット [Set default site ID]
            this.SetDefaultSiteId(objMappable);

            if(!objMappable.multiple_sites)
            {
                objMappable.site_id = objMappable.config_site_id;
            }

            var setup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(objMappable.site_id));

            if (setup != null)
            {
                objMappable.OverwriteWithParameter(setup.GetPropertiesAsDictionary());
            }
        }
    }
}