﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using ersAdmin.Models;
using jp.co.ivp.ers.util;
using ersAdmin.Models.store;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Store.Mappers
{
    public class StoreCarriageMapper : SiteRegisterBaseMapper, IMapper<IStoreCarriageMappable>
    {
        public void Map(IStoreCarriageMappable objMappable)
        {
            if (!objMappable.IsValid)
            { 
                 this.setReturnData(objMappable);
            }
            else
            {
                this.FindData(objMappable);
            }
        }

        /// <summary>
        /// DBから値を取得
        /// </summary>
        public void FindData(IStoreCarriageMappable objMappable)
        {
            // 初期サイトIDセット [Set default site ID]
            this.SetDefaultSiteId(objMappable);

            if(!objMappable.multiple_sites)
            {
                objMappable.site_id = objMappable.config_site_id;
            }

            var repository = ErsFactory.ersCommonFactory.GetErsPrefRepository();

            var criteria = ErsFactory.ersCommonFactory.GetErsPrefCriteria();
            criteria.active = EnumActive.Active;

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, criteria, "pref_t");

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            var prefList = repository.Find(criteria);

            var list = new List<store_carriage_table>();

            var itemCountInLine = 3;
            for (int index = 0; index < prefList.Count; index++)
            {
                var model = new store_carriage_table();
                model.top = ((index + 1) % itemCountInLine == 1);
                model.end = ((index + 1) % itemCountInLine == 0);
                model.id = prefList[index].id;
                model.pref_name = prefList[index].pref_name;
                model.carriage = prefList[index].carriage;
                list.Add(model);
            }
            objMappable.allList = list;

            var setup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(objMappable.site_id));
            objMappable.free = setup.free;
            objMappable.enable_carriage_tax = setup.enable_carriage_tax;
        }

        /// <summary>
        /// エラー時、入力された値から表示する値を取得
        /// </summary>
        public void setReturnData(IStoreCarriageMappable objMappable)
        {
            var itemCountInLine = 3;
            foreach (var model in objMappable.allList)
            {
                model.top = (model.lineNumber % itemCountInLine == 1);
                model.end = (model.lineNumber % itemCountInLine == 0);
                model.pref_name = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(model.id, (int)EnumSiteId.COMMON_SITE_ID);
            }
        }
    }
}