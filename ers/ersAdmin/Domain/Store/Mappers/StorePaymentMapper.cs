﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using ersAdmin.Models;
using ersAdmin.Models.store;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Store.Mappers
{
    public class StorePaymentMapper : SiteRegisterBaseMapper, IMapper<IStorePaymentMappable>
    {
        public void Map(IStorePaymentMappable objMappable)
        {
            this.FindData(objMappable);
        }
        /// <summary>
        /// DBから値を取得
        /// </summary>
        public void FindData(IStorePaymentMappable objMappable)
        {
            // 初期サイトIDセット [Set default site ID]
            this.SetDefaultSiteId(objMappable);

            if(!objMappable.multiple_sites)
            {
                objMappable.site_id = objMappable.config_site_id;
            }

            //Pay_tから一覧を取得してくる
            var payRepository = ErsFactory.ersOrderFactory.GetErsPayRepository();
            var payCriteria = ErsFactory.ersOrderFactory.GetErsPayCriteria();

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, payCriteria, "pay_t");

            payCriteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
            payCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            payCriteria.id_not_equal = 0;
            var payList = payRepository.Find(payCriteria);

            objMappable.store_payment_table = new List<Store_payment_table>();
            foreach (var payData in payList)
            {
                var store_payment = new Store_payment_table();
                store_payment.OverwriteWithParameter(payData.GetPropertiesAsDictionary());
                objMappable.store_payment_table.Add(store_payment);
            }

            //catrd_tから一覧を取得してくる
            var cardRepository = ErsFactory.ersOrderFactory.GetErsCardRepository();
            var cardCriteria = ErsFactory.ersOrderFactory.GetErsCardCriteria();

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, cardCriteria);

            cardCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var cardList = cardRepository.Find(cardCriteria);
            objMappable.store_card_table = new List<Store_card_table>();
            foreach (var cardData in cardList)
            {
                var store_card = new Store_card_table();
                store_card.OverwriteWithParameter(cardData.GetPropertiesAsDictionary());
                objMappable.store_card_table.Add(store_card);
            }
        }
    }
}