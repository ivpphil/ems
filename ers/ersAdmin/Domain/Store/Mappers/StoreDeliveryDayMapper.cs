﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.Send;

namespace ersAdmin.Domain.Store.Mappers
{
    public class StoreDeliveryDayMapper:IMapper<IStoreDeliveryDayMappable>
    {

        public void Map(IStoreDeliveryDayMappable ObjMappable)
        {
            if (!ObjMappable.IsValid)
            {
                this.setReturnData(ObjMappable);
            }
            else
            {
                this.FindData(ObjMappable);
            }

        }
        /// <summary>
        /// DBから値を取得
        /// </summary>
        public void FindData(IStoreDeliveryDayMappable ObjMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            ObjMappable.sendday = setup.sendday;
            ObjMappable.sendday_count = setup.sendday_count;


            var objSetup = ErsFactory.ersBatchFactory.getSetup();
            ObjMappable.shipping_csv_output_min_days = objSetup.shipping_csv_output_min_days;
            ObjMappable.create_regular_order_days = objSetup.create_regular_order_days;


            var sendtimeRepository = ErsFactory.ersOrderFactory.GetErsSendTimeRepository();
            var sendtimeCriteria = ErsFactory.ersOrderFactory.GetErsSendTimeCriteria();
            sendtimeCriteria.active = EnumActive.Active;
            sendtimeCriteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var list = sendtimeRepository.Find(sendtimeCriteria);
            ObjMappable.allList = ErsCommon.ConvertEntityListToDictionaryList(list);
        }

        public void setReturnData(IStoreDeliveryDayMappable ObjMappable)
        {
            List<ErsSendTime> list = new List<ErsSendTime>();
            foreach (var key in ObjMappable.sendtime.Keys)
            {
                var tmpData = ErsFactory.ersOrderFactory.GetErsSendTime();
                tmpData.id = key;
                tmpData.sendtime = ObjMappable.sendtime[key];
                list.Add(tmpData);
            }
            ObjMappable.allList = ErsCommon.ConvertEntityListToDictionaryList(list);
        }
    }
}