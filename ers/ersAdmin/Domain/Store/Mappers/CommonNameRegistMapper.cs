﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.merchandise.category;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using ersAdmin.Models.store;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Store.Mappers
{
    public class CommonNameRegistMapper
        : IMapper<ICommonNameRegistMappable>
    {
        public void Map(ICommonNameRegistMappable objMappable)
        {
            this.Type_code_list(objMappable);
            if (objMappable.Type_Code_List.Count() > 0 && objMappable.error_flg == false)
            {
                this.Type_code_search(objMappable);
            }
        }


        private void Type_code_list(ICommonNameRegistMappable objMappable)
        {
            var repository = ErsFactory.ersCommonFactory.GetErsCommonNameCodeRepository();
            var criteria = ErsFactory.ersCommonFactory.GetErsCommonNameCodeCriteria();
            criteria.type_code = EnumCommonNameType.CmnType;
            criteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
            var record = repository.Find(criteria);
            objMappable.Type_Code_List = record;
            if (record.Count <= 0)
            {
                return;
            }


            //ｵﾌﾟｼｮﾝ項目名
            var type_code = objMappable.Type_Code_List.First();
            if (objMappable.type_code == null)
            {
                if (record.Count == 1)
                {
                    objMappable.type_code = (EnumCommonNameType)Enum.Parse(typeof(EnumCommonNameType), type_code.opt_chr1);
                    if (type_code.opt_chr2 == null)
                    {
                        objMappable.item_flg = false;
                        return;
                    }
                    string stData = type_code.opt_chr2;
                    string[] stArrayData = stData.Split(',');
                    if (stArrayData.Length == 6)
                    {
                        int i = 0;
                        objMappable.stArrayData = new string[6];
                        foreach (string Data in stArrayData)
                        {
                            objMappable.stArrayData[i] = Data;
                            i += 1;

                        }
                        objMappable.item_flg = true;
                    }
                    else
                    {
                        objMappable.item_flg = false;
                    }
                }
  
            }
            else
            {
                foreach (var Data in record)
                {
                    if (objMappable.type_code == (EnumCommonNameType)Enum.Parse(typeof(EnumCommonNameType), ErsResources.GetMessage(Data.opt_chr1)))
                    {
                        if (Data.opt_chr2 == null)
                        {
                            objMappable.item_flg = false;
                            return;
                        }
                        string stData = Data.opt_chr2;
                        string[] stArrayData = stData.Split(',');
                        if (stArrayData.Length == 6)
                        {
                            int i = 0;
                            objMappable.stArrayData = new string[6];
                            foreach (string Data0 in stArrayData)
                            {
                                objMappable.stArrayData[i] = Data0;
                                i += 1;

                            }
                            objMappable.item_flg = true;
                        }
                        else
                        {
                            objMappable.item_flg = false;
                        }
                    }
                }
                    
            }



        }

        private void Type_code_search(ICommonNameRegistMappable objMappable)
        {
            if (objMappable.type_code == null)
            {
                return;
            }
            var repository = ErsFactory.ersCommonFactory.GetErsCommonNameCodeRepository();
            var criteria = ErsFactory.ersCommonFactory.GetErsCommonNameCodeCriteria();
            criteria.type_code = objMappable.type_code;
            criteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);

            var record = repository.Find(criteria);

            common_name_detail details = new common_name_detail();
            objMappable.common_name_table = new List<common_name_detail>();

            var listRec = new List<common_name_detail>();

            foreach (var list in record)
            {
                details = new common_name_detail();
                details.OverwriteWithParameter(list.GetPropertiesAsDictionary());

                listRec.Add(details);
            }
            var recordCount = listRec.Count;
            objMappable.common_name_table = listRec;
            objMappable.common_name_table = setDefaultLineNumber(objMappable, listRec, Convert.ToInt32(recordCount + 20), recordCount);
        }

        protected static List<common_name_detail> setDefaultLineNumber(ICommonNameRegistMappable objMappable, List<common_name_detail> contentList, int defRow, long ctr)
        {
            int defaultRow = defRow - (int)ctr;
            foreach (var row in Enumerable.Range(0, defaultRow))
            {
                var contentIteminsert = new common_name_detail();
                contentList.Add(contentIteminsert);
            }

            return contentList;
        }
    }
}