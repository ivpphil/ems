﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Store.Mappers
{
    public class StoreMailTextMapper : SiteRegisterBaseMapper, IMapper<IStoreMailTextMappable>
    {
        public void Map(IStoreMailTextMappable objMappable)
        {
            this.FindData(objMappable);
        }

        /// <summary>
        /// DBから値を取得
        /// </summary>
        public void FindData(IStoreMailTextMappable objMappable)
        {
            // 初期サイトIDセット [Set default site ID]
            this.SetDefaultSiteId(objMappable);

            if (!objMappable.multiple_sites)
            {
                objMappable.site_id = objMappable.config_site_id;
            }

            var templateRepository = ErsFactory.ersAdministratorFactory.GetErsMailTemplateRepository();
            var templateCriteria = ErsFactory.ersAdministratorFactory.GetErsMailTemplateCriteria();

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, templateCriteria, "mail_template_t");

            var templateList = templateRepository.Find(templateCriteria);
            this.getValue(templateList, objMappable);
        }

        /// <summary>
        /// 値をモデルにセットする
        /// </summary>
        /// <param name="temp"></param>
        private void getValue(IList<ErsMailTemplate> temp, IStoreMailTextMappable objMappable)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (ErsMailTemplate template in temp)
            {
                dictionary.Add(template.key, template.value);
            }
            objMappable.OverwriteWithParameter(dictionary);
        }
    }
}