﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Mappers
{
    public class StoreMallMailTextMapper:IMapper<IStoreMallMailTextMappable>
    {

        public void Map(IStoreMallMailTextMappable ObjMappable)
        {
            this.FindData(ObjMappable);
        }

        /// <summary>
        /// DBから値を取得
        /// </summary>
        public void FindData(IStoreMallMailTextMappable ObjMappable)
        {
            var templateRepository = ErsFactory.ersAdministratorFactory.GetErsMailTemplateRepository();
            var templateCriteria = ErsFactory.ersAdministratorFactory.GetErsMailTemplateCriteria();
            templateCriteria.site_id = ObjMappable.site_id;
            var templateList = templateRepository.Find(templateCriteria);
            this.getValue(templateList, ObjMappable);

        }

        /// <summary>
        /// 値をモデルにセットする
        /// </summary>
        /// <param name="temp"></param>
        private void getValue(IList<ErsMailTemplate> temp, IStoreMallMailTextMappable ObjMappable)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (ErsMailTemplate template in temp)
            {
                dictionary.Add(template.key, template.value);
            }
            ObjMappable.OverwriteWithParameter(dictionary);
        }
    }
}