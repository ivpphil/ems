﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using ersAdmin.Models.store;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Mappers
{
    public class StorePaymentListRecordMapper : IMapper<IStorePaymentListRecordMappable>
    {
        public void Map(IStorePaymentListRecordMappable objMappable)
        {
            this.LoadEtcList(objMappable);
        }

        public void LoadEtcList(IStorePaymentListRecordMappable objMappable)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsEtcRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsEtcCriteria();
            criteria.pay = objMappable.id.Value;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            objMappable.store_etc_table = new List<store_etc_table>();

            IList<ErsEtc> dataList = repository.Find(criteria);
            foreach (ErsEtc data in dataList)
            {
                store_etc_table tmpData = new store_etc_table();
                objMappable.store_etc_table.Add(tmpData);
                tmpData.lineNumber = objMappable.store_etc_table.Count;
                tmpData.OverwriteWithParameter(data.GetPropertiesAsDictionary());
            }
            if (objMappable.store_etc_table.Count < 9)
            {
                for (var i = objMappable.store_etc_table.Count; i <= 9; i++)
                {
                    store_etc_table tmpData = new store_etc_table();
                    objMappable.store_etc_table.Add(tmpData);
                    tmpData.pay = objMappable.id;
                    tmpData.id = -1;
                    tmpData.down = 0;
                    tmpData.more = 0;
                    tmpData.price = 0;
                }
            }
        }
    }
}