﻿using System.Collections.Generic;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersAdmin.Domain.Store.Mappers
{
    /// <summary>
    /// マッパー [Mapper]
    /// </summary>
    public class StoreMallSettingMapper
        : IMapper<IStoreMallSettingMappable>
    {
        /// <summary>
        /// マップ [Map]
        /// </summary>
        /// <param name="objMappable">マッパブル [Mappable]</param>
        public void Map(IStoreMallSettingMappable mappable)
        {
            // サイトリストセット [Set site list]
            this.SetSiteList(mappable);

            if (mappable.site_id.HasValue)
            {
                // モール店舗区分取得 [Get mall shop type]
                mappable.mall_shop_kbn = this.GetMallShopKbn(mappable);

                if (mappable.isFirstDisplay)
                {
                    // モール設定セット [Set mall setting]
                    this.SetMallSetting(mappable);
                }
            }
        }

        /// <summary>
        /// サイトリストセット [Set site list]
        /// </summary>
        /// <param name="mappable">マッパブル [Mappable]</param>
        protected void SetSiteList(IStoreMallSettingMappable mappable)
        {
            var repository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var criteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();

            criteria.mall_shop_kbn_not_in = new EnumMallShopKbn?[] { EnumMallShopKbn.ERS };
            criteria.SetActiveOnly();

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            mappable.listSite = repository.Find(criteria);
        }

        /// <summary>
        /// モール店舗区分取得 [Get mall shop type]
        /// </summary>
        /// <param name="mappable">マッパブル [Mappable]</param>
        /// <returns>モール店舗区分 [Mall shop type]</returns>
        protected EnumMallShopKbn? GetMallShopKbn(IStoreMallSettingMappable mappable)
        {
            // モール店舗区分取得 [Get mall shop type]
            foreach (var data in mappable.listSite)
            {
                if (data.id == mappable.site_id)
                {
                    return data.mall_shop_kbn;
                }
            }

            return null;
        }

        /// <summary>
        /// モール設定セット [Set mall setting]
        /// </summary>
        /// <param name="mappable">マッパブル [Mappable]</param>
        protected void SetMallSetting(IStoreMallSettingMappable mappable)
        {
            // モール設定取得（サイトIDから） [Get mall settings by site id]
            var dicData = this.GetMallSetupBySiteId(mappable.site_id);

            if (dicData != null)
            {
                mappable.OverwriteWithParameter(dicData);
            }
        }

        /// <summary>
        /// モール設定取得（サイトIDから） [Get mall settings by site id]
        /// </summary>
        /// <param name="site_id"></param>
        /// <returns></returns>
        protected IDictionary<string, object> GetMallSetupBySiteId(int? site_id)
        {
            var repository = ErsMallFactory.ersSiteFactory.GetErsMallSetupRepository();
            var criteria = ErsMallFactory.ersSiteFactory.GetErsMallSetupCriteria();

            criteria.site_id = site_id;

            if (repository.GetRecordCount(criteria) == 0)
            {
                return null;
            }

            var dicRet = new Dictionary<string, object>();

            var listFind = repository.Find(criteria);

            foreach (var data in listFind)
            {
                dicRet[data.key] = data.value;
            }

            return dicRet;
        }
    }
}