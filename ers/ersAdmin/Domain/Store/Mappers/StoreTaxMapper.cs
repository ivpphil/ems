﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using ersAdmin.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Store.Mappers
{
    public class StoreTaxMapper:IMapper<IStoreTaxMappable>
    {
        public void Map(IStoreTaxMappable ObjMappable)
        {
            this.FindData(ObjMappable);
        }
        /// <summary>
        /// DBから値を取得
        /// </summary>
        public void FindData(IStoreTaxMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            objMappable.tax = setup.tax;
            objMappable.enable_carriage_tax = setup.enable_carriage_tax;

            
            var repo = ErsFactory.ersOrderFactory.GetErsPayRepository();
            var cri = ErsFactory.ersOrderFactory.GetErsPayCriteria();
            cri.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
            cri.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            cri.site_id = (int)EnumSiteId.COMMON_SITE_ID;
            cri.active = EnumActive.Active;
            var list = repo.Find(cri);

            objMappable.store_payment_table = new List<Store_tax_payment_table>();
            foreach (var item in list)
            {
                var store_payment = new Store_tax_payment_table();
                store_payment.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                objMappable.store_payment_table.Add(store_payment);
            }
        }
    }
}