﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.administrator.role_group;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Mappers
{
    public class RoleMapper : IMapper<IRoleMappable>
    {
        public void Map(IRoleMappable objMappable)
        {
            Init(objMappable);
        }

        /// <summary>
        /// リスト取得
        /// </summary>
        public void Init(IRoleMappable objMappable)
        {            
            var repository = ErsFactory.ersAdministratorFactory.GetErsRoleGroupRepository();

            var criteria = GetCriteria();

            objMappable.search_result_cnt = repository.GetRecordCount(criteria);

            if (objMappable.search_result_cnt <= 0)
            {
                throw new ErsException("10200");
            }

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);
            var list = repository.Find(criteria);
            objMappable.Role_Group_List = ErsCommon.ConvertEntityListToDictionaryList(list);
        }

        /// <summary>
        /// 検索条件取得
        /// </summary>
        /// <returns></returns>
        private ErsRoleGroupCriteria GetCriteria()
        {
            var criteria = ErsFactory.ersAdministratorFactory.GetErsRoleGroupCriteria();
            criteria.active = EnumActive.Active;
            return criteria;
        }
    }
}