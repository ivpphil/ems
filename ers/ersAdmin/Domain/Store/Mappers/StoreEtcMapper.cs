﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using ersAdmin.Models;
using ersAdmin.Models.store;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappers;

namespace ersAdmin.Domain.Store.Mappers
{
    public class StoreEtcMapper : SiteRegisterBaseMapper, IMapper<IStoreEtcMappable>
    {

        public void Map(IStoreEtcMappable objMappable)
        {
            this.FindData(objMappable);
        }

        /// <summary>
        /// DBから値を取得
        /// </summary>
        public void FindData(IStoreEtcMappable objMappable)
        {
            // 初期サイトIDセット [Set default site ID]
            this.SetDefaultSiteId(objMappable);

            if(!objMappable.multiple_sites)
            {
                objMappable.site_id = objMappable.config_site_id;
            }

            //Pay_tから有効なものを取得してくる
            var payRepository = ErsFactory.ersOrderFactory.GetErsPayRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsPayCriteria();

            criteria.id_not_equal = 0;

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, criteria, "pay_t");

            criteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            var payList = payRepository.Find(criteria);

            objMappable.store_payment_table = new List<Store_payment_table>();

            //Pay_tの一覧をもとにリストをつくる
            foreach (var pay_data in payList)
            {
                var tmpPayment = new Store_payment_table();
                objMappable.store_payment_table.Add(tmpPayment);
                tmpPayment.OverwriteWithParameter(pay_data.GetPropertiesAsDictionary());

                //tmpPayment.LoadEtcList();

                this.LoadEtcList(tmpPayment, Convert.ToInt32(objMappable.site_id));
            }
        }

        internal void LoadEtcList(IStorePaymentListRecordMappable objMappable, int site_id)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsEtcRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsEtcCriteria();
            criteria.pay = objMappable.id.Value;
            criteria.site_id = site_id;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            objMappable.store_etc_table = new List<store_etc_table>();

            IList<ErsEtc> dataList = repository.Find(criteria);
            foreach (ErsEtc data in dataList)
            {
                store_etc_table tmpData = new store_etc_table();
                objMappable.store_etc_table.Add(tmpData);
                tmpData.lineNumber = objMappable.store_etc_table.Count;
                tmpData.OverwriteWithParameter(data.GetPropertiesAsDictionary());
            }
            if (objMappable.store_etc_table.Count < 9)
            {
                for (var i = objMappable.store_etc_table.Count; i <= 9; i++)
                {
                    store_etc_table tmpData = new store_etc_table();
                    objMappable.store_etc_table.Add(tmpData);
                    tmpData.pay = objMappable.id;
                    tmpData.id = -1;
                    tmpData.down = 0;
                    tmpData.more = 0;
                    tmpData.price = 0;
                }
            }
        }

    }
}