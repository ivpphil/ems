﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.Send;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class StoreCalendarHandler : ICommandHandler<IStoreCalendarCommand>
    {
        public ICommandResult Submit(IStoreCalendarCommand command)
        {
            this.UpdateData(command);
            return new CommandResult(true);
        }
        internal void UpdateData(IStoreCalendarCommand command)
        {
            //一旦当該年月のデータを削除する
            this.DeleteData(command);

            if (command.checkDays == null || command.checkDays.Length == 0)
                return;

            //チェックついてるやつを登録
            foreach (int day in command.checkDays)
            {
                ErsCalendarRepository calendarRepository = ErsFactory.ersOrderFactory.GetErsCalendarRepository();
                DateTime now = DateTime.Parse(command.ddlYear.ToString() + "/" + command.ddlMonth.ToString() + "/" + day);

                var insData = ErsFactory.ersOrderFactory.GetErsCalendar();
                insData.close_date = now;
                calendarRepository.Insert(insData);
            }
        }



        private void DeleteData(IStoreCalendarCommand command)
        {
            ErsCalendarRepository calendarRepository = ErsFactory.ersOrderFactory.GetErsCalendarRepository();
            DateTime fromDate = DateTime.Parse(command.ddlYear.ToString() + "/" + command.ddlMonth.ToString() + "/01");
            DateTime toDate = DateTime.Parse(command.ddlYear.ToString() + "/" + command.ddlMonth.ToString() + "/01");
            toDate = toDate.AddMonths(1);
            toDate = toDate.AddDays(-1);
            ErsCalendarCriteria delteteCriteria = ErsFactory.ersOrderFactory.GetErsCalendarCriteria();
            delteteCriteria.close_date_from = fromDate;
            delteteCriteria.close_date_to = toDate;
            IList<ErsCalendar> allData = calendarRepository.Find(delteteCriteria);
            foreach (ErsCalendar data in allData)
            {
                calendarRepository.Delete(data);
            }
        }
    }
}