﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateRoleModify : IValidationHandler<IRoleModifyCommand>
    {
        public IEnumerable<ValidationResult> Validate(IRoleModifyCommand command)
        {
            yield return command.CheckRequired("id");
            yield return ErsFactory.ersAdministratorFactory.GetCheckRoleGroupStgy().CheckId(command.id);

            if (command.submit_change)
            {
                yield return command.CheckRequired("role_gname");
                if (command.role_action == null)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10206", new[] { ErsResources.GetFieldName("role_group_t.role_action") }), new[] { "role_action" });
                }
                else
                {
                    foreach (var action in command.role_action)
                    {
                        yield return ErsFactory.ersAdministratorFactory.GetCheckRoleActionStgy().Check(action);
                    }
                }

                //重複チェック
                if (command.role_gname != null)
                {
                    if (!ErsFactory.ersAdministratorFactory.GetCheckDuplicateRoleGnameStgy().CheckDuplicate(command.role_gname, command.id))
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("10103",
                            new[] { ErsResources.GetFieldName("role_group_t.role_gname"), command.role_gname }), new[] { "role_gname" });
                    }
                }

            }
        }
    }
}