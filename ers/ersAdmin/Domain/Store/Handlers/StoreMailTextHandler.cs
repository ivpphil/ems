﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class StoreMailTextHandler : ICommandHandler<IStoreMailTextCommand>
    {
        public ICommandResult Submit(IStoreMailTextCommand command)
        {
            if(!command.multiple_sites)
            {
                command.site_id = command.config_site_id;
            }

            this.InsertTemplateData(command);
            return new CommandResult(true);
        }
        internal void InsertTemplateData(IStoreMailTextCommand command)
        {
            Dictionary<string, object> tmpDic = command.GetPropertiesAsDictionary();
            foreach (var key in GetKeyList())
            {
                this.RegistValue(tmpDic, key, Convert.ToInt32(command.site_id));
            }

        }

        private List<string> GetKeyList()
        {
            var list = new List<string>();
            list.Add("entry_pc_title");
            list.Add("entry_pc_body");
            list.Add("entry_mobile_title");
            list.Add("entry_mobile_body");
            list.Add("register_pc_title");
            list.Add("register_pc_body");
            list.Add("register_mobile_title");
            list.Add("register_mobile_body");
            list.Add("delivery_pc_title");
            list.Add("delivery_pc_body");
            list.Add("delivery_mobile_title");
            list.Add("delivery_mobile_body");
            list.Add("quit_pc_title");
            list.Add("quit_pc_body");
            list.Add("quit_mobile_title");
            list.Add("quit_mobile_body");
            list.Add("passrim_pc_title");
            list.Add("passrim_pc_body");
            list.Add("passrim_mobile_title");
            list.Add("passrim_mobile_body");
            list.Add("individual_pc_title");
            list.Add("individual_pc_body");
            list.Add("quest_pc_title");
            list.Add("quest_pc_body");
            list.Add("regular_order_pc_title");
            list.Add("regular_order_pc_body");
            list.Add("regular_order_mobile_title");
            list.Add("regular_order_mobile_body");
            list.Add("quest_accept_pc_title");
            list.Add("quest_accept_pc_body");
            list.Add("quest_accept_mobile_title");
            list.Add("quest_accept_mobile_body");
            return list;
        }

        private void RegistValue(Dictionary<string, object> tmpDic, string key, int? site_id)
        {
            var templateRepository = ErsFactory.ersAdministratorFactory.GetErsMailTemplateRepository();
            var templateCriteria = ErsFactory.ersAdministratorFactory.GetErsMailTemplateCriteria();
            templateCriteria.key = key;
            templateCriteria.site_id = site_id;

            ErsMailTemplate template;
            var templateList = templateRepository.Find(templateCriteria);
            if (templateList.Count == 0)
            {
                template = ErsFactory.ersAdministratorFactory.GetErsMailTemplate();
                template.key = key;
                template.site_id = site_id;
                template.value = Convert.ToString(tmpDic[key]);
                templateRepository.Insert(template);
            }
            else
            {
                var oldTemplate = templateRepository.Find(templateCriteria)[0];

                template = templateList[0];
                template.value = Convert.ToString(tmpDic[key]);
                template.site_id = site_id;

                templateRepository.Update(oldTemplate, template);
            }
        }
    }
}