﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class RoleRegistHandler : ICommandHandler<IRoleRegistCommand>
    {
        public ICommandResult Submit(IRoleRegistCommand command)
        {
            //insert role
            Insert(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// データ登録
        /// </summary>
        public void Insert(IRoleRegistCommand command)
        {
            //トランザクション開始

            var repository = ErsFactory.ersAdministratorFactory.GetErsRoleGroupRepository();

            var NewsRoleGroup = ErsFactory.ersAdministratorFactory.GetErsRoleGroup();
            NewsRoleGroup.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            NewsRoleGroup.intime = DateTime.Now;

            repository.Insert(NewsRoleGroup, true);
        }
    }
}