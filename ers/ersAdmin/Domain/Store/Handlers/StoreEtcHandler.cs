﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class StoreEtcHandler : ICommandHandler<IStoreEtcCommand>
    {
        public ICommandResult Submit(IStoreEtcCommand command)
        {
            if(!command.multiple_sites)
            {
                command.site_id = command.config_site_id;
            }

            this.UpdateEtc(command);
            return new CommandResult(true);
        }


        internal void UpdateEtc(IStoreEtcCommand command)
        {
            var payRepository = ErsFactory.ersOrderFactory.GetErsPayRepository();
            foreach (var payItem in command.store_payment_table)
            {
                var pay = ErsFactory.ersOrderFactory.GetErsPayWithIdAndSiteId(payItem.id, Convert.ToInt32(command.site_id));
                pay.OverwriteWithModel(payItem);
                var old_pay = ErsFactory.ersOrderFactory.GetErsPayWithIdAndSiteId(payItem.id.Value, Convert.ToInt32(command.site_id));
                payRepository.Update(old_pay, pay);

                var etcRepository = ErsFactory.ersOrderFactory.GetErsEtcRepository();
                foreach (var etcItem in payItem.store_etc_table)
                {
                    if (etcItem.id == -1)
                    {
                        var etc = ErsFactory.ersOrderFactory.GetErsEtc();
                        etc.OverwriteWithModel(etcItem);
                        etc.site_id = Convert.ToInt32(command.site_id);
                        etcRepository.Insert(etc, true);
                    }
                    else
                    {
                        var etc = ErsFactory.ersOrderFactory.GetErsEtcWithId(etcItem.id.Value);
                        etc.OverwriteWithModel(etcItem);
                        var old_etc = ErsFactory.ersOrderFactory.GetErsEtcWithId(etcItem.id.Value);
                        etcRepository.Update(old_etc, etc);
                    }
                }
            }
        }
    }
}