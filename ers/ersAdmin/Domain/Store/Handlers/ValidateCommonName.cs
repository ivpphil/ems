﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.store;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.doc_bundle;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateCommonName
        : IValidationHandler<ICommonNameCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICommonNameCommand command)
        {

            var listCode_c = new List<int>();

            if (command.common_name_table != null)
            {

                //必須チェック表示
                foreach (var item in command.common_name_table)
                {

                    foreach (var errorMessage in item.GetAllErrorMessageList())
                        yield return new ValidationResult(errorMessage, new[] { "common_name_table" });
                    if (!IsEmpty(item))
                    {
                        common_name_detail listCode = new common_name_detail();
                        listCode = item;

                        listCode.AddInvalidField(command.controller.commandBus.Validate<ICommonNameRecordCommand>(listCode));
                        if (!listCode.IsValid)
                        {
                            foreach (var errorMessage in listCode.GetAllErrorMessageList())
                                yield return new ValidationResult(errorMessage, new[] { "common_name_table" });
                        }

                        if (item.code != null)
                        {
                            if (listCode_c.Contains(item.code.Value))
                            {
                                yield return new ValidationResult(
                                    ErsResources.GetMessage("10103", ErsResources.GetFieldName("common_namecode_t.code"), item.code),
                                    new[] { "code" });
                            }
                            else
                            {
                                listCode_c.Add(item.code.Value);
                            }
                        }
                    }

                }
            }
            yield break;
        }

        public bool IsEmpty(ICommonNameRecordCommand command)
        {
            if (command.id == null
                && command.code == null
                && command.namename == null
                && command.opt_chr1 == null
                && command.opt_chr2 == null
                && command.opt_flg1 == 0
                && command.opt_flg2 == 0
                && command.opt_num1 == null
                && command.opt_num2 == null
                && command.disp_order == null
                && command.delete == false)
                return true;

            return false;
        }
    }
}