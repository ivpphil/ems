﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class StorePaymentHandler : ICommandHandler<IStorePaymentCommand>
    {
        public ICommandResult Submit(IStorePaymentCommand command)
        {
            if (!command.multiple_sites)
            {
                command.site_id = command.config_site_id;
            }

            this.UpdatePayment(command);
            return new CommandResult(true);
        }

        internal void UpdatePayment(IStorePaymentCommand command)
        {

            var payRepository = ErsFactory.ersOrderFactory.GetErsPayRepository();
            var cardRepository = ErsFactory.ersOrderFactory.GetErsCardRepository();
            foreach (var store_payment in command.store_payment_table)
            {
                if (store_payment.id != null)
                {
                    var pay = ErsFactory.ersOrderFactory.GetErsPayWithIdAndSiteId(store_payment.id.Value, Convert.ToInt32(command.site_id));
                    pay.OverwriteWithModel(store_payment);
                    pay.site_id = Convert.ToInt32(command.site_id);
                    var old_pay = ErsFactory.ersOrderFactory.GetErsPayWithIdAndSiteId(store_payment.id.Value, Convert.ToInt32(command.site_id));
                    payRepository.Update(old_pay, pay);
                }
            }

            foreach (var store_card in command.store_card_table)
            {
                if (store_card.id != null)
                {
                    var card = ErsFactory.ersOrderFactory.GetErsCardWithId(store_card.id.Value);
                    card.OverwriteWithModel(store_card);
                    card.site_id = Convert.ToInt32(command.site_id);
                    var old_card = ErsFactory.ersOrderFactory.GetErsCardWithId(store_card.id.Value);
                    cardRepository.Update(old_card, card);
                }
            }

        }
    }
}