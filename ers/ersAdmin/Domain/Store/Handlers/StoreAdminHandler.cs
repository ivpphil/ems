﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class StoreAdminHandler
        : ICommandHandler<IStoreAdminCommand>
    {
        public ICommandResult Submit(IStoreAdminCommand command)
        {
            if(!command.multiple_sites)
            {
                command.site_id = command.config_site_id;
            }

            //メールアドレスの登録
            this.RegistEmail(command);
            //管理者アカウントの更新
            this.RegistAdministrator(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// メールアドレスの登録
        /// </summary>
        private void RegistEmail(IStoreAdminCommand command)
        {
            ErsSetupRepository setupRepository = ErsFactory.ersUtilityFactory.GetErsSetupRepository();
            ErsSetup oldSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(command.site_id));
            ErsSetup newSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(command.site_id));
            newSetup.r_email = command.replying_mail_addr;
            newSetup.f_email1 = command.report_mail_addr1;
            newSetup.f_email2 = command.report_mail_addr2;
            newSetup.f_email3 = command.report_mail_addr3;
            newSetup.quest_email_to = command.quest_email_to;
            setupRepository.Update(oldSetup, newSetup);
        }

        /// <summary>
        /// 管理者情報の更新をする
        /// </summary>
        private void RegistAdministrator(IStoreAdminCommand command)
        {
            foreach (var item in command.adminList)
            {
                item.user_cd = item.user_login_id;

                if (!this.IsEmpty(item))
                {
                    if (item.id != null)
                    {
                        if (item.delete)
                        {
                            this.DeleteAdministrator(item);
                        }
                        else
                        {
                            this.UpdateAdministrator(item);
                        }
                    }
                    else
                    {
                        this.InsertAdministrator(item);
                    }
                }
            }
        }

        public bool IsEmpty(IStoreAdminListRecordCommand command)
        {
            if (command.id == null
                && command.user_login_id == null
                && command.user_name == null
                && command.passwd == null
                && command.role_gcode == null
                && command.agent_id == null
              )
                return true;

            return false;
        }

        /// <summary>
        /// 管理者を新規登録する
        /// </summary>
        /// <param name="item"></param>
        private void InsertAdministrator(IStoreAdminListRecordCommand item)
        {
            var administratorRepository = ErsFactory.ersAdministratorFactory.GetErsAdministratorRepository();

            var objAdministrator = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithParameter(item.GetPropertiesAsDictionary());
            objAdministrator.pass_date = DateTime.Now;

            administratorRepository.Insert(objAdministrator);
        }

        /// <summary>
        /// 管理者を更新する
        /// </summary>
        /// <param name="item"></param>
        private void UpdateAdministrator(IStoreAdminListRecordCommand item)
        {
            var administratorRepository = ErsFactory.ersAdministratorFactory.GetErsAdministratorRepository();

            var oidAdministrator = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithId(item.id);

            var objAdministrator = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithId(item.id);
            objAdministrator.OverwriteWithParameter(item.GetPropertiesAsDictionary());

            if (oidAdministrator.passwd != objAdministrator.passwd)
            {
                objAdministrator.pass_date = DateTime.Now;
            }

            administratorRepository.Update(oidAdministrator, objAdministrator);
        }

        /// <summary>
        /// 管理者を削除する
        /// </summary>
        /// <param name="item"></param>
        private void DeleteAdministrator(IStoreAdminListRecordCommand item)
        {
            var administratorRepository = ErsFactory.ersAdministratorFactory.GetErsAdministratorRepository();

            var objAdministrator = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithParameter(item.GetPropertiesAsDictionary());

            administratorRepository.Delete(objAdministrator);
        }
    }
}