﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreAdminListRecord
        : IValidationHandler<IStoreAdminListRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IStoreAdminListRecordCommand command)
        {
            foreach(var result in this.ValidateValue(command))
            {
                yield return result;
            }
        }

        public IEnumerable<ValidationResult> ValidateValue(IStoreAdminListRecordCommand command)
        {
            yield return command.CheckRequired("user_login_id");
            yield return command.CheckRequired("passwd");
            yield return command.CheckRequired("role_gcode");

            if (!string.IsNullOrEmpty(command.user_login_id))
            {
                //自分と同じ内容のデータを取得して、あったらエラー
                var repository = ErsFactory.ersAdministratorFactory.GetErsAdministratorRepository();
                var criteria = ErsFactory.ersAdministratorFactory.GetErsAdministratorCriteria();
                criteria.user_login_id = command.user_login_id;
                criteria.id_not_equal = command.id;
                if (repository.GetRecordCount(criteria) > 0)
                {
                    //エラー
                    yield return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("user_login_id"), command.user_login_id), new[] { "user_login_id" });
                }
                else
                {
                    var containerModel = (IStoreAdminCommand)command.containerModel;

                    foreach (var anotherRecord in containerModel.adminList)
                    {
                        if (anotherRecord == command)
                        {
                            continue;
                        }

                        if (anotherRecord.user_login_id == command.user_login_id)
                        {
                            //エラー
                            yield return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("user_login_id"), command.user_login_id), new[] { "user_login_id" });
                        }
                    }
                }
            }

            var validateRoleGroup = ErsFactory.ersAdministratorFactory.GetValidateRoleGroupStgy();

            foreach (var result in validateRoleGroup.Validate(new[] { command.role_gcode }))
            {
                yield return result;
            }

            yield return ErsFactory.ersCtsOperatorFactory.GetCheckExistAgentStgy().Validate(command.agent_id);
        }
    }
}