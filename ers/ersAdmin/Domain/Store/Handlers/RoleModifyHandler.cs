﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class RoleModifyHandler : ICommandHandler<IRoleModifyCommand>
    {
        public ICommandResult Submit(IRoleModifyCommand command)
        {
            //update role
            Update(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// アップデート
        /// </summary>
        public void Update(IRoleModifyCommand command)
        {
            if (command.submit_change)
            {
                //トランザクション開始
                var repository = ErsFactory.ersAdministratorFactory.GetErsRoleGroupRepository();

                var NewsRoleGroup = ErsFactory.ersAdministratorFactory.GetErsRoleGroupWithId(command.id);
                NewsRoleGroup.OverwriteWithParameter(command.GetPropertiesAsDictionary());

                var old_RoleGourp = ErsFactory.ersAdministratorFactory.GetErsRoleGroupWithId(command.id);
                NewsRoleGroup.intime = old_RoleGourp.intime;
                repository.Update(old_RoleGourp, NewsRoleGroup);
            }

        }
    }
}