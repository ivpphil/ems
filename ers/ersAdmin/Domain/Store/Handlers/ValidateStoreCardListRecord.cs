﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreCardListRecord : IValidationHandler<IStoreCardListRecordCommand>
    {
        public virtual IEnumerable<ValidationResult> Validate(IStoreCardListRecordCommand command)
        {
            yield return command.CheckRequired("id");
            yield return command.CheckRequired("card_name");

            if (command.active == null)
                command.active = EnumActive.NonActive;
        }
    }
}