﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;

namespace ersAdmin.Domain.Store.Handlers
{
    public class StoreEtcListRecordHandler : ICommandHandler<IStoreEtcListRecordCommand>
    {

        public ICommandResult Submit(IStoreEtcListRecordCommand command)
        {

            //this.InsertAlertData(command);

            return new CommandResult(true);
        }
    }
}