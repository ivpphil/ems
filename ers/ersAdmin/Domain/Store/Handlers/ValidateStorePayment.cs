﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Models;
using ersAdmin.Models.store;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStorePayment : IValidationHandler<IStorePaymentCommand>
    {
        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(IStorePaymentCommand command)
        {
            if (command.store_payment_btn)
            {
                if (command.multiple_sites)
                {
                    yield return command.CheckRequired("site_id");
                }

                if (command.store_payment_table != null)
                {
                    foreach (Store_payment_table model in command.store_payment_table)
                    {
                        model.AddInvalidField(command.controller.commandBus.Validate<IStorePaymentListRecordCommand>(model));

                        if (!model.IsValid)
                        {
                            foreach (var errorMessage in model.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "store_payment_table" });
                            }
                        }
                    }
                }

                if (command.store_card_table != null)
                {
                    foreach (Store_card_table model in command.store_card_table)
                    {
                        model.AddInvalidField(command.controller.commandBus.Validate<IStoreCardListRecordCommand>(model));

                        if (!model.IsValid)
                        {
                            foreach (var errorMessage in model.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "store_payment_table" });
                            }
                        }
                    }
                }
            }
        }
    }
}