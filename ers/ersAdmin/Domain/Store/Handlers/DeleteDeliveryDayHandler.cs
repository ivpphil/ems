﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class DeleteDeliveryDayHandler : ICommandHandler<IDeleteDeliveryDayCommand>
    {
        public ICommandResult Submit(IDeleteDeliveryDayCommand command)
        {
            this.DeleteData(command);
            return new CommandResult(true);
        }

        internal void DeleteData(IDeleteDeliveryDayCommand command)
        {
            var sendtimeRepository = ErsFactory.ersOrderFactory.GetErsSendTimeRepository();
            var objDelete = ErsFactory.ersOrderFactory.GetErsSendTimeWithId(command.delete_id);
            sendtimeRepository.Delete(objDelete);
        }
    }
}