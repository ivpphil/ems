﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class StoreAlertHandler : ICommandHandler<IStoreAlertCommand>
    {
        public ICommandResult Submit(IStoreAlertCommand command)
        {
            if(!command.multiple_sites)
            {
                command.site_id = command.config_site_id;
            }
         
            this.InsertAlertData(command);

            return new CommandResult(true);
        }


        internal void InsertAlertData(IStoreAlertCommand command)
        {
            ErsSetupRepository repository = ErsFactory.ersUtilityFactory.GetErsSetupRepository();
            ErsSetup oldSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(command.site_id));
            ErsSetup newSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(command.site_id));
            newSetup.pri_memo = command.pri_memo;
            newSetup.regi_memo = command.regi_memo;
            newSetup.error_message = command.error_message;
            newSetup.m_error_message = command.m_error_message;
            newSetup.wh_owner_description = command.wh_owner_description;
            repository.Update(oldSetup, newSetup);

            var payRepository = ErsFactory.ersOrderFactory.GetErsPayRepository();
            var cardRepository = ErsFactory.ersOrderFactory.GetErsCardRepository();
            foreach (var store_payment in command.store_payment_table)
            {
                if (store_payment.id != null)
                {
                    var pay = ErsFactory.ersOrderFactory.GetErsPayWithIdAndSiteId(store_payment.id.Value, Convert.ToInt32(command.site_id));
                    pay.OverwriteWithModel(store_payment);
                    var old_pay = ErsFactory.ersOrderFactory.GetErsPayWithIdAndSiteId(store_payment.id.Value, Convert.ToInt32(command.site_id));
                    payRepository.Update(old_pay, pay);
                }
            }
        }    
    }
}