﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreCarriageRecord
        : IValidationHandler<IStoreCarriageRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IStoreCarriageRecordCommand command)
        {
            yield return command.CheckRequired("id");
            yield return command.CheckRequired("carriage");
        }
    }
}