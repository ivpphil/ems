﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using ersAdmin.Models.store;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStorePaymentListRecord : IValidationHandler<IStorePaymentListRecordCommand>
    {

        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> Validate(IStorePaymentListRecordCommand command)
        {
            if (command.store_etc_table != null)
            {
                var lastDown = -1;

                //foreach (var store_etc in command.store_etc_table)
                //{
                foreach (store_etc_table model in command.store_etc_table)
                {

                    model.AddInvalidField(command.controller.commandBus.Validate<IStoreEtcListRecordCommand>(model));

                    if (model.more != null && model.down != null && (model.more != 0 || model.down != 0) && model.more < lastDown)
                    {
                        var lastMessage = ErsResources.GetMessage("line_name", model.lineNumber - 1) + ErsResources.GetFieldName("down");
                        var currentMessage = ErsResources.GetFieldName("more");
                        model.AddInvalidField(new ValidationResult(ErsResources.GetMessage("10023", currentMessage, lastMessage), new[] { "down", "more" }));
                    }

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "store_etc_table" });
                        }
                    }
                    else
                    {
                        lastDown = model.down.Value;
                    }
                }
            }

            yield return command.CheckRequired("id");
            yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().Validate("id", command.id, false, false);

            yield return command.CheckRequired("pay_name");
            yield return command.CheckRequired("etc_name");

            if (command.active == null)
                command.active = EnumActive.NonActive;

            if (command.etc_flg == null)
                command.etc_flg = EnumOnOff.Off;
        }

    }
}