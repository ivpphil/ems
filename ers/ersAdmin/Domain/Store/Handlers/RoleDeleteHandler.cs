﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.db;
using ersAdmin.Domain.Store.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class RoleDeleteHandler : ICommandHandler<IRoleDeleteCommand>
    {
        public ICommandResult Submit(IRoleDeleteCommand command)
        {
            //update role
            Delete(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// データ削除
        /// </summary>
        public void Delete(IRoleDeleteCommand command)
        {
            if (command.submit_delete)
            {
                command.controller.mapperBus.Map<IRoleModifyMappable>((IRoleModifyMappable)command);

                //トランザクション開始

                var repository = ErsFactory.ersAdministratorFactory.GetErsRoleGroupRepository();

                var NewsRoleGroup = ErsFactory.ersAdministratorFactory.GetErsRoleGroupWithId(command.id);

                repository.Delete(NewsRoleGroup);

            }
        }
    }
}