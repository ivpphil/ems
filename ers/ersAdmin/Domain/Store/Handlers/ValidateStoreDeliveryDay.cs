﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.Send;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreDeliveryDay : IValidationHandler<IStoreDeliveryDayCommand>
    {
        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(IStoreDeliveryDayCommand command)
        {
            if (command.store_delivery_day_btn)
            {
                yield return command.CheckRequired("sendday");
                yield return command.CheckRequired("sendday_count");

                var sendday_count = ErsFactory.ersUtilityFactory.getSetup().MaxSenddayCount;
                if (command.sendday_count.HasValue && command.sendday_count > sendday_count)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10024", ErsResources.GetFieldName("sendday_count"), sendday_count), new[] { "sendday_count" });
                }

                //yield return CheckDuplicate(0, add_sendtime);
                ErsSendTimeRepository repository = ErsFactory.ersOrderFactory.GetErsSendTimeRepository();
                ErsSendTimeCriteria criteria = ErsFactory.ersOrderFactory.GetErsSendTimeCriteria();
                foreach (var key in command.sendtime.Keys)
                {
                    yield return command.CheckRequired("希望時間帯", "sendtime", command.sendtime[key]);
                    yield return CheckDuplicate(key, command.sendtime[key]);
                    if (command.add_sendtime != "" && command.add_sendtime == command.sendtime[key])
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("sendtime"), command.add_sendtime), new[] { "sendtime" });
                    }
                }

                if (command.IsValidField("sendday") && command.IsValidField("sendday_count"))
                {
                    if (command.sendday > command.sendday_count)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("10023", ErsResources.GetFieldName("sendday_count"), command.sendday), new[] { "sendday_count" });
                    }
                    else
                    {
                        if (0 == command.sendday_count)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("10023", ErsResources.GetFieldName("sendday_count"), 0), new[] { "sendday_count" });
                        }
                    }
                }
            }
        }

        private ValidationResult CheckDuplicate(int? id, string value)
        {
            ErsSendTimeRepository repository = ErsFactory.ersOrderFactory.GetErsSendTimeRepository();
            ErsSendTimeCriteria criteria = ErsFactory.ersOrderFactory.GetErsSendTimeCriteria();
            criteria.sendtime = value;
            criteria.active = EnumActive.Active;
            IList<ErsSendTime> dataList = repository.Find(criteria);
            if (dataList.Count != 0)
            {
                //見つけてきたのが自分かどうか、idで見る
                if (dataList[0].id != id)
                {
                    //エラー
                    return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("sendtime"), value), new[] { "sendtime" });
                }
            }
            return null;
        }

    }
}