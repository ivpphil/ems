﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.store;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateCommonNameRecord
        : IValidationHandler<ICommonNameRecordCommand>
    {
        public virtual IEnumerable<ValidationResult> Validate(ICommonNameRecordCommand command)
        {
            if (command.delete) yield break;

            yield return command.CheckRequired("code");
            yield return command.CheckRequired("type_code");
            yield return command.CheckRequired("namename");
            yield return command.CheckRequired("disp_order");
            //yield return command.CheckRequired("opt_num1");
            //yield return command.CheckRequired("opt_num2");
        }
    }
}