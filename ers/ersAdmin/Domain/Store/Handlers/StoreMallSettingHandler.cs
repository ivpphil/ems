﻿using System;
using System.Collections.Generic;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.api.shop;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Store.Handlers
{
    /// <summary>
    /// ハンドラ [Handler]
    /// </summary>
    public class StoreMallSettingHandler
        : ICommandHandler<IStoreMallSettingCommand>
    {
        /// <summary>
        /// 最終更新日時パラメータ接尾語 [Suffix for last updated datetime parameter]
        /// </summary>
        protected const string LAST_UPDATED_DATETIME_PARAM_SUFFIX = "_utime";

        /// <summary>
        /// 楽天：在庫更新最大リクエスト数 [Rakuten : Max request count for update stock quantity]
        /// </summary>
        protected const int RAKUTEN_STOCK_MAXPOSTINGSKUCOUNT = 400;


        /// <summary>
        /// サブミット [Submit]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>結果 [Result]</returns>
        public ICommandResult Submit(IStoreMallSettingCommand command)
        {
            if (command.register_btn)
            {
                // モール設定登録 [Register mall setting]
                this.RegisterMallSetting(command);
            }

            return new CommandResult(true);
        }

        #region モール設定登録 [Register mall setting]
        /// <summary>
        /// モール設定登録 [Register mall setting]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        protected void RegisterMallSetting(IStoreMallSettingCommand command)
        {
            var shopKbn = this.GetMallShopKbn(command.site_id.Value);
            var keyPrefix = shopKbn.ToString().ToLower();

            var repository = ErsMallFactory.ersSiteFactory.GetErsMallSetupRepository();
            var criteria = ErsMallFactory.ersSiteFactory.GetErsMallSetupCriteria();

            criteria.site_id = command.site_id;

            // 更新 [Update]
            if (repository.GetRecordCount(criteria) > 0)
            {
                criteria.key = keyPrefix + "_shop_id";

                var objMallSetupShopId = repository.FindSingle(criteria);

                // 店舗情報更新パラメータ取得 [Get the parameter for update shop information]
                var param = this.GetUpdateShopInfoParam(command, shopKbn, Convert.ToInt32(objMallSetupShopId.value));

                // 店舗情報更新 [Update shop information]
                var shopId = ErsMallFactory.ersMallShopFactory.GetUpdateShopInfoStgy().UpdateShopInfo(param);

                // 設定パラメータ取得 [Get setting parameter]
                var dicParam = this.GetSettingParameter(command, keyPrefix);

                var properties = command.GetPropertiesAsDictionary();

                foreach (var data in dicParam)
                {
                    criteria.Clear();
                    criteria.site_id = command.site_id;
                    criteria.key = data.Key;

                    var objOld = repository.FindSingle(criteria);
                    var objNew = ErsMallFactory.ersSiteFactory.GetErsMallSetupWithParameters(objOld.GetPropertiesAsDictionary());

                    objNew.value = Convert.ToString(data.Value);

                    repository.Update(objOld, objNew);


                    var keyUtime = data.Key + LAST_UPDATED_DATETIME_PARAM_SUFFIX;

                    // パラメータ最終更新日時更新 [Update last updated datetime for parameter]
                    if (properties.ContainsKey(keyUtime))
                    {
                        if (!ErsCommon.CompareValue(objOld.value, objNew.value))
                        {
                            this.UpdateLastUpdatedDatetimeParam(command.site_id, keyUtime);
                        }
                    }
                }
            }
            // 登録 [Insert]
            else
            {
                throw new NotSupportedException("Not supported add shop info.");

                //// 店舗情報追加パラメータ取得 [Get the parameter for add shop information]
                //var param = this.GetAddShopInfoParam(command, shopKbn);

                //// 店舗情報追加 [Add shop information]
                //var shopId = ErsMallFactory.ersMallShopFactory.GetAddShopInfoStgy().AddShopInfo(param);

                //// 設定パラメータ取得 [Get setting parameter]
                //var dicParam = this.GetSettingParameter(command, keyPrefix);

                //// 登録用設定パラメータ取得 [Get setting parameter for Insert]
                //dicParam = this.GetSettingParameterForInsert(dicParam, keyPrefix, shopId);

                //var objMallSetup = ErsMallFactory.ersSiteFactory.GetErsMallSetup();

                //objMallSetup.site_id = command.site_id;

                //foreach (var data in dicParam)
                //{
                //    objMallSetup.key = data.Key;
                //    objMallSetup.value = Convert.ToString(data.Value);

                //    repository.Insert(objMallSetup);
                //}
            }
        }

        /// <summary>
        /// パラメータ最終更新日時更新 [Update last updated datetime for parameter]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="keyUtime">キー [Key]</param>
        protected void UpdateLastUpdatedDatetimeParam(int? siteId, string keyUtime)
        {
            var repository = ErsMallFactory.ersSiteFactory.GetErsMallSetupRepository();
            var criteria = ErsMallFactory.ersSiteFactory.GetErsMallSetupCriteria();

            criteria.Clear();
            criteria.site_id = siteId;
            criteria.key = keyUtime;

            var objOldUtime = repository.FindSingle(criteria);
            var objNewUtime = ErsMallFactory.ersSiteFactory.GetErsMallSetupWithParameters(objOldUtime.GetPropertiesAsDictionary());

            objNewUtime.value = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

            repository.Update(objOldUtime, objNewUtime);
        }
        #endregion

        #region モール店舗区分取得 [Get mall shop kbn]
        /// <summary>
        /// モール店舗区分取得 [Get mall shop kbn]
        /// </summary>
        /// <param name="site_id">サイトID [Site id]</param>
        /// <returns>モール店舗区分 [Mall shop type]</returns>
        protected EnumMallShopKbn GetMallShopKbn(int site_id)
        {
            var objSite = ErsMallFactory.ersSiteFactory.GetErsSiteByID(site_id);
            return objSite.mall_shop_kbn.Value;
        }
        #endregion

        #region 店舗情報追加パラメータ取得 [Get the parameter for add shop information]
        /// <summary>
        /// 店舗情報追加パラメータ取得 [Get the parameter for add shop information]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <param name="shopKbn">モール店舗区分 [Mall shop type]</param>
        /// <returns>店舗情報追加パラメータ [The parameter for add shop information]</returns>
        protected AddShopInfoParam GetAddShopInfoParam(IStoreMallSettingCommand command, EnumMallShopKbn shopKbn)
        {
            var param = default(AddShopInfoParam);

            switch (shopKbn)
            {
                case EnumMallShopKbn.RAKUTEN:
                    param.shopName = command.rakuten_shop_name;
                    param.shopType = EnumMallHarcShopType.RAKUTEN;

                    param.basicParam = this.GetRakutenBasicPostingParamDic(command);
                    param.stockPostingParam = this.GetRakutenStockPostingParamDic(command);
                    param.orderScrapingParam = this.GetRakutenOrderPostingParamDic(command);
                    break;

                case EnumMallShopKbn.YAHOO:
                    param.shopName = command.yahoo_shop_name;
                    param.shopType = EnumMallHarcShopType.YAHOO;

                    param.basicParam = this.GetYahooBasicPostingParamDic(command);
                    param.stockPostingParam = this.GetYahooStockPostingParamDic(command);
                    param.productPostingParam = this.GetYahooProductPostingParamDic(command);
                    param.orderScrapingParam = this.GetYahooOrderPostingParamDic(command);
                    break;

                case EnumMallShopKbn.AMAZON:
                    param.shopName = command.amazon_shop_name;
                    param.shopType = EnumMallHarcShopType.AMAZON;

                    param.stockPostingParam = this.GetAmazonStockPostingParamDic(command);
                    param.orderScrapingParam = this.GetAmazonOrderPostingParamDic(command);
                    break;
            }

            return param;
        }
        #endregion

        #region 店舗情報更新パラメータ取得 [Get the parameter for update shop information]
        /// <summary>
        /// 店舗情報更新パラメータ取得 [Get the parameter for update shop information]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <param name="shopKbn">モール店舗区分 [Mall shop type]</param>
        /// <returns>店舗情報追加パラメータ [The parameter for add shop information]</returns>
        protected UpdateShopInfoParam GetUpdateShopInfoParam(IStoreMallSettingCommand command, EnumMallShopKbn shopKbn, int? shopId)
        {
            var param = default(UpdateShopInfoParam);

            param.shopId = shopId;

            switch (shopKbn)
            {
                case EnumMallShopKbn.RAKUTEN:
                    param.shopName = command.rakuten_shop_name;

                    param.basicParam = this.GetRakutenBasicPostingParamDic(command);
                    param.stockPostingParam = this.GetRakutenStockPostingParamDic(command);
                    param.orderScrapingParam = this.GetRakutenOrderPostingParamDic(command);
                    break;

                case EnumMallShopKbn.YAHOO:
                    param.shopName = command.yahoo_shop_name;

                    param.basicParam = this.GetYahooBasicPostingParamDic(command);
                    param.stockPostingParam = this.GetYahooStockPostingParamDic(command);
                    param.productPostingParam = this.GetYahooProductPostingParamDic(command);
                    param.orderScrapingParam = this.GetYahooOrderPostingParamDic(command);
                    break;

                case EnumMallShopKbn.AMAZON:
                    param.shopName = command.amazon_shop_name;

                    param.stockPostingParam = this.GetAmazonStockPostingParamDic(command);
                    param.orderScrapingParam = this.GetAmazonOrderPostingParamDic(command);
                    break;
            }

            return param;
        }
        #endregion

        #region 楽天パラメータ取得 [Get parameter for Rakuten]
        /// <summary>
        /// 楽天基本パラメータ取得 [Get basic parameter for Rakuten]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>パラメータ [Parameter]</returns>
        protected IDictionary<string, object> GetRakutenBasicPostingParamDic(IStoreMallSettingCommand command)
        {
            var dicResult = new Dictionary<string, object>();

            dicResult["shopName"] = command.rakuten_mall_shop_name;

            return dicResult;
        }

        /// <summary>
        /// 楽天在庫パラメータ取得 [Get stock parameter for Rakuten]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>パラメータ [Parameter]</returns>
        protected IDictionary<string, object> GetRakutenStockPostingParamDic(IStoreMallSettingCommand command)
        {
            var dicResult = new Dictionary<string, object>();

            dicResult["ftpHost"] = command.rakuten_ftp_host;
            dicResult["ftpUser"] = command.rakuten_ftp_user;
            dicResult["ftpPassword"] = command.rakuten_ftp_pass;
            dicResult["useWebApi"] = command.rakuten_use_webapi.ToString();
            dicResult["webApiUserName"] = command.rakuten_webapi_user;
            dicResult["webApiShopUrl"] = command.rakuten_webapi_url;
            dicResult["webApiAuthKey"] = command.rakuten_webapi_auth_key;
            dicResult["maxPostingSkuCount"] = RAKUTEN_STOCK_MAXPOSTINGSKUCOUNT;

            return dicResult;
        }

        /// <summary>
        /// 楽天受注パラメータ取得 [Get order parameter for Rakuten]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>パラメータ [Parameter]</returns>
        protected IDictionary<string, object> GetRakutenOrderPostingParamDic(IStoreMallSettingCommand command)
        {
            var dicResult = new Dictionary<string, object>();

            dicResult["rlogin_account"] = command.rakuten_rlogin_user;
            dicResult["rlogin_password"] = command.rakuten_rlogin_pass;
            dicResult["account"] = command.rakuten_account_user;
            dicResult["password"] = command.rakuten_account_pass;

            return dicResult;
        }
        #endregion

        #region Yahoo!パラメータ取得 [Get parameter for Yahoo!]
        /// <summary>
        /// Yahoo!基本パラメータ取得 [Get basic parameter for Yahoo!]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>パラメータ [Parameter]</returns>
        protected IDictionary<string, object> GetYahooBasicPostingParamDic(IStoreMallSettingCommand command)
        {
            var dicResult = new Dictionary<string, object>();

            dicResult["shopName"] = command.yahoo_mall_shop_name;

            return dicResult;
        }

        /// <summary>
        /// Yahoo!在庫パラメータ取得 [Get stock parameter for Yahoo!]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>パラメータ [Parameter]</returns>
        protected IDictionary<string, object> GetYahooStockPostingParamDic(IStoreMallSettingCommand command)
        {
            var dicResult = new Dictionary<string, object>();

            dicResult["account"] = command.yahoo_api_account;

            return dicResult;
        }

        /// <summary>
        /// Yahoo!商品パラメータ取得 [Get product parameter for Yahoo!]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>パラメータ [Parameter]</returns>
        protected IDictionary<string, object> GetYahooProductPostingParamDic(IStoreMallSettingCommand command)
        {
            var dicResult = new Dictionary<string, object>();

            dicResult["bussiness_account"] = command.yahoo_business_user;
            dicResult["bussiness_password"] = command.yahoo_business_pass;
            dicResult["account"] = command.yahoo_account_user;
            dicResult["password"] = command.yahoo_account_pass;
            dicResult["store_account_name"] = command.yahoo_mall_shop_name;

            return dicResult;
        }

        /// <summary>
        /// Yahoo!受注パラメータ取得 [Get order parameter for Yahoo!]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>パラメータ [Parameter]</returns>
        protected IDictionary<string, object> GetYahooOrderPostingParamDic(IStoreMallSettingCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var dicResult = new Dictionary<string, object>();

            dicResult["bussiness_account"] = command.yahoo_business_user;
            dicResult["bussiness_password"] = command.yahoo_business_pass;
            dicResult["account"] = command.yahoo_account_user;
            dicResult["password"] = command.yahoo_account_pass;
            dicResult["store_account_name"] = command.yahoo_mall_shop_name;
            dicResult["existsPaymentData"] = command.yahoo_exists_payment_data.ToString();

            return dicResult;
        }
        #endregion

        #region Amazonパラメータ取得 [Get parameter for Amazon]
        /// <summary>
        /// Amazon在庫パラメータ取得 [Get stock parameter for Amazon]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>パラメータ [Parameter]</returns>
        protected IDictionary<string, object> GetAmazonStockPostingParamDic(IStoreMallSettingCommand command)
        {
            var dicResult = new Dictionary<string, object>();

            dicResult["account"] = command.amazon_merchant_id;

            return dicResult;
        }

        /// <summary>
        /// Amazon受注パラメータ取得 [Get order parameter for Amazon]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>パラメータ [Parameter]</returns>
        protected IDictionary<string, object> GetAmazonOrderPostingParamDic(IStoreMallSettingCommand command)
        {
            var dicResult = new Dictionary<string, object>();

            dicResult["merchantId"] = command.amazon_merchant_id;
            dicResult["marketplaceId"] = command.amazon_marketplace_id;

            return dicResult;
        }
        #endregion

        #region 設定パラメータ取得 [Get setting parameter]
        /// <summary>
        /// 設定パラメータ取得 [Get setting parameter]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <param name="keyPrefix">キー接頭語 [Key prefix]</param>
        /// <returns>設定パラメータ [Setting parameter]</returns>
        protected IDictionary<string, object> GetSettingParameter(IStoreMallSettingCommand command, string keyPrefix)
        {
            var dicRet = new Dictionary<string, object>();

            var properties = command.GetPropertiesAsDictionary();

            foreach (var p in properties)
            {
                if (p.Key.StartsWith(keyPrefix + "_") && !p.Key.EndsWith(LAST_UPDATED_DATETIME_PARAM_SUFFIX))
                {
                    dicRet[p.Key] = properties[p.Key];
                }
            }

            return dicRet;
        }
        #endregion

        #region 登録用設定パラメータ取得 [Get setting parameter for Insert]
        /// <summary>
        /// 登録用設定パラメータ取得 [Get setting parameter for Insert]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <param name="keyPrefix">キー接頭語 [Key prefix]</param>
        /// <param name="shopId">店舗ID [Shop ID]</param>
        /// <returns>設定パラメータ [Setting parameter]</returns>
        protected IDictionary<string, object> GetSettingParameterForInsert(IDictionary<string, object> dicParam, string keyPrefix, int? shopId)
        {
            dicParam[keyPrefix + "_shop_id"] = shopId;

            return dicParam;
        }
        #endregion
    }
}