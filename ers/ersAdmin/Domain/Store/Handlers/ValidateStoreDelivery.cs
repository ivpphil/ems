﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreDelivery : IValidationHandler<IStoreDeliveryCommand>
    {
        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(IStoreDeliveryCommand command)
        {
            if (command.store_delivery_btn)
            {
                if (command.multiple_sites)
                {
                    yield return command.CheckRequired("site_id");
                }

                yield return command.CheckRequired("delivery");
                yield return command.CheckRequired("url");
                yield return command.CheckRequired("mail_url");
                yield return command.CheckRequired("mail_delivery");
            }
        }
    }
}