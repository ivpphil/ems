﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class StoreCarriageHandler : ICommandHandler<IStoreCarriageCommand>
    {
        public ICommandResult Submit(IStoreCarriageCommand command)
        {
            if(!command.multiple_sites)
            {
                command.site_id = command.config_site_id;
            }

            this.UpdateCarriageData(command);

            return new CommandResult(true);
        }

        internal void UpdateCarriageData(IStoreCarriageCommand command)
        {
            var repository = ErsFactory.ersCommonFactory.GetErsPrefRepository();

            foreach (var record in command.allList)
            {
                var updateObj = ErsFactory.ersCommonFactory.GetErsPrefWithIdAndSiteId(record.id, Convert.ToInt32(command.site_id));
                updateObj.carriage = record.carriage;
                updateObj.utime = DateTime.Now;
                updateObj.site_id = Convert.ToInt32(command.site_id);
                var old_updateObj = ErsFactory.ersCommonFactory.GetErsPrefWithIdAndSiteId(record.id, Convert.ToInt32(command.site_id));
                repository.Update(old_updateObj, updateObj);
            }

            ErsSetupRepository setupRepository = ErsFactory.ersUtilityFactory.GetErsSetupRepository();
            ErsSetup oldSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(command.site_id));
            ErsSetup newSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(command.site_id));
            newSetup.free = command.free;
            setupRepository.Update(oldSetup, newSetup);
        }
    }
}