﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Models;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreTax:IValidationHandler<IStoreTaxCommand>
    {
        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(IStoreTaxCommand command)
        {
            if (command.store_tax_btn)
            {
                yield return command.CheckRequired("tax");

                yield return command.CheckRequired("enable_carriage_tax");

                if (command.store_payment_table != null)
                {
                    foreach (Store_tax_payment_table model in command.store_payment_table)
                    {
                        model.AddInvalidField(command.controller.commandBus.Validate<IStoreTaxPaymentListRecordCommand>(model));

                        if (!model.IsValid)
                        {
                            foreach (var errorMessage in model.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "store_payment_table" });
                            }
                        }
                    }
                }
            }


        }
    }
}