﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateRoleRegist : IValidationHandler<IRoleRegistCommand>
    {
        public IEnumerable<ValidationResult> Validate(IRoleRegistCommand command)
        {
            if (command.submit_regist)
            {
                yield return command.CheckRequired("role_gname");
                if (command.role_action == null)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10206", new[] { ErsResources.GetFieldName("role_group_t.role_action") }), new[] { "role_action" });
                }
                else
                {
                    foreach (var action in command.role_action)
                    {
                        yield return ErsFactory.ersAdministratorFactory.GetCheckRoleActionStgy().Check(action);
                    }
                }

                //重複チェック
                if (command.role_gname != null)
                {
                    if (!ErsFactory.ersAdministratorFactory.GetCheckDuplicateRoleGnameStgy().CheckDuplicate(command.role_gname, 0))
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("10103",
                            new[] { ErsResources.GetFieldName("role_group_t.role_gname"), command.role_gname }), new[] { "role_gname" });
                    }
                }
            }
        }
    }
}