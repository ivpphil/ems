﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreCarriage : IValidationHandler<IStoreCarriageCommand>
    {
        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(IStoreCarriageCommand command)
        {
            if (command.store_carriage_btn)
            {
                if (command.multiple_sites)
                {
                    yield return command.CheckRequired("site_id");
                }
                if (command.allList != null)
                {
                    foreach (var model in command.allList)
                    {
                        model.AddInvalidField(command.controller.commandBus.Validate<IStoreCarriageRecordCommand>(model));
                        if (!model.IsValid)
                        {
                            foreach (var errorMessage in model.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "allList" });
                            }
                        }
                    }
                }
            }
        }
    }
}