﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class StoreMallMailTextHandler : ICommandHandler<IStoreMallMailTextCommand>
    {
        public ICommandResult Submit(IStoreMallMailTextCommand command)
        {
            this.InsertTemplateData(command);
            return new CommandResult(true);
        }
        internal void InsertTemplateData(IStoreMallMailTextCommand command)
        {
            Dictionary<string, object> tmpDic = command.GetPropertiesAsDictionary();
            foreach (var key in GetKeyList())
            {
                this.RegistValue(tmpDic, key, command.site_id);
            }

        }

        private List<string> GetKeyList()
        {
            var list = new List<string>();
            //Mall
            list.Add("register_pc_title");
            list.Add("register_pc_body");
            list.Add("delivery_pc_title");
            list.Add("delivery_pc_body");
            list.Add("individual_pc_title");
            list.Add("individual_pc_body");
            return list;
        }

        private void RegistValue(Dictionary<string, object> tmpDic, string key, int? site_id)
        {
            var templateRepository = ErsFactory.ersAdministratorFactory.GetErsMailTemplateRepository();
            var templateCriteria = ErsFactory.ersAdministratorFactory.GetErsMailTemplateCriteria();
            templateCriteria.key = key;
            templateCriteria.site_id = site_id;
            ErsMailTemplate template;
            var templateList = templateRepository.Find(templateCriteria);
            if (templateList.Count == 0)
            {
                template = ErsFactory.ersAdministratorFactory.GetErsMailTemplate();
                template.key = key;
                template.value = Convert.ToString(tmpDic[key]);
                template.site_id = site_id;
                templateRepository.Insert(template);
            }
            else
            {
                var oldTemplate = templateRepository.Find(templateCriteria)[0];

                template = templateList[0];
                template.value = Convert.ToString(tmpDic[key]);

                templateRepository.Update(oldTemplate, template);
            }
        }
    }
}