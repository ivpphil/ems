﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreMailText:IValidationHandler<IStoreMailTextCommand>
    {
        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(IStoreMailTextCommand command)
        {
            if (command.store_mail_template_btn)
            {
                if(command.multiple_sites)
                {
                    yield return command.CheckRequired("site_id");
                }
                
                foreach (var key in this.GetKeyList())
                {
                    yield return command.CheckRequired(key);
                }
            }
        }

        private List<string> GetKeyList()
        {
            var list = new List<string>();
            list.Add("entry_pc_title");
            list.Add("entry_pc_body");
            list.Add("entry_mobile_title");
            list.Add("entry_mobile_body");
            list.Add("register_pc_title");
            list.Add("register_pc_body");
            list.Add("register_mobile_title");
            list.Add("register_mobile_body");
            list.Add("delivery_pc_title");
            list.Add("delivery_pc_body");
            list.Add("delivery_mobile_title");
            list.Add("delivery_mobile_body");
            list.Add("quit_pc_title");
            list.Add("quit_pc_body");
            list.Add("quit_mobile_title");
            list.Add("quit_mobile_body");
            list.Add("passrim_pc_title");
            list.Add("passrim_pc_body");
            list.Add("passrim_mobile_title");
            list.Add("passrim_mobile_body");
            list.Add("individual_pc_title");
            list.Add("individual_pc_body");
            list.Add("quest_pc_title");
            list.Add("quest_pc_body");
            list.Add("quest_accept_pc_title");
            list.Add("quest_accept_pc_body");
            list.Add("quest_accept_mobile_title");
            list.Add("quest_accept_mobile_body");
            list.Add("regular_order_pc_title");
            list.Add("regular_order_pc_body");
            list.Add("regular_order_mobile_title");
            list.Add("regular_order_mobile_body");
            return list;
        }
    }
}