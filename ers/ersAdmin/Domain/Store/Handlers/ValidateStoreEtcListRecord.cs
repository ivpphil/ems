﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreEtcListRecord : IValidationHandler<IStoreEtcListRecordCommand>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty(IStoreEtcListRecordCommand command)
        {
            if (command.id == null
                && command.pay == null
                && command.more == null
                && command.down == null
                && command.price == null)
                return true;

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lineName"></param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> Validate(IStoreEtcListRecordCommand command)
        {
            yield return command.CheckRequired("id");
            yield return command.CheckRequired("pay");
            yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().Validate("pay", command.pay, false, false);

            if (!this.IsEmpty(command))
            {
                yield return command.CheckRequired("more");
                yield return command.CheckRequired("down");
                yield return command.CheckRequired("price");
            }

            if (command.down != null && command.more != null)
            {
                if ((command.more != 0 || command.down != 0) && command.more >= command.down)
                    yield return new ValidationResult(ErsResources.GetMessage("10023",
                        ErsResources.GetFieldName("down"),
                        ErsResources.GetFieldName("more")),
                        new[] { "down", "more" });
            }
        }
    }
}