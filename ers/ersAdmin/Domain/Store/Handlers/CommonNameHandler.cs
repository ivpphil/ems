﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store;
using ersAdmin.Models.store;
using jp.co.ivp.ers;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.common;

namespace ersAdmin.Domain.Store.Handlers
{
    public class CommonNameHandler : ICommandHandler<ICommonNameCommand>
    {
        ErsCommonNameCodeRepository repository = new ErsCommonNameCodeRepository();

        public ICommandResult Submit(ICommonNameCommand command)
        {
            this.CommandSubmit(command);
            return new CommandResult(true);
        }

        private void CommandSubmit(ICommonNameCommand command)
        {
            foreach (common_name_detail item in command.common_name_table)
            {
                if (!IsEmpty(item))
                {
                    if (!item.delete)
                    {
                        var criteria = ErsFactory.ersCommonFactory.GetErsCommonNameCodeCriteria();
                        criteria.id = item.id;
                        long recordCount = repository.GetRecordCount(criteria);
                        if (recordCount > 0)
                            this.Update(item);
                        else
                            this.Insert(item);
                    }
                    else
                        this.Delete(item);
                }
            }
        }

        private void Insert(common_name_detail item)
        {
            var entity = ErsFactory.ersCommonFactory.GetErsCommonNameCode();
            entity.OverwriteWithModel(item);
            //entity.active = (item.active == null) ? EnumActive.NonActive : item.active;
            if (entity.opt_flg1 != 0)
            {
                entity.opt_flg1 = 1;
            }
            if (entity.opt_flg2 != 0)
            {
                entity.opt_flg2 = 1;
            }
            entity.active = EnumActive.Active;
            repository.Insert(entity);
        }

        private void Update(common_name_detail item)
        {
            var old_entity = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithId(item.id);
            var new_entity = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithParameter(item.GetPropertiesAsDictionary());
            new_entity.intime = old_entity.intime;
            new_entity.utime = DateTime.Now;
            new_entity.type_code = old_entity.type_code;
            new_entity.code = old_entity.code;
            new_entity.opt_chr1 = item.opt_chr1;
            new_entity.opt_chr2 = item.opt_chr2;
            if (item.opt_flg1 != 0)
            {
                item.opt_flg1 = 1;
            }
            if (item.opt_flg2 != 0)
            {
                item.opt_flg2 = 1;
            }
            new_entity.opt_flg1 = item.opt_flg1;
            new_entity.opt_flg2 = item.opt_flg2;
            if (item.opt_num1 != null)
            {
                new_entity.opt_num1 = item.opt_num1;
            }
            else
            {
                new_entity.opt_num1 = 0;
            }
            if (item.opt_num2 != null)
            {
                new_entity.opt_num2 = item.opt_num2;
            }
            else
            {
                new_entity.opt_num2 = 0;
            }
            new_entity.disp_order = item.disp_order;
            //new_entity.active = (item.active == null) ? EnumActive.NonActive : item.active;
            new_entity.active = old_entity.active;
            repository.Update(old_entity, new_entity);
        }

        private void Delete(common_name_detail item)
        {
            var entity = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithId(item.id);
            repository.Delete(entity);
        }

        public bool IsEmpty(common_name_detail item)
        {
            if (item.id == null && item.namename == null)
                return true;

            return false;
        }

    }
}