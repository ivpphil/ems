﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class StoreTaxHandler : ICommandHandler<IStoreTaxCommand>
    {
        public ICommandResult Submit(IStoreTaxCommand command)
        {
            this.InsertTaxData(command);
            return new CommandResult(true);
        }

        internal void InsertTaxData(IStoreTaxCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var sites = setup.ersSiteId;

            foreach (string site in sites)
            {

                ErsSetupRepository repository = ErsFactory.ersUtilityFactory.GetErsSetupRepository();
                ErsSetup oldSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(int.Parse(site));
                ErsSetup newSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(int.Parse(site));
                newSetup.tax = command.tax;
                newSetup.enable_carriage_tax = command.enable_carriage_tax;
                repository.Update(oldSetup, newSetup);

                var payRepository = ErsFactory.ersOrderFactory.GetErsPayRepository();
                foreach (var store_payment in command.store_payment_table)
                {
                    if (store_payment.id != null)
                    {
                        var pay_repository = ErsFactory.ersOrderFactory.GetErsPayRepository();
                        var pay_criteria = ErsFactory.ersOrderFactory.GetErsPayCriteria();
                        pay_criteria.id = store_payment.id;
                        var list = pay_repository.Find(pay_criteria);

                        foreach (var pay in list)
                        {
                            pay.enable_tax = store_payment.enable_tax;
                            var old_pay = ErsFactory.ersOrderFactory.GetErsPayWithIdAndSiteId(store_payment.id.Value, pay.site_id);
                            payRepository.Update(old_pay, pay);
                        }
                    }
                }

            }

        }
    }
}