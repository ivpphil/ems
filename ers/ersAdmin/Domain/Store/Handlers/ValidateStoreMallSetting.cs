﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreMallSetting
        : IValidationHandler<IStoreMallSettingCommand>
    {
        /// <summary>
        /// バリデーション [Validation]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>結果 [Result]</returns>
        public IEnumerable<ValidationResult> Validate(IStoreMallSettingCommand command)
        {
            if (command.register_btn)
            {
                if (command.site_id.HasValue && command.IsValidField("site_id"))
                {
                    // モール店舗区分取得 [Get mall shop kbn]
                    var shopKbn = this.GetMallShopKbn(command.site_id.Value);

                    switch (shopKbn)
                    {
                        case EnumMallShopKbn.RAKUTEN:
                            yield return command.CheckRequired("rakuten_shop_name");
                            yield return command.CheckRequired("rakuten_mall_shop_name");
                            yield return command.CheckRequired("rakuten_mall_shop_url");
                            yield return command.CheckRequired("rakuten_ftp_host");
                            yield return command.CheckRequired("rakuten_ftp_user");
                            yield return command.CheckRequired("rakuten_ftp_pass");
                            yield return command.CheckRequired("rakuten_webapi_user");
                            yield return command.CheckRequired("rakuten_webapi_url");
                            yield return command.CheckRequired("rakuten_webapi_auth_key");
                            yield return command.CheckRequired("rakuten_rlogin_user");
                            yield return command.CheckRequired("rakuten_rlogin_pass");
                            yield return command.CheckRequired("rakuten_account_user");
                            yield return command.CheckRequired("rakuten_account_pass");
                            yield return command.CheckRequired("rakuten_mail_from");
                            yield return command.CheckRequired("rakuten_mail_host");
                            yield return command.CheckRequired("rakuten_mail_port");
                            yield return command.CheckRequired("rakuten_mail_auth_id");
                            yield return command.CheckRequired("rakuten_mail_auth_pass");
                            break;

                        case EnumMallShopKbn.YAHOO:
                            yield return command.CheckRequired("yahoo_shop_name");
                            yield return command.CheckRequired("yahoo_mall_shop_name");
                            yield return command.CheckRequired("yahoo_ftp_host");
                            yield return command.CheckRequired("yahoo_ftp_user");
                            yield return command.CheckRequired("yahoo_ftp_pass");
                            yield return command.CheckRequired("yahoo_api_account");
                            yield return command.CheckRequired("yahoo_business_user");
                            yield return command.CheckRequired("yahoo_business_pass");
                            yield return command.CheckRequired("yahoo_account_user");
                            yield return command.CheckRequired("yahoo_account_pass");
                            yield return command.CheckRequired("yahoo_mail_from");
                            break;

                        case EnumMallShopKbn.AMAZON:
                            yield return command.CheckRequired("amazon_shop_name");
                            yield return command.CheckRequired("amazon_merchant_id");
                            yield return command.CheckRequired("amazon_marketplace_id");
                            yield return command.CheckRequired("amazon_accesskey_id");
                            yield return command.CheckRequired("amazon_secret_accesskey");
                            yield return command.CheckRequired("amazon_mail_from");
                            break;
                    }
                }
            }

            yield return null;
        }

        #region モール店舗区分取得 [Get mall shop kbn]
        /// <summary>
        /// モール店舗区分取得 [Get mall shop kbn]
        /// </summary>
        /// <param name="site_id">サイトID [Site id]</param>
        /// <returns>モール店舗区分 [Mall shop type]</returns>
        protected EnumMallShopKbn GetMallShopKbn(int site_id)
        {
            var objSite = ErsMallFactory.ersSiteFactory.GetErsSiteByID(site_id);
            return objSite.mall_shop_kbn.Value;
        }
        #endregion
    }
}