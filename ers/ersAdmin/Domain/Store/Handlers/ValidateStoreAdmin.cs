﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreAdmin
        : IValidationHandler<IStoreAdminCommand>
    {
        public IEnumerable<ValidationResult> Validate(IStoreAdminCommand command)
        {
            if (command.store_admin_btn)
            {
                if(command.multiple_sites)
                {
                    yield return command.CheckRequired("site_id");
                }
                
                yield return command.CheckRequired("replying_mail_addr");
                yield return command.CheckRequired("report_mail_addr1");
                yield return command.CheckRequired("quest_email_to");

                var controller = command.controller;
                foreach (var model in command.adminList)
                {
                    if (this.IsEmpty(model))
                    {
                        continue;
                    }

                    model.AddInvalidField(controller.commandBus.Validate<IStoreAdminListRecordCommand>(model));

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "adminList" });
                        }
                    }
                }
            }
        }

        public bool IsEmpty(IStoreAdminListRecordCommand command)
        {
            if (command.id == null
                && command.user_login_id == null
                && command.user_name == null
                && command.passwd == null
                && command.role_gcode == null
                && command.agent_id == null
              )
                return true;

            return false;
        }
    }
}