﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class StoreDeliveryHandler:ICommandHandler<IStoreDeliveryCommand>
    {
        public ICommandResult Submit(IStoreDeliveryCommand command)
        {
            if (!command.multiple_sites)
            {
                command.site_id = command.config_site_id;
            }

            this.InsertDeliveryData(command);
            return new CommandResult(true);
        }
        internal void InsertDeliveryData(IStoreDeliveryCommand command)
        {
            ErsSetupRepository repository = ErsFactory.ersUtilityFactory.GetErsSetupRepository();
            ErsSetup oldSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(command.site_id));
            ErsSetup newSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(Convert.ToInt32(command.site_id));
            newSetup.delivery = command.delivery;
            newSetup.url = command.url;
                newSetup.mail_delivery = command.mail_delivery;
                newSetup.mail_url = command.mail_url;
            repository.Update(oldSetup, newSetup);
        }
    }
}