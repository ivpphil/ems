﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.Send;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Handlers
{
    public class StoreDeliveryDayHandler : ICommandHandler<IStoreDeliveryDayCommand>
    {
        public ICommandResult Submit(IStoreDeliveryDayCommand command)
        {
            this.InsertAndUpdateData(command);
            return new CommandResult(true);
        }

        internal void InsertAndUpdateData(IStoreDeliveryDayCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var sites = setup.ersSiteId;

            foreach (string site in sites)
            {
                ErsSetupRepository setupRepository = ErsFactory.ersUtilityFactory.GetErsSetupRepository();
                ErsSetup oldSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(int.Parse(site));
                ErsSetup newSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(int.Parse(site));
                newSetup.sendday = command.sendday;
                newSetup.sendday_count = command.sendday_count;
                newSetup.shipping_csv_output_min_days = command.shipping_csv_output_min_days;
                newSetup.create_regular_order_days = command.create_regular_order_days;
                setupRepository.Update(oldSetup, newSetup);
            }


            var sendtimeRepository = ErsFactory.ersOrderFactory.GetErsSendTimeRepository();
            foreach (var key in command.sendtime.Keys)
            {
                var objSendtime = ErsFactory.ersOrderFactory.GetErsSendTimeWithId(key);
                if (objSendtime != null)
                {
                    ErsSendTime oldSendTime = objSendtime;
                    ErsSendTime newSendTime = ErsFactory.ersOrderFactory.GetErsSendTimeWithId(key);
                    newSendTime.sendtime = command.sendtime[key];
                    sendtimeRepository.Update(oldSendTime, newSendTime);
                }
            }

            if (!string.IsNullOrEmpty(command.add_sendtime))
            {
                var objSendtime = ErsFactory.ersOrderFactory.GetErsSendTime();
                objSendtime.sendtime = command.add_sendtime;
                sendtimeRepository.Insert(objSendtime);
            }
        }
    }
}