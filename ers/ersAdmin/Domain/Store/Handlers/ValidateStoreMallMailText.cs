﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreMallMailText:IValidationHandler<IStoreMallMailTextCommand>
    {
        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(IStoreMallMailTextCommand command)
        {
            if (command.store_mail_template_btn)
            {
                yield return command.CheckRequired("site_id");

                foreach (var key in this.GetKeyList())
                {
                    yield return command.CheckRequired(key);
                }
            }
        }

        private List<string> GetKeyList()
        {
            var list = new List<string>();
            //Mall
            list.Add("register_pc_title");
            list.Add("register_pc_body");
            list.Add("delivery_pc_title");
            list.Add("delivery_pc_body");
            list.Add("individual_pc_title");
            list.Add("individual_pc_body");
            return list;
        }
    }
}