﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Store.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using ersAdmin.Models.store;

namespace ersAdmin.Domain.Store.Handlers
{
    public class ValidateStoreTaxPaymentListRecord : IValidationHandler<IStoreTaxPaymentListRecordCommand>
    {

        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> Validate(IStoreTaxPaymentListRecordCommand command)
        {
            yield return command.CheckRequired("id");
            yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().Validate("id", command.id, false, false);

            yield return command.CheckRequired("enable_tax");
            
        }

    }
}