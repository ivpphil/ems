﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.store;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStorePaymentListRecordCommand:ICommand
    {
        List<store_etc_table> store_etc_table { get; set; }

        EnumPaymentType? id { get; }

        EnumActive? active { get; set; }

        EnumOnOff? etc_flg { get; set; }
    }
}