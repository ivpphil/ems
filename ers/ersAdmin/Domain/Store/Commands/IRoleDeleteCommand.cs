﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IRoleDeleteCommand : ICommand
    {
        int id { get; set; }
        bool submit_delete { get; }
    }
}