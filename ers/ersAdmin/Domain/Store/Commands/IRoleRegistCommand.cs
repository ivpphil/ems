﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc.validation;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IRoleRegistCommand : ICommand
    {
        bool submit_regist { get; set; }
        string role_gname { get; set; } 
        string[] role_action { get; set; }

        DateTime intime { get; set; }
        DateTime? utime { get; set; }
    }
}