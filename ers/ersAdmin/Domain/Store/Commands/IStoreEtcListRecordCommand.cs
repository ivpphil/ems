﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Commands
{
    public interface  IStoreEtcListRecordCommand:ICommand
    {
        int? down { get; }

        int? more { get; }

        EnumPaymentType? pay { get; }

        int? id { get; }

        int? price { get; }
    }
}