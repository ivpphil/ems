﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Store.Commands
{
    /// <summary>
    /// コマンド [Command]
    /// </summary>
    public interface IStoreMallSettingCommand
        : ICommand
    {
        #region ボタン [Buttons]
        /// <summary>
        /// "登録する"ボタン [Register button]
        /// </summary>
        bool register_btn { get; set; }
        #endregion

        #region サイト [Site]
        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        int? site_id { get; set; }

        /// <summary>
        /// モール店舗区分 [Mall shop type]
        /// </summary>
        EnumMallShopKbn? mall_shop_kbn { get; set; }
        #endregion

        #region 楽天用 [For Rakuten]
        /// <summary>
        /// ショップ名 [Shop name]
        /// </summary>
        string rakuten_shop_name { get; set; }

        /// <summary>
        /// 楽天店舗名 [Rakuten mall shop name]
        /// </summary>
        string rakuten_mall_shop_name { get; set; }

        /// <summary>
        /// 楽天店舗URL [Rakuten mall shop url]
        /// </summary>
        string rakuten_mall_shop_url { get; set; }

        /// <summary>
        /// FTPホスト [FTP host]
        /// </summary>
        string rakuten_ftp_host { get; set; }

        /// <summary>
        /// FTPユーザ [FTP user]
        /// </summary>
        string rakuten_ftp_user { get; set; }

        /// <summary>
        /// FTPパスワード [FTP password]
        /// </summary>
        string rakuten_ftp_pass { get; set; }

        /// <summary>
        /// WebAPI利用フラグ [Use WebAPI flag]
        /// </summary>
        int rakuten_use_webapi { get; set; }

        /// <summary>
        /// WebAPIユーザID [WebAPI user id]
        /// </summary>
        string rakuten_webapi_user { get; set; }

        /// <summary>
        /// WebAPI店舗URL [WebAPI shop url]
        /// </summary>
        string rakuten_webapi_url { get; set; }

        /// <summary>
        /// WebAPI認証キー [WebAPI authorization key]
        /// </summary>
        string rakuten_webapi_auth_key { get; set; }

        /// <summary>
        /// R-Login ID [R-Login id
        /// </summary>
        string rakuten_rlogin_user { get; set; }

        /// <summary>
        /// R-Login パスワード [R-Login password]
        /// </summary>
        string rakuten_rlogin_pass { get; set; }

        /// <summary>
        /// 楽天会員ID [Rakuten account id]
        /// </summary>
        string rakuten_account_user { get; set; }

        /// <summary>
        /// 楽天会員パスワード [Rakuten account password]
        /// </summary>
        string rakuten_account_pass { get; set; }

        /// <summary>
        /// 楽天メール送信者メールアドレス [From mail address for Rakuten mail]
        /// </summary>
        string rakuten_mail_from { get; set; }

        /// <summary>
        /// 楽天メールサーバホスト [Mail server host for Rakuten mail]
        /// </summary>
        string rakuten_mail_host { get; set; }

        /// <summary>
        /// 楽天メールサーバポート [Mail server port for Rakuten mail]
        /// </summary>
        string rakuten_mail_port { get; set; }

        /// <summary>
        /// 楽天メールサーバ認証ID [Mail server id for Rakuten mail]
        /// </summary>
        string rakuten_mail_auth_id { get; set; }

        /// <summary>
        /// 楽天メールサーバ認証パスワード [Mail server password for Rakuten mail]
        /// </summary>
        string rakuten_mail_auth_pass { get; set; }
        #endregion

        #region Yahoo!用 [For Yahoo!]
        /// <summary>
        /// ショップ名 [Shop name]
        /// </summary>
        string yahoo_shop_name { get; set; }

        /// <summary>
        /// Yahoo!店舗名 [Yahoo! mall shop name]
        /// </summary>
        string yahoo_mall_shop_name { get; set; }

        /// <summary>
        /// FTPホスト [FTP host]
        /// </summary>
        string yahoo_ftp_host { get; set; }

        /// <summary>
        /// FTPユーザ [FTP user]
        /// </summary>
        string yahoo_ftp_user { get; set; }

        /// <summary>
        /// FTPパスワード [FTP password]
        /// </summary>
        string yahoo_ftp_pass { get; set; }

        /// <summary>
        /// Yahoo!APIアカウント名 [API account]
        /// </summary>
        string yahoo_api_account { get; set; }

        /// <summary>
        /// Yahoo!ビジネスID [Yahoo! business id]
        /// </summary>
        string yahoo_business_user { get; set; }

        /// <summary>
        /// Yahoo!ビジネスパスワード [Yahoo! business password]
        /// </summary>
        string yahoo_business_pass { get; set; }

        /// <summary>
        /// Yahoo!会員ID [Yahoo! account id]
        /// </summary>
        string yahoo_account_user { get; set; }

        /// <summary>
        /// Yahoo!会員パスワード [Yahoo! account password]
        /// </summary>
        string yahoo_account_pass { get; set; }

        /// <summary>
        /// Yahoo!決済データ存在フラグ [Yahoo! exists payment data]
        /// </summary>
        int yahoo_exists_payment_data { get; set; }

        /// <summary>
        /// Yahoo!メール送信者メールアドレス [From mail address for Yahoo! mail]
        /// </summary>
        string yahoo_mail_from { get; set; }
        #endregion

        #region Amazon用 [For Amazon]
        /// <summary>
        /// ショップ名 [Shop name]
        /// </summary>
        string amazon_shop_name { get; set; }

        /// <summary>
        /// 出品者ID [Merchant id]
        /// </summary>
        string amazon_merchant_id { get; set; }

        /// <summary>
        /// マーケットプレイスID [Marketplace id]
        /// </summary>
        string amazon_marketplace_id { get; set; }

        /// <summary>
        /// アクセスキーID [Access key id]
        /// </summary>
        string amazon_accesskey_id { get; set; }

        /// <summary>
        /// シークレットアクセスキー [Secret access key]
        /// </summary>
        string amazon_secret_accesskey { get; set; }

        /// <summary>
        /// Amazonメール送信者メールアドレス [From mail address for Amazon mail]
        /// </summary>
        string amazon_mail_from { get; set; }
        #endregion
    }
}