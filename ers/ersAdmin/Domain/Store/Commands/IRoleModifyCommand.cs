﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IRoleModifyCommand : ICommand
    {
        int id { get; set; }
        string role_gname { get; set; }
        string[] role_action { get; set; }
        bool submit_change { get; }        
    }
}