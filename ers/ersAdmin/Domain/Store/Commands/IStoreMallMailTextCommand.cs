﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStoreMallMailTextCommand : ICommand
    {
        bool store_mail_template_btn { get; }

        int? site_id { get; set; }
    }
}