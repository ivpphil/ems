﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.store;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStoreCarriageCommand : ISiteRegisterBaseCommand, ICommand
    {
        List<store_carriage_table> allList { get; set; }

        int? free { get; }

        bool store_carriage_btn { get; }

        EnumOnOff? enable_carriage_tax { get; set; }
    }
}