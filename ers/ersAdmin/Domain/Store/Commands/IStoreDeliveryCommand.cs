﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStoreDeliveryCommand : ISiteRegisterBaseCommand, ICommand
    {
        bool store_delivery_btn { get; }

        string delivery { get; }

        string url { get; }

        string mail_delivery { get; }

        string mail_url { get; }

    }
}