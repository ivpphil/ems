﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Commands
{
    public interface ICommonNameRecordCommand
        : ICommand
    {
        int? id { get; set; }

        EnumCommonNameType? type_code { get; set; }

        int? code { get; set; }

        string namename { get; set; }

        string opt_chr1 { get; set; }

        string opt_chr2 { get; set; }

        int opt_flg1 { get; set; }

        int opt_flg2 { get; set; }

        int? opt_num1 { get; set; }

        int? opt_num2 { get; set; }

        int? disp_order { get; set; }

        EnumActive? active { get; set; }

        DateTime? intime { get; set; }

        DateTime? utime { get; set; }

        bool delete { get; set; }

    }
}