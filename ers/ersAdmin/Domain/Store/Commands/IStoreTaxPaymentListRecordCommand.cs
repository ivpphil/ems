﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.store;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStoreTaxPaymentListRecordCommand:ICommand
    {
        EnumPaymentType? id { get; }

        EnumOnOff? enable_tax { get; set; }
    }
}