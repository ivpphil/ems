﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStoreAdminListRecordCommand
        : ICommand
    {
        int? id { get; set; }

        string user_cd { get; set; }

        string user_login_id { get; set; }

        string user_name { get; set; }

        string passwd { get; set; }

        string role_gcode { get; set; }

        int? agent_id { get; set; }
    }
}