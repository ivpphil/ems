﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStoreEtcCommand : ISiteRegisterBaseCommand, ICommand
    {
        List<Store_payment_table> store_payment_table { get; set; }

        bool store_etc_btn { get; set; }
    }
}