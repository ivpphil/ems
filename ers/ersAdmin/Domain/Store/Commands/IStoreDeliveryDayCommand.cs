﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStoreDeliveryDayCommand:ICommand
    {
        bool store_delivery_day_btn { get; }
        Dictionary<int?, string> sendtime { get; }
        string add_sendtime { get; }
        int? sendday { get; }
        int? sendday_count { get; set; }
        int? delete_id { get; }

        int shipping_csv_output_min_days { get; set; }
        int create_regular_order_days { get; set; }

    }
}