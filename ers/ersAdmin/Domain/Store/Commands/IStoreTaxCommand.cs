﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Models;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStoreTaxCommand:ICommand
    {
        int? tax { get; }
        bool store_tax_btn { get; }
        
        EnumOnOff? enable_carriage_tax { get; set; }
       
        IList<Store_tax_payment_table> store_payment_table { get; set; }
    }
}