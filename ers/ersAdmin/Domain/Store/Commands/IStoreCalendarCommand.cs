﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStoreCalendarCommand:ICommand
    {
        bool store_delivery_day_btn { get; }

        int[] checkDays { get; set; }

        int? ddlYear { get; }

        int? ddlMonth { get; }
    }
}