﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IDeleteDeliveryDayCommand:ICommand
    {
        bool store_delivery_day_btn { get; }
        Dictionary<int?, string> sendtime { get; }
        string add_sendtime { get; }
        int? sendday { get; }
        int? delete_id { get; }
    }
}