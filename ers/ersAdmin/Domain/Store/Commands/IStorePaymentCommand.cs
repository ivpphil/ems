﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models;
using ersAdmin.Models.store;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStorePaymentCommand : ISiteRegisterBaseCommand, ICommand
    {
        IList<Store_payment_table> store_payment_table { get; set; }

        IList<Store_card_table> store_card_table { get; set; }

        bool store_payment_btn { get; set; }
    }
}