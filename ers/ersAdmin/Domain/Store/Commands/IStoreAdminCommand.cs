﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStoreAdminCommand
        : ISiteRegisterBaseCommand, ICommand
    {
        List<AdminListData> adminList { get; set; }

        bool store_admin_btn { get; set; }

        string replying_mail_addr { get; set; }

        string report_mail_addr1 { get; set; }

        string report_mail_addr2 { get; set; }

        string report_mail_addr3 { get; set; }

        string quest_email_to { get; set; }
    }
}