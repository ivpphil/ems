﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models.store;

namespace ersAdmin.Domain.Store.Commands
{
    public interface ICommonNameCommand : ICommand
    {
        List<common_name_detail> common_name_table { get; set; }
    }
}