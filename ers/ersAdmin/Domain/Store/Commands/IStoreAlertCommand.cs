﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Models;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStoreAlertCommand : ISiteRegisterBaseCommand, ICommand
    {
        List<Store_payment_table> store_payment_table { get; set; }

        bool store_alert_btn { get;  }

        string pri_memo { get; }

        string regi_memo { get; }

        string error_message { get; }

        string m_error_message { get; }

        string wh_owner_description { get; }
    }
}