﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Store.Commands
{
    public interface IStoreCardListRecordCommand:ICommand
    {
        EnumActive? active { get; set; }
    }
}