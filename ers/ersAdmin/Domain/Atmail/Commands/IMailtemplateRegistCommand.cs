﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.atmail;

namespace ersAdmin.Domain.Atmail.Commands
{
    public interface IMailtemplateRegistCommand
        : ICommand
    {
        bool modify { get; } 
    }
}