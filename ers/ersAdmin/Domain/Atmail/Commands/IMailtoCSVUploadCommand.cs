﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Atmail.Commands
{
    public interface IMailtoCSVUploadCommand
        : ICommand
    {
        ErsCsvContainer<ersAdmin.Models.csv.mailto_csv_upload> csv_file { get; set; }

        int? id { get; set; }

        bool upload { get; }

        bool chk_find { get; }

        bool regist { get; set; }
    }
}