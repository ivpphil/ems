﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.SiteBase.Command;

namespace ersAdmin.Domain.Atmail.Commands
{
    public interface IMailsetupCommand
        : ISiteRegisterBaseCommand, ICommand
    {
        int? id { get; }

        bool confirm { get; }

        string r_email { get; set; }

        string p_from { get; }
    }
}