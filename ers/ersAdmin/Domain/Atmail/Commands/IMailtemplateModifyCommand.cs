﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Atmail.Commands
{
    public interface IMailtemplateModifyCommand
        : ICommand
    {
        int? id { get; }

        bool modify { get; }

        bool IsConfirmationPage { get; }

        bool IsModifyCompletionPage { get; }
    }
}