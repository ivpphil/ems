﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersAdmin.Domain.Atmail.Commands
{
    public interface ITestMailCommand
        : ICommand
    {
        int? id { get; }

        string default_send_testmail { get; }

        string body { get; }

        string html_body { get; }

        string feature_body { get; }

        string subject { get; }
    }
}