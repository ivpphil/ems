﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Commands
{
    public interface IMailinfoCommand
        : ICommand
    {
        int? id { get; }

        string pageName { get; }

        string html_body { get; set; }

        string s_hour { get; set; }

        string s_date { get; set; }

        EnumAmSSet? s_set { get; }

        DateTime? scheduled_date { get; set; }

        int? setup_id { get; set; }
    }
}