﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Atmail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.atmail.process;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Atmail.Mappers
{
    public class MailinglistTotalMapper
        : IMapper<IMailinglistTotalMappable>
    {
        public void Map(IMailinglistTotalMappable objMappable)
        {
            GetTotalList(objMappable);
        }

        //function for total page (summary)
        public void GetTotalList(IMailinglistTotalMappable objMappable)
        {
            objMappable.list = GetList(objMappable);
            objMappable.recordCount = objMappable.list.Count();

            objMappable.thisMnthFrm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            objMappable.thisMnthTo = objMappable.thisMnthFrm.AddMonths(1).AddDays(-1);
            objMappable.lstMnthFrm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1);
            objMappable.lstMnthTo = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1);

            objMappable.this25To = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 25);
            objMappable.this25Frm = objMappable.this25To.AddMonths(-1).AddDays(1);
            objMappable.lst25To = objMappable.this25Frm.AddDays(-1);
            objMappable.lst25Frm = objMappable.lst25To.AddMonths(-1).AddDays(1);

            objMappable.thisMnthCnt = getDateCount(objMappable.thisMnthFrm, objMappable.thisMnthTo, 0);
            objMappable.thisMnthNotCnt = getDateCount(objMappable.thisMnthFrm, objMappable.thisMnthTo, 1);
            objMappable.lstMnthCnt = getDateCount(objMappable.lstMnthFrm, objMappable.lstMnthTo, 0);
            objMappable.lstMnthNotCnt = getDateCount(objMappable.lstMnthFrm, objMappable.lstMnthTo, 1);
            objMappable.this25Cnt = getDateCount(objMappable.this25Frm, objMappable.this25To, 0);
            objMappable.this25NotCnt = getDateCount(objMappable.this25Frm, objMappable.this25To, 1);
            objMappable.lst25Cnt = getDateCount(objMappable.lst25Frm, objMappable.lst25To, 0);
            objMappable.lst25NotCnt = getDateCount(objMappable.lst25Frm, objMappable.lst25To, 1);
        }

        //get the record count per date
        internal long getDateCount(DateTime dateFrm, DateTime dateTo, int sw)
        {
            var mailtoRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var processRepo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var processCri = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            processCri.id_not_equal = setup.monitorMassSendProcessID;
            processCri.scheduled_date_from = dateFrm.GetStartForSearch();
            processCri.scheduled_date_to = dateTo.GetEndForSearch();
            processCri.step_mail_id = null;
            var listall = processRepo.Find(processCri);
            long cnt = 0;
            foreach (var maillist in listall)
            {
                var mailtoCri = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
                mailtoCri.process_id = maillist.id;
                if (sw == 1)
                    mailtoCri.sent_flg = 0;

                cnt = cnt + mailtoRepo.GetRecordCount(mailtoCri);
            }

            return cnt;
        }

        //get the list of not sent records
        internal List<Dictionary<string, object>> GetList(IMailinglistTotalMappable objMappable)
        {
            var processRepo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var mailtoRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var criteria = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();
            criteria.id_not_equal = setup.monitorMassSendProcessID;
            criteria.scheduled_date_from = DateTime.Now.AddMonths(-2);
            criteria.scheduled_date_to = DateTime.Now;
            criteria.step_mail_id = null;
            if (objMappable.sort != 1)
            {
                criteria.SetOrderByProcessID(Criteria.OrderBy.ORDER_BY_ASC);
            }
            else
            {
                criteria.SetOrderByProcessID(Criteria.OrderBy.ORDER_BY_DESC);
            }

            var listall = processRepo.Find(criteria);
            var ret = new List<Dictionary<string, object>>();
            foreach (var maillist in listall)
            {
                long sent = 0; long not_sent = 0;
                var dictionary = maillist.GetPropertiesAsDictionary();
                var mailtoCri = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
                mailtoCri.process_id = maillist.id;
                mailtoCri.sent_flg = EnumSentFlg.Sent;
                sent = mailtoRepo.GetRecordCount(mailtoCri);
                dictionary.Add("sent_cnt", sent);

                mailtoCri = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
                mailtoCri.process_id = maillist.id;
                mailtoCri.sent_flg = 0;
                not_sent = mailtoRepo.GetRecordCount(mailtoCri);
                dictionary.Add("not_sent_cnt", not_sent);

                dictionary.Add("total_cnt", sent + not_sent);

                dictionary.Add("status_desc", ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.AmProcessStatus, EnumCommonNameColumnName.namename, (int)maillist.status));

                if (sent != 0)
                    ret.Add(dictionary);
            }

            return ret;
        }
    }
}