﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Atmail.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using ersAdmin.Models.atmail;


namespace ersAdmin.Domain.Atmail.Mappers
{
    public class MailTemplateMapper : IMapper<IMailTemplateMappable>
    {
        public void Map(IMailTemplateMappable objMappable)
        {
            LoadList(objMappable);
        }


        //for getting the list of template
        public void LoadList(IMailTemplateMappable objMappable)
        {
            var templateCriteria = ErsFactory.ErsAtMailFactory.GetErsAmTemplateCriteria();
            templateCriteria.active = EnumActive.Active;

            var setTemplateRepo = ErsFactory.ErsAtMailFactory.GetErsAmTemplateRepository();

            objMappable.recordCount = setTemplateRepo.GetRecordCount(templateCriteria);

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(templateCriteria);
            }

            templateCriteria.SetOrderByTemplateID(Criteria.OrderBy.ORDER_BY_ASC);

            var result = setTemplateRepo.Find(templateCriteria);

            var listTemplateList = new List<mailtemplate_record>();
            foreach (var record in result)
            {
                var mailtemplate_record = new mailtemplate_record();
                mailtemplate_record.OverwriteWithParameter(record.GetPropertiesAsDictionary());
                listTemplateList.Add(mailtemplate_record);
            }
            objMappable.templateList = listTemplateList;

        }
    }
}