﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Atmail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Mappers
{
    public class MailinfoBodyMapper : IMapper<IMailinfoBodyMappable>
    {
        public void Map(IMailinfoBodyMappable objMappable)
        {
            LoadBody(objMappable);
        }

        //for loading am_process_t records
        public void LoadBody(IMailinfoBodyMappable objMappable)
        {
            var process = ErsFactory.ErsAtMailFactory.GetErsProcess();
            var mailinfo = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(objMappable.id.Value);
            objMappable.OverwriteWithParameter(mailinfo.GetPropertiesAsDictionary());
            objMappable.status_desc = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.AmProcessStatus, EnumCommonNameColumnName.namename, (int)objMappable.status);
            objMappable.feature_body = mailinfo.feature_body.Replace("\r\n", "<br />");
            objMappable.body = mailinfo.body.Replace("\r\n", "<br />");
        }
    }
}