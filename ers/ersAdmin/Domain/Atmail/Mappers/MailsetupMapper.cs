﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Atmail.Mappables;
using jp.co.ivp.ers.mvc;
using System.Text.RegularExpressions;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappers;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Atmail.Mappers
{
    public class MailsetupMapper
        : SiteRegisterBaseMapper, IMapper<IMailsetupMappable>
    {
        public void Map(IMailsetupMappable objMappable)
        {
            loadSetup(objMappable);
        }

        //load setup records
        private void loadSetup(IMailsetupMappable objMappable)
        {
            // 初期サイトIDセット [Set default site ID]
            this.SetDefaultSiteId(objMappable);

            var setMstRepo = ErsFactory.ErsAtMailFactory.GetErsAmSetupRepository();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsErsAmSetupCriteria();

            // 検索用サイトIDプロパティセット [Set search property of site ID to Criteria]
            this.SetSearchPropertyOfSiteIdToCriteria(objMappable, criteria);

            var setup = setMstRepo.Find(criteria);

            if (setMstRepo.GetRecordCount(null) != 0)
            {
                var setup_rec = setup[0];
                objMappable.OverwriteWithParameter(setup_rec.GetPropertiesAsDictionary());

                var matchs = SplitMailPart(objMappable.r_email);
                if (matchs.Count != 0)
                {
                    objMappable.p_from = matchs[0].Groups[1].Value;
                    objMappable.r_email = matchs[0].Groups[2].Value;
                }
            }

        }

        //split part of email
        private static MatchCollection SplitMailPart(string mailAddress)
        {
            string operationTagStart = "^(.+)?<(.+)>$";
            var matchs = new Regex(operationTagStart, RegexOptions.IgnoreCase).Matches(mailAddress);
            return matchs;
        }
    }
}