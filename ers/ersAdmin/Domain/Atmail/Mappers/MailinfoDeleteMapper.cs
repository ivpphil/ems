﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Atmail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Mappers
{
    public class MailinfoDeleteMapper
        : IMapper<IMailinfoDeleteMappable>
    {
        public void Map(IMailinfoDeleteMappable objMappable)
        {
            LoadInfo(objMappable);
        }

        //for loading am_process_t records
        private void LoadInfo(IMailinfoDeleteMappable objMappable)
        {
            var mailtoRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var processCri = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();
            processCri.id = objMappable.id;

            var mailinfo = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(objMappable.id.Value);
            objMappable.OverwriteWithParameter(mailinfo.GetPropertiesAsDictionary());

            //total count
            var mailtoCri = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            mailtoCri.process_id = objMappable.id;
            objMappable.mailto_total_cnt = mailtoRepo.GetRecordCount(mailtoCri);

            //total sent count
            var mailtoCri1 = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            mailtoCri1.process_id = objMappable.id;
            mailtoCri1.sent_flg = EnumSentFlg.Sent;
            objMappable.mailto_sent_cnt = mailtoRepo.GetRecordCount(mailtoCri1);

            //total not sent count
            objMappable.mailto_notsent_cnt = objMappable.mailto_total_cnt - objMappable.mailto_sent_cnt;
        }
    }
}