﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Atmail.Mappables;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Mappers
{
    public class TestMailMapper
        : IMapper<ITestMailMappable>
    {
        public void Map(ITestMailMappable objMappable)
        {
            LoadDefaultData(objMappable);
        }

        internal void LoadDefaultData(ITestMailMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (String.IsNullOrEmpty(objMappable.default_send_testmail))
                objMappable.default_send_testmail = setup.default_send_testmail;

            if (objMappable.id != null)
            {
                var processRepo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
                var process = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(objMappable.id.Value);
                objMappable.OverwriteWithParameter(process.GetPropertiesAsDictionary());
            }
        }
    }
}