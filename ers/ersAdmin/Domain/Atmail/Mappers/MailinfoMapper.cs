﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.Domain.Atmail.Mappables;

namespace ersAdmin.Domain.Atmail.Mappers
{
    public class MailinfoMapper
        : IMapper<IMailinfoMappable>
    {
        public void Map(IMailinfoMappable objMappable)
        {
            if (objMappable.IsTemplatePage2)
            {
                if (objMappable.template_id != null)
                {
                    LoadTemplate(objMappable);
                    objMappable.html_body = returnHTMLbody(objMappable.html_body);
                    return;
                }
            }

            if (objMappable.pageName == "tmail3")
            {
                LoadDate(objMappable);
                objMappable.html_body = updateHTMLbody(objMappable.html_body);
                return;
            }

            if (objMappable.pageName == "tmail4")
            {
                objMappable.html_body = updateHTMLbody(objMappable.html_body);
                return;
            }

            LoadInfo(objMappable);
        }

        ////for loading am_process_t records
        public void LoadInfo(IMailinfoMappable objMappable)
        {
            var mailtoRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var mailinfo = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(objMappable.id.Value);
            objMappable.OverwriteWithParameter(mailinfo.GetPropertiesAsDictionary());
            objMappable.setup_id = GetSetupId(mailinfo.from_name, mailinfo.from_email);

            //total count
            var mailtoCri = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            mailtoCri.process_id = objMappable.id;
            objMappable.mailto_total_cnt = mailtoRepo.GetRecordCount(mailtoCri);

            //total sent count
            var mailtoCri1 = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            mailtoCri1.process_id = objMappable.id;
            mailtoCri1.sent_flg = EnumSentFlg.Sent;
            objMappable.mailto_sent_cnt = mailtoRepo.GetRecordCount(mailtoCri1);

            //total not sent count
            objMappable.mailto_notsent_cnt = objMappable.mailto_total_cnt - objMappable.mailto_sent_cnt;
        }

        public void LoadTemplate(IMailinfoMappable objMappable)
        {
            if (objMappable.template_id == "none")
            {
                objMappable.subject = "";
                objMappable.body = "";
                objMappable.feature_body = "";
                objMappable.html_body = "";
            }
            else
            {
                var templateinfo = ErsFactory.ErsAtMailFactory.GetErsAmTemplateWithId(System.Convert.ToInt32(objMappable.template_id));
                objMappable.subject = templateinfo.subject;
                objMappable.body = templateinfo.body;
                objMappable.feature_body = templateinfo.feature_body;
                objMappable.html_body = templateinfo.html_body;
            }
        }

        //use for loading of datenow
        public void LoadDate(IMailinfoMappable objMappable)
        {
            objMappable.s_date = DateTime.Now.ToString("yyyy/MM/dd");
            objMappable.s_hour = DateTime.Now.Hour.ToString();
        }
        
        //update html body that uses <ers:redirectURL>
        public string updateHTMLbody(string strBody)
        {
            string updateBody = string.Empty;

            if (strBody != null)
            {
                updateBody = strBody.Replace("&lt;", "<");
                updateBody = updateBody.Replace("&gt;", ">");
            }

            return updateBody;
        }

        //return the correct html body using <ers:redirectURL>
        public string returnHTMLbody(string strBody)
        {
            string updateBody = string.Empty;

            if (strBody != null)
            {
                updateBody = strBody.Replace("<ers:redirectURL>", "&lt;ers:redirectURL&gt;");
                updateBody = updateBody.Replace("</ers:redirectURL>", "&lt;/ers:redirectURL&gt;");
            }

            return updateBody;
        }

        /// <summary>
        /// Gets am_setup_t's id using r_email.
        /// </summary>
        /// <param name="from_name"></param>
        /// <param name="from_email"></param>
        /// <returns></returns>
        private int? GetSetupId(string from_name, string from_email)
        {
            if (!string.IsNullOrEmpty(from_name) && !string.IsNullOrEmpty(from_email))
            {
                var criteria = ErsFactory.ErsAtMailFactory.GetErsErsAmSetupCriteria();

                criteria.r_email = from_name + "<" + from_email + ">";

                var result = ErsFactory.ErsAtMailFactory.GetErsAmSetupRepository().FindSingle(criteria);

                if (result != null)
                {
                    return result.id;
                }
            }

            return null;
        }
    }
}