﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Atmail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Mappers
{
    public class MailinfoNewListMapper
        : IMapper<IMailinfoNewListMappable>
    {
        public void Map(IMailinfoNewListMappable objMappable)
        {
            LoadInfo(objMappable);
        }

        //loading am_process_t records
        private void LoadInfo(IMailinfoNewListMappable objMappable)
        {
            var mailtoRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var mailinfo = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(objMappable.id.Value);
            objMappable.OverwriteWithParameter(mailinfo.GetPropertiesAsDictionary());

            //total count
            var mailtoCri = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            mailtoCri.process_id = objMappable.id;
            objMappable.mailto_total_cnt = mailtoRepo.GetRecordCount(mailtoCri);
        }
    }
}