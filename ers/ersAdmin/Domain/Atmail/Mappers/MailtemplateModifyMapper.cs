﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Atmail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Atmail.Mappers
{
    public class MailtemplateModifyMapper
        : IMapper<IMailtemplateModifyMappable>
    {
        public void Map(IMailtemplateModifyMappable objMappable)
        {
            if (objMappable.IsModifyCompletionPage)
            {
                objMappable.html_body = updateHTMLbody(objMappable.html_body);
                return;
            }

            if (objMappable.IsConfirmationPage)
            {
                objMappable.html_body = returnHTMLbody(objMappable.html_body);
                return;
            }


            loadTemplate(objMappable);
            
        }

        //use to get the value of template per id
        public void loadTemplate(IMailtemplateModifyMappable objMappable)
        {
            var setTemplateRepo = ErsFactory.ErsAtMailFactory.GetErsAmTemplateRepository();
            var templateCriteria = ErsFactory.ErsAtMailFactory.GetErsAmTemplateCriteria();

            templateCriteria.id = objMappable.id;
            templateCriteria.active = EnumActive.Active;
            var template = setTemplateRepo.Find(templateCriteria);
            objMappable.recordCount = setTemplateRepo.GetRecordCount(templateCriteria);

            if (objMappable.recordCount != 0)
            {
                var temp_rec = template[0];
                objMappable.OverwriteWithParameter(temp_rec.GetPropertiesAsDictionary());
                objMappable.template_rec = ErsCommon.ConvertEntityListToDictionaryList(setTemplateRepo.Find(null));
            }
        }

        //return html body that uses <ers:redirectURL>
        public string returnHTMLbody(string strBody)
        {
            string updateBody = string.Empty;

            if (strBody != null)
            {
                updateBody = strBody.Replace("<ers:redirectURL>", "&lt;ers:redirectURL&gt;");
                updateBody = updateBody.Replace("</ers:redirectURL>", "&lt;/ers:redirectURL&gt;");
            }

            return updateBody;
        }

        //update html body that uses <ers:redirectURL>
        public string updateHTMLbody(string strBody)
        {
            string updateBody = string.Empty;
            if (!String.IsNullOrEmpty(strBody))
            {
                updateBody = strBody.Replace("&lt;", "<");
                updateBody = updateBody.Replace("&gt;", ">");
            }

            return updateBody;
        }
    }
}