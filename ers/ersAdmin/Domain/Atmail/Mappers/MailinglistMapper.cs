﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Atmail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.atmail.process;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.Atmail.Mappers
{
    public class MailinglistMapper
        : IMapper<IMailinglistMappable>
    {
        public void Map(IMailinglistMappable objMappable)
        {
            LoadList(objMappable);
        }

        //get the list of process
        public void LoadList(IMailinglistMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var processRepo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var processCri = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();
            processCri.step_mail_id = null;
            processCri.id_not_equal = setup.monitorMassSendProcessID;

            //件数
            objMappable.recordCount = processRepo.GetRecordCount(processCri);

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(processCri);
            }

            if (objMappable.sort == 1)
                processCri.SetOrderByProcessID(Criteria.OrderBy.ORDER_BY_ASC);
            else
                processCri.SetOrderByProcessID(Criteria.OrderBy.ORDER_BY_DESC);

            objMappable.list = GetList(false, processCri);
        }

        //get the list of not sent records
        internal List<Dictionary<string, object>> GetList(Boolean not_sw, ErsProcessCriteria criteria)
        {
            var processRepo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var mailtoRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();

            var listall = processRepo.Find(criteria);
            var ret = new List<Dictionary<string, object>>();
            foreach (var maillist in listall)
            {
                long sent = 0; long not_sent = 0;
                var dictionary = maillist.GetPropertiesAsDictionary();
                var mailtoCri = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
                mailtoCri.process_id = maillist.id;
                mailtoCri.sent_flg = EnumSentFlg.Sent;
                sent = mailtoRepo.GetRecordCount(mailtoCri);
                dictionary.Add("sent_cnt", sent);

                mailtoCri = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
                mailtoCri.process_id = maillist.id;
                mailtoCri.sent_flg = 0;
                not_sent = mailtoRepo.GetRecordCount(mailtoCri);
                dictionary.Add("not_sent_cnt", not_sent);

                dictionary.Add("total_cnt", sent + not_sent);

                dictionary.Add("status_desc", ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.AmProcessStatus, EnumCommonNameColumnName.namename, (int)maillist.status));

                if (not_sw)
                {
                    if (not_sent != 0)
                        ret.Add(dictionary);
                }
                else
                {
                    ret.Add(dictionary);
                }
            }

            return ret;
        }
    }
}