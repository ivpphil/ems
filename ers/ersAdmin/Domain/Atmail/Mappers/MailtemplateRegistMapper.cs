﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Atmail.Mappables;

namespace ersAdmin.Domain.Atmail.Mappers
{
    public class MailtemplateRegistMapper
        :IMapper<IMailtemplateRegistMappable>
    {
        public void Map(IMailtemplateRegistMappable objMappable)
        {
            if (objMappable.IsCompletionPage)
                objMappable.html_body = updateHTMLbody(objMappable.html_body);
        }

        //update html body that uses <ers:redirectURL>
        public string updateHTMLbody(string strBody)
        {
            string updateBody = string.Empty;
            if (!String.IsNullOrEmpty(strBody))
            {
                updateBody = strBody.Replace("&lt;", "<");
                updateBody = updateBody.Replace("&gt;", ">");
            }

            return updateBody;
        }
    }
}