﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersAdmin.Domain.Atmail.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using ersAdmin.Models.csv;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Mappers
{
    public class MailinfoDownloadMapper
        : IMapper<IMailinfoDownloadMappable>
    {
        public void Map(IMailinfoDownloadMappable objMappable)
        {
            if (!objMappable.IsDownloadCompletionPage)
            {
                LoadInfo(objMappable);
                return;
            }

            CreateCsvFile(objMappable);
        }

        //use for loading am_process_t records
        private void LoadInfo(IMailinfoDownloadMappable objMappable)
        {
            var mailtoRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var mailinfo = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(objMappable.id.Value);
            objMappable.OverwriteWithParameter(mailinfo.GetPropertiesAsDictionary());

            //total count
            var mailtoCri = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            mailtoCri.process_id = objMappable.id;
            objMappable.mailto_total_cnt = mailtoRepo.GetRecordCount(mailtoCri);
        }

        //use for creating csv file
        private void CreateCsvFile(IMailinfoDownloadMappable objMappable)
        {
            objMappable.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();

            var mailtoRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var filename = DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";

            ersAdmin.Models.csv.mailinfo_csv mailinfo_csv;
            var mailtoCri = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            mailtoCri.process_id = objMappable.id;
            var listmail = mailtoRepo.Find(mailtoCri);

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                foreach (var item in listmail)
                {
                    mailinfo_csv = new ersAdmin.Models.csv.mailinfo_csv();
                    mailinfo_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());

                    objMappable.csvCreater.WriteBody(mailinfo_csv, writer);
                }
            }
        }
    }
}