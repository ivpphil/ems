﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersAdmin.Domain.Atmail.Mappables
{
    public interface IMailinglistMappable
        : IMappable
    {
        long recordCount { set; }

        ErsPagerModel pager { get; }

        int? sort { get; }

        List<Dictionary<string, object>> list { set; }
    }
}