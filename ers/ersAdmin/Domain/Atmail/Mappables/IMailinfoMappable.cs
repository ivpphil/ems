﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Atmail.Mappables
{
    public interface IMailinfoMappable
        : IMappable
    {
        int? id { get; }

        int? setup_id { set; }

        string pageName { get; }

        long? mailto_total_cnt { get; set; }

        long? mailto_sent_cnt { get; set; }

        long? mailto_notsent_cnt { set; }

        string template_id { get; }

        string subject { set; }

        string body { get; set; }

        string feature_body { get; set; }

        string html_body { get;  set; }

        string s_date { set; }

        string s_hour { set; }

        bool IsTemplatePage2 { get; }
    }
}