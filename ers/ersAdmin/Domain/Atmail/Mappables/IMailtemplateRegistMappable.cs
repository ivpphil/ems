﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.atmail;

namespace ersAdmin.Domain.Atmail.Mappables
{
    public interface IMailtemplateRegistMappable
        : IMappable
    {
        string html_body { get; set; }

        bool IsCompletionPage { get; }
    }
}