﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Atmail.Mappables
{
    public interface IMailinglistTotalMappable
        : IMappable
    {
        int? sort { get; }

        long recordCount { set; }

        List<Dictionary<string, object>> list { get; set; }

        DateTime thisMnthTo { get; set; }
        DateTime thisMnthFrm { get;  set; }
        DateTime lstMnthTo { get;  set; }
        DateTime lstMnthFrm { get; set; }
        DateTime this25To { get; set; }
        DateTime this25Frm { get; set; }
        DateTime lst25To { get;  set; }
        DateTime lst25Frm { get;  set; }

        long thisMnthCnt { set; }
        long thisMnthNotCnt { set; }
        long lstMnthCnt { set; }
        long lstMnthNotCnt { set; }
        long this25Cnt { set; }
        long this25NotCnt { set; }
        long lst25Cnt { set; }
        long lst25NotCnt { set; }


    }
}