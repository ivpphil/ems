﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Domain.SiteBase.Mappables;

namespace ersAdmin.Domain.Atmail.Mappables
{
    public interface IMailsetupMappable
        : ISiteRegisterBaseMappable, IMappable
    {
        string r_email { get; set; }

        string p_from { set; }

    }
}