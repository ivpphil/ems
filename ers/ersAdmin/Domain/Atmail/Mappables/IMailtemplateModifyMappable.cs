﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Atmail.Mappables
{
    public interface IMailtemplateModifyMappable
         : IMappable
    {
        int? id { get; }

        long recordCount { get; set; }

        List<Dictionary<string, object>> template_rec { get; set; }

        bool IsConfirmationPage { get; }

        bool IsModifyCompletionPage { get; }

        string html_body { get; set; }
    }
}