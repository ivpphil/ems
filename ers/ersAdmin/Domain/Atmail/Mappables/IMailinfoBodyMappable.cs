﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Mappables
{
    public interface IMailinfoBodyMappable : IMappable
    {
        int? id { get; }

        EnumAmProcessStatus? status { get;  }

        string status_desc { set; }

        string body { set; }

        string feature_body { set; }
    }
}