﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;

namespace ersAdmin.Domain.Atmail.Mappables
{
    public interface IMailinfoDownloadMappable
        : IMappable
    {
        int? id { get; }

        long? mailto_total_cnt { set; }

        bool IsDownloadCompletionPage { get; }

        ErsCsvCreater csvCreater { get; set; }
    }
}