﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersAdmin.Models.atmail;
using jp.co.ivp.ers.mvc.pager;


namespace ersAdmin.Domain.Atmail.Mappables
{
    public interface IMailTemplateMappable : IMappable
    {
        long recordCount { set; }
        IList<mailtemplate_record> templateList { set; }
        ErsPagerModel pager { get; }
    }
}