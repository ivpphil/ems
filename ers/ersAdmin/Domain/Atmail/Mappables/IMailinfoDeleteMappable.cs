﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Atmail.Mappables
{
    public interface IMailinfoDeleteMappable
        : IMappable
    {
        int? id { get; }

        long? mailto_total_cnt { get;  set; }

        long? mailto_sent_cnt { get; set; }

        long? mailto_notsent_cnt { set; }
    }
}