﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Atmail.Mappables
{
    public interface IMailinfoNewListMappable
        : IMappable
    {
        int? id { get; }

        long? mailto_total_cnt { set; }
    }
}