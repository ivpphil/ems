﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersAdmin.Domain.Atmail.Mappables
{
    public interface ITestMailMappable
        : IMappable
    {
        string default_send_testmail { get; set; }

        int? id { get; }
    }
}