﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class ValidateMailtemplateModify
        : IValidationHandler<IMailtemplateModifyCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IMailtemplateModifyCommand command)
        {
            if (command.modify)
                yield return command.CheckRequired("template_name");
        }
    }
}