﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class MailinfoDeleteHandler
        : ICommandHandler<IMailinfoDeleteCommand>
    {
        public ICommandResult Submit(IMailinfoDeleteCommand command)
        {
            DeleteProcess(command);
            return new CommandResult(true);
        }

        //use for deleting am_process_t and am_mailto_t table
        private void DeleteProcess(IMailinfoDeleteCommand command)
        {
            var mailtoRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var processRepo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var mailinfo = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(command.id.Value);

            processRepo.Delete(mailinfo);

            var MailToCriteria = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            MailToCriteria.process_id = command.id.Value;
            if (mailtoRepo.GetRecordCount(MailToCriteria) != 0)
                mailtoRepo.Delete(MailToCriteria);

        }
    }
}