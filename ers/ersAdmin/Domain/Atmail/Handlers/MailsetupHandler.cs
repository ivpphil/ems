﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class MailsetupHandler
        : ICommandHandler<IMailsetupCommand>
    {
        public ICommandResult Submit(IMailsetupCommand command)
        {
            insertSetup(command);
            return new CommandResult(true);
        }

        //inserting/updating reply/return email address on setup table
        private void insertSetup(IMailsetupCommand command)
        {
            var string_old_rmail = string.Empty;
            string_old_rmail = command.r_email;
            var setMstRepo = ErsFactory.ErsAtMailFactory.GetErsAmSetupRepository();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsErsAmSetupCriteria();
            criteria.id = command.id;
            var cnt = setMstRepo.GetRecordCount(criteria);
            var setMst = ErsFactory.ErsAtMailFactory.GetErsAmSetup();

            command.r_email = command.p_from + " <" + command.r_email + ">";
            setMst.OverwriteWithParameter(command.GetPropertiesAsDictionary());
            setMst.site_id = Convert.ToInt32(command.site_id);

            //inserting records on setup

            if (cnt == 0)
            {
                setMstRepo.Insert(setMst);
            }
            //for updating existing record on setup
            else
            {
                var setup = setMstRepo.Find(criteria);
                setMstRepo.Update(setup[0], setMst);
            }
            command.r_email = string_old_rmail;
        }
    }
}