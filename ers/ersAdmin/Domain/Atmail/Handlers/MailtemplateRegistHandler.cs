﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class MailtemplateRegistHandler
        : ICommandHandler<IMailtemplateRegistCommand>
    {
        public ICommandResult Submit(IMailtemplateRegistCommand command)
        {
            this.insertTemplate(command);
            return new CommandResult(true);
        }

        //inserting template record
        private void insertTemplate(IMailtemplateRegistCommand command)
        {
            var setTemplateRepo = ErsFactory.ErsAtMailFactory.GetErsAmTemplateRepository();
            var amTemplate = ErsFactory.ErsAtMailFactory.GetErsAmTemplate();
            amTemplate.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            amTemplate.active = EnumActive.Active;
            setTemplateRepo.Insert(amTemplate);



        }
    }
}