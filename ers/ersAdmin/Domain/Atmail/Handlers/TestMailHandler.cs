﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.sendmail.mass_send;
using jp.co.ivp.ers.atmail;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class TestMailHandler
        : ICommandHandler<ITestMailCommand>
    {
        public ICommandResult Submit(ITestMailCommand command)
        {
            SendMail(command);

            return new CommandResult(true);
        }

        internal void SendMail(ITestMailCommand command)
        {
            var sendmail = ErsFactory.ersMailFactory.GetErsSendMailAtMail();
            var mailtoRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();

            string mail_fom = string.Empty;

            var mailtoCri = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            mailtoCri.process_id = command.id;

            var setMstRepo = ErsFactory.ErsAtMailFactory.GetErsAmSetupRepository();
            if (setMstRepo.GetRecordCount(null) != 0)
            {
                var am_setup = setMstRepo.Find(null)[0];
                mail_fom = am_setup.r_email;
            }

            var mailTo = this.GetTestMailTo();
            var setup = ErsFactory.ersBatchFactory.getSetup();


            if (!string.IsNullOrEmpty(command.body) || !string.IsNullOrEmpty(command.html_body))
            {
                sendmail = getNewSendMail(command, mail_fom);
                sendmail.Bind(mailTo, command.body, command.html_body, null);
                sendmail.SendMail(setup.smtpHostName, setup.smtpPort);
            }

            if (!string.IsNullOrEmpty(command.feature_body))
            {
                sendmail = getNewSendMail(command, mail_fom);
                sendmail.Bind(mailTo, command.feature_body, null);
                sendmail.SendMail(setup.smtpHostName, setup.smtpPort);
            }
        }

        EmailToModel GetTestMailTo()
        {
            var mailTo = ErsFactory.ErsAtMailFactory.GetEmailToModel();
            mailTo.mcode = "99999999";
            mailTo.lname = "テスト姓";
            mailTo.fname = "てすと名";
            mailTo.etc1 = "備考１";
            mailTo.etc2 = "備考２";

            return mailTo;
        }

        private ErsSendMailAtMail getNewSendMail(ITestMailCommand command, string mail_from)
        {
            var sendmail = ErsFactory.ersMailFactory.GetErsSendMailAtMail();

            sendmail.mail_to = command.default_send_testmail;
            sendmail.subject = command.subject;
            sendmail.mail_from = mail_from;

            return sendmail;
        }
    }
}