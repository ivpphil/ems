﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class MailtoCSVUploadHandler
        : ICommandHandler<IMailtoCSVUploadCommand>
    {
        public ICommandResult Submit(IMailtoCSVUploadCommand command)
        {
            Update(command);
            return new CommandResult(true);
        }

        //inserting records from csv file to process and mailto table
        internal void Update(IMailtoCSVUploadCommand command)
        {
            var mailtoList = new List<ErsMailTo>();
            //getting member list from csv file
            foreach (var item in command.csv_file.GetValidModels())
            {
                var dictionary = ErsFactory.ErsAtMailFactory.GetErsMailToWithParameters(item.GetPropertiesAsDictionary());

                mailtoList.Add(dictionary);
            }

            var mailToRegist = ErsFactory.ErsAtMailFactory.GetRegistMailToStgy();

            //inserting am_process_t and am_mailto_t record
            command.id = mailToRegist.InsertNewMailList(EnumUpKind.UPLOAD, mailtoList);

            //update status after inserting all the member records
            var old_process = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(command.id.Value);
            var new_process = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(command.id.Value);
            new_process.status = EnumAmProcessStatus.NotSend;
            ErsFactory.ErsAtMailFactory.GetErsProcessRepository().Update(old_process, new_process);


        }
    }
}