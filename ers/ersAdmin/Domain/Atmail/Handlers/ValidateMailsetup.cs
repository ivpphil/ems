﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class ValidateMailsetup
        : IValidationHandler<IMailsetupCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IMailsetupCommand command)
        {
            if (command.confirm)
            {
                yield return command.CheckRequired("site_id");
                yield return command.CheckRequired("p_from");

                if ((command.p_from + " <" + command.r_email + ">").Length > 255)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10037", ErsResources.GetMessage("10100", ErsResources.GetFieldName("p_from"), ErsResources.GetFieldName("r_email")), 255));
                }
            }
        }
    }
}