﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class ValidateMailinfoDelete
        : IValidationHandler<IMailinfoDeleteCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IMailinfoDeleteCommand command)
        {
            if (command.id == null)
                yield return command.CheckRequired("id");
        }
    }
}