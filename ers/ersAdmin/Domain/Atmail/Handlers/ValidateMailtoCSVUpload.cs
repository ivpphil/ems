﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class ValidateMailtoCSVUpload
        : IValidationHandler<IMailtoCSVUploadCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IMailtoCSVUploadCommand command)
        {
            if (!command.regist)
            {
                if (command.csv_file.csv_file == null)
                {
                    //アップロードファイルを指定してください。
                    //Specify the uploaded file
                    throw new ErsException("10202");
                }
            }

            var controller = command.controller;
            foreach (var model in command.csv_file.GetValidatedModels(command.chk_find))
            {
                if (!model.IsValid)
                {
                    if (!command.regist)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "csv_file" });
                        }
                    }
                    command.csv_file.MarkRecordAsInvalid(model);
                }
            }

            //保持された登録情報が無い場合、エラーメッセージを表示。
            if (command.csv_file.validIndexes.Count() == 0)
            {
                //対象データが存在しません。確認してください。
                yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "csv_file" });
            }
        }
    }
}