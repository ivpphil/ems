﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class MailtemplateModifyHandler
        : ICommandHandler<IMailtemplateModifyCommand>
    {
        public ICommandResult Submit(IMailtemplateModifyCommand command)
        {
            if (command.IsModifyCompletionPage)
                this.modifyTemplate(command);

            return new CommandResult(true);
        }

        //updating template record
        private void modifyTemplate(IMailtemplateModifyCommand command)
        {
            var setTemplateRepo = ErsFactory.ErsAtMailFactory.GetErsAmTemplateRepository();
            var setTemplate = ErsFactory.ErsAtMailFactory.GetErsAmTemplate();
            var templateCriteria = ErsFactory.ErsAtMailFactory.GetErsAmTemplateCriteria();

            setTemplate.OverwriteWithParameter(command.GetPropertiesAsDictionary());
            templateCriteria.id = command.id;
            var template = setTemplateRepo.Find(templateCriteria);
            setTemplateRepo.Update(template[0], setTemplate);

        }
    }
}