﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.atmail;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class ValidationTestMail
        : IValidationHandler<ITestMailCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ITestMailCommand command)
        {
            yield return command.CheckRequired("id");

            if (command.id != null)
            {
                yield return command.CheckRequired("default_send_testmail");

                var processRepo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
                var process = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(command.id.Value);

                if (process != null)
                {
                    if (!IsEmpty(process))
                    {
                        throw new ErsException("40004");
                    }
                }
            }

        }

        bool IsEmpty(ErsProcess proc)
        {
            if (string.IsNullOrEmpty(proc.body) && string.IsNullOrEmpty(proc.html_body) && string.IsNullOrEmpty(proc.feature_body))
                return false;

            return true;
        }
    }
}