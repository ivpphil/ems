﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class MailtemplateDeleteHandler
        : ICommandHandler<IMailtemplateDeleteCommand>
    {
        public ICommandResult Submit(IMailtemplateDeleteCommand command)
        {

            if (command.IsDeleteCompletionPage)
                this.deleteTemplate(command);

            return new CommandResult(true);
        }



        //deleting template records
        private void deleteTemplate(IMailtemplateDeleteCommand command)
        {
            var setTemplateRepo = ErsFactory.ErsAtMailFactory.GetErsAmTemplateRepository();
            var templateCriteria = ErsFactory.ErsAtMailFactory.GetErsAmTemplateCriteria();

            templateCriteria.id = command.id;
            var template = setTemplateRepo.Find(templateCriteria);

            if (setTemplateRepo.GetRecordCount(templateCriteria) != 0)
                setTemplateRepo.Delete(template[0]);

        }
    }
}