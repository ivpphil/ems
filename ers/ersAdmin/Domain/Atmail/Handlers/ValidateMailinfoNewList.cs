﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class ValidateMailinfoNewList
        : IValidationHandler<IMailinfoNewListCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IMailinfoNewListCommand command)
        {
            if (command.id == null)
                yield return command.CheckRequired("id");
        }
    }
}