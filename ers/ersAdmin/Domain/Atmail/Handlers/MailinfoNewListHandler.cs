﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class MailinfoNewListHandler
        : ICommandHandler<IMailinfoNewListCommand>
    {
        public ICommandResult Submit(IMailinfoNewListCommand command)
        {
            CopyProcess(command);
            return new CommandResult(true);
        }

        //copy am_mailto_t records of selected process id
        private void CopyProcess(IMailinfoNewListCommand command)
        {
            var mailtoRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var process = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(command.id.Value);
            command.OverwriteWithParameter(process.GetPropertiesAsDictionary());

            var mailtoList = new List<ErsMailTo>();

            //get member list of selected process id
            var mailtoCri = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            mailtoCri.process_id = command.id;
            var listmail = mailtoRepo.Find(mailtoCri);
            foreach (var item in listmail)
            {
                var dictionary = ErsFactory.ErsAtMailFactory.GetErsMailToWithParameters(item.GetPropertiesAsDictionary());
                dictionary.mformat = item.mformat;
                mailtoList.Add(dictionary);
            }

            var mailToRegist = ErsFactory.ErsAtMailFactory.GetRegistMailToStgy();

            //inserting am_process_t and am_mailto_t record
            command.id = mailToRegist.InsertNewMailList(EnumUpKind.COPY, mailtoList);


        }
    }
}