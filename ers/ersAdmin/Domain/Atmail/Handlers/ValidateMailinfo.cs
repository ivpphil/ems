﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.jp.co.ivp.ers;
using System.ComponentModel.DataAnnotations;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class ValidateMailinfo
        : IValidationHandler<IMailinfoCommand>
    {
        public IEnumerable<ValidationResult> Validate(IMailinfoCommand command)
        {
            if (!string.IsNullOrEmpty(command.html_body))
            {
                command.html_body = command.html_body.Replace("<br>", "<br>" + Environment.NewLine).Replace("<br/>", "<br/>" + Environment.NewLine).Replace("<br />", "<br />" + Environment.NewLine);
            }

            if (command.pageName == "tmail4" || command.pageName == "tmail3")
            {
                yield return command.CheckRequired("setup_id");
                yield return command.CheckRequired("subject");
                yield return command.CheckRequired("body");

                if (command.setup_id.HasValue)
                {
                    yield return CheckAmSetupIdExists(command.setup_id);
                }
            }

            if (command.pageName == "tmail4")
            {
                yield return command.CheckRequired("s_set");
                if (command.s_set == EnumAmSSet.Later)
                {
                    yield return command.CheckRequired("s_date");
                    yield return command.CheckRequired("s_hour");
                }
            }

            if (!string.IsNullOrEmpty(command.pageName))
            {
                var mailinfo = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(command.id.Value);
                if (mailinfo.status == EnumAmProcessStatus.Uploading)
                    throw new ErsException("63089");
                else if (mailinfo.status == EnumAmProcessStatus.Sent || mailinfo.status == EnumAmProcessStatus.Sending)
                    throw new ErsException("63090");
            }
        }

        /// <summary>
        /// Returns an error if there are no existing record using the ID as criteria.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private ValidationResult CheckAmSetupIdExists(int? id)
        {
            if (id.HasValue)
            {
                var criteria = ErsFactory.ErsAtMailFactory.GetErsErsAmSetupCriteria();

                criteria.id = id;

                if (ErsFactory.ErsAtMailFactory.GetErsAmSetupRepository().GetRecordCount(criteria) == 1)
                {
                    return null;
                }
            }
            
            return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName("mail_from_id")));
        }
    }
}