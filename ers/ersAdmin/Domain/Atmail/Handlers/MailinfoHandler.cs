﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersAdmin.Domain.Atmail.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers;
using ersAdmin.jp.co.ivp.ers;
using System.Text.RegularExpressions;

namespace ersAdmin.Domain.Atmail.Handlers
{
    public class MailinfoHandler
        : ICommandHandler<IMailinfoCommand>
    {
        public ICommandResult Submit(IMailinfoCommand command)
        {
            if (command.pageName == "tmail3")
            {
                UpdateProcess(command);
                command.html_body = returnHTMLbody(command.html_body);
            }

            if (command.pageName == "tmail4")
            {
                UpdateDate(command);
            }

            return new CommandResult(true);
        }

        //use for updating the record on am_process_t
        private void UpdateProcess(IMailinfoCommand command)
        {
            var processRepo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var mailinfo_new = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(command.id.Value);
            mailinfo_new.OverwriteWithModel(command);

            var setMstRepo = ErsFactory.ErsAtMailFactory.GetErsAmSetupRepository();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsErsAmSetupCriteria();
            criteria.id = command.setup_id;
            if (setMstRepo.GetRecordCount(criteria) != 0)
            {
                var setup = setMstRepo.Find(criteria).First();

                var matchs = SplitMailPart(setup.r_email);
                if (matchs.Count != 0)
                {
                    mailinfo_new.from_name = matchs[0].Groups[1].Value;
                    mailinfo_new.from_email = matchs[0].Groups[2].Value;
                    mailinfo_new.reply_email = setup.p_email;
                }
            }

            var mailinfo_old = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(command.id.Value);
            processRepo.Update(mailinfo_old, mailinfo_new);
        }

        //return the correct html body using <ers:redirectURL>
        public string returnHTMLbody(string strBody)
        {
            string updateBody = string.Empty;

            if (strBody != null)
            {                
                updateBody = strBody.Replace("<ers:redirectURL>", "&lt;ers:redirectURL&gt;");
                updateBody = updateBody.Replace("</ers:redirectURL>", "&lt;/ers:redirectURL&gt;");
            }

            return updateBody;
        }

        //use for updating process and mailto records and update scheduled date field depends on the option selected
        private void UpdateDate(IMailinfoCommand command)
        {
            if (String.IsNullOrEmpty(command.s_hour))
                command.s_hour = "00";

            command.s_hour = Convert.ToInt32(command.s_hour).ToString("D2");

            DateTime p;
            if (DateTime.TryParse(command.s_date, out p))
                command.s_date = p.ToString("yyyy/MM/dd") + " " + command.s_hour + ":" + 00 + ":" + 00;

            if (command.s_set == EnumAmSSet.Immediately)
                command.scheduled_date = DateTime.Now;
            else
                command.scheduled_date = System.Convert.ToDateTime(command.s_date);

            this.UpdateMail(command);
        }

        //for updating am_mailto_t and am_process_t table
        public void UpdateMail(IMailinfoCommand command)
        {
            var processRepo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var mailtoRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var mailtocriteria = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();

            mailtocriteria.process_id = command.id;
            IList<ErsMailTo> listmail = mailtoRepo.Find(mailtocriteria);

            var mailinfo_new = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(command.id.Value);
            mailinfo_new.OverwriteWithModel(command);

            var setMstRepo = ErsFactory.ErsAtMailFactory.GetErsAmSetupRepository();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsErsAmSetupCriteria();
            criteria.id = command.setup_id;
            if (setMstRepo.GetRecordCount(criteria) != 0)
            {
                var setup = setMstRepo.Find(criteria).First();

                var matchs = SplitMailPart(setup.r_email);
                if (matchs.Count != 0)
                {
                    mailinfo_new.from_name = matchs[0].Groups[1].Value;
                    mailinfo_new.from_email = matchs[0].Groups[2].Value;
                    mailinfo_new.reply_email = setup.p_email;
                }
            }

            var mailinfo_old = ErsFactory.ErsAtMailFactory.GetErsProcessWithId(command.id.Value);
            processRepo.Update(mailinfo_old, mailinfo_new);

            foreach (var mail in listmail)
            {
                mail.scheduled_date = command.scheduled_date;
                var mail_old = ErsFactory.ErsAtMailFactory.GetErsMailTo();
                mail_old.mcode = mail.mcode;
                mail_old.id = mail.id;
                mail_old.sent_flg = mail.sent_flg;
                mailtoRepo.Update(mail_old, mail);
            }
        }

        //split part of email
        private static MatchCollection SplitMailPart(string mailAddress)
        {
            string operationTagStart = "^(.+)?<(.+)>$";
            var matchs = new Regex(operationTagStart, RegexOptions.IgnoreCase).Matches(mailAddress);
            return matchs;
        }
    }
}