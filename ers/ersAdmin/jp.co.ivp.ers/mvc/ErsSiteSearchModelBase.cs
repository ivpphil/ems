﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersAdmin.Domain.SiteBase.Mappables;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers.mall;

namespace jp.co.ivp.ers.mvc
{
    public class ErsSiteSearchModelBase : ErsModelBase, ISiteSearchBaseMappable
    {
        #region サイトID検索用 [Site ID for search]

        /// <summary>
        /// 検索用サイトID [Site ID for search]
        /// </summary>
        [ErsOutputHidden("searchGroup", "input", "lpregist_base", "lpregist_detail", "lpregist_questionnaire")]
        [DisplayName("site_t.id")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, isArray = true)]
        public virtual int[] s_site_id { get; set; }

        /// <summary>
        /// 検索用モールショップ区分 [Mall shop division for search]
        /// </summary>
        public virtual EnumMallShopKbn? s_mall_shop_kbn { get { return null; } }

        /// <summary>
        /// 検索用サイト名 [Site name for search]
        /// </summary>
        public virtual string s_site_name
        {
            get
            {
                if (this.s_site_id != null && this.s_site_id.Length > 0)
                {
                    return ErsMallFactory.ersMallViewServiceFactory.GetErsViewSiteService().GetStringFromIds(this.s_site_id);
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// 検索用サイトリスト [List of site for search]
        /// </summary>
        public virtual List<Dictionary<string, object>> s_site_list
        {
            get
            {
                return ErsMallFactory.ersMallViewServiceFactory.GetErsViewSiteService().SelectAsList(this.s_site_id, this.s_mall_shop_kbn);
            }
        }

        public virtual bool multiple_sites
        {
            get
            {
                return ErsFactory.ersUtilityFactory.getSetup().Multiple_sites;
            }
        }

        public virtual int config_site_id
        {
            get
            {
                return (int)EnumSiteId.COMMON_SITE_ID;
            }
        }

        #endregion
    }
}