﻿using System;
using System.Web.Mvc;
using jp.co.ivp.ers.administrator.function_group;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class RoleCheckAttribute
        :FilterAttribute, IAuthorizationFilter
    {
        #region IAuthorizationFilter メンバー

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var controller_name = filterContext.RouteData.Values["controller"].ToString();
            var method_name = filterContext.RouteData.Values["action"].ToString();

            // search FunctionGroup
            var functionGroupCriteria = ErsFactory.ersAdministratorFactory.GetErsFunctionGroupCriteria();

            functionGroupCriteria.controller_name = controller_name;
            functionGroupCriteria.method_name = method_name;
            functionGroupCriteria.SetActiveOnly();

            var functionGroupRepository = ErsFactory.ersAdministratorFactory.GetErsFunctionGroupRepository();
            var functionGroupList = functionGroupRepository.Find(functionGroupCriteria);
            // no data is no problem.
            if (functionGroupList.Count <= 0)
            {
                return;
            }

            //search RoleGroup
            String action_role = functionGroupList[0].func_id;

            //無条件にアクセス可能な場合はOK
            if (action_role == ErsFunctionGroup.GLOBAL_FUNC_ID)
            {
                return;
            }

            String user_cd = ErsContext.sessionState.Get("user_cd");

            var roleGroupCriteria = ErsFactory.ersAdministratorFactory.GetErsRoleGroupCriteria();
            roleGroupCriteria.role_action = action_role;
            roleGroupCriteria.user_cd = user_cd;
            var userRoleSpecification = ErsFactory.ersAdministratorFactory.GetUserRoleSpecification();
            int countData = userRoleSpecification.GetCountData(roleGroupCriteria);
            // no data is invalid.
            if (countData <= 0)
            {
                throw new ErsException("60002");
            }

            return;
        }

        #endregion
    }
}
