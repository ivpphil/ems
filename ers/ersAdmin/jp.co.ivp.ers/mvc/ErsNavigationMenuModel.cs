﻿using System;
using System.Linq;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc
{
    public class ErsNavigationMenuModel
        : ErsModelBase
    {
        public string current_menu_id { get; protected set; }
        public IList<Dictionary<string, object>> listHeaderNavigation { get; protected set; }
        public IList<Dictionary<string, object>> listMenu { get; protected set; }
        public IList<Dictionary<string, object>> listHeaderMenu { get; protected set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="controllerName">Name of controller</param>
        /// <param name="menuName">Name of menu</param>
        public ErsNavigationMenuModel(string controllerName, string actionName)
        {
            this.listHeaderNavigation = this.SetHeaderNavigationList(null, null);

            var listCurrentNavigation = this.SetHeaderNavigationList(controllerName, actionName);
            if (listCurrentNavigation.Count > 0)
            {
                this.current_menu_id = Convert.ToString(listCurrentNavigation.First()["menu_id"]);
            }

            this.listHeaderMenu = this.SetMenuList(null, null);
            this.listMenu = this.SetMenuList(controllerName, actionName);
        }

        /// <summary>
        /// Set lis of menu
        /// </summary>
        /// <param name="controllerName">Name of controller</param>
        /// <param name="menuName">Name of Menu</param>
        private IList<Dictionary<string, object>> SetMenuList(string controllerName, string actionName)
        {
            var criteria = ErsFactory.ersAdministratorFactory.GetErsFunctionGroupCriteria();
            var spec = ErsFactory.ersAdministratorFactory.GetFunctionNameListSearchSpec();
            
            var user_cd = ErsContext.sessionState.Get("user_cd");

            if (string.IsNullOrEmpty(user_cd))
            {
                return null;
            }

            criteria.SetAuthedFunctionUser_cd(user_cd);
            criteria.SetActiveOnly();

            if (!string.IsNullOrEmpty(controllerName) && !string.IsNullOrEmpty(actionName))
            {
                criteria.SetRequestedAction(controllerName, actionName);
            }
            criteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByFunc_id(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByFunc_name(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            var lstRet = new List<Dictionary<string, object>>();

            var listFind = spec.GetSearchData(criteria);

            foreach (var data in listFind)
            {
                data["func_name"] = ErsResources.GetMessage(Convert.ToString(data["func_name"]));
                lstRet.Add(data);
            }

            return lstRet;
        }

        /// <summary>
        /// Set lis of menu
        /// </summary>
        /// <param name="controllerName">Name of controller</param>
        /// <param name="menuName">Name of Menu</param>
        private IList<Dictionary<string, object>> SetHeaderNavigationList(string controllerName, string actionName)
        {
            var criteria = ErsFactory.ersAdministratorFactory.GetErsFunctionGroupCriteria();
            var spec = ErsFactory.ersAdministratorFactory.GetMenuNameListSearchSpec();

            var user_cd = ErsContext.sessionState.Get("user_cd");

            if (string.IsNullOrEmpty(user_cd))
            {
                return null;
            }

            criteria.SetAuthedMenuUser_cd(user_cd);
            criteria.SetActiveOnly();

            if (!string.IsNullOrEmpty(controllerName) && !string.IsNullOrEmpty(actionName))
            {
                criteria.SetRequestedAction(controllerName, actionName);
            }
            criteria.SetGroupByMenu_id();
            criteria.SetGroupByMenu_name();
            criteria.SetOrderByMinDispOrder(Criteria.OrderBy.ORDER_BY_ASC);

            var lstRet = new List<Dictionary<string, object>>();

            var listFind = spec.GetSearchData(criteria);

            foreach (var data in listFind)
            {
                data["menu_name"] = ErsResources.GetMessage(Convert.ToString(data["menu_name"]));
                lstRet.Add(data);
            }

            return lstRet;
        }
    }
}