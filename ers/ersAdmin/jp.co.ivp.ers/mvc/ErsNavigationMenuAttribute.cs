﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple=false)]
    public class ErsNavigationMenuAttribute
           : FilterAttribute, IActionFilter
    {
        /// <summary>
        /// サイトメニューが0件の場合の権限エラーを発生させない場合はFalseを設定します。
        /// </summary>
        public bool RaiseRoleError { get; set; }

        public ErsNavigationMenuAttribute()
        {
            RaiseRoleError = true;
        }

        #region IActionFilter メンバー

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controllerName = Convert.ToString(filterContext.RouteData.Values["controller"]);
            var actionName = Convert.ToString(filterContext.RouteData.Values["action"]);

            if (string.IsNullOrEmpty(controllerName))
            {
                return;
            }

            ErsNavigationMenuModel sideModel= new ErsNavigationMenuModel(controllerName, actionName);

            if (RaiseRoleError && (sideModel.listMenu == null || sideModel.listMenu.Count == 0))
            {
                throw new ErsException("63106");
            }

            var controller = (ErsControllerBase)filterContext.Controller;

            // サイドメニューモデル
            if (!controller.HasAdditionalModel("sideModel"))
            {
                controller.AddModelToView("sideModel", sideModel);
            }
        }

        #endregion
    }
}