﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.state;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited=true)]
    public class ErsAuthorizationAttribute
        : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var controler = (ErsControllerSecureAdmin)filterContext.Controller;

            EnumUserState state = ((ISession)ErsContext.sessionState).getUserState();

            object[] obj = filterContext.ActionDescriptor.GetCustomAttributes(typeof(NoNeedSessionAttribute), false);
            if (obj.Length == 0)
            {
                if (state != EnumUserState.LOGIN)
                {
                    //ログイン状態をチェックし、ログイン状態でなかったらエラー
                    filterContext.Result = controler.ExecuteError();
                }
            }

            controler.ViewBag.role_gcode = ErsContext.sessionState.Get("role_gcode");
        }
    }
}