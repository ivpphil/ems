﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc
{
    public class ErsAdminProcessCompletionAttribute
        : ErsProcessCompletionAttributeBase
    {
        public ErsAdminProcessCompletionAttribute(string completedPageKeys)
            : base(completedPageKeys)
        {
        }

        public override void OnCheckCompletionError()
        {
            throw new ErsException("10042");
        }
    }
}