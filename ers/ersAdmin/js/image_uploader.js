/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function(){
    var ersObj = ErsImageUploader();
    ersObj.imageUploader();
});




/* ErsCommonオブジェクト生成コンストラクタ */
var ErsImageUploader = function () {

    var that = {};

    /* チェックボックス全選択、全解除
    ---------------------------------------------------------------- */
    that.imageUploader = function () {
        var fired_submit = false;
        var tagFormName;

        (function ($) {

            $.fn.ContainsString = function (value) {
                return (this.value.indexOf(value) > -1);
            };

            $.fn.image_uploader = function () {

            };

            $.fn.InitFileUploader = function (options) {
                $(this).image_uploader(options.url_send_to, options.domain, options.multipart_params, options);
            };

            //custom prototype for to set init plupload and options
            $.fn.image_uploader = function (url, domain, multipart_params, ers_custom_options) {
                //Only if browser support set runtime

                setOtherCustomOptions.setOptions(this, ers_custom_options);
                ers_uploader.init(this, url, domain, multipart_params, ers_custom_options);

            };

            //custom options for viewbox sizes
            var setOtherCustomOptions = {
                setOptions: function (self, ers_custom_options) {
                    var defaultSize = {
                        width: '100%'
                    };

                    if (ers_custom_options.styleOptions) {
                        for (var custom_key in ers_custom_options.styleOptions) {
                            defaultSize[custom_key] = ers_custom_options.styleOptions[custom_key];
                        }
                    }

                    $(self).css('width', defaultSize['width']);
                }
            };

            //Pluploader Init functions
            var ers_uploader = {
                init: function (self, url, domain, multipart_params, ers_custom_options) {

                    SetDefaultErsCustomOptions(ers_custom_options);

                    //this was defined in /plupload-2.1.1/js/jquery.ui.plupload/ui.plupload.templete.js
                    //To customize default plupload options set ers_custom_options.custom_pl_options and then prototype name and value
                    $(self).plupload(ErsPLUploadOptions.getDefaultOptions(self, url, domain, multipart_params, ers_custom_options));
                }
            };

            //Upload all images was inserted in viewboxes
            $.fn.doUpload = function (image_table_id, return_completion) {
                var objForm = $(this)[0];
                tagFormName = objForm.getAttribute('name');

                $.each(image_table_id, function (i, table_id) {
                    if (SetDestroyedAsCompletedStatus(objForm, table_id) == false) {//Only if browser support set runtime
                        if ($('#' + table_id).plupload('getUploader').id) {
                            submit_images.upload(objForm, table_id);
                        }
                    }
                });

                //Check if all files done
                if (CheckAllCompleted(objForm)) {
                    //set hidden error list(not bindable)
                    if (ErsImageValues.getErrorMessageCount() > 0) {
                        ErsImageValues.SetErrorList();
                    }

                    //Set bindable error output hidden
                    ErsImageValues.SetErrorOutputHidden();

                    if (!fired_submit) {
                        fired_submit = true;
                        document[tagFormName].submit();
                    }
                }

                return return_completion;
            }

            //Init declared form tag
            $.fn.initForm = function (image_table_id) {

                if (!$.isArray(image_table_id)) {
                    image_table_id = [image_table_id];
                }

                $.each(this, function (i, form) {
                    var objForm = this;

                    if (!this.process_status) {
                        this.process_status = {};
                    }

                    $.each(image_table_id, function (i, table_id) {
                        if ($('#' + table_id).plupload('getUploader').id) {
                            var uploader = $('#' + table_id).plupload('getUploader');
                            //Set status as processing
                            objForm.process_status[uploader.id] = ErsImageValues.PendingStatus.PROCESSING;
                        }
                    });
                });

                $(this).submit(function () {
                    return $(this).doUpload(image_table_id, false);
                });


            };

            //Perform submit uploaded submitted fules
            var submit_images = {
                upload: function (objForm, image_table_id) {
                    var uploader = $('#' + image_table_id).plupload('getUploader');
                    if (uploader.files.length > 0) {

                        // When all files are uploaded submit form
                        $('#' + image_table_id).on('complete', function () {
                            //Set status when it was done
                            objForm.process_status[uploader.id] = ErsImageValues.PendingStatus.DONE;

                            if (CheckAllCompleted(objForm)) {
                                if (ErsImageValues.getErrorMessageCount() > 0) {
                                    ErsImageValues.SetErrorList();
                                }

                                ErsImageValues.SetErrorOutputHidden();

                                //trigger form submit
                                if (!fired_submit) {
                                    fired_submit = true;
                                    document[tagFormName].submit();
                                }
                            }
                        });

                        //Start uploading images on declared url
                        $('#' + image_table_id).plupload('start');
                    } else {
                        //Set status when it was done
                        objForm.process_status[uploader.id] = ErsImageValues.PendingStatus.DONE;
                    }
                }
            };

            //Check if all files was successfully uploaded
            var CheckAllCompleted = function (objForm) {
                var result = true;

                if (objForm.process_status) {
                    $.each(objForm.process_status, function (key, status) {
                        if (status == ErsImageValues.PendingStatus.PROCESSING) {
                            result = false;
                            return;
                        }
                    });
                }

                return result;
            }

            var SetDestroyedAsCompletedStatus = function (objForm, table_id) {
                var result = false;

                if (objForm.process_status) {
                    $.each(ErsImageValues.destroyedUploader, function (key, uploader_id) {
                        if (key == table_id) {
                            objForm.process_status[uploader_id] = ErsImageValues.PendingStatus.DONE;
                            result = true;
                            return;
                        }
                    });
                }

                return result;
            }

        })(jQuery);

        //Set Custom Ers Options images uploader
        function SetDefaultErsCustomOptions(ers_custom_options) {
            if (!ers_custom_options.error_field_name) {
                ers_custom_options.error_field_name = ErsImageValues.DEFAULT_ERROR_FIELD;
            }

            if (!ers_custom_options.function_group) {
                ers_custom_options.function_group = {};
            }

            if (!ers_custom_options.onRefreshViewBox) {
                ers_custom_options.onRefreshViewBox = ErsImageValues.IsBrowserIE();
            }

            if (!ers_custom_options.notToErsValidExt) {
                ers_custom_options.notToErsValidExt = false;
            }
        }
    }

    return that;


}

//Support Library and Functions
/* ErsCommonオブジェクト生成コンストラクタ */
var GlobalImageEntryValues = function () {

    var that = {};

    var uploadErrorMessageList = [];

    var pluploader_bindable_err = [];

    that.DEFAULT_ERROR_FIELD = "api_errorList";

    that.PendingStatus = { PROCESSING: 0, FAILED: 1, DONE: 200 };

    that.destroyedUploader = {};

    that.IsDestroyedUploader = function (table_id) {
        var result = false;

        $.each(ErsImageValues.destroyedUploader, function (key, uploader_id) {
            if (key == table_id) {
                result = true;
                return;
            }
        });

        return result;
    }

    that.IsBrowserIE = function () {

        if (o && o.Env) {
            return o.Env.browser.toLocaleLowerCase() === 'ie';
        } else if ($.browser && $.browser.msie) {
            return ($.browser.msie);
        }

        return ErsLib.isIE() || ErsLib.getBrowser() === "Other";
    }

    //upload all images that completed in pending onload by with onDone sequence
    that.uploadAllCompleted = function (uploader) {
        if (uploader && uploader.pending_onload) {
            var pending_onload = uploader.pending_onload;
            for (var key in pending_onload) {
                if (pending_onload[key].status == that.PendingStatus.DONE)
                    that.AddFilesOnUploader(uploader, pending_onload[key].blob);
            }
        }
    }

    //Upload all images that completed in pending onload by with sequence no
    that.uploadAllCompletedBySequence = function (uploader) {
        if (uploader && uploader.pending_onload) {
            var pending_onload = uploader.pending_onload;
            var keysCount = Object.keys(pending_onload).length;

            for (var count = 0; keysCount > count; count++) {
                var imgObj = that.getPendingImgObjWithSequence(count, pending_onload);

                if (imgObj && imgObj.status == that.PendingStatus.DONE) {
                    that.AddFilesOnUploader(uploader, imgObj.blob);
                }

                if (imgObj && (imgObj.status == that.PendingStatus.PROCESSING || imgObj.status == that.PendingStatus.FAILED)) {
                    imgObj.status = that.PendingStatus.FAILED;
                    uploader.trigger('Error', { code: plupload.FILE_UPLOADING_FAILED, message: _('File Uploading Failed.'), src: imgObj.img_src });
                }
            }
        }
    }

    //Get the Pending Image obj with sequence no
    that.getPendingImgObjWithSequence = function (sequence_no, pending_onload) {
        for (var key in pending_onload) {
            if (parseInt(pending_onload[key].sequence_no, 10) === parseInt(sequence_no, 10))
                return pending_onload[key];
        }

        return null;
    }

    //Add and then upload the image blob obj
    that.AddFilesOnUploader = function (uploader, imgBlobObj) {
        uploader.addFile(imgBlobObj);
    }

    //Check if all pending images has been completed
    that.IsAllPendingCompleted = function (uploader) {
        if (uploader && uploader.pending_onload) {

            for (var sequence_no in uploader.valid_images) {
                var img = that.getPendingImgObjWithSequence(sequence_no, uploader.pending_onload);
                var valid_img = uploader.valid_images[sequence_no];

                if (!img)
                    return false;

                if (img.status !== that.PendingStatus.DONE || valid_img.status !== that.PendingStatus.DONE)
                    return false;
            }

            return true;
        }

        return false;
    }

    //create and get new instance of moxie Image and set onload function
    that.createNewInstanceOnload = function (uploader) {
        var img = new mOxie.Image();

        //Set uploader container and set images as currently pending
        //Set status of completion ErsImageValues.PendingStatus
        img.uploader = uploader;
        img.uploader.pending_onload[img.uid] = img;
        img.uploader.pending_onload[img.uid].status = that.PendingStatus.PROCESSING;

        //get and set onload functions
        img.onload = onAddPluploader;
        return img;
    }

    //Checking the valid uploaded files(not bindable)
    that.IsValidAddedFile = function (pluploader, blob_file_name) {
        var pl_files = pluploader.getOption().ers_custom_options.add_files;

        for (var count = 0; count < pl_files.length; count++) {
            if (pl_files[count].indexOf(blob_file_name) != -1)
                return true;
        }

        return false;
    }

    //Added error messages(not bindable)
    that.AddErrorMessageList = function (pluploader, err_message) {
        err_message = err_message.replace('下記エラーをご確認下さい。', '').replace('<br>', '')
        uploadErrorMessageList.push(err_message.replace('正しくありません。', pluploader.getOption().field_name));
    }

    //Added error messages(not bindable)
    that.getErrorMessageCount = function () {
        return uploadErrorMessageList.length;
    }

    //Clear error messages(not bindable)
    that.ClearErrorMessageList = function () {
        uploadErrorMessageList = [];
    }

    //set hidden error messages for bindable
    that.SetErrorList = function () {
        $('input[name="' + pl_options.ers_custom_options.error_field_name + '"]').val(uploadErrorMessageList);
    }

    //Set bindable error messages
    that.SetBindableErrorMessage = function (pluploader, err_message, index_file) {
        if (!pluploader.getOption().ers_custom_options.bindable_error) {
            pluploader.getOption().ers_custom_options.bindable_error = {};
        }

        if (!pluploader.getOption().ers_custom_options.bindable_error[index_file]) {
            pluploader.getOption().ers_custom_options.bindable_error[index_file] = [];
        }

        pluploader.getOption().ers_custom_options.bindable_error[index_file].push(err_message);
        pluploader_bindable_err.push(pluploader);
    }

    //Add image object file
    that.AddImageFile = function (url) {
        var AddImageFile = new Image();

        AddImageFile.src = encodeURI(url);

        return AddImageFile;
    }

    //Add files builder
    that.AddFilesBuilder = function (add_files) {
        var validUrls = [];
        var add_urls = add_files.split(',')

        for (var count = 0; count < add_urls.length; count++) {
            var url = add_urls[count];
            var urlinfo = ErsUrlFileInfo.Init({ source: url });
            if (urlinfo.filename) {
                validUrls.push(url);
            }
        }

        return validUrls;
    }

    //Set status of image that is ready to onload
    that.SetStatusValidImage = function (uploader, added_files) {
        uploader.valid_images = {};

        for (var count = 0; count < added_files.length; count++) {
            uploader.valid_images[count] = {};
            uploader.valid_images[count].img_src = added_files[count];
            uploader.valid_images[count].status = that.PendingStatus.PROCESSING;
        }
    }

    //Add Valid Files as Blob Object and trigger callback function
    that.AddAsBlob = function (callback_param, callback_add_file) {
        var fileinfo;

        var callonload = function () {
            callback_param.fileinfo = this;

            if (this.response_status == 200) {
                if (this.isImage) {

                    var ImageFile = new Image();

                    ImageFile.onload = function () {
                        callback_add_file(this, callback_param);
                    }

                    //Convert blob to base64
                    var reader = new FileReader();
                    reader.readAsDataURL(this);
                    reader.onloadend = function () {
                        ImageFile.src = reader.result;
                    }

                    //Called callback function when image is loaded
                    if (ImageFile.naturalWidth && ImageFile.naturalWidth > 0) {
                        ImageFile.onload = null;
                        callback_add_file(ImageFile, callback_param);
                    }
                } else {
                    callback_add_file(this, callback_param);
                }
            }
        }

        var callonerror = function () {
            callback_param.uploader.trigger('Error', { code: plupload.FILE_UPLOADING_FAILED, message: _('File Uploading Failed.'), src: this.source });
        }

        if (ErsUrlFileInfo.IsSupportedAsBlob()) {
            fileinfo = ErsUrlFileInfo.Init({
                source: callback_param.url,
                onloadinfo: callonload,
                onerrorinfo: callonerror,
                deletestring: callback_param.file_identifier
            }, true);
        }
    }

    //Remove File indetifier
    that.RemoveFileIdentifier = function (file_name, file_identifier) {
        return file_name.replace(new RegExp(file_identifier, 'gi'), '');
    }

    //Refresh all files removed status of 5(DONE)
    that.RefreshPluploader = function (objUploader) {
        if (objUploader != undefined) {
            objUploader.refresh();

            if (objUploader.files.length > 0) {
                for (var count = 0; count < objUploader.files.length; count++) {
                    var file = objUploader.files[count];
                    file.status = 1;
                }
            }
        }
    }

    //SetOutput hidden in custom object form
    that.OutputHiddenForm = function (plupload, fields) {
        var options = (plupload.options) ? plupload.options : plupload.getOption();

        if (options.ers_custom_options.outputhidden_form.OBJFORM) {
            var object_form = options.ers_custom_options.outputhidden_form.OBJFORM;
            var object_type = ErsImageValues.TypeOfForm(object_form);

            if (object_type === 'string') {
                var forms = object_form.split(',');

                for (var count = 0; count < forms.length; count++) {
                    $(forms).append(fields);
                }
            } else if (object_type === 'object') {
                $(object_form).each(function () {
                    $(this).append(fields);
                });

            } else if (object_type === 'array') {

                for (var count = 0; count < object_form.length; count++) {
                    $(object_form).append(fields);
                }
            }

        } else if (options.ers_custom_options.outputhidden_form.ALL) {
            $('form').each(function () {
                $(this).append(fields);
            });
        }
    }

    //Determine type of object
    that.TypeOfForm = function (object_form) {
        var object_type = typeof (object_form);

        if (object_type === 'string') {
            return 'string';
        } else if ($.isArray(object_form)) {
            return 'array';

        } else if (object_type === 'object') {
            return 'object';
        }

        return object_type;
    }

    //Set all error in Output hidden for bindable
    that.SetErrorOutputHidden = function () {
        for (var err_index in pluploader_bindable_err) {
            var pluploader_err = pluploader_bindable_err[err_index];
            var pl_options = pluploader_err.getOption();
            var outputhidden_all = false;

            if (pl_options.ers_custom_options.bindable_error) {

                $.each(pl_options.ers_custom_options.bindable_error, function (err_key, err_messages) {

                    $.each(err_messages, function (r_count, err_message) {

                        var input_error = $('<input>').attr({
                            type: 'hidden',
                            name: pl_options.ers_custom_options.record_key + "_" + err_key + "_" + pl_options.ers_custom_options.error_field_name,
                            value: err_message
                        });

                        if (pl_options.ers_custom_options.outputhidden_form) {
                            outputhidden_all = (pl_options.ers_custom_options.outputhidden_form.ALL);
                            ErsImageValues.OutputHiddenForm(pluploader_err, input_error.clone());
                        }

                        if (!outputhidden_all) {
                            $('#' + pluploader_err.files[r_count].id).find('.plupload_file_fields').append(input_error.clone());
                        }


                    });
                });

            }
        }
    }

    //Bind Refresh ViewBox「Usually use for IE browser」
    that.BindRefreshViewBox = function (uploader) {
        uploader.bind('onRefreshViewBox', ErsImageValues.RefreshViewBox);
    }

    //Trigger Refreshing ViewBox「Usually use for IE browser」
    that.RefreshViewBox = function (event, view_box) {
        if (!view_box) {
            view_box = this.getOption().ers_custom_options.function_group.ViewBox;
        }

        var ers_custom_options = view_box.options.ers_custom_options;
        if (ers_custom_options.onRefreshViewBox) {
            view_box._displayThumbs();
            view_box.element.trigger('selected');
        }
    }

    //Excute functions that upload the Images in Viewbox
    that.UploaderViewBoxExecutor = function (uploader) {

        if (uploader) {

            if (uploader.timer_id) {
                clearTimeout(uploader.timer_id);
            }

            ErsImageValues.uploadAllCompletedBySequence(uploader);
            ErsImageValues.BindRefreshViewBox(uploader);
        }
    }

    return that;
};

//Use to call support library function
var ErsImageValues = GlobalImageEntryValues();

//Creating clone object
var clone = (function () {
    return function (obj) { Clone.prototype = obj; return new Clone() };
    function Clone() { }
} ());

//onload for add files
onAddPluploader = function () {

    var blob;

    if (this.blob && this.blob.isImage === false) {
        blob = this;
        this.fileinfo = this.blob;

        if (this.p_uid) {
            this.uploader.pending_onload[this.uid] = this.uploader.pending_onload[this.p_uid];
            delete this.uploader.pending_onload[this.p_uid];
        }

    } else {
        blob = this.getAsBlob();
    }

    blob.name = this.fileinfo.filename;

    this.uploader.pending_onload[this.uid].status = ErsImageValues.PendingStatus.DONE;
    this.uploader.pending_onload[this.uid].blob = blob;

    if (ErsImageValues.IsAllPendingCompleted(this.uploader)) {
        ErsImageValues.UploaderViewBoxExecutor(this.uploader);
    }
};

var ErsUrlFileInfo = {

    Init: function (s, getBlob) {

        function InstanceSetup() {
            var setup = new Object();
            $.each(ErsUrlFileInfo._defaultInfo, function (key, value) {
                setup[key] = value;
            });

            return setup;
        }

        var g = new InstanceSetup();

        if (s) {
            g.source = s.source;
            g.onloadinfo = s.onloadinfo;
            g.onerrorinfo = s.onerrorinfo;
        }

        if (g.source) {
            var source = g.source;
            if (g.source.indexOf('?') >= 0) {
                var cc = g.source.split('?');
                source = source.replace(new RegExp(cc[1], 'g'), '');
            }

            var fileinfoexp = new RegExp('([^\\\\/]+\\.(\\w+))$', 'gi');
            var match = fileinfoexp.exec(source);

            if (match) {
                g.filename = match[1];
                g.originalname = g.filename;

                if (s.deletestring) {
                    g.filename = g.filename.replace(new RegExp(s.deletestring, 'gi'), '');
                }

                g.extension = match[2];
                g.noext = g.filename.replace('.' + g.extension, '');
                g.isImage = ErsUrlFileInfo.TypeIsErsImage(g.filename);
            }

            if (ErsUrlFileInfo.IsSupportedAsBlob()) {
                if (getBlob) {

                    var objReq = new XMLHttpRequest();
                    objReq.open("GET", g.source, true);
                    objReq.responseType = "blob";

                    objReq.onload = function (event) {
                        if (objReq.response) {
                            var blob = objReq.response;
                            g.blob = objReq.response;
                            g.size = g.blob.size;
                            g.type = g.blob.type;
                            g.response_status = objReq.status;

                            $.each(g, function (key, value) {
                                blob[key] = value;
                            });

                            if (blob.response_status === 200) {
                                if (blob.onloadinfo)
                                    blob.onloadinfo();
                            } else {
                                if (blob.onerrorinfo) {
                                    blob.onerrorinfo();
                                }
                            }
                        }
                    }

                    objReq.send();
                }
            }
        }

        return g;
    },

    _defaultInfo: {
        source: null,
        filename: null,
        originalname: null,
        noext: null,
        extension: null,
        blob: null,
        size: null,
        type: null,
        onloadinfo: null,
        onerrorinfo: null,
        isImage: null,
        response_status: 200
    },

    TypeIsErsImage: function (filename, ce) {
        if (filename) {
            var ext = ce || ErsPLUploadOptions.ERS_SUPPORT_IMAGE;
            var validImages = ext.replace(new RegExp(',', 'gi'), '|');

            var imgexp = new RegExp('\\.(' + validImages + ')', 'gi');
            var match = imgexp.exec(filename);
            if (match)
                return true;
        }

        return false;
    },

    TypeIsImage: function (filename) {
        var i = 'jpg|jpeg|gif|bmp|png|exif|tiff|rif|ppm|pgm|pnm|bpg|webp';
        return ErsUrlFileInfo.TypeIsErsImage(filename, i);
    },

    BrowserType: function () {
        var p = {
            browser: '',
            version: null
        };

        if (o && o.Env) {
            $.each(o.Env, function (key, value) {
                p[key] = value;
            });
        }

        return p;
    },

    IsSupportedAsBlob: function () {
        if (this.BrowserType().browser.toLowerCase() === 'ie') {
            return parseInt(this.BrowserType().version) > 9;
        }

        return true;
    },

    JP_FIELD_NAME: 'ファイル名：'
};
