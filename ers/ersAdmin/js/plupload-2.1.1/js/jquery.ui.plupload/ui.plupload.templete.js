var PLUPLOAD_TEMPLETE =
{
    VDEFAULT: 'Default',
    V1: 'V1',
    V2: 'V2'
}

function _(str) {
    return plupload.translate(str) || str;
}

function renderUITemplete(obj) {
    var options = obj.plupload('option');

    if(options.ers_custom_options.plupload_templete){
        window["renderUITemplate" + options.ers_custom_options.plupload_templete](obj);
    }else{
        this.renderUITemplateDefault(obj);
    }
}

function renderUITemplateDefault(obj) {
    obj.id = obj.attr('id');
    var options = obj.plupload('option');

    obj.html(
		'<div class="plupload_wrapper">' +
            this.AddDummyAutoFocus(obj) +
			'<div class="ui-widget-content plupload_container">' +
				'<div class="ui-state-default ui-widget-header plupload_header">' +
					'<div class="plupload_header_content">' +
						'<div class="plupload_view_switch">' +
							'<input type="radio" id="' + obj.id + '_view_list" name="view_mode_' + obj.id + '" checked="checked" /><label class="plupload_button" for="' + obj.id + '_view_list" data-view="list">' + _('List') + '</label>' +
							'<input type="radio" id="' + obj.id + '_view_thumbs" name="view_mode_' + obj.id + '" /><label class="plupload_button"  for="' + obj.id + '_view_thumbs" data-view="thumbs">' + _('Thumbnails') + '</label>' +
						'</div>' +
					'</div>' +
				'</div>' +

				'<table class="plupload_filelist plupload_filelist_header ui-widget-header">' +
				'<tr>' +
					'<td class="plupload_cell plupload_file_name">' + _('Filename') + '</td>' +
					'<td class="plupload_cell plupload_file_status">' + _('Status') + '</td>' +
					'<td class="plupload_cell plupload_file_size">' + _('Size') + '</td>' +
					'<td class="plupload_cell plupload_file_action">&nbsp;</td>' +
				'</tr>' +
				'</table>' +

				'<div class="plupload_content">' +
					'<div class="plupload_droptext">' + _("Drag files here.") + '</div>' +
					'<ul class="plupload_filelist_content"> </ul>' +
					'<div class="plupload_clearer">&nbsp;</div>' +
				'</div>' +

				'<table class="plupload_filelist plupload_filelist_footer ui-widget-header">' +
				'<tr>' +
					'<td class="plupload_cell plupload_file_name">' +
						'<div class="plupload_buttons"><!-- Visible -->' +
							'<a class="plupload_button plupload_add">' + _('Add Files') + '</a>&nbsp;' +
						'</div>' +

						'<div class="plupload_started plupload_hidden"><!-- Hidden -->' +
							'<div class="plupload_progress plupload_right">' +
								'<div class="plupload_progress_container"></div>' +
							'</div>' +

							'<div class="plupload_cell plupload_upload_status"></div>' +

							'<div class="plupload_clearer">&nbsp;</div>' +
						'</div>' +
					'</td>' +
					'<td class="plupload_file_status"><span class="plupload_total_status">0%</span></td>' +
					'<td class="plupload_file_size"><span class="plupload_total_file_size">0 kb</span></td>' +
					'<td class="plupload_file_action"></td>' +
				'</tr>' +
				'</table>' +

			'</div>' +
			'<input class="plupload_count" value="0" type="hidden">' +
		'</div>'
	);
}

renderUITemplateV1 = function (obj) {
    obj.id = obj.attr('id');

    obj.html(
		'<div class="plupload_wrapper plupload_wrapper_v1">' +
            this.AddDummyAutoFocus(obj) +
			'<div class="ui-widget-content plupload_container plupload_container_v1">' +
				'<div class="ui-state-default ui-widget-header plupload_header">' +
					'<div class="plupload_header_content">' +
                         '<div class="plupload_file_status_action_v1">' +
                        '<span class="plupload_total_status">0%</span>' +
                        '<span class="plupload_total_file_size">0 kb</span>' +
                        '</div>' +
                        '<div class="plupload_buttons plupload_buttons_top_left_v1"><!-- Visible -->' +
							'<a class="plupload_button plupload_add">' + _('Add Files') + '</a>&nbsp;' +
						'</div>' +
						'<div class="plupload_view_switch">' +
							'<input type="radio" id="' + obj.id + '_view_list" name="view_mode_' + obj.id + '" checked="checked" /><label class="plupload_button" for="' + obj.id + '_view_list" data-view="list">' + _('List') + '</label>' +
							'<input type="radio" id="' + obj.id + '_view_thumbs" name="view_mode_' + obj.id + '" /><label class="plupload_button"  for="' + obj.id + '_view_thumbs" data-view="thumbs">' + _('Thumbnails') + '</label>' +
						'</div>' +
					'</div>' +
				'</div>' +

				'<table class="plupload_filelist plupload_filelist_header ui-widget-header">' +
				'<tr>' +
					'<td class="plupload_cell plupload_file_name">' + _('Filename') + '</td>' +
					'<td class="plupload_cell plupload_file_status">' + _('Status') + '</td>' +
					'<td class="plupload_cell plupload_file_size">' + _('Size') + '</td>' +
					'<td class="plupload_cell plupload_file_action">&nbsp;</td>' +
				'</tr>' +
				'</table>' +

				'<div class="plupload_content plupload_content_v1">' +
					'<div class="plupload_droptext plupload_droptext_v1">' + _("Drag files here.") + '</div>' +
					'<ul class="plupload_filelist_content"> </ul>' +
					'<div class="plupload_clearer">&nbsp;</div>' +
				'</div>' +

				'<table class="plupload_filelist plupload_filelist_footer ui-widget-header">' +
				'<tr>' +
					'<td class="plupload_cell plupload_file_name">' +
						

						'<div class="plupload_started plupload_hidden"><!-- Hidden -->' +
							'<div class="plupload_progress plupload_right">' +
								'<div class="plupload_progress_container"></div>' +
							'</div>' +

							'<div class="plupload_cell plupload_upload_status"></div>' +

							'<div class="plupload_clearer">&nbsp;</div>' +
						'</div>' +
					'</td>' +
					'<td class="plupload_file_action"></td>' +
				'</tr>' +
				'</table>' +

			'</div>' +
			'<input class="plupload_count" value="0" type="hidden">' +
		'</div>'
	);
}

function AddDummyAutoFocus(obj) {
    var options = obj.plupload('option');

    if (options.ers_custom_options.dummy_autofucos) {
        return '<input type="hidden" autofocus="autofocus" />';
    }

    return '';
}


var PLUploadOptions = function () {

    var that = {};

    that.ERS_SUPPORT_IMAGE = 'jpg,jpeg,gif,bmp,png,ping';

    that.ERS_SUPPORT_DOC = 'pdf,js,css,doc,xls,txt';

    that.defaultOptions = function (self, url, domain, multipart_params, ers_custom_options) {
        return {
            // General settings
            runtimes: 'html5,flash,silverlight,html4',
            url: url,
            field_name: $(self).attr('id'),
            ers_custom_options: ers_custom_options,

            // User can upload no more then 20 files in one go (sets multiple_queues to false)
            //max_file_count: 10,
            multiple_queues: true,

            chunk_size: 0,

            // Resize images on clientside if we can
            //resize: {
            //width: 200,
            //height: 200,
            // quality: 90,
            //crop: true // crop to exact dimensions
            // },

            multipart: true,
            multipart_params: multipart_params, // adding this to keep consistency across the runtimes

            filters: {
                // Maximum file size
                max_file_size: that.getCustomOptions('max_file_size', ers_custom_options, 0),
                // Specify what files to browse for
                mime_types: [
				{ title: that.getFiltersMimeTitle(ers_custom_options), extensions: that.getFiltersExtensions(ers_custom_options) }
			]
            },

            // Rename files by clicking on their titles
            rename: false,

            // Sort files
            sortable: true,

            // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
            dragdrop: true,

            // Views to activate
            views: {
                list: true,
                thumbs: true, // Show thumbs
                active: 'thumbs'
            },

            // Flash settings
            flash_swf_url: domain + 'js/plupload-2.1.1/js/Moxie.swf',

            // Silverlight settings
            silverlight_xap_url: domain + 'js/plupload-2.1.1/js/Moxie.xap'
        }
    };


    that.getDefaultOptions = function (self, url, domain, multipart_params, ers_custom_options) {
        var default_options = that.defaultOptions(self, url, domain, multipart_params, ers_custom_options);

        if (ers_custom_options.custom_pl_options) {
            for (var key in ers_custom_options.custom_pl_options) {
                default_options[key] = ers_custom_options.custom_pl_options[key];
            }
        }

        return default_options;
    }

    that.getFiltersExtensions = function (ers_custom_options) {
        if (ers_custom_options['filters_extensions'] && $.trim(ers_custom_options['filters_extensions']) !== '*') {
            return ers_custom_options['filters_extensions'];
        }

        var extra_filters_extension = '';
        if (ers_custom_options['extra_filters_extensions']) {
            extra_filters_extension = ers_custom_options['extra_filters_extensions'];

            if (extra_filters_extension.substring(0, 1) !== ',' && extra_filters_extension.length > 0) {
                extra_filters_extension = ',' + extra_filters_extension;
            }
        }

        return that.ERS_SUPPORT_IMAGE + ',' + that.ERS_SUPPORT_DOC + extra_filters_extension;
    }

    that.getFiltersMimeTitle = function (ers_custom_options) {
        if (ers_custom_options['filters_title']) {
            return ers_custom_options['filters_title'];
        }

        return 'Image files';
    }

    that.getCustomOptions = function (key, custom_options, defaultValue) {

        if (key && custom_options && key in custom_options) {
            return custom_options[key];
        }

        if (key && custom_options && custom_options.custom_pl_options && key in custom_options.custom_pl_options) {
            return custom_options.custom_pl_options[key];
        }

        return defaultValue;
    }

    return that;
};

var ErsPLUploadOptions = PLUploadOptions();