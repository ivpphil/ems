﻿var mouse_is_inside = false;
var ctr = 0;
var ctrCond = 0;
var idSelected = "";
var isSelected = false;
var isCondSelected = false;
var selectedExpression = false;
var isCondTypeSelected = false;
var isCondTypeMainSelected = false;
var idColFunction = "";
//////added margerie
var spanId = 0;
var mySpanIds = new Array();
var mySpanColumnIds = new Array();
var idCon = "";
var idConVal = "";
var tn = "";
var fn = "";
var ntype = "";
var ntypeID = 0;
var value1 = "";
var value2 = "";
var pressEnter = "";
var operatorNum = 1;
var CondTypeSelectedName = "";
var isNullClicked = false;
var iscolSortingPop = false;
var currentAggregate = "";
var firstVal = "";
var condHtml = "";
var idSortSelected = 0;
var hasValue=false;
var hasValue1=false;
////end
var isErrorCond = false;
var documentCondClick = false;


function displayNone(attr) {
    $(attr).css('display', 'none');
}

function styleNone(attr) {
    $(attr).attr('style', "");
}

function displayInline(attr) {
    $(attr).css('display', 'inline');
}

function toggle(offset, attr) {

    var top = offset.top + 18;
    $(attr).attr('style', "left:" + offset.left + "px;top:" + top + "px");
    $(attr).toggle();

}

function bckImageNone(attr, isNone) {
    if (isNone)
        $(attr).attr('style', "background-image: none");
    else
        $(attr).attr('style', "");
}

function notEditBox(e) {
    if (isSelected == true) {

        displayNone('input:text.ers-qc-ce-editbox');
        displayNone('input:text.ers-qc-ve-editbox');

        var changeValue = $('#editText_' + idSelected).val();

        $('a#' + idSelected).text(changeValue);

        editText(e);

    }
}

function checkType(event) {
    var idSelected = event.target.parentElement.children[0].id;

    var idName = event.target.id

    idName = idName.substring(0, 13);
    //"editText_" + idSelected;
    if (idName == "editText_cond") {
        editTextCondition(event, idSelected);
    }
    else {
        editText(event);
    }
    jsoncreateQuery();
}



function getValueFunction(name) {
    var result = 0;

    if (name == "Sum") result = 1;
    else if (name == "Count") result = 2;
    else if (name == "Average") result = 3;
    else if (name == "Minimum") result = 4;
    else if (name == "Maximum") result = 5;

    return result;
}


function itemDevCondition(e) {
    var myName = idConVal;
    var contypenum = 1;

    if (e.target.id == "") operatorNum = operatorNum;
    else operatorNum = e.target.id;

    //if ((this.textContent == "all") || (this.textContent == "any") || (this.textContent == "none") || (this.textContent == "not all"))
    if (isCondTypeMainSelected == true) {
        myName = e.target.textContent;
        idConVal = myName;
    }

    contypenum = myFunction(myName);

    var myValue = ""; var myIntValue = 0;

    if (myName == "all") { myValue = "and"; }
    else if (myName == "any") { myValue = "or"; myIntValue = 1; }

    //        if (pressEnter == "yes") {
    if (isCondTypeMainSelected == true) {
        var changeValue = e.target.innerHTML;
        $("#" + idSelected).text(changeValue);
        $("#condTypeMainPop").toggle();
        $("span.ers-qp-condition-conjuction").html(myValue);
        isCondTypeMainSelected = false;

        var idArr = [];

        var trs = document.getElementsByTagName("td");
        var idNo = 1;

        for (var i = 0; i < trs.length; i++) {
            idArr.push(trs[i].id);

            var elemName1 = "div_" + idNo;

            var elem1 = document.getElementById(elemName1);
            elem1.parentNode.removeChild(elem1);

            var HTML7 = '<div id="div_' + idNo + '"><input type="hidden" id="conditiontypename" name="record_key1_' + idNo + '_conditiontypename" value="' + contypenum + '" /></div>';
            $('td#td_' + trs[i].id).append(HTML7);

            idNo++;
        }

        jsoncreateQuery();

    }
    //        }

    var splitId = idSelected.split('_');

    var parentFields = document.getElementById("fields_" + splitId[1]);
    if (parentFields != null) {
        fn = parentFields.children[0].defaultValue;
        tn = parentFields.children[1].defaultValue;
        ntypeID = parentFields.children[2].defaultValue;

        myNewType(ntypeID);

        removeElement('fields_' + splitId[1]);
        myFields(splitId[1], "true", splitId[1]);
    }
    var myTextContent = e.target.textContent;

    var parent = document.getElementById("entCond_" + splitId[1]);
    var child = document.getElementById(parent.children[2].id);
    var grandChild = child.children[0].id;

    var splitedName = grandChild.split('_');

    if (myTextContent == "is null") {
        $('a#' + grandChild).attr("style", "display:none");
        $('span#spanDate_' + splitedName[1] + "_" + splitedName[2]).attr("style", "display:none");
        $('a#condDate_' + splitedName[1] + "_" + splitedName[2]).attr("style", "display:none");
        $('a#' + grandChild).text('[enter value]');
        $('a#condDate_' + splitedName[1] + "_" + splitedName[2]).text('[enter value]');
        document.getElementById('editText_condDate_' + splitedName[1] + '_' + splitedName[2]).value = "";
        document.getElementById('editText_cond_' + splitedName[1] + '_' + splitedName[2]).value = "";
        isNullClicked = true;
    }
    else if (myTextContent == "between") {
        if (isNullClicked == true) $('a#' + grandChild).attr("style", "display:inline-block");

        $('span#spanDate_' + splitedName[1] + "_" + splitedName[2]).attr("style", "display: inline-block");
        $('a#condDate_' + splitedName[1] + "_" + splitedName[2]).attr("style", "display: inline-block");
    }
    else {
        if (isCondTypeMainSelected == false) {
            $('a#' + grandChild).attr("style", "display: inline-block");

            $('span#spanDate_' + splitedName[1] + "_" + splitedName[2]).attr("style", "display:none");
            $('a#condDate_' + splitedName[1] + "_" + splitedName[2]).attr("style", "display:none");

            document.getElementById('editText_condDate_' + splitedName[1] + '_' + splitedName[2]).value = "";
            $('a#condDate_' + splitedName[1] + "_" + splitedName[2]).text('[enter value]');
        }
    }

    jsoncreateQuery();

}

function myFunction(myString) {
    var contypenum = 0;
    if ((myString == "all") || (myString == "aAll")) contypenum = 0;
    else if ((myString == "any") || (myString == "aAny")) contypenum = 1;
    else if ((myString == "none") || (myString == "aNone")) contypenum = 2;
    else if ((myString == "not all") || (myString == "aNotAll")) contypenum = 3;
    return contypenum;
}


function appendHTML2(id, name, nameThis) {
    ///margerie

    var getCount = document.getElementById("conditioncount").value;
    if (getCount != "0") ctrCond = getCount;
    ctrCond++;


    spanId = ctrCond;

    var HTML1 = '<div class="ers-qp-row ers-qp-row-condition" id="entCond_' + ctrCond + '">'; //Condition groupings
    //var HTML1 = '<tr id="divCon_' + ctrCond + '"><td id="divCon_td_' + ctrCond + '"><div class="ers-qp-row ers-qp-row-condition" id="entCond_' + ctrCond + '">'; //Condition groupings

    var HTMLConj = '';
    if (ctrCond > 1) {
        if (idCon == "aAll") {
            HTMLConj = '<span id="conjuction_' + ctrCond + '_' + id + '" name="conjuction" class="ers-qp-condelement ers-qp-condition-conjuction">and</span>';
        }
        else if (idCon == "aAny") {
            HTMLConj = '<span id="conjuction_' + ctrCond + '_' + id + '" name="conjuction" class="ers-qp-condelement ers-qp-condition-conjuction">or</span>';
        }
        else if ((idCon == "aNone") || (idCon == "aNotAll")) {
            HTMLConj = '<span id="conjuction_' + ctrCond + '_' + id + '" name="conjuction" class="ers-qp-condelement ers-qp-condition-conjuction"></span>';
        }
    }

    mySpanIds.push("conjuction_" + ctrCond + "_" + id);

    var HTML2 = HTMLConj + '<div class="ers-qp-condelement ers-qp-attrelement" id="' + fn + '-' + ntype + '">';
    var HTML3 = '<a href="javascript:void(0)"  id="expCond_' + ctrCond + '_' + id + '" class="selectCondExp">' + name + '</a></div></div>';
    //var HTML3 = '<a href="javascript:void(0)"  id="expCond_' + ctrCond + '_' + id + '" class="selectCondExp">' + name + '</a></div></div></td></tr>';

    var HTMLinsert = HTML1 + HTML2 + HTML3;
    $('#addValCon').append(HTMLinsert);
    //$('#conditionTable').append(HTMLinsert);

    condHtml = HTMLinsert;

    var HTML1_ = '<div id="operelement_' + ctrCond + '" class="ers-qp-condelement ers-qp-operelement">';

    var HTML2_ = myCondTYpe(ctrCond, id, "false");

    var HTML1 = '<div id="valueelement_' + ctrCond + '" class="ers-qp-condelement ers-qp-valueelement">';

    var HTML2 = '<a href="javascript:void(0)" style="display: inline-block;" class="conditionValue" id="cond_' + ctrCond + '_' + id + '">[enter value]</a>';
    HTML2 = HTML2 + '<input class="ers-qp-ve-editbox" id="editText_cond_' + ctrCond + '_' + id + '" type="text" style="display: none;">';
    var HTML3 = '<span id="spanDate_' + ctrCond + '_' + id + '" style="display:none">&nbsp;and&nbsp;</span><a href="javascript:void(0)" style="display:none;" class="conditionValueDate" id="condDate_' + ctrCond + '_' + id + '">[enter value]</a>';
    HTML3 = HTML3 + '<input class="ers-qp-ve-editbox" id="editText_condDate_' + ctrCond + '_' + id + '" type="text" style="display: none;"></div>';
    var HTML4 = '<div class="ers-qp-condition-buttonsBlock" id="butBlck_' + ctrCond + '_' + id + '">';
    var HTML5 = '<div class="ers-qp-condition-button ers-qp-condition-button-enable enabled ers-qp-condition-button" id="condition-button-type_' + ctrCond + '_" title="Toggle Enable" style="background-image: none;"></div>';
    var HTML6 = '<div class="ers-qp-condition-button ers-qp-condition-button-delete ers-qp-condition-button" id="condition-button-delete_' + ctrCond + '_" title="Delete" style="background-image: none;"></div></div></div>';
    var HTMLinsert = HTML1_ + HTML2_ + HTML1 + HTML2 + HTML3 + HTML4 + HTML5 + HTML6;
    $('div#entCond_' + ctrCond).append(HTMLinsert);

    condHtml = condHtml + HTMLinsert;

    myFields(ctrCond, "false");

    var HTML7 = '<div id="divVal_' + ctrCond + '"><input id="enteredValue" type="hidden"  name="record_key1_' + ctrCond + '_enteredValue" value="0" ></div>';
    HTML7 = HTML7 + '<div id="divVal1_' + ctrCond + '"><input id="enteredValue1" type="hidden"  name="record_key1_' + ctrCond + '_enteredValue1" value="0" ></div>';
    $('td#td_' + ctrCond).append(HTML7);

    jsoncreateQuery();
    //        pressEnter="";

}

function myFields(ctrCond, fieldsOnly, tdId) {
    var strKey = "record_key1_";

    var conditiontypeNUm = myFunction(idCon);

    var HTML7 = "";

    if (fieldsOnly == "false") {
        HTML7 = '<tr id="tr_' + ctrCond + '" style="display:none"><td id="td_' + ctrCond + '">';
    }

    var xx = myCondTYpe(ctrCond, ctrCond, "true");

    HTML7 = HTML7 + '<div id="fields_' + ctrCond + '">';
    HTML7 = HTML7 + '<input type="hidden" id="fieldName" name="' + strKey + ctrCond + '_fieldName" value="' + fn + '" />';
    HTML7 = HTML7 + '<input type="hidden" id="tableName" name="' + strKey + ctrCond + '_tableName" value="' + tn + '" />';
    HTML7 = HTML7 + '<input type="hidden" id="type" name="' + strKey + ctrCond + '_type" value="' + ntypeID + '" />';
    HTML7 = HTML7 + '<input type="hidden" id="operatorNum" name="' + strKey + ctrCond + '_operatorNum" value="' + operatorNum + '" />';
    HTML7 = HTML7 + '<input type="hidden" id="id" name="' + strKey + ctrCond + '_id" value="' + ctrCond + '" />';
    HTML7 = HTML7 + '</div>';

    if (fieldsOnly == "false") {
        HTML7 = HTML7 + '<div id="div_' + ctrCond + '"><input type="hidden" id="conditiontypename" name="' + strKey + ctrCond + '_conditiontypename" value="' + conditiontypeNUm + '" /></div>';
        HTML7 = HTML7 + '</td></tr>'///last
    }

    if (fieldsOnly == "false") {
        $('#conditionTable').append(HTML7);
    }
    else {
        $('td#td_' + tdId).append(HTML7);
    }

    condHtml = condHtml + HTML7;

    //        document.getElementById("conhtml").value = condHtml;
}

function myNewType(id) {
    if (id == "0") ntype = "String";
    else if (id == "1") ntype = "Date";
    else if (id == "2") ntype = "Integer";
    else if (id == "3") ntype = "Boolean";
}

function myCondTYpe(ctrCond, id, textOnly) {
    //        operatorNum = 1;
    var HTML2_ = "";

    if (ntype == "String") {
        ntypeID = 0;
        if (textOnly == "true") { HTML2_ = "contains" }
        else {
            HTML2_ = '<a href="javascript:void(0)" id="expCondType_' + ctrCond + '_' + id + '" class="selectCondTypeExp">contains</a><div id="style_' + ctrCond + '" style="display: none;"></div></div>';
        }
    }
    else if (ntype == "Date") {
        ntypeID = 1;
        if (textOnly == "true") { HTML2_ = "is" }
        else {
            HTML2_ = '<a href="javascript:void(0)" id="expCondType_' + ctrCond + '_' + id + '" class="selectCondTypeExp">is</a><div id="style_' + ctrCond + '" style="display: none;"></div></div>';
        }
    }
    else if (ntype == "Integer") {
        ntypeID = 2;
        if (textOnly == "true") { HTML2_ = "is" }
        else {
            HTML2_ = '<a href="javascript:void(0)" id="expCondType_' + ctrCond + '_' + id + '" class="selectCondTypeExp">is</a><div id="style_' + ctrCond + '" style="display: none;"></div></div>';
        }
    }
    else if (ntype == "Boolean") {
        ntypeID = 3;
        if (textOnly == "true") { HTML2_ = "is true" }
        else {
            HTML2_ = '<a href="javascript:void(0)" id="expCondType_' + ctrCond + '_' + id + '" class="selectCondTypeExp">is true</a><div id="style_' + ctrCond + '" style="display: none;"></div></div>';
        }
    }

    return HTML2_;
}

function removeElement(id) {
    var elem = document.getElementById(id);
    if (elem != null) {
        elem.parentNode.removeChild(elem);
    }
}

function getValue(id) {
    return document.getElementById(id).value;
}

function infoText(idSelected) {
    //dongui
    $('input:text#editText_' + idSelected).css('display', 'none');
    //
    $('a#' + idSelected).css('display', 'inline');
    $('input:text.ers-qp-ve-editbox').attr('style', 'display:none');
    var changeValue = $('#editText_' + idSelected).val();
    if (changeValue != undefined) if (changeValue.length <= 0) changeValue = "[enter value]";
    if (changeValue != undefined) $('a#' + idSelected).text(changeValue);
    isSelected = false;
    return changeValue;
}

function valueEntered(idSelected) {
    var splitId = idSelected.split('_');
    var oldValue = $('#' + idSelected).text();
    var conDate = idSelected.substring(0, 9);

    if (oldValue != "[enter value]" && oldValue != "") {
        var elem = "enteredValue";
        if (conDate == "condDate_") elem = "enteredValue1";

        var Node1 = document.getElementById('fields_' + splitId[1]);
        if (Node1 != null) {
            var len = Node1.childNodes.length;

            for (var i = 0; i < len; i++) {
                if (Node1.childNodes[i] != undefined) {
                    if (Node1.childNodes[i].id == elem) {
                        firstVal = Node1.childNodes[i].value;
                        Node1.removeChild(Node1.childNodes[i]);
                    }
                }
            }
        }
    }
    isSelected = true;
}

function editText(event) {
    var changeValue = infoText(idSelected);

    var splitId = idSelected.split('_');

    removeElement('displayName_' + splitId[0]);

    var HTML7 = '<div id="displayName_' + splitId[0] + '"><input type="hidden" id="columndisplayname" name="record_key2_' + splitId[0] + '_columndisplayname" value="' + changeValue + '" /></div>';
    $('td#tdColumn_' + splitId[0]).append(HTML7);

    jsoncreateQuery();
}

function editValues(id, name, isTrue) {
    var HTML7 = "";
    var check = false;
    var value = Number(name);
    if (ntype == "Integer") {
        if (Math.floor(value) == value) {
            check = true;
        }
    }
    else if (ntype == "Date") {
        re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        if (textValue.match(re)) {
            check = true;
        }
    }
    else if ((ntype == "String") || (ntype == "Boolean")) check = true;

    if (check == true) {
        if (isTrue == true) {
            if (name == "") {
                var elem = document.getElementById("divVal_" + id);
                name = elem.children[0].value;
            }
            removeElement("divVal_" + id);
            HTML7 = '<div id="divVal_' + id + '"><input id="enteredValue" type="hidden"  name="record_key1_' + id + '_enteredValue" value="' + name + '" ></div>';
        }
        else {
            if (name == "") {
                var elem = document.getElementById("divVal1_" + id);
                name = elem.children[0].value;
            }
            removeElement("divVal1_" + id);
            HTML7 = '<div id="divVal1_' + id + '"><input id="enteredValue1" type="hidden"  name="record_key1_' + id + '_enteredValue1" value="' + name + '" ></div>';
        }
        $('td#td_' + id).append(HTML7);
    }
}

function editTextCondition(event, idSelected) {
    var splitId = idSelected.split('_');

    var isTrue = true;

    if (event.target.id == "editText_condDate_" + splitId[1] + "_" + splitId[2]) {
        isTrue = false;
    }
    if (isTrue == false) {
        idSelected = "condDate_" + splitId[1] + "_" + splitId[2];
    }

    var oldValue = event.target.parentElement.children[0].text;

    if (isTrue == false) {
        oldValue = event.target.parentElement.children[3].text;
    }

    if (oldValue != "[enter value]") {
        var elem = "enteredValue";
        if (isTrue == false) {
            elem = "enteredValue1";
        }
        removeElement(elem);
    }

    //        removeElement('div_' + splitId[1]);

    var changeValue = infoText(idSelected);

    var conditiontypeNUm = myFunction(idConVal);

    //        var addNew = '<div id="div_' + splitId[1] + '"><input type="hidden" id="conditiontypename" name="record_key1_' + splitId[1] + '_conditiontypename" value="' + conditiontypeNUm + '" /></div>';
    //        $('td#td_' + splitId[1]).append(addNew);

    editValues(splitId[1], changeValue, isTrue);

}

$(document).ready(function () {

    $(document).click(function (event) {
        obtainCondition(event);
    });

    $('.ers-ep-entity-node-button').click(function (e) {
        var sID = e.target.id;

        var sColID = "bImage" + sID;
        if (e.target.className != "ers-ep-entity-node-button ers-ep-entity-node-button-open") {
            $(this).attr('class', 'ers-ep-entity-node-button ers-ep-entity-node-button-open');
        }
        else {
            $(this).attr('class', 'ers-ep-entity-node-button');
        }

        $("*[id='" + sColID + "']").toggle();
    });

    $('.checkAll').click(function () {
        var areaName = 'checkItem_' + $(this).attr('id');
        $('.' + areaName).attr('checked', this.checked);

    });

    $("input[class^= 'checkItem_tab']").click(function () {
        var gName = $(this).attr('name');
        var gVal = $(this).attr('checked');

        var n = 0;
        if ($(this).is(':checked')) {
            $('#tab' + gName).attr('checked', true);
        };
        $("input[class^= 'checkItem_tab']").each(function () {
            if (this.name == gName) {
                var n = $("input:checked").length;
                if (n == 1) {
                    $('#tab' + gName).attr('checked', false);
                }
                else if (n > 1) {
                    $('#tab' + gName).attr('checked', true);
                };
            }

        });
    });

    $('.ers-ep-tool-panel-deselect-all').click(function () {
        $('input:checkbox').attr('checked', false);
    });

    $('#predelement').click(function () {
        $('#predelementTypes').toggle();
    });

    //show table selections
    //    $('#addColumn').click(function () {
    //        selectedExpression = false;

    //        var test = $("ul[id^='TablesCol_hover']");
    //        for (var i = 0; i < test.length; i++) {
    //            var x = test[i].attributes['id'].nodeValue;
    //            var getCount = x.substr(15);

    //            var sColID2 = "#TablesCol_hover" + getCount;
    //            $(sColID2).css('display', 'none');
    //        }
    //        var offset = $("#addColumn").offset();
    //        var top = offset.top + 18;
    //        $("#colPop").attr('style', "left:" + offset.left + "px;top:" + top + "px");
    //        $("#colPop").toggle();


    //    });



    //        $("input:checkbox").draggable({
    //            containment: "document",
    //            helper: "clone",
    //            revert: "invalid",
    //            scope: "entityAttr",
    //            distance: 20
    //        })
    //$(function() {
    //    $("input:checkbox").draggable({
    //        cursor:'move',
    //        opacity:    0.3,
    //        revert:     true,
    //        proxy:      'clone',
    //    });
    //});


    $('div#addEntityCol').click(function () {
        selectedExpression = false;
        var test = $(".checkAll");

        for (var i = 0; i < test.length; i++) {
            if (test[i].checked == true) {
                var thisId = test[i].id.substr(3);
                var checked = $(".checkItem_tab" + thisId);
                var x = 1;
                for (var ii = 0; ii < checked.length; ii++) {
                    if (checked[ii].checked == true) {
                        var chkId = checked[ii];
                        var labelId = $('#label' + chkId.name + '_' + x);
                        if (selectedExpression != true) {
                            var splitID = chkId.id.split('_');
                            idSelected = chkId.name;
                            var name = $.trim($('#label' + idSelected + '_' + splitID[2]).text());

                            var coltype = $('#type_FN' + splitID[2]);

                            tn = getValue("checkTableName_" + splitID[2]);
                            fn = getValue("checkColName_" + splitID[2]);
                            ntype = getValue("checkColType_" + splitID[2]);

                            appendHTML(idSelected, name, name, coltype.val());
                        }
                    }
                    x++;
                }
            }
        }
    });

    $('div#addEntityCon').click(function () {
        selectedExpression = false;
        var test = $(".checkAll");

        for (var i = 0; i < test.length; i++) {
            if (test[i].checked == true) {
                var thisId = test[i].id.substr(3);
                var checked = $(".checkItem_tab" + thisId);
                var x = 1;
                for (var ii = 0; ii < checked.length; ii++) {
                    if (checked[ii].checked == true) {
                        var chkId = checked[ii];
                        var labelId = $('#label' + chkId.name + '_' + x);
                        if (selectedExpression != true) {
                            var splitID = chkId.id.split('_');
                            idSelected = chkId.name;
                            var name = $.trim($('#label' + idSelected + '_' + splitID[2]).text());

                            tn = getValue("checkTableName_" + splitID[2]);
                            fn = getValue("checkColName_" + splitID[2]);
                            ntype = getValue("checkColType_" + splitID[2]);

                            appendHTML2(idSelected, name, name);
                        }
                    }
                    x++;
                }
            }
        }
    });

    /////////////////
    //show columns based on the table selected
    //    $('.columns').mouseover(function () {
    //        var sMainID = $(this).attr("id");
    //        var sColID = "#TablesCol_" + sMainID;

    //        $(sColID).toggle();

    //        var last = sMainID.substr(5);
    //        var sColID = "#TablesCol_hover" + last;

    //        var test = $("ul[id^='TablesCol_hover']");
    //        for (var i = 0; i < test.length; i++) {
    //            var x = test[i].attributes['id'].nodeValue;
    //            var getCount = x.substr(15);
    //            if (last != getCount) {
    //                var sColID2 = "#TablesCol_hover" + getCount;
    //                //                $(sColID2).hide();
    //                $(sColID2).css('display', 'none');
    //            }
    //        }

    //        var d = '#' + $(this).attr('id');
    //        $(d).removeClass('columns');
    //        $(d).addClass('ui-state-hover');
    //    });
    ///////////////////////

    $("a").bind("mouseleave", function (e) {
        var d = '#' + $(this).attr('id');
        $(d).removeClass('ui-state-hover');

    });

    $("a").bind("click", function (e) {
        if (this.id == "predelement") {
            if ((this.id == "aAll") || (this.id == "aAny") || (this.id == "aNone") || (this.id == "aNotAll")) {
                $('a#predelement').text($(this).text()); $(this.id).toggle();
                $('#predelementTypes').toggle();
            }
        }


        var d = $("#rootConType"); // $(this).attr('rootConType');

        if (d[0].text == "all") {
            idCon = "aAll";
        }
        else if (d[0].text == "any") {
            idCon = "aAny";
        }
        else if (d[0].text == "none") {
            idCon = "aNone";
        }
        else if (d[0].text == "not all") {
            idCon = "aNotAll";
        }

        idConVal = d[0].text;

        if (e.target.className == "hover_table2 selected ui-state-hover") {
            var id = e.target.id.substring(2);
            tn = getValue("tableName" + id);
            fn = getValue("columnName" + id);
            ntype = getValue("columntype" + id);
        }
        else if (e.target.className == "hover_table selected ui-state-hover") {
            tn = getValue("tblName_" + e.target.id);
            fn = getValue("fldName_" + e.target.id);
            ntype = getValue("type_" + e.target.id);
        }
    });


    //clear queries
    $('#btnClear').click(function () {
        $('.ers-qc-row-column-entattr').remove();
        $('.ers-qp-row-condition').remove();
        var removeCol = $('#columnTable').find('tbody');
        removeCol.remove();

        var removeCon = $('#conditionTable').find('tbody');
        removeCon.remove();

        var removeVal = $('div.ers-sql-result');
        removeVal[0].innerHTML = "";
        var HTML = '<input id="enteredbtnClear" type="hidden"  name="buttonClear" value="1" >';
        $('div#btnClear').append(HTML);
        ctr = 0;
        ctrCond = 0;
        idSelected = "";
        ntype = "";
    });

    ///Conditions panel

    //show table selectio
    $('#addCondition').click(function () {
        selectedExpression = false;
        documentCondClick = false;
        var test = $("ul[id^='TablesCon_hover']");
        for (var i = 0; i < test.length; i++) {
            var x = test[i].attributes['id'].nodeValue;
            var getCount = x.substr(15);

            var sColID2 = "#TablesCon_hover" + getCount;
            //                $(sColID2).hide();
            $(sColID2).css('display', 'none');
        }
        var offset = $("#addCondition").offset();
        var top = offset.top + 18;
        $("#conPop").attr('style', "left:" + offset.left + "px;top:" + top + "px");
        $("#conPop").toggle();

    });

    ///Show main condition type selections
    $('.selectCondTypeMain').click(function () {
        idSelected = this.id;
        var offset = $("#" + idSelected).offset();
        var top = offset.top + 18;
        var height = offset.height + 30;
        $("#condTypeMainPop").attr('style', "left:" + offset.left + "px; top: " + top + "px; width: 150px;");

        $("#condTypeMainPop").toggle();
        isCondTypeMainSelected = true;
    });

    /////////////////
    //show columns based on the table selected
    $('.conditions').mouseover(function () {
        var sMainID = $(this).attr("id");
        var sColID = "#TablesCon_" + sMainID;

        $(sColID).toggle();

        var last = sMainID.substr(5);
        var sColID = "#TablesCon_hover" + last;

        var test = $("ul[id^='TablesCon_hover']");
        for (var i = 0; i < test.length; i++) {
            var x = test[i].attributes['id'].nodeValue;
            var getCount = x.substr(15);
            if (last != getCount) {
                var sColID2 = "#TablesCon_hover" + getCount;
                $(sColID2).hide();
            }
        }

        var d = '#' + $(this).attr('id');

        $(d).removeClass('conditions');
        $(d).addClass('ui-state-hover');
    });
    ///////////////////////

    //        $(".ers-ep-entity-attr-label").draggable({
    //            containment: "document",
    //            helper: "clone",
    //            revert: "invalid",
    //            scope: "entityAttr",
    //            distance: 20
    //        });

        $("div").mouseover(
                function (e) {
    //                if (e.target.id != "addColumn") {
    //                    if (e.target.className != "columns hover_table") {
    //                        $("#colPop").hide();
    //                        //$("#colPop").toggle();
    //                    }
    //                }
                    if (e.target.className == "ers-qp-condition-button ers-qp-condition-button-enable enabled ers-qp-condition-button"
                    || e.target.className == "ers-qp-condition-button ers-qp-condition-button-enable ers-qp-condition-button"
                    || e.target.className == "ers-qp-condition-button ers-qp-condition-button-delete ers-qp-condition-button"
                    || e.target.className == "ers-qp-condition-buttonsBlock"
                    || e.target.className == "ers-qp-row ers-qp-row-condition"
                    || e.target.className == "conditionValue"
                    || e.target.className == "selectCondTypeExp"
                    || e.target.className == "selectCondExp"
                    || e.target.className == "ers-qp-condelement ers-qp-condition-conjuction") {
                        var xx = e.target.id.indexOf('_');
                        if (xx > 0) {
                            var splitID = e.target.id.split('_');
                            if (e.target.className == "selectedTitle")
                                var counter = splitID[0];
                            else
                                var counter = splitID[1];

                            if (xx == 10) {
//                                if (e.target.id.substring(0, 11) == "sortbutton_") {
//                                    var splitId = e.target.id.split('_');
//                                    idSortSelected = splitId[1];
//                                }
                            }
                        }
                        else
                            var counter = e.target.id.substr(3);

                        var qc = e.target.className.indexOf('ers-qc');
//                        var buttonType = $("div[id^='column-button-type_" + counter + "']");

//                        if (e.target.className.indexOf('ers-qc') >= 0 || e.target.className.indexOf('selectedTitle') >= 0 || e.target.className.indexOf('selectExp') >= 0) {

//                            //dongui
//                            $('div#sortbutton_' + counter + '_').attr('style', "");
//                            if (buttonType[0] != undefined)
//                                $('div#' + buttonType[0].id).attr('style', "");
//                            //

//                            $('div#column-button-delete_' + counter + '_').attr('style', "");
//                            $('div.ers-qc-column-buttonsBlock').css('display', 'inline');
//                        }
//                        else {

                            //dongui

                            $('div#condition-button-delete_' + counter + '_').attr('style', "");

                            $('div#condition-button-type_' + counter + '_').attr('style', "");
                            $('ers-qp-condition-buttonsBlock').css('display', 'inline');

//                        }
                        var test = $("div[id^='ent']");
                        for (var i = 0; i < test.length; i++) {
                            var x = test[i].attributes['id'].nodeValue;
                            var getCount = x.substr(8);
                            if (counter != getCount) {

//                                var buttonType = $("div[id^='column-button-type_" + getCount + "']");
//                                $('div#sortbutton_' + getCount + '_').attr('style', "background-image: none");
//                                //dongui
//                                if (buttonType[0] != undefined)
//                                    $('div#' + buttonType[0].id).attr('style', "background-image: none");

//                                $('div#column-button-delete_' + getCount + '_').attr('style', "background-image: none");
                                $("div.ers-qp-condition-buttonsBlock").attr('class', 'ers-qp-condition-buttonsBlock');


                                $('div#condition-button-type_' + getCount + '_').attr('style', "background-image: none");
                                $('div#condition-button-delete_' + getCount + '_').attr('style', "background-image: none");
                            }
                        }
                    }
                    else {
                        $('div.ers-qc-sortbutton-none').attr('style', "background-image: none");
                        $('div.ers-qc-column-button-type').attr('style', "background-image: none");
                        $('div.ers-qc-column-button-delete').attr('style', "background-image: none");
                        $('div.ers-qp-condition-button-enable').attr('style', "background-image: none");
                        $('div.ers-qp-condition-button-delete').attr('style', "background-image: none");
                    }


                    //dongui 04/16
//                    if (e.target.className == "hover_table selected" || e.target.className == "hover_table2 selected") {
//                        $("a#" + e.target.id).addClass('ui-state-hover');
//                    }
                    //
                });

    $("body").click
            (
              function (e) {

                  if (e.target.id != "btnClear")
                      $('#enteredbtnClear').remove();

                  //                  if (e.target.id != "addColumn") {
                  //                      if (e.target.className != "columns hover_table") {
                  //                          $("#colPop").hide();
                  //                          //$("#colPop").toggle();
                  //                      }
                  //                  }

                  //dongui
                  //                  if (e.target.className == "hover_table selected ui-state-hover") {
                  //                      if (selectedExpression != true) {
                  //                          documentCondClick = false;
                  //                          idSelected = e.target.id;

                  //                          var coltype = $('#type_' + idSelected);
                  //                          var colname = $('#fldName_' + idSelected);
                  //                          var coltblname = $('#tblName_' + idSelected);
                  //                          var coltblID = $('#tableID_' + idSelected);

                  //                          appendHTML(idSelected, e.target.innerHTML, e.target.innerHTML, coltype.val());
                  //                      }
                  //                      else {
                  //                          var tabId = idSelected.split("_");
                  //                          var idFunc = tabId[0] + '_' + tabId[1];
                  //                          var changeValue = e.target.innerHTML;
                  //                          $('#exp_' + idSelected).text(changeValue);

                  //                          if ($('a#func_' + idFunc).attr('style') == "") {
                  //                              var aggregate = $('a#func_' + idFunc).text();
                  //                              $('a#' + idSelected).text(changeValue + ' ' + aggregate);

                  //                          }
                  //                          else
                  //                              $('a#' + idSelected).text(changeValue);

                  //                          var replaceId = tabId[0] + '_' + e.target.id


                  //                          $('input:text#editText_' + idSelected).text(changeValue);

                  //                          $('#exp_' + idSelected).attr('id', 'exp_' + replaceId);
                  //                          $('a#' + idSelected).attr('id', replaceId);
                  //                          $('input:text#editText_' + idSelected).attr('id', 'editText_' + replaceId);

                  //                          $('a#func_' + idFunc).attr('id', 'func_' + replaceId);
                  //                          $('#span_' + idFunc).attr('id', 'span_' + replaceId);

                  //                          $('#column-button-type_' + idFunc).attr('id', 'column-button-type_' + replaceId);
                  //                          var changeType = $('#type_' + e.target.id).val();

                  //                          $('#colType_' + idFunc).val(changeType);
                  //                          $('a#func_' + replaceId).text('Count');
                  //                          var editVal = $('#exp_' + replaceId).text();

                  //                          $('#colType_' + idFunc).attr('id', 'colType_' + tabId[0] + '_' + e.target.id);

                  //                          var elem = document.getElementById("function_" + tabId[0]);

                  //                          if (elem.children[0].value == "2") {
                  //                              $('a#' + replaceId).text(editVal + ' Count');
                  //                              $('input:text#editText_' + replaceId).val(editVal + ' Count');
                  //                          }

                  //                          $('input:text#editText_' + replaceId).text(changeValue);

                  //                          var xx = myCondTYpe(ctrCond, ctrCond, "true");

                  //                          document.getElementById("colType_" + tabId[0] + "_" + e.target.id).value = ntype;

                  //                          removeElement('trColumn_' + tabId[0]);

                  //                          currentAggregate = aggregate;

                  //                          myColumnFields(tabId[0], e.target.id, changeValue);

                  //                          selectedExpression = false;

                  //                          jsoncreateQuery();

                  //                      }
                  //                  }

                  ///
                  if (e.target.className == "hover_table2 selected ui-state-hover") {
                      documentCondClick = false;
                      if (selectedExpression != true) {
                          idSelected = e.target.id;
                          appendHTML2(idSelected, e.target.innerHTML, e.target.innerHTML);
                      }
                      else {
                          var tabId = idSelected.split("_");
                          var changeValue = e.target.innerHTML;
                          $('#expCond_' + idSelected).text(changeValue);
                          $('a#cond_' + idSelected).text('[enter value]');
                          $('a#condDate_' + idSelected).text('[enter value]');
                          $('#expCond_' + idSelected).attr('id', 'expCond_' + tabId[0] + '_' + e.target.id);
                          $('a#cond_' + idSelected).attr('id', 'cond_' + tabId[0] + '_' + e.target.id);
                          $('a#condDate_' + idSelected).attr('id', 'condDate_' + tabId[0] + '_' + e.target.id);
                          $('input:text#editText_cond_' + idSelected).attr('id', 'editText_cond_' + tabId[0] + '_' + e.target.id);

                          document.getElementById('editText_cond_' + tabId[0] + '_' + e.target.id).value = "";
                          $('input:text#editText_condDate_' + idSelected).attr('id', 'editText_condDate_' + tabId[0] + '_' + e.target.id);
                          document.getElementById('editText_condDate_' + tabId[0] + '_' + e.target.id).value = "";
                          $('a#condDate_' + tabId[0] + '_' + e.target.id).attr("style", "display:none");
                          $('span#spanDate_' + idSelected).attr('id', 'spanDate_' + tabId[0] + '_' + e.target.id);
                          $('span#spanDate_' + tabId[0] + '_' + e.target.id).attr("style", "display:none");

                          selectedExpression = false;

                          var parent = document.getElementById("entCond_" + tabId[0]);
                          var child = parent.children[0].id;
                          var childSubstring = parent.children[0].id.substring(0, 11);

                          if (childSubstring == "conjuction_") {
                              child = parent.children[1].id
                              $('span#conjuction_' + idSelected).attr('id', 'conjuction_' + tabId[0] + '_' + e.target.id);
                          }

                          mySpanIds[tabId[0] - 1] = 'conjuction_' + tabId[0] + '_' + e.target.id;

                          $('div#' + child).attr('id', fn + "-" + ntype);
                          $('div#butBlck_' + idSelected).attr('id', 'butBlck_' + tabId[0] + '_' + e.target.id);

                          var idNo = "td_" + tabId[0];

                          removeElement("fields_" + tabId[0]);

                          myFields(tabId[0], "true", tabId[0]);

                          removeElement("expCondType_" + tabId[0] + "_" + tabId[1]);
                          removeElement("style_" + tabId[0]);

                          var HTML2_ = myCondTYpe(tabId[0], e.target.id);

                          $('div#operelement_' + tabId[0]).append(HTML2_);

                          child = document.getElementById(parent.children[2].id);
                          var grandChild = child.children[0].id;

                          $('a#' + grandChild).attr("style", "display: inline-block");

                          jsoncreateQuery();
                      }
                  }
//                  if (e.target.className == "selectedTitle") {
//                      documentCondClick = false;
//                      editText(e);
//                      idSelected = e.target.id;
//                      $('.selectedTitle').css('display', 'inline');

//                      $('a#' + idSelected).css('display', 'inline');

//                      $('input:text.ers-qc-ce-editbox').css('display', 'none');
//                      $('input:text.ers-qp-ve-editbox').attr('style', 'display:none;');
//                      $('.conditionValue').css('display', 'inline');

//                      var test = $("input[id^='editText_']");
//                      for (var i = 0; i < test.length; i++) {
//                          if (test[i].className == "ers-qc-ce-editbox") {
//                              var getSelected = test[i].attributes['id'].nodeValue;
//                              getSelected = getSelected.substr(9);
//                              if (test[i].value != "")
//                                  var changeValue = test[i].value;
//                              else
//                                  var changeValue = test[i].attributes["value"].nodeValue;

//                              test[i].value = changeValue;
//                              if (isSelected == true) {
//                                  $('a#' + getSelected).text(changeValue);
//                              }
//                          }
//                      }
//                      //                      }

//                      splitID = e.target.id.split('_');
//                      idSelected = e.target.id;
//                      e.target.style.cssText = "display: none;";
//                      $('input:text#editText_' + e.target.id).css('display', 'inline');
//                      $('input:text#editText_' + e.target.id).focus();

//                      $('input:text.ers-qc-ve-editbox').css('display', 'none');

//                      isSelected = true;
//                  }

//                  else if (e.target.className != "ers-qc-ce-editbox" && e.target.className != "ers-qp-ve-editbox") {

//                      if (isSelected == true) {
//                          $('input:text.ers-qc-ve-editbox').attr('style', 'display:none');
//                          $('input:text.ers-qc-ce-editbox').attr('style', 'display:none');
//                          var changeValue = $('#editText_' + idSelected).val();

//                          $('a#' + idSelected).text(changeValue);

//                          editText(e);

//                      }

//                  }

                  if (e.target.className == "conditionValue" || e.target.className == "conditionValueDate") {
                      documentCondClick = true;
                      isErrorCond = false;
                      var isCond = false;
                      var isCondDate = false;
                      var splitID = idSelected.split('_');
                      var condDateId = splitID[1] + "_" + splitID[2];
                      //                      idSelected = e.target.id;

                      var x = $('a#expCondType_' + condDateId + ':contains("between")').length;
                      if (x > 0) {
                          $('a#condDate_' + condDateId).css('display', 'inline');
                      }

                      $('input:text.ers-qc-ce-editbox').css('display', 'none');
                      $('input:text.ers-qp-ve-editbox').css('display', 'none');
                      $('.conditionValue').css('display', 'inline');

                      var test = $("input[id^='editText_cond']");
                      for (var i = 0; i < test.length; i++) {
                          var getSelected = test[i].attributes['id'].nodeValue;
                          getSelected = getSelected.substr(9);

                          var changeValue = test[i].value;

                          getSelected = getSelected;

                          if (isCondSelected == true) {
                              if (changeValue.length <= 0) changeValue = "[enter value]";
                              $('a#' + getSelected).text(changeValue);
                          }
                      }

                      var textValue = $('a#' + splitID[0] + "_" + splitID[1] + "_" + splitID[2]).text();


                      //                      removeElement('fields_' + splitID[1]);
                      //                      myFields(splitID[1], "true", splitID[1]);

                      if (splitID[0] == "cond") {
                          var getVal = document.getElementById("editText_" + idSelected).value;
                          if (getVal != "[enter value]") {
                              editValues(splitID[1], getVal, true);
                          }
                      }
                      else if (splitID[0] == "condDate") {
                          var getVal = document.getElementById("editText_condDate_" + splitID[1] + "_" + splitID[2]).value;
                          editValues(splitID[1], getVal, false);
                      }

                      var find = splitID[0].indexOf('cond');
                      if (e.target.className == "conditionValueDate" || x > 0) {
                          var condValue = $('a#cond_' + condDateId);
                          var condDateValue = $('a#condDate_' + condDateId);


                          if (isNaN(condValue[0].innerHTML) && condValue[0].innerHTML != "[enter value]")
                              var textValCond = $('a#cond' + "_" + splitID[1] + "_" + splitID[2]).text();
                          else if (isNaN(condDateValue[0].innerHTML))
                              var textValCond = $('a#condDate' + "_" + splitID[1] + "_" + splitID[2]).text();

                          if (textValCond != "[enter value]") {
                              if (textValCond == undefined) {
                                  isCond = true;
                                  textValue = $('a#' + splitID[0] + "_" + splitID[1] + "_" + splitID[2]).text();
                              }
                              else {
                                  textValue = textValCond;
                                  if (isNaN(condValue[0].innerHTML) && condValue[0].innerHTML != "[enter value]")
                                      isCond = true;
                                  else
                                      isCondDate = true;
                              }
                          }

                      }
                      else {
                          isCond = true;
                      }

                      var value = Number(textValue);

                      var parentId = e.target.parentElement.parentElement.id.substring(0, 8);
                      if (textValue != "[enter value]") {
                          var char = splitID[0].substr(0, 4);
                          if (char.toString() == "cond" && textValue != undefined) {
                              if (parentId != "entAdds_") {
                                  if (ntype == "Integer") {
                                      if (Math.floor(value) == value) checkType(e);
                                      else {
                                          isErrorCond = true;
                                          alert("Please input valid value");
                                          var changeValue = "[enter value]";
                                          $('a#' + idSelected).text(changeValue);
                                          if (isCond == true) {
                                              $('input:text#editText_cond_' + splitID[1] + "_" + splitID[2]).val('');

                                          }
                                          else if (isCondDate == true) {
                                              $('input:text#editText_condDate_' + splitID[1] + "_" + splitID[2]).val('');

                                          }

                                          $('input:text#editText_' + e.target.id).css('display', 'none');

                                          //                                          ///4.26
                                          //                                          removeElement('fields_' + splitID[1]);
                                          //                                          myFields(splitID[1], "true", splitID[1]);
                                      }
                                  }
                                  else if (ntype == "Date") {
                                      re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                                      isErrorCond = true;
                                      if (!textValue.match(re)) {
                                          alert("Invalid date format: " + textValue);
                                          var changeValue = "[enter value]";
                                          $('a#' + idSelected).text(changeValue);
                                          $('input:text#editText_cond_' + splitID[1] + "_" + splitID[2]).val('');

                                          //                                          ///4.26
                                          //                                          removeElement('fields_' + splitID[1]);
                                          //                                          myFields(splitID[1], "true", splitID[1]);
                                      }
                                      else {
                                          isErrorCond = false;
                                          checkType(e);
                                      }
                                  }
                                  else {
                                      checkType(e);
                                      isErrorCond = false;
                                  }
                              }
                              else {
                                  checkType(e);
                              }
                          }
                      }
                      splitID = e.target.id.split('_');
                      if (isErrorCond == false) {
                          checkType(e);
                          idSelected = e.target.id;
                          e.target.style.cssText = "display: none;";
                          $('input:text#editText_' + e.target.id).css('display', 'inline');
                          $('input:text#editText_' + e.target.id).focus();
                          isCondSelected = true;
                      }
                  }
                  if (e.target.className != "ers-qp-ve-editbox" && e.target.className != "ers-qc-ce-editbox"
                    ) {
                      if (isCondSelected == true && idSelected != e.target.id && isErrorCond == false) {
                          if ($('a#' + idSelected)) {


                              valueEntered(idSelected);

                              var splitId = idSelected.split('_');

                              removeElement('div_' + splitId[1]);

                              var changeValue = infoText(idSelected);

                              var conditiontypeNUm = myFunction(idConVal);

                              if (changeValue == undefined) {
                                  changeValue = firstVal;
                              }


                              var HTML7 = "";

                              if (splitId[0] == "condDate") {
                                  editValues(splitId[1], changeValue, false);
                              }
                              else {
                                  editValues(splitId[1], changeValue, true);
                              }


                              var addNew = '<div id="div_' + splitId[1] + '"><input type="hidden" id="conditiontypename" name="record_key1_' + splitId[1] + '_conditiontypename" value="' + conditiontypeNUm + '" /></div>';
                              $('td#td_' + splitId[1]).append(addNew);

                          }
                      }
                  }

//                  if (e.target.className == "selectExp") {
//                      idSelected = e.target.id.substr(4);
//                      selectedExpression = true;
//                      var targetId = e.target.id;
//                      var offset = $("#" + targetId).offset();
//                      var top = offset.top + 18;
//                      $("#colPop").attr('style', "left:" + offset.left + "px; top: " + top + "px;");
//                      $("#colPop").toggle();
//                  }

//                  if (e.target.className == "ers-qc-column-button ers-qc-column-button-delete") {
//                      var splitID = e.target.id.split('_');
//                      var counter = splitID[1];
//                      $('div#entAdds_' + counter).remove();
//                      $('tr#trColumn_' + counter).remove();

//                      ctr = ctr - 1;

//                      jsoncreateQuery();
//                  }

                  ///For conditions

                  if (e.target.id != "addCondition") {
                      if (e.target.className != "condition hover_table") {
                          $("#conPop").hide();
                      }
                  }

                  if (e.target.className == "selectCondExp") {
                      idSelected = e.target.id.substr(8);
                      selectedExpression = true;
                      var test = $("ul[id^='TablesCon_hover']");
                      for (var i = 0; i < test.length; i++) {
                          var x = test[i].attributes['id'].nodeValue;
                          var getCount = x.substr(15);
                          var sColID2 = "#TablesCon_hover" + getCount;

                          $(sColID2).css('display', 'none');
                      }
                      var offset = e.target.offsetTop;
                      var top = e.target.offsetTop + 20;
                      $("#conPop").attr('style', "left:" + e.target.offsetLeft + "px;top:" + top + "px");
                      $("#conPop").toggle();
                  }

                  if (e.target.className != "selectCondTypeMain") {
                      if (isCondTypeMainSelected == true) {
                          if (e.target.className == "ers-menu-itemDiv") {
                              var changeValue = e.target.innerHTML;
                              $("#" + idSelected).text(changeValue);
                          }
                          $("#condTypeMainPop").toggle();
                          isCondTypeMainSelected = false;
                      }
                  }

                  if (e.target.className != "selectCondTypeExp") {
                      if (isCondTypeSelected == true) {
                          if (e.target.className == "ers-menu-itemDiv") {
                              var changeValue = e.target.innerHTML;
                              $("#" + idSelected).text(changeValue);
                          }

                          if (CondTypeSelectedName == "String") {
                              $("#condTypePop").toggle();
                          }
                          else if (CondTypeSelectedName == "Date") {
                              $("#condTypePopDate").toggle();
                          }
                          else if (CondTypeSelectedName == "Integer") {
                              $("#condTypePopInteger").toggle();
                          }
                          else if (CondTypeSelectedName == "Boolean") {
                              $("#condTypePopBoolean").toggle();
                          }
                          isCondTypeSelected = false;
                      }
                  }

                  ///Show main condition type selections
                  if (e.target.className == "selectCondTypeExp") {
                      idSelected = e.target.id;
                      var offset = $("#" + idSelected).offset();
                      var top = offset.top + 18;

                      var tabId = idSelected.split('_');

                      var parent = document.getElementById("entCond_" + tabId[1]);
                      var child = parent.children[0].id;
                      var childSubstring = parent.children[0].id.substring(0, 11);

                      if (childSubstring == "conjuction_") {
                          nameType = e.target.parentElement.parentElement.children[1].id;
                      }
                      else { nameType = e.target.parentElement.parentElement.children[0].id; }

                      var splitName = nameType.split('-');

                      if (splitName[1] == "String") {
                          $("#condTypePop").attr('style', "left:" + offset.left + "px; top: " + top + "px; width: 150px;");
                          $("#condTypePop").toggle();
                          CondTypeSelectedName = "String";
                      }
                      else if (splitName[1] == "Date") {
                          $("#condTypePopDate").attr('style', "left:" + offset.left + "px; top: " + top + "px; width: 150px;");
                          $("#condTypePopDate").toggle();
                          CondTypeSelectedName = "Date";
                      }
                      else if (splitName[1] == "Integer") {
                          $("#condTypePopInteger").attr('style', "left:" + offset.left + "px; top: " + top + "px; width: 150px;");
                          $("#condTypePopInteger").toggle();
                          CondTypeSelectedName = "Integer";
                      }
                      else if (splitName[1] == "Boolean") {
                          $("#condTypePopBoolean").attr('style', "left:" + offset.left + "px; top: " + top + "px; width: 150px;");
                          $("#condTypePopBoolean").toggle();
                          CondTypeSelectedName = "Boolean";
                      }
                      isCondTypeSelected = true;
                  }
                  ////

                  ///show table selections
                  if (e.target.className == "ers-qp-condition-button ers-qp-condition-button-addCondition") {
                      selectedExpression = false;
                      var test = $("ul[id^='TablesCon_hover']");
                      for (var i = 0; i < test.length; i++) {
                          var x = test[i].attributes['id'].nodeValue;
                          var getCount = x.substr(15);
                          var sColID2 = "#TablesCon_hover" + getCount;

                          $(sColID2).css('display', 'none');
                      }
                      var offset = e.target.offsetTop;
                      var top = e.target.offsetTop + 20;
                      $("#conPop").attr('style', "left:" + e.target.offsetLeft + "px;top:" + top + "px");
                      $("#conPop").toggle();
                  }

                  //dongui
//                  if (e.target.className == "ers-qc-column-button ers-qc-column-button-type"
//                    || e.target.className == "selectFunction") {
//                      var idTarget = e.target.id.split('_');

//                      idColFunction = idTarget[1] + '_' + idTarget[2];
//                      var styleFunc = $('#func_' + idColFunction);
//                      if ($('#func_' + idColFunction).attr('style') != '' || e.target.className == "selectFunction") {
//                          var offset = e.target.offsetTop;
//                          var top = e.target.offsetTop + 20;
//                          $("#colFunction").attr('style', "left:" + e.target.offsetLeft + "px;top:" + top + "px");
//                          var colTypeId = $("input#colType_" + idColFunction).val();

//                          var filterCol = $("div[id^='columnfunc']");

//                          for (var i = 1; i <= filterCol.length; i++) {
//                              $("#" + filterCol[i - 1].id).attr('id', 'columnfunc' + i + '_' + idColFunction);
//                          }

//                          if (colTypeId == "String") {
//                              $("#columnfunc1_" + idColFunction).hide();
//                              $("#columnfunc3_" + idColFunction).hide();
//                              $("#columnfunc4_" + idColFunction).hide();
//                              $("#columnfunc5_" + idColFunction).hide();
//                          }
//                          else if (colTypeId == "Date") {
//                              $("#columnfunc1_" + idColFunction).hide();
//                              $("#columnfunc3_" + idColFunction).hide();
//                              $("#columnfunc4_" + idColFunction).hide();
//                              $("#columnfunc5_" + idColFunction).hide()
//                              $("#columnfunc4_" + idColFunction).toggle();
//                              $("#columnfunc5_" + idColFunction).toggle();
//                          }
//                          else if (colTypeId == "Integer") {
//                              $("#columnfunc1_" + idColFunction).hide();
//                              $("#columnfunc3_" + idColFunction).hide();
//                              $("#columnfunc4_" + idColFunction).hide();
//                              $("#columnfunc5_" + idColFunction).hide();
//                              $("#columnfunc1_" + idColFunction).toggle();
//                              $("#columnfunc3_" + idColFunction).toggle();
//                              $("#columnfunc4_" + idColFunction).toggle();
//                              $("#columnfunc5_" + idColFunction).toggle();
//                          }

//                          $("#colFunction").toggle();
//                      }
//                      else {
//                          $('a#func_' + idColFunction).css('display', 'none');
//                          $('span#span_' + idColFunction).css('display', 'none');
//                          var editVal = $('#exp_' + idColFunction).text();
//                          $('#' + idColFunction).text(editVal);
//                          $('#editText_' + idColFunction).text(editVal);

//                          var splitId = idColFunction.split('_');

//                          removeElement("function_" + splitId[0]);

//                          var HTML7 = '<div id="function_' + splitId[0] + '"><input type="hidden" id="functionType" name="record_key2_' + splitId[0] + '_functionType" value="" /></div>';
//                          $('td#tdColumn_' + splitId[0]).append(HTML7);

//                          removeElement("displayName_" + splitId[0]);

//                          HTML7 = '<div id="displayName_' + splitId[0] + '"><input type="hidden" id="columndisplayname" name="record_key2_' + splitId[0] + '_columndisplayname" value="' + editVal + '" /></div>';
//                          $('td#tdColumn_' + splitId[0]).append(HTML7);

//                          jsoncreateQuery();
//                      }
//                  }
//                  else {
//                      idColFunction = "";
//                      $("#colFunction").hide();
//                  }
                  //

//                  if (e.target.className == "ers-qc-colelement ers-qc-sortbutton ers-qc-sortbutton-none") {
//                      selectedExpression = false;

//                      var offset = e.target.offsetTop;
//                      var top = e.target.offsetTop + 20;
//                      $("#colSortingPop").attr('style', "left:" + e.target.offsetLeft + "px;top:" + top + "px");
//                      $("#colSortingPop").toggle();

//                      iscolSortingPop = true;
//                  }
//                  else
//                      $("#colSortingPop").hide();

                  if (e.target.className == "ers-qp-condition-button ers-qp-condition-button-delete ers-qp-condition-button") {
                      var splitID = e.target.id.split('_');
                      var counter = splitID[1];
                      $('div#entCond_' + counter).remove();

                      $('tr#tr_' + counter).remove();
                      spanId = spanId - 1;
                      ctrCond = ctrCond - 1;

                      jsoncreateQuery();
                  }

                  if (e.target.className == "ers-qp-condition-button ers-qp-condition-button-enable enabled ers-qp-condition-button") {
                      var splitID = e.target.id.split('_');
                      var counter = splitID[1];
                      $('#' + e.target.id).attr('class', 'ers-qp-condition-button ers-qp-condition-button-enable ers-qp-condition-button');
                      $('#entCond_' + counter).attr('class', 'ers-qp-row ers-qp-row-condition ers-qp-disabled active');

                      var cond = $("a[id^='cond_" + counter + "']");
                      for (var i = 0; i < cond.length; i++) {
                          var id = cond[i].attributes['id'].nodeValue;
                          var link = "<span id='" + id + "' >[enter value]</span>";
                          $("#" + id).replaceWith(link);
                      }

                      var expcondType = $("a[id^='expCondType_" + counter + "']");
                      for (var i = 0; i < expcondType.length; i++) {
                          var id = expcondType[i].attributes['id'].nodeValue;
                          var link = "<span id='" + id + "' >contains</span>";
                          $("#" + id).replaceWith(link);
                      }

                      var expcond = $("a[id^='expCond_" + counter + "']");
                      for (var i = 0; i < expcond.length; i++) {
                          var id = expcond[i].attributes['id'].nodeValue;
                          var link = "<span id='" + id + "' >" + $("#" + id).html() + "</span>";
                          $("#" + id).replaceWith(link);
                      }
                  }
                  else if (e.target.className == "ers-qp-condition-button ers-qp-condition-button-enable ers-qp-condition-button") {
                      var splitID = e.target.id.split('_');
                      var counter = splitID[1];
                      $('#' + e.target.id).attr('class', 'ers-qp-condition-button ers-qp-condition-button-enable enabled ers-qp-condition-button');
                      $('#entCond_' + counter).attr('class', 'ers-qp-row ers-qp-row-condition');

                      var cond = $("span[id^='cond_" + counter + "']");
                      for (var i = 0; i < cond.length; i++) {
                          var id = cond[i].attributes['id'].nodeValue;
                          var link = "<a id='" + id + "' class='conditionValue' style='display: inline-block;' href='javascript:void(0)'>[enter value]</a>";
                          $("#" + id).replaceWith(link);
                      }

                      var expcondType = $("span[id^='expCondType_" + counter + "']");
                      for (var i = 0; i < expcondType.length; i++) {
                          var id = expcondType[i].attributes['id'].nodeValue;
                          var link = "<a id='" + id + "' class='selectCondTypeExp' href='javascript:void(0)'>contains</a>";
                          $("#" + id).replaceWith(link);
                      }

                      var expcond = $("span[id^='expCond_" + counter + "']");
                      for (var i = 0; i < expcond.length; i++) {
                          var id = expcond[i].attributes['id'].nodeValue;
                          var link = "<a id='" + id + "' class='selectCondExp' href='javascript:void(0)'>" + $("#" + id).html() + "</a>";
                          $("#" + id).replaceWith(link);
                      }
                  }

                  jsoncreateQuery();

              });



    $(document).keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);

        if (keycode == '13') {
            event.preventDefault();

            var textValue = event.target.value;

            var value = Number(textValue);

            var parentId = event.target.parentElement.parentElement.id.substring(0, 8);

            if (parentId != "entAdds_") {
                if (ntype == "Integer") {
                    if (Math.floor(value) == value) checkType(event);
                    else {
                        alert("Please input valid value");
                        //                        ///4.26
                        //                        removeElement('fields_' + splitID[1]);
                        //                        myFields(splitID[1], "true", splitID[1]);
                    }
                }
                else if (ntype == "Date") {
                    // regular expression to match required date format
                    re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

                    if (!textValue.match(re)) {
                        alert("Invalid date format: " + textValue);
                        //                        ///4.26
                        //                        removeElement('fields_' + splitID[1]);
                        //                        myFields(splitID[1], "true", splitID[1]);
                    }
                    else checkType(event);
                }
                else checkType(event);
            }
            else {
                checkType(event);
            }

            pressEnter = "yes";
        }
        else { pressEnter = ""; }
    });


    function obtainCondition(event) {
        //        if (documentCondClick == true) {
        var isCond = false;
        var isCondDate = false;
        var isNotEnterValue = false;
        if (pressEnter != "yes") {
            var splitID = idSelected.split('_');
            var condDateId = splitID[1] + "_" + splitID[2];
            if (ntype == "Integer" || ntype == "Date") {
                if (event.target.className != "conditionValueDate" && event.target.className != "conditionValue"
                        && event.target.className != "ers-qp-ve-editbox" && event.target.className != "ers-qp-ce-editbox"
                        && event.target.className != "ers-menu-itemDiv" && event.target.className != "selectCondTypeExp") {
                    if ($('a#cond_' + condDateId).attr('style') == "display: inline;" || $('a#condDate_' + condDateId).attr('style') == "display: inline;"
                            || $('a#editText_condDate_' + condDateId).attr('style') == "display: inline;"
                            || $('a#editText_cond_' + condDateId).attr('style') == "display: inline;") {

                        var x = $('a#expCondType_' + condDateId + ':contains("between")').length;
                        if (x > 0) {
                            var condDateValue = $('a#condDate_' + condDateId);
                            var condVal = $('a#cond_' + condDateId);

                            if (condVal[0].innerHTML != "[enter value]" && condDateValue[0].innerHTML != "[enter value]") {
                                if (!isNaN(condVal[0].innerHTML) && !isNaN(condDateValue[0].innerHTML))
                                    isNotEnterValue = true;
                            }
                        }
                        var condValue = $('a#' + idSelected);

                        if (isNotEnterValue == false) {

                            if (condValue[0].innerHTML != "[enter value]" && x > 0) {
                                var conDateText = $('a#condDate_' + condDateId);
                                var s = $('a#condDate_' + condDateId).attr('style');
                                if ($('a#condDate_' + condDateId).attr('style') == "display: none;"
                                                                    && $('input:text#editText_cond_' + condDateId).attr('style') == "display: inline;")
                                    $('a#condDate_' + condDateId).css('display', 'inline');
                                var g = $('#editText_condDate' + "_" + condDateId).val();
                                if (isNaN($('#editText_condDate' + "_" + condDateId).val())) {
                                    var textValue = $('#editText_condDate' + "_" + condDateId).val();
                                    isCondDate = true;
                                }
                                else if (conDateText[0].innerHTML != "[enter value]" && isNaN(conDateText[0].innerHTML) != isNaN($('#editText_condDate' + "_" + condDateId).val())) {
                                    var textValue = $('#editText_condDate' + "_" + condDateId).val();
                                }
                                else if (isNaN($('#editText_cond' + "_" + condDateId).val())) {
                                    var textValue = $('#editText_cond' + "_" + condDateId).val();
                                    isCond = true;
                                }
                                else
                                    var textValue = $('#editText_' + idSelected).val();
                            }
                            else {
                                var textValCond = $('#editText_cond' + "_" + condDateId).val();

                                if (textValCond != "[enter value]") {
                                    textValue = textValCond;
                                    isCond = true;
                                }
                            }

                            var value = Number(textValue);

                            var parentId = event.target.parentElement.parentElement.id.substring(0, 8);
                            var test = $("input[id^='editText_cond']");
                            for (var i = 0; i < test.length; i++) {
                                var getSelected = test[i].attributes['id'].nodeValue;
                                getSelected = getSelected.substr(9);

                                var changeValue = test[i].value;

                                getSelected = getSelected;

                                if (isCondSelected == true) {
                                    if (changeValue.length <= 0) changeValue = "[enter value]";
                                    $('a#' + getSelected).text(changeValue);
                                }
                            }
                            if (parentId != "entAdds_") {
                                if (ntype == "Integer") {
                                    if (Math.floor(value) != value) {
                                        isErrorCond = true;
                                        alert("Please input valid value");
                                        var changeValue = "[enter value]";
                                        $('a#' + idSelected).text(changeValue);
                                        if (isCond == true) {
                                            $('input:text#editText_cond_' + condDateId).val('');
                                        }
                                        else if (isCondDate == true) {
                                            $('input:text#editText_condDate_' + condDateId).val('');
                                        }
                                        //                                            ///4.26
                                        //                                            removeElement('fields_' + splitID[1]);
                                        //                                            myFields(splitID[1], "true", splitID[1]);
                                    }
                                }
                            }
                            else if (ntype == "Date") {
                                // regular expression to match required date format
                                re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                                if (!textValue.match(re)) {
                                    alert("Invalid date format: " + textValue);
                                    isErrorCond = true;
                                    var changeValue = "[enter value]";
                                    $('a#' + idSelected).text(changeValue);
                                    $('input:text#editText_cond_' + condDateId).val('');
                                }
                            }
                        }

                    }

                }

            }
            pressEnter = "";
        }
        //        }
    }

    $('.ers-menu-itemDiv').click(function (e) {
        if (e.target.className == "ers-menu-itemDiv selected") itemDevFunction(e);
        else {
            if (iscolSortingPop == false) itemDevCondition(e);
            else if (iscolSortingPop == true) itemDevColumns(e);
        }
    });


});
