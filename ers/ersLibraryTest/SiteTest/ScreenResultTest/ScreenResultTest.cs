﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using OpenQA.Selenium.Remote;

namespace ersLibraryTest.SiteTest.ScreenResultTest
{
    class ScreenResultTest
    {
        public class ScreenResultTestInput
        {
            [FindsBy(How=How.Name, Using="input_value")]
            public IWebElement input_value { get; set; }

            [FindsBy(How=How.Name, Using="submit_input")]
            public IWebElement submit_input { get; set; }

            [FindsBy(How=How.Name, Using="submit_confirm")]
            public IWebElement submit_confirm { get; set; }
        }

        [Test()]
        public void ScreenResultTestCase()
        {
            var listDriver = new List<RemoteWebDriver>();
            try
            {
                for (var i = 0; i < 5; i++)
                {
                    var driver = new FirefoxDriver();
                    listDriver.Add(driver);
                }

                var listSet = new List<AsyncSet>();
                foreach (var driver in listDriver)
                {
                    var set = new AsyncSet();
                    var send = new Action<RemoteWebDriver>(this.CheckSite);
                    set.send = send;
                    set.result = send.BeginInvoke(driver, null, null);
                    listSet.Add(set);
                }

                foreach (var set in listSet)
                {
                    try
                    {
                        set.result.AsyncWaitHandle.WaitOne();
                        set.send.EndInvoke(set.result);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
            finally
            {
                foreach (var driver in listDriver)
                {
                    try
                    {
                        driver.Close();
                    }
                    catch { }
                }
            }
        }

        private void CheckSite(RemoteWebDriver driver)
        {
            var url = "https://bausch-test.ivp.co.jp/test/top/Home/asp/ScreenResultTestInput.asp";
            //var url = "https://dev-ersv7.ivp.co.jp/test/top/Home/asp/ScreenResultTestInput.asp";
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));//10秒
            var pageEnd = By.Id("page_end");

            for (var roopCount = 0; roopCount < 50; roopCount++)
            {
                try
                {
                    Console.WriteLine("::" + DateTime.Now.ToString() + "[" + Thread.CurrentThread.ManagedThreadId + " / " + (roopCount + 1) + "]");
                    driver.Url = url;
                    wait.Until(ExpectedConditions.ElementExists(pageEnd));

                    var validValue = "aaaaa+" + Thread.CurrentThread.ManagedThreadId + "+" + (roopCount + 1) + "+aaaaa";
                    var login = new ScreenResultTestInput();
                    PageFactory.InitElements(driver, login);

                    login.input_value.SendKeys(validValue);
                    login.submit_input.Click();
                    wait.Until(ExpectedConditions.ElementExists(pageEnd));

                    Assert.IsTrue(driver.PageSource.Contains(validValue), validValue + "\r\n" + driver.PageSource);

                    PageFactory.InitElements(driver, login);
                    login.submit_confirm.Click();
                    wait.Until(ExpectedConditions.ElementExists(pageEnd));

                    Assert.IsTrue(driver.PageSource.Contains(validValue), validValue + "\r\n" + driver.PageSource);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return;
                }
            }
        }

        public struct AsyncSet
        {
            public Action<RemoteWebDriver> send;
            public IAsyncResult result;
        }
    }
}
