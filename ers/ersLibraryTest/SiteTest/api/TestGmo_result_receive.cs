﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using System.Net;

namespace ersLibraryTest.SiteTest.api
{
    class TestGmo_result_receive
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var url = "https://ersv7-1-0.local.host:13443/top/api/asp/gmo_result_receive.asp";

            var repositroy = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.pay_in = new[] { EnumPaymentType.CONVENIENCE_STORE };
            criteria.order_payment_status_in = new[] { EnumOrderPaymentStatusType.NOT_PAID };
            criteria.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.LIMIT = 1;
            var listOrder = repositroy.Find(criteria);

            var objOrder = listOrder.First();

            var model = new GmoResultReceive();

            model.ShopID = "tshop00009989";
            model.ShopPass = "**********";
            model.AccessID = objOrder.access_id;
            model.AccessPass = "********************************";
            model.OrderID = objOrder.credit_order_id;
            model.Status = EnumGmoResultStatus.PAYSUCCESS;
            model.Amount = objOrder.total;
            model.TranID = "testtesttest";
            model.TranDate = DateTime.Now.ToString("yyyyMMddHHmmss");
            model.PayType = EnumGmoPayType.Convenience;
            model.CvsCode = objOrder.conv_code;
            model.CvsConfNo = objOrder.conv_conf_no;
            model.CvsReceiptNo = objOrder.conv_receipt_no;
            model.FinishDate = DateTime.Now.ToString("yyyyMMddHHmmss");
            model.ReceiptDate = DateTime.Now.ToString("yyyyMMddHHmmss");
            model.PaymentTerm = objOrder.conv_payment_term.Value.ToString("yyyyMMddHHmmss");
            model.Currency = "JPY";
            model.Tax = 0;

            var SendValues = model.GetPropertiesAsDictionary();

            ErsHttpClient.SetIgnoreInvalidSSL();
            CookieCollection cookies = null;
            var result = ErsHttpClient.PostSend(url, SendValues, ErsEncoding.ShiftJIS, ref cookies);

            Assert.AreEqual("0", result);
        }

        [Test()]
        public void TestMain2()
        {
            var url = "https://ersv7-1-0.local.host:13443/top/api/asp/gmo_result_receive.asp";

            var SendValues = new Dictionary<string, object>();
            SendValues["ShopID"] = "tshop00009989";
            SendValues["ShopPass"] = "**********";
            SendValues["AccessID"] = "afd1459656cdd4f904617210be2488dc";
            SendValues["AccessPass"] = "********************************";
            SendValues["OrderID"] = "30002743-01-0000000169";
            SendValues["Status"] = "PAYSUCCESS";
            SendValues["JobCd"] = "";
            SendValues["Amount"] = "100";
            SendValues["Tax"] = "0";
            SendValues["Currency"] = "JPN";
            SendValues["Forward"] = "";
            SendValues["Method"] = "";
            SendValues["PayTimes"] = "";
            SendValues["TranID"] = "14031100000000213768";
            SendValues["Approve"] = "";
            SendValues["TranDate"] = "20140312212000";
            SendValues["ErrCode"] = "";
            SendValues["ErrInfo"] = "";
            SendValues["PayType"] = "3";
            SendValues["CvsCode"] = "00002";
            SendValues["CvsConfNo"] = "3769";
            SendValues["CvsReceiptNo"] = "WNT39309433";
            SendValues["EdyReceiptNo"] = "";
            SendValues["EdyOrderNo"] = "";
            SendValues["SuicaReceiptNo"] = "";
            SendValues["SuicaOrderNo"] = "";
            SendValues["CustID"] = "";
            SendValues["BkCode"] = "";
            SendValues["ConfNo"] = "";
            SendValues["PaymentTerm"] = "20140321235959";
            SendValues["EncryptReceiptNo"] = "";
            SendValues["FinishDate"] = "20140312212000";
            SendValues["ReceiptDate"] = "20140311210100";
            SendValues["WebMoneyManagementNo"] = "";
            SendValues["WebMoneySettleCode"] = "";
            SendValues["AuPayInfoNo"] = "";
            SendValues["AuPayMethod"] = "";
            SendValues["AuCancelAmount"] = "";
            SendValues["AuCancelTax"] = "";
            SendValues["DocomoSettlementCode"] = "";
            SendValues["DocomoCancelAmount"] = "";
            SendValues["DocomoCancelTax"] = "";
            SendValues["SbTrackingId"] = "";
            SendValues["SbCancelAmount"] = "";
            SendValues["SbCancelTax"] = "";
            SendValues["JibunReceiptNo"] = "";
            SendValues["AuFirstAmount"] = "";
            SendValues["AuFirstTax"] = "";
            SendValues["AuAccountMonth"] = "";
            SendValues["AuContinueAccountId"] = "JcbPrecaSalesCode=";

            ErsHttpClient.SetIgnoreInvalidSSL();
            CookieCollection cookies = null;
            var result = ErsHttpClient.PostSend(url, SendValues, ErsEncoding.ShiftJIS, ref cookies);

            Assert.AreEqual("0", result);
        }

        public class GmoResultReceive
        : ErsModelBase, IGmoResultReceive
        {
            /// <summary>
            /// CHAR 13 ショップID
            /// </summary>
            public string ShopID { get; set; }

            /// <summary>
            /// CHAR 10 ショップパスワード “*” 10桁固定
            /// </summary>
            public string ShopPass { get; set; }

            /// <summary>
            /// CHAR 32 取引ID
            /// </summary>
            public string AccessID { get; set; }

            /// <summary>
            /// CHAR 32 取引パスワード “*” 32桁固定
            /// </summary>
            public string AccessPass { get; set; }

            /// <summary>
            /// CHAR 27 オーダーID
            /// </summary>
            public string OrderID { get; set; }

            /// <summary>
            /// CHAR - 現状態
            /// </summary>
            public EnumGmoResultStatus? Status { get; set; }

            /// <summary>
            /// CHAR - 処理区分
            /// </summary>
            public EnumGmoJobCd? JobCd { get; set; }

            /// <summary>
            /// NUMBER 10 利用金額
            /// </summary>
            public int? Amount { get; set; }

            /// <summary>
            /// NUMBER 10 税送料
            /// </summary>
            public int? Tax { get; set; }

            /// <summary>
            /// CHAR 3 通貨コード決済に利用された通貨を返却します。
            /// </summary>
            public string Currency { get; set; }

            /// <summary>
            /// CHAR 7 仕向先会社コードカード・ｉＤネット決済時のみ返却
            /// </summary>
            public string Forward { get; set; }

            /// <summary>
            /// CHAR 1 支払方法
            /// </summary>
            public EnumGmoMethod? Method { get; set; }

            /// <summary>
            /// NUMBER 2 支払回数カード・ｉＤネット決済時のみ返却
            /// </summary>
            public int? PayTimes { get; set; }

            /// <summary>
            /// CHAR 28 トランザクションID
            /// </summary>
            public string TranID { get; set; }

            /// <summary>
            /// CHAR 7 承認番号カード・ｉＤネット決済時のみ返却
            /// </summary>
            public string Approve { get; set; }

            /// <summary>
            /// CHAR 14 処理日付 yyyyMMddHHmmss書式
            /// </summary>
            public string TranDate { get; set; }

            /// <summary>
            /// CHAR 3 エラーコードエラー発生時のみ ※2
            /// </summary>
            public string ErrCode { get; set; }

            /// <summary>
            /// CHAR 9 エラー詳細コードエラー発生時のみ ※2
            /// </summary>
            public string ErrInfo { get; set; }

            /// <summary>
            /// CHAR 1 決済方法
            /// </summary>
            public EnumGmoPayType? PayType { get; set; }

            /// <summary>
            /// CHAR 5 支払先コンビニコード
            /// </summary>
            public EnumConvCode? CvsCode { get; set; }

            /// <summary>
            /// CHAR 20 コンビニ確認番号
            /// </summary>
            public string CvsConfNo { get; set; }

            /// <summary>
            /// CHAR 32 コンビニ確認番号
            /// </summary>
            public string CvsReceiptNo { get; set; }

            /// <summary>
            /// CHAR 16 Edy受付番号
            /// </summary>
            public string EdyReceiptNo { get; set; }

            /// <summary>
            /// CHAR 40 Edy注文番号
            /// </summary>
            public string EdyOrderNo { get; set; }

            /// <summary>
            /// CHAR 9 Suica受付番号
            /// </summary>
            public string SuicaReceiptNo { get; set; }

            /// <summary>
            /// CHAR 40 Suica注文番号
            /// </summary>
            public string SuicaOrderNo { get; set; }

            /// <summary>
            /// CHAR 11 Pay-easyお客様番号
            /// </summary>
            public string CustID { get; set; }

            /// <summary>
            /// CHAR 5 Pay-easy収納機関番号
            /// </summary>
            public string BkCode { get; set; }

            /// <summary>
            /// CHAR 20 Pay-easy確認番号
            /// </summary>
            public string ConfNo { get; set; }

            /// <summary>
            /// CHAR 14 Pay-easy支払期限日時(yyyyMMddHHmmss書式)
            /// </summary>
            public string PaymentTerm { get; set; }

            /// <summary>
            /// CHAR 128 Pay-easy暗号化決済番号
            /// </summary>
            public string EncryptReceiptNo { get; set; }

            /// <summary>
            /// CHAR 14 入金確定日時(yyyyMMddHHmmss書式)(モバイルSuica・Edy・コンビニ・Pay-easy)
            /// </summary>
            public string FinishDate { get; set; }

            /// <summary>
            /// CHAR 14 受付日時(yyyyMMddHHmmss書式)(モバイルSuica・Edy・コンビニ・Pay-easy)
            /// </summary>
            public string ReceiptDate { get; set; }

            /// <summary>
            /// CHAR 16 購入に使用されたWebMoneyの管理番号
            /// </summary>
            public string WebMoneyManagementNo { get; set; }

            /// <summary>
            /// CHAR 25 WebMoneyセンターが返却した決済コード
            /// </summary>
            public string WebMoneySettleCode { get; set; }
        }
    }
}
