﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersLibraryTest.jp.co.ivp.ers.util
{
    public class ErsEncryptionTest
         : TestCommon
    {
        [TestCase("test1-")]
        [TestCase("test1-")]
        public void HexEncodeDecodeTest(string testValue)
        {
            var objEnc = ErsFactory.ersUtilityFactory.getErsEncryption();

            var encValue = objEnc.HexEncode(testValue);
            Assert.AreNotEqual(testValue, encValue, "encryption is not working");

            var decValue = objEnc.HexDecode(encValue);
            Assert.AreEqual(testValue, decValue, "decryption is not working");
        }

        [Test()]
        public void EncodeBuckupTest()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var path = setup.site_log_path;
            if (!path.EndsWith("\\"))
            {
                path += "\\";
            }

            var testValue = "あいうえおかくけこさしすせそ";
            var testpath = path + "test.txt";

            var objEnc = ErsFactory.ersUtilityFactory.getErsEncryption();
            objEnc.EncodeBuckup(testValue, testpath);

            this.TestWithMultiThread(testValue, testpath, objEnc);
        }

        /// <summary>
        /// マルチスレッドテスト
        /// </summary>
        /// <param name="testValue"></param>
        /// <param name="testpath"></param>
        /// <param name="objEnc"></param>
        private void TestWithMultiThread(string testValue, string testpath, ErsEncryption objEnc)
        {
            var listBackUp = new List<Buckup>();
            var listSet = new List<AsyncSet>();
            for (var i = 0; i < 1000; i++)
            {
                Buckup backup = new Buckup(objEnc.EncodeBuckup);
                listBackUp.Add(backup);
            }

            foreach (var backup in listBackUp)
            {
                var set = new AsyncSet();
                set.backup = backup;
                set.result = backup.BeginInvoke(testValue, testpath, null, null);
                listSet.Add(set);
            }

            foreach (var set in listSet)
            {
                set.result.AsyncWaitHandle.WaitOne();
                set.backup.EndInvoke(set.result);
            }
        }

        public delegate bool Buckup(string value, string path);

        public struct AsyncSet
        {
            public Buckup backup;
            public IAsyncResult result;
        }
    }
}
