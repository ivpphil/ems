﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersLibraryTest.jp.co.ivp.ers.state.strategy
{
    class ObtainRansuAdminStgyTest
         : TestCommon
    {
        public class ObtainRansuAdminStgy
            : global::jp.co.ivp.ers.state.strategy.ObtainRansuAdminStgy
        {
            public bool RetryActionMethodTest(Action actionMethod)
            {
                return this.RetryActionMethod(actionMethod);
            }
        }

        public class ObtainRansuStgy
            : global::jp.co.ivp.ers.state.strategy.ObtainRansuStgy
        {
            public bool RetryActionMethodTest(Action actionMethod)
            {
                return this.RetryActionMethod(actionMethod);
            }
        }

        public class ObtainRansuContactStgy
            : global::jp.co.ivp.ers.state.strategy.ObtainRansuContactStgy
        {
            public bool RetryActionMethodTest(Action actionMethod)
            {
                return this.RetryActionMethod(actionMethod);
            }
        }

        [Test()]
        public void RetryActionMethodTest()
        {
            var stgyAdmin = new ObtainRansuAdminStgy();
            if (!stgyAdmin.RetryActionMethodTest(() => this.InsertDuplicateValue()))
            {

            }

            var stgy = new ObtainRansuStgy();
            if (!stgy.RetryActionMethodTest(() => this.InsertDuplicateValue()))
            {

            }

            var stgyContact = new ObtainRansuContactStgy();
            if (!stgy.RetryActionMethodTest(() => this.InsertDuplicateValue()))
            {

            }

            Assert.Pass();
        }

        private void InsertDuplicateValue()
        {
            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuAdminRepository();
            var objRansu = ErsFactory.ersSessionStateFactory.GetErsRansuAdmin();
            objRansu.ransu = "testtesttest";
            objRansu.user_cd = "000000";
            repository.Insert(objRansu);
            repository.Insert(objRansu);
        }
    }
}
