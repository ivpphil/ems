﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.mvc;
using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers;

namespace ersLibraryTest.jp.co.ivp.ers.sendmail
{
    public class ErsSmptTest
        : TestCommon
    {
        [Test()]
        public void SendTest()
        {
            //正常チェック
            var sendmail = ErsFactory.ersMailFactory.GetErsSmtp();
            var mailFrom = "フロム <nagaike@ivp.co.jp>";
            var replyto = "とぅー <y.nagaike@gmail.com>";
            var mailTo = "あて先 <nagaike@ivp.co.jp>";
            var cc = new[] { "CCに <nagaike@ivp.co.jp>", "CCに(2通目) <y.nagaike@gmail.com>" };
            var bcc = new[] { "BCCに <nagaike@ivp.co.jp>", "BCCに(2通目) <y.nagaike@gmail.com>" };
            var subject = "テスト題名";
            var body = "テキスト本文\r\n.←文頭にドット打つテスト\r\n↓文末にドット打つテスト\r\n.";
            var htmlBody = "<a href=\"http://www.ivp.co.jp/\">リンクだ</a>";
            sendmail.SendSynchronous(mailFrom, replyto, mailTo, cc, bcc, subject, body, htmlBody);

            //send cc and bcc null
            cc = null;
            bcc = null;
            sendmail.SendSynchronous(mailFrom, replyto, mailTo, cc, bcc, subject, body, htmlBody);

            //send cc and bcc empty email
            cc = new string[] { "" };
            bcc = new string[] { "" };
            sendmail.SendSynchronous(mailFrom, replyto, mailTo, cc, bcc, subject, body, htmlBody);
        }

        [Test()]
        public void SendTestForSmtpAuth()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            
            var logFileUserName = setup.logFileUserName;
            var logFileUserPassword = setup.logFileUserPassword;

            var smtpHostName = "********";
            var smtpPort = 587;
            var smtpAuthId = "********";
            var smtpAuthPassword = "********";
            var smtpOperationLogPath = setup.smtpOperationLogPath;
            var smtpErrorLogPath = setup.smtpErrorLogPath;

            //send with retry
            var retryProperty = ErsFactory.ersMailFactory.GetErsSmtpRetryProperty();
            retryProperty.SetEnableRetry(1, 1, setup.smtpRetryTextPath);

            var sendmail = new ErsSmtp(smtpHostName, smtpPort, logFileUserName, logFileUserPassword, smtpOperationLogPath, smtpErrorLogPath, retryProperty, smtpAuthId, smtpAuthPassword);

            //正常チェック
            var mailFrom = "********";
            var mailTo = "********************";
            var subject = "テスト題名";
            var body = "テストテキスト";
            sendmail.SendSynchronous(mailFrom, null, mailTo, null, null, subject, body, null);
        }

        [Test()]
        public void SendErrorTest()
        {
            var mailFrom = "フロム <nagaike@ivp.co.jp>";
            var replyto = "とぅー <y.nagaike@gmail.com>";
            var mailTo = "あて先 <nagaike@ivp.co.jp>";
            var cc = new[] { "CCに <nagaike@ivp.co.jp>", "CCに(2通目) <y.nagaike@gmail.com>" };
            var bcc = new[] { "BCCに <nagaike@ivp.co.jp>", "BCCに(2通目) <y.nagaike@gmail.com>" };
            var subject = "テスト題名";
            var body = "テキスト本文\r\n.←文頭にドット打つテスト\r\n↓文末にドット打つテスト\r\n.";
            var htmlBody = "<a href=\"http://www.ivp.co.jp/\">リンクだ</a>";

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var logFileUserName = setup.logFileUserName;
            var logFileUserPassword = setup.logFileUserPassword;

            var smtpHostName = "127.0.0.2";
            var smtpPort = setup.smtpPort;
            var smtpOperationLogPath = setup.smtpOperationLogPath;
            var smtpErrorLogPath = setup.smtpErrorLogPath;

            //send with retry
            var retryProperty = ErsFactory.ersMailFactory.GetErsSmtpRetryProperty();
            retryProperty.SetEnableRetry(1, 1, setup.smtpRetryTextPath);

            var sendmail = new ErsSmtp(smtpHostName, smtpPort, logFileUserName, logFileUserPassword, smtpOperationLogPath, smtpErrorLogPath, retryProperty);

            Assert.Throws<ErsSmtpConnectionException>(() => sendmail.SendSynchronous(mailFrom, replyto, mailTo, cc, bcc, subject, body, htmlBody));

            //send without retry
            retryProperty = ErsFactory.ersMailFactory.GetErsSmtpRetryProperty();
            retryProperty.SetDisableRetry();

            sendmail = new ErsSmtp(smtpHostName, smtpPort, logFileUserName, logFileUserPassword, smtpOperationLogPath, smtpErrorLogPath, retryProperty);

            Assert.Throws<ErsSmtpConnectionException>(() => sendmail.SendSynchronous(mailFrom, replyto, mailTo, cc, bcc, subject, body, htmlBody));
        }

        [Test()]
        [ExpectedException(ExpectedException = typeof(System.Exception), ExpectedMessage = "error at connect")]
        public void SendTestCcOrBccNull()
        {
            //エラーチェック
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var logFileUserName = setup.logFileUserName;
            var logFileUserPassword = setup.logFileUserPassword;

            var smtpHostName = setup.smtpHostName;
            var smtpPort = setup.smtpPort;
            var smtpOperationLogPath = setup.smtpOperationLogPath;
            var smtpErrorLogPath = setup.smtpErrorLogPath;

            var retryProperty = ErsFactory.ersMailFactory.GetErsSmtpRetryProperty();
            retryProperty.SetDisableRetry();

            var sendmail = new ErsSmtp("127.0.0.1", smtpPort, logFileUserName, logFileUserPassword, smtpOperationLogPath, smtpErrorLogPath, retryProperty);

            var mailFrom = "フロム <nagaike@ivp.co.jp>";
            var replyto = "とぅー <y.nagaike@gmail.com>";
            var mailTo = "あて先 <nagaike@ivp.co.jp>";
            string[] cc = null;
            string[] bcc = null;
            var subject = "テスト題名";
            var body = "テキスト本文\r\n.←文頭にドット打つテスト\r\n↓文末にドット打つテスト\r\n.";
            var htmlBody = "<a href=\"http://www.ivp.co.jp/\">リンクだ</a>";
            sendmail.SendSynchronous(mailFrom, replyto, mailTo, cc, bcc, subject, body, htmlBody);

        }

        [Test()]
        public void SendTestMultiThread()
        {
            var sendmail = ErsFactory.ersMailFactory.GetErsSmtp();
            var mailFrom = "フロム <nagaike@ivp.co.jp>";
            var replyto = "とぅー <y.nagaike@gmail.com>";
            var mailTo = "あて先 <nagaike@ivp.co.jp>";
            var cc = new[] { "CCに <nagaike@ivp.co.jp>", "CCに(2通目) <y.nagaike@gmail.com>" };
            var bcc = new[] { "BCCに <nagaike@ivp.co.jp>", "BCCに(2通目) <y.nagaike@gmail.com>" };
            var subject = "テスト題名";
            var body = "テキスト本文\r\n.←文頭にドット打つテスト\r\n↓文末にドット打つテスト\r\n.";
            var htmlBody = "<a href=\"http://www.ivp.co.jp/\">リンクだ</a>";

            var listSend = new List<Send>();
            var listSet = new List<AsyncSet>();
            for (var i = 0; i < 1000; i++)
            {
                Send send = new Send(sendmail.Send);
                listSend.Add(send);
            }

            foreach (var send in listSend)
            {
                var set = new AsyncSet();
                set.send = send;
                set.result = send.BeginInvoke(mailFrom, replyto, mailTo, cc, bcc, subject, body, htmlBody, null, null);
                listSet.Add(set);
            }

            foreach (var set in listSet)
            {
                try
                {
                    set.result.AsyncWaitHandle.WaitOne();
                    set.send.EndInvoke(set.result);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public delegate void Send(string mailFrom, string replyto, string mailTo, string[] cc, string[] bcc, string subject, string body, string htmlBody);

        public struct AsyncSet
        {
            public Send send;
            public IAsyncResult result;
        }
    }
}
