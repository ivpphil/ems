﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersLibraryTest.jp.co.ivp.ers.transaction
{
    class TransactionConflictTest
           : TestCommon
    {
        [TestCase(10, 100)]
        public void TestConflict(int threadCount, int repeatCount)
        {
            var connectionString = ErsFactory.ersUtilityFactory.getSetup().getConnectionStrings();
            using (var conn = new NpgsqlConnection(connectionString))
            {
                conn.Open();
                NpgsqlCommand command;
                command = new NpgsqlCommand("DELETE FROM check_t WHERE id = 100", conn);
                command.ExecuteNonQuery();

                command = new NpgsqlCommand("INSERT INTO check_t (id, serial, dt) VALUES (100, 0, current_timestamp)", conn);
                command.ExecuteNonQuery();

                conn.Close();
            }

            var listUpdateThread = new List<UpdateThread>();
            var listSet = new List<AsyncSet>();
            for (var i = 0; i < threadCount; i++)
            {
                var updateThread = new UpdateThread(UpdateThereadFunction);
                listUpdateThread.Add(updateThread);
            }

            foreach (var updateThread in listUpdateThread)
            {
                var set = new AsyncSet();
                set.updateThread = updateThread;
                set.result = updateThread.BeginInvoke(connectionString, repeatCount, null, null);
                listSet.Add(set);
            }

            foreach (var set in listSet)
            {
                try
                {
                    set.result.AsyncWaitHandle.WaitOne();
                    set.updateThread.EndInvoke(set.result);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            int actualCount;
            using (var conn = new NpgsqlConnection(connectionString))
            {
                conn.Open();
                NpgsqlCommand command;
                command = new NpgsqlCommand("SELECT serial FROM check_t WHERE id = 100", conn);
                actualCount = Convert.ToInt32(command.ExecuteScalar());

                command = new NpgsqlCommand("DELETE FROM check_t WHERE id = 100", conn);
                command.ExecuteNonQuery();

                conn.Close();

            }
            Assert.AreEqual(threadCount * repeatCount, actualCount);
        }


        public struct AsyncSet
        {
            public UpdateThread updateThread;
            public IAsyncResult result;
        }
        public delegate void UpdateThread(string connectionString, int repeatCount);

        public void UpdateThereadFunction(string connectionString, int repeatCount)
        {
            using (var conn = new NpgsqlConnection(connectionString))
            {
                for (var i = 0; i < repeatCount; i++)
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        NpgsqlCommand command = new NpgsqlCommand("UPDATE check_t SET serial = serial + 1 WHERE id = (SELECT MAX(id) FROM check_t)", conn);
                        command.ExecuteNonQuery();
                        tx.Commit();
                    }
                    conn.Close();
                }
            }
        }
    }
}
