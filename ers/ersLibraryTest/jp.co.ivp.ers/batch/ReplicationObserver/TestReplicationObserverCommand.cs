﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.ReplicationObserver;
using jp.co.ivp.ers.batch;

namespace ersLibraryTest.jp.co.ivp.ers.batch.ReplicationObserver
{
    class TestReplicationObserverCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new ReplicationObserverCommand();
            targetClass.Run(null, null, new Dictionary<string, object>(), null, null);
        }
    }
}
