﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.batch.MemberRank;

namespace ersLibraryTest.jp.co.ivp.ers.batch.MemberRank
{
    class TestMemberRankCommand
         : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            //バッチを実行する
            var objErsBatchExecuter = new ErsBatchExecuter();

            var commandClassInfo = typeof(MemberRankCommand);

            var batchDataContainer = new BatchDataContainer();
            batchDataContainer.executeDate = DateTime.Now;
            batchDataContainer.batchId = commandClassInfo.Name;
            batchDataContainer.batchName = commandClassInfo.Name;
            batchDataContainer.class_name = commandClassInfo.FullName;
            batchDataContainer.enableMutex = false;
            batchDataContainer.batchLocation = System.Reflection.Assembly.GetCallingAssembly().Location;

            objErsBatchExecuter.ExecuteClass(batchDataContainer);
        }
   }
}
