﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.DeleteTables;

namespace ersLibraryTest.jp.co.ivp.ers.batch.DeleteTables
{
    class TestDeleteTablesCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new DeleteTablesCommand();
            targetClass.Run(null, null, new Dictionary<string, object>(), null, null);
        }
    }
}
