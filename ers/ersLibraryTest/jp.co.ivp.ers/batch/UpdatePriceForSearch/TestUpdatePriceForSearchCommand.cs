﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers.batch.UpdatePriceForSearch;

namespace ersLibraryTest.jp.co.ivp.ers.batch.UpdatePriceForSearch
{
    class TestUpdatePriceForSearchCommand
         : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new UpdatePriceForSearchCommand();
            targetClass.Run(null, null, null, null, null);
        }
    }
}
