﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.DeleteSpecifiedColumn;

namespace ersLibraryTest.jp.co.ivp.ers.batch.DeleteSpecifiedColumn
{
    class TestDeleteSpecifiedColumnCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new DeleteSpecifiedColumnCommand();
            targetClass.Run(null, DateTime.Now, null, null, null);
        }
    }
}
