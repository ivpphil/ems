﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers.batch.UpdateProductKeyword;
using NUnit.Framework;

namespace ersLibraryTest.jp.co.ivp.ers.batch.UpdateProductKeyword
{
    public class TestUpdateProductKeywordCommand
         : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new UpdateProductKeywordCommand();
            targetClass.Run(null, DateTime.Now, new Dictionary<string, object>() { { "last_success_date", DateTime.Now.AddMinutes(-30) } }, null, null);
        }
    }
}
