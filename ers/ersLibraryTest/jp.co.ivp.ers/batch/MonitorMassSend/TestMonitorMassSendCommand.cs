﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using System.Configuration;
using NUnit.Framework;
using jp.co.ivp.ers.batch.util;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.batch.MonitorMassSend;

namespace ersLibraryTest.jp.co.ivp.ers.batch.MonitorMassSend
{
    class TestMonitorMassSendCommand
         : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            //バッチを実行する
            var objErsBatchExecuter = new ErsBatchExecuter();

            var commandClassInfo = typeof(MonitorMassSendCommand);

            var batchDataContainer = new BatchDataContainer();
            batchDataContainer.executeDate = DateTime.Now;
            batchDataContainer.batchId = commandClassInfo.Name;
            batchDataContainer.batchName = commandClassInfo.Name;
            batchDataContainer.class_name = commandClassInfo.FullName;
            batchDataContainer.enableMutex = false;
            batchDataContainer.batchLocation = System.Reflection.Assembly.GetCallingAssembly().Location;

            objErsBatchExecuter.ExecuteClass(batchDataContainer);
        }
    }
}
