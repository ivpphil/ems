﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.CreateListForStepMail;

namespace ersLibraryTest.jp.co.ivp.ers.batch.CreateListForStepMail
{
    class TestCreateListForStepMailCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new CreateListForStepMailCommand();
            targetClass.Run(null, DateTime.Now, new Dictionary<string, object>() { { "datetime", DateTime.Now.Date } }, null, null);
        }
    }
}
