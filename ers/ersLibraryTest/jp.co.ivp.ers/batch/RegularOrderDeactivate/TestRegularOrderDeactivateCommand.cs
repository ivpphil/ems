﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers.batch.RegularOrderDeactivate;
using jp.co.ivp.ers;
using Npgsql;
using Dapper;
using FluentAssertions;

namespace ersLibraryTest.jp.co.ivp.ers.batch.RegularOrderDeactivate
{
    [TestFixture(Category = "UnitTest")]
    class TestRegularOrderDeactivateCommand
         : TestCommon
    {
        public void TestMain()
        {
            var targetClass = new RegularOrderDeactivateCommand();
            targetClass.Run(null, null, new Dictionary<string, object>(), null, null);
        }

        [Test()]
        [RegularOrderTestData]
        public void ers_default_30()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                // update member_card to status 1
                db.Execute("UPDATE member_card_t SET update_status = 0 WHERE id = @id", new { id = RegularOrderTestDataAttribute.member_card_id });

                this.TestMain();

                var checkQueryError = db.Query(@"SELECT * FROM regular_detail_t WHERE id = @id", new { id = RegularOrderTestDataAttribute.regularDetailId });

                var afterRegularDetail = checkQueryError.SingleOrDefault();
                ((DateTime?)afterRegularDetail.delete_date).Should().NotHaveValue();

                // update member_card to status 0
                db.Execute("UPDATE member_card_t SET update_status = 1 WHERE id = @id", new { id = RegularOrderTestDataAttribute.member_card_id });

                this.TestMain();

                var checkQuerySuccess = db.Query(@"SELECT * FROM regular_detail_t WHERE id = @id", new { id = RegularOrderTestDataAttribute.regularDetailId });
                afterRegularDetail = checkQuerySuccess.SingleOrDefault();
                ((DateTime)afterRegularDetail.delete_date).ToLongDateString().Should().Be(((DateTime)afterRegularDetail.next_date).ToLongDateString());

                db.Close();
            }
        }
    }

    public class RegularOrderTestDataAttribute
          : Attribute, ITestAction
    {
        [ThreadStatic]
        public static string mcode;

        [ThreadStatic]
        public static int member_card_id;

        [ThreadStatic]
        public static int regularDetailId;

        public void BeforeTest(TestDetails testDetails)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                // Prepare test data
                mcode = db.Query(@"
                    SELECT * FROM member_t WHERE 
                    EXISTS (SELECT * FROM member_card_t WHERE mcode = member_t.mcode AND active = 1)
                    AND EXISTS (SELECT * FROM regular_detail_t WHERE pay = 1 AND mcode = member_t.mcode AND (delete_date IS NULL OR delete_date > next_date))
                    AND member_t.email LIKE '%@ivp.co.jp'
                    ORDER BY id ASC
                    LIMIT 1
                ").SingleOrDefault().mcode;

                var regularDetail = db.Query(@"SELECT * FROM regular_detail_t WHERE pay = 1 AND mcode = @mcode ORDER BY id ASC LIMIT 1", new { mcode = mcode }).SingleOrDefault();

                regularDetailId = regularDetail.id;
                member_card_id = regularDetail.member_card_id;

                db.Execute("UPDATE regular_detail_t SET delete_date = NULL WHERE id = @id", new { id = regularDetailId });

                db.Close();
            }
        }

        public void AfterTest(TestDetails testDetails)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                db.Execute("UPDATE regular_detail_t SET delete_date = NULL WHERE id = @id", new { id = regularDetailId });

                db.Close();
            }
        }

        public ActionTargets Targets
        {
            get { return ActionTargets.Test; }
        }
    }
}
