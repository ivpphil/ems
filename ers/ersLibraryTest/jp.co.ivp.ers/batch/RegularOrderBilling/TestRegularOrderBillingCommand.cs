﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.RegularOrderBilling;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using Npgsql;
using Dapper;
using FluentAssertions;

namespace ersLibraryTest.jp.co.ivp.ers.batch.RegularOrderBilling
{
    [TestFixture(Category = "UnitTest")]
    class TestRegularOrderBillingCommand
         : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new RegularOrderBillingCommand();
            targetClass.Run(null, null, new Dictionary<string, object>(), null, null);
        }

        [Test()]
        [RegularOrderTestData]
        public void ers_default_1()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                // update member_card to status 1
                db.Execute("UPDATE member_card_t SET update_status = 1 WHERE id = @id", new { id = RegularOrderTestDataAttribute.member_card_id });
                
                this.TestMain();

                var checkQueryError = db.Query(@"SELECT * FROM regular_detail_t WHERE id = @id", new { id = RegularOrderTestDataAttribute.regularDetailId });

                var afterRegularDetail = checkQueryError.SingleOrDefault();
                ((DateTime)afterRegularDetail.next_date).ToLongDateString().Should().Be(RegularOrderTestDataAttribute.testDate.ToLongDateString());

                // update member_card to status 0
                db.Execute("UPDATE member_card_t SET update_status = 0 WHERE id = @id", new { id = RegularOrderTestDataAttribute.member_card_id });

                this.TestMain();

                var checkQuerySuccess = db.Query(@"SELECT * FROM regular_detail_t WHERE id = @id", new { id = RegularOrderTestDataAttribute.regularDetailId });
                afterRegularDetail = checkQuerySuccess.SingleOrDefault();
                ((DateTime)afterRegularDetail.next_date).ToLongDateString().Should().NotBe(RegularOrderTestDataAttribute.testDate.ToLongDateString());

                db.Close();
            }
        }

        [Test()]
        [RegularOrderTestData]
        public void ers_default_2()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                // update member_card to status 1
                db.Execute("UPDATE regular_detail_t SET next_date = @next_date, amount = 1, price = @total, total = @total WHERE id = @id",
                    new
                    {
                        id = RegularOrderTestDataAttribute.regularDetailId,
                        total = setup.gmo_compensable_total,
                        next_date = RegularOrderTestDataAttribute.testDate
                    });

                this.TestMain();

                var checkQueryNotAuth = db.Query(@"SELECT * FROM d_master_t WHERE d_no IN (SELECT d_no FROM ds_master_t WHERE regular_detail_id = @id ORDER BY id DESC LIMIT 1)", new { id = RegularOrderTestDataAttribute.regularDetailId });

                var order = checkQueryNotAuth.SingleOrDefault();
                ((int)order.order_payment_status).Should().Be(20);
                ((string)order.credit_order_id).Should().NotBeNull();

                // update member_card to status 0
                db.Execute("UPDATE regular_detail_t SET next_date = @next_date, amount = 1, price = @total, total = @total WHERE id = @id",
                    new
                    {
                        id = RegularOrderTestDataAttribute.regularDetailId,
                        total = setup.gmo_compensable_total + 1,
                        next_date = RegularOrderTestDataAttribute.testDate
                    });

                this.TestMain();

                var checkQueryAuth = db.Query(@"SELECT * FROM d_master_t WHERE d_no IN (SELECT d_no FROM ds_master_t WHERE regular_detail_id = @id ORDER BY id DESC LIMIT 1)", new { id = RegularOrderTestDataAttribute.regularDetailId });
                order = checkQueryAuth.SingleOrDefault();
                ((int)order.order_payment_status).Should().Be(10);
                ((string)order.credit_order_id).Should().NotBeNull();

                db.Close();
            }
        }
    }

    public class RegularOrderTestDataAttribute
          : Attribute, ITestAction
    {
        [ThreadStatic]
        public static DateTime testDate;

        [ThreadStatic]
        public static string mcode;

        [ThreadStatic]
        public static int member_card_id;

        [ThreadStatic]
        public static int regularDetailId;

        public void BeforeTest(TestDetails testDetails)
        {
            testDate = DateTime.Now;

            var setup = ErsFactory.ersUtilityFactory.getSetup();
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                // Prepare test data
                mcode = db.Query(@"
                    SELECT * FROM member_t WHERE 
                    EXISTS (SELECT * FROM member_card_t WHERE mcode = member_t.mcode AND active = 1)
                    AND EXISTS (SELECT * FROM regular_detail_t WHERE pay = 1 AND mcode = member_t.mcode AND (delete_date IS NULL OR delete_date > next_date))
                    AND member_t.email LIKE '%@ivp.co.jp'
                    ORDER BY id ASC
                    LIMIT 1
                ").SingleOrDefault().mcode;

                var regularDetail = db.Query(@"SELECT * FROM regular_detail_t WHERE pay = 1 AND mcode = @mcode ORDER BY id ASC LIMIT 1", new { mcode = mcode }).SingleOrDefault();

                regularDetailId = regularDetail.id;
                member_card_id = regularDetail.member_card_id;

                db.Execute("UPDATE stock_t SET stock = 100 WHERE scode = @scode", new { scode = regularDetail.scode });
                
                db.Execute("UPDATE regular_detail_t SET next_date = @next_date WHERE id = @id", new { next_date = testDate, id = regularDetailId });

                db.Execute("DELETE FROM d_master_t WHERE d_no IN (SELECT d_no FROM ds_master_t WHERE regular_detail_id = @id) ", new { id = regularDetailId });

                db.Execute("DELETE FROM ds_master_t WHERE regular_detail_id = @id ", new { id = regularDetailId });

                db.Close();
            }
        }

        public void AfterTest(TestDetails testDetails)
        {
        }

        public ActionTargets Targets
        {
            get { return ActionTargets.Test; }
        }
    }
}
