﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using System.Configuration;
using NUnit.Framework;
using jp.co.ivp.ers.batch.util;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.batch.GivePointToMember;

namespace ersLibraryTest.jp.co.ivp.ers.batch.GivePointToMember
{
    class TestGivePointToMemberCommand
         : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            //バッチを実行する
            var objErsBatchExecuter = new ErsBatchExecuter();

            var GivePointToMemberCommand = typeof(GivePointToMemberCommand);

            var batchDataContainer = new BatchDataContainer();
            batchDataContainer.executeDate = DateTime.Now;
            batchDataContainer.batchId = GivePointToMemberCommand.Name;
            batchDataContainer.batchName = GivePointToMemberCommand.Name;
            batchDataContainer.class_name = GivePointToMemberCommand.FullName;
            batchDataContainer.enableMutex = false;
            batchDataContainer.batchLocation = System.Reflection.Assembly.GetCallingAssembly().Location;

            objErsBatchExecuter.ExecuteClass(batchDataContainer);
        }
    }
}
