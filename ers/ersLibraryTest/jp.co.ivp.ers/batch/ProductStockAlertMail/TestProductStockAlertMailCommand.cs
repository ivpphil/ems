﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers.batch.ProductStockAlertMail;

namespace ersLibraryTest.jp.co.ivp.ers.batch.ProductStockAlertMail
{
    public class TestProductStockAlertMailCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new ProductStockAlertMailCommand();
            targetClass.Run(null, null, new Dictionary<string, object>(), null, null);
        }
    }
}
