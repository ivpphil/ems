﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.batch.util;
using System.Configuration;

namespace ersLibraryTest.jp.co.ivp.ers.batch
{
    class TestTaskScheduler
          : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var executeDate = DateTime.Now;
            var targetBatch = new BatchDataContainer();
            targetBatch.batchId = "UpdatePriceForSearch";
            targetBatch.batchName  = "UpdatePriceForSearch";
            targetBatch.schedule = "* * * * *";
            targetBatch.lastDate = executeDate.AddDays(-1);
            targetBatch.options = "".PadLeft(3000, '*');
            targetBatch.executeDate = executeDate;

            var batchLocation = ConfigurationManager.AppSettings["ersRoot"];
            targetBatch.batchLocation = new Uri(new Uri(batchLocation), "../../batch/TaskScheduler/bin/Debug/TaskScheduler.exe").AbsolutePath;

            ErsBatchProcess.PrepareProcessExeFile(targetBatch);
            ErsBatchProcess.ExecuteProcess(targetBatch, EnumBatchMode.Execute);
        }
    }
}
