﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.SendRegularOrderMail;

namespace ersLibraryTest.jp.co.ivp.ers.batch.SendRegularOrderMail
{
    class TestSendRegularOrderMailCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new SendRegularOrderMailCommand();
            targetClass.Run(null, DateTime.Now, new Dictionary<string, object>(), null, null);
        }
    }
}
