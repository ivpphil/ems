﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using System.Configuration;
using NUnit.Framework;
using jp.co.ivp.ers.batch.util;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.batch.CreditCapturePayment;

namespace ersLibraryTest.jp.co.ivp.ers.batch.CreditCapturePayment
{
    class TestCreditCapturePaymentCommand
         : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            //バッチを実行する
            var objErsBatchExecuter = new ErsBatchExecuter();

            var CreditCapturePaymentCommand = typeof(CreditCapturePaymentCommand);

            var batchDataContainer = new BatchDataContainer();
            batchDataContainer.executeDate = DateTime.Now;
            batchDataContainer.batchId = CreditCapturePaymentCommand.Name;
            batchDataContainer.batchName = CreditCapturePaymentCommand.Name;
            batchDataContainer.class_name = CreditCapturePaymentCommand.FullName;
            batchDataContainer.enableMutex = false;
            batchDataContainer.batchLocation = System.Reflection.Assembly.GetCallingAssembly().Location;

            objErsBatchExecuter.ExecuteClass(batchDataContainer);
        }
    }
}
