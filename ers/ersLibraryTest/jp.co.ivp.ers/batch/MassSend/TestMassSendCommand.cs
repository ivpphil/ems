﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.MassSend;
using System.Configuration;
using jp.co.ivp.ers;
using jp.co.ivp.ers.sendmail.mass_send;

namespace ersLibraryTest.jp.co.ivp.ers.batch.MassSend
{
    class TestMassSendCommand
         : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var batchExePath = ConfigurationManager.AppSettings["batchExePath"];
            var targetClass = new MassSendCommand();
            var options = new Dictionary<string, object>()
            {
                {"processID", "767"},
                {"idFrom", "4034"},
                {"idTo", "4034"},
                {"mailFrom", "nagaike <appli_dev@ivp.co.jp>"},
                {"replayTo", "maveronica_sison@yahoo.com"},
                {"subject", "tes"},
                {"body", "tes"},
                {"html_body", "<p>te</p>"},
                {"feature_body", "tes"},
                {"ExecuteDateTime", "2014/01/23 13:17:59.6450564"}
            };
            targetClass.Run("MassSendCommand", DateTime.Now, options, null, batchExePath);
        }

        [Test()]
        public void TestErsSendMailAtMailWithCache()
        {
            SendErsSendMailAtMail("testKey");
        }

        [Test()]
        public void TestErsSendMailAtMailWithoutCache()
        {
            SendErsSendMailAtMail(null);
        }

        private void SendErsSendMailAtMail(string pathKey)
        {
            var mailTo = ErsFactory.ErsAtMailFactory.GetEmailToModel();
            mailTo.lname = "長池";
            mailTo.fname = "テスト";
            mailTo.mcode = "1";
            mailTo.email = "nagaike@ivp.co.jp";
            mailTo.etc1 = "etc1";
            mailTo.etc2 = "etc2";

            var body = @"配信テスト
リダイレクトURL
?id=1379&ptn=http%3a%2f%2fdev-ersv7.ivp.co.jp%2ftop%2fdetail%2fasp%2fdetail.asp%3fgcode%3d3264
lname<%=.Model.lname%>
fname<%=.Model.fname%>
mcode<%=.Model.mcode%>
email<%=.Model.email%>
etc1<%=.Model.etc1%>
etc2<%=.Model.etc2%>
リダイレクトURL2
?id=1379&ptn=http%3a%2f%2fdev-ersv7.ivp.co.jp%2ftop%2fdetail%2fasp%2fdetail.asp%3fgcode%3d3265%0d
ほげほげ";
            string html_body = null;

            for (var i = 0; i < 100; i++)
            {
                var sendmail = this.GetSendMail(pathKey);
                sendmail.Bind(mailTo, body, body, pathKey);

                //D3) Use the method of ErsSmtp SendSynchronous class to send an email.
                sendmail.SendMail("spf-test.ivp.co.jp", 25);
            }
        }

        private ErsSendMailAtMail GetSendMail(string pathKey)
        {
            var sendmail = ErsFactory.ersMailFactory.GetErsSendMailAtMail();

            sendmail.mail_to = "kitano@spf-test.ivp.co.jp";
            sendmail.mail_from = "kitano@spf-test.ivp.co.jp";
            sendmail.reply_to = "kitano@spf-test.ivp.co.jp";
            sendmail.subject = "テンプレートキャッシュテスト[" + pathKey + "]";
            return sendmail;
        }
    }
}
