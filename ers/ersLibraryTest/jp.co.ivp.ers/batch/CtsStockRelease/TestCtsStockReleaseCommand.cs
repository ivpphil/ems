﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.CtsStockRelease;

namespace ersLibraryTest.jp.co.ivp.ers.batch.CtsStockRelease
{
    public class TestCtsStockReleaseCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new CtsStockReleaseCommand();
            targetClass.Run(null, null, new Dictionary<string, object>(), null, null);
        }
    }
}
