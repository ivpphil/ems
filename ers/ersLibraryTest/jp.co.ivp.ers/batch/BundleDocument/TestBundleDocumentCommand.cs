﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;

namespace ersLibraryTest.jp.co.ivp.ers.batch.BundleDocument
{
    class TestBundleDocumentCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new global::jp.co.ivp.ers.batch.BundleDocument.BundleDocumentCommand();
            targetClass.Run(null, null, new Dictionary<string, object>(), null, null);
        }
    }
}
