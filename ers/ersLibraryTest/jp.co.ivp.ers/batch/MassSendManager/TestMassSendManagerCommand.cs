﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.MassSendManager;
using System.Configuration;

namespace ersLibraryTest.jp.co.ivp.ers.batch.MassSendManager
{
    class TestMassSendManagerCommand
         : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var batchExePath = ConfigurationManager.AppSettings["batchExePath"];
            var targetClass = new MassSendManagerCommand();
            targetClass.Run(null, null, new Dictionary<string, object>() { { "date", DateTime.Now.ToString() } }, null, batchExePath);
        }
    }
}
