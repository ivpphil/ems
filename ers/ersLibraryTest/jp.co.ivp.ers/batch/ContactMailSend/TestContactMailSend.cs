﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;

namespace ersLibraryTest.jp.co.ivp.ers.batch.ContactMailSend
{
    class TestContactMailSend
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new global::jp.co.ivp.ers.batch.ContactMailSend.ContactMailSend();
            targetClass.Run(null, null, new Dictionary<string, object>(), null, null);
        }
    }
}
