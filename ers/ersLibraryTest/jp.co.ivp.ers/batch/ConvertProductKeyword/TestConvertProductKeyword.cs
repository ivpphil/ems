﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.ConvertProductKeyword;

namespace ersLibraryTest.jp.co.ivp.ers.batch.ConvertProductKeyword
{
    public class TestConvertProductKeywordCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new ConvertProductKeywordCommand();
            targetClass.Run(null, DateTime.Now, new Dictionary<string, object>() { { "datetime", DateTime.Now.Date } }, null, null);
        }
    }
}
