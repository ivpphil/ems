﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers.batch.ProductRanking;
using NUnit.Framework;

namespace ersLibraryTest.jp.co.ivp.ers.batch.ProductRanking
{
    public class TestProductRankingCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new ProductRankingCommand();
            targetClass.Run(null, DateTime.Now, new Dictionary<string, object>(), null, null);
        }
    }
}
