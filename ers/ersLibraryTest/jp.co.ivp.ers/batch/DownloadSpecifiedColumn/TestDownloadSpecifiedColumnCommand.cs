﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.DownloadSpecifiedColumn;

namespace ersLibraryTest.jp.co.ivp.ers.batch.DownloadSpecifiedColumn
{
    class TestDownloadSpecifiedColumnCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new DownloadSpecifiedColumnCommand();
            targetClass.Run(null, null, null, null, null);
        }
    }
}
