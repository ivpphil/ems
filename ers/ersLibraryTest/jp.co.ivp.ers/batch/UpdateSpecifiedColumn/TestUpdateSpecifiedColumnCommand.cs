﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.UpdateSpecifiedColumn;

namespace ersLibraryTest.jp.co.ivp.ers.batch.UpdateSpecifiedColumn
{
    class TestUpdateSpecifiedColumnCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new UpdateSpecifiedColumnCommand();
            targetClass.Run(null, null, null, null, null);
        }
    }
}
