﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ersLibraryTest.jp.co.jp.libraryTest;

namespace ersLibraryTest.jp.co.ivp.ers.batch.ContactMailReception
{
    class TestContactMailReception
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new global::jp.co.ivp.ers.batch.ContactMailReception.ContactMailReception();
            targetClass.Run(null, null, new Dictionary<string, object>(), null, null);
        }
    }
}
