﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.InsertSpecifiedColumn;

namespace ersLibraryTest.jp.co.ivp.ers.batch.InsertSpecifiedColumn
{
    class TestInsertSpecifiedColumnCommand
        : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new InsertSpecifiedColumnCommand();
            targetClass.Run(null, null, null, null, null);
        }
    }
}
