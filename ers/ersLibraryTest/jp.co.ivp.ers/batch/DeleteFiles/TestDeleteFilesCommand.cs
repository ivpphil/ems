﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.DeleteFiles;

namespace ersLibraryTest.jp.co.ivp.ers.batch.DeleteFiles
{
    public class TestDeleteFilesCommand
         : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new DeleteFilesCommand();
            targetClass.Run(null, null, new Dictionary<string, object>(), null, null);
        }
    }
}
