﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using jp.co.ivp.ers.batch.CreditContinualBillingDownload;
using Npgsql;
using Dapper;
using FluentAssertions;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using System.IO;
using ersLibraryTest.jp.co.jp.libraryTest;

namespace ersLibraryTest.jp.co.ivp.ers.batch.CreditContinualBillingDownload
{
    [TestFixture(Category = "UnitTest")]
    class TestCreditContinualBillingDownload
         : TestCommon
    {
        [ThreadStatic]
        public static string tempFilePath;

        public void TestMain(Dictionary<string, object> argDictionary)
        {
            var targetClass = new CreditContinualBillingDownloadCommand();
            targetClass.Run(typeof(TestCreditContinualBillingDownload).ToString(), DateTime.Now, argDictionary, null, null);
        }

        [Test]
        [OrderTestData]
        public void ers_default_5()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            var order = OrderTestDataAttribute.order;

            var csvModel = OrderTestDataAttribute.csvModel;
            csvModel.process_result = EnumContinualBillingResult.Success;

            var TestDate = DateTime.Now.AddMonths(1);

            this.CreateTestData(TestDate, csvModel);

            this.TestMain(new Dictionary<string, object>()
            {
                {"date", TestDate}
            });

            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                order = db.Query<d_master_t>(@"
                    SELECT * 
                    FROM d_master_t 
                    WHERE d_no = @d_no", new { d_no = order.d_no }).SingleOrDefault();

                order.paid_date.Should().HaveValue();
                order.paid_price.Should().Be(order.total);
                order.order_payment_status.Should().Be(30);

                db.Close();
            }

        }

        [Test]
        [OrderTestData]
        public void ers_default_6()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var csvModel = OrderTestDataAttribute.csvModel;
            csvModel.process_result = EnumContinualBillingResult.AuthoryError;
            csvModel.authory_result = "FAILEDAUTH";

            var TestDate = DateTime.Now.AddMonths(1);

            this.CreateTestData(TestDate, csvModel);

           this.TestMain(new Dictionary<string, object>()
            {
                {"date", TestDate}
            });

            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                var order = db.Query<d_master_t>(@"
                    SELECT * 
                    FROM d_master_t 
                    WHERE credit_order_id = @credit_order_id", csvModel).SingleOrDefault();

                // CHECK
                order.Should().NotBeNull();
                order.order_payment_status.Should().Be(20);

                var mail = db.Query<tp_mail_t>(@"
                    SELECT * 
                    FROM tp_mail_t 
                    WHERE subject = @subject", new { subject = setup.creditContinualBillingDownloadGmoErrMailTitle }).SingleOrDefault();

                // CHECK
                mail.Should().NotBeNull();
                mail.message.Should().Contain(csvModel.credit_order_id);

                var regular_error_t = db.Query<regular_error_t>(@"
                    SELECT * 
                    FROM regular_error_t 
                    WHERE credit_order_id = @credit_order_id", csvModel).SingleOrDefault();

                // CHECK
                regular_error_t.Should().NotBeNull();
                regular_error_t.error_description.Should().Contain(EnumContinualBillingResult.AuthoryError.ToString());
                regular_error_t.error_description.Should().Contain(csvModel.authory_result);
                regular_error_t.disp_flg.Should().Be((int)EnumRegularErrLogDispFlg.PaymentError);
                regular_error_t.active.Should().Be((int)EnumActive.Active);

                db.Close();
            }
        }

        [Test]
        [OrderTestData]
        public void ers_default_7()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var csvModel = OrderTestDataAttribute.csvModel;
            csvModel.process_result = EnumContinualBillingResult.SaleError;

            var TestDate = DateTime.Now.AddMonths(1);

            this.CreateTestData(TestDate, csvModel);

            this.TestMain(new Dictionary<string, object>()
            {
                {"date", TestDate}
            });

            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                var order = db.Query<d_master_t>(@"
                    SELECT * 
                    FROM d_master_t 
                    WHERE credit_order_id = @credit_order_id", csvModel).SingleOrDefault();

                // CHECK
                order.Should().NotBeNull();
                order.order_payment_status.Should().Be(20);

                var mail = db.Query<tp_mail_t>(@"
                    SELECT * 
                    FROM tp_mail_t 
                    WHERE subject = @subject", new { subject = setup.creditContinualBillingDownloadGmoErrMailTitle }).SingleOrDefault();

                // CHECK
                mail.Should().NotBeNull();
                mail.message.Should().Contain(csvModel.credit_order_id);

                var regular_error_t = db.Query<regular_error_t>(@"
                    SELECT * 
                    FROM regular_error_t 
                    WHERE credit_order_id = @credit_order_id", csvModel).SingleOrDefault();

                // CHECK
                regular_error_t.Should().NotBeNull();
                regular_error_t.error_description.Should().Contain(EnumContinualBillingResult.SaleError.ToString());
                regular_error_t.disp_flg.Should().Be((int)EnumRegularErrLogDispFlg.PaymentError);
                regular_error_t.active.Should().Be((int)EnumActive.Active);

                db.Close();
            }
        }

        [Test]
        [OrderTestData]
        public void ers_default_8()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var csvModel = OrderTestDataAttribute.csvModel;
            csvModel.process_result = EnumContinualBillingResult.ValueError;

            var TestDate = DateTime.Now.AddMonths(1);

            this.CreateTestData(TestDate, csvModel);

            Action testAction = () => this.TestMain(new Dictionary<string, object>()
            {
                {"date", TestDate}
            });

            testAction.ShouldThrow<Exception>().And.Message.Should().Contain(csvModel.credit_order_id);

            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                var order = db.Query<d_master_t>(@"
                    SELECT * 
                    FROM d_master_t 
                    WHERE credit_order_id = @credit_order_id", csvModel).SingleOrDefault();

                // CHECK
                order.Should().NotBeNull();
                order.order_payment_status.Should().Be(20);

                var mail = db.Query<tp_mail_t>(@"
                    SELECT * 
                    FROM tp_mail_t 
                    WHERE subject = @subject", new { subject = setup.creditContinualBillingDownloadGmoErrMailTitle }).SingleOrDefault();

                // CHECK
                mail.Should().NotBeNull();
                mail.message.Should().Contain(csvModel.credit_order_id);

                var regular_error_t = db.Query<regular_error_t>(@"
                    SELECT * 
                    FROM regular_error_t 
                    WHERE credit_order_id = @credit_order_id", csvModel).SingleOrDefault();

                // CHECK
                regular_error_t.Should().NotBeNull();
                regular_error_t.error_description.Should().Contain(EnumContinualBillingResult.ValueError.ToString());
                regular_error_t.disp_flg.Should().Be((int)EnumRegularErrLogDispFlg.PaymentError);
                regular_error_t.active.Should().Be((int)EnumActive.Active);

                db.Close();
            }
        }

        private string csvExtension = ".txt";
        private string tarZipExtension = ".tar.gz";
        private string uploadOkFileExtension = ".ok";

        /// <summary>
        /// テストデータを作成する
        /// </summary>
        /// <param name="currentDate"></param>
        /// <param name="csvModel"></param>
        internal void CreateTestData(DateTime currentDate, CsvUploadRecord csvModel)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            var baseFileName = "uri" + setup.gmo_shop_id + currentDate.ToString("yyyyMM") + "01";
            var csvFileName = "R" + baseFileName + this.csvExtension;
            tempFilePath = setup.log_path + setup.creditContinualBillingUploadTempFilePath;

            OrderTestDataAttribute.tempUploadFilePath = tempFilePath;
            OrderTestDataAttribute.tempFilePath = setup.log_path + setup.creditContinualBillingDownloadTempFilePath;
            OrderTestDataAttribute.CsvFileName = csvFileName;

            ErsDirectory.CreateDirectories(tempFilePath);

            //CSVファイルをtempフォルダへ作成
            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            using (var writer = csvCreater.GetWriter(tempFilePath, csvFileName))
            {
                csvCreater.WriteBody(csvModel, writer);
            }

            //TarZIPファイルの生成
            var tarZipFileName = this.CreateTarZip(csvFileName);

            OrderTestDataAttribute.tarZipFileName = "R" + tarZipFileName;

            //SFTPにて、既定のディレクトリへ転送（PUT）
            this.PutTarZip(tarZipFileName);
        }

        #region "TarZIPファイルの生成"
        /// <summary>
        /// TarZIPファイルの生成
        /// </summary>
        /// <param name="csvFileName"></param>
        /// <returns></returns>
        private string CreateTarZip(string csvFileName)
        {
            //ZIPファイル保存先取得
            var zipFileName = System.IO.Path.GetFileNameWithoutExtension(csvFileName.Substring(1)) + tarZipExtension;

            //圧縮ファイル作成
            var zipFileManager = ErsFactory.ersUtilityFactory.GetTarZipFileManager();
            zipFileManager.Compress(tempFilePath + csvFileName, tempFilePath + zipFileName);

            //Put完了ファイルを作成
            this.CreateUploadOkFile(zipFileName);

            //パスを返却
            return zipFileName;
        }

        /// <summary>
        /// Put完了ファイルを作成
        /// </summary>
        /// <param name="fileName"></param>
        private void CreateUploadOkFile(string zipFileName)
        {
            using (var hStream = System.IO.File.Create(tempFilePath + zipFileName + uploadOkFileExtension))
            {
                // 作成時に返される FileStream を利用して閉じる
                if (hStream != null)
                {
                    hStream.Close();
                }
            }
        }
        #endregion

        #region "SFTPにて、既定のディレクトリへ転送（PUT）"
        /// <summary>
        /// SFTPにて、既定のディレクトリへ転送（PUT）
        /// </summary>
        /// <param name="csvFileName"></param>
        private void PutTarZip(string tarZipFileName)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            string upTempPath = null;
            if (setup.creditContinualBillingDownloadSftpSshKeyPath.HasValue())
            {
                upTempPath = setup.root_path + setup.creditContinualBillingDownloadSftpSshKeyPath;
            }

            using (var sftp = SFTPClient.Connect(
                setup.creditContinualBillingDownloadSftpHost,
                setup.creditContinualBillingDownloadSftpUser,
                setup.creditContinualBillingDownloadSftpPass,
                setup.creditContinualBillingDownloadSftpPort,
                upTempPath,
                setup.creditContinualBillingDownloadSftpSshPassPhrase))
            {
                var sourcePath = tempFilePath + tarZipFileName;
                var putPath = setup.creditContinualBillingDownloadSftpUserSftpPutPath + "R" + tarZipFileName;

                //TarZipをput
                sftp.PutFile(sourcePath, putPath);

                //サーバーにアップ完了フラグファイルのアップ
                sftp.PutFile(sourcePath + uploadOkFileExtension, putPath + uploadOkFileExtension);

                sftp.Close();
            }
        }
        #endregion
    }

    public class CsvUploadRecord : ErsBindableModel
    {
        /// <summary>
        /// ショップID
        /// </summary>
        [CsvField]
        public virtual string gmo_shop_id { get; set; }

        /// <summary>
        /// 会員ID
        /// </summary>
        [CsvField]
        public virtual string card_mcode { get; set; }

        /// <summary>
        /// カード登録連番
        /// </summary>
        [CsvField]
        public virtual string card_sequence { get; set; }

        /// <summary>
        /// 取引コード
        /// </summary>
        [CsvField]
        public virtual int? transaction_code { get; set; }

        /// <summary>
        /// 利用年月日
        /// </summary>
        [CsvField]
        public virtual string intime { get; set; }

        /// <summary>
        /// オーダーID
        /// </summary>
        [CsvField]
        public virtual string credit_order_id { get; set; }

        /// <summary>
        /// 商品コード
        /// </summary>
        [CsvField]
        public virtual string scode { get; set; }

        /// <summary>
        /// 利用金額
        /// </summary>
        [CsvField]
        public virtual int? total { get; set; }

        /// <summary>
        /// 税送料
        /// </summary>
        [CsvField]
        public virtual string tax { get; set; }

        /// <summary>
        /// 支払方法
        /// </summary>
        [CsvField]
        public virtual int pay { get; set; }

        /// <summary>
        /// 支払回数
        /// </summary>
        [CsvField]
        public virtual string pay_times { get; set; }

        /// <summary>
        /// ボーナス回数
        /// </summary>
        [CsvField]
        public virtual string bonus_times { get; set; }

        /// <summary>
        /// ボーナス金額
        /// </summary>
        [CsvField]
        public virtual string bonus_total { get; set; }

        /// <summary>
        /// 端末処理通番
        /// </summary>
        [CsvField]
        public virtual string process_sequence { get; set; }

        /// <summary>
        /// 加盟店自由項目
        /// </summary>
        [CsvField]
        public virtual string remarks { get; set; }

        /// <summary>
        /// 処理番号
        /// </summary>
        [CsvField]
        public virtual string process_id { get; set; }

        /// <summary>
        /// 処理結果
        /// </summary>
        [CsvField]
        public virtual EnumContinualBillingResult? process_result { get; set; }

        /// <summary>
        /// 仕向先コード
        /// </summary>
        [CsvField]
        public virtual string card_code { get; set; }

        /// <summary>
        /// オーソリ結果
        /// </summary>
        [CsvField]
        public virtual string authory_result { get; set; }
    }

    public class OrderTestDataAttribute
          : Attribute, ITestAction
    {
        [ThreadStatic]
        public static string tempFilePath;

        [ThreadStatic]
        public static string tempUploadFilePath;

        [ThreadStatic]
        public static d_master_t order;

        [ThreadStatic]
        public static CsvUploadRecord csvModel;

        [ThreadStatic]
        public static string CsvFileName;

        [ThreadStatic]
        public static string tarZipFileName;

        public ActionTargets Targets
        {
            get { return ActionTargets.Test; }
        }

        public void BeforeTest(TestDetails testDetails)
        {

            var setup = ErsFactory.ersBatchFactory.getSetup();
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                order = db.Query<d_master_t>(@"
                    SELECT * 
                    FROM d_master_t 
                    WHERE d_master_t.order_payment_status IN ( 20 ) 
                    AND EXISTS(SELECT inner_ds.* FROM ds_master_t AS inner_ds WHERE inner_ds.order_status = ANY((ARRAY[10, 20, 30])) AND inner_ds.d_no = d_master_t.d_no)
                    ORDER BY id ASC LIMIT 1").SingleOrDefault();

                db.Execute("UPDATE d_master_t SET paid_price = NULL, paid_date = NULL WHERE d_no = @d_no", new { d_no = order.d_no });

                var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
                memberCardCriteria.id = order.member_card_id;
                var memberCard = memberCardRepository.FindSingle(memberCardCriteria);

                csvModel = new CsvUploadRecord();
                csvModel.card_mcode = memberCard.card_mcode;
                csvModel.card_sequence = memberCard.card_sequence;
                csvModel.credit_order_id = order.credit_order_id;
                csvModel.total = order.total;

                db.Execute("DELETE FROM regular_error_t WHERE credit_order_id = @credit_order_id", new { credit_order_id = order.credit_order_id });

                db.Execute("DELETE FROM tp_mail_t WHERE subject = @subject", new { subject = setup.creditContinualBillingDownloadGmoErrMailTitle });

                db.Execute("DELETE FROM regular_error_t WHERE credit_order_id = @credit_order_id", order);

                db.Close();
            }
        }

        private string uploadOkFileExtension = ".ok";
        public void AfterTest(TestDetails testDetails)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                db.Execute("UPDATE d_master_t SET order_payment_status = 20 WHERE d_no = @d_no", new { d_no = order.d_no });

                db.Close();
            }
            
            File.Delete(tempFilePath + tarZipFileName);
            File.Delete(tempUploadFilePath + CsvFileName);
            File.Delete(tempUploadFilePath + tarZipFileName.Substring(1));
            File.Delete(tempUploadFilePath + tarZipFileName.Substring(1) + uploadOkFileExtension);
        }
    }

    public class d_master_t
    {
        public string d_no { get; set; }

        public int member_card_id { get; set; }

        public string credit_order_id { get; set; }

        public int total { get; set; }

        public int order_payment_status { get; set; }

        public DateTime? paid_date { get; set; }

        public int paid_price { get; set; }
    }

    public class tp_mail_t
    {
        public string subject { get; set; }
        public string message { get; set; }
    }

    public class regular_error_t
    {
        public string error_description { get; set; }
        public int disp_flg { get; set; }
        public int active { get; set; }
    }
}
