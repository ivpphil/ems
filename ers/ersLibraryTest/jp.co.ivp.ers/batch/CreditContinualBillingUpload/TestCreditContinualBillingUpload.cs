﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers.batch.CreditContinualBillingUpload;
using jp.co.ivp.ers;
using Npgsql;
using Dapper;
using FluentAssertions;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.batch.CreditContinualBillingUpload.model;
using System.IO;

namespace ersLibraryTest.jp.co.ivp.ers.batch.CreditContinualBillingUpload
{
    [TestFixture(Category = "UnitTest")]
    public class TestCreditContinualBillingUpload
         : TestCommon
    {
        private string csvExtension = ".txt";
        private string tarZipExtension = ".tar.gz";
        private string uploadOkFileExtension = ".ok";
        
        public void TestMain(Dictionary<string, object> argDictionary)
        {
            var targetClass = new CreditContinualBillingUploadCommand();
            targetClass.Run(typeof(TestCreditContinualBillingUpload).ToString(), DateTime.Now, argDictionary, null, null);
        }

        [Test]
        [OrderTestData]
        public void ers_default_4()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var TestDate = DateTime.Now.AddMonths(1);

            this.TestMain(new Dictionary<string, object>()
            {
                {"date", TestDate}
            });

            var tempFilePath = setup.log_path + setup.creditContinualBillingUploadTempFilePath;
            var tarZipFileName = "uri" + setup.gmo_shop_id + TestDate.ToString("yyyyMM") + "01.tar.gz";
            var CsvFileName = this.GetCsvFileName(tarZipFileName);

            OrderTestDataAttribute.tempFilePath = tempFilePath;
            OrderTestDataAttribute.tarZipFileName = tarZipFileName;
            OrderTestDataAttribute.CsvFileName = CsvFileName;

            File.Exists(tempFilePath + CsvFileName).Should().BeFalse();
            File.Exists(tempFilePath + tarZipFileName).Should().BeTrue();

            File.Delete(tempFilePath + CsvFileName);
            File.Delete(tempFilePath + tarZipFileName);

            using (var sftp = this.GetSftpConnection())
            {
                var ftpFilePath = setup.creditContinualBillingUploadSftpUserSftpPutPath + tarZipFileName;
                sftp.Exists(ftpFilePath + this.uploadOkFileExtension).Should().BeTrue();
                sftp.Exists(ftpFilePath).Should().BeTrue();

                //サーバーから対象ファイルDL
                sftp.GetFile(ftpFilePath, tempFilePath + tarZipFileName);

                sftp.DeleteFile(ftpFilePath + this.uploadOkFileExtension);
                sftp.DeleteFile(ftpFilePath);
            }
      
            //圧縮ファイル作成
            var zipFileManager = ErsFactory.ersUtilityFactory.GetTarZipFileManager();
            zipFileManager.Extract(tempFilePath + tarZipFileName, tempFilePath);

            var recordFound = false;
            var csvLoader = ErsFactory.ersUtilityFactory.GetErsCsvContainer<CsvUploadRecord>();
            csvLoader.LoadPostedFile(tempFilePath + CsvFileName);

            var order = OrderTestDataAttribute.order;

            foreach (var model in csvLoader.GetValidModels())
            {
                if (model.credit_order_id == order.credit_order_id)
                {
                    recordFound = true;

                    var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                    var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
                    memberCardCriteria.id = order.member_card_id;

                    var memberCard = memberCardRepository.FindSingle(memberCardCriteria);
                    model.gmo_shop_id.Should().Be(setup.gmo_shop_id);
                    model.card_mcode.Should().Be(memberCard.card_mcode);
                    model.card_sequence.Should().Be(memberCard.card_sequence);
                    model.transaction_code.Should().Be(1);
                    model.intime.Should().Be(((DateTime)order.intime).ToString("yyyyMMdd"));
                    model.credit_order_id.Should().Be(order.credit_order_id);
                    model.scode.Should().BeNullOrEmpty();
                    model.total.Should().Be(order.total);
                    model.tax.Should().BeNullOrEmpty();
                    model.pay.Should().Be(1);
                    model.pay_times.Should().BeNullOrEmpty();
                    model.bonus_times.Should().BeNullOrEmpty();
                    model.bonus_total.Should().BeNullOrEmpty();
                    model.process_sequence.Should().HaveLength(5);
                    Convert.ToInt32(model.process_sequence).Should().BeInRange(1, 99999);
                    model.remarks.Should().BeNullOrEmpty();
                    model.process_id.Should().BeNullOrEmpty();
                    model.process_result.Should().BeNullOrEmpty();
                    model.card_code.Should().BeNullOrEmpty();
                    model.authory_result.Should().BeNullOrEmpty();
                }
            }

            recordFound.Should().BeTrue();

            var newOrder = ErsFactory.ersOrderFactory.GetOrderWithD_no((string)order.d_no);
            newOrder.sent_continual_billing.Should().Be(EnumSentContinualBillingFlg.Sent);
        }

        /// <summary>
        /// CSVファイル名取得
        /// </summary>
        /// <returns></returns>
        private string GetCsvFileName(string tarZipFileName)
        {
            return System.IO.Path.GetFileName(tarZipFileName).Replace(tarZipExtension, string.Empty) + csvExtension;
        }

        #region "サーバーからファイルDL"

        /// <summary>
        /// SFTPのコネクションを取得する
        /// </summary>
        /// <returns></returns>
        private SFTPClient GetSftpConnection()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            string upTempPath = null;
            if (setup.creditContinualBillingUploadSftpSshKeyPath.HasValue())
            {
                upTempPath = setup.root_path + setup.creditContinualBillingUploadSftpSshKeyPath;
            }

            return SFTPClient.Connect(
                            setup.creditContinualBillingUploadSftpHost,
                            setup.creditContinualBillingUploadSftpUser,
                            setup.creditContinualBillingUploadSftpPass,
                            setup.creditContinualBillingUploadSftpPort,
                            upTempPath,
                            setup.creditContinualBillingUploadSftpSshPassPhrase);
        }
        #endregion
    }

    public class OrderTestDataAttribute
          : Attribute, ITestAction
    {
        [ThreadStatic]
        public static dynamic order;

        [ThreadStatic]
        public static string tempFilePath;

        [ThreadStatic]
        public static string CsvFileName;
        
        [ThreadStatic]
        public static string tarZipFileName;

        public ActionTargets Targets
        {
            get { return ActionTargets.Test; }
        }

        public void BeforeTest(TestDetails testDetails)
        {

            var setup = ErsFactory.ersUtilityFactory.getSetup();
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                order = db.Query(@"
                    SELECT * 
                    FROM d_master_t 
                    WHERE d_master_t.order_payment_status IN ( 20 ) 
                    AND EXISTS(SELECT inner_ds.* FROM ds_master_t AS inner_ds WHERE inner_ds.order_status = ANY((ARRAY[10, 20, 30])) AND inner_ds.d_no = d_master_t.d_no)
                    ORDER BY id ASC LIMIT 1").SingleOrDefault();

                db.Execute("UPDATE ds_master_t SET order_status = 30 WHERE d_no = @d_no", new { d_no = order.d_no });

                db.Close();
            }
        }

        public void AfterTest(TestDetails testDetails)
        {
            File.Delete(tempFilePath + CsvFileName);
            File.Delete(tempFilePath + tarZipFileName);
        }
    }
}
