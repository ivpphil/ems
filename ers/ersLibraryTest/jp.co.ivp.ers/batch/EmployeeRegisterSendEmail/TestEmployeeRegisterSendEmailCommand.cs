﻿using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers.batch.EmployeeRegisterSendEmail;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ersLibraryTest.jp.co.ivp.ers.batch.EmployeeRegisterSendEmail
{
    class TestEmployeeRegisterSendEmailCommand
         : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var targetClass = new EmployeeRegisterSendEmailCommand();
            targetClass.Run(null, null, new Dictionary<string, object>(), null, null);
        }
    }
}
