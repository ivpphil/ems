﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using jp.co.ivp.ers.batch.CreditAccountLaunderingDownload;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using Npgsql;
using Dapper;
using FluentAssertions;
using System.IO;
using jp.co.ivp.ers.mvc;

namespace ersLibraryTest.jp.co.ivp.ers.batch.CreditAccountLaunderingDownload
{
    [TestFixture(Category = "UnitTest")]
    class TestCreditAccountLaunderingDownload
            : TestCommon
    {
        [ThreadStatic]
        public static string tempFilePath;

        public void TestMain(Dictionary<string, object> argDictionary)
        {
            var targetClass = new CreditAccountLaunderingDownloadCommand();
            targetClass.Run(typeof(TestCreditAccountLaunderingDownload).ToString(), DateTime.Now, argDictionary, null, null);
        }

        [Test]
        [OrderTestData]
        public void ers_default_9()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var member_card = OrderTestDataAttribute.member_card;
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                member_card.update_status = 1;

                db.Execute("UPDATE member_card_t SET update_status = @update_status WHERE id = @id", member_card);

                db.Close();
            }

            var csvModel = OrderTestDataAttribute.csvModel;
            csvModel.result = EnumAccountLaunderingResult.Success;

            var TestDate = DateTime.Now.AddMonths(1);

            this.CreateTestData(TestDate, csvModel);

            this.TestMain(new Dictionary<string, object>()
            {
                {"date", TestDate}
            });

            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                member_card = db.Query<member_card_t>(@"
                    SELECT * 
                    FROM member_card_t 
                    WHERE id = @id", member_card).SingleOrDefault();

                member_card.update_status.Should().Be(0);

                db.Close();
            }

        }

        [Test]
        [OrderTestData]
        public void ers_default_10()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var member_card = OrderTestDataAttribute.member_card;
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                member_card.update_status = 0;

                db.Execute("UPDATE member_card_t SET update_status = @update_status WHERE id = @id", member_card);

                db.Close();
            }

            var csvModel = OrderTestDataAttribute.csvModel;
            csvModel.result = EnumAccountLaunderingResult.Fail;

            var TestDate = DateTime.Now.AddMonths(1);

            this.CreateTestData(TestDate, csvModel);

            this.TestMain(new Dictionary<string, object>()
            {
                {"date", TestDate}
            });

            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                member_card = db.Query<member_card_t>(@"
                    SELECT * 
                    FROM member_card_t 
                    WHERE id = @id", member_card).SingleOrDefault();

                // CHECK
                member_card.update_status.Should().Be(1);

                var regular_error_t = db.Query<regular_error_t>(@"
                    SELECT * 
                    FROM regular_error_t 
                    WHERE card_mcode = @card_mcode AND card_sequence = @card_sequence", member_card).SingleOrDefault();

                // CHECK
                regular_error_t.Should().NotBeNull();
                regular_error_t.error_description.Should().Contain(ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.AccountLaundering, EnumCommonNameColumnName.namename, (int)EnumAccountLaunderingResult.Fail));
                regular_error_t.disp_flg.Should().Be((int)EnumRegularErrLogDispFlg.PaymentError);
                regular_error_t.active.Should().Be((int)EnumActive.Active);

                db.Close();
            }
        }

        [Test]
        [OrderTestData]
        public void ers_default_11()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var member_card = OrderTestDataAttribute.member_card;
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                member_card.update_status = 0;

                db.Execute("UPDATE member_card_t SET update_status = @update_status WHERE id = @id", member_card);

                db.Close();
            }

            var csvModel = OrderTestDataAttribute.csvModel;
            csvModel.result = EnumAccountLaunderingResult.MatchingError;

            var TestDate = DateTime.Now.AddMonths(1);

            this.CreateTestData(TestDate, csvModel);

            this.TestMain(new Dictionary<string, object>()
            {
                {"date", TestDate}
            });

            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                member_card = db.Query<member_card_t>(@"
                    SELECT * 
                    FROM member_card_t 
                    WHERE id = @id", member_card).SingleOrDefault();

                // CHECK
                member_card.update_status.Should().Be(1);

                var regular_error_t = db.Query<regular_error_t>(@"
                    SELECT * 
                    FROM regular_error_t 
                    WHERE card_mcode = @card_mcode AND card_sequence = @card_sequence", member_card).SingleOrDefault();

                // CHECK
                regular_error_t.Should().NotBeNull();
                regular_error_t.error_description.Should().Contain(ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.AccountLaundering, EnumCommonNameColumnName.namename, (int)EnumAccountLaunderingResult.MatchingError));
                regular_error_t.disp_flg.Should().Be((int)EnumRegularErrLogDispFlg.PaymentError);
                regular_error_t.active.Should().Be((int)EnumActive.Active);

                db.Close();
            }
        }

        [Test]
        [OrderTestData]
        public void ers_default_12()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var member_card = OrderTestDataAttribute.member_card;
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                member_card.update_status = 0;

                db.Execute("UPDATE member_card_t SET update_status = @update_status WHERE id = @id", member_card);

                db.Close();
            }

            var csvModel = OrderTestDataAttribute.csvModel;
            csvModel.result = EnumAccountLaunderingResult.ValueError;

            var TestDate = DateTime.Now.AddMonths(1);

            this.CreateTestData(TestDate, csvModel);

            Action testAction = () => this.TestMain(new Dictionary<string, object>()
            {
                {"date", TestDate}
            });

            testAction.ShouldThrow<Exception>().And.Message.Should().Contain(csvModel.card_mcode);

            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                member_card = db.Query<member_card_t>(@"
                    SELECT * 
                    FROM member_card_t 
                    WHERE id = @id", member_card).SingleOrDefault();

                // CHECK
                member_card.update_status.Should().Be(1);

                var regular_error_t = db.Query<regular_error_t>(@"
                    SELECT * 
                    FROM regular_error_t 
                    WHERE card_mcode = @card_mcode AND card_sequence = @card_sequence", member_card).SingleOrDefault();

                // CHECK
                regular_error_t.Should().NotBeNull();
                regular_error_t.error_description.Should().Contain(ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.AccountLaundering, EnumCommonNameColumnName.namename, (int)EnumAccountLaunderingResult.ValueError));
                regular_error_t.disp_flg.Should().Be((int)EnumRegularErrLogDispFlg.SystemError);
                regular_error_t.active.Should().Be((int)EnumActive.Active);

                db.Close();
            }
        }

        private string csvExtension = ".txt";
        private string tarZipExtension = ".tar.gz";
        private string uploadOkFileExtension = ".ok";

        /// <summary>
        /// テストデータを作成する
        /// </summary>
        /// <param name="currentDate"></param>
        /// <param name="csvModel"></param>
        internal void CreateTestData(DateTime currentDate, CsvUploadRecord csvModel)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            var baseFileName = "arai" + setup.gmo_shop_id + currentDate.ToString("yyyyMM") + "01";
            var csvFileName = "R" + baseFileName + this.csvExtension;
            tempFilePath = setup.log_path + setup.creditAccountLaunderingUploadTempFilePath;

            OrderTestDataAttribute.tempUploadFilePath = tempFilePath;
            OrderTestDataAttribute.tempFilePath = setup.log_path + setup.creditAccountLaunderingDownloadTempFilePath;
            OrderTestDataAttribute.CsvFileName = csvFileName;

            ErsDirectory.CreateDirectories(tempFilePath);

            //CSVファイルをtempフォルダへ作成
            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            using (var writer = csvCreater.GetWriter(tempFilePath, csvFileName))
            {
                csvCreater.WriteBody(csvModel, writer);
            }

            //TarZIPファイルの生成
            var tarZipFileName = this.CreateTarZip(csvFileName);

            OrderTestDataAttribute.tarZipFileName = "R" + tarZipFileName;

            //SFTPにて、既定のディレクトリへ転送（PUT）
            this.PutTarZip(tarZipFileName);
        }

        #region "TarZIPファイルの生成"
        /// <summary>
        /// TarZIPファイルの生成
        /// </summary>
        /// <param name="csvFileName"></param>
        /// <returns></returns>
        private string CreateTarZip(string csvFileName)
        {
            //ZIPファイル保存先取得
            var zipFileName = System.IO.Path.GetFileNameWithoutExtension(csvFileName.Substring(1)) + tarZipExtension;

            //圧縮ファイル作成
            var zipFileManager = ErsFactory.ersUtilityFactory.GetTarZipFileManager();
            zipFileManager.Compress(tempFilePath + csvFileName, tempFilePath + zipFileName);

            //Put完了ファイルを作成
            this.CreateUploadOkFile(zipFileName);

            //パスを返却
            return zipFileName;
        }

        /// <summary>
        /// Put完了ファイルを作成
        /// </summary>
        /// <param name="fileName"></param>
        private void CreateUploadOkFile(string zipFileName)
        {
            using (var hStream = System.IO.File.Create(tempFilePath + zipFileName + uploadOkFileExtension))
            {
                // 作成時に返される FileStream を利用して閉じる
                if (hStream != null)
                {
                    hStream.Close();
                }
            }
        }
        #endregion

        #region "SFTPにて、既定のディレクトリへ転送（PUT）"
        /// <summary>
        /// SFTPにて、既定のディレクトリへ転送（PUT）
        /// </summary>
        /// <param name="csvFileName"></param>
        private void PutTarZip(string tarZipFileName)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            string upTempPath = null;
            if (setup.creditAccountLaunderingDownloadSftpSshKeyPath.HasValue())
            {
                upTempPath = setup.root_path + setup.creditAccountLaunderingDownloadSftpSshKeyPath;
            }

            using (var sftp = SFTPClient.Connect(
                setup.creditAccountLaunderingDownloadSftpHost,
                setup.creditAccountLaunderingDownloadSftpUser,
                setup.creditAccountLaunderingDownloadSftpPass,
                setup.creditAccountLaunderingDownloadSftpPort,
                upTempPath,
                setup.creditAccountLaunderingDownloadSftpSshPassPhrase))
            {
                var sourcePath = tempFilePath + tarZipFileName;
                var putPath = setup.creditAccountLaunderingDownloadSftpUserSftpPutPath + "R" + tarZipFileName;

                //TarZipをput
                sftp.PutFile(sourcePath, putPath);

                //サーバーにアップ完了フラグファイルのアップ
                sftp.PutFile(sourcePath + uploadOkFileExtension, putPath + uploadOkFileExtension);

                sftp.Close();
            }
        }
        #endregion
    }

    public class CsvUploadRecord : ErsBindableModel
    {
        [CsvField]
        public virtual string card_mcode { get; set; }

        [CsvField]
        public virtual string card_sequence { get; set; }

        [CsvField]
        public virtual string old_card_no { get; private set; }

        [CsvField]
        public virtual string old_expiration { get; private set; }

        [CsvField]
        public virtual string old_card_name { get; private set; }

        [CsvField]
        public virtual string old_card_code { get; private set; }

        [CsvField]
        public virtual EnumAccountLaunderingResult? result { get; set; }

        [CsvField]
        public virtual string new_card_no { get; private set; }

        [CsvField]
        public virtual string new_expiration { get; private set; }

        [CsvField]
        public virtual string new_card_code { get; private set; }

        [CsvField]
        public virtual string execute_date { get; private set; }

        [CsvField]
        public virtual string remark { get; private set; }

        [CsvField]
        public virtual string process_number { get; private set; }
    }

    public class OrderTestDataAttribute
          : Attribute, ITestAction
    {
        [ThreadStatic]
        public static member_card_t member_card;

        [ThreadStatic]
        public static CsvUploadRecord csvModel;

        [ThreadStatic]
        public static string tempFilePath;

        [ThreadStatic]
        public static string tempUploadFilePath;

        [ThreadStatic]
        public static string CsvFileName;

        [ThreadStatic]
        public static string tarZipFileName;

        public ActionTargets Targets
        {
            get { return ActionTargets.Test; }
        }

        public void BeforeTest(TestDetails testDetails)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();
            using (var db = new NpgsqlConnection(setup.getConnectionStrings()))
            {
                db.Open();

                member_card = db.Query<member_card_t>(@"
                    SELECT * 
                    FROM member_card_t 
                    WHERE
                    EXISTS 
                    (SELECT 1 
                        FROM regular_detail_t 
                        INNER JOIN regular_t ON regular_t.id = regular_detail_t.regular_id 
                        WHERE (regular_detail_t.delete_date IS NULL OR regular_detail_t.next_date < regular_detail_t.delete_date) AND regular_detail_t.mcode = member_card_t.mcode 
                    )
                    ORDER BY id ASC LIMIT 1").SingleOrDefault();

                csvModel = new CsvUploadRecord();
                csvModel.card_mcode = member_card.card_mcode;
                csvModel.card_sequence = member_card.card_sequence;

                db.Execute("DELETE FROM regular_error_t WHERE card_mcode = @card_mcode AND card_sequence = @card_sequence", member_card);

                db.Close();
            }
        }

        private string uploadOkFileExtension = ".ok";
        public void AfterTest(TestDetails testDetails)
        {
            File.Delete(tempFilePath + tarZipFileName);
            File.Delete(tempUploadFilePath + CsvFileName);
            File.Delete(tempUploadFilePath + tarZipFileName.Substring(1));
            File.Delete(tempUploadFilePath + tarZipFileName.Substring(1) + uploadOkFileExtension);
        }
    }

    public class member_card_t
    {
        public int id { get; set; }

        public string card_mcode { get; set; }

        public string card_sequence { get; set; }

        public int update_status { get; set; }
    }

    public class regular_error_t
    {
        public string error_description { get; set; }
        public int disp_flg { get; set; }
        public int active { get; set; }
    }
}
