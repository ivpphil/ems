﻿using ersLibraryTest.jp.co.jp.libraryTest;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jp.co.ivp.ers.batch.UploadEmpSchedule;

namespace ersLibraryTest.jp.co.ivp.ers.batch.UploadEmpSchedule
{
    class UploadEmpScheduleCommand : TestCommon
    {
        [Test()]
        public void TestMain()
        {
            var UploadEmpSchedule = new UploadEmpScheduleFile();
            UploadEmpSchedule.Run("UploadEmpSchedule", DateTime.Now, new Dictionary<string, object>() { { "datetime", DateTime.Now.Date } }, DateTime.Now, null);
        }
    }
}
