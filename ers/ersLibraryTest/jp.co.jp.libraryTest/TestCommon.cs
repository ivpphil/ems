﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using jp.co.ivp.ers.batch.util;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.mall.batch;

namespace ersLibraryTest.jp.co.jp.libraryTest
{
    public class TestCommon
    {
        public ErsMershaller ersMershaller { get; set; }

        public TestCommon()
        {
            var ersRoot = ConfigurationManager.AppSettings["ersRoot"];

            var objErsBatchEnvironment = new ErsBatchEnvironment();
            objErsBatchEnvironment.Initialization(ersRoot);

            var objErsMallBatchEnvironment = new ErsMallBatchEnvironment();
            objErsMallBatchEnvironment.Initialization(ersRoot);

            ErsLogger.Initialize(ConfigurationManager.AppSettings["logFilePath"]);
        }
    }
}
