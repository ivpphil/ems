﻿using System.Collections.Generic;
using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers.mall.batch.UpdateRakutenCampaignProducts;
using NUnit.Framework;

namespace ersLibraryTest.jp.co.ivp.ers.mall.batch.UpdateRakutenCampaignProducts
{
    public class TestUpdateRakutenCampaignProductsCommand
         : TestCommon
    {
        /// <summary>
        /// 
        /// </summary>
        [Test()]
        public void TestMain()
        {
            var targetClass = new UpdateRakutenCampaignProductsCommand();
            IDictionary<string, object> dicArg = null;
            targetClass.Run(null, null, dicArg, null, null);
        }
    }
}
