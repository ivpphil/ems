﻿using System;
using System.Collections.Generic;
using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.api.stock;
using NUnit.Framework;

namespace ersLibraryTest.jp.co.ivp.ers.mall.batch.RecoveryMallStock
{
    class TestRecalcStock
         : TestCommon
    {
        /// <summary>
        /// 
        /// </summary>
        [Test()]
        public void TestMain()
        {
            var listParam = new List<UpdateStockParam>();

            UpdateStockParam param = default(UpdateStockParam);

            param.productCode = "MALLTEST001";

            param.quantity = 2;
            param.operation = EnumMallStockOperation.sub;
            listParam.Add(param);

            param.quantity = 3;
            param.operation = EnumMallStockOperation.add;
            listParam.Add(param);


            // モール在庫更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);

            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }
        }
    }
}
