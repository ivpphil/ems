﻿using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers.mall.batch.MonitorUpdateMallStockHistory;
using NUnit.Framework;

namespace ersLibraryTest.jp.co.ivp.ers.mall.batch.MonitorUpdateMallStockHistory
{
    class TestMonitorUpdateMallStockHistoryCommand
         : TestCommon
    {
        /// <summary>
        /// 
        /// </summary>
        [Test()]
        public void TestMain()
        {
            var targetClass = new MonitorUpdateMallStockHistoryCommand();
            targetClass.Run(null, null, null, null, null);
        }
    }
}
