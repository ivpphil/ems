﻿using System.Collections.Generic;
using ersLibraryTest.jp.co.jp.libraryTest;
using jp.co.ivp.ers.mall.batch.UpdateMallSalesPeriodProducts;
using NUnit.Framework;

namespace ersLibraryTest.jp.co.ivp.ers.mall.batch.UpdateMallSalesPeriodProducts
{
    class TestUpdateMallSalesPeriodProductsCommand
         : TestCommon
    {
        /// <summary>
        /// 
        /// </summary>
        [Test()]
        public void TestMain()
        {
            var targetClass = new UpdateMallSalesPeriodProductsCommand();
            IDictionary<string, object> dicArg = null;
            targetClass.Run(null, null, dicArg, null, null);
        }
    }
}
