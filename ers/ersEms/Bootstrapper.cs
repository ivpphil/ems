using System.Reflection;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.mvc.CommandProcessor.Dispatcher;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using Microsoft.Practices.Unity;
using Unity.Mvc3;

namespace ersEms
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            //Employee
            container.RegisterType<IValidationHandler<Domain.Register.Commands.IEmpRegistCommand>, Domain.Register.Handlers.ValidateEmpRegist>();
            container.RegisterType<IValidationHandler<Domain.Employee.Commands.IEmpSearchCommand>, Domain.Employee.Handlers.ValidateEmpSearch>();
            container.RegisterType<ICommandHandler<Domain.Register.Commands.IEmpRegistCommand>, Domain.Register.Handlers.EmpRegistHandler>();
            container.RegisterType<IMapper<Domain.Employee.Mappables.IEmpListMappable>, Domain.Employee.Mappers.EmpListMapper>();
            container.RegisterType<IMapper<Domain.Employee.Mappables.IEmpSearchMappable>, Domain.Employee.Mappers.EmpSearchMapper>();
            container.RegisterType<IMapper<Domain.Employee.Mappables.IEmpSearchTempMappable>, Domain.Employee.Mappers.EmpSearchTempMapper>();
            container.RegisterType<IMapper<Domain.Employee.Mappables.IEmpImageMappable>, Domain.Employee.Mappers.EmpImageMapper>();
            container.RegisterType<IMapper<Domain.Employee.Mappables.ITeamManagementMappable>, Domain.Employee.Mappers.TeamManagementMapper>();
            container.RegisterType<ICommandHandler<Domain.Employee.Commands.IRemoveTeamMemberCommand>, Domain.Employee.Handlers.RemoveTeamMemberHandler>();
            container.RegisterType<ICommandHandler<Domain.Employee.Commands.IAddToTeamCommand>, Domain.Employee.Handlers.AddToTeamHandler>();
            container.RegisterType<IValidationHandler<Domain.Employee.Commands.IRemoveTeamMemberCommand>, Domain.Employee.Handlers.ValidateRemoveTeamMember>();
            container.RegisterType<IValidationHandler<Domain.Employee.Commands.IAddToTeamCommand>, Domain.Employee.Handlers.ValidateAddToTeam>();
            container.RegisterType<IMapper<Domain.Employee.Mappables.IEmpCsvMappable>, Domain.Employee.Mappers.EmpCsvMapper>();
            container.RegisterType<IValidationHandler<Domain.Employee.Commands.IEmpCsvUploadCommand>, Domain.Employee.Handlers.ValidateEmpCsvUpload>();
            container.RegisterType<IValidationHandler<Domain.Employee.Commands.IEmpCsvUploadRecordsCommand>, Domain.Employee.Handlers.ValidateEmpCsvUploadRecords>();
            container.RegisterType<ICommandHandler<Domain.Employee.Commands.IEmpCsvUploadCommand>, Domain.Employee.Handlers.EmpCsvUploadHandler>();

            container.RegisterType<IValidationHandler<Domain.Employee.Commands.IEmpProfileEditCommand>, Domain.Employee.Handlers.ValidateEmpProfileEdit>();
            container.RegisterType<ICommandHandler<Domain.Employee.Commands.IEmpProfileEditCommand>, Domain.Employee.Handlers.EmpProfileEditHandler>();
            container.RegisterType<IMapper<Domain.Employee.Mappables.IEmpProfileMappable>, Domain.Employee.Mappers.EmpProfileMapper>();
            container.RegisterType<IMapper<Domain.Employee.Mappables.IEmpProfileEditMappable>, Domain.Employee.Mappers.EmpProfileEditMapper>();
            container.RegisterType<IMapper<Domain.Employee.Mappables.IEmpMessageMappable>, Domain.Employee.Mappers.EmpMessageMapper>();
            container.RegisterType<ICommandHandler<Domain.Employee.Commands.IEmpMessageCommand>, Domain.Employee.Handlers.EmpMessageHandler>();
            container.RegisterType<IValidationHandler<Domain.Employee.Commands.IEmpMessageCommand>, Domain.Employee.Handlers.ValidateEmpMessage>();

            //Update Employee
            container.RegisterType<IValidationHandler<Domain.Employee.Commands.IEmpManageCommand>, Domain.Employee.Handlers.ValidateEmpManage>();
            container.RegisterType<IMapper<Domain.Employee.Mappables.IEmpManageMappable>, Domain.Employee.Mappers.EmpManageMapper>();
            container.RegisterType<ICommandHandler<Domain.Employee.Commands.IEmpManageCommand>, Domain.Employee.Handlers.EmpManageHandler>();

            //Login

            container.RegisterType<ICommandHandler<Domain.Login.Commands.ILoginCommand>, Domain.Login.Handlers.LoginHandler>();
            container.RegisterType<IValidationHandler<Domain.Login.Commands.ILoginCommand>, Domain.Login.Handlers.ValidateLogin>();
            container.RegisterType<IMapper<Domain.Login.Mappables.IForgotPassMappable>, Domain.Login.Mappers.ForgotPassMapper>();
            container.RegisterType<IMapper<Domain.Login.Mappables.IChangePassMappable>, Domain.Login.Mappers.ChangePassMapper>();
            container.RegisterType<ICommandHandler<Domain.Login.Commands.IChangePassCommand>, Domain.Login.Handlers.ChangePassHandler>();
            container.RegisterType<IValidationHandler<Domain.Login.Commands.IChangePassCommand>, Domain.Login.Handlers.ValidateChangePass>();
            container.RegisterType<IValidationHandler<Domain.Login.Commands.IForgotPassCommand>, Domain.Login.Handlers.ValidateForgotPass>();


       

            //Report
            container.RegisterType<ICommandHandler<Domain.Report.Commands.IDReportAddCommand>,Domain.Report.Handlers.DReportAddHandler>();
            container.RegisterType<ICommandHandler<Domain.Report.Commands.IDReportEditCommand>, Domain.Report.Handlers.DReportEditHandler>();
            container.RegisterType<IValidationHandler<Domain.Report.Commands.IDReportAddCommand>, Domain.Report.Handlers.ValidateDReportAdd>();
            container.RegisterType<IValidationHandler<Domain.Report.Commands.IDReportDetailsCommand>, Domain.Report.Handlers.ValidateDReportDetails>();
            container.RegisterType<IValidationHandler<Domain.Report.Commands.IDReportEditCommand>, Domain.Report.Handlers.ValidateDReportEdit>();
            container.RegisterType<IValidationHandler<Domain.Report.Commands.IDReportEditDetailsCommand>, Domain.Report.Handlers.ValidateDReportEditDetails>();
            container.RegisterType<IMapper<Domain.Report.Mappables.IDReportDetailsMappable>, Domain.Report.Mappers.DReportDetailsMapper>();
            container.RegisterType<IMapper<Domain.Report.Mappables.IDReportSearchMappable>, Domain.Report.Mappers.DReportSearchMapper>();
            container.RegisterType<IMapper<Domain.Report.Mappables.IDReportCSVMappable>, Domain.Report.Mappers.DReportCsvMapper>();
            container.RegisterType<IValidationHandler<Domain.Report.Commands.IDReportSearchCommand>, Domain.Report.Handlers.ValidateDReportSearch>();
            container.RegisterType<IMapper<Domain.Report.Mappables.IDReportDateRangeCSVMappable>, Domain.Report.Mappers.DReportDateRangeCSVMapper>();
            container.RegisterType<IMapper<Domain.Report.Mappables.IDReportRegistrationMappable>, Domain.Report.Mappers.DReportRegistrationMapper>();
            container.RegisterType<IMapper<Domain.Report.Mappables.IDReportEditMappable>, Domain.Report.Mappers.DReportEditMapper>();
            container.RegisterType<ICommandHandler<Domain.Report.Commands.IDReportCsvCommand>, Domain.Report.Handlers.DReportCsvHandler>();



            //pcode management

            container.RegisterType<ICommandHandler<Domain.Project.Commands.IPcodeRegistCommand>, Domain.Project.Handlers.PcodeRegistHandler>();
            container.RegisterType<IValidationHandler<Domain.Project.Commands.IPcodeRegistCommand>, Domain.Project.Handlers.ValidatePcodeRegist>();
            container.RegisterType<IValidationHandler<Domain.Project.Commands.IPcodeDetailsRegistCommand>, Domain.Project.Handlers.ValidatePcodeDetailsRegist>();
            container.RegisterType<IMapper<Domain.Project.Mappable.IPcodeListMappable>, Domain.Project.Mapper.PcodeListMapper>();
            container.RegisterType<IValidationHandler<Domain.Project.Commands.IPcodeDeleteCommand>, Domain.Project.Handlers.ValidatePcodeDelete>();
            container.RegisterType<ICommandHandler<Domain.Project.Commands.IPcodeDeleteCommand>, Domain.Project.Handlers.PcodeDeleteHandler>();
            container.RegisterType<IMapper<Domain.Project.Mappable.IPcodeEditMappable>, Domain.Project.Mapper.PcodeEditMapper>();
       
      

            //Employee Project
            container.RegisterType<IMapper<Domain.Employee.Mappables.IEmployeeProjectMappable>, Domain.Employee.Mappers.EmployeeProjectMapper>();

            //Employee Schedule
            container.RegisterType<IMapper<Domain.Schedule.Mappables.IScheduleMemberMappable>,Domain.Schedule.Mappers.ScheduleMemberMapper>();
            container.RegisterType<IValidationHandler<Domain.Schedule.Commands.IUploadEmpScheduleCommand>, Domain.Schedule.Handlers.ValidateUploadEmpSchedule>();
            container.RegisterType<IValidationHandler<Domain.Schedule.Commands.IUploadEmpScheduleRecordCommand>, Domain.Schedule.Handlers.ValidateUploadEmpScheduleRecord>();
            container.RegisterType<ICommandHandler<Domain.Schedule.Commands.IUploadEmpScheduleCommand>, Domain.Schedule.Handlers.UploadEmpScheduleHandler> ();
            container.RegisterType<IMapper<Domain.Schedule.Mappables.IScheduleSearchMappable>, Domain.Schedule.Mappers.ScheduleSearchMapper>();
            container.RegisterType<IValidationHandler<Domain.Schedule.Commands.IEmployeeScheduleCommand>, Domain.Schedule.Handlers.ValidateEmployeeSchedule>();

            //Request
            container.RegisterType<IMapper<Domain.Request.Mappables.IRequestMappable>, Domain.Request.Mappers.RequestMapper>();
            container.RegisterType<IValidationHandler<Domain.Request.Commands.IUndertimeCommand>, Domain.Request.Handlers.ValidateUndertime>();
            container.RegisterType<ICommandHandler<Domain.Request.Commands.IUndertimeCommand>, Domain.Request.Handlers.UndertimeHandler>();
            container.RegisterType<IValidationHandler<Domain.Request.Commands.IOvertimeCommand>, Domain.Request.Handlers.ValidateOvertime>();
            container.RegisterType<IValidationHandler<Domain.Request.Commands.IOvertimeDetailsCommand>, Domain.Request.Handlers.ValidateOvertimeDetails>();
            container.RegisterType<ICommandHandler<Domain.Request.Commands.IOvertimeCommand>, Domain.Request.Handlers.OvertimeHandler>();
            container.RegisterType<IValidationHandler<Domain.Request.Commands.IAttendanceLogsheetCommand>, Domain.Request.Handlers.ValidateAttendanceLogsheet>();
            container.RegisterType<IValidationHandler<Domain.Request.Commands.IAttendanceLogsheetDetailsCommand>, Domain.Request.Handlers.ValidateAttendanceLogsheetDetails>();
            container.RegisterType<ICommandHandler<Domain.Request.Commands.IAttendanceLogsheetCommand>, Domain.Request.Handlers.AttendanceLogsheetHandler>();
            container.RegisterType<IMapper<Domain.Request.Mappables.ILeaveRequestMappable>, Domain.Request.Mappers.LeaveRequestMapper>();
            container.RegisterType<IValidationHandler<Domain.Request.Commands.ILeaveRequestCommand>, Domain.Request.Handlers.ValidateLeaveRequest>();
            container.RegisterType<ICommandHandler<Domain.Request.Commands.ILeaveRequestCommand>, Domain.Request.Handlers.LeaveRequestHandler>();
            container.RegisterType<IMapper<Domain.Request.Mappables.IRequestMappable>, Domain.Request.Mappers.RequestMapper>();
            container.RegisterType<ICommandHandler<Domain.Request.Commands.IUpdateRequestStatusCommand>, Domain.Request.Handlers.UpdateRequestStatusHandler>();
            container.RegisterType<IValidationHandler<Domain.Request.Commands.IUpdateRequestStatusCommand>, Domain.Request.Handlers.ValidateUpdateRequestStatus>();
            container.RegisterType<IMapper<Domain.Request.Mappables.IEmployeeDetailsRequestMappable>, Domain.Request.Mappers.EmployeeDetailsRequestMapper>();
            container.RegisterType<IValidationHandler<Domain.Request.Commands.IRequestSearchCommand>, Domain.Request.Handlers.ValidateRequestSearch>();
            container.RegisterType<ICommandHandler<Domain.Request.Commands.IReceiveRequestCommand>, Domain.Request.Handlers.ReceiveRequestHandler>();
            container.RegisterType<IValidationHandler<Domain.Request.Commands.IReceiveRequestCommand>, Domain.Request.Handlers.ValidateReceiveRequestStatus>();

            //Pdf 
            container.RegisterType<IMapper<Domain.Pdf.Mappables.IRequestPdfMappable>, Domain.Pdf.Mappers.RequestPdfMapper>();
            container.RegisterType<IMapper<Domain.Pdf.Mappables.IRequestDetailPdfMappable>, Domain.Pdf.Mappers.RequestDetailPdfMapper>();

            //api
            container.RegisterType<IMapper<Domain.api.Mappables.IRequestScheduleApiMappable>, Domain.api.Mappers.RequestScheduleApiMapper>();
            container.RegisterType<IMapper<Domain.api.Mappables.IUnseenMessageMappable>,Domain.api.Mappers.UnseenMessageMapper>();
            container.RegisterType<IMapper<Domain.api.Mappables.IErrorHeaderMappable>,Domain.api.Mappers.ErrorHeaderMapper>();
            container.RegisterType<IMapper<Domain.api.Mappables.IEmpListMappable>, Domain.api.Mappers.EmpListMapper>();
            container.RegisterType<IMapper<Domain.api.Mappables.IEmpDetailsMappable>, Domain.api.Mappers.EmpDetailsMapper>();
            container.RegisterType<IMapper<Domain.api.Mappables.IGetImageMappable>,Domain.api.Mappers.GetImageMapper>();
            container.RegisterType<IMapper<Domain.api.Mappables.ITeamManagementApiMappable>, Domain.api.Mappers.TeamManagementApiMapper>();


            //Home
            container.RegisterType<IValidationHandler<Domain.Home.Commands.IHomeAnnouncementCommand>, Domain.Home.Handlers.ValidateHomeAnnouncement>();
            container.RegisterType<ICommandHandler<Domain.Home.Commands.IHomeAnnouncementCommand>, Domain.Home.Handlers.HomeAnnouncementHandler>();
            container.RegisterType<IMapper<Domain.Home.Mappables.IHomeAnnouncementMappable>, Domain.Home.Mappers.HomeAnnouncementMapper>();
            container.RegisterType<IMapper<Domain.Home.Mappables.IHomeEmpMappable>, Domain.Home.Mappers.HomeEmpMapper>();
            container.RegisterType<IMapper<Domain.Home.Mappables.IEmployeeBirthdayDataMappable>, Domain.Home.Mappers.EmployeeBirthdayDataMapper>();

            // Home Graphs
            container.RegisterType<IMapper<Domain.Home.Mappables.IHomeDailyReportDataMappable>, Domain.Home.Mappers.HomeDailyReportDataMapper>();

            // Job
            container.RegisterType<IMapper<Domain.Job.Mappables.IJobTitleRegistMappable>, Domain.Job.Mappers.JobTitleRegistMapper>();
            container.RegisterType<IMapper<Domain.Job.Mappables.IJobTitleRegistRecordMappable>, Domain.Job.Mappers.JobTitleRegistRecordMapper>();
            container.RegisterType<ICommandHandler<Domain.Job.Commands.IJobTitleRegistCommand>, Domain.Job.Handlers.JobTitleRegistHandler>();
            container.RegisterType<IValidationHandler<Domain.Job.Commands.IJobTitleRegistCommand>, Domain.Job.Handlers.ValidateJobTitleRegist>();
            container.RegisterType<IValidationHandler<Domain.Job.Commands.IJobTitleRegistRecordCommand>, Domain.Job.Handlers.ValidateJobTitleRegistRecord>();


            return container;
        }
    }
}