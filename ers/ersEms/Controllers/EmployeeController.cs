﻿using ersEms.Domain.Employee.Commands;
using ersEms.Domain.Employee.Mappables;
using ersEms.Domain.Home.Commands;
using ersEms.Domain.Home.Mappables;
using ersEms.Models.Employee;
using ersEms.Models.Employee_Manage;
using ersEms.Models.Home;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ersEms.mvc;
using Microsoft.AspNet.SignalR;
using System;
using System.Web.Mvc;

namespace ersEms.Controllers
{
    [ErsAuthorization]
    [RoleCheck]
    [ValidateInput(false)]
    public class employeeController : ErsControllerSecure
    {
        private static IHubContext _hubContext = GlobalHost.ConnectionManager.GetHubContext<MyHub>();

        public ActionResult emp_home(home home, EnumEck? eck)
        {
            if (((ISession)ErsContext.sessionState).getUserState() != EnumUserState.LOGIN)
            {
                return RedirectToAction("Login", "login");
            }

            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(home);
                }
            }
            mapperBus.Map<IEmployeeBirthdayDataMappable>(home);

            home.report_summary = EnumReportSummary.Month;
            return View("emp_home", home);
        }

        [HttpPost]
        public ActionResult display_home(home home)
        {
            home.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", home.pageCnt, home.maxItemCount);

            // Fetch Announcements
            mapperBus.Map<IHomeAnnouncementMappable>(home);
            home.pager.LoadPageList(home.recordCount);

            // Fetch Employee's related details
            mapperBus.Map<IHomeEmpMappable>(home);


            // Fetch Daily Report Data for Graph
            if (home.isDefaultLoad)
            {
                home.report_summary = EnumReportSummary.Month;
            }
            if (home.loadGraph)
            {
                mapperBus.Map<IHomeDailyReportDataMappable>(home);
            }      
        

            // Retrieve declared properties of the current model.
            var modelDictionary = home.GetCurrentModelPropertiesAsDictionary();
            // Added position view data.
            modelDictionary.Add("position", ViewData["position"]);


            home.hasChanges = false;
            _hubContext.Clients.All.AnnouncementAdded(home.hasChanges); //does all the magic

            return Json(new { Model = modelDictionary }, JsonRequestBehavior.AllowGet);
        }

      

        [HttpPost]
        public ActionResult announcement_crud(home home)
        {

            ModelState.AddModelErrors(commandBus.Validate<IHomeAnnouncementCommand>(home), home);
            if (ModelState.IsValid)
            {
                this.commandBus.Submit((IHomeAnnouncementCommand)home, EnumCommandTransaction.BeginTransaction);
                home.hasChanges = true;
            }

            home.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", home.pageCnt, home.maxItemCount);

            // Fetch Announcements
            mapperBus.Map<IHomeAnnouncementMappable>(home);
            home.pager.LoadPageList(home.recordCount);

            // Fetch Employee's related details
            mapperBus.Map<IHomeEmpMappable>(home);


            // Retrieve declared properties of the current model.
            var modelDictionary = home.GetCurrentModelPropertiesAsDictionary();
            // Added position view data.
            modelDictionary.Add("position", ViewData["position"]);
            if (home.GetAllErrorMessageList().Count > 0)
            {
                modelDictionary.Add("errorList", home.GetAllErrorMessageList());
            }

            _hubContext.Clients.All.AnnouncementAdded(home.hasChanges);
            return Json(new{ Model = modelDictionary }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult emp_manage_complete(Employee_Manage emp_manage, Employee_Search emp_search, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IEmpManageCommand>(emp_manage), emp_manage);

            if (ModelState.IsValid)
            {
                commandBus.Submit((IEmpManageCommand)emp_manage, EnumCommandTransaction.BeginTransaction);
            }

            var modelDictionary = emp_manage.GetCurrentModelPropertiesAsDictionary();

            if(emp_manage.GetAllErrorMessageList().Count > 0)
            {
                modelDictionary.Add("errorList", emp_manage.GetAllErrorMessageList());
            }

            AddModelToView("emp_search", emp_search);

            emp_search.SetOutputHidden("emp_search", true);
            //return View("emp_manage_complete", emp_manage);
            return Json(new { manageModel = modelDictionary }, JsonRequestBehavior.AllowGet);
        }

        [ErsEmsProcessCompletion("emp_manage", mode = EnumHandlingMode.RESET)]
        public ActionResult emp_manage(Employee_Manage emp_manage, Employee_Search emp_search, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IEmpManageCommand>(emp_manage), emp_manage);
            if (!ModelState.IsValid && !this.IsErrorBack(eck))
            {
                ClearModelState(emp_manage);
            }
            mapperBus.Map<IEmpManageMappable>(emp_manage);

            AddModelToView("emp_search", emp_search);

            emp_manage.SetOutputHidden("emp_manage", true);

            emp_search.SetOutputHidden("emp_search", true);

            return View("emp_manage", emp_manage);
        }


        [ErsEmsProcessCompletion("emp_manage", mode = EnumHandlingMode.CHECK)]
        public ActionResult emp_manage_confirm(Employee_Manage emp_manage, Employee_Search emp_search, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IEmpManageCommand>(emp_manage), emp_manage);

            if (!ModelState.IsValid)
            {
                return this.emp_manage(emp_manage, emp_search, EnumEck.Error);
            }

            AddModelToView("emp_search", emp_search);

            emp_manage.SetOutputHidden(true);

            emp_search.SetOutputHidden("emp_search", true);

            return View("emp_manage_confirm", emp_manage);
        }


        //[ErsEmsProcessCompletion("emp_manage", mode = EnumHandlingMode.COMPLETION)]
       

        public ActionResult emp_search(emp_search emp_search, EnumEck? eck)
        {
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(emp_search);

                }
            }

            return View("employee_search",emp_search);
        }

        public virtual ActionResult emp_csv_download(Employee_Search emp_search)
        {

            mapperBus.Map<IEmpCsvMappable>(emp_search);
            return this.CsvFile(emp_search.csvCreater.filePath);
        }

        public ActionResult emp_csv_upload_confirm(EmpCsvUpload employeeCsv, Employee employee, emp_search emp_search)
        {
            if (employeeCsv.pager == false)
            {
                ModelState.AddModelErrors(commandBus.Validate<IEmpCsvUploadCommand>(employeeCsv), employeeCsv);
                if (!ModelState.IsValid)
                {
                    return this.emp_search(emp_search, EnumEck.Error);
                }
                commandBus.Submit<IEmpCsvUploadCommand>(employeeCsv, EnumCommandTransaction.BeginTransaction);
            }

            return this.emp_search(emp_search, EnumEck.Normal);
        }


        public ActionResult employee_message(Employee_Message employee_message, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IEmpMessageCommand>(employee_message), employee_message);
            if (!ModelState.IsValid)
            {
                if (!IsErrorBack(eck))
                {
                    ClearModelState(employee_message);
                }
            }
            mapperBus.Map<IEmpMessageMappable>(employee_message);

            employee_message.SetOutputHidden(true);

            return View("employee_message", employee_message);
        }

        public ActionResult employee_send_message(Employee_Message employee_message, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IEmpMessageCommand>(employee_message), employee_message);

            if (!ModelState.IsValid)
            {
                return this.employee_message(employee_message, EnumEck.Error);
            }

            commandBus.Submit<IEmpMessageCommand>(employee_message, EnumCommandTransaction.BeginTransaction);

            return this.employee_message(employee_message, null);
        }

        public ActionResult employee_delete_thread(Employee_Message employee_message, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IEmpMessageCommand>(employee_message), employee_message);
            if (!ModelState.IsValid)
            {
                return this.employee_message(employee_message, EnumEck.Error);
            }

            commandBus.Submit<IEmpMessageCommand>(employee_message, EnumCommandTransaction.BeginTransaction);

            return this.employee_message(employee_message, null);
        }

        public ActionResult team_management_list(Employee_Search team_mgnt, EnumEck? eck)
        {
            if (team_mgnt.MemberSearch == true)
            {
                ModelState.AddModelErrors(commandBus.Validate<IEmpSearchCommand>(team_mgnt), team_mgnt);
            }
            if (!ModelState.IsValid)
            {
                //Display error on this page.
            }

            //var team_pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", team_mgnt.pageCnt, team_mgnt.maxItemCount);
            //team_mgnt.pager = team_pager;

            //var non_team_pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "NonTeamPager", team_mgnt.non_team_page_count, team_mgnt.maxItemCount);
            //team_mgnt.non_team_pager = non_team_pager;

            //team_pager.LoadPageList(team_mgnt.recordCount);
            //non_team_pager.LoadPageList(team_mgnt.non_team_recordCount);

            return View("team_management_list", team_mgnt);
        }

        public ActionResult remove_team_member(Employee_Search team_mgnt)
        {
            ModelState.AddModelErrors(commandBus.Validate<IRemoveTeamMemberCommand>(team_mgnt), team_mgnt);
            if (!ModelState.IsValid)
            {
                return team_management_list(team_mgnt, EnumEck.Error);
            }
            commandBus.Submit<IRemoveTeamMemberCommand>(team_mgnt, EnumCommandTransaction.BeginTransaction);
            return RedirectToAction("team_management_list", "Employee");
        }

        public ActionResult add_to_team(Employee_Search team_mgnt)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAddToTeamCommand>(team_mgnt), team_mgnt);
            if (!ModelState.IsValid)
            {
                return team_management_list(team_mgnt, EnumEck.Error);
            }
            commandBus.Submit<IAddToTeamCommand>(team_mgnt, EnumCommandTransaction.BeginTransaction);
            return RedirectToAction("team_management_list", "Employee");
        }

        public ActionResult emp_profile(emp_profile emp_profile)
        {
            mapperBus.Map<IEmpProfileMappable>(emp_profile);
            return View("emp_profile", emp_profile);
        }

        [ErsEmsProcessCompletion("emp_profile_edit",mode = EnumHandlingMode.RESET)]
        public ActionResult emp_profile_edit(emp_profile_edit emp_profile_edit, EnumEck? eck, emp_profile emp_profile = null)
        {
            if (this.IsErrorBack(eck))
            {
                ModelState.AddModelErrors(commandBus.Validate<IEmpProfileEditCommand>(emp_profile_edit), emp_profile_edit);
                if (!ModelState.IsValid)
                {
                    ClearModelState(emp_profile);
                }
            }
            else
            {
                mapperBus.Map<IEmpProfileMappable>(emp_profile);
                if (emp_profile.employee != null)
                {
                    emp_profile_edit.OverwriteWithParameter(emp_profile.employee.GetPropertiesAsDictionary());
                    emp_profile_edit.email_confirm = emp_profile_edit.email;
                }

            }
            mapperBus.Map<IEmpProfileEditMappable>(emp_profile_edit);

            emp_profile_edit.SetOutputHidden("edit", true);

            return View("emp_profile_edit", emp_profile_edit);
        }

        [ErsEmsProcessCompletion("emp_profile_edit", mode = EnumHandlingMode.CHECK)]
        public ActionResult emp_profile_edit_confirm(emp_profile_edit emp_profile_edit)
        {
            ModelState.AddModelErrors(commandBus.Validate<IEmpProfileEditCommand>(emp_profile_edit), emp_profile_edit);
            if (!ModelState.IsValid)
            {
                return this.emp_profile_edit(emp_profile_edit, EnumEck.Error);
            }

            mapperBus.Map<IEmpProfileEditMappable>(emp_profile_edit);

            emp_profile_edit.SetOutputHidden("edit",true);

            emp_profile_edit.SetOutputHidden("edit_confirm", true);

            return View("emp_profile_edit_confirm", emp_profile_edit);
        }

        [ErsEmsProcessCompletion("emp_profile_edit", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult emp_profile_edit_complete(emp_profile_edit emp_profile_edit)
        {
            ModelState.AddModelErrors(commandBus.Validate<IEmpProfileEditCommand>(emp_profile_edit), emp_profile_edit);

            if (!ModelState.IsValid)
            {
                return this.emp_profile_edit(emp_profile_edit, EnumEck.Error);
            }

            emp_profile_edit.position = (EnumPosition?)Convert.ToInt32(ViewData["position"]);

            mapperBus.Map<IEmpProfileEditMappable>(emp_profile_edit);

            commandBus.Submit((IEmpProfileEditCommand)emp_profile_edit, EnumCommandTransaction.BeginTransaction);

            return View("emp_profile_edit_complete", emp_profile_edit);

        }

    }
}