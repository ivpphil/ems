﻿using ersEms.Domain.Pdf.Mappables;
using ersEms.Models.Pdf;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ersEms.mvc;
using System.Web.Mvc;

namespace ersEms.Controllers
{
    [ErsAuthorization]
    [RoleCheck]
    [ValidateInput(false)]
    public class PdfController : ErsControllerSecure
    {
        /// <summary>
        /// download approved request forms using request_ids
        /// </summary>
        /// <param name="download_requests"></param>
        /// <returns></returns>
        public ActionResult download_requests(download_request download_request)
        {
            this.mapperBus.Map<IRequestPdfMappable>(download_request);

            download_request.form.init(download_request.pdf_template, download_request.file_name);

            return PdfFile(download_request.form.file_path);
        }
    }
}