﻿using ersEms.Domain.Login.Commands;
using ersEms.Domain.Login.Mappables;
using ersEms.jp.co.ivp.ers.mvc;
using ersEms.Models.Login;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using System.Web.Mvc;

namespace ersEms.Controllers
{
    [ValidateInput(false)]
    [ErsRequireHttps]
    public class LoginController:ErsControllerSecure
    {
        
        public ActionResult login(Login login,EnumEck? eck)
        {
            var objc = new OtherCookie();
            if(objc.GetCoookieEmail("login_email")!= null)
            {
                login.email = objc.GetCoookieEmail("login_email");
                login.email_ck = true;
            }


            if (((ISession)ErsContext.sessionState).getUserState() == EnumUserState.LOGIN)
            {
                return RedirectToAction("emp_home", "Employee");
            }

            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {

                    this.ClearModelState(login);

                }
            }


            return View("login",login);
        }
    

        [HttpPost]
        public virtual ActionResult loginaction(Login login)
        {

            ModelState.AddModelErrors(commandBus.Validate<ILoginCommand>(login), login);
            if (!ModelState.IsValid)
            {
                return this.login(login, EnumEck.Error);
            }
            
            commandBus.Submit<ILoginCommand>(login, EnumCommandTransaction.BeginTransaction);

            login.SetOutputHidden(true);
            EnumUserState state = ((ISession)ErsContext.sessionState).getUserState();
            
            
            if(((ISession)ErsContext.sessionState).getUserState() == EnumUserState.LOGIN)
            {
              
                return RedirectToAction("emp_home", "employee");
            }

            return RedirectToAction("login", "Login");
        }



        public virtual ActionResult logout(Login login)
        {

           ((ISession)ErsContext.sessionState).LogOut();
           var objc = new OtherCookie();
           return RedirectToAction("index", "home");
        }


      

        public ActionResult forgot_pass(Forgot_pass forgot_pass,EnumEck? eck)
        {

            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {

                    this.ClearModelState(forgot_pass);

                }
            }

            return View("forgot_pass",forgot_pass);
        }

        public ActionResult forgot_pass2(Forgot_pass forgot_pass)
        {
            ModelState.AddModelErrors(commandBus.Validate<IForgotPassCommand>(forgot_pass), forgot_pass);
            if (!ModelState.IsValid)
            {
                return this.forgot_pass(forgot_pass, EnumEck.Error);
            }

            this.mapperBus.Map<IForgotPassMappable>(forgot_pass);

            var sendmailforgot_pass = ErsFactory.ersMailFactory.getErsSendMailForgetPass();
            sendmailforgot_pass.Send(forgot_pass, forgot_pass.email, forgot_pass.mformat);

            return View("forgot_pass2", forgot_pass);

            
        }


        public ActionResult change_pass1(Change_pass change_pass, EnumEck? eck = null)
        {

            ModelState.AddModelErrors(commandBus.Validate<IChangePassCommand>(change_pass), change_pass);
            if(!ModelState.IsValid)
            {
                if(!this.IsErrorBack(eck))
                {
                    ClearModelState(change_pass);
                }
            }

            change_pass.SetOutputHidden("session", true);
            return View("change_pass1", change_pass);
        }

        public ActionResult change_pass2(Change_pass change_pass, EnumEck? eck = null)
        {
            ModelState.AddModelErrors(commandBus.Validate<IChangePassCommand>(change_pass), change_pass);
            if (!ModelState.IsValid)
            {
                return change_pass1(change_pass, EnumEck.Error);

            }
            this.mapperBus.Map<IChangePassMappable>(change_pass);
            commandBus.Submit<IChangePassCommand>(change_pass, EnumCommandTransaction.BeginTransaction);
            return View("change_pass2", change_pass);
        }

        public ActionResult about(Login login)
        {
            return View("about",login);
        }

    }
}