﻿using ersEms.Domain.Job.Commands;
using ersEms.Domain.Job.Mappables;
using ersEms.Models.Job;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ersEms.mvc;
using System.Web.Mvc;

namespace ersEms.Controllers
{
    [ErsAuthorization]
    [RoleCheck]
    [ValidateInput(false)]
    public class jobController:ErsControllerSecure
    {
        [ErsEmsProcessCompletion("job_title_regist", mode = EnumHandlingMode.RESET)]
        public ActionResult job_title_regist(Job job_title_regist, EnumEck? eck)
        {
            if (!this.ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(job_title_regist);
                }
            }
            else if(eck == EnumEck.BackPage)
            {
                job_title_regist.isInitialized = true;
            }
            mapperBus.Map<IJobTitleRegistMappable>(job_title_regist);

            if (job_title_regist.job_details == null || job_title_regist.job_details.Count == 0)
            {
                job_title_regist.noRecord_flg = true;
            }
            
            return View("job_title_regist", job_title_regist);
        }

        [ErsEmsProcessCompletion("job_title_regist", mode = EnumHandlingMode.CHECK)]
        public ActionResult job_title_regist_confirm(Job job_title_regist)
        {
            ModelState.AddModelErrors(commandBus.Validate<IJobTitleRegistCommand>(job_title_regist), job_title_regist);
            if (!this.ModelState.IsValid)
            {
                job_title_regist.isInitialized = true;
                return this.job_title_regist(job_title_regist, EnumEck.Error);
            }

            job_title_regist.SetOutputHidden(true);
            return View("job_title_regist_confirm", job_title_regist);
        }

        [ErsEmsProcessCompletion("job_title_regist", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult job_title_regist_complete(Job job_title_regist)
        {
            ModelState.AddModelErrors(commandBus.Validate<IJobTitleRegistCommand>(job_title_regist), job_title_regist);
            if (!this.ModelState.IsValid)
            {
                job_title_regist.isInitialized = true;
                return this.job_title_regist(job_title_regist, EnumEck.Error);
            }
            
            commandBus.Submit((IJobTitleRegistCommand)job_title_regist, EnumCommandTransaction.BeginTransaction);
            return View("job_title_regist_complete", job_title_regist);
        }
        
    }
}