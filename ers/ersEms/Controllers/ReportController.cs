﻿using ersEms.Domain.Report.Commands;
using ersEms.Domain.Report.Mappables;
using ersEms.Models.Report;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ersEms.mvc;
using System;
using System.Web.Mvc;

namespace ersEms.Controllers
{
    [ErsAuthorization]
    [RoleCheck]
    [ValidateInput(false)]
    public class reportController:ErsControllerSecure
    {
        [ErsEmsProcessCompletion("dreport_edit", mode = EnumHandlingMode.RESET)]
        public ActionResult dreport_edit(DReport dreport,EnumEck? eck)
        {
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(dreport);
                }
            }

            mapperBus.Map<IDReportEditMappable>(dreport);
            return View("dreport_edit", dreport);
        }


        [ErsEmsProcessCompletion("dreport_edit", mode = EnumHandlingMode.CHECK)]
        public ActionResult dreport_edit_confirm(DReport dreport,DReportSearch dreport_search)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDReportEditCommand>(dreport), dreport);
            if (!ModelState.IsValid)
            {
                return this.dreport_edit(dreport, EnumEck.Error);
            }
           
            dreport.SetOutputHidden(true);
            dreport.fromedit = true;
            this.AddModelToView(dreport_search);

            return View("dreport_edit_confirm", dreport);
        }
        

        [ErsEmsProcessCompletion("dreport_edit", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult dreport_edit_complete(DReport dreport)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDReportEditCommand>(dreport), dreport);
            if (!ModelState.IsValid)
            {
                return this.dreport_edit(dreport, EnumEck.Error);
            }
            
            dreport.SetOutputHidden("emp_pos", true);
            commandBus.Submit((IDReportEditCommand)dreport, EnumCommandTransaction.BeginTransaction);

            return View("dreport_edit_complete", dreport);
        }
                
        public ActionResult dreport_details(DReport dreport,DReportSearch dreport_search)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDReportEditCommand>(dreport), dreport);

            if (!ModelState.IsValid)
            {
                return this.dreport_search(dreport_search, EnumEck.Error);
            }

            mapperBus.Map<IDReportDetailsMappable>(dreport);

            this.AddModelToView(dreport_search);
            dreport_search.SetOutputHidden(true);
            dreport.SetOutputHidden(true);

            return View("dreport_details", dreport);
        }
        
        [ErsEmsProcessCompletion("dreport_add", mode = EnumHandlingMode.RESET)]
        public ActionResult dreport_add(DReport dreport,EnumEck? eck)
        {
            if (!ModelState.IsValid)
            {
                if(!this.IsErrorBack(eck))
                {
                    this.ClearModelState(dreport);
                }
            }
            
            dreport.SetDefaultReportDate();
            dreport.SetOutputHidden("emp_pos", true);

            return View("dreport_add", dreport);
        }
        
        [ErsEmsProcessCompletion("dreport_add", mode = EnumHandlingMode.CHECK)]
        public ActionResult dreport_add_confirm(DReport dreport)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDReportAddCommand>(dreport), dreport);
            if (!ModelState.IsValid)
            {
                return this.dreport_add(dreport, EnumEck.Error);
            }
                         
            dreport.SetOutputHidden(true);
            return View("dreport_add_confirm", dreport);
        }


        [ErsEmsProcessCompletion("dreport_add", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult dreport_add_complete(DReport dreport)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDReportAddCommand>(dreport), dreport);
            if (!ModelState.IsValid)
            {
                return this.dreport_add(dreport, EnumEck.Error);
            }
           
            commandBus.Submit((IDReportAddCommand)dreport, EnumCommandTransaction.BeginTransaction);
            return View("dreport_add_complete", dreport);
        }

        public ActionResult dreport_search(DReportSearch dreport_search,EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDReportSearchCommand>(dreport_search), dreport_search);            

            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))

                {
                    this.ClearModelState(dreport_search);
                   
                }
            }
                    
            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", dreport_search.pageCnt, dreport_search.maxItemCount);
            dreport_search.pager = pager;
            dreport_search.emp_pos = (EnumPosition)Convert.ToInt32(ViewData["position"]);
            mapperBus.Map<IDReportSearchMappable>(dreport_search);
            pager.LoadPageList(dreport_search.recordCount);

            return View("dreport_search", dreport_search);
        }

        public ActionResult dreport_search_result(DReportSearch dreport_search)
        {
            ModelState.AddModelErrors(commandBus.Validate<IDReportSearchCommand>(dreport_search), dreport_search);
            if (!ModelState.IsValid)
            {

                return this.dreport_search(dreport_search, EnumEck.Error);
            }
            
            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", dreport_search.pageCnt, dreport_search.maxItemCount);
            dreport_search.pager = pager;
            dreport_search.SetOutputHidden(true);
            dreport_search.emp_pos = (EnumPosition)Convert.ToInt32(ViewData["position"]);
            mapperBus.Map<IDReportSearchMappable>(dreport_search);
            pager.LoadPageList(dreport_search.recordCount);

            return View("dreport_search", dreport_search);
        }
        
        public virtual ActionResult download_csv_all(DReportSearch dreport_search)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            dreport_search.emp_pos = (EnumPosition)Convert.ToInt32(ViewData["position"]);
            mapperBus.Map<IDReportCSVMappable>(dreport_search);
            commandBus.Submit((IDReportCsvCommand)dreport_search, EnumCommandTransaction.BeginTransaction);

            return this.CsvFile(dreport_search.csvCreater.filePath);
        }

        public virtual ActionResult download_csv_page(DReportSearch dreport_search)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            dreport_search.emp_pos = (EnumPosition)Convert.ToInt32(ViewData["position"]);
            dreport_search.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(dreport_search.pageCnt, dreport_search.maxItemCount);
            mapperBus.Map<IDReportSearchMappable>(dreport_search);
            mapperBus.Map<IDReportCSVMappable>(dreport_search);
            commandBus.Submit((IDReportCsvCommand)dreport_search, EnumCommandTransaction.BeginTransaction);

            return this.CsvFile(dreport_search.csvCreater.filePath);
        }
                
        public virtual ActionResult download_csv_date_range(DReportSearch dreport_search)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            dreport_search.emp_pos = (EnumPosition)Convert.ToInt32(ViewData["position"]);
            mapperBus.Map<IDReportSearchMappable>(dreport_search);
            mapperBus.Map<IDReportDateRangeCSVMappable>(dreport_search);
            commandBus.Submit((IDReportCsvCommand)dreport_search, EnumCommandTransaction.BeginTransaction);

            return this.CsvFile(dreport_search.csvCreater.filePath);
        }
    }
}