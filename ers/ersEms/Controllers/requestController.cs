using ersEms.Domain.Request.Commands;
using ersEms.Domain.Request.Mappables;
using ersEms.Models.Request;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ersEms.mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

namespace ersEms.Controllers
{
    [ErsAuthorization]
    [RoleCheck]
    [ValidateInput(false)]
    public class requestController : ErsControllerSecure
    {
        public ActionResult request(Request request, EnumEck? eck)
        {
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(request);
                }
            }
        
                request.position = (EnumPosition?)Convert.ToInt32(ViewData["position"]);
            this.mapperBus.Map<IRequestMappable>(request);

            var setup = ErsFactory.ersUtilityFactory.getSetup();
            if (setup.hr_emp_no == request.emp_no)
            {
                request.isHR = true;
            }

            return View("request", request);

        }

        public ActionResult request_search(Request request, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IRequestSearchCommand>(request), request);
            if (!ModelState.IsValid)
            {
                return View("request", request);
            }
            return this.request(request, EnumEck.Normal);
        }

        [ErsEmsProcessCompletion("undertime_form", mode = EnumHandlingMode.RESET)]
        public ActionResult undertime_form(Request undertime, EnumEck? eck)
        {
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(undertime);
                }
            }

            mapperBus.Map<IEmployeeDetailsRequestMappable>(undertime);

            undertime.SetOutputHidden("ut", true);

            return View("undertime_form", undertime);
        }

        [ErsEmsProcessCompletion("undertime_form", mode = EnumHandlingMode.CHECK)]
        public ActionResult undertime_form_confirm(Request undertime)
        {
            ModelState.AddModelErrors(commandBus.Validate<IUndertimeCommand>(undertime), undertime);
            if (!ModelState.IsValid)
            {
                return this.undertime_form(undertime, EnumEck.Error);
            }

            mapperBus.Map<IEmployeeDetailsRequestMappable>(undertime);

            undertime.SetOutputHidden(true);

            return View("undertime_form_confirm", undertime);
        }

        [ErsEmsProcessCompletion("undertime_form", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult undertime_form_complete(Request undertime)
        {
            ModelState.AddModelErrors(commandBus.Validate<IUndertimeCommand>(undertime), undertime);
            if (!ModelState.IsValid)
            {
                return this.undertime_form(undertime, EnumEck.Error);
            }

            commandBus.Submit((IUndertimeCommand)undertime, EnumCommandTransaction.BeginTransaction);

            return View("undertime_form_complete", undertime);
        }

        [ErsEmsProcessCompletion("logsheet_form", mode = EnumHandlingMode.RESET)]
        public ActionResult logsheet_form(Request logsheet, EnumEck? eck)
        {
            if (!ModelState.IsValid && !this.IsErrorBack(eck))
            {
                this.ClearModelState(logsheet);
            }

            mapperBus.Map<IEmployeeDetailsRequestMappable>(logsheet);
            return View("logsheet_form", logsheet);
        }

        [ErsEmsProcessCompletion("logsheet_form", mode = EnumHandlingMode.CHECK)]
        public ActionResult logsheet_form_confirm(Request logsheet)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAttendanceLogsheetCommand>(logsheet), logsheet);
            if (!this.ModelState.IsValid)
            {
                return this.logsheet_form(logsheet, EnumEck.Error);
            }

            logsheet.SetOutputHidden(true);
            return View("logsheet_form_confirm", logsheet);
        }

        [ErsEmsProcessCompletion("logsheet_form", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult logsheet_form_complete(Request logsheet)
        {
            ModelState.AddModelErrors(commandBus.Validate<IAttendanceLogsheetCommand>(logsheet), logsheet);
            if (!this.ModelState.IsValid)
            {
                return this.logsheet_form(logsheet, EnumEck.Error);
            }

            commandBus.Submit((IAttendanceLogsheetCommand)logsheet, EnumCommandTransaction.BeginTransaction);
            return View("logsheet_form_complete", logsheet);
        }

        [ErsEmsProcessCompletion("overtime_request", mode = EnumHandlingMode.RESET)]
        public ActionResult overtime_request(Request overtime, EnumEck? eck)
        {
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(overtime);
                }
            }
            mapperBus.Map<IEmployeeDetailsRequestMappable>(overtime);

            return View("overtime_request", overtime);
        }

        [ErsEmsProcessCompletion("overtime_request", mode = EnumHandlingMode.CHECK)]
        public ActionResult overtime_request_confirm(Request overtime, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IOvertimeCommand>(overtime), overtime);
            if (!ModelState.IsValid)
            {
                return this.overtime_request(overtime, EnumEck.Error);
            }
            mapperBus.Map<IEmployeeDetailsRequestMappable>(overtime);
            overtime.SetOutputHidden(true);
            return View("overtime_request_confirm", overtime);
        }

        [ErsEmsProcessCompletion("overtime_request", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult overtime_request_complete(Request overtime, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IOvertimeCommand>(overtime), overtime);
            if (!ModelState.IsValid)
            {
                return this.overtime_request(overtime, EnumEck.Error);
            }

            commandBus.Submit((IOvertimeCommand)overtime, EnumCommandTransaction.BeginTransaction);
            return View("overtime_request_complete", overtime);
        }


        public ActionResult request_status_update(Request req, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IUpdateRequestStatusCommand>(req), req);
            if (!ModelState.IsValid)
            {
                return this.request(req, EnumEck.Error);
            }

            commandBus.Submit((IUpdateRequestStatusCommand)req, EnumCommandTransaction.BeginTransaction);
            return RedirectToAction("request", "Request");
        }

        [ErsEmsProcessCompletion("leave_request", mode = EnumHandlingMode.RESET)]
        public ActionResult leave_request(Request leave, EnumEck? eck = null)
        {
            if (!ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(leave);
                }
            }

            if (!this.IsErrorBack(eck) && !leave.IsReturn)
            {
                this.ClearModelState(leave);
                mapperBus.Map<ILeaveRequestMappable>(leave);
            }

            mapperBus.Map<IEmployeeDetailsRequestMappable>(leave);

            return View("leave_request", leave);
        }

        [ErsEmsProcessCompletion("leave_request", mode = EnumHandlingMode.CHECK)]
        public ActionResult leave_request_confirm(Request leave)
        {
            ModelState.AddModelErrors(commandBus.Validate<ILeaveRequestCommand>(leave), leave);
            if (!ModelState.IsValid)
            {
                return this.leave_request(leave, EnumEck.Error);
            }

            mapperBus.Map<IEmployeeDetailsRequestMappable>(leave);

            leave.SetOutputHidden(true);
            return View("leave_request_confirm", leave);
        }

        [ErsEmsProcessCompletion("leave_request", mode = EnumHandlingMode.COMPLETION)]
        public ActionResult leave_request_complete(Request leave)
        {
            ModelState.AddModelErrors(commandBus.Validate<ILeaveRequestCommand>(leave), leave);
            if (!ModelState.IsValid)
            {
                return this.leave_request(leave, EnumEck.Error);
            }

            commandBus.Submit((ILeaveRequestCommand)leave, EnumCommandTransaction.BeginTransaction);

            return View("leave_request_complete", leave);
        }

        public ActionResult receive_request(Request req, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IReceiveRequestCommand>(req), req);
            if (!ModelState.IsValid)
            {
                return this.request(req, EnumEck.Error);
            }
            
            commandBus.Submit((IReceiveRequestCommand)req, EnumCommandTransaction.BeginTransaction);

            if (req.isHR == true)
            {
                var list = new Dictionary<string, object>();
                list["single_download_request"] = req.str_request_id;

                return this.RedirectToAction("download_requests", "Pdf", new RouteValueDictionary(list));
            }
             return RedirectToAction("request","Request");
        }        
    }
}