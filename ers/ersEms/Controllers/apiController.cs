﻿using ersEms.Domain.api.Mappables;
using ersEms.Domain.Employee.Mappables;
using ersEms.Models.api;
using ersEms.Models.Employee;
using ersEms.Models.Request;
using jp.co.ivp.ers;
using jp.co.ivp.ers.state;
using System.Web.Mvc;

namespace ersEms.Controllers
{
    public class apiController : ErsControllerSecure
    {
        public ActionResult errorHeader(error_header err)
        {
            mapperBus.Map<IErrorHeaderMappable>(err);
            return Json(new { position = err.pos, islogin = err.islogin }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getImageMember(Employee employee)
        {
            mapperBus.Map<IGetImageMappable>(employee);
            return Json(new { hasImage = employee.hasImage, fullname = employee.fullname,lname = employee.lname, fname = employee.fname, emp_no = employee.emp_no }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getUnseenMessage(unseen_messages unseenmsg)
        {
            mapperBus.Map<IUnseenMessageMappable>(unseenmsg);
            return Json(new { inbox_message_count = unseenmsg.unseen_message_count }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getShiftSched(shift_sched shiftSched)
        {
            mapperBus.Map<IRequestScheduleApiMappable>(shiftSched);
            return Json(new { Shift_schedule = shiftSched.shift }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getOtHours(overtime_details otdetails)
        {
            return Json(new { ot_hours =  otdetails.ot_hours}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getEmployeeList(emp_list emp_list)
        {
            base.mapperBus.Map<Domain.api.Mappables.IEmpListMappable>(emp_list);
            var returnObj = emp_list.GetCurrentModelPropertiesAsDictionary();
            return Json(new { Model = returnObj }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getEmpDetails(emp_details emp_details)
        {
            base.mapperBus.Map<Domain.api.Mappables.IEmpDetailsMappable>(emp_details);
            //return Json(new { Model = returnObj }, JsonRequestBehavior.AllowGet);
            return null;
        }

        public ActionResult getTeamList(team_management team_mgnt)
        {
            var team_pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", team_mgnt.pageCnt, team_mgnt.maxItemCount);
            team_mgnt.pager = team_pager;

            var non_team_pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "NonTeamPager", team_mgnt.non_team_page_count, team_mgnt.maxItemCount);
            team_mgnt.non_team_pager = non_team_pager;

            base.mapperBus.Map<ITeamManagementApiMappable>(team_mgnt);
            var returnObj = team_mgnt.GetCurrentModelPropertiesAsDictionary();
            return Json(new { Model = returnObj }, JsonRequestBehavior.AllowGet);
        }
    }
}