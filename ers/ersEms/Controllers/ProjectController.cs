﻿using ersEms.Domain.Project.Commands;
using ersEms.Domain.Project.Mappable;
using ersEms.jp.co.ivp.ers.mvc;
using ersEms.Models.Project;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ersEms.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ersEms.Controllers
{
    [ErsAuthorization]
    [RoleCheck]
    [ValidateInput(false)]
    public class projectController:ErsControllerSecure
    {
        public ActionResult pcode_regist(Pcode pcode_regist, EnumEck? eck)
        {
            if (!this.ModelState.IsValid)
            {
                if(!this.IsErrorBack(eck))
                {
                    this.ClearModelState(pcode_regist);
                }
            }

            if(pcode_regist.pcode_details == null || pcode_regist.pcode_details.Count == 0)
            {
                pcode_regist.noRecord_flg = true;
            }

            return View("pcode_regist", pcode_regist);
        }
        
        public ActionResult pcode_regist_confirm(Pcode pcode_regist)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPcodeRegistCommand>(pcode_regist), pcode_regist);
            if(!this.ModelState.IsValid)
            {
                return this.pcode_regist(pcode_regist, EnumEck.Error);
            }

            pcode_regist.SetOutputHidden(true);
            return View("pcode_regist_confirm",pcode_regist);
        }

        public ActionResult pcode_regist_complete(Pcode pcode_regist)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPcodeRegistCommand>(pcode_regist), pcode_regist);
            if (!this.ModelState.IsValid)
            {
                return this.pcode_regist(pcode_regist, EnumEck.Error);
            }
            
            commandBus.Submit((IPcodeRegistCommand)pcode_regist, EnumCommandTransaction.BeginTransaction);
            return View("pcode_regist_complete",pcode_regist);
        }
        
        public ActionResult pcode_list(Pcode pcode_list, EnumEck? eck)
        {
            if (!this.ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(pcode_list);
                }
            }
            pcode_list.pager  = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", pcode_list.pageCnt, pcode_list.maxItemCount);

            mapperBus.Map<IPcodeListMappable>(pcode_list);

            pcode_list.pager.LoadPageList(pcode_list.recordCount);

            return View("pcode_list", pcode_list);
        }

        public ActionResult pcode_delete(Pcode pcodes)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPcodeDeleteCommand>(pcodes), pcodes);
            if (!this.ModelState.IsValid)
            {
                return this.pcode_list(pcodes, EnumEck.Error);
            }

            mapperBus.Map<IPcodeEditMappable>(pcodes);
          
            return View("pcode_delete",pcodes);
        }
        
        public ActionResult pcode_delete_complete(Pcode pcodes)
        {
            ModelState.AddModelErrors(commandBus.Validate<IPcodeDeleteCommand>(pcodes), pcodes);
           if(!this.ModelState.IsValid)
            {
                return this.pcode_list(pcodes, EnumEck.Error);
            }

            commandBus.Submit((IPcodeDeleteCommand)pcodes, EnumCommandTransaction.BeginTransaction);
            return View("pcode_delete_complete",pcodes);
        }

        public ActionResult dreport_pcode_list(Pcode pcode, EnumEck? eck)
        {
            if (!this.ModelState.IsValid)
            {
                if (!this.IsErrorBack(eck))
                {
                    this.ClearModelState(pcode);
                }
            }
            pcode.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", pcode.pageCnt, pcode.maxItemCount);

            mapperBus.Map<IPcodeListMappable>(pcode);      

            pcode.pager.LoadPageList(pcode.recordCount);

            return View("dreport_pcode_list", pcode);
        }
    }
}