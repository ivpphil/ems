﻿using ersEms.Models;
using ersEms.Models.Login;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ersEms.Controllers
{
    public class HomeController : ErsControllerSecure
    {
        // GET: Home
        public ActionResult Index(Login login)
        {

            if (((ISession)ErsContext.sessionState).getUserState() == EnumUserState.LOGIN)
            {
                return RedirectToAction("emp_home", "Employee");
            }
            
            return RedirectToAction("login", "Login");
        }
    }
}