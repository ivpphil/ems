﻿using ersEms.Domain.Schedule.Commands;
using ersEms.Domain.Schedule.Mappables;
using ersEms.Models.Schedule;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ersEms.mvc;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;

namespace ersEms.Controllers
{
    [ValidateInput(false)]
    public class scheduleController :ErsControllerSecure
    {
        [ErsAuthorization]
        public ActionResult schedule(schedule sched,EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IEmployeeScheduleCommand>(sched), sched);
            if (!ModelState.IsValid)
            {
                return View("schedule", sched);
            }

            if (sched.selectedField != 0)
            {
                mapperBus.Map<IScheduleSearchMappable>(sched);
            }
            else
            {
                mapperBus.Map<IScheduleMemberMappable>(sched);
            }

            return View("schedule",sched);
        }
        [ErsAuthorization]
        public ActionResult schedule_search(schedule sched, EnumEck? eck)
        {
            ModelState.AddModelErrors(commandBus.Validate<IEmployeeScheduleCommand>(sched), sched);
            if (!ModelState.IsValid)
            {
                return View("schedule", sched);
            }

            return this.schedule(sched, EnumEck.Normal);            
        }


        public ActionResult upload_emp_schedule(upload_emp_schedule upload_emp_schedule)
        {
            //string IPAddress = "";
            //IPHostEntry Host = default(IPHostEntry);
            //string Hostname = null;
            //Hostname = System.Environment.MachineName;
            //Host = Dns.GetHostEntry(Hostname);
            //foreach (IPAddress IP in Host.AddressList)
            //{
            //    if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            //    {
            //        IPAddress = Convert.ToString(IP);
            //    }
            //}


            var isRunning = Process.GetProcessesByName("UploadEmpSchedule");
            if (isRunning.Count() == 0)
            {
                throw new ErsException(ErsResources.GetMessage("unauthorized_access"));
            }
            return View("upload_emp_schedule", upload_emp_schedule);

        }

        public ActionResult upload_emp_schedule_complete(upload_emp_schedule upload_emp_schedule)
        {
            ModelState.AddModelErrors(commandBus.Validate<IUploadEmpScheduleCommand>(upload_emp_schedule), upload_emp_schedule);
            if (!ModelState.IsValid)
            {
                if (upload_emp_schedule.GetAllErrorMessageList().Count > 0)
                {
                    upload_emp_schedule.hasError = true;
                    upload_emp_schedule.error_messages = string.Join("<br/>", upload_emp_schedule.GetAllErrorMessageList());
                }
            }
            commandBus.Submit((IUploadEmpScheduleCommand)upload_emp_schedule, EnumCommandTransaction.BeginTransaction);

            return View("upload_emp_schedule_complete", upload_emp_schedule);            
        }               
    }
}