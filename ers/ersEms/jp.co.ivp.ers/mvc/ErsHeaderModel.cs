﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jp.co.ivp.ersEms.mvc
{
    public class ErsHeaderModel
        : ErsModelBase
    {
        /// <summary>
        /// メニュー名
        /// </summary>
        public string menu_name { get; protected set; }

        /// <summary>
        /// ログインユーザ名
        /// </summary>
        public string login_user_name { get; protected set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsHeaderModel(string menu_name)
        {
            this.menu_name = menu_name;

            this.Init();
        }

        /// <summary>
        /// Init
        /// </summary>
        protected void Init()
        {
            string user_cd = ErsContext.sessionState.Get("news_user_cd");

            if (!string.IsNullOrEmpty(user_cd))
            {
                // 管理者情報取得
                var objNewsAdmin = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithUserCode(user_cd);

                if (objNewsAdmin != null)
                {
                    this.login_user_name = objNewsAdmin.user_name;
                }
            }
        }
    }
}