﻿using jp.co.ivp.ers.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.jp.co.ivp.ers.mvc
{
    public class OtherCookie
    {
        /// <summary>
        /// クッキー書き込み
        /// </summary>
        /// <param name="cookie_name">クッキー名</param>
        /// <param name="cookie_value">設定値</param>
        /// <param name="minutes">保持期限（分）</param>
        public void SetCookieEmail(string cookie_name, string cookie_value, int? minutes)
        {
            var enc = new ErsEncryption();

            if (cookie_value != null && cookie_value != "")
            {
                if (cookie_value.Length > 250)
                {
                    return;
                }
                cookie_value = enc.HexEncode(cookie_value);
            }
            else
            {
                cookie_value = "";
            }
            //★不正文字チェック
            HttpCookie myCookie = new HttpCookie(cookie_name);
            DateTime now = DateTime.Now;

            // Set the cookie value.
            myCookie.Value = cookie_value;
            // Set the cookie expiration date.
            if (minutes.HasValue)
            {
                myCookie.Expires = now.AddMinutes(minutes.Value);
            }
            else
            {
                myCookie.Expires = now.AddYears(10);
            }

            // Add the cookie.
            var Rsp = HttpContext.Current.Response;
            Rsp.Cookies.Add(myCookie);
        }

        /// <summary>
        /// クッキー読み込み
        /// </summary>
        /// <returns></returns>
        public string GetCoookieEmail(string cookie_name)
        {
            var Req = HttpContext.Current.Request;
            var enc = new ErsEncryption();
            string cookie_value = "";
            if (Req.Cookies[cookie_name] != null)
            {
                if (Req.Cookies[cookie_name].Value.Length > 500)
                {
                    return null;
                }
                //★不正文字チェック
                cookie_value = HttpUtility.HtmlEncode(Req.Cookies[cookie_name].Value);
            }
            if (cookie_value != null && cookie_value != "")
            {
                cookie_value = enc.HexDecode(cookie_value);
            }
            return cookie_value;
        }

        /// <summary>
        /// クッキー削除
        /// </summary>
        /// <param name="cookie_name"></param>
        public void DeleteCookie(string cookie_name)
        {
            HttpCookie myCookie = new HttpCookie(cookie_name);
            DateTime now = DateTime.Now;

            // Set the cookie value.
            myCookie.Value = "";
            // Set the cookie expiration date.
            myCookie.Expires = now.AddMinutes(-1);

            // Add the cookie.
            HttpContext.Current.Response.Cookies.Add(myCookie);
        }
    }
}