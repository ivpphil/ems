﻿using System;
using System.Web.Mvc;
using jp.co.ivp.ers.administrator.function_group;
using ersEms.jp.co.ivp.ers.mvc;
using System.Linq;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class RoleCheckAttribute
        :FilterAttribute, IAuthorizationFilter
    {
        #region IAuthorizationFilter メンバー


        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var controller_name = filterContext.RouteData.Values["controller"].ToString();
            var method_name = filterContext.RouteData.Values["action"].ToString();

         

            var objc = new OtherCookie();
            var mcode =   objc.GetCoookieEmail("erssession1_encode_mcode");

            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            cri.emp_no = mcode;
            var find = repo.Find(cri);


            var member = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(mcode);
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //if (!string.IsNullOrEmpty(member.mcode))
            if (member.mcode.HasValue())
            {
                if (member.position == (EnumPosition)member.position)
                {
                    objc.SetCookieEmail("position", member.position.ToString(), setup.cookieTimer);
                    objc.SetCookieEmail("islogin", member.position.ToString(), setup.cookieTimer);
                }
                else
                {
                    throw new ErsException(ErsResources.GetMessage("You are unauthorize to access this page"));
                }
            }
        }

        #endregion
    }
}
