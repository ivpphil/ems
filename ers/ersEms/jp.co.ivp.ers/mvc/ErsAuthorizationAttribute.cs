﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using ersEms.jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ersEms.mvc
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class ErsAuthorizationAttribute
        : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var controler = (ErsControllerSecure)filterContext.Controller;

            object[] obj = filterContext.ActionDescriptor.GetCustomAttributes(typeof(NoNeedSessionAttribute), false);
            if (obj.Length == 0)
            {
                this.SessionCheck(filterContext);
            }
        }

        /// <summary>
        /// セッションチェックを行うか否か。
        /// </summary>
        public virtual void SessionCheck(AuthorizationContext filterContext=null)
        {
            var controller = (ErsControllerSecure)filterContext.Controller;

            //Include language processing to view in error page.
            //ErsContext.SessionCheck(filterContext);
            EnumUserState state = ((ISession)ErsContext.sessionState).getUserState();

            if (state != EnumUserState.LOGIN)
            {
                if (filterContext != null)
                {
                    filterContext.Controller.ViewData.ModelState.Clear();
                }
                var objc = new OtherCookie();
                objc.DeleteCookie("islogin");
                //ログイン状態をチェックし、ログイン状態でなかったらエラー
                filterContext.Result = controller.ExecuteError();
            }

        }
    }
}