﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;

namespace jp.co.ivp.ersEms.mvc
{
    public class ErsSiteRegisterModelBase : ErsModelBase
    {
        #region サイトID登録用 [Site ID for register]

        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        [DisplayName("site_t.id")]
        [ErsOutputHidden("input_form", "lpregist_detail", "lpregist_questionnaire")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, isArray = true)]
        public virtual object site_id { get; set; }

        /// <summary>
        /// 配列型サイトID [Array of site ID]
        /// </summary>
        protected virtual int[] site_id_array
        {
            get
            {
                if (this.site_id == null || this.site_id == "")
                {
                    return null;
                }
                else if (this.site_id.GetType() == typeof(int[]))
                {
                    return (int[])this.site_id;
                }
                else
                {
                    var array = this.site_id.ToString().Split(',').Select(e => Convert.ToInt32(e)).ToArray();

                    if (array.Length > 1)
                    {
                        this.site_id = array;
                    }

                    return array;
                }
            }
        }

        /// <summary>
        /// 非配列型サイトID [Not array of site ID]
        /// </summary>
        protected virtual int? site_id_not_array
        {
            get
            {
                if (this.site_id != null && this.site_id.GetType() == typeof(int))
                {
                    return Convert.ToInt32(this.site_id);
                }
                return null;
            }
        }

        /// <summary>
        /// モールショップ区分 [Mall shop division]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn { get { return null; } }


        public virtual bool multiple_sites
        {
            get
            {
                return ErsFactory.ersUtilityFactory.getSetup().Multiple_sites;
            }
        }

        public virtual int config_site_id
        {
            get
            {
                return (int)EnumSiteId.COMMON_SITE_ID;
            }
        }

        #endregion
    }
}