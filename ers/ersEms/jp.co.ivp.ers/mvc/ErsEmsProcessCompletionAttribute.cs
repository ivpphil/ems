﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ersEms.mvc
{
    public class ErsEmsProcessCompletionAttribute
        : ErsProcessCompletionAttributeBase
    {
        public ErsEmsProcessCompletionAttribute(string completedPageKeys)
            : base(completedPageKeys)
        {
        }

        public override void OnCheckCompletionError()
        {
            throw new ErsException("10042");
        }
    }
}