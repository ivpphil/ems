﻿using System;
using System.Linq;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;

namespace jp.co.ivp.ersEms.mvc
{
    public class ErsLanguageMenuModel
        : ErsModelBase
    {
        public IList<Dictionary<string, object>> langList { get; protected set; }
        public string current_page { get; protected set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public ErsLanguageMenuModel(string action, string controller)
        {
            current_page = "top/" + controller + "/asp/" + action + ".asp";
            this.langList = this.SetMenuList();
        }

        public ErsLanguageMenuModel()
        {
            
        }

        [ErsSchemaValidation("language_t.culture")]
        public string lang
        {
            get
            {
               return  ErsContext.sessionState.Get("culture");
            }
            set
            {
               ErsContext.sessionState.Add("culture", value); 
            }
        }

        /// <summary>
        /// Set lis of menu
        /// </summary>
        private IList<Dictionary<string, object>> SetMenuList()
        {
            var criteria = ErsFactory.ersLanguageFactory.GetErsLanguageCriteria();
            var repository = ErsFactory.ersLanguageFactory.GetErsLanguageRepository();
            criteria.active = EnumActive.Active;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            var lstRet = new List<Dictionary<string, object>>();

            var listFind = repository.Find(criteria);

            foreach (var data in listFind)
            {
                lstRet.Add(data.GetPropertiesAsDictionary());
            }

            return lstRet;
        }

       
    }
}