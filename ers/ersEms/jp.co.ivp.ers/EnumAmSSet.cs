﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersAdmin.jp.co.ivp.ers
{
    public enum EnumAmSSet
    {
        /// <summary>
        /// 0: そのまま送信
        /// </summary>
        Immediately = 0,

        /// <summary>
        /// 1: 日時を設定して送信 
        /// </summary>
        Later
    }
}