﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(ersEms.Startup))]
namespace ersEms
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        { 
            app.MapSignalR();
        }
    }
}
