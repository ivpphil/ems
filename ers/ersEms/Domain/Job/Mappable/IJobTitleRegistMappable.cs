﻿using ersEms.Models.Job;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System.Collections.Generic;

namespace ersEms.Domain.Job.Mappables
{
    public interface IJobTitleRegistMappable : IMappable
    {
        IList<job_details> job_details { get; set; }

        bool isRegist { get; set; }

        bool isInitialized { get; set; }

        bool noRecord_flg { get; set; }
    }
}
