﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersEms.Domain.Job.Mappables
{
    public interface IJobTitleRegistRecordMappable : IMappable
    {
        int? id { get; set; }
        string job_title { get; set; }
        string job_description { get; set; }
        int? disp_order { get; set; }
        bool deleteFlg { get; set; }
    }
}
