﻿using ersEms.Domain.Job.Mappables;
using ersEms.Models.Job;
using jp.co.ivp.ers;
using jp.co.ivp.ers.job;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System.Collections.Generic;
using static jp.co.ivp.ers.db.Criteria;

namespace ersEms.Domain.Job.Mappers
{
    public class JobTitleRegistMapper : IMapper<IJobTitleRegistMappable>
    {
        public void Map(IJobTitleRegistMappable objMappable)
        {
            if (!objMappable.isInitialized)
            {
                var repo = ErsFactory.ersJobFactory.GetErsJobTitleRepository();
                var cri = ErsFactory.ersJobFactory.GetErsJobTitleCriteria();
                cri.SetOrderByDispOrder(OrderBy.ORDER_BY_ASC);

                var list = repo.Find(cri);
                if (list.Count == 0)
                {
                    objMappable.noRecord_flg = true;
                }
                else
                {
                    objMappable.job_details = getJobList(objMappable);
                }
            }         
        }

        public List<job_details> getJobList(IJobTitleRegistMappable objMappable)
        {
            var jobList = new List<job_details>();

            var repo = ErsFactory.ersJobFactory.GetErsJobTitleRepository();
            var crit = ErsFactory.ersJobFactory.GetErsJobTitleCriteria();

            var list = repo.Find(crit);

            foreach (var item in list)
            {
                var job_detail = new job_details();
                job_detail.OverwriteWithParameter(item.GetPropertiesAsDictionary());

                objMappable.controller.mapperBus.Map<IJobTitleRegistRecordMappable>(job_detail);
                jobList.Add(job_detail);
            }

            return jobList;
        }
    }
}