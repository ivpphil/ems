﻿using ersEms.Domain.Job.Mappables;
using ersEms.Models.Job;
using jp.co.ivp.ers;
using jp.co.ivp.ers.job;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;

namespace ersEms.Domain.Job.Mappers
{
    public class JobTitleRegistRecordMapper : IMapper<IJobTitleRegistRecordMappable>
    {
        public void Map(IJobTitleRegistRecordMappable objMappable)
        {
            if (objMappable.id.HasValue)
            {
                var job = ErsFactory.ersJobFactory.GetErsJobTitleWithID(Convert.ToInt32(objMappable.id));
                if (job != null)
                {
                    objMappable.job_title = job.job_title;
                    objMappable.job_description = job.job_description;
                    objMappable.disp_order = job.disp_order;

                }
            }       
        }
    }
}