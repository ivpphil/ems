﻿using ersEms.Models.Job;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;

namespace ersEms.Domain.Job.Commands
{
    public interface IJobTitleRegistRecordCommand : ICommand
    {       
        int? id { get; set; }   
        string job_title { get; set; }     
        string job_description { get; set; }     
        int? disp_order { get; set; }
        bool deleteFlg { get; set; }

    }
}
