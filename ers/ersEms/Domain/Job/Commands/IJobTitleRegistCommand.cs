﻿using ersEms.Models.Job;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;

namespace ersEms.Domain.Job.Commands
{
    public interface IJobTitleRegistCommand : ICommand
    {
       IList<job_details> job_details { get; set; }

       bool isRegist { get; set; }
    }
}
