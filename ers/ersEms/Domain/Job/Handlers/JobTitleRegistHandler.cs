﻿using ersEms.Domain.Job.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;

namespace ersEms.Domain.Job.Handlers
{
    public class JobTitleRegistHandler : ICommandHandler<IJobTitleRegistCommand>
    {
        public ICommandResult Submit(IJobTitleRegistCommand command)
        {
            this.InsertUpdate(command);
            return new CommandResult(true);
        }

        public void InsertUpdate(IJobTitleRegistCommand command)
        {
            var repository = ErsFactory.ersJobFactory.GetErsJobTitleRepository();
            foreach (var details in command.job_details)
            {
                details.lineNumber = command.job_details.IndexOf(details) + 1;

                var entity = ErsFactory.ersJobFactory.GetErsJobTitle();

                if (!details.id.HasValue)
                {
                    entity.OverwriteWithParameter(details.GetPropertiesAsDictionary());
                    entity.disp_order = details.lineNumber;
                    repository.Insert(entity, true);
                }
                else if (details.deleteFlg)
                {
                    var crit = ErsFactory.ersJobFactory.GetErsJobTitleCriteria();
                    crit.id = details.id;
                    repository.Delete(crit);

                    this.UpdateEmployee(command, details.id);

                }
                else
                {
                    var oldObj = ErsFactory.ersJobFactory.GetErsJobTitleWithID(Convert.ToInt32(details.id));
                    var newObj = ErsFactory.ersJobFactory.GetErsJobTitle();

                    newObj.OverwriteWithParameter(oldObj.GetPropertiesAsDictionary());
                    newObj.job_title = details.job_title;
                    newObj.job_description = details.job_description;
                    newObj.disp_order = details.lineNumber;
                    newObj.utime = DateTime.Now;
                    repository.Update(oldObj, newObj);
                }
            }
        }

        public void UpdateEmployee(IJobTitleRegistCommand command, int? id)
        {
            var repository = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var crit = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
            crit.job_title = id;
            var list = repository.Find(crit);


            foreach (var employee in list)
            {
                var oldObj = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(employee.emp_no);
                var newObj = ErsFactory.ersEmployeeFactory.GetErsEmployee();
                var setup = ErsFactory.ersUtilityFactory.getSetup();

                newObj.OverwriteWithParameter(oldObj.GetPropertiesAsDictionary());
                newObj.job_title = setup.job_title_default;
                newObj.utime = DateTime.Now;
                repository.Update(oldObj, newObj);
            }      
        }
    }
}