﻿using ersEms.Domain.Job.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Job.Handlers
{
    public class ValidateJobTitleRegistRecord : IValidationHandler<IJobTitleRegistRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IJobTitleRegistRecordCommand command)
        {
            yield return command.CheckRequired("job_title");
            yield return command.CheckRequired("job_description");

        }
    }
}