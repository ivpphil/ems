﻿using ersEms.Domain.Job.Commands;
using ersEms.Domain.Project.Commands;
using ersEms.Models.Job;
using ersEms.Models.Project;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Job.Handlers
{
    public class ValidateJobTitleRegist : IValidationHandler<IJobTitleRegistCommand>
    {

        public IEnumerable<ValidationResult> Validate(IJobTitleRegistCommand command)
        {
            if (command.job_details.Count > 0)
            {
                var list = new List<string>();

                foreach (job_details job_details in command.job_details)
                {
                    job_details.lineNumber = command.job_details.IndexOf(job_details) + 1;

                    job_details.AddInvalidField(command.controller.commandBus.Validate<IJobTitleRegistRecordCommand>(job_details));

                    if (job_details.job_title.HasValue())
                    {
                        if (list.Contains(job_details.job_title))
                        {
                            job_details.AddInvalidField(new ValidationResult(ErsResources.GetMessage("10103a", ErsResources.GetFieldName("job_title"), job_details.job_title), new[] { "job_title" }));
                        }
                        else
                        {
                            list.Add(job_details.job_title);
                        }
                    }
                    if (!job_details.IsValid)
                    {
                        foreach (var errorMessage in job_details.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "job_details" });
                        }
                    }
                }
            }
            else
            {
                yield return new ValidationResult(ErsResources.GetMessage("10000", ErsResources.GetFieldName("job_title")));
                yield return new ValidationResult(ErsResources.GetMessage("10000", ErsResources.GetFieldName("job_description")));

            }
        }
    }
}