﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using System.Collections.Generic;

namespace ersEms.Domain.Employee.Mappables
{
    public interface IEmpSearchTempMappable : IMappable
    {
        string s_keyword { get; set; }

        EnumEmpSearch? emp_search_by { get; set; }

        EnumTeam? team_search { get; set; }

        int? job_title_search { get; set; }

        IList<ErsEmployee> emp_list { get; set; }

        bool hasNoRecord { get; set; }
    }
}
