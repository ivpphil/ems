﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersEms.Domain.Employee.Mappables
{
    public interface IEmpProfileMappable : IMappable
    {

        bool hasImageFile { get; set; }

        ErsEmployee employee { get; set; }

        bool hasImage { get; set; }

        string profile_emp_no { get; set; }

        EnumPosition? position { get; set; }
    }
}