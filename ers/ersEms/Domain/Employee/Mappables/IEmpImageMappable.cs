﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using System.Web;

namespace ersEms.Domain.Employee.Mappables
{
    public interface IEmpImageMappable
        :IMappable
    {
        HttpPostedFileBase imgfile { get; set; }
        string temp_file_name { get; set; }
        string emp_no { get; set; }
    }
}