﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using System.Collections.Generic;

namespace ersEms.Domain.Employee.Mappables
{
    public interface ITeamManagementMappable
        :IMappable, IEmpSearchMappable
    {
        ErsPagerModel non_team_pager { get; set; }

        long non_team_recordCount { get; set; }

        List<Dictionary<string, object>> non_team_list { get; set; }
    }
}