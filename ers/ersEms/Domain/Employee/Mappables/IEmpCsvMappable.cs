﻿using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.util;
using System.Collections.Generic;

namespace ersEms.Domain.Employee.Mappables
{
    public interface IEmpCsvMappable: IEmpSearchMappable
    {
        ErsCsvCreater csvCreater { get; set; }

        string[] emp_no_arr { get; set; }

        string emp_no_string { get; set; }

    }
}
