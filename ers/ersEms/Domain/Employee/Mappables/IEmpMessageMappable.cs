﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using System.Collections.Generic;

namespace ersEms.Domain.Employee.Mappables
{
    public interface IEmpMessageMappable
        : IMappable
    {
        List<Dictionary<string,object>> threads { get; set; }

        List<Dictionary<string, object>> messages { get; set; }

        bool fromEmpList { get; set; }
        
        bool write_new { get; set; }

        string emp_no { get; }

        int? thread_no { get; set; }

        string recipient_emp_no { get; set; }

        string recipient_name { get;}

        string subject { get; set; }
    }
}