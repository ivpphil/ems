﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using System;
using System.Collections.Generic;

namespace ersEms.Domain.Employee.Mappables
{
    public  interface IEmpListMappable:IMappable
    {
        ErsPagerModel pager { get; set; }
        
        long recordCount { get; set; }
        
        long maxItemCount { get; }

        int pageCnt { get; set; }

        string emp_no { get; set; }

        string fname { get; set; }

        string lname { get; set; }

        DateTime? birthday { get; set; }

        string address { get; set; }

        string contact_no { get; set; }

        EnumPosition? position { get; set; }

        EnumTeam? team { get; set; }

        EnumEmpStatus? status { get; set; }

        string password { get; set; }

        string password_confirm { get; set; }

        string email { get; set; }

        string email_confirm { get; set; }

        List<Dictionary<string, object>> list { get; set; }

        int? gender { get; set; }

        string w_team { get;}

        string image_file { get; set; }

        bool img_file_exist { get; set; }

        bool ImgInDB { get; set; }
    }
}
