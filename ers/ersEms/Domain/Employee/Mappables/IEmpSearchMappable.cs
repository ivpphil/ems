﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using System.Collections.Generic;

namespace ersEms.Domain.Employee.Mappables
{
    public interface IEmpSearchMappable:IMappable
    {
        ErsPagerModel pager { get; set; }
        
        long recordCount { get; set; }
        
        long maxItemCount { get; }

        int pageCnt { get; set; }
        
        string s_emp_no { get; set; }

        string s_fname { get; set; }

        string s_lname { get; set; }

        string s_email { get; set; }

        string s_team { get; set; }

        string s_position { get; set; }

        string s_keyword { get;}

        EnumEmpSearch? searchField { get; set; }

        EnumTeam? SearchTeam { get; set; }

        EnumPosition? SearchPos { get; set; }

        List<Dictionary<string, object>> list { get; set; }

        EnumPosition? emp_pos { get; set; }

        bool fromEmpList { get; set; }

        bool MemberSearch { get; set; }

        string user_emp_no { get; set; }
    }
}
