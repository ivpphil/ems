﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using System.Collections.Generic;

namespace ersEms.Domain.Employee.Mappables
{
    public interface IEmployeeProjectMappable:IMappable
    {
        List<Dictionary<string,object>> projectList { get; set; }

        string emp_no { get; set; }

        int? projectCount { get; set; }
    }
}