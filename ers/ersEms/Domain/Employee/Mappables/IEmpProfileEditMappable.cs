﻿using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersEms.Domain.Employee.Mappables
{
    public interface IEmpProfileEditMappable : IMappable
    {
        ErsEmployee old_records { get; }

        string profile_emp_no { get; set; }

        string temp_file_name { get; set; }

        string image_file { get; set; }

        bool hasImage { get; set; }

        bool hasTempFile { get; set; }

        string temp_path { get; set; }

        string stored_img_path { get; set; }
    }
}