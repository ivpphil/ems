﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace ersEms.Domain.Employee.Handlers
{
    public class ValidateEmpProfileEdit : IValidationHandler<IEmpProfileEditCommand>
    {
        public IEnumerable<ValidationResult> Validate(IEmpProfileEditCommand command)
        {
            if (command.img_file != null && command.img_file.FileName.HasValue())
            {
                SaveTemp(command);
            }
            
            if (command.email.HasValue() && command.email_confirm.HasValue())
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckEmailConfirm(command.email, command.email_confirm);
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckOtherEmail(command.profile_emp_no, command.email);
            }
            
            if (command.password.HasValue() || command.password_confirm.HasValue())
            {
                if (command.password.HasValue())
                {
                    yield return command.CheckRequired("password_confirm");
                }
                if (command.password_confirm.HasValue())
                {
                    yield return command.CheckRequired("password");
                }
            }

            yield return command.CheckRequired("current_password");
            if (command.password.HasValue() && command.password_confirm.HasValue())
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckPasswordConfirm(command.email, command.email_confirm);
            }

            if (command.current_password.HasValue())
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckCorrectPass(command.profile_emp_no, command.current_password);
            }

            yield return command.CheckRequired("address");
            
            if (command.birthday != null)
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckValidBirtday((DateTime)command.birthday);
               
            }
            
            yield return command.CheckRequired("contact_no");
        }

        private void SaveTemp(IEmpProfileEditCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();
            var temp_path = setup.image_temp_directory + command.profile_emp_no + "\\";

            //delete old temp file
            uploadedFileHelper.DeleteTempFile(temp_path);

            //save new temp file
            if (!File.Exists(temp_path + command.img_file))
            {
                if (command.img_file.ContentLength > 0)
                {
                    command.temp_file_name = uploadedFileHelper.GetTempFileName(command.img_file);
                    uploadedFileHelper.SaveTempFile(command.img_file, temp_path, command.temp_file_name);
                }
            }

        }
    }
}