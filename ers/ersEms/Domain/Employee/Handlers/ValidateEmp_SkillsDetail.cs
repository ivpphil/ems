﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Employee.Handlers
{
    public class ValidateEmp_SkillsDetail:IValidationHandler<IEmp_SkillsdetailCommand>

    {

        public IEnumerable<ValidationResult> Validate(IEmp_SkillsdetailCommand command)
        {
            yield return command.CheckRequired("emp_no");
            yield return command.CheckRequired("skill_desc");
            yield return command.CheckRequired("years_exp");


        }
    }
}