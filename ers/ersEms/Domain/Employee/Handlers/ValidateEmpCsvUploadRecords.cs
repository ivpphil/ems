﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Employee.Handlers
{
    public class ValidateEmpCsvUploadRecords : IValidationHandler<IEmpCsvUploadRecordsCommand>
    {
        public IEnumerable<ValidationResult> Validate(IEmpCsvUploadRecordsCommand command)
        {
            yield return command.CheckRequired("emp_no");
            yield return command.CheckRequired("desknet_id");
            yield return command.CheckRequired("fname");
            yield return command.CheckRequired("lname");
            yield return command.CheckRequired("email");
            yield return command.CheckRequired("w_team");
            yield return command.CheckRequired("date_hired");
            yield return command.CheckRequired("total_vl");
            yield return command.CheckRequired("total_sl");
            yield return command.CheckRequired("sss_no");
            yield return command.CheckRequired("tin_no");
            yield return command.CheckRequired("w_job_title");

            if (command.emp_no.HasValue())
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().check_empNoFormat(command.emp_no);
            }

            if (command.w_team.HasValue())
            {
                if (!ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().ExistName(EnumCommonNameType.Team, command.team_convert))
                {
                    yield return new ValidationResult(ErsResources.GetMessage("team_invalid"), new[] { "team" });
                }
            }

            if (command.w_job_title.HasValue())
            {
                ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckJobTitleExisting(command.w_job_title);
            }

            if (command.IsValidField(nameof(command.total_vl)) && command.IsValidField(nameof(command.total_sl)))
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                if (command.total_vl > setup.max_vl_sl)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("max_vl_sl", command.total_vl, ErsResources.GetFieldName("total_vl")), new[] { "total_vl" });
                }

                if (command.total_sl > setup.max_vl_sl)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("max_vl_sl", command.total_sl, ErsResources.GetFieldName("total_sl")), new[] { "total_sl" });
                }
            }
        }
    }
}