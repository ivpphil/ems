﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersEms.Domain.Employee.Handlers
{
    public class ValidateAddToTeam
        : IValidationHandler<IAddToTeamCommand>
    {
        public IEnumerable<ValidationResult> Validate(IAddToTeamCommand command)
        {
            if (command.update_emp.HasValue())
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckEmpLeader(command.update_emp);
                var employee = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(command.update_emp);

                if (employee == null)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("CheckEmpNoErrorMessage2", command.update_emp));
                }
                else
                {
                    if ((employee.team_leader.HasValue()) || (employee.position != EnumPosition.Member) || (employee.emp_no == ErsContext.sessionState.Get("mcode")))
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("add_team_member_err"));
                    }
                }
            }
            else
            {
                yield return new ValidationResult(ErsResources.GetMessage("CheckEmpNoErrorMessage1"));
            }
        }
    }
}