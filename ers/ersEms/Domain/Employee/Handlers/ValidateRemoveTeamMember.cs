﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersEms.Domain.Employee.Handlers
{
    public class ValidateRemoveTeamMember
        : IValidationHandler<IRemoveTeamMemberCommand>
    {
        public IEnumerable<ValidationResult> Validate(IRemoveTeamMemberCommand command)
        {
            if (command.update_emp.HasValue())
            {                
                var employee = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(command.update_emp);
                var team_leader = ErsContext.sessionState.Get("mcode");

                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckRemoveEmpLeader(command.update_emp, team_leader);

                if (employee == null || employee.team_leader != team_leader)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("CheckEmpNoErrorMessage2", command.update_emp));
                }
            }
            else
            {
                yield return new ValidationResult(ErsResources.GetMessage("CheckEmpNoErrorMessage1"));
            }
        }
    }
}