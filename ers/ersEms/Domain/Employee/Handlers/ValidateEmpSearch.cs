﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ersEms.Domain.Employee.Handlers
{
    public class ValidateEmpSearch:IValidationHandler<IEmpSearchCommand>
    {
        public IEnumerable<ValidationResult> Validate(IEmpSearchCommand command)
        {
            if (command.searchField.HasValue)
            {
                var enumVal = Enum.GetNames(typeof(EnumEmpSearch)) ;
                bool isExist = enumVal.ToList().Contains(Convert.ToString(command.searchField));
                if (!isExist)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("invalid_filter"));
                }
            }

            if (command.SearchTeam.HasValue)
            {
                var enumVal = Enum.GetNames(typeof(EnumTeam));
                bool isExist = enumVal.ToList().Contains(Convert.ToString(command.SearchTeam));
                if (!isExist)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("invalid_filter"));
                }
            }

            if (command.SearchPos.HasValue)
            {
                var enumVal = Enum.GetNames(typeof(EnumPosition));
                bool isExist = enumVal.ToList().Contains(Convert.ToString(command.SearchPos));
                if (!isExist)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("invalid_filter"));
                }
            }

            if (!String.IsNullOrEmpty(command.s_emp_no))
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().check_empNoFormat(command.s_emp_no);
            }
           
            if(command.s_position.HasValue())
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckPositionIfExist(command.s_position);
            }

            if (command.s_team.HasValue())
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckTeamIfExist(command.s_team);
            }

        }
    }
}