﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Employee.Handlers
{
    public class Emp_SkillsHandler:ICommandHandler<IEmp_SkillsCommand>
    {
        public ICommandResult Submit(IEmp_SkillsCommand command)
        {
            var entity = ErsFactory.ersEmployeeFactory.getErsEmpSkills();
            int count = command.Emp_skills_detail.Count();

            for(int i=0;i<count;i++)
            {
                command.Emp_skills_detail[i].emp_no = ErsContext.sessionState.Get("mcode");
                entity.OverwriteWithParameter(command.Emp_skills_detail[i].GetPropertiesAsDictionary());

                ErsFactory.ersEmployeeFactory.getErsEmpSkillsRepository().Insert(entity, true);
            }

            return new CommandResult(true);
        }

    }
}