﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.IO;

namespace ersEms.Domain.Employee.Handlers
{
    public class EmpProfileEditHandler : ICommandHandler<IEmpProfileEditCommand>
    {
        public ICommandResult Submit(IEmpProfileEditCommand command)
        {
            SaveImage(command);

            if (command.position == EnumPosition.Administrator)
            {
                UpdateMemberDetails(command);
            }
            else
            {
                InsertMemberCorrection(command);
            }

            return new CommandResult(true);
        }
        
        private void UpdateMemberDetails(IEmpProfileEditCommand command)
        {
            var repository = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();

            var new_records = ErsFactory.ersEmployeeFactory.getErsEmployeeWithParameter(command.old_records.GetPropertiesAsDictionary());

            new_records.OverwriteWithModel(command);

            if (!command.password.HasValue())
            {
                new_records.password = command.old_records.password;
            }
            else
            {
                command.passChanged = true;
            }

            repository.Update(command.old_records, new_records);
        }

        private void InsertMemberCorrection(IEmpProfileEditCommand command)
        {
            //change password
            var repository = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();

            var new_records = ErsFactory.ersEmployeeFactory.getErsEmployeeWithParameter(command.old_records.GetPropertiesAsDictionary());
            if (!command.password.HasValue())
            {
                new_records.password = command.old_records.password;
            }

            //check if has changes
            repository.Update(command.old_records, new_records);

            getReasonUpdateStr(command);

            //insert details to employee_correction_t
            var correctionRepository = ErsFactory.ersEmployeeFactory.GetErsEmployeeCorrectionRepository();

            var correction = ErsFactory.ersEmployeeFactory.getErsEmployeeCorrectionWithParameter(command.GetPropertiesAsDictionary());

            correctionRepository.InsertEmployeeCorrectionRequest(correction,command.reasonStr,command.employee_correction_list, true);
        }

        private void SaveImage(IEmpProfileEditCommand command)
        {
            var uploadHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();

            if (command.temp_file_name.HasValue())
            {
                string old_image = command.image_file;

                command.imageChanged = true;

                if (!Directory.Exists(command.stored_img_path))
                {
                    Directory.CreateDirectory(command.stored_img_path);
                }

                var oldImgFile = command.stored_img_path + old_image;
                command.image_file = command.profile_emp_no + ".png";
                if (File.Exists(oldImgFile))
                {
                    uploadHelper.DeleteOldImageFile(oldImgFile);
                }

                uploadHelper.SaveResizedImage(command.temp_path, command.temp_file_name, command.stored_img_path, command.image_file, 300);

            }

            if (command.temp_path.HasValue())
            {
                uploadHelper.DeleteTempFile(command.temp_path);
            }

        }

        private void getReasonUpdateStr(IEmpProfileEditCommand command)
        {
            //changes of records
            string reason = ErsResources.GetMessage("EditReason");
            var listStr = new List<string>();
            command.employee_correction_list = new List<string>();
            var new_records = ErsFactory.ersEmployeeFactory.getErsEmployeeWithParameter(command.old_records.GetPropertiesAsDictionary());
            new_records.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            foreach (var property in command.old_records.GetType().GetProperties())
            {
                if (property.Name.Equals("password") || property.Name.Equals("image_file") ||property.Name.Contains("w_"))
                {
                    continue;
                }
                var old_prop = Convert.ToString(property.GetValue(command.old_records)).Trim();
                var new_prop = Convert.ToString(new_records.GetType().GetProperty(property.Name).GetValue(new_records)).Trim();

                this.SetPropertyData(ref new_prop, ref old_prop, property.Name);

                if (old_prop != new_prop)
                {
                 
                    listStr.Add(String.Concat(ErsResources.GetFieldName(property.Name), ": ", old_prop, " ~ ", new_prop));
                    command.employee_correction_list.Add(property.Name);
                }
            }

            if (listStr.Count > 0)
            {
                command.reasonStr = string.Concat(reason, System.Environment.NewLine, string.Join(System.Environment.NewLine, listStr));

            }
            else
            {
                command.reasonStr = null;
            }
        }

        private void SetPropertyData(ref string new_prop, ref string old_prop, string propName)
        {
            
            const string notSet = "Not Set";

            if (propName.Equals("birthday"))
            {
                new_prop = new_prop.HasValue() ? Convert.ToDateTime(new_prop).ToShortDateString() : notSet;
                old_prop = old_prop.HasValue() ? Convert.ToDateTime(old_prop).ToShortDateString() : notSet;
            }
            if (propName.Equals("gender"))
            {
                new_prop = new_prop.HasValue() ? Convert.ToString((EnumSex?)Convert.ToInt32(new_prop)) : notSet;
                old_prop = old_prop.HasValue() ? Convert.ToString((EnumSex?)Convert.ToInt32(old_prop)) : notSet;
            }
            if (!new_prop.HasValue())
            {
                new_prop = notSet;
            }

            if (!old_prop.HasValue())
            {
                old_prop = notSet;
            }
        }
    }
}