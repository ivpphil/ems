﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersEms.Domain.Employee.Handlers
{
    public class ValidateEmpMessage
        : IValidationHandler<IEmpMessageCommand>
    {
        public IEnumerable<ValidationResult> Validate(IEmpMessageCommand command)
        {
            if (command.send)
            {
                yield return command.CheckRequired("recipient_emp_no");

                if (!command.thread_no.HasValue)
                {
                    yield return command.CheckRequired("subject");
                }

                yield return command.CheckRequired("message");
                
                if (command.thread_no.HasValue)
                {
                    yield return ErsFactory.ersEmployeeFactory.GetValidationMessageStgy().checkThreadExist(command.thread_no);

                    if (command.emp_no == command.recipient_emp_no)
                    {
                        throw new ErsException(ErsResources.GetMessage("recipient_invalid"));
                    }
                    else
                    {
                        if (command.recipient_emp_no.HasValue())
                        {
                            if (ErsFactory.ersEmployeeFactory.GetValidationMessageStgy().checkRecipientIfExist(command.recipient_emp_no))
                            {
                                yield return ErsFactory.ersEmployeeFactory.GetValidationMessageStgy().checkThreadPersonsInvolve(command.thread_no, command.emp_no, command.recipient_emp_no);
                            }
                            else
                            {
                                yield return new ValidationResult(ErsResources.GetMessage("recipient_invalid"));
                            }
                        }
                    }
                }
            }
            else if (command.delete)
            {
                if (command.delete_thread_array == null)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("no_thread_selected"));
                }
            }
        }
    }
}