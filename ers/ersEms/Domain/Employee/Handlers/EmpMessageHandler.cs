﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Linq;

namespace ersEms.Domain.Employee.Handlers
{
    public class EmpMessageHandler : ICommandHandler<IEmpMessageCommand>
    {
        public ICommandResult Submit(IEmpMessageCommand command)
        {
            if (command.send)
            {
                SendMessage(command);
            }

            if (command.delete)
            {
                DeleteMessage(command);
            }
            
            return new CommandResult(true);
        }

        private void SendMessage(IEmpMessageCommand command)
        {
            if (!command.thread_no.HasValue)
            {
                var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadRepository();
                var thread = ErsFactory.ersEmployeeFactory.GetErsEmployeeThread();

                if (command.recipient_emp_no.HasValue())
                {
                    thread.recipient_emp_no = command.recipient_emp_no;
                }

                if (command.emp_no.HasValue())
                {
                    thread.sender_emp_no = command.emp_no;
                }

                if (command.subject.HasValue())
                {
                    thread.subject = command.subject;
                }

                thread.intime = DateTime.Now;
                thread.sender_last_active = DateTime.Now;
                thread.recipient_last_active = DateTime.Now;

                repo.Insert(thread, true);

                command.thread_no = thread.id;
            }

            if (command.thread_no.HasValue)
            { 
                SaveMessageDetails(command);
            }

        }

        private void SaveMessageDetails(IEmpMessageCommand command)
        {
            if (command.thread_no.HasValue)
            {
                var details = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadDetails();
                var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadDetailsRepository();

                if (command.emp_no.HasValue())
                {
                    details.maker_emp_no = command.emp_no;
                }

                if (command.message.HasValue())
                {
                    details.message = command.message;
                }

                if (command.thread_no.HasValue)
                {
                    details.thread_no = (int)command.thread_no;
                }

                details.intime = DateTime.Now;
                details.seen_flg = EnumSeenFlg.unseen;

                repo.Insert(details, true);
            }

        }

        private void DeleteMessage(IEmpMessageCommand command)
        {
            if (command.delete_thread_array != null && command.delete_thread_array.Count() > 0)
            {
                foreach (var item in command.delete_thread_array)
                {
                    var old_thread = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadWithId(item);
                    var new_thread = ErsFactory.ersEmployeeFactory.GetErsEmployeeThread();
                    new_thread.OverwriteWithModel(old_thread);

                    if (command.emp_no == new_thread.recipient_emp_no)
                    {
                        new_thread.recipient_last_active = DateTime.Now;
                    }
                    else if (command.emp_no == new_thread.sender_emp_no)
                    {
                        new_thread.sender_last_active = DateTime.Now;
                    }

                    var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadRepository();

                    repo.Update(old_thread, new_thread);

                }
            }
        }
    }
}