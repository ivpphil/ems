﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Employee.Handlers
{
    public class AddToTeamHandler
        : ICommandHandler<IAddToTeamCommand>
    {
        public ICommandResult Submit(IAddToTeamCommand command)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();

            var new_member = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(command.update_emp);
            var old_member = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(command.update_emp);

            new_member.team_leader = ErsContext.sessionState.Get("mcode");

            repo.Update(old_member, new_member);

            return new CommandResult(true);
        }
    }
}