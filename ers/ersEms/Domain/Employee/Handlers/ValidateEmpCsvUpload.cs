﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ersEms.Domain.Employee.Handlers
{
    public class ValidateEmpCsvUpload : IValidationHandler<IEmpCsvUploadCommand>
    {
        public IEnumerable<ValidationResult> Validate(IEmpCsvUploadCommand command)
        {
            if (command.csv_file.csv_file == null)
            {
                yield return new ValidationResult(ErsResources.GetMessage("empty_csv_file"));
                yield break;
            }

            int lineNumber = 0;

            if(command.csv_file.GetValidatedModels(command.chk_find).Count() != 0 )
            {
                foreach (var model in command.csv_file.GetValidatedModels(command.chk_find))
                {
                    model.AddInvalidField(command.controller.commandBus.Validate<IEmpCsvUploadRecordsCommand>(model));
                    if (!model.IsValid)
                    {
                        if (lineNumber < model.lineNumber)
                        {
                            lineNumber = model.lineNumber;
                            foreach (var errorMessage in model.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "csv_file" });
                            }
                            command.csv_file.MarkRecordAsInvalid(model);
                        }

                        continue;
                    }

                    if (model.email.HasValue() && model.emp_no.HasValue())
                    {
                        var empNoExistOtherRecord = command.csv_file.GetValidatedModels(command.chk_find).Where(x => x.emp_no == model.emp_no && x.lineNumber != model.lineNumber);
                        var emailExistOtherRecord = command.csv_file.GetValidatedModels(command.chk_find).Where(x => x.email == model.email && x.lineNumber != model.lineNumber);
                        var desknetIDExistOtherRecord = command.csv_file.GetValidatedModels(command.chk_find).Where(x => x.desknet_id == model.desknet_id && x.lineNumber != model.lineNumber);
                        var empNoExistDb = ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckDuplicateEmpNoRegister(model.emp_no);
                        var emailExistDb = ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckDuplicateEmailRegister(model.email);
                        
                        if (empNoExistOtherRecord != null && empNoExistOtherRecord.Count() > 0)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("empno_exist_other_records", new[] { model.lineName, model.emp_no }));
                        }

                        if (emailExistOtherRecord != null && emailExistOtherRecord.Count() > 0)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("email_exist_other_records", new[] { model.lineName, model.email }));
                        }
                        
                        if (desknetIDExistOtherRecord != null && desknetIDExistOtherRecord.Count() > 0)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("desknet_id_exist_other_records", new[] { model.lineName, model.desknet_id.ToString() }));
                        }

                        if (emailExistDb && !empNoExistDb)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("email_registered_already", new[] { model.lineName, model.email }));
                        }
                        
                        if (model.desknet_id.HasValue && !empNoExistDb)
                        {
                            var desknetIDExistDb = ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckDuplicateDesknet_ID(Convert.ToInt16(model.desknet_id));
                            if (desknetIDExistDb)
                            {
                                yield return new ValidationResult(ErsResources.GetMessage("desknet_id_registered_already", new[] { model.lineName, model.desknet_id.ToString() }));
                            }
                        }
                    }
                }
            }

            if (command.csv_file.GetValidatedModels(command.chk_find).Count() == 0)
            {
                yield return new ValidationResult(ErsResources.GetMessage("no_record_upload"));
            }
        }
    }
}