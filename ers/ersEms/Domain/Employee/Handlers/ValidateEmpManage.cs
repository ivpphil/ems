﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Employee.Handlers
{
    public class ValidateEmpManage:IValidationHandler<IEmpManageCommand>
    {
        public IEnumerable<ValidationResult> Validate(IEmpManageCommand command)
        {
            if (command.team.HasValue)
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckEmpTeamExisting(command.team);
            }

            //if (command.position.HasValue)
            //{
            //    yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckEmpPositionExisting(command.position);
            //}


            if(command.job_title.HasValue)
            {
                var job = ErsFactory.ersJobFactory.GetErsJobTitleWithID(Convert.ToInt32(command.job_title));
                if (job == null)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("NotExist", ErsResources.GetFieldName("job_title"), command.job_title));

                }
            }

            if (command.status.HasValue)
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckEmpStatusExisting(command.status);
            }

            yield return command.CheckRequired("emp_no");
            yield return command.CheckRequired("job_title");
            yield return command.CheckRequired("status");
            yield return command.CheckRequired("team");

            if (command.emp_no.HasValue())
            {
                var employee = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(command.emp_no);
                if (employee == null)
                {
                    throw new ErsException(ErsResources.GetMessage("CheckEmpNoErrorMessage2",command.emp_no));
                }
            }
        }
    }
}