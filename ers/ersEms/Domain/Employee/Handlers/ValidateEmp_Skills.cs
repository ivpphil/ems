﻿using ersEms.Domain.Employee.Commands;
using ersEms.Models.Employee;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Employee.Handlers
{
    public class ValidateEmp_Skills:IValidationHandler<IEmp_SkillsCommand>
    {

        public IEnumerable<ValidationResult>Validate(IEmp_SkillsCommand command)
        {

            if(command.Emp_skills_detail.Count > 0)
            {
                foreach (Emp_skills_detail emp_skills_detail in command.Emp_skills_detail)
                {
                    emp_skills_detail.lineNumber = command.Emp_skills_detail.IndexOf(emp_skills_detail) + 1;
                    emp_skills_detail.AddInvalidField(command.controller.commandBus.Validate<IEmp_SkillsdetailCommand>(emp_skills_detail));
                    if(!emp_skills_detail.IsValid)
                    {
                        foreach(var errorMessage in emp_skills_detail.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "emp_skills_detail" });
                        }
                    }
                }


            }

            else
            {
                yield return new ValidationResult(ErsResources.GetMessage("No Records Inputted"));
            }
        }
    }
}