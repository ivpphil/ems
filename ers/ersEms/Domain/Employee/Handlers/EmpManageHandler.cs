﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Employee.Handlers
{
    public class EmpManageHandler : ICommandHandler<IEmpManageCommand>
    {
        public ICommandResult Submit(IEmpManageCommand command)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var repoLeave = ErsFactory.ersRequestFactory.GetErsLeaveBalanceRepository();

            var old_employee = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(command.emp_no);
            var new_employee = ErsFactory.ersEmployeeFactory.GetErsEmployee();


            new_employee.OverwriteWithModel(old_employee);
            new_employee.status = command.status;
            new_employee.job_title = command.job_title;
            new_employee.team = command.team;
            InsertLeaveBalance(command);

            repo.Update(old_employee, new_employee);

            return new CommandResult(true);
        }

        /// <summary>
        /// insert new leave total
        /// </summary>
        /// <param name="command"></param>
        private void InsertLeaveBalance(IEmpManageCommand command)
        {
            var repo = ErsFactory.ersRequestFactory.GetErsLeaveBalanceRepository();
            var leave_bl = ErsFactory.ersRequestFactory.GetErsLeaveBalance();
            leave_bl.emp_no = command.emp_no;
            leave_bl.now_sl = 0;
            leave_bl.now_vl = 0;
            leave_bl.last_sl = command.sick_leave;
            leave_bl.last_vl = command.vacation_leave;
            leave_bl.total_sl = command.sick_leave;
            leave_bl.total_vl = command.vacation_leave;
            repo.Insert(leave_bl);
          
        }
    } 
}