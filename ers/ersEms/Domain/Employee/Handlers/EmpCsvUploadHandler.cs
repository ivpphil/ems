﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Employee.Handlers
{
    public class EmpCsvUploadHandler : ICommandHandler<IEmpCsvUploadCommand>
    {
        public ICommandResult Submit(IEmpCsvUploadCommand command)
        {
            foreach (var model in command.csv_file.GetValidModels())
            {
                var employee = ErsFactory.ersEmployeeFactory.GetErsEmployee();
                var repository = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();

                var empNoExistDb = ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckDuplicateEmpNoRegister(model.emp_no);

                if (model != null && !empNoExistDb)
                {
                    employee.OverwriteWithParameter(model.GetPropertiesAsDictionary());
                    repository.Insert(employee, true);
                }
                else
                {
                    var old_model = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(model.emp_no);
                    var new_model = ErsFactory.ersEmployeeFactory.getErsEmployeeWithParameter(old_model.GetPropertiesAsDictionary());
                    new_model.OverwriteWithModel(model);
                    repository.Update(old_model, new_model);
                }

                var leaveRepo = ErsFactory.ersRequestFactory.GetErsLeaveBalanceRepository();
                var leaveObj = ErsFactory.ersRequestFactory.GetErsLeaveBalance();

                leaveObj.emp_no = model.emp_no;
                leaveObj.total_vl = model.total_vl;
                leaveObj.total_sl = model.total_sl;
                leaveObj.reason = ErsResources.GetMessage("from_csv");

                leaveRepo.Insert(leaveObj);

                command.controller.AddInformation(ErsResources.GetMessage("csv_upload_success"));
            }

            return new CommandResult(true);
        }
    }
}