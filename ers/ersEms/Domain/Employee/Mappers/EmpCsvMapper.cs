﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.request;
using System;
using System.Collections.Generic;

namespace ersEms.Domain.Employee.Mappers
{
    public class EmpCsvMapper : EmpSearchMapper,IMapper<IEmpCsvMappable>
    {
        public void Map(IEmpCsvMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }

        internal virtual void CreateCsvFile(IEmpCsvMappable objMappable)
        {
            objMappable.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();

            
            var employeeList = new List<ErsEmployee>();
            if (!String.IsNullOrEmpty(objMappable.emp_no_string))
            {
                objMappable.emp_no_arr = objMappable.emp_no_string.Split('-');

            }
       
            foreach (var emp in objMappable.emp_no_arr)
            {
                this.GetEmployee(emp, employeeList);
            }

            var filename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

            Models.Employee.EmpCsv emp_csv;

            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                objMappable.csvCreater.WriteCsvHeader<Models.Employee.EmpCsv>(writer);
                
                foreach (var item in employeeList)
                {
                    var format = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo;
                    item.fname = format.ToTitleCase(item.fname.ToLower());
                    item.lname = format.ToTitleCase(item.lname.ToLower());

                    emp_csv = new Models.Employee.EmpCsv();
                    emp_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());

                    if (item.position.HasValue && Enum.IsDefined(typeof(EnumPosition), item.position.Value))
                    {
                        emp_csv.position = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.position, EnumCommonNameColumnName.namename, Convert.ToInt32(item.position));
                    }
                    else
                    {
                        emp_csv.position = ErsResources.GetMessage("position_notexists");
                    }

                    if (item.team.HasValue && Enum.IsDefined(typeof(EnumTeam), item.team.Value))
                    {
                        emp_csv.team = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Team, EnumCommonNameColumnName.namename, Convert.ToInt32(item.team));
                    }
                    else
                    {
                        emp_csv.team = ErsResources.GetMessage("team_notexists");
                    }

                    if (item.job_title.HasValue)
                    {
                        var job = ErsFactory.ersJobFactory.GetErsJobTitleWithID(item.job_title.Value);
                        emp_csv.job_title = job?.job_title;
                    }

                    emp_csv.team_leader = item.team_leader;

                    emp_csv.date_hired = item.date_hired?.ToShortDateString();
                    emp_csv.reg_date = item.reg_date?.ToShortDateString();

                    var leaveDetail = ErsFactory.ersRequestFactory.GetErsLeaveBalanceWithEmpNo(item.emp_no);
                    emp_csv.total_vl = leaveDetail?.total_vl;
                    emp_csv.total_sl = leaveDetail?.total_sl;

                    objMappable.csvCreater.WriteBody(emp_csv, writer);
                }
            }
        }

        public void GetEmployee(string emp_no, List<ErsEmployee> empList)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            cri.emp_no = emp_no;
            cri.SetorderByStatus(Criteria.OrderBy.ORDER_BY_ASC);
            cri.SortEmployeeNo(Criteria.OrderBy.ORDER_BY_ASC);

            var result = repo.FindSingle(cri);
            if (result != null)
            {
                empList.Add(result);
            }
        }
    }
}