﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace ersEms.Domain.Employee.Mappers
{
    public class EmpSearchMapper : IMapper<IEmpSearchMappable>
    {
        public void Map(IEmpSearchMappable objMappable)
        {
            this.SearchList(objMappable);

            objMappable.user_emp_no = ErsContext.sessionState.Get("mcode");
            objMappable.fromEmpList = true;
        }

        public void SearchList(IEmpSearchMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var empRepo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = this.GetEmployeeCriteria(objMappable);

            objMappable.recordCount = empRepo.GetRecordCount(cri);
            cri.SetorderByStatus(Criteria.OrderBy.ORDER_BY_ASC);
            cri.SortEmployeeNo(Criteria.OrderBy.ORDER_BY_ASC);
            
            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(cri);
            }

            var listEmployee = empRepo.Find(cri);
            var listEmp = new List<Dictionary<string, object>>();

            var imagesetup = ErsFactory.ersUtilityFactory.getSetup().image_directory;
            TextInfo txtInfo = new CultureInfo("en-US", false).TextInfo;

            foreach (var emp_details in listEmployee)
            {
                var emp = emp_details.GetPropertiesAsDictionary();

                if (emp["team"] != null)
                {
                    emp["team"] = emp["team"].ToString() == "CSharp" ? ErsResources.GetMessage("CSharp") : emp["team"].ToString();
                }
                emp["gender"] = emp["gender"] != null? ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Gender, EnumCommonNameColumnName.namename, (int?)emp["gender"]) : ErsResources.GetMessage("NotSet");
                emp["address"] = emp["address"] != null ? emp["address"] : ErsResources.GetMessage("NotSet");
                emp["contact_no"] = emp["contact_no"] != null ? emp["contact_no"] : ErsResources.GetMessage("NotSet");

                var bdayConvert = Convert.ToDateTime(emp["birthday"]).ToShortDateString();
                emp["birthday"] = emp["birthday"] != null ? bdayConvert == DateTime.MinValue.ToShortDateString() ? ErsResources.GetMessage("NotSet"): bdayConvert : ErsResources.GetMessage("NotSet");
              
                emp["fullname"] = string.Format("{0} {1}", txtInfo.ToTitleCase(emp["fname"].ToString()), txtInfo.ToTitleCase(emp["lname"].ToString()));

                if (emp["image_file"] == null)
                {
                    emp["image_file"] = setup.default_name_image;
                }
                else if (!File.Exists(imagesetup + emp["emp_no"].ToString() + "\\" + emp["image_file"].ToString() + ""))
                {
                    emp["image_file"] = setup.default_name_image;
                }

                emp["position"] = emp["position"].ToString() != null ? emp["position"] : ErsResources.GetMessage("NotSet");
                
                listEmp.Add(emp);
            }
            
            objMappable.list = listEmp;
        }
        
        protected ErsEmployeeCriteria GetEmployeeCriteria(IEmpSearchMappable objMappable)
        {
            ErsEmployeeCriteria criteria = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
            if (objMappable.s_keyword.HasValue() && objMappable.searchField.ToString() == "" && objMappable.SearchTeam.ToString() == "" && objMappable.SearchPos.ToString() == "")
            {
                criteria.SearchCriteria(objMappable.s_keyword);
            }
            else if (objMappable.s_keyword.HasValue() && objMappable.searchField.ToString() == "" && objMappable.SearchTeam.HasValue && objMappable.SearchPos.ToString() == "")
            {
                criteria.SearchCriteria(objMappable.s_keyword);
                if (objMappable.s_team.HasValue())
                {
                    var team_list = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetIdFromStringLike(EnumCommonNameType.Team, objMappable.s_team);
                    if (!team_list.Count.Equals(0))
                    {
                        criteria.team_in = team_list;
                    }
                }
            }
            else if (objMappable.s_keyword.HasValue() && objMappable.searchField.ToString() == "" && objMappable.SearchTeam.ToString() == "" && objMappable.SearchPos.HasValue )
            {
                criteria.SearchCriteria(objMappable.s_keyword);
                if (objMappable.s_position.HasValue())
                {
                    var position_list = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetIdFromStringLike(EnumCommonNameType.position, objMappable.s_position);
                    if (!position_list.Count.Equals(0))
                    {
                        criteria.position_in = position_list;
                    }
                }
            }
            else
            {
                if (objMappable.s_emp_no.HasValue())
                {
                    criteria.emp_no = objMappable.s_keyword;
                }

                if (objMappable.s_email.HasValue())
                {
                    criteria.email = objMappable.s_keyword;
                }

                if (objMappable.s_fname.HasValue())
                {
                    criteria.fname_like = objMappable.s_keyword;
                }

                if (objMappable.s_lname.HasValue())
                {
                    criteria.lname_like = objMappable.s_keyword;
                }

                if (objMappable.s_team.HasValue())
                {
                    var team_list = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetIdFromStringLike(EnumCommonNameType.Team, objMappable.s_team);
                    if (!team_list.Count.Equals(0))
                    {
                        criteria.team_in = team_list;
                    }
                }

                if (objMappable.s_position.HasValue())
                {
                    var position_list = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetIdFromStringLike(EnumCommonNameType.position, objMappable.s_position);
                    if (!position_list.Count.Equals(0))
                    {
                        criteria.position_in = position_list;
                    }
                }
            }
            
            return criteria;
        }
    }
}