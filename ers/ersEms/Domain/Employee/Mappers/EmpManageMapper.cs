﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersEms.Domain.Employee.Mappers
{
    public class EmpManageMapper:IMapper<IEmpManageMappable>
    {
        public void Map(IEmpManageMappable objMappable)
        {
            var employee = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(objMappable.emp_no);
            if (employee != null)
            {
                objMappable.OverwriteWithParameter(employee.GetPropertiesAsDictionary());
            }

            var leave_bal = ErsFactory.ersRequestFactory.GetErsLeaveBalanceWithEmpNo(objMappable.emp_no);
            if (leave_bal != null)
            {
                objMappable.sick_leave = leave_bal.total_sl.Value;
                objMappable.vacation_leave = leave_bal.total_vl.Value;
            }

        }
    }
}