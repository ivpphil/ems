﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ersEms.Domain.Employee.Mappers
{
    public class EmpMessageMapper
        : IMapper<IEmpMessageMappable>
    {
        public void Map(IEmpMessageMappable objMappable)
        {
            updateSeenFlg(objMappable.thread_no, objMappable.emp_no);

            if (objMappable.emp_no.HasValue())
            {
                //get all thread conversation
                objMappable.threads = GetMessageThreads(objMappable);
            }

            if (objMappable.write_new)
            {
                if (!objMappable.fromEmpList)
                {
                    objMappable.recipient_emp_no = string.Empty;
                }
                return;
            }

            if (objMappable.threads != null && objMappable.threads.Count > 0)
            {
                //get most recent conversation thread set thread_no
                getDefaultThread(objMappable);
            }

            if (objMappable.emp_no.HasValue())
            {
                //get all thread conversation
                objMappable.threads = GetMessageThreads(objMappable);
            }

            if (objMappable.thread_no.HasValue)
            {
                objMappable.messages = GetMessages(objMappable.thread_no, objMappable.emp_no);
            }

        }

        public List<Dictionary<string, object>> GetMessageThreads(IEmpMessageMappable objMappable)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetEmployeeThreadSpecification();

            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadCriteria();

            if (objMappable.fromEmpList && objMappable.recipient_emp_no.HasValue())
            {
                cri.conversation_thread = new[] { objMappable.emp_no, objMappable.recipient_emp_no };
            }
            else
            {
                cri.sender_or_recipient = objMappable.emp_no;
            }

            cri.getThreadActiveOnly = objMappable.emp_no;
            cri.SetOrderByRecentLog(Criteria.OrderBy.ORDER_BY_DESC);

            var list = repo.GetSearchData(cri);

            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    var name_display = string.Empty;
                    if (!objMappable.emp_no.Equals(Convert.ToString(item["sender_emp_no"])))
                    {
                        name_display = getNameDisplay(Convert.ToString(item["sender_name"]));

                        item.Add("inbox_name", name_display);
                        item.Add("inbox_emp_no", Convert.ToString(item["sender_emp_no"]));
                        item.Add("inbox_unseen", Convert.ToInt32(item["sender_unseen_cnt"]));


                    }
                    else if (!objMappable.emp_no.Equals(Convert.ToString(item["recipient_emp_no"])))
                    {
                        name_display = getNameDisplay(Convert.ToString(item["recipient_name"]));

                        item.Add("inbox_name", name_display);
                        item.Add("inbox_emp_no", Convert.ToString(item["recipient_emp_no"]));
                        item.Add("inbox_unseen", Convert.ToInt32(item["recipient_unseen_cnt"]));

                    }
                }
                return list;
            }
            return null;
        }

        public void getDefaultThread(IEmpMessageMappable objMappable)
        {
            var recent_thread = ErsFactory.ersEmployeeFactory.GetRecentThread(new[] { objMappable.emp_no, objMappable.recipient_emp_no }, objMappable.thread_no);

            if (recent_thread == null)
            {
                return;
            }

            if (!objMappable.emp_no.Equals(recent_thread.recipient_emp_no))
            {
                objMappable.recipient_emp_no = recent_thread.recipient_emp_no;

            }
            else if (!objMappable.emp_no.Equals(recent_thread.sender_emp_no))
            {
                objMappable.recipient_emp_no = recent_thread.sender_emp_no;
            }

            objMappable.subject = recent_thread.subject;

            if (!objMappable.thread_no.HasValue)
            {
                objMappable.thread_no = recent_thread.id;
            }

            updateSeenFlg(objMappable.thread_no, objMappable.emp_no);

        }

        public List<Dictionary<string, object>> GetMessages(int? thread_no, string emp_no)
        {
            var thread = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadWithId(thread_no);
            if (thread == null)
            {
                return null;
            }
            var returnList = new List<Dictionary<string, object>>();
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadDetailsRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadDetailsCriteria();


            if (thread.recipient_emp_no == emp_no)
            {
                cri.intime_greater_than = thread.recipient_last_active;
            }
            else if (thread.sender_emp_no == emp_no)
            {
                cri.intime_greater_than = thread.sender_last_active;
            }
            else
            {
                return null;
            }

            cri.thread_no = thread_no;
            cri.active = EnumActive.Active;
            cri.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_ASC);

            var list = repo.Find(cri);
            if (list.Count() > 0)
            {
                foreach (var item in list)
                {
                    var dic = new Dictionary<string, object>();
                    dic = item.GetPropertiesAsDictionary();
                    returnList.Add(dic);
                }
            }
            return returnList;
        }

        public void updateSeenFlg(int? thread_no, string emp_no)
        {
            var threadExist = ErsFactory.ersEmployeeFactory.CheckThreadDetailsExist(thread_no, emp_no);

            if (!threadExist || !thread_no.HasValue || !emp_no.HasValue())
            {
                return;
            }
            var repo = ErsFactory.ersEmployeeFactory.GetEmployeeThreadSeenSpecification();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadDetailsCriteria();

            cri.maker_emp_no_not_equal = emp_no;
            cri.thread_no = thread_no;

            repo.update(cri);
        }

        public string getNameDisplay(string param_name)
        {
            var fname = string.Empty;
            var lname = string.Empty;

            if (param_name.Contains(" "))
            {
                var names = param_name.Split(' ');
                fname = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(names[0].ToLower());

                if (names.Count() > 1)
                {
                    lname = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(names[1].ToLower());
                }
            }
            return param_name = fname + " " + lname;
        }
    }
}