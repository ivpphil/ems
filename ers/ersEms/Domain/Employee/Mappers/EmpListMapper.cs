﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System.Collections.Generic;

namespace ersEms.Domain.Employee.Mappers
{
    public class EmpListMapper:IMapper<IEmpListMappable>
    {
        public void Map(IEmpListMappable objMappable)
        {
            this.emp_list(objMappable);            
        }

        internal void emp_list(IEmpListMappable objMappable)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            objMappable.recordCount = repo.GetRecordCount(cri);

            cri.SetOrderByEmployeeNo(Criteria.OrderBy.ORDER_BY_ASC);            

            if(objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(cri);
            }
   
            var emp_list = repo.Find(cri); 
            var employee_list = new List<Dictionary<string, object>>();

            foreach(var emp in emp_list)
            {
                var dictionary = emp.GetPropertiesAsDictionary();
                employee_list.Add(dictionary);
            }

            objMappable.list = employee_list;
                
        }
    }
}