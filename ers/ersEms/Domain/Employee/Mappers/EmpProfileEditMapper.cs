﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System.IO;

namespace ersEms.Domain.Employee.Mappers
{
    public class EmpProfileEditMapper : IMapper<IEmpProfileEditMappable>
    {
        public void Map(IEmpProfileEditMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            objMappable.temp_path = setup.image_temp_directory + objMappable.profile_emp_no + "\\";
            objMappable.stored_img_path = setup.image_directory + objMappable.profile_emp_no + "\\";

            var temp_path_file = objMappable.temp_path + objMappable.temp_file_name;
            var stored_img_path_file = objMappable.stored_img_path + objMappable.image_file;
            
            if (objMappable.temp_file_name.HasValue() || objMappable.image_file.HasValue())
            {
                objMappable.hasImage = true;

                if (File.Exists(temp_path_file))
                {
                    objMappable.hasTempFile = true;
                }
                else if (!File.Exists(stored_img_path_file))
                {
                    objMappable.hasImage = false;
                }
            }
        }
    }
}