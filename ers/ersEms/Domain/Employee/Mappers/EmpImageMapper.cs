﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System.IO;

namespace ersEms.Domain.Employee.Mappers
{
    public class EmpImageMapper
        : IMapper<IEmpImageMappable>
    {
        public void Map(IEmpImageMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();
            var temp_path = setup.image_temp_directory +objMappable.emp_no + "\\";

            if (!Directory.Exists(temp_path))
            {
                Directory.CreateDirectory(temp_path);
            }
            if(!string.IsNullOrEmpty(objMappable.imgfile.FileName))
            {
                if (!File.Exists(temp_path + objMappable.imgfile))
                {

                    if (objMappable.imgfile.ContentLength > 0)
                    {
                        objMappable.temp_file_name = uploadedFileHelper.GetTempFileName(objMappable.imgfile);
                        uploadedFileHelper.SaveTempFile(objMappable.imgfile, temp_path, objMappable.temp_file_name);
                    }
                }
            }
        }
    }
}