﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;

namespace ersEms.Domain.Employee.Mappers
{
    public class TeamManagementMapper
        : EmpSearchMapper, IMapper<ITeamManagementMappable>
    {
        ErsEmployeeRepository repository = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
        TextInfo thread = Thread.CurrentThread.CurrentCulture.TextInfo;
        Setup setup = ErsFactory.ersUtilityFactory.getSetup();
        string team_lead_mcode = ErsContext.sessionState.Get("mcode");

        public void Map(ITeamManagementMappable objMappable)
        {
            this.GetTeamMemberList(objMappable);
            this.GetNonTeamMemberList(objMappable);
        }

        internal void GetTeamMemberList(ITeamManagementMappable objMappable)
        {
            var team_criteria = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            team_criteria.team_leader = team_lead_mcode;

            objMappable.recordCount = repository.GetRecordCount(team_criteria);

            team_criteria.SetOrderByEmployeeNo(Criteria.OrderBy.ORDER_BY_ASC);

            if(objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(team_criteria);
            }

            var team_result_list = repository.Find(team_criteria);

            var team_list = new List<Dictionary<string, object>>();

            foreach(var member in team_result_list)
            {
                member.fname = thread.ToTitleCase(member.fname.ToLower());
                member.lname = thread.ToTitleCase(member.lname.ToLower());
                member.image_file = member.image_file.HasValue() && File.Exists(setup.image_directory + member.emp_no + "\\" + member.image_file) ? member.image_file : setup.default_name_image;

                var dictionary = member.GetPropertiesAsDictionary();
                team_list.Add(dictionary);
            }

            objMappable.list = team_list;
        }

        internal void GetNonTeamMemberList(ITeamManagementMappable objMappable)
        {
            var tl_details = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(team_lead_mcode);
            var non_team_criteria = GetEmployeeCriteria(objMappable);

            non_team_criteria.team_leader_isNull();
            non_team_criteria.position = (int?)EnumPosition.Member;
            non_team_criteria.team = tl_details.team;

            objMappable.non_team_recordCount = repository.GetRecordCount(non_team_criteria);

            non_team_criteria.SetOrderByEmployeeNo(Criteria.OrderBy.ORDER_BY_ASC);

            if (objMappable.non_team_pager != null)
            {
                objMappable.non_team_pager.SetLimitAndOffsetToCriteria(non_team_criteria);
            }

            var non_member_result_list = repository.Find(non_team_criteria);

            var non_member_list = new List<Dictionary<string, object>>();

            foreach (var non_member in non_member_result_list)
            {
                non_member.fname = thread.ToTitleCase(non_member.fname.ToLower());
                non_member.lname = thread.ToTitleCase(non_member.lname.ToLower());
                non_member.image_file = non_member.image_file.HasValue() && File.Exists(setup.image_directory + non_member.emp_no + "\\" + non_member.image_file) ? non_member.image_file : setup.default_name_image;

                var non_member_dictionary = non_member.GetPropertiesAsDictionary();
                non_member_list.Add(non_member_dictionary);
            }

            objMappable.non_team_list = non_member_list;
        }
    }
}