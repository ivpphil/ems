﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System.IO;

namespace ersEms.Domain.Employee.Mappers
{
    public class EmpProfileMapper : IMapper<IEmpProfileMappable>
    {
        public void Map(IEmpProfileMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (!objMappable.profile_emp_no.HasValue())
            {
                objMappable.profile_emp_no = ErsContext.sessionState.Get("mcode");
            }
            var emp = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(objMappable.profile_emp_no);

            if (emp == null)
            {
                return;
            }

            var employee_profile = ErsFactory.ersEmployeeFactory.GetErsEmployee();
            employee_profile.OverwriteWithModel(emp);
            objMappable.employee = employee_profile;
            var path = setup.image_directory + emp.emp_no + "\\" + emp.image_file;

            if (File.Exists(path))
            {
                objMappable.hasImage = true;
            }

        }
    }
}