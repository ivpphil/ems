﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersEms.Domain.Employee.Mappers
{
    public class EmployeeProjectMapper:IMapper<IEmployeeProjectMappable>
    {
        public void Map(IEmployeeProjectMappable objMappable)
        {
            this.GetProjectList(objMappable);            
        }

        internal void GetProjectList(IEmployeeProjectMappable objMappable)
        {
            var spec = ErsFactory.ersEmployeeFactory.GetEmployeeProjectSpecification();
            var crit = ErsFactory.ersEmployeeFactory.GetErsDreportCriteria();

            crit.emp_no = objMappable.emp_no;
            crit.ignorePcodes();
            crit.setActivePcode();
            crit.SetGroupByPcode();
            crit.SetGroupByPcodeDescription();
            crit.SetOrderByProjReport_date(Criteria.OrderBy.ORDER_BY_DESC);

            objMappable.projectCount = spec.GetCountData(crit);
            var list = spec.GetSearchData(crit);

            objMappable.projectList = list;           
        }
    }
}