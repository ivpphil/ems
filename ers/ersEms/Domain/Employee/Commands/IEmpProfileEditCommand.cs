﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.Web;

namespace ersEms.Domain.Employee.Commands
{
    public interface IEmpProfileEditCommand : ICommand
    {
        HttpPostedFileBase img_file { get; set; }

        ErsEmployee old_records { get; }

        List<string> employee_correction_list { get; set; }

        string image_file { get; set; }
        
        string profile_emp_no { get; set; }

        string fname { get; set; }

        string lname { get; set; }

        string email { get; set; }

        string email_confirm { get; set; }

        string password { get; set; }

        string password_confirm { get; set; }

        string current_password { get; set; }

        string address { get; set; }

        DateTime? birthday { get; set; }

        EnumSex? gender { get; set; }

        string contact_no { get; set; }

        string temp_path { get; set; }

        string temp_file_name { get; set; }

        string stored_img_path { get; set; }

        EnumPosition? position { get; set; }

        string reasonStr { get; set; }

        bool imageChanged { get; set; }

        bool passChanged { get; set; }
    }
}