﻿using ersEms.Models.csv;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Employee.Commands
{
    public interface IEmpCsvUploadCommand : ICommand
    {
       ErsCsvContainer<EmployeeCSV> csv_file { get; set; }

       bool chk_find { get; set; }
    }
}
