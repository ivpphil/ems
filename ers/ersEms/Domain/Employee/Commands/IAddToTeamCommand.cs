﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Employee.Commands
{
    public interface IAddToTeamCommand
        :ICommand
    {
        string update_emp { get; set; }
    }
}