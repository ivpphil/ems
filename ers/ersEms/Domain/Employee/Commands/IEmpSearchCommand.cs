﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Employee.Commands
{
    public interface IEmpSearchCommand:ICommand,IEmpSearchMappable
    {
        EnumEmpSearch? searchField { get; set; }

        EnumTeam? SearchTeam { get; set; }

        EnumPosition? SearchPos { get; set; }
    }
}
