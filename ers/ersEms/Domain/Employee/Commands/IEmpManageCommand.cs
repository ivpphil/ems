﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Employee.Commands
{
    public interface IEmpManageCommand : ICommand
    {
        string fname { get; set; }

        string lname { get; set; }

        string emp_no { get; set; }

        int? job_title { get; set; }

        EnumTeam? team { get; set; }

        EnumEmpStatus? status { get; set; }

        double vacation_leave { get; set; }

        double sick_leave { get; set; }
    }
}
