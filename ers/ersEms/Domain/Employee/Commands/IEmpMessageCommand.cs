﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Employee.Commands
{
    public interface IEmpMessageCommand : ICommand
    {
        bool send { get; set; }

        bool delete { get; set; }

        int? thread_no { get; set; }

        string message { get; set; }

        string recipient_emp_no { get; set; }

        string emp_no { get; }

        int?[] delete_thread_array { get; set; }

        string subject { get; set; }

    }
}