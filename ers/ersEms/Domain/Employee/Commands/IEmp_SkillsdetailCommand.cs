﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersEms.Domain.Employee.Commands
{
    public interface IEmp_SkillsdetailCommand:ICommand
    {
         int? id { get; set; }

        string emp_no { get; set; }

        string skill_desc { get; set; }

        int? years_exp { get; set; }

        string remarks { get; set; }

        DateTime? in_time { get; set; }
    }
}
