﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Employee.Commands
{
    public interface IEmpCsvUploadRecordsCommand : ICommand
    {
        string emp_no { get; set; }

        int? desknet_id { get; set; }

        string fname { get; set; }

        string lname { get; set; }
    
        string email { get; set; }

        string w_team { get; set; }

        int? team { get; }

        string team_convert { get; }

        string team_leader { get; set; }

        double? total_vl { get; set; }

        double? total_sl { get; set; }

        int? job_title { get; }

        string w_job_title { get; set; }

        string job_title_convert { get; }
        
    }
}
