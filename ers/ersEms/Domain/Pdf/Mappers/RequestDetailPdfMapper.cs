﻿using ersEms.Domain.Employee.Mappables;
using ersEms.Domain.Pdf.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Pdf.Mappers
{
    public class RequestDetailPdfMapper
        : IMapper<IRequestDetailPdfMappable>
    {
        public void Map(IRequestDetailPdfMappable objMappable)
        {

            var repo = ErsFactory.ersRequestFactory.GetErsRequestRepository();
            var cri = ErsFactory.ersRequestFactory.GetErsRequestCriteria();

            cri.id = objMappable.request_id;
            cri.active = EnumActive.Active;
            cri.status = EnumStatusRequest.Approved;

            var request = repo.FindSingle(cri);

            if (request == null)
            {
                throw new ErsException(ErsResources.GetMessage("pdf_request_not_found"));
            }

            var emp = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(request.emp_no);
            var schedule = ErsFactory.ersRequestFactory.getErsScheduleWithDesknetIDAndDate(Convert.ToInt16(emp.desknet_id),Convert.ToDateTime(request.date_start));
            if (schedule != null)
            {
                objMappable.schedule = Convert.ToDateTime(schedule.time_start).ToString("hh:mm tt") + " ~ "+ Convert.ToDateTime(schedule.time_end).ToString("hh:mm tt");
            }
            else
            {
                objMappable.schedule = "10:00 AM ~ 07:00 PM";
            }

            objMappable.OverwriteWithParameter(request.GetPropertiesAsDictionary());
            setLeaveBalances(objMappable);
        }
        public void setLeaveBalances(IRequestDetailPdfMappable objMappable)
        {
            var repo = ErsFactory.ersRequestFactory.GetErsLeaveBalanceRepository();
            var cri = ErsFactory.ersRequestFactory.GetErsLeaveBalanceCriteria();
            cri.emp_no = objMappable.emp_no;
            cri.AddOrderBy("intime", Criteria.OrderBy.ORDER_BY_DESC);
            cri.LIMIT = 1;

            var leave_bal = repo.FindSingle(cri);

            if (leave_bal != null)
            {
                objMappable.vl_balance = leave_bal.total_vl;
                objMappable.sl_balance = leave_bal.total_sl;
            }
        }
    }
}