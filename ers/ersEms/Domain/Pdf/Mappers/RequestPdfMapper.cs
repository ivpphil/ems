﻿using ersEms.Domain.Employee.Mappables;
using ersEms.Domain.Pdf.Mappables;
using ersEms.Models.Pdf.pdf;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Pdf.Mappers
{
    public class RequestPdfMapper
        : IMapper<IRequestPdfMappable>
    {
        public void Map(IRequestPdfMappable objMappable)
        {
            objMappable.form = ErsFactory.ersPdfFactory.GetErsPdfForm();

            var form_list = new List<ErsModelBase>();

            if (objMappable.single_download_request != null)
            {
                objMappable.request_id = objMappable.single_download_request.Split('-');
            }

            if (objMappable.request_id == null)
            {
                throw new ErsException(ErsResources.GetMessage("pdf_request_not_found"));
            }

            string[] id_array = new[] { String.Empty };
            foreach (var id in objMappable.request_id)
            {
                id_array = id.Split('-');
                foreach (var item in id_array)
                {
                    var template = new RequestTemplatePdf();
                    int i = 0;
                    Int32.TryParse(item,out i) ;

                    template.request_id = i;
                    objMappable.controller.mapperBus.Map<IRequestDetailPdfMappable>(template);
                    form_list.Add(template);
                }
            }            
            objMappable.pdf_template = form_list;
        }
    }
}