﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Pdf.Mappables
{
    public interface IRequestDetailPdfMappable
        :IMappable
    {
        int request_id { get; set; }

        EnumStatusRequest? status { get; set; }
        
        string emp_no { get; set; }

        EnumLeaveType leave_type { get; set; }

        string reason { get; set; }

        DateTime date_start { get; set; }

        DateTime date_end { get; set; }

        DateTime time_start { get; set; }

        DateTime time_end { get; set; }

        double? vl_balance { get; set; }

        double? sl_balance { get; set; }

        string schedule { get; set; }
    }
}