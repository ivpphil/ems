﻿using ersEms.Models.Pdf.pdf;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Pdf.Mappables
{
    public interface IRequestPdfMappable
        : IMappable
    {
        string[] request_id { get; set; }

        List<ErsModelBase> pdf_template { get; set; }

        ErsPdfForm form { get; set; }

        string single_download_request { get; set; }

    }
}