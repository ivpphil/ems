﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;

namespace ersEms.Domain.Schedule.Mappables
{
    public interface IScheduleMemberMappable
        :IMappable
    {
        DateTime date { get; set; }
        List<Dictionary<string, object>> main_schedule_list { get; set; }
        List<string> scheduleHeaderList { get; set; }
        long recordCount { get; set; }
    }
}