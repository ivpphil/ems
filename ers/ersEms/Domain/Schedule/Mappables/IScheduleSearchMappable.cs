﻿using System;
using System.Collections.Generic;

namespace ersEms.Domain.Schedule.Mappables
{
    public interface IScheduleSearchMappable :IScheduleMemberMappable
    {
        string search { get; set; }
        int selectedField { get; set; }
        int time_start { get; set; }
        int time_end { get; set; }
        DateTime? s_schedule_date { get; set; }
        Dictionary<int, string> timeList { get; set; }
    }
}