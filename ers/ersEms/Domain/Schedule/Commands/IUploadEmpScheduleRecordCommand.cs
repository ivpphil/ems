﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Schedule.Commands
{
    public interface IUploadEmpScheduleRecordCommand : ICommand
    {
        int? desknet_id { get; set; }
        string sched_owner_name { get; set; }
        string sched_type { get; set; }
        string sched_details { get; set; }
    }
}