﻿using ersEms.Models.Schedule;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Schedule.Commands
{
    public interface IUploadEmpScheduleCommand : ICommand
    {
        bool hasError { get; set; }
        ErsCsvContainer<upload_emp_schedule_record> csv_file { get; set; }
        bool chk_find { get; set; }
        string error_messages { get; set; }

    }
}