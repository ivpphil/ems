﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;

namespace ersEms.Domain.Schedule.Commands
{
    public interface IEmployeeScheduleCommand : ICommand
    {
        DateTime date { get; set; }
        int selectedField { get; set; }
        List<Dictionary<string, object>> main_schedule_list { get; set; }
        string search { get; set; }
        int time_start { get; set; }
        int time_end { get; set; }
        long recordCount { get; set; }
    }
}