﻿using ersEms.Domain.Schedule.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ersEms.Domain.Schedule.Mappers
{
    public class ScheduleSearchMapper : IMapper<IScheduleSearchMappable>
    {
        public void Map(IScheduleSearchMappable objMappable)
        {
            var prevDate = new string[7];
            var endDate = objMappable.date.AddDays(6).Date;

            GetScheduleHeaderList(objMappable, prevDate);
            GetScheduleList(objMappable, prevDate);
        }

        private void GetScheduleHeaderList(IScheduleSearchMappable objMappable, string[] prevDate)
        {
            if (objMappable.date == DateTime.MinValue)
            {
                objMappable.date = DateTime.Now;
            }
            if (objMappable.s_schedule_date != null)
            {
                objMappable.date = Convert.ToDateTime(objMappable.s_schedule_date);
            }

            objMappable.scheduleHeaderList = ErsFactory.ersRequestFactory.GetSetScheduleEmployeeDetailsStgy().GetScheduleHeader(objMappable.date, prevDate);
        }

        public void GetScheduleList(IScheduleSearchMappable objMappable, string[] prevDate)
        {
            var employeeRepo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var employeeCri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            var spec = ErsFactory.ersRequestFactory.GetEmployeeWeeklyScheduleSpecification();
            var schedCri = ErsFactory.ersRequestFactory.GetErsScheduleCriteria();

            var emp_details_list = new List<Dictionary<string, object>>();
            var emp_details = new Dictionary<string, object>();
            var stgy = ErsFactory.ersRequestFactory.GetSetScheduleEmployeeDetailsStgy();

            if ((objMappable.selectedField == Convert.ToInt16(EnumSchedSearch.FirstName) || objMappable.selectedField == Convert.ToInt16(EnumSchedSearch.LastName)))
            {
                if (!string.IsNullOrEmpty(objMappable.search))
                {
                    if (objMappable.selectedField == Convert.ToInt16(EnumSchedSearch.FirstName))
                    {
                        employeeCri.fname_lower_like = objMappable.search;
                    }
                    if (objMappable.selectedField == Convert.ToInt16(EnumSchedSearch.LastName))
                    {
                        employeeCri.lname_lower_like = objMappable.search;
                    }
                }

                var employeeList = employeeRepo.Find(employeeCri);

                foreach (var employee in employeeList)
                {
                    if (employee.desknet_id != 0)
                    {
                        var scheduleList = spec.ScheduleSearch(schedCri, prevDate, employee.desknet_id);

                        foreach (var sched in scheduleList)
                        {
                            emp_details = stgy.GetEmployeeDetails(sched, prevDate);
                            emp_details_list.Add(emp_details);
                        }
                    }
                }
            }
            else if (objMappable.selectedField == Convert.ToInt16(EnumSchedSearch.Schedule))
            {
                if (objMappable.time_start != 0)
                {
                    spec.time_start = ErsFactory.ersRequestFactory.GetScheduleTimeIntervalStgy().CheckStringTime(objMappable.time_start, objMappable.timeList);
                }
                if (objMappable.time_end != 0)
                {
                    spec.time_end = ErsFactory.ersRequestFactory.GetScheduleTimeIntervalStgy().CheckStringTime(objMappable.time_end, objMappable.timeList);
                }

                var scheduleList = spec.ScheduleSearch(schedCri, prevDate, null);

                foreach (var sched in scheduleList)
                {
                    if (sched["emp_no"] != null)
                    {
                        sched["day1_w_leave_type"] = sched["day1_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day1_leave_type"])));
                        sched["day2_w_leave_type"] = sched["day2_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day2_leave_type"])));
                        sched["day3_w_leave_type"] = sched["day3_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day3_leave_type"])));
                        sched["day4_w_leave_type"] = sched["day4_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day4_leave_type"])));
                        sched["day5_w_leave_type"] = sched["day5_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day5_leave_type"])));
                        sched["day6_w_leave_type"] = sched["day6_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day6_leave_type"])));
                        sched["day7_w_leave_type"] = sched["day7_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day7_leave_type"])));
                        
                        ErsFactory.ersRequestFactory.GetSetScheduleDefaultTimeStgy().GetDefaultTime(sched, prevDate);
                        emp_details = stgy.GetEmployeeDetails(sched, prevDate);
                        emp_details_list.Add(emp_details);
                    }
                }
            }

            objMappable.main_schedule_list = emp_details_list.Select(y => y).OrderBy(y => y["fname"]).ToList();
            objMappable.recordCount = objMappable.main_schedule_list.Count();

            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }
        }
    }
}