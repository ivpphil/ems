﻿using ersEms.Domain.Schedule.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ersEms.Domain.Schedule.Mappers
{
    public class ScheduleMemberMapper : IMapper<IScheduleMemberMappable>
    {
        public void Map(IScheduleMemberMappable objMappable)
        {
            var prevDate = new string[7];

            GetScheduleHeaderList(objMappable, prevDate);
            GetScheduleList(objMappable, prevDate);
        }

        private void GetScheduleHeaderList(IScheduleMemberMappable objMappable, string[] prevDate)
        {
            if (objMappable.date == DateTime.MinValue)
            {
                objMappable.date = DateTime.Now;
            }

            objMappable.scheduleHeaderList = ErsFactory.ersRequestFactory.GetSetScheduleEmployeeDetailsStgy().GetScheduleHeader(objMappable.date, prevDate);
        }

        private void GetScheduleList(IScheduleMemberMappable objMappable, string[] prevDate)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var image_directory = ErsFactory.ersUtilityFactory.getSetup().image_directory;
            var end_date = objMappable.date.AddDays(6).Date;            
            var user_details = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(ErsContext.sessionState.Get("mcode"));

            var criteria = ErsFactory.ersRequestFactory.GetErsScheduleCriteria();
            var schedule_list = ErsFactory.ersRequestFactory.GetEmployeeWeeklyScheduleSpecification().ScheduleSearch(criteria, prevDate,null);

            var schedList = new List<Dictionary<string, object>>();
            var otherList = new List<Dictionary<string, object>>();
            var memberList = new List<Dictionary<string, object>>();

            var stgy = ErsFactory.ersRequestFactory.GetSetScheduleEmployeeDetailsStgy();

            if (schedule_list.Count != 0)
            {
                foreach (var sched in schedule_list)
                {
                    ErsFactory.ersRequestFactory.GetSetScheduleDefaultTimeStgy().GetDefaultTime(sched, prevDate);
                    stgy.GetEmployeeDetails(sched, prevDate);

                    if (sched["emp_no"] != null)
                    {
                        sched["day1_w_leave_type"] = sched["day1_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day1_leave_type"])));
                        sched["day2_w_leave_type"] = sched["day2_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day2_leave_type"])));
                        sched["day3_w_leave_type"] = sched["day3_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day3_leave_type"])));
                        sched["day4_w_leave_type"] = sched["day4_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day4_leave_type"])));
                        sched["day5_w_leave_type"] = sched["day5_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day5_leave_type"])));
                        sched["day6_w_leave_type"] = sched["day6_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day6_leave_type"])));
                        sched["day7_w_leave_type"] = sched["day7_leave_type"] == null ? null : stgy.getSchedType(sched, (EnumSchedType)(Convert.ToInt32(sched["day7_leave_type"])));

                        if (sched["emp_no"].ToString() == user_details.mcode)
                        {
                            schedList.Add(sched);
                        }

                        if (sched["team_leader"] != null)
                        {
                            if (sched["emp_no"].ToString() != user_details.mcode)
                            {
                                if (sched["emp_no"].ToString() != user_details.mcode && sched["team_leader"].ToString() == user_details.mcode)
                                {
                                    memberList.Add(sched);
                                }
                                else
                                {
                                    otherList.Add(sched);
                                }
                            }
                        }
                        else
                        {
                            if (sched["emp_no"].ToString() == user_details.mcode)
                            {
                                continue;
                            }

                            otherList.Add(sched);
                        }
                    }                   
                }

                objMappable.main_schedule_list = schedList.Concat(memberList.Select(x => x).OrderBy(x => x["fname"]).ToList()).Concat(otherList.Select(x=>x).OrderBy(x=>x["fname"]).ToList()).ToList();
                objMappable.recordCount = objMappable.main_schedule_list.Count();
                if (objMappable.recordCount == 0)
                {
                    objMappable.controller.AddInformation("No records to display.");
                    return;
                }
            }
        }
    }
}