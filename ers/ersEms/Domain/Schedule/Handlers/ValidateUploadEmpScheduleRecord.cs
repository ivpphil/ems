﻿using ersEms.Domain.Schedule.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Schedule.Handlers
{
    public class ValidateUploadEmpScheduleRecord : IValidationHandler<IUploadEmpScheduleRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(IUploadEmpScheduleRecordCommand command)
        {
            if (command.desknet_id.HasValue && !command.sched_owner_name.HasValue())
            {
                yield return command.CheckRequired("sched_type");
                if (command.sched_type.HasValue())
                {
                    int? sched_type_id = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetIdFromString(EnumCommonNameType.SchedType, EnumCommonNameColumnName.namename, command.sched_type);
                    if (!sched_type_id.HasValue)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("invalid_field", ErsResources.GetFieldName("sched_type")), new[] { "sched_type" });
                    }

                    if ((EnumSchedType)sched_type_id == EnumSchedType.NoSchedType && !command.sched_details.HasValue())
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("no_ETA"));
                    }
                }
            }
            else
            {
                yield return command.CheckRequired("desknet_id");
            }         
        }
    }
}