﻿using ersEms.Domain.Schedule.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Schedule.Handlers
{
    public class ValidateEmployeeSchedule : IValidationHandler<IEmployeeScheduleCommand>
    {
        public IEnumerable<ValidationResult> Validate(IEmployeeScheduleCommand command)
        {
            if (command.selectedField == 0)
            {
                if (!string.IsNullOrEmpty(command.search) || (command.time_end != 0 && command.time_start != 0))
                {
                    yield return new ValidationResult(ErsResources.GetMessage("no_searchby_filter"));
                }
            }
            else {
                var selected = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.EmployeeSearch, EnumCommonNameColumnName.namename, command.selectedField);
                if (string.IsNullOrEmpty(selected))
                {
                    yield return new ValidationResult(ErsResources.GetMessage("not_valid_search_by"));
                }
            }

            yield return null;
        }
    }
}