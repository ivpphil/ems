﻿using ersEms.Domain.Schedule.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.util;
using System;

namespace ersEms.Domain.Schedule.Handlers
{
    public class UploadEmpScheduleHandler : ICommandHandler<IUploadEmpScheduleCommand>
    {
        public ICommandResult Submit(IUploadEmpScheduleCommand command)
        {
            int? desknet_id = 0;
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            
            if (!command.hasError)
            {
                foreach (var model in command.csv_file.GetValidModels())
                {
                    var repository = ErsFactory.ersRequestFactory.GetErsScheduleRepository();
                    var criteria = ErsFactory.ersRequestFactory.GetErsScheduleCriteria();

                    if (model.desknet_id.HasValue && model.sched_owner_name.HasValue())
                    {
                        // Setup date range filter
                        // Start of the week is always on Sunday
                        DateTime startOfTheWeek = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Sunday);
                        DateTime DateFrom = startOfTheWeek;
                        DateTime DateTo = DateTime.Now;

                        string today = DateTime.Now.DayOfWeek.ToString();

                        if (setup.newSchedDays.Contains(today))         // NewSchedDays : Retrieve this weeks schedule and + a weeks onward. 
                        {
                            DateTo = startOfTheWeek.AddDays(setup.newSchedGetLength);
                        }
                        else if (setup.weekDays.Contains(today))        // WeekDays : Retrieve this weeks schedule only.
                        {
                            DateTo = startOfTheWeek.AddDays(setup.thisWeekSchedGetLength);
                        }

                        criteria.date_range(DateFrom, DateTo);
                        criteria.desknet_id = model.desknet_id;
                        repository.Delete(criteria);

                        desknet_id = model.desknet_id;
                    }
                    else
                    {
                        model.desknet_id = desknet_id;
                    }

                    var emp_details = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithDesknetID((int)model.desknet_id);

                    if (emp_details != null)
                    {
                        if (model.desknet_id.HasValue && !model.sched_owner_name.HasValue())
                        {
                            int? sched_type_id = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetIdFromString(EnumCommonNameType.SchedType, EnumCommonNameColumnName.namename, model.sched_type);
                            var schedule = ErsFactory.ersRequestFactory.GetErsSchedule();

                            schedule.OverwriteWithParameter(model.GetPropertiesAsDictionary());
                            if (schedule.sched_details.HasValue())
                            {
                                string sched_details = schedule.sched_details.ToUpper();
                                schedule.sched_details = (sched_details.Contains("ETA") || sched_details.Contains("HALF DAY")) ? sched_details : schedule.sched_details;
                            }

                            schedule.sched_type = (EnumSchedType?)sched_type_id;

                            repository.Insert(schedule, true);
                        }
                    }
                }
            }
            else
            {
                var batchDetails = ErsFactory.ersRequestFactory.getErsScheduleBatchLogWithDate(DateTime.Today);
                var repository = ErsFactory.ersRequestFactory.GetErsScheduleBatchLogRepository();

                if (batchDetails != null)
                {
                    var old_log = ErsFactory.ersRequestFactory.getErsScheduleBatchLogWithID((int)batchDetails.id);
                    var new_log = ErsFactory.ersRequestFactory.getErsScheduleBatchLogWithParameter(old_log.GetPropertiesAsDictionary());

                    new_log.details = setup.batch_error;
                    new_log.error_details = command.error_messages ?? setup.tampered_csv;
                    new_log.error_exception = null;
                    new_log.status = EnumBatchStatus.Error;

                    repository.Update(old_log, new_log);
                }
                else
                {
                    var entity = ErsFactory.ersRequestFactory.GetErsScheduleBatchLog();

                    entity.error_details = command.error_messages;
                    entity.details = setup.no_csv_file;
                    entity.status = EnumBatchStatus.Error;

                    repository.Insert(entity, true);
                }
            }
            return new CommandResult(true);
        }
    }
}