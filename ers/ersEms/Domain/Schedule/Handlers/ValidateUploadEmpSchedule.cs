﻿using ersEms.Domain.Schedule.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Schedule.Handlers
{
    public class ValidateUploadEmpSchedule : IValidationHandler<IUploadEmpScheduleCommand>
    {
        public IEnumerable<ValidationResult> Validate(IUploadEmpScheduleCommand command)
        {
            int? desknet_id = 0;

            if (command.csv_file.csv_file == null)
            {
                yield return new ValidationResult(ErsResources.GetMessage("empty_csv_file"));
                yield break;
            }
            else
            {
                foreach (var model in command.csv_file.GetValidatedModels(command.chk_find))
                {
                    if (model.desknet_id.HasValue)
                    {
                        desknet_id = model.desknet_id;
                    }

                    model.desknet_id = desknet_id;
                    model.AddInvalidField(command.controller.commandBus.Validate<IUploadEmpScheduleRecordCommand>(model));

                    if (model.sched_type.HasValue())
                    {
                        EnumSchedType sched = (EnumSchedType)ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetIdFromString(EnumCommonNameType.SchedType, EnumCommonNameColumnName.namename, model.sched_type);
                        if (sched == EnumSchedType.VacationLeave && !model.IsValidField("time_end"))
                        {
                            model.RemoveInvalidField("time_end");
                        }
                    }

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "csv_file" });
                        }

                        command.csv_file.MarkRecordAsInvalid(model);
                        continue;
                    }
                }
            }           
        }

    }
}