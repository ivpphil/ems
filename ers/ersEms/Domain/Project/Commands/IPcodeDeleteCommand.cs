﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Project.Commands
{
    public interface IPcodeDeleteCommand:ICommand
    {
        int? id { get; set; }

        string pcode { get; set; }
    }
}
