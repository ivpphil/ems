﻿using ersEms.Models.Project;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;

namespace ersEms.Domain.Project.Commands
{
    public interface IPcodeRegistCommand: ICommand
    {
       IList<pcode_details> pcode_details { get; set; }

    }
}
