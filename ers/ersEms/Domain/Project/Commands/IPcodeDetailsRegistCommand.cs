﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Project.Commands
{
    public interface IPcodeDetailsRegistCommand:ICommand
    {

        int? id { get; set; }

        string pcode { get; set; }

        string pcode_desc { get; set; }
    }
}