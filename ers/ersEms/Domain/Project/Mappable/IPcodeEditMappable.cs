﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersEms.Domain.Project.Mappable
{
    public interface IPcodeEditMappable:IMappable
    {

        int? id { get; set; }
        string pcode { get; set; }
        string pcode_desc { get; set; }

    }
}
