﻿using ersEms.Models.Project;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using System.Collections.Generic;

namespace ersEms.Domain.Project.Mappable
{
    public interface IPcodeListMappable:IMappable
    {
        ErsPagerModel pager { get; }

        string pcode { get; set; }

        string s_pcode { get; set; }

        long recordCount { get; set; }

        IList<pcode_details> pcode_details { get; set; }

        bool deleteFlg { get; set; }

    }
}
