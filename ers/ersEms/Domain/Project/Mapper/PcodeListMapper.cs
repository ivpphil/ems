﻿using ersEms.Domain.Project.Mappable;
using ersEms.Models.Project;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System.Collections.Generic;

namespace ersEms.Domain.Project.Mapper
{
    public class PcodeListMapper:IMapper<IPcodeListMappable>
    {
        public void Map(IPcodeListMappable objMappable)
        {
            this.pcodeList(objMappable);
        }

        internal void pcodeList(IPcodeListMappable objMappable)
        {
            var repo = ErsFactory.ersPcodeFactory.GetErsPcodeRepository();
            var cri = ErsFactory.ersPcodeFactory.GetErsPcodeCriteria();

            if(!string.IsNullOrEmpty(objMappable.s_pcode))
            {
                cri.pcode = objMappable.s_pcode;                
            }

            objMappable.recordCount = repo.GetRecordCount(cri);
            cri.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("no_pcode_err"));
                return;
            }

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(cri);
            }            

            var pcode_list = repo.Find(cri);
            
            pcode_details pcode_details = new pcode_details();
            objMappable.pcode_details = new List<pcode_details>();

            foreach(var list in pcode_list)
            {
                pcode_details = new pcode_details();

                pcode_details.pcode = list.pcode;
                pcode_details.pcode_desc = list.pcode_desc;
                pcode_details.id = list.id;
                pcode_details.deleteFlg = CheckDeletePcode(list.pcode);
                objMappable.pcode_details.Add(pcode_details);
            }

        }

        public bool CheckDeletePcode(string pcode)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsDreportRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsDreportCriteria();

            cri.pcode = pcode;

            long dreport_count = repo.GetRecordCount(cri);

            if (dreport_count > 1)
            {
                return false;
            }

            return true;
        }
    }
}