﻿using ersEms.Domain.Project.Mappable;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersEms.Domain.Project.Mapper
{
    public class PcodeEditMapper:IMapper<IPcodeEditMappable>
    {
        public void Map(IPcodeEditMappable objMappable)
        {

            var repo = ErsFactory.ersPcodeFactory.GetErsPcodeRepository();
            var cri = ErsFactory.ersPcodeFactory.GetErsPcodeCriteria();

            cri.pcode = objMappable.pcode;

            var pcodes = repo.FindSingle(cri);
            if (pcodes == null)
            {
                throw new ErsException(ErsResources.GetMessage("no_pcode_err"));
            }
            else
            {
                objMappable.OverwriteWithParameter(pcodes.GetPropertiesAsDictionary());
            }

        }
    }
}