﻿using ersEms.Domain.Project.Commands;
using ersEms.Models.Project;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Project.Handlers
{
    public class ValidatePcodeRegist : IValidationHandler<IPcodeRegistCommand>
    {

        public IEnumerable<ValidationResult> Validate(IPcodeRegistCommand command)
        {
            if (command.pcode_details.Count > 0)
            {
                if (command.pcode_details.Count > 10)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("RegisterLimit",10));
                }
                else
                {
                    var list = new List<string>();

                    foreach (pcode_details pcode_details in command.pcode_details)
                    {
                        pcode_details.lineNumber = command.pcode_details.IndexOf(pcode_details) + 1;

                        pcode_details.AddInvalidField(command.controller.commandBus.Validate<IPcodeDetailsRegistCommand>(pcode_details));

                        if (list.Contains(pcode_details.pcode))
                        {
                            pcode_details.AddInvalidField(new ValidationResult(ErsResources.GetMessage("10103a", "PCODE", pcode_details.pcode), new[] { "pcode" }));
                        }
                        else
                        {
                            list.Add(pcode_details.pcode);
                        }

                        if (!pcode_details.IsValid)
                        {
                            foreach (var errorMessage in pcode_details.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "pcode_details" });
                            }
                        }
                    }
                }              
            }
            else
            {
               yield return new  ValidationResult(ErsResources.GetMessage("10000", "PCODE and PCODE Description"));
            }       
        }
    }
}