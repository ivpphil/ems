﻿using ersEms.Domain.Project.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Project.Handlers
{
    public class PcodeDeleteHandler:ICommandHandler<IPcodeDeleteCommand>
    {
        public ICommandResult Submit(IPcodeDeleteCommand command)
        {
            var repo = ErsFactory.ersPcodeFactory.GetErsPcodeRepository();
            var cri = ErsFactory.ersPcodeFactory.GetErsPcodeCriteria();
            cri.id = command.id;
            cri.pcode = command.pcode;
            repo.Delete(cri);
            return new CommandResult(true);
        }
    }
}