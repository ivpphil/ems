﻿using ersEms.Domain.Project.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Project.Handlers
{
    public class ValidatePcodeDelete : IValidationHandler<IPcodeDeleteCommand>
    {
        public IEnumerable<ValidationResult>Validate(IPcodeDeleteCommand command)
        {
            yield return command.CheckRequired("pcode");
            yield return command.CheckRequired("id");

            yield return ErsFactory.ersPcodeFactory.GetErsStgyPcode().CheckIfPcodeExist((int)command.id, command.pcode);
            yield return ErsFactory.ersPcodeFactory.GetErsStgyPcode().CheckUsedPcode(command.pcode);
        }
    }
}