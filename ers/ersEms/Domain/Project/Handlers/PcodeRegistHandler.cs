﻿using ersEms.Domain.Project.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Project.Handlers
{
    public class PcodeRegistHandler : ICommandHandler<IPcodeRegistCommand>
    {
        public ICommandResult Submit(IPcodeRegistCommand command)
        {
            var entity = ErsFactory.ersPcodeFactory.GetErsPcode();

            foreach(var details in command.pcode_details)
            {
                entity.OverwriteWithParameter(details.GetPropertiesAsDictionary());
                ErsFactory.ersPcodeFactory.GetErsPcodeRepository().Insert(entity, true); 
            }

            return new CommandResult(true);
        }
    }
}