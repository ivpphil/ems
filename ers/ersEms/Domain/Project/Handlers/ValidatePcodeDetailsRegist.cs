﻿using ersEms.Domain.Project.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Project.Handlers
{
    public class ValidatePcodeDetailsRegist:IValidationHandler<IPcodeDetailsRegistCommand>
    {
        public IEnumerable<ValidationResult> Validate(IPcodeDetailsRegistCommand command)
        {
            yield return command.CheckRequired("pcode");
            yield return command.CheckRequired("pcode_desc");

            yield return ErsFactory.ersPcodeFactory.GetErsStgyPcode().CheckDuplicatePcode(command.pcode);
        }
    }
}