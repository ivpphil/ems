﻿using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ersEms.Domain.Login.Commands
{
  public interface IChangePassCommand:ICommand
    {

        ErsEmployee employee { get; set; }

        bool submit_pass { get; set; }

        string password { get; set; }

        string password_confirm { get; set; }

        string enc_mcode { get; set; }

        string enc_ransu { get; set; }

        string mcode { get; set; }

        string ransu { get; set; }
    }
}
