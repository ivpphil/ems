﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ersEms.Domain.Login.Commands
{
 public interface ILoginCommand :ICommand
    {
        string emp_no { get; set; }
        string email { get; set; }
        string password { get; set; }

        bool email_ck { get; set; }
        string mcode { get; set; }
        string fname { get; set; }
        string lname { get; set; }
    }
}
