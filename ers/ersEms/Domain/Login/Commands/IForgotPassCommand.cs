﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Login.Commands
{
    public interface IForgotPassCommand:ICommand
    {
        string mcode { get; set; }

        string email { get; set; }

        string lname { get; set; }

        string fname { get; set; }

        string changeUrl { get; set; }
    }
}