﻿using ersEms.Domain.Login.Commands;
using ersEms.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Login.Handlers
{
    public class LoginHandler:ICommandHandler<ILoginCommand>
    {
        public ICommandResult Submit(ILoginCommand command)
        {
            this.LoginAction(command);

            return new CommandResult(true);
        }

        internal void LoginAction(ILoginCommand command)
        {

            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            cri.email = command.email;
            cri.password = command.password;

            var result  = repo.FindSingle(cri);

            if(result != null)
            {
                ErsContext.sessionState.ClearLoginData();
                var objSession = ((ISession)ErsContext.sessionState);
                
                objSession.getNewSSLransu(result.mcode);

                var emp = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(result.emp_no);

                var nickName = result.fname + " " + result.lname;
                ErsContext.sessionState.Add(ErsSessionState.nickNameKey, HttpUtility.UrlEncode(nickName, HttpContext.Current.Response.ContentEncoding));
                var objc = new OtherCookie();
                objc.SetCookieEmail("firstname", result.fname, ErsFactory.ersUtilityFactory.getSetup().cookieTimer);
                objc.SetCookieEmail("lastname", result.lname, ErsFactory.ersUtilityFactory.getSetup().cookieTimer);

                if (command.email_ck)
                {
                    objc.SetCookieEmail("login_email", command.email, ErsFactory.ersUtilityFactory.getSetup().cookieTimer);

                }
                else
                {
                    objc.DeleteCookie("login_email");
                }

            }
        }
    }
}