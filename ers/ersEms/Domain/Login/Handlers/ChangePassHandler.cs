﻿using ersEms.Domain.Login.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Login.Handlers
{
    public class ChangePassHandler:ICommandHandler<IChangePassCommand>
    {

        public ICommandResult Submit(IChangePassCommand command)
        {

            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var new_emp = ErsFactory.ersEmployeeFactory.GetErsEmployee();

            new_emp.OverwriteWithModel(command.employee);

            new_emp.password = command.password;

            repo.Update(command.employee, new_emp);

            //var changePassService = ErsFactory.ersMemberFactory.GetErsPasswordReminderService();

            //changePassService.InsertPassRansu(command.ransu, command.mcode);

            return new CommandResult(true);
        }
    }
}