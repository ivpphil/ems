﻿using ersEms.Domain.Login.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Login.Handlers
{
    public class ValidateChangePass:IValidationHandler<IChangePassCommand>
    {

        public IEnumerable<ValidationResult> Validate(IChangePassCommand command)
        {
            yield return command.CheckRequired("enc_mcode");
            yield return command.CheckRequired("enc_ransu");
            yield return command.CheckRequired("password");

            if (command.enc_mcode == null || command.enc_ransu == null)
                yield break;


            var encObj = ErsFactory.ersUtilityFactory.getErsEncryption();
            var mcode = encObj.HexDecode(command.enc_mcode);
            var ransu = encObj.HexDecode(command.enc_ransu);

            var passrimService = ErsFactory.ersMemberFactory.GetErsPasswordReminderService();


            if (!passrimService.isValidPassRimRansu(ransu))
            {
                throw new ErsException("10042");
            }

            if (!passrimService.isValidRansu(ransu))
            {
                throw new ErsException("10210");
            }
            ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckEmpNo(mcode,true);

            if (command.password != null)
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckPasswordConfirm(command.password, command.password_confirm);
            }
        }
    }
}