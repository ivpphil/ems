﻿using ersEms.Domain.Login.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Login.Handlers
{
    public class ValidateForgotPass:IValidationHandler<IForgotPassCommand>
    {
        public IEnumerable<ValidationResult>Validate(IForgotPassCommand command)
        {
            yield return command.CheckRequired("email");

            if (!string.IsNullOrEmpty(command.email)) 
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckEmail(command.email);
            }
        }
    }
}