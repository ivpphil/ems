﻿using ersEms.Domain.Login.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Login.Handlers
{
    public class ValidateLogin : IValidationHandler<ILoginCommand>
    {
        public IEnumerable<ValidationResult> Validate(ILoginCommand command)
        {
            yield return command.CheckRequired("email");
            yield return command.CheckRequired("password");


            if(command.IsValidField("email","password"))
            {
                var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
                var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

                cri.email = command.email;
                cri.password = command.password;


                if(repo.GetRecordCount(cri) == 0)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("InvalidLogin"));
                }
                else
                {
                    var result = repo.FindSingle(cri);
                    if(result.status == EnumEmpStatus.Resigned || result.status == EnumEmpStatus.Terminated)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("RestrictedLogin"));
                    }
                }
            }
        }
    }
}