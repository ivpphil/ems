﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ersEms.Domain.Login.Mappables
{
  public  interface IForgotPassMappable:IMappable
    {
        string mcode { get; set; }

        string email { get; set; }

        string lname { get; set; }

        string fname { get; set; }

        string changeUrl { get; set; }

        EnumMformat? mformat { get; set; }
    }
}
