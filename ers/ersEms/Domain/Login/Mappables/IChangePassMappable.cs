﻿using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ersEms.Domain.Login.Mappables
{
    public interface IChangePassMappable:IMappable
    {
        ErsEmployee employee { get; set; }

        string enc_mcode { get; set; }

        string enc_ransu { get; set; }

        string mcode { get; set; }

        string ransu { get; set; }

    }
}
