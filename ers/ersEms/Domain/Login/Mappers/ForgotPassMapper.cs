﻿using ersEms.Domain.Login.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Login.Mappers
{
    public class ForgotPassMapper:IMapper<IForgotPassMappable>
    {

        public void Map(IForgotPassMappable objMappable)
        {

            var member = GetMcodeWithEmail(objMappable.email);

            var mcode = member.mcode;
            objMappable.mcode = mcode;
            objMappable.mformat = EnumMformat.PC;
            objMappable.lname = member.lname;
            objMappable.fname = member.fname;

            ErsPasswodReminderService forgetPassService = ErsFactory.ersMemberFactory.GetErsPasswordReminderService();
            var ransu = forgetPassService.MakePassRansu(mcode);

            var encObj = ErsFactory.ersUtilityFactory.getErsEncryption();
            var encMcode = encObj.HexEncode(mcode);
            var encRansu = encObj.HexEncode(ransu);

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            string param = "?enc_ransu=" + encRansu + "&enc_mcode=" + encMcode;
            objMappable.changeUrl = setup.sec_url + setup.emp_change_url + param;

        }




        private ErsEmployee GetMcodeWithEmail(string email)
        {

            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();


            cri.email = email;

            var list = repo.Find(cri);

            if(list.Count != 1)
            {
                return null;
            }

            return list.First();
        }
    }
}