﻿using ersEms.Domain.Login.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Login.Mappers
{
    public class ChangePassMapper:IMapper<IChangePassMappable>
    {

        public void Map(IChangePassMappable objMappable)
        {

            var encObj = ErsFactory.ersUtilityFactory.getErsEncryption();
            objMappable.mcode = encObj.HexDecode(objMappable.enc_mcode);
            objMappable.ransu = encObj.HexDecode(objMappable.enc_ransu);
            var emp = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(objMappable.mcode);
            if (emp != null && emp.status == EnumEmpStatus.Employed)
            {
                objMappable.employee = emp;
            }
            
        }
    }
}