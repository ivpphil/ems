﻿using ersEms.Domain.Home.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Home.Mappers
{
    public class HomeDailyReportDataMapper : IMapper<IHomeDailyReportDataMappable>
    {
        public void Map(IHomeDailyReportDataMappable objMappable)
        {
            GetDailyReportData(objMappable);
        }

        internal void GetDailyReportData(IHomeDailyReportDataMappable objMappable)
        {
            var dailyReportSpec = ErsFactory.ersEmployeeFactory.GetDailyReportsDataForGraphsSpecification();
            var criteria = ErsFactory.ersEmployeeFactory.GetErsDreportCriteria();

            //dailyReportSpec.isPersonal = false;
           dailyReportSpec.report_summary = objMappable.report_summary;

            var dailyReport = dailyReportSpec.GetSearchData(criteria);

            List<Dictionary<string, object>> daily_report_graph_data = new List<Dictionary<string, object>>();
            double totalHours = Convert.ToDouble(dailyReport[dailyReport.Count() - 1]["total_hours"]);
            var projectSpans = dailyReport;
            projectSpans.Remove(dailyReport.Last());

            foreach (var details in projectSpans)
            {
                string y;
                string percentage = ((Convert.ToDouble(details["total_hours"]) / totalHours) * 100.0).ToString();

                y = percentage;  // For Whole number
                if (percentage.Contains('.'))
                {
                    string[] pSplit = percentage.Split('.');
                    if (pSplit.Length > 1)
                    {
                        y = String.Format("{0}.{1}", pSplit[0], pSplit[1].Substring(0, 2));  // Get 2 Decimal place only
                    }
                }


                Dictionary<string, object> data = new Dictionary<string, object>();
                data.Add("y", y);
                data.Add("legendText", String.Format("{0} - {1}%", details["proj_desc"], y));

                daily_report_graph_data.Add(data);
            }

            objMappable.daily_report_list = daily_report_graph_data;
        }
    }
}