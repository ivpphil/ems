﻿using ersEms.Domain.Home.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Home.Mappers
{
    public class EmployeeBirthdayDataMapper : IMapper<IEmployeeBirthdayDataMappable>
    {
        public void Map(IEmployeeBirthdayDataMappable objMappable)
        {
            GetBirthdayList(objMappable);
            //ListOfTimeInOut(objMappable);
        }

        private void ListOfTimeInOut(IEmployeeBirthdayDataMappable objMappable)
        {
            var rep = ErsFactory.ersMDBFactory.GetErsMdbCheckInOutRepository();
            var cri = ErsFactory.ersMDBFactory.GetErsMdbCheckInOutCriteria();
            cri.Year = DateTime.Now.Year;
            cri.Month = 1;
            cri.Day = 29;
            cri.GroupByName();
            cri.GroupByFormatCHECKTIME();
            objMappable.time_in_out_list = rep.Find(cri);
        }
        
        private void GetBirthdayList(IEmployeeBirthdayDataMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var rep = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
            cri.getBirthdayMonthRange();

            var employee = new Dictionary<string, object>();
            var list = rep.Find(cri);
            var bdayList = new List<Dictionary<string, object>>();
            foreach (var emp in list)
            {
                employee = emp.GetPropertiesAsDictionary();

                if (employee["image_file"] == null)
                {
                    employee["image_path"] = String.Format("{0}/images/{1}", setup.pc_sec_url, setup.default_name_image);
                }
                else if (File.Exists(setup.image_directory + employee["emp_no"].ToString() + "\\" + employee["image_file"].ToString() + ""))
                {
                    employee["image_path"] = String.Format("{0}/images/{1}/{2}", setup.pc_sec_url, employee["emp_no"].ToString(), employee["image_file"].ToString());
                }
                else
                {
                    employee["image_path"] = String.Format("{0}/images/{1}", setup.pc_sec_url, setup.default_name_image);
                }
                bdayList.Add(employee);
            }
            objMappable.birthday_list = bdayList;
        }
    }
}