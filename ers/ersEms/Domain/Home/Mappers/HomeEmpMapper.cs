﻿using ersEms.Domain.Home.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace ersEms.Domain.Home.Mappers
{
    public class HomeEmpMapper : IMapper<IHomeEmpMappable>
    {
        public void Map(IHomeEmpMappable objMappable)
        {
            GetEmployeeFullnameAndImage(objMappable);
            GetProjectList(objMappable);
        }

        internal void GetEmployeeFullnameAndImage(IHomeEmpMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var emp_details = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(objMappable.emp_no);


            objMappable.fullname = ErsFactory.ersEmployeeFactory.GetFormatFnameAndLnameStgy().FormatFnameAndLname(emp_details.fname.ToString(), emp_details.lname.ToString());

            if (emp_details.image_file == null)
            {
                objMappable.image_path = String.Format("{0}/images/{1}", setup.pc_sec_url, setup.default_name_image);
            }
            else if (File.Exists(setup.image_directory + emp_details.emp_no.ToString() + "\\" + emp_details.image_file.ToString() + ""))
            {
                objMappable.image_path = String.Format("{0}/images/{1}/{2}", setup.pc_sec_url, emp_details.emp_no, emp_details.image_file);
            }
            else
            {
                objMappable.image_path = String.Format("{0}/images/{1}", setup.pc_sec_url, setup.default_name_image);
            }

        }

        internal void GetProjectList(IHomeEmpMappable objMappable)
        {
            var spec = ErsFactory.ersEmployeeFactory.GetEmployeeProjectSpecification();
            var cri = ErsFactory.ersEmployeeFactory.GetErsDreportCriteria();

            cri.emp_no = objMappable.emp_no;
            cri.ignorePcodes();
            cri.setActivePcode();
            cri.SetGroupByPcode();
            cri.SetGroupByPcodeDescription();
            cri.SetOrderByProjReport_date(Criteria.OrderBy.ORDER_BY_DESC);

            var list = spec.GetSearchData(cri);

            objMappable.projectCount = list.Count();
            objMappable.projectList = list;
            
        }



    }
}