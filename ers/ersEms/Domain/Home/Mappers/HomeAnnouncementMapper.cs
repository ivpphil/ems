﻿using ersEms.Domain.Home.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace ersEms.Domain.Home.Mappers
{
    public class HomeAnnouncementMapper : IMapper<IHomeAnnouncementMappable>
    {

        public void Map(IHomeAnnouncementMappable objMappable)
        {
            GetAnnouncement(objMappable);
        }

        internal void GetAnnouncement(IHomeAnnouncementMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var announcementSpec = ErsFactory.ersEmployeeFactory.GetAnnouncementSpecification();
            var criteria = ErsFactory.ersEmployeeFactory.getAnnouncementCriteria();

            criteria.not_status = EnumAnnouncementStatus.Deleted;


            objMappable.recordCount = announcementSpec.GetCountData(criteria);
            objMappable.totalPager = Convert.ToInt16(Math.Ceiling((double)objMappable.recordCount / (double)setup.AnnouncementListItemNumberOnPage));

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            }

            criteria.LatestPost(Criteria.OrderBy.ORDER_BY_DESC);
            var announcementList = announcementSpec.GetSearchData(criteria);


            if (announcementList.Count() > 0)
            {
                foreach (var announcement in announcementList)
                {
                    announcement["fullname"] = ErsFactory.ersEmployeeFactory.GetFormatFnameAndLnameStgy().FormatFnameAndLname(announcement["fname"].ToString().ToLower(), announcement["lname"].ToString().ToLower());

                    if (announcement["image_file"] == null)
                    {
                        announcement["image_path"] = String.Format("{0}/images/{1}", setup.pc_sec_url, setup.default_name_image);
                    }
                    else if (File.Exists(setup.image_directory + announcement["emp_no"].ToString() + "\\" + announcement["image_file"].ToString() + ""))
                    {
                        announcement["image_path"] = String.Format("{0}/images/{1}/{2}", setup.pc_sec_url, announcement["emp_no"].ToString(), announcement["image_file"].ToString());
                    }
                    else
                    {
                        announcement["image_path"] = String.Format("{0}/images/{1}", setup.pc_sec_url, setup.default_name_image);
                    }

                    announcement["in_time"] = Convert.ToDateTime(announcement["in_time"]).ToString(@"yyyy/MM/dd hh:mm:ss tt", new CultureInfo("en-US"));
                    announcement["u_time"] = Convert.ToDateTime(announcement["u_time"]).ToString(@"yyyy/MM/dd hh:mm:ss tt", new CultureInfo("en-US"));
                }
            }

            objMappable.announcementList = announcementList;
        }
    }
}