﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using System.Collections.Generic;

namespace ersEms.Domain.Home.Mappables
{
    public interface IHomeEmpMappable : IMappable
    {
        string emp_no { get; }
        string fullname { get; set; }
        string image_path { get; set; }

        List<Dictionary<string,object>> projectList { get; set; }
        int projectCount { get; set; }
    }
}