﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Home.Mappables
{
    public interface IEmployeeBirthdayDataMappable : IMappable
    {
        bool isDefaultLoad { get; set; }

        bool loadGraph { get; set; }

        List<Dictionary<string, object>> birthday_list { get; set; }

        List<Dictionary<string, object>> time_in_out_list { get; set; }

    }
}