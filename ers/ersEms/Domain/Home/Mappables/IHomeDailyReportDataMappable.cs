﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Home.Mappables
{
    public interface IHomeDailyReportDataMappable : IMappable
    {
        bool isDefaultLoad { get; set; }

        bool loadGraph { get; set; }

        EnumReportSummary? report_summary { get; set; }

        List<Dictionary<string,object>> daily_report_list { get; set; }
    }
}