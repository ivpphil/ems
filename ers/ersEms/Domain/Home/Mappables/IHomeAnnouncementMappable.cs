﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using System.Collections.Generic;

namespace ersEms.Domain.Home.Mappables
{
    public interface IHomeAnnouncementMappable : IMappable
    {
        ErsPagerModel pager { get; }
        int totalPager { get; set; }

        long recordCount { get; set; }

        IList<Dictionary<string, object>> announcementList { get; set; }
    }
}