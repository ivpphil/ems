﻿using ersEms.Domain.Home.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Home.Handlers
{
    public class ValidateHomeAnnouncement : IValidationHandler<IHomeAnnouncementCommand>
    {

        public IEnumerable<ValidationResult> Validate(IHomeAnnouncementCommand command)
        {
            var empDetails = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(command.emp_no);


            yield return command.CheckRequired("announcementCmdType");


            if (command.announcementCmdType.HasValue)
            {
                yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("announcementCmdType", EnumCommonNameType.AnnouncementCmdType, (int?)command.announcementCmdType);

                switch (command.announcementCmdType)
                {
                    case EnumAnnouncementCmdtype.Create:

                        yield return command.CheckRequired("news");
                        break;

                    case EnumAnnouncementCmdtype.Modify:

                        yield return command.CheckRequired("id");
                        if (command.id.HasValue)
                        {
                            var announcementDetails = ErsFactory.ersEmployeeFactory.getAnnouncementWithID((int)command.id);
                            if (announcementDetails == null)
                            {
                                yield return new ValidationResult(ErsResources.GetMessage("NotExist", "Announcement"), new [] { "id" });
                            }
                            else
                            {
                                yield return ErsFactory.ersEmployeeFactory.GetCheckOwnershipOfAnnouncementStgy().Validate(command.emp_no, (int)command.id);
                            }
                        }
                        yield return command.CheckRequired("news");
                        break;

                    case EnumAnnouncementCmdtype.Delete:

                        yield return command.CheckRequired("id");
                        if (command.id.HasValue)
                        {
                            var announcementDetails = ErsFactory.ersEmployeeFactory.getAnnouncementWithID((int)command.id);
                            if (announcementDetails == null)
                            {
                                yield return new ValidationResult(ErsResources.GetMessage("NotExist", "Announcement"), new[] { "id" });
                            }
                            else
                            {
                                if (empDetails.position != EnumPosition.Administrator)
                                {
                                    yield return ErsFactory.ersEmployeeFactory.GetCheckOwnershipOfAnnouncementStgy().Validate(command.emp_no, (int)command.id);
                                }
                            }
                        }
                        break;
                }
            }
        }

    }
}