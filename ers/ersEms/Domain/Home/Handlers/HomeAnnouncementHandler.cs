﻿using ersEms.Domain.Home.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersEms.Domain.Home.Handlers
{
    public class HomeAnnouncementHandler : ICommandHandler<IHomeAnnouncementCommand>
    {
        public ICommandResult Submit(IHomeAnnouncementCommand command)
        {
            if (command.announcementCmdType == EnumAnnouncementCmdtype.Create)
            {
                CreateAnnouncement(command);
            }
            else
            {
                ModifyOrDeleteAnnouncement(command);
            }

            return new CommandResult(true);
        }


        internal void CreateAnnouncement(IHomeAnnouncementCommand command)
        {
            var repository = ErsFactory.ersEmployeeFactory.getAnnouncementRepository();
            var entity = ErsFactory.ersEmployeeFactory.getAnnouncement();

            entity.OverwriteWithParameter(command.GetPropertiesAsDictionary());
            entity.in_time = DateTime.Now;

            repository.Insert(entity, true);
        }

        internal void ModifyOrDeleteAnnouncement(IHomeAnnouncementCommand command)
        {
            var repository = ErsFactory.ersEmployeeFactory.getAnnouncementRepository();

            var old_obj = ErsFactory.ersEmployeeFactory.getAnnouncementWithID((int)command.id);
            var new_obj = ErsFactory.ersEmployeeFactory.getAnnouncementWithParameter(old_obj.GetPropertiesAsDictionary());

            if (command.announcementCmdType == EnumAnnouncementCmdtype.Modify)
            {
                new_obj.news = command.news;
                new_obj.status = EnumAnnouncementStatus.Edited;
            }
            else
            {
                new_obj.status = EnumAnnouncementStatus.Deleted;
            }

            new_obj.u_time = DateTime.Now;   
            repository.Update(old_obj, new_obj);
        }

    }
}