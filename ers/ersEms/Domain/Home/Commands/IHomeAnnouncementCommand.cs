﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersEms.Domain.Home.Commands
{
    public interface IHomeAnnouncementCommand : ICommand
    {
        string emp_no { get; }

        int? id { get; set; }
        string news { get; set; }
        EnumAnnouncementCmdtype? announcementCmdType { get; set; }

    }
}