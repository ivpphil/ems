﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using System.Collections.Generic;

namespace ersEms.Domain.api.Mappables
{
    public interface ITeamManagementApiMappable
        :IMappable
    {        
        List<Dictionary<string, object>> non_team_list { get; set; }

        List<Dictionary<string, object>> list { get; set; }

        ErsPagerModel pager { get; set; }

        long recordCount { get; set; }

        ErsPagerModel non_team_pager { get; set; }

        long non_team_recordCount { get; set; }

        //string s_lname { get; set; }

        //string s_email { get; set; }

        //string s_team { get; set; }

        //string s_position { get; set; }

        //string s_keyword { get; }

        //EnumEmpSearch? searchField { get; set; }

        //EnumTeam? SearchTeam { get; set; }

        //EnumPosition? SearchPos { get; set; }

        EnumPosition? emp_pos { get; set; }

        //bool fromEmpList { get; set; }

        //bool MemberSearch { get; set; }

        //string user_emp_no { get; set; }
    }
}