﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.api.Mappables
{
    public interface IEmpDetailsMappable : IMappable
    {
         string emp_no { get; set; }

         ErsEmployee employee { get; set; }

         int vacation_leave { get; set; }

         int sick_leave { get; set; }
    }
}