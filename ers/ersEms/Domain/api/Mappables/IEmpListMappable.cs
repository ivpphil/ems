﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.api.Mappables
{
    public interface IEmpListMappable : IMappable
    {
        string url { get; set; }

        string s_keyword { get; set; }

        EnumEmpSearch? emp_search_by { get; set; }

        EnumTeam? team_search { get; set; }

        int? job_title_search { get; set; }
        
        List<Dictionary<string, object>> list { get; set; }
    }
}