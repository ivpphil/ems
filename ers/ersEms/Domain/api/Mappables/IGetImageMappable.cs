﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.api.Mappables
{
    public interface IGetImageMappable : IMappable
    {
        string emp_no { get; set; }

        string lname { get; set; }

        string fname { get; set; }

        bool hasImage { get; set; }

        string fullname { get; set; }
    }
}