﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.api.Mappables
{
    public interface IRequestScheduleApiMappable
        : IMappable
    {
        int desknet_id { get; set; }
        string s_date_start { get; set; }
        string shift { get; set; }
    }
}