﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.api.Mappables
{
    public interface IErrorHeaderMappable:IMappable
    {
        int pos { get; set; }
        string islogin { get; set; }
    }
}