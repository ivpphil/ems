﻿using ersEms.Domain.api.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;

namespace ersEms.Domain.api.Mappers
{
    public class RequestScheduleApiMapper: IMapper<IRequestScheduleApiMappable>
    {
        public void Map(IRequestScheduleApiMappable objMappable)
        {
            var emp_no = ErsContext.sessionState.Get("mcode");
            if (string.IsNullOrEmpty(emp_no))
            {
                return;
            }

            var emp_record = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(emp_no);
            if (emp_record == null)
            {
                return;
            }

            var sched_cri = ErsFactory.ersRequestFactory.getErsScheduleWithDesknetIDAndDate(emp_record.desknet_id, Convert.ToDateTime(objMappable.s_date_start));
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            if (sched_cri!=null)
            {
                if (sched_cri.time_start.HasValue && sched_cri.time_end.HasValue)
                {
                    objMappable.shift = sched_cri.time_start.Value.ToString("hh:mm tt") + "-" + sched_cri.time_end.Value.ToString("hh:mm tt");
                }
            }
            if((sched_cri==null || !sched_cri.time_start.HasValue || !sched_cri.time_end.HasValue) && (Convert.ToDateTime(objMappable.s_date_start).DayOfWeek != DayOfWeek.Saturday && Convert.ToDateTime(objMappable.s_date_start).DayOfWeek != DayOfWeek.Sunday))
            {
                objMappable.shift = setup.def_time_start + "-" + setup.def_time_end;
            }
        }
    }
}