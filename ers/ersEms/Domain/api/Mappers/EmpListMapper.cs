﻿using ersEms.Domain.api.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ersEms.Domain.api.Mappers
{
    public class EmpListMapper : IMapper<IEmpListMappable>
    {
        public void Map(IEmpListMappable objMappable)
        {
            var list = new List<Dictionary<string, object>>();
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var crit = GetSearchCriteria(objMappable);

            if (repo.GetRecordCount(crit) > 0)
            {
                crit.SetOrderByEmployeeNo(Criteria.OrderBy.ORDER_BY_ASC);
                crit.SetorderByStatus(Criteria.OrderBy.ORDER_BY_ASC);
                foreach (var employee in repo.Find(crit))
                {
                    var empDictionary = new Dictionary<string, object>();
                    var path = setup.image_directory + employee.emp_no + "\\" + employee.image_file;
                    empDictionary = employee.GetCurrentModelPropertiesAsDictionary();
                    empDictionary.Add("full_name", string.Join(" ", employee.fname, employee.lname));
                    if (File.Exists(path))
                    {
                        empDictionary.Add("image_path", objMappable.url + "images/" + employee.emp_no + "/" + employee.image_file);
                    }
                    else
                    {
                        empDictionary.Add("image_path", objMappable.url + "images/img_avatar1.png");
                    }
                    var leave_bal = ErsFactory.ersRequestFactory.GetErsLeaveBalanceWithEmpNo(employee.emp_no);

                    empDictionary.Add("sick_leave", leave_bal?.total_sl);
                    empDictionary.Add("vacation_leave", leave_bal?.total_vl);

                    list.Add(empDictionary);
                }
                objMappable.list = list;


            }
        }
        protected ErsEmployeeCriteria GetSearchCriteria(IEmpListMappable objMappable)
        {
            var criteria = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            if (objMappable.emp_search_by != null)
            {
                switch (objMappable.emp_search_by)
                {
                    case EnumEmpSearch.EmpNo:
                        criteria.emp_no = objMappable.s_keyword;
                        break;
                    case EnumEmpSearch.EmpEmail:
                        criteria.email_like = objMappable.s_keyword;
                        break;
                    case EnumEmpSearch.FirstName:
                        criteria.lname_like = objMappable.s_keyword;
                        break;
                    case EnumEmpSearch.LastName:
                        criteria.lname_like = objMappable.s_keyword;
                        break;

                }
            }
            else
            {
                if (objMappable.s_keyword.HasValue())
                {
                    criteria.SearchCriteria(objMappable.s_keyword);
                }
            }

            if (objMappable.team_search != null)
            {
                criteria.team = objMappable.team_search;
            }

            if (objMappable.job_title_search.HasValue)
            {
                criteria.job_title = objMappable.job_title_search;
            }
            return criteria;
        }
    }
}