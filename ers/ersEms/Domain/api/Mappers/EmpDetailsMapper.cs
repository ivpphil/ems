﻿using ersEms.Domain.api.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ersEms.Domain.api.Mappers
{
    public class EmpDetailsMapper : IMapper<IEmpDetailsMappable>
    {
        public void Map(IEmpDetailsMappable objMappable)
        {
            var employee = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(objMappable.emp_no);
            if (employee != null)
            {
                objMappable.employee = employee;
            }
        }

    }
}