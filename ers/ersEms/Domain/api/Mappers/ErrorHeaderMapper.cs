﻿using ersEms.Domain.api.Mappables;
using ersEms.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.api.Mappers
{
    public class ErrorHeaderMapper : IMapper<IErrorHeaderMappable>
    {
        public void Map(IErrorHeaderMappable objMappable)
        {
            var objc = new OtherCookie();
            objMappable.pos = 0;
            var positionString = objc.GetCoookieEmail("position");
            objMappable.islogin = objc.GetCoookieEmail("islogin");
            if (positionString.HasValue())
            {
                objMappable.pos = (int)Enum.Parse(typeof(EnumPosition), positionString);
            }

        }
    }
}