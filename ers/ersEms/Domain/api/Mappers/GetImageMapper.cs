﻿using ersEms.Domain.api.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersEms.Domain.api.Mappers
{
    public class GetImageMapper :IMapper<IGetImageMappable>
    {
        public void Map(IGetImageMappable objMappable)
        {
            if (objMappable.emp_no == null)
            {
                var emp_no = ErsContext.sessionState.Get("mcode");
                var detail = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(emp_no);
                objMappable.emp_no = detail.emp_no;
                objMappable.lname = detail.lname;
                objMappable.fname = detail.fname;
                objMappable.fullname = detail.fname + " " + detail.lname;

            }
            else
            {
                objMappable.lname = objMappable.lname;
                objMappable.fname = objMappable.fname;
                objMappable.fullname = objMappable.fname + " " + objMappable.lname;
            }
            objMappable.hasImage = ErsFactory.ersEmployeeFactory.CheckEmpImageExist(objMappable.emp_no);
        }
    }
}