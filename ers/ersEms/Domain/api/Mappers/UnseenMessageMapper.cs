﻿using ersEms.Domain.api.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.api.Mappers
{
    public class UnseenMessageMapper : IMapper<IUnseenMessageMappable>
    {
        public void Map(IUnseenMessageMappable objMappable)
        {
            objMappable.unseen_message_count = 0;
            var repo = ErsFactory.ersEmployeeFactory.GetEmployeeThreadSpecification();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadCriteria();
            var emp_no = ErsContext.sessionState.Get("mcode");

            if (emp_no.HasValue())
            {
                cri.sender_or_recipient = emp_no;
            }
            var list = repo.GetSearchData(cri);
            foreach (var item in list)
            {
                int unseen;
                if (emp_no == Convert.ToString(item["sender_emp_no"]))
                {
                    if (int.TryParse(Convert.ToString(item["recipient_unseen_cnt"]), out unseen))
                    {
                        objMappable.unseen_message_count += unseen;
                    }
                }
                else if (emp_no == Convert.ToString(item["recipient_emp_no"]))
                {
                    if (int.TryParse(Convert.ToString(item["sender_unseen_cnt"]), out unseen))
                    {
                        objMappable.unseen_message_count += unseen;
                    }
                }
            }
        }
    }
}