﻿using ers.jp.co.ivp.ers.employee;
using ersEms.Domain.Report.Mappables;
using ersEms.Models.Report;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ersEms.Domain.Report.Mappers
{
    public class DReportDateRangeCSVMapper : DReportSearchMapper, IMapper<IDReportDateRangeCSVMappable>
    {
        public void Map(IDReportDateRangeCSVMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }

        internal void CreateCsvFile(IDReportDateRangeCSVMappable objMappable)
        {
            objMappable.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            
            var repo = ErsFactory.ersEmployeeFactory.GetReportSpecification();
            var cri = this.GetErsDreportCriteria(objMappable);
            IEnumerable<string> repCodeList = new List<string>();

            if (objMappable.emp_pos == EnumPosition.Member)
            {
                cri.emp_no = ErsContext.sessionState.Get("mcode");
            }
            
            if (objMappable.list != null && objMappable.list.Count > 0)
            {
                repCodeList = objMappable.list.Select(x => Convert.ToString(x["report_code"]));
            }

            cri.SetOrderByReport_date(Criteria.OrderBy.ORDER_BY_DESC);
            cri.SetOrderByEmpNo(Criteria.OrderBy.ORDER_BY_DESC);
            
            repo.isCSV = true;

            var tempList = repo.GetSearchData(cri);
            var listDreport = new List<ErsDReport>();

            foreach (var item in tempList)
            {
                var report = new ErsDReport();
                report.OverwriteWithParameter(item);
                listDreport.Add(report);
            }

            var startDate = listDreport.OrderBy(x => x.report_date).First().report_date;
            var endDate = listDreport.OrderBy(x => x.report_date).Last().report_date;

            if (objMappable.src_from_date != null && objMappable.src_to_date != null)
            {
                startDate = (DateTime)objMappable.src_from_date;
                endDate = (DateTime)objMappable.src_to_date;
            }

            var filename =  "Date_range_"+ startDate.ToString("MMddyyyy") +"-"+ endDate.ToString("MMddyyyy")+"_" + DateTime.Now.ToString("yyyyMMddHHmmss")+ ".csv";
            
            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {                
                var group_emp_list = listDreport.GroupBy(x => x.emp_no);

                foreach (var emp in group_emp_list)
                {
                    var list = getHeaderList(emp.First(), objMappable, startDate, endDate);
                    //writes custom header
                    foreach (var header in list)
                    {
                        objMappable.csvCreater.WriteBody(header, writer);
                    }

                    //writes record header
                    objMappable.csvCreater.WriteCsvHeader<DReportDateRangeCSV>(writer);

                    //writes compiled records by pcode
                    foreach (var pcodeList in emp.GroupBy(y => y.pcode))
                    {
                        DReportDateRangeCSV dreport_csv = new DReportDateRangeCSV();
                        List<string> summaryList = new List<string>();
                        dreport_csv.pcode = pcodeList.First().pcode;
                        dreport_csv.proj_desc = pcodeList.First().proj_desc;
                        dreport_csv.um_hours = pcodeList.Sum(x => x.um_hours);
                        dreport_csv.completed_tasks = pcodeList.Where(x => x.status == EnumReportStatus.CompletedTask).Count();
                        dreport_csv.on_going_tasks = pcodeList.Where(x => x.status != EnumReportStatus.CompletedTask).Count();

                        foreach (var pcode in pcodeList)
                        {
                            if (pcode.summary != null)
                            {
                                var summary = pcode.summary.ToLower();
                                if (!summaryList.Contains(summary))
                                {
                                    summaryList.Add(summary);
                                }
                            }
                        }

                        dreport_csv.summary = string.Join("\n", summaryList.ToArray());
                        double temp = 0;

                        if (dreport_csv.completed_tasks > 0)
                        {
                            temp = (Convert.ToDouble(dreport_csv.completed_tasks) / Convert.ToDouble(pcodeList.Count()) * 100.0);
                        }

                        dreport_csv.progress = Convert.ToInt32(temp);
                        objMappable.csvCreater.WriteBody(dreport_csv, writer);
                    }

                    //writes new line for new record
                    Dictionary<string, object> reportBreak = new Dictionary<string, object>();
                    var space = " ";
                    reportBreak.Add("space", space);
                    objMappable.csvCreater.WriteBody(reportBreak, writer);
                }
            }
        }

        public List<Dictionary<string, object>> getHeaderList(ErsDReport report, IDReportDateRangeCSVMappable objMappable,DateTime? startDate,DateTime? endDate)
        {   
            List<Dictionary<string, object>> employeeHeader = new List<Dictionary<string, object>>();
            var employee = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(report.emp_no);
            
            Dictionary<string, object> nameHeader = new Dictionary<string, object>();
            var name = employee.fname + " " + employee.lname;
            nameHeader.Add("name", name.ToUpper());
            employeeHeader.Add(nameHeader);

            Dictionary<string, object> teamHeader = new Dictionary<string, object>();
            var namecode = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType.Team, Convert.ToInt16(employee.team));
            var team = "TEAM: ";

            if (namecode != null)
            {
                team += namecode.namename;
            }

            teamHeader.Add("team", team.ToUpper());
            employeeHeader.Add(teamHeader);
            
            Dictionary<string, object> supervisorHeader = new Dictionary<string, object>();
            var supervisor = "SUPERVISOR: ";

            if (employee.position == EnumPosition.Member || employee.position == EnumPosition.TeamLeader)
            {
                supervisor += ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Supervisor, EnumCommonNameColumnName.namename, (int)EnumSupervisor.Programmer);
            }
            else
            {
                supervisor += ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Supervisor, EnumCommonNameColumnName.namename, (int)EnumSupervisor.Admin);
            }

            supervisorHeader.Add("supervisor", supervisor.ToUpper());
            employeeHeader.Add(supervisorHeader);

            Dictionary<string, object> dateRangeHeader = new Dictionary<string, object>();
            var range = "DATE RANGE: ";

            if (objMappable.src_from_date != null && objMappable.src_to_date != null)
            {
                range += (Convert.ToDateTime(objMappable.src_from_date)).ToString("MM/dd/yyyy") + " ~ " + (Convert.ToDateTime(objMappable.src_to_date)).ToString("MM/dd/yyyy");
            }
            else
            {
                range += (Convert.ToDateTime(startDate)).ToString("MM/dd/yyyy") + " ~ " + (Convert.ToDateTime(endDate)).ToString("MM/dd/yyyy");
            }

            dateRangeHeader.Add("range", range.ToUpper());
            employeeHeader.Add(dateRangeHeader);

            return employeeHeader;
        }
    }
}