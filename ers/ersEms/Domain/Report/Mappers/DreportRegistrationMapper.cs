﻿using ersEms.Domain.Report.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersEms.Domain.Report.Mappers
{
    public class DReportRegistrationMapper : IMapper<IDReportRegistrationMappable>
    {
        public void Map(IDReportRegistrationMappable objMappable)
        {
            this.ReportAdd(objMappable);
        }

        internal void ReportAdd(IDReportRegistrationMappable objMappable)
        {
            var count = objMappable.dreport_details.Count;
            for (int i = 0; i < count; i++)
            {
                objMappable.dreport_details[i].report_date = objMappable.dreport_details[0].report_date;
                objMappable.dreport_details[i].emp_no = ErsContext.sessionState.Get("mcode");

                if (objMappable.dreport_details[0].report_date.HasValue)
                {
                    objMappable.dreport_details[i].report_code = objMappable.dreport_details[0].report_date.Value.ToString("yyyyMMdd") + '-' + objMappable.dreport_details[0].emp_no;
                }
            }
        }
    }
}