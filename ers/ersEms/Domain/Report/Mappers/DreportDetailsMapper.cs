﻿using ersEms.Domain.Report.Mappables;
using ersEms.Models.Report;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace ersEms.Domain.Report.Mappers
{
    public class DReportDetailsMapper : IMapper<IDReportDetailsMappable>
    {
        public void Map(IDReportDetailsMappable objMappable)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsDreportRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsDreportCriteria();
            cri.report_code = objMappable.report_code;

            var report_list = repo.Find(cri);

            DReportDetails dreport_details = new DReportDetails();
            objMappable.dreport_details = new List<DReportDetails>();

            foreach (var list in report_list)
            {
                objMappable.report_date = list.report_date;
                dreport_details = new DReportDetails();

                dreport_details.pcode = list.pcode;
                dreport_details.report_code = list.report_code;
                dreport_details.report_date = list.report_date;
                dreport_details.emp_no = list.emp_no;
                dreport_details.proj_desc = list.proj_desc;
                dreport_details.ref_no = list.ref_no;
                dreport_details.progress = list.progress;
                dreport_details.status = list.status;
                dreport_details.summary = list.summary;
                dreport_details.um_hours = list.um_hours;
                dreport_details.start_date = list.start_date;
                dreport_details.due_date = list.due_date;

                dreport_details.id = list.id;

                if (dreport_details.start_date != null)
                {
                    DateTime dt = DateTime.ParseExact(dreport_details.start_date, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                    dreport_details.start_date = dt.ToString("MM/dd/yyyy");
                }

                if (dreport_details.due_date != null)
                {
                    DateTime du = DateTime.ParseExact(dreport_details.due_date, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                    dreport_details.due_date = du.ToString("MM/dd/yyyy");
                }

                objMappable.dreport_details.Add(dreport_details);

                if (list.emp_no == ErsContext.sessionState.Get("mcode"))
                {
                    objMappable.owner_flg = true;
                }

                objMappable.downloaded = list.downloaded;

            }
        }
    }
}