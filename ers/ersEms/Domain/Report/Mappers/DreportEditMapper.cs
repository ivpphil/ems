﻿using ersEms.Domain.Report.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersEms.Domain.Report.Mappers
{
    public class DReportEditMapper:IMapper<IDReportEditMappable>
    {
        public void Map(IDReportEditMappable objMappable)
        {
            var count = objMappable.dreport_details.Count;
            for (int i = 0; i < count; i++)
            {
                objMappable.dreport_details[i].report_date = objMappable.report_date;
                objMappable.dreport_details[i].emp_no = ErsContext.sessionState.Get("mcode");
                objMappable.dreport_details[i].report_code = objMappable.report_code;
            }
        }
    }
}