﻿using ers.jp.co.ivp.ers.employee;
using ersEms.Domain.Report.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;

namespace ersEms.Domain.Report.Mappers
{
    public class DReportSearchMapper : IMapper<IDReportSearchMappable>
    {
        public void Map(IDReportSearchMappable objMappable)
        {
            this.SearchList(objMappable);
        }

        public void SearchList(IDReportSearchMappable objMappable)
        {
            var spec = ErsFactory.ersEmployeeFactory.GetReportSpecification();
            var cri = this.GetErsDreportCriteria(objMappable);

            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;

            if (objMappable.emp_pos == EnumPosition.Member)
            {
                cri.emp_no = ErsContext.sessionState.Get("mcode");
            }

            cri.status = EnumEmpStatus.Employed;
            objMappable.recordCount = spec.GetSearchData(cri).Count();

            cri.SetOrderByReport_date(Criteria.OrderBy.ORDER_BY_DESC);
            cri.SetOrderByEmpNo(Criteria.OrderBy.ORDER_BY_DESC);

            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("No reports found."));
                objMappable.error = true;
                return;
            }

            if (objMappable.pager != null)
            {
                objMappable.pager.SetLimitAndOffsetToCriteria(cri);
            }
            
            var list = spec.GetSearchData(cri);

            foreach (var item in list)
            {
                string fname = item["fname"].ToString();
                string lname = item["lname"].ToString();

                fname = myTI.ToTitleCase(fname.ToLower());
                lname = myTI.ToTitleCase(lname.ToLower());

                item["fname"] = fname;
                item["lname"] = lname;
                item["w_team"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Team, EnumCommonNameColumnName.namename, Convert.ToInt32(item["team"]));
            }

            objMappable.list = list;
        }


        protected ErsDReportCriteria GetErsDreportCriteria(IDReportSearchMappable objMappable)
        {
            ErsDReportCriteria cri = ErsFactory.ersEmployeeFactory.GetErsDreportCriteria();
            
            if (objMappable.src_report_date != null)
            {
                cri.report_date = objMappable.src_report_date;
            }

            if (objMappable.src_from_date != null)
            {
                cri.from_date = objMappable.src_from_date;
            }

            if (objMappable.src_to_date != null)
            {
                cri.to_date = objMappable.src_to_date;
            }

            if (objMappable.src_emp_no.HasValue())
            {
                cri.emp_no = objMappable.src_emp_no;
            }

            if (objMappable.src_emp_no.HasValue())
            {
                cri.emp_no = objMappable.src_emp_no;
            }

            if (objMappable.src_fname.HasValue())
            {
                cri.fname_like(objMappable.src_fname);
            }

            if (objMappable.src_lname.HasValue())
            {
                cri.lname_like(objMappable.src_lname);
            }

            if (objMappable.src_team.HasValue)
            {
                cri.team = objMappable.src_team;
            }

            var my_emp_no = string.Empty;
            List<string> newList = new List<string>();
            var mcode = ErsContext.sessionState.Get("mcode");
            
            if (objMappable.include_my_report == true)
            {
                newList.Add(mcode);
            }

            if ((objMappable.src_byTeamLead) && objMappable.src_team_leader != null)
            {
                foreach (var item in objMappable.src_team_leader)
                {
                    newList.Add(item);
                }
            }
            else
            {
                if (objMappable.emp_pos == EnumPosition.TeamLeader)
                {
                    var team_cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
                    var team_repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
                    team_cri.team_leader = mcode;
                    var find = team_repo.Find(team_cri);
                    foreach (var item in find)
                    {
                        newList.Add(item.mcode);
                    }


                    newList.Add(mcode);
                }
            }

            if (newList.Count != 0)
            {
                cri.setSearchByTeam(newList);
            }

            cri.status = EnumEmpStatus.Employed;

            return cri;
        }
    }
}