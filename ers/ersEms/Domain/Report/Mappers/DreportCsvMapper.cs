﻿using ers.jp.co.ivp.ers.employee;
using ersEms.Domain.Report.Mappables;
using ersEms.Models.Report;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ersEms.Domain.Report.Mappers
{
    public class DReportCsvMapper:DReportSearchMapper,IMapper<IDReportCSVMappable>
    {
        public void Map(IDReportCSVMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }

        internal void CreateCsvFile(IDReportCSVMappable objMappable)
        {
            objMappable.csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            
            var spec = ErsFactory.ersEmployeeFactory.GetReportSpecification();
            var cri = this.GetErsDreportCriteria(objMappable);

            if (objMappable.emp_pos == EnumPosition.Member)
            {
                cri.emp_no = ErsContext.sessionState.Get("mcode");
            }

            IEnumerable<string> repCodeList = new List<string>();

            if (objMappable.list != null && objMappable.list.Count > 0)
            {
                 repCodeList = objMappable.list.Select(x => Convert.ToString(x["report_code"]));
            }

            if (!objMappable.download_csv_all)
            {
                cri.InReportCodeList(repCodeList);
            }

            cri.SetOrderByReport_date(Criteria.OrderBy.ORDER_BY_DESC);
            cri.SetOrderByEmpNo(Criteria.OrderBy.ORDER_BY_DESC);
            
            spec.isCSV= true;

            var dReportList = spec.GetSearchData(cri);

            objMappable.csv_list = objMappable.download_csv_all ? dReportList : null;

            var filename = DateTime.Now.ToString("yyyyMMddHHmmss") + "Daily Report" + ".csv";

            DReportCSV dreport_csv;
            using (var writer = objMappable.csvCreater.GetWriter(filename))
            {
                List<string> currentCode = new List<string>();
                foreach(var item in dReportList)
                {
                    var dReportEntity = new ErsDReport();
                    dReportEntity.OverwriteWithParameter(item);

                    if (currentCode.Contains(dReportEntity.report_code))
                    {
                        continue;
                    }

                    currentCode.Add(dReportEntity.report_code);

                    List<Dictionary<string, object>> employeeHeader = new List<Dictionary<string, object>>();
                    Dictionary<string, object> nameHeader = new Dictionary<string, object>();

                    var currentReportList = dReportList.Where(x => Convert.ToString(x["report_code"]) == dReportEntity.report_code);
                    var name = Convert.ToString(item["fname"]) + " " + Convert.ToString(item["lname"]);
                    var namecode = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType.Team, Convert.ToInt16(dReportEntity.team));

                    nameHeader.Add("name", name.ToUpper());
                    employeeHeader.Add(nameHeader);

                    var team = "TEAM: ";
                    if (namecode != null)
                    {
                        team += namecode.namename;
                    }

                    Dictionary<string, object> teamHeader = new Dictionary<string, object>();
                    teamHeader.Add("team", team.ToUpper());
                    employeeHeader.Add(teamHeader);

                    Dictionary<string, object> empNoHeader = new Dictionary<string, object>();
                    var emp_no = "EMP NO: " + dReportEntity.emp_no;
                    empNoHeader.Add("emp_no", emp_no.ToUpper());
                    employeeHeader.Add(empNoHeader);

                    Dictionary<string, object> dateHeader = new Dictionary<string, object>();
                    var date = "REPORT DATE: " + dReportEntity.report_date.ToString("D");
                    dateHeader.Add("date", date.ToUpper());
                    employeeHeader.Add(dateHeader);

                    Dictionary<string, object> totalHrHeader = new Dictionary<string, object>();
                    var total_hour = "TOTAL USED HOUR: " + (currentReportList.Sum(x => Convert.ToDouble(x["um_hours"]))).ToString();
                    totalHrHeader.Add("total", total_hour.ToUpper());
                    employeeHeader.Add(totalHrHeader);

                    Dictionary<string, object> reportCdHeader = new Dictionary<string, object>();
                    var rep_code = "REPORT CODE: " + dReportEntity.report_code;
                    reportCdHeader.Add("rep_code", rep_code.ToUpper());
                    employeeHeader.Add(reportCdHeader);
                    
                    foreach (var header in employeeHeader)
                    {
                        objMappable.csvCreater.WriteBody(header, writer);
                    }

                    //writes column header 
                    objMappable.csvCreater.WriteCsvHeader<DReportCSV>(writer);

                    foreach (var report in currentReportList)
                    {
                        dreport_csv = new DReportCSV();
                        dreport_csv.OverwriteWithParameter(report);

                        objMappable.csvCreater.WriteBody(dreport_csv, writer);
                    }

                    Dictionary<string, object> reportBreak = new Dictionary<string, object>();
                    var space = " ";
                    reportBreak.Add("space", space);
                    objMappable.csvCreater.WriteBody(reportBreak, writer);
                }
            }
        }
    }
}