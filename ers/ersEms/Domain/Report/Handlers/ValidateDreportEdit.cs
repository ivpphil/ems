﻿using ersEms.Domain.Report.Commands;
using ersEms.Models.Report;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Report.Handlers
{
    public class ValidateDReportEdit:IValidationHandler<IDReportEditCommand>
    { 
        public IEnumerable<ValidationResult> Validate(IDReportEditCommand command)
        {
            if (!string.IsNullOrEmpty(command.report_code))
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyDreport().CheckReportCode (command.report_code);
            }

            if (command.dreport_details != null)
            {
                foreach (DReportDetails dreport_details in command.dreport_details)
                {
                    dreport_details.report_date = command.report_date;
                    dreport_details.lineNumber = command.dreport_details.IndexOf(dreport_details) + 1;
                    dreport_details.AddInvalidField(command.controller.commandBus.Validate<IDReportEditDetailsCommand>(dreport_details));

                    dreport_details.emp_no = ErsContext.sessionState.Get("mcode");

                    if (dreport_details.report_date.HasValue)
                    {
                        dreport_details.report_code = dreport_details.report_date.Value.ToString("yyyyMMdd") + '-' + dreport_details.emp_no;
                    }

                    if (!dreport_details.IsValid)
                    {
                        foreach (var errorMessage in dreport_details.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "dreport_details" });
                        }
                    }
                }
            }
        }
    }
}