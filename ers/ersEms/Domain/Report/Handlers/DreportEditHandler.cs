﻿using ersEms.Domain.Report.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Linq;

namespace ersEms.Domain.Report.Handlers
{
    public class DReportEditHandler:ICommandHandler<IDReportEditCommand>
    {
        public ICommandResult Submit(IDReportEditCommand command)
        {
            UpdateReport(command);
            DeleteReport(command);
            return new CommandResult(true);
        }

        internal void UpdateReport(IDReportEditCommand command)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsDreportRepository();
            int count = command.dreport_details.Count();
            
            for(int i = 0; i <count; i++)
            {
                var criteria = ErsFactory.ersEmployeeFactory.GetErsDreportCriteria();
                criteria.id = command.dreport_details[i].id;

                var list = repo.Find(criteria);

                if(list.Count == 0)
                {
                    var dreport = ErsFactory.ersEmployeeFactory.GetErsDreport();
                    dreport.OverwriteWithParameter(command.dreport_details[i].GetPropertiesAsDictionary());
                    repo.Insert(dreport);
                }

                if (list.Count != 0)
                {
                    var new_report = list.First();
                    command.dreport_details[i].report_code = command.report_code;

                    new_report.OverwriteWithParameter(command.dreport_details[i].GetPropertiesAsDictionary());
                    var old_report = ErsFactory.ersEmployeeFactory.getDreportId(command.dreport_details[i].id);
                    
                    repo.Update(old_report, new_report);
                }
            }
        }

        internal void DeleteReport(IDReportEditCommand command)
        {
            if (!string.IsNullOrEmpty(command.deleted_id))
            {
                if (command.deleted_id != null)
                {
                    command.deleted_id =   command.deleted_id + "_";
                }
                var searchfor = command.deleted_id.IndexOf("_");

                if (searchfor > 0)
                {
                    var deleted_report = command.deleted_id.Split('_');
                    var deleted_reportCount = deleted_report.Count();

                    for (int i = 0; i < deleted_reportCount; i++)
                    {
                        if (deleted_report[i].HasValue())
                        {
                            var criteria = ErsFactory.ersEmployeeFactory.GetErsDreportCriteria();

                            criteria.id = Convert.ToInt32(deleted_report[i]);

                            ErsFactory.ersEmployeeFactory.GetErsDreportRepository().Delete(criteria);
                            
                        }                        
                    }
                }
            }
        }
    }
}