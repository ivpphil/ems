﻿using ersEms.Domain.Report.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Report.Handlers
{
    public class ValidateDReportEditDetails:IValidationHandler<IDReportEditDetailsCommand>
    {
        public IEnumerable<ValidationResult> Validate(IDReportEditDetailsCommand command)
        {
            yield return command.CheckRequired("report_date");
            yield return command.CheckRequired("pcode");
            yield return command.CheckRequired("proj_desc");
            yield return command.CheckRequired("um_hours");
            yield return command.CheckRequired("progress");
            yield return command.CheckRequired("status");

            yield return ErsFactory.ersEmployeeFactory.GetErsStgyDreport().Check_start_due_date(command.start_date, command.due_date);

            yield return ErsFactory.ersEmployeeFactory.GetErsStgyDreport().checkpCodeExist(command.pcode, command.proj_desc);

            yield return ErsFactory.ersEmployeeFactory.GetErsStgyDreport().checkProgress(command.progress, command.status);
        }
    }
}