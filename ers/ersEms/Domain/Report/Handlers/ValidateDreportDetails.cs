﻿using ersEms.Domain.Report.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Report.Handlers
{
    public class ValidateDReportDetails : IValidationHandler<IDReportDetailsCommand>
    {
        public IEnumerable<ValidationResult> Validate(IDReportDetailsCommand command)
        {
            var currentDate = DateTime.Now;
            Convert.ToDateTime(currentDate);

            yield return command.CheckRequired("report_date");
            yield return command.CheckRequired("pcode");
            yield return command.CheckRequired("proj_desc");
            yield return command.CheckRequired("um_hours");
            yield return command.CheckRequired("progress");

            if (command.progress != null)
            {
                bool isValidProgress =(command.progress % 10 == 0);
                if (!isValidProgress)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("ProgressVal"));
                }
            }

            yield return command.CheckRequired("status");
            
            yield return ErsFactory.ersEmployeeFactory.GetErsStgyDreport().Check_start_due_date(command.start_date, command.due_date);

            if (command.IsValidField("report_date"))
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyDreport().Check_reportDate(command.emp_no, Convert.ToDateTime(command.report_date));
            }

            if ((command.start_date != null) && (command.due_date != null))
            {
                if (currentDate < command.report_date || Convert.ToDateTime(command.start_date) > currentDate || Convert.ToDateTime(command.start_date) == Convert.ToDateTime(command.due_date))
                {
                    yield return new ValidationResult(ErsResources.GetMessage("Date is invalid."));
                }
            }

            yield return ErsFactory.ersEmployeeFactory.GetErsStgyDreport().checkpCodeExist(command.pcode, command.proj_desc);
            yield return ErsFactory.ersEmployeeFactory.GetErsStgyDreport().checkProgress(command.progress, command.status);
        } 
    }
}