﻿using ersEms.Domain.Report.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Linq;

namespace ersEms.Domain.Report.Handlers
{
    public class DReportAddHandler:ICommandHandler<IDReportAddCommand>    
    {
        public ICommandResult Submit(IDReportAddCommand command)
        {
            var empCri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
            var empRepo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            empCri.emp_no = ErsContext.sessionState.Get("mcode");

            var result = empRepo.FindSingle(empCri);
            
            int count = command.dreport_details.Count();

            for(int i=0; i < count; i++)
            {
                var entity = ErsFactory.ersEmployeeFactory.GetErsDreport();
                var repo = ErsFactory.ersEmployeeFactory.GetErsDreportRepository();

                entity.OverwriteWithParameter(command.dreport_details[i].GetPropertiesAsDictionary());

                repo.Insert(entity, true);
            }

            return new CommandResult(true);
        }
    }
}