﻿using ersEms.Domain.Report.Commands;
using ersEms.Models.Report;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ersEms.Domain.Report.Handlers
{
    public class ValidateDReportAdd: IValidationHandler<IDReportAddCommand>
    {
        public IEnumerable<ValidationResult> Validate(IDReportAddCommand command)
        {
            if (command.dreport_details != null)
            {
                if (command.dreport_details[0].report_date.HasValue)
                {
                    command.report_date = command.dreport_details[0].report_date;
                }

                var count = command.dreport_details.Count;
                for (int i = 0; i < count; i++)
                {
                    command.dreport_details[i].report_date = command.dreport_details[0].report_date;
                    command.dreport_details[i].emp_no = ErsContext.sessionState.Get("mcode");

                    if (command.dreport_details[0].report_date.HasValue)
                    {
                        command.dreport_details[i].report_code = command.dreport_details[0].report_date.Value.ToString("yyyyMMdd") + '-' + command.dreport_details[0].emp_no;
                    }
                }
                
                foreach (DReportDetails dreport_details in command.dreport_details)
                {
                    dreport_details.lineNumber = command.dreport_details.IndexOf(dreport_details) + 1;
                    dreport_details.AddInvalidField(command.controller.commandBus.Validate<IDReportDetailsCommand>(dreport_details));

                    var existingRefNo = command.dreport_details.Where(x => x.ref_no == dreport_details.ref_no && x.pcode == dreport_details.pcode);
                    if (existingRefNo.Count() > 1)
                    {
                        dreport_details.AddInvalidField(new ValidationResult(ErsResources.GetMessage("DuplicateRef", new[] { ErsResources.GetFieldName("tasks") +  dreport_details.ref_no })));
                    }

                    if (!dreport_details.IsValid)
                    {
                        foreach(var errorMessage in dreport_details.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "dreport_details" });
                        }
                    }

                    if (Convert.ToDateTime(dreport_details.report_date).Date > DateTime.Now.Date)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("ReportDateInvalidErrMessage"), new[] { "report_date" });
                    }

                    if (Convert.ToDateTime(dreport_details.report_date).Date < DateTime.Now.Date)
                    {
                        yield return ErsFactory.ersEmployeeFactory.GetErsStgyDreport().CheckLessThanReportDate(Convert.ToDateTime(dreport_details.report_date).Date);
                    }
                }

                var dailyManHrsSum = command.dreport_details.Sum(x => x.um_hours);
                if (dailyManHrsSum > 0)
                {
                    yield return ErsFactory.ersEmployeeFactory.GetErsStgyDreport().checkTotalUsedManHours(dailyManHrsSum);
                }
            }
        }
    }
}