﻿using ersEms.Domain.Report.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ersEms.Domain.Report.Handlers
{
    public class DReportCsvHandler: ICommandHandler<IDReportCsvCommand>
    {
        public ICommandResult Submit(IDReportCsvCommand command)
        {
            var list = command.list != null ? command.list : command.csv_list != null ? command.csv_list : null;

            if (list != null)
            {
                UpdateReportCsv(list);
            }
            
            return new CommandResult(true);
        }

        internal void UpdateReportCsv(List<Dictionary<string, object>> reportList)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsDreportRepository();
            
            foreach(var item in reportList)
            {
                var cri = ErsFactory.ersEmployeeFactory.GetErsDreportCriteria();
                cri.report_code = Convert.ToString(item["report_code"]);

                var report_list = repo.Find(cri);

                foreach(var reportitem in report_list)
                {
                    var old_report = ErsFactory.ersEmployeeFactory.getDreportId(reportitem.id);
                    var new_report = ErsFactory.ersEmployeeFactory.getDreportId(reportitem.id);

                    new_report.downloaded = EnumOnOff.On;

                    repo.Update(old_report, new_report);
                }
            }
        }
    }
}