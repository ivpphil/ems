﻿using ersEms.Domain.Report.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Report.Handlers
{
    public class ValidateDReportSearch:IValidationHandler<IDReportSearchCommand>
    {
        public IEnumerable<ValidationResult>Validate(IDReportSearchCommand command)
        {         
            if (command.src_from_date != null && command.src_to_date != null)
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyDreport().Check_start_due_date(command.src_from_date.ToString(), command.src_to_date.ToString());
            }
 
            yield return ErsFactory.ersEmployeeFactory.GetErsStgyDreport().CheckDate(command.src_report_date);

            if(command.src_team.HasValue)
            {
                var team_error = ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("team", EnumCommonNameType.Team, (int?)command.src_team); ;
                if (team_error != null)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("invalid_field", ErsResources.GetFieldName("team")), new[] { "team" });
                }
            }
         
            if(command.src_byTeamLead && !command.hasSelected)
            {
                yield return command.CheckRequired("src_team_leader");
            }

            if (command.hasSelected)
            {
                foreach (var tl in command.src_team_leader)
                {
                    var tl_repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
                    var empCrit = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
                    empCrit.emp_no = tl;

                    var result = tl_repo.FindSingle(empCrit);

                    if ((tl_repo.GetRecordCount(empCrit) == 0) || (result.position != EnumPosition.TeamLeader))
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("team_lead_select_err"));
                    }
                    empCrit.Clear();
                }
            }




        }
    }
}