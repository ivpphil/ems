﻿using ersEms.Models.Report;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System.Collections.Generic;

namespace ersEms.Domain.Report.Mappables
{
    public interface IDReportRegistrationMappable : IMappable
    {
        IList<DReportDetails> dreport_details { get; set; }

        List<Dictionary<string, object>> list { get; set; }
    }
}
