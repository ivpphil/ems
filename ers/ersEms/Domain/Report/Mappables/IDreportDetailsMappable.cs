﻿using ersEms.Models.Report;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;

namespace ersEms.Domain.Report.Mappables
{
    public interface IDReportDetailsMappable:IMappable
    {
        IList<DReportDetails> dreport_details { get; set; }

        DateTime? report_date { get; set; }

        string report_code { get; set; }

        List<Dictionary<string, object>> list { set; }

        bool owner_flg { get; set; }

        EnumOnOff downloaded { get; set; }
    }
}
