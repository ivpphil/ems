﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using System;
using System.Collections.Generic;

namespace ersEms.Domain.Report.Mappables
{
    public interface IDReportSearchMappable:IMappable
    {
        ErsPagerModel pager { get; set; }

        bool error { get; set; }

        long limit { get; set; }

        long recordCount { get; set; }

        int? src_id { get; set; }

        DateTime? src_report_date { get; set; }

        string src_emp_no { get; set; }

        string src_report_code { get; set; }

        string src_fname { get; set; }

        string src_lname { get; set; }

        EnumTeam? src_team { get; set; }

        DateTime? src_from_date { get; set; }

        DateTime? src_to_date { get; set; }

        List<Dictionary<string, object>> list { get; set; }

        long maxItemCount { get;}

        int pageCnt { get; set; }

        bool download_csv_all { get; set; }

        bool src_byTeamLead { get; set; }
           
        string[] src_team_leader { get; set; }

        EnumPosition? emp_pos { get; set; }

        bool include_my_report { get; set; }
    }
}
