﻿using jp.co.ivp.ers.util;
using System.Collections.Generic;

namespace ersEms.Domain.Report.Mappables
{
    public interface IDReportCSVMappable : IDReportSearchMappable
    {
        ErsCsvCreater csvCreater { get; set; }

        List<Dictionary<string, object>> csv_list { get; set; }
    }
}