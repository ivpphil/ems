﻿using jp.co.ivp.ers.util;

namespace ersEms.Domain.Report.Mappables
{
    public interface IDReportDateRangeCSVMappable : IDReportSearchMappable
    {
        ErsCsvCreater csvCreater { get; set; }
    }
}