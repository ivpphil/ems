﻿using ersEms.Models.Report;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;

namespace ersEms.Domain.Report.Commands
{
    public interface IDReportAddCommand:ICommand
    {
        IList<DReportDetails> dreport_details { get; set; }
        DateTime? report_date { get; set; }
    }
}
