﻿using ersEms.Models.Report;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;

namespace ersEms.Domain.Report.Commands
{
    public interface IDReportCsvCommand:ICommand
    {
        List<Dictionary<string, object>> list { get; set; }

        List<Dictionary<string, object>> csv_list { get; set; }
    }
}
