﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersEms.Domain.Report.Commands
{
    public interface IDReportDetailsCommand : ICommand
    {

        int? id { get; set; }

        DateTime? report_date { get; set; }

        string emp_no { get; set; }

        string report_code { get; set; }

        string pcode { get; set; }

        string proj_desc { get; set; }

        string ref_no { get; set; }

        int? progress { get; set; }

        string summary { get; set; }

        string start_date { get; set; }
       
        EnumReportStatus? status { get; set; }

        string due_date { get; set; }

        double um_hours { get; set; }

    }
}
