﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersEms.Domain.Report.Commands
{
    public interface IDReportSearchCommand:ICommand
    {
        DateTime? src_report_date { get; set; }

        DateTime? src_from_date { get; set; }

        DateTime? src_to_date { get; set; }

        string src_emp_no { get; set; }

        string src_report_code { get; set; }

        string src_fname { get; set; }

        string src_lname { get; set; }

        EnumTeam? src_team { get; set; }

        bool src_byTeamLead { get; set; }

        bool hasSelected { get; }

        string[] src_team_leader { get; set; }

    }
}
