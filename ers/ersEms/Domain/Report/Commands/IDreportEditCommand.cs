﻿using ersEms.Models.Report;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;

namespace ersEms.Domain.Report.Commands
{
    public interface IDReportEditCommand:ICommand
    {
        IList<DReportDetails> dreport_details { get; set; }

       DateTime? report_date { get; set; }
       string report_code { get; set; }
       string deleted_id { get; set; }
    }
}
