﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ersEms.Domain.Register.Commands
{
   public interface IEmpRegistCommand: ICommand
    {
        int id { get; set; }

        string emp_no { get; set; }

        int? desknet_id { get; set; }

        string fname { get; set; }

        string lname { get; set; }

        DateTime? birthday { get; set; }

        string address { get; set; }

        string contact_no { get; set; }

        EnumPosition? position { get; set; }

        EnumTeam? team { get; set; }

        string password { get; set; }

        string password_confirm { get; set; }

        string email { get; set; }

        string email_confirm { get; set; }

        string mcode { get; set; }

        EnumMformat? mformat { get; set; }

        int m_flg { get; set; }
    }
}
