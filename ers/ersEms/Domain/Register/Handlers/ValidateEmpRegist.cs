﻿using ersEms.Domain.Register.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Register.Handlers
{
    public class ValidateEmpRegist: IValidationHandler<IEmpRegistCommand>
    {

        public IEnumerable<ValidationResult> Validate(IEmpRegistCommand command)
        {
            if (!command.password.HasValue())
            {
                command.password = ErsFactory.ersEmployeeFactory.generateRandomPassword();
            }

            yield return command.CheckRequired("emp_no");
            yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckDuplicateEmpNo(command.emp_no);


            yield return command.CheckRequired("desknet_id");
            if (command.desknet_id.HasValue)
            {
                yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckDuplicateDesknetID((int)command.desknet_id);
            }
          
            yield return command.CheckRequired("lname");
            yield return command.CheckRequired("fname");

            yield return command.CheckRequired("email");
            yield return command.CheckRequired("email_confirm");
            yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().CheckEmailConfirm(command.email, command.email_confirm);
            yield return ErsFactory.ersEmployeeFactory.GetErsStgyEmployee().ChecDuplicateRegister(command.email);

            yield return command.CheckRequired("position");
            if (command.position.HasValue)
            {
                yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("position", EnumCommonNameType.position, (int?)command.position);
            }

            yield return command.CheckRequired("team");
            if (command.team.HasValue)
            {
                yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("Team", EnumCommonNameType.Team, (int?)command.team);
            }

        }

    }
}
