﻿using ersEms.Domain.Register.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.Register.Handlers
{
    public class EmpRegistHandler : ICommandHandler<IEmpRegistCommand>
    {
        public ICommandResult Submit(IEmpRegistCommand command)
        {

            var entity = ErsFactory.ersEmployeeFactory.GetErsEmployee();
            entity.OverwriteWithModel(command);
            entity.mcode = command.emp_no;
            entity.m_flg = 1;


            ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository().Insert(entity, true);

            return new CommandResult(true);
        }
    }
}