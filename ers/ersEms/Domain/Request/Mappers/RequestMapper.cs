﻿using ersEms.Domain.Request.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.request;
using jp.co.ivp.ers.util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ersEms.Domain.Request.Mappers
{
    public class RequestMapper
        : IMapper<IRequestMappable>
    {
        public void Map(IRequestMappable objMappable)
        {
            objMappable.emp_no = ErsContext.sessionState.Get("mcode");
            var repository = ErsFactory.ersRequestFactory.GetErsRequestRepository();
            
            objMappable.pending_list = this.GetRequestList(objMappable, repository, "pending");
            objMappable.my_approved_list = this.GetRequestList(objMappable, repository,"my_approved");
            objMappable.others_approved_list = this.GetRequestList(objMappable, repository, "others_approved");

            objMappable.pendingCount = objMappable.pending_list.Count();
            objMappable.myApprovedCount = objMappable.my_approved_list?.Where(x => Convert.ToString(x["emp_no"]) == objMappable.emp_no).Count();
            objMappable.otherApprovedCount = objMappable.others_approved_list?.Where(x => Convert.ToString(x["emp_no"]) != objMappable.emp_no).Count();

        }

        public ErsRequestCriteria GetCriteria(IRequestMappable objMappable)
        {
            var criteria = ErsFactory.ersRequestFactory.GetErsRequestCriteria();

            if (objMappable.position.HasValue)
            {
                if (objMappable.position.Value == EnumPosition.Member)
                {
                    criteria.emp_no = objMappable.emp_no;
                }
                else if (objMappable.position.Value == EnumPosition.TeamLeader)
                {
                    criteria.getTeamList(objMappable.emp_no);
                }
                else if (objMappable.s_request_type != null)
                {
                    criteria.request_type_in = objMappable.s_request_type;
                }
            }

            criteria.active = EnumActive.Active;
            criteria.SetOrderByDateFiled(Criteria.OrderBy.ORDER_BY_DESC);

            return criteria;
        }
        
        internal List<Dictionary<string, object>> GetRequestList(IRequestMappable objMappable, ErsRequestRepository repository, string stat)
        {
            var criteria = this.GetCriteria(objMappable);
            if (stat == "pending")
            {
                criteria.status = EnumStatusRequest.Pending;
            }
            else
            {
                criteria.status_not_equal = EnumStatusRequest.Pending;
                if (stat == "my_approved")
                {
                    criteria.getHROwnRequest();
                    criteria.emp_no = objMappable.emp_no;
                }
                if ((objMappable.emp_no == ErsFactory.ersUtilityFactory.getSetup().hr_emp_no) && stat == "others_approved")
                {
                    Tuple<DateTime, DateTime> cutoff = ErsFactory.ersRequestFactory.GetCutOffStgy().GetCutOff(DateTime.Now);
                    criteria.getMonitoredRequest(cutoff.Item1, cutoff.Item2);
                }
            }
            var result = repository.Find(criteria);
            criteria.Clear();

            return this.GetGroupedList(result, objMappable);
        }

        protected void GetGroupDetails(ErsEmployee member, Dictionary<string, object> g_dic, dynamic group )
        {
            g_dic["emp_name"] = member?.fname + " " + member?.lname;
            g_dic["emp_no"] = group.emp_no;
            g_dic["date_filed"] = group.date_filed.ToString("MM/dd/yyyy (ddd)");
            g_dic["request_type"] = group.request_type;
            g_dic["leave_type"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.LeaveType, EnumCommonNameColumnName.namename, (int?)group.leave_type);
        }

        protected void GetDetails(ErsRequest detail, Dictionary<string,object> detail_dic, ErsEmployee member)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (detail.date_start != null)
            {
                detail_dic["date_start"] = detail.date_start.Value.ToString("MM/dd/yyyy (ddd)");
            }

            if (detail.date_end != null)
            {
                detail_dic["date_end"] = detail.date_end.Value.ToString("MM/dd/yyyy (ddd)");
            }

            if (detail.time_start != null)
            {
                detail_dic["time_start"] = detail.time_start.ToShortTimeString();
            }

            if (detail.time_end != null)
            {
                detail_dic["time_end"] = detail.time_end.ToShortTimeString();
            }

            if (detail.request_type == EnumRequestType.Overtime || detail.request_type == EnumRequestType.Undertime)
            {
                detail_dic["duration"] = (detail.time_end - detail.time_start).ToString(@"hh\:mm");
            }
            
            if (detail.date_start != null)
            {
                var sched = ErsFactory.ersRequestFactory.getErsScheduleWithDesknetIDAndDate(member.desknet_id, (DateTime)detail.date_start);

                if (sched != null && sched.time_start.HasValue && sched.time_end.HasValue)
                {
                    detail_dic["sched"] = sched.time_start.Value.ToShortTimeString() + " ~ " + sched.time_end.Value.ToShortTimeString();
                }
                else
                {
                    detail_dic["sched"] = setup.def_time_start + " ~ " + setup.def_time_end;
                }
            }

        }


        protected List<Dictionary<string, object>> GetGroupedList(IList<ErsRequest> list_result, IRequestMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var groupList = new List<Dictionary<string, object>>();
            var group_by = list_result.Select(x => new { x.emp_no, x.request_type, x.date_filed, x.leave_type, x.status }).Distinct().Where(x => x.request_type != EnumRequestType.Leave);
            var leave_group_by = list_result.Select(x=> new { x.emp_no, x.request_type, x.date_filed, x.leave_type, x.status, x.date_start, x.id, x.used_leave }).Distinct().Where(x=> x.request_type == EnumRequestType.Leave);
            
            foreach (var leave_group in leave_group_by)
            {
                var g_dic = new Dictionary<string, object>();
                var member = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(leave_group.emp_no);
                var request_ids = new List<int>();

                this.GetGroupDetails(member, g_dic,leave_group);
                               
                var detailList = new List<Dictionary<string, object>>();
                var received_flg = new List<EnumReceivedRequestFlg>();
                foreach (var detail in list_result)
                {
                    if (detail.emp_no == leave_group.emp_no && detail.date_filed.Value.ToShortDateString() == leave_group.date_filed.Value.ToShortDateString()
                        && detail.request_type == leave_group.request_type && detail.leave_type == leave_group.leave_type && leave_group.id == detail.id)
                    {
                        var detail_dic = detail.GetPropertiesAsDictionary();

                        this.GetDetails(detail, detail_dic,member);
                        
                        request_ids.Add(Convert.ToInt32(detail.id));
                        received_flg.Add(detail.received_flg);

                        detailList.Add(detail_dic);

                        g_dic["status"] = detail.status;

                        var approverList = ErsFactory.ersRequestFactory.GetErsApproverListWithRequestID(detail.id);
                        g_dic["approval_count"] = approverList?.Count(i => i.approval_status == EnumApprovalStatus.Approved) ?? 0;
                        g_dic["approver_flg"] = approverList?.Any(i => i.emp_no.Contains(objMappable.emp_no)) ?? false;

                        if (detail.status == EnumStatusRequest.Declined)
                        {
                            g_dic["decline_reason"] = approverList?.Where(i => i.approval_status == EnumApprovalStatus.Declined)?.Select(x => x.reason);
                        }                    
                    }
                }

                g_dic["received_flg"] = received_flg.Contains(EnumReceivedRequestFlg.Received) ? EnumReceivedRequestFlg.Received : EnumReceivedRequestFlg.Pending;              
                g_dic["req_details"] = detailList;
                g_dic["request_ids"] = String.Join("-", request_ids.ToArray());

                if (objMappable.emp_no == ErsFactory.ersUtilityFactory.getSetup().hr_emp_no)
                {
                    g_dic["hr"] = true;

                    if (leave_group.request_type == EnumRequestType.Leave)
                    {
                        var leavebal = ErsFactory.ersRequestFactory.GetErsLeaveBalanceWithEmpNo(leave_group.emp_no);
                        g_dic["vl"] = leavebal?.total_vl;
                        g_dic["vl_less"] = leave_group.leave_type == EnumLeaveType.VacationLeave || leave_group.leave_type == EnumLeaveType.EmergencyLeave ? leave_group.used_leave : 0;
                        g_dic["sl"] = leavebal?.total_sl;
                        g_dic["sl_less"] = leave_group.leave_type == EnumLeaveType.SickLeave ? leave_group.used_leave : 0;
                    }
                }

                groupList.Add(g_dic);
            }

            foreach (var group in group_by)
            {
                var g_dic = new Dictionary<string, object>();
                var request_ids = new List<int>();
                var member = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(group.emp_no);

                this.GetGroupDetails(member, g_dic, group);

                var detailList = new List<Dictionary<string, object>>();

                foreach (var detail in list_result)
                {
                    if (detail.emp_no == group.emp_no && detail.date_filed.Value.ToShortDateString() == group.date_filed.Value.ToShortDateString()
                        && detail.request_type == group.request_type && detail.leave_type == group.leave_type)
                    {
                        var detail_dic = detail.GetPropertiesAsDictionary();

                        this.GetDetails(detail, detail_dic, member);

                        request_ids.Add(Convert.ToInt32(detail.id));

                        detailList.Add(detail_dic);

                        g_dic["status"] = detail.status;

                        var approverList = ErsFactory.ersRequestFactory.GetErsApproverListWithRequestID(detail.id);
                        g_dic["approval_count"] = approverList?.Count(i => i.approval_status == EnumApprovalStatus.Approved) ?? 0;
                        g_dic["approver_flg"] = approverList?.Any(i => i.emp_no.Contains(objMappable.emp_no)) ?? false;

                        if (detail.status == EnumStatusRequest.Declined)
                        {
                            g_dic["decline_reason"] = approverList?.Where(i => i.approval_status == EnumApprovalStatus.Declined)?.Select(x => x.reason);
                        }

                    }
                }

                g_dic["req_details"] = detailList;
                g_dic["request_ids"] = String.Join("-", request_ids.ToArray());

                if (objMappable.emp_no == ErsFactory.ersUtilityFactory.getSetup().hr_emp_no)
                {
                    g_dic["hr"] = true;
                  
                }

                groupList.Add(g_dic);
            }

            var sortGroupList = groupList.Select(x => x).OrderByDescending(x => x["date_filed"]).ToList();
            return sortGroupList;
        }
        
    }
}