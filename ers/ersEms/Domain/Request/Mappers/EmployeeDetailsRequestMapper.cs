﻿using ersEms.Domain.Request.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System.Globalization;

namespace ersEms.Domain.Request.Mappers
{
    public class EmployeeDetailsRequestMapper : IMapper<IEmployeeDetailsRequestMappable>
    {
        public void Map(IEmployeeDetailsRequestMappable objMappable)
        {
            if ((objMappable.overtime_details == null || objMappable.overtime_details.Count == 0) && (objMappable.logsheet_details == null || objMappable.logsheet_details.Count == 0))
            {
                objMappable.noRecord_flg = true;
            }
            objMappable.emp_no = ErsContext.sessionState.Get("mcode");
            var member_details = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(objMappable.emp_no);
            if(member_details != null)
            {
                objMappable.fname = member_details.fname;
                objMappable.lname = member_details.lname;
                objMappable.position = member_details.position;

                TextInfo textinfo = new CultureInfo("en-US", false).TextInfo;
                objMappable.fullname = string.Format("{0} {1}", textinfo.ToTitleCase(member_details.fname.ToLower()), textinfo.ToTitleCase(member_details.lname.ToLower()));
                objMappable.contact_no = member_details.contact_no;
            }
        }
    }
}