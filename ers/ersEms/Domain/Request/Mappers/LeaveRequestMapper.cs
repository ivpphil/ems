﻿using ersEms.Domain.Request.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersEms.Domain.Request.Mappers
{
    public class LeaveRequestMapper : IMapper<ILeaveRequestMappable>
    {
        public void Map(ILeaveRequestMappable objMappable)
        {
            this.LeaveRequestInput(objMappable);
        }

        internal void LeaveRequestInput(ILeaveRequestMappable objMappable)
        {
            objMappable.emp_no = ErsContext.sessionState.Get("mcode");
            objMappable.status = EnumStatusRequest.Pending;
            
            var leavebal = ErsFactory.ersRequestFactory.GetErsLeaveBalanceWithEmpNo(objMappable.emp_no);

            if (leavebal != null)
            {
                objMappable.total_vl = leavebal.total_vl;
                objMappable.total_sl = leavebal.total_sl;
                objMappable.now_vl = 0;
                objMappable.now_sl = 0;
                objMappable.last_vl = objMappable.total_vl;
                objMappable.last_sl = objMappable.total_sl;
            }
        }
    }
}