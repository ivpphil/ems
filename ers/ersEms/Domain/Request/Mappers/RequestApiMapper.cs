﻿using ersEms.Domain.TimeKeeping.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.TimeKeeping.Mappers
{
    public class RequestApiMapper: IMapper<IRequestApiMappable>
    {
        public void Map(IRequestApiMappable objMappable)
        {
            var emp_no = ErsContext.sessionState.Get("mcode");
            var emp_repo = ErsFactory.ersEmployeeFactory.getErsEmployeeNo(emp_no);
            objMappable.desknet_id = emp_repo.desknet_id;
            
            var sched_cri = ErsFactory.ersScheduleFactory.getErsScheduleWithDesknetIDAndDate(objMappable.desknet_id,objMappable.date_start_ot);
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            if (sched_cri!=null)
            {
                if (sched_cri.time_start.HasValue && sched_cri.time_end.HasValue)
                {
                    objMappable.shift = sched_cri.time_start.Value.ToString("hh:mm tt") + "-" + sched_cri.time_end.Value.ToString("hh:mm tt");
                }
            }
            if(sched_cri==null || !sched_cri.time_start.HasValue || !sched_cri.time_end.HasValue)
            {
                objMappable.shift = setup.def_time_start + "-" + setup.def_time_end;
            }
        }
    }
}