﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.request;
using System;
using System.Linq;
using System.Reflection;

namespace ersEms.Domain.Request.Handlers
{
    public class UpdateRequestStatusHandler : ICommandHandler<IUpdateRequestStatusCommand>
    {
        public ICommandResult Submit(IUpdateRequestStatusCommand command)
        {
            var requestRepo = ErsFactory.ersRequestFactory.GetErsRequestRepository();
            var objRequest = ErsFactory.ersRequestFactory.GetErsRequest();

            string[] collection_id = command.str_request_id.Split('-'); // some request have multiple ids separated by '-'

            foreach (string id in collection_id)
            {
                command.request_id = Convert.ToInt32(id);
                if (command.status == EnumStatusRequest.Cancelled)
                {
                    
                    var old_obj = ErsFactory.ersRequestFactory.GetErsRequestWithid(Convert.ToInt32(command.request_id));
                    objRequest.OverwriteWithModel(old_obj);
                    objRequest.status = EnumStatusRequest.Cancelled;
                    objRequest.active = EnumActive.NonActive;
                    objRequest.cancel_reason = command.cancel_reason;
                    requestRepo.Update(old_obj, objRequest);
                }
                else
                {
                    var approverRepo = ErsFactory.ersRequestFactory.GetErsApproverRepository();
                    var approverCriteria = ErsFactory.ersRequestFactory.GetErsApproverCriteria();
                    var approverEmpNum = ErsContext.sessionState.Get("mcode");
                    approverCriteria.emp_no = approverEmpNum;
                    approverCriteria.request_id = command.request_id;
                    if(approverRepo.GetRecordCount(approverCriteria) == 0)
                    {
                        var objApprove = ErsFactory.ersRequestFactory.GetErsApprover();
                        objApprove.OverwriteWithModel(command);
                        objApprove.emp_no = approverEmpNum;
                        approverRepo.Insert(objApprove, true);
                    }

                    var objApproveList = ErsFactory.ersRequestFactory.GetErsApproverListWithRequestID(Convert.ToInt32(command.request_id));
                    var approvedCount = objApproveList?.Count(i => i.approval_status == EnumApprovalStatus.Approved) ?? 0;
                    if (command.approval_status == EnumApprovalStatus.Declined)
                    {
                        UpdateRequest(command, objRequest, requestRepo);
                    }
                    else
                    {
                        var setup = ErsFactory.ersUtilityFactory.getSetup();
                        var request = ErsFactory.ersRequestFactory.GetErsRequestWithid(Convert.ToInt32(command.request_id));

                        //if two or more approvers 
                        if (command.request_type != EnumRequestType.Undertime && command.request_type != EnumRequestType.InfoCorrection)
                        {
                            var requestor_position = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(request.emp_no)?.position;
                            if (requestor_position == EnumPosition.Administrator)
                            {
                                UpdateRequest(command, objRequest, requestRepo);

                            }
                            else
                            {
                                if (approvedCount > 1)
                                {
                                    UpdateRequest(command, objRequest, requestRepo);
                                }
                            }
                        }
                        // only one approver
                        else
                        {
                            if (command.request_type == EnumRequestType.InfoCorrection)
                            {
                                UpdateEmployeeInfo(command);
                            }

                            UpdateRequest(command, objRequest, requestRepo);
                        }
                    }
                }
            }
            return new CommandResult(true);
        }

        internal void UpdateRequest(IUpdateRequestStatusCommand command, ErsRequest objRequest, ErsRequestRepository requestRepo)
        {
            var old_obj = ErsFactory.ersRequestFactory.GetErsRequestWithid(Convert.ToInt32(command.request_id));

            objRequest.OverwriteWithModel(old_obj);
            objRequest.status = command.approval_status == EnumApprovalStatus.Approved ? EnumStatusRequest.Approved : EnumStatusRequest.Declined;
            requestRepo.Update(old_obj, objRequest);
        }
        
        internal void UpdateEmployeeInfo(IUpdateRequestStatusCommand command)
        {
            var objRequest = ErsFactory.ersRequestFactory.GetErsRequestWithid(Convert.ToInt32(command.request_id));
            var empRepo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var old_obj = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(objRequest?.emp_no);

            var new_obj = ErsFactory.ersEmployeeFactory.GetErsEmployee();
            var correction_list = ErsFactory.ersEmployeeFactory.GetErsEmployeeCorrectionWithID(objRequest.correction_id);

            new_obj.OverwriteWithModel(old_obj);
            foreach (PropertyInfo prop in correction_list.GetType().GetProperties())
            {
                var propValue = prop.GetValue(correction_list, null);
                if (propValue != null && !prop.Name.Equals("id") && !prop.Name.Equals("emp_no"))
                {
                    new_obj.GetType().GetProperty(prop.Name).SetValue(new_obj, propValue, null);
                }
            }
            new_obj.intime = DateTime.Now;

            empRepo.Update(old_obj, new_obj);
        }
    }
}