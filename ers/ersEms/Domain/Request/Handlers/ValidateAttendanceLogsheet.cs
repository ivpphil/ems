﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using ersEms.Models.Request;
using System;

namespace ersEms.Domain.Request.Handlers
{
    public class ValidateAttendanceLogsheet : IValidationHandler<IAttendanceLogsheetCommand>
    {
        public IEnumerable<ValidationResult> Validate(IAttendanceLogsheetCommand command)
        {
            if (command.logsheet_details?.Count > 0)
            {
                if(command.logsheet_details.Count > 15)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("RegisterLimit", 15), new[] { "logsheet_details" });
                }
                else
                {
                    foreach (logsheet_details logsheet_details in command.logsheet_details)
                    {
                        logsheet_details.lineNumber = command.logsheet_details.IndexOf(logsheet_details) + 1;

                        logsheet_details.AddInvalidField(command.controller.commandBus.Validate<IAttendanceLogsheetDetailsCommand>(logsheet_details));

                        if (logsheet_details.date_start.HasValue())
                        {
                            if (Convert.ToDateTime(logsheet_details.date_start) < Convert.ToDateTime(command.w_date_filed))
                            {
                                yield return ErsFactory.ersRequestFactory.GetCutOffStgy().GetCheckWithinCutOffStgy(Convert.ToDateTime(logsheet_details.date_start));
                            }
                        }

                        if (!logsheet_details.IsValid)
                        {
                            foreach(var errorMessage in logsheet_details.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "logsheet_details" });
                            }
                        }
                    }
                }
            }
            else
            {
                yield return new ValidationResult(ErsResources.GetMessage("logsheet_blank_row"), new[] { "logsheet_details" });
            }
        }
    }
}