﻿using ersEms.Domain.Request.Commands;
using ersEms.Models.Request;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Request.Handlers
{
    public class ValidateOvertime: IValidationHandler<IOvertimeCommand>
    {
        public virtual IEnumerable<ValidationResult> Validate(IOvertimeCommand command)
        {
            if (command.overtime_details?.Count > 0)
            {
                if (command.overtime_details.Count > 10)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("RegisterLimit", 10));
                }
                else
                {
                    var list = new List<string>();

                    foreach (overtime_details overtime_details in command.overtime_details)
                    {
                        overtime_details.lineNumber = command.overtime_details.IndexOf(overtime_details) + 1;

                        overtime_details.AddInvalidField(command.controller.commandBus.Validate<IOvertimeDetailsCommand>(overtime_details));

                        if (overtime_details.date_start.HasValue)
                        {
                            if (list.Contains(overtime_details.date_start.ToString()))
                            {
                                overtime_details.AddInvalidField(new ValidationResult(ErsResources.GetMessage("10103a", "Date", overtime_details.date_start?.ToShortDateString()), new[] { "date" }));
                            }
                            else
                            {
                                list.Add(overtime_details.date_start.ToString());
                            }

                        }                     
                    
                        if (!overtime_details.IsValid)
                        {
                            foreach (var errorMessage in overtime_details.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "overtime_details" });
                            }
                        }
                    }
                }
            }
            else
            {
                yield return command.CheckRequired("date_start");
                yield return command.CheckRequired("start_hours");
                yield return command.CheckRequired("start_minutes");
                yield return command.CheckRequired("start_period");
                yield return command.CheckRequired("end_hours");
                yield return command.CheckRequired("end_minutes");
                yield return command.CheckRequired("end_period");
                yield return command.CheckRequired("reason");                    
            }
        }
    }
}