﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Linq;

namespace ersEms.Domain.Request.Handlers
{
    public class OvertimeHandler : ICommandHandler<IOvertimeCommand>
    {
        public ICommandResult Submit(IOvertimeCommand command)
        {
            var requestRepo = ErsFactory.ersRequestFactory.GetErsRequestRepository();
            var requestEntity = ErsFactory.ersRequestFactory.GetErsRequest();

            int count = command.overtime_details.Count();

            for (int i = 0; i < count; i++)
            {
                var dictionary = command.overtime_details[i].GetPropertiesAsDictionary();
                requestEntity.OverwriteWithParameter(dictionary);
                requestEntity.request_type = EnumRequestType.Overtime;
                requestEntity.status = EnumStatusRequest.Pending;
                requestEntity.emp_no = ErsContext.sessionState.Get("mcode");
                requestEntity.active = EnumActive.Active;

                var short_date_now = DateTime.Now.ToShortDateString();
                requestEntity.date_filed = Convert.ToDateTime(short_date_now);
                
                requestRepo.Insert(requestEntity, true);
            }

            return new CommandResult(true);
        }
    }
}