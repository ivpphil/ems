﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.request;
using System;
using System.Linq;
using System.Reflection;

namespace ersEms.Domain.Request.Handlers
{
    public class ReceiveRequestHandler : ICommandHandler<IReceiveRequestCommand>
    {
        public ICommandResult Submit(IReceiveRequestCommand command)
        {
            var leaveBalanceRepo = ErsFactory.ersRequestFactory.GetErsLeaveBalanceRepository();
            var objReceiveLeaveBalance = ErsFactory.ersRequestFactory.GetErsLeaveBalance();
            
            string[] collection_id = command.str_request_id.Split('-'); // some request have multiple ids separated by '-'
            
            foreach (string id in collection_id)
            {
                var objRequest = ErsFactory.ersRequestFactory.GetErsRequestWithid(Convert.ToInt32(id));
                var newObjRequest = ErsFactory.ersRequestFactory.GetErsRequest();
                newObjRequest.OverwriteWithModel(objRequest);

                var repo = ErsFactory.ersRequestFactory.GetErsRequestRepository();

                if (command.request_type.Equals(EnumRequestType.Leave))
                {
                    objReceiveLeaveBalance.emp_no = objRequest.emp_no;
                    objReceiveLeaveBalance.reason = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.LeaveType, EnumCommonNameColumnName.namename, (int?)objRequest.leave_type);

                    var leaveBal = GetLastLeaveBalance(objRequest);
                    if (objRequest.leave_type.Equals(EnumLeaveType.VacationLeave) || objRequest.leave_type.Equals(EnumLeaveType.EmergencyLeave))
                    {                        
                        objReceiveLeaveBalance.last_vl = leaveBal.total_vl;
                        objReceiveLeaveBalance.now_vl = -objRequest.used_leave; // convert to negative
                        objReceiveLeaveBalance.total_vl = objReceiveLeaveBalance.last_vl + objReceiveLeaveBalance.now_vl; // subtract last_vl to now_vl


                        objReceiveLeaveBalance.last_sl = leaveBal.last_sl;
                        objReceiveLeaveBalance.now_sl = leaveBal.now_sl; // convert to negative
                        objReceiveLeaveBalance.total_sl = leaveBal.total_sl; // subtract last_sl to now_sl

                    }
                    if (objRequest.leave_type.Equals(EnumLeaveType.SickLeave))
                    {
                        objReceiveLeaveBalance.last_vl = leaveBal.last_vl;
                        objReceiveLeaveBalance.now_vl = leaveBal.now_vl;
                        objReceiveLeaveBalance.total_vl = leaveBal.total_vl; // subtract last_vl to now_vl

                        objReceiveLeaveBalance.last_sl = leaveBal.total_sl;
                        objReceiveLeaveBalance.now_sl = -objRequest.used_leave; // convert to negative
                        objReceiveLeaveBalance.total_sl = objReceiveLeaveBalance.last_sl + objReceiveLeaveBalance.now_sl; // subtract last_sl to now_sl
                    }

                    leaveBalanceRepo.Insert(objReceiveLeaveBalance,true);
                }

                newObjRequest.received_flg = EnumReceivedRequestFlg.Received;

                repo.Update(objRequest, newObjRequest);


            }
            return new CommandResult(true);
        }

        private void SetLeaveData(ErsRequest objRequest)
        {

        }


        public ErsLeaveBalance GetLastLeaveBalance(ErsRequest objRequest)
        {
            var criteria = ErsFactory.ersRequestFactory.GetErsLeaveBalanceCriteria();
            var repository = ErsFactory.ersRequestFactory.GetErsLeaveBalanceRepository();

            criteria.emp_no = objRequest.emp_no;
            criteria.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);

            var result = repository.Find(criteria).FirstOrDefault();

            if (result != null)
            { 
                return result;
            }

            return null;
        }



    }
}