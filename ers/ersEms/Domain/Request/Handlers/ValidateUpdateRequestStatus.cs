﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersEms.Domain.Request.Handlers
{
    public class ValidateUpdateRequestStatus : IValidationHandler<IUpdateRequestStatusCommand>
    {
        public IEnumerable<ValidationResult> Validate(IUpdateRequestStatusCommand command)
        {
            string[] collection_id = command.str_request_id.Split('-'); // some request have multiple ids separated by '-'

            foreach (string id in collection_id)
            {
                command.request_id = Convert.ToInt32(id);
                yield return ErsFactory.ersRequestFactory.GetRequestValidationStgy().CheckRequestID(command.request_id, command.request_type);
                if(!ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().ExistValue(EnumCommonNameType.RequestType, (int?)command.request_type))
                {
                    yield return new ValidationResult(ErsResources.GetMessage("NotExist", ErsResources.GetFieldName("request_type")));
                }

                if (command.status != EnumStatusRequest.Cancelled)
                {
                    yield return ErsFactory.ersRequestFactory.GetRequestValidationStgy().CheckRequestMCodeWithID(command.request_id);
                }
            }

        }
       
    }
}