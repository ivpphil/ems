﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace ersEms.Domain.Request.Handlers
{
    public class ValidateRequestSearch : IValidationHandler<IRequestSearchCommand>
    {
        public IEnumerable<ValidationResult> Validate(IRequestSearchCommand command)
        {
            if (command.s_request_type != null)
            {
                foreach (var s_req_type in command.s_request_type)
                {
                    if (!ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().ExistValue(EnumCommonNameType.RequestType, s_req_type))
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("NotExist", ErsResources.GetFieldName("request_type")));
                    }
                }
            }
        }
    }
}