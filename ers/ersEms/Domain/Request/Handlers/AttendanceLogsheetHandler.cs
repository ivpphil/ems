﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Linq;

namespace ersEms.Domain.Request.Handlers
{
    public class AttendanceLogsheetHandler : ICommandHandler<IAttendanceLogsheetCommand>
    {
        public ICommandResult Submit(IAttendanceLogsheetCommand command)
        {
            var repo = ErsFactory.ersRequestFactory.GetErsRequestRepository();
            var entity = ErsFactory.ersRequestFactory.GetErsRequest();

            int log_count = command.logsheet_details.Count();

            for(int i=0; i<log_count; i++)
            {
                var logs_dictionary = command.logsheet_details[i].GetPropertiesAsDictionary();
                entity.OverwriteWithParameter(logs_dictionary);
                entity.active = EnumActive.Active;
                entity.status = EnumStatusRequest.Pending;
                entity.emp_no = ErsContext.sessionState.Get("mcode");
                entity.request_type = EnumRequestType.LogsheetCorrection;
                
                var short_date = DateTime.Now.ToShortDateString();
                entity.date_filed = Convert.ToDateTime(short_date);

                repo.Insert(entity, true);

            }

            return new CommandResult(true);
        }
    }
}