﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersEms.Domain.Request.Handlers
{
    public class UndertimeHandler : ICommandHandler<IUndertimeCommand>
    {
        public ICommandResult Submit(IUndertimeCommand command)
        {
            var requestRepo = ErsFactory.ersRequestFactory.GetErsRequestRepository();
            var requestEntity = ErsFactory.ersRequestFactory.GetErsRequest();

            requestEntity.OverwriteWithModel(command);
            requestEntity.request_type = EnumRequestType.Undertime;
            requestEntity.reason = command.w_reason;
            requestEntity.date_start = command.ut_date;
            requestEntity.date_end = command.ut_date;
            requestEntity.status = EnumStatusRequest.Pending;
            requestEntity.active = EnumActive.Active;

            requestRepo.Insert(requestEntity, true);

            return new CommandResult(true);
        }
    }
}