﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using System.IO;
using System;

namespace ersEms.Domain.Request.Handlers
{
    public class LeaveRequestHandler : ICommandHandler<ILeaveRequestCommand>
    {
        public ICommandResult Submit(ILeaveRequestCommand command)
        {
            command.emp_no = ErsContext.sessionState.Get("mcode");
            this.InsertRequest(command);

            return new CommandResult(true);
        }

        internal void InsertRequest(ILeaveRequestCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var temp_path = setup.image_temp_directory + command.emp_no + "\\required_files\\";
            var temp_file = temp_path + command.temp_file_name;
            var extention = temp_file.Split('.');
            var file_path = setup.image_directory + command.emp_no + "\\required_files\\";
            var file_name = string.Empty;
            if (command.leave_type == EnumLeaveType.SickLeave)
            {
                if (Convert.ToDouble(command.now_sl) >= setup.sl_count_medcert)
                {
                    if (!Directory.Exists(file_path))
                    {
                        Directory.CreateDirectory(file_path);
                    }
                    file_name = string.Concat("Med_cert_", command.date_filed?.ToString("MMddyyyyhhmmss"), '.', extention[temp_file.Split('.').Length - 1]);
                    file_path += file_name;
                    CopyTempImages(temp_file, file_path, true);

                }
            }
            else if (command.leave_type == EnumLeaveType.PaternityLeave)
            {
                if (!Directory.Exists(file_path))
                {
                    Directory.CreateDirectory(file_path);
                }
                file_name = string.Concat("Paternity_ID_", command.date_filed?.ToString("MMddyyyyhhmmss"), '.', extention[temp_file.Split('.').Length - 1]);
                file_path += file_name;
                CopyTempImages(temp_file, file_path, true);
            }
            var repository = ErsFactory.ersRequestFactory.GetErsRequestRepository();
            var entity = ErsFactory.ersRequestFactory.GetErsRequest();
            entity.file_name = file_name;
            entity.OverwriteWithParameter(command.GetPropertiesAsDictionary());
            entity.active = EnumActive.Active;
            entity.request_type = EnumRequestType.Leave;
            entity.status = EnumStatusRequest.Pending;

            repository.Insert(entity, true);
        }


        private void CopyTempImages(string tempFile, string destinationFile, bool overwrite = true)
        {

            if (File.Exists(tempFile))
            {
                File.Copy(tempFile, destinationFile, overwrite);
                File.Delete(tempFile);
            }
        }

    }
}