﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Request.Handlers
{
    public class ValidateOvertimeDetails : IValidationHandler<IOvertimeDetailsCommand>
    {
        public virtual IEnumerable<ValidationResult> Validate(IOvertimeDetailsCommand command)
        {
            yield return command.CheckRequired("date_start");
            yield return command.CheckRequired("start_hours");
            yield return command.CheckRequired("start_minutes");
            yield return command.CheckRequired("start_period");
            yield return command.CheckRequired("end_hours");
            yield return command.CheckRequired("end_minutes");
            yield return command.CheckRequired("end_period");
            yield return command.CheckRequired("reason");

            if (command.date_start != null)
            {
                yield return ErsFactory.ersRequestFactory.GetRequestValidationStgy().CheckDuplicateRequest(command.date_start, EnumRequestType.Overtime);
            }

            yield return ErsFactory.ersRequestFactory.GetRequestValidationStgy().CheckValidOvertime(command.time_start,command.time_end,command.shift_end,command.w_time_start,command.date_start,command.w_date_filed);
            
        }
    }
}