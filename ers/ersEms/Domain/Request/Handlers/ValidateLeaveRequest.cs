﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using System.IO;

namespace ersEms.Domain.Request.Handlers
{
    public class ValidateLeaveRequest : IValidationHandler<ILeaveRequestCommand>
    {
        public IEnumerable<ValidationResult> Validate(ILeaveRequestCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            command.emp_no = ErsContext.sessionState.Get("mcode");

            yield return command.CheckRequired("leave_type");
            yield return command.CheckRequired("used_leave");
            yield return command.CheckRequired("date_start");
            yield return command.CheckRequired("date_end");

            if (command.leave_type == EnumLeaveType.SickLeave)
            {
                if (Convert.ToDouble(command.now_sl) >= setup.sl_count_medcert)
                {
                    if (command.sick_file_proof != null && command.sick_file_proof.ContentLength > 0)
                    {
                        SaveTempImage(command);
                    }
                    else
                    {
                        if (!command.temp_file_name.HasValue())
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("10000", new[] { ErsResources.GetFieldName("sick_file_proof") }));
                        }
                    }

                }
            }

            if (command.leave_type == EnumLeaveType.PaternityLeave)
            {

                if (command.paternal_file_proof != null && command.paternal_file_proof.ContentLength > 0)
                {
                    SaveTempImage(command);
                }
                else
                {
                    if (!command.temp_file_name.HasValue())
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("10000", new[] { ErsResources.GetFieldName("paternal_file_proof") }));
                    }
                }
            }

            yield return command.CheckRequired("reason");

            if (command.used_leave == 0)
            {
                yield return new ValidationResult(ErsResources.GetMessage("used_leave_zero"), new[] { "used_leave" });
            }

            if (command.leave_type.HasValue && command.date_start.HasValue)
            {
                yield return ErsFactory.ersRequestFactory.GetRequestValidationStgy().CheckDuplicateRequest(command.date_start, EnumRequestType.Leave);
            }

            if (command.date_start.HasValue && command.date_end.HasValue)
            {
                var date_start = Convert.ToString(command.date_start);
                var date_end = Convert.ToString(command.date_end);

                yield return ErsFactory.ersRequestFactory.GetRequestValidationStgy().CheckTimeComparison(date_start, date_end);

                yield return ErsFactory.ersRequestFactory.GetRequestValidationStgy().CheckBetweenDateLeaveRequest(command.date_start, command.date_end);
            }

            if (command.leave_type.HasValue)
            {
                if (command.leave_type == EnumLeaveType.SickLeave)
                {
                    if (command.used_leave > command.last_sl)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("used_leave_limit", ErsResources.GetFieldName("sl_balance")), new[] { "used_leave" });
                    }
                }

                if (command.leave_type == EnumLeaveType.VacationLeave)
                {
                    if (command.used_leave > command.last_vl)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("used_leave_limit"), new[] { "used_leave" });
                    }
                    if (command.date_start.HasValue && command.date_end.HasValue && command.date_filed.HasValue)
                    {
                        var OneDayVLNotice = command.date_filed.Value.AddDays(2);
                        var TwoOrMoreDaysVLNotice = command.date_filed.Value.AddDays(4);

                        if (command.used_leave <= 1)
                        {
                            if (OneDayVLNotice > command.date_start)
                            {
                                yield return new ValidationResult(ErsResources.GetMessage("notice_days", 3), new[] { "date_start" });
                            }
                        }
                        else if (command.used_leave > 1)
                        {
                            if (TwoOrMoreDaysVLNotice > command.date_start)
                            {
                                yield return new ValidationResult(ErsResources.GetMessage("notice_days", 5), new[] { "date_start" });
                            }
                        }
                    }
                }
                else if (command.leave_type == EnumLeaveType.MaternityLeave)
                {
                    if (command.used_leave > 78)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("specific_leave_limit",
                            ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.LeaveType, EnumCommonNameColumnName.namename, (int?)EnumLeaveType.MaternityLeave)),
                            new[] { "used_leave" });
                    }
                }
                else if (command.leave_type == EnumLeaveType.PaternityLeave)
                {
                    if (command.used_leave > 7)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("specific_leave_limit",
                            ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.LeaveType, EnumCommonNameColumnName.namename, (int?)EnumLeaveType.PaternityLeave)),
                            new[] { "used_leave" });
                    }

                    var notice_date = command.date_filed.Value.AddDays(14);

                    if (command.date_start < notice_date)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("notice_days", 14), new[] { "date_start" });
                    }
                }
                else if (command.leave_type == EnumLeaveType.ParentalLeave || command.leave_type == EnumLeaveType.NoPayLeave)
                {
                    var notice_date_one = command.date_filed.Value.AddDays(3);
                    var notice_date_two = command.date_filed.Value.AddDays(5);

                    if (command.used_leave == 1)
                    {
                        if (command.date_start < notice_date_one)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("notice_days", 3), new[] { "date_start" });
                        }
                    }
                    else if (command.used_leave > 1)
                    {
                        if (command.date_start < notice_date_two)
                        {
                            yield return new ValidationResult(ErsResources.GetMessage("notice_days", 5), new[] { "date_start" });
                        }
                    }

                    if (command.leave_type == EnumLeaveType.ParentalLeave)
                    {
                        yield return ErsFactory.ersRequestFactory.GetRequestValidationStgy().ParentalLeaveLimit(command.emp_no, command.used_leave);
                    }
                }
                else if (command.leave_type == EnumLeaveType.CompassionateLeave)
                {
                    if (command.used_leave > 3)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("specific_leave_limit",
                            ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.LeaveType, EnumCommonNameColumnName.namename, (int?)EnumLeaveType.CompassionateLeave)),
                            new[] { "used_leave" });
                    }
                }
                else if (command.leave_type == EnumLeaveType.MarriageLeave)
                {
                    var notice_date = command.date_filed.Value.AddDays(14);

                    if (command.date_start < notice_date)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("notice_days"), new[] { "date_start" });
                    }
                }

                if (command.date_start.HasValue)
                {
                    if (command.date_start < command.date_filed)
                    {
                        if (command.leave_type == EnumLeaveType.SickLeave || command.leave_type == EnumLeaveType.EmergencyLeave)
                        {
                            bool isELorSL = true;
                            yield return ErsFactory.ersRequestFactory.GetCutOffStgy().GetCheckWithinCutOffStgy(Convert.ToDateTime(command.date_start), isELorSL);
                        }
                        else
                        {
                            yield return ErsFactory.ersRequestFactory.GetCutOffStgy().GetCheckWithinCutOffStgy(Convert.ToDateTime(command.date_start));
                        }
                    }
                }
            }
        }

        private void SaveTempImage(ILeaveRequestCommand command)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var uploadedFileHelper = ErsFactory.ersUtilityFactory.GetSaveUploadedFileHelper();
            var temp_path = setup.image_temp_directory + command.emp_no + "\\required_files\\";

            if (Directory.Exists(temp_path))
            {
                //clean up
                foreach (var file in Directory.GetFiles(temp_path))
                {
                    File.Delete(file);
                }
            }

            if (command.leave_type == EnumLeaveType.SickLeave && command.sick_file_proof != null)
            {
                command.temp_file_name = command.sick_file_proof.FileName;
                uploadedFileHelper.SaveTempFile(command.sick_file_proof, temp_path, command.temp_file_name);
            }
            else if (command.leave_type == EnumLeaveType.PaternityLeave && command.paternal_file_proof != null)
            {
                command.temp_file_name = command.paternal_file_proof.FileName;
                uploadedFileHelper.SaveTempFile(command.paternal_file_proof, temp_path, command.temp_file_name);
            }
            else
            {
                command.temp_file_name = string.Empty;
                return;
            }

        }
    }
}