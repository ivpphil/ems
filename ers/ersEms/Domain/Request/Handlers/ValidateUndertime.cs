﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersEms.Domain.Request.Handlers
{
    public class ValidateUndertime : IValidationHandler<IUndertimeCommand>
    {
        public IEnumerable<ValidationResult> Validate(IUndertimeCommand command)
        {
            yield return command.CheckRequired("ut_date");
            yield return command.CheckRequired("start_hours");
            yield return command.CheckRequired("start_minutes");
            yield return command.CheckRequired("start_period");
            yield return command.CheckRequired("end_hours");
            yield return command.CheckRequired("end_minutes");
            yield return command.CheckRequired("end_period");
            yield return command.CheckRequired("ut_reason_type");
            yield return command.CheckRequired("reason");

            if (command.start_hours.HasValue() && command.start_minutes.HasValue() && command.start_period.HasValue)
            {
                var start_time = command.start_hours + ":" + command.start_minutes + " " + command.start_period;
                command.time_start = start_time;
            }

            if (command.end_hours.HasValue() && command.end_minutes.HasValue() && command.end_period.HasValue)
            {
                var end_time = command.end_hours + ":" + command.end_minutes + " " + command.end_period;
                command.time_end = end_time;
            }

            var start_prd = ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("start_period", EnumCommonNameType.ClockPeriod, (int?)command.start_period);
            var end_prd = ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("end_period", EnumCommonNameType.ClockPeriod, (int?)command.end_period);

            if (command.time_start.HasValue() && command.time_end.HasValue() && start_prd == null && end_prd == null)
            {
                var start_tm = DateTime.Parse(command.time_start);
                var end_tm = DateTime.Parse(command.time_end);
                if (start_tm > end_tm)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("ut_time_invalid"));
                }
                if (start_tm == end_tm)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("invalid_equal_params", ErsResources.GetFieldName("time_start"), ErsResources.GetFieldName("time_end")));
                }
            }

            if (command.ut_reason_type.HasValue)
            {
                yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("ut_reason_type", EnumCommonNameType.UndertimeReason, (int?)command.ut_reason_type);
            }

            if (command.request_type.HasValue)
            {
                if (command.request_type != EnumRequestType.Undertime)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("request_type_invalid"));
                }
            }

            if(command.ut_date.HasValue)
            {
                yield return ErsFactory.ersRequestFactory.GetRequestValidationStgy().CheckDuplicateRequest(command.ut_date, EnumRequestType.Undertime);
            }

            if (command.ut_date.HasValue)
            {
                if (command.ut_date < command.date_filed)
                {
                    yield return ErsFactory.ersRequestFactory.GetCutOffStgy().GetCheckWithinCutOffStgy(Convert.ToDateTime(command.ut_date));
                }
            }
            if(command.undertime_hours.HasValue())
            {
                if(Convert.ToDouble(command.undertime_hours) >= 4)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("HalfdayEL"));
                }
            }
        }
    }
}