﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using System;

namespace ersEms.Domain.Request.Handlers
{
    public class ValidateAttendanceLogsheetDetails : IValidationHandler<IAttendanceLogsheetDetailsCommand>
    {
        public IEnumerable<ValidationResult> Validate(IAttendanceLogsheetDetailsCommand command)
        {
            yield return command.CheckRequired("date_start");
            yield return command.CheckRequired("start_hours");
            yield return command.CheckRequired("start_minutes");
            yield return command.CheckRequired("start_period");
            yield return command.CheckRequired("end_hours");
            yield return command.CheckRequired("end_minutes");
            yield return command.CheckRequired("end_period");
            yield return command.CheckRequired("reason");

            if(command.time_start != null && command.time_end != null)
            {
                yield return ErsFactory.ersRequestFactory.GetRequestValidationStgy().CheckTimeComparison(Convert.ToString(command.time_start), Convert.ToString(command.time_end));
            }

            if (command.time_start == command.time_end)
            {
               yield return new ValidationResult(ErsResources.GetMessage("invalid_equal_params", ErsResources.GetFieldName("time_start"), ErsResources.GetFieldName("time_end")));
            }
        }
    }
}