﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersEms.Domain.Request.Commands
{
    public interface IRequestSearchCommand : ICommand
    {
        int?[] s_request_type { get; set; }
    }
}