﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersEms.Domain.Request.Commands
{
    public interface IOvertimeDetailsCommand : ICommand
    {
        string emp_no { get; set; }

        DateTime? date_start { get; set; }

        DateTime? time_start { get; set; }

        DateTime? time_end { get; set; }

        string shift_end { get; set; }

        string w_date_filed { get;}

        string w_date_start { get; }

        string w_time_start { get; }
    }
}