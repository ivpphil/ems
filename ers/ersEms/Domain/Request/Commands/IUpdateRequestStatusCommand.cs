﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersEms.Domain.Request.Commands
{
    public interface IUpdateRequestStatusCommand : ICommand
    {
        int? request_id { get; set; }

        string str_request_id { get; set; }

        string emp_no { get; set; }

        EnumRequestType? request_type { get; set; }

        EnumApprovalStatus? approval_status { get; set; }

        EnumStatusRequest? status { get; set; }

        string reason { get; set; }

        string cancel_reason { get; set; }

        DateTime intime { get; set; }

        EnumPosition? position { get; set; }

    }
}