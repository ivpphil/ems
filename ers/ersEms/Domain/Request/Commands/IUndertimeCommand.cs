﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersEms.Domain.Request.Commands
{
    public interface IUndertimeCommand : ICommand
    {
        string emp_no { get; set; }

        EnumRequestType? request_type { get; set; }

        DateTime? date_filed { get; }

        EnumStatusRequest? status { get; set; }

        string reason { get; set; }

        EnumUndertimeReason? ut_reason_type { get; set; }

        string w_reason { get; }

        DateTime? ut_date { get; set; }

        string time_start { get; set; }

        string time_end { get; set; }

        EnumLeaveType? leave_type { get; set; }

        string start_hours { get; set; }

        string start_minutes { get; set; }

        EnumClockPeriod? start_period { get; set; }

        string end_hours { get; set; }

        string end_minutes { get; set; }

        EnumClockPeriod? end_period { get; set; }

        string undertime_hours { get; set; }
    }
}