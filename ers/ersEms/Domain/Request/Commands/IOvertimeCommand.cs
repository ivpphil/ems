﻿using ersEms.Models.Request;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;

namespace ersEms.Domain.Request.Commands
{
    public interface IOvertimeCommand: ICommand
    {
        IList<overtime_details> overtime_details { get; set; }

        string emp_no { get; set; }

        string w_date_filed { get;}
    }
}