﻿using ersEms.Models.Request;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;

namespace ersEms.Domain.Request.Commands
{
    public interface IAttendanceLogsheetCommand : ICommand
    {
        IList<logsheet_details> logsheet_details { get; set; }

        string w_date_filed { get; }
    }
}
