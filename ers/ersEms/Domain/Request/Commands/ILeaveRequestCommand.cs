﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;
using System.Web;

namespace ersEms.Domain.Request.Commands
{
    public interface ILeaveRequestCommand : ICommand
    {
        string emp_no { get; set; }

        EnumRequestType? request_type { get; set; }

        DateTime? date_filed { get; }

        EnumStatusRequest? status { get; set; }

        string reason { get; set; }

        DateTime? date_start { get; set; }

        DateTime? date_end { get; set; }

        EnumLeaveType? leave_type { get; set; }

        double used_leave { get; set; }

        double? last_vl { get; set; }

        double? now_vl { get; set; }

        double? total_vl { get; set; }

        double? last_sl { get; set; }

        double? now_sl { get; set; }

        double? total_sl { get; set; }

        HttpPostedFileBase sick_file_proof { get; set; }

        HttpPostedFileBase paternal_file_proof { get; set; }

        string temp_file_name { get; set; }
        
    }
}