﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersEms.Domain.Request.Commands
{
    public interface IAttendanceLogsheetDetailsCommand : ICommand
    {
        int? id { get; set; }

        string date_start { get; set; }

        DateTime? time_start { get; set; }

        DateTime? time_end { get; set; }

        string start_hours { get; set; }

        string start_minutes { get; set; }

        string end_hours { get; set; }

        string end_minutes { get; set; }

        string reason { get; set; }

        string s_time_start { get; set; }

        string s_time_end { get; set; }

        string shift_schedule { get; set; }
    }
}
