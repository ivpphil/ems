﻿using ersEms.Models.Request;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System.Collections.Generic;

namespace ersEms.Domain.Request.Mappables
{
    public interface IEmployeeDetailsRequestMappable : IMappable
    {
        IList<overtime_details> overtime_details { get; set; }

        IList<logsheet_details> logsheet_details { get; set; }
        
        bool noRecord_flg { get; set; }

        string emp_no { get; set; }

        string fname { get; set; }

        string lname { get; set; }

        EnumPosition? position { get; set; }

        string fullname { get; set; }

        string contact_no { get; set; }

        int?[] s_request_type
        {
            get; set;
        }

    }
}