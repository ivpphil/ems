﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System;

namespace ersEms.Domain.Request.Mappables
{
    public interface ILeaveRequestMappable : IMappable
    {
        string emp_no { get; set; }

        string fname { get; set; }

        string lname { get; set; }

        string contact_no { get; set; }

        EnumPosition? position { get; set; }

        EnumStatusRequest? status { get; set; }

        DateTime? date_filed { get; }

        double? last_vl { get; set; }

        double? now_vl { get; set; }

        double? total_vl { get; set; }

        double? last_sl { get; set; }

        double? now_sl { get; set; }

        double? total_sl { get; set; }

        double used_leave { get; set; }

        EnumLeaveType? leave_type { get; set; }
    }
}