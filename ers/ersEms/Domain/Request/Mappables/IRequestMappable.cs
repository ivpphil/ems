﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System.Collections.Generic;

namespace ersEms.Domain.Request.Mappables
{
    public interface IRequestMappable
        :IMappable
    {
        List<Dictionary<string, object>> pending_list { get; set; }

        List<Dictionary<string, object>> others_approved_list { get; set; }

        List<Dictionary<string, object>> my_approved_list { get; set; }

        string emp_no { get; set; }

        EnumPosition? position { get; set; }

        int? pendingCount { get; set; }

        int? myApprovedCount { get; set; }

        int? otherApprovedCount { get; set; }

        int?[] s_request_type { get; set; }
    }
}