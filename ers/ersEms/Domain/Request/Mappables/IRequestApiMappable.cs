﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Domain.TimeKeeping.Mappables
{
    public interface IRequestApiMappable
        : IMappable
    {
        int desknet_id { get; set; }
        DateTime date_start_ot { get; set; }
        string shift { get; set; }
    }
}