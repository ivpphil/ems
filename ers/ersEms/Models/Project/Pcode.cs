﻿using ersEms.Domain.Project.Commands;
using ersEms.Domain.Project.Mappable;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using System.Collections.Generic;

namespace ersEms.Models.Project
{
    public class Pcode:ErsModelBase,IPcodeRegistCommand,IPcodeListMappable,IPcodeEditMappable,IPcodeDeleteCommand
    {
        [BindTable("pcode_details")]
        public IList<pcode_details> pcode_details { get; set; }

        [ErsSchemaValidation("pcode_t.id")]
        [ErsOutputHidden("id")]
        public int? id { get; set; }

        [ErsSchemaValidation("pcode_t.pcode")]
        [ErsOutputHidden]
        public string pcode { get; set; }

        [ErsSchemaValidation("pcode_t.pcode")]
        [ErsOutputHidden("search")]
        public string s_pcode { get; set; }

        [ErsSchemaValidation("pcode_t.pcode_desc")]
        [ErsOutputHidden]
        public string pcode_desc { get; set; }

        public ErsPagerModel pager { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long recordCount { get; set; }

        [ErsUniversalValidation(type =CHK_TYPE.All)]
        [ErsOutputHidden]
        public string emp_pos { get; set; }

        [HtmlSubmitButton]
        public bool deleteFlg { get; set; }

        public int? sent_message_count { get; set; }
        
        public bool noRecord_flg { get; set; }
    }
}