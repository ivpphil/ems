﻿using ersEms.Domain.Project.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersEms.Models.Project
{
    public class pcode_details : ErsBindableModel, IPcodeDetailsRegistCommand
    {
        [ErsSchemaValidation("pcode_t.id")]
        [ErsOutputHidden]
        public int? id { get; set; }

        [ErsSchemaValidation("pcode_t.pcode")]
        [ErsOutputHidden]
        public string pcode { get; set; }

        [ErsSchemaValidation("pcode_t.pcode_desc")]
        [ErsOutputHidden]
        public string pcode_desc { get; set; }

        [HtmlSubmitButton]
        public bool deleteFlg { get; set; }
        
    }
}