﻿using ersEms.Domain.Register.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Models.Register
{
    public class Emp_regist : ErsModelBase, IEmpRegistCommand
    {
        public int id { get; set; }
        public string mcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.emp_no")]
        public string emp_no { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.desknet_id")]
        public int? desknet_id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.fname")]
        public string fname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.lname")]
        public string lname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.birthday")]
        public DateTime? birthday { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.address")]
        public string address { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.contact_no")]
        public string contact_no { get; set; }


        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.email")]
        public string email { get; set; }


        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.email")]
        public string email_confirm { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.password")]
        public string password { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.password")]
        public string password_confirm { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.position")]
        public EnumPosition? position { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.team")]
        public EnumTeam? team { get; set; }

        public EnumMformat? mformat { get; set; }

        public List<Dictionary<String, object>> team_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Team, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<String, object>> pos_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.position, EnumCommonNameColumnName.namename); }
        }

        public string w_pos
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.position, EnumCommonNameColumnName.namename, (int?)this.position);

            }
        }

        public string w_team
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Team, EnumCommonNameColumnName.namename, (int?)this.team); }
        }


        public string w_pass
        {
            get
            {
                if(this.password.HasValue())
                {
                    return new string('*', this.password.Length);
                }
                return null;
            }
        }

        //public string w_bday
        //{
        //    get
        //    {
        //        var bday = birthday.Date;

        //        if(this.birthday != null)
        //        {
        //            return birthday.Date.ToString("d");
        //        }
        //        return null;
        //    }
        //}


        public int m_flg { get; set; }

    }
}