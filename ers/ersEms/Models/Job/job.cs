﻿using ersEms.Domain.Job.Commands;
using ersEms.Domain.Job.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using System.Collections.Generic;

namespace ersEms.Models.Job
{
    public class Job:ErsModelBase, IJobTitleRegistCommand, IJobTitleRegistMappable
    {
        [BindTable("job_details")]
        public IList<job_details> job_details { get; set; }

        [ErsSchemaValidation("job_title_t.id")]
        [ErsOutputHidden("id")]
        public int? id { get; set; }

        [ErsSchemaValidation("job_title_t.job_title")]
        [ErsOutputHidden]
        public string job_title { get; set; }

        [ErsSchemaValidation("job_title_t.job_description")]
        [ErsOutputHidden("search")]
        public string job_description { get; set; }
     
        public long recordCount { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        [ErsOutputHidden]
        public string emp_pos { get; set; }

        [HtmlSubmitButton]
        public bool noRecord_flg { get; set; }

        [HtmlSubmitButton]
        public bool isRegist { get; set; }

        [HtmlSubmitButton]
        public bool isInitialized { get; set; }
           
    }
}