﻿using ersEms.Domain.Job.Commands;
using ersEms.Domain.Job.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersEms.Models.Job
{
    public class job_details : ErsBindableModel, IJobTitleRegistRecordMappable, IJobTitleRegistRecordCommand
    {
        [ErsSchemaValidation("job_title_t.id")]
        [ErsOutputHidden]
        public int? id { get; set; }

        [ErsSchemaValidation("job_title_t.job_title")]
        [ErsOutputHidden]
        public string job_title { get; set; }

        [ErsSchemaValidation("job_title_t.job_description")]
        [ErsOutputHidden]
        public string job_description { get; set; }

        [ErsSchemaValidation("job_title_t.disp_order")]
        [ErsOutputHidden]
        public int? disp_order { get; set; }

        [HtmlSubmitButton]
        [ErsOutputHidden]
        public bool deleteFlg { get; set; }
        
    }
}