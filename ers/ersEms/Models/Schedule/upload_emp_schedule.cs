﻿using ersEms.Domain.Schedule.Commands;
using jp.co.ivp.ers.mvc;

namespace ersEms.Models.Schedule
{
    public class upload_emp_schedule : ErsBindableModel, IUploadEmpScheduleCommand
    {

        public bool hasError { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public virtual bool chk_find { get; set; }

        [ErsOutputHidden]
        [BindCsvFile]
        public ErsCsvContainer<upload_emp_schedule_record> csv_file { get; set; }

        public string error_messages{ get; set; }

        public override string lineName
        {
            get
            {
                return string.Empty;
            }
        }

    }
}