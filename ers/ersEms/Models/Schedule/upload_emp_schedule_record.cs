﻿using ersEms.Domain.Schedule.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System.ComponentModel;

namespace ersEms.Models.Schedule
{
    public class upload_emp_schedule_record : ErsBindableModel, IUploadEmpScheduleRecordCommand
    {

        [CsvField]
        [DisplayName("sched_owner_id")]
        [ErsSchemaValidation("schedule_t.id")]
        public virtual int? desknet_id { get; set; }

        [CsvField]
        [DisplayName("sched_owner_name")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string sched_owner_name { get; set; }

        [CsvField]
        [DisplayName("sched_id")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string sched_id { get; set; }

        [CsvField]
        [DisplayName("date_start")]
        [ErsSchemaValidation("schedule_t.date_start")]
        public virtual string date_start { get; set; }

        [CsvField]
        [DisplayName("time_start")]
        [ErsSchemaValidation("schedule_t.time_start")]
        public virtual string time_start { get; set; }

        [CsvField]
        [DisplayName("date_end")]
        [ErsSchemaValidation("schedule_t.date_end")]
        public virtual string date_end { get; set; }

        [CsvField]
        [DisplayName("time_end")]
        [ErsSchemaValidation("schedule_t.time_end")]
        public virtual string time_end { get; set; }

        [CsvField]
        [DisplayName("sched_type")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string sched_type { get; set; }

        [CsvField]
        [DisplayName("sched_detail")]
        [ErsUniversalValidation(type = CHK_TYPE.All, prohibitionChars = "￥”’＜＞、；")]
        public virtual string sched_details { get; set; }

        [CsvField]
        [DisplayName("location")]
        [ErsSchemaValidation("schedule_t.sched_location")]
        public virtual string sched_location { get; set; }

        [CsvField]
        [DisplayName("location_details")]
        [ErsUniversalValidation(type = CHK_TYPE.All, prohibitionChars = "￥”’＜＞、；")]
        public virtual string sched_location_details { get; set; }

        [CsvField]
        [DisplayName("details")]
        [ErsUniversalValidation(type = CHK_TYPE.All, prohibitionChars = "￥”＜＞、；")]
        public virtual string details { get; set; }
        
       
        [CsvField]
        [DisplayName("owner_id")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string owner_id { get; set; }

        public override string lineName
        {
            get
            {
                return string.Join(" ","Line", base.lineNumber);
            }
        }

    }
}