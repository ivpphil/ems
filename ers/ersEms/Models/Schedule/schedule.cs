﻿using ersEms.Domain.Schedule.Commands;
using ersEms.Domain.Schedule.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;

namespace ersEms.Models.Schedule
{
    public class schedule : ErsModelBase, IScheduleMemberMappable, IScheduleSearchMappable, IEmployeeScheduleCommand
    {
        [ErsOutputHidden]
        [ErsUniversalValidation]
        public List<string> scheduleHeaderList { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime date { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation]        
        public List<Dictionary<string, object>> main_schedule_list { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation]
        public string search { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation (type = CHK_TYPE.Numeric)]
        public int selectedField { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation]
        public int time_start { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation]
        public int time_end { get; set; }

        public long recordCount { get; set; }
        
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? s_schedule_date { get; set; }
        
        public Dictionary<int,string> timeList
        {
            get
            {
                TimeSpan startTime = new TimeSpan(8, 0, 0);                
                return ErsFactory.ersRequestFactory.GetScheduleTimeIntervalStgy().GetTimeInterval(startTime, 30);               
            }
            set { }
        }

        public List<Dictionary<string, object>> searchByList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.EmployeeSearch, EnumCommonNameColumnName.namename); }
        }
    }
}