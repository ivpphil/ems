﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersEms.Models.Employee
{
    public class emp_profile : ErsModelBase, IEmpProfileMappable
    { 
        public bool hasImageFile { get; set; }

        public ErsEmployee employee { get; set; }

        public bool hasImage { get; set; }

        [ErsSchemaValidation("employee_t.emp_no")]
        public string profile_emp_no
        {
            get;set;
        }

        public EnumPosition? position { get; set; }
    }
}