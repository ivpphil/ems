﻿using ersEms.Domain.Employee.Commands;
using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.common;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ersEms.Models.Employee
{
    public class emp_profile_edit : ErsModelBase, IEmpProfileEditMappable, IEmpProfileEditCommand
    {
        [ErsSchemaValidation("employee_t.emp_no")]
        public string profile_emp_no { get; set; }

        public ErsEmployee old_records { get { return ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(profile_emp_no); } }

        public List<string> employee_correction_list { get; set; }

        [BindPicture(EnumPictureType.JPEG, EnumPictureType.PNG, EnumPictureType.GIF)]
        public HttpPostedFileBase img_file { get; set; }

        public bool hasImage { get; set; }

        public bool hasTempFile { get; set; }

        [ErsOutputHidden("edit")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string temp_file_name { get; set; }

        [ErsOutputHidden("edit")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string image_file { get; set; }

        [ErsOutputHidden("edit_confirm")]
        [ErsSchemaValidation("employee_t.fname")]
        public string fname { get; set; }

        [ErsOutputHidden("edit_confirm")]
        [ErsSchemaValidation("employee_t.lname")]
        public string lname { get; set; }

        [ErsOutputHidden("edit_confirm")]
        [ErsSchemaValidation("employee_t.email")]
        public string email { get; set; }

        [ErsOutputHidden("edit_confirm")]
        [ErsSchemaValidation("employee_t.email")]
        public string email_confirm { get; set; }

        [DisplayName("new_passwd")]
        [ErsOutputHidden("edit_confirm")]
        [ErsSchemaValidation("employee_t.password")]
        public string password { get; set; }

        public string w_pass {
            get
            {
                if (password.HasValue())
                {
                    return String.Concat(Enumerable.Repeat("*", password.Count()));
                }
                return null;
            }
        }

        [DisplayName("passwd_confirm")]
        [ErsOutputHidden("edit_confirm")]
        [ErsSchemaValidation("employee_t.password")]
        public string password_confirm { get; set; }

        [DisplayName("passwd")]
        [ErsOutputHidden("edit_confirm")]
        [ErsSchemaValidation("employee_t.password")]
        public string current_password { get; set; }

        [ErsOutputHidden("edit_confirm")]
        [ErsSchemaValidation("employee_t.address")]
        public string address { get; set; }

        [ErsOutputHidden("edit_confirm")]
        [ErsSchemaValidation("employee_t.birthday")]
        public DateTime? birthday { get; set; }

        public string w_birthday
        {
            get
            {
                if (birthday != null)
                {
                    return ((DateTime)birthday).ToString("MM/dd/yyyy");
                }
                return null;
            }
        }


        [ErsOutputHidden("edit_confirm")]
        [ErsSchemaValidation("employee_t.gender")]
        public EnumSex? gender { get; set; }


        public string w_gender
        {
            get
            {
                if (gender != null)
                {
                    var namecode = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType.Gender, (int?)gender);
                    if (namecode != null)
                    {
                        return namecode.namename;
                    }
                }
                return null;
            }
        }

        public IList<ErsCommonNameCode> GenderList
        {
            get
            {
                var repo = ErsFactory.ersCommonFactory.GetErsCommonNameCodeRepository();
                var cri = ErsFactory.ersCommonFactory.GetErsCommonNameCodeCriteria();
                cri.type_code = EnumCommonNameType.Gender;
                cri.active = EnumActive.Active;

                var list = repo.Find(cri);
                if (list != null)
                {
                    return list;
                }
                return null;
            }
        }

        [ErsOutputHidden("edit_confirm")]
        [ErsSchemaValidation("employee_t.contact_no")]
        public string contact_no { get; set; }

        public string temp_path { get; set; }

        public string stored_img_path { get; set; }

        public EnumPosition? position { get; set; }

        public string reasonStr { get; set;}

        public bool imageChanged { get; set; }

        public bool passChanged { get; set; }

        public bool noChange
        {
            get
            {
                return (!imageChanged && !passChanged && !reasonStr.HasValue());
            }

        }
    }
}