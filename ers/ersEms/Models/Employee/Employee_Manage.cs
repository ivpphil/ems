﻿using ersEms.Domain.Employee.Commands;
using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;

namespace ersEms.Models.Employee_Manage
{
    public class Employee_Manage : ErsModelBase, IEmpManageCommand, IEmpManageMappable
    {
        [ErsOutputHidden("emp_manage")]
        [ErsSchemaValidation("employee_t.emp_no")]
        public string emp_no { get; set; }

        [ErsOutputHidden("emp_manage")]
        [ErsSchemaValidation("employee_t.fname")]
        public string fname { get; set; }

        [ErsOutputHidden("emp_manage")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string full_name { get; set; }

        [ErsOutputHidden("emp_manage")]
        [ErsSchemaValidation("employee_t.lname")]
        public string lname { get; set; }

        //[ErsOutputHidden]
        //[ErsSchemaValidation("employee_t.position")]
        //public EnumPosition? position { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.job_title")]
        public int? job_title { get; set; }
        
        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.team")]
        public EnumTeam? team { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.status")]
        public EnumEmpStatus? status { get; set; }

        [ErsSchemaValidation("leave_balance_t.total_vl")]
        public double vacation_leave { get; set; }

        [ErsSchemaValidation("leave_balance_t.total_sl")]
        public double sick_leave { get; set; }

        public string w_team
        {
            get
            {
                if (team.HasValue)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Team, EnumCommonNameColumnName.namename, (int?)this.team);
                }
                return ErsResources.GetMessage("NotSet");
            }
        }

        public string w_status
        {
            get
            {
                if (status.HasValue)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Emp_Status, EnumCommonNameColumnName.namename, (int?)this.status);
                }
                return ErsResources.GetMessage("NotSet");

            }
        }
        public List<Dictionary<String, object>> team_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Team, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<String, object>> status_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Emp_Status, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<String, object>> pos_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.position, EnumCommonNameColumnName.namename); }
        }
    }
}