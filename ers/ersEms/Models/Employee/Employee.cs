﻿using ersEms.Domain.api.Mappables;
using ersEms.Domain.Employee.Commands;
using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.Web;

namespace ersEms.Models.Employee
{
    public class Employee : ErsModelBase, IEmpImageMappable, IRemoveTeamMemberCommand, IAddToTeamCommand, IEmployeeProjectMappable,IGetImageMappable

    {
        public ErsPagerModel pager { get; set; }

        public ErsPagerModel non_team_pager { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int non_team_page_count { get; set; }

        public long recordCount { get; set; }

        public long non_team_recordCount { get; set; }

        public int? sent_message_count { get; set; }

        [ErsOutputHidden("fromEmpList","update")]
        [ErsSchemaValidation("employee_t.emp_no")]
        public string emp_no { get; set; }


        [ErsOutputHidden("fromEmpList", "emp_profile")]
        [ErsSchemaValidation("employee_t.fname")]
        public string fname { get; set; }

        [ErsOutputHidden("fromEmpList", "emp_profile")]
        [ErsSchemaValidation("employee_t.lname")]
        public string lname { get; set; }

        [ErsOutputHidden("emp_profile")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public DateTime? birthday { get; set; }

        [ErsOutputHidden("emp_profile")]
        [ErsSchemaValidation("employee_t.address")]
        public string address { get; set; }


        [ErsOutputHidden("emp_profile")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public string contact_no { get; set; }

        [ErsOutputHidden("fromEmpList", "update")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public EnumPosition? position { get; set; }

        [ErsOutputHidden("update")]
        [ErsSchemaValidation("employee_t.team")]
        public EnumTeam? team { get; set; }

        [ErsOutputHidden("fromEmpList", "update")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public EnumEmpStatus? status { get; set; }

        [ErsOutputHidden("emp_profile")]
        [ErsSchemaValidation("employee_t.password")]
        public string password { get; set; }

        [ErsOutputHidden("emp_profile")]
        [ErsSchemaValidation("employee_t.password")]
        public string password_confirm { get; set; }

        [ErsOutputHidden("emp_profile")]
        [ErsSchemaValidation("employee_t.email")]
        public string email { get; set; }

        [ErsOutputHidden("emp_profile")]
        [ErsSchemaValidation("employee_t.email")]
        public string email_confirm { get; set; }

        [ErsOutputHidden("emp_profile")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public int? gender { get; set; }

        [ErsOutputHidden("emp_profile")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public EnumProfile? prof { get; set; }
        
        [HtmlSubmitButton]
        public bool from_prof { get; set; }

        [ErsOutputHidden("emp_profile")]
        [HtmlSubmitButton]
        public bool emp_regist { get; set; }

        [ErsOutputHidden("emp_profile")]
        [HtmlSubmitButton]
        public bool emp_update { get; set; }

        public virtual EnumPosition? emp_pos { get; set; }

        public Dictionary<string, object> ImagesList { get; set; }

        public List<Dictionary<string, object>> list { get; set; }

        public List<Dictionary<string, object>> sexList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Gender, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<String, object>> team_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Team, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<String, object>> status_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Emp_Status, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<String, object>> pos_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.position, EnumCommonNameColumnName.namename); }
        }

        public string w_gender
        {
            get
            {
                if (gender.HasValue)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Gender, EnumCommonNameColumnName.namename, (int?)this.gender);
                }
                return ErsResources.GetMessage("NotSet");
            }
        }

        public string w_pos
        {
            get
            {
                if (position.HasValue)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.position, EnumCommonNameColumnName.namename, (int?)this.position);
                }
                return ErsResources.GetMessage("NotSet");
           
            }
        }

        public string w_team
        {
            get
            {
                if (team.HasValue)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Team, EnumCommonNameColumnName.namename, (int?)this.team);
                }
                return ErsResources.GetMessage("NotSet");
            }
        }


        public string w_status
        {
            get
            {
                if (status.HasValue)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Emp_Status, EnumCommonNameColumnName.namename, (int?)this.status);
                }
                return ErsResources.GetMessage("NotSet");

            }
        }

        public string w_pass
        {
            get
            {
                if (this.password.HasValue())
                {
                    return new string('*', this.password.Length);
                }
                return null;
            }
        }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.emp_no")]
        public string update_emp { get; set; }

        [BindPicture(2147483647, new[] { EnumPictureType.JPEG, EnumPictureType.PNG, EnumPictureType.GIF })]
        public HttpPostedFileBase imgfile { get; set; }

        [ErsOutputHidden("emp_profile")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string temp_file_name { get; set; }

        [ErsOutputHidden("emp_profile")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string image_file { get; set; }

        [HtmlSubmitButton]
        public bool img_file_exist { get; set; }

        [HtmlSubmitButton]
        public bool ImgInDB { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.emp_no")]
        public string s_emp_no { get; set; }

        public List<Dictionary<string, object>> non_team_list { get; set; }

        public List<Dictionary<string, object>> projectList { get; set; }

        [HtmlSubmitButton]
        [ErsOutputHidden("fromEmpList")]
        public virtual bool fromEmpList { get; set; }

        [HtmlSubmitButton]
        public bool showEmpDetails { get; set; }

        [HtmlSubmitButton]
        public bool MemberSearch { get; set; }

        public int? projectCount { get; set; }

        public string w_birthday
        {
            get
            {
                if (birthday != null)
                {
                    string dt = Convert.ToDateTime(birthday).ToShortDateString();    //converted the birthday to remove the time
                    if (dt == DateTime.MinValue.ToShortDateString())     //if the birthday(dt) is equal to default datetime(1/1/0001), it will return "Not Set" word
                    {
                        dt = "Not Set";
                    }

                    return dt;
                }
                return ErsResources.GetMessage("NotSet");
            }
        }

        public DateTime? utime { get; set; }        

        public List<Dictionary<string, object>> searchFieldList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.EmployeeSearch, EnumCommonNameColumnName.namename); }
        }

        [ErsUniversalValidation]
        public bool hasImage { get; set; }

        [ErsUniversalValidation]
        public string fullname {get;set;}
    }
}