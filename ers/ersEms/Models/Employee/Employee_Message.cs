﻿using ersEms.Domain.Employee.Commands;
using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace ersEms.Models.Employee
{
    public class Employee_Message : ErsModelBase, IEmpMessageMappable, IEmpMessageCommand
    {
        public List<Dictionary<string, object>> threads { get; set; }

        public List<Dictionary<string, object>> messages { get; set; }

        public bool messageNotEmpty
        {
            get
            {
                return messages != null && this.messages.Count > 0;
            }
        }


        [ErsUniversalValidation(type = CHK_TYPE.All,rangeTo =10)]
        public string inbox_emp_no { get; set; }

        public bool image_exists
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                var file_path = setup.image_directory + "\\" + this.inbox_emp_no + "\\" + this.inbox_emp_no + ".jpg";
                if (File.Exists(file_path))
                {
                    return true;
                }
                return false;
            }
        }

        [ErsSchemaValidation("emp_thread_details_t.thread_no")]
        public int? thread_no { get; set; }

        public string emp_no { get { return ErsContext.sessionState.Get("mcode"); } }

        [ErsSchemaValidation("emp_thread_t.subject")]
        public string subject { get; set; }

        [ErsSchemaValidation("emp_thread_details_t.message")]
        public string message { get; set; }

        [HtmlSubmitButton]
        public bool send { get; set; }

        [HtmlSubmitButton]
        public bool delete { get; set; }

        [HtmlSubmitButton]
        public bool write_new { get; set; }

        [ErsUniversalValidation(type=CHK_TYPE.Numeric, isArray =true)]
        public int?[] delete_thread_array { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool fromEmpList { get; set; }

        [ErsSchemaValidation("emp_thread_t.recipient_emp_no")]
        public string recipient_emp_no { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string recipient_name
        {
            get
            {

                var returnStr = String.Empty;
                var emp = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(this.recipient_emp_no);
                var converter = CultureInfo.CurrentCulture.TextInfo;
                if (emp != null)
                {
                    returnStr = String.Concat(converter.ToTitleCase(emp.fname.ToLower()), " ", converter.ToTitleCase(emp.lname.ToLower()));
                }
                return returnStr;
            }
        }

        public List<Dictionary<string, object>> list_employee
        {
            get
            {
                return ErsFactory.ersEmployeeFactory.GetEmployeeListForDropDown();
            }
        }

        public bool recipientImageExist
        {
            get
            {
                return ErsFactory.ersEmployeeFactory.CheckEmpImageExist(this.recipient_emp_no);
            }
        }
    }
}