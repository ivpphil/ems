﻿using ersEms.Domain.Employee.Commands;
using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace ersEms.Models.Employee
{
    public class Employee_Search : Employee, IEmpSearchMappable, IEmpSearchCommand, IEmpCsvMappable, ITeamManagementMappable
    {
        public ErsCsvCreater csvCreater { get; set; }
        
        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.fname")]
        public string s_fname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.lname")]
        public string s_lname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.email")]
        public string s_email { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.team")]
        public string s_team { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.position")]
        public string s_position { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.position")]
        public override EnumPosition? emp_pos { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.emp_no")]
        public string user_emp_no { get; set; }

        string _s_keyword = string.Empty;
        
        [HtmlSubmitButton]
        [ErsOutputHidden("fromEmpList")]
        public override bool fromEmpList { get; set; }

        [HtmlSubmitButton]
        [ErsOutputHidden]
        public bool errorBackFromUpload { get; set; }

        [HtmlSubmitButton]
        [ErsOutputHidden]
        public bool hasInformation { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public EnumEmpSearch? searchField { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public EnumTeam? SearchTeam { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public EnumPosition? SearchPos { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string s_keyword
        {
            get
            {
                return _s_keyword;
            }

            set
            {
                _s_keyword = value;
                switch (this.searchField)
                {
                    case EnumEmpSearch.EmpNo:
                        this.s_emp_no = value;
                        break;
                    case EnumEmpSearch.EmpEmail:
                        this.s_email = value;
                        break;
                    case EnumEmpSearch.FirstName:
                        this.s_fname = value;
                        break;
                    case EnumEmpSearch.LastName:
                        this.s_lname = value;
                        break;
                }
                switch(this.SearchTeam)
                {
                    case EnumTeam.CSharp:
                        this.s_team = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Team, EnumCommonNameColumnName.namename, (int?)EnumTeam.CSharp);
                        break;
                    case EnumTeam.FrontEnd:
                        this.s_team = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Team, EnumCommonNameColumnName.namename, (int?)EnumTeam.FrontEnd);
                        break;
                    case EnumTeam.Administrative:
                        this.s_team = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Team, EnumCommonNameColumnName.namename, (int?)EnumTeam.Administrative);
                        break;
                    default:
                        this.s_team = "";
                        break;
                }
                switch (this.SearchPos)
                {
                    case EnumPosition.Member:
                        this.s_position = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.position, EnumCommonNameColumnName.namename, (int?)EnumPosition.Member);
                        break;
                    case EnumPosition.TeamLeader:
                        this.s_position = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.position, EnumCommonNameColumnName.namename, (int?)EnumPosition.TeamLeader);
                        break;
                    case EnumPosition.Administrator:
                        this.s_position = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.position, EnumCommonNameColumnName.namename, (int?)EnumPosition.Administrator);
                        break;
                    default:
                        this.s_position = "";
                        break;
                }
            }
        }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All, isArray = true)]
        public string[] emp_no_arr { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string emp_no_string { get; set; }

    }
}