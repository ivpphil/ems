﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System;

namespace ersEms.Models.Employee
{
    public class EmpCsv:ErsModelBase
    {
        [CsvField]
        public string emp_no { get; set; }

        [CsvField]
        public string fname { get; set; }

        [CsvField]
        public string lname { get; set; }

        [CsvField]
        public string email { get; set; }

        [CsvField]
        public string position { get; set; }

        [CsvField]
        public string team { get; set; }

        [CsvField]
        public int? desknet_id { get; set; }

        [CsvField]
        public string team_leader { get; set; }

        [CsvField]
        public string date_hired { get; set; }

        [CsvField]
        public string reg_date { get; set; }

        [CsvField]
        public double? total_vl { get; set; }

        [CsvField]
        public double? total_sl { get; set; }

        [CsvField]
        public string sss_no { get; set; }

        [CsvField]
        public string tin_no { get; set; }

        [CsvField]
        public string job_title { get; set; }
    }
}