﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Models.Employee
{
    public class Emp_skills_detail: ErsBindableModel,IEmp_SkillsdetailCommand
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("emp_skills_t.id")]
        public int? id { get; set; }


        [ErsOutputHidden]
        [ErsSchemaValidation("emp_skills_t.emp_no")]
        public string emp_no { get; set; }


        [ErsOutputHidden]
        [ErsSchemaValidation("emp_skills_t.skill_desc")]
        public string skill_desc { get; set; }


        [ErsOutputHidden]
        [ErsUniversalValidation(type =CHK_TYPE.All)]
        public int? years_exp { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("emp_skills_t.remarks")]
        public string remarks { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("emp_skills_t.in_time")]
        public DateTime? in_time { get; set; }






    }
}