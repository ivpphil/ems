﻿using ersEms.Domain.Employee.Commands;
using ersEms.Models.csv;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using System.IO;

namespace ersEms.Models.Employee
{
    public class EmpCsvUpload : ErsBindableModel, IEmpCsvUploadCommand
    {
        public EmpCsvUpload()
        {
            this.CreateFolder();
        }

        [ErsOutputHidden]
        [BindCsvFile]
        public ErsCsvContainer<EmployeeCSV> csv_file { get; set; }

        [HtmlSubmitButton]
        public bool pager { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public virtual bool chk_find { get; set; }

        [HtmlSubmitButton]
        public virtual bool csv_btn { get; set; }

        public void CreateFolder()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var csvpath = "ersEms\\file_upload\\";
            var root = setup.root_path;
            var temppath = root + csvpath;

            if (!Directory.Exists(temppath))
            {
                Directory.CreateDirectory(temppath);
            }
        }
    }    
}