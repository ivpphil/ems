﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Models.Employee
{
    public class Emp_skills:ErsModelBase,IEmp_SkillsCommand
    {
        [ErsOutputHidden]
        [BindTable("Emp_skills_detail")]
        public IList<Emp_skills_detail> Emp_skills_detail { get; set; }
    }
}