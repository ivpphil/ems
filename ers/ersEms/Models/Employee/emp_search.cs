﻿using ersEms.Domain.Employee.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.job;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using System.Collections.Generic;

namespace ersEms.Models.Employee
{
    public class emp_search : ErsModelBase, IEmpSearchTempMappable
    {

        public int pageCnt { get; set; }

        public int maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        public ErsPagerModel pager { get; set; }

        public long recordCount { get; set; }

        public IList<ErsEmployee> emp_list { get; set; }

        public bool hasNoRecord { get; set; }

        public string s_keyword { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 4)]
        public EnumEmpSearch? emp_search_by { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 6)]
        public EnumTeam? team_search { get; set; }
 
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? job_title_search { get; set; }

        public IList<ErsJobTitle> job_title_list {
            get
            {
                var repo = ErsFactory.ersJobFactory.GetErsJobTitleRepository();
                var crit = ErsFactory.ersJobFactory.GetErsJobTitleCriteria();
                crit.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
                var list = repo.Find(crit);

                return list;

            }
        }        

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 10)]
        public EnumPositionTitle? position_title_search { get; set; }

        public bool isHR
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                var emp_no = ErsContext.sessionState.Get("mcode");
                if (setup.hr_emp_no == emp_no)
                {
                    return true;
                }
                return false;
            }
        }


    }
}