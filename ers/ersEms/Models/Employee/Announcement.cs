﻿using ers.jp.co.ivp.ers;
using ersEms.Domain.Announcement.Commands;
using ersEms.Domain.Announcement.Mappables;
using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Models.Employee
{
    public class Announcement : ErsModelBase, IAnnouncementMappable, IAnnouncementCommand, IEditAnnouncementCommand
    {

        //Property on Inputting Details

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string emp_no { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string news { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string fullname { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime in_time { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string image_file { get; set; }

        //Property on Mapping all the announcement 

        public IList<ErsAnnouncementList> an_list { get; set; }

        public ErsPagerModel pager { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().AnnouncementListItemNumberOnPage; } }

        public long maxLineCount { get { return ErsFactory.ersUtilityFactory.getSetup().AnnouncementListItemNumberOnOneLine; } }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long recordCount { get; set; }

        //Property when updating announcement
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string edit_news { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string id { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public EnumModtype? modtype { get; set; }

        public List<Dictionary<string, object>> ListTimeInOut { get; set;}

    }

    public class ErsAnnouncementList : ErsModelBase
    {
        public virtual int id {get;set;}

        public virtual string emp_no { get; set; }

        public virtual string fullname { get; set; }

        public virtual string news { get; set; }

        public virtual DateTime in_time { get; set; }

        public virtual DateTime? u_time { get; set; }

        public virtual int status { get; set; }

        public virtual string image_file { get; set; }

    }
}