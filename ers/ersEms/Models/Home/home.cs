﻿using ers.jp.co.ivp.ers;
using ersEms.Domain.Home.Commands;
using ersEms.Domain.Home.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ersEms.Models.Home
{
    public class home : ErsModelBase, IHomeAnnouncementCommand, IHomeAnnouncementMappable, IHomeEmpMappable, IHomeDailyReportDataMappable, IEmployeeBirthdayDataMappable
    {

        public bool hasChanges { get; set; }

        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().AnnouncementListItemNumberOnPage; } }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public int totalPager { get; set; }

        public long recordCount { get; set; }


        public string fullname { get; set; }
        public string image_path { get; set; }
        public string emp_no { get { return ErsContext.sessionState.Get("mcode"); } }

        [DisplayName("announcement_id")]
        [ErsSchemaValidation("announcement_t.id")]
        public int? id { get; set; }

        [ErsSchemaValidation("announcement_t.news")]
        public string news { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public EnumAnnouncementCmdtype? announcementCmdType { get; set; }

        public IList<Dictionary<string, object>> announcementList { get; set; }
        public List<Dictionary<string, object>> projectList { get; set; }

        public int projectCount { get; set; }


        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public bool isDefaultLoad { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public bool loadGraph { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public EnumReportSummary? report_summary { get; set; }

        public List<Dictionary<string,object>> daily_report_list { get; set; }

        public List<Dictionary<string,object>> report_summary_list
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ReportSummary, EnumCommonNameColumnName.namename);
            }
        }

       public List<Dictionary<string, object>> time_in_out_list { get; set; }

       public List<Dictionary<string, object>> birthday_list { get; set; }

    }
}