﻿using ersEms.Domain.Pdf.Mappables;
using ersEms.Models.Pdf.pdf;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.Pdf;
using System;
using System.Collections.Generic;

namespace ersEms.Models.Pdf
{
    public class download_request : ErsModelBase, IRequestPdfMappable
    {
        [ErsUniversalValidation(type = CHK_TYPE.All,isArray = true)]
        public string[] request_id { get; set; }

        public List<ErsModelBase> pdf_template { get; set; }
        
        public ErsPdfForm form { get; set; }

        public string file_name { get { return string.Concat("Form_", DateTime.Now.ToString("yyyyMMddHHmms")); } }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string single_download_request { get; set; }        
    }

}