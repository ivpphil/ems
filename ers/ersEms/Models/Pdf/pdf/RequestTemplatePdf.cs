﻿using ersEms.Domain.Pdf.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ersEms.Models.Pdf.pdf
{
    public class RequestTemplatePdf : ErsModelBase, IRequestDetailPdfMappable
    {
        public int request_id { get; set; }

        [PdfField]
        public EnumRequestType? request_type { get; set; }

        [PdfField]
        public string emp_no { get; set; }

        [PdfField]
        public EnumStatusRequest? status { get; set; }

        [DisplayName("leave_type")]
        [PdfField(request_types = new[] { (int)EnumRequestType.Leave })]
        public string w_leave_type { get; set; }

        public EnumLeaveType _leave_type { get; set; }
        public EnumLeaveType leave_type
        {
            get
            {
                return _leave_type;
            }
            set
            {
                var namecode = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType.LeaveType, (int?)value);
                if (namecode != null)
                {
                    w_leave_type = namecode.namename;
                }
                _leave_type = value;
            }
        }


        [PdfField]
        public string period
        {
            get
            {
                string date_start = string.Concat(this.date_start.ToShortDateString(), " ", this.time_start.ToShortTimeString());
                string date_end = string.Concat(this.date_start.ToShortDateString(), " ", this.time_end.ToShortTimeString());

                return date_start + " ~ " + date_end;
            }
        }

        [PdfField]
        public string reason { get; set; }

        public virtual DateTime date_start { get; set; }

        public virtual DateTime date_end { get; set; }

        public virtual DateTime time_start { get; set; }

        public virtual DateTime time_end { get; set; }

        [PdfField(request_types = new[] { (int)EnumRequestType.Leave })]
        public double? vl_balance { get; set; }

        [PdfField(request_types = new[] { (int)EnumRequestType.Leave })]
        public double? sl_balance { get; set; }

        [PdfField]
        public string duration
        {
            get
            {
                TimeSpan dur = this.time_end.Subtract(this.time_start);
                var duration = dur.TotalHours.ToString() +" hour(s)";
                return duration;
            }
        }

        [PdfField]
        public string schedule {get;set;}
    }
}