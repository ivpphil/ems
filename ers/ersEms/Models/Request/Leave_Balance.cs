﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersEms.Models.Request
{
    public class Leave_Balance : Approver
    {
        [ErsOutputHidden("leave_credits")]
        [ErsSchemaValidation("leave_balance_t.last_vl")]
        public virtual double? last_vl { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("leave_balance_t.now_vl")]
        public virtual double? now_vl { get; set; }

        [ErsOutputHidden("leave_credits")]
        [ErsSchemaValidation("leave_balance_t.total_vl")]
        public virtual double? total_vl { get; set; }

        [ErsOutputHidden("leave_credits")]
        [ErsSchemaValidation("leave_balance_t.last_sl")]
        public virtual double? last_sl { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("leave_balance_t.now_sl")]
        public virtual double? now_sl { get; set; }

        [ErsOutputHidden("leave_credits")]
        [ErsSchemaValidation("leave_balance_t.total_sl")]
        public virtual double? total_sl { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("leave_balance_t.reason")]
        public override string reason { get; set; }

    }
}