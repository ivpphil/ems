﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;

namespace ersEms.Models.Request
{
    public class logsheet_details : ErsBindableModel, IAttendanceLogsheetDetailsCommand
    {
        [ErsSchemaValidation("request_t.id")]
        public virtual int? id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.date_start")]
        public virtual string date_start { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.date_end")]
        public virtual DateTime? date_end
        {
            get
            {
                if (time_start != null && time_end != null && date_start != null)
                {
                    DateTime getDate = Convert.ToDateTime(date_start);
                    if (start_period == EnumClockPeriod.PM && end_period == EnumClockPeriod.AM)
                    {
                        return getDate.AddDays(1);
                    }
                }

                return null;
            }
        }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.time_start")]
        public virtual DateTime? time_start
        {
            get
            {
                DateTime t_start = DateTime.Now;
                if (start_hours.HasValue() && start_minutes.HasValue() && start_period.HasValue)
                {
                    t_start = Convert.ToDateTime(start_hours + ":" + start_minutes + start_period);
                }
                else
                {
                    return null;
                }
                return t_start;
            }
            set { }
        }
        
        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.time_end")]
        public virtual DateTime? time_end
        {
            get
            {
                DateTime t_end = DateTime.Now;

                if (end_hours.HasValue() && end_minutes.HasValue() && end_period.HasValue)
                {
                    t_end = Convert.ToDateTime(end_hours + ":" + end_minutes + end_period);

                    if (end_period == EnumClockPeriod.AM)
                    {
                        t_end = t_end.AddDays(1);
                    }

                }
                else
                {
                    return null;
                }
                return t_end;
            }
            set { }
        }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string shift_schedule { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string start_hours { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string start_minutes { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public EnumClockPeriod? start_period { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string end_hours { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string end_minutes { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public EnumClockPeriod? end_period { get; set; }
        
        public virtual string s_time_start
        {
            get
            {
                string t_start = "";
                if (time_start != null)
                {
                    t_start = time_start.Value.ToString("hh:mm tt");
                }
                return t_start;
            }
            set { }
        }

        public virtual string s_time_end
        {
            get
            {
                string t_end = "";
                if (time_end != null)
                {
                    t_end = time_end.Value.ToString("hh:mm tt");
                }
                return t_end;
            }
            set { }
        }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.reason")]
        public virtual string reason { get; set; }
    }
}