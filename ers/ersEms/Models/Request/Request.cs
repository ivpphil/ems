﻿using ersEms.Domain.Request.Commands;
using ersEms.Domain.Request.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.Web;

namespace ersEms.Models.Request
{
    public class Request : Leave_Balance, IReceiveRequestCommand, IEmployeeDetailsRequestMappable, IUndertimeCommand, IAttendanceLogsheetCommand, IRequestMappable, IOvertimeCommand, IUpdateRequestStatusCommand, ILeaveRequestCommand, ILeaveRequestMappable, IRequestSearchCommand
    {
        public Request()
        {
            date_filed = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 00:00:00");
        }

        [ErsSchemaValidation("request_t.id")]
        public override int? request_id { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string str_request_id { get; set;}

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.emp_no")]
        public override string emp_no { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.fname")]
        public virtual string fname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.lname")]
        public virtual string lname { get; set; }

        public EnumPosition? position { get; set; }

        [ErsOutputHidden("ut")]
        [ErsSchemaValidation("request_t.request_type")]
        public virtual EnumRequestType? request_type { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.date_filed")]
        public virtual DateTime? date_filed { get; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.status")]
        public virtual EnumStatusRequest? status { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.reason")]
        public override string reason { get; set; }
        
        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.date_start")]
        public virtual DateTime? date_start { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public virtual DateTime? ut_date { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.date_end")]
        public virtual DateTime? date_end { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.time_start")]
        public virtual string time_start { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.time_end")]
        public virtual string time_end { get; set; }

        [BindPicture(new[] { EnumPictureType.JPEG,EnumPictureType.GIF,EnumPictureType.PNG})]
        public HttpPostedFileBase sick_file_proof { get; set; }

        [BindPicture(new[] { EnumPictureType.JPEG, EnumPictureType.GIF, EnumPictureType.PNG })]
        public HttpPostedFileBase paternal_file_proof { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string temp_file_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.leave_type")]
        public virtual EnumLeaveType? leave_type { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.cancel_reason")]
        public virtual string cancel_reason { get; set; }

        [ErsOutputHidden("leave_credits")]
        [ErsSchemaValidation("request_t.used_leave")]
        public virtual double used_leave { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string undertime_hours { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string start_hours { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string start_minutes { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 2)]
        public EnumClockPeriod? start_period { get; set; }
        
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string end_hours { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string end_minutes { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 2)]
        public EnumClockPeriod? end_period { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual EnumUndertimeReason? ut_reason_type { get; set; }

        public List<Dictionary<String, object>> undertime_reason_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.UndertimeReason, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<String, object>> clock_period_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ClockPeriod, EnumCommonNameColumnName.namename); }
        }

        public List<string> hr_list
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewTimeService().GetListHour();
            }
        }

        public List<string> mn_list
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewTimeService().GetListMinutes();
            }
        }

        public string w_date_filed
        {
            get
            {
                if (date_filed != null)
                {
                    return date_filed.Value.ToShortDateString();
                }

                return String.Empty;
            }
        }

        public string w_time_start
        {
            get
            {
                if (start_hours.HasValue() && start_minutes.HasValue())
                {
                    return start_hours + ":" + start_minutes + " " + start_period;
                }

                return null;
            }
        }

        public string w_time_end
        {
            get
            {
                if (end_hours.HasValue() && end_minutes.HasValue())
                {
                    return end_hours + ":" + end_minutes + " " + end_period;
                }

                return null;
            }
        }

        public string w_reason
        {
            get
            {
                if (this.ut_reason_type.HasValue && ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().ExistValue(EnumCommonNameType.UndertimeReason, (int?)this.ut_reason_type))
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.UndertimeReason, EnumCommonNameColumnName.namename, (int?)this.ut_reason_type) + ": " + reason;
                }
                return null;
            }
        }

        public string w_ut_date
        {
            get
            {
                if (ut_date != null)
                {
                    return ut_date.Value.ToShortDateString();
                }

                return String.Empty;
            }
        }

        //Logsheet start
        [BindTable("logsheet_details")]
        public IList<logsheet_details> logsheet_details { get; set; }

        public string fullname { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public string contact_no { get; set; }

        public bool noRecord_flg { get; set; }

        public List<string> logsheet_mn_list
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewTimeService().GetListEveryMinute();
            }
        }

        public List<Dictionary<string, object>> pending_list { get; set; }

        public List<Dictionary<string, object>> my_approved_list { get; set; }

        public List<Dictionary<string, object>> others_approved_list { get; set; }

        [BindTable("overtime_details")]
        public IList<overtime_details> overtime_details { get; set; }

        //Leave Request
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual bool half_day_start { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual bool half_day_end { get; set; }

        [HtmlSubmitButton]
        public virtual bool IsReturn { get; set; }

        public List<Dictionary<string, object>> leaveList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.LeaveType, EnumCommonNameColumnName.namename);
            }
        }

        public virtual string w_leave
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.LeaveType, EnumCommonNameColumnName.namename, (int?)this.leave_type);
            }
        }

        public virtual string w_pos
        {
            get
            {
                if (position.HasValue)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.position, EnumCommonNameColumnName.namename, (int?)this.position);
                }
                return ErsResources.GetMessage("NotSet");
            }
        }
                
        #region search 

        [ErsUniversalValidation(type = CHK_TYPE.All, prohibitionChars = "$")]
        public virtual int?[] s_request_type
        {
            get; set;
        }

        public List<Dictionary<string, object>> requestTypeList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.RequestType, EnumCommonNameColumnName.namename);
            }
        }

        #endregion

        public virtual int? pendingCount { get; set; }

        public virtual int? myApprovedCount { get; set; }

        public virtual int? otherApprovedCount { get; set; }

        [HtmlSubmitButton]
        public virtual bool isHR { get; set; }


    }
}