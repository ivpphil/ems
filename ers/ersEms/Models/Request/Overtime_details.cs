﻿using ersEms.Domain.Request.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.ComponentModel;

namespace ersEms.Models.Request
{
    public class overtime_details : ErsBindableModel, IOvertimeDetailsCommand
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.emp_no")]
        public virtual string emp_no { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.date_start")]
        public virtual DateTime? date_start { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.date_end")]
        public virtual DateTime? date_end {
            get {

                if (w_time_start.HasValue() && w_time_end.HasValue() && date_start != null)
                {
                    DateTime getDate = Convert.ToDateTime(date_start);
                    if (start_period == EnumClockPeriod.PM && end_period == EnumClockPeriod.AM)
                    {
                        return getDate.AddDays(1);
                    }                 
                }

                return null;
            }
        }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string shift_schedule { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.time_start")]
        public virtual DateTime? time_start {
            get
            {
                DateTime timeText = new DateTime();
                if (w_time_start.HasValue() && date_start != null)
                {
                    var date = date_start?.ToShortDateString();
                    timeText = Convert.ToDateTime(date + " " + w_time_start);
                    return timeText;
                }

                return null;
            }
            set { } }

        
        public virtual string w_time_start
        {
            get
            {
                if (start_hours.HasValue() && start_minutes.HasValue() && start_period != null)
                {
                    return start_hours + ":" + start_minutes + " " + start_period;
                }

                return null;
            }
        }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string start_hours { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string start_minutes { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 2)]
        public EnumClockPeriod? start_period { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.time_end")]
        public virtual DateTime? time_end
        {
            get
            {
                DateTime timeText = new DateTime();
                if (w_time_end.HasValue() && date_start != null)
                {
                    DateTime getDate = Convert.ToDateTime(date_start);
                    if (end_period == EnumClockPeriod.AM)
                    {
                        getDate = getDate.AddDays(1);
                    }
                    var date = getDate.ToShortDateString();

                    timeText = Convert.ToDateTime(date + " " + w_time_end);
                    return timeText;
                }

                return null;
            }
            set { }
        }
        
        public virtual string w_time_end
        {
            get
            {
                if (end_hours.HasValue() && end_minutes.HasValue() && end_period != null)
                {
                    return end_hours + ":" + end_minutes + " " + end_period;
                }

                return null;
            }
        }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string end_hours { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string end_minutes { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 2)]
        public EnumClockPeriod? end_period { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string ot_hours
        {
            get
            {
                string ot_s = "";
                var ot = time_end - time_start;
                if (ot != null)
                {
                    if (ot?.Minutes != 0)
                    {
                        ot_s = ot?.Hours + " Hour/s " + ot?.Minutes + " Minute/s";
                    }
                    else
                    {
                        ot_s = ot?.Hours + " Hour/s";
                    }
                    return ot_s;
                }
                return null;
            }
            set { }
        }
        
        [ErsOutputHidden]
        [ErsSchemaValidation("request_t.reason")]
        [DisplayName("Reason")]
        public virtual string reason { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string shift_end
        {
            get
            {
                if (shift_schedule.HasValue())
                {
                    var endIndex = shift_schedule.IndexOf('-') + 1;
                    DateTime shiftendTime;
                    var result = DateTime.TryParse(shift_schedule.Substring(endIndex), out shiftendTime);
                    if (result)
                    {
                        return shiftendTime.ToString("hh: mm tt");
                    }
                }
                return null;
            }
      
            set { }
        }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string w_date_filed
        {         
            get
            {
                var date = DateTime.Now;
                return date.ToShortDateString();
            }
            set { }
        }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string w_date_start
        {
            get
            {                
                string dt_start = date_start?.ToString("yyyy-MM-dd");
                return dt_start;
            }
        }
    }
}