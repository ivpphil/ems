﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;

namespace ersEms.Models.Request
{
    public class Approver : ErsModelBase
    {
        [ErsSchemaValidation("approver_t.request_id")]
        public virtual int? request_id { get; set; }

        [ErsSchemaValidation("approver_t.emp_no")]
        public virtual string emp_no { get; set; }

        [ErsSchemaValidation("approver_t.approval_status")]
        public virtual EnumApprovalStatus? approval_status { get; set; }

        [ErsSchemaValidation("approver_t.reason")]
        public virtual string reason { get; set; }

        public virtual DateTime intime { get; set; }
    }
}