﻿using ersEms.Domain.api.Mappables;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Models.api
{
    public class unseen_messages
        :ErsModelBase, IUnseenMessageMappable
    {
        public int unseen_message_count { get; set; }
    }
}