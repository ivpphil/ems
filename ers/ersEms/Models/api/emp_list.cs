﻿using ersEms.Domain.api.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.job;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Models.api
{
    public class emp_list
        :ErsModelBase, IEmpListMappable
    {
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string s_keyword { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 4)]
        public EnumEmpSearch? emp_search_by { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 6)]
        public EnumTeam? team_search { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? job_title_search { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.WebAddress)]
        public string url { get; set; }

        public List<Dictionary<string, object>> list { get; set; }

        public List<Dictionary<string, object>> emp_search_option
        {
            get
            {
                return ErsFactory.ersCommonFactory.GetCommonNameCodeOption(EnumCommonNameType.EmployeeSearch);
            }
        }

        public List<Dictionary<string, object>> emp_team_option
        {
            get
            {
                return ErsFactory.ersCommonFactory.GetCommonNameCodeOption(EnumCommonNameType.Team);
            }
        }

        public List<Dictionary<string, object>> emp_position_option
        {
            get
            {
                return ErsFactory.ersCommonFactory.GetCommonNameCodeOption(EnumCommonNameType.PositionTitle);
            }
        }

        public IList<ErsJobTitle> job_title_list
        {
            get
            {
                return ErsFactory.ersJobFactory.GetJobTitleStrategy().GetJobTitleList();
            }
        }

        public List<Dictionary<string, object>> emp_status_option
        {
            get
            {
                return ErsFactory.ersCommonFactory.GetCommonNameCodeOption(EnumCommonNameType.Emp_Status);
            }
        }

        public long? MaxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().maxEmployeeCount; } }

    }
}