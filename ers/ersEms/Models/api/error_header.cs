﻿using ersEms.Domain.api.Mappables;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Models.api
{
    public class error_header
        :ErsModelBase, IErrorHeaderMappable
    {
        public int pos { get; set; }
        public string islogin { get; set; }
    }
}