﻿using ersEms.Domain.api.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System;
using System.Collections.Generic;

namespace ersEms.Models.api
{
    public class team_management : api_employee, ITeamManagementApiMappable
    {
        public ErsCsvCreater csvCreater { get; set; }
              
        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.position")]
        public virtual EnumPosition? emp_pos { get; set; }

        public ErsPagerModel pager { get; set; }

        public ErsPagerModel non_team_pager { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [HtmlSubmitButton]
        [ErsOutputHidden]
        public bool hasInformation { get; set; }
             
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All, isArray = true)]
        public string[] emp_no_arr { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string emp_no_string { get; set; }

        public virtual List<Dictionary<string, object>> non_team_list { get; set; }

        public virtual List<Dictionary<string, object>> list { get; set; }

        public virtual List<Dictionary<string, object>> searchFieldList
        {
            get
            {
                var list = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.EmployeeSearch, EnumCommonNameColumnName.namename);
                var retList = new List<Dictionary<string, object>>();
                foreach(var item in list)
                {
                    if (Convert.ToInt32(item["value"]) != 5)
                    {
                        retList.Add(item);
                    }
                }

                return retList;
            }
        }
    }
}