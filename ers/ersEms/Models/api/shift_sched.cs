﻿using ersEms.Domain.api.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Models.api
{
    public class shift_sched
        :ErsModelBase, IRequestScheduleApiMappable
    {
        public string shift { get; set; }

        [ErsUniversalValidation]
        public string s_date_start { get; set; }

        public int desknet_id{ get; set; }
    }
}