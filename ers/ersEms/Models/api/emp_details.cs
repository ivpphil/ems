﻿using ersEms.Domain.api.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Models.api
{
    public class emp_details
        : ErsModelBase, IEmpDetailsMappable
    { 

        [ErsSchemaValidation("employee_t.emp_no")]
        public string emp_no { get; set; }

        public ErsEmployee employee { get; set; } 

        public int vacation_leave { get; set; }

        public int sick_leave { get; set; }

    }
}