﻿using ersEms.Domain.Employee.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System;
using System.ComponentModel;
using System.Threading;

namespace ersEms.Models.csv
{
    public class EmployeeCSV
        : ErsBindableModel, IEmpCsvUploadRecordsCommand
    {
        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string emp_no { get; set; }

        [CsvField]
        [ErsSchemaValidation("employee_t.fname")]
        public virtual string fname { get; set; }

        [CsvField]
        [ErsSchemaValidation("employee_t.lname")]
        public virtual string lname { get; set; }

        [CsvField]
        [ErsSchemaValidation("employee_t.email")]
        public virtual string email { get; set; }

        [CsvField]
        [DisplayName("Team")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string w_team { get; set; }

        [CsvField]
        [ErsSchemaValidation("employee_t.desknet_id")]
        public int? desknet_id { get; set; }


        [ErsSchemaValidation("employee_t.team")]
        public virtual int? team
        {
            get
            {
                if (this.w_team.HasValue())
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetIdFromString(EnumCommonNameType.Team, EnumCommonNameColumnName.namename, this.team_convert);
                }

                return null;
            }
        }

        //convert to case insenstive
        public string team_convert
        {
            get {
                var team_w = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Team, EnumCommonNameColumnName.namename, 2);
                if (this.w_team.ToUpper() == team_w)
                {
                    team_w = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.w_team.ToUpper());
                }
                else
                {
                    team_w = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.w_team.ToLower());
                }
                return team_w;
            }
        }


        [CsvField]
        [DisplayName("Team Leader")]
        [ErsSchemaValidation("employee_t.team_leader")]
        public string team_leader { get; set; }

        [CsvField]
        [ErsSchemaValidation("employee_t.date_hired")]
        public DateTime? date_hired { get; set; }

        [CsvField]
        [ErsSchemaValidation("employee_t.reg_date")]
        public DateTime? reg_date { get; set; }
        
        [CsvField]
        [ErsSchemaValidation("leave_balance_t.total_vl")]
        public double? total_vl { get; set; }
        
        [CsvField]
        [ErsSchemaValidation("leave_balance_t.total_sl")]
        public double? total_sl { get; set; }

        [CsvField]
        [ErsSchemaValidation("employee_t.sss_no")]
        public string sss_no { get; set; }

        [CsvField]
        [ErsSchemaValidation("employee_t.tin_no")]
        public string tin_no { get; set; }

        [CsvField]
        [DisplayName("Job Title")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string w_job_title { get; set; }

        [ErsSchemaValidation("employee_t.job_title")]
        public virtual int? job_title
        {
            get
            {
                if (this.w_job_title.HasValue())
                {
                    var job = ErsFactory.ersJobFactory.GetErsJobIdWithJobTitle(w_job_title);
                    return job?.id;
                }

                return null;
            }
        }

        public string job_title_convert
        {
            get { return Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.w_job_title.ToLower()); }
        }
    }
}