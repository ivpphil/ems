﻿using ersEms.Domain.Report.Commands;
using ersEms.Domain.Report.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;

namespace ersEms.Models.Report
{
    public class DReport:ErsModelBase,IDReportAddCommand,IDReportEditCommand, IDReportRegistrationMappable, IDReportDetailsMappable, IDReportEditMappable
    {
        [BindTable("dreport_details")]
        public IList<DReportDetails> dreport_details { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.report_date")]
        public DateTime? report_date { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string report_code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.emp_no")]
        public string emp_no { get; set; }

        public List<Dictionary<String, object>> rpcode_list
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewEmpPcodeService().SelectAsList();
            }
        }

        [HtmlSubmitButton]
        public bool fromedit { get; set; }

        public List<Dictionary<string, object>> list { get; set; }

        public int? sent_message_count { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string deleted_id { get; set; }

        public List<int> rprog_list
        {
            get
            {
                var list = new List<int>();

                for (int i = 10; i <= 100;)
                {
                    list.Add(i);
                    i = i + 10;
                }

                return list;
            }
        }

        public List<double> um_hours_list
        {
            get
            {
                var list = new List<double>();

                for (double i = 0.25; i <= 24;)
                {
                    list.Add(i);
                    i = i + 0.25;
                }

                return list;
            }
        }

        public void SetDefaultReportDate()
        {
            if (!report_date.HasValue)
            {
                report_date = DateTime.Now;
            }
        }

        public virtual bool owner_flg { get; set; }

        public virtual EnumOnOff downloaded { get; set; }
    }
}