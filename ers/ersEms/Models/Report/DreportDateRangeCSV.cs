﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System;
using System.ComponentModel;

namespace ersEms.Models.Report
{
    public class DReportDateRangeCSV : ErsModelBase
    {
        [CsvField]
        [DisplayName("pcode_csv")]
        public virtual string pcode { get; set; }

        [CsvField]
        [DisplayName("proj_desc_csv")]
        public virtual string proj_desc { get; set; }

        [CsvField]
        [DisplayName("um_hours_csv")]
        public virtual double um_hours { get; set; }

        [CsvField]
        [DisplayName("completed_tasks_csv")]
        public virtual int completed_tasks { get; set; }

        [CsvField]
        [DisplayName("on_going_tasks_csv")]
        public virtual int on_going_tasks { get; set; }

        [CsvField]
        [DisplayName("summary_csv")]
        public virtual string summary { get; set; }

        [CsvField]
        [DisplayName("progress_csv")]
        public virtual string w_progress
        {
            get
            {
                if (this.progress != null)
                {
                    return Convert.ToString(this.progress) + "%";
                }
                return null;
            }
        }

        public virtual int? progress { get; set; }
    }
}