﻿using ersEms.Domain.Report.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;

namespace ersEms.Models.Report
{
    public class DReportDetails:ErsBindableModel,IDReportDetailsCommand,IDReportEditDetailsCommand
    {
        public override string lineName
        {
            get
            {
                return ErsResources.GetFieldName("tasks") + lineNumber;
            }
        }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.id")]
        public int? id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.report_date")]
        public DateTime? report_date { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.emp_no")]
        public string emp_no { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.report_code")]
        public string report_code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.pcode")]
        public string pcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.proj_desc")]
        public string proj_desc { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.ref_no")]
        public string ref_no { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.progress")]
        public int? progress { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.status")]
        public EnumReportStatus? status { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.summary")]
        public string summary { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.start_date")]
        public string start_date { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.due_date")]
        public string due_date { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.um_hours")]
        public double um_hours { get; set; }
        
        public List<Dictionary<String, object>> rstatus_list
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsEmpReportStatusService().SelectAsList(); }
        }

        public List<int> rprog_list
        {
            get
            {
                var list = new List<int>();

                for (int i = 10; i <= 100;)
                {
                    list.Add(i);
                    i = i + 10;
                }

                return list;
            }
        }

        public string w_status
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsEmpReportStatusService().GetStringFromId((int)status); }
        }

        public List<double> um_hours_list
        {
            get
            {
                var list = new List<double>();

                for (double i = 0.25; i <= 100;)
                {
                    list.Add(i);
                    i = i + 0.25;
                }

                return list;
            }
        }
    }
}