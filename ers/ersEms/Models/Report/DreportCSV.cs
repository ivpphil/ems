﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System;
using System.ComponentModel;

namespace ersEms.Models.Report
{
    public class DReportCSV : ErsModelBase
    {
        [DisplayName("pcode_csv")]
        [CsvField]
        public virtual string pcode { get; set; }
        
        [DisplayName("proj_desc_csv")]
        [CsvField]
        public virtual string proj_desc { get; set; }
        
        [DisplayName("ref_no_csv")]
        [CsvField]
        public virtual string ref_no { get; set; }
        
        [DisplayName("progress_csv")]
        [CsvField]
        public virtual string disp_progress
        {
            get
            {
                if (this.progress != null)
                {
                    string w_progress = this.progress + "%";
                    return w_progress;
                }
                return "";
            }
        }

        public virtual string progress { get; set; }
        
        [DisplayName("summary_csv")]
        [CsvField]
        public virtual string summary { get; set; }
        
        [DisplayName("start_date_csv")]
        [CsvField]
        public virtual string w_start_date {
            get
            {
                if (start_date != null)
                {
                    return Convert.ToDateTime(start_date).ToShortDateString();
                }

                return null;
            }
        }

        public virtual DateTime? start_date { get; set; }
        
        [DisplayName("due_date_csv")]
        [CsvField]
        public virtual string w_due_date {
            get
            {
                if (due_date != null)
                {
                    return Convert.ToDateTime(due_date).ToShortDateString();
                }

                return null;
            }
        }

        public virtual DateTime? due_date { get; set; }
        
        [DisplayName("um_hours_csv")]
        [CsvField]
        public virtual string um_hours { get; set; }

        [CsvField]
        [DisplayName("status_csv")]
        public virtual string disp_status
        {
            get
            {
                if (this.status != null)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsEmpReportStatusService().GetStringFromId(Convert.ToInt16(this.status));
                }
                return "";
            }
        }

        public virtual string status { get; set; }
    }
}