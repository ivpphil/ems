﻿using ers.jp.co.ivp.ers.employee;
using ersEms.Domain.Report.Commands;
using ersEms.Domain.Report.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ersEms.Models.Report
{
    public class DReportSearch : ErsModelBase, IDReportSearchCommand, IDReportSearchMappable, IDReportCSVMappable, IDReportDateRangeCSVMappable, IDReportCsvCommand
    {
        public DReportSearch()
        {
            error = false;
        }

        public bool error { get; set; }

        public ErsCsvCreater csvCreater { get; set; }

        public ErsPagerModel pager { get; set; }
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [ErsOutputHidden]
        public long limit { get; set; }

        public long recordCount { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public int? src_id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.report_date")]
        public DateTime? src_report_date { get; set; }
        
        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.emp_no")]
        public string src_emp_no { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.report_code")]
        public string src_report_code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.pcode")]
        public string src_pcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("dailyreport_details_t.proj_desc")]
        public string src_proj_desc { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.fname")]
        public string src_fname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.lname")]
        public string src_lname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.team")]
        public EnumTeam? src_team { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? src_from_date { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? src_to_date { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.OneByteCharacter)]
        public bool? src_tl_flg { get; set; }

        public EnumPosition? emp_pos { get; set; }

        public int? sent_message_count { get; set; }

        public string w_team
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Team, EnumCommonNameColumnName.namename, (int?)src_team); }
        }


        public List<Dictionary<String, object>> team_list
        {
            get
            {
                List<Dictionary<string, object>> returnList = new List<Dictionary<string, object>>();
               var list_code =   ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Team, EnumCommonNameColumnName.code);
               
                return list_code;
            }
        }
        
        public List<Dictionary<string, object>> list { get; set; }
        
        public List<Dictionary<string, object>> csv_list { get; set; }

        [HtmlSubmitButton]
        public bool download_csv_all { get; set; }

        public IList<ErsEmployee> TeamLeadList
        {
            get
            {
                return ErsFactory.ersEmployeeFactory.GetOtherTeamLeadList();
                    
            }
        }

        [ErsOutputHidden]
        [ErsUniversalValidation(isArray = true)]
        public string[] src_team_leader { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.OneByteCharacter)]
        public bool src_byTeamLead { get; set; }
        
        public bool hasSelected {
            get
            {
                return (src_team_leader != null && src_team_leader.Count() > 0);
            }
        }

        public string own_emp_no { get { return ErsContext.sessionState.Get("mcode"); } }

        [ErsOutputHidden]
        [ErsUniversalValidation]
        public bool include_my_report { get; set; }
    }
}