﻿using ersEms.Domain.Login.Commands;
using ersEms.Domain.Login.Mappables;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersEms.Models.Login
{
    public class Change_pass:ErsModelBase,IChangePassMappable,IChangePassCommand
    {

        public ErsEmployee employee { get; set; }

        [HtmlSubmitButton]
        public bool submit_pass { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("employee_t.password")]
        public string password { get; set; }

        [ErsSchemaValidation("employee_t.password")]
        public string password_confirm { get; set; }

        [ErsOutputHidden("session")]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 200)]
        public string enc_mcode { get; set; }

        [ErsOutputHidden("session")]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 200)]
        public string enc_ransu { get; set; }

        public string mcode { get; set; }

        public string ransu { get; set; }
    }
}