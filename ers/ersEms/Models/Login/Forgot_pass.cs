﻿using ersEms.Domain.Login.Commands;
using ersEms.Domain.Login.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersEms.Models.Login
{
    public class Forgot_pass:ErsModelBase,IForgotPassMappable,IForgotPassCommand
    {

        [ErsSchemaValidation("member_t.email")]
        public string email { get; set; }

        public EnumMformat? mformat { get; set; }

        public string mcode { get; set; }

        public string lname { get; set; }

        public string fname { get; set; }

        public string changeUrl { get; set; }


        [HtmlSubmitButton]
        public virtual bool submit_btn { get; set; }
    }
}