﻿using ersEms.Domain.Login.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;

namespace ersEms.Models.Login
{
    public class Login:ErsModelBase,ILoginCommand
    {
        public string emp_no { get; set; }

        public string mcode { get; set; }

        [HtmlSubmitButton]
        public bool email_ck { get; set; }

        [ErsSchemaValidation("employee_t.email")]
        public string email { get; set; }

        [ErsSchemaValidation("employee_t.password")]
        [DisplayName("passwd")]
        public string password { get;  set; }

        [ErsSchemaValidation("employee_t.lname")]
        public string lname { get; set; }

        [ErsSchemaValidation("employee_t.fname")]
        public string fname { get; set; }
    }
}