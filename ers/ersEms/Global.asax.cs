﻿using System.Web.Mvc;
using System.Web.Routing;
using jp.co.ivp.ers;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.common;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers.coupon;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.stepmail;
using jp.co.ivp.ers.summary;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.warehouse;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers.target;
using jp.co.ivp.ers.doc_bundle;
using jp.co.ivp.ers.language;
using jp.co.ivp.ers.ranking;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mdb;
using jp.co.ivp.ers.request;
using jp.co.ivp.ers.Pdf;
using jp.co.ivp.ers.projects;
using jp.co.ivp.ers.job;

namespace ersEms
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : ErsMvcApplication
    {
        protected override string[] GetNamespace()
        {
            return new[] { "ersEms.Controllers" };
        }

        protected override void SetFactory()
        {

            ErsFactory.ersBasketFactory = new ErsBasketFactory();

            ErsFactory.ersMerchandiseFactory = new ErsMerchandiseFactory();

            ErsFactory.ersMemberFactory = new ErsMemberFactory();

            ErsFactory.ersMailFactory = new ErsMailFactory();

            ErsFactory.ersSessionStateFactory = new ErsSessionStateFactory();

            ErsFactory.ersViewServiceFactory = new ErsViewServiceFactory();

            ErsFactory.ersUtilityFactory = new ErsUtilityFactory();

            ErsFactory.ersOrderFactory = new ErsOrderFactory();

            ErsFactory.ersAddressInfoFactory = new ErsAddressInfoFactory();

            ErsFactory.ersPointHistoryFactory = new ErsPointHistoryFactory();

            ErsFactory.ersAdministratorFactory = new ErsAdministratorFactory();

            ErsFactory.ersTargetFactory = new ErsTargetFactory();

            ErsFactory.ersDocBundleFactory = new ErsDocBundleFactory();

            ErsFactory.ersCouponFactory = new ErsCouponFactory();

            ErsFactory.ersContentsFactory = new ErsContentsFactory();

            ErsFactory.ErsAtMailFactory = new ErsAtMailFactory();

            ErsFactory.ersStepMailFactory = new ErsStepMailFactory();

            ErsFactory.ersCommonFactory = new ErsCommonFactory();

            ErsFactory.ersWarehouseFactory = new ErsWarehouseFactory();

            ErsFactory.ersSummaryFactory = new ErsSummaryFactory();

            ErsFactory.ersRankingFactory = new ErsRankingFactory();

            ErsFactory.ersLpFactory = new ErsLpFactory();

            ErsFactory.ersBatchFactory = new ErsBatchFactory();

            ErsFactory.ersLanguageFactory = new ErsLanguageFactory();

            ErsFactory.ersEmployeeFactory = new ErsEmployeeFactory();

            ErsFactory.ersMDBFactory = new ErsMDBFactory();

            ErsFactory.ersRequestFactory = new ErsRequestFactory();

            ErsFactory.ersPdfFactory = new ErsPdfFactory();

            ErsFactory.ersPcodeFactory = new ErsPcodeFactory();

            ErsFactory.ersJobFactory = new ErsJobFactory();

        }

        protected override void RegisterRoutes(RouteCollection routes)
        {
            routes.RouteExistingFiles = true;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //http error
            routes.MapRoute(
              "HttpError",
              "HttpError/{action}/{statusCode}",
              new { controller = "HttpError", action = "Error" },
              GetNamespace()
            );

            //handle images/spacer.gif before ignore files under images
            routes.MapRoute(
                "ErsStepMailOpenCounter", // Route name
                "images/spacer.gif", // URL with parameters
                new { controller = "images", action = "spacer" }, // Parameter defaults
                GetNamespace()
            );

            //画像変換
            routes.MapRoute(
               "BimgConvert", // Route name
               "images/bimg/{scode}/{*fileName}", // URL with parameters
               new { controller = "ResizeImage", action = "resizeImage", dirName = "bimg" }, // Parameter defaults
                  GetNamespace()
            );

            //画像変換
            routes.MapRoute(
               "SimgConvert", // Route name
               "images/simg/{scode}/{*fileName}", // URL with parameters
               new { controller = "ResizeImage", action = "resizeImage", dirName = "simg" }, // Parameter defaults
                  GetNamespace()
            );

            //画像変換
            routes.MapRoute(
               "BimgConvertNor", // Route name
               "images/bimg/{*fileName}", // URL with parameters
               new { controller = "ResizeImage", action = "resizeImage", dirName = "bimg" }, // Parameter defaults
                  GetNamespace()
            );

            //画像変換
            routes.MapRoute(
               "SimgConvertNor", // Route name
               "images/simg/{*fileName}", // URL with parameters
               new { controller = "ResizeImage", action = "resizeImage", dirName = "simg" }, // Parameter defaults
                  GetNamespace()
            );

            //ignore handle of files
            routes.IgnoreRoute("{*files}", new { files = @".*\.(" + ErsFactory.ersUtilityFactory.getSetup().ignore_file_extensions + ")(/.*)?" });
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.IgnoreRoute("{*allimage}", new { allimage = @".*\.(jpg|jpeg|gif|bmp|png|ping)(/.*)?" });

            routes.MapRoute(
                "ErsLegacy", // Route name
                "top/{controller}/asp/{action}.asp", // URL with parameters
                new { controller = "Home", action = "index" }, // Parameter defaults
                GetNamespace()
            );

            routes.MapRoute(
                "ErsLegacyRoot", // Route name
                "top/{controller}/asp/", // URL with parameters
                new { controller = "Home", action = "index" }, // Parameter defaults
                 GetNamespace()
            );

            routes.MapRoute(
              "DefaultIndex", // Route name
              "", // URL with parameters
              new { controller = "Home", action = "index" }, // Parameter defaults
                 GetNamespace()
           );

            routes.MapRoute(
             "Static", // Route name
             "{*staticPath}", // URL with parameters
             new { controller = "Home", action = "staticRooting" }, // Parameter defaults
                GetNamespace()
          );

        }

        protected override void SetResolver()
        {
           Bootstrapper.Initialise();
        }
    }
}