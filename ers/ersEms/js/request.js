﻿
var details = "See Details";
var post_maxHeight = 150;

$(document).ready(function () {

    $('.id_checkbox').hide();
    jQuery("input[name='btn_download_all']").hide();

    $('.leaveDiv').each(function () {
        if ($(this)[0].offsetHeight > post_maxHeight) {
            $(this).height(post_maxHeight);
            $(this).css('overflow', 'hidden');
            $(this).closest('div').html($(this).closest('div').html());
        }
    }); 
    
    //DOWNLOAD Button
    $(".sel-download[value='Download']").click(function () {
        var arrID = new Array();
        var checked = false;

        $(this).parent().parent().parent().find('.my_request_cb').each(function () {
            if (this.checked) {
                checked = true;
                arrID.push($(this).val());
            }
        });

        if (!checked) {
            //for single request
            if ($(this).parent().parent().parent().find('.request-id').length) {

                $(this).parent().parent().parent().find('.request-id').each(function () {
                    arrID.push($(this).val());
                });

                //seperate array with dash
                $('#single_download_request').val(arrID.join("-"));
                document.getElementById('download_pdf_one').submit();
            }
            else {
                //If there are no checked "Include" checkbox, show select request dialog box.
                $('#select-request').modal('show');
            }
        }
        else {
            $('#single_download_request').val(arrID.join("-"));

            document.getElementById('download_pdf_one').submit();
        }
      
    });
    
    //APPROVE Button
    $(".sel-success[value='Approve']").click(function () {
        var arrID = new Array();
        var checked = false;

        //If there are checked "Include" checkbox, show approve dialog box.
        $(this).parent().parent().find('.request_id_cb').each(function () {
            if (this.checked) {
                checked = true;
                arrID.push($(this).val());
            }
        });

        if (checked) {
            $('#confirm-approve').modal('show');
        }
        else {
            //for single request
            if ($(this).parent().parent().find('.request-id').length) {
                $('#confirm-approve').modal('show');

                $(this).parent().parent().find('.request-id').each(function () {
                    arrID.push($(this).val());
                });
            }
            else {
                //If there are no checked "Include" checkbox, show select request dialog box.
                $('#select-request').modal('show');
            }
        }

        //seperate array with dash
        $('#approve_req_id').val(arrID.join("-"));
        //transfer request type to modal hidden field
        var id = $(this).attr('id').split('_').pop();  // get the last string as an id
        var reqTypeValue = $('#request_type_' + id).val();
        $('#approve_req_type').val(reqTypeValue);
    });

    //DECLINE button
    $(".sel-decline[value='Decline']").click(function () {
        var arrID = new Array();
        $("#btn-confirm-decline").attr("disabled", true);

        var checked = false;
        //If there are checked "Include" checkbox, show approve dialog box.

        $(this).parent().parent().find('.request_id_cb').each(function () {
            if (this.checked) {
                checked = true;
                arrID.push($(this).val());
            }
        });

        if (checked) {
            $('#confirm-decline').modal('show');
        }
        else {
            //for single request
            if ($(this).parent().parent().find('.request-id').length) {
                $('#confirm-decline').modal('show');

                $(this).parent().parent().find('.request-id').each(function () {
                    arrID.push($(this).val());
                });
            }
            else {
                //If there are no checked "Include" checkbox, show select request dialog box.
                $('#select-request').modal('show');
            }
        }

        //seperate array with dash
        $('#decline_req_id').val(arrID.join("-"));
        $('#remarks-decline').val("");
        //transfer request type to modal hidden field
        var id = $(this).attr('id').split('_').pop(); // get the last string as an id
        var reqTypeValue = $('#request_type_' + id).val();
        $('#decline_req_type').val(reqTypeValue);
    });

    //CANCEL Button
    $(".sel-cancel[value='Cancel']").click(function () {
        var arrID = new Array();
        $("#btn-confirm-cancel").attr("disabled", true);

        var checked = false;
        //If there are checked "Include" checkbox, show approve dialog box.

        $(this).parent().parent().find('.request_id_cb').each(function () {
            if (this.checked) {
                checked = true;
                arrID.push($(this).val());
            }
        });

        if (checked) {
            $('#confirm-cancel').modal('show');
        }
        else {
            //for single request
            if ($(this).parent().parent().find('.request-id').length) {
                $('#confirm-cancel').modal('show');

                $(this).parent().parent().find('.request-id').each(function () {
                    arrID.push($(this).val());
                });
            }
            else {
                //If there are no checked "Include" checkbox, show select request dialog box.
                $('#select-request').modal('show');
            }
        }

        //seperate array with dash
        $('#cancel_req_id').val(arrID.join("-"));
        $('#remarks-cancel').val("");
        //transfer request type to modal hidden field
        var id = $(this).attr('id').split('_').pop(); // get the last string as an id
        var reqTypeValue = $('#request_type_' + id).val();
        $('#cancel_req_type').val(reqTypeValue);
    });

    //CANCEL APPROVED REQUEST Button
    $(".sel-cancel-req[value='Cancel']").click(function () {
        var arrID = new Array();
        $("#btn-confirm-cancel").attr("disabled", true);

        var checked = false;
       
        $(this).parent().parent().parent().find('.my_request_cb').each(function () {
            if (this.checked) {
                checked = true;
                arrID.push($(this).val());
            }
        });

        if (checked) {
            $('#confirm-cancel').modal('show');
        }
        else {
            //for single request
            if ($(this).parent().parent().parent().find('.request-id').length) {
                $('#confirm-cancel').modal('show');

                $(this).parent().parent().parent().find('.request-id').each(function () {
                    arrID.push($(this).val());
                });
            }
            else {
                //If there are no checked "Include" checkbox, show select request dialog box.
                $('#select-request').modal('show');
            }
        }
      
        //seperate array with dash
        $('#cancel_req_id').val(arrID.join("-"));
        $('#remarks-cancel').val("");
        //transfer request type to modal hidden field
        var id = $(this).attr('id').split('_').pop(); // get the last string as an id
        var reqTypeValue = $('#request_type_' + id).val();
        $('#cancel_req_type').val(reqTypeValue);
    });

    //RECEIVE Button
    $(".sel-received[value='Receive']").click(function () {
        var arrID = new Array();

        $('#confirm-received').modal('show');

        $(this).parent().parent().parent().find('.request-id').each(function () {
            arrID.push($(this).val());
        });

        //seperate array with dash
        $('#received_req_id').val(arrID.join("-"));
        //transfer request type to modal hidden field
        var id = $(this).attr('id').split('_').pop();  // get the last string as an id
        var reqTypeValue = $('#request_type_' + id).val();
        $('#received_req_type').val(reqTypeValue);
    });
    
    $('#select_id').change(function () {
        $('.single_cb').prop('checked', $(this).prop('checked'));
        if (this.checked) {
            jQuery("input[name='btn_download']").hide();
            jQuery("input[name='btn_download_all']").show();
            $('.my_request_cb').css('display', 'none');
            $('.include_single').css('display', 'none');            
            $('.id_checkbox').show();
        }
        else {
            jQuery("input[name='btn_download']").show();
            jQuery("input[name='btn_download_all']").hide();
            $('.my_request_cb').css('display', 'inline');
            $('.include_single').css('display', 'inline');
            $('.id_checkbox').hide();
        }
    });

    $('.single_cb').change(function () {
        if ($('.single_cb:checked').length == $('.single_cb').length) {
            $('#select_id').prop('checked', true);
        }
        else {
            $('#select_id').prop('checked', false);
        }
    });
})

function check_submit_decline() {
    if ($.trim($('#remarks-decline').val()) == "") {
        $("#btn-confirm-decline").attr("disabled", true)
    }
    else {
        $("#btn-confirm-decline").removeAttr("disabled");
    }

}
function check_submit_cancel() {
    if ($.trim($('#remarks-cancel').val()) == "") {
        $("#btn-confirm-cancel").attr("disabled", true);
    }
    else {
        $("#btn-confirm-cancel").removeAttr("disabled");
    }
}


$(document).on('click', '.see_details', function (e) {

    e.preventDefault();
    var $post = $(this).closest('div');

    if ($post[0].offsetHeight < $post[0].scrollHeight) {
        $post.animate({ 'height': $post[0].scrollHeight + 'px' }, 1000);
    }
    else {
        $post.animate({ 'height': post_maxHeight + 'px' }, 1000);
        $post.css('overflow', 'hidden');
    }


});

function goToSchedule() {
    document.getElementById("form").submit();
}
