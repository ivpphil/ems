﻿var arr_index = 0;
var deleted_id = "";
var taskCounter;

var trCount = $('#job_table tr').length;

$(document).ready(function () {
    if (trCount > 9) {
        $('#report_table body tr:nth-child(12)').remove();
    }

    $("#addField").click(function () {

        var trCount = $("#job_table tr").length - 1;             
        var record_key = "record_key_"
        
        if (trCount < 10) {
            $("#job_table tbody").append(
                    "<tr>" +
                    "<td>" +
                    "<input type='text' id='job_title' name='record_key_" + trCount + "_job_title'/>" +
                    "</td>" +
                    "<td>" +
                    "<input type='text' id='job_description' name='record_key_" + trCount + "_job_description' size='100'/>" +
                    "</td>" +
                    "<td><input type='button' class='btn btnl-info btn-sm' value='Delete' name='deleteBtn' onclick='return DeleteRow(this)' /></td>" +
                    "</tr> </br> </br>"
                );
            trCount++;
        }
    });
});

function DeleteRow(deleteBtn)
{
    var trCount = $("#job_table tr").length - 1;

    if (trCount > 1) {
        $(deleteBtn).closest('tr').remove();
        trCount--;
    }
    
  
}


function OnclickOut(inputField, column)
{
        
    var inputValue = $(inputField).val();
    var inputName = $(inputField).attr("id");

    var row_index = $(inputField).closest("tr").index() -1;
    var record_key = "record_key_"
    var new_name = "";


    if (inputValue.length > 0)
    {

        var matchStringIndex = inputName.search("record_key_");

        if (matchStringIndex == -1)
        {

            new_name = record_key + row_index + "_" + inputName;
            $(inputField).attr("name", new_name);


        }
    }

    else {


        var defaultFieldname = "";

        switch (column) {


            case 0:
                defaultFieldname = "job_title";
                break;

            case 1:
                defaultFieldname = "job_description";
                break;
           

        }

        $(inputField).attr("id", defaultFieldname);
    }

}