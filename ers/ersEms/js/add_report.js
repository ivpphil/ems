﻿var arr_index = 0;
var deleted_id = "";
var taskCounter;

var trCount = $('#report_table tr').length;

var um_hours_list;
var i;
for (i = 0.25; i <= 24;) {
    um_hours_list += "<option value='" + i + "'>" + i + "</option>";
    i = i + 0.25;
}

var progress;
var a;
for (a = 10; a <= 100;) {
    progress += "<option value='" + a + "'>" + a + "%</option>";
    a = a + 10;
}

function addNewTable(row_index) {
    var record_key = "record_key_" + row_index + "_";
    var newRow = "</br>" +
        "<tr>" +
        "<th class='col-sm-2 form-group'>Task</th>" +
        "<td>" +
        "<div class='row'>" +
        "<div class='col-sm-4 form-group'>" +
        "<label> PCODE </label>" + "<label class='required'>&nbsp;*</label>" +
        "<input type='text' class='form-control' name='" + record_key + "pcode' value='' onfocusout = 'OnclickOut(this,0)'  id='" + record_key + "pcode' />" +
        "<input type='button' value='Select' id='btnSetting' onclick='return pcode_select(this)' class='f_button info' name = '" + record_key + "' />" +
        "</div>" +
        "<div class='col-sm-4 form-group'>" +
        "<label> Description </label>" + "<label class='required'>&nbsp;*</label>" +
        "<input type='text' class='form-control' name='" + record_key + "proj_desc' value= '' onfocusout = 'OnclickOut(this,1)' id= '" + record_key + "proj_desc' />" +
        " </div>" +
        "</div>" +

        "<div class='row'>" +
        "<div class='col-sm-4 form-group'>" +
        "<label> Reference Number </label>" +
        "<input type='text' class='form-control' name='" + record_key + "ref_no' onfocusout = 'OnclickOut(this,2)' />" +
        " </div>" +
        " </div>" +

        "<div class='row'>" +
        "<div class='col-sm-4 form-group'>" +
        "<label> Used Man Hours</label>" + "<label class='required'>&nbsp;*</label>" +
        "<select name='" + record_key + "um_hours' id= 'sel1' class='form-control' onfocusout= 'OnclickOut(this,8)' > " +
        "<option value=''></option>" +
        um_hours_list +
        "</select> " +
        " </div>" +
        "</div>" +

        "<div class='row'>" +
        "<div class='col-sm-2 form-group'>" +
        "<label> Progress</label>" + "<label class='required'>&nbsp;*</label>" +
        " <select name='" + record_key + "progress' id= 'sel1' class='form-control' onfocusout = 'OnclickOut(this,3)' > " +
        "<option value=''></option>" +
        progress +
        "</select>" +
        " </div>" +
        " </div>" +

        "<div class='row'>" +
        "<div class='col-sm-3 form-group'>" +
        "<label> Status</label>" + "<label class='required'>&nbsp;*</label>" +
        " <select name='" + record_key + "status' id= 'sel1' class='form-control' onfocusout = 'OnclickOut(this,4)' > " +
        "<option value=''></option>" +
        "<option value='1'>Completed Task</option>" +
        "<option value='2'>On Going Task</option>" +
        "<option value='3'>On Going Task with Problem</option>" +
        "</select>" +
        " </div>" +
        " </div>" +

        "<div class='form-group'>" +
        "<label> Summary</label>" +
        " <textarea name='" + record_key + "summary' id= 'sel1' rows= '3' class='form-control' onfocusout = 'OnclickOut(this,5)' > " +
        "</textarea>" +
        " </div>" +

        "<div class='row'>" +
        "<div class='col-sm-4 form-group'>" +
        "<label> Start date </label>" +
        "<input type='date' class='form-control' name='" + record_key + "start_date' onfocusout = 'OnclickOut(this,6)' />" +
        " </div>" +

        "<div class='col-sm-4 form-group'>" +
        "<label> Due date </label>" +
        "<input type='date' class='form-control' name='" + record_key + "due_date' onfocusout = 'OnclickOut(this,7)' />" +
        " </div>" +
        " </div>" +
        " <input type='button' class='btn btn-info btn-sm' value='Delete' name='deleteBtn' id='deleteBtn' onclick='DeleteRow(this)' />&nbsp;" +
        "</td>" +

        "</tr> </br> </br>";

    return newRow;
}
var pcode_id
function pcode_select(btn) {
    var row_index = $(btn).closest("tr").find('#btnSetting').attr("name");
    window.row_index = row_index;
    window.pcode_id_field = row_index + "pcode";
    window.proj_desc_field = row_index + "proj_desc";
  
    
    result = window.open("/top/project/asp/dreport_pcode_list.asp", '_blank', "status=yes,width=1024px,height=768px,resizable=yes,scrollbars=yes");

    result.focus();
    document.onmousedown = disable_parent;
    document.onkeyup = disable_parent;


    result[result.addEventListener ? 'addEventListener' : 'attachEvent']


    ((result.attachEvent ? 'on' : '') + 'load', function () {

        result.RunCallbackFunction = callback_func;
        result.reco_number = reco_number;

    }, false);


}

function focusPopup() {
    if (result && result.closed) {
        result.focus();
    }
}

function disable_parent() {
    if (result && !result.closed) {
        result.focus();
    }
}


function callback_func(reco_number, pcode, pcode_desc, proj_desc_field, row_index) {
    if (pcode != undefined) {
        $("#" + reco_number).val(pcode);
        $("#" + proj_desc_field).val(pcode_desc);


    }
}                         

$(document).ready(function () {



    if (trCount > 9) {

        $('#report_table body tr:nth-child(12)').remove();


    }

    $("#addField").click(function () {

        var trCount = $("#report_table tr").length - 1;
 
        for (x = trCount; x < 10; x++) {
            var record_key = "record_key_" + x + "_";
            if (document.getElementById(record_key + "pcode") == null) {
                trCount = x;
                break;
            }
        }

        if (trCount < 10) {
            $("#report_table tbody").append(addNewTable(trCount));
            trCount++;
        }
    });
});

function DeleteRow(deleteBtn)
{
 
    $(deleteBtn).closest('tr').remove();
    trCount--;

    if ($('#id').val() != 0) {
        if ($(deleteBtn).closest('tr').find('#id').val() != null && $(deleteBtn).closest('tr').find('#id').val().length != 0) {
            var result = $(deleteBtn).closest('tr').find('#id').val();

            if (deleted_id == "") {
                deleted_id = result;
            }
            else {
                deleted_id = deleted_id + "_" + result;
            }
            arr_index++;

            $('#deleted_id').val(deleted_id);
        }
    }
}