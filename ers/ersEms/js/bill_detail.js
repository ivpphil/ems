/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function(){
	var ersObj = ErsEntry();
	ersObj.payCk();
	ersObj.addressCk();
	ersObj.zipAutoedit();
	ersObj.dispDelivery();
	ersObj.stockUpdateCk();
});

/* ErsEntryオブジェクト生成コンストラクタ */
var ErsEntry = function () {

    var that = {};

    //formオブジェクト
    var objForm = $("form[name='bill_change']");

    /* 支払い方法別の表示
    ---------------------------------------------------------------- */
    that.payCk = function () {
        //引数の設定
        var objPay = {
            "strSelector": "input[name='pay']:checked",
            "ckVal": ["1", "8"], 		//サイバーソース or ネットプロテクションカード決済
            "domDisp": $("#cardform")
        }

        //onchangeイベントをバインド
        $('input[name="pay"]:radio').change(function () {
            ErsLib.dispChange(objPay);
        }).change();
    }

    /* お届け先選択時の項目自動入力
    ---------------------------------------------------------------- */
    that.addressCk = function () {
        //ロード時にチェック
        try {
            selectChange($('#member_add_id').attr('value'));
        } catch (e) {
            //何もしない
        }

        //onchangeイベントをバインド
        $('#member_add_id').change(function () {
            selectChange($('#member_add_id').attr('value'));
        })
    }

    /* 郵便番号から住所自動入力
    ---------------------------------------------------------------- */
    that.zipAutoedit = function () {
        var thisId = ""; 	//クリックされた要素のID
        var objAddress = {}; 	//郵便番号検索の引数設定オブジェクト
        //clickイベントをバインド
        $("#zip_flg1,#zip_flg2").click(function () {
            var thisId = $(this).attr("id");

            if (thisId === "zip_flg1") {
                //address1用引数の設定
                objAddress = {
                    "domZip": $("#zip"),
                    "domPref": $("#pref"),
                    "domAddress": $("#address"),
                    "domAddress2": $("#taddress"),
                    "domAddress3": $("#maddress"),
                    "zip_search_error": $("#zip_search_error")
                }
            } else if (thisId === "zip_flg2") {
                //address2用引数の設定
                objAddress = {
                    "domZip": $("#add_zip"),
                    "domPref": $("#add_pref"),
                    "domAddress": $("#add_address"),
                    "domAddress2": $("#add_taddress"),
                    "domAddress3": $("#add_maddress"),
                    "zip_search_error": $("#add_zip_search_error")
                }
            }

            //郵便番号検索
            ErsLib.zipSearch(objAddress);

            return false;
        });
    }

	/* 別お届け先の表示切り替え
	---------------------------------------------------------------- */
	that.dispDelivery = function () {
		//一旦非表示にする
		$(".delivery_address").hide();

		$("#send").change(function(){
			if($(this).val() === "2"){
				$(".delivery_address").show();
			}else{
				$(".delivery_address").hide();
			}
		}).change();
	}

	/* 在庫更新チェックボックスの切り替え
	---------------------------------------------------------------- */
	that.stockUpdateCk = function () {

		var domStockUpdate = $(".stock_update_ck")
		var domAmount = $("input[id^='up_amount']")
		var objAmountOld = {};
		var objAmountNew = {};

		//一旦非表示にする
		domStockUpdate.hide();

		//初期入力値の設定
		domAmount.each(function(){
			objAmountOld[$(this).attr("id")] = $(this).attr("defaultValue");
		});

		var amountChange = function(){
			//現在値の取得
			$("input[id^='up_amount']").each(function(){
				objAmountNew[$(this).attr("id")] = $(this).attr("value")
			});

			//初期値と比較
			for( i in objAmountNew){
				if(objAmountOld[i] !== objAmountNew[i]){
					//変更あり
					return true;
				}
			}
			//変更なし
			return false;
		}

		domAmount.blur(function(){
			if(amountChange() === false){
				domStockUpdate.hide();
			}else{
				domStockUpdate.show();
			}
		}).blur();
	}


	return that;
}
