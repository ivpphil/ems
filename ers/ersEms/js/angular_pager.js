﻿app.controller('PagerController', function ($scope, $http, Data) {

    $scope.pageData = function (event) {

        var fields = $(event.toElement).parents("form").serializeArray();
        fields.push({ loadGraph : false });

        var current_page = $(event.toElement).siblings("input[name='pageCnt']").val();
        $("#currentPage").val(current_page);

        $http({ method: 'POST', data: $.param(fields), headers: { 'Content-Type': 'application/x-www-form-urlencoded' }, url: $scope.Data.url }).
        success(function (data, status, headers, config) {
            SetObjectValues(data)
        }).
        error(function (data, status, headers, config) {
            alert('error');
        });

        $(window).scrollTop($('#container').offset().top);
        $("html, body").animate({ scrollTop: "250px" });

    };

    // Overwrite object values base on keys
    function SetObjectValues(data) {
        $.each(data, function (key, value) {
            $scope.Data[key] = data[key];
        });

    };   
});


