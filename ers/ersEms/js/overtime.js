﻿var arr_index = 0;
var deleted_id = "";
var taskCounter;

var ot_hours_list;
var i;
for (i = 1; i <= 12;) {
    if(i<10){
        ot_hours_list += "<option value='0" + i + "'>0" + i + "</option>";
    }
    else {
        ot_hours_list += "<option value='" + i + "'>" + i + "</option>";
    }
    i = i + 1;
}

var ot_mins_list;
for (i = 0; i <= 45;) {
    if (i < 10) {
        ot_mins_list += "<option value='0" + i + "'>0" + i + "</option>";
    }
    else {
        ot_mins_list += "<option value='" + i + "'>" + i + "</option>"
    }
    i = i + 15;
}

var ot_period_list;
for (i = 1; i <= 2;) {
    if (i == 1) {
        ot_period_list += "<option value='" + i + "'>AM</option>";
    }
    else {
        ot_period_list += "<option value='" + i + "'>PM</option>"
    }
    i = i + 1;
}

var trCount = $('#overtime_table tbody tr').length;
var trRecordKey = $('#overtime_table tbody tr').length - 1;

function AppendNewRow(trCount)
{
    var rec_key = "record_key_" + trCount + "_";

    var newRow =
    "<tr>" +
        "<td>" +
            "<input type='date' id='" + rec_key + "date_start' name='" + rec_key + "date_start' class='date_start form-control w_160' />" +
        "</td>" +
        "<td>" +
            "<label id='" + rec_key + "shift_sched' ></label>" +
            "<input type='hidden' id='" + rec_key + "shift_schedule' name='" + rec_key + "shift_schedule' class='Shift_schedule form-control' />" +
         "</td>" +
         "<td>" +
            "<div class='w_215'>" +
                "<div class='download_button'>" +
                    "<select id='" + rec_key + "start_hours' name='" + rec_key + "start_hours' class='start_hours form-control w_65'>" +
                        "<option value=''></option>" +
                        ot_hours_list +
                    "</select>" +
                "</div>" +
                "&nbsp;" +
                "<strong>:</strong>" +
                "&nbsp;" +
                "<div class='download_button'>" +
                    "<select id='" + rec_key + "start_minutes' name='" + rec_key + "start_minutes' class='start_minutes form-control w_65'>" +
                    "<option value=''></option>" +
                        ot_mins_list +
                    "</select>" +
                "</div>" +
                "&nbsp;" +
                "<div class='download_button'>" +
                    "<select id='" + rec_key + "start_period' name='" + rec_key + "start_period' class='start_period form-control w_68'>" +
                    "<option value=''></option>" +
                        ot_period_list +
                    "</select>" +
                "</div>" +
            "</div>" +
         "</td>" +
         "<td>" +
             "<div class='w_215'>" +
                "<div class='download_button'>" +
                    "<select id='" + rec_key + "end_hours' name='" + rec_key + "end_hours' class='end_hours form-control w_65'>" +
                        "<option value=''></option>" +
                        ot_hours_list +
                    "</select>" +
                "</div>" +
                "&nbsp;" +
                "<strong>:</strong>" +
                "&nbsp;" +
                "<div class='download_button'>" +
                    "<select id='" + rec_key + "end_minutes' name='" + rec_key + "end_minutes' class='end_minutes form-control w_65'>" +
                    "<option value=''></option>" +
                        ot_mins_list +
                    "</select>" +
                "</div>" +
                "&nbsp;" +
                "<div class='download_button'>" +
                    "<select id='" + rec_key + "end_period' name='" + rec_key + "end_period' class='end_period form-control w_68'>" +
                    "<option value=''></option>" +
                        ot_period_list +
                    "</select>" +
                "</div>" +
            "</div>" +
         "</td>" +
         "<td>" +
            "<label id='" + rec_key + "othours'></label>" +
            "<input type='hidden' id='" + rec_key + "ot_hours' name='" + rec_key + "ot_hours' class='ot_hours form-control'/>" +
         "</td>" +
         "<td>" +
            "<input type='text' name='" + rec_key + "reason' id='" + rec_key + "reason' class='form-control'/>" +
         "</td>" +
         "<td>" +
            "<input type='button' class='btn btnl-info btn-sm' value='Delete' name='deleteBtn' onclick='return DeleteRow(this)' />" +
         "</td>" +
    "</tr> </br> </br>";

    $("#overtime_table tbody").append(newRow);
}

$(document).ready(function () {

    if (trCount > 9) {

        $('#report_table body tr:nth-child(12)').remove();
    }

      $("#addField").click(function () {

        var trCount = $("#overtime_table tbody tr").length;

        if (trCount < 10) {
            AppendNewRow(trCount);

            trCount++;
        }
    });
});

function DeleteRow(deleteBtn) {
    var trCount = $("#overtime_table tbody tr").length;

    if (trCount > 1) {
        $(deleteBtn).closest('tr').remove();
        trCount--;
    }
}


function OnclickOut(inputField, column) {

    var inputValue = $(inputField).val();
    var inputName = $(inputField).attr("name");

    var trCount = $("#overtime_table tbody tr").length - 1;
    var record_key = "record_key_"
    var new_name = "";


    if (inputValue.length > 0) {

        var matchStringIndex = inputName.search("record_key_");

        if (matchStringIndex == -1) {

            new_name = record_key + trCount + "_" + inputName;
            $(inputField).attr("name", new_name);
            $(inputField).attr("id", new_name);

        }
    }

    else {


        var defaultFieldname = "";

        switch (column) {


            case 0:
                defaultFieldname = "date_start";
                break;

            case 1:
                defaultFieldname = "start_hours";
                break;

            case 2:
                defaultFieldname = "start_minutes";
                break;

            case 3:
                defaultFieldname = "start_period";
                break;

            case 4:
                defaultFieldname = "end_hours";
                break;

            case 5:
                defaultFieldname = "end_minutes";
                break;

            case 6:
                defaultFieldname = "end_period";
                break;

            case 7:
                defaultFieldname = "Reason";
                break;

        }

        $(inputField).attr("name", defaultFieldname);
    }

}

