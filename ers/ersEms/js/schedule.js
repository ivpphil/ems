﻿
window.onload = function () {
        var date = document.getElementById("date").value;
        var s_schedule_date = document.getElementById("s_schedule_date");
        var scheduleDateVal = document.getElementById("schedule_date").value;
        $("#s_schedule_date").datepicker();


        var dayList = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        var now = new Date();
        var dayNow = dayList[now.getDay()] + " " + now.getDate().toString();

        var first_date = new Date(date);
        var end_date = new Date(first_date.getTime() + 1000 * 60 * 60 * 24 * 6);
        var head = document.getElementsByClassName("schedulehead");

        for (var i = 0; head.length > i;i++)
        {
            if (head[i].innerHTML == dayNow && (first_date.getMonth() + 1) == (now.getMonth() + 1))
            {
                head[i].style.backgroundColor += "yellow";
                head[i].style.color += "black";
                head[i].style.boxShadow += "inset 0 0 15px #FF8C00";
            }
            if (scheduleDateVal != "") {
                head[1].style.backgroundColor += "pink";
                head[1].style.color += "black";
                head[1].style.boxShadow += "inset 0 0 15px #FF8C00";
            }
        }

        document.getElementsByTagName('h3')[0].innerHTML = dayList[first_date.getDay()] + ' ' + (first_date.getMonth() + 1) + '/' + first_date.getDate() + '/' + first_date.getFullYear() +
            '- ' + dayList[end_date.getDay()] + ' ' + (end_date.getMonth() + 1) + '/' + end_date.getDate() + '/' + end_date.getFullYear();

        var frm_id = document.getElementById("from");
        var to_id = document.getElementById("to");
        var strtVal = document.getElementById("time_start").value;
        var endVal = document.getElementById("time_end").value;
        var selectedField = document.getElementById("selectedField").value;

        document.getElementById("selectDrpDwn").value  = selectedField;
        if (selectedField == 5) {
            frm_id.selectedIndex = strtVal;
            to_id.selectedIndex = endVal;
            document.getElementById("search").style.display += "none";
            s_schedule_date.style.display += "inline";
            s_schedule_date.value = scheduleDateVal;

            if (scheduleDateVal != "") {
                var schedule_date = new Date(scheduleDateVal);

                var month = schedule_date.getMonth() + 1;
                var day = schedule_date.getDate();
                var year = schedule_date.getFullYear();
                s_schedule_date.value = month + "/" + day + "/" + year;

                }

        }
        else {
            frm_id.style.display += "none";
            to_id.style.display += "none";
            s_schedule_date.style.display += "none";
        }
}
    function date_today() {
        var today = new Date();
        document.getElementById("date").value = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        s_schedule_date.value = null;
        document.getElementById("form").submit();
    };
    function prev_day() {
        var date = new Date(document.getElementById("date").value);
        var today = new Date(date.getTime() - 1000 * 60 * 60 * 24);
        document.getElementById("date").value = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        s_schedule_date.value = null;
        document.getElementById("form").submit();
    };
    function prev_week() {
        var date = new Date(document.getElementById("date").value);
        var today = new Date(date.getTime() - 1000 * 60 * 60 * 24 * 7);
        document.getElementById("date").value = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        s_schedule_date.value = null;
        document.getElementById("form").submit();
    };
    function next_day() {
        var date = new Date(document.getElementById("date").value);
        var today = new Date(date.getTime() + 1000 * 60 * 60 * 24);
        document.getElementById("date").value = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        s_schedule_date.value = null;
        document.getElementById("form").submit();
    };
    function next_week() {
        var date = new Date(document.getElementById("date").value);
        var today = new Date(date.getTime() + 1000 * 60 * 60 * 24 * 7);
        document.getElementById("date").value = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + (today.getDate());
        s_schedule_date.value = null;
        document.getElementById("form").submit();
    };


    function select_sched(selectObj) {
        var frm_id = document.getElementById("from");
        var to_id = document.getElementById("to");
        var selectedField = document.getElementById("selectedField");
        selectedField.value = selectObj.value;

        var time_start = document.getElementById("time_start");
        var time_end = document.getElementById("time_end");
        var search = document.getElementById("search");
        var s_schedule_date = document.getElementById("s_schedule_date");


        if (selectObj.value == "5") {
            frm_id.style.display = "inline";
            to_id.style.display = "inline";
            search.style.display = "none";
            search.value = "";
            s_schedule_date.style.display = "inline";
        }
        else {
            time_start.value = "0";
            time_end.value = "0";
            frm_id.style.display = "none";
            to_id.style.display = "none";
            search.style.display = "inline";
            s_schedule_date.style.display = "none";
        }
    };
    function select_time_start(obj) {
        var time_start = document.getElementById("time_start");
        time_start.value = obj.value;
    };
    function select_time_end(obj) {
        var time_end = document.getElementById("time_end");
        time_end.value = obj.value;
    };

    function searchSubmit() {
        var frm_id = document.getElementById("from");
        var to_id = document.getElementById("to");
        var selectedField = document.getElementById("selectedField");
        selectedField = document.getElementById("selectDrpDwn").value;
        document.getElementById("form").submit();
    };


  