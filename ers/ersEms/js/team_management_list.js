﻿$(document).ready(function () {
    $('#nonTeam input[name=pageCnt]').attr('name', 'non_team_page_count');
});


// Angular Coding
var app = angular.module('teamApp', []);
// Data Factory
app.factory('Data', function () {
    return {
        Model: '',
        pagerModel: {
            pager: [],
            pager_parts: [],
            pager_part: ''
        },
        prevModel: '',
        manageModel: '',

    };
});

function NotNullUndefinedEmpty(value) {
    return (value !== null && !angular.isUndefined(value) && value !== "");
}


app.controller('teamController', function ($scope, $http, Data, $rootScope) {
    $scope.Data = Data;

    getPageList();

    function getPageList() {
        var url_arry = (window.location.href).split("top");
        var prev_url = url_arry[0] + 'top/api/asp/getTeamList.asp';
        $http.get(prev_url).then(function (response) {
            $scope.Model = response.data.Model;
            SetObjectValues(response.data.Model);

        });
    }

    function SetObjectValues(data) {
        $.each(data, function (key, value) {
            $scope.Data[key] = data[key];
        });

    }


    //get pager numbers 
    $scope.getPager = function (RecordCount) {
        $scope.Data.pagerModel.pager = [];
        $scope.t_no_page;
        $scope.Data.pagerModel.pager_parts = [];


        var limit = $scope.Data.Model.MaxItemCount
        var exact = (RecordCount % limit) === 0;

        if (exact) {
            //exact item count for page number
            t_no_page = Math.floor(RecordCount / limit);
        }
        else {
            //excess 
            t_no_page = (Math.floor(RecordCount / limit)) + 1;
        }

        //set pager parts for prev and next button
        for (var i = 1; i <= t_no_page; i++) {
            $scope.Data.pagerModel.pager.push(i);
            if ((i % limit) === 0 || i === t_no_page) {
                $scope.Data.pagerModel.pager_parts.push($scope.Data.pagerModel.pager);
                $scope.Data.pagerModel.pager = [];
            }
        }

        //hides prev next button if pager_parts <= 1
        if ($scope.Data.pagerModel.pager_parts.length <= 1) {
            $(".page-nav").hide();
        }
        else {
            $(".page-nav").show();
        }

        if (!NotNullUndefinedEmpty($scope.Data.pagerModel.pager_part)) {
            $scope.Data.pagerModel.pager_part = 0;
        }

        //show pager parts 
        return $scope.Data.pagerModel.pager_parts[$scope.Data.pagerModel.pager_part]

    }

    $scope.initTeamSetPageList = function (pageCnt) {
        $scope.$broadcast("TeamSetPageListEvent", pageCnt);
    }
    $scope.initNonTeamSetPageList = function (pageCnt) {
        $scope.$broadcast("NonTeamSetPageListEvent", pageCnt);
    }

    $scope.nonTeamCriteriaMatch = function (criteria) {
        return function (item, nonTeamSearchField) {
            var returnBool = true;
            if (angular.isUndefined(criteria)) {
                return returnBool;
            }
            else {
                if (NotNullUndefinedEmpty(criteria.non_team_s_keyword)) {

                    if (NotNullUndefinedEmpty(criteria.nonTeamSearchField)) {
                        if (parseInt(criteria.nonTeamSearchField) == 1) {
                            returnBool = (item.emp_no.toLowerCase().match(criteria.non_team_s_keyword.toLowerCase()));
                        }
                        else if (parseInt(criteria.nonTeamSearchField) == 2) {
                            returnBool = (item.email.toLowerCase().match(criteria.non_team_s_keyword.toLowerCase()));
                        }
                        else if (parseInt(criteria.nonTeamSearchField) == 3) {
                            returnBool = (item.fname.toLowerCase().match(criteria.non_team_s_keyword.toLowerCase()));
                        }
                        else if (parseInt(criteria.nonTeamSearchField) == 4) {
                            returnBool = (item.lname.toLowerCase().match(criteria.non_team_s_keyword.toLowerCase()));
                        }
                        //reset pager and page_part in new search
                        $scope.Data.pagerModel.pager_part = 0;
                        $scope.initNonTeamSetPageList(1);
                    }
                }
            }
            return returnBool;
        }
    }

    $scope.teamCriteriaMatch = function (criteria) {
        return function (item, teamSearchField) {
            var returnBool = true;
            if (angular.isUndefined(criteria)) {
                return returnBool;
            }
            else {
                if (NotNullUndefinedEmpty(criteria.team_s_keyword)) {

                    if (NotNullUndefinedEmpty(criteria.teamSearchField)) {
                        if (parseInt(criteria.teamSearchField) == 1) {
                            returnBool = (item.emp_no.toLowerCase().match(criteria.team_s_keyword.toLowerCase()));
                        }
                        else if (parseInt(criteria.teamSearchField) == 2) {
                            returnBool = (item.email.toLowerCase().match(criteria.team_s_keyword.toLowerCase()));
                        }
                        else if (parseInt(criteria.teamSearchField) == 3) {
                            returnBool = (item.fname.toLowerCase().match(criteria.team_s_keyword.toLowerCase()));
                        }
                        else if (parseInt(criteria.teamSearchField) == 4) {
                            returnBool = (item.lname.toLowerCase().match(criteria.team_s_keyword.toLowerCase()));
                        }
                        //reset pager and page_part in new search
                        $scope.Data.pagerModel.pager_part = 0;
                        $scope.initTeamSetPageList(1);
                    }
                }
            }
            return returnBool;
        }
    }
});


//pager controller
app.controller("pagerController", function ($scope, Data) {
    $scope.Data = Data;

    //event for SetPageList
    $scope.$on("NonTeamSetPageListEvent", function (event, pageCnt) {
        $scope.SetNonTeamPageList(pageCnt);
    });

    //event for SetPageList
    $scope.$on("TeamSetPageListEvent", function (event, pageCnt) {
        $scope.SetTeamPageList(pageCnt);
    });

    //page previous navigator for page_part
    $scope.listPagePrev = function (part) {
        if ($scope.Data.pagerModel.pager_parts.length > 0) {
            if (part > 0) {
                $scope.Data.pagerModel.pager_part = part - 1;
                $scope.SetNonTeamPageList(($scope.Data.pagerModel.pager_parts[$scope.Data.pagerModel.pager_part])[0]);
                $scope.SetTeamPageList(($scope.Data.pagerModel.pager_parts[$scope.Data.pagerModel.pager_part])[0]);

            }
        }
    }

    //page next navigator for page_part
    $scope.listPageNext = function (part) {
        if ($scope.Data.pagerModel.pager_parts.length > 0) {
            if (part < ($scope.Data.pagerModel.pager_parts.length - 1)) {
                $scope.Data.pagerModel.pager_part = part + 1;
                $scope.SetNonTeamPageList(($scope.Data.pagerModel.pager_parts[$scope.Data.pagerModel.pager_part])[0]);
                $scope.SetTeamPageList(($scope.Data.pagerModel.pager_parts[$scope.Data.pagerModel.pager_part])[0]);

            }
        }
    }

    //set offset for team_list (pager function)
    $scope.SetTeamPageList = function (pageCnt) {
        var page_count;

        var limit = $scope.Data.maxItemCount;
        if (angular.isUndefined(pageCnt)) {
            page_count = 1;
        }
        else {
            page_count = pageCnt;
        }
        $scope.selected_page = page_count;
        var offset = (limit * (page_count - 1));
        $scope.Data.team_offset = offset;
    }

    //set offset for non_team_list (pager function)
    $scope.SetNonTeamPageList = function (pageCnt) {
        var page_count;

        var limit = $scope.Data.maxItemCount;
        if (angular.isUndefined(pageCnt)) {
            page_count = 1;
        }
        else {
            page_count = pageCnt;
        }
        $scope.selected_page = page_count;
        var offset = (limit * (page_count - 1));
        $scope.Data.non_team_offset = offset;
    }

    //get pager numbers 
    $scope.getPager = function (RecordCount) {
        $scope.Data.pagerModel.pager = [];
        $scope.t_no_page;
        $scope.Data.pagerModel.pager_parts = [];


        var limit = $scope.Data.maxItemCount
        var exact = (RecordCount % limit) === 0;

        if (exact) {
            //exact item count for page number
            t_no_page = Math.floor(RecordCount / limit);
        }
        else {
            //excess 
            t_no_page = (Math.floor(RecordCount / limit)) + 1;
        }

        //set pager parts for prev and next button
        for (var i = 1; i <= t_no_page; i++) {
            $scope.Data.pagerModel.pager.push(i);
            if ((i % limit) === 0 || i === t_no_page) {
                $scope.Data.pagerModel.pager_parts.push($scope.Data.pagerModel.pager);
                $scope.Data.pagerModel.pager = [];
            }
        }

        //hides prev next button if pager_parts <= 1
        if ($scope.Data.pagerModel.pager_parts.length <= 1) {
            $(".page-nav").hide();
        }
        else {
            $(".page-nav").show();
        }

        if (!NotNullUndefinedEmpty($scope.Data.pagerModel.pager_part)) {
            $scope.Data.pagerModel.pager_part = 0;
        }

        //show pager parts 
        return $scope.Data.pagerModel.pager_parts[$scope.Data.pagerModel.pager_part]

    }
});

