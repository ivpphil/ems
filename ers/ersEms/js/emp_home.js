﻿var announcement_id;
var announcement_news;
var announcementCommand;
var report_summary;
var DefaultLoad;
var loadGraph;


// Set command value upon Creating New Announcement
$(document).ready(function () {
    $("#btnAdd").click(function () {
        announcementCommand = 1;
    });
});

// Set command value upon Modifying or Deleting an Announcement
function manageAnnouncement(element, command) {
    // Commands : 1 = Create | 2 = Modify | 3 = Delete
    announcementCommand = command;
    announcement_id = $(element).siblings('input[name="id"]').val();        // Fetch announcement id by selecting the sibling input[name='id']

    if (command == 2) {
        var news = $(element).parents("div[name='announcementDIV']").children("div:nth-child(2)").children("input[type=hidden]").val();
        $("#edit_news").val(news);
    }
}

// Angular Coding
var app = angular.module('app', []);

// Format  Announcements
app.directive('myPostRepeatDirective', function () {
    return function (scope, element,Data, attrs) {
        element.children()[0].innerHTML = scope.announcement.news.replace(/\r?\n/g, '<br/>');
    };


});;
// Data Factory
app.factory('Data', function () {
    return {
        Model : '',
      
        // URL's are very importtant for fetching data and executing transactions
        url: 'https://ivp-ems.com/top/Employee/asp/display_home.asp',
        url_crud: 'https://ivp-ems.com/top/Employee/asp/announcement_crud.asp',
    };
});


// Function name =  ng-controller
// This is the base controller
app.controller('PageController', function ($scope, $http, Data) {
    $scope.Data = Data;

    $(function () {
        loadGraph = true;
        DefaultLoad = true;
        LoadPage();
        DefaultLoad = false; // Is set to false because it already loaded.
    });


    // Overwrite object values base on keys
    function SetObjectValues(data) {
        $.each(data, function (key, value) {
            $scope.Data[key] = data[key];
        });
    };

    function LoadGraphs(list) {
        $("#chartContainer").removeClass().addClass((list.length > 0 ||list != null) ? "pieChart" : 'emptyPieChart');   
        var chart = new CanvasJS.Chart("chartContainer", {
            title: {
                text: "Daily Reports Summary"
            },
            animationEnabled: true,
            legend: {
                verticalAlign: "center",
                horizontalAlign: "left",
                fontSize: 20,
                fontFamily: "Helvetica"
            },
            theme: "theme2",
            data: [
            {
                type: "pie",
                indexLabelFontFamily: "Garamond",
                indexLabelFontSize: 20,
                startAngle: -20,
                showInLegend: true,
                dataPoints: list,
            }
            ]
        });
        chart.render();
        if (list.length == 0) {
            $("#chartContainer div").remove()
            $("#chartContainer").append("<h3> No Records To Display </h3>")
        }
        
    };



    // Load List and other values by CALLING the ACTION in CONTROLLER
    function LoadPage() { // #currentPage is located at the start of the pager
        $http({ method: 'POST', data: $.param({ pageCnt: $('#currentPage').val(), loadGraph: loadGraph, isDefaultLoad: DefaultLoad }), headers: { 'Content-Type': 'application/x-www-form-urlencoded' }, url: $scope.Data.url }).
        success(function (data, status, headers, config) {

            SetObjectValues(data)
            LoadGraphs(data.Model.daily_report_list);        
        }).
        error(function (data, status, headers, config) {
            alert('error');
        });
    }


   
 

    $("#btnAdd").click(function () {
        $("#news").focus();
    });

    $("#report_summary").on("change", function () {
        $http({
            method: 'POST',
            data: $.param({
                report_summary: $(this).val(),
                loadGraph: true
            }), headers: { 'Content-Type': 'application/x-www-form-urlencoded' }, url: $scope.Data.url
        }).
        success(function (data, status, headers, config) {
            LoadGraphs(data.Model.daily_report_list);
        }).
        error(function (data, status, headers, config) {
            alert('error');
        });
    });

    // Event for CRUD functionality [ADD , UPDATE , DELETE]
    $('button[name="btnModal"]').click(function () {

        var element_id = $(this).attr("id");
        var news;

        switch (announcementCommand) {
            case 1:
                news = $("#news").val();
                break;
            case 2:
                news = $('#edit_news').val();
                break;
        }

        $http({
            method: 'POST',
            data: $.param({
                id: announcement_id,
                news: news,
                announcementCmdType: announcementCommand,
                pageCnt: $('#currentPage').val(),
                loadGraph : false
            }), headers: { 'Content-Type': 'application/x-www-form-urlencoded' }, url: $scope.Data.url_crud
        }).
        success(function (data, status, headers, config) {
            SetObjectValues(data)
            $("#news").val('');

            if (data["errorList"] != null) {
                $('#errorList').focus();
            }

        
        }).
        error(function (data, status, headers, config) {
            alert('error');
        });
    });



    // Fetch data using Angular JS to SignalR Hub
    $scope.hub = $.connection.myHub;
    $scope.initPushNotifications = function () {
        $scope.hub.client.AnnouncementAdded = function (hasChanges) {
            $scope.$apply(function () {
                if (hasChanges) {
                    loadGraph = true;
                    LoadPage();
                }
            });
        }
        $.connection.hub.start();
    }
    $scope.initPushNotifications();


 


});



