/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function () {
    var ersObj = ErsCommon();
    ersObj.submitBtnConfirm();
    ersObj.formSubmit();
    ersObj.dispGnaviPullDown();
    ersObj.imgChange();
    ersObj.inputFocus();
    ersObj.setFlexiGrid();
    ersObj.setJqueryUiDialog();
    ersObj.setJqueryUiDatePicker();
    ersObj.treeToggle();
    //	ersObj.smoothScroll();
    ersObj.smoothScroll2();
    ersObj.checkAll();
    ersObj.searchCate();
    ersObj.siteChange();
    ersObj.backSubmit();
});


/* ErsCommonオブジェクト生成コンストラクタ */
var ErsCommon = function () {

    var that = {};

    /* フォームsubmit処理
    ---------------------------------------------------------------- */
    that.formSubmit = function () {
        //flexigridの影響を避けるため、setTimeoutでキューの最後にもってくる
        window.setTimeout(function () {
            $(".form_btn").click(function () {
                var formInfo = $(this).attr("rel").split('$');
                //対象form名
                var formName = formInfo[0];
                //submit先
                var formPath = formInfo[1];
                //submit処理
                $("#" + formName).attr("action", formPath).submit();
            })
        }, 0)
    }

    /* グローバルプルダウンメニュー表示
    ---------------------------------------------------------------- */
    that.dispGnaviPullDown = function () {
        var objGnavi = $("#ERS_g_navi");
        var objPullDown = $("ul.pulldown", objGnavi)
        $("ul.gnavi li a", objGnavi).mouseenter(function () {
            $("ul li", objGnavi).removeClass("navi_over");
            $(this).closest("li").addClass("navi_over");
            objPullDown.css("left", $(this).offset().left).show();
            $("li", objPullDown).hide();
            $("li[rel=" + $(this).attr("rel") + "]", objPullDown).show();
        });
        objGnavi.mouseleave(function () {
            objPullDown.hide();
            $("ul li", objGnavi).removeClass("navi_over");
            $("li", objPullDown).hide();
        });
    }

    /* ロールオーバーイメージ表示
    ---------------------------------------------------------------- */
    that.imgChange = function () {
        var img_out; //mouseout時のsrc属性を格納
        var img_in; //mouseover時のsrc属性を格納
        var objChangeImg = $(".change");
        var hoverDom = {	//マウスオーバーされているDOM要素
            "tarDom": "",
            "img_out": ""
        };

        objChangeImg.hover(function () {
            //mouseover時の処理
            img_out = $(this).attr("src");
            img_in = img_out.replace("_off.gif", "_on.gif");
            $(this).attr("src", img_in);
            hoverDom = {
                "tarDom": $(this),
                "img_out": img_out
            };
        },
		function () {
		    //mouseout時の処理
		    $(this).attr("src", img_out);
		});

        //ページ移動時にロールオーバーを戻す
        $(window).unload(function () {
            //ブラウザバックの時にエラーになるため、try～catch
            try {
                hoverDom["tarDom"].attr("src", hoverDom["img_out"]);
            } catch (e) {
                //何もしない
            }
        });
    }

    /* titleに設定された文言でアラート表示
    ---------------------------------------------------------------- */
    that.submitBtnConfirm = function () {
        $(".submit_btn").click(function () {
            if (!window.confirm($(this).attr("title"))) {
                return false;
            }
        })
    }

    /* inputフォーカス時に背景色変更
    ---------------------------------------------------------------- */
    that.inputFocus = function () {
        var bgColor = "#ffF4e1"
        var tarDom = $("input[type='text'],input[type='password'],textarea");

        tarDom.focus(function () {
            $(this).css("backgroundColor", bgColor);
        });

        tarDom.blur(function () {
            $(this).css("backgroundColor", "");
        });
    }

    /* flexigridを設定
    ---------------------------------------------------------------- */
    that.setFlexiGrid = function () {
        //class=flexigridの以下にあるtableに設定
        try {
            $jquery_flexgrid(".flexigrid table").flexigrid();
        } catch (e) {
            //何もしない
        }
    }

    /* jquery ui Dialogの設定
    ---------------------------------------------------------------- */
    that.setJqueryUiDialog = function () {

        var tarForm = "";
        var tarQuery = "";

        //class=disp_dialogで始まるもの
        $("[class^=disp_dialog]").click(function () {
            tarForm = $(this).attr("href");
            var dialog_name = ($(this).attr("class")).replace("disp_", "")
            $("#" + dialog_name).dialog('open')
            return false;
        });

        $("[id^=dialog]").dialog({
            autoOpen: false, // hide dialog  
            bgiframe: true, 	// for IE6  
            resizable: false,
            height: 140,
            modal: true,
            buttons: [
                {
                    text: " OK ",
                    class: "__dialog_ok_button",
                    "click": function () {
                        $(this).dialog('close');
                        //aタグに自動でパラメータ付与されるので除外する。
                        var quesIndex = tarForm.indexOf("?");
                        if (quesIndex > 0) {
                            tarQuery = tarForm.substring(quesIndex + 1);
                            tarForm = tarForm.substring(0, quesIndex);

                            var urlArr = document[tarForm].action.split("#");
                            var tarUrl = urlArr[0];
                            tarUrl = tarUrl + (tarUrl.indexOf("?") > 0 ? "&" : "?") + tarQuery;
                            if (urlArr.length > 1) {
                                tarUrl = tarUrl + "#" + urlArr[1];
                            }
                            document[tarForm].action = tarUrl;
                        }
                        //document[tarForm].submit();

                        //used native submit event to detect the native onsubmit event
                        $(document[tarForm]).trigger('submit');
                    }
                },
                {
                    text: "キャンセル",
                    class: "__dialog_cancel_button",
                    "click": function () {
                        $(this).dialog('close');
                    }
                }
            ]
        });

        if ($('#lang').val() != "ja-JP") {
            $('.ui-dialog-buttonpane button:contains(キャンセル) span').text('ＣＡＮＣＥＬ')
        }
    }

    /* jquery ui DatePickerの設定
    ---------------------------------------------------------------- */
    that.setJqueryUiDatePicker = function () {        
        
        if ($('#lang').val() =="ja-JP") {
            
            //datepicker共通設定
            var commonSetup = {
                closeText: '閉じる',
                prevText: '&#x3c;前',
                nextText: '次&#x3e;',
                currentText: '今日',
                monthNames: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
                monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
                dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
                dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
                dayNamesMin: ['日', '月', '火', '水', '木', '金', '土'],
                weekHeader: '週',
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: true,
                yearSuffix: '年'
            };

            $(".datepicker_start").datepicker($.extend({}, commonSetup, {
                dateFormat: "yy/mm/dd 00:00:00"
            }));

            $(".datepicker_end").datepicker($.extend({}, commonSetup, {
                dateFormat: "yy/mm/dd 23:59:59"
            }));

            $(".datepicker_date").datepicker($.extend({}, commonSetup, {
                dateFormat: "yy/mm/dd"
            }));           

        } else {
            $(".datepicker_start").datepicker({
                dateFormat: "yy/mm/dd 00:00:00"
            });

            $(".datepicker_end").datepicker({
                dateFormat: "yy/mm/dd 23:59:59"
            });

            $(".datepicker_date").datepicker({
                dateFormat: "yy/mm/dd"
            });           
        }
    }

    /* サイドメニューツリー表示
    ---------------------------------------------------------------- */
    that.treeToggle = function () {
        $(".tree").click(function () {
            $("." + $(this).attr("rel")).toggle();
            $(this).toggleClass("tree_open")
        })

    }

    /* チェックボックス全選択、全解除
    ---------------------------------------------------------------- */
    that.checkAll = function () {
        $('[id^=chk_all]').click(function () {
            var areaName = 'item_' + $(this).attr('class');
            $('.' + areaName).attr('checked', 'checked');
        });

        $('[id^=chk_none]').click(function () {
            var areaName = 'item_' + $(this).attr('class');
            $('.' + areaName).attr('checked', '');
        });
    }


    /* スムーススクロール
    ---------------------------------------------------------------- */
    that.smoothScroll = function () {
        var a_list = $("a[href^='#']").click(function (e) {
            var end = $(this).attr("href");
            ahead = end.substr(1);
            start_scroll(ahead);
            return false;
        });

        function start_scroll(end) {
            //出発地、到着地の要素取得
            var end_posi;
            try {
                end_posi = $("#" + end).get(0);
            } catch (e) {
                try {
                    end_posi = $("@name=" + end).get(0);
                } catch (e) {
                    return false;
                }
            }

            //出発地、到着地の座標取得処理
            var start_co;
            var screen_size;
            var doc_height = document.body.offsetHeight;
            var end_co = end_posi.offsetTop;

            if (ErsLib.isIE()) { // IE判別
                start_co = document.documentElement.scrollTop;
                screen_size = document.documentElement.clientHeight;
            }
            else {
                start_co = window.pageYOffset;
                screen_size = window.innerHeight;
            }

            if (doc_height - end_co < screen_size) {
                end_co = doc_height - screen_size;
            }

            var sabun = end_co - start_co;
            if (sabun == 0) return false;

            //スクロールスピードとかの設定
            var speed = Math.round(sabun / 10); //スクロールスピードの設定
            var riding = Math.round(sabun * 1 / 2); //着地の設定
            var scroll = window.scrollBy;
            var page_ck; //ページ下まで行った時のチェック用
            var doc_ele = document.documentElement;
            // マウス操作でスクロールストップ
            if (window.addEventListener) window.addEventListener("DOMMouseScroll", scroll_stop, false);
            window.onmousewheel = document.onmousewheel = scroll_stop;
            function scroll_stop() {
                window.clearInterval(scroll_trigger);
            }

            // スクロールスタートのトリガー
            if (sabun < 0) { // 下から上へ移動する場合
                if (ErsLib.isIE()) { // IE用処理
                    var scroll_trigger = setInterval(up_timer_ie, 2);
                }
                else {
                    var scroll_trigger = setInterval(up_timer, 2);
                }
            }
            else { // 上から下へ移動する場合
                if (ErsLib.isIE()) { // IE用処理
                    var scroll_trigger = setInterval(bottom_timer_ie, 2);
                }
                else {
                    window.clearInterval(scroll_trigger);
                    var scroll_trigger = setInterval(bottom_timer, 2);
                }
            }

            // スクロールさせる関数
            // 下から上へスクロールさせる場合（InternetEXplorer用）
            function up_timer_ie() { // 
                if (end_co - doc_ele.scrollTop < riding) {
                    scroll(0, speed);
                }
                else if ((end_co - doc_ele.scrollTop) / 10 < -1) {
                    scroll(0, (end_co - doc_ele.scrollTop) / 10);
                }
                else {
                    scroll(0, -1);
                    if (end_co - doc_ele.scrollTop == 0) window.clearInterval(scroll_trigger);
                }
            }

            // 下から上へスクロールさせる場合（IE以外のモダンブラウザ用）
            function up_timer() {
                if (end_co - window.pageYOffset < riding) {
                    scroll(0, speed);
                }
                else if ((end_co - window.pageYOffset) / 10 < -1) {
                    scroll(0, (end_co - window.pageYOffset) / 10);
                }
                else {
                    scroll(0, -1);
                    if (end_co - window.pageYOffset == 0) window.clearInterval(scroll_trigger);
                }
            }

            // 上から下へスクロールさせる場合（InternetEXplorer用）
            function bottom_timer_ie() {
                if (end_co - doc_ele.scrollTop > riding) {
                    page_ck = end_co - doc_ele.scrollTop;
                    scroll(0, speed);
                    if (page_ck == end_co - doc_ele.scrollTop) window.clearInterval(scroll_trigger);
                }
                else if ((end_co - doc_ele.scrollTop) / 10 > 1) {
                    page_ck = end_co - doc_ele.scrollTop;
                    scroll(0, page_ck / 10);
                    if (page_ck == end_co - doc_ele.scrollTop) window.clearInterval(scroll_trigger);
                }
                else {
                    scroll(0, 1);
                    if (end_co - doc_ele.scrollTop == 0) window.clearInterval(scroll_trigger);
                }
            }

            // 上から下へスクロールさせる場合（IE以外のモダンブラウザ用）
            function bottom_timer() {
                if (end_co - window.pageYOffset > riding) {
                    page_ck = end_co - window.pageYOffset;
                    scroll(0, speed);
                    if (page_ck == end_co - window.pageYOffset) window.clearInterval(scroll_trigger);
                }
                else if ((end_co - window.pageYOffset) / 10 > 1) {
                    page_ck = end_co - window.pageYOffset;
                    scroll(0, page_ck / 10);
                    if (page_ck == end_co - window.pageYOffset) window.clearInterval(scroll_trigger);
                }
                else {
                    scroll(0, 1);
                    if (end_co - window.pageYOffset == 0) window.clearInterval(scroll_trigger);
                }
            }
        }
    }

    /* スムーススクロール（フェードイン）
    ---------------------------------------------------------------- */
    that.smoothScroll2 = function () {
        // Show or hide the sticky footer button
        $(window).scroll(function () {
            if ($(this).scrollTop() > 200) {
                $('#page_top').fadeIn(200);
            } else {
                $('#page_top').fadeOut(200);
            }
        });

        // Animate the scroll to top
        $('#page_top').click(function (event) {
            event.preventDefault();

            $('html, body').animate({ scrollTop: 0 }, 300);
        });


    }

    /* 商品検索画面でのカテゴリ表示
    ---------------------------------------------------------------- */
    that.searchCate = function () {
        var form_change = function (cate_num) {
            var cate_checked = new Array(5);
            var formObj = new Array(5);
            var j; 	//汎用

            if (cate_num == "") {
                cate_num = 5;
            }

            //選択されているカテゴリを取得。
            for (cnt = 1; cnt <= 5; cnt++) {
                //selectのオブジェクトを生成。
                formObj[cnt] = eval("document.review_form.cate" + cnt)
                if (!!formObj[cnt]) {
                    //javascript稼動時
                    if (!form_selected[cnt]) {
                        for (i = (formObj[cnt].length + 1); i >= 0; i--) {
                            if (formObj[cnt].options[i] && formObj[cnt].options[i].selected && cate_num >= cnt) {
                                cate_checked[cnt] = formObj[cnt].options[i].value
                            }
                        }
                        //全画面からpostされた値でselected
                    } else {
                        //check parent value, reset children if parent is changed
                        if (cnt > 1) {
                            if (formObj[cnt - 1].value != form_selected[cnt - 1]) {
                                cate_checked[cnt] = formObj[cnt].value;
                                for (i = cnt; i <= 5; i++) {
                                    cate_checked[i] = "";
                                    form_selected[i] = "";
                                }
                            }
                            else if (formObj[cnt].value != form_selected[cnt]) {
                                cate_checked[cnt] = formObj[cnt].value;
                            }
                            else {
                                cate_checked[cnt] = form_selected[cnt];
                            }
                        }
                        else {
                            if (formObj[cnt].value == form_selected[cnt]) {
                                cate_checked[cnt] = form_selected[cnt];
                            }
                            else {
                                cate_checked[cnt] = formObj[cnt].value;
                            }
                        }
                    }
                }
            }


            //フォームを一旦すべてリセット。
            for (cnt = 1; cnt <= 5; cnt++) {
                if (!!formObj[cnt]) {
                    for (i = (formObj[cnt].length + 1); i >= 0; i--) {
                        formObj[cnt].options[i] = null;
                    }
                }
            }

            //カテゴリを順に表示
            for (cnt = 1; cnt <= 5; cnt++) {

                //フォーム自体が出現していないと、処理しない。
                if (formObj[cnt]) {

                    //値を初期化
                    j = 1;

                    //フォームの一番上を作っておく。
                    formObj[cnt].options[0] = new Option("全て", "");

                    for (i = 0; i < form_array_cate[cnt].length; i++) {

                        //全表示する場合
                        if (cnt == 1 || cate_relation_level < cnt) {
                            formObj[cnt].options[j] = new Option(form_array_cate[cnt][i].value, form_array_cate[cnt][i].id);
                            j++;

                            //親カテゴリによって、表示を絞る場合。
                        } else if (form_array_cate[cnt][i].parent_id == cate_checked[cnt - 1] && cate_checked[cnt - 1] != 0) {
                            formObj[cnt].options[j] = new Option(form_array_cate[cnt][i].value, form_array_cate[cnt][i].id);
                            j++;

                        }
                        //選択済み
                        if (j > 0 && formObj[cnt].options[j - 1] && formObj[cnt].options[j - 1].value == cate_checked[cnt]) {
                            formObj[cnt].options[j - 1].selected = true
                        }
                    }

                    //アイテムが出現しなければ、非活性
                    if (j == 1) {
                        formObj[cnt].style.backgroundcolor = "#DDDDDD";
                        formObj[cnt].disabled = true;
                    } else {
                        formObj[cnt].disabled = false;
                    }
                }
            }
        };


        //onchangeイベントをバインド
        $('select[id^="cate"]').change(function () {
            form_change($(this).attr('id').slice(-1));
        }).change();
    }

    /* サイト切り替え
    ---------------------------------------------------------------- */
    that.siteChange = function () {

        //onchangeイベントをバインド
        $('select[id^="site_select"]').change(function () {
            var site_id = $(this).val();
            var url = location.href;

            //Form作成
            var form = document.createElement("form");
            form.setAttribute("action", url);
            form.setAttribute("method", "post");
            form.style.display = "none";
            document.body.appendChild(form);

            //パラメタ設定
            var input = document.createElement('input');
            input.setAttribute('type', 'hidden');
            input.setAttribute('name', 'site_id');
            input.setAttribute('value', site_id);
            form.appendChild(input);

            //submit
            form.submit();
        });
    }

    /* add history back to button if return url is not specified (IE only)
    ---------------------------------------------------------------- */
    that.backSubmit = function () {
        $(".back_button").click(function () {
            var userAgent = window.navigator.userAgent.toLowerCase();
            var appVersion = window.navigator.appVersion.toLowerCase();
            if (userAgent.indexOf("msie") >= 0) {
                //IE6・IE7・IE8
                history.back();
            }
        });
    }

    return that;

}

