/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function(){
	var ersObj = CateRegist();
	ersObj.dispEditCate();
});

/* CateRegistオブジェクト生成コンストラクタ */
var CateRegist = function(){

	var that = {};

	/* 編集エリア表示切り替え
	 ---------------------------------------------------------------- */
	that.dispEditCate = function(){
		$(".edit_cate").click(function(){
			var cateName = "edit_cate" + $(this).attr("id");
//			$(".cate_table>div").fadeOut("fast",function(){
//				$("." + cateName).fadeIn("fast");
//			});
			$(".cate_table>div").hide();
			$("." + cateName).fadeIn("slow");
		});
	}

	return that;
}

