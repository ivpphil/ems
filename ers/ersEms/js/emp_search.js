﻿

// Angular Coding
var app = angular.module('app', []);
// Data Factory
app.factory('Data', function () {
    return {
        Model: '',
        pagerModel: {
            pager: [],
            pager_parts: [],
            pager_part:''
        },
        prevModel: '',
        manageModel: '',

    };
});

//Value checker if not null, undefined and empty
function NotNullUndefinedEmpty(value) {
    return (value !== null && !angular.isUndefined(value) && value !== "");
}


app.controller('EmpSearchController', function ($scope, $http, Data,$rootScope) {
    $scope.Data = Data;

    $scope.$on('UpdateList', function (event) {
 
        getPageList();
    });
    //converts emp number to in for sorting
    $scope.sorterFunc = function (employee) {
        return parseInt(employee.emp_no);
    };

    //Filter for employee list 
    $scope.criteriaMatch = function (criteria) {

        //prev next pagination checker 
        if ($scope.Data.pagerModel.pager_parts.length > 1)
        {
            if ($scope.Data.pagerModel.pager_part === 0) {
                $("#prev").hide();
            } else {
                $("#prev").show();
            }

            if ($scope.Data.pagerModel.pager_part === $scope.Data.pagerModel.pager_parts.length - 1) {
                $("#next").hide();
            } else {
                $("#next").show();
            }     
        }

        // boolean return for filter based on criteria 
        return function (item, position, team) {
            var returnBool = true;
            if (angular.isUndefined(criteria)) {
                return returnBool;
            }
            else {

                if (NotNullUndefinedEmpty(criteria.s_keyword)) {
                    var _cri = new RegExp(".*" + criteria.s_keyword.toLowerCase() + ".*");
                    returnBool = (item.emp_no.toLowerCase().match(_cri) || item.fname.toLowerCase().match(_cri) || item.lname.toLowerCase().match(_cri) || item.email.toLowerCase().match(_cri));   

                    //reset pageer and page_part in new search
                    $scope.Data.pagerModel.pager_part = 0
                    $scope.initSetPageList(1);
                }
            }

            if (NotNullUndefinedEmpty(criteria.team_filter)) {
                returnBool = (returnBool && item.team === parseInt(criteria.team_filter));
                //reset pageer and page_part in new search
                $scope.Data.pagerModel.pager_part = 0;
                $scope.initSetPageList(1);
            }

            if (NotNullUndefinedEmpty(criteria.position_filter)) {
                returnBool = (returnBool && item.job_title === parseInt(criteria.position_filter));
                //reset pageer and page_part in new search
                $scope.Data.pagerModel.pager_part = 0;
                $scope.initSetPageList(1);
            }

            return returnBool;
        }
    };

    // initialize list on refresh
    $(function () {
        LoadList();        
    });

    function LoadList() {
        $('input[name="s_keyword"]').keypress(function (e) {
            if (event.which == 13) {
                event.preventDefault();
                return false;
            }
        });
        getPageList()
    }

    function getPageList() {
        var url_arry = (window.location.href).split("top");
        var prev_url = url_arry[0] + 'top/api/asp/getEmployeeList.asp';
        var formValues = $("#form_search").serializeArray();
        formValues.push({ name: "url", value: url_arry[0] });
        $http({ method: 'POST', data: $.param(formValues), headers: { 'Content-Type': 'application/x-www-form-urlencoded' }, url: prev_url }).
            success(function (data, status, headers, config) {
                SetObjectValues(data);

            }).
            error(function (data, status, headers, config) {
                alert('error');
            });
    }

    //detail binder for dictionary
    function SetObjectValues(data) {
        $.each(data, function (key, value) {
            $scope.Data[key] = data[key];
        });

    }

    $scope.initPrevModel = function (hover, emp_no) {
        $scope.$broadcast("profilePrevHoverEvent", { hover: hover, emp_no, emp_no });
    }


    $scope.initDetailModel = function (click, emp_no) {
        $scope.$broadcast("detailClickEvent", { click, click, emp_no, emp_no });
    }

    $scope.initSetPageList = function(pageCnt)
    {
        $scope.$broadcast("SetPageListEvent", pageCnt);
    }

    $scope.initManage = function (emp_no) {
        $scope.$broadcast("manageEmployeeEvent", emp_no);
    }

    $scope.initDownloadCSVperPage = function() {
        $("#employeeCSVForm").submit();
    }

    $scope.sendMessage = function (emp_no)
    {
        $("recipient_emp_no").val(emp_no);
        $("#messageSubmit").submit();
    }

    $scope.initDownloadAllCSV = function(list){
        var arrId = new Array();

        $.each(list, function (key, value) {
            arrId.push(list[key].emp_no);
        });

        $('#emp_no_string').val(arrId.join('-'));
        $("#employeeCSVForm").submit();
    }

    $scope.initProfile = function (profile_emp_no,position)
    {
        if (position !== 3) {
            $scope.$broadcast("profilePrevHoverEvent", { hover: hover, profile_emp_no, profile_emp_no });
        }
        else {
            $("#profile_emp_no").val(profile_emp_no);
            $("#profileSubmit").submit();
        }
    }
});

//pager controller
app.controller("pagerController", function ($scope,Data) {
    $scope.Data = Data;

    //event for SetPageList
    $scope.$on("SetPageListEvent", function (event, pageCnt) {
        $scope.SetPageList(pageCnt);
    });

    //page previous navigator for page_part
    $scope.listPagePrev = function (part) {
        if ($scope.Data.pagerModel.pager_parts.length > 0) {
            if (part > 0) {
                $scope.Data.pagerModel.pager_part = part - 1;
                $scope.SetPageList(($scope.Data.pagerModel.pager_parts[$scope.Data.pagerModel.pager_part])[0]);
            }
        }
    }

    //page next navigator for page_part
    $scope.listPageNext = function (part) {
        if ($scope.Data.pagerModel.pager_parts.length > 0) {
            if (part < ($scope.Data.pagerModel.pager_parts.length - 1)) {
                $scope.Data.pagerModel.pager_part = part + 1;
                $scope.SetPageList(($scope.Data.pagerModel.pager_parts[$scope.Data.pagerModel.pager_part])[0]);
            }
        }
    }

    //set offset for emp_list (pager function)
    $scope.SetPageList = function (pageCnt) {
        var page_count;

        var limit = $scope.Data.Model.MaxItemCount;
        if (angular.isUndefined(pageCnt)) {
            page_count = 1;
        }
        else {
            page_count = pageCnt;
        }
        $scope.selected_page = page_count;
        var offset = (limit * (page_count - 1));
        $scope.Data.Model.offset = offset;
    }

    //get pager numbers 
    $scope.getPager = function (RecordCount) {
        $scope.Data.pagerModel.pager = [];
        $scope.t_no_page;
        $scope.Data.pagerModel.pager_parts = [];


        var limit = $scope.Data.Model.MaxItemCount
        var exact = (RecordCount % limit) === 0;

        if (exact) {
            //exact item count for page number
            t_no_page = Math.floor(RecordCount / limit);
        }
        else {
            //excess 
            t_no_page = (Math.floor(RecordCount / limit)) + 1;
        }

        //set pager parts for prev and next button
        for (var i = 1; i <= t_no_page; i++) {
            $scope.Data.pagerModel.pager.push(i);
            if ((i % limit) === 0 || i === t_no_page) {
                $scope.Data.pagerModel.pager_parts.push($scope.Data.pagerModel.pager);
                $scope.Data.pagerModel.pager = [];
            }
        }

        //hides prev next button if pager_parts <= 1
        if ($scope.Data.pagerModel.pager_parts.length <= 1) {
            $(".page-nav").hide();
        }
        else {
            $(".page-nav").show();
        }

        if (!NotNullUndefinedEmpty($scope.Data.pagerModel.pager_part)) {
            $scope.Data.pagerModel.pager_part = 0;
        }

        //show pager parts 
        return $scope.Data.pagerModel.pager_parts[$scope.Data.pagerModel.pager_part]

    }
});

//preview controller
app.controller("previewController", function ($scope, Data) {
    $scope.Data = Data;

    $scope.$on("profilePrevHoverEvent", function (event,args) {
        $scope.profilePrevHover(args)
    });
   
    $scope.profilePrevHover = function (args) {
        if (args.hover) {
            var elementPosition = $(event.toElement).position();
            populatePrevMod(args.emp_no);
            $("#divPrev").css("left", (elementPosition).left + 60).css("top", (elementPosition).top - 260);
            $("#divPrev").attr("hidden", false);
        }
        else {
            $("#divPrev").attr("hidden", true);
        }
    };

    //populate prevModel
    function populatePrevMod(emp_no) {
        var emp = $scope.Data.Model.list.find(x => x.emp_no === emp_no);
        if (!angular.isUndefined(emp)) {
            $scope.Data.prevModel = emp;
        }
    }

});

//detail controller
app.controller("detailController", function ($scope, Data) {
    $scope.Data = Data;
    $scope.$on("detailClickEvent", function (event, args) {
        $scope.detailModel(args)
    });

    $scope.detailModel = function (args) {
        if (args.click) {
            populateDetailMod(args.emp_no);
        }
    }
    //populate prevModel
    function populateDetailMod(emp_no) {
        var emp = $scope.Data.Model.list.find(x => x.emp_no === emp_no);
        if (!angular.isUndefined(emp)) {
            $scope.Data.detailModel = emp;
        }
    }


});

//manage controller
app.controller("manageController", function ($scope, $http, Data) {

    function SetObjectValues(data) {
        $.each(data, function (key, value) {
            $scope.Data[key] = data[key];
        });
    };
    $scope.Data = Data;

    $scope.$on("manageEmployeeEvent", function (event, emp_no) {
        populateManageMod(emp_no)
    });

    $scope.update = function (data) {
        $scope.master;
    }

    //populate prevModel
    function populateManageMod(emp_no) {
        var emp = $scope.Data.Model.list.find(x => x.emp_no === emp_no);
        if (!angular.isUndefined(emp)) {
            $scope.Data.manageModel = emp;
        }
    }

    var emp_no = emp_no;

   

    $('button[name="saveDetails"]').click(function () {

        $http({
            method: 'POST',
            data: $.param({
                emp_no: $scope.Data.manageModel.emp_no,
                job_title: $("#job_title").val(),
                status: $("#emp_status").val(),
                team: $("#team").val(),
                full_name: $scope.Data.manageModel.full_name,
                vacation_leave: $scope.Data.manageModel.vacation_leave,
                sick_leave: $scope.Data.manageModel.sick_leave,
            }), headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: (window.location.href).split("top")[0] + 'top/Employee/asp/emp_manage_complete.asp'
        }).
        success(function (data, status, headers, config) {
   
            SetObjectValues(data);
 
            $scope.$broadcast('UpdateList');

            $scope.initSetPageList(1);

            if (data["errorList"] != null) {
                $('#errorList').focus();
            } else {
                $('#manageModal').modal('toggle');
            }
        }).
        error(function (data, status, headers, config) {
            alert('error');
        });
    });

});

$(document).ready(function () {

   
});