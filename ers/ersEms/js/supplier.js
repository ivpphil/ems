/*============================================================================
Author and Copyright
製作者: IVP CO,LTD（http://www.ivp.co.jp/）
作成日: 2009-03-06
修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
処理の内容（使用ページ：そのページのファイル名）
---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function () {
    var ersObj = ErsEntry();
    ersObj.zipAutoedit();
});

/* ErsEntryオブジェクト生成コンストラクタ */
var ErsEntry = function () {
    
    var that = {};

    //formオブジェクト
    var objForm = $("form[name='reg_form']");


    /* 郵便番号から住所自動入力
    ---------------------------------------------------------------- */
    that.zipAutoedit = function () {
        var objAddress = {}; 	//郵便番号検索の引数設定オブジェクト
        //clickイベントをバインド
        $("#zip_flg1").click(function () {
            document.getElementById("zip").value = document.getElementById("zip1").value + "-" + document.getElementById("zip2").value
            var thisId = $(this).attr("id");

            if (thisId === "zip_flg1") {
                //address1用引数の設定
                objAddress = {
                    "domZip1": $("#zip1"),
                    "domZip2": $("#zip2"),
                    "domZip": $("#zip"),
                    "domPref": $("#pref"),
                    "domAddressComplete": $("#address"),
                    "zip_search_error": $("#zip_search_error")
                }
            }

            //郵便番号検索
            ErsLib.zipSearch(objAddress);

            return false;
        });
    }

    return that;
}
