/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function(){
    var ersObj = ErsUiDialogPLUpCommon();
    ersObj.setEventUiDialogPlupload();
	ersObj.setJqueryUiDialogPlupload();
});


/* ErsCommonオブジェクト生成コンストラクタ */
var ErsUiDialogPLUpCommon = function () {

    var that = {};

    that.setEventUiDialogPlupload = function () {
        //class=disp_plupload_dialogで始まるもの
        var p = $("[class^=disp_plupload_dialog]");
        $.each(p, function (c) {
            p[c].onclick = function () {

                var dialog_name = ($(this).attr("class")).replace("disp_", "");
                $("#" + dialog_name).dialog('open');

                var table_id = $("#" + dialog_name + ' div:eq(0)').attr('id');
                if (ErsImageValues.IsDestroyedUploader(table_id) == false) {//Only if browser support set runtime
                    if ($("#" + table_id).plupload('option')) {

                        var options = $("#" + table_id).plupload('option');

                        var default_focus = 1;
                        if (options.ers_custom_options.autofocus) {
                            default_focus = options.ers_custom_options.autofocus;
                        }

                        var radio_id = $('#' + dialog_name + ' .plupload_view_switch :radio:eq(' + default_focus + ')').attr('id');
                        $('label[for=' + radio_id + ']').click();
                    }
                }
            }
        });
    }

    /* jquery ui Dialogの設定
    ---------------------------------------------------------------- */
    that.setJqueryUiDialogPlupload = function () {

        $("[id^=plupload_dialog]").dialog({
            autoOpen: false, // hide dialog  
            bgiframe: true, 	// for IE6  
            resizable: false,
            width: 500,
            height: 430,
            modal: true,
            buttons: {
                "閉める": function () {
                    $(this).dialog('close');
                }
            }
        });

    }

    that.refreshEventUntilFlexgridCompleted = function (flexg_id, e) {
        var n = o || $.browser;

        if (o && o.Env) {
            n = o.Env;
        }

        if (n.msie) {
            n.browser = 'ie';
        }

        if (n.browser.toLowerCase() === 'ie' && n.version < 9) {
            $(document).ready(function () {

                if (!e) {
                    e = 'div';
                }

                var retry = 1;
                var sid = setInterval(function () {
                    if (retry >= 10) {
                        clearInterval(sid); return false;
                    }

                    var u = ['nBtn', 'nDiv', 'hDiv', 'cDrag', 'bDiv', 'vGrip'];
                    var result = [];
                    $.each(u, function (c) {
                        if ($(e + '[flexg_id="' + flexg_id + '"]').find('div[class*="' + u[c] + '"]').length > 0) {
                            result.push(true);
                        }
                    });


                    if (result.length >= u.length) {
                        clearInterval(sid);
                        that.setEventUiDialogPlupload();
                        return false;
                    }

                    retry++;
                }, 1000);
            });
        }
    }

    return that;

}

