﻿function get_shift(inputField) {
    var host = location.host;
    var protocol = location.protocol;
    var res = inputField.getAttribute("id").substring(11, 12);

    var s_date_start = $("#record_key_" + res + "_date_start").val();
    $.ajax({
        url: protocol + "//" + host + "/top/api/asp/getShiftSched.asp",
        data: {
            s_date_start: s_date_start
        },
        success: function (data) {
            if (data["Shift_schedule"] == "" || data["Shift_schedule"] == null) {
                $("#record_key_" + res + "_shift_schedule").val(" ");
                $("#record_key_" + res + "_shift_sched").text(" ");
                $("#record_key_" + res + "_ot_hours").val(" ");
                $("#record_key_" + res + "_othours").text(" ");
            }
            else {
                $("#record_key_" + res + "_shift_schedule").val(data["Shift_schedule"]);
                $("#record_key_" + res + "_shift_sched").text(data["Shift_schedule"]);
            }
        }
    });
}

function get_otHours(inputfield) {
    var host = location.host;
    var protocol = location.protocol;
    var row_index = inputfield.getAttribute("id").substring(11, 12);
    //var date_start = $("#record_key_" + row_index + "_date_start").val();
    //var start_hours = $("#record_key_" + row_index + "_start_hours").val();
    //var start_minutes = $("#record_key_" + row_index + "_start_minutes").val();
    //var start_period = $("#record_key_" + row_index + "_start_period").val();
    //var end_hours = $("#record_key_" + row_index + "_end_hours").val();
    //var end_minutes = $("#record_key_" + row_index + "_end_minutes").val();
    //var end_period = $("#record_key_" + row_index + "_end_period").val();
    $.ajax({
        url: protocol + "//" + host + "/top/api/asp/getOtHours.asp",
        data: {
            start_hours: $("#record_key_" + row_index + "_start_hours").val(),
            start_minutes: $("#record_key_" + row_index + "_start_minutes").val(),
            start_period: $("#record_key_" + row_index + "_start_period").val(),
            end_hours: $("#record_key_" + row_index + "_end_hours").val(),
            end_minutes: $("#record_key_" + row_index + "_end_minutes").val(),
            end_period: $("#record_key_" + row_index + "_end_period").val(),
            date_start: $("#record_key_" + row_index + "_date_start").val()
        },
        success: function (data) {
            if (data["ot_hours"] == "" || data["ot_hours"] == null) {
                $("#record_key_" + row_index + "_ot_hours").val(" ");
                $("#record_key_" + row_index + "_othours").text(" ");
            }
            else {
                $("#record_key_" + row_index + "_ot_hours").val(data["ot_hours"]);
                $("#record_key_" + row_index + "_othours").text(data["ot_hours"]);
            }
        }
    });
}

function OnclickIn(inputField, column) {
    var inputName = $(inputField).attr("name");

    var row_index = $(inputField).closest("tr").index() - 1;
    var record_key = "record_key_"
    var new_name = "";

    var matchStringIndex = inputName.localeCompare("record_key_");
    if (matchStringIndex == -1) {
        new_name = record_key + row_index + "_" + inputName;
        $(inputField).attr("name", new_name);
        $(inputField).attr("id", new_name);
        new_name = record_key + row_index + "_shift_sched";
        $("#shift_sched").attr("id", new_name);
        new_name = record_key + row_index + "_shift_schedule";
        $("#shift_schedule").attr("name", new_name);
        $("#shift_schedule").attr("id", new_name);
        new_name = record_key + row_index + "_emp_no";
        $("#emp_no").attr("name", new_name);
        $("#emp_no").attr("id", new_name);
    }
    else if (matchStringIndex == 1) {

    }
    else {
        var defaultFieldname = "";

        switch (column) {


            case 0:
                defaultFieldname = "date_start";
                break;
        }

        $(inputField).attr("name", defaultFieldname);
        $(inputField).attr("id", defaultFieldname);
    }
}

$(document).ready(function () {

    $("#contents").delegate(".date_start", "change", function () {
        OnclickIn(this, 0);
        get_shift(this);
        get_otHours(this);
    });
    $("#contents").delegate(".check-ot", "change", function () {

        //var row_index = $(this).closest("tr").index() - 1;
        var row_index = this.getAttribute("id").substring(11, 12);
        var record_key = "record_key_"
        var new_name = "";
        new_name = record_key + row_index + "_othours";
        $("#othours").attr("id", new_name);
        new_name = record_key + row_index + "_ot_hours";
        $("#ot_hours").attr("name", new_name);
        $("#ot_hours").attr("id", new_name);
        new_name = record_key + row_index + "_end_period";
        $("#end_period").attr("name", new_name);
        $("#end_period").attr("id", new_name);
        get_otHours(this);
    });
});