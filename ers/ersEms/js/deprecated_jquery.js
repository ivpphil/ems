/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function(){
    var ersObj = ErsDeprecatedJquery();
    ersObj.Init();
});


/* ErsCommonオブジェクト生成コンストラクタ */
var ErsDeprecatedJquery = function () {

    var that = {};

    /* チェックボックス全選択、全解除
    ---------------------------------------------------------------- */
    that.Init = function () {
        (function ($) {         

            $.browser = {
                msie: ErsLib.isIE() || ErsLib.getBrowser() == "Other",
                opera: ErsLib.getBrowser() == "Opera",
                mozilla: ErsLib.getBrowser() == "FX",
                safari: ErsLib.getBrowser() == "Safari" || ErsLib.getBrowser() == "Chrome",
            };

            if($.browser.msie){
                $.browser.version = checkV(window.navigator.userAgent.toLowerCase()).version;
            }

        })(jQuery);
    }

    return that;


}

var checkV = function (ver) {r = /(webkit)[ \/]([\w.]+)/; s = /(opera)(?:.*version)?[ \/]([\w.]+)/; t = /(msie) ([\w.]+)/; u = /(mozilla)(?:.*? rv:([\w.]+))?/; ver = ver.toLowerCase(); var b = r.exec(ver) || s.exec(ver) || t.exec(ver) || ver.indexOf("compatible") < 0 && u.exec(ver) || []; return { browser: b[1] || "", version: b[2] || "0"} }