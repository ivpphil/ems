﻿$(document).ready(function () {
    var ersObj = ErsColumnsPanel();
    //function for listing the columns per table selection
    ersObj.hoverTablecolumns();

    //function for hovering of columns
    ersObj.hoverColumns();

    //functions for mouseover event
    ersObj.mouseover();

    //functions for click event
    ersObj.click();
});

var ErsColumnsPanel = function () {
    var that = {};

    that.hoverTablecolumns = function () {
        $('#addColumn').click(function () {
            selectedExpression = false;

            var test = $("ul[id^='TablesCol_hover']");
            for (var i = 0; i < test.length; i++) {
                var x = test[i].attributes['id'].nodeValue;
                var getCount = x.substr(15);

                var sColID2 = "#TablesCol_hover" + getCount;
                displayNone(sColID2);
            }
            var popCol = "#colPop";
            var offset = $("#addColumn").offset();
            toggle(offset, popCol);

            return false;
        });
    }

    that.hoverColumns = function () {
        $('.columns').mouseover(function () {
            var sMainID = $(this).attr("id");
            var sColID = "#TablesCol_" + sMainID;

            $(sColID).toggle();

            var last = sMainID.substr(5);
            var sColID = "#TablesCol_hover" + last;

            var test = $("ul[id^='TablesCol_hover']");
            for (var i = 0; i < test.length; i++) {
                var x = test[i].attributes['id'].nodeValue;
                var getCount = x.substr(15);
                if (last != getCount) {
                    var sColID2 = "#TablesCol_hover" + getCount;
                    displayNone(sColID2);
                }
            }

            var d = '#' + $(this).attr('id');
            $(d).removeClass('columns');
            $(d).addClass('ui-state-hover');
        });
    }
    that.mouseover = function () {
        $("div").mouseover(
         function (e) {
             //             if (e.target.id != "addColumn") {
             //                 if (e.target.className != "columns hover_table") {
             //                     $("#colPop").hide();
             //                 }
             //             }

             var countId = e.target.id.indexOf('_');

             if (countId > 0) {
                 var splitID = e.target.id.split('_');
                 if (e.target.className == "selectedTitle")
                     var counter = splitID[0];
                 else
                     var counter = splitID[1];

                 if (countId == 10) {
                     if (e.target.id.substring(0, 11) == "sortbutton_") {
                         var splitId = e.target.id.split('_');
                         idSortSelected = splitId[1];
                     }
                 }
             }
             else
                 var counter = e.target.id.substr(3);

             if (counter != "") {
                 var qc = e.target.className.indexOf('ers-qc');

                 var buttonType = $("div[id^='column-button-type_" + counter + "']");

                 if (e.target.className.indexOf('ers-qc') >= 0 || e.target.className.indexOf('selectedTitle') >= 0
             || e.target.className.indexOf('selectExp') >= 0) {

                     var divSortButton = 'div#sortbutton_' + counter + '_';
                     bckImageNone(divSortButton, false);

                     styleNone('div#sortbutton_' + counter + '_');
                     if (buttonType[0] != undefined)
                         styleNone('div#' + buttonType[0].id);

                     styleNone('div#column-button-delete_' + counter + '_');
                     displayInline('div.ers-qc-column-buttonsBlock');
                 }
                 else {
                     styleNone('div#condition-button-delete_' + counter + '_');
                     styleNone('div#condition-button-type_' + counter + '_');

                     displayInline('ers-qp-condition-buttonsBlock');

                 }
                 var test = $("div[id^='ent']");
                 for (var i = 0; i < test.length; i++) {
                     var x = test[i].attributes['id'].nodeValue;
                     var getCount = x.substr(8);
                     if (counter != getCount) {

                         var buttonType = $("div[id^='column-button-type_" + getCount + "']");
                         var divSortButton = 'div#sortbutton_' + getCount + '_';
                         var divbuttontype = 'div#' + buttonType[0].id;
                         var divColbuttondel = 'div#column-button-delete_' + getCount + '_';
                         var divConbuttondel = 'div#condition-button-delete_' + getCount + '_';
                         var divConbuttontype = 'div#condition-button-type_' + getCount + '_';

                         bckImageNone(divSortButton, true)

                         if (buttonType[0] != undefined)
                             bckImageNone(divbuttontype, true)

                         bckImageNone(divColbuttondel, true);
                         bckImageNone(divConbuttondel, true);
                         bckImageNone(divConbuttontype, true);

                         $("div.ers-qp-condition-buttonsBlock").attr('class', 'ers-qp-condition-buttonsBlock');
                     }
                 }
             }

             if (e.target.className == "hover_table selected" || e.target.className == "hover_table2 selected") {
                 $("a#" + e.target.id).addClass('ui-state-hover');
             }
             return false;
         });
    }

    that.click = function () {
        $("body").click(
         function (e) {
             if (e.target.id != "addColumn") {
                 if (e.target.className != "columns hover_table") {
                     $("#colPop").hide(); //hide popup columns             
                 }
             }

             //for append/changed selected columns
             if (e.target.className == "hover_table selected ui-state-hover") {
                 if (selectedExpression != true) {
                     documentCondClick = false;
                     idSelected = e.target.id;

                     var coltype = $('#type_' + idSelected);
                     var colname = $('#fldName_' + idSelected);
                     var coltblname = $('#tblName_' + idSelected);
                     var coltblID = $('#tableID_' + idSelected);

                     appendHTML(idSelected, e.target.innerHTML, e.target.innerHTML, coltype.val());
                 }
                 else {
                     var tabId = idSelected.split("_");
                     var idFunc = tabId[0] + '_' + tabId[1];
                     var changeValue = e.target.innerHTML;
                     $('#exp_' + idSelected).text(changeValue);

                     //                     if ($('a#func_' + idFunc).attr('style') == "") {
                     //                         var aggregate = $('a#func_' + idFunc).text();
                     //                         $('a#' + idSelected).text(changeValue + ' ' + aggregate);

                     //                     }
                     //                     else
                     //                         $('a#' + idSelected).text(changeValue);

                     var replaceId = tabId[0] + '_' + e.target.id

                     var elem = document.getElementById("function_" + tabId[0]);
                     var elemSort = document.getElementById("sort_" + tabId[0]);

                     $('input:text#editText_' + idSelected).text(changeValue);

                     $('#exp_' + idSelected).attr('id', 'exp_' + replaceId);
                     $('a#' + idSelected).attr('id', replaceId);
                     $('input:text#editText_' + idSelected).attr('id', 'editText_' + replaceId);

                     $('a#func_' + idFunc).attr('id', 'func_' + replaceId);
                     $('#span_' + idFunc).attr('id', 'span_' + replaceId);

                     $('#column-button-type_' + idFunc).attr('id', 'column-button-type_' + replaceId);
                     var changeType = $('#type_' + e.target.id).val();

                     $('#colType_' + idFunc).val(changeType);
                     $('a#func_' + replaceId).text('Count');
                     var editVal = $('#exp_' + replaceId).text();

                     elem.children[0].value = "2";

                     $('#colType_' + idFunc).attr('id', 'colType_' + tabId[0] + '_' + e.target.id);


                     $('a#' + replaceId).text(editVal + ' Count');
                     $('input:text#editText_' + replaceId).val(editVal + ' Count');

                     $('input:text#editText_' + replaceId).text(changeValue);

                     var xx = myCondTYpe(ctrCond, ctrCond, "true");

                     document.getElementById("colType_" + tabId[0] + "_" + e.target.id).value = ntype;

                     removeElement('trColumn_' + tabId[0]);

                     currentAggregate = "Count";

                     myColumnFields(tabId[0], e.target.id, changeValue, elemSort.children[0].value);

                     selectedExpression = false;

                     jsoncreateQuery();

                 }
             }

             //change of title name of columns
             if (e.target.className == "selectedTitle") {
                 documentCondClick = false;
                 editText(e);
                 idSelected = e.target.id;

                 displayInline('.selectedTitle');
                 displayInline('a#' + idSelected);
                 displayNone('input:text.ers-qc-ce-editbox');
                 displayNone('input:text.ers-qp-ve-editbox');
                 displayInline('.conditionValue');

                 var test = $("input[id^='editText_']");
                 for (var i = 0; i < test.length; i++) {
                     if (test[i].className == "ers-qc-ce-editbox") {
                         var getSelected = test[i].attributes['id'].nodeValue;
                         getSelected = getSelected.substr(9);
                         if (test[i].value != "")
                             var changeValue = test[i].value;
                         else
                             var changeValue = test[i].attributes["value"].nodeValue;

                         test[i].value = changeValue;
                         if (isSelected == true) {
                             $('a#' + getSelected).text(changeValue);
                         }
                     }
                 }

                 splitID = e.target.id.split('_');
                 idSelected = e.target.id;
                 e.target.style.cssText = "display: none;";

                 displayInline('input:text#editText_' + e.target.id);
                 $('input:text#editText_' + e.target.id).focus();

                 isSelected = true;
             }
             else if (e.target.className != "ers-qc-ce-editbox" && e.target.className != "ers-qp-ve-editbox") {
                 notEditBox(e);
             }

             //change of appended columns
             if (e.target.className == "selectExp") {
                 idSelected = e.target.id.substr(4);
                 selectedExpression = true;
                 var targetId = e.target.id;
                 var offset = $("#" + targetId).offset();
                 var popCol = "#colPop";
                 toggle(offset, popCol);
             }

             //delete selected columns
             if (e.target.className == "ers-qc-column-button ers-qc-column-button-delete") {
                 var splitID = e.target.id.split('_');
                 var counter = splitID[1];
                 $('div#entAdds_' + counter).remove();
                 $('tr#trColumn_' + counter).remove();
                 removeElement('tr#trColumn_' + counter);

//                 ctr = ctr - 1;

                 jsoncreateQuery();
             }

             //select function types per columns like Sum||Count||Average||Minimum|Maximum
             if (e.target.className == "ers-qc-column-button ers-qc-column-button-type"
                    || e.target.className == "selectFunction") {
                 var idTarget = e.target.id.split('_');

                 idColFunction = idTarget[1] + '_' + idTarget[2];
                 var styleFunc = $('#func_' + idColFunction);
                 if ($('#func_' + idColFunction).attr('style') != '' || e.target.className == "selectFunction") {
                     var offset = e.target.offsetTop;
                     var top = e.target.offsetTop + 20;
                     $("#colFunction").attr('style', "left:" + e.target.offsetLeft + "px;top:" + top + "px");
                     var colTypeId = $("input#colType_" + idColFunction).val();

                     var filterCol = $("div[id^='columnfunc']");

                     for (var i = 1; i <= filterCol.length; i++) {
                         $("#" + filterCol[i - 1].id).attr('id', 'columnfunc' + i + '_' + idColFunction);
                     }

                     for (i = 1; i < 6; i++) {
                         if (i == 1 || i == 3 || i == 4 || i == 5)
                             $("#columnfunc" + i + "_" + idColFunction).hide();
                     }

                     if (colTypeId == "Date") {
                         $("#columnfunc4_" + idColFunction).toggle();
                         $("#columnfunc5_" + idColFunction).toggle();
                     }
                     else if (colTypeId == "Integer") {
                         $("#columnfunc1_" + idColFunction).toggle();
                         $("#columnfunc3_" + idColFunction).toggle();
                         $("#columnfunc4_" + idColFunction).toggle();
                         $("#columnfunc5_" + idColFunction).toggle();
                     }

                     $("#colFunction").toggle();
                 }
                 else {
                     displayNone('a#func_' + idColFunction);
                     displayNone('span#span_' + idColFunction);

                     var editVal = $('#exp_' + idColFunction).text();
                     $('#' + idColFunction).text(editVal);
                     $('#editText_' + idColFunction).text(editVal);

                     var splitId = idColFunction.split('_');

                     removeElement("function_" + splitId[0]);

                     var HTML7 = '<div id="function_' + splitId[0] + '"><input type="hidden" id="functionType" name="record_key2_' + splitId[0] + '_functionType" value="" /></div>';
                     $('td#tdColumn_' + splitId[0]).append(HTML7);

                     removeElement("displayName_" + splitId[0]);

                     HTML7 = '<div id="displayName_' + splitId[0] + '"><input type="hidden" id="columndisplayname" name="record_key2_' + splitId[0] + '_columndisplayname" value="' + editVal + '" /></div>';
                     $('td#tdColumn_' + splitId[0]).append(HTML7);

                     jsoncreateQuery();
                 }
             }
             else {
                 idColFunction = "";
                 $("#colFunction").hide();
             }

             //selection of sorting like notsorted||ascending||descending
             if (e.target.className == "ers-qc-colelement ers-qc-sortbutton ers-qc-sortbutton-none") {
                 selectedExpression = false;

                 var offset = e.target.offsetTop;
                 var top = e.target.offsetTop + 20;
                 $("#colSortingPop").attr('style', "left:" + e.target.offsetLeft + "px;top:" + top + "px");
                 $("#colSortingPop").toggle();

                 iscolSortingPop = true;
             }
             else
                 $("#colSortingPop").hide();
         });
    }

    return that;

}

/////Functions for columns

//append selected columns
function appendHTML(id, name, nameThis, coltype) {
    ctr++;
    //
    var HTML1 = '<div class="ers-qc-row ers-qc-row-column-entattr" id="entAdds_' + ctr + '">';
    //        var HTML1 = '<tr id="divCol_' + ctr + '"><td id="divCol_td_' + ctr + '"><div class="ers-qc-row ers-qc-row-column-entattr" id="entAdds_' + ctr + '">';
    var HTML2 = '<div class="ers-qc-colelement ers-qc-sortbutton ers-qc-sortbutton-none" id="sortbutton_' + ctr + '_" title="Sorting" style="background-image: none;"></div>';
    var HTML3 = '<div class="ers-qc-expr-block" id="exp' + ctr + '">';
    var HTML4 = '<div class="ers-qc-colelement ers-qc-attrelement">';
    var HTML5 = '<input type="hidden" id="colType_' + ctr + '_' + id + '" value="' + coltype + '">';
    var HTML6 = '<a href="javascript:void(0)"  id="func_' + ctr + '_' + id + '" class="selectFunction" style="display: none;">Count </a>';
    var HTML7 = '<span id="span_' + ctr + '_' + id + '" class="ers-qc-colelement" style="display: none;">&nbsp;of</span>';
    //var HTML8 = '<a href="javascript:void(0)"  id="exp_' + ctr + '_' + id + '" class="selectExp">' + name + '</a></div></div></div>';
    var HTML8 = '<a href="javascript:void(0)"  id="exp_' + ctr + '_' + id + '" class="selectExp">' + name + '</a></div></div></div></td></tr>';
    var HTMLinsert = HTML1 + HTML2 + HTML3 + HTML4 + HTML5 + HTML6 + HTML7 + HTML8;

    $('#addVal').append(HTMLinsert);
    //        $('#columnTable').append(HTMLinsert);


    var HTML1 = '<div class="ers-qc-colelement ers-qc-captionelement">';
    var HTML2 = '<a href="javascript:void(0)" style="display: inline;" class="selectedTitle" id="' + ctr + '_' + id + '">' + nameThis + '</a>';
    var HTML3 = '<input class="ers-qc-ce-editbox" id="editText_' + ctr + '_' + id + '" type="text" style="display: none;"  value="' + nameThis + '"></div>';
    var HTML4 = '<div class="ers-qc-column-buttonsBlock" id="butBlck_' + ctr + '_' + id + '">';
    var HTML5 = '<div class="ers-qc-column-button ers-qc-column-button-type" id="column-button-type_' + ctr + '_' + id + '" title="Change to aggregate column" style="background-image: none;"></div>';
    var HTML6 = '<div class="ers-qc-column-button ers-qc-column-button-delete" id="column-button-delete_' + ctr + '_" title="Delete" style="background-image: none;"></div></div></div>';
    var HTMLinsert = HTML1 + HTML2 + HTML3 + HTML4 + HTML5 + HTML6;
    $('div#entAdds_' + ctr).append(HTMLinsert);

    myColumnFields(ctr, idSelected, name,'');

    jsoncreateQuery();

}

//apply selected sort order per column in sql
function itemDevColumns(e) {
    var myName = e.target.textContent;

    var myValue = 0;
    if (myName == "Ascending") myValue = 1;
    else if (myName == "Descending") myValue = 2;


    var splitId = e.target.id.split('_');

    var sortId = ctr;
    if (splitId[0] == "colSort") sortId = idSortSelected;


    removeElement("sort_" + sortId);

    var HTML7 = '<div id="sort_' + sortId + '"><input type="hidden" id="sort" name="record_key2_' + sortId + '_sort" value="' + myValue + '" /></div>';
    $('td#tdColumn_' + sortId).append(HTML7);

    idSortSelected = splitId[1];
    iscolSortingPop = false;

    jsoncreateQuery();
}

//apply selected function type per column in sql
function itemDevFunction(e) {
    var splitId = e.target.id.toString().split('_');
    var id = splitId[1] + '_' + splitId[2];
    var append = $('#exp' + splitId[1]);

    $('#func_' + id).attr('style', '');
    $('a#func_' + id).text(e.target.innerHTML);
    $('#span_' + id).attr('style', '');
    var editVals = $('#' + id).text();
    var editVal = $('#exp_' + id).text();
    $('#' + id).text(editVal + ' ' + e.target.innerHTML);
    $('#editText_' + id).text(editVal + ' ' + e.target.innerHTML);
    $('#editText_' + id).val(editVal + ' ' + e.target.innerHTML);

    //        isSelected = true;

    removeElement("function_" + splitId[1]);

    var HTML7 = '<div id="function_' + splitId[1] + '"><input type="hidden" id="functionType" name="record_key2_' + splitId[1] + '_functionType" value="' + getValueFunction(e.target.innerHTML) + '" /></div>';
    $('td#tdColumn_' + splitId[1]).append(HTML7);

    removeElement("displayName_" + splitId[1]);
    HTML7 = '<div id="displayName_' + splitId[1] + '"><input type="hidden" id="columndisplayname" name="record_key2_' + splitId[1] + '_columndisplayname" value="' + editVal + ' ' + e.target.innerHTML + '" /></div>';
    $('td#tdColumn_' + splitId[1]).append(HTML7);

    jsoncreateQuery()
}

//append selected columns for the processing of sql
function myColumnFields(ctrCond, idSelected, name,sortId) {
    var strKey = "record_key2_";

    var xx = myCondTYpe(ctrCond, ctrCond, "true");

    var HTML7 = "";
    HTML7 = '<tr id="trColumn_' + ctrCond + '" style="display:none"><td id="tdColumn_' + ctrCond + '">';

    HTML7 = HTML7 + '<div id="Columnfields_' + ctrCond + '">';
    HTML7 = HTML7 + '<input type="hidden" id="columntablename" name="' + strKey + ctrCond + '_columntablename" value="' + tn + '" />';
    HTML7 = HTML7 + '<input type="hidden" id="columnname" name="' + strKey + ctrCond + '_columnname" value="' + fn + '" />';
    HTML7 = HTML7 + '<input type="hidden" id="columntype" name="' + strKey + ctrCond + '_columntype" value="' + ntypeID + '" />';
    HTML7 = HTML7 + '<input type="hidden" id="columnID" name="' + strKey + ctrCond + '_columnID" value="' + ctrCond + '" />';
    HTML7 = HTML7 + '</div>'; //last for div Columnfields_

    HTML7 = HTML7 + '<div id="displayName_' + ctrCond + '"><input type="hidden" id="columndisplayname" name="' + strKey + ctrCond + '_columndisplayname" value="' + name + '" /></div>';
    HTML7 = HTML7 + '<div id="sort_' + ctrCond + '"><input type="hidden" id="sort" name="' + strKey + ctrCond + '_sort" value="'+sortId+'" /></div>';
    HTML7 = HTML7 + '<div id="function_' + ctrCond + '"><input type="hidden" id="functionType" name="' + strKey + ctrCond + '_functionType" value="' + getValueFunction(currentAggregate) + '" /></div>';

    HTML7 = HTML7 + '</td></tr>'///last for tr and td
  
    $('#columnTable').append(HTML7);
}