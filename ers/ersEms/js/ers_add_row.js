/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2014-04-25
        修正日: 2014-04-28

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//▼ Add Row FlexGridTableの設定 「START」
$(function () {
    var table_key = 'record_key_';
    var remove_prefix = "_remove_";

    var lineNumberArr = [];
    var max_lineNumber = -1;

    /*▼When clicking add row button 「START」
    add row buttonをこつこつすれば
    */
    $('.add_row_flexgrid').on('click', function () {
        var target_table_with_id = this.name;
        var table_id_selector = '#' + target_table_with_id;


        /*▼Search all name with record_key_ at {table id > tbody > tr > td } and add the lineNumber「START」
        {table id > tbody > tr > td }で、名前はrecord_key_を検索します
        lineNumberはをくわえる
        */
        $(table_id_selector + ' > tbody > tr > td [name*="' + table_key + '"]').each(function () {

            var name_key = this.name.replace(table_key, "");
            var lineNumber = name_key.split('_')[0];
            lineNumberArr.push(lineNumber);

        });
        /*▲ Search all name ith record_key_「END」*/


        /*▼ Get Max lineNumber「START」*/
        if (lineNumberArr != undefined && lineNumberArr.length != 0) {
            max_lineNumber = Math.max.apply(Math, lineNumberArr);
        }
        /*▲ Get Max lineNumber「END」*/


        if (max_lineNumber != -1) {

            //Create new clone for new TR
            var top_tr = $(table_id_selector + ' > tbody > tr > td [name*="' + table_key + max_lineNumber + '"]').closest('tr').clone(true);

            if (top_tr != undefined && top_tr.length == 1) {
                // Set next lineNumber
                var next_lineNumber = max_lineNumber + 1;

                //set value = empty for all new tags
                //if name has contain remove or class has keep_value and then keep the value
                top_tr.find('[name*="' + table_key + max_lineNumber + '"]').each(function () {
                    var setValue = '';

                    if (this.name.indexOf(remove_prefix) > -1) {
                        setValue = 1;
                    }

                    if ($(this).hasClass('keep_value')) {
                        setValue = this.value;
                    }

                    this.setAttribute('value', setValue);
                });

                //Find All class with label_lineNumber change text with next_lineNumber + 1
                //クラスはlabel_lineNumberテキストをかわる
                $(top_tr[0]).find('.label_lineNumber').text(next_lineNumber + 1);

                //Remove all class with removable_tag
                //クラスはremovable_tagをさくじょする
                $(top_tr[0]).find('.removable_tag').each(function () {

                    var remove_key = this.name.replace(table_key, "");
                    var remove_prop_name = table_key + next_lineNumber + remove_prefix + remove_key.split('_')[1];
                    $('<input type="hidden" />').attr({ name: remove_prop_name, value: 1 }).insertBefore(this);
                }).remove();

                //Replace all current record_key with next record_key
                //つぎrecord_keyにげんざいrecord_keyをかわる
                var regex = new RegExp(table_key + max_lineNumber, 'g')
                top_tr[0].innerHTML = top_tr[0].innerHTML.replace(regex, table_key + next_lineNumber);

                //Append created tr clone at table id +  > tbody
                //table id +  > tbodyでtr cloneをくわえる
                var append_clone = top_tr.appendTo($(table_id_selector + ' > tbody'));

                //Re-Initialize the Datepicker for New TR
                //新TRために、DatapickerをRe-Initializeします
                append_clone.find('[class*="datepicker"]').removeClass('hasDatepicker');
                ErsCommon().setJqueryUiDatePicker();


            }
        }

    });

});