﻿var month_counter = null;
var bindKey = null;
var recKey = "record_key_";
var strKey = "_inner_record_key_";
var months = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
var c_year = null;
var pBindKey = null;
var day_counter = null;
var date = null;
var trg_Delete = [];
var arr_index = 0;
var row_count = null;

$(document).ready(function () {
    c_year = $('#year').val();

    //To rename months
    row_count = $('.mon_c').length - 1;

    for (var indexer = 0; indexer <= row_count; indexer++) {
        var this_month = $('.mon_c').eq(indexer).find('#month').val();
        $('.mon_c h1').eq(indexer).text(months[this_month - 1]);
        $('.mon_c table').eq(indexer).attr('id', 'month' + this_month);
        $('.mon_c #add_date').eq(indexer).attr('onclick', 'return AddDate(' + this_month + ', this)');
    }

    $('#addMonth').click(function () {
        month_counter = eval($('.mon_c').eq(row_count).find('#month').val()) + 1;
        pBindKey = $('.mon_c').length;

        var new_month = '<tr class="mon_c">' +
                            '<td>' +
                                '<div>' +
                                    '<h1>' + months[month_counter - 1] + '</h1>' +
                                    '<table id="month' + month_counter + '">' +
                                        '<tr>' +
                                            '<th class="w_50">Date</th>' +
                                            '<th class="w_450">Plan</th>' +
                                            '<th class="w_50"> </th>' +
                                        '</tr>' +
                                        '<input type="hidden" name="record_key_' + pBindKey + '_record_keyplanner_table" value="inner_record_key">' +
                                            '<tr>' +
                                                '<td>' +
                                                    '<input type="text" id="day" name="' + recKey + pBindKey + strKey + 0 + '_day" class="record_counter" size=5/>' +
                                                    '<input type="hidden" id="year" name="' + recKey + pBindKey + strKey + 0 + '_year" value="' + c_year + '"/>' +
                                                    '<input type="hidden" id="month" name="' + recKey + pBindKey + strKey + 0 + '_month" value="' + month_counter + '"/>' +
                                                '</td>' +
                                                '<td>' +
                                                    '<textarea id="plan" name="' + recKey + pBindKey + strKey + 0 + '_plan" cols="60" rows="5"/></textarea>' +
                                                '</td>' +
                                                '<td><input type="button" id="delete_btn" value="Delete Plan" class="btn01" onclick="return DeletePlan(this)" /></td>' +
                                            '</tr>' +
                                    '</table>' +
                                    '<br /><p><input id="add_date" type="button" value="Add Date" name="add_date" class="btn01" onclick="return AddDate(' + month_counter + ', this)"/></p><br />' +
                                '</div>' +
                            '</td>' +
                        '</tr>';

        if (month_counter <= 12) {
            $('#planner_tbl').append(new_month);
            row_count++;
        }
    });
});

function AddDate(mon, trg_btn) {
    bindKey = $('#month' + mon + ' tr').length - 1;
    pBindKey = $(trg_btn).closest('tr').index();
    input_date = mon + '/' + (bindKey + 1) + '/' + c_year;
    date = new Date(input_date);

    var new_day = '<tr>' +
                    '<td>' +
                        '<input type="text" id="day" name="' + recKey + pBindKey + strKey + bindKey + '_day" class="record_counter" size="5" />' +
                        '<input type="hidden" id="year" name="' + recKey + pBindKey + strKey + bindKey + '_year" value="' + c_year + '" />' +
                        '<input type="hidden" id="month" name="' + recKey + pBindKey + strKey + bindKey + '_month" value="' + mon + '"/>' +
                    '</td>' +
                    '<td>' +
                        '<textarea id="plan" name="' + recKey + pBindKey + strKey + bindKey + '_plan" cols="60" rows="5"/></textarea>' +
                    '</td>' +
                    '<td><input type="button" id="delete_btn" value="Delete Plan" class="btn01" onclick="return DeletePlan(this)" /></td>' +
                  '</tr>';

    if (input_date === date.toLocaleDateString()) {
        $('#month' + mon + '').append(new_day);
    }
}

function DeletePlan(trg_btn) {
    var day = $(trg_btn).closest('tr').find('#day').val();
    var month = $(trg_btn).closest('tr').find('#month').val();
    var year = $(trg_btn).closest('tr').find('#year').val();
    var trg_date = month + '/' + day + '/' + year;
    date = new Date(trg_date);
    date = date.toLocaleDateString(date);

    if (date != null && date == trg_date) {
        trg_Delete[arr_index] = date;
        arr_index++;
        $('#deleted').val(trg_Delete);
    }

    if ($(trg_btn).closest('table').find('tr').length > 2) {
        $(trg_btn).closest('tr').remove();
        bindKey--;
    }
    else {
        $(trg_btn).closest('tr').find('#day').val('');
        $(trg_btn).closest('tr').find('#plan').val('');
    }
}