﻿// Display employee details on mouse hover
$(document).ready(function () {
    var element = document.getElementById('message-body');
    element.scrollTop = element.scrollHeight - element.clientHeight;
    $('.delete').hide();
    $('#cancel-btn').hide();
    $('.confirm-delete-btn').hide();

    $('#delete-btn').click(function () {
        $('#delete-btn').hide();
        $('.delete').show();
        $('.confirm-delete-btn').show("slow");

    });

    $('#confirm-delete-btn').click(function () {
        var thread_no = [];
        $('.delete-check-box').each(function () {
            if ($(this).prop('checked') === true) {
                thread_no.push(this.id);
            }
        });

        $("input[name='delete_thread_array']").val(thread_no);
    });

    $('#send-btn').click(function () {
        $('input[name="send"]').click();
    });


    $('.delete-all-chkbox').change(function () {
        var checked = $('.delete-all-chkbox').prop("checked")

        if (checked === true) {
            $('.delete-check-box').prop('checked', true);
        }
        else {
            $('.delete-check-box').prop("checked", false);
        }

    });

    $('.delete-check-box').change(function () {
        var checked = $(this).prop("checked")

        if (checked === false) {
            $('.delete-all-chkbox').prop('checked', false);
        }

    });

    $('#cancel-btn').click(function () {
        $('.delete-check-box').removeAttr('checked');
        $('.delete').hide();
        $('.confirm-delete-btn').hide("slow");
        $('#delete-btn').show("slow");
    });



    $('#recipient_emp_no').change(function () {
        var emp_no = this.value;
        $.ajax({
            url: "<%=.sec_url%>top/api/asp/getImageMember.asp",
            data: {
                emp_no: emp_no
            },
            success: function (data) {
                var img_src = "<%=.sec_url%>images/" + emp_no + "/" + emp_no + ".jpg";
                if (data.hasImage === true) {

                    $('#member_img').attr("src", img_src);
                }
            }
        });
        $('#emp_no_label').text("Emp no: " + emp_no)
    });

});

function submitThread(i) {
    $
    $("#thread-form" + i).submit();
}