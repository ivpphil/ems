﻿function closeChild() {
    window.close();
}

(function () {
    var app = angular.module('requestApp', []);

    app.controller('UTController', function () {
        this.getUTHours = function () {
            if (angular.hasValue(this.start_hours) && angular.hasValue(this.start_minutes) && angular.hasValue(this.start_period) &&
                angular.hasValue(this.end_hours) && angular.hasValue(this.end_minutes) && angular.hasValue(this.end_period))
            {
                this.ut_duration = getDuration(this.start_hours, this.start_minutes, this.start_period, this.end_hours, this.end_minutes, this.end_period);
            }
        };
    });

    app.controller('LeaveController', function () {
        this.now_vl = now_vl.value;
        this.now_sl = now_sl.value;
        this.total_vl = total_vl.value;
        this.total_sl = total_sl.value;
        this.used_leave = used_leave.value;
        this.date_start = date_start.value;
        this.date_end = date_end.value;
        this.leave_type = getRBVal();
        this.half_day_start = half_day_start.value;
        this.half_day_end = half_day_end.value

        this.temp_file_name =  temp_file_name.value;

        if (!angular.hasValue(date_start) || !angular.hasValue(date_end) ||
                date_start.value === date_end.value) {
            this.cbAnimate = true;
        }
        else {
            this.cbAnimate = false;
        }

        this.clickFile = function ()
        {
            switch (this.leave_type)
            {
                case "2":
                    angular.element("#sick_file_proof").click();
                    break;
                case "4":
                    angular.element("#paternal_file_proof").click();
                    break;
            }
        }

        this.fileNameChanged = function (e) {
            alert("test!");
        }

        this.ComputeUsedLeave = function () {
            //Determine whether hide or show half_day_end check box
            if (!angular.hasValue(date_start) || !angular.hasValue(date_end) ||
                date_start.value === date_end.value) {
                this.cbAnimate = true;
            }
            else {
                this.cbAnimate = false;
            }

            if (angular.hasValue(this.leave_type) && angular.hasValue(date_start) && angular.hasValue(date_end)) {
                this.now_vl = 0;
                this.now_sl = 0;
                this.total_vl = last_vl.value;
                this.total_sl = last_sl.value;
                this.used_leave = 0;
                var half_day = 0;
                var d_start = new Date(date_start.value);
                var d_end = new Date(date_end.value);

                if (angular.hasValue(half_day_start) && half_day_start.checked === true) {
                    half_day = 0.5;
                }
                if (angular.hasValue(half_day_end) && half_day_end.checked === true) {
                    half_day = half_day + 0.5;
                }

                var weekends = getWeekends(d_start, d_end);
                var computed_days = (Math.abs(d_start - d_end) / 86400000) - weekends + 1 - Math.abs(half_day)
                if (d_start <= d_end && !isNaN(computed_days)) {
                    this.used_leave = computed_days;

                    if (this.leave_type === "1" || this.leave_type === "9") {
                        this.now_vl = this.used_leave;
                        this.total_vl = last_vl.value - this.now_vl;
                    }

                    if (this.leave_type === "2") {
                        this.now_sl = this.used_leave;
                        this.total_sl = last_sl.value - this.now_sl;
                    }
                }
                else {
                    this.used_leave = "Invalid duration. Please check Start Date and End Date."
                    return;
                }
            }
        };
      

    });


    //Get the count of the weekends from the selected period of leave request.
    function getWeekends(from_date, to_date) {
        var day_count = 0;
        var start = new Date(from_date);
        var end = new Date(to_date);

        while (start <= end) {
            if (start.getDay() === 0 || start.getDay() === 6) {
                day_count++;
                start.setDate(start.getDate() + 1);
            }
            else {
                start.setDate(start.getDate() + 1);
            }
        }

        return day_count;
    };
    
    function getDuration(start_hrs, start_mins, start_cp,
        end_hrs, end_mins, end_cp) {

        var s_period = getPeriod(start_cp);
        var e_period = getPeriod(end_cp);

        if (s_period === null || e_period === null) {
            this.ut_duration = "Invalid Clock Period";
            return;
        }

        var stime = start_hrs + ":" + start_mins + " " + s_period;
        var etime = end_hrs + ":" + end_mins + " " + e_period;
        var start = new Date("01/01/2007 " + stime);
        var end = new Date("01/01/2007 " + etime);
        var tot_hours = Math.abs(start - end) / 36e5

        if (start > end || isNaN(tot_hours)) {
            return "Invalid time";
        }

        return tot_hours;
    };

    function getRBVal() {
        var val = null;
        $('.rb_leave_type').each(function () {
            if ($(this).attr("checked")) {
                $(this).prop("checked", true);
                val = $(this).val();
            }
        });
        return val;
    }

    angular.hasValue = function (val) {
        var x = angular.isDefined(val) && val !== null && val !== "" && val.value !== "";
        return x;
    }

    var getPeriod = function (val) {
        if (!isNaN(val)) {
            if (val === "1") {
                return "AM";
            }
            else if (val === "2") {
                return "PM";
            }
            else {
                null;
            }
        }
        return val;
    }

    app.directive('convertNumber', function () {
        return {
            require: 'ngModel',
            link: function (scope, el, attr, ctrl) {
                ctrl.$parsers.push(function (value) {
                    return parseInt(value, 10);
                });

                ctrl.$formatters.push(function (value) {
                    if (angular.hasValue(value)) {
                        if (value < 10) {
                            return '0' + value.toString();
                        }
                        else {
                            return value.toString();
                        }
                    }
                });
            }
        }
    });

    app.directive('toNumber', function () {
        return {
            require: 'ngModel',
            link: function (scope, el, attr, ctrl) {
                ctrl.$formatters.push(function (value) {
                    if (angular.hasValue(value)) {
                        if (value === 'AM') {
                            return '1';
                        }
                        else if(value === 'PM') {
                            return '2';
                        }
                        else {
                            null;
                        }
                    }
                });
            }
        }
    });
})();
