﻿var ot_hours_list;
var i;
for (i = 1; i <= 12;) {
    if (i < 10) {
        ot_hours_list += "<option value='0" + i + "'>0" + i + "</option>";
    }
    else {
        ot_hours_list += "<option value='" + i + "'>" + i + "</option>";
    }
    i = i + 1;
}

var ot_mins_list;
for (i = 0; i <= 45;) {
    if (i < 10) {
        ot_mins_list += "<option value='0" + i + "'>0" + i + "</option>";
    }
    else {
        ot_mins_list += "<option value='" + i + "'>" + i + "</option>"
    }
    i = i + 15;
}

var ot_period_list;
for (i = 1; i <= 2;) {
    if (i == 1) {
        ot_period_list += "<option value='" + i + "'>AM</option>";
    }
    else {
        ot_period_list += "<option value='" + i + "'>PM</option>"
    }
    i = i + 1;
}


$(document).ready(function () {

    $("#addField").click(function () {
        var table = document.getElementById("attendance_tbody");

        var trCount = $('#attendance_table tbody tr').length;

        if (trCount < 15) {

            AppendNewRow(trCount);
            trCount++;
        }
    });
});

function DeleteRow(deleteBtn) {
    var trCount = $("#attendance_table tbody tr").length;

    if (trCount > 1) {
        $(deleteBtn).closest('tr').remove();
        trCount--;
    }
}

function AppendNewRow(trCount) {
    var rec_key = "record_key_" + trCount + "_";

    var newRow ="<tr id='rowToClone'>" +
            "<td>"+
            "<input type='date' id='" + rec_key + "date_start' name='" + rec_key + "date_start' class='check-ot date_start form-control w_160'>" +
            "</td>" +
            "<td>" +
                "<label id='" + rec_key + "shift_sched'></label>" +
                "<input type='hidden' id='" + rec_key + "shift_schedule' name='" + rec_key + "shift_schedule' value=''/>" +
            "</td>" +
            "<td>" +
                "<div class='w_230'>" +
                    "<div class='download_button'>" +
                        "<select id='" + rec_key + "start_hours' class='check-ot form-control w_65' name='" + rec_key + "start_hours'>" +
                            "<option value=''></option>" +
                          ot_hours_list +
                        "</select>" +
                    "</div>" +
                    "<strong> : </strong>" +
                    "<div class='download_button'>" +
                        "<select id='" + rec_key + "start_minutes' class='check-ot form-control w_65' name='" + rec_key + "start_minutes'>" +
                            "<option value=''></option>" +
                            ot_mins_list +
                        "</select>" +
                    "</div> &nbsp;" +
                    "<div class='download_button'>" +
                        "<select id='" + rec_key + "start_period' name='" + rec_key + "start_period' class='check-ot form-control w_68'> " +
                            "<option value=''></option>" +
                            ot_period_list +
                        "</select>" +
                    "</div>" +
            "</td>" +
            "<td>" +
                "<div class='w_230'>" +
                    "<div class='download_button'>" +
                        "<select id='" + rec_key + "end_hour' class='check-ot form-control w_65' name='" + rec_key + "end_hours'>" +
                            "<option value=''></option>" +
                            ot_hours_list +
                        "</select>" +
                    "</div>" +
                    "<strong> : </strong>" +
                    "<div class='download_button'>" +
                        "<select id='" + rec_key + "end_minutes' class='check-ot form-control w_65' name='" + rec_key + "end_minutes'> " +
                            "<option value=''></option>" +
                            ot_mins_list +
                        "</select>" +
                    "</div> &nbsp;" +
                    "<div class='download_button'>" +
                        "<select id='" + rec_key + "end_period' name='" + rec_key + "end_period' class='check-ot form-control w_68'>" +
                            "<option value=''></option>" +
                            ot_period_list +
                        "</select>" +
                    "</div>" +
                "</div>" +
            "</td>" +
            "<td>" +
                "<input type='text' id='" + rec_key + "reason' name='" + rec_key + "reason' class='check-ot form-control' />" +
            "</td> " +
            "<td><input type='button' class='btn btnl-info btn-sm' value='Delete' name='deleteBtn' onclick='return DeleteRow(this)' /></td>" +
        "</tr>"

    $("#attendance_table tbody").append(newRow);
}
