﻿$(document).ready(function () {
    var login = Cookies.get('islogin');
    var position = Cookies.get('position');
    $('#emp_pos').val(position);
    if (login != undefined) {
        $('.home').show();
        $('.login').css('display', 'none');
    }
    else {
        $('.login').show();
        $('.home').css('display', 'none');
    }
});

   