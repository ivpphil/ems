﻿var arr_index = 0;
var deleted_id = "";
var taskCounter;

var trCount = $('#report_table tr').length;

var um_hours_list;
var i;
for (i = 0.25; i <= 24;) {
    um_hours_list += "<option value='" + i + "'>" + i + "</option>";
    i = i + 0.25;
}

var progress;
var a;
for (a = 10; a <= 100;) {
    progress += "<option value='" + a + "'>" + a + "%</option>";
    a = a + 10;
}

var newRow = "</br>" +
                "<tr>" +
                   "<th class='col-sm-2 form-group'>Task</th>" +
                    "<td>" +
                       "<div class='row'>" +
                           "<div class='col-sm-4 form-group'>" +
                              "<label> PCODE </label>" +
                                     "<input type='text' class='form-control' name='pcode' value='' onfocusout = 'OnclickOut(this,0)'  id='pcode'/>" +
                                     "<input type='button' value='Select' id='btnSetting' onclick='return pcode_select(this)' class='f_button info' />" +
                            "</div>" +
                            "<div class='col-sm-4 form-group'>" +
                              "<label> Description </label>" +
                                    "<input type='text' class='form-control' name='project_desc' value='' onfocusout = 'OnclickOut(this,1)' id='proj_desc'/>" +
                            " </div>" +
                        "</div>" +
                         "<div class='row'>" +
                            "<div class='col-sm-4 form-group'>" +
                                "<label> Reference Number </label>" +
                                    "<input type='text' class='form-control' name='ref_no' onfocusout = 'OnclickOut(this,2)'/>" +
                              " </div>" +
                           " </div>" +

                           "<div class='row'>" +
                             "<div class='col-sm-4 form-group'>" +
                                 "<label> Used Man Hours</label>" +
                                    "<select name='um_hours' id='sel1' class='form-control' onfocusout='OnclickOut(this,8)'>" +
                                            "<option value=''></option>" +
                                            um_hours_list +
                                    "</select> " +
                             " </div>" +
                            "</div>" +

                           "<div class='row'>" +
                             "<div class='col-sm-2 form-group'>" +
                                 "<label> Progress</label>" +
                                    " <select name='progress' id='sel1' class='form-control' onfocusout = 'OnclickOut(this,3)'>" +
                                                 "<option value=''></option>" +
                                                 progress +
                                             "</select>" +
                              " </div>" +
                           " </div>" +

                           "<div class='row'>" +
                             "<div class='col-sm-3 form-group'>" +
                                 "<label> Status</label>" +
                                         " <select name='status' id='sel1' class='form-control' onfocusout = 'OnclickOut(this,4)'>" +
                                                 "<option value=''></option>" +
                                                 "<option value='1'>Completed Task</option>" +
                                                 "<option value='2'>On Going Task</option>" +
                                                 "<option value='3'>On Going Task with Problem</option>" +
                                             "</select>" +
                                " </div>" +
                              " </div>" +

                           "<div class='form-group'>" +
                                 "<label> Summary</label>" +
                                         " <textarea name='summary' id='sel1' rows='3' class='form-control' onfocusout = 'OnclickOut(this,5)'>" +
                                           "</textarea>" +
                              " </div>" +

                           "<div class='row'>" +
                            "<div class='col-sm-4 form-group'>" +
                                "<label> Start date </label>" +
                                    "<input type='date' class='form-control' name='start_date' onfocusout = 'OnclickOut(this,6)' />" +
                              " </div>" +

                              "<div class='col-sm-4 form-group'>" +
                                "<label> Due date </label>" +
                                    "<input type='date' class='form-control' name='due_date' onfocusout = 'OnclickOut(this,7)' />" +
                              " </div>" +
                           " </div>" +
                           " <input type='button' class='btn btn-info btn-sm' value='Delete' name='deleteBtn' id='deleteBtn' onclick='DeleteRow(this)' />&nbsp;" +
                           "</td>" +

                           "</tr> </br> </br>";






var pcode_id
function pcode_select(btn) {


    var row_index = $(btn).closest("tr").index() - 1;
    window.row_index = row_index;
    $(btn).closest("tr").find('#pcode').attr("name", "record_key_" + row_index + "_pcode");
    $(btn).closest("tr").find('#proj_desc').attr("name", "record_key_" + row_index + "_proj_desc");

    $(btn).closest("tr").find('#pcode').attr("id", "pcode" + row_index);
    $(btn).closest("tr").find('#proj_desc').attr("id", "proj_desc" + row_index);
    window.pcode_id_field = "pcode" + row_index;
    window.proj_desc_field = "proj_desc" + row_index;




    result = window.open("/top/project/asp/dreport_pcode_list.asp", '_blank', "status=yes,width=1024px,height=768px,resizable=yes,scrollbars=yes");

    result.focus();
    document.onmousedown = disable_parent;
    document.onkeyup = disable_parent;


    result[result.addEventListener ? 'addEventListener' : 'attachEvent']


    ((result.attachEvent ? 'on' : '') + 'load', function () {

        result.RunCallbackFunction = callback_func;
        result.reco_number = reco_number;

    }, false);


}

function focusPopup() {
    if (result && result.closed) {
        result.focus();
    }
}

function disable_parent() {
    if (result && !result.closed) {
        result.focus();
    }
}


function callback_func(reco_number, pcode, pcode_desc, proj_desc_field, row_index) {
    if (pcode != undefined) {
        $("#" + reco_number).val(pcode);
        $("#" + proj_desc_field).val(pcode_desc);


    }
}



$(document).ready(function () {



    if (trCount > 9) {

        $('#report_table body tr:nth-child(12)').remove();


    }

    $("#addField").click(function () {

        var trCount = $("#report_table tr").length - 1;


        if (trCount < 10) {
            $("#report_table tbody").append(newRow);
            trCount++;
        }
    });
});

function DeleteRow(deleteBtn) {

    $(deleteBtn).closest('tr').remove();
    trCount--;

    if ($('#id').val() != 0) {
        if ($(deleteBtn).closest('tr').find('#id').val() != null && $(deleteBtn).closest('tr').find('#id').val().length != 0) {
            var result = $(deleteBtn).closest('tr').find('#id').val();

            if (deleted_id == "") {
                deleted_id = result;
            }
            else {
                deleted_id = deleted_id + "_" + result;
            }
            arr_index++;

            $('#deleted_id').val(deleted_id);
        }
    }
}


function OnclickOut(inputField, column) {

    var inputValue = $(inputField).val();
    var inputName = $(inputField).attr("name");

    var row_index = $(inputField).closest("tr").index() - 1;
    var record_key = "record_key_"
    var new_name = "";


    if (inputValue.length > 0) {

        var matchStringIndex = inputName.search("record_key_");

        if (matchStringIndex == -1) {

            new_name = record_key + row_index + "_" + inputName;
            $(inputField).attr("name", new_name);


        }
    }

    else {


        var defaultFieldname = "";

        switch (column) {


            case 0:
                defaultFieldname = "pcode";
                break;

            case 1:
                defaultFieldname = "project_desc";
                break;
            case 2:
                defaultFieldname = "ref_no";
                break;
            case 3:
                defaultFieldname = "progress";
                break;

            case 4:
                defaultFieldname = "status";
                break;

            case 5:
                defaultFieldname = "summary";
                break;

            case 6:
                defaultFieldname = "start_date";
                break;

            case 7:
                defaultFieldname = "due_date";
                break;
            case 8:
                defaultFieldname = "um_hours";
                break;

        }

        $(inputField).attr("name", defaultFieldname);
    }

}