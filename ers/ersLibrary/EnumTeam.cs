﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers
{
    public enum EnumTeam : short
    {
        Administrative = 1,
        CSharp,
        FrontEnd,
        BusinessAnalyst,
        QualityAssurance,
        SystemAdmin
    }
}
