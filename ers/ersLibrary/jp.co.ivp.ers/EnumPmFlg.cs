﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// 購入経路
    /// </summary>
    public enum EnumPmFlg
        : short
    {
        /// <summary>
        /// 0: PC
        /// </summary>
        PC = 0,

        /// <summary>
        /// 1: MOBILE
        /// </summary>
        MOBILE = 1,

        /// <summary>
        /// 2: CTS
        /// </summary>
        CTS = 2,

        /// <summary>
        /// 3:REGULAR
        /// </summary>
        REGULAR = 3,

        /// <summary>
        /// 4: TEL
        /// </summary>
        TEL = 4,

        /// <summary>
        /// 5: LETTER
        /// </summary>
        LETTER = 5,

        /// <summary>
        /// 6: FAX
        /// </summary>
        FAX = 6,

        /// <summary>
        /// 7: Smartphone
        /// </summary>
        SmartPhone = 7,

        /// <summary>
        /// 10: Mall
        /// </summary>
        Mall = 10,
    }
}
