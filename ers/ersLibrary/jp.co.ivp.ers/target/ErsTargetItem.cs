﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace jp.co.ivp.ers.target
{
    public class ErsTargetItem : ErsRepositoryEntity
    {
        [ErsSchemaValidation("target_item_t.id")]
        public override int? id { get; set; }

        [ErsSchemaValidation("target_item_t.target_id")]
        public virtual int? target_id { get; set; }

        [ErsSchemaValidation("target_item_t.target_kbn")]
        public virtual EnumTargetKbn? target_kbn { get; set; }

        [ErsSchemaValidation("target_item_t.gcode")]
        public virtual string gcode { get; set; }

        [ErsSchemaValidation("target_item_t.scode")]
        public virtual string scode { get; set; }

        [ErsSchemaValidation("target_item_t.order_type")]        
        public virtual EnumOrderType? order_type { get; set; }
        
        [ErsSchemaValidation("target_item_t.intime")]
        public virtual DateTime? intime { get; set; }

        [ErsSchemaValidation("target_item_t.intime")]
        public virtual DateTime? utime { get; set; }

        [ErsSchemaValidation("target_item_t.active")]
        public virtual int? active { get; set; }
    }
}
