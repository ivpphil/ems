﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.Web;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.step_scenario;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.target.strategy
{
    /// <summary>
    /// ステップメールシナリオ対象商品取得 [ObtainStepMailScenarioItemListStgy]
    /// </summary>
    public class ObtainTargetItemListStgy
    {
        /// <summary>
        /// ステップメールシナリオ対象商品リスト取得 [Obtain]
        /// </summary>
        /// <param name="listStepMail">ステップメールリスト [List of StepMail]</param>
        /// <returns>ステップメールシナリオ対象商品リスト（キーはシナリオID） [List of StepMailScenarioItem (Key is Scenario id)]</returns>
        public IList<ErsTargetItem> Obtain(int? target_id)
        {
            var respository = ErsFactory.ersTargetFactory.GetErsTargetItemRepository();
            var criteria = ErsFactory.ersTargetFactory.GetErsTargetItemCriteria();

            criteria.SetActiveOnly();
            criteria.target_id = target_id;

            return respository.Find(criteria);
        }
    }
}
