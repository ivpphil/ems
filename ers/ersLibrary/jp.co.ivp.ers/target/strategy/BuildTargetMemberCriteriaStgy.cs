﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.target.strategy
{
    public class BuildTargetMemberCriteriaStgy
    {
        /// <summary>
        /// レコードカウント取得
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public void Build(ErsTarget target, ErsMemberCriteria criteria, DateTime dateTarget, IList<ErsTargetItem> listTargetItem, string d_no)
        {
            // 伝票条件 [Order condition]
            this.SetOrderCondition(criteria, target, dateTarget);

            var additionalQuery = string.Empty;
            var additionalQueryParameter = new Dictionary<string, object>();
            if (d_no.HasValue())
            {
                additionalQuery = "AND d_master_t.d_no <> :d_no";
                additionalQueryParameter.Add("d_no", d_no);
            }

            // 伝票商品条件 [Order item condition]
            this.SetOrderItemCondition(criteria, target, listTargetItem, additionalQuery, additionalQueryParameter);
        }

        #region 伝票条件セット [Set condition of order]
        /// <summary>
        /// 伝票条件セット [Set condition of order]
        /// </summary>
        /// <param name="criteria">クライテリア（会員） [ErsMemberCriteria]</param>
        /// <param name="objTarget">ステップメールシナリオ [StepMailScenario]</param>
        /// <param name="dateTarget">実行対象日時 [DateTime for Execute]</param>
        protected void SetOrderCondition(ErsMemberCriteria criteria, ErsTarget objTarget, DateTime dateTarget)
        {
            if (objTarget.site_id == null || objTarget.site_id.Length == 0)
            {
                criteria.site_id = null;
                return;
            }

            var saledate_to = this.GetTargetSaleDate(objTarget.recency_to.Value, dateTarget.AddDays(-objTarget.recency_to.Value).Date);
            var saledate_from = this.GetTargetSaleDate(objTarget.recency_from.Value, dateTarget.AddDays(-objTarget.recency_from.Value).Date.AddDays(1));

            // 最終購入日時 [last_sale_date]
            criteria.SetBetweenLastSaleDate(saledate_to, saledate_from, objTarget.site_id);

            // 購入回数 [sale_num]
            criteria.SetBetweenSaleTimes(objTarget.frequency_from.Value, objTarget.frequency_to.Value, objTarget.site_id);

            // 購入金額 [sale]
            criteria.SetBetweenSale(objTarget.monetary_from.Value, objTarget.monetary_to.Value, objTarget.site_id);
        }

        protected DateTime? GetTargetSaleDate(int? recency_value, DateTime? returnValue)
        {
            if (!recency_value.HasValue || recency_value == 0 || recency_value == ushort.MaxValue)
            {
                return null;
            }

            return returnValue;
        }

        #endregion

        #region 伝票商品条件セット [Set condition of order item]
        /// <summary>
        /// 伝票商品条件セット [Set condition of order item]
        /// </summary>
        /// <param name="criteria">クライテリア（会員） [ErsMemberCriteria]</param>
        /// <param name="objStepScenario">ステップメールシナリオ [StepMailScenario]</param>
        /// <param name="dicTargetItem">ステップメールシナリオ対象商品リスト [List of StepMailScenarioItem]</param>
        protected void SetOrderItemCondition(ErsMemberCriteria criteria, ErsTarget objTarget, IList<ErsTargetItem> listTargetItem, string additionalQuery, Dictionary<string, object> additionalQueryParameter)
        {
            switch (objTarget.order_ptn_kbn)
            {
                // 通常商品 [Usually]
                case EnumOrderPattern.Usually:
                    criteria.SetTargetExistsUsually(additionalQuery, additionalQueryParameter);
                    break;

                // 定期商品 [Subscription]
                case EnumOrderPattern.Subscription:
                    criteria.SetTargetExistsSubscription(additionalQuery, additionalQueryParameter);
                    break;

                // 対象商品 [Purchase Item]
                case EnumOrderPattern.PurchaseItem:
                    this.SetOrderPurchaseItemCondition(criteria, objTarget, listTargetItem, additionalQuery, additionalQueryParameter);
                    break;
            }

        }

        /// <summary>
        /// 伝票対象商品条件セット [Set condition of order purchase item]
        /// </summary>
        /// <param name="criteria">クライテリア（会員） [ErsMemberCriteria]</param>
        /// <param name="objStepScenario">ステップメールシナリオ [StepMailScenario]</param>
        /// <param name="dicTargetItem">ステップメールシナリオ対象商品リスト [List of StepMailScenarioItem]</param>
        protected void SetOrderPurchaseItemCondition(ErsMemberCriteria criteria, ErsTarget objTarget, IList<ErsTargetItem> listTargetItem, string additionalQuery, Dictionary<string, object> additionalQueryParameter)
        {
            // 対象商品条件セット [Set condition of Including item]
            this.SetOrderPurchaseIncludeItemCondition(criteria, listTargetItem, additionalQuery, additionalQueryParameter);

            // 除外商品条件セット [Set condition of Excluding item]
            this.SetOrderPurchaseExcludeItemCondition(criteria, listTargetItem, additionalQuery, additionalQueryParameter);
        }

        /// <summary>
        /// 伝票対象商品条件セット（対象商品） [Set condition of order purchase item (Including item)]
        /// </summary>
        /// <param name="criteria">クライテリア（会員） [ErsMemberCriteria]</param>
        /// <param name="listTargetItem">ステップメールシナリオ対象商品リスト [List of StepMailScenarioItem]</param>
        public void SetOrderPurchaseIncludeItemCondition(ErsMemberCriteria criteria, IList<ErsTargetItem> listTargetItem, string additionalQuery, Dictionary<string, object> additionalQueryParameter)
        {
            List<Dictionary<string, object>> listItem = new List<Dictionary<string, object>>();

            // 対象商品がない場合は、必ず対象が0になります
            if (listTargetItem.Count == 0)
            {
                criteria.mcode = null;
            }

            foreach (var item in listTargetItem)
            {
                // 対象商品 [Item]
                if (item.target_kbn == EnumTargetKbn.Item)
                {
                    int[] order_type;
                    if (item.order_type == EnumOrderType.BothUsuallyAndSubscription)
                    {
                        order_type = new int[] { (int)EnumOrderType.Usually, (int)EnumOrderType.Subscription };
                    }
                    else
                    {
                        order_type = new int[] { (int)item.order_type };
                    }
                    listItem.Add(new Dictionary<string, object>() { { "scode", item.scode }, { "order_type", order_type } });
                }
            }
            
            // 対象商品あり [Including items]
            if (listItem.Count > 0)
            {
                criteria.SetTargetExistsSpecifiedItem(listItem, additionalQuery, additionalQueryParameter);
            }
        }

        /// <summary>
        /// 伝票対象商品条件セット（除外商品） [Set condition of order purchase item (Excluding item)]
        /// </summary>
        /// <param name="criteria">クライテリア（会員） [ErsMemberCriteria]</param>
        /// <param name="listTargetItem">ステップメールシナリオ対象商品リスト [List of StepMailScenarioItem]</param>
        protected void SetOrderPurchaseExcludeItemCondition(ErsMemberCriteria criteria, IList<ErsTargetItem> listTargetItem, string additionalQuery, Dictionary<string, object> additionalQueryParameter)
        {
            List<Dictionary<string, object>> listItem = new List<Dictionary<string, object>>();

            foreach (var item in listTargetItem)
            {
                // 除外商品 [ExcludingItem]
                if (item.target_kbn == EnumTargetKbn.ExcludingItem)
                {
                    int[] order_type;
                    if (item.order_type == EnumOrderType.BothUsuallyAndSubscription)
                    {
                        order_type = new int[] { (int)EnumOrderType.Usually, (int)EnumOrderType.Subscription };
                    }
                    else
                    {
                        order_type = new int[] { (int)item.order_type };
                    }
                    listItem.Add(new Dictionary<string, object>() { { "scode", item.scode }, { "order_type", order_type } });
                }
            }

            // 除外商品あり [Excluding items]
            if (listItem.Count > 0)
            {
                criteria.SetTargetExistsSpecifiedItem(listItem, additionalQuery, additionalQueryParameter, false);
            }
        }
        #endregion
    }
}
