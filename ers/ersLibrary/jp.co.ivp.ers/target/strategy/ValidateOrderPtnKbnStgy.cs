﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.target.strategy
{
    public class ValidateOrderPtnKbnStgy
    {
        /// <summary>
        ///  対象商品指定の基準日が設定されているステップメールがある場合は、対象商品指定以外を選択できない。
        /// </summary>
        /// <param name="target_id"></param>
        /// <param name="order_ptn_kbn"></param>
        /// <param name="fieldKeys"></param>
        /// <returns></returns>
        public ValidationResult ValidateTarget(int? target_id, EnumOrderPattern? order_ptn_kbn, string fieldKeys)
        {
            if (order_ptn_kbn == EnumOrderPattern.PurchaseItem)
            {
                // 対象商品指定の場合はチェックしない
                return null;
            }

            var repository = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioRepository();
            var criteria = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioCriteria();
            criteria.target_id = target_id;
            criteria.mail_ref_date_kbn_in = new[] { EnumReferenceDate.LastItemPurchasedDate, EnumReferenceDate.LastItemShippedDate };
            criteria.active = EnumActive.Active;
            if (repository.GetRecordCount(criteria) > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("65006", ErsResources.GetFieldName(fieldKeys)), new[] { fieldKeys });
            }

            return null;
        }

        /// <summary>
        /// 対象商品指定の基準日が設定されている場合、対象商品指定以外が設定されたターゲットを選択できない。
        /// </summary>
        /// <param name="target_id"></param>
        /// <param name="mail_ref_date_kbn"></param>
        /// <param name="fieldKeys"></param>
        /// <returns></returns>
        public ValidationResult ValidateScenario(int? target_id, EnumReferenceDate? mail_ref_date_kbn, string fieldKeys)
        {
            if (mail_ref_date_kbn != EnumReferenceDate.LastItemPurchasedDate
                && mail_ref_date_kbn != EnumReferenceDate.LastItemShippedDate)
            {
                // 対象商品指定の場合はチェックしない
                return null;
            }

            var repository = ErsFactory.ersTargetFactory.GetErsTargetRepository();
            var criteria = ErsFactory.ersTargetFactory.GetErsTargetCriteria();
            criteria.id = target_id;
            criteria.order_ptn_kbn = EnumOrderPattern.PurchaseItem;
            if (repository.GetRecordCount(criteria) == 0)
            {
                return new ValidationResult(ErsResources.GetMessage("65007", ErsResources.GetFieldName(fieldKeys)), new[] { fieldKeys });
            }

            return null;
        }
    }
}
