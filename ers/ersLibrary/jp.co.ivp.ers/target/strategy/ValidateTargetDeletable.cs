﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.target.strategy
{
    public class ValidateTargetDeletable
    {
        public IEnumerable<ValidationResult> Validate(int? target_id, string target_idKey)
        {
            if (!this.CheckStepScenario(target_id))
            {
                yield return new ValidationResult(ErsResources.GetMessage("65008", ErsResources.GetFieldName("function_name_stepmail")), new[] { target_idKey });
            }

            if (!this.CheckDocBundling(target_id))
            {
                yield return new ValidationResult(ErsResources.GetMessage("65008", ErsResources.GetFieldName("function_name_doc_bundling")), new[] { target_idKey });
            }
        }

        private bool CheckStepScenario(int? target_id)
        {
            var repository = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioRepository();
            var criteria = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioCriteria();
            criteria.target_id = target_id;
            return repository.GetRecordCount(criteria) == 0;
        }

        private bool CheckDocBundling(int? target_id)
        {
            var repository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();
            var criteria = ErsFactory.ersDocBundleFactory.GetErsCampaignCriteria();
            criteria.target_id = target_id;
            return repository.GetRecordCount(criteria) == 0;
        }
    }
}