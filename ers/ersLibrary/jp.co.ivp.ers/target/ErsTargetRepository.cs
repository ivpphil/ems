﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using System.Data;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.target
{
    public class ErsTargetRepository
        : ErsRepository<ErsTarget>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsTargetRepository()
            : base("target_t")
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsTargetRepository(ErsDatabase objDB)
            : base("target_t", objDB)
        {
        }
    }
}
