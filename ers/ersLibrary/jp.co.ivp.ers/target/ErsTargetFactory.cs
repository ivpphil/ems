﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.target.strategy;



namespace jp.co.ivp.ers.target
{
    /// <summary>
    /// Factory
    /// </summary>
    public class ErsTargetFactory
    {
        protected static ErsTargetRepository _ErsTargetRepository
        {
            get
            {
                return (ErsTargetRepository)ErsCommonContext.GetPooledObject("_ErsTargetRepository");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_ErsTargetRepository", value);
            }
        }

        /// <summary>
        /// Repositoryを取得する
        /// </summary>
        /// <returns></returns>
        public virtual ErsTargetRepository GetErsTargetRepository()
        {
            if (_ErsTargetRepository == null)
                _ErsTargetRepository = new ErsTargetRepository();
            return _ErsTargetRepository;
        }

        /// <summary>
        /// ErsTargetRespository用Criteria
        /// </summary>
        /// <returns></returns>
        public virtual ErsTargetCriteria GetErsTargetCriteria()
        {
            return new ErsTargetCriteria();
        }

        /// <summary>
        /// クラスを取得する
        /// </summary>
        /// <returns></returns>
        public virtual ErsTarget GetErsTarget()
        {
            return new ErsTarget();
        }


        /// <summary>
        /// IDをもとに、クラスを取得する。
        /// </summary>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual ErsTarget GetErsTargetWithId(int? id)
        {
            var repository = GetErsTargetRepository();

            var c = this.GetErsTargetCriteria();
            c.id = id;
            var list = repository.Find(c);
            if (list.Count != 1)
                return null;

            return list[0];
        }

        public virtual ErsTarget GetErsTargetWithParameter(Dictionary<string, object> parameter)
        {
            var Target = this.GetErsTarget();
            Target.OverwriteWithParameter(parameter);
            return Target;
        }

        public virtual ErsTarget GetErsTargetWithModel(ErsModelBase model)
        {
            var Target = this.GetErsTarget();
            Target.OverwriteWithModel(model);
            return Target;
        }

        public virtual BuildTargetMemberCriteriaStgy GetBuildTargetMemberCriteriaStgy()
        {
            return new BuildTargetMemberCriteriaStgy();
        }

        public virtual ErsTargetItemCriteria GetErsTargetItemCriteria()
        {
            return new ErsTargetItemCriteria();
        }

        public virtual ErsTargetItemRepository GetErsTargetItemRepository()
        {
            return new ErsTargetItemRepository();
        }

        public virtual ErsTargetItem GetErsTargetItem()
        {
            return new ErsTargetItem();
        }

        public virtual ErsTargetItem GetErsTargetItemWithModel(ErsModelBase model)
        {
            var news = this.GetErsTargetItem();
            news.OverwriteWithModel(model);
            return news;
        }

        public virtual ErsTargetItem GetErsTargetItemWithParameters(Dictionary<string, object> parameters)
        {
            var news = this.GetErsTargetItem();
            news.OverwriteWithParameter(parameters);
            return news;
        }

        public virtual ErsTargetItem GetErsTargetItemWithId(int? id)
        {
            var repository = this.GetErsTargetItemRepository();
            var criteria = this.GetErsTargetItemCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");

            return list[0];
        }

        /// <summary>
        /// Get the obtain the list of step mail scenario item class
        /// </summary>
        /// <returns>returns instance of ObtainStepMailScenarioItemListStgy</returns>
        public virtual ObtainTargetItemListStgy GetObtainTargetItemListStgy()
        {
            return new ObtainTargetItemListStgy();
        }

        public virtual ValidateOrderPtnKbnStgy GetValidateOrderPtnKbnStgy()
        {
            return new ValidateOrderPtnKbnStgy();
        }

        public virtual ValidateTargetDeletable GetValidateTargetDeletable()
        {
            return new ValidateTargetDeletable();
        }
    }
}
