﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.target
{
    public class ErsTargetItemRepository
        : ErsRepository<ErsTargetItem>
    {

        public ErsTargetItemRepository()
            : base("target_item_t")
        {
        }

        public ErsTargetItemRepository(ErsDatabase objDB)
            : base("target_item_t", objDB)
        {
        }

        public override IList<ErsTargetItem> Find(db.Criteria criteria)
        {
            var retList = new List<ErsTargetItem>();

            var list = this.ersDB_table.gSelect(criteria);

            if (list.Count == 0)
                return new List<ErsTargetItem>();

            foreach (var dr in list)
            {
                var stepMail = ErsFactory.ersTargetFactory.GetErsTargetItemWithParameters(dr);
                retList.Add(stepMail);
            }
            return retList;
        }
    }
}
