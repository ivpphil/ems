﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.target
{
    public class ErsTargetCriteria
        : Criteria
    {

        public virtual int? id
        {
            set { this.Add(Criteria.GetCriterion("target_t.id", value, Operation.EQUAL)); }
        }

        public virtual int? not_id
        {
            set { this.Add(Criteria.GetCriterion("target_t.id", value, Operation.NOT_EQUAL)); }
        }

        public virtual DateTime? intime
        {
            set { this.Add(Criteria.GetCriterion("target_t.intime", value, Operation.EQUAL)); }
        }

        public virtual DateTime? utime
        {
            set { this.Add(Criteria.GetCriterion("target_t.utime", value, Operation.EQUAL)); }
        }

        public virtual EnumOrderPattern? order_ptn_kbn
        {
            set { this.Add(Criteria.GetCriterion("target_t.order_ptn_kbn", (int?)value, Operation.EQUAL)); }
        }

        public virtual int? site_id
        {
            set { this.Add(Criteria.GetCriterion("target_t.site_id", value, Operation.EQUAL)); }
        }

        /// <summary>
        /// 登録日範囲（開始）
        /// </summary>
        public virtual DateTime intime_from
        {
            set { this.Add(Criteria.GetCriterion("target_t.intime", value, Operation.GREATER_EQUAL)); }
        }

        /// <summary>
        /// 登録日範囲（終了）
        /// </summary>
        public virtual DateTime intime_to
        {
            set { this.Add(Criteria.GetCriterion("target_t.intime", value, Operation.LESS_EQUAL)); }
        }

        /// <summary>
        /// ターゲット名称
        /// </summary>
        public virtual string target_name_like
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("target_t.target_name", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        /// <summary>
        /// 購買日
        /// </summary>
        public virtual int recency_from
        {
            set { this.Add(Criteria.GetCriterion("target_t.recency_from", value, Operation.GREATER_EQUAL)); }
        }
        public virtual int recency_to
        {
            set { this.Add(Criteria.GetCriterion("target_t.recency_to", value, Operation.LESS_EQUAL)); }
        }

        /// <summary>
        /// 購買日(同梱処理用)
        /// </summary>
        public int? recency_between
        {
            set { this.Add(Criteria.GetBetweenCriterion(ColumnName("target_t.recency_from"), ColumnName("target_t.recency_to"), value)); }
        }

        /// <summary>
        /// 購買日(同梱処理用)
        /// </summary>
        public string recency_between_from_mcode
        {
            set
            { 
                var dicParam = new Dictionary<string, object>();
                dicParam["mcode"] = value;

                var SQL = string.Format(@"(
                        SELECT DATE_PART('day', now() - MAX(d_master_t.intime)) FROM d_master_t 
                        INNER JOIN ds_master_t
                        ON d_master_t.d_no = ds_master_t.d_no
                        AND ds_master_t.order_status NOT IN({0}) 
                        WHERE d_master_t.mcode = :mcode 
                        AND target_t.site_id = d_master_t.site_id 
                    ) BETWEEN target_t.recency_from AND target_t.recency_to 
                    AND target_t.site_id IS NOT NULL ",
                string.Join(",", new int[] { (int)EnumOrderStatusType.CANCELED, (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER }));

                this.Add(Criteria.GetUniversalCriterion(SQL, dicParam));
            }
        }

        /// <summary>
        /// 購買数
        /// </summary>
        public virtual int frequency_from
        {
            set { this.Add(Criteria.GetCriterion("target_t.frequency_from", value, Operation.GREATER_EQUAL)); }
        }
        public virtual int frequency_to
        {
            set { this.Add(Criteria.GetCriterion("target_t.frequency_to", value, Operation.LESS_EQUAL)); }
        }

        /// <summary>
        /// 購買数(同梱処理用)
        /// </summary>
        public int? frequency_between
        {
            set { this.Add(Criteria.GetBetweenCriterion(ColumnName("target_t.frequency_from"), ColumnName("target_t.frequency_to"), value)); }
        }

        /// <summary>
        /// 購買数(同梱処理用)
        /// </summary>
        public string frequency_between_from_mcode
        {
            set
            {
                var dicParam = new Dictionary<string, object>();
                dicParam["mcode"] = value;

                var SQL = string.Format(@"(
                        SELECT COUNT(d_master_t.id) FROM d_master_t 
                        INNER JOIN ds_master_t
                        ON d_master_t.d_no = ds_master_t.d_no
                        AND ds_master_t.order_status NOT IN({0}) 
                        WHERE d_master_t.mcode = :mcode 
                        AND target_t.site_id = d_master_t.site_id
                    ) BETWEEN target_t.frequency_from AND target_t.frequency_to 
                    AND target_t.site_id IS NOT NULL ",
                string.Join(",", new int[] { (int)EnumOrderStatusType.CANCELED, (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER }));

                this.Add(Criteria.GetUniversalCriterion(SQL, dicParam));
            }
        }

        /// <summary>
        /// 購買金額
        /// </summary>
        public virtual int monetary_from
        {
            set { this.Add(Criteria.GetCriterion("target_t.monetary_from", value, Operation.GREATER_EQUAL)); }
        }
        public virtual int monetary_to
        {
            set { this.Add(Criteria.GetCriterion("target_t.monetary_to", value, Operation.LESS_EQUAL)); }
        }

        /// <summary>
        /// 購買金額(同梱処理用)
        /// </summary>
        public int? monetary_between
        {
            set { this.Add(Criteria.GetBetweenCriterion(ColumnName("target_t.monetary_from"), ColumnName("target_t.monetary_to"), value)); }
        }

        /// <summary>
        /// 購買金額(同梱処理用)
        /// </summary>
        public string monetary_between_from_mcode
        {
            set
            {
                var dicParam = new Dictionary<string, object>();
                dicParam["mcode"] = value;

                var SQL = string.Format(@"(
                        SELECT SUM(d_master_t.subtotal) FROM d_master_t 
                        INNER JOIN ds_master_t
                        ON d_master_t.d_no = ds_master_t.d_no
                        AND ds_master_t.order_status NOT IN({0}) 
                        WHERE d_master_t.mcode = :mcode 
                        AND target_t.site_id = d_master_t.site_id
                    ) BETWEEN target_t.monetary_from AND target_t.monetary_to 
                    AND target_t.site_id IS NOT NULL ",
                string.Join(",", new int[] { (int)EnumOrderStatusType.CANCELED, (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER }));

                this.Add(Criteria.GetUniversalCriterion(SQL, dicParam));
            }
        }

        /// <summary>
        ///  Add Order by id
        /// </summary>
        /// <param name="orderBy"></param>
        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("target_t.id", orderBy);
        }
    }
}
