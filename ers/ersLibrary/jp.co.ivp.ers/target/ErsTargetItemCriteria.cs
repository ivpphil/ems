﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.target
{
    public class ErsTargetItemCriteria : Criteria
    {
        public virtual int? id
        {
            set
            {
                Add(Criteria.GetCriterion("target_item_t.id", value, Operation.EQUAL));
            }
        }

        public virtual int? target_id
        {
            set
            {
                Add(Criteria.GetCriterion("target_item_t.target_id", value, Operation.EQUAL));
            }
        }

        public virtual IEnumerable<int> target_id_in
        {
            set
            {
                Add(Criteria.GetInClauseCriterion("target_item_t.target_id", value));
            }
        }

        public virtual EnumTargetKbn? target_kbn 
        { 
            set 
            {
                this.Add(Criteria.GetCriterion("target_item_t.target_kbn", Convert.ToInt32(value), Operation.EQUAL)); 
            } 
        }

        public virtual string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("target_item_t.scode", value, Operation.EQUAL));
            }
        }

        public virtual string gcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("target_item_t.gcode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void SetActiveOnly()
        {
            this.Add(Criteria.GetCriterion("target_item_t.active", (int)EnumActive.Active, Operation.EQUAL));
        }
    }
}
