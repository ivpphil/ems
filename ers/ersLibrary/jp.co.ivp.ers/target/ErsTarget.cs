﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;


namespace jp.co.ivp.ers.target
{
    public class ErsTarget
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public virtual string target_name { get; set; }
        
        public virtual int? recency_from { get; set; }
        
        public virtual int? recency_to { get; set; }

        public virtual int? frequency_from { get; set; }
        
        public virtual int? frequency_to { get; set; }

        public virtual int? monetary_from { get; set; }
        
        public virtual int? monetary_to { get; set; }

        public virtual EnumOrderPattern? order_ptn_kbn { get; set; }
        
        public virtual DateTime intime { get; set; }
        
        public virtual DateTime? utime { get; set; }

        public virtual int[] site_id { get; set; }
    }
}
