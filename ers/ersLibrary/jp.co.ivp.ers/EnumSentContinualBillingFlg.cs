﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Represents state of continual order billing
    /// </summary>
    public enum EnumSentContinualBillingFlg
    {
        /// <summary>
        /// 0 : 対象外[None]
        /// </summary>
        None = 0,

        /// <summary>
        /// 1 : 送信待ち [Waiting]
        /// </summary>
        Waiting = 1,

        /// <summary>
        /// 2 : 送信済み [Sent]
        /// </summary>
        Sent = 2,
    }
}
