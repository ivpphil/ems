﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;

namespace jp.co.ivp.ers.mdb
{

    public class ErsMDBFactory
    {
        
        public virtual ErsMdbCheckInOut GetErsMdbCheckInOut()
        {
            return new ErsMdbCheckInOut();
        }
        public virtual ErsMdbCheckInOutCriteria GetErsMdbCheckInOutCriteria()
        {
            return new ErsMdbCheckInOutCriteria();
        }
        public virtual ErsMdbCheckInOutRepository GetErsMdbCheckInOutRepository()
        {
            return new ErsMdbCheckInOutRepository();
        }
    }
}
