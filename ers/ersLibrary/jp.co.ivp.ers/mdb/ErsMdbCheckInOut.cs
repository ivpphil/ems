﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;


namespace jp.co.ivp.ers.mdb
{
    public class ErsMdbCheckInOut : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public int? USERID { get; set; }
        public DateTime? CHECKTIME { get; set; }
        public string CHECKTYPE { get; set; }
        public int? VERIFYCODE { get; set; }
        public string SENSORID { get; set; }
        public string Memoinfo { get; set; }
        public int? WorkCode { get; set; }
        public string sn { get; set; }
        public int? UserExtFmt { get; set; }
    }
}
