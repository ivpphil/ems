﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.Data.OleDb;
using System.Data;

namespace jp.co.ivp.ers.mdb
{
    public class ErsMdbCheckInOutRepository
    {
        public List<Dictionary<string, object>> Find(ErsMdbCheckInOutCriteria cri)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
           
            string Condition = string.Empty;
            string Group = string.Empty;
            string Order = string.Empty;

            if (cri.ListCondition.Count> 0)
            {
                Condition = " WHERE " + string.Join(" AND ", cri.ListCondition);
            }
            if (cri.ListGroupBy.Count > 0)
            {
                Group = " GROUP BY  " + string.Join(",", cri.ListGroupBy);
            }

            if (cri.ListOrderBy.Count > 0)
            {
                Order = " ORDER BY  " + string.Join(",", cri.ListOrderBy);
            }


            var Myconnection = new OleDbConnection(setup.MdbConnectionStrings);
            DataSet myDataSet = new DataSet();
            DataTable dt = new DataTable();
            OleDbCommand myAccessCommand = new OleDbCommand("SELECT USERINFO.Name, Min(CHECKINOUT.CHECKTIME) AS checkIn, Max(CHECKINOUT.CHECKTIME) AS checkOut "
                                                                 + " FROM USERINFO INNER "
                                                                 + " JOIN CHECKINOUT ON USERINFO.USERID = CHECKINOUT.USERID " + Condition + Group + Order, Myconnection);

            OleDbDataAdapter myDataAdapter = new OleDbDataAdapter(myAccessCommand);

            // Open connection
            Myconnection.Open();
            myDataAdapter.Fill(myDataSet, "INOUT");
            dt = myDataSet.Tables["INOUT"];

            //Get Value
            var ListCheckInOut = new List<Dictionary<string, object>>();
            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> objVal = new Dictionary<string, object>();
                foreach (DataColumn column in dt.Columns)
                {
                    objVal.Add(column.ColumnName, row[column].ToString());
                }
                ListCheckInOut.Add(objVal);
            }

            //return List
            return ListCheckInOut;
        }
    }
}
