﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mdb
{
 
    public class ErsMdbCheckInOutCriteria
    {
        public List<string> ListCondition {get;set;}
        public List<string> ListGroupBy { get; set; }
        public List<string> ListOrderBy { get; set; }

        public ErsMdbCheckInOutCriteria()
        {
            ListCondition = new List<string>();
            ListGroupBy = new List<string>();
            ListOrderBy = new List<string>();
        }

        public virtual int? CHECKINOUT_USERID
        {
            set
            {
                ListCondition.Add(" CHECKINOUT.USERID = " + value);
            }
        }

        public virtual int? Year
        {
            set
            {
                ListCondition.Add(" Year([CHECKTIME]) = " + value);
            }
        }

        public virtual int? Month
        {
            set
            {
                ListCondition.Add(" Month([CHECKTIME]) = " + value);
            }
        }

        public virtual int? Day
        {
            set
            {
                ListCondition.Add(" Day([CHECKTIME])=" + value);
            }
        }

        #region Grouping

        public virtual void GroupByName()
        {
            ListGroupBy.Add(" USERINFO.Name");
        }

        public virtual void GroupByFormatCHECKTIME()
        {
            ListGroupBy.Add(" Format(CHECKTIME,'mm/dd/yyyy') ");
        }

        public virtual void GroupByUSERID()
        {
            ListGroupBy.Add(" CHECKINOUT.USERID");
        }

        public virtual void GroupByYear()
        {
            ListGroupBy.Add(" Year([CHECKTIME])");
        }

        public virtual void GroupByMonth()
        {
            ListGroupBy.Add(" Month([CHECKTIME])");
        }

        public virtual void GroupByDay()
        {
            ListGroupBy.Add(" Day([CHECKTIME])");
        }

        #endregion

        #region sorting
        public virtual void OrderByName(string DescAcs)
        {
            ListOrderBy.Add(" USERINFO.Name" + DescAcs);
        }

        public virtual void OrderByMinCheckTime(string DescAcs)
        {
            ListOrderBy.Add(" Min(CHECKINOUT.CHECKTIME)" + DescAcs);
        }

        public virtual void OrderByCHECKTIMEYear(string DescAcs)
        {
            ListOrderBy.Add(" Year([CHECKTIME])" + DescAcs);
        }

        public virtual void OrderByCHECKTIMEMonth(string DescAcs)
        {
            ListOrderBy.Add(" Month([CHECKTIME])" + DescAcs);
        }

        public virtual void OrderByCHECKTIMEDay(string DescAcs)
        {
            ListOrderBy.Add(" Day([CHECKTIME])" + DescAcs);
        }
        #endregion
    }
}
