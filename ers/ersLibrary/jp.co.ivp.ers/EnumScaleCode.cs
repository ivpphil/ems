﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Represents the Type of Stock
    /// </summary>
    public enum EnumScaleCode
    {
        byday,
        bymonth,
        byhalfhour
    }
}
