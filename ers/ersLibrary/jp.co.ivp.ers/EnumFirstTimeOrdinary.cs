﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumFirstTimeOrdinary
    {
        /// <summary>
        /// 0: 最短
        /// </summary>
        Shortest,
        /// <summary>
        /// 1: 定期と同じ
        /// </summary>
        WithRegular,
        /// <summary>
        /// 2: 日付を指定
        /// </summary>
        Specify
    }
}
