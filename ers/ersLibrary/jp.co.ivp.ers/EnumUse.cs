﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumUse
    {
        /// <summary>
        /// 0: 利用しない
        /// </summary>
        NoUse = 0,

        /// <summary>
        /// 1: 利用する
        /// </summary>
        Use
    }
}
