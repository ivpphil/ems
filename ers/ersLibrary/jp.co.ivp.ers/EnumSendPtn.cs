﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumSendPtn
        : short
    {
        NORMAL = 0,//通常商品
        MONTH_INTERVALS,
        WEEK_INTERVALS,
        DAY_INTERVALS
    }
}
