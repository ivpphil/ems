﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpPageManageCriteria
        : Criteria
    {
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_page_manage_t.id", value, Operation.EQUAL));
            }
        }

        public virtual int active
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_page_manage_t.active", value, Operation.EQUAL));
            }
        }

        public virtual string page_name
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("lp_page_manage_t.page_name", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual string lp_group_name_like
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("lp_page_manage_t.lp_group_name", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual string lp_group_name
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_page_manage_t.lp_group_name", value, Operation.EQUAL));
            }
        }

        public virtual string ccode
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_page_manage_t.ccode", value, Operation.EQUAL));
            }
        }

        public virtual string template_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_page_manage_t.template_id", value, Operation.EQUAL));
            }
        }


        public virtual string page_title
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_page_manage_t.page_title", value, Operation.EQUAL));
            }
        }

        public virtual string logo_image_file
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_page_manage_t.logo_image_file", value, Operation.EQUAL));
            }
        }

        public virtual int? basic_stgy_kbn
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_page_manage_t.basic_stgy_kbn", value, Operation.EQUAL));
            }
        }

        public virtual int? basic_stgy_kbn_not_equals
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_page_manage_t.basic_stgy_kbn", value, Operation.NOT_EQUAL));
            }
        }

        public virtual string JoinWithOrPageNameAndCcode
        {
            set
            {
                this.Add(
                Criteria.JoinWithOR(
                    new[] 
                    {
                        Criteria.GetLikeClauseCriterion("lp_page_manage_t.page_name", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH),
                        Criteria.GetCriterion("lp_page_manage_t.ccode", value, Operation.EQUAL)
                    })
                );
                        
            }
        }

        public string[] ignore_gcode
        {
            set
            {
                var strSQL = "NOT EXISTS (SELECT 1 FROM s_master_t WHERE gcode = ANY(:arrGcode) AND (scode = lp_page_manage_t.sell_scode OR scode = lp_page_manage_t.upsell_scode))";
                var dicParam = new Dictionary<string, object>()
                {
                    {"arrGcode", value}
                };

                this.Add(Criteria.GetUniversalCriterion(strSQL, dicParam));
            }
        }

        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("lp_page_manage_t.id", orderBy);
        }

        internal void SetOrderByMinId(OrderBy orderBy)
        {
            this.AddOrderBy("MIN(lp_page_manage_t.id)", orderBy);
        }

        public virtual void SetOrderByPageName(OrderBy orderBy)
        {
            this.AddOrderBy("lp_page_manage_t.page_name", orderBy);
        }

        public virtual void SetOrderByPublicStartDate(OrderBy orderBy)
        {
            this.AddOrderBy("lp_page_manage_t.public_st_date", orderBy);
        }

        public virtual void SetOrderByPublicEndDate(OrderBy orderBy)
        {
            this.AddOrderBy("lp_page_manage_t.public_ed_date", orderBy);
        }

        public void SetOrderByIntime(OrderBy orderBy)
        {
            this.AddOrderBy("lp_page_manage_t.intime", orderBy);
        }

        public void SetOrderbyRandom(OrderBy orderBy)
        {
            this.AddOrderBy("RANDOM()", orderBy);
        }

        public virtual void SetOrderByLpGroupName(OrderBy orderBy)
        {
            this.AddOrderBy("lp_page_manage_t.lp_group_name", orderBy);
        }

        internal void AddGroupByLp_group_name()
        {
            this.AddGroupBy("lp_page_manage_t.lp_group_name");
        }

        internal void AddGroupByCcode()
        {
            this.AddGroupBy("lp_page_manage_t.ccode");
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("lp_page_manage_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("lp_page_manage_t.site_id",(int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}
