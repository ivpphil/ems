﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpQuestionnaire
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public int? lp_page_manage_id { get; set; }
        public string item_code { get; set; }
        public EnumUse is_used { get; set; }
        public EnumRequired is_required { get; set; }
        public string item_note { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
    }   
}
