﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpQuestionnaireSetupRepository
        : ErsRepository<ErsLpQuestionnaireSetup>
    {
        public ErsLpQuestionnaireSetupRepository()
            : base("lp_questionnaire_setup_t")
        {
        }
    }
}
