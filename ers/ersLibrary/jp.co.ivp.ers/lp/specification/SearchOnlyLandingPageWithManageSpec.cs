﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.lp.specification
{
    public class SearchOnlyLandingPageWithManageSpec
        : SearchSpecificationBase
    {

        protected override string GetSearchDataSql()
        {
            return @"SELECT lp_page_t.* 
                    FROM lp_page_manage_t
                    INNER JOIN lp_page_t ON lp_page_manage_t.id = lp_page_t.lp_page_manage_id ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return @"SELECT COUNT(DISTINCT lp_page_t.id) AS " + countColumnAlias + " "
                   + @"FROM lp_page_manage_t
                    INNER JOIN lp_page_t ON lp_page_manage_t.id = lp_page_t.lp_page_manage_id ";
        }
    }
}
