﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.lp.specification
{
    public class SearchDQuestionnaireWithRelatedLpSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return @"SELECT page_id, lp_group_name, d_no, item_name, value FROM d_questionnaire_t 
                    INNER JOIN lp_questionnaire_setup_t ON d_questionnaire_t.item_code = lp_questionnaire_setup_t.item_code
                    INNER JOIN lp_page_manage_t ON d_questionnaire_t.page_id = lp_page_manage_t.id ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(d_questionnaire_t.id) as " + countColumnAlias + " FROM d_questionnaire_t " +
                   @"INNER JOIN lp_questionnaire_setup_t ON d_questionnaire_t.item_code = lp_questionnaire_setup_t.item_code
                    INNER JOIN lp_page_manage_t ON d_questionnaire_t.page_id = lp_page_manage_t.id ";
        }
    }
}
