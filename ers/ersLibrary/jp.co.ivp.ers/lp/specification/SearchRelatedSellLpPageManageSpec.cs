﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.lp.specification
{
    public class SearchRelatedSellLpPageManageSpec
        : SearchRelatedLpPageManageSpec
    {
        protected override string GetSearchDataSql()
        {
            return @"SELECT * 	
                    FROM lp_page_manage_t	
                    WHERE EXISTS
                        (
                        SELECT * FROM lp_page_t
	                    WHERE page_type_code = '" + EnumLpPageTypeCode.Sell + "' "
                        + "AND active = " + (int)EnumActive.Active
                        + @" AND lp_page_manage_id = lp_page_manage_t.id
                        )
                    AND current_timestamp BETWEEN public_st_date AND public_ed_date	";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(DISTINCT lp_page_manage_t.id) AS " + countColumnAlias + " "
                    + @"FROM lp_page_manage_t	
                    WHERE EXISTS
                        (
                        SELECT * FROM lp_page_t
	                    WHERE page_type_code = '" + EnumLpPageTypeCode.Sell + "' "
                        + "AND active = " + (int)EnumActive.Active
                        + @" AND lp_page_manage_id = lp_page_manage_t.id
                        )
                    AND current_timestamp BETWEEN public_st_date AND public_ed_date	";
        }
    }
}
