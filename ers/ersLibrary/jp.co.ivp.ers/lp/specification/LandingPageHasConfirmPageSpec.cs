﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.lp.specification
{
    public class LandingPageHasConfirmPageSpec
    {
        public bool IsSatisfiedBy(ErsLpPageManage lp_page_manage)
        {
            if (lp_page_manage != null)
            {
                var repository = ErsFactory.ersLpFactory.GetErsLpPageRepository();
                var criteria = ErsFactory.ersLpFactory.GetErsLpPageCriteria();

                criteria.lp_page_manage_id = lp_page_manage.id;
                criteria.page_type_code = EnumLpPageTypeCode.Confirm.ToString();
                criteria.active = EnumActive.Active;

                return repository.GetRecordCount(criteria) == 1;
            }

            return false;
        }
    }
}
