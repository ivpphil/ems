﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.lp.specification
{
    public class LandingPageStatusCampaignSpec
    {
        public bool HasSatisfiedByCcode(string ccode)
        {
            if (String.IsNullOrEmpty(ccode))
                return false;

            var spec = ErsFactory.ersLpFactory.GetSearchRelatedSellLpPageManageSpec();
            var criteria = this.GetCriteria(ccode, null);

            if (spec.GetCountData(criteria) == 0)
                return false;

            return true;
        }

        public bool HasSatisfiedCcodeAndPageId(string ccode, int? page_id)
        {
            if (String.IsNullOrEmpty(ccode) || !page_id.HasValue)
                return false;

            var spec = ErsFactory.ersLpFactory.GetSearchRelatedSellLpPageManageSpec();
            var criteria = this.GetCriteria(ccode, page_id);

            if (spec.GetCountData(criteria) == 0)
                return false;

            return true;
        }

        public bool HasSatisfiedByCcodeUpSellorSell(ErsLpPageManage lpManage, bool upsell)
        {
            var mechandise = new ErsMerchandise();

            var item = new ErsSku();
            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var skuCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            if(upsell)
            {
                skuCriteria.scode = lpManage.upsell_scode;
                mechandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(lpManage.upsell_scode, null);
            }
            else
            {
                skuCriteria.scode = lpManage.sell_scode;
                mechandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(lpManage.sell_scode, null);
            }
            item = skuRepository.Find(skuCriteria).First();

            var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(lpManage.sell_scode,null);

            if (item != null && mechandise != null)
            {
                var date_from = mechandise.date_from;
                var date_to = mechandise.date_to;

                if ((item.active == EnumActive.NonActive) || (DateTime.Now <= date_from || DateTime.Now >= date_to) || (mechandise.doc_bundling_flg == 1) || (merchandise.active == EnumActive.NonActive))
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }


        private ErsLpPageManageCriteria GetCriteria(string ccode, int? page_id)
        {
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageManageCriteria();
            if (!String.IsNullOrEmpty(ccode))
                criteria.ccode = ccode;

            if (page_id.HasValue)
                criteria.id = page_id;

            criteria.active = (int)EnumActive.Active;

            return criteria;
        }



    }
}
