﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.lp.specification
{
    public class SearchLpQuestionnaireWithSetupSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return @"SELECT lp_questionnaire_t.*, lp_questionnaire_setup_t.item_name AS item_name, lp_questionnaire_setup_t.template_name
                    FROM lp_questionnaire_t
                    INNER JOIN lp_questionnaire_setup_t
	                ON lp_questionnaire_t.item_code = lp_questionnaire_setup_t.item_code ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(DISTINCT lp_questionnaire_t.id) AS " + countColumnAlias + " "
                    + @" FROM lp_questionnaire_t	
                    INNER JOIN lp_questionnaire_setup_t	
	                ON lp_questionnaire_t.item_code = lp_questionnaire_setup_t.item_code ";
        }
    }
}
