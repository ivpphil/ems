﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpTemplateCriteria
        : Criteria
    {
        public virtual int id
        {
            set
            {
                Add(Criteria.GetCriterion("id", value, Operation.EQUAL));
            }
        }

        public virtual string template_code
        {
            set { this.Add(Criteria.GetCriterion("lp_template_t.template_code", value, Operation.EQUAL)); }
        }

        public virtual string[] template_code_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("lp_template_t.template_code", value));
            }
        }

        public EnumActive? active
        {
            set { this.Add(Criteria.GetCriterion("lp_template_t.active", value, Operation.EQUAL)); }
        }

        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("lp_template_t.id", orderBy);
        }
    }
}
