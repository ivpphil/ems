﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpPageTypeRepository
        : ErsRepository<ErsLpPageType>
    {
        public ErsLpPageTypeRepository()
            : base("lp_page_type_t")
        {
        }
    }
}
