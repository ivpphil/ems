﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpTemplate
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string template_code { get; set; }
        public string template_name { get; set; }
        public string template_file_path { get; set; }
        public string template_img_file_path { get; set; }
        public EnumCmsFieldType? block_1_head_use_flg { get; set; }
        public EnumCmsFieldType? block_1_body_use_flg { get; set; }
        public EnumCmsFieldType? block_1_free_use_flg { get; set; }
        public EnumCmsFieldType? block_2_head_use_flg { get; set; }
        public EnumCmsFieldType? block_2_body_use_flg { get; set; }
        public EnumCmsFieldType? block_2_free_use_flg { get; set; }
        public EnumCmsFieldType? block_3_head_use_flg { get; set; }
        public EnumCmsFieldType? block_3_body_use_flg { get; set; }
        public EnumCmsFieldType? block_3_free_use_flg { get; set; }
        public EnumCmsFieldType? block_4_head_use_flg { get; set; }
        public EnumCmsFieldType? block_4_body_use_flg { get; set; }
        public EnumCmsFieldType? block_4_free_use_flg { get; set; }
        public EnumCmsFieldType? block_5_head_use_flg { get; set; }
        public EnumCmsFieldType? block_5_body_use_flg { get; set; }
        public EnumCmsFieldType? block_5_free_use_flg { get; set; }
        public EnumCmsFieldType? block_6_head_use_flg { get; set; }
        public EnumCmsFieldType? block_6_body_use_flg { get; set; }
        public EnumCmsFieldType? block_6_free_use_flg { get; set; }
        public EnumCmsFieldType? block_7_head_use_flg { get; set; }
        public EnumCmsFieldType? block_7_body_use_flg { get; set; }
        public EnumCmsFieldType? block_7_free_use_flg { get; set; }
        public EnumCmsFieldType? block_8_head_use_flg { get; set; }
        public EnumCmsFieldType? block_8_body_use_flg { get; set; }
        public EnumCmsFieldType? block_8_free_use_flg { get; set; }
        public EnumCmsFieldType? block_9_head_use_flg { get; set; }
        public EnumCmsFieldType? block_9_body_use_flg { get; set; }
        public EnumCmsFieldType? block_9_free_use_flg { get; set; }
        public EnumCmsFieldType? block_10_head_use_flg { get; set; }
        public EnumCmsFieldType? block_10_body_use_flg { get; set; }
        public EnumCmsFieldType? block_10_free_use_flg { get; set; }
        public EnumCmsFieldType? upsell_button_1_use_flg { get; set; }
        public EnumCmsFieldType? upsell_button_2_use_flg { get; set; }
        public EnumCmsFieldType? upsell_button_3_use_flg { get; set; }
        public int? disp_order { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }

    }
}
