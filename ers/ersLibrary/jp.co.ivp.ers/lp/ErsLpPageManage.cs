﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpPageManage
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string page_name { get; set; }
        public string page_title { get; set; }
        public string lp_group_name { get; set; }
        public string logo_image_file { get; set; }
        public string ccode { get; set; }
        public DateTime? public_st_date { get; set; }
        public DateTime? public_ed_date { get; set; }
        public EnumLpBasicStgy? basic_stgy_kbn { get; set; }
        public EnumLpUpsellStgy? upsell_stgy_kbn { get; set; }
        public EnumLpBuyLimit? buy_limit_kbn { get; set; }
        public string sell_scode { get; set; }
        public EnumOrderType? sell_order_type { get; set; }
        public int? sell_price { get; set; }
        public EnumUse? sell_discount_flg { get; set; }
        public int? sell_discount_amount { get; set; }
        public int? sell_discount_price { get; set; }
        public int? sell_upsell_price { get; set; }
        public int? sell_first_regular_price { get; set; }
        public int? sell_regular_price { get; set; }
        public int? sell_max_amount { get; set; }
        public int? sell_max_stock { get; set; }
        public string upsell_scode { get; set; }
        public EnumOrderType? upsell_order_type { get; set; }
        public int? upsell_price { get; set; }
        public EnumUse? upsell_discount_flg { get; set; }
        public int? upsell_discount_amount { get; set; }
        public int? upsell_discount_price { get; set; }
        public int? upsell_first_regular_price { get; set; }
        public int? upsell_regular_price { get; set; }
        public int? upsell_max_amount { get; set; }
        public int? upsell_max_stock { get; set; }
        public EnumUse? coupon_flg { get; set; }
        public string coupon_code { get; set; }
        public EnumUse? carriage_free_flg { get; set; }
        public int? carriage_free_price { get; set; }
        public EnumUse? personal_info_kbn { get; set; }
        public string personal_info_url { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
        public virtual int[] site_id { get; set; }
    }
}
