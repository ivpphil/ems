﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpTemplateRepository
        : ErsRepository<ErsLpTemplate>
    {
        public ErsLpTemplateRepository()
            : base("lp_template_t")
        {
        }
    }
}
