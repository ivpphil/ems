﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.lp
{
    public class ErsDQuestionnaire
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public int? page_id { get; set; }
        public string item_code { get; set; }
        public string d_no { get; set; }
        public string mcode { get; set; }
        public string value { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
    }
}
