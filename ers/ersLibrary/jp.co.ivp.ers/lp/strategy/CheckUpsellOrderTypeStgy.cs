﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.lp.strategy
{
    public class CheckUpsellOrderTypeStgy
    {
        public ValidationResult Check(EnumOrderType? sell_order_type, EnumOrderType? upsell_order_type)
        {
            if (!sell_order_type.HasValue || !upsell_order_type.HasValue)
            {
                return null;
            }

            if (sell_order_type != EnumOrderType.BothUsuallyAndSubscription && sell_order_type != upsell_order_type)
            {
                return new ValidationResult(ErsResources.GetMessage("LP0002723"), new[] { "sell_order_type", "upsell_order_type" });
            }

            return null;
        }
    }
}
