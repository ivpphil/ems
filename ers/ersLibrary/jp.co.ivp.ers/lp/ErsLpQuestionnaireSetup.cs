﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpQuestionnaireSetup
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string item_code { get; set; }

        public string item_name { get; set; }

        public string template_name { get; set; }

        public string validation_comment { get; set; }

        public EnumCmsFieldType? is_system_required { get; set; }

        public int? disp_order { get; set; }

        public DateTime? intime { get; set; }

        public DateTime? utime { get; set; }

        public EnumActive? active { get; set; }
    }
}
