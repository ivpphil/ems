﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpQuestionnaireRepository
        : ErsRepository<ErsLpQuestionnaire>
    {
        public ErsLpQuestionnaireRepository()
            : base("lp_questionnaire_t")
        {
        }
    }
}
