﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpPageCriteria
        : Criteria
    {

        public int? lp_page_id
        {
            set
            {
                Add(Criteria.GetCriterion("lp_page_t.id", value, Operation.EQUAL));
            }
        }

        public int? lp_page_manage_id
        {
            set
            {
                Add(Criteria.GetCriterion("lp_page_t.lp_page_manage_id", value, Operation.EQUAL));
            }
        }

        public string page_type_code
        {
            set
            {
                Add(Criteria.GetCriterion("lp_page_t.page_type_code", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("lp_page_t.active", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("lp_page_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("lp_page_t.site_id",(int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}
