﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpQuestionnaireCriteria
        : Criteria
    {
        public int? lp_page_manage_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_questionnaire_t.lp_page_manage_id", value, Operation.EQUAL));
            }
        }

        public string item_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_questionnaire_t.item_code", value, Operation.EQUAL));
            }
        }

        public void SetActiveLpQuestionnaireAndLpSetup()
        {
            this.Add(Criteria.GetCriterion("lp_questionnaire_t.active", EnumActive.Active, Operation.EQUAL));
            this.Add(Criteria.GetCriterion("lp_questionnaire_setup_t.active", EnumActive.Active, Operation.EQUAL));
        }

        public EnumCmsFieldType? lp_setup_system_required
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_questionnaire_setup_t.is_system_required", value, Operation.EQUAL));
            }
        }

        public EnumCmsFieldType? lp_setup_system_required_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_questionnaire_setup_t.is_system_required", value, Operation.NOT_EQUAL));
            }
        }

        public EnumUse? is_used
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_questionnaire_t.is_used", value, Operation.EQUAL));
            }
        }

        public void SetOrderById(OrderBy orderby)
        {
            this.AddOrderBy("lp_questionnaire_t.id", orderby);
        }
    }
}
