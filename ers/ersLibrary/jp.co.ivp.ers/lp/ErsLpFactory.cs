﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.lp.specification;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.lp.strategy;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpFactory
    {
        public virtual ErsLpPageCriteria GetErsLpPageCriteria()
        {
            return new ErsLpPageCriteria();
        }

        public virtual ErsLpPageRepository GetErsLpPageRepository()
        {
            return new ErsLpPageRepository();
        }

        public virtual ErsLpPage GetErsLpPage()
        {
            return new ErsLpPage();
        }

        public virtual ErsLpPageManageCriteria GetErsLpPageManageCriteria()
        {
            return new ErsLpPageManageCriteria();
        }

        public virtual ErsLpPageManageRepository GetErsLpPageManageRepository()
        {
            return new ErsLpPageManageRepository();
        }

        public virtual ErsLpPageManage GetErsLpPageManage()
        {
            return new ErsLpPageManage();
        }

        public virtual ErsLpPageManage GetErsLpPageManageWithId(int? id)
        {
            var repository = this.GetErsLpPageManageRepository();
            var criteria = this.GetErsLpPageManageCriteria();
            criteria.id = id;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            var list = repository.Find(criteria);

            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        public virtual ErsLpPageManage GetErsLpPageManageWithIdForAdmin(int? id)
        {
            var repository = this.GetErsLpPageManageRepository();
            var criteria = this.GetErsLpPageManageCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        public virtual ErsLpPageManage GetErsLpPageManageWithCcode(string ccode)
        {
            var repository = this.GetErsLpPageManageRepository();
            var criteria = this.GetErsLpPageManageCriteria();
            criteria.ccode = ccode;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            var list = repository.Find(criteria);

            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        public virtual ErsLpPageManage GetErsLpPageManageWithIdAndCcode(int? id, string ccode)
        {
            var repository = this.GetErsLpPageManageRepository();
            var criteria = this.GetErsLpPageManageCriteria();
            criteria.id = id;
            criteria.ccode = ccode;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            var list = repository.Find(criteria);

            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        public virtual ErsLpPageManage GetRelatedRandomErsLpPageManageWithCcode(string ccode)
        {
            var spec = this.GetSearchRelatedSellLpPageManageSpec();
            var criteria = this.GetErsLpPageManageCriteria();
            criteria.ccode = ccode;
            criteria.active = (int)EnumActive.Active;
            criteria.SetOrderbyRandom(db.Criteria.OrderBy.ORDER_BY_ASC);
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.LIMIT = 1;

            var list = spec.GetSearchData(criteria);

            foreach (var page in list)
            {
                var lp_page_manage = this.GetErsLpPageManage();
                lp_page_manage.OverwriteWithParameter(page);

                var checkSpec = ErsFactory.ersLpFactory.GetLandingPageStatusCampaignSpec();
                if (!checkSpec.HasSatisfiedByCcodeUpSellorSell(lp_page_manage, false))
                {
                    continue;
                }

                if (lp_page_manage.basic_stgy_kbn == EnumLpBasicStgy.UpSell && !checkSpec.HasSatisfiedByCcodeUpSellorSell(lp_page_manage, true))
                {
                    continue;
                }

                return lp_page_manage;
            }

            return null;
        }

        public virtual ErsLpQuestionnaireSetupRepository GetErsLpQuestionnaireSetupRepository()
        {
            return new ErsLpQuestionnaireSetupRepository();
        }

        public virtual ErsLpQuestionnaireSetupCriteria GetErsLpQuestionnaireSetupCriteria()
        {
            return new ErsLpQuestionnaireSetupCriteria();
        }

        public virtual ErsLpQuestionnaireSetup GetErsLpQuestionnaireSetup()
        {
            return new ErsLpQuestionnaireSetup();
        }

        public virtual ErsLpQuestionnaireRepository GetErsLpQuestionnaireRepository()
        {
            return new ErsLpQuestionnaireRepository();
        }

        public virtual ErsLpQuestionnaireCriteria GetErsLpQuestionnaireCriteria()
        {
            return new ErsLpQuestionnaireCriteria();
        }

        public virtual ErsLpQuestionnaire GetErsLpQuestionnaire()
        {
            return new ErsLpQuestionnaire();
        }

        public virtual ErsLpQuestionnaire GetErsLpQuestionnaireWithItemCode(int? lp_page_manage_id, string item_code)
        {
            var repository = this.GetErsLpQuestionnaireRepository();
            var criteria = this.GetErsLpQuestionnaireCriteria();
            criteria.lp_page_manage_id = lp_page_manage_id;
            criteria.item_code = item_code;
            var listQuestionnaire = repository.Find(criteria);
            if (listQuestionnaire.Count != 1)
            {
                return null;
            }

            return listQuestionnaire.First();
        }

        public virtual ErsLpPageTypeRepository GetErsLpPageTypeRepository()
        {
            return new ErsLpPageTypeRepository();
        }

        public virtual ErsLpPageTypeCriteria GetErsLpPageTypeCriteria()
        {
            return new ErsLpPageTypeCriteria();
        }

        public virtual ErsLpPageType GetErsLpPageType()
        {
            return new ErsLpPageType();
        }

        public virtual ErsLpTemplateRepository GetErsLpTemplateRepository()
        {
            return new ErsLpTemplateRepository();
        }

        public virtual ErsLpTemplateCriteria GetErsLpTemplateCriteria()
        {
            return new ErsLpTemplateCriteria();
        }

        public virtual ErsLpTemplate GetErsLpTemplate()
        {
            return new ErsLpTemplate();
        }

        public virtual ErsLpTemplate GetErsLpTemplateWithTemplateCode(string template_code)
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpTemplateRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpTemplateCriteria();
            criteria.template_code = template_code;
            criteria.active = EnumActive.Active;
            var listTemplate = repository.Find(criteria);
            if (listTemplate.Count == 0)
            {
                return null;
            }

            return listTemplate.First();
        }

        public virtual ErsDQuestionnaire GetErsDQuestionnaire()
        {
            return new ErsDQuestionnaire();
        }

        public virtual ErsDQuestionnaireRepository GetErsDQuestionnaireRepository()
        {
            return new ErsDQuestionnaireRepository();
        }

        public virtual ErsDQuestionnaireCriteria GetErsDQuestionnaireCriteria()
        {
            return new ErsDQuestionnaireCriteria();
        }

        public virtual ErsDQuestionnaire GetErsDQuestionnaireWithParameter(Dictionary<string, object> parameters)
        {
            var d_questionnaire = this.GetErsDQuestionnaire();
            d_questionnaire.OverwriteWithParameter(parameters);

            return d_questionnaire;
        }

        public virtual ErsDQuestionnaire GetErsDQuestionnaireWithId(int? id, bool IsActive = false)
        {
            var repository = this.GetErsDQuestionnaireRepository();
            var criteria = this.GetErsDQuestionnaireCriteria();
            criteria.id = id;

            if (IsActive)
                criteria.active = EnumActive.Active;

            if (repository.GetRecordCount(criteria) != 1)
                return null;

            return repository.Find(criteria).First();
        }

        public virtual SearchRelatedLpPageManageSpec GetSearchRelatedLpPageManageSpec()
        {
            return new SearchRelatedLpPageManageSpec();
        }

        public virtual SearchRelatedSellLpPageManageSpec GetSearchRelatedSellLpPageManageSpec()
        {
            return new SearchRelatedSellLpPageManageSpec();
        }

        public virtual SearchLpQuestionnaireWithSetupSpec GetSearchLpQuestionnaireWithSetupSpec()
        {
            return new SearchLpQuestionnaireWithSetupSpec();
        }

        public virtual LandingPageStatusCampaignSpec GetLandingPageStatusCampaignSpec()
        {
            return new LandingPageStatusCampaignSpec();
        }

        public virtual SearchLandingPageMangeHasUpSellSpec GetSearchLandingPageMangeHasUpSellSpec()
        {
            return new SearchLandingPageMangeHasUpSellSpec();
        }

        public virtual LandingPageHasConfirmPageSpec GetLandingPageHasConfirmPageSpec()
        {
            return new LandingPageHasConfirmPageSpec();
        }

        public virtual SearchOnlyLandingPageWithManageSpec GetSearchOnlyLandingPageWithManageSpec()
        {
            return new SearchOnlyLandingPageWithManageSpec();
        }

        public virtual CheckUpsellOrderTypeStgy GetCheckUpsellOrderTypeStgy()
        {
            return new CheckUpsellOrderTypeStgy();
        }

        public virtual SearchDQuestionnaireWithRelatedLpSpec GetSearchDQuestionnaireWithRelatedLpSpec()
        {
            return new SearchDQuestionnaireWithRelatedLpSpec();
        }
    }
}
