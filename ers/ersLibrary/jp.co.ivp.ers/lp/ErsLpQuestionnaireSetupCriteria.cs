﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpQuestionnaireSetupCriteria
        : Criteria
    {
        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("lp_questionnaire_setup_t.active", value, Operation.EQUAL));
            }
        }

        public void SetOrderByDisp_order(OrderBy orderBy)
        {
            this.AddOrderBy("lp_questionnaire_setup_t.disp_order", orderBy);
        }
    }
}
