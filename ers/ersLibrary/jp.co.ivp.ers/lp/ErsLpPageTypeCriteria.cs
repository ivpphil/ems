﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpPageTypeCriteria
        : Criteria
    {
        public virtual EnumLpPageTypeCode page_type_code
        {
            set { this.Add(Criteria.GetCriterion("lp_page_type_t.page_type_code", value.ToString(), Operation.EQUAL)); }
        }

        public EnumActive? active
        {
            set { this.Add(Criteria.GetCriterion("lp_page_type_t.active", value, Operation.EQUAL)); }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("lp_page_type_t.id", orderBy);
        }
    }
}
