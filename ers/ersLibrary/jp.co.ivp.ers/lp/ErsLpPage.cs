using System.Collections.Generic; 
using System.Text; 
using System;
using jp.co.ivp.ers.mvc;
using System.Web;


namespace jp.co.ivp.ers.lp {

    public class ErsLpPage : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public int? lp_page_manage_id { get; set; }
        public string page_type_code { get; set; }
        public string template_code { get; set; }
        public string block_1_head { get; set; }
        public string block_1_body { get; set; }
        public string block_1_free { get; set; }
        public string block_2_head { get; set; }
        public string block_2_body { get; set; }
        public string block_2_free { get; set; }
        public string block_3_head { get; set; }
        public string block_3_body { get; set; }
        public string block_3_free { get; set; }
        public string block_4_head { get; set; }
        public string block_4_body { get; set; }
        public string block_4_free { get; set; }
        public string block_5_head { get; set; }
        public string block_5_body { get; set; }
        public string block_5_free { get; set; }
        public string block_6_head { get; set; }
        public string block_6_body { get; set; }
        public string block_6_free { get; set; }
        public string block_7_head { get; set; }
        public string block_7_body { get; set; }
        public string block_7_free { get; set; }
        public string block_8_head { get; set; }
        public string block_8_body { get; set; }
        public string block_8_free { get; set; }
        public string block_9_head { get; set; }
        public string block_9_body { get; set; }
        public string block_9_free { get; set; }
        public string block_10_head { get; set; }
        public string block_10_body { get; set; }
        public string block_10_free { get; set; }
        public string upsell_button_1_file_name { get; set; }
        public string upsell_button_2_file_name { get; set; }
        public string upsell_button_3_file_name { get; set; } 
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
       
    }
}
