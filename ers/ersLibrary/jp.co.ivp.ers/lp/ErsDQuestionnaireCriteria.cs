﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.lp
{
    public class ErsDQuestionnaireCriteria
        : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_questionnaire_t.id", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_questionnaire_t.active", value, Operation.EQUAL));
            }
        }

        public string value_not_eq
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_questionnaire_t.value", value, Operation.NOT_EQUAL));
            }
        }

        public int? page_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_questionnaire_t.page_id", value, Operation.EQUAL));
            }
        }

        public void SetOrderByPageId(OrderBy orderBy)
        {
            this.AddOrderBy("d_questionnaire_t.page_id", orderBy);
        }

        public void SetOrderByDno(OrderBy orderBy)
        {
            this.AddOrderBy("d_questionnaire_t.d_no", orderBy);
        }

        public void SetOrderByLpQuestId(OrderBy orderBy)
        {
            this.AddOrderBy("lp_questionnaire_setup_t.id", orderBy);
        }
    }
}
