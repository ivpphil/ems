﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.lp
{
    public class ErsDQuestionnaireRepository
        : ErsRepository<ErsDQuestionnaire>
    {
        public ErsDQuestionnaireRepository()
            : base("d_questionnaire_t") 
        { }
    }
}
