﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.lp
{
    public class ErsLpPageType
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string page_type_code { get; set; }
        public string page_type_name { get; set; }
        public string[] available_template { get; set; }
        public string block_1_head_name { get; set; }
        public string block_1_body_name { get; set; }
        public string block_1_free_name { get; set; }
        public string block_2_head_name { get; set; }
        public string block_2_body_name { get; set; }
        public string block_2_free_name { get; set; }
        public string block_3_head_name { get; set; }
        public string block_3_body_name { get; set; }
        public string block_3_free_name { get; set; }
        public string block_4_head_name { get; set; }
        public string block_4_body_name { get; set; }
        public string block_4_free_name { get; set; }
        public string block_5_head_name { get; set; }
        public string block_5_body_name { get; set; }
        public string block_5_free_name { get; set; }
        public string block_6_head_name { get; set; }
        public string block_6_body_name { get; set; }
        public string block_6_free_name { get; set; }
        public string block_7_head_name { get; set; }
        public string block_7_body_name { get; set; }
        public string block_7_free_name { get; set; }
        public string block_8_head_name { get; set; }
        public string block_8_body_name { get; set; }
        public string block_8_free_name { get; set; }
        public string block_9_head_name { get; set; }
        public string block_9_body_name { get; set; }
        public string block_9_free_name { get; set; }
        public string block_10_head_name { get; set; }
        public string block_10_body_name { get; set; }
        public string block_10_free_name { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
  }
}
