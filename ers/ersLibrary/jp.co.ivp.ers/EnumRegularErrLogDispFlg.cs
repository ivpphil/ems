﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumRegularErrLogDispFlg
        : short
    {
        SystemError,
        PaymentError,
        StockError
    }
}
