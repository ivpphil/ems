﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumMobileCarrier
    {
        OTHER,
        DOCOMO,
        AU,
        SOFTBANK,
        GoogleBot,
        YahooBot,
        SmartPhone
    }
}
