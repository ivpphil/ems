﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    //use for process UpKind
    public enum EnumUpKind
    {
        /// <summary>
        /// 0 : 監視 [MONITOR]
        /// </summary>
        MONITOR,

        /// <summary>
        /// 1 : ERS顧客抽出 [MEMBER]
        /// </summary>
        MEMBER,

        /// <summary>
        /// 2 : CSVアップロード [UPLOAD]
        /// </summary>
        UPLOAD,

        /// <summary>
        /// 3 : 登録済みリストコピー [COPY]
        /// </summary>
        COPY,

        /// <summary>
        /// 4 : ステップメール [STEP_MAIL]
        /// </summary>
        STEP_MAIL,
    }
}
