﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Enums for ReferenceDate (birthday, lastpurchaseddate or registrationDate)
    /// </summary>
    public enum EnumReferenceDate
    {
        /// <summary>
        /// 1 : 誕生日 [Birthdate]
        /// </summary>
        Birthdate = 1,

        /// <summary>
        /// 2 : 最終購入日 [LastPurchasedDate]
        /// </summary>
        LastPurchasedDate = 2,

        /// <summary>
        /// 3 : 会員登録日 [RegistrationDate]
        /// </summary>
        RegistrationDate = 3,

        /// <summary>
        /// 4 : 商品購入日 [LastItemPurchasedDate]
        /// </summary>
        LastItemPurchasedDate = 4,

        /// <summary>
        /// 5 : 商品配送日 [LastItemShippedDate]
        /// </summary>
        LastItemShippedDate = 5
    }
}
