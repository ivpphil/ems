﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.atmail.process;
using jp.co.ivp.ers.atmail.specification;
using jp.co.ivp.ers.atmail.mail_to;
using jp.co.ivp.ers.atmail.set_template;
using jp.co.ivp.ers.atmail.strategy;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.atmail.set_mst;

namespace jp.co.ivp.ers.atmail
{
    /// <summary>
    /// Factory class for AtMail.
    /// Provide methods that are related in AtMail.
    /// </summary>
    public class ErsAtMailFactory
    {
        /// <summary>
        /// Obtain instance of ErsMailTo class 
        /// </summary>
        /// <returns>returns instance of ErsMailTo</returns>
        public ErsMailTo GetErsMailTo()
        {
            return new ErsMailTo();
        }

        /// <summary>
        /// Obtain instance of ErsAmSetup class 
        /// </summary>
        /// <returns>returns instance of ErsAmSetup</returns>
        public ErsAmSetup GetErsAmSetup()
        {
            return new ErsAmSetup();
        }

        /// <summary>
        /// Obtain instance of ErsProcess class 
        /// </summary>
        /// <returns>returns instance of ErsProcess</returns>
        public ErsProcess GetErsProcess()
        {
            return new ErsProcess();
        }
        /// <summary>
        /// get process record using id
        /// </summary>
        /// <param name="process_id"></param>
        /// <returns>returns firs record filtered process</returns>
        public ErsProcess GetErsProcessWithId(int? process_id)
        {
            var processRepo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var processCriteria = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();
            processCriteria.id = process_id;
            var listProcessRepo = processRepo.Find(processCriteria);
            if (processRepo.GetRecordCount(processCriteria) == 0)
            {
                return null;
            }
            else
            {
                return listProcessRepo.First();
            }
        }
        public ErsProcess GetErsProcessWithId(int? process_id, ErsDatabase db)
        {
            var processRepo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository(db);
            var processCriteria = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();
            processCriteria.id = process_id;
            var listProcessRepo = processRepo.Find(processCriteria);
            if (processRepo.GetRecordCount(processCriteria) == 0)
            {
                return null;
            }
            else
            {
                return listProcessRepo.First();
            }
        }

        /// <summary>
        /// Obtain instance of ErsProcessRepository class 
        /// </summary>
        /// <returns>returns instance of ErsProcessRepository</returns>
        public ErsProcessRepository GetErsProcessRepository()
        {
            return new ErsProcessRepository();
        }
        /// <summary>
        /// Obtain instance of ErsProcessCriteria class 
        /// </summary>
        /// <returns>returns instance of ErsProcessCriteria</returns>
        public ErsProcessRepository GetErsProcessRepository(ErsDatabase db)
        {
            return new ErsProcessRepository(db);
        }

        public ErsProcessCriteria GetErsProcessCriteria()
        {
            return new ErsProcessCriteria();
        }

        /// <summary>
        /// Obtain instance of ErsMailToRepository class 
        /// </summary>
        /// <returns>returns instance of ErsMailToRepository</returns>
        public ErsMailToRepository GetErsMailToRepository()
        {
            return new ErsMailToRepository();
        }

        public ErsMailToRepository GetErsMailToRepository(ErsDatabase db)
        {
            return new ErsMailToRepository(db);
        }

        /// <summary>
        /// Obtain instance of ErsMailToCriteria class 
        /// </summary>
        /// <returns>returns instance of ErsMailToCriteria</returns>

        public ErsMailToCriteria GetErsMailToCriteria()
        {
            return new ErsMailToCriteria();
        }

        /// <summary>
        /// Obtain instance of ErsAmSetupRepository class 
        /// </summary>
        /// <returns>returns instance of ErsAmSetupRepository</returns>
        public ErsAmSetupRepository GetErsAmSetupRepository()
        {
            return new ErsAmSetupRepository();
        }

        public ErsAmSetupRepository GetErsAmSetupRepository(ErsDatabase db)
        {
            return new ErsAmSetupRepository(db);
        }
        /// <summary>
        /// Obtain instance of ErsAmSetupCriteria class 
        /// </summary>
        /// <returns>returns instance of ErsAmSetupCriteria</returns>
        public ErsAmSetupCriteria GetErsErsAmSetupCriteria()
        {
            return new ErsAmSetupCriteria();
        }
        /// <summary>
        /// Obtain instance of ErsAmTemplateRepository class 
        /// </summary>
        /// <returns>returns instance of ErsAmTemplateRepository</returns>
        public ErsAmTemplateRepository GetErsAmTemplateRepository()
        {
            return new ErsAmTemplateRepository();
        }
        /// <summary>
        /// Obtain instance of ErsAmTemplateCriteria class 
        /// </summary>
        /// <returns>returns instance of ErsAmTemplateCriteria</returns>
        public ErsAmTemplateCriteria GetErsAmTemplateCriteria()
        {
            return new ErsAmTemplateCriteria();
        }

        /// <summary>
        /// Obtain instance of CheckMailToExistStgy class 
        /// </summary>
        /// <returns>returns instance of CheckMailToExistStgy</returns>
        public CheckMailToExistStgy GetCheckMailToExistStgy()
        {
            return new CheckMailToExistStgy();
        }

        /// <summary>
        /// Obtain instance of CheckProcessExistStgy class 
        /// </summary>
        /// <returns>returns instance of CheckProcessExistStgy</returns>
        public CheckProcessExistStgy GetCheckProcessExistStgy()
        {
            return new CheckProcessExistStgy();
        }

        /// <summary>
        /// Get records from process table using OverwriteWithParameter function of ErsProcess
        /// </summary>
        /// <param name="parameters">values saved in dictionary</param>
        /// <returns>return values of ErsProcess</returns>
        public virtual ErsProcess  GetErsProcessWithParameters(Dictionary<string, object> parameters)
        {
            var process = this.GetErsProcess();
            process.OverwriteWithParameter (parameters);
            return process;
        }

        /// <summary>
        /// Get records from mailto table using OverwriteWithParameter function of ErsMailTo
        /// </summary>
        /// <param name="parameters">values saved in dictionary</param>
        /// <returns>return values of ErsMailTo</returns>
        public virtual ErsMailTo GetErsMailToWithParameters(Dictionary<string, object> parameters)
        {
            var mail = this.GetErsMailTo();
            mail.OverwriteWithParameter(parameters);
            return mail;
        }
        /// <summary>
        /// Get records from setup_t table using OverwriteWithParameter function of ErsAmSetup
        /// </summary>
        /// <param name="parameters">values saved in dictionary</param>
        /// <returns>return values of ErsAmSetup</returns>
        public virtual ErsAmSetup GetErsAmSetupWithParameters(Dictionary<string, object> parameters)
        {
            var setup = this.GetErsAmSetup();
            setup.OverwriteWithParameter(parameters);
            return setup;
        }

        /// <summary>
        /// Obtain instance of ErsAmTemplate class 
        /// </summary>
        /// <returns>returns instance of ErsAmTemplate</returns>
        public ErsAmTemplate GetErsAmTemplate()
        {
            return new ErsAmTemplate();
        }
        
        /// <summary>
        /// Get records from template_t table using OverwriteWithParameter function of ErsAmTemplate
        /// </summary>
        /// <param name="parameters">values saved in dictionary</param>
        /// <returns>return values of ErsAmTemplate</returns>
        public virtual ErsAmTemplate GetErsAmTemplateWithParameters(Dictionary<string, object> parameters)
        {
            var template = this.GetErsAmTemplate();
            template.OverwriteWithParameter(parameters);
            return template;
        }
        /// <summary>
        /// Obtain instance of RegistMailToStgy class 
        /// </summary>
        /// <returns>returns instance of RegistMailToStgy</returns>
        public RegistMailToStgy GetRegistMailToStgy()
        {
            return new RegistMailToStgy();
        }

        /// <summary>
        /// get template_t resord using id 
        /// </summary>
        /// <param name="template_id"></param>
        /// <returns>first record on template_t</returns>
        public ErsAmTemplate GetErsAmTemplateWithId(int template_id)
        {
            var templateRepo = ErsFactory.ErsAtMailFactory.GetErsAmTemplateRepository();
            var templateCriteria = ErsFactory.ErsAtMailFactory.GetErsAmTemplateCriteria();
            templateCriteria.id = template_id;
            var listTemplateRepo = templateRepo.Find(templateCriteria);
            if (templateRepo.GetRecordCount(templateCriteria) == 0)
            {
                return null;
            }
            else
            {
                return listTemplateRepo.First();
            }
        }

        /// <summary>
        /// get ErsMailTo resord using id 
        /// </summary>
        /// <param name="template_id"></param>
        /// <returns>first record on template_t</returns>
        public virtual ErsMailTo GetErsAtMailProcessId(int? id)
        {
            var ersMailToRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var ersMailToCriteria = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            ersMailToCriteria.id = id;
            var list = ersMailToRepo.Find(ersMailToCriteria);
            if (list.Count != 1)
                return null;

            return list[0];
        }

        /// <summary>
        /// get ErsMailTo resord using id using record lock 
        /// </summary>
        /// <param name="template_id"></param>
        /// <returns>first record on template_t</returns>
        public virtual ErsMailTo GetErsAtMailProcessIdWithRecordLock(int? id)
        {
            var ersMailToRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var ersMailToCriteria = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            ersMailToCriteria.id = id;
            ersMailToCriteria.ForUpdate = true;
            var list = ersMailToRepo.Find(ersMailToCriteria);
            if (list.Count != 1)
                return null;

            return list[0];
        }

        public virtual ErsMailTo GetErsAtMailProcessId(int id, ErsDatabase dbCon)
        {
            var ersMailToRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository(dbCon);
            var ersMailToCriteria = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();
            ersMailToCriteria.id = id;
            var list = ersMailToRepo.Find(ersMailToCriteria);
            if (list.Count != 1)
                return null;

            return list[0];
        }

        /// <summary>
        /// Get instance of CheckRedirectUrlStgy.
        /// </summary>
        /// <returns></returns>
        public virtual CheckRedirectUrlStgy GetCheckRedirectUrlStgy()
        {
            return new CheckRedirectUrlStgy();
        }
        /// <summary>
        /// Get instance of CheckEncodedMailAddressStgy.
        /// </summary>
        /// <returns></returns>
        public  virtual CheckEncodedMailAddressStgy GetCheckEncodedMailAddressStgy()
        {
            return new CheckEncodedMailAddressStgy();
        }

        /// <summary>
        /// Get instance of MailToIdSearchSpecification.
        /// </summary>
        /// <returns></returns>
        public virtual MailToIdSearchSpecification GetMailToIdSearchSpecification()
        {
            return new MailToIdSearchSpecification();
        }

        public virtual AtMailSearchSpecification GetAtMailSearchSpecification()
        {
            return new AtMailSearchSpecification();
        }

        public EmailToModel GetEmailToModel()
        {
            return new EmailToModel();
        }

        public DetermineMformatStgy GetDetermineMformatStgy()
        {
            return new DetermineMformatStgy();
        }
    }
}
