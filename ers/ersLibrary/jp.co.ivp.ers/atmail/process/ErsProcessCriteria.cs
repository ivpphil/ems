﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.atmail.process
{
    /// <summary>
    /// use for am_process_t filtering
    /// </summary>
    public class ErsProcessCriteria
        : Criteria
    {
        #region am_process_t
        //filter records using am_process_t.id
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("am_process_t.id", value, Operation.EQUAL));
            }
        }

        public int? id_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("am_process_t.id", value, Operation.NOT_EQUAL));
            }
        }

        //filter records using am_process_t.id
        public IEnumerable<int> id_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("am_process_t.id", value));
            }
        }

        //filter records using am_process_t.active
        public EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("am_process_t.active", (int)value, Operation.EQUAL));
            }
        }

        //filter records using am_process_t.up_kind
        public EnumUpKind? up_kind
        {
            set
            {
                Add(Criteria.GetCriterion("am_process_t.up_kind", (int)value, Operation.EQUAL));
            }
        }

        //filter records using am_process_t.secheduled_date greater than or equal
        public DateTime scheduled_date_from
        {
            set
            {
                this.Add(Criteria.GetCriterion("am_process_t.scheduled_date", value, Operation.GREATER_EQUAL));
            }
        }
        //filter records using am_process_t.scheduled_date less than or equal
        public DateTime? scheduled_date_to
        {
            set
            {
                this.Add(Criteria.GetCriterion("am_process_t.scheduled_date", value, Operation.LESS_EQUAL));
            }
        }
        //filter records using am_process_t.step_mail_id is not null
        public int? step_mail_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("am_process_t.step_mail_id", value, Operation.EQUAL));
            }
        }

        public IEnumerable<int> step_mail_id_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("am_process_t.step_mail_id", value));
            }
        }

        //filter records using am_process_t.step_mail_id is not null
        public int? step_mail_id_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("am_process_t.step_mail_id", value, Operation.NOT_EQUAL));
            }
        }

        //filter records using am_process_t.status
        public EnumAmProcessStatus? status
        {
            set
            {
                this.Add(Criteria.GetCriterion("am_process_t.status", (int)value, Operation.EQUAL));
            }
        }
        //added by Sean Seno 20130416
        public EnumAmProcessStatus[] status_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("am_process_t.status", value.Cast<short>()));
            }
        }

        /// <summary>
        ///  search condition for exists active mailto
        /// </summary>
        /// <param name="exists">true : "EXISTS" / false : "NOT EXISTS"</param>
        public void SetExistsActiveMailTo(bool exists = true)
        {
            string NOT = exists ? string.Empty : "NOT ";

            var dicData = new Dictionary<string, object>();

            // アクティブ [Active]
            dicData["active"] = EnumActive.Active;

            string SQL = "(" + NOT + "EXISTS (SELECT 1 FROM am_mailto_t"
                + " WHERE am_mailto_t.process_id = am_process_t.id"
                + " AND am_mailto_t.active = :active))";

            Add(Criteria.GetUniversalCriterion(SQL, dicData));
        }

        //ordering record by am_process_t.id
        public void SetOrderByProcessID(OrderBy orderBy)
        {
            AddOrderBy("am_process_t.id", orderBy);
        }

        public void SetOrderByScheduleDate(OrderBy orderBy)
        {
            this.AddOrderBy("am_process_t.scheduled_date", orderBy);
        }
        #endregion

        #region am_mailto_t
        public EnumActive? mailTo_active
        {
            set
            {
                this.Add(Criteria.GetCriterion("am_mailto_t.active", (int)value, Operation.EQUAL));
            }
        }

        public EnumSentFlg? mailTo_sent_flg
        {
            set
            {
                this.Add(Criteria.GetCriterion("am_mailto_t.sent_flg", value, Operation.EQUAL));
            }
        }

        public DateTime mailTo_scheduled_date_less_or_null
        {
            set
            {
                var scheduled_date_less = Criteria.GetCriterion("am_mailto_t.scheduled_date", value, Operation.LESS_THAN);
                var scheduled_date_null = Criteria.GetCriterion("am_mailto_t.scheduled_date", null, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { scheduled_date_less, scheduled_date_null }));
            }
        }

        public DateTime mailTo_scheduled_date_greater
        {
            set
            {
                this.Add(Criteria.GetCriterion("am_mailto_t.scheduled_date", value, Operation.GREATER_THAN));
            }
        }

        public IEnumerable<string> mailTo_mcode_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("am_mailto_t.mcode", value));
            }
        }
        #endregion

        public void SetHasTargetMailto(DateTime? executeDate)
        {
            var strSQL = "EXISTS(SELECT * FROM am_mailto_t WHERE ( scheduled_date < :executeDate OR  scheduled_date IS NULL) AND active = :active and am_process_t.id = process_id)";
            this.Add(Criteria.GetUniversalCriterion(strSQL,
                new Dictionary<string, object>() { 
                { "executeDate", executeDate }, 
                { "active", (int)EnumActive.Active } }));
        }
    }
}
