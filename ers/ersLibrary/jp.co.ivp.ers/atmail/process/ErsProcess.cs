﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.atmail
{
    /// <summary>
    /// Hold values from process table in ersmail database.
    /// Inherits ErsRepositoryEntity class.
    /// </summary>
    public class ErsProcess : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual int? step_mail_id { get; set; }
        public virtual EnumUpKind? up_kind { get; set; }
        public virtual EnumAmProcessStatus? status { get; set; }
        public virtual string from_email { get; set; }
        public virtual string from_name { get; set; }
        public virtual string reply_email { get; set; }
        public virtual string subject { get; set; }
        public virtual string body { get; set; }
        public virtual string html_body { get; set; }
        public virtual string feature_body { get; set; }
        public virtual string template_name { get; set; }
        public virtual DateTime? scheduled_date { get; set; }
        public virtual DateTime? sent_date { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual EnumActive? active { get; set; }
    }
}
