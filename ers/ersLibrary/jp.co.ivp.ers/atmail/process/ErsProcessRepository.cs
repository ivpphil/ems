﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using System.Data;
using jp.co.ivp.ers.state;
using System.Collections;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.atmail.specification;
using jp.co.ivp.ers.atmail.process;

namespace jp.co.ivp.ers.atmail
{
    /// <summary>
    /// Provide methods to connect with am_process_t table in ersmail database. 
    /// Inherits ErsRepository<ErsProcess>
    /// </summary>
    public class ErsProcessRepository
        : ErsRepository<ErsProcess>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsProcessRepository()
            : base("am_process_t")
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsProcessRepository(ErsDatabase objDB)
            : base("am_process_t", objDB)
        {
        }

        public  IList<ErsProcess> FindMailSend(Criteria criteria)
        {
            var retList = new List<ErsProcess>();

            AtMailSearchSpecification mailSend = ErsFactory.ErsAtMailFactory.GetAtMailSearchSpecification();
            var list = mailSend.GetSearchData(criteria);

            if (list.Count == 0)
                return new List<ErsProcess>();

            foreach (var dr in list)
            {
                var ersPublishDateFactory = ErsFactory.ErsAtMailFactory;
                var mail = ersPublishDateFactory.GetErsProcessWithParameters(dr);
                retList.Add(mail);
            }

            return retList;
        }

        public int GetRecordCountMailSend(Criteria criteria)
        {
            AtMailSearchSpecification mailSend = ErsFactory.ErsAtMailFactory.GetAtMailSearchSpecification();
            return mailSend.GetCountData(criteria);
        }
    }
}
