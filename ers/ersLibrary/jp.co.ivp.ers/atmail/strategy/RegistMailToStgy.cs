﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.viewService;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.atmail.strategy
{
    public class RegistMailToStgy 
    {
        /// <summary>
        /// regist new at_process_t record and add mailto using memberlist
        /// @メールリスト新規作成処理
        /// </summary>
        public virtual int InsertNewMailList(EnumUpKind up_kind, IList<ErsMember> memberList)
        {
            var process = this.InsertNewProcess(up_kind);

            var sid = process.id.Value;

            this.AddMailList(sid, null, memberList);

            return sid;
        }

        /// <summary>
        /// regist new at_process_t record and add mailto using mailto
        /// @メールリスト新規作成処理
        /// </summary>
        public virtual int InsertNewMailList(EnumUpKind up_kind, IList<ErsMailTo> mailtoList)
        {
            var process = this.InsertNewProcess(up_kind);

            var sid = process.id.Value;

            this.AddMailList(sid, null, mailtoList);

            return sid;
        }

        /// <summary>
        /// Insert new process record
        /// </summary>
        /// <param name="up_kind">depends from where page came from (customer search, new list or new copy)</param>
        /// <returns>returns ErsProcess inserted</returns>
        protected virtual ErsProcess InsertNewProcess(EnumUpKind up_kind)
        {
            var processRepo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();

            //process_t
            var process = ErsFactory.ErsAtMailFactory.GetErsProcess();

            process.up_kind = up_kind;

            if (up_kind == EnumUpKind.UPLOAD)
                process.status = EnumAmProcessStatus.Uploading;
            else
                process.status = EnumAmProcessStatus.NotSend;

            process.active = EnumActive.Active;

            process.id = processRepo.GetNextSequence();
            processRepo.Insert(process);
            return process;
        }

        /// <summary>
        /// add mail to using memberlist
        /// @メールリスト追加挿入処理
        /// </summary>
        public virtual void AddMailList(int process_id, IList<ErsMember> memberList)
        {
            this.AddMailList(process_id, null, memberList);
        }

        /// <summary>
        /// add mail to with scheduled_date using memberlist
        /// @メールリスト追加挿入処理
        /// </summary>
        public virtual void AddMailList(int process_id, DateTime? scheduled_date, IList<ErsMember> memberList)
        {
            var mailRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var mailToStgy = ErsFactory.ErsAtMailFactory.GetCheckMailToExistStgy();

            foreach (var member in memberList)
            {
                if (!member.email.HasValue())
                {
                    //メールアドレスの登録がない場合はスキップ
                    continue;
                }

                var mailTo = this.GetNewMailToInstance(member, process_id, scheduled_date);

                //mailtoのデータ存在チェック
                if (!mailToStgy.CheckIfMailToExist(mailTo))
                {
                    mailRepo.Insert(mailTo);
                }
            }
        }

        /// <summary>
        /// add mail to with scheduled_date using mailto
        /// @メールリスト追加挿入処理
        /// </summary>
        public virtual void AddMailList(int process_id, DateTime? scheduled_date, IList<ErsMailTo> mailtoList)
        {
            var mailRepo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var mailToStgy = ErsFactory.ErsAtMailFactory.GetCheckMailToExistStgy();

            foreach (var mail in mailtoList)
            {
                var mailTo = this.GetNewMailtoInstance(mail, process_id, scheduled_date);

                //mailtoのデータ存在チェック
                if (!mailToStgy.CheckIfMailToExist(mailTo))
                {
                    mailRepo.Insert(mailTo);
                }
            }
        }

        /// <summary>
        /// Get new instance of ErsMailTo
        /// </summary>
        /// <param name="member">member list</param>
        /// <param name="process_id">process id selected</param>
        /// <param name="scheduled_date">inputted scheduled date</param>
        /// <returns>returns ErsMailTo inserted</returns>
        public virtual ErsMailTo GetNewMailToInstance(ErsMember member, int? process_id, DateTime? scheduled_date)
        {
            var mformat = member.mformat;

            if (mformat == null)
            {
                mformat = ErsFactory.ErsAtMailFactory.GetDetermineMformatStgy().WithDomain(member.email);
            }

            var mailTo = ErsFactory.ErsAtMailFactory.GetErsMailTo();

            mailTo.process_id = process_id;
            mailTo.mcode = member.mcode;
            mailTo.email = member.email;
            mailTo.lname = member.lname;
            mailTo.fname = member.fname;
            mailTo.etc1 = member.lnamek;
            mailTo.etc2 = member.fnamek;
            mailTo.mformat = mformat.Value;
            mailTo.scheduled_date = scheduled_date;
            mailTo.sent_flg = EnumSentFlg.NotSent;
            mailTo.active = EnumActive.Active;

            return mailTo;
        }
        /// <summary>
        /// Get new instance of ErsMailTo
        /// </summary>
        /// <param name="member">mailto list</param>
        /// <param name="process_id">process id selected</param>
        /// <param name="scheduled_date">inputted scheduled date</param>
        /// <returns>returns ErsMailTo inserted</returns>
        protected virtual ErsMailTo GetNewMailtoInstance(ErsMailTo mailtoValue, int? process_id, DateTime? scheduled_date)
        {
            var mformat = mailtoValue.mformat;

            if (mformat == null)
            {
                mformat = ErsFactory.ErsAtMailFactory.GetDetermineMformatStgy().WithDomain(mailtoValue.email);
            }

            var mailTo = ErsFactory.ErsAtMailFactory.GetErsMailTo();

            mailTo.process_id = process_id;
            mailTo.mcode = mailtoValue.mcode;
            mailTo.email = mailtoValue.email;
            mailTo.lname = mailtoValue.lname;
            mailTo.fname = mailtoValue.fname;
            mailTo.etc1 = mailtoValue.etc1;
            mailTo.etc2 = mailtoValue.etc2;
            mailTo.mformat = (EnumMformat)mformat;
            mailTo.scheduled_date = scheduled_date;
            mailTo.sent_flg = EnumSentFlg.NotSent;
            mailTo.active = EnumActive.Active;
            return mailTo;
        }

    }
}
