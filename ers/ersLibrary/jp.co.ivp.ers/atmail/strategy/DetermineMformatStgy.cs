﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.atmail
{
    /// <summary>
    /// This strategy checks whether the criteria for MailTo is existing or not.
    /// </summary>
    public class DetermineMformatStgy
    {
        /// <summary>
        /// for checking domain for mobile email based on setup.config
        /// </summary>
        /// <param name="email">email from am_mailto_t table</param>
        /// <returns>returns 1 if email is from pc, 2 if email from mobile/existing in setup.config</returns>
        public EnumMformat WithDomain(string email)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var domainMobile = setup.DomainMobile;

            if (!FindMatchesDomain(email,domainMobile))
                return EnumMformat.PC;
            else
                return EnumMformat.MOBILE;
        }

        /// <summary>
        /// find if email is existing on domain_mobile on setup.config
        /// </summary>
        /// <param name="email">email from am_mailto_t table</param>
        /// <param name="strDomain">list of domain mobile on setup.config</param>
        /// <returns>returns true if has match on domain mobile on setup.config, false if not existing</returns>
        static Boolean FindMatchesDomain(string email, string[] strDomain)
        {
            Boolean DomainMatch = false;
            if (!string.IsNullOrEmpty(email) && strDomain!=null)
            {
                for (int i = 0; i < strDomain.Length; i++)
                {
                    Match Match = Regex.Match(email, strDomain[i], RegexOptions.IgnoreCase);
                    if (Match.Success)
                    {
                        Console.WriteLine(Match.Groups[0]);
                        DomainMatch = true;
                    }
                }
            }
            return DomainMatch;
        }
    }
}
