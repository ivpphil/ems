﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.atmail
{
    /// <summary>
    /// This strategy checks whether the criteria for MailTo is existing or not.
    /// </summary>
    public class CheckMailToExistStgy
    {

        /// <summary>
        /// mailtoのデータ存在チェック
        /// </summary>
        /// <param name="mailTo">values of ErsMailTo use for finding of records using ErsAtMailCriteria</param>
        /// <param name="member_id">member id use for finding of records using ErsAtMailCriteria</param>
        /// <returns>returns true for existing record, returns false for no record</returns>
        public Boolean CheckIfMailToExist(ErsMailTo mailTo)
        {
            var repo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();

            criteria.process_id = mailTo.process_id;
            criteria.email = mailTo.email;
            criteria.active = EnumActive.Active;

            var cnt = repo.GetRecordCount(criteria);

            return (cnt > 0);

        }
    }
}
