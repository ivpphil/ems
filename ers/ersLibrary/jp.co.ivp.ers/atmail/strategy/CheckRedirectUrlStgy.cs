﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.atmail.strategy
{
    public class CheckRedirectUrlStgy
    {
        /// <summary>
        /// URLが現在のコンテキストに含まれているかを検証する
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public ValidationResult CheckUrl(string url, string fieldName)
        {
            UrlHelper urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            if (!urlHelper.IsLocalUrl(url))
            {
                return new ValidationResult(ErsResources.GetMessage("40004", ErsResources.GetFieldName(fieldName)));
            }

            return null;
        }
    }
}
