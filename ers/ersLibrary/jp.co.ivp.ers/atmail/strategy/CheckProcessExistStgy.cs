﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.atmail
{
    /// <summary>
    /// This strategy checks whether the criteria for Process is existing or not.
    /// </summary>
    public class CheckProcessExistStgy
    {


        /// <summary>
        /// processのデータ存在チェック
        /// </summary>
        /// <param name="sid">process id use for finding of records using ErsAtMailCriteria</param>
        /// <returns>returns true if the record is existing, returns false if no record found</returns>
        public virtual ValidationResult CheckProcessExist(int? process_id)
        {
            var criteria = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();

            criteria.id = process_id;
            criteria.active = EnumActive.Active;

            return this.CheckCount(criteria);
        }

        /// <summary>
        /// stemp_mail_idを持つprocessのデータ存在チェック
        /// </summary>
        /// <param name="process_id"></param>
        /// <returns></returns>
        public virtual ValidationResult CheckProcessAndStepmailExist(int? process_id)
        {
            var criteria = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();

            criteria.id = process_id;
            criteria.step_mail_id_not_equal = null;
            criteria.active = EnumActive.Active;

            return this.CheckCount(criteria);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        private ValidationResult CheckCount(process.ErsProcessCriteria criteria)
        {
            var repo = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var cnt = repo.GetRecordCount(criteria);

            if (cnt == 0)
            {
                return new ValidationResult(ErsResources.GetMessage("40000")); ;
            }

            return null;
        }
    }
}
