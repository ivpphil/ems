﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.atmail.strategy
{
    public class CheckEncodedMailAddressStgy
    {
        public ValidationResult CheckEncodedMailAddress(string enc_email, string fieldName)
        {
            var encoder = ErsFactory.ersUtilityFactory.getErsEncryption();
            string email;
            try
            {
                email = encoder.HexDecode(enc_email);
            }
            catch(Exception ex)
            {
                return new ValidationResult(ex.Message, new[] { fieldName });
            }

            var checkResult = new ErsCheckUniversal().Check(CHK_TYPE.EMailAddr, email, fieldName, null, null, null, null, null, true, false, false, false, null);
            if (!string.IsNullOrEmpty(checkResult))
            {
                return new ValidationResult(checkResult, new[] { fieldName });
            }

            return null;
        }
    }
}
