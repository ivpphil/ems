﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using System.Data;
using jp.co.ivp.ers.state;
using System.Collections;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
//using jp.co.ivp.ers.coupon;


namespace jp.co.ivp.ers.atmail
{
    /// <summary>
    /// Provide methods to connect with set_mst table in ersmail database. 
    /// Inherits ErsRepository<ErsProcess>
    /// </summary>
    public class ErsAmSetupRepository
        : ErsRepository<ErsAmSetup>
    {

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsAmSetupRepository()
            : base("am_setup_t")
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsAmSetupRepository(ErsDatabase objDB)
            : base("am_setup_t", objDB)
        {
        }
    }
}
