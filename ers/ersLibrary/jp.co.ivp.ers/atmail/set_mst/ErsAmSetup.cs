﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.atmail
{
    /// <summary>
    /// Hold values from set_mst table in ersmail database.
    /// Inherits ErsRepositoryEntity class.
    /// </summary>/// 
    public class ErsAmSetup : ErsRepositoryEntity 
    {
        public override int? id { get; set; }
        public virtual string admin_email { get; set; }
        public virtual string p_email { get; set; }
        public virtual string r_email { get; set; }
        public virtual int? site_id { get; set; }
    }
}
