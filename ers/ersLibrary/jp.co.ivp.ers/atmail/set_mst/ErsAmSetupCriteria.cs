﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.atmail.set_mst
{
    /// <summary>
    /// use for am_setup_t filtering
    /// </summary>
    public class ErsAmSetupCriteria
        : Criteria
    {
        //filter records using am_setup_t.id
        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("am_setup_t.id", value, Operation.EQUAL));
            }
        }

        public string r_email
        {
            set
            {
                Add(Criteria.GetCriterion("am_setup_t.r_email", value, Operation.EQUAL));
            }
        }

        //ordering records by am_setup_t.id
        public void SetOrderByID(OrderBy orderBy)
        {
            AddOrderBy("am_setup_t.id", orderBy);
        }
    }
}
