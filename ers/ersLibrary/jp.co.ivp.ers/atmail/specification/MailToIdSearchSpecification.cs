﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.atmail.specification
{
    public class MailToIdSearchSpecification
        : SearchSpecificationBase
    {
        /// <summary>
        /// SQL文
        /// </summary>
        protected string SQL
        {
            get
            {
                return " FROM am_mailto_t"
                        + " INNER JOIN am_process_t"
                        + " ON am_mailto_t.process_id = am_process_t.id";
            }
        }

        protected override string GetSearchDataSql()
        {
            return "SELECT am_mailto_t.id"
                    + this.SQL;
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(*) AS " + countColumnAlias + " "
                    + this.SQL;
        }
    }
}
