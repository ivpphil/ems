﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.atmail.specification
{
    public class AtMailSearchSpecification
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return "SELECT DISTINCT am_process_t.* "
                 + "FROM am_process_t "
                 + "INNER JOIN am_mailto_t "
                 + " ON am_process_t.id = am_mailto_t.process_id ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(DISTINCT(am_process_t.id)) AS " + countColumnAlias + " "
                 + "FROM am_process_t "
                 + "INNER JOIN am_mailto_t "
                 + " ON am_process_t.id = am_mailto_t.process_id ";
        }
    }
}
