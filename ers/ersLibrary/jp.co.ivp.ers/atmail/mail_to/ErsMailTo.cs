﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.atmail
{
    /// <summary>
    /// Hold values from mailto table in ersmail database.
    /// Inherits ErsRepositoryEntity class.
    /// </summary>
    public class ErsMailTo : ErsRepositoryEntity 
    {
        /// <summary>
        /// Primary Key ID
        /// </summary>
        public override int? id { get; set; }
        public virtual long? process_id { get; set; }
        public virtual string mcode { get; set; }
        public virtual string email { get; set; }
        public virtual string lname { get; set; }
        public virtual string fname { get; set; }
        public virtual string etc1 { get; set; }
        public virtual string etc2 { get; set; }
        public virtual EnumMformat? mformat { get; set; }
        public virtual EnumSentFlg? sent_flg { get; set; }
        public virtual DateTime? scheduled_date { get; set; }
        public virtual DateTime? sent_date { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual EnumActive? active { get; set; }
    }
}
