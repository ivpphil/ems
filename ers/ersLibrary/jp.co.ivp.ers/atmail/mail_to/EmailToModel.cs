﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.sendmail.mass_send;

namespace jp.co.ivp.ers.atmail
{
    public class EmailToModel : ErsModelBase, IErsAtmailModelBase
    {
        public int? process_id { get; set; }
        public virtual string mcode { get; set; }
        public virtual string email { get; set; }
        public virtual string lname { get; set; }
        public virtual string fname { get; set; }
        public virtual string etc1 { get; set; }
        public virtual string etc2 { get; set; }
    }
}
