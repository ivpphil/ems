﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.atmail.mail_to
{
    /// <summary>
    /// use for am_mailto_t filtering
    /// </summary>
    public class ErsMailToCriteria
        : Criteria
    {
        //filter records using am_mailto_t.process_id
        public long? process_id
        {
            set
            {
                Add(Criteria.GetCriterion("am_mailto_t.process_id", value, Operation.EQUAL));
            }
        }
        //filter records using am_mailto_t.id
        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("am_mailto_t.id", value, Operation.EQUAL));
            }
        }
        //filter records using am_mailto_t.id
        public IEnumerable<int> id_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("am_mailto_t.id", value));
            }
        }
        //filter records using am_mailto_t.mcode
        public string mcode
        {
            set
            {
                Add(Criteria.GetCriterion("am_mailto_t.mcode", value, Operation.EQUAL));
            }
        }
        //filter records using am_mailto_t.sent_flg
        public EnumSentFlg? sent_flg
        {
            set
            {
                Add(Criteria.GetCriterion("am_mailto_t.sent_flg", (int)value, Operation.EQUAL));
            }
        }

        public DateTime? scheduled_date_less_or_null
        {
            set
            {
                var scheduled_date_less = Criteria.GetCriterion("am_mailto_t.scheduled_date", value, Operation.LESS_THAN);
                var scheduled_date_null = Criteria.GetCriterion("am_mailto_t.scheduled_date", null, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { scheduled_date_less, scheduled_date_null }));
            }
        }

        //filter records using am_mailto_t.active
        public EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("am_mailto_t.active", (int)value, Operation.EQUAL));
            }
        }
        //filter records using am_mailto_t.email
        public virtual string email
        {
            set
            {
                Add(Criteria.GetCriterion("am_mailto_t.email", value, Operation.EQUAL));
            }
        }

        public void SetOrderById(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("am_mailto_t.id", orderBy);
        }

        public int? idFrom
        {
            set
            {
                Add(Criteria.GetCriterion("am_mailto_t.id",value,Operation.GREATER_EQUAL));
            }
        }
        public int? idTo
        {
            set
            {
                Add(Criteria.GetCriterion("am_mailto_t.id", value, Operation.LESS_EQUAL));
            }
        }

    }
}
