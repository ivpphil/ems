﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using System.Data;
using jp.co.ivp.ers.state;
using System.Collections;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.atmail.mail_to;
//using jp.co.ivp.ers.coupon;


namespace jp.co.ivp.ers.atmail
{
    /// <summary>
    /// Provide methods to connect with am_mailto_t table in ersmail database. 
    /// Inherits ErsRepository<ErsMailTo>
    /// </summary>
    public class ErsMailToRepository
        : ErsRepository<ErsMailTo>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsMailToRepository()
            : base("am_mailto_t")
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsMailToRepository(ErsDatabase objDB)
            : base("am_mailto_t", objDB)
        {
        }
    }
}
