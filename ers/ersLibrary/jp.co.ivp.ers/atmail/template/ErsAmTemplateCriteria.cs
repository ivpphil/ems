﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.atmail.set_template
{
    /// <summary>
    /// use for am_template_t filtering
    /// </summary>
    public class ErsAmTemplateCriteria
        : Criteria
    {
        //filter records using am_template_t.id
        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("am_template_t.id", value, Operation.EQUAL));
            }
        }
        //ordering records by am_template_t.id
        public void SetOrderByTemplateID(OrderBy orderBy)
        {
            AddOrderBy("am_template_t.id", orderBy);
        }
        //filter records using am_template_t.active
        public EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("am_template_t.active", (int)value, Operation.EQUAL));
            }
        }

        internal void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("am_template_t.id", orderBy);
        }
    }
}
