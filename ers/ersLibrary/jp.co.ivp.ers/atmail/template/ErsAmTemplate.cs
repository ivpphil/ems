﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.atmail
{
    /// <summary>
    /// Hold values from template_t table in ersmail database.
    /// Inherits ErsRepositoryEntity class.
    /// </summary>/// 
    public class ErsAmTemplate : ErsRepositoryEntity 
    {
        public override int? id { get; set; }
        public virtual string subject { get; set; }
        public virtual string template_name { get; set; }
        public virtual string feature_body { get; set; }
        public virtual string body { get; set; }
        public virtual string html_body { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual EnumActive? active { get; set; }
    }
}
