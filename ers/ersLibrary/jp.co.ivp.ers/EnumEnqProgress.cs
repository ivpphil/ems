﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumEnqProgress
    {
        /// <summary>
        /// 0: None
        /// </summary>
        None = 0,
        /// <summary>
        /// 1: Open
        /// </summary>
        Open = 1,
        /// <summary>
        /// 2:Close
        /// </summary>
        Close
    }
}
