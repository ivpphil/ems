﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.basket
{
    /// <summary>
    /// represents searched conditions for properties that are in ErsMixGroup class.
    /// Inherits Criteria class.
    /// </summary>
    public class ErsMixGroupCriteria : Criteria
    {

        /// <summary>
        /// sets criteria for ID with the condition of id = value
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for mixed_group_code with the condition of mixed_group_code = value
        /// </summary>
        public virtual string mixed_group_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("mixed_group_code", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for mixed_group_name with the condition of mixed_group_name = value
        /// </summary>
        public virtual string mixed_group_name
        {
            set
            {
                this.Add(Criteria.GetCriterion("mixed_group_name", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// get's sort order by id
        /// </summary>
        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("mixed_group_t.id", orderBy);
        }
    }
}
