﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.basket.strategy
{
    /// <summary>
    /// Validate if item price purchased with order type subscription is correct based on member ranked. Return true if all prices are correct.
    /// </summary>
    public class ValidateRegularBasketRecordPriceStgy
    {
        public bool ValidatePrice(int? memberRank, ErsBasket basket)
        {
            if (basket.objRegularBasketRecord.Count > 0)
            {
                foreach (var item in basket.objRegularBasketRecord.Values)
                {
                    var service = ErsFactory.ersOrderFactory.GetManageRegularPatternService(item.send_ptn.Value);
                    var merchandise = service.GetMerchandise(basket.ransu, item.scode, item, memberRank);
                    if (merchandise.price != item.price)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
