﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.basket.strategy
{
    /// <summary>
    /// 該当乱数のバスケットをクリアする
    /// </summary>
    public class EmptyBasketStrategy
    {
        public void EmptyBasket(string ransu)
        {
            var basketRepository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            var basketCriteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
            basketCriteria.ransu = ransu;
            basketRepository.Delete(basketCriteria);
        }

    }
}
