﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.basket.strategy
{
    /// <summary>
    /// Validate if item price purchased with order type usually is correct based on member ranked. Return true if all prices are correct.
    /// </summary>
    public class ValidateBasketRecordPriceStgy
    {
        public bool ValidatePrice(int? memberRank, ErsBasket basket)
        {
            if (basket.objBasketRecord.Count > 0)
            {
                foreach (var item in basket.objBasketRecord.Values)
                {
                    var merchandise = ErsFactory.ersBasketFactory.GetErsBaskRecordWithScode(basket.ransu, item.scode, memberRank);

                    if (merchandise == null)
                    {
                        continue;
                    }

                    if (merchandise.price != item.price)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
