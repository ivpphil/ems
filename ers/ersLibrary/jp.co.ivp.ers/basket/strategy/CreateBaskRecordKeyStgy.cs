﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.basket.strategy
{
    public class CreateBaskRecordKeyStgy
    {
        /// <summary>
        /// Gets Key of ErsBasket.objBasketRecord
        /// </summary>
        /// <returns></returns>
        public string GetKey(ErsBaskRecord merchandise)
        {
            var regular_detail_id = string.Empty;
            if (merchandise.regular_detail_id != null)
            {
                regular_detail_id = string.Join("_", merchandise.regular_detail_id);
            }
            //regular_detail_idを追加する
            return merchandise.scode
               + "_" + merchandise.price
               + "_" + regular_detail_id
               + "_" + merchandise.next_date
               + "_" + merchandise.send_ptn
               + "_" + merchandise.ptn_interval_month
               + "_" + merchandise.ptn_interval_week
               + "_" + merchandise.ptn_interval_day
               + "_" + merchandise.ptn_day
               + "_" + merchandise.ptn_weekday;
        }
    }
}
