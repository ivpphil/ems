﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.order.strategy;

namespace jp.co.ivp.ers.basket.strategy
{
    public class AddRegularOrderToCartStrategy
    {
        public void AddMerchandise(ErsBasket basket, ErsBaskRecord merchandise, int amount, EnumOrderType orderType, EnumOrderStatusType? status = null)
        {
            var calcService = ErsFactory.ersOrderFactory.GetErsCalcService();

            //Checks the parameters.
            if (string.IsNullOrEmpty(merchandise.ransu))
            {
                throw new ArgumentException("The ransu is not initialized.");
            }

            var regularPatternService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(merchandise.send_ptn.Value);
            var next_date = regularPatternService.CalculateFirstTime(merchandise, DateTime.Now);
            if (next_date.HasValue)
            {
                merchandise.next_date = CalcNextSendStgy.CalculateWeekendOperation(next_date.Value, EnumWeekendOperation.NONE);
            }

            var recordKey = ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(merchandise);

            if (basket.objRegularBasketRecord.ContainsKey(recordKey))
            {
                //remove item if the merchandise already exists in the basket.
                basket.RemoveRegular(recordKey);
            }

            calcService.calcBaskRecord(merchandise, amount);

            if (status == null)
            {
                merchandise.order_status = EnumOrderStatusType.NEW_ORDER;
            }
            else
            {
                merchandise.order_status = status.Value;
            }

            merchandise.order_type = orderType;

            merchandise.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            basket.objRegularBasketRecord.Add(recordKey, merchandise);

            //再計算
            calcService.calcBasket(basket);
        }
    }
}