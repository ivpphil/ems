﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.basket.strategy
{
    /// <summary>
    /// implements AddToCartStrategy and adds value in ErsMerchandiseInBasket
    /// </summary>
    public class OrdinaryAddToCartStgy
    {
        /// <summary>
        /// Adds value in ErsMerchandiseInBasket and computes the tax and total amount
        /// </summary>
        /// <param name="basket">values in ErsBasket to be use from adding records in s_master_t and bask_t table</param>
        /// <param name="merchandise">Values in ERsMerchandiseInBasket to be use from adding records in s_master_t and bask_t table</param>
        /// <param name="amount">total amount, use for calculating the price value from price_t and the amount given</param>
        public virtual void AddMerchandise(ErsBasket basket, ErsBaskRecord merchandise, int amount, EnumOrderType orderType, EnumOrderStatusType? status = null)
        {
            var calcService = ErsFactory.ersOrderFactory.GetErsCalcService();

            //Checks the parameters.
            if (string.IsNullOrEmpty(merchandise.ransu))
            {
                throw new ArgumentException("The ransu is not initialized.");
            }

            calcService.calcBaskRecord(merchandise, amount);
            if (status == null)
            {
                merchandise.order_status = EnumOrderStatusType.NEW_ORDER;
            }
            else
            {
                merchandise.order_status = status.Value;
            }

            merchandise.order_type = orderType;

            merchandise.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            basket.objBasketRecord.Add(ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(merchandise), merchandise);

            //再計算
            calcService.calcBasket(basket);

        }
    }
}
