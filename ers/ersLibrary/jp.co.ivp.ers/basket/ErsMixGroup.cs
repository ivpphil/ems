﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.basket.specification;

namespace jp.co.ivp.ers.basket
{
    /// <summary>
    /// Hold values from mixed_group_t table.
    /// Inherits ErsRepositoryEntity class.
    /// </summary>
    public class ErsMixGroup
        :ErsRepositoryEntity
    {

        public ErsMixGroup()
        {
        }

        /// <summary>
        /// Unique key ID
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 混在制御コード
        /// </summary>
        public virtual string mixed_group_code { get; set; }

        /// <summary>
        /// 混在メッセージ
        /// </summary>
        public virtual string mixed_group_name { get; set; }
    }
}
