﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.basket.specification;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.campaign.campaign;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.basket
{
    /// <summary>
    /// Hold values from bask_t, s_master_t, g_master_t and price_t tables.
    /// Inherits ErsMerchandise class.
    /// </summary>
    public class ErsMerchandiseInBasket
        : ErsMerchandiseOld, IManageRegularPatternDatasource
    {
        protected virtual ErsMerchandiseOld merchandise { get; set; }

        public virtual string ransu { get; set; }

        /// <summary>
        /// amount per item 
        /// </summary>
        public virtual int amount { get; protected set; }

        /// <summary>
        /// total amount of an item
        /// </summary>
        public virtual int total { get; protected set; }
        public virtual EnumOrderStatusType? order_status { get; set; }

        // Defines the fields which defined in bask_t as new properties.

        /// <summary>
        /// Unique key ID
        /// </summary>
        public new int? id { get; set; }

        /// <summary>
        /// Group product code
        /// </summary>
        public new string gcode { get; set; }

        /// <summary>
        /// Group product name
        /// </summary>
        public new string gname { get; set; }

        /// <summary>
        /// commodity group name
        /// </summary>
        public new string m_gname { get; set; }

        public new DateTime? date_from { get; set; }

        public new DateTime? date_to { get; set; }

        /// <summary>
        /// sales method
        /// </summary>
        public new short? s_sale_ptn { get; set; }

        /// <summary>
        /// Display or hide stock
        /// </summary>
        public new short? stock_flg { get; set; }

        /// <summary>
        /// Setting one stock
        /// </summary>
        public new int? stock_set1 { get; set; }

        /// <summary>
        /// Setting two stocks
        /// </summary>
        public new int? stock_set2 { get; set; }

        /// <summary>
        /// Category 1
        /// </summary>
        public new int[] cate1 { get; set; }

        public new int[] cate2 { get; set; }

        public new int[] cate3 { get; set; }

        public new int[] cate4 { get; set; }

        public new int[] cate5 { get; set; }

        /// <summary>
        /// Product recommendation 1
        /// </summary>
        public new string recommend1 { get; set; }

        public new string recommend2 { get; set; }

        public new string recommend3 { get; set; }

        public new string recommend4 { get; set; }

        public new string recommend5 { get; set; }

        /// <summary>
        /// meta element, title attribute
        /// </summary>
        public new string metatitle { get; set; }

        public new string metadescription { get; set; }

        public new string metawords { get; set; }

        public virtual string metaalt { get; set; }

        /// <summary>
        /// size information
        /// </summary>
        public new string size_info { get; set; }

        /// <summary>
        /// Produc description (for Product details)
        /// </summary>
        public new string description { get; set; }

        /// <summary>
        /// Portable product description
        /// </summary>
        public new string m_description { get; set; }
        /// <summary>
        /// list of searched products
        /// </summary>
        public new string description_for_list { get; set; }

        /// <summary>
        /// Product code
        /// </summary>
        public new string scode { get; set; }

        /// <summary>
        /// Accord Installation code
        /// </summary>
        public new string jancode { get; set; }

        /// <summary>
        /// Product name
        /// </summary>
        public new string sname { get; set; }

        /// <summary>
        /// Portable product name
        /// </summary>
        public new string m_sname { get; set; }

        public new string attribute1 { get; set; }

        public new string attribute2 { get; set; }

        /// <summary>
        /// Display order
        /// </summary>
        public new short? disp_order { get; set; }

        /// <summary>
        /// insert time
        /// </summary>
        public new DateTime? intime { get; set; }

        public new string mixed_group_code { get; set; }

        /// <summary>
        /// maximum purchase count
        /// </summary>
        public new int? max_purchase_count { get; set; }

        /// <summary>
        /// flag lists
        /// </summary>
        public new EnumDisp_list_flg? disp_list_flg { get; set; }

        /// <summary>
        /// List price
        /// </summary>
        public new int? price2 { get; set; }

        public new int? point { get; set; }

        public virtual int? old_amount { get; set; }

        
        /// <summary>
        /// Gets sold out flg
        /// </summary>
        public override EnumSoldoutFlg? soldout_flg
        {
            get
            {
                if (merchandise == null)
                    return 0;

                //常にs_master_tから取得
                return merchandise.soldout_flg;
            }
        }

        /// <summary>
        /// Get's merchandise details and prices from s_master_t and price_t tables using ErsMerchandiseFactory based on the given parameter
        /// </summary>
        /// <param name="dictionary">based on GetPropertiesAsDictionary from InitWithEntity</param>
        public override void OverwriteWithParameter(IDictionary<string, object> dictionary)
        {
            base.OverwriteWithParameter(dictionary);
            String dic_scode = Convert.ToString(dictionary["scode"]);
            if (dic_scode == "")
            {
                this.merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseOldWithGcode(gcode);
            }
            else
            {
                this.merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseOldWithScode(dic_scode);
            }

            if (merchandise != null)
                LoadDetailAndPrice(merchandise, dic_scode);
        }

        /// <summary>
        /// Get's merchandise details and prices from ErsMerchandise and setting the current product code
        /// </summary>
        /// <param name="merchandise">values from ErsMerchandise</param>
        /// <param name="scode">scode (Product code)</param>
        public virtual void LoadDetailAndPrice(ErsMerchandiseOld merchandise, string scode)
        {
            this.merchandiseDetails = merchandise.merchandiseDetails;
            this.merchandisePrices = merchandise.merchandisePrices;
            this.setCurrentScode(scode);
        }

        public virtual DateTime? next_date_base { get; set; }
        public virtual DateTime? next_date { get; set; }
        public virtual EnumSendPtn send_ptn { get; set; }
        public virtual short? ptn_interval_month { get; set; }
        public virtual short? ptn_interval_week { get; set; }
        public virtual short? ptn_interval_day { get; set; }
        public virtual short? ptn_day { get; set; }
        public virtual DayOfWeek? ptn_weekday { get; set; }

        public virtual bool IsMinDeliveryDate
        {
            get
            {
                return this.next_date == null;
            }
        }

        /// <summary>
        /// Unit price
        /// </summary>
        public new virtual int? price
        {
            get
            {
                //regular order
                if (send_ptn != EnumSendPtn.NORMAL)
                    return this.CurrentPrice.regular_first_price;

                //ordinary order
                else
                    return _price;
            }
            set
            {
                _price = value;
            }
        }
        private int? _price;

        public virtual new int? regular_price { get; set; }

        public virtual int? regular_total { get { return regular_price * amount; } }

        public virtual int? regular_detail_id { get; set; }

        public virtual string d_no { get; set; }

        public virtual int? sendtime { get; set; }

        public virtual string w_sendtime
        {
            get
            {
                if (this.sendtime != null)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId((int)this.sendtime);
                }
                return null;
            }
        }

        public virtual string ccode
        {
            get
            {
                if (string.IsNullOrEmpty(this._ccode))
                {
                    return this._ccode;
                }
                return this._ccode.ToUpper();
            }
            set
            {
                this._ccode = value;
            }
        }
        private string _ccode;

        public virtual EnumOrderType? order_type { get; set; }
    }
}
