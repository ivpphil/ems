﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.basket
{
    public class ErsBaskRecord
        : ErsRepositoryEntity, IManageRegularPatternDatasource
    {
        public override int? id { get; set; }
        public virtual string ransu { get; set; }
        public virtual int? site_id { get; set; }
        public virtual string gcode { get; set; }
        public virtual string gname { get; set; }
        public virtual string m_gname { get; set; }
        public virtual DateTime? date_from { get; set; }
        public virtual DateTime? date_to { get; set; }
        public virtual EnumSalePatternType? s_sale_ptn { get; set; }
        public virtual EnumStockFlg? stock_flg { get; set; }
        public virtual int? stock_set1 { get; set; }
        public virtual int? stock_set2 { get; set; }
        public virtual string scode { get; set; }
        public virtual string jancode { get; set; }
        public virtual string sname { get; set; }
        public virtual string m_sname { get; set; }
        public virtual string attribute1 { get; set; }
        public virtual string attribute2 { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual string mixed_group_code { get; set; }
        public virtual int? max_purchase_count { get; set; }
        public virtual int? price { get; set; }
        public virtual int? price2 { get; set; }
        public virtual int? point { get; set; }
        public virtual int amount { get; set; }
        public virtual int total { get; set; }
        public virtual DateTime? next_date { get; set; }
        public virtual EnumSendPtn? send_ptn { get; set; }
        public short? ptn_interval_day { get; set; }
        public short? ptn_interval_month { get; set; }
        public short? ptn_day { get; set; }
        public short? ptn_interval_week { get; set; }
        public DayOfWeek? ptn_weekday { get; set; }
        public virtual EnumCarriageCostType? carriage_cost_type { get; set; }
        public virtual EnumPluralOrderType? plural_order_type
        {
            get
            {
                if (this.merchandise != null)
                {
                    return this.merchandise.plural_order_type;
                }
                return null;
            }
        }
        public virtual EnumSetFlg? set_flg { get; set; }
        public virtual EnumDocBundlingFlg doc_bundling_flg { get; set; }
        public virtual EnumDelvMethod? deliv_method { get; set; }
        public virtual string shipping_sname { get; set; }
        public virtual EnumOrderStatusType? order_status { get; set; }
        public virtual string d_no { get; set; }
        public virtual int? ds_id { get; set; }
        public virtual string ccode { get; set; }
        public virtual int? old_amount { get; set; }
        public virtual int? regular_price { get; set; }
        public virtual int[] regular_detail_id { get; set; }
        public virtual EnumOrderType? order_type { get; set; }
        public virtual string cts_sname { get; set; }

        /// <summary>
        /// モール販売フラグ [Mall sale flag]
        /// </summary>
        public virtual EnumOnOff? h_mall_flg
        {
            get
            {
                if (this.merchandise != null)
                {
                    return this.merchandise.h_mall_flg;
                }
                return null;
            }
        }

        //以下はテーブルには存在しないが処理上ひつようなカラム。
        public virtual int? regular_first_price { get; set; }
        public virtual DateTime? next_date_base { get; set; }
        public virtual int? sendtime { get; set; }
        public virtual int? regular_total { get { return this.regular_price * this.amount; } }

        public virtual bool IsOrderUpdate { get; set; }

        public virtual int? cost_price { get; set; }
        public virtual string supplier_code { get; set; }
        public virtual EnumWhOrderType wh_order_type { get; set; }

        public virtual int? member_rank { get; set; }

        public virtual EnumStockReservation? stock_reservation { get; set; }

        public virtual string maker_scode { get; set; }

        public string w_sendtime
        {
            get
            {
                if (this.sendtime != null)
                    return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId(this.sendtime);
                else
                    return string.Empty;

            }
        }

        /// <summary>
        /// Gets stock minus the amount using GetStock from ObtainMerchandiseStockStgy of ErsStockFactory
        /// </summary>
        public virtual int? stock
        {
            get
            {
                //常にs_master_tから取得。amountを引いた数
                if (merchandise == null)
                    return 0;

                var stock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetObtainMerchandiseStockStgy().GetStock(merchandise.scode, merchandise.stock);

                if (!this.IsOrderUpdate)
                {
                    return stock - amount;
                }
                else
                {
                    //CTS EditMode
                    var old_amount = this.old_amount== null? 0:this.old_amount;
                    return stock - amount + old_amount;
                }
            }
        }

        /// <summary>
        /// Gets sold out flg
        /// </summary>
        public virtual EnumSoldoutFlg? soldout_flg
        {
            get
            {
                if (merchandise == null)
                    return 0;

                //常にs_master_tから取得
                return merchandise.soldout_flg;
            }
        }

        private ErsMerchandise _merchandise { get; set; }
        public ErsMerchandise merchandise
        {
            get
            {
                if (string.IsNullOrEmpty(this.scode))
                {
                    return null;
                }

                if (this._merchandise != null)
                {
                    return this._merchandise;
                }

                var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(this.scode, this.member_rank);

                if (merchandise == null)
                {
                    return null;
                }
                this._merchandise = merchandise;
                return this._merchandise;
            }
        }

        public void CalcRegularFirstDate(EnumWeekendOperation weekend_operation)
        {
            var regularPatternService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(this.send_ptn.Value);

            this.next_date_base = regularPatternService.CalculateFirstTime(this, DateTime.Now);

            if (this.next_date == null)
            {
                return;
            }

            this.next_date = regularPatternService.CalculateActual(this, this.next_date_base.Value, weekend_operation);
        }

        public string ptn_day_name
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.PtnDay, EnumCommonNameColumnName.namename, this.ptn_day);
            }
        }

        public string ptn_weekday_name
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename, (int?)this.ptn_weekday);
            }
        }

        public string ptn_interval_name
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsRegularOrderViewService().GetDeliveryTerm(this);
            }
        }
    }
}
