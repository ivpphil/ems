﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket.strategy;
using jp.co.ivp.ers.basket.specification;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.basket
{
    /// <summary>
    /// カート関連クラス用Factory.
    /// Provide methods that are related in basket.
    /// </summary>
    public class ErsBasketFactory
    {
        /// <summary>
        /// バスケットクラスを取得する
        /// </summary>
        /// <returns>returns instance of ErsBasket</returns>
        public virtual ErsBasket GetErsBasket()
        {
            return new ErsBasket();
        }

        /// <summary>
        /// バスケットクラスを取得する
        /// </summary>
        /// <returns>returns instance of ErsBasket</returns>
        public virtual ErsLPBasket GetErsLPBasket()
        {
            return new ErsLPBasket();
        }

        /// <summary>
        /// 
        /// </summary>
        protected static ErsBaskRecordRepository _ErsBaskRecordRepository
        {
            get
            {
                return (ErsBaskRecordRepository)ErsCommonContext.GetPooledObject("_ErsBaskRecordRepository");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_ErsBaskRecordRepository", value);
            }
        }

        /// <summary>
        /// カートクラス用Repositoryを取得する
        /// </summary>
        /// <returns>returns instance of _ErsBasketRepository</returns>
        public virtual ErsBaskRecordRepository GetErsBaskRecordRepository()
        {
            if (_ErsBaskRecordRepository == null)
                _ErsBaskRecordRepository = new ErsBaskRecordRepository();
            return _ErsBaskRecordRepository;
        }

        /// <summary>
        /// 商品にカートを投入する処理をおこなうStratetyを取得する。
        /// </summary>
        /// <returns>returns instance of OrdinaryAddToCartStgy</returns>
        public virtual OrdinaryAddToCartStgy GetOrdinaryAddToCartStgy()
        {
            return new OrdinaryAddToCartStgy();
        }

        /// <summary>
        /// カート投入可能か
        /// </summary>
        /// <returns>returns instance of CartInSpecification</returns>
        public virtual CartInSpecification GetCartInSpecification()
        {
            return new CartInSpecification();
        }

        /// <summary>
        /// 伝票編集可能か
        /// </summary>
        /// <returns>returns instance of CartInSpecification</returns>
        public virtual OrderEditSpecification GetOrderEditSpecification()
        {
            return new OrderEditSpecification();
        }

        public virtual ErsBaskRecord GetErsBaskRecord()
        {
            return new ErsBaskRecord();
        }

        public virtual ErsBaskRecord GetErsBaskRecordWithParameter(Dictionary<string, object> parameters)
        {
            var objBask = this.GetErsBaskRecord();
            objBask.OverwriteWithParameter(parameters);
            return objBask;
        }

        /// <summary>
        /// 商品番号を元にバスケットに特化した商品クラスを取得する
        /// </summary>
        /// <param name="ransu">random alphanumeric characters</param>
        /// <param name="scode">scode (Product code)</param>
        /// <returns>returns list of records of basket when there's record found, returns null if it's not existing.</returns>
        public virtual ErsBaskRecord GetErsBaskRecordWithScode(string ransu, string scode, int? member_rank)
        {
            var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(scode, member_rank);

            if (merchandise == null)
            {
                return null;
            }

            if (ErsFactory.ersMerchandiseFactory.GetOnCampaignSpecification().IsSatisfiedBy(merchandise.p_date_from, merchandise.p_date_to))
            {
                merchandise.price = merchandise.sale_price;
            }

            //use campaign_point by point_campaign_from and point_campaign_to
            if (ErsFactory.ersMerchandiseFactory.GetOnPointCampaignSpecification().IsSatisfiedBy(merchandise.point_campaign_from, merchandise.point_campaign_to,DateTime.Now))
            {                
                    merchandise.point = merchandise.campaign_point;
            }

            var dictionary = merchandise.GetPropertiesAsDictionary();
            if (dictionary.ContainsKey("id"))
                dictionary.Remove("id");

            var emBaskRecord = this.GetErsBaskRecordWithParameter(dictionary);
            emBaskRecord.ransu = ransu;
            emBaskRecord.intime = null;
            emBaskRecord.member_rank = member_rank;
            return emBaskRecord;
        }

        public virtual ErsBaskRecord GetErsBaskRecordWithId(int? id)
        {
            var repository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            var criteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
            criteria.id = id;

            var listBaskRecord = repository.Find(criteria);

            if (listBaskRecord.Count != 1)
            {
                return null;
            }

            return listBaskRecord.First();
        }

        /// <summary>
        /// Obtain instance of ErsBasketCriteria class
        /// </summary>
        /// <returns>returns instance of ErsBasketCriteria</returns>
        public virtual ErsBaskRecordCriteria GetErsBaskRecordCriteria()
        {
            return new ErsBaskRecordCriteria();
        }

        /// <summary>
        /// Obtain instance of MixedCartInSpecification class
        /// </summary>
        /// <returns>returns instance of MixedCartInSpecification</returns>
        public virtual MixedCartInSpecification GetMixedCartInSpecification()
        {
            return new MixedCartInSpecification();
        }

        /// <summary>
        /// Obtain instance of MaxPurchaseCartInSpecification class
        /// </summary>
        /// <returns>returns instance of MaxPurchaseCartInSpecification</returns>
        public virtual MaxPurchaseCartInSpecification GetMaxPurchaseCartInSpecification()
        {
            return new MaxPurchaseCartInSpecification();
        }

        /// <summary>
        /// Obtain instance of CarriageFreeSpecification class
        /// </summary>
        /// <returns>returns instance of CarriageFreeSpecification</returns>
        public virtual CarriageFreeSpecification GetCarriageFreeSpecification()
        {
            return new CarriageFreeSpecification();
        }

        /// <summary>
        /// Obtain instance of BasketSearchSpecification class
        /// </summary>
        /// <returns>returns instance of BasketSearchSpecification</returns>
        internal virtual BasketSearchSpecification GetBasketSearchSpecification()
        {
            return new BasketSearchSpecification();
        }

        /// <summary>
        /// Obtain instance of ErsMixGroup class 
        /// </summary>
        /// <returns>returns instance of ErsMixGroup</returns>
        public ErsMixGroup GetErsMixGroup()
        {
            return new ErsMixGroup();
        }

        /// <summary>
        /// Gets list of searched records according to the criteria using id
        /// </summary>
        /// <param name="id">mixed_group_t.id use for finding of record using ErsMixGroupCriteria</param>
        /// <returns>returns data from mixed_group_t table, returns null if not existing</returns>
        public virtual ErsMixGroup GetErsMixGroupWithId(int id)
        {
            var criteria = this.GetErsMixGroupCriteria();
            var repository = this.GetErsMixGroupRepository();

            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count != 1)
                return null;
            else
                return list[0];
        }

        /// <summary>
        /// Obtain instance of ErsMixGroupRepository class 
        /// </summary>
        /// <returns>returns instance of ErsMixGroupRepository</returns>
        public virtual ErsMixGroupRepository GetErsMixGroupRepository()
        {
            return new ErsMixGroupRepository();
        }

        /// <summary>
        /// Obtain instance of ErsMixGroupCriteria class 
        /// </summary>
        /// <returns>returns instance of ErsMixGroupCriteria</returns>
        public virtual ErsMixGroupCriteria GetErsMixGroupCriteria()
        {
            return new ErsMixGroupCriteria();
        }

        /// <summary>
        /// Get records from mixed_group_t using OverwriteWithParameter function of ErsMixGroup
        /// </summary>
        /// <param name="data">values saved in dictionary</param>
        /// <returns>return values from ErsMixGroup</returns>
        internal virtual ErsMixGroup GetErsMixGroupWithParameters(Dictionary<string, object> parameters)
        {
            var mixGroup = this.GetErsMixGroup();
            mixGroup.OverwriteWithParameter(parameters);
            return mixGroup;
        }

        /// <summary>
        /// Get records from mixed_group_t using OverwriteWithModel function of ErsMixGroup
        /// </summary>
        /// <param name="data">values saved in model</param>
        /// <returns>return values from ErsMixGroup</returns>        
        public virtual ErsMixGroup GetErsMixGroupWithModel(ErsModelBase model)
        {
            var mixGroup = this.GetErsMixGroup();
            mixGroup.OverwriteWithModel(model);
            return mixGroup;
        }

        public virtual AddRegularOrderToCartStrategy GetAddRegularOrderToCartStrategy()
        {
            return new AddRegularOrderToCartStrategy();
        }

        public virtual RegularShippingFreeSpec GetRegularShippingFreeSpec()
        {
            return new RegularShippingFreeSpec();
        }

        public virtual CarriageCostTypeFreeSpec GetCarriageCostTypeFreeSpec()
        {
            return new CarriageCostTypeFreeSpec();
        }

        public virtual EmptyBasketStrategy GetEmptyBasketStrategy()
        {
            return new EmptyBasketStrategy();
        }

        public virtual CreateBaskRecordKeyStgy GetCreateBaskRecordKeyStgy()
        {
            return new CreateBaskRecordKeyStgy();
        }

        public virtual BasketDelivMethodMailMaxPurchaseSpec GetBasketDelivMethodMailMaxPurchaseSpec()
        {
            return new BasketDelivMethodMailMaxPurchaseSpec();
        }

        public virtual LpShippingFreeSpec GetLpShippingFreeSpec()
        {
            return new LpShippingFreeSpec();
        }

        public virtual CountPluralOrderAmountSpec GetCountPluralOrderAmountSpec()
        {
            return new CountPluralOrderAmountSpec();
        }

        public virtual ValidateBasketRecordPriceStgy GetValidateBasketRecordPriceStgy()
        {
            return new ValidateBasketRecordPriceStgy();
        }
        public virtual ValidateRegularBasketRecordPriceStgy GetValidateRegularBasketRecordPriceStgy()
        {
            return new ValidateRegularBasketRecordPriceStgy();
        }
    }
}
