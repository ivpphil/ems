﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.basket
{
    /// <summary>
    /// Provide methods to connect with bask_t table. 
    /// Inherits ErsRepository<ErsMerchandiseInBasket>
    /// </summary>
    public class ErsBaskRecordRepository
        : ErsRepository<ErsBaskRecord>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsBaskRecordRepository()
            : base("bask_t")
        {
        }

        /// <summary>
        /// Gets list of records that have been searched in ErsMerchandiseInBasket group of tables from criteria values using BasketSearchSpecification
        /// </summary>
        /// <param name="criteria">set of property conditions found in ErsBasketCriteria</param>
        /// <returns>returns list of records in ErsMerchandiseInBasket based on the criterias given</returns>
        public override IList<ErsBaskRecord> Find(Criteria criteria)
        {
            var specBasketSearch = ErsFactory.ersBasketFactory.GetBasketSearchSpecification();

            List<ErsBaskRecord> lstRet = new List<ErsBaskRecord>();

            // 検索
            var list = specBasketSearch.GetSearchData(criteria);

            foreach (var item in list)
            {
                var em = ErsFactory.ersBasketFactory.GetErsBaskRecordWithParameter(item);
                lstRet.Add(em);
            }
            return lstRet;
        }

        /// <summary>
        /// gets record count in ErsMerchandiseInBasket group of tables using BasketSearchSpecification
        /// </summary>
        /// <param name="criteria">set of property conditions found ErsBasketCriteria </param>
        /// <returns>returns record count based on the criterias given</returns>      
        public override long GetRecordCount(Criteria criteria)
        {
            var specBasketSearch = ErsFactory.ersBasketFactory.GetBasketSearchSpecification();
            return specBasketSearch.GetCountData(criteria);
        }
    }
}
