﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.basket
{
    public class ErsLPBasket
        : ErsBasket
    {
        public override EnumCarriageFreeStatus carriageFree
        {
            get
            {
                if (ErsFactory.ersBasketFactory.GetLpShippingFreeSpec().IsSatisfiedBy(this))
                    return EnumCarriageFreeStatus.CARRIAGE_FREE;

                return base.carriageFree;
            }
        }

        public EnumUse? carriage_free_flg { get; set; }

        public int? lp_carriage_free { get; set; }

        protected override ErsBasket GetErsBasket()
        {
            var lp_basket = ErsFactory.ersBasketFactory.GetErsLPBasket();
            lp_basket.lp_carriage_free = this.lp_carriage_free;
            lp_basket.carriage_free_flg = this.carriage_free_flg;

            return lp_basket;
        }
    }
}
