﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.basket.specification
{
    public class CountPluralOrderAmountSpec
        : ISpecificationForSQL
    {
        public int GetCountData(string scode, string ransu)
        {
            this.scode = scode;
            this.ransu = ransu;

            var result = ErsRepository.SelectSatisfying(this);

            if (result.Count == 0)
            {
                return 0;
            }

            return Convert.ToInt32(result.First()["result"]);
        }

        public string asSQL()
        {
            return @"
                SELECT
                    COALESCE(bask_t.amount, 0) AS result
                FROM
                    bask_t
		        WHERE ransu = '" + ransu + @"' AND bask_t.scode = '" + scode + @"'
                ";
        }

        public string scode { get; set; }

        public string ransu { get; set; }
    }
}
