﻿using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.basket.specification
{
    public class OrderEditSpecification
    {
        /// <summary>
        /// 伝票編集可能か
        /// </summary>
        /// <param name="scode">scode</param>
        public virtual bool IsSatisfiedBy(string scode)
        {
            var marchandise = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(scode);

            //商品マスタに存在するか
            if (marchandise != null)
            {
                //存在する場合、編集不可フラグを立てない
                return false;
            }

            return true;
        }
    }
}
