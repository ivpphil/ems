﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.basket.specification
{
    public class MaxPurchaseCartInSpecification
    {
        public virtual void IsSatisfiedBy(ErsBaskRecord merchandise, ErsBasket eb)
        {           
            if (eb.GetType() == typeof(ErsBasket))
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();

                if (merchandise.max_purchase_count != 0 && merchandise.amount > merchandise.max_purchase_count || merchandise.amount > setup.common_max_purchase_count)
                {
                    if (merchandise.max_purchase_count < setup.common_max_purchase_count && merchandise.max_purchase_count != 0)
                    {
                        throw new ErsException("20201", merchandise.sname, merchandise.max_purchase_count);
                    }

                    throw new ErsException("20201", merchandise.sname, setup.common_max_purchase_count);
                }
            }
        }
    }
}