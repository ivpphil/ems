﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.basket.specification
{
    /// <summary>
    /// Search and count records on bask_t joined with s_master_t and g_master_t.
    /// Implements ISearchSpecification.
    /// </summary>
    public class BasketSearchSpecification
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return " SELECT DISTINCT bask_t.* FROM bask_t "
                    + "LEFT JOIN s_master_t ON bask_t.scode = s_master_t.scode "
                    + "LEFT JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(DISTINCT bask_t.id) AS " + countColumnAlias + " FROM bask_t "
                    + "LEFT JOIN s_master_t ON bask_t.scode = s_master_t.scode "
                    + "LEFT JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode ";
        }
    }
}
