﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.basket.specification
{
    /// <summary>
    /// Checks ErsMerchandiseInBasket and ErsBasket if it's satisfied by the conditions.
    /// Called by CartInSpecification class
    /// </summary>
    public class MixedCartInSpecification
    {
        /// <summary>
        /// カートの中にある商品のmixed_group_codeがすべて同じか
        /// </summary>
        /// <param name="em">values in ErsMerchandiseInBasket</param>
        /// <param name="eb">values in ErsBasket</param>
        /// <returns>returns if satistied, if not, throw error exception</returns>
        public virtual void IsSatisfiedBy(ErsBaskRecord em, ErsBasket eb)
        {
            if (eb.objBasketRecord.Count == 0 && eb.objRegularBasketRecord.Count == 0)
            {
                return;
            }

            ErsBaskRecord cartEm;
            if (eb.objBasketRecord.Count == 0)
            {
                cartEm = eb.objRegularBasketRecord.Values.First();
            }
            else
            {
                cartEm = eb.objBasketRecord.Values.First();
            }

            if (em.mixed_group_code != cartEm.mixed_group_code)
            {
                if (em.doc_bundling_flg != EnumDocBundlingFlg.ON)
                {
                    throw new ErsException("20204"
                        , this.GetMixedMessage(em.mixed_group_code, em.sname)
                        , this.GetMixedMessage(cartEm.mixed_group_code, cartEm.sname));
                }
            }
        }

        private string GetMixedMessage(string mixed_group_code, string scode)
        {
            var repositroy = ErsFactory.ersBasketFactory.GetErsMixGroupRepository();
            var criteria = ErsFactory.ersBasketFactory.GetErsMixGroupCriteria();
            criteria.mixed_group_code = mixed_group_code;
            var listMixecMessage = repositroy.Find(criteria);

            if (listMixecMessage.Count == 0)
            {
                return scode;
            }

            return listMixecMessage.First().mixed_group_name;
        }
    }
}
