﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.basket.specification
{

    /// <summary>
    /// Checks ErsMerchandiseInBasket and ErsBasket if it's satisfied by the conditions
    /// </summary>
    /// 
    public class CartInSpecification
    {
        /// <summary>
        /// カート投入可能か
        /// </summary>
        /// <param name="em">values in ErsMerchandiseInBasket</param>
        /// <param name="eb">values in ErsBasket</param>
        public virtual void IsSatisfiedBy(ErsBaskRecord merchandise, ErsBasket eb, bool isReload = false)
        {
            if (!eb.IsOrderUpdate && !ErsFactory.ersMerchandiseFactory.GetChecksActivenessOfMerchandiseSpec().Check(merchandise.merchandise))
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                throw new ErsException(string.Empty, null, 
                    ErsResources.GetMessage("20006", merchandise.scode) + ErsResources.GetMessage("20007", setup.sec_url + setup.cart_url));
            }

            if (merchandise.plural_order_type != EnumPluralOrderType.NoLimit && merchandise.amount > (int?)merchandise.plural_order_type)
            {
                throw new ErsException("20209", new[] { merchandise.sname });
            }

            var amount = merchandise.amount;
            if (eb.IsOrderUpdate)
            {
                //ds_master_tから、merchandise.ds_idをｷｰに取得
                var oldRecord = ErsFactory.ersOrderFactory.GetErsOrderRecordWithId(merchandise.ds_id);
                if (oldRecord != null)
                {
                    amount = amount - oldRecord.GetAmount();
                }
            }

            if (ErsFactory.ersMerchandiseFactory.GetOutOfStockAtBasketSpecification().IsSatisfiedBy(merchandise.ransu, merchandise.scode, amount, merchandise.stock, merchandise.soldout_flg, merchandise.set_flg, eb.IsOrderUpdate))
            {
                throw new ErsException("20200", merchandise.sname);
            }

            ErsFactory.ersBasketFactory.GetMixedCartInSpecification().IsSatisfiedBy(merchandise, eb);

            if (eb.IsOrderUpdate)
            {
                ErsFactory.ersBasketFactory.GetMaxPurchaseCartInSpecification().IsSatisfiedBy(merchandise, eb);
            }

            ErsFactory.ersOrderFactory.GetValidatePluralOrderTypeStgy().CheckForCartIn(merchandise, eb.mcode, eb.ransu, merchandise.d_no, isReload, eb.IsOrderUpdate);
        }
    }
}
