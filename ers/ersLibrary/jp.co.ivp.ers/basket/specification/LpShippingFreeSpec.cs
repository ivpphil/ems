﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.basket.specification
{
    public class LpShippingFreeSpec
    {
        public bool IsSatisfiedBy(ErsBasket basket)
        {
            if (basket is ErsLPBasket)
            {
                var lpbasket = basket as ErsLPBasket;

                if (IsSatisfiedBy(lpbasket.carriage_free_flg, lpbasket.subtotal, lpbasket.lp_carriage_free))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsSatisfiedBy(EnumUse? carriage_free_flg, int subtotal, int? lp_carriage_free)
        {
            return carriage_free_flg == EnumUse.Use && subtotal >= lp_carriage_free;
        }
    }
}
