﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.basket.specification
{
    /// <summary>
    /// 送料に関するステータス判定を行う
    /// </summary>
    public class CarriageFreeSpecification
    {

        /// <summary>
        /// Gets CarriageFree status based on the conditions given
        /// </summary>
        /// <param name="total">total amount</param>
        /// <returns>returns status of CarriageFree</returns>
        public virtual EnumCarriageFreeStatus GetCarriageFreeStatus(int total)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            if (setup.free == null || setup.free == 0)
            {
                return EnumCarriageFreeStatus.FREE_SETTING_NOTHING;
            }
            else if (total >= (int)setup.free)
            {
                return EnumCarriageFreeStatus.CARRIAGE_FREE;
            }
            else
            {
                return EnumCarriageFreeStatus.CARRIAGE_NOT_FREE;
            }
        }
        
    }
}
