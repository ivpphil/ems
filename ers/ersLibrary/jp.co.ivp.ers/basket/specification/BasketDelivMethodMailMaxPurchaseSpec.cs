﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.basket.specification
{
    public class BasketDelivMethodMailMaxPurchaseSpec
    {
        public bool IsSpecifiedByExistedMail(string ransu, string scode, bool IsRegular = false)
        {
            var repository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            var criteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();

            var criteriaOrderType = this.GetCriteriaByOrderType(ransu, IsRegular);

            criteria.ransu = ransu;
            criteria.deliv_method = EnumDelvMethod.Mail;
            criteria.scode_not_in = new[] { scode };

            var jonCriteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();

            jonCriteria.Add(Criteria.JoinWithOR(new[] { criteria, criteriaOrderType }));

            return repository.GetRecordCount(jonCriteria) > 0;
        }

        /// <summary>
        /// specifies the scode with EnumDelvMethod.Mail that are not in the given scode (parameter)
        /// </summary>
        /// <param name="ransu">random alphanumeric characters</param>
        /// <param name="scode">scode (Product code)</param>
        /// <param name="IsRegular">check if it's regular basket</param>
        /// <returns>returns scode that has deliv_method = "2" (mail shipping)</returns>
        public string scodeSpecifiedByExistedMail(string ransu, string scode, bool IsRegular = false)
        {
            var repository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            var criteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();

            var criteriaOrderType = this.GetCriteriaByOrderType(ransu, IsRegular);

            criteria.ransu = ransu;
            criteria.deliv_method = EnumDelvMethod.Mail;
            criteria.scode_not_in = new[] { scode };

            var jonCriteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();

            jonCriteria.Add(Criteria.JoinWithOR(new[] { criteria, criteriaOrderType }));

            var record = repository.FindSingle(jonCriteria);

            //return empty string if no record found
            return (record==null?string.Empty:record.scode);
        }

        public bool IsSpecifiedByDelvMethodType(string ransu, ErsMerchandise merchandise)
        {
            if (merchandise != null)
            {
                var repository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
                var criteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();

                criteria.ransu = ransu;
                criteria.scode_not_in = new[] { merchandise.scode };

                if (repository.GetRecordCount(criteria) > 0)
                {
                    return merchandise.deliv_method == EnumDelvMethod.Mail;
                }
            }

            return false;
        }


        private ErsBaskRecordCriteria GetCriteriaByOrderType(string ransu, bool IsRegular)
        {
            var criteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
            criteria.ransu = ransu;
            criteria.deliv_method = EnumDelvMethod.Mail;

            if (IsRegular)
            {
                criteria.order_typeOrNull = EnumOrderType.Usually;   
            }
            else
            {
                criteria.order_type = EnumOrderType.Subscription;
            }

            return criteria;
        }
    }
}
