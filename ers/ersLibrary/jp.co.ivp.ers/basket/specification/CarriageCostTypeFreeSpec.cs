﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.basket.specification
{
    public class CarriageCostTypeFreeSpec
    {
        public bool IsSatisfiedBy(IEnumerable<ErsOrderRecord> orderRecords)
        {
            if (orderRecords != null && orderRecords.Count() == 0)
            {
                return false;
            }

            foreach (var record in orderRecords)
            {
                if (record.carriage_cost_type != EnumCarriageCostType.Free && record.amount > 0)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsSatisfiedBy(IEnumerable<ErsBaskRecord> orderRecords)
        {
            if (orderRecords == null || orderRecords.Count() == 0)
            {
                return false;
            }

            foreach (var record in orderRecords)
            {
                if (record.carriage_cost_type != EnumCarriageCostType.Free && record.amount > 0)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsSatisfiedBy(string scode)
        {
            if (!scode.HasValue())
            {
                return false;
            }

            var record = ErsFactory.ersMerchandiseFactory.GetActiveErsMerchandiseWithScode(scode, null);

            if (record.carriage_cost_type != EnumCarriageCostType.Free)
            {
                return false;
            }

            return true;
        }
    }
}