﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.basket.specification
{
    public class RegularShippingFreeSpec
    {
        public bool IsSatisfiedBy(IEnumerable<ErsOrderRecord> orderRecords)
        {
            foreach (var record in orderRecords)
            {
                if (record.order_type == EnumOrderType.Subscription && record.amount > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsSatisfiedBy(ErsBasket basket)
        {
            foreach (var record in basket.objRegularBasketRecord.Values)
            {
                if (record.order_type == EnumOrderType.Subscription && record.amount > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}