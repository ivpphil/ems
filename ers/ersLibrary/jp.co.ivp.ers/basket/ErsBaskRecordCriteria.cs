﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.basket
{
    /// <summary>
    /// represents searched conditions for properties that are in ErsBasket class.
    /// Inherits Criteria class.
    /// </summary>
    public class ErsBaskRecordCriteria
        : Criteria
    {

        /// <summary>
        /// sets criteria for ID with the condition of bask_t.id = value
        /// </summary>
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("bask_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for ransu with the condition of bask_t.ransu = value
        /// </summary>
        public virtual string ransu
        {
            set
            {
                this.Add(Criteria.GetCriterion("bask_t.ransu", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for scode with the condition of bask_t.scode = value
        /// </summary>
        public virtual string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("bask_t.scode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for active with the condition of g_master_t.active = value
        /// </summary>
        public virtual EnumActive g_active
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.active", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for active with the condition of s_master_t.active = value
        /// </summary>
        public virtual EnumActive s_active
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.active", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for gcode with the condition of bask_t.gcode = value
        /// </summary>
        public virtual string gcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("bask_t.gcode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// get's sort order of id
        /// </summary>
        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("id", orderBy);
        }

        public virtual EnumCarriageCostType carriage_cost_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("bask_t.carriage_cost_type", value, Operation.EQUAL));
            }
        }

        public virtual EnumPluralOrderType plural_order_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("bask_t.plural_order_type", value, Operation.EQUAL));
            }
        }


        public virtual EnumDelvMethod? deliv_method
        {
            set
            {
                this.Add(Criteria.GetCriterion("bask_t.deliv_method", value, Operation.EQUAL));
            }
        }

        public virtual string[] scode_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("bask_t.scode", value));
            }
        }

        public virtual string[] scode_not_in
        {
            set
            {
                this.Add(Criteria.GetNotInClauseCriterion("bask_t.scode", value));
            }
        }


        public int amount_not_equals
        {
            set
            {
                this.Add(Criteria.GetCriterion("bask_t.amount", value, Operation.NOT_EQUAL));
            }
        }

        internal void SetOrderByNext_date(OrderBy orderBy)
        {
            this.AddOrderBy("next_date", orderBy);
        }

        public EnumSendPtn? send_ptn { set { this.Add(Criteria.GetCriterion("send_ptn", value, Operation.EQUAL)); } }

        public string ccode { set { this.Add(Criteria.GetCriterion("ccode", value.ToUpper(), Operation.EQUAL)); } }

        public string ccode_not_equal { set { this.Add(Criteria.GetCriterion("ccode", value.ToUpper(), Operation.NOT_EQUAL)); } }

        /// <summary>
        /// 対応区分
        /// </summary>
        public virtual EnumOrderStatusType[] order_status_not_in
        {
            set
            {
                this.Add(Criteria.GetNotInClauseCriterion("bask_t.order_status", value.Cast<int>()));
            }
        }

        public EnumOrderType? order_type { set { this.Add(Criteria.GetCriterion("order_type", value, Operation.EQUAL)); } }

        public EnumOrderType? order_typeOrNull
        {
            set
            {
                this.Add(Criteria.JoinWithOR(new[] {
                     Criteria.GetCriterion("order_type", value, Operation.EQUAL),
                     Criteria.GetCriterion("order_type", null, Operation.EQUAL),
                }));
            }
        }

        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("bask_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("bask_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }


    }
}
