﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.basket.specification;
using jp.co.ivp.ers.basket.strategy;
using jp.co.ivp.ers.state;

namespace jp.co.ivp.ers.basket
{
    /// <summary>
    /// Hold values s_master_t,g_master_t, price_t and bask_t table.
    /// </summary>
    public class ErsBasket
    {
        protected bool initialized = false;

        protected internal ErsBasket()
        {
            objBasketRecord = new Dictionary<string, ErsBaskRecord>();
            objRegularBasketRecord = new Dictionary<string, ErsBaskRecord>();
        }

        /// <summary>
        /// ransu
        /// </summary>
        public virtual string ransu { get; set; }

        /// <summary>
        /// member code
        /// </summary>
        public string mcode { get; set; }

        /// <summary>
        /// gets the total amount from the basket
        /// </summary>
        public virtual int amounttotal
        {
            get
            {
                var ret = 0;
                foreach (var record in this.objBasketRecord.Values)
                {
                    ret += record.amount;
                }
                return ret;
            }
        }

        /// <summary>
        /// gets the total amount of regular product from the basket
        /// </summary>
        public virtual int regular_amounttotal
        {
            get
            {
                var ret = 0;
                foreach (var record in this.objRegularBasketRecord.Values)
                {
                    ret += record.amount;
                }
                return ret;
            }
        }

        /// <summary>
        /// sub total of an item
        /// </summary>
        public virtual int subtotal { get; set; }

        /// <summary>
        /// tax of an item
        /// </summary>
        public virtual int tax { get; set; }

        /// <summary>
        /// total amount
        /// </summary>
        public virtual int total { get; set; }

        public virtual int order_status { get; set; }

        public virtual int old_amount { get; set; }

        public virtual int regular_subtotal { get; set; }
        
        public virtual int regular_tax { get; set; }
        
        public virtual int regular_total { get; set; }

        public virtual int regular_subtotal_next { get; set; }

        public virtual int regular_tax_next { get; set; }

        public virtual int regular_total_next { get; set; }

        /// <summary>
        /// 配送希望時間
        /// </summary>
        /// <returns></returns>
        public virtual int? sendtime { get; set; }

        /// <summary>
        /// バスケットの中の商品
        /// </summary>
        public virtual Dictionary<string, ErsBaskRecord> objBasketRecord { get; protected set; }

        /// <summary>
        /// バスケットの中の定期商品
        /// </summary>
        public virtual Dictionary<string, ErsBaskRecord> objRegularBasketRecord { get; protected set; }
        
        //public virtual EnumUse carriage_free_flg { get; set; }

        /// <summary>
        /// 送料に関するステータス
        /// </summary>
        public virtual EnumCarriageFreeStatus carriageFree
        {
            get
            {
                if (ErsFactory.ersBasketFactory.GetRegularShippingFreeSpec().IsSatisfiedBy(this)
                    || ErsFactory.ersBasketFactory.GetCarriageCostTypeFreeSpec().IsSatisfiedBy(objBasketRecord.Values))
                {
                    return EnumCarriageFreeStatus.CARRIAGE_FREE;
                }
                else
                {
                    return ErsFactory.ersBasketFactory.GetCarriageFreeSpecification().GetCarriageFreeStatus(this.subtotal + this.regular_subtotal);
                }
            }
        }

        /// <summary>
        /// 送料無料になる金額
        /// </summary>
        public virtual int free
        {
            get
            {
                if (this.carriageFree == EnumCarriageFreeStatus.FREE_SETTING_NOTHING)
                {
                    return 0;
                }
                Setup setup = ErsFactory.ersUtilityFactory.getSetup();
                return (int)setup.free;
            }
        }

        /// <summary>
        /// 送料無料までの金額
        /// </summary>
        public virtual int free_get
        {
            get
            {
                if (this.carriageFree == EnumCarriageFreeStatus.FREE_SETTING_NOTHING)
                {
                    return 0;
                }
                Setup setup = ErsFactory.ersUtilityFactory.getSetup();
                return (int)setup.free - (subtotal + regular_subtotal);
            }
        }

        /// <summary>
        /// Prepares session state before putting merchandise into basket.
        /// </summary>
        public virtual void PrepareSession()
        {
            var session = (ISession)ErsContext.sessionState;
            EnumUserState state = session.getUserState();

            if (state == EnumUserState.NON_ASSIGNED_RANSU
                || state == EnumUserState.ASSIGNED_RANSU_BUT_INVALID)
            {
                session.getNewRansu();
            }
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="mcode">member code use for initializing the basket</param> 
        /// <param name="ransu">random alphanumeric characters, use for initializing the basket</param>
        public virtual void Init(string mcode, string ransu, EnumOrderType? orderType = null)
        {
            if (this.initialized)
                return;

            this.ransu = ransu;
            this.mcode = mcode;

            InitializeBasket(orderType);

            initialized = true;
        }

        public virtual void InitOrderBakset(ErsOrder order)
        {
            if (this.initialized)
                return;

            this.ransu = order.ransu;
            this.mcode = order.mcode;

            InitializeOrderBasket(order);

            initialized = true;
        }

        /// <summary>
        /// initialize basket data
        /// </summary>
        protected virtual void InitializeBasket(EnumOrderType? orderType)
        {
            var criteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
            criteria.ransu = ransu;
            if (orderType != null)
            {
                criteria.order_type = orderType;
            }
            criteria.SetOrderByNext_date(db.Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderById(db.Criteria.OrderBy.ORDER_BY_ASC);
            var repository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            var list = repository.Find(criteria);
            foreach (var item in list)
            {
                item.ransu = ransu;

                AddToBasket(item, item.amount, item.order_status);
            }
        }

        /// <summary>
        /// If this value is true, then supress the error of no stock.
        /// </summary>
        public bool IsOrderUpdate { get; set; }

        protected virtual void InitializeOrderBasket(ErsOrder order)
        {
            ErsFactory.ersBasketFactory.GetEmptyBasketStrategy().EmptyBasket(order.ransu);

            var criteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
            criteria.d_no = order.d_no;
            criteria.SetOrderById(db.Criteria.OrderBy.ORDER_BY_ASC);
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var list = repository.Find(criteria);

            foreach (var item in list)
            {
                var dictionary = item.GetPropertiesAsDictionary();
                if (dictionary.ContainsKey("id"))
                    dictionary.Remove("id");

                var merchandise = ErsFactory.ersBasketFactory.GetErsBaskRecordWithParameter(dictionary);
                merchandise.ransu = order.ransu;
                merchandise.d_no = order.d_no;
                merchandise.ds_id = item.id;
                merchandise.old_amount = merchandise.amount;
                merchandise.regular_detail_id = item.regular_detail_id;

                var g_master = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(item.gcode);
                if (g_master != null)
                {
                    merchandise.deliv_method = g_master.deliv_method;
                }

                this.AddToBasket(merchandise, item.GetAmount(), item.order_status);

                var basketRepository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
                basketRepository.Insert(merchandise);
            }
        }

        /// <summary>
        /// ErsMerchandiseInBasketをカートに追加する
        /// </summary>
        /// <param name="scode">value of scode (Product code) use for finding the records using GetErsMerchandiseInBasketWithScode</param>
        /// <param name="amount">amount</param>
        public virtual void Add(ErsBaskRecord merchandise, int amount, EnumOrderStatusType? status = null)
        {
            merchandise.amount = amount;

            ErsFactory.ersBasketFactory.GetCartInSpecification().IsSatisfiedBy(merchandise, this, false);

            if (!CheckIfExist(merchandise))
            {
                this.AddToBasket(merchandise, amount, status);

                var basketRepository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
                basketRepository.Insert(merchandise);
            }
        }

        private bool CheckIfExist(ErsBaskRecord merchandise)
        {
            if (this.IsOrderUpdate)
            {
                return this.objBasketRecord.Where((value) => value.Value.order_type == merchandise.order_type && value.Value.scode == merchandise.scode).Count() > 0;
            }

            return this.objBasketRecord.ContainsKey(ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(merchandise));
        }

        /// <summary>
        /// ErsMerchandiseInBasketをカートに追加する
        /// </summary>
        /// <param name="merchandise">values in ErsMerchandiseInBasket</param>
        /// <param name="amount">amount</param>
        public virtual void AddToBasket(ErsBaskRecord merchandise, int amount, EnumOrderStatusType? status = null)
        {
            var orderType =  (merchandise.send_ptn == null || merchandise.send_ptn == EnumSendPtn.NORMAL)? EnumOrderType.Usually: EnumOrderType.Subscription;
            if (!this.IsOrderUpdate && orderType == EnumOrderType.Subscription)
            {
                //regular order
                var cartInStrategy = ErsFactory.ersBasketFactory.GetAddRegularOrderToCartStrategy();

                cartInStrategy.AddMerchandise(this, merchandise, amount, orderType, status);
            }
            else
            {
                var cartInStrategy = ErsFactory.ersBasketFactory.GetOrdinaryAddToCartStgy();

                cartInStrategy.AddMerchandise(this, merchandise, amount, orderType, status);
            }
        }

        /// <summary>
        /// 指定したscodeを削除
        /// </summary>
        /// <param name="key">scode use to remove records in the basket</param>
        public virtual void Remove(string key)
        {
            this.Remove(key, this.objBasketRecord);
        }

        public virtual void RemoveRegular(string del_regular_key)
        {
            this.Remove(del_regular_key, this.objRegularBasketRecord);
        }

        /// <summary>
        /// 指定したscodeを削除
        /// </summary>
        /// <param name="key">scode</param>
        /// <param name="records">dictionary values taken from ErsMerchandiseInBasket</param>
        public virtual void Remove(string key, Dictionary<string, ErsBaskRecord> records)
        {
            ErsBaskRecord emb;
            if (records.ContainsKey(key))
            {
                emb = records[key];
                records.Remove(key);
                var repository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
                repository.Delete(emb);
            }

            //再計算
            ErsFactory.ersOrderFactory.GetErsCalcService().calcBasket(this);
        }

        /// <summary>
        /// カート個数追加/変更/削除
        /// </summary>
        /// <param name="up_amount_list">list of up_amount of cart</param>
        public virtual void ReCompute(string mcode, Dictionary<string, int?> up_amount_list, Dictionary<string, EnumOrderStatusType> order_status_list = null)
        {
            var cartInStrategy = ErsFactory.ersBasketFactory.GetOrdinaryAddToCartStgy();

            var message = string.Empty;

            //作成し直し
            foreach (string up_key in up_amount_list.Keys)
            {
                if (!this.objBasketRecord.ContainsKey(up_key))
                    continue;

                var merchandise = this.objBasketRecord[up_key];

                if (((ISession)ErsContext.sessionState).getUserState() == EnumUserState.LOGIN)
                {
                    var memberRank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().Retrieve(mcode);
                    // recompute when rank has value.
                    if (memberRank.HasValue)
                    {
                        var item = ErsFactory.ersBasketFactory.GetErsBaskRecordWithScode(this.GetErsBasket().ransu, merchandise.scode, memberRank);

                        if(item != null)
                        {
                            if (merchandise.order_type == EnumOrderType.Subscription)
                            {
                                var repo = ErsFactory.ersOrderFactory.GetOrderSerachSpecification();
                                var crit = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

                                crit.scode = merchandise.scode;
                                crit.mcode = mcode;
                                crit.order_type = EnumOrderType.Subscription;
                                crit.order_status_not_in = new[] { EnumOrderStatusType.CANCELED };
                                if (repo.GetCountData(crit) > 1)
                                {
                                    merchandise.price = item.regular_price;
                                }
                                else
                                {
                                    merchandise.price = item.regular_first_price;
                                }
                            }
                            else
                            {
                                merchandise.price = item.price;
                            }
                        }

                        merchandise.member_rank = memberRank;
                    }
                }
                Remove(up_key);

                EnumOrderStatusType? status = null;
                if (order_status_list != null)
                {
                    status = order_status_list[up_key];
                }
                else
                {
                    status = merchandise.order_status;
                }

                cartInStrategy.AddMerchandise(this, merchandise, up_amount_list[up_key]??0,merchandise.order_type.GetValueOrDefault(EnumOrderType.Usually), status);

                try
                {
                    merchandise.IsOrderUpdate = this.IsOrderUpdate;
                    ErsFactory.ersBasketFactory.GetCartInSpecification().IsSatisfiedBy(merchandise, this);
                    merchandise.IsOrderUpdate = false;
                }
                catch (ErsException ex)
                {
                    message += ex.Message + Environment.NewLine;
                    continue;
                }

                merchandise.order_status = status;

                var basketRepository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
                basketRepository.Insert(merchandise);
            }

            if (!string.IsNullOrEmpty(message))
            {
                throw new ErsException("universal", message + ErsResources.GetMessage("20207", ErsFactory.ersUtilityFactory.getSetup().nor_url));
            }
        }

        /// <summary>
        /// Recompute Regular order
        /// </summary>
        /// <param name="listModel"></param>
        public virtual void ReComputeRegular(IEnumerable<IManageRegularPatternDatasource> listModel, int? member_rank)
        {
            var message = string.Empty;
            if (listModel != null)
            {
                //作成し直し
                foreach (var datasource in listModel)
                {
                    if (!((ErsModelBase)datasource).IsValid)
                    {
                        continue;
                    }

                    var parameters = datasource.GetPropertiesAsDictionary();
                    var regular_key = (string)parameters["regular_key"];
                    var up_amount = (int)parameters["amount"];
                    string scode = null;
                    if (this.objRegularBasketRecord.ContainsKey(regular_key))
                    {
                        scode = this.objRegularBasketRecord[regular_key].scode;
                    }

                    EnumOrderStatusType? order_status = null;
                    if (parameters.ContainsKey("order_status"))
                        order_status = (EnumOrderStatusType)parameters["order_status"];

                    if (!this.objRegularBasketRecord.ContainsKey(regular_key))
                        continue;

                    var repository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
                    var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
                    criteria.scode = scode;

                    if (repository.GetRecordCount(criteria) != 0)
                    {
                        message += ReComputeRegularRecord(datasource, regular_key, up_amount, order_status, member_rank);
                    }
                }

                if (!string.IsNullOrEmpty(message))
                {
                    throw new ErsException("universal", message + ErsResources.GetMessage("20207", ErsFactory.ersUtilityFactory.getSetup().nor_url));
                }
            }
        }

        /// <summary>
        /// Recompute Regular order( one record )
        /// </summary>
        /// <param name="datasource"></param>
        /// <param name="regular_key"></param>
        /// <param name="up_amount"></param>
        /// <returns></returns>
        public string ReComputeRegularRecord(IManageRegularPatternDatasource datasource, string regular_key, int up_amount, EnumOrderStatusType? order_status, int? member_rank)
        {
            var cartInStrategy = ErsFactory.ersBasketFactory.GetAddRegularOrderToCartStrategy();

            var up_scode = this.objRegularBasketRecord[regular_key].scode;

            var up_price = this.objRegularBasketRecord[regular_key].price;

            var regular_detail_id = this.objRegularBasketRecord[regular_key].regular_detail_id;

            //2014/03/06 Willy Retain and Overwrite first the old record of bask_t.
            var retain_old_bask = ErsFactory.ersBasketFactory.GetErsBaskRecordWithParameter(this.objRegularBasketRecord[regular_key].GetPropertiesAsDictionary());

            RemoveRegular(regular_key);

            EnumOrderStatusType? status = null;
            if (order_status != null)
            {
                status = order_status.Value;
            }

            var service = ErsFactory.ersOrderFactory.GetManageRegularPatternService(datasource.send_ptn.Value);
           
            var merchandise = service.GetMerchandise(this.ransu, up_scode, datasource, member_rank);
            merchandise.regular_detail_id = regular_detail_id;
            //merchandise.price = up_price; remove to prevent duplicate item when recompute.

            cartInStrategy.AddMerchandise(this, merchandise, up_amount, EnumOrderType.Subscription, status);

            try
            {
                ErsFactory.ersBasketFactory.GetCartInSpecification().IsSatisfiedBy(merchandise, this,true);
            }
            catch (ErsException ex)
            {
                //return ex.Message + Environment.NewLine;
                throw new ErsException("universal", Environment.NewLine + ex.Message);
            }

            //2014/03/06 Willy set these properties using retain_old_bask
            merchandise.old_amount = retain_old_bask.old_amount;
            merchandise.stock_reservation = retain_old_bask.stock_reservation;

            var basketRepository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            basketRepository.Insert(merchandise);

            return string.Empty;
        }

        /// <summary>
        /// バスケットの内容をチェック
        /// </summary>
        public virtual void CheckBasketItems(bool IsOrderUpdate)
        {
            if (objBasketRecord.Count == 0 && objRegularBasketRecord.Count == 0)
            {
                throw new ErsException("20223", false, ErsFactory.ersUtilityFactory.getSetup().nor_url);
            }

            var message = string.Empty;

            foreach (var em in objBasketRecord.Values)
            {
                try
                {
                    em.IsOrderUpdate = IsOrderUpdate;
                    ErsFactory.ersBasketFactory.GetCartInSpecification().IsSatisfiedBy(em, this, true);
                    em.IsOrderUpdate = false;
                }
                catch (ErsException ex)
                {
                    message += ex.Message + Environment.NewLine;
                    continue;
                }
            }

            foreach (var em in objRegularBasketRecord.Values)
            {
                try
                {

                    ErsFactory.ersBasketFactory.GetCartInSpecification().IsSatisfiedBy(em, this, true);

                }
                catch (ErsException ex)
                {
                    message += ex.Message + Environment.NewLine;
                    continue;
                }
            }

            if (!string.IsNullOrEmpty(message))
            {
                throw new ErsException("universal", message);
            }
        }

        /// <summary>
        /// Gets Shortest delivery date from regularOrder.
        /// </summary>
        public virtual DateTime? GetRegularShortestDate()
        {
            var shortestDate = DateTime.MaxValue;
            foreach (var record in this.objRegularBasketRecord.Values)
            {
                //if next_date is null, return null in order to stop calculation forcibly.
                if (record.next_date == null)
                    return null;

                if (shortestDate > record.next_date)
                    shortestDate = record.next_date.Value;
            }
            return shortestDate != DateTime.MaxValue ? shortestDate : (DateTime?)null;
        }

        /// <summary>
        /// Calcurate first delivery date of regular purchase orders
        /// </summary>
        public void CalcRegularFirstDate(EnumWeekendOperation weekend_operation)
        {
            foreach (var record in this.objRegularBasketRecord.Values)
            {
                record.CalcRegularFirstDate(weekend_operation);
            }
        }

        /// <summary>
        /// return dictionary of basket grouped by senddate
        /// </summary>
        /// <param name="ordinaryOrderSenddate"></param>
        /// <returns></returns>
        public Dictionary<DateTime, ErsBasket> LoadBasketAsList(DateTime? ordinaryOrderSenddate, int? sendtime, int? regular_sendtime)
        {
            if (ordinaryOrderSenddate == null)
                ordinaryOrderSenddate = DateTime.MinValue;

            var dictionaryBasket = new Dictionary<DateTime, ErsBasket>();

            var newBasket = this.GetErsBasket();
            newBasket.Init(this.mcode, this.ransu, EnumOrderType.Usually);
            dictionaryBasket[ordinaryOrderSenddate.Value] = newBasket;
            newBasket.sendtime = sendtime;

            foreach (var merchandise in this.objRegularBasketRecord.Values)
            {
                DateTime next_date;
                if (merchandise.next_date == null)
                    next_date = DateTime.MinValue;
                else
                    next_date = merchandise.next_date.Value;
                if (!dictionaryBasket.ContainsKey(next_date))
                {
                    dictionaryBasket[next_date] = ErsFactory.ersBasketFactory.GetErsBasket();
                }

                //Add regular order record to basket as ordinary, if nextdate is the same as senddate.
                ErsFactory.ersBasketFactory.GetOrdinaryAddToCartStgy().AddMerchandise(dictionaryBasket[next_date], merchandise, merchandise.amount, EnumOrderType.Subscription, merchandise.order_status);

                //Sets regular order basket in order to caluculates carriage free status later.
                dictionaryBasket[next_date].objRegularBasketRecord = this.objRegularBasketRecord;

                dictionaryBasket[next_date].sendtime = regular_sendtime;
            }

            return dictionaryBasket;
        }

        protected virtual ErsBasket GetErsBasket()
        {
            return ErsFactory.ersBasketFactory.GetErsBasket();
        }
    }
}
