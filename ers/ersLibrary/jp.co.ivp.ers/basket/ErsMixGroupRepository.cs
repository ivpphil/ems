﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using System.Data;
using jp.co.ivp.ers.state;
using System.Collections;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.basket
{
    /// <summary>
    /// Provide methods to connect with mixed_group_t table. 
    /// Inherits ErsRepository<ErsMixGroup>
    /// </summary>
    public class ErsMixGroupRepository
        : ErsRepository<ErsMixGroup>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsMixGroupRepository()
            : base("mixed_group_t")
        {
        }
    }
}
