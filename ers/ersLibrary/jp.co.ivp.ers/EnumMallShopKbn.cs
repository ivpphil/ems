﻿
namespace jp.co.ivp.ers
{
    /// <summary>
    /// モール店舗区分 [Mall shop type]
    /// </summary>
    public enum EnumMallShopKbn
    {
        /// <summary>
        /// 0 : ERS本店 [ERS Main]
        /// </summary>
        ERS = 0,

        /// <summary>
        /// 1 : Yahoo! [Yahoo!]
        /// </summary>
        YAHOO = 1,

        /// <summary>
        /// 2 : 楽天 [Rakuten]
        /// </summary>
        RAKUTEN = 2,

        /// <summary>
        /// 3 : Amazon [Amazon]
        /// </summary>
        AMAZON = 3,
    }
}
