﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.projects
{
    public class ErsPcodeRepository:ErsRepository<ErsPcode>
    {
        public ErsPcodeRepository() : base("pcode_t")
        {

        }

        public ErsPcodeRepository(ErsDatabase objDB) : base("pcode_t",objDB)
        {

        }
    }
}
