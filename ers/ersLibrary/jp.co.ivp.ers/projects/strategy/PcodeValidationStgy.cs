﻿using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.projects.strategy
{
    public  class ValidationStgyPcode
    {
        ErsPcodeRepository repo = ErsFactory.ersPcodeFactory.GetErsPcodeRepository();
        ErsPcodeCriteria cri = ErsFactory.ersPcodeFactory.GetErsPcodeCriteria();

        public virtual ValidationResult CheckDuplicatePcode(string pcode)
        {
            cri.pcode = pcode;

            var result = repo.GetRecordCount(cri);
            cri.Clear();
            if(result > 0 )
            {
                return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("pcode"), pcode));
            }
            return null;
        }
         
        public virtual ValidationResult CheckUsedPcode(string pcode)
        {
            cri.pcode =  pcode;

            int result = repo.Find(cri).Count;
            cri.Clear();
            if (result > 1)
            {
                return new ValidationResult(ErsResources.GetMessage("pcode_in_use"));
            }

            return null;
        }

        public virtual ValidationResult CheckIfPcodeExist(int id, string pcode)
        {
            cri.id = id;
            cri.pcode = pcode;

            var result = repo.FindSingle(cri);
            cri.Clear();

            if(result == null)
            {
                return new ValidationResult(ErsResources.GetMessage("10200"));
            }

            return null;
        }
    }
}
