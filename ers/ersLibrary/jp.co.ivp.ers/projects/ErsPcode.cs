﻿using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.projects
{
    public class ErsPcode:ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public virtual string pcode { get; set; }

        public virtual string pcode_desc { get; set; }
    }
}
