﻿using jp.co.ivp.ers.projects.strategy;
using System.Linq;

namespace jp.co.ivp.ers.projects
{
    public class ErsPcodeFactory
    {
        public virtual ErsPcode GetErsPcode()
        {
            return new ErsPcode();
        }

        public virtual ErsPcodeRepository GetErsPcodeRepository()
        {
            return new ErsPcodeRepository();
        }

        public virtual ErsPcodeCriteria GetErsPcodeCriteria()
        {
            return new ErsPcodeCriteria();
        }

        public ValidationStgyPcode GetErsStgyPcode()
        {
            return new ValidationStgyPcode();
        }

        public virtual ErsPcode GetErsPcodeWithID(int id)
        {
            var repo = GetErsPcodeRepository();
            var cri = GetErsPcodeCriteria();
            cri.id = id;

            var list = repo.Find(cri);

            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }
    }
}
