﻿using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.projects
{
    public class ErsPcodeCriteria:Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("pcode_t.id", value, Operation.EQUAL));
            }
        }
        
        public string pcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("pcode_t.pcode", value, Operation.EQUAL));
            }
        }

        public int id_not_equal
        {
            set
            {
                Add(Criteria.GetCriterion("pcode_t.id", value, Operation.NOT_EQUAL));
            }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("pcode_t.id", orderBy);
        }

        public string pcode_desc
        {
            set
            {
                this.Add(Criteria.GetCriterion("pcode_t.pcode_desc", value, Operation.EQUAL));
            }
        }
    }
}
