﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// 在庫のステータス
    /// </summary>
    public enum EnumStockStatus
    {
        LESS_THAN_STOCK_SET1 = 1,
        MORE_THAN_STOCK_SET2,
        INSIDE_STOCK_SET,
        OUT_OF_STOCK,
    }
}
