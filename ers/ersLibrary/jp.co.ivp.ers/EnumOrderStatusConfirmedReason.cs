﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// 受注確認理由 [Reason of order status confirmed]
    /// </summary>
    public enum EnumOrderStatusConfirmedReason
    {
        /// <summary>
        /// 1 : オーソリエラー [Authorization error]
        /// </summary>
        AuthorizationError = 1,

        /// <summary>
        /// 2 : 住所エラー [Address error]
        /// </summary>
        AddressError = 2,

        /// <summary>
        /// 3 : 代引き＋０円 [Non payment on COD]
        /// </summary>
        NonPaymentCod = 3,

        /// <summary>
        /// 4 : いたずら [Mischief]
        /// </summary>
        Mischief = 4,

        /// <summary>
        /// 5 : 海外 [Overseas]
        /// </summary>
        Overseas = 5,
    }
}
