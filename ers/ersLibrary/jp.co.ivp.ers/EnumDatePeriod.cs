﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// 販売期間のステータス
    /// </summary>
    public enum EnumDatePeriod
    {
        INSIDE = 1,
        AFTER = 2,
        BEFORE = 3
    }
}
