﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.job
{
    public class ErsJobTitleRepository : ErsRepository<ErsJobTitle>
    {
        public ErsJobTitleRepository() : base("job_title_t")
        {

        }

        public ErsJobTitleRepository(ErsDatabase objDB) : base("job_title_t",objDB)
        {

        }
    }
}
