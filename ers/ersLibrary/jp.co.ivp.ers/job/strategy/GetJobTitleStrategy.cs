﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static jp.co.ivp.ers.db.Criteria;

namespace jp.co.ivp.ers.job.strategy
{
    public  class GetJobTitleStrategy
    {
        ErsJobTitleRepository repo = ErsFactory.ersJobFactory.GetErsJobTitleRepository();
        ErsJobTitleCriteria cri = ErsFactory.ersJobFactory.GetErsJobTitleCriteria();

        public IList<ErsJobTitle> GetJobTitleList()
        {
            cri.SetOrderByDispOrder(OrderBy.ORDER_BY_ASC);
            var list = repo.Find(cri);
            return list;

        }

        public ErsJobTitle GetJobTitleById(int? job_title)
        {
            if (job_title.HasValue)
            {
                var job = ErsFactory.ersJobFactory.GetErsJobTitleWithID(Convert.ToInt32(job_title));
                return job;
            }
            return null;
        }
    }
}
