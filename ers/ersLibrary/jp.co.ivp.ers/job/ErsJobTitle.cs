﻿using jp.co.ivp.ers.mvc;
using System;

namespace jp.co.ivp.ers.job
{
    public class ErsJobTitle : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public virtual string job_title { get; set; }

        public virtual string job_description { get; set; }

        public virtual int? disp_order { get; set;}

        public virtual DateTime? intime { get; set; }

        public virtual DateTime? utime { get; set; }
    }
}
