﻿using jp.co.ivp.ers.db;
using System.Collections.Generic;

namespace jp.co.ivp.ers.job
{
    public class ErsJobTitleCriteria : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("job_title_t.id", value, Operation.EQUAL));
            }
        }
        
        public string job_title
        {
            set
            {
                this.Add(Criteria.GetCriterion("job_title_t.job_title", value, Operation.EQUAL));
            }
        }

        public virtual string job_title_like
        {
            set
            {
                Add(Criteria.GetUniversalCriterion("(LOWER(job_title) LIKE :name)", new Dictionary<string, object> { { "name", "%" + value.ToLower() + "%" } }));
            }
        }
        
        public void SetOrderByDispOrder(OrderBy orderBy)
        {
            this.AddOrderBy("job_title_t.disp_order", orderBy);
        }

        public string job_description
        {
            set
            {
                this.Add(Criteria.GetCriterion("job_title_t.job_description", value, Operation.EQUAL));
            }
        }

        public int? disp_order
        {
            set
            {
                this.Add(Criteria.GetCriterion("job_title_t.disp_order", value, Operation.EQUAL));
            }
        }
    }
}
