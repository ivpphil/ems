﻿using jp.co.ivp.ers.job.strategy;
using System.Linq;

namespace jp.co.ivp.ers.job
{
    public class ErsJobFactory
    {
        public virtual ErsJobTitle GetErsJobTitle()
        {
            return new ErsJobTitle();
        }

        public virtual ErsJobTitleRepository GetErsJobTitleRepository()
        {
            return new ErsJobTitleRepository();
        }

        public virtual ErsJobTitleCriteria GetErsJobTitleCriteria()
        {
            return new ErsJobTitleCriteria();
        }
        
        public virtual ErsJobTitle GetErsJobTitleWithID(int id)
        {
            var repo = GetErsJobTitleRepository();
            var cri = GetErsJobTitleCriteria();
            cri.id = id;

            var list = repo.Find(cri);

            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        public virtual ErsJobTitle GetErsJobIdWithJobTitle(string job_title)
        {
            var repo = GetErsJobTitleRepository();
            var cri = GetErsJobTitleCriteria();
            cri.job_title_like = job_title;

            var list = repo.Find(cri);

            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        public GetJobTitleStrategy GetJobTitleStrategy()
        {
            return new GetJobTitleStrategy();
        }
        
    }
}
