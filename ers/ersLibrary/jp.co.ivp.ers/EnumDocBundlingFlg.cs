﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumDocBundlingFlg
        : short
    {
        /// <summary>
        /// 0: OFF
        /// </summary>
        OFF = 0,

        /// <summary>
        /// 1: ON
        /// </summary>
        ON
    }
}
