﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumWhOrderStatus
        : short
    {
        /// <summary>
        /// 入庫未完
        /// </summary>
        NotStorage = 1,

        /// <summary>
        /// 入庫完了
        /// </summary>
        Storaged = 2,

        /// <summary>
        /// 発注取消
        /// </summary>
        Canceled = 3
    }
}
