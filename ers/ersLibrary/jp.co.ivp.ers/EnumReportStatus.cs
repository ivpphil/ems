﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumReportStatus
    {
        CompletedTask = 1,
        OngoingTask,
        OngoingTaskWithProblem
    }
}
