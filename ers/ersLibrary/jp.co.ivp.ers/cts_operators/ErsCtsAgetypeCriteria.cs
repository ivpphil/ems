﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.cts_operators
{
    public class ErsCtsAgetypeCriteria
        : Criteria
    {
        internal void SetOrderById(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("cts_agetype_t.id", orderBy);
        }

        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_agetype_t.id", value, Operation.EQUAL));
            }
        }
    }
}
