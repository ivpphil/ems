﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.cts_operators
{
    public class ErsCtsOperator : ErsRepositoryEntity
	{
        public const string DEFAULT_USER_ID = "0";
        public const string DEFAULT_USER_AUTHORITY = "OP";
        public const string DEFAULT_USER_NAME = "Guest";

        public override int? id { get; set; }

        public string user_id { get; set; }

        public string passwd { get; set; }

        public string authority { get; set; }

        public string ag_name { get; set; }
        public string agent { get; set; }

        public EnumAgType? ag_type { get; set; }

        public DateTime? utime { get; set; }

        public DateTime? intime { get; set; }
        public int? active { get; set; }

        public CtsAuthorityType authoritytypes;
        public int? site_id { get; set; }
	}
}
