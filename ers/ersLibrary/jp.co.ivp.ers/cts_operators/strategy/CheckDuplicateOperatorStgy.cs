﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.cts_operators.strategy
{
    /// <summary>
    /// Checks Whether the user already exists. Uses user id.
    /// </summary>
    public class CheckDuplicateOperatorStgy
    {
        /// <summary>
        /// Return true if the same member exists.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public virtual ValidationResult CheckDuplicate(int? id, string cts_user_id)
        {
            var criteria = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorCriteria();

            criteria.exclude_id = id;
            criteria.user_id = cts_user_id;
            criteria.active = EnumActive.Active;

            var repository = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorRepository();

            if (repository.GetRecordCount(criteria) > 0)
            {
                //return new ValidationResult("Duplicate User ID.");
                return new ValidationResult(
                                ErsResources.GetMessage("10103", ErsResources.GetFieldName("cts_login_t.user_id"), cts_user_id),
                                new[] { "user_id" });
            }

            return null;
        }
    }
}
