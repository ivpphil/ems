﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.cts_operators.strategy
{
    public class CheckExistAgentTypeStgy
    {
        public virtual ValidationResult ValidAgentType(EnumAgType? id)
        {
            if (!id.HasValue)
            {
                return null;
            }

            //if (id <= 0)
            if (!ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentTypeService().ExistValue(id))
            {
                return new ValidationResult(
                                ErsResources.GetMessage("10025", ErsResources.GetFieldName("ag_type"), id),
                                new[] { "ag_type" });
            }
            return null;
        }
    }
}
