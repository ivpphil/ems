﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.cts_operators;

namespace jp.co.ivp.ers.cts_operators.strategy
{

    //agent_idからagentTypeを取得し値を返す
    public class ObtainAgentTypeStgy
    {

        public EnumAgType? getAgeType(int id)
        {
            var repository = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorRepository();
            var criteria = getAgeTypeCriteria(id);
            return repository.Find(criteria)[0].ag_type;
        }

        private ErsCtsOperatorCriteria getAgeTypeCriteria(int id)
        {
            var criteria = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorCriteria();
            criteria.id = id;
            criteria.active = EnumActive.Active;
            return criteria;
        }
    }
}
