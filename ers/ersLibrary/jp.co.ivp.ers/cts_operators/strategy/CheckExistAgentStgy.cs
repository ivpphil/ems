﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.cts_operators.strategy
{
    public class CheckExistAgentStgy
    {
        public virtual ValidationResult Validate(int? id)
        {
            if (!id.HasValue)
            {
                return null;
            }

            //if (id <= 0)
            if (!ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentService().ExistValue(null, id))
            {
                return new ValidationResult(
                                ErsResources.GetMessage("10025", ErsResources.GetFieldName("agent_id"), id),
                                new[] { "agent_id" });
            }
            return null;
        }
    }
}
