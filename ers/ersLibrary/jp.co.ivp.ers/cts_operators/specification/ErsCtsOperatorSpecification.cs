﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.cts_operators
{
    public class ErsCtsOperatorSpecification
        :SearchSpecificationBase
    {
       protected override string GetSearchDataSql()
        {
            string strQuery = @"SELECT cts_login_t.*, cts_agetype_t.agent
                                      FROM cts_login_t 
                                     INNER JOIN cts_agetype_t on cts_login_t.ag_type = cts_agetype_t.id ";
            return strQuery;
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            string strQuery = @"SELECT COUNT(DISTINCT cts_login_t.id) AS " + countColumnAlias + " FROM cts_login_t ";

            return strQuery;
        }
    }
}
