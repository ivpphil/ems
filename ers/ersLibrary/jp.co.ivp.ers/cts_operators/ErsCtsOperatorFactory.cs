﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.cts_operators.strategy;

namespace jp.co.ivp.ers.cts_operators
{
    public class ErsCtsOperatorFactory
    {

        public ErsCtsOperatorCriteria GetErsCtsOperatorCriteria()
        {
            return new ErsCtsOperatorCriteria();
        }

        public ErsCtsOperatorRepository GetErsCtsOperatorRepository()
        {
            return new ErsCtsOperatorRepository();
        }

        public ErsCtsOperator GetErsCtsOperator()
        {
            return new ErsCtsOperator();
        }

        public ErsCtsOperatorSpecification GetErsCtsOperatorSpecification()
        {
            return new ErsCtsOperatorSpecification(); 
        }

        public ErsCtsOperator GetErsOperatorWithModel(ErsModelBase model)
        {
            var Operator = this.GetErsCtsOperator();
            Operator.OverwriteWithModel(model);
            return Operator;
        }

        public ErsCtsOperator GetErsCtsOperatorWithParameters(Dictionary<string, object> parameters)
        {
            var Operator = this.GetErsCtsOperator();
            Operator.OverwriteWithParameter(parameters);
            return Operator;
        }

        public ErsCtsOperator GetErsCtsOperatorWithId(int? id)
        {
            var repository = this.GetErsCtsOperatorRepository();
            var criteria = this.GetErsCtsOperatorCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count == 0)
            {
                return null;
            }

            return list[0];
        }

        public virtual ErsCtsAgetypeRepository GetErsCtsAgetypeRepository()
        {
            return new ErsCtsAgetypeRepository();
        }

        public virtual ErsCtsAgetypeCriteria GetErsCtsAgetypeCriteria()
        {
            return new ErsCtsAgetypeCriteria();
        }

        public virtual ErsCtsAgetype GetErsCtsAgetype()
        {
            return new ErsCtsAgetype();
        }

        public virtual ErsCtsAgetype GetErsCtsAgetypeWithParameter(Dictionary<string, object> dr)
        {
            var objCtsAgetype = this.GetErsCtsAgetype();
            objCtsAgetype.OverwriteWithParameter(dr);
            return objCtsAgetype;
        }

        public virtual CheckDuplicateOperatorStgy GetCheckDuplicateOperatorStgy()
        {
            return new CheckDuplicateOperatorStgy();
        }

        public virtual CheckExistAgentTypeStgy GetCheckExistAgentTypeStgy()
        {
            return new CheckExistAgentTypeStgy();
        }

        public virtual CheckExistAgentStgy GetCheckExistAgentStgy()
        {
            return new CheckExistAgentStgy();
        }

        public virtual ObtainAgentTypeStgy GetObtainAgentTypeStgy()
        {
            return new ObtainAgentTypeStgy();
        }
    }
}
