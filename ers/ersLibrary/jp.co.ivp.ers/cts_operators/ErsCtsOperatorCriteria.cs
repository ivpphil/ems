﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.cts_operators
{
    public class ErsCtsOperatorCriteria : Criteria
	{

        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_login_t.id", value, Operation.EQUAL));
            }
        }

        public string user_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_login_t.user_id", value, Operation.EQUAL));
            }
        }

        public string passwd
        {
            set
            {
                Add(Criteria.GetCriterion("cts_login_t.passwd", value, Operation.EQUAL));
            }
        }

        public string authority
        {
            set
            {
                Add(Criteria.GetCriterion("cts_login_t.authority", value, Operation.EQUAL));
            }
        }

        public string ag_name
        {
            set
            {
                Add(Criteria.GetCriterion("cts_login_t.ag_name", value, Operation.EQUAL));
            }
        }

        public EnumAgType? ag_type
        {
            set
            {
                Add(Criteria.GetCriterion("cts_login_t.ag_type", value, Operation.EQUAL));
            }
        }

        public int? exclude_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_login_t.id", value, Operation.NOT_EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("cts_login_t.active", value, Operation.EQUAL));
            }
        }

        public int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("cts_login_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_login_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("id", orderBy);
        }

        public void SetOrderByUserID(OrderBy orderBy)
        {
            AddOrderBy("cts_login_t.id", orderBy);
        }


	}
}
