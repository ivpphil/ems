﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.cts_operators
{
    public class CtsAuthorityType 
    {
        private CtsAuthorityType()
        {
        }

        public const string SUPERVISOR = "SV";
        public const string OPERATOR = "OP";
    }
}
