﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.cts_operators
{
    public class ErsCtsAgetype
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string agent { get; set; }

        public EnumActive? active { get; set; }
    }
}
