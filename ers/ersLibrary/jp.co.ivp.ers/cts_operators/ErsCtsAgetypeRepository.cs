﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.cts_operators
{
    public class ErsCtsAgetypeRepository
        :ErsRepository<ErsCtsAgetype>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
            public ErsCtsAgetypeRepository()
                : base("cts_agetype_t")
        {
        }
    }
}
