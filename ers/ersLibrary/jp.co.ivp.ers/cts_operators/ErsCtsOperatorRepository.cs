﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.cts_operators
{
    public class ErsCtsOperatorRepository
        : ErsRepository<ErsCtsOperator>
    {
        public ErsCtsOperatorRepository()
            : base("cts_login_t")
        {
        }

        public ErsCtsOperatorRepository(ErsDatabase objDB)
            : base("cts_login_t", objDB)
        {
        }
        
        public IList<ErsCtsOperator> FindOperator(db.Criteria criteria)
        {
            var spec = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorSpecification();
            List<ErsCtsOperator> lstRet = new List<ErsCtsOperator>();
            var list = spec.GetSearchData(criteria);
            foreach (var dr in list)
            {
                var dir = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithParameters(dr);
                lstRet.Add(dir);
            }
            return lstRet;
        }
    }
}
