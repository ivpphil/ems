﻿
namespace jp.co.ivp.ers.common
{
    public class ErsCommonConst
    {
        /// <summary>
        /// 商品画像枚数 [Number of image]
        /// </summary>
        public static int MERCHANDISE_IMAGE_NUM = 3;

        /// <summary>
        /// 商品画像拡張子 [Extentions of image]
        /// </summary>
        public static string[] MERCHANDISE_IMAGE_EXTENSIONS = new string[] { ".jpg" };

        /// <summary>
        /// SKU画像パスフォーマット [Path format of SKU image]
        /// <para>0 : setup.image_directory</para>
        /// <para>1 : scode</para>
        /// <para>2 : index</para>
        /// </summary>
        public static string SKU_IMAGE_PATH_FORMAT = "{0}bimg\\S{1}_{2:D2}";

        /// <summary>
        /// SKU画像URLフォーマット [Url format of SKU image]
        /// <para>0 : setup.pc_nor_url</para>
        /// <para>1 : scode</para>
        /// <para>2 : index</para>
        /// </summary>
        public static string SKU_IMAGE_URL_FORMAT = "{0}images/bimg/S{1}_{2:D2}";
    }
}
