﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.common.strategy
{
    public class ValidateCommonNameCodeStgy
    {
        public ValidationResult Validate(string fieldNameKey, EnumCommonNameType type, int? value)
        {
            if (!value.HasValue)
            {
                return null;
            }

            if (!ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().ExistValue(type, value))
            {
                return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName(fieldNameKey)), new[] { fieldNameKey });
            }
            return null;
        }
    }
}
