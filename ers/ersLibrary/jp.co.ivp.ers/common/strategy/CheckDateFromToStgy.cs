﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.common.strategy
{
    /// <summary>
    /// 日付の前後関係のチェック
    /// </summary>
    public class CheckDateFromToStgy
    {
        /// <summary>
        /// 日付比較チェック
        /// </summary>
        /// <param name="model"></param>
        /// <param name="date_from"></param>
        /// <param name="date_to"></param>
        /// <param name="lineName"></param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> CheckDateTime(string date_fromFieldKey, DateTime? date_from, string date_toFieldKey, DateTime? date_to)
        {
            if (!date_from.HasValue || !date_to.HasValue)
                yield break;

            if (date_from > date_to)
                yield return new ValidationResult(
                    ErsResources.GetMessage("10045",
                        ErsResources.GetFieldName(date_toFieldKey),
                        ErsResources.GetFieldName(date_fromFieldKey)),
                    new[] { date_fromFieldKey, date_toFieldKey });

            yield break;
        }

        /// <summary>
        /// 期間内日付チェック
        /// </summary>
        /// <param name="model">モデルクラス</param>
        /// <param name="date_fromFieldKey">項目名称キー</param>
        /// <param name="date_from">日付のFROM</param>
        /// <param name="date_toFieldKey">項目名称キー</param>
        /// <param name="date_to">日付のTO</param>
        /// <param name="date_postFieldKey">項目名称キー</param>
        /// <param name="date_post">掲載日等の期間内にあるべき日</param>
        /// <param name="lineName">CSVの行数</param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> CheckDateTimeTerm(
            string date_fromFieldKey, DateTime? date_from,
            string date_toFieldKey, DateTime? date_to,
            string date_postFieldKey, DateTime? date_value)
        {
            if (!date_from.HasValue || !date_to.HasValue || !date_value.HasValue)
                yield break;

            if (date_from > date_value || date_value > date_to)
                yield return new ValidationResult(
                    ErsResources.GetMessage("10047",
                        ErsResources.GetFieldName(date_postFieldKey),
                        ErsResources.GetFieldName(date_toFieldKey),
                        ErsResources.GetFieldName(date_fromFieldKey)),
                    new[] { date_postFieldKey });
            yield break;
        }

        /// <summary>
        /// 日付比較チェック(時間判定なし)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="date_from"></param>
        /// <param name="date_to"></param>
        /// <param name="lineName"></param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> CheckDate(string date_fromFieldKey, DateTime? date_from, string date_toFieldKey, DateTime? date_to, bool allowSameDate = true)
        {
            if (!date_from.HasValue || !date_to.HasValue)
                yield break;

            if ((allowSameDate & date_from.Value.Date > date_to.Value.Date) ||
                (!allowSameDate & date_from.Value.Date >= date_to.Value.Date))
            {
                yield return new ValidationResult(
                    ErsResources.GetMessage("10045",
                        date_toFieldKey.HasValue() ? ErsResources.GetFieldName(date_toFieldKey) : date_to.Value.ToString("yyyy/MM/dd"),
                        date_fromFieldKey.HasValue() ? ErsResources.GetFieldName(date_fromFieldKey) : date_from.Value.ToString("yyyy/MM/dd")),
                    new[] { date_fromFieldKey, date_toFieldKey });
            }

            yield break;
        }

        /// <summary>
        /// 期間内日付チェック(時間判定なし)
        /// </summary>
        /// <param name="model">モデルクラス</param>
        /// <param name="date_fromFieldKey">項目名称キー</param>
        /// <param name="date_from">日付のFROM</param>
        /// <param name="date_toFieldKey">項目名称キー</param>
        /// <param name="date_to">日付のTO</param>
        /// <param name="date_postFieldKey">項目名称キー</param>
        /// <param name="date_post">掲載日等の期間内にあるべき日</param>
        /// <param name="lineName">CSVの行数</param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> CheckDateTerm(
            string date_fromFieldKey, DateTime? date_from,
            string date_toFieldKey, DateTime? date_to,
            string date_postFieldKey, DateTime? date_value)
        {
            if (!date_from.HasValue || !date_to.HasValue || !date_value.HasValue)
                yield break;

            if (date_from.Value.Date > date_value.Value.Date ||
                date_value.Value.Date > date_to.Value.Date)
                yield return new ValidationResult(
                    ErsResources.GetMessage("10047",
                    ErsResources.GetFieldName(date_postFieldKey),
                    ErsResources.GetFieldName(date_toFieldKey),
                    ErsResources.GetFieldName(date_fromFieldKey)),
                    new[] { date_postFieldKey });
            yield break;
        }

        /// <summary>
        /// 日付比較チェック(日付判定なし)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="date_from"></param>
        /// <param name="date_to"></param>
        /// <param name="lineName"></param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> CheckTime(
            string time_fromFieldKey, int? hour_from, int? minutes_from, int? seconds_from, 
            string time_toFieldKeyy, int? hour_to, int? minutes_to, int? seconds_to)
        {
            if (!hour_from.HasValue || !minutes_from.HasValue || !seconds_from.HasValue || 
                !hour_to.HasValue || !minutes_to.HasValue || !seconds_to.HasValue)
                yield break;

            var time_from = new TimeSpan(hour_from.Value, minutes_from.Value, seconds_from.Value);
            var time_to = new TimeSpan(hour_to.Value, minutes_to.Value, seconds_to.Value);

            if (time_from > time_to)
                yield return new ValidationResult(
                    ErsResources.GetMessage("10045",
                    ErsResources.GetFieldName(time_toFieldKeyy),
                    ErsResources.GetFieldName(time_fromFieldKey)),
                    new[] { time_fromFieldKey, time_toFieldKeyy });

            yield break;
        }

        /// <summary>
        /// 期間内日付チェック(日付判定なし)
        /// </summary>
        /// <param name="model">モデルクラス</param>
        /// <param name="date_fromFieldKey">項目名称キー</param>
        /// <param name="date_from">日付のFROM</param>
        /// <param name="date_toFieldKey">項目名称キー</param>
        /// <param name="date_to">日付のTO</param>
        /// <param name="date_postFieldKey">項目名称キー</param>
        /// <param name="date_post">掲載日等の期間内にあるべき日</param>
        /// <param name="lineName">CSVの行数</param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> CheckTimeTerm(
            string time_fromFieldKey, int? hour_from, int? minutes_from, int? seconds_from,
            string time_toFieldKey, int? hour_to, int? minutes_to, int? seconds_to,
            string time_postFieldKey, int? hour_value, int? minutes_value, int? seconds_value)
        {
            if (!hour_from.HasValue || !minutes_from.HasValue || !seconds_from.HasValue || 
                !hour_to.HasValue || !minutes_to.HasValue || !seconds_to.HasValue ||
                !hour_value.HasValue || !minutes_value.HasValue || !seconds_value.HasValue)
                yield break;

            var time_from = new TimeSpan(hour_from.Value, minutes_from.Value, seconds_from.Value);
            var time_to = new TimeSpan(hour_to.Value, minutes_to.Value, seconds_to.Value);
            var time_value = new TimeSpan(hour_value.Value, minutes_value.Value, seconds_value.Value);

            if (time_from > time_value ||
                time_value > time_to)
                yield return new ValidationResult(
                    ErsResources.GetMessage("10047",
                    ErsResources.GetFieldName(time_postFieldKey),
                    ErsResources.GetFieldName(time_toFieldKey),
                    ErsResources.GetFieldName(time_fromFieldKey)),
                    new[] { time_postFieldKey });
            yield break;
        }
    }
}
