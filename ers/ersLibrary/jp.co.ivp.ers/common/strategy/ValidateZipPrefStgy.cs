﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.common.strategy
{
    public class ValidateZipPrefStgy
    {
        public ValidationResult Validate(string zipColumnName, string zip, string prefColumnName = null, int? pref = null, bool activeInFrontOnly = true)
        {
            if (!zip.HasValue())
            {
                return null;
            }

            var isOverseasZipStgy = ErsFactory.ersCommonFactory.GetIsOverseasZipSpec();

            var isOverseas = isOverseasZipStgy.IsOverseas(zip);

            if (!isOverseas || pref == (int)EnumPrefecture.OVERSEAS)
            {
                return null;
            }

            var errorMessage = ErsResources.GetMessage("10050", ErsResources.GetFieldName(zipColumnName));
            var fields = new List<string>() { zipColumnName };

            if (pref.HasValue)
            {
                if (!activeInFrontOnly || isOverseasZipStgy.isEnableOrverseasInFront())
                {
                    //管理画面またはフロントで海外配送がONの場合のみ、海外を選ぶ旨のメッセージを表示する。
                    errorMessage += ErsResources.GetMessage("10213", ErsResources.GetFieldName(prefColumnName));
                    fields.Add(prefColumnName);
                }
            }

            // 海外配送
            return new ValidationResult(errorMessage, fields);
        }
    }
}
