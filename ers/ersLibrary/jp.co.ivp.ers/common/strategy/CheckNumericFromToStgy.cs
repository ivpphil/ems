﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.common.strategy
{
    /// <summary>
    /// 数値の前後関係のチェック
    /// </summary>
    public class CheckNumericFromToStgy
    {
        /// <summary>
        /// CSVチェック用
        /// </summary>
        /// <param name="model"></param>
        /// <param name="num_from"></param>
        /// <param name="num_to"></param>
        /// <param name="lineName"></param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> Check(string num_fromFieldKey, int? num_from, string num_toFieldKey, int? num_to)
        {
            if (num_from == null || num_to == null)
                yield break;

            if (num_from > num_to)
                yield return new ValidationResult(
                    ErsResources.GetMessage("10023",
                        ErsResources.GetFieldName(num_toFieldKey),
                        ErsResources.GetFieldName(num_fromFieldKey)),
                    new[] { num_fromFieldKey, num_toFieldKey });

            yield break;
        }

        /// <summary>
        /// 期間内日付チェック
        /// </summary>
        /// <param name="model">モデルクラス</param>
        /// <param name="num_fromFieldKey">項目名称キー</param>
        /// <param name="num_from">日付のFROM</param>
        /// <param name="num_toFieldKey">項目名称キー</param>
        /// <param name="num_to">日付のTO</param>
        /// <param name="num_postFieldKey">項目名称キー</param>
        /// <param name="num_post">掲載日等の期間内にあるべき日</param>
        /// <param name="lineName">CSVの行数</param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> DateTermCheck(
            string num_fromFieldKey, int? num_from,
            string num_toFieldKey, int? num_to,
            string num_postFieldKey, int? num_value)
        {
            if (num_from == null || num_to == null || num_value == null)
                yield break;

            if (num_from > num_value || num_value > num_to)
                yield return new ValidationResult(
                    ErsResources.GetMessage("10022",
                        ErsResources.GetFieldName(num_postFieldKey),
                        ErsResources.GetFieldName(num_fromFieldKey),
                        ErsResources.GetFieldName(num_toFieldKey)),
                    new[] { num_postFieldKey });
            yield break;
        }
    }
}
