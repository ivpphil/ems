﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.common
{
    public class ErsEraRepository
        : ErsRepository<ErsEra>
    {
        public ErsEraRepository()
            : base("era_t")
        {
        }
    }
}
