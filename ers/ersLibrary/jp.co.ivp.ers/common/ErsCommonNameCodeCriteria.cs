﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.common
{
    public class ErsCommonNameCodeCriteria
        : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("common_namecode_t.id", value, Operation.EQUAL));
            }
        }

        public EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("common_namecode_t.active", value, Operation.EQUAL));
            }
        }

        public EnumCommonNameType? type_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("common_namecode_t.type_code", Convert.ToString(value), Operation.EQUAL));
            }
        }

        public int? code
        {
            set
            {
                this.Add(Criteria.GetCriterion("common_namecode_t.code", value, Operation.EQUAL));
            }
        }

        public void SetOrderByCode(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("common_namecode_t.code", orderBy);
        }

        public void SetOrderByDispOrder(OrderBy orderBy)
        {
            this.AddOrderBy("common_namecode_t.disp_order", orderBy);
        }

        public virtual string namename_like
        {
            set
            {
                this.Add(Criteria.GetUniversalCriterion("common_namecode_t.namename ILIKE  '%" + value + "%'"));
            }
        }
    }
}
