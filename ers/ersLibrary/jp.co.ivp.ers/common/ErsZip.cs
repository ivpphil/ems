﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.common
{
    public class ErsZip
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string zip { get; set; }
        public int? pref { get; set; }
        public string address { get; set; }
        public string address2 { get; set; }
    }
}
