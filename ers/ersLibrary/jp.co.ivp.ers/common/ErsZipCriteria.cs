﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.common
{
    public class ErsZipCriteria
        : Criteria
    {
        public string zip_prefix_search
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("zip", value, Criteria.LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }
    }
}
