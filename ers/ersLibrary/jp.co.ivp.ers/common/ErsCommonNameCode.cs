﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.common
{
    public class ErsCommonNameCode
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string type_code { get; set; }

        public int? code { get; set; }

        public string namename { get; set; }

        public string opt_chr1 { get; set; }

        public string opt_chr2 { get; set; }

        public int opt_flg1 { get; set; }

        public int opt_flg2 { get; set; }

        public int? opt_num1 { get; set; }

        public int? opt_num2 { get; set; }

        public int? disp_order { get; set; }

        public DateTime? intime { get; set; }

        public DateTime? utime { get; set; }

        public EnumActive active { get; set; }
    }
}
