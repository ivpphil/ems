﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.common
{
    public class ErsCtsEnquiryCategoryCriteria
        : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_category_t.id", value, Operation.EQUAL));
            }
        }

        public EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_category_t.active", value, Operation.EQUAL));
            }
        }

        public EnumCtsEnquiryCategoryType? type_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_category_t.type_code", Convert.ToString(value), Operation.EQUAL));
            }
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("cts_enquiry_category_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_enquiry_category_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

        public void SetOrderByCode(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("cts_enquiry_category_t.code", orderBy);
        }

        public void SetOrderByDispOrder(OrderBy orderBy)
        {
            this.AddOrderBy("cts_enquiry_category_t.disp_order", orderBy);
        }
    }
}
