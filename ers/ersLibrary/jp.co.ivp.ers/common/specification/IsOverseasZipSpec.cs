﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.common.specification
{
    public class IsOverseasZipSpec
    {
        public bool IsOverseas(string zip)
        {
            return !Regex.IsMatch(zip, @"^[0-9]{3}-?[0-9]{4}$");
        }

        public bool isEnableOrverseasInFront()
        {
            var objPref = ErsFactory.ersCommonFactory.GetErsPrefWithIdAndSiteId((int)EnumPrefecture.OVERSEAS, (int)EnumSiteId.COMMON_SITE_ID);

            return objPref.active_in_front == EnumActive.Active;
        }
    }
}
