﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.common
{
    public class ErsCountry
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string country { get; set; }

        public string country_e { get; set; }

        public string country_c { get; set; }

        public string tel { get; set; }

        public EnumActive active { get; set; }
    }
}
