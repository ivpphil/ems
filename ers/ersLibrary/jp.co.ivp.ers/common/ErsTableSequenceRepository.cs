﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.common
{
    public class ErsTableSequenceRepository
        : ErsRepository<ErsTableSequence>
    {
        public ErsTableSequenceRepository(string tableName)
            : base(tableName)
        {
        }

        public ErsTableSequenceRepository(string tableName, db.ErsDatabase objDB)
            : base(tableName, objDB)
        {
        }
    }
}
