﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.common
{
    public class ErsCtsEnquiryCategory
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string type_code { get; set; }

        public int? code { get; set; }

        public string namename { get; set; }

        public int? parent_code { get; set; }

        public int? disp_order { get; set; }

        public DateTime? intime { get; set; }

        public DateTime? utime { get; set; }

        public EnumActive active { get; set; }

        public int? site_id { get; set; }
    }
}
