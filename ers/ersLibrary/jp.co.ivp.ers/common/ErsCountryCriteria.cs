﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.common
{
    public class ErsCountryCriteria
        : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("country_t.id", value, Operation.EQUAL));
            }
        }

        internal void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("country_t.id", orderBy);
        }
    }
}
