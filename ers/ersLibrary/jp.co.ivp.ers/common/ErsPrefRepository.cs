﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.common
{
    public class ErsPrefRepository
        :ErsRepository<ErsPref>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
            public ErsPrefRepository()
                : base("pref_t")
        {
        }

        public override void Update(ErsPref old_obj, ErsPref new_obj)
        {
            var oldDic = old_obj.GetPropertiesAsDictionary(ersDB_table);
            var newDic = new_obj.GetPropertiesAsDictionary(ersDB_table);

            var changedColumns = ErsCommon.GetChangedKeys(oldDic, newDic);
            if (changedColumns.Length > 0)
                ersDB_table.gUpdateColumn(changedColumns, newDic, "WHERE id = '" + old_obj.id + "' AND site_id = '" + old_obj.site_id + "'");
        }
    }
}
