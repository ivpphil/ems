﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.common
{
    public class ErsTableSequence
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string sequence_name { get; set; }

        public long? last_value { get; set; }
    }
}
