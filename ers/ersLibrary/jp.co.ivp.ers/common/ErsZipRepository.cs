﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.common
{
    public class ErsZipRepository
        : ErsRepository<ErsZip>
    {
        public ErsZipRepository()
            : base("zip_t")
        {
        }
    }
}
