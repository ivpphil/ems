﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.common
{
    public class ErsPrefCriteria
        : Criteria
    {
        public void SetOrderById(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("pref_t.id", orderBy);
        }

        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("pref_t.id", value, Operation.EQUAL));
            }
        }

        public int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("pref_t.site_id", value, Operation.EQUAL));
            }
        }

        public EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("pref_t.active", value, Operation.EQUAL));
            }
        }
    }
}
