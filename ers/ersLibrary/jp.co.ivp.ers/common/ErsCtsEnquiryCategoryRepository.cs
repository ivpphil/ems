﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.common
{
    public class ErsCtsEnquiryCategoryRepository
        : ErsRepository<ErsCtsEnquiryCategory>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsCtsEnquiryCategoryRepository()
            : base("cts_enquiry_category_t")
        {
        }
    }
}
