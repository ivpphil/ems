﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.common
{
    public class ErsPref
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string pref_name { get; set; }

        public int? carriage { get; set; }

        public EnumActive? active { get; set; }

        public DateTime? intime { get; set; }

        public DateTime? utime { get; set; }

        public EnumActive? active_in_front { get; set; }

        public int? site_id { get; set; }
    }
}
