﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.common.strategy;
using jp.co.ivp.ers.common.specification;

namespace jp.co.ivp.ers.common
{
    public class ErsCommonFactory
    {
        public virtual ErsPrefRepository GetErsPrefRepository()
        {
            return new ErsPrefRepository();
        }

        public virtual ErsPrefCriteria GetErsPrefCriteria()
        {
            return new ErsPrefCriteria();
        }

        public virtual ErsPref GetErsPref()
        {
            return new ErsPref();
        }

        public virtual ErsPref GetErsPrefWithIdAndSiteId(int? id, int? site_id)
        {
            var repository = this.GetErsPrefRepository();
            var criteria = this.GetErsPrefCriteria();
            criteria.id = id;
            criteria.site_id = site_id;
            var listObj = repository.Find(criteria);
            if (listObj.Count == 0)
            {
                return null;
            }

            return listObj.First();
        }

        public virtual ErsPref GetErsPrefWithParameter(Dictionary<string, object> dr)
        {
            var objPref = this.GetErsPref();
            objPref.OverwriteWithParameter(dr);
            return objPref;
        }

        public virtual ErsCountryRepository GetErsCountryRepository()
        {
            return new ErsCountryRepository();
        }

        public virtual ErsCountryCriteria GetErsCountryCriteria()
        {
            return new ErsCountryCriteria();
        }

        public virtual ErsCountry GetErsCountry()
        {
            return new ErsCountry();
        }

        public virtual ErsCountry GetErsCountryWithParameter(Dictionary<string, object> dr)
        {
            var objCountry = this.GetErsCountry();
            objCountry.OverwriteWithParameter(dr);
            return objCountry;
        }

        public virtual CheckDateFromToStgy GetCheckDateFromToStgy()
        {
            return new CheckDateFromToStgy();
        }

        public virtual ErsCommonNameCodeRepository GetErsCommonNameCodeRepository()
        {
            return new ErsCommonNameCodeRepository();
        }

        public virtual ErsCommonNameCodeCriteria GetErsCommonNameCodeCriteria()
        {
            return new ErsCommonNameCodeCriteria();
        }

        public virtual ErsCommonNameCode GetErsCommonNameCode()
        {
            return new ErsCommonNameCode();
        }

        public virtual ErsCommonNameCode GetErsCommonNameCodeWithParameter(Dictionary<string, object> parameter)
        {
            var objCommonNameCode = this.GetErsCommonNameCode();
            objCommonNameCode.OverwriteWithParameter(parameter);
            return objCommonNameCode;
        }

        public virtual ErsCommonNameCode GetErsCommonNameCodeWithId(int? id)
        {
            var repository = this.GetErsCommonNameCodeRepository();
            var criteria = this.GetErsCommonNameCodeCriteria();
            criteria.id = id;
            var listObj = repository.Find(criteria);

            if (listObj.Count == 0)
            {
                return null;
            }

            return listObj.First();
        }

        public virtual ErsCommonNameCode GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType type_code, int? code)
        {
            var repository = this.GetErsCommonNameCodeRepository();
            var criteria = this.GetErsCommonNameCodeCriteria();
            criteria.type_code = type_code;
            criteria.code = code;
            var listObj = repository.Find(criteria);

            if (listObj.Count == 0)
            {
                return null;
            }

            return listObj.First();
        }

        public List<Dictionary<string, object>> GetCommonNameCodeOption(EnumCommonNameType type)
        {
            var returnList = new List<Dictionary<string, object>>();
            var repo = ErsFactory.ersCommonFactory.GetErsCommonNameCodeRepository();
            var cri = ErsFactory.ersCommonFactory.GetErsCommonNameCodeCriteria();
            cri.type_code = type;
            cri.active = EnumActive.Active;

            if (repo.GetRecordCount(cri) > 0)
            {
                foreach (var item in repo.Find(cri))
                {
                    var dic = new Dictionary<string, object>();
                    dic.Add("name", item.namename);
                    dic.Add("code", item.code);
                    returnList.Add(dic);
                }

                return returnList;
            }
            return null;
        }

        public virtual ErsCtsEnquiryCategoryRepository GetErsCtsEnquiryCategoryRepository()
        {
            return new ErsCtsEnquiryCategoryRepository();
        }

        public virtual ErsCtsEnquiryCategoryCriteria GetErsCtsEnquiryCategoryCriteria()
        {
            return new ErsCtsEnquiryCategoryCriteria();
        }

        public virtual ErsCtsEnquiryCategory GetErsCtsEnquiryCategory()
        {
            return new ErsCtsEnquiryCategory();
        }

        public virtual ErsCtsEnquiryCategory GetErsCtsEnquiryCategoryWithParameter(Dictionary<string, object> parameter)
        {
            var objCtsEnquiryCategory = this.GetErsCtsEnquiryCategory();
            objCtsEnquiryCategory.OverwriteWithParameter(parameter);
            return objCtsEnquiryCategory;
        }

        public virtual ErsCtsEnquiryCategory GetErsCtsEnquiryCategoryWithId(int id)
        {
            var repository = this.GetErsCtsEnquiryCategoryRepository();
            var criteria = this.GetErsCtsEnquiryCategoryCriteria();
            criteria.id = id;
            var listObj = repository.Find(criteria);

            if (listObj.Count == 0)
            {
                return null;
            }

            return listObj.First();
        }

        public virtual ErsEraRepository GetErsEraRepository()
        {
            return new ErsEraRepository();
        }

        public virtual ErsEraCriteria GetErsEraCriteria()
        {
            return new ErsEraCriteria();
        }

        public virtual ErsEra GetErsEra()
        {
            return new ErsEra();
        }

        public virtual ErsEra GetErsEraWithParameter(Dictionary<string, object> parameter)
        {
            var objEra = this.GetErsEra();
            objEra.OverwriteWithParameter(parameter);
            return objEra;
        }

        public virtual ErsZipRepository GetErsZipRepository()
        {
            return new ErsZipRepository();
        }

        public virtual ErsZipCriteria GetErsZipCriteria()
        {
            return new ErsZipCriteria();
        }

        public virtual ErsZip GetErsZip()
        {
            return new ErsZip();
        }

        public virtual CheckNumericFromToStgy GetCheckNumericFromToStgy()
        {
            return new CheckNumericFromToStgy();
        }

        public virtual ValidateCommonNameCodeStgy GetValidateCommonNameCodeStgy()
        {
            return new ValidateCommonNameCodeStgy();
        }

        public virtual ValidateZipPrefStgy GetValidateZipPrefStgy()
        {
            return new ValidateZipPrefStgy();
        }

        public virtual IsOverseasZipSpec GetIsOverseasZipSpec()
        {
            return new IsOverseasZipSpec();
        }
    }
}
