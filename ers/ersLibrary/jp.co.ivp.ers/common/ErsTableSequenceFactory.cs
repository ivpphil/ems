﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.common
{
    public class ErsTableSequenceFactory
    {
        public ErsTableSequence GetErsTableSequence()
        {
            return new ErsTableSequence();
        }

        public ErsTableSequenceRepository GetErsTableSequenceRepository(string tableName)
        {
            return new ErsTableSequenceRepository(tableName);
        }

        public ErsTableSequenceCriteria GetErsTableSequenceCriteria()
        {
            return new ErsTableSequenceCriteria();
        }

        public long? GetLastValueBySequenceName(string sequenceName)
        {
            var entity = this.GetErsTableSequenceRepository(sequenceName).FindSingle(null);

            if (entity != null)
                return entity.last_value;

            return null;
        }
    }
}
