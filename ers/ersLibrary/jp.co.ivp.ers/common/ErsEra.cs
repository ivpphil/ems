﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.common
{
    public class ErsEra
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public int? year { get; set; }
        public string era { get; set; }
    }
}
