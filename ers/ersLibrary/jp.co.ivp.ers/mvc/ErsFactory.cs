﻿using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers
{
    public class ErsFactory
    {
        /// <summary>
        /// カート関連クラスのFactory
        /// </summary>
        public static basket.ErsBasketFactory ersBasketFactory { get; set; }

        /// <summary>
        /// 商品関連クラスのFactory
        /// </summary>
        public static merchandise.ErsMerchandiseFactory ersMerchandiseFactory { get; set; }

        /// <summary>
        /// 会員関連クラスのFactory
        /// </summary>
        public static member.ErsMemberFactory ersMemberFactory { get; set; }

        /// <summary>
        /// メール関連クラスのFactory
        /// </summary>
        public static sendmail.ErsMailFactory ersMailFactory { get; set; }

        /// <summary>
        /// セッション関連クラスのFactory
        /// </summary>
        public static state.ErsSessionStateFactory ersSessionStateFactory { get; set; }

        /// <summary>
        /// ViewServiceクラスのFactory
        /// </summary>
        public static viewService.ErsViewServiceFactory ersViewServiceFactory { get; set; }

        /// <summary>
        /// ユーティリティ・設定関連クラスのFactory
        /// </summary>
        public static ErsUtilityFactory ersUtilityFactory { get; set; }

        /// <summary>
        /// 伝票関連クラスのFactory
        /// </summary>
        public static order.ErsOrderFactory ersOrderFactory { get; set; }

        /// <summary>
        /// お届け先関連クラスのFactory
        /// </summary>
        public static member.ErsAddressInfoFactory ersAddressInfoFactory { get; set; }

        /// <summary>
        /// ポイント関連クラスのFactory
        /// </summary>
        public static member.ErsPointHistoryFactory ersPointHistoryFactory { get; set; }

        /// <summary>
        /// 管理者情報関連クラスのFactory
        /// </summary>
        public static administrator.ErsAdministratorFactory ersAdministratorFactory { get; set; }

        /// <summary>
        /// Cts Operator Factory
        /// </summary>
        public static cts_operators.ErsCtsOperatorFactory ersCtsOperatorFactory { get; set; }

        /// <summary>
        /// Cts Information Factory
        /// </summary>
        public static information.ErsCtsInformationFactory ersCtsInformationFactory { get; set; }

        /// <summary>
        /// Cts Order Factory
        /// </summary>
        public static ctsorder.ErsCtsOrderFactory ersCtsOrderFactory { get; set; }

        /// <summary>
        /// Search Order Factory
        /// </summary>
        public static search.ErsCtsSearchFactory ersCtsSearchFactory { get; set; }

        /// Search FAQ
        /// </summary>
        public static faq.ErsCtsFAQFactory ersCtsFAQFactory { get; set; }

        /// <summary>
        /// Inquiry
        /// </summary>
        public static Inquiry.ErsCtsInquiryFactory ersCtsInquiryFactory { get; set; }

        /// <summary>
        /// Common
        /// </summary>
        public static common.ErsCommonFactory ersCommonFactory { get; set; }

        /// <summary>
        /// Merge
        /// </summary>
        public static merge.ErsCtsMergeFactory ersCtsMergeFactory { get; set; }

        /// <summary>
        /// Target Factory
        /// </summary>
        public static target.ErsTargetFactory ersTargetFactory { get; set; }

        /// <summary>
        /// Document Bundle Factory
        /// </summary>
        public static doc_bundle.ErsDocBundleFactory ersDocBundleFactory { get; set; }

        /// <summary>
        /// coupon　Factory
        /// </summary>
        public static coupon.ErsCouponFactory ersCouponFactory { get; set; }

        /// <summary>
        /// atMail Factory
        /// </summary>
        public static atmail.ErsAtMailFactory ErsAtMailFactory { get; set; }

        /// <summary>
        /// batch Factory
        /// </summary>
        public static batch.ErsBatchFactory ersBatchFactory { get; set; }

        //public static reports.ErsCtsRepProdFactory ersCtsRepProdFactory { get; set; }


        public static reports.ErsCtsRepFactory ersCtsRepFactory { get; set; }

        /// <summary>
        /// Direction
        /// </summary>
        public static direction.ErsCtsDirectionFactory ersCtsDirectionFactory { get; set; }

        /// <summary>
        /// ERS_CMS
        /// </summary>
        public static contents.ErsContentsFactory ersContentsFactory { get; set; }

        public static stepmail.ErsStepMailFactory ersStepMailFactory { get; set; }

        public static step_scenario.ErsStepScenarioFactory ersStepScenarioFactory { get; set; }

        public static warehouse.ErsWarehouseFactory ersWarehouseFactory { get; set; }

        public static summary.ErsSummaryFactory ersSummaryFactory { get; set; }

        public static ranking.ErsRankingFactory ersRankingFactory { get; set; }

        public static lp.ErsLpFactory ersLpFactory { get; set; }

        public static cts_wishlist.ErsCtsWishListFactory ersCtsWishListFactory { get; set; }

        public static update_specified_column.ErsUpdateSpecifiedColumnFactory ersUpdateSpecifiedColumnFactory { get; set; }

        public static common.ErsTableSequenceFactory ersTableSequenceFactory { get; set; }

        public static language.ErsLanguageFactory ersLanguageFactory { get; set; }

        public static request.ErsRequestFactory ersRequestFactory { get; set; }

        public static employee.ErsEmployeeFactory ersEmployeeFactory { get; set; }

        public static mdb.ErsMDBFactory ersMDBFactory { get; set; }

        public static Pdf.ErsPdfFactory ersPdfFactory { get; set; }

        public static projects.ErsPcodeFactory ersPcodeFactory { get; set; }

        public static job.ErsJobFactory ersJobFactory { get; set; }

    }
}
