﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc
{
    public class RedirectSiteAttribute
        : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            this.DeteminIfRedirect(filterContext);
        }

        private void DeteminIfRedirect(ActionExecutingContext filterContext)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (setup.redirect_site_types.Count() == 0)
            {
                return;
            }

            var mobileCommon = ErsFactory.ersUtilityFactory.getErsMobileCommon();

            var CurrentExecutionFilePath = filterContext.HttpContext.Request.AppRelativeCurrentExecutionFilePath.Replace("~/", string.Empty);

            var site_type = mobileCommon.GetSiteType();

            if (!setup.redirect_site_types.Contains(site_type))
            {
                // リダイレクト先指定されていない場合は処理しない
                return;
            }

            //異なるサイトにデバイスでアクセスされたら、リダイレクト
            if (setup.site_type != site_type)
            {
                string redirectUrl;
                if (!filterContext.HttpContext.Request.IsSecureConnection)
                {
                    redirectUrl = SiteTypeVariables.GetNorUrl(site_type, setup.site_id);
                }
                else
                {
                    redirectUrl = SiteTypeVariables.GetSecUrl(site_type, setup.site_id);
                }

                var queryString = string.Empty;
                foreach (string key in filterContext.HttpContext.Request.QueryString.Keys)
                {
                    queryString += "&" + key + "=" + filterContext.HttpContext.Request[key];
                }
                if (queryString.HasValue())
                {
                    queryString = "?" + queryString.Substring(1);
                }

                filterContext.Result = new RedirectResult(redirectUrl + CurrentExecutionFilePath + queryString);
            }
        }
    }
}