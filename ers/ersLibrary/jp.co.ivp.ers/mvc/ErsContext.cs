﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections.Generic;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using System.Web.Mvc;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// リクエスト固有のstaticな値を保持
    /// </summary>
    public class ErsContext
    {

        /// <summary>
        /// セッション情報
        /// </summary>
        public static ErsState sessionState
        {
            get { return _sessionState; }
            set { _sessionState = value; }
        }

        /// <summary>
        /// sets and gets session state
        /// </summary>
        protected static ErsState _sessionState
        {
            get
            {
                return (ErsState)ErsCommonContext.GetPooledObject("_sessionState");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_sessionState", value);
            }
        }


        /// <summary>
        /// セッションチェックを行う
        /// </summary>
        public static void SessionCheck(AuthorizationContext clearTargetFilterContext = null)
        {
            EnumUserState state = ((ISession)ErsContext.sessionState).getUserState();

            if (state != EnumUserState.LOGIN)
            {
                if (clearTargetFilterContext != null)
                {
                    clearTargetFilterContext.Controller.ViewData.ModelState.Clear();
                }
                //ログイン状態をチェックし、ログイン状態でなかったらエラー
                throw new ErsException("10203", ErsFactory.ersUtilityFactory.getSetup().nor_url);
            }
        }

    }
}
