﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;
using jp.co.ivp.ers.util;
using System.Web.Routing;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public abstract class ErsProcessCompletionAttributeBase
        : ActionFilterAttribute
    {
        public EnumHandlingMode mode { get; set; }

        public ErsProcessCompletionAttributeBase(string completedPageKeys)
        {
            this.completedPageKeys = completedPageKeys;
        }

        protected string completedPageKeys { get; set; }

        internal ErsProcessCompletionModel GetPostedFormValue(ControllerBase controllerBase)
        {
            var key = "ErsProcessCompletionAttributeBase.ErsProcessCompletionModel";
            var model = (ErsProcessCompletionModel)ErsCommonContext.GetPooledObject(key);
            if (model != null)
            {
                return model;
            }

            var controller = (ErsControllerBase)controllerBase;
            model = controller.GetBindedModel<ErsProcessCompletionModel>();
            if (!model.IsValid)
            {
                model = new ErsProcessCompletionModel();
            }

            ErsCommonContext.SetPooledObject(key, model);

            if (model.process_completion_check_ransu == null)
            {
                model.process_completion_check_ransu = new string[] { };
            }

            return model;
        }

        /// <summary>
        /// before the action method executes
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var model = this.GetPostedFormValue(filterContext.Controller);

            if (mode == EnumHandlingMode.RESET)
            {
                //既存の乱数があれば削除
                model.process_completion_check_ransu = this.DeleteCompletionFormValue(model.process_completion_check_ransu);

                var newRansu = ErsFactory.ersSessionStateFactory.GetObtainRansuCacheStgy().GetCacheRansu(this.completedPageKeys);
                model.process_completion_check_ransu = model.process_completion_check_ransu.Concat(new[] { newRansu }).ToArray();
            }
            else if (mode == EnumHandlingMode.COMPLETION || mode == EnumHandlingMode.CHECK)
            {
                this.CheckCompletion(model, filterContext);
            }

            this.SetCompletionFormValue(model, (ErsControllerBase)filterContext.Controller);

            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// 完了エラー時の処理を定義する。
        /// </summary>
        abstract public void OnCheckCompletionError();

        /// <summary>
        ///  after the action method executes
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //not execute when an error occurred.
            if (!filterContext.Controller.ViewData.ModelState.IsValid || filterContext.Exception != null)
            {
                base.OnActionExecuted(filterContext);
                return;
            }


            if (mode == EnumHandlingMode.COMPLETION)
            {
                var model = this.GetPostedFormValue(filterContext.Controller);

                model.process_completion_check_ransu = this.DeleteCompletionFormValue(model.process_completion_check_ransu);
                this.SetCompletionFormValue(model, (ErsControllerBase)filterContext.Controller);
            }

            base.OnActionExecuted(filterContext);
        }

        internal void SetCompletionFormValue(ErsProcessCompletionModel model, ErsControllerBase controller)
        {
            var key = ErsOutputHiddenAttribute.PROCESS_COMPLETION_CHECK_RANSU_KEY;
            model.SetOutputHidden(key, true);
            controller.AddModelToView(key, model);
        }

        internal virtual void CheckCompletion(ErsProcessCompletionModel model, ActionExecutingContext filterContext)
        {
            foreach (var ransu in model.process_completion_check_ransu)
            {
                if (ransu.StartsWith(this.completedPageKeys))
                {
                    var repository = ErsFactory.ersSessionStateFactory.GetErsCacheRepository();
                    var criteria = ErsFactory.ersSessionStateFactory.GetErsCacheCriteria();
                    criteria.ransu = ransu;
                    criteria.key = this.completedPageKeys;

                    if (repository.GetRecordCount(criteria) > 0)
                    {
                        return;
                    }
                }
            }

            this.OnCheckCompletionError();
        }

        private string[] DeleteCompletionFormValue(string[] process_completion_check_ransu)
        {
            //既存の乱数を削除
            var listRansu = process_completion_check_ransu.ToList();
            foreach (var ransu in process_completion_check_ransu)
            {
                if (ransu.StartsWith(this.completedPageKeys))
                {
                    this.DeleteRansu(ransu);
                    listRansu.Remove(ransu);
                }
            }
            return listRansu.ToArray();
        }

        protected void DeleteRansu(string completionFormValue)
        {
            var repository = ErsFactory.ersSessionStateFactory.GetErsCacheRepository();
            var criteria = ErsFactory.ersSessionStateFactory.GetErsCacheCriteria();
            criteria.ransu = completionFormValue;
            criteria.key = completedPageKeys;

            repository.Delete(criteria);
        }
    }
}
