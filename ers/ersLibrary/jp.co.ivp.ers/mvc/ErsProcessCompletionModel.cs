﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace jp.co.ivp.ers.mvc
{
    class ErsProcessCompletionModel
        : ErsModelBase
    {
        [ErsSchemaValidation("cache_t.ransu", isArray = true)]
        [ErsOutputHidden(ErsOutputHiddenAttribute.PROCESS_COMPLETION_CHECK_RANSU_KEY)]
        public string[] process_completion_check_ransu { get; set; }
    }
}
