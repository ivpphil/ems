﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc
{
    /// <summary>
    /// The attribute class that specify session or cookie expiration duration
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class SourceExpirationAttribute
        : Attribute
    {
        public SourceExpirationAttribute(string configKey)
        {
            this.configKey = configKey;
        }

        public string configKey { get; private set; }
    }
}
