﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Holds values of addressbook_t record.
    /// Inherit from repository
    /// </summary>
    public class ErsAddressInfo
        : ErsRepositoryEntity
    {
        /// <summary>
        /// ID
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 会員コード
        /// </summary>
        public virtual string mcode { get; set; }

        /// <summary>
        /// アドレス帳名
        /// </summary>
        public virtual string address_name { get; set; }

        /// <summary>
        /// 住所情報
        /// </summary>
        public virtual string add_lname { get; set; }
        public virtual string add_fname { get; set; }
        public virtual string add_lnamek { get; set; }
        public virtual string add_fnamek { get; set; }
        public virtual string add_compname { get; set; }
        public virtual string add_compnamek { get; set; }
        public virtual string add_division { get; set; }
        public virtual string add_divisionk { get; set; }
        public virtual string add_tlname { get; set; }
        public virtual string add_tfname { get; set; }
        public virtual string add_tlnamek { get; set; }
        public virtual string add_tfnamek { get; set; }
        public virtual string add_tel { get; set; }
        public virtual string add_fax { get; set; }
        public virtual string add_zip { get; set; }
        public virtual string add_address { get; set; }
        public virtual string add_taddress { get; set; }
        public virtual string add_maddress { get; set; }
        public virtual int? add_pref { get; set; }
        public virtual string add_pref_name { get; set; }

        /// <summary>
        /// 登録時刻
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新時刻
        /// </summary>
        public virtual DateTime? utime { get; set; }

        public virtual int? site_id { get; set; }
    }
}
