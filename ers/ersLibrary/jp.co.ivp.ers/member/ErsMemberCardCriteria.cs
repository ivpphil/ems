﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Represents the search condition of member_card_t
    /// inherit from criteria
    /// </summary>
    public class ErsMemberCardCriteria
        : Criteria
    {
        /// <summary>
        /// search condition for active
        /// </summary>
        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_card_t.active", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for mcode
        /// </summary>
        public string mcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_card_t.mcode", value, Operation.EQUAL));
            }
        }

        public string card_mcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_card_t.card_mcode", value, Operation.EQUAL));
            }
        }

        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_card_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for card_sequence
        /// </summary>
        public string card_sequence
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_card_t.card_sequence", value, Operation.EQUAL));
            }
        }

        internal void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("member_card_t.id", orderBy);
        }

        public void SetHasRegular()
        {
            var sql = "EXISTS "
                + "(SELECT 1 "
                + "FROM regular_detail_t "
                + "INNER JOIN regular_t ON regular_t.id = regular_detail_t.regular_id "
                + "WHERE (regular_detail_t.delete_date IS NULL OR regular_detail_t.next_date < regular_detail_t.delete_date) AND regular_detail_t.mcode = member_card_t.mcode "
                + ")";

            this.Add(Criteria.GetUniversalCriterion(sql));
        }

        public EnumCardUpdateStatus? update_status
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_card_t.update_status", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        /// <param name="local_site_id"></param>
        /// <param name="common_site_id"></param>
        public int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_card_t.site_id", value, Operation.EQUAL));
            }
        }
    }
}
