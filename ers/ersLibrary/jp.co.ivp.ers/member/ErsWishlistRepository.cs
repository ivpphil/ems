﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.db;


namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Provides methods to merchandise wish list with CRUD
    /// inherit from ErsRepository
    /// </summary>
    public class ErsWishlistRepository
        : ErsRepository<ErsWishlist>
    {
        public ErsWishlistRepository()
            : base("wishlist_t")
        {
        }

        /// <summary>
        /// search records from wishlist_t
        /// </summary>
        /// <param name="criteria">ErsMerchandiseInWishlistCriteria</param>
        /// <returns>return a list of result</returns>
        public override IList<ErsWishlist> Find(Criteria criteria)
        {
            var ret = new List<ErsWishlist>();

            var listRecord = ersDB_table.gSelect(criteria);

            foreach (var item in listRecord)
            {
                var merchandise = ErsFactory.ersMemberFactory.GetErsWishlistWithParameter(item);

                if (merchandise != null)
                    ret.Add(merchandise);
            }

            return ret;
        }

        public virtual IEnumerable<ErsMerchandise> FindWishListItemList(Criteria groupCriteria)
        {
            var ret = new List<ErsMerchandise>();

            var wishlistSerchSpec = ErsFactory.ersMemberFactory.GetWishlistSearchSpecification();
            var listRecord = wishlistSerchSpec.GetSearchData(groupCriteria);

            foreach (var record in listRecord)
            {
                var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandise();
                merchandise.OverwriteWithParameter(record);
                ret.Add(merchandise);
            }

            return ret;
        }
    }
}
