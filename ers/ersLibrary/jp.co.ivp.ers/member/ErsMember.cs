﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.order.specification;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Holds values of member_t record.
    /// Inherit from repository
    /// </summary>
    public class ErsMember
        : ErsRepositoryEntity, IIsMonitorSpecDatasource
    {
        /// <summary>
        /// default mcode
        /// </summary>
        public const string DEFAUTL_MCODE = "0";

        public ErsMember()
        {
        }

        public override int? id { get; set; }

        /// <summary>
        /// member id 
        /// </summary>
        public virtual string mcode { get; set; }
        /// <summary>
        /// email address
        /// </summary>
        public virtual string email { get; set; }
        public virtual string email3 { get; set; }
        public virtual string ccode
        {
            get
            {
                if (string.IsNullOrEmpty(this._ccode))
                {
                    return this._ccode;
                }
                return this._ccode.ToUpper();
            }
            set
            {
                this._ccode = value;
            }
        }
        private string _ccode;

        /// <summary>
        /// 確認メール受信形式
        /// </summary>
        public virtual EnumMformat? mformat { get; set; }

        public virtual string passwd { get; set; }
        public virtual DateTime? birth { get; set; }
        public virtual EnumSex? sex { get; set; }

        public virtual int? sale { get; set; }
        public virtual EnumMFlg? m_flg { get; set; }
        public virtual string memo { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual short? blacklist { get; set; }

        /// <summary>
        /// その他項目
        /// </summary>
        public virtual string country { get; set; }

        /// <summary>
        /// 住所情報
        /// </summary>
        public virtual string lname { get; set; }
        public virtual string fname { get; set; }
        public virtual string lnamek { get; set; }
        public virtual string fnamek { get; set; }
        public virtual string compname { get; set; }
        public virtual string compnamek { get; set; }
        public virtual string division { get; set; }
        public virtual string divisionk { get; set; }
        public virtual string tlname { get; set; }
        public virtual string tfname { get; set; }
        public virtual string tlnamek { get; set; }
        public virtual string tfnamek { get; set; }
        public virtual string tel { get; set; }
        public virtual string fax { get; set; }
        public virtual string zip { get; set; }
        public virtual string address { get; set; }
        public virtual string taddress { get; set; }
        public virtual string maddress { get; set; }
        public virtual int? pref { get; set; }

        /// <summary>
        /// Job information
        /// </summary>
        public virtual int? job { get; set; }

        /// <summary>
        /// questions information
        /// </summary>
        public int? ques { get; set; }

        public string ans { get; set; }

        public virtual int? age_code { get; set; }

        public virtual EnumPmFlg? pm_flg { get; set; }

        public virtual EnumDmFlg? dm_flg { get; set; }
        public virtual EnumOutBoundFlg? out_bound_flg { get; set; }

        public DateTime? last_sale_date { get; set; }
        public int? sale_times { get; set; }

        public EnumDeleted? deleted { get; set; }

        public int? rank { get; set; }

        public EnumAccountStatus account_status { get; set; }
        public int login_try_count { get; set; }
        public int? site_id { get; set; }
    }
}
