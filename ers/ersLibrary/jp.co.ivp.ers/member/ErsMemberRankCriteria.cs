﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.member
{
    public class ErsMemberRankCriteria
        : Criteria
    {
        public string mcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_t.mcode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for active
        /// </summary>
        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_t.active", value, Operation.EQUAL));
            }
        }

        public int? rank
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_t.rank", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public int? site_id_for_admin
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID 非一致 [Site ID(Not equal)]
        /// </summary>
        public int? site_id_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_t.site_id", value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// サイトIDにおいてのソート [For sorting of records using site ID]
        /// </summary> 
        public void SetOrderBySiteId(OrderBy orderBy)
        {
            AddOrderBy("member_rank_t.site_id", orderBy);
        }

        public void NonRankMemberPurchaseTimes(int lowerLimit, DateTime recordDateFrom, DateTime recordDateTo, bool member_rank_centralization, int? site_id)
        {
            var strSQL = " COALESCE";
            strSQL += " ((SELECT COUNT(d_master_t.mcode) ";
            strSQL += "FROM d_master_t ";
            strSQL += "INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no ";
            strSQL += "WHERE d_master_t.intime BETWEEN :intime_from AND :intime_to ";
            strSQL += "AND ds_master_t.order_status <> ANY(:arrCancelStatus) and d_master_t.mcode = member_rank_t.mcode ";
            if(!member_rank_centralization)
            {
                strSQL += "AND d_master_t.site_id = " + site_id + " ";
            }
            strSQL += "GROUP BY d_master_t.mcode),0) ";
            strSQL += " < :lowerLimit";


            this.Add(Criteria.GetUniversalCriterion(strSQL, new Dictionary<string, object>(){
                {"intime_from", recordDateFrom}, 
                {"intime_to", recordDateTo}, 
                {"arrCancelStatus", ErsFactory.ersOrderFactory.GetErsOrderCriteria().CancelStatusArray.Select((value) => (int)value)}, 
                {"lowerLimit", lowerLimit}}));
        }

        public void RankMemberPurchaseTimes(int? value_from, int? value_to, DateTime recordDateFrom, DateTime recordDateTo, bool member_rank_centralization, int? site_id)
        {

            var strSQL = " COALESCE";
            strSQL += " ((SELECT COUNT(d_master_t.mcode) ";
            strSQL += "FROM d_master_t ";
            strSQL += "INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no ";
            strSQL += "WHERE d_master_t.intime BETWEEN :intime_from AND :intime_to ";
            strSQL += "AND ds_master_t.order_status <> ANY(:arrCancelStatus) and d_master_t.mcode = member_t.mcode ";
            if (!member_rank_centralization)
            {
                strSQL += "AND d_master_t.site_id = " + site_id + " ";
            }
            strSQL += "GROUP BY d_master_t.mcode),0) ";
            strSQL += " BETWEEN :value_from AND :value_to";

            this.Add(Criteria.GetUniversalCriterion(strSQL, new Dictionary<string, object>(){
                {"intime_from", recordDateFrom}, 
                {"intime_to", recordDateTo}, 
                {"arrCancelStatus", ErsFactory.ersOrderFactory.GetErsOrderCriteria().CancelStatusArray.Select((value) => (int)value)}, 
                {"value_from", value_from},
                {"value_to", value_to}}));
        }
        public void RankMemberPurchaseTimesUpdate(int? value_from, int? value_to, DateTime recordDateFrom, DateTime recordDateTo, bool member_rank_centralization, int? site_id)
        {
            var strSQL = " COALESCE";
            strSQL += " ((SELECT COUNT(d_master_t.mcode) ";
            strSQL += "FROM d_master_t ";
            strSQL += "INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no ";
            strSQL += "WHERE d_master_t.intime BETWEEN :intime_from AND :intime_to ";
            strSQL += "AND ds_master_t.order_status <> ANY(:arrCancelStatus) and d_master_t.mcode = member_rank_t.mcode ";
            if (!member_rank_centralization)
            {
                strSQL += "AND d_master_t.site_id = " + site_id + " ";
            }
            strSQL += "GROUP BY d_master_t.mcode),0) ";
            strSQL += " BETWEEN :value_from AND :value_to";

            this.Add(Criteria.GetUniversalCriterion(strSQL, new Dictionary<string, object>(){
                {"intime_from", recordDateFrom}, 
                {"intime_to", recordDateTo}, 
                {"arrCancelStatus", ErsFactory.ersOrderFactory.GetErsOrderCriteria().CancelStatusArray.Select((value) => (int)value)}, 
                {"value_from", value_from},
                {"value_to", value_to}}));
        }

        public void NonRankMemberPurchaseTotal(int lowerLimit, DateTime recordDateFrom, DateTime recordDateTo, bool member_rank_centralization, int? site_id)
        {
            var strSQL = " COALESCE";
            strSQL += " ((SELECT SUM(d_master_t.total) ";
            strSQL += "FROM d_master_t ";
            strSQL += "INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no ";
            strSQL += "WHERE d_master_t.intime BETWEEN :intime_from AND :intime_to ";
            strSQL += "AND ds_master_t.order_status <> ANY(:arrCancelStatus) and d_master_t.mcode = member_rank_t.mcode ";
            if (!member_rank_centralization)
            {
                strSQL += "AND d_master_t.site_id = " + site_id + " ";
            }
            strSQL += "GROUP BY d_master_t.mcode),0) ";
            strSQL += " < :lowerLimit";

            this.Add(Criteria.GetUniversalCriterion(strSQL, new Dictionary<string, object>(){
                {"intime_from", recordDateFrom}, 
                {"intime_to", recordDateTo}, 
                {"arrCancelStatus", ErsFactory.ersOrderFactory.GetErsOrderCriteria().CancelStatusArray.Select((value) => (int)value)}, 
                {"lowerLimit", lowerLimit}}));
        }

        public void RankMemberPurchaseTotal(int? value_from, int? value_to, DateTime recordDateFrom, DateTime recordDateTo, bool member_rank_centralization, int? site_id)
        {
            var strSQL = " COALESCE";
            strSQL += " ((SELECT SUM(d_master_t.total)  ";
            strSQL += "FROM d_master_t ";
            strSQL += "INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no ";
            strSQL += "WHERE d_master_t.intime BETWEEN :intime_from AND :intime_to ";
            strSQL += "AND ds_master_t.order_status <> ANY(:arrCancelStatus) and d_master_t.mcode = member_t.mcode "; if (!member_rank_centralization)
            {
                strSQL += "AND d_master_t.site_id = " + site_id + " ";
            }

            strSQL += "GROUP BY d_master_t.mcode),0) ";
            strSQL += " BETWEEN :value_from AND :value_to";

            this.Add(Criteria.GetUniversalCriterion(strSQL, new Dictionary<string, object>(){
                {"intime_from", recordDateFrom}, 
                {"intime_to", recordDateTo}, 
                {"arrCancelStatus", ErsFactory.ersOrderFactory.GetErsOrderCriteria().CancelStatusArray.Select((value) => (int)value)}, 
                {"value_from", value_from},
                {"value_to", value_to}}));
        }

        public void RankMemberPurchaseTotalUpdate(int? value_from, int? value_to, DateTime recordDateFrom, DateTime recordDateTo, bool member_rank_centralization, int? site_id)
        {
            var strSQL = " COALESCE";
            strSQL += " ((SELECT SUM(d_master_t.total) ";
            strSQL += "FROM d_master_t ";
            strSQL += "INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no ";
            strSQL += "WHERE d_master_t.intime BETWEEN :intime_from AND :intime_to ";
            strSQL += "AND ds_master_t.order_status <> ANY(:arrCancelStatus) and d_master_t.mcode = member_rank_t.mcode "; if (!member_rank_centralization)
            {
                strSQL += "AND d_master_t.site_id = " + site_id + " ";
            }

            strSQL += "GROUP BY d_master_t.mcode),0) ";
            strSQL += " BETWEEN :value_from AND :value_to";

            this.Add(Criteria.GetUniversalCriterion(strSQL, new Dictionary<string, object>(){
                {"intime_from", recordDateFrom}, 
                {"intime_to", recordDateTo}, 
                {"arrCancelStatus", ErsFactory.ersOrderFactory.GetErsOrderCriteria().CancelStatusArray.Select((value) => (int)value)}, 
                {"value_from", value_from},
                {"value_to", value_to}}));
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 退会状況による検索 (Fileter for member_t.deleted  )
        /// </summary>
        public EnumDeleted deleted
        {
            set
            {
                var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
                criteria.deleted = value;
                this.Add(criteria);
            }
        }

        public void ignoreMonitor()
        {
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            criteria.ignoreMonitor();
            this.Add(criteria);
        }
    }
}
