﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member
{
    public class ErsQuesRepository
        : ErsRepository<ErsQues>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsQuesRepository()
            : base("ques_t")
        {
        }
    }
}
