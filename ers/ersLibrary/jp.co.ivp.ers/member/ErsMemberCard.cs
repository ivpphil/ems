﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Holds values of member_card_t record.
    /// Inherit from repository
    /// </summary>
    public class ErsMemberCard
        : ErsRepositoryEntity
    {

        public override int? id { get; set; }
        /// <summary>
        /// member id
        /// </summary>
        public virtual string mcode { get; set; }

        public virtual string card_mcode { get; set; }

        /// <summary>
        /// bank card number
        /// </summary>
        public virtual string card_sequence { get; set; }

        public virtual EnumActive? active { get; set; }

        public EnumCardUpdateStatus? update_status { get; set; }

        public int? site_id { get; set; }
    }
}
