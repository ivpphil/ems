﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// obtains table name ransu_t and pass_ransu_t
    /// </summary>
    public class ErsPasswodReminderService
    {
        /// <summary>
        /// 乱数生成
        /// </summary>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual string MakePassRansu(string mcode)
        {
            var objDB = ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(ErsWebDatabaseFactory.GetCloudConnectionStrings());
            var ransu = ErsFactory.ersSessionStateFactory.GetErsRansu();

            ransu.ransu = ErsFactory.ersSessionStateFactory.GetObtainRansuStgy().CreateNewRansu();
            ransu.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            ErsFactory.ersSessionStateFactory.GetErsRansuRepository(objDB).Insert(ransu);

            return ransu.ransu;
        }

        /// <summary>
        /// SSL乱数生成
        /// </summary>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual void MakeSSLPassRansu(string mcode)
        {
            ((ISession)ErsContext.sessionState).getNewSSLransu(mcode);
        }

        /// <summary>
        /// パスリム乱数インサート
        /// </summary>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual void InsertPassRansu(string ransu, string mcode)
        {
            var repository = ErsFactory.ersSessionStateFactory.GetErsPassRansuRepository();
            var objPassRansu = ErsFactory.ersSessionStateFactory.GetErsPassRansu();
            objPassRansu.ransu = ransu;
            objPassRansu.mcode = mcode;
            objPassRansu.intime = DateTime.Now;
            objPassRansu.utime  = DateTime.Now;
            repository.Insert(objPassRansu);
        }

        /// <summary>
        /// パスリム乱数チェック
        /// </summary>
        /// <param name="ssl_ransu"></param>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual Boolean isValidPassRimRansu(string ransu)
        {
            var repository = ErsFactory.ersSessionStateFactory.GetErsPassRansuRepository();
            var criteria = ErsFactory.ersSessionStateFactory.GetErsPassRansuCriteria();
            criteria.ransu = ransu;
            return (repository.GetRecordCount(criteria) == 0);
        }


        /// <summary>
        /// 接続乱数チェック
        /// </summary>
        /// <param name="ssl_ransu"></param>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual Boolean isValidRansu(string ransu)
        {
            return (ErsFactory.ersSessionStateFactory.GetObtainInvalidRansuCountStgy().Obtain(ransu) == 1);
        }

        /// <summary>
        /// 複合乱数チェック
        /// </summary>
        /// <param name="ssl_ransu"></param>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual Boolean isValidPasswordRansu(string ransu)
        {
            return (this.isValidRansu(ransu) && this.isValidPassRimRansu(ransu));
        }
    }
}
