﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.state;

namespace jp.co.ivp.ers.member.strategy
{
    public class RetrieveMemberRankStgy
    {
        public int? RetrieveWithSession()
        {
            if (((ErsSessionState)ErsContext.sessionState).IsLoginButNotSecure())
            {
                var mcode = ((ErsSessionState)ErsContext.sessionState).GetDecodedMcode();
                return this.Retrieve(mcode);
            }

            return null;
        }

        public int? Retrieve(string mcode)
        {
            if (!mcode.HasValue())
            {
                return null;
            }

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRankRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberRankCriteria();
            criteria.mcode = mcode;
            criteria.active = EnumActive.Active;

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (setup.member_rank_centralization)
            {
                criteria.site_id_for_admin = (int)EnumSiteId.COMMON_SITE_ID;
            }
            else
            {
                criteria.site_id_for_admin = ErsFactory.ersUtilityFactory.getSetup().site_id;
            }

            var listMemberRank = repository.Find(criteria);
            if (listMemberRank.Count == 1)
            {
                return listMemberRank.First().rank;
            }

            return null;
        }
    }
}
