﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.member.strategy
{
    /// <summary>
    /// Checking for password and confirmation
    /// </summary>
    public class CheckPasswdConfirmStgy
    {
        /// <summary>
        /// check if password and confirm password is the same
        /// </summary>
        /// <param name="passwd">password</param>
        /// <param name="passwd_confirm">password confirmation</param>
        /// <returns>Returns error if the password and password confirmation is not equal</returns>
        public virtual ValidationResult CheckPasswdConfirm(string passwd, string passwd_confirm)
        {
            if (passwd != passwd_confirm)
            {
                return new ValidationResult(string.Format(ErsResources.GetMessage("10102"), ErsResources.GetFieldName("passwd"), ErsResources.GetFieldName("passwd_confirm")), new[] { "passwd", "passwd_confirm" });
            }

            return null;
        }

    }
}
