﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.member.strategy
{
    /// <summary>
    /// コンビニ決済の定期伝票がある場合、メールアドレスの登録を必須とする。
    /// </summary>
    public class CheckEmailRequiredStgy
    {
        /// <summary>
        /// コンビニ決済の定期伝票がある場合、メールアドレスの登録を必須とする。
        /// </summary>
        /// <param name="mcode"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public ValidationResult Check(string mcode, string email)
        {
            if (!mcode.HasValue() || email.HasValue())
            {
                return null;
            }

            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderCriteria();
            criteria.SetActiveOnly();
            criteria.mcode = mcode;
            criteria.pay = EnumPaymentType.CONVENIENCE_STORE;
            if (repository.GetRecordCount(criteria) == 0)
            {
                return null;
            }

            //コンビニ決済の定期がある場合は、emailは必須
            return new ValidationResult(ErsResources.GetMessage("20239"), new[] { "email" });
        }
    }
}
