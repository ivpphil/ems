﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member.strategy
{
    /// <summary>
    /// セキュリティアンサー一致チェック
    /// </summary>
    public class CheckQuesConfirmStgy
    {
        /// <summary>
        /// セキュリティアンサー一致チェック
        /// </summary>
        /// <param name="mcode">member id</param>
        /// <param name="ans">answer</param>
        /// <param name="ques">questions</param>
        /// <returns>Returns error if the records is not equal 1</returns>
        public virtual ValidationResult CheckQuesConfirm(string mcode, string ans, int? ques)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();

            criteria.mcode = mcode;
            criteria.ans = ans;
            criteria.ques = ques;
            criteria.deleted = EnumDeleted.NotDeleted;

            if (repository.GetRecordCount(criteria) != 1)
            {
                return new ValidationResult(ErsResources.GetMessage("10025", ErsResources.GetFieldName("ans")), new[] { "ans" });
            }

            return null;

        }

    }
}
