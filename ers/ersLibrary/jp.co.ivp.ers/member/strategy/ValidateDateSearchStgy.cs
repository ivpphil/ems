﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member.strategy
{
    public class ValidateDateSearchStgy
    {
        public virtual IEnumerable<ValidationResult> Validate(string from, string to, string key)
        {
            DateTime date_from = DateTime.Now;
            DateTime date_to = DateTime.Now;

            //日付形式チェック
            if (!string.IsNullOrEmpty(from))
            {
                if (!DateTime.TryParse(from, out date_from))
                {
                    yield break;
                }

                else if (!this.RangeChk(date_from))
                {
                    yield break;
                }
            }

            if (!string.IsNullOrEmpty(to))
            {
                if (!DateTime.TryParse(to, out date_to))
                {
                    yield break;
                }

                else if (!this.RangeChk(date_to))
                {
                    yield break;
                }
            }

            //超過チェック
            if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
            {
                if (date_from > date_to && date_from.Year < 3000 && date_from.Year > 1000 && date_to.Year < 3000 && date_to.Year > 1000)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10023", ErsResources.GetFieldName(key) + "To", ErsResources.GetFieldName(key) + "From"), new[] { key });
                    yield break;
                }
            }
        }

        public virtual IEnumerable<ValidationResult> ValidateNonDateTime(string from, string to, string key)
        {
            DateTime date_from = DateTime.Now;
            DateTime date_to = DateTime.Now;

            //日付形式チェック
            if (!string.IsNullOrEmpty(from))
            {
                if (!DateTime.TryParse(from, out date_from))
                {
                    yield return new ValidationResult(ErsResources.GetMessage("63049", ErsResources.GetFieldName(key)), new[] { key });
                    yield break;
                }

                else if (!this.RangeChk(date_from))
                {
                    yield return new ValidationResult(ErsResources.GetMessage("63049", ErsResources.GetFieldName(key)), new[] { key });
                    yield break;
                }
            }

            if (!string.IsNullOrEmpty(to))
            {
                if (!DateTime.TryParse(to, out date_to))
                {
                    yield return new ValidationResult(ErsResources.GetMessage("63049", ErsResources.GetFieldName(key)), new[] { key });
                    yield break;
                }

                else if (!this.RangeChk(date_to))
                {
                    yield return new ValidationResult(ErsResources.GetMessage("63049", ErsResources.GetFieldName(key)), new[] { key });
                    yield break;
                }
            }

            //超過チェック
            if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
            {
                if (date_from > date_to && date_from.Year < 3000 && date_from.Year > 1000 && date_to.Year < 3000 && date_to.Year > 1000)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10023", ErsResources.GetFieldName(key) + "To" , ErsResources.GetFieldName(key) + "From"), new[] { key });
                    yield break;
                }
            }
        }

        public bool RangeChk(DateTime date)
        {
            var min = DateTime.Parse("1000/1/1");
            var max = DateTime.Parse("3000/1/1");

            if (date < min || date > max)
                return false;

            return true;
        }
    }
}
