﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member.strategy
{
    /// <summary>
    /// Checks Whether the member already exists from member_t.
    /// </summary>
    public class CheckDuplicateEmailStgy
    {
        /// <summary>
        /// check for duplicate mcode and email
        /// </summary>
        /// <param name="mcode">member id</param>
        /// <param name="email">email address</param>
        /// <returns>Return error if the same member exists.</returns>
        public virtual ValidationResult CheckDuplicate(string mcode, string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return null;
            }

            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();

            criteria.mcode_not_equal = mcode;
            criteria.email = email;
            criteria.deleted = EnumDeleted.NotDeleted;

            var setup = ErsFactory.ersUtilityFactory.getSetup();
            if (!setup.member_centralization)
            {
                criteria.site_id = setup.site_id;
            }

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();

            if (repository.GetRecordCount(criteria) > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("email"), email), new[] { "email", "email_confirm" });
            }

            return null;
        }

    }
}
