﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.member.strategy
{
    public class CheckInOperationCardRegularOrderStgy
    {

        /// <summary>
        /// 削除対象カードが定期で稼働中でないか
        /// </summary>
        /// <returns></returns>
        public virtual ValidationResult CheckInOperationCard(string mcode, int? card_id)
        {
            if (!card_id.HasValue)
            {
                return null;
            }

            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);
            if (ErsFactory.ersMemberFactory.GetIsCardHasRegularOrderSpec().Has(mcode, card_id)
                || ErsFactory.ersMemberFactory.GetHasNotYetSaledIssuedRegularOrderSpec().Has(mcode, card_id, member.site_id))
            {
                return new ValidationResult(ErsResources.GetMessage("20304"));
            }

            return null;
        }
    }
}
