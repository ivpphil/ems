﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.Payment;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.order.related;

namespace jp.co.ivp.ers.member.strategy
{
    public class ValidateCardSaveCountStgy
    {
        public ValidationResult Validate(ErsMember member, IEnumerable<int?> del_card_id, IEnumerable<string> memberNames)
        {
            var paymentCredit = (IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD);
            var listCreditCardInfo = paymentCredit.ObtainMemberCardInfo(member);
            var cardCount = listCreditCardInfo.Count;
            if (del_card_id != null)
            {
                //削除予定カードを考慮
                var listCardCount = listCreditCardInfo.Where(cardInfo => !del_card_id.Contains(cardInfo.card_id));
                cardCount = listCardCount.Count();
            }

            if (cardCount >= ErsFactory.ersUtilityFactory.getSetup().entryCardInfoCount)
            {
                return new ValidationResult(ErsResources.GetMessage("53301"), memberNames);
            }

            return null;
        }

    }
}
