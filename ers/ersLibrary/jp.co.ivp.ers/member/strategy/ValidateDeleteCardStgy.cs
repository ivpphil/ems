﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.member.strategy
{
    public class ValidateDeleteCardStgy
    {
        public ValidationResult Validate(ErsMember member, int? card_id, EnumCardSave card_save, int?[] del_card_id, string[] memberNames)
        {
            if (member == null || del_card_id == null)
            {
                return null;
            }

            //削除カードは決済に使用できない
            if (card_save == EnumCardSave.Save && del_card_id.Contains(card_id))
            {
                return new ValidationResult(ErsResources.GetMessage("53302"), memberNames);
            }

            //定期用のカードを削除する場合はカード預けは必須
            if (card_save != EnumCardSave.Save && this.HasRegularOrder(member.mcode, del_card_id))
            {
                return new ValidationResult(ErsResources.GetMessage("20305"), memberNames);
            }

            //継続課金の未売上での使用カードを削除する場合は変更不可
            if (del_card_id != null && this.HasNotYetSaledOrder(member.mcode, del_card_id))
            {
                return new ValidationResult(ErsResources.GetMessage("20303"), memberNames);
            }

            return null;
        }

        /// <summary>
        /// 定期での使用カードを削除しようとしている場合はTrue
        /// </summary>
        /// <param name="del_card_id"></param>
        /// <returns></returns>
        private bool HasRegularOrder(string mcode, int?[] del_card_id)
        {
            foreach (var card_id in del_card_id)
            {
                if (ErsFactory.ersMemberFactory.GetIsCardHasRegularOrderSpec().Has(mcode, card_id))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 継続課金の未売上での使用カードを削除しようとしている場合はTrue
        /// </summary>
        /// <param name="p"></param>
        /// <param name="del_card_id"></param>
        /// <returns></returns>
        private bool HasNotYetSaledOrder(string mcode, int?[] del_card_id)
        {
            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);
            foreach (var card_id in del_card_id)
            {
                if (ErsFactory.ersMemberFactory.GetHasNotYetSaledIssuedRegularOrderSpec().Has(mcode, card_id, member.site_id))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
