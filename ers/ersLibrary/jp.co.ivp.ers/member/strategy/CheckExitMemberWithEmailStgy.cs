﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member.strategy
{
    /// <summary>
    /// Check if email, last name, first name is already exist from member_t
    /// </summary>
    public class CheckExitMemberWithEmailStgy
    {
        /// <summary>
        /// check if existing 
        /// </summary>
        /// <param name="email">email address</param>
        /// <param name="lname">last name</param>
        /// <param name="fname">first name</param>
        /// <returns>Returns error if the record is not equal 1</returns>
        public virtual ValidationResult CheckExitMember(string email, string lname, string fname, int? site_id, string[] fields)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            
            criteria.email = email;
            criteria.lname = lname;
            criteria.fname = fname;
            criteria.deleted = EnumDeleted.NotDeleted;

            if (!setup.member_centralization)
            {
                criteria.site_id = site_id;
            }

            if (repository.GetRecordCount(criteria) != 1)
            {
                return new ValidationResult(ErsResources.GetMessage("20302"), fields);
            }

            return null;
        }
    }
}
