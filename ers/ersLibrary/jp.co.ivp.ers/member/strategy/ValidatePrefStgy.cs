﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member.strategy
{

    /// <summary>
    /// Check preference to Validate from pref_t
    /// </summary>
    public class ValidatePrefStgy
    {
        /// <summary>
        /// validating a preference
        /// </summary>
        /// <param name="model">model</param>
        /// <param name="fieldKey">name of field key</param>
        /// <param name="pref">integer value of preference</param>
        /// <returns>Return error if not exist</returns>
        public virtual IEnumerable<ValidationResult> Validate(string fieldKey, int? pref, bool activeInFrontOnly = true)
        {
            if (pref == null)
                yield break;

            var objPref = ErsFactory.ersCommonFactory.GetErsPrefWithIdAndSiteId(pref, ErsFactory.ersUtilityFactory.getSetup().site_id);
            if (objPref == null
                || (activeInFrontOnly && objPref.active_in_front == EnumActive.NonActive))
            {
                yield return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName(fieldKey)), new[] { fieldKey });
            }
        }
    }
}
