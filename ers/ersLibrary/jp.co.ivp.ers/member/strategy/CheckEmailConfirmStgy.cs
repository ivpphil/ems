﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.member.strategy
{   
    /// <summary>
    /// Checking for email and Confirmation email
    /// </summary>
    public class CheckEmailConfirmStgy
    {
        /// <summary>
        /// check if email and email confirmation is the same
        /// </summary>
        /// <param name="email">email address</param>
        /// <param name="email_confirm">confirmation of email address</param>
        /// <returns>Return error if the email and nd emai confirmation is not equal.</returns>
        public virtual ValidationResult CheckEmailConfirm(string email, string email_confirm)
        {
            if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(email_confirm))
            {
                return null;
            }

            if (email != email_confirm)
            {
                return new ValidationResult(string.Format(ErsResources.GetMessage("10102"), ErsResources.GetFieldName("email"), ErsResources.GetFieldName("email_confirm")), new[] { "email", "email_confirm" });
            }

            return null;
        }

    }
}
