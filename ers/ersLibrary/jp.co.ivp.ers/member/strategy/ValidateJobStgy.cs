﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member.strategy
{
    /// <summary>
    /// Check Job to Validate from job_t
    /// </summary>
    public class ValidateJobStgy
    {
        /// <summary>
        /// validating of job service
        /// </summary>
        /// <param name="model">model</param>
        /// <param name="job">integer value for job services</param>
        /// <returns>Returns error if not exist</returns>
        public virtual IEnumerable<ValidationResult> Validate(int? job)
        {
            var fieldKey = "job";
            if (job == null)
                yield break;

            if (!ErsFactory.ersViewServiceFactory.GetErsViewJobService().ExistValue(job.Value))
            {
                yield return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName(fieldKey)), new[] { fieldKey });
            }
        }
    }
}
