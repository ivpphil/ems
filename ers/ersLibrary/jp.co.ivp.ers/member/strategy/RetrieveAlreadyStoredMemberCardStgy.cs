﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.member.strategy
{
    public class RetrieveAlreadyStoredMemberCardStgy
    {
        /// <summary>
        /// 既に預けられているカード情報を取得する。
        /// </summary>
        /// <param name="memberCard"></param>
        /// <returns></returns>
        public virtual ErsMemberCard Retrieve(string card_mcode, string card_sequence)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
            criteria.active = EnumActive.Active;
            criteria.card_sequence = card_sequence;
            criteria.card_mcode = card_mcode;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId(); 
            var listMemberCard = repository.Find(criteria);
            if (listMemberCard.Count == 0)
            {
                return null;
            }
            return listMemberCard.First();
        }
    }
}
