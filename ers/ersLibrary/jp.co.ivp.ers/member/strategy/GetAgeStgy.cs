﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.member.strategy
{
    public class GetAgeStgy
    {
        /// <summary>
        /// 年齢
        /// </summary>
        public virtual int? GetAge(DateTime? birth)
        {
            int? tmp_age = null;

            if (birth != null)
            {
                DateTime dt1 = DateTime.Today; //基準日
                DateTime dt2 = Convert.ToDateTime(birth); //誕生日
                long d1 = Convert.ToInt64(dt1.ToString("yyyyMMdd")); //基準日を半角数値に変換
                long d2 = Convert.ToInt64(dt2.ToString("yyyyMMdd")); //誕生日を半角数値に変換
                tmp_age = (int)Math.Floor((double)((d1 - d2) / 10000));

            }
            return tmp_age;
        }
    }
}
