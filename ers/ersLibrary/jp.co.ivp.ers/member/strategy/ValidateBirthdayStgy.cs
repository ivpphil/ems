﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member.strategy
{
    /// <summary>
    /// Checking of input date to validate
    /// </summary>
    public class ValidateBirthdayStgy
    {
        /// <summary>
        /// validating and checking of inputted date 
        /// </summary>
        /// <param name="model">model</param>
        /// <param name="birthday_y">year</param>
        /// <param name="birthday_m">moth</param>
        /// <param name="birthday_d">day</param>
        /// <returns>Returns error if the value of year,month and day is not correct</returns>
        public virtual IEnumerable<ValidationResult> Validate(int? birthday_y, int? birthday_m, int? birthday_d)
        {
            if (birthday_y == null && birthday_m == null && birthday_d == null)
                yield break;

            if (birthday_y == null || birthday_m == null || birthday_d == null)
                yield break;

            DateTime dateTime;
            if (!DateTime.TryParse(birthday_y.Value + "/" + birthday_m.Value + "/" + birthday_d.Value, out dateTime))
            {
                yield return new ValidationResult(ErsResources.GetMessage("10001", ErsResources.GetFieldName("birth")), new[] { "birthday_y", "birthday_m", "birthday_d" });
            }
            else
            {
                foreach (var result in this.ValidateRange(dateTime))
                {
                    yield return result;
                }
            }
        }

        /// <summary>
        /// overload method validating min and max  user birthdate 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="birth"></param>
        /// <param name="min_user_age"></param>
        /// <param name="max_user_age"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> ValidateRange(DateTime? birth)
        {
            var min_user_age = ErsFactory.ersUtilityFactory.getSetup().min_user_age;
            var max_user_age = ErsFactory.ersUtilityFactory.getSetup().max_user_age;

            if (birth == null)
                yield break;

            DateTime minDate = DateTime.Now.AddYears(-max_user_age);
            DateTime maxDate = DateTime.Now.AddYears(-min_user_age);

            if (birth < minDate || birth > maxDate)
            {
                yield return new ValidationResult(ErsResources.GetMessage("10047", ErsResources.GetFieldName("birth"), maxDate.ToShortDateString(), minDate.ToShortDateString()));
            }
        }
    }
}
