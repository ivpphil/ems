﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.member.strategy
{
    public class GetAgeCodeStgy
    {
        /// <summary>
        /// 年齢から年齢コード算出
        /// </summary>
        /// <returns></returns>
        public virtual int? GetAgeCode(int? age)
        {
            int? age_code = null;
            if (age.HasValue)
            {
                string tempAgeCode = Convert.ToString(age);

                if (tempAgeCode.Length == 1)
                {
                    age_code = 0;
                }
                else
                {   
                    tempAgeCode = tempAgeCode.Substring(0, tempAgeCode.Length - 1);
                    age_code = Convert.ToInt32(tempAgeCode);

                    if (tempAgeCode.Length > 1)
                    {
                        age_code = Convert.ToInt32(tempAgeCode.Substring(0, tempAgeCode.Length)) - Convert.ToInt32(tempAgeCode.Substring(1, tempAgeCode.Length - 1));
                    }
                    
                }
            }
            
            return age_code;
        }
    }
}
