﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.member.strategy
{
    public class ObtainMemberRankNameStgy
    {
        /// <summary>
        /// ランク名取得 [Obtain rank name]
        /// </summary>
        /// <param name="mcode">string</param>
        /// <param name="site_id">int</param>
        /// <returns>string</returns>
        public virtual string Obtain(string mcode, int site_id)
        {
            var search_spec = ErsFactory.ersMemberFactory.GetMemberRankJoinSetupSearchSpecification();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberRankCriteria();

            criteria.mcode = mcode;
            criteria.active = EnumActive.Active;

            if (ErsFactory.ersUtilityFactory.getSetup().member_rank_centralization)
            {
                criteria.site_id_for_admin = (int)EnumSiteId.COMMON_SITE_ID;
            }
            else
            {
                criteria.site_id_not_equal = (int)EnumSiteId.COMMON_SITE_ID;

                if (site_id != 0)
                {
                    criteria.site_id_for_admin = site_id;
                }
            }

            if (search_spec.GetCountData(criteria) == 0)
            {
                return string.Empty;
            }

            criteria.SetOrderBySiteId(Criteria.OrderBy.ORDER_BY_ASC);

            var rank_list = search_spec.GetSearchData(criteria);

            if (ErsFactory.ersUtilityFactory.getSetup().member_rank_centralization)
            {
                return Convert.ToString(rank_list.First()["rank_name"]);
            }
            else
            {
                return string.Join(", ", rank_list.Select(e => Convert.ToString(e["site_name"]) + ":" + Convert.ToString(e["rank_name"])));
            }
        }
    }
}
