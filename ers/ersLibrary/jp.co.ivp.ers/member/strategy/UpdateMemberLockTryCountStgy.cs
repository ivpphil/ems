﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.member.strategy
{   
    /// <summary>
    /// Checking for email and Confirmation email
    /// </summary>
    public class UpdateMemberLockTryCountStgy
    {

        /// <summary>
        /// Update Member Account to Lock
        /// </summary>
        /// <param name="email"></param>
        public void UpdateMemberToLock(string email)
        {
            var repo = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var oldMemberInfo = ErsFactory.ersMemberFactory.getErsMemberWithEmail(email);
            if (oldMemberInfo == null)
                return;
            var newMemberInfo = ErsFactory.ersMemberFactory.getErsMemberWithEmail(email);
            newMemberInfo.account_status = EnumAccountStatus.Locked;
            repo.Update(oldMemberInfo, newMemberInfo);
        }

        /// <summary>
        /// Increment Login try count when it's failed
        /// </summary>
        /// <param name="email"></param>
        public void IncrementMemberLoginTryCount(string email)
        {
            var repo = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var oldMemberInfo = ErsFactory.ersMemberFactory.getErsMemberWithEmail(email);
            if (oldMemberInfo == null)
                return;
            var newMemberInfo = ErsFactory.ersMemberFactory.getErsMemberWithEmail(email);
            newMemberInfo.login_try_count++;
            repo.Update(oldMemberInfo, newMemberInfo);
        }

        /// <summary>
        /// Reset Login try count if success
        /// </summary>
        /// <param name="email"></param>
        public void ResetMemberLoginTryCount(string email)
        {
            var repo = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var oldMemberInfo = ErsFactory.ersMemberFactory.getErsMemberWithEmail(email);
            if (oldMemberInfo == null)
                return;
            var newMemberInfo = ErsFactory.ersMemberFactory.getErsMemberWithEmail(email);
            newMemberInfo.login_try_count = 0;
            repo.Update(oldMemberInfo, newMemberInfo);
        }


        /// <summary>
        /// Check limit if not less than the Loging try count
        /// </summary>
        /// <param name="model">model</param>
        /// <param name="ques">integer value of lock</param>
        /// <returns>Returns error if member lock</returns>
        public virtual void Validate(string email)
        {
            var objSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSite();

            this.IncrementMemberLoginTryCount(email);
            if (objSetup.login_try_limit <= ErsFactory.ersMemberFactory.GetMemberLockTryCount(email))
            {
               this.UpdateMemberToLock(email);
            }
        }
    }
}
