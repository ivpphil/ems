﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.member.strategy
{
    public class ObtainMemberPointStgy
    {
        /// <summary>
        /// Get points in cache
        /// </summary>
        protected virtual Dictionary<string, ErsPointHistory> pointCache
        {
            get
            {
                if (ErsCommonContext.GetPooledObject("pointCache") == null)
                    pointCache = new Dictionary<string, ErsPointHistory>();

                return (Dictionary<string, ErsPointHistory>)ErsCommonContext.GetPooledObject("pointCache");
            }
            set
            {
                ErsCommonContext.SetPooledObject("pointCache", value);
            }
        }

        /// <summary>
        /// get points history
        /// </summary>
        /// <param name="mcode">mcode</param>
        /// <returns>return the value of total points</returns>
        public virtual int GetPoint(string mcode, int? site_id = null)
        {
            var pointHistory = this.GetPointHistory(mcode, site_id);

            if (pointHistory != null)
                return pointHistory.total_p;
            else
                return 0;
        }

        public virtual int GetPointNotCache(string mcode, int? site_id = null)
        {
            var pointHistory = this.GetPointHistory(mcode, site_id, true);

            if (pointHistory != null)
                return pointHistory.total_p;
            else
                return 0;
        }

        /// <summary>
        /// to get the total obtained points of a member
        /// </summary>
        /// <param name="mcode">mcode</param>
        /// <returns>returns a dictionary list of point history</returns>
        public virtual ErsPointHistory GetPointHistory(string mcode, int? site_id = null, bool not_cache = false)
        {
            if (mcode == null)
            {
                return null;
            }

            if (not_cache || !pointCache.ContainsKey(mcode))
            {
                //ポイント用リポジトリクラス　インスタンス化処理
                var repository = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryRepository();

                //ポイント用クライテリアクラス　インスタンス化処理
                var pointCri = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryCriteria();

                //検索条件をクライテリアへ
                pointCri.mcode = mcode;

                pointCri.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetPointSiteId(site_id);
                pointCri.SetOrderBySiteId(Criteria.OrderBy.ORDER_BY_ASC);
                pointCri.SetOrderByDt(Criteria.OrderBy.ORDER_BY_DESC);
                pointCri.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);

                pointCri.LIMIT = 1;

                IList<ErsPointHistory> list = repository.Find(pointCri);
                if (list.Count > 0)
                {
                    pointCache[mcode] = list[0];
                }
                else
                {
                    pointCache[mcode] = null;
                }
            }

            return pointCache[mcode];
        }

        public ValidationResult ValidateMaxPoints(int last_point, int add_point)
        {
            var remaining = Int32.MaxValue - last_point;

            if (remaining < add_point)
            {
                return new ValidationResult(ErsResources.GetMessage("10024", ErsResources.GetFieldName("cuspoint_points_granted"),remaining));
              
            }

            return null;
        }
    }
}
