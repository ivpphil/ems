﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.member.strategy
{
    public class ObtainMemberSiteIdStgy
    {
        /// <summary>
        /// Get site_id to be assigned for member registration
        /// </summary>
        /// <returns></returns>
        public virtual int GetPointSiteId(object site_id)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            if (!setup.Multiple_sites)
            {
                return (int)EnumSiteId.COMMON_SITE_ID;
            }
            else
            {
                return Convert.ToInt32(site_id ?? setup.site_id);
            }
        }

        /// <summary>
        /// Get site_id to be assigned for member registration
        /// </summary>
        /// <returns></returns>
        public virtual int GetSiteId()
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            if (setup.member_centralization)
            {
                return (int)EnumSiteId.COMMON_SITE_ID;
            }
            else
            {
                return setup.site_id;
            }
        }

        /// <summary>
        /// Get member_t.site_id
        /// </summary>
        /// <returns></returns>
        public virtual int? GetMemberSiteId(string mcode)
        {
            return ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode).site_id;
        }
    }
}
