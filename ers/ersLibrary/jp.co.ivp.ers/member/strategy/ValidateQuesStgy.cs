﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member.strategy
{
    /// <summary>
    /// Check question to validae from ques_t
    /// </summary>
    public class ValidateQuesStgy
    {
        /// <summary>
        /// validating the question
        /// </summary>
        /// <param name="model">model</param>
        /// <param name="ques">integer value of question</param>
        /// <returns>Returns error if not exist</returns>
        public virtual IEnumerable<ValidationResult> Validate(int? ques)
        {
            var fieldKey = "ques";

            if (ques == null)
                yield break;

            if (!ErsFactory.ersViewServiceFactory.GetErsViewQuesService().ExistValue(ques.Value))
            {
                yield return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName(fieldKey)), new[] { fieldKey });
            }
        }
    }
}
