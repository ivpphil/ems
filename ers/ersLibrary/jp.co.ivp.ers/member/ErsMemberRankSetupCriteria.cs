﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.member
{
    public class ErsMemberRankSetupCriteria
        : Criteria
    {
        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_setup_t.active", value, Operation.EQUAL));
            }
        }

        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_setup_t.id", value, Operation.EQUAL));
            }
        }

        public int? rank
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_setup_t.rank", value, Operation.EQUAL));
            }
        }

        public string mcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_t.mcode", value, Operation.EQUAL));
            }
        }

        public int? site_id_for_admin
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_setup_t.site_id", value, Operation.EQUAL));
            }
        }

        public int? site_id_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_setup_t.site_id", value, Operation.NOT_EQUAL));
            }
        }

        public void SetOrderByRank(OrderBy orderBy)
        {
            this.AddOrderBy("member_rank_setup_t.rank", orderBy);
        }

        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("member_rank_setup_t.id", orderBy);
        }

        /// <summary>
        /// Sets the criteria for member_rank_setup_t.site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_setup_t.site_id", value, Operation.EQUAL));
            }
        }

        public virtual int? site_id_of_member_rank_t
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_rank_t.site_id", value, Operation.EQUAL));
            }
        }
    }
}
