﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Provides methods to member_card with CRUD
    /// </summary>
    public class ErsMemberCardRepository
        : ErsRepository<ErsMemberCard>
    {
        public ErsMemberCardRepository()
            : base(new ErsDB_member_card_t())
        {
        }
    }
}
