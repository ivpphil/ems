﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member
{
    public class ErsMemberRankRepository
        : ErsRepository<ErsMemberRank>
    {
          /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsMemberRankRepository()
            : base("member_rank_t")
        {
        }
  }
}
