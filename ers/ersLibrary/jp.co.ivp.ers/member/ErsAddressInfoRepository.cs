﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using System.Data;
using jp.co.ivp.ers.state;
using System.Collections;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Provides methods to addressinfo with CRUD
    /// inherit from ErsRepository
    /// </summary>
    public class ErsAddressInfoRepository
        : ErsRepository<ErsAddressInfo>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsAddressInfoRepository()
            : base(new ErsDB_addressbook_t())
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsAddressInfoRepository(ErsDatabase objDB)
            : base(new ErsDB_addressbook_t(objDB))
        {
        }

        /// <summary>
        /// search records from addressbook_t
        /// </summary>
        /// <param name="criteria">ErsMemberCriteria</param>
        /// <returns>return a list of result</returns>
        public override IList<ErsAddressInfo> Find(Criteria criteria)
        {
            var specAdminSearchCount = ErsFactory.ersAddressInfoFactory.GetShippingSearchSpecification();
            var list = specAdminSearchCount.GetSearchData(criteria);

            var retList = new List<ErsAddressInfo>();
            foreach (var dr in list)
            {
                var address = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithParameter(dr);
                retList.Add(address);
            }
            return retList;
        }
    }
}
