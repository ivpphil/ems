﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member
{
    public class ErsMemberRankSetupRepository
        : ErsRepository<ErsMemberRankSetup>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsMemberRankSetupRepository()
            : base("member_rank_setup_t")
        {
        }
  }
}
