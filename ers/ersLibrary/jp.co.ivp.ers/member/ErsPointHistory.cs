﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;

namespace jp.co.ivp.ers.member
{

    /// <summary>
    /// Holds values of point_t record.
    /// Inherit from repository
    /// </summary>
    public class ErsPointHistory
        :ErsRepositoryEntity
    {
        /// <summary>
        /// default mcode
        /// </summary>
        public const string DEFAUTL_MCODE = "0";

        public ErsPointHistory()
        {
        }

        //ID
        public override int? id { get; set; }

        //会員番号
        public virtual string mcode { get; set; }

        //ポイント更新日
        public virtual DateTime? dt { get; set; }

        //今回ポイント
        public virtual int now_p { get; set; }

        //変更前ポイント
        public virtual int last_p { get; set; }

        //所持ポイント
        public virtual int total_p { get; set; }

        public virtual string reason { get; set; }

        public virtual DateTime? odate { get; set; }

        //伝票番号
        public virtual string d_no { get; set; }

        public virtual int total { get; set; }

        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public virtual int? site_id { get; set; }
    }
}
