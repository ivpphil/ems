﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Represents the search condition of addressbook_t
    /// inherit from criteria
    /// </summary>
    public class ErsAddressInfoCriteria
        :Criteria
    {
        /// <summary>
        /// search condition for mcode
        /// </summary>
        public virtual string mcode
        {
            set
            {
                Add(Criteria.GetCriterion("addressbook_t.mcode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for id
        /// </summary>
        public virtual int? id
        {
            set
            {
                Add(Criteria.GetCriterion("addressbook_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for site_id
        /// </summary>        
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("addressbook_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("addressbook_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

        /// <summary>
        /// for sorting of records using id
        /// </summary>
        public virtual void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("id", orderBy);
        }
    }
}
