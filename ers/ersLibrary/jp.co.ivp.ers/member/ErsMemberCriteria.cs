﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Represents the search condition of member_t
    /// inherit from criteria
    /// </summary>
    public class ErsMemberCriteria
        :Criteria
    {
        /// <summary>
        /// ID
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for mcode
        /// </summary>        
        public virtual string mcode
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.mcode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for mcode
        /// </summary>        
        public virtual IEnumerable<string> mcode_in
        {
            set
            {
                Add(Criteria.GetInClauseCriterion("member_t.mcode", value));
            }
        }

        /// <summary>
        /// search condition for mcode (not equal)
        /// </summary> 
        public virtual string mcode_not_equal
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.mcode", value, Operation.NOT_EQUAL));
            }
        }
        /// <summary>
        /// search condition for last name
        /// </summary> 
        public virtual string lname
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.lname", value, Operation.EQUAL));
            }
        }
        /// <summary>
        /// search condition for first name
        /// </summary> 
        public virtual string fname
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.fname", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for lnamek
        /// </summary> 
        public virtual string lnamek
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.lnamek", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for fnamek
        /// </summary> 
        public virtual string fnamek
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.fnamek", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for email, use for login
        /// </summary>
        /// This is used for login. so if you change this property, please define new property.
        public virtual string email
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.email", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for passwd
        /// </summary> 
        public virtual string passwd
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.passwd", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for ans
        /// </summary> 
        public virtual string ans
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.ans", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for ques
        /// </summary> 
        public virtual int? ques
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.ques", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for tel
        /// </summary> 
        public virtual string tel
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.tel", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for compname (like)
        /// </summary> 
        public virtual string compname
        {
            set
            {
              //  Add(Criteria.GetCriterion("compname", value, Operation.EQUAL));
                Add(Criteria.GetLikeClauseCriterion("member_t.compname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }
        /// <summary>
        /// 登録日検索 (greater than equal)
        /// </summary>
        public virtual DateTime regdate_f 
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.intime", value, Operation.GREATER_EQUAL));
            }
        }
        /// <summary>
        /// search condition for regdate_t (less than equal)
        /// </summary> 
        public virtual DateTime regdate_t
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.intime", value, Operation.LESS_EQUAL));
            }
        }
        /// <summary>
        /// 年齢検索 (less than equal)
        /// </summary>
        public virtual int age_f
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.birth", age_to_birth(value), Operation.LESS_EQUAL));
            }
        }
        /// <summary>
        /// search condition for age_t (greater than)
        /// </summary> 
        public virtual int age_t
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.birth", age_to_birth(value).AddYears(-1), Operation.GREATER_THAN));
            }
        }
        /// <summary>
        /// search condition for pref
        /// </summary> 
        public virtual int? pref
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.pref", value, Operation.EQUAL));
            }
        }
        /// <summary>
        /// search condition for sex 
        /// </summary> 
        public virtual EnumSex sex
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.sex", (int)value, Operation.EQUAL));
            }
        }
        /// <summary>
        /// ポイントテーブルと結びつける場合のみ (greater than equal)
        /// </summary>
        public virtual long? point_f
        {
            set
            {
                if (value == 0)
                {
                    Add(Criteria.JoinWithOR(new[]{
                    Criteria.GetCriterion("point_t.total_p", null, Operation.EQUAL),
                    Criteria.GetCriterion("point_t.total_p", 0, Operation.GREATER_EQUAL)}));
                }
                else
                {
                    Add(Criteria.GetCriterion("point_t.total_p", value, Operation.GREATER_EQUAL));
                }
            }
        }

        /// <summary>
        /// search condition for point_t (less than equal)
        /// </summary> 
        public virtual long? point_t
        {
            set
            {
                Add(Criteria.JoinWithOR(new[]{
                        Criteria.GetCriterion("point_t.total_p", null, Operation.EQUAL),
                        Criteria.GetCriterion("point_t.total_p", value, Operation.LESS_EQUAL)}));
            }
        }

        /// <summary>
        /// 何れかのサイトのポイントが、入力値より大きい
        /// [The point of the site of one is bigger than an input value]
        /// </summary>
        public virtual long? point_from_for_admin
        {
            set
            {
                var dic = new Dictionary<string, object>();
                dic.Add("total_p_from", value);

                this.Add(Criteria.GetUniversalCriterion(string.Format(
                    @" EXISTS(SELECT 1 FROM
                        (
                            SELECT mcode, total_p, site_id FROM 
                            (
	                            SELECT DISTINCT ON (mcode, site_id)* FROM point_t
	                            ORDER BY mcode ASC, site_id ASC, dt DESC, id DESC
                            ) AS point_t
                            WHERE mcode = member_t.mcode
                            AND total_p >= :total_p_from
                        ) AS exist_point
                    ) "), dic));
            }
        }

        /// <summary>
        /// 何れかのサイトのポイントが、入力値より大きい
        /// [The point of the site of one is smaller than an input value]
        /// </summary>
        public virtual long? point_to_for_admin
        {
            set
            {
                var dic = new Dictionary<string, object>();
                dic.Add("total_p_to", value);

                this.Add(Criteria.GetUniversalCriterion(string.Format(
                    @" EXISTS(SELECT 1 FROM
                        (
                            SELECT mcode, total_p, site_id FROM 
                            (
	                            SELECT DISTINCT ON (mcode, site_id)* FROM point_t
	                            ORDER BY mcode ASC, site_id ASC, dt DESC, id DESC
                            ) AS point_t
                            WHERE mcode = member_t.mcode
                            AND total_p <= :total_p_to
                        ) AS exist_point
                    ) "), dic));
            }
        }

        /// <summary>
        /// 何れかのサイトのポイントが、入力値の範囲内
        /// [The point of the site of one is within the input value]
        /// </summary>
        /// <param name="from">long?</param>
        /// <param name="to">long?</param>
        public virtual void point_between_for_admin(long? from, long? to)
        {
            var dic = new Dictionary<string, object>();
            dic.Add("total_p_from", from);
            dic.Add("total_p_to", to);

            this.Add(Criteria.GetUniversalCriterion(string.Format(
                @" EXISTS(SELECT 1 FROM
                    (
                        SELECT mcode, total_p, site_id FROM 
                        (
	                        SELECT DISTINCT ON (mcode, site_id)* FROM point_t
	                        ORDER BY mcode ASC, site_id ASC, dt DESC, id DESC
                        ) AS point_t
                        WHERE mcode = member_t.mcode
                        AND total_p BETWEEN :total_p_from AND :total_p_to
                    ) AS exist_point
                ) "), dic));
        }

        public virtual string address
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.address", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 伝票番号
        /// </summary>
        public virtual string d_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.d_no", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// ファックス
        /// </summary>
        public virtual string fax
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.fax", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 年数から誕生日(と同じ本日)を求める
        /// （○年前の本日を求める）
        /// </summary>
        /// <param name="intAge">年数</param>
        /// <returns>○年前の本日</returns>
        protected virtual DateTime age_to_birth(int intAge)
        {
                DateTime base_date = System.DateTime.Today;
                base_date = base_date.AddYears(-1 * intAge);
                return base_date;
        }

        /// <summary>
        /// search condition for m_flg
        /// </summary> 
        public virtual EnumMFlg m_flg
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.m_flg", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for age_code
        /// </summary>
        public virtual int age_code
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.age_code", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for active
        /// </summary>
        public virtual EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_t.active", Convert.ToInt32(value), Operation.EQUAL));
            }
        }

        /// <summary>
        /// 退会状況による検索 (Fileter for  )
        /// </summary>
        public EnumDeleted deleted
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_t.deleted", Convert.ToInt32(value), Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for dm_flg
        /// </summary> 
        public virtual EnumDmFlg dm_flg
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.dm_flg", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for out_bound_flg
        /// </summary> 
        public virtual EnumOutBoundFlg out_bound_flg
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.out_bound_flg", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for member_rank
        /// </summary> 
        public virtual int? member_rank
        {
            set
            {
                var sql = " EXISTS " +
                    " ( " +
                    "     SELECT 1 FROM member_rank_t " +
                    "     INNER JOIN member_rank_setup_t  " +
                    "     ON member_rank_t.rank = member_rank_setup_t.rank  " +
                    "     AND member_rank_t.site_id = member_rank_setup_t.site_id " +
                    "     WHERE member_rank_t.mcode = member_t.mcode  " +
                    "     AND member_rank_setup_t.id = :rank_id  " +
                    "     AND member_rank_t.active = :active ) ";

                var dic = new Dictionary<string, object>();
                dic.Add("rank_id", value);
                dic.Add("active", EnumActive.Active);

                this.Add(Criteria.GetUniversalCriterion(sql, dic));
            }
        }

        #region For Stepmail
        /// <summary>
        ///  search condition for exists usually item
        /// </summary>
        /// <param name="exists">true : "EXISTS" / false : "NOT EXISTS"</param>
        public void SetTargetExistsUsually(string additionalQuery, Dictionary<string, object> additionalQueryParameter, bool exists = true)
        {
            string NOT = exists ? string.Empty : "NOT ";
            var SQL = "(" + NOT + "EXISTS (SELECT 1 FROM d_master_t"
                + " INNER JOIN ds_master_t"
                + " ON d_master_t.d_no = ds_master_t.d_no"
                + " AND ds_master_t.order_type = :oder_type"
                + " AND ds_master_t.order_status <> ANY :order_status"
                + " WHERE d_master_t.mcode = member_t.mcode"
                + " " + additionalQuery + "))";

            var dicData = new Dictionary<string, object>(additionalQueryParameter);
            dicData["oder_type"] = (int)EnumOrderType.Usually;
            //dicData["order_status"] = (new OrderStatusType[] { OrderStatusType.CANCELED, OrderStatusType.CANCELED_AFTER_DELIVER }).Cast<int>();
            dicData["order_status"] = (new int[] { (int)EnumOrderStatusType.CANCELED }).Cast<int>();

            Add(Criteria.GetUniversalCriterion(SQL, dicData));
        }

        /// <summary>
        ///  search condition for exists subscription item
        /// </summary>
        /// <param name="exists">true : "EXISTS" / false : "NOT EXISTS"</param>
        public void SetTargetExistsSubscription(string additionalQuery, Dictionary<string, object> additionalQueryParameter, bool exists = true)
        {
            string NOT = exists ? string.Empty : "NOT ";
            var SQL = "(" + NOT + "EXISTS (SELECT 1 FROM regular_t"
                + " INNER JOIN regular_detail_t"
                + " ON regular_t.id = regular_detail_t.regular_id"
                + " LEFT JOIN d_master_t"
                + " ON regular_t.d_no = d_master_t.d_no"
                + " AND (regular_detail_t.delete_date IS NULL OR (regular_detail_t.delete_date IS NOT NULL AND regular_detail_t.delete_date > regular_detail_t.next_date))"
                + " WHERE regular_t.mcode = member_t.mcode"
                + " " + additionalQuery + "))";

            Add(Criteria.GetUniversalCriterion(SQL, additionalQueryParameter));
        }

        /// <summary>
        ///  search condition for exists specified item
        /// </summary>
        /// <param name="listItem">listItem</param>
        /// <param name="exists">true : "EXISTS" / false : "NOT EXISTS"</param>
        public void SetTargetExistsSpecifiedItem(List<Dictionary<string, object>> listItem, string additionalQuery, Dictionary<string, object> additionalQueryParameter, bool exists = true)
        {
            string NOT = exists ? string.Empty : "NOT ";

            string ITEM = string.Empty;
            IList<string> listItemWhere = new List<string>();

            var dicData = new Dictionary<string, object>(additionalQueryParameter);

            // 商品コード AND 販売方法 [Scode and Order type]
            for (int i = 0; i < listItem.Count; i++)
            {
                // WHERE句 [Statement of WHERE]
                listItemWhere.Add(string.Format("(ds_master_t.scode = :scode{0} AND ds_master_t.order_type = ANY(:order_type{0}) {1})", i, additionalQuery));

                // パラメータ [Parameter]
                dicData["scode" + i] = listItem[i]["scode"];
                dicData["order_type" + i] = listItem[i]["order_type"];
            }

            // OR連結 [Join with "AND"]
            ITEM = String.Join(" OR ", listItemWhere);

            // 対応区分 [Order status]
            //dicData["order_status"] = (new OrderStatusType[] { OrderStatusType.CANCELED, OrderStatusType.CANCELED_AFTER_DELIVER });
            dicData["order_status"] = (new int[] { (int)EnumOrderStatusType.CANCELED });

            string SQL = "(" + NOT + "EXISTS (SELECT 1 FROM d_master_t"
                + " INNER JOIN ds_master_t"
                + " ON d_master_t.d_no = ds_master_t.d_no"
                + " AND (" + ITEM + ")"
                + " AND ds_master_t.order_status <> ANY :order_status"
                + " WHERE d_master_t.mcode = member_t.mcode))";

            Add(Criteria.GetUniversalCriterion(SQL, dicData));
        }

        /// <summary>
        /// search condition for last_sale_date (last_sale_date BETWEEN from AND to)
        /// </summary>
        /// <param name="from">DateTime for from</param>
        /// <param name="to">DateTime for to</param>
        public void SetBetweenLastSaleDate(DateTime from, DateTime to, int[] site_id)
        {
            var dicParam = new Dictionary<string, object>();
            dicParam["date_from"] = from;
            dicParam["date_to"] = to;

            var SQL = string.Format(@"EXISTS (SELECT 1 FROM d_master_t 
                INNER JOIN ds_master_t
                ON d_master_t.d_no = ds_master_t.d_no
                AND ds_master_t.order_status NOT IN({0}) 
                WHERE d_master_t.mcode = member_t.mcode 
                AND d_master_t.site_id IN({1}) 
                GROUP BY mcode 
                HAVING MAX(d_master_t.intime) >= :date_from
                AND MAX(d_master_t.intime) < :date_to)",
                string.Join(",", new int[] { (int)EnumOrderStatusType.CANCELED, (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER }),
                string.Join(",", site_id));
            this.Add(Criteria.GetUniversalCriterion(SQL, dicParam));
        }

        /// <summary>
        /// For Target member result count and Target Download CSV member
        /// search condition for last_sale_date (last_sale_date BETWEEN from AND to)
        /// </summary>
        /// <param name="from">DateTime for from</param>
        /// <param name="to">DateTime for to</param>
        /// <param name="site_id">int[] for site_id</param>
        public void SetBetweenLastSaleDate(DateTime? from, DateTime? to, int[] site_id)
        {
            if (from.HasValue || to.HasValue)
            {
                var dicParam = new Dictionary<string, object>();

                var SQL = string.Format(@"0 <> (SELECT COUNT(d_master_t.d_no) FROM d_master_t 
                    INNER JOIN ds_master_t
                    ON d_master_t.d_no = ds_master_t.d_no
                    WHERE d_master_t.mcode = member_t.mcode 
                    AND ds_master_t.order_status NOT IN({0}) 
                    AND d_master_t.site_id IN({1}) 
                    GROUP BY mcode 
                    HAVING ",
                    string.Join(",",new int[] { (int)EnumOrderStatusType.CANCELED, (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER }),
                    string.Join(",", site_id));

                if (from.HasValue)
                {
                    SQL += " MAX(d_master_t.intime) >= :date_from ";
                    dicParam["date_from"] = from;
                }

                if (to.HasValue)
                {
                    if (from.HasValue)
                        SQL += " AND ";

                    SQL += " MAX(d_master_t.intime) < :date_to";
                    dicParam["date_to"] = to;
                }

                SQL += ")";

                this.Add(Criteria.GetUniversalCriterion(SQL, dicParam));
            }
        }

        /// <summary>
        /// search condition for sale_times (sale_times BETWEEN from AND to)
        /// </summary>
        /// <param name="from">Count for from</param>
        /// <param name="to">Count for to</param>
        /// <param name="site_id">int[] for site_id</param>
        public void SetBetweenSaleTimes(int from, int to, int[] site_id)
        {
            var dicParam = new Dictionary<string, object>();
            dicParam["from"] = from;
            dicParam["to"] = to;

            var SQL = string.Format(@"COALESCE((SELECT COUNT(d_master_t.id) FROM d_master_t 
                INNER JOIN ds_master_t
                ON d_master_t.d_no = ds_master_t.d_no
                WHERE d_master_t.mcode = member_t.mcode 
                AND ds_master_t.order_status NOT IN({0})
                AND d_master_t.site_id IN({1})),0) BETWEEN :from AND :to ",
                string.Join(",", new int[] { (int)EnumOrderStatusType.CANCELED, (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER }),
                string.Join(",", site_id));

            this.Add(Criteria.GetUniversalCriterion(SQL, dicParam));
        }

        /// <summary>
        /// search condition for sale (sale BETWEEN from AND to)
        /// </summary>
        /// <param name="from">Amount for from</param>
        /// <param name="to">Amount for to</param>
        /// <param name="site_id">int[] for site_id</param>
        public void SetBetweenSale(int from, int to, int[] site_id)
        {
            var dicParam = new Dictionary<string, object>();
            dicParam["from"] = from;
            dicParam["to"] = to;

            var SQL = string.Format(@"COALESCE((SELECT SUM(d_master_t.total) FROM d_master_t 
                INNER JOIN ds_master_t
                ON d_master_t.d_no = ds_master_t.d_no
                WHERE d_master_t.mcode = member_t.mcode 
                AND ds_master_t.order_status NOT IN({0})
                AND d_master_t.site_id IN({1})),0) BETWEEN :from AND :to ",
                string.Join(",", new int[] { (int)EnumOrderStatusType.CANCELED, (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER }),
                string.Join(",", site_id));

            this.Add(Criteria.GetUniversalCriterion(SQL, dicParam));
        }

        /// <summary>
        /// search condition for member intime
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        internal void SetBetweenIntime(DateTime from, DateTime to)
        {
            this.Add(Criteria.GetBetweenCriterion(from, to, Criteria.ColumnName("member_t.intime")));
        }

        /// <summary>
        /// search condition for birth as date (MMDD)
        /// </summary>
        public virtual string birth_as_mmdd
        {
            set
            {
                string SQL = "(to_char(member_t.birth, 'MMDD') = :birth)";

                var dicData = new Dictionary<string, object>();
                dicData["birth"] = value;

                Add(Criteria.GetUniversalCriterion(SQL, dicData));
            }
        }

        /// <summary>
        /// search condition for intime as date
        /// </summary>
        public virtual DateTime? intime_as_date
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.intime", value.Value.GetStartForSearch(), Operation.GREATER_EQUAL));
                Add(Criteria.GetCriterion("member_t.intime", value.Value.AddDays(1).GetStartForSearch(), Operation.LESS_THAN));
            }
        }
        #endregion


        /// <summary>
        /// for sorting of records using mcode
        /// </summary> 
        public void SetOrderByMcode(OrderBy orderBy)
        {
            AddOrderBy("member_t.mcode", orderBy);
        }


        /// <summary>
        /// Sets criterions in order to search data for puchase monitoring.
        /// </summary>
        public void SetMonitorSearch()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            this.lname = setup.monitorFirstLname;
            this.fname = setup.monitorFirstFname;
        }

        public void SetOrderType(bool d_master_t, bool cts_order_t, bool regular_t, string d_no, int? site_id)
        {
            var listCriteria = new List<CriterionBase>();
            var listCriteriaForParameter = new List<CriterionBase>();

            //伝票検索クライテリア
            if (d_master_t)
            {
                //for Parameter
                var d_criteria = new Criteria();
                d_criteria.Add(Criteria.GetCriterion("d_master_t.mcode", ColumnName("member_t.mcode"), Operation.EQUAL));
                d_criteria.Add(Criteria.GetCriterion("ds_master_t.order_type", EnumOrderType.Usually, Operation.EQUAL));
                if (d_no.HasValue())
                {
                    d_criteria.Add(Criteria.GetLikeClauseCriterion("d_master_t.d_no", d_no, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
                }

                //site_id criteria
                var site_id_criteria = Criteria.GetCriterion("d_master_t.site_id", site_id, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("d_master_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                d_criteria.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));

                listCriteriaForParameter.Add(d_criteria);

                //for statement
                listCriteria.Add(Criteria.GetUniversalCriterion("EXISTS (SELECT * FROM d_master_t INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no WHERE " + d_criteria.GetWhere() + ")"));
            }

            //定期伝票検索クライテリア
            if (regular_t)
            {
                //for Parameter
                var regular_criteria = new Criteria();
                regular_criteria.Add(Criteria.GetCriterion("d_master_t.mcode", ColumnName("member_t.mcode"), Operation.EQUAL));
                regular_criteria.Add(Criteria.GetCriterion("ds_master_t.order_type", EnumOrderType.Subscription, Operation.EQUAL));
                if (d_no.HasValue())
                {
                    regular_criteria.Add(Criteria.GetLikeClauseCriterion("d_master_t.d_no", d_no, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
                }

                //site_id criteria
                var site_id_criteria = Criteria.GetCriterion("d_master_t.site_id", site_id, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("d_master_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                regular_criteria.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));

                listCriteriaForParameter.Add(regular_criteria);

                //for statement
                listCriteria.Add(Criteria.GetUniversalCriterion("EXISTS (SELECT * FROM d_master_t INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no WHERE " + regular_criteria.GetWhere() + ")"));
            }

            //CTS伝票検索クライテリア
            if (cts_order_t)
            {
                //for Parameter
                var cts_criteria = new Criteria();
                cts_criteria.Add(Criteria.GetCriterion("cts_order_t.mcode", ColumnName("member_t.mcode"), Operation.EQUAL));
                if (d_no.HasValue())
                {
                    cts_criteria.Add(Criteria.GetLikeClauseCriterion("cts_order_t.temp_d_no", d_no, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
                }

                //site_id criteria
                var site_id_criteria = Criteria.GetCriterion("cts_order_t.site_id", site_id, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_order_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                cts_criteria.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));

                listCriteriaForParameter.Add(cts_criteria);

                //for statement
                listCriteria.Add(Criteria.GetUniversalCriterion("EXISTS (SELECT * FROM cts_order_t WHERE " + cts_criteria.GetWhere() + ")"));
            }

            this.Add(Criteria.JoinWithOR(listCriteria));

            this.Add(new Criteria(listCriteriaForParameter.ToArray()) { ParamaterOnly = true });
        }

        /// <summary>
        /// 返品日（開始）
        /// </summary>
        public virtual DateTime af_cancel_date_from
        {
            set
            {
                this.Add(Criteria.GetUniversalCriterion(
                    string.Format(
                        @"EXISTS (SELECT 1 FROM d_master_t AS d
                        INNER JOIN ds_master_t AS ds ON d.d_no = ds.d_no
                        INNER JOIN ds_status_history_t AS h ON ds.id = h.ds_id 
                        WHERE d.mcode = member_t.mcode 
                            AND h.new_order_status = {0}
                            AND h.tdate >= '{1}') "
                        , (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER
                        , value.ToString())));
            }
        }

        /// <summary>
        /// 返品日（終了）
        /// </summary>
        public virtual DateTime af_cancel_date_to
        {
            set
            {
                this.Add(Criteria.GetUniversalCriterion(
                    string.Format(
                        @"EXISTS (SELECT 1 FROM d_master_t AS d
                        INNER JOIN ds_master_t AS ds ON d.d_no = ds.d_no
                        INNER JOIN ds_status_history_t AS h ON ds.id = h.ds_id 
                        WHERE d.mcode = member_t.mcode 
                            AND h.new_order_status = {0}
                            AND h.tdate <= '{1}') "
                        , (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER
                        , value.ToString())));
            }
        }

        /// <summary>
        /// キャンセル日（開始）
        /// </summary>
        public virtual DateTime cancel_date_from
        {
            set
            {
                this.Add(Criteria.GetUniversalCriterion(
                    string.Format(
                        @"EXISTS (SELECT 1 FROM d_master_t AS d
                        INNER JOIN ds_master_t AS ds ON d.d_no = ds.d_no
                        INNER JOIN ds_status_history_t AS h ON ds.id = h.ds_id 
                        WHERE d.mcode = member_t.mcode 
                            AND h.new_order_status = {0}
                            AND h.tdate >= '{1}') "
                        , (int)EnumOrderStatusType.CANCELED
                        , value.ToString())));
            }
        }

        /// <summary>
        /// キャンセル日（終了）
        /// </summary>
        public virtual DateTime cancel_date_to
        {
            set
            {
                this.Add(Criteria.GetUniversalCriterion(
                    string.Format(
                        @"EXISTS (SELECT 1 FROM d_master_t AS d
                        INNER JOIN ds_master_t AS ds ON d.d_no = ds.d_no
                        INNER JOIN ds_status_history_t AS h ON ds.id = h.ds_id 
                        WHERE d.mcode = member_t.mcode 
                            AND h.new_order_status = {0}
                            AND h.tdate <= '{1}') "
                        , (int)EnumOrderStatusType.CANCELED
                        , value.ToString())));
            }
        }

        /// <summary>
        /// 顧客番号（前方一致）
        /// </summary>
        public virtual string mcodeLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("member_t.mcode", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// 姓（前方一致）
        /// </summary>
        public virtual string lnameLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("member_t.lname", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// 名（前方一致）
        /// </summary>
        public virtual string fnameLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("member_t.fname", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// 姓カナ（前方一致）
        /// </summary>
        public virtual string lnamekLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("member_t.lnamek", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// 名（前方一致）
        /// </summary>
        public virtual string fnamekLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("member_t.fnamek", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// アドレス（前方一致）
        /// </summary>
        public virtual string addressLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("member_t.address", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual string taddressLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("member_t.taddress", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// メールアドレス（前方一致）
        /// </summary>
        public virtual string emailLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("member_t.email", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// 電話（前方一致）
        /// </summary>
        public virtual string telLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("member_t.tel", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// ファックス（前方一致）
        /// </summary>
        public virtual string faxLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("member_t.fax", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual string zipLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("member_t.zip", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// 媒体番号（前方一致）
        /// </summary>
        public virtual string ccodeLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("member_t.ccode", value.ToUpper(), LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual int? campaign_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("campaign_t.id", value, Operation.EQUAL));
            }
        }

        public virtual void SetOrderByName()
        {
            this.AddOrderBy("member_t.lnamek", OrderBy.ORDER_BY_ASC);
            this.AddOrderBy("member_t.fnamek", OrderBy.ORDER_BY_ASC);
            this.AddOrderBy("member_t.pref", OrderBy.ORDER_BY_ASC);
            this.AddOrderBy("member_t.mcode", OrderBy.ORDER_BY_ASC);
        }

        public void ignoreMonitor()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var strSQL = " (member_t.lname, member_t.fname) NOT IN ((:monitorFirstLname, :monitorFirstFname), (:monitorSecondLname, :monitorSecondFname))";

            this.Add(Criteria.GetUniversalCriterion(strSQL,
                new Dictionary<string, object>() { 
                    { "monitorFirstLname", setup.monitorFirstLname },
                    { "monitorFirstFname", setup.monitorFirstFname },
                    { "monitorSecondLname", setup.monitorSecondLname },
                    { "monitorSecondFname", setup.monitorSecondFname } }));
        }

        public void SetLikeAddress(string address)
        {
            var SQL = " COALESCE(member_t.address,'')||COALESCE(member_t.taddress,'')||COALESCE(member_t.maddress,'') LIKE '%" + address + "%'";

            Add(Criteria.GetUniversalCriterion(SQL));
        }

        public virtual int? login_try_count
        {
            set
            {
                this.Add(Criteria.GetCriterion("campaign_t.login_try_count", value, Operation.GREATER_EQUAL));
            }
        }

        /// <summary>
        /// search condition for site_id
        /// </summary>        
        public virtual int? site_id
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for member_rank_t site_id
        /// </summary>        
        public virtual int? member_rank_site_id
        {
            set
            {
                var dic = new Dictionary<string, object>();
                if (value.HasValue)
                {
                    dic.Add("site_id", value);
                }

                var query = " member_rank_t.site_id = :site_id  ";

                var sql = " rank in ( SELECT rank FROM member_rank_setup_t  WHERE ( ";
                if (value.HasValue)
                {
                    sql += query;
                }
                sql += " AND  member_rank_setup_t.active = {0}) )";

                this.Add(Criteria.GetUniversalCriterion(string.Format(
                    sql, (int)EnumActive.Active), dic));
            }
        }

    }
}
