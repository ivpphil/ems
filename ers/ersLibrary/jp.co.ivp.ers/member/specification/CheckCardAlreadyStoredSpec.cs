﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member.specification
{
    public class CheckCardAlreadyStoredSpec
    {
        /// <summary>
        /// 指定されたカード情報が既に預けられているか否か
        /// </summary>
        /// <param name="memberCard"></param>
        /// <returns></returns>
        public virtual bool Check(string mcode, int? card_id)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
            criteria.active = EnumActive.Active;
            criteria.id = card_id;
            criteria.mcode = mcode;

            return repository.GetRecordCount(criteria) == 0;
        }
    }
}
