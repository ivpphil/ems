﻿using System;
using System.Data;
using System.Collections.Generic;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member.specification
{
    /// <summary>
    /// Search a data from member_rank_t joined with member_t and.
    /// </summary>
    public class MemberRankJoinSetupSearchSpecification
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return string.Format(@" SELECT member_rank_t.*, member_rank_setup_t.rank_name, site_t.site_name FROM member_rank_t 
                    INNER JOIN member_rank_setup_t ON member_rank_t.rank = member_rank_setup_t.rank AND member_rank_t.site_id = member_rank_setup_t.site_id 
                    LEFT JOIN site_t ON site_t.id = member_rank_setup_t.site_id ");
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return string.Format(@" SELECT COUNT(DISTINCT member_rank_t.mcode) AS {0} From member_rank_t 
                    INNER JOIN member_rank_setup_t ON member_rank_t.rank = member_rank_setup_t.rank AND member_rank_t.site_id = member_rank_setup_t.site_id 
                    LEFT JOIN site_t ON site_t.id = member_rank_setup_t.site_id ", countColumnAlias);
        }
    }

}