﻿using System;
using System.Data;
using System.Collections.Generic;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member.specification
{
    /// <summary>
    /// Search a data from addressbook_t joined with pref_t and.
    /// </summary>
    public class ShippingSearchSpecification
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return " SELECT DISTINCT addressbook_t.*, pref_t.pref_name AS add_pref_name FROM addressbook_t "
                    + "INNER JOIN pref_t ON addressbook_t.add_pref = pref_t.id ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(DISTINCT addressbook_t.id) AS " + countColumnAlias + " FROM addressbook_t "
                    + "INNER JOIN pref_t ON addressbook_t.add_pref = pref_t.id ";
        }
    }

}