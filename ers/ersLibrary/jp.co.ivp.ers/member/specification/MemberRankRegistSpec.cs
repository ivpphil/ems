﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member.specification
{
    public class MemberRankRegistSpec
        : ISpecificationForSQL
    {
        public int? member_rank { get; set; }

        public string strSQL { get; set; }

        public void Regist(Criteria criteria, int? member_rank, int? site_id)
        {
            this.member_rank = member_rank;

            this.strSQL = "INSERT INTO member_rank_t (mcode, rank, active, site_id) ";
            this.strSQL = this.strSQL + "SELECT mcode, " + this.member_rank + ", " + (int)EnumActive.Active + ", " + site_id + " ";
            this.strSQL = this.strSQL + "FROM member_t ";
            this.strSQL = this.strSQL + "WHERE mcode NOT IN (SELECT mcode FROM member_rank_t WHERE active = " + (int)EnumActive.Active + " AND site_id = " + site_id + ") ";
            ErsRepository.UpdateSatisfying(this, criteria);
        }

        public void Update(Criteria criteria, int? member_rank, int? site_id)
        {
            this.member_rank = member_rank;

            this.strSQL = "UPDATE member_rank_t SET rank = " + this.member_rank + ", utime = '" + DateTime.Now + "' ";
            this.strSQL = this.strSQL + "WHERE active = " + (int)EnumActive.Active + " AND rank <> " + this.member_rank + " AND site_id = " + site_id + " ";
            ErsRepository.UpdateSatisfying(this, criteria);

        }

        public string asSQL()
        {
            return strSQL;
        }
    }
}
