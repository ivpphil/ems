﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.member.specification
{
    public class IsCardHasRegularOrderSpec
    {
        public bool Has(string mcode, int? card_id)
        {
            var repo = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();

            criteria.mcode = mcode;
            criteria.detail_pay = 1;
            criteria.SetActiveOnly();
            criteria.member_card_id = card_id;

            return (repo.GetRecordCount(criteria) != 0);
        }
    }
}
