﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member.specification
{
    public class RetrieveMemberRankGrantLowerLimitSpec
        : ISpecificationForSQL
    {
        public int Retrieve(int? site_id)
        {
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberRankSetupCriteria();
            criteria.active = EnumActive.Active;
            criteria.site_id = site_id;

            var listResult = ErsRepository.SelectSatisfying(this, criteria);

            return Convert.ToInt32(listResult.First()["lower_limit"]);
        }

        public string asSQL()
        {
            return "SELECT COALESCE(MIN(value_from), 0) AS lower_limit FROM member_rank_setup_t";
        }
    }
}
