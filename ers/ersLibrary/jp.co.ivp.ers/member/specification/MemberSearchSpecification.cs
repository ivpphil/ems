﻿using System;
using System.Data;
using System.Collections.Generic;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member.specification
{
    /// <summary>
    /// Search a data from member_t joined with point_t and.
    /// </summary>
    public class MemberSearchSpecification
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return " SELECT DISTINCT member_t.*, point_t.total_p, CASE WHEN member_rank_t.active = 1 THEN member_rank_t.rank ELSE NULL END AS rank FROM member_t "
                    + "LEFT JOIN (SELECT DISTINCT ON (mcode)* FROM point_t ORDER BY mcode ASC, dt DESC, id DESC) AS point_t ON member_t.mcode = point_t.mcode "
                    + "LEFT JOIN (SELECT DISTINCT ON (mcode)* FROM member_rank_t ORDER BY mcode ASC, id DESC) AS member_rank_t ON member_t.mcode = member_rank_t.mcode AND member_rank_t.active = 1 ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(DISTINCT member_t.mcode) AS " + countColumnAlias + " FROM member_t "
                    + "LEFT JOIN (SELECT DISTINCT ON (mcode)* FROM point_t ORDER BY mcode ASC, dt DESC, id DESC) AS point_t ON member_t.mcode = point_t.mcode "
                    + "LEFT JOIN (SELECT DISTINCT ON (mcode)* FROM member_rank_t ORDER BY mcode ASC, id DESC) AS member_rank_t ON member_t.mcode = member_rank_t.mcode AND member_rank_t.active = 1 ";
        }
    }

}