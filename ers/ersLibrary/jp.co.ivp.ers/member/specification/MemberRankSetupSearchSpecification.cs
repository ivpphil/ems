﻿using System;
using System.Data;
using System.Collections.Generic;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member.specification
{
    /// <summary>
    /// Search a data from member_rank_setup_t joined with member_rank_t
    /// </summary>
    public class MemberRankSetupSearchSpecification
        : ISpecificationForSQL
    {
        private string mcode { get; set; }
                /// <summary>
        /// 親を持たないカテゴリのリストを取得する。
        /// </summary>
        /// <param name="activeOnly">active=true</param>
        /// <returns>returns list of records that has no parent record and with 
        /// active only sorted by disp_order and id</returns>
        public virtual List<Dictionary<string, object>> SelectAsList(string mcode, int? site_id = null)
        {
            this.mcode = mcode;

            var retList = new List<Dictionary<string, object>>();
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberRankSetupCriteria();
            
            if (setup.member_rank_centralization)
            {
                criteria.site_id = (int)EnumSiteId.COMMON_SITE_ID;
                criteria.site_id_of_member_rank_t = (int)EnumSiteId.COMMON_SITE_ID;
            }
            else
            {
                if (site_id == null)
                {
                    criteria.site_id = setup.site_id;
                    criteria.site_id_of_member_rank_t = setup.site_id;
                }
                else
                {
                    criteria.site_id = site_id;
                    criteria.site_id_of_member_rank_t = site_id;
                }
            }

            return ErsRepository.SelectSatisfying(this, criteria);
        }


        public string asSQL()
        {
             return  string.Format(" SELECT * From member_rank_setup_t "
                 + " INNER JOIN member_rank_t ON member_rank_setup_t.rank = member_rank_t.rank AND mcode ='{0}' AND member_rank_t.active = 1",mcode);
        }
    }

}