﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member.specification
{
    public class MemberRankDeleteSpec
        : ISpecificationForSQL
    {
        public void Delete(Criteria criteria)
        {
            ErsRepository.UpdateSatisfying(this, criteria);
        }

        public string asSQL()
        {
            return "UPDATE member_rank_t SET active = " + (int)EnumActive.NonActive + ", utime = '" + DateTime.Now + "' WHERE active = " + (int)EnumActive.Active;
        }
    }
}
