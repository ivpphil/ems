﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.member.specification
{
    public class MemberRegularErrListSpec
         : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            string sql = "SELECT "
                    + "  member_t.* "
                    + " ,regular_error_t.* "
                    + "FROM member_t "
                    + "INNER JOIN member_card_t ON member_t.mcode = member_card_t.mcode "
                    + "INNER JOIN regular_error_t ON member_card_t.card_mcode = regular_error_t.card_mcode "
                    + "WHERE true";

            return sql;
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            string sql = "SELECT COUNT(*) AS " + countColumnAlias + " "
                    + "FROM member_t "
                    + "INNER JOIN member_card_t ON member_t.mcode = member_card_t.mcode "
                    + "INNER JOIN regular_error_t ON member_card_t.card_mcode = regular_error_t.card_mcode "
                    + "WHERE true";

            return sql;
        }
    }
}
