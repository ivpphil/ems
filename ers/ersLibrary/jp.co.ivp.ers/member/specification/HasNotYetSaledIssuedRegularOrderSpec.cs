﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.member.specification
{
    public class HasNotYetSaledIssuedRegularOrderSpec
    {
        /// <summary>
        /// Checks if the specified card is used in an order that is not saled.
        /// </summary>
        /// <param name="mcode"></param>
        /// <param name="card_id"></param>
        /// <returns></returns>
        public bool Has(string mcode, int? card_id, int? site_id)
        {
            if (!card_id.HasValue)
            {
                throw new Exception("Please specify card_id.");
            }

            var repo = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

            criteria.mcode = mcode;
            criteria.member_card_id = card_id;
            if (site_id != (int)EnumSiteId.COMMON_SITE_ID)
            {
                criteria.site_id = site_id;
            }
            criteria.order_payment_status_in = new[] { EnumOrderPaymentStatusType.NO_AUTH };
            criteria.order_status_not_in = criteria.CancelStatusArray;

            return (repo.GetRecordCount(criteria) != 0);
        }

        /// <summary>
        /// Checks if the specified member has an order that is not saled.
        /// </summary>
        /// <param name="mcode"></param>
        /// <param name="card_id"></param>
        /// <returns></returns>
        public bool Has(string mcode, int? site_id)
        {
            var repo = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

            criteria.mcode = mcode;
            if (site_id != (int)EnumSiteId.COMMON_SITE_ID)
            {
                criteria.site_id = site_id;
            }
            criteria.order_payment_status_in = new[] { EnumOrderPaymentStatusType.NO_AUTH };
            criteria.order_status_not_in = criteria.CancelStatusArray;

            return (repo.GetRecordCount(criteria) != 0);
        }
    }
}
