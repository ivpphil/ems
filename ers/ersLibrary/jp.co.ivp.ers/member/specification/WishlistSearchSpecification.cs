﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.member.specification
{
    /// <summary>
    /// Search a data from wishlist_t joined with s_master_t and g_master_t .
    /// </summary>
    public class WishlistSearchSpecification
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return "SELECT DISTINCT ON (wishlist_t.id) * "
                + "FROM g_master_t "
                + "INNER JOIN s_master_t ON g_master_t.gcode = s_master_t.gcode "
                + "INNER JOIN wishlist_t ON s_master_t.scode = wishlist_t.scode "
                + "LEFT JOIN( "
                + "	SELECT scode, price, price2 FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.NORMAL + " "
                + ") AS price_t ON s_master_t.scode = price_t.scode "
                + "LEFT JOIN( "
                + "	SELECT scode, price AS regular_first_price FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.REGULAR_FIRST + " "
                + ") AS regular_first_price ON s_master_t.scode = regular_first_price.scode "
                + "LEFT JOIN( "
                + "	SELECT scode, price AS regular_price FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.REGULAR + " "
                + ") AS regular_price ON s_master_t.scode = regular_price.scode "
                + "LEFT JOIN( "
                + "	SELECT scode, price AS sale_price, date_from AS p_date_from, date_to AS p_date_to FROM price_t WHERE current_timestamp BETWEEN date_from AND date_to AND price_kbn = " + (int)EnumPriceKbn.SALE + " "
                + ") AS sale_price ON s_master_t.scode = sale_price.scode "
                + "WHERE true ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(DISTINCT g_master_t.gcode) AS " + countColumnAlias + " "
                + "FROM g_master_t "
                + "INNER JOIN s_master_t ON g_master_t.gcode = s_master_t.gcode "
                + "INNER JOIN wishlist_t ON s_master_t.scode = wishlist_t.scode ";
        }
    }
}
