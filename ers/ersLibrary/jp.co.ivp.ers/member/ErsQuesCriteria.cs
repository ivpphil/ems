﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.member
{
    public class ErsQuesCriteria
        : Criteria
    {
        public EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("ques_t.active", value, Operation.EQUAL));
            }
        }

        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("ques_t.id", value, Operation.EQUAL));
            }
        }

        internal void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("ques_t.id", orderBy);
        }
    }
}
