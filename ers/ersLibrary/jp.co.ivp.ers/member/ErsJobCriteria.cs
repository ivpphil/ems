﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.member
{
    public class ErsJobCriteria
        : Criteria
    {
        internal void SetOrderById(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("job_t.id", orderBy);
        }

        public int? id 
        {
            set
            {
                this.Add(Criteria.GetCriterion("job_t.id", value, Operation.EQUAL));
            }
        }
    }
}
