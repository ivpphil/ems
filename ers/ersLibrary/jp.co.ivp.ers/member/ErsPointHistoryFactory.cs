﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.member.specification;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.member
{
    public class ErsPointHistoryFactory
    {

        /// <summary>
        /// Get the repository of ErsPointHistory
        /// </summary>
        /// <returns>new instance of ErsPointHistoryRepository</returns>
        public ErsPointHistoryRepository GetErsPointHistoryRepository()
        {
            return new ErsPointHistoryRepository();
        }

        /// <summary>
        /// Get the class of ErsPointHistory
        /// </summary>
        /// <returns>new instance of ErsPointHistory</returns>
        public ErsPointHistory GetErsPointHistory()
        {
            return new ErsPointHistory();
        }

        /// <summary>
        /// ErsPointHistoryCriteria用Criteria
        /// </summary>
        /// <returns>new instance of ErsPointHistoryCriteria</returns>
        public virtual ErsPointHistoryCriteria GetErsPointHistoryCriteria()
        {
            return new ErsPointHistoryCriteria();
        }

        /// <summary>
        /// Get the class of NewPointSpecification
        /// </summary>
        /// <returns>new instance of NewPointSpecification</returns>
        public virtual NewPointSpecification GetNewPointSpecification()
        {
            return new NewPointSpecification();
        }

        /// <summary>
        /// Based on the input value , to get the class ErsPointHistory 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>new instance of ErsPointHistory</returns>
        internal virtual ErsPointHistory GetErsPointHistoryWithParameter(Dictionary<string, object> parameters)
        {
            var pointHistory = this.GetErsPointHistory();
            pointHistory.OverwriteWithParameter(parameters);
            return pointHistory;
        }
    }
}
