﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using System.Data;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Provides methods to member_card with CRUD
    /// inherit from ErsRepository
    /// </summary>
    public class ErsMemberRepository
        : ErsRepository<ErsMember>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsMemberRepository()
            : base(new ErsDB_member_t())
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsMemberRepository(ErsDatabase objDB)
            : base(new ErsDB_member_t(objDB))
        {
        }

        /// <summary>
        /// insert records into member_t
        /// </summary>
        /// <param name="obj">ErsMember</param>
        public override void Insert(ErsMember obj, bool storeNewIdToObject = false)
        {
            if (storeNewIdToObject == true)
            {
                obj.id = this.ersDB_table.GetNextSequence();
                obj.mcode = obj.id.ToString();
            }
            base.Insert(obj);
        }

        /// <summary>
        /// search records from member_t
        /// </summary>
        /// <param name="criteria">ErsMemberCriteria</param>
        /// <returns>return a list of result</returns>
        public override IList<ErsMember> Find(Criteria criteria)
        {
            var specAdminSearchCount = ErsFactory.ersMemberFactory.GetMemberSerachSpecification();
            var list = specAdminSearchCount.GetSearchData(criteria);

            var retList = new List<ErsMember>();
            foreach (var dr in list)
            {
                var member = ErsFactory.ersMemberFactory.getErsMemberWithParameter(dr);
                retList.Add(member);
            }
            return retList;
        }
        /// <summary>
        /// get record count of member_t
        /// </summary>
        /// <param name="criteria">ErsMemberCriteria</param>
        /// <returns>return a record count</returns>
        public override long GetRecordCount(Criteria criteria)
        {
            var specAdminSearchCount = ErsFactory.ersMemberFactory.GetMemberSerachSpecification();
            return specAdminSearchCount.GetCountData(criteria);

        }

    }
}
