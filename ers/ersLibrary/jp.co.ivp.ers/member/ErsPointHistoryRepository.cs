﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using System.Data;
using jp.co.ivp.ers.state;
using System.Collections;

namespace jp.co.ivp.ers.member
{

    /// <summary>
    /// Provides methods to pointhistory with CRUD
    /// inherit from ErsRepository
    /// </summary>
    public class ErsPointHistoryRepository
        : ErsRepository<ErsPointHistory>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsPointHistoryRepository()
            : base(new ErsDB_point_t())
        {
        }
    }
}
