﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Represents the search condition of s_master_t join with g_master_t,tp_s_master_t 
    /// inherit from criteria
    /// </summary>
    public class ErsWishlistCriteria
        : Criteria
    {
        /// <summary>
        /// search condition for mcode
        /// </summary>
        public virtual string mcode
        {
            set
            {
                Add(Criteria.GetCriterion("wishlist_t.mcode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for scode
        /// </summary>
        public virtual string scode
        {
            set
            {
                Add(Criteria.GetCriterion("wishlist_t.scode", value, Operation.EQUAL));
            }
        }

        public void SetActiveOnly(DateTime? currentDateTime)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            criteria.SetActiveOnly(currentDateTime);
            this.Add(criteria);
        }

        /// <summary>
        /// set order by for wishlist_t.id
        /// </summary>
        public virtual void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("wishlist_t.id", orderBy);
        }

        /// <summary>
        /// set order by for g_master_t.date_from
        /// </summary>
        public virtual void SetOrderByDate_from(OrderBy orderBy)
        {
            AddOrderBy("g_master_t.date_from", orderBy);
        }


        /// <summary>
        /// search condition for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                Add(Criteria.GetCriterion("wishlist_t.site_id", value, Operation.EQUAL));
            }
        }

    }
}
