﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Represents the search condition of point_t
    /// inherit from criteria
    /// </summary>
    public class ErsPointHistoryCriteria : Criteria
    {

        /// <summary>
        /// search condition for id
        /// </summary>
        public virtual int id
        {
            set
            {
                Add(Criteria.GetCriterion("id", value, Operation.EQUAL));
            }
        }

        //検索条件(会員コード)
        public virtual string mcode
        {
            set
            {
                Add(Criteria.GetCriterion("mcode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                Add(Criteria.GetCriterion("site_id", value, Operation.EQUAL));
            }
        }

        //検索条件(検索開始日)
        public virtual DateTime dt_from
        {
            set
            {
                Add(Criteria.GetCriterion("dt", value, Operation.GREATER_EQUAL));
            }
        }

        //検索条件(検索終了日)
        public virtual DateTime dt_to
        {
            set
            {
                Add(Criteria.GetCriterion("dt", value, Operation.LESS_EQUAL));
            }
        }

        public virtual string d_no
        {
            set
            {
                Add(Criteria.GetCriterion("d_no", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// set order by for dt
        /// </summary>
        public virtual void SetOrderByDt(OrderBy orderBy)
        {
            AddOrderBy("dt", orderBy);
        }

        /// <summary>
        /// set order by for id
        /// </summary>
        public virtual void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("id", orderBy);
        }

        /// <summary>
        /// set order by for site id
        /// </summary>
        public virtual void SetOrderBySiteId(OrderBy orderBy)
        {
            AddOrderBy("site_id", orderBy);
        }

        /// <summary>
        /// Get latest record per a user
        /// </summary>
        public virtual void GetLatestRecord()
        {
            this.Add(Criteria.GetUniversalCriterion(
                "id IN (SELECT DISTINCT ON(mcode) id FROM point_t ORDER BY mcode ASC, dt DESC)"));
        }

        public virtual void GetLatestRecord(int site_id)
        {
            this.Add(Criteria.GetUniversalCriterion(string.Format(
                "id IN (SELECT DISTINCT ON(mcode) id FROM point_t WHERE site_id = {0} ORDER BY mcode ASC, dt DESC)", site_id)));
        }

        /// <summary>
        /// Get records whitch don't have recent orders
        /// </summary>
        /// <param name="expirationDate"></param>
        public virtual void DontHaveRecentOrder(DateTime expirationDate)
        {
            this.Add(Criteria.GetUniversalCriterion(
                @"NOT EXISTS (SELECT * FROM d_master_t INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no
                WHERE :cancel_order_status = order_status AND d_master_t.intime > :intime AND d_master_t.mcode = point_t.mcode AND point_t.site_id = d_master_t.site_id )", new Dictionary<string, object>() { 
                                                                { "cancel_order_status", ErsOrderCriteria.CANCEL_STATUS_ARRAY.Select((value) => (int)value) },
                                                                { "intime", expirationDate }}));
        }
    }
}
