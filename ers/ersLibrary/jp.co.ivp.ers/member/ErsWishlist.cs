﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Holds values of s_master_t join with g_master_t,tp_s_master_t  record.
    /// Inherit from repository
    /// </summary>
    public class ErsWishlist
        :ErsRepositoryEntity
    {
        public override int? id { get; set; }
        
        /// <summary>
        /// member id
        /// </summary>
        public virtual string mcode { get; set; }

        public virtual string scode { get; set; }

        public int? site_id { get; set; }
    }
}
