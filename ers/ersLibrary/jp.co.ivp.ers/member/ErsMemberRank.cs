﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member
{
    public class ErsMemberRank
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string mcode { get; set; }
        public int? rank { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
        public int? site_id { get; set; }
    }
}
