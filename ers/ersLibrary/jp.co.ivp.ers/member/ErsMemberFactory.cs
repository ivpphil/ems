﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.member.specification;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.member.strategy;
using jp.co.ivp.ers.order.strategy;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Provides 会員関連クラス用Factory
    /// </summary>
    public class ErsMemberFactory
    {

        protected static ErsMemberRepository _ErsMemberRepository
        {
            get
            {
                return (ErsMemberRepository)ErsCommonContext.GetPooledObject("_ErsMemberRepository");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_ErsMemberRepository", value);
            }
        }

        /// <summary>
        /// 会員クラス用Repositoryを取得する
        /// </summary>
        /// <returns>instance of ErsMemberRepository</returns>
        public virtual ErsMemberRepository GetErsMemberRepository()
        {
            if (_ErsMemberRepository == null)
                _ErsMemberRepository = new ErsMemberRepository();
            return _ErsMemberRepository;
        }

        /// <summary>
        /// 乱数を元に、会員クラスを取得する。
        /// </summary>
        /// <param name="sessionState"></param>
        /// <returns>instance of ErsMember</returns>
        public virtual ErsMember getErsMemberWithRansu(ErsState sessionState)
        {
            var repository = GetErsMemberRepository();

            if (((ISession)ErsContext.sessionState).getUserState() != EnumUserState.LOGIN)
            {
                return null;
            }

            var criteria = this.GetErsMemberCriteria();
            criteria.mcode = ErsContext.sessionState.Get("mcode");
            criteria.deleted = EnumDeleted.NotDeleted;
            criteria.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();
            var listMember = repository.Find(criteria);
            if (listMember.Count != 1)
            {
                return null;
            }

            return listMember.First();
        }

        /// <summary>
        /// 会員コードをもとに、会員クラスを取得する。
        /// </summary>
        /// <param name="mcode"></param>
        /// <returns>instance of ErsMember</returns>
        public virtual ErsMember getErsMemberWithMcode(string mcode, bool includeResignedMember = false)
        {
            var repository = GetErsMemberRepository();

            var criteria = this.GetErsMemberCriteria();
            criteria.mcode = mcode;
            criteria.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();

            if (includeResignedMember == false)
            {
                criteria.deleted = EnumDeleted.NotDeleted;
            }
            var list = repository.Find(criteria);
            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        /// <summary>
        /// 会員コードをもとに、会員クラスを取得する。
        /// </summary>
        /// <param name="mcode"></param>
        /// <returns>instance of ErsMember</returns>
        public virtual ErsMember getErsMemberWithMcodeForAdmin(string mcode, bool includeResignedMember = false)
        {
            var repository = GetErsMemberRepository();

            var criteria = this.GetErsMemberCriteria();
            criteria.mcode = mcode;

            if (includeResignedMember == false)
            {
                criteria.deleted = EnumDeleted.NotDeleted;
            }
            var list = repository.Find(criteria);
            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        public virtual ErsMember getErsMemberWithEmail(string email, bool includeResignedMember = false)
        {
            var repository = GetErsMemberRepository();

            var criteria = this.GetErsMemberCriteria();
            criteria.email = email;
            if (includeResignedMember == false)
            {
                criteria.deleted = EnumDeleted.NotDeleted;
            }
            var list = repository.Find(criteria);
            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }


        /// <summary>
        /// 入力値をもとに、会員クラスを取得する。
        /// </summary>
        /// <param name="mcode"></param>
        /// <returns>instance of ErsMember</returns>
        public virtual ErsMember getErsMemberWithParameter(Dictionary<string, object> parameters)
        {
            var member = ErsFactory.ersMemberFactory.GetErsMember();
            member.OverwriteWithParameter(parameters);
            return member;
        }

        /// <summary>
        /// 会員クラスを取得する
        /// </summary>
        /// <returns>new instance of ErsMember</returns>
        public virtual ErsMember GetErsMember()
        {
            return new ErsMember();
        }

        /// <summary>
        /// ErsMemberRespository用Criteria
        /// </summary>
        /// <returns>new instance of ErsMemberCriteria</returns>
        public virtual ErsMemberCriteria GetErsMemberCriteria()
        {
            return new ErsMemberCriteria();
        }

        /// <summary>
        /// キャンセル可能かを判定するSpecification
        /// </summary>
        /// <returns>new instance of CancelMembershipSpecification</returns>
        public virtual CancelMembershipSpecification GetCancelMembershipSpecification()
        {
            return new CancelMembershipSpecification();
        }

        /// <summary>
        /// パスワードリマインダ用サービス
        /// </summary>
        /// <returns>new instance of ErsPasswodReminderService</returns>
        public virtual ErsPasswodReminderService GetErsPasswordReminderService()
        {
            return new ErsPasswodReminderService();
        }

        /// <summary>
        /// 全会員数を取得する
        /// </summary>
        /// <returns>new instance of NewMemberAllCountSpecification</returns>
        public virtual NewMemberAllCountSpecification GetNewMemberAllCountSpecification()
        {
            return new NewMemberAllCountSpecification();
        }

        /// <summary>
        /// 会員情報の一覧を取得する。
        /// </summary>
        /// <returns>new instance of MemberSearchSpecification</returns>
        public virtual MemberSearchSpecification GetMemberSerachSpecification()
        {
            return new MemberSearchSpecification();
        }

        /// <summary>
        /// Get the class of ObtainMemberPointStgy
        /// </summary>
        /// <returns>new instance of ObtainMemberPointStgy</returns>
        public virtual ObtainMemberPointStgy GetObtainMemberPointStgy()
        {
            return new ObtainMemberPointStgy();
        }
        /// <summary>
        ///  セキュリティアンサー一致チェック
        /// </summary>
        /// <returns>new instance of CheckQuesConfirmStgy</returns>
        public virtual CheckQuesConfirmStgy GetCheckQuesConfirmStgy()
        {
            return new CheckQuesConfirmStgy();
        }
        /// <summary>
        /// Get the class of CheckExitMemberWithEmailStgy
        /// </summary>
        /// <returns>new instance of CheckExitMemberWithEmailStgy</returns>
        public virtual CheckExitMemberWithEmailStgy GetCheckExitMemberWithEmailStgy()
        {
            return new CheckExitMemberWithEmailStgy();
        }
        /// <summary>
        /// Get the class of WishlistSearchSpecification
        /// </summary>
        /// <returns>new instance of WishlistSearchSpecification</returns>
        public virtual WishlistSearchSpecification GetWishlistSearchSpecification()
        {
            return new WishlistSearchSpecification();
        }
        /// <summary>
        /// Get the class of CheckPasswdConfirmStgy
        /// </summary>
        /// <returns>new instance of CheckPasswdConfirmStgy</returns>
        public virtual CheckPasswdConfirmStgy GetCheckPasswdConfirmStgy()
        {
            return new CheckPasswdConfirmStgy();
        }
        /// <summary>
        /// Get the class of CheckEmailConfirmStgy
        /// </summary>
        /// <returns>new instance of CheckEmailConfirmStgy</returns>
        public virtual CheckEmailConfirmStgy GetCheckEmailConfirmStgy()
        {
            return new CheckEmailConfirmStgy();
        }

        /// <summary>
        /// Get the class of CheckDuplicateEmailStgy
        /// </summary>
        /// <returns>new instance of CheckDuplicateEmailStgy</returns>
        public virtual CheckDuplicateEmailStgy GetCheckDuplicateEmailStgy()
        {
            return new CheckDuplicateEmailStgy();
        }

        /// <summary>
        /// Get the repository of ErsMemberCard
        /// </summary>
        /// <returns>new instance of ErsMemberCardRepository</returns>
        public virtual ErsMemberCardRepository GetErsMemberCardRepository()
        {
            return new ErsMemberCardRepository();
        }
        /// <summary>
        /// ErsMemberCardRepository用Criteria
        /// </summary>
        /// <returns>new instance of ErsMemberCardCriteria</returns>
        public virtual ErsMemberCardCriteria GetErsMemberCardCriteria()
        {
            return new ErsMemberCardCriteria();
        }
        /// <summary>
        /// Get the class ErsMemberCard
        /// </summary>
        /// <returns>new instance of ErsMemberCard</returns>
        public virtual ErsMemberCard GetErsMemberCard()
        {
            return new ErsMemberCard();
        }

        /// <summary>
        /// Get the class of ValidatePrefStgy
        /// </summary>
        /// <returns>new instance of ValidatePrefStgy</returns>
        public virtual ValidatePrefStgy GetValidatePrefStgy()
        {
            return new ValidatePrefStgy();
        }

        /// <summary>
        /// Get the class of ValidateJobStgy
        /// </summary>
        /// <returns>new instance of ValidateJobStgy</returns>
        public virtual ValidateJobStgy GetValidateJobStgy()
        {
            return new ValidateJobStgy();
        }
        /// <summary>
        /// Get the class of ValidateBirthdayStgy
        /// </summary>
        /// <returns>new instance of ValidateBirthdayStgy</returns>
        public virtual ValidateBirthdayStgy GetValidateBirthdayStgy()
        {
            return new ValidateBirthdayStgy();
        }

        /// <summary>
        /// Get the class of ValidateQuesStgy
        /// </summary>
        /// <returns>new instance of ValidateQuesStgy</returns>
        public virtual ValidateQuesStgy GetValidateQuesStgy()
        {
            return new ValidateQuesStgy();
        }

        public virtual ErsRegularOrderRecordRepository GetErsRegularOrderRecordRepository()
        {
            return new ErsRegularOrderRecordRepository();
        }

        public virtual ValidateDateSearchStgy GetValidateDateSearchStgy()
        {
            return new ValidateDateSearchStgy();
        }

        public virtual string GetMemberCodeByEmail(String email)
        {
            //メアドに紐づく最新の会員コード1件を取得
            email = email.Replace("<", "").Replace(">", "").Trim() ;
            var memRepo = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            criteria.email = email;
            criteria.deleted = EnumDeleted.NotDeleted;
            criteria.AddOrderBy("mcode", Criteria.OrderBy.ORDER_BY_DESC);

            var list = memRepo.Find(criteria);

            if (list.Count != 1)
            { 
                return  null;
            }

            return list.First().mcode ;
        }

        public virtual CheckInOperationCardRegularOrderStgy GetCheckInOperationCardRegularOrder()
        {
            return new CheckInOperationCardRegularOrderStgy();
        }

        public virtual GetAgeCodeStgy GetGetAgeCodeStgy()
        {
            return new GetAgeCodeStgy();
        }

        public virtual GetAgeStgy GetGetAgeStgy()
        {
            return new GetAgeStgy();
        }

        public virtual ErsQuesRepository GetErsQuesRepository()
        {
            return new ErsQuesRepository();
        }

        public virtual ErsQuesCriteria GetErsQuesCriteria()
        {
            return new ErsQuesCriteria();
        }

        public virtual ErsQues GetErsQues()
        {
            return new ErsQues();
        }

        public virtual ErsQues GetErsQuesWithParameter(Dictionary<string, object> dr)
        {
            var objQues = this.GetErsQues();
            objQues.OverwriteWithParameter(dr);
            return objQues;
        }

        public virtual ErsJobRepository GetErsJobRepository()
        {
            return new ErsJobRepository();
        }

        public virtual ErsJobCriteria GetErsJobCriteria()
        {
            return new ErsJobCriteria();
        }

        public virtual ErsJob GetErsJobWithParameter(Dictionary<string, object> dr)
        {
            var objJob = this.GetErsJob();
            objJob.OverwriteWithParameter(dr);
            return objJob;
        }

        public virtual ErsJob GetErsJob()
        {
            return new ErsJob();
        }

        public virtual CheckCardAlreadyStoredSpec GetCheckCardAlreadyStoredSpec()
        {
            return new CheckCardAlreadyStoredSpec();
        }

        public virtual MemberRegularErrListSpec GetMemberRegularErrListSpec()
        {
            return new MemberRegularErrListSpec();
        }

        public virtual ErsWishlistRepository GetErsWishlistRepository()
        {
            return new ErsWishlistRepository();
        }

        public virtual ErsWishlistCriteria GetErsWishlistCriteria()
        {
            return new ErsWishlistCriteria();
        }

        public virtual ErsWishlist GetErsWishlist()
        {
            return new ErsWishlist();
        }

        public virtual ErsWishlist GetErsWishlistWithParameter(Dictionary<string, object> parameters)
        {
            var objWishlist = this.GetErsWishlist();
            objWishlist.OverwriteWithParameter(parameters);
            return objWishlist;
        }

        public virtual ValidateCardSaveCountStgy GetValidateCardSaveCountStgy()
        {
            return new ValidateCardSaveCountStgy();
        }

        public virtual ValidateDeleteCardStgy GetValidateDeleteCardStgy()
        {
            return new ValidateDeleteCardStgy();
        }

        public virtual IsCardHasRegularOrderSpec GetIsCardHasRegularOrderSpec()
        {
            return new IsCardHasRegularOrderSpec();
        }

        public virtual RetrieveAlreadyStoredMemberCardStgy GetRetrieveAlreadyStoredMemberCardStgy()
        {
            return new RetrieveAlreadyStoredMemberCardStgy();
        }

        public virtual ErsMemberRankRepository GetErsMemberRankRepository()
        {
            return new ErsMemberRankRepository();
        }

        public virtual ErsMemberRankCriteria GetErsMemberRankCriteria()
        {
            return new ErsMemberRankCriteria();
        }

        public virtual ErsMemberRank GetErsMemberRank()
        {
            return new ErsMemberRank();
        }

        public virtual ErsMemberRankSetupRepository GetErsMemberRankSetupRepository()
        {
            return new ErsMemberRankSetupRepository();
        }

        public virtual MemberRankSetupSearchSpecification GetErsMemberRankSetupSearchSpecification()
        {
            return new MemberRankSetupSearchSpecification();
        }

        public virtual ErsMemberRankSetupCriteria GetErsMemberRankSetupCriteria()
        {
            return new ErsMemberRankSetupCriteria();
        }

        public virtual ErsMemberRankSetup GetErsMemberRankSetup()
        {
            return new ErsMemberRankSetup();
        }

        public virtual ErsMemberRankSetup GetErsMemberRankSetupWithRank(int? rank, int? site_id)
        {
            var repository = this.GetErsMemberRankSetupRepository();
            var criteria = this.GetErsMemberRankSetupCriteria();
            criteria.rank = rank;
            criteria.active = EnumActive.Active;
            criteria.site_id = site_id;

            var listRankSetup = repository.Find(criteria);
            if (listRankSetup.Count != 1)
            {
                return null;
            }
            return listRankSetup.First();
        }

        public virtual string GetRankNameWithRank(int? rank, int? site_id)
        {
            var repository = this.GetErsMemberRankSetupRepository();
            var criteria = this.GetErsMemberRankSetupCriteria();
            criteria.rank = rank;
            criteria.active = EnumActive.Active;

            if (ErsFactory.ersUtilityFactory.getSetup().member_rank_centralization)
            {
                criteria.site_id_for_admin = (int)EnumSiteId.COMMON_SITE_ID;
            }
            else
            {
                criteria.site_id_not_equal = (int)EnumSiteId.COMMON_SITE_ID;
            }

            var listRankSetup = repository.FindSingle(criteria);
            if(listRankSetup == null)
            {
                return null;
            }
            if (Convert.ToInt32(listRankSetup.site_id) != Convert.ToInt32(site_id))
            {
                return null;
            }

            return listRankSetup.rank_name;
        }

        public virtual ErsMemberRankSetup GetErsMemberRankSetupWithId(int? id)
        {
            var repository = this.GetErsMemberRankSetupRepository();
            var criteria = this.GetErsMemberRankSetupCriteria();
            criteria.id = id;
            var listRankSetup = repository.Find(criteria);
            if (listRankSetup.Count != 1)
            {
                return null;
            }
            return listRankSetup.First();
        }

        public virtual RetrieveMemberRankStgy GetRetrieveMemberRankStgy()
        {
            return new RetrieveMemberRankStgy();
        }

        public virtual ErsMemberRankSetup getErsMemberRankSetupWithParameter(Dictionary<string, object> parameters)
        {
            var member_rank_setup = ErsFactory.ersMemberFactory.GetErsMemberRankSetup();
            member_rank_setup.OverwriteWithParameter(parameters);
            return member_rank_setup;
        }

        public virtual RetrieveMemberRankGrantLowerLimitSpec GetRetrieveMemberRankGrantLowerLimitSpec()
        {
            return new RetrieveMemberRankGrantLowerLimitSpec();
        }

        public virtual MemberRankDeleteSpec GetMemberRankDeleteSpec()
        {
            return new MemberRankDeleteSpec();
        }

        public virtual MemberRankRegistSpec GetMemberRankRegistSpec()
        {
            return new MemberRankRegistSpec();
        }

        public virtual CheckEmailRequiredStgy GetCheckEmailRequiredStgy()
        {
            return new CheckEmailRequiredStgy();
        }   

        public virtual HasNotYetSaledIssuedRegularOrderSpec GetHasNotYetSaledIssuedRegularOrderSpec()
        {
            return new HasNotYetSaledIssuedRegularOrderSpec();
        }

        public virtual UpdateMemberLockTryCountStgy GetUpdateMemberLockTryCountStgy()
        {
            return new UpdateMemberLockTryCountStgy();
        }

        public virtual int? GetMemberLockTryCount(string email)
        {
            var rep = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var cri = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            cri.email = email;
            cri.deleted = EnumDeleted.NotDeleted;
            var result = rep.Find(cri);
            if (result.Count == 1)
            {
                return result.First().login_try_count++;
            }
            return null;
        }

        public virtual EnumAccountStatus GetMemberAccountStatus(string email)
        {
            var rep = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var cri = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            cri.email = email;
            cri.deleted = EnumDeleted.NotDeleted;
            var result = rep.Find(cri);
            if (result.Count == 1)
            {
                return result.First().account_status;
            }
            return EnumAccountStatus.NotLocked;
        }

        public virtual ObtainMemberSiteIdStgy GetObtainMemberSiteIdStgy()
        {
            return new ObtainMemberSiteIdStgy();
        }

        /// <summary>
        /// MemberRankJoinSetupSearchSpecification取得 [Get MemberRankJoinSetupSearchSpecification]
        /// </summary>
        /// <returns>MemberRankJoinSetupSearchSpecification</returns>
        public virtual MemberRankJoinSetupSearchSpecification GetMemberRankJoinSetupSearchSpecification()
        {
            return new MemberRankJoinSetupSearchSpecification();
        }

        /// <summary>
        /// ObtainMemberRankNameStgy取得 [Get ObtainMemberRankNameStgy]
        /// </summary>
        /// <returns>ObtainMemberRankNameStgy</returns>
        public virtual ObtainMemberRankNameStgy GetObtainMemberRankNameStgy()
        {
            return new ObtainMemberRankNameStgy();
        }
    }
}
