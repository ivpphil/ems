﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.member
{
    public class ErsQues
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string ques_name { get; set; }

        public EnumActive? active { get; set; }

        public DateTime? intime { get; set; }

        public DateTime? utime { get; set; }

        public int? site_id { get; set; }
    }
}
