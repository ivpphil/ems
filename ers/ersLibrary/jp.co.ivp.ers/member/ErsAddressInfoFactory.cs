﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.member.specification;

namespace jp.co.ivp.ers.member
{
    /// <summary>
    /// Provides methods to obtain instances of addressinfo-related classes
    /// </summary>
    public class ErsAddressInfoFactory
    {
        /// <summary>
        /// get the class repository of addressInfo
        /// </summary>
        /// <returns></returns>
        public virtual ErsAddressInfoRepository GetErsAddressInfoRepository()
        {
            return new ErsAddressInfoRepository();
        }

        /// <summary>
        /// get the class addressInfo using with id and mcode
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual ErsAddressInfo GetErsAddressInfoWithId(int? id, string mcode)
        {
            var repository = GetErsAddressInfoRepository();
            var criteria = GetErsAddressInfoCriteria();
            criteria.id = id;
            criteria.mcode = mcode;
            var list = repository.Find(criteria);

            if (list.Count != 1)
                return null;

            return list[0];
        }

        /// <summary>
        /// get the class addressInfo with parameter
        /// </summary>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        public virtual ErsAddressInfo GetErsAddressInfoWithParameter(Dictionary<string, object> dictionary)
        {
            var address = GetErsAddressInfo();
            address.OverwriteWithParameter(dictionary);
            return address;
        }

        /// <summary>
        /// get the class of addressInfo
        /// </summary>
        /// <returns></returns>
        public virtual ErsAddressInfo GetErsAddressInfo()
        {
            return new ErsAddressInfo();
        }


        /// <summary>
        /// ErsAddressInfoRepository用Criteria
        /// </summary>
        /// <returns></returns>
        public virtual ErsAddressInfoCriteria GetErsAddressInfoCriteria()
        {
            return new ErsAddressInfoCriteria();
        }

        /// <summary>
        /// 配送先情報の一覧を取得する。
        /// </summary>
        /// <returns>new instance of ShippingSearchSpecification</returns>
        public virtual ShippingSearchSpecification GetShippingSearchSpecification()
        {
            return new ShippingSearchSpecification();
        }
    }
}
