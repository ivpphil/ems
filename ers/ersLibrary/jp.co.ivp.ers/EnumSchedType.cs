﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers
{
    public enum EnumSchedType : short
    {
        NoSchedType = 0,    // --
        Meeting = 1,        // 会議
        Rest = 2,           // 休み
        ToVisit = 3,        // 往訪
        Visit = 4,          // 来訪
        Operation = 5,      // 作業
        BusinessTrip = 6,   // 出張
        Other = 7,          // その他
        WorkSchedule = 8, 
        VacationLeave = 9,
        SickLeave = 10,
        EmergencyLeave = 11


    }
}
