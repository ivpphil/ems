﻿namespace jp.co.ivp.ers
{
    public enum EnumLeaveType
    {
        VacationLeave = 1,
        SickLeave,
        MaternityLeave,
        PaternityLeave,
        ParentalLeave,
        NoPayLeave,
        CompassionateLeave,
        MarriageLeave,
        EmergencyLeave,
        Others
    }
}
