﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumEnqType
    {
        None = 0,
        InboundPhone = 1,
        InboundEmail,
        InboundFax
    }
}
