﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    //use for getting process status
    public enum EnumAmProcessStatus
        : short
    {
        /// <summary>
        /// 0 : 未設定 [None]
        /// </summary>
        None,

        /// <summary>
        /// 1 : 未配信 [NotSend]
        /// </summary>
        NotSend,

        /// <summary>
        /// 2 : アップロード中 [Uploading]
        /// </summary>
        Uploading,

        /// <summary>
        /// 3 : 配信中 [Sending]
        /// </summary>
        Sending,

        /// <summary>
        /// 4 : 配信完了 [Sent]
        /// </summary>
        Sent
    }
}
