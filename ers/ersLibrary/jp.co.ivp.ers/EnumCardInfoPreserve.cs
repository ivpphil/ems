﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumCardInfoPreserve
    {
        /// <summary>
        /// カード番号をサーバーに保持
        /// </summary>
        OnServer,

        /// <summary>
        /// カード情報を決済会社に保持
        /// </summary>
        OnService
    }
}
