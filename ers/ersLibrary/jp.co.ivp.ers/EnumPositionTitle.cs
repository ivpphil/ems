﻿namespace jp.co.ivp.ers
{
    public enum EnumPositionTitle
    {
        Not_set,
        President,
        Vice_President,
        Director,
        HR,
        Team_Leader,
        System_Administrator,
        Software_Engineer,
        Business_Analyst,
        Quality_Assurance,
        Frontend_Developer,

    }
}
