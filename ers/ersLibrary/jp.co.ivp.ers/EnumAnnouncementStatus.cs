﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers
{
    public enum EnumAnnouncementStatus : short
    {
        Deleted = 0,
        New = 1,
        Edited = 2
    }
}
