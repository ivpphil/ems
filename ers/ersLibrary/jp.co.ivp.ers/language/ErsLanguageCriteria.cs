﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.language
{
    /// <summary>
    /// represents searched conditions for properties that are in ErsAdministrator class.
    /// Inherits Criteria class.
    /// </summary>
    public class ErsLanguageCriteria
        : Criteria
    {
        /// <summary>
        /// sets criteria for id with the condition of language_t.id = value
        /// </summary>
        public int id
        {
            set
            {
                this.Add(Criteria.GetCriterion("language_t.id", value, Operation.EQUAL));
            }
        }

        public string abbrev
        {
            set
            {
                this.Add(Criteria.GetCriterion("language_t.abbrev", value, Operation.EQUAL));
            }
        }

        public int? id_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("language_t.id", value, Operation.NOT_EQUAL));
            }
        }

        public EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("language_t.active", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// gets sort order by id
        /// </summary>
        public void SetOrderById(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("language_t.id", orderBy);
        }
      
    }
}
