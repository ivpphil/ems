﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.state;

namespace jp.co.ivp.ers.language
{
 
    /// <summary>
    /// Hold values from language_t table.
    /// Inherits ErsRepositoryEntity class.
    /// Language function
    /// </summary>
    public class ErsLanguage
        : ErsRepositoryEntity
    {
        /// <summary>
        /// Unique key ID
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// language 
        /// </summary>
        public virtual string lang { get; set; }

        /// <summary>
        /// language en
        /// </summary>
        public virtual string lang_en { get; set; }


        /// <summary>
        /// abbreviation
        /// </summary>
        public virtual string abbrev { get; set; }

        /// <summary>
        /// language 
        /// </summary>
        public virtual string culture { get; set; }

        /// <summary>
        /// active
        /// </summary>
        public virtual EnumActive active { get; set; }

        /// <summary>
        /// intime
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// utime
        /// </summary>
        public virtual DateTime? utime { get; set; }
    }
}
