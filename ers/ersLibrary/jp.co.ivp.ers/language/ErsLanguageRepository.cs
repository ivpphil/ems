﻿using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.language
{
    /// <summary>
    /// Provide methods to connect with language_t table. 
    /// Inherits ErsRepository<ErsLanguage>
    /// </summary>
    public class ErsLanguageRepository
        : ErsRepository<ErsLanguage>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsLanguageRepository()
            : base("language_t")
        {
        }
    }
}
