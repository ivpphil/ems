﻿using System.Collections.Generic;
using System.Linq;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.language
{
    /// <summary>
    /// Factory class for ErsLanguage.
    /// Provide methods that are related in Language.
    /// </summary>
    public class ErsLanguageFactory
    {
        protected static ErsLanguageRepository ErsLanguageRepository
        {
            get
            {
                return (ErsLanguageRepository)ErsCommonContext.GetPooledObject("_ErsLanguageRepository");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_ErsLanguageRepository", value);
            }
        }

        /// <summary>
        /// 管理者情報リポジトリを取得する
        /// </summary>
        /// <returns>returns instance of ErsLanguageRepository.</returns>
        public virtual ErsLanguageRepository GetErsLanguageRepository()
        {
            return new ErsLanguageRepository(); ;
        }

        /// <summary>
        /// Obtain instance of ErsLanguageCriteria class.
        /// </summary>
        /// <returns>returns instance of ErsLanguageCriteria.</returns>
        public ErsLanguageCriteria GetErsLanguageCriteria()
        {
            return new ErsLanguageCriteria();
        }
        /// <summary>
        /// Obtain instance of ErsLanguage class 
        /// </summary>
        /// <returns>returns instance of ErsLanguage</returns>
        public virtual ErsLanguage GetErsLanguage()
        {
            return new ErsLanguage();
        }

        /// <summary>
        /// Get records from Language_t using OverwriteWithParameter function of ErsLanguage
        /// </summary>
        /// <param name="data">values of Language saved in dictionary</param>
        /// <returns>return values from ErsLanguage</returns>
        public virtual ErsLanguage GetErsLanguageWithParameter(Dictionary<string, object> data)
        {
            ErsLanguage tmpData = this.GetErsLanguage();
            tmpData.OverwriteWithParameter(data);
            return tmpData;
        }

        /// <summary>
        /// Gets list of searched records according to the criteria using user code
        /// </summary>
        /// <param name="abbrev">user code</param>
        /// <returns>returns record from Language_t table when there's record found, returns null if it's not existing.</returns>
        public virtual ErsLanguage GetErsLanguageWithAbbrev(string abbrev)
        {
            var adminRepository = this.GetErsLanguageRepository();
            var adminCriteria = this.GetErsLanguageCriteria();
            adminCriteria.abbrev = abbrev;
            var dataList = adminRepository.Find(adminCriteria);

            if (dataList.Count == 1)
            {
                return dataList[0];
            }

            return null;
        }

        /// <summary>
        /// Gets list of searched records according to the criteria using id
        /// </summary>
        /// <param name="id">Language ID use for finding of record using ErsLanguageCriteria</param>
        /// <returns>returns data from Language_t table, returns null if not existing</returns>
        public ErsLanguage GetErsLanguageWithId(int? id)
        {
            if (!id.HasValue)
            {
                return null;
            }

            var adminRepository = this.GetErsLanguageRepository();
            var adminCriteria = this.GetErsLanguageCriteria();
            adminCriteria.id = id.Value;
            var dataList = adminRepository.Find(adminCriteria);

            if (dataList.Count == 1)
            {
                return dataList[0];
            }

            return null;
        }
     
    }
}
