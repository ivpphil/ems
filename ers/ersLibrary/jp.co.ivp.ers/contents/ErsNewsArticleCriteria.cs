﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.contents
{
    public class ErsNewsArticleCriteria : Criteria
    {
        public virtual int? id
        {
            set
            {
                Add(Criteria.GetCriterion("id", value, Operation.EQUAL));
            }
        }

        public virtual string article_code
        {
            set
            {
                Add(Criteria.GetCriterion("news_article_t.article_code", value, Operation.EQUAL));
            }
        }

        public virtual string contents_code
        {
            set { this.Add(Criteria.GetCriterion("news_article_t.contents_code", value, Operation.EQUAL)); }
        }

        public virtual string scode
        {
            set
            {
                Add(Criteria.GetCriterion("news_article_t.scode", value, Operation.EQUAL));
            }
        }

        public virtual string title
        {
            set { this.Add(Criteria.GetCriterion("news_article_t.title", value, Operation.EQUAL)); }
        }

        public virtual string sub_title
        {
            set { this.Add(Criteria.GetCriterion("news_article_t.sub_title", value, Operation.EQUAL)); }
        }

        public virtual string body
        {
            set { this.Add(Criteria.GetCriterion("news_article_t.body", value, Operation.EQUAL)); }
        }

        public virtual string add_body
        {
            set { this.Add(Criteria.GetCriterion("news_article_t.add_body", value, Operation.EQUAL)); }
        }

        public virtual EnumActive? active
        {
            set { this.Add(Criteria.GetCriterion("news_article_t.active", value, Operation.EQUAL)); }
        }

        public virtual string keywords
        {
            set
            {
                this.Add(Criteria.JoinWithOR(
                        new[] { 
                            Criteria.GetLikeClauseCriterion("news_article_t.title", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH),
                            Criteria.GetLikeClauseCriterion("administrator_t.user_name", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH)
                        }));
            }
        }

        public virtual DateTime? posted_date_than_eq
        {
            set
            {
                this.Add(Criteria.GetCriterion("news_article_t.posted_date", value, Operation.GREATER_EQUAL));
            }
        }

        public virtual DateTime? posted_date_less_eq
        {
            set
            {
                this.Add(Criteria.GetCriterion("news_article_t.posted_date", value, Operation.LESS_EQUAL));
            }
        }

        public virtual DateTime? posted_date_than
        {
            set
            {
                this.Add(Criteria.GetCriterion("news_article_t.posted_date", value, Operation.GREATER_THAN));
            }
        }

        public virtual DateTime? posted_date_less
        {
            set
            {
                this.Add(Criteria.GetCriterion("news_article_t.posted_date", value, Operation.LESS_THAN));
            }
        }

        public virtual DateTime? period_from_than_eq
        {
            set
            {
                this.Add(Criteria.GetCriterion("news_article_t.period_from", value, Operation.GREATER_EQUAL));
            }
        }

        public virtual DateTime? period_from_than
        {
            set
            {
                this.Add(Criteria.GetCriterion("news_article_t.period_from", value, Operation.GREATER_THAN));
            }
        }

        public virtual DateTime? period_from_less_eq
        {
            set
            {
                this.Add(Criteria.GetCriterion("news_article_t.period_from", value, Operation.LESS_EQUAL));
            }
        }

        public virtual DateTime? period_from_less
        {
            set
            {
                this.Add(Criteria.GetCriterion("news_article_t.period_from", value, Operation.LESS_THAN));
            }
        }

        public virtual DateTime? period_to_than_eq
        {
            set
            {
                this.Add(Criteria.GetCriterion("news_article_t.period_to", value, Operation.GREATER_EQUAL));
            }
        }

        public virtual DateTime? period_to_less_eq
        {
            set
            {
                this.Add(Criteria.GetCriterion("news_article_t.period_to", value, Operation.LESS_EQUAL));
            }
        }

        public virtual DateTime? period_to_than
        {
            set
            {
                this.Add(Criteria.GetCriterion("news_article_t.period_to", value, Operation.GREATER_THAN));
            }
        }

        public virtual DateTime? period_to_less
        {
            set
            {
                this.Add(Criteria.GetCriterion("news_article_t.period_to", value, Operation.LESS_THAN));
            }
        }

        public void SetOrderByPosted_date(OrderBy orderBy)
        {
            this.AddOrderBy("news_article_t.posted_date", orderBy);
        }

        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("news_article_t.id", orderBy);
        }

        public void SetGroupById()
        {
            this.AddGroupBy("news_article_t.id");
        }

        /// <summary>
        /// Sets the criteria for cms_contents_t.site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var sql = "EXISTS(SELECT * FROM cms_contents_t WHERE cms_contents_t.contents_code = news_article_t.contents_code AND (cms_contents_t.site_id = :site_id OR cms_contents_t.site_id = :common_site_id))";
                var values = new Dictionary<string, object>() { 
                { "site_id", value },
                { "common_site_id", (int)EnumSiteId.COMMON_SITE_ID }};
                this.Add(Criteria.GetUniversalCriterion(sql, values));
            }
        }

        /// <summary>
        /// Sets the criteria for cms_contents_t.available_template
        /// </summary>
        public void available_template()
        {
            this.Add(Criteria.GetUniversalCriterion("EXISTS(SELECT * FROM cms_contents_t WHERE cms_contents_t.contents_code = news_article_t.contents_code AND cms_contents_t.available_template = ANY(array[news_article_t.template_code]) )"));
        }
    }
}
