﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;

namespace jp.co.ivp.ers.contents
{
    public class ErsCmsContents : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual string contents_code { get; set; }
        public virtual string contents_name { get; set; }
        public virtual string contents_name_admin { get; set; }
        public virtual string code_name { get; set; }
        public virtual string title_name { get; set; }
        public virtual string sub_title_name { get; set; }
        public virtual string body_name { get; set; }
        public virtual string add_body_name { get; set; }
        public virtual string period_name { get; set; }
        public virtual string[] available_template { get; set; }
        public virtual int? link_count { get; set; }
        public virtual int? file_count { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual EnumActive? active { get; set; }
        public virtual EnumRequired? req_login_flg { get; set; }
        public virtual int? site_id { get; set; }
    }
}
