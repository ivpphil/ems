﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;

namespace jp.co.ivp.ers.contents
{
    public class ErsNewsArticle : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual string article_code { get; set; }
        public virtual string contents_code { get; set; }
        public virtual string template_code { get; set; }
        public virtual DateTime? posted_date { get; set; }
        public virtual string scode { get; set; }
        public virtual string title { get; set; }
        public virtual string sub_title { get; set; }
        public virtual string body { get; set; }
        public virtual string add_body { get; set; }
        public virtual int? category { get; set; }
        public virtual DateTime? period_from { get; set; }
        public virtual DateTime? period_to { get; set; }
        public virtual string[] img_file_name { get; set; }
        public virtual string link_string_1 { get; set; }
        public virtual string link_url_1 { get; set; }
        public virtual string link_string_2 { get; set; }
        public virtual string link_url_2 { get; set; }
        public virtual string link_string_3 { get; set; }
        public virtual string link_url_3 { get; set; }
        public virtual string link_string_4 { get; set; }
        public virtual string link_url_4 { get; set; }
        public virtual string link_string_5 { get; set; }
        public virtual string link_url_5 { get; set; }
        public virtual string file_string_1 { get; set; }
        public virtual string file_real_name_1 { get; set; }
        public virtual string file_name_1 { get; set; }
        public virtual string file_string_2 { get; set; }
        public virtual string file_real_name_2 { get; set; }
        public virtual string file_name_2 { get; set; }
        public virtual string file_string_3 { get; set; }
        public virtual string file_real_name_3 { get; set; }
        public virtual string file_name_3 { get; set; }
        public virtual string file_string_4 { get; set; }
        public virtual string file_real_name_4 { get; set; }
        public virtual string file_name_4 { get; set; }
        public virtual string file_string_5 { get; set; }
        public virtual string file_real_name_5 { get; set; }
        public virtual string file_name_5 { get; set; }
        public virtual int? create_user_id { get; set; }
        public virtual int? upd_user_id { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual EnumActive? active { get; set; }
        public virtual int? site_id { get; set; }
    }
}
