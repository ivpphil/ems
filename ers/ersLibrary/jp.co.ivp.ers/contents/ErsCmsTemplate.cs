﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;

namespace jp.co.ivp.ers.contents
{
    public class ErsCmsTemplate : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual string template_code { get; set; }
        public virtual string template_type { get; set; }
        public virtual string template_file_path { get; set; }
        public virtual EnumCmsFieldType? code_use_flg { get; set; }
        public virtual EnumCmsFieldType? title_use_flg { get; set; }
        public virtual EnumCmsFieldType? sub_title_use_flg { get; set; }
        public virtual EnumCmsFieldType? body_use_flg { get; set; }
        public virtual EnumCmsFieldType? add_body_use_flg { get; set; }
        public virtual string[] img_item_name { get; set; }
        public virtual int[] img_group_count { get; set; }
        public virtual int[] img_width { get; set; }
        public virtual int? disp_order { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual int? active { get; set; }
    }
}
