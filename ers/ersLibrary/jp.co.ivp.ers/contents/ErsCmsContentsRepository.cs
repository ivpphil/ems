﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.util;
using System.Collections;

namespace jp.co.ivp.ers.contents
{
    public class ErsCmsContentsRepository : ErsRepository<ErsCmsContents>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsCmsContentsRepository()
            : base("cms_contents_t")
        {
        }
    }
}
