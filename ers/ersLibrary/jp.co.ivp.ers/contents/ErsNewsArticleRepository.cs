﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.util;
using System.Collections;
using jp.co.ivp.ers.contents.specification;

namespace jp.co.ivp.ers.contents
{
    public class ErsNewsArticleRepository : ErsRepository<ErsNewsArticle>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsNewsArticleRepository()
            : base("news_article_t")
        {
        }

        public override IList<ErsNewsArticle> Find(Criteria criteria)
        {
            var newsArticleSearchSpecification = ErsFactory.ersContentsFactory.GetNewsArticleSearchSpecification();
            List<ErsNewsArticle> retList = new List<ErsNewsArticle>();
            var tmpList = newsArticleSearchSpecification.GetSearchData(criteria);
            foreach (Dictionary<string, object> data in tmpList)
            {
                ErsNewsArticle tmpAdminData = ErsFactory.ersContentsFactory.GetErsNewsArticleWithParameters(data);
                retList.Add(tmpAdminData);
            }
            return retList;
        }

        public override long GetRecordCount(Criteria criteria)
        {
            var newsArticleSearchSpecification = ErsFactory.ersContentsFactory.GetNewsArticleSearchSpecification();
            return newsArticleSearchSpecification.GetCountData(criteria);
        }
    }
}
