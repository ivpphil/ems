﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.contents
{
    public class ErsCmsContentsCriteria : Criteria
    {
        public virtual int? id
        {
            set
            {
                Add(Criteria.GetCriterion("id", value, Operation.EQUAL));
            }
        }

        public int? id_not_equal
        {
            set
            {
                Add(Criteria.GetCriterion("id", value, Operation.NOT_EQUAL));
            }
        }

        public virtual string contents_code
        {
            set { this.Add(Criteria.GetCriterion("cms_contents_t.contents_code", value, Operation.EQUAL)); }
        }

        public virtual string contents_code_like_prefix
        {
            set { this.Add(Criteria.GetLikeClauseCriterion("cms_contents_t.contents_code", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH)); }
        }

        public virtual string contents_code_like_suffix
        {
            set { this.Add(Criteria.GetLikeClauseCriterion("cms_contents_t.contents_code", value, LIKE_SEARCH_TYPE.SUFFIX_SEARCH)); }
        }

        public virtual string contents_code_not
        {
            set { this.Add(Criteria.GetCriterion("cms_contents_t.contents_code", value, Operation.NOT_EQUAL)); }
        }

        public virtual void SetActiveOnly()
        {
            this.Add(Criteria.GetCriterion("cms_contents_t.active", 1, Operation.EQUAL));
        }

        public virtual void SetOrderByUtime(OrderBy orderBy)
        {
            AddOrderBy("cms_contents_t.utime", orderBy);
        }

        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("cms_contents_t.id", orderBy);
        }

        public void SetOrderByContentsCode(OrderBy orderBy)
        {
            this.AddOrderBy("cms_contents_t.contents_code", orderBy);
        }

        /// <summary>
        /// Sets the criteria for s_master_t.site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("cms_contents_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cms_contents_t.site_id",(int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}
