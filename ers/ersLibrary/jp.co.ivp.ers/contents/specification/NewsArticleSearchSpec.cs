﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.contents.specification
{
    public class NewsArticleSearchSpecification
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return "SELECT DISTINCT news_article_t.* FROM news_article_t LEFT JOIN administrator_t ON  news_article_t.upd_user_id = administrator_t.id or news_article_t.create_user_id= administrator_t.id ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT count(DISTINCT news_article_t.id) AS " + countColumnAlias + " FROM news_article_t LEFT JOIN administrator_t ON  news_article_t.upd_user_id = administrator_t.id or news_article_t.create_user_id= administrator_t.id ";
        }
    }
}
