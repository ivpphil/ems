﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.contents.specification;

namespace jp.co.ivp.ers.contents
{
    public class ErsContentsFactory
    {
        public virtual ErsCmsContentsCriteria GetErsCmsContentsCriteria()
        {
            return new ErsCmsContentsCriteria();
        }

        public virtual ErsCmsContentsRepository GetErsCmsContentsRepository()
        {
            return new ErsCmsContentsRepository();
        }

        public virtual ErsCmsContents GetErsCmsContents()
        {
            return new ErsCmsContents();
        }

        public virtual ErsCmsContents GetErsCmsContentsWithModel(IErsModelBase model)
        {
            var contents = this.GetErsCmsContents();
            contents.OverwriteWithModel(model);
            return contents;
        }

        public virtual ErsCmsContents GetErsCmsContentsWithParameters(Dictionary<string, object> parameters)
        {
            var contents = this.GetErsCmsContents();
            contents.OverwriteWithParameter(parameters);
            return contents;
        }

        public virtual ErsCmsContents GetErsCmsContentsWithId(int? id)
        {
            var repository = this.GetErsCmsContentsRepository();
            var criteria = this.GetErsCmsContentsCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count == 0)
                return null;

            return list[0];
        }

        public virtual ErsCmsContents GetErsCmsContentsWithContentsCode(string contents_code)
        {
            var repository = this.GetErsCmsContentsRepository();

            var criteria = this.GetErsCmsContentsCriteria();
            criteria.contents_code = contents_code;
            criteria.SetActiveOnly();

            var listContents = repository.Find(criteria);

            if (listContents.Count == 0)
            {
                return null;
            }

            return listContents.First();
        }

        public virtual ErsCmsTemplateCriteria GetErsCmsTemplateCriteria()
        {
            return new ErsCmsTemplateCriteria();
        }

        public virtual ErsCmsTemplateRepository GetErsCmsTemplateRepository()
        {
            return new ErsCmsTemplateRepository();
        }

        public virtual ErsCmsTemplate GetErsCmsTemplate()
        {
            return new ErsCmsTemplate();
        }

        public virtual ErsCmsTemplate GetErsCmsTemplateWithTemplateCode(string template_code)
        {
            var repository = this.GetErsCmsTemplateRepository();

            var criteria = this.GetErsCmsTemplateCriteria();
            criteria.template_code = template_code;

            var listContents = repository.Find(criteria);

            if (listContents.Count == 0)
            {
                return null;
            }

            return listContents.First();
        }

        public virtual ErsCmsTemplate ErsCmsTemplateWithParameter(Dictionary<string, object> parameter)
        {
            var objTemplate = this.GetErsCmsTemplate();
            objTemplate.OverwriteWithParameter(parameter);
            return objTemplate;
        }

        public virtual ErsNewsArticleCriteria GetErsNewsArticleCriteria()
        {
            return new ErsNewsArticleCriteria();
        }

        public virtual ErsNewsArticleRepository GetErsNewsArticleRepository()
        {
            return new ErsNewsArticleRepository();
        }

        public virtual ErsNewsArticle GetErsNewsArticle()
        {
            return new ErsNewsArticle();
        }

        public virtual ErsNewsArticle GetErsNewsArticleWithModel(IErsModelBase model)
        {
            var news = this.GetErsNewsArticle();
            news.OverwriteWithModel(model);
            return news;
        }

        public virtual ErsNewsArticle GetErsNewsArticleWithParameters(Dictionary<string, object> parameters)
        {
            var news = this.GetErsNewsArticle();
            news.OverwriteWithParameter(parameters);
            return news;
        }

        public virtual ErsNewsArticle GetErsNewsArticleWithId(int id)
        {
            var repository = this.GetErsNewsArticleRepository();
            var criteria = this.GetErsNewsArticleCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count == 0)
            {
                return null;
            }

            return list[0];
        }

        public virtual ErsNewsArticle GetErsNewsArticleWithArticleCode(string article_code)
        {
            var repository = this.GetErsNewsArticleRepository();
            var criteria = this.GetErsNewsArticleCriteria();
            criteria.article_code = article_code;

            var list = repository.Find(criteria);

            if (list.Count == 0)
            {
                return null;
            }

            return list[0];
        }

        public virtual NewsArticleSearchSpecification GetNewsArticleSearchSpecification()
        {
            return new NewsArticleSearchSpecification();
        }
    }
}
