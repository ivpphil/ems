﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.contents
{
    public class ErsCmsTemplateCriteria : Criteria
    {
        public virtual int id
        {
            set
            {
                Add(Criteria.GetCriterion("id", value, Operation.EQUAL));
            }
        }

        public virtual string template_code
        {
            set { this.Add(Criteria.GetCriterion("cms_template_t.template_code", value, Operation.EQUAL)); }
        }

        public virtual string[] template_code_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("cms_template_t.template_code", value));
            }
        }

        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("cms_template_t.id", orderBy);
        }

        public virtual void SetOrderByDispOrder(OrderBy orderBy)
        {
            this.AddOrderBy("cms_template_t.disp_order", orderBy);
        }
    }
}
