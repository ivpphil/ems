﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumLpUpsellStgy
    {
        /// <summary>
        /// 1: 売商品も合わせて販売する (販売商品＋アップセル販売商品)
        /// </summary>
        Combination = 1,

        /// <summary>
        /// 2: 販売商品をキャンセルする (アップセル販売商品のみ)
        /// </summary>
        UpSellOnly
    }
}
