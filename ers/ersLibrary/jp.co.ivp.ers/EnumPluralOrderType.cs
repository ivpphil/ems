﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// 商品複数回購入制限区分
    /// </summary>
    public enum EnumPluralOrderType
    {
        /// <summary>
        /// 0: 制限なし
        /// </summary>
        NoLimit = 0,

        /// <summary>
        /// 1回のみ
        /// </summary>
        Once = 1
    }
}
