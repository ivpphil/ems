﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumBlackList
        : short
    {
        /// <summary>
        /// 1: 通常のメンバー
        /// </summary>
        Usual = 1,
        /// <summary>
        /// 2 : 灰色のメンバー
        /// </summary>
        Grey,
        /// <summary>
        /// 3: ブラックメンバー
        /// </summary>
        Black
    }
}
