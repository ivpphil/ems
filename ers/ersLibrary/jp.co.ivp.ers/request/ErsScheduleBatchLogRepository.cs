﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.Collections.Generic;

namespace jp.co.ivp.ers.request
{
    public class ErsScheduleBatchLogRepository : ErsRepository<ErsScheduleBatchLog>
    {
        public ErsScheduleBatchLogRepository() : base("sched_batch_log_t")
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsScheduleBatchLogRepository(ErsDatabase objDB) : base("sched_batch_log_t", objDB)
        {
        }

        /// <summary>
        /// insert records into member_t
        /// </summary>
        /// <param name="obj">ErsMember</param>
        public override void Insert(ErsScheduleBatchLog obj, bool storeNewIdToObject = false)
        {
            if (storeNewIdToObject == true)
            {
                obj.id = this.ersDB_table.GetNextSequence();
            }
            base.Insert(obj);
        }

        /// <summary>
        /// search records from member_t
        /// </summary>
        /// <param name="criteria">ErsMemberCriteria</param>
        /// <returns>return a list of result</returns>
        public override IList<ErsScheduleBatchLog> Find(Criteria criteria)
        {
            var specScheduleBatchLog = ErsFactory.ersRequestFactory.GetErsScheduleBatchLogSpecification();
            var list = specScheduleBatchLog.GetSearchData(criteria);

            var retList = new List<ErsScheduleBatchLog>();
            foreach (var dr in list)
            {
                var sched_batch_log = ErsFactory.ersRequestFactory.getErsScheduleBatchLogWithParameter(dr);
                retList.Add(sched_batch_log);
            }
            return retList;
        }
        /// <summary>
        /// get record count of member_t
        /// </summary>
        /// <param name="criteria">ErsMemberCriteria</param>
        /// <returns>return a record count</returns>
        public override long GetRecordCount(Criteria criteria)
        {
            var specScheduleBatchLog = ErsFactory.ersRequestFactory.GetErsScheduleBatchLogSpecification();
            return specScheduleBatchLog.GetCountData(criteria);

        }
    }
}
