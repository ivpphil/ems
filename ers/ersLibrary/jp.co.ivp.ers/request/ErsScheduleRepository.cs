﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.Collections.Generic;

namespace jp.co.ivp.ers.request
{
    public class ErsScheduleRepository : ErsRepository<ErsSchedule>
    {
        public ErsScheduleRepository() : base("schedule_t")
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsScheduleRepository(ErsDatabase objDB) : base("schedule_t", objDB)
        {
        }

        /// <summary>
        /// insert records into member_t
        /// </summary>
        /// <param name="obj">ErsMember</param>
        public override void Insert(ErsSchedule obj, bool storeNewIdToObject = false)
        {
            if (storeNewIdToObject == true)
            {
                obj.id = this.ersDB_table.GetNextSequence();
            }
            base.Insert(obj);
        }

        /// <summary>
        /// search records from member_t
        /// </summary>
        /// <param name="criteria">ErsMemberCriteria</param>
        /// <returns>return a list of result</returns>
        public override IList<ErsSchedule> Find(Criteria criteria)
        {
            var specEmployeeSchedule = ErsFactory.ersRequestFactory.GetEmployeeScheduleSpecification();
            var list = specEmployeeSchedule.GetSearchData(criteria);

            var retList = new List<ErsSchedule>();
            foreach (var dr in list)
            {
                var schedule = ErsFactory.ersRequestFactory.getErsScheduleWithParameter(dr);
                retList.Add(schedule);
            }
            return retList;
        }
        /// <summary>
        /// get record count of member_t
        /// </summary>
        /// <param name="criteria">ErsMemberCriteria</param>
        /// <returns>return a record count</returns>
        public override long GetRecordCount(Criteria criteria)
        {
            var specEmployeeScheduleCount = ErsFactory.ersRequestFactory.GetEmployeeScheduleSpecification();
            return specEmployeeScheduleCount.GetCountData(criteria);

        }

    }



}

