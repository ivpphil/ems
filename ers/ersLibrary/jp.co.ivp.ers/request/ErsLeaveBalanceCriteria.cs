﻿using jp.co.ivp.ers.db;
using System;

namespace jp.co.ivp.ers.request
{
    public class ErsLeaveBalanceCriteria : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("leave_balance_t.id", value, Operation.EQUAL));
            }
        }

        public string emp_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("leave_balance_t.emp_no", value, Operation.EQUAL));
            }
        }

        public double? last_vl
        {
            set
            {
                this.Add(Criteria.GetCriterion("leave_balance_t.last_vl", value, Operation.EQUAL));
            }
        }

        public double? now_vl
        {
            set
            {
                this.Add(Criteria.GetCriterion("leave_balance_t.now_vl", value, Operation.EQUAL));
            }
        }

        public double? total_vl
        {
            set
            {
                this.Add(Criteria.GetCriterion("leave_balance_t.total_vl", value, Operation.EQUAL));
            }
        }

        public double? last_sl
        {
            set
            {
                this.Add(Criteria.GetCriterion("leave_balance_t.last_sl", value, Operation.EQUAL));
            }
        }

        public double? now_sl
        {
            set
            {
                this.Add(Criteria.GetCriterion("leave_balance_t.now_sl", value, Operation.EQUAL));
            }
        }

        public double? total_sl
        {
            set
            {
                this.Add(Criteria.GetCriterion("leave_balance_t.total_sl", value, Operation.EQUAL));
            }
        }

        public string reason
        {
            set
            {
                this.Add(Criteria.GetCriterion("leave_balance_t.reason", value, Operation.EQUAL));
            }
        }

        public DateTime intime
        {
            set
            {
                this.Add(Criteria.GetCriterion("leave_balance_t.intime", value, Operation.EQUAL));
            }
        }

        public double? leave_credits
        {
            set
            {
                this.Add(Criteria.GetCriterion("leave_balance_t.leave_credits", value, Operation.EQUAL));
            }
        }

        public void SetOrderByIntime(OrderBy orderBy)
        {
            AddOrderBy("leave_balance_t.intime", orderBy); 
        }

        public void GetByLeaveType(EnumLeaveType? leave_type)
        {
            if (leave_type.Equals(EnumLeaveType.VacationLeave) || leave_type.Equals(EnumLeaveType.EmergencyLeave))
            {
                this.Add(Criteria.GetUniversalCriterion(@"leave_balance_t.last_vl IS NOT NULL"));
            }
            if (leave_type.Equals(EnumLeaveType.SickLeave))
            {
                this.Add(Criteria.GetUniversalCriterion(@"leave_balance_t.last_sl IS NOT NULL"));
            }
        }

    }
}
