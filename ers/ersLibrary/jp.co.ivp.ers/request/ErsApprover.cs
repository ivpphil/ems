﻿using jp.co.ivp.ers.mvc;
using System;

namespace jp.co.ivp.ers.request
{
    public class ErsApprover : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public virtual int? request_id { get; set; }

        public virtual string emp_no { get; set; }

        public virtual EnumApprovalStatus? approval_status { get; set; }

        public virtual string reason { get; set; }

        public virtual DateTime intime { get; set; }
    }
}
