﻿using jp.co.ivp.ers.mvc;
using System;

namespace jp.co.ivp.ers.request
{
    public class ErsLeaveBalance : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public virtual string emp_no { get; set; }

        public virtual double? last_vl { get; set; }

        public virtual double? now_vl { get; set; }

        public virtual double? total_vl { get; set; }

        public virtual double? last_sl { get; set; }

        public virtual double? now_sl { get; set; }

        public virtual double? total_sl { get; set; }

        public virtual string reason { get; set; }

        public virtual DateTime intime { get; set; }

        public virtual DateTime utime { get; set; }
    }
}
