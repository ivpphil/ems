﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.request
{
    public class ErsRequestRepository : ErsRepository<ErsRequest>
    {
        public ErsRequestRepository() : base("request_t")
        {

        }

        public ErsRequestRepository(ErsDatabase objDB) : base("request_t", objDB)
        {

        }

    }
}
