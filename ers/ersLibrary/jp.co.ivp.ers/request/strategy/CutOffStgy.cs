﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ers.jp.co.ivp.ers.request.strategy
{
    public class CutOffStgy
    {
        /// <summary>
        /// get the cutoff period for the current date
        /// </summary>
        public virtual Tuple<DateTime, DateTime> GetCutOff(DateTime current_date)
        {
            DateTime start_date = new DateTime();
            DateTime end_date = new DateTime();

            if (current_date.Day <= 15)
            {
                if (current_date.Month == 1)
                {
                    start_date = new DateTime(current_date.AddYears(-1).Year, current_date.AddMonths(-1).Month, 16).GetStartForSearch();
                    end_date = new DateTime(current_date.Year, current_date.Month, 15).GetEndForSearch();
                }
                else
                {
                    start_date = new DateTime(current_date.Year, current_date.AddMonths(-1).Month, 16).GetStartForSearch();
                    end_date = new DateTime(current_date.Year, current_date.Month, 15).GetEndForSearch();
                }
            }
            else if (current_date.Day >= 16)
            {
                if (current_date.Month == 12)
                {
                    start_date = new DateTime(current_date.Year, current_date.Month, 16).GetStartForSearch();
                    end_date = new DateTime(current_date.AddYears(1).Year, current_date.AddMonths(1).Month, 15).GetEndForSearch();
                }
                else
                {
                    start_date = new DateTime(current_date.Year, current_date.Month, 16).GetStartForSearch();
                    end_date = new DateTime(current_date.Year, current_date.AddMonths(1).Month, 15).GetEndForSearch();
                }
            }

            var result = Tuple.Create<DateTime, DateTime>(start_date, end_date);
            return result;

        }

        /// <summary>
        /// validate if date filed is within the cutoff period of start date
        /// </summary>
        public virtual ValidationResult GetCheckWithinCutOffStgy(DateTime cutoff_date, bool isELorSL = false)
        {
            Tuple<DateTime, DateTime> cutoff = this.GetCutOff(cutoff_date);
            var date_filed = DateTime.Now;
            var start_date = cutoff.Item1;
            var end_date = cutoff.Item2;

            if (isELorSL)
            {
                end_date = cutoff.Item2.AddDays(2);
            }

            if (date_filed < start_date || date_filed > end_date)
            {
                return new ValidationResult(ErsResources.GetMessage("WithinCutoff", ErsResources.GetFieldName("date_filed")));
            }
            return null;

        }
    }
}
