﻿using jp.co.ivp.ers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ers.jp.co.ivp.ers.request.strategy
{
    public class SetScheduleDefaultTimeStgy
    {
        public void GetDefaultTime(Dictionary<string,object> sched, string[] prevDate)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            for (var i = 0; i < prevDate.Count(); i++)
            {
                var num = i;
                num++;
                var day = "day" + num;
                var day_num = Convert.ToInt32(Convert.ToDateTime(prevDate[i]).DayOfWeek);
                if (day_num == 0 || day_num == 6)
                {
                    sched[day] = "";
                }
                else
                {
                    if (Convert.ToString (sched[day]) == "")
                    {
                        sched[day] = setup.def_time_start + " - " + setup.def_time_end;
                    }
                    else
                    {
                        sched[day] = sched[day];
                    }
                }
            }
        }
    }
}
