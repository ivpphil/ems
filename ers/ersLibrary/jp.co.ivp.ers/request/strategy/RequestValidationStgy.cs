﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.request;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ers.jp.co.ivp.ers.request.strategy
{
    public class RequestValidationStgy
    {
        ErsRequestRepository repository = ErsFactory.ersRequestFactory.GetErsRequestRepository();
        ErsRequestCriteria criteria = ErsFactory.ersRequestFactory.GetErsRequestCriteria();

        public virtual ValidationResult CheckTimeComparison(string s_time_start, string s_time_end)
        {
            DateTime start_t = Convert.ToDateTime(s_time_start);
            DateTime end_t = Convert.ToDateTime(s_time_end);

            if (start_t > end_t)
            {
                return new ValidationResult(ErsResources.GetMessage("TimeGreaterThanError", ErsResources.GetFieldName("time_start"), ErsResources.GetFieldName("time_end")));
            }
            return null;
        }

        public virtual ValidationResult CheckValidOvertime(DateTime? time_start, DateTime? time_end, string shift_end, string w_time_start, DateTime? date_start, string w_date_filed)
        {
            if (time_start != null && time_end != null)
            {
                if (time_start > time_end)
                {
                    return new ValidationResult(ErsResources.GetMessage("TimeGreaterThanError", ErsResources.GetFieldName("time_start"), ErsResources.GetFieldName("time_end")));
                }
                else if (time_start == time_end)
                {
                    return new ValidationResult(ErsResources.GetMessage("invalid_equal_params", ErsResources.GetFieldName("time_start"), ErsResources.GetFieldName("time_end")));
                }

                if (shift_end.HasValue())
                {
                    DateTime time_start_s = Convert.ToDateTime(time_start);
                    DateTime time_end_s = Convert.ToDateTime(shift_end);
                    if (time_start_s.TimeOfDay < time_end_s.TimeOfDay)
                    {
                        return new ValidationResult(ErsResources.GetMessage("ot_time_invalid"));
                    }
                }
            }

            if (date_start != null)
            {
                var date = Convert.ToDateTime(date_start);
                if (DateTime.Parse(date.ToShortDateString()) < DateTime.Parse(w_date_filed))
                {
                    return new ValidationResult(ErsResources.GetMessage("ot_date_invalid"));
                }
            }
            return null;
        }

        public virtual ValidationResult ParentalLeaveLimit(string EmpNo, double used_leave)
        {
            var cri = ErsFactory.ersRequestFactory.GetErsRequestCriteria();

            cri.emp_no = EmpNo;
            cri.leave_type = EnumLeaveType.ParentalLeave;
            cri.status = EnumStatusRequest.Approved;

            var result = repository.Find(cri);
            double limit_leave = 7;
            double accumulated_leave = 0;
            double count = 0;

            if (result.Count != 0)
            {
                foreach (var res in result)
                {
                    accumulated_leave = res.used_leave;
                    count += accumulated_leave;
                }
                var countLeave = count + used_leave;

                var UsedLeave = Convert.ToString(used_leave);
                var AccumulatedLeave = Convert.ToString(accumulated_leave);
                if (countLeave > 7)
                {
                    return new ValidationResult(ErsResources.GetMessage("parental_limit", new[] { UsedLeave, AccumulatedLeave }));
                }
            }
            else
            {
                var UsedLeave = Convert.ToString(used_leave);
                var LimitLeave = Convert.ToString(limit_leave);
                if (used_leave > 7)
                {
                    return new ValidationResult(ErsResources.GetMessage("parental_limit", new[] { UsedLeave, LimitLeave }));
                }
            }

            return null;
        }

        public virtual ValidationResult CheckRequestID(int? request_id, EnumRequestType? request_type)
        {
            var requestRecord = ErsFactory.ersRequestFactory.GetErsRequestWithid((int)request_id);
            if (requestRecord != null)
            {
                if (requestRecord.request_type != request_type)
                {
                    return new ValidationResult(ErsResources.GetMessage("request_is_invalid"));
                }
            }
            else
            {
                return new ValidationResult(ErsResources.GetMessage("NotExist", "Request ID"));
            }

            return null;
        }

        public virtual ValidationResult CheckRequestMCodeWithID(int? request_id)
        {
            string emp_no = ErsContext.sessionState.Get("mcode");
            var requestRecord = ErsFactory.ersRequestFactory.GetErsRequestWithid((int)request_id);
            if (requestRecord != null)
            {
                if (requestRecord.emp_no == emp_no)
                {
                    return new ValidationResult(ErsResources.GetMessage("own_request_approver"));
                }
            }
            return null;
        }

        public virtual ValidationResult CheckDuplicateRequest(DateTime? date_start, EnumRequestType? request_type)
        {
            criteria.emp_no = ErsContext.sessionState.Get("mcode");
            criteria.date_start = date_start;
            criteria.request_type = request_type;
            criteria.status_not_in = new[] { EnumStatusRequest.Declined, EnumStatusRequest.Cancelled };
            criteria.active = EnumActive.Active;

            var result = repository.GetRecordCount(criteria);

            if(result > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("reqExist"));
            }

            return null;
        }

        public virtual ValidationResult CheckBetweenDateLeaveRequest(DateTime? date_start, DateTime? date_end)
        {
            criteria.emp_no = ErsContext.sessionState.Get("mcode");
            criteria.request_type = EnumRequestType.Leave;
            criteria.status_not_in = new[] { EnumStatusRequest.Declined, EnumStatusRequest.Cancelled };
            criteria.active = EnumActive.Active;

            var result = repository.Find(criteria).Where(req => (date_start >= req.date_start && date_start <= req.date_end) || (date_end >= req.date_start && date_end <= req.date_end) ||
                                                 (req.date_start >= date_start && req.date_start <= date_end) || (req.date_end >= date_start && req.date_end <= date_end)).Count();

            if (result > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("reqExist"));
            }

            return null;
        }
    }
}