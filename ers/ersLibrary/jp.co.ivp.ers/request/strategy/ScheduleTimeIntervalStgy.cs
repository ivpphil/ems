﻿using System;
using System.Collections.Generic;

namespace ers.jp.co.ivp.ers.request.strategy
{
    public class ScheduleTimeIntervalStgy
    {
        public Dictionary<int, string> GetTimeInterval(TimeSpan time, int minInterval)
        {
            var timeIntervals = new Dictionary<int, string>();

            DateTime startDate = new DateTime(DateTime.MinValue.Ticks); // Date to be used to get shortTime format.
            for (int i = 0; i < 48; i++)
            {
                var j = 1 + i;
                int minutesToBeAdded = minInterval * i;      // Increasing minutes by 30 minutes interval
                TimeSpan timeToBeAdded = new TimeSpan(0, minutesToBeAdded, 0);
                TimeSpan t = time.Add(timeToBeAdded);
                DateTime result = startDate + t;
                timeIntervals.Add(j, result.ToShortTimeString());
            }

            return timeIntervals;
        }

        public string CheckStringTime(int time, Dictionary<int,string> timeList)
        {
            var stringTime = "";
            timeList.TryGetValue(time, out stringTime);            
            return stringTime;
        }

    }
}
