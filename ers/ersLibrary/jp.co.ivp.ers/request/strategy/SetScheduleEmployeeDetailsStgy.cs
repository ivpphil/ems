﻿using jp.co.ivp.ers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace ers.jp.co.ivp.ers.request.strategy
{
    public class SetScheduleEmployeeDetailsStgy
    {
        public Dictionary<string,object> GetEmployeeDetails(Dictionary<string, object> sched, string[] prevDate)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var imagesetup = ErsFactory.ersUtilityFactory.getSetup().image_directory;
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            var list = new List<Dictionary<string, object>>();

            if (sched["team"] != null)
            {
                sched.Add("w_team", ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Team, EnumCommonNameColumnName.namename, Convert.ToInt32(sched["team"])));
            }
         
            if (sched["job_title"] != null)
            {
                var job = ErsFactory.ersJobFactory.GetErsJobTitleWithID(Convert.ToInt32(sched["job_title"]));                
                sched.Add("w_job_title", job == null ? string.Empty : job.job_title);
            }
            if (sched["status"] != null)
            {
                sched.Add("w_status", ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Emp_Status, EnumCommonNameColumnName.namename, Convert.ToInt32(sched["status"])));
            }

            sched["w_gender"] = sched["gender"] != null ? ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Gender, EnumCommonNameColumnName.namename, Convert.ToInt32(sched["gender"])) : ErsResources.GetMessage("NotSet");
            sched["address"] = sched["address"] != null ? sched["address"] : ErsResources.GetMessage("NotSet");
            sched["contact_no"] = sched["contact_no"] != null ? sched["contact_no"] : ErsResources.GetMessage("NotSet");

            var bdayConvert = Convert.ToDateTime(sched["birthday"]).ToShortDateString();
            sched["birthday"] = sched["birthday"] != null ? bdayConvert == DateTime.MinValue.ToShortDateString() ? ErsResources.GetMessage("NotSet") : bdayConvert : ErsResources.GetMessage("NotSet");

            if (sched["image_file"] == null)
            {
                sched["image_file"] = setup.default_name_image;
            }
            else if (!File.Exists(imagesetup + sched["emp_no"].ToString() + "\\" + sched["image_file"].ToString() + ""))
            {
                sched["image_file"] = setup.default_name_image;
            }
            sched["fullname"] = textInfo.ToTitleCase(Convert.ToString(sched["fname"]) + " " + Convert.ToString(sched["lname"]));

            ErsFactory.ersRequestFactory.GetSetScheduleDefaultTimeStgy().GetDefaultTime(sched, prevDate);

            return sched;
        }

        public List<string> GetScheduleHeader(DateTime date, string[] prevDate)
        { 
            if (date == DateTime.MinValue)
            {
                date = DateTime.Now;
            }

            for (var i = 0; prevDate.Length > i; i++)
            {
                var newDate = date.AddDays(i).ToString("yyyy-MM-dd");
                prevDate[i] = newDate;
            }

            List<string> listOfdaysofWeek = new List<string>();
            for (int i = 0; i < 7; i++)
            {
                var schedDate = date.AddDays(i);
                var schedule = schedDate.DayOfWeek.ToString().Substring(0, 3) + " " + schedDate.Day.ToString();
                listOfdaysofWeek.Add(schedule);
            }

            return listOfdaysofWeek;
        }

        public string getSchedType(Dictionary<string, object> sched, EnumSchedType sched_type)
        {
            string w_leave_type = "";

            switch (sched_type)
            {
                case EnumSchedType.VacationLeave:
                    w_leave_type = "Vacation Leave";
                    break;

                case EnumSchedType.SickLeave:
                    w_leave_type = "Sick Leave";
                    break;

                case EnumSchedType.EmergencyLeave:
                    w_leave_type = "Emergency Leave";
                    break;

            }

            return w_leave_type;
        }
    }
}
