﻿using jp.co.ivp.ers.db;
using System;
using System.Text;

namespace jp.co.ivp.ers.request
{
    public class ErsScheduleCriteria : Criteria
    {

        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("schedule_t.id", value, Operation.EQUAL));
            }
        }

        public int? desknet_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("schedule_t.desknet_id", value, Operation.EQUAL));
            }
        }

        public virtual DateTime? date_start
        {
            set
            {
                Add(Criteria.GetCriterion("schedule_t.date_start", value, Operation.EQUAL));
            }
        }

        public virtual DateTime date_end
        {
            set
            {
                Add(Criteria.GetCriterion("schedule_t.date_end", value, Operation.EQUAL));
            }
        }

        public virtual EnumSchedType? sched_type
        {
            set
            {
                Add(Criteria.GetCriterion("schedule_t.sched_type", value, Operation.EQUAL));
            }
        }

        public void date_range(DateTime? start, DateTime end)
        {
            this.Add(Criteria.JoinWithAnd(
                    new[] {
                                Criteria.GetCriterion("schedule_t.date_start", start, Operation.GREATER_EQUAL),
                                 Criteria.GetCriterion("schedule_t.date_start", end, Operation.LESS_EQUAL)
                    }));
        }


        public void OrderByTeam(string emp_no)
        {
            var tableName = "emp";
            StringBuilder sql = new StringBuilder();

            sql.AppendFormat(" ORDER BY ( {0}.team_leader = {1} ) ASC, {0}.team_leader", tableName, emp_no);
            this.Add(Criteria.GetUniversalCriterion(sql.ToString()));

        }

        public virtual string fname
        {
            set
            {

                Add(Criteria.GetLikeClauseCriterion("schedule_t.fname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual string fname_like
        {
            set
            {

                this.Add(Criteria.GetUniversalCriterion("LOWER(schedule_t.fname) LIKE  LOWER('%" + value + "%')"));
            }
        }

        public virtual string lname
        {
            set
            {

                Add(Criteria.GetLikeClauseCriterion("schedule_t.lname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual string lname_like
        {
            set
            {

                this.Add(Criteria.GetUniversalCriterion("LOWER(schedule_t.lname) LIKE  LOWER('%" + value + "%')"));
            }
        }

        public void SetOrderByTimestart(OrderBy orderBy)
        {
            this.AddOrderBy("schedule_t.time_start", orderBy);
        }
               
        public virtual string sched_day1_like
        {
            set
            {

                Add(Criteria.GetLikeClauseCriterion("sched.day1", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual string sched_date_start
        {
            set
            {

                Add(Criteria.GetCriterion("sched.date_end", value, Operation.EQUAL));
            }
        }

        public virtual string sched_date_end
        {
            set
            {

                Add(Criteria.GetCriterion("sched.date_start", value, Operation.EQUAL));
            }
        }
    }
}
