﻿using jp.co.ivp.ers.mvc;
using System;

namespace jp.co.ivp.ers.request
{
    public class ErsScheduleBatchLog : ErsRepositoryEntity
    {
        public ErsScheduleBatchLog()
        {
        }

        public override int? id { get; set; }
        public string emp_no { get; set; }
        public DateTime? execution_date { get; set; }
        public DateTime? execution_time { get; set; }
        public string details { get; set; }
        public string error_details { get; set; }
        public string error_exception { get; set; }
        public DateTime? utime { get; set; }
        public EnumBatchStatus? status { get; set; }

    }
}
