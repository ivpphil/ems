﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;

namespace jp.co.ivp.ers.request
{
    public class ErsLeaveBalanceRepository : ErsRepository<ErsLeaveBalance>
    {
        public ErsLeaveBalanceRepository() : base("leave_balance_t")
        {

        }

        public ErsLeaveBalanceRepository(ErsDatabase objDB) : base("leave_balance_t", objDB)
        {

        }

        public override void Insert(ErsLeaveBalance obj, bool storeNewIdToObject = false)
        {
            obj.intime = DateTime.Now;
            base.Insert(obj, storeNewIdToObject);
        }
    }
}
