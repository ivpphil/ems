﻿using ers.jp.co.ivp.ers.request.strategy;
using jp.co.ivp.ers.request.specification;
using jp.co.ivp.ers.util;
using System;
using System.Collections.Generic;
using System.Linq;
using static jp.co.ivp.ers.db.Criteria;

namespace jp.co.ivp.ers.request
{
    public class ErsRequestFactory
    {
        public ErsRequest GetErsRequest()
        {
            return new ErsRequest();
        }

        public ErsRequestRepository GetErsRequestRepository()
        {
            return new ErsRequestRepository();
        }

        public ErsRequestCriteria GetErsRequestCriteria()
        {
            return new ErsRequestCriteria();
        }

        public RequestValidationStgy GetRequestValidationStgy()
        {
            return new RequestValidationStgy();
        }

        public virtual EnumRequestType? GetRequestTypeWithid(int request_id)
        {

            var repo = this.GetErsRequestRepository();
            var cri = this.GetErsRequestCriteria();

            cri.id = request_id;
            var request = repo.FindSingle(cri);

            if (request != null)
            {
                return (EnumRequestType)request.request_type;
            }
            return null;
        }

        public virtual ErsRequest GetErsRequestWithid(int request_id)
        {

            var repo = this.GetErsRequestRepository();
            var cri = this.GetErsRequestCriteria();

            cri.id = request_id;
            var request = repo.FindSingle(cri);

            if (request != null)
            {
                return request;
            }
            return null;
        }


        protected static ErsScheduleRepository _ErsScheduleRepository
        {
            get
            {
                return (ErsScheduleRepository)ErsCommonContext.GetPooledObject("_ErsScheduleRepository");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_ErsScheduleRepository", value);
            }
        }

        /// <summary>
        /// 会員クラスを取得する
        /// </summary>
        /// <returns>new instance of ErsSchedule</returns>
        public virtual ErsSchedule GetErsSchedule()
        {
            return new ErsSchedule();
        }

        /// <summary>
        /// 会員クラス用Repositoryを取得する
        /// </summary>
        /// <returns>instance of ErsScheduleRepository</returns>
        public virtual ErsScheduleRepository GetErsScheduleRepository()
        {
            if (_ErsScheduleRepository == null)
                _ErsScheduleRepository = new ErsScheduleRepository();
            return _ErsScheduleRepository;
        }

        /// <summary>
        /// ErsMemberRespository用Criteria
        /// </summary>
        /// <returns>new instance of ErsScheduleCriteria</returns>
        public virtual ErsScheduleCriteria GetErsScheduleCriteria()
        {
            return new ErsScheduleCriteria();
        }


        /// <summary>
        /// 入力値をもとに、会員クラスを取得する。
        /// </summary>
        /// <param name="id"></param>
        /// <returns>instance of ErsSchedule</returns>
        public virtual ErsSchedule getErsScheduleWithParameter(Dictionary<string, object> parameters)
        {
            var schedule = ErsFactory.ersRequestFactory.GetErsSchedule();
            schedule.OverwriteWithParameter(parameters);
            return schedule;
        }

        /// <summary>
        /// 会員コードをもとに、会員クラスを取得する。
        /// </summary>
        /// <param name="mcode"></param>
        /// <returns>instance of ErsMember</returns>
        public virtual ErsSchedule getErsScheduleWithID(int id)
        {
            var repository = GetErsScheduleRepository();

            var criteria = this.GetErsScheduleCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);
            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        /// <summary>
        /// 会員コードをもとに、会員クラスを取得する。
        /// </summary>
        /// <param name="mcode"></param>
        /// <returns>instance of ErsMember</returns>
        public virtual ErsSchedule getErsScheduleWithDesknetIDAndDate(int id, DateTime sched_date)
        {
            var repository = GetErsScheduleRepository();

            var criteria = this.GetErsScheduleCriteria();
            criteria.desknet_id = id;
            criteria.date_start = sched_date;
            criteria.SetOrderByTimestart(OrderBy.ORDER_BY_ASC);

            var result = repository.Find(criteria);

            return result.FirstOrDefault();
        }

        /// <summary>
        /// 会員情報の一覧を取得する。
        /// </summary>
        /// <returns>new instance of MemberSearchSpecification</returns>
        public virtual EmployeeScheduleSpecification GetEmployeeScheduleSpecification()
        {
            return new EmployeeScheduleSpecification();
        }

        public virtual EmployeeWeeklyScheduleSpecification GetEmployeeWeeklyScheduleSpecification()
        {
            return new EmployeeWeeklyScheduleSpecification();
        }

        protected static ErsScheduleBatchLogRepository _ErsScheduleBatchLogRepository
        {
            get
            {
                return (ErsScheduleBatchLogRepository)ErsCommonContext.GetPooledObject("_ErsScheduleBatchLogRepository");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_ErsScheduleBatchLogRepository", value);
            }
        }

        /// <summary>
        /// 会員クラスを取得する
        /// </summary>
        /// <returns>new instance of ErsScheduleBatchLog</returns>
        public virtual ErsScheduleBatchLog GetErsScheduleBatchLog()
        {
            return new ErsScheduleBatchLog();
        }

        /// <summary>
        /// 会員クラス用Repositoryを取得する
        /// </summary>
        /// <returns>instance of ErsScheduleBatchLogRepository</returns>
        public virtual ErsScheduleBatchLogRepository GetErsScheduleBatchLogRepository()
        {
            if (_ErsScheduleBatchLogRepository == null)
                _ErsScheduleBatchLogRepository = new ErsScheduleBatchLogRepository();
            return _ErsScheduleBatchLogRepository;
        }

        /// <summary>
        /// ErsScheduleBatchLogRepository用Criteria
        /// </summary>
        /// <returns>new instance of ErsScheduleBatchLogCriteria</returns>
        public virtual ErsScheduleBatchLogCriteria GetErsScheduleBatchLogCriteria()
        {
            return new ErsScheduleBatchLogCriteria();
        }

        /// <summary>
        /// 入力値をもとに、会員クラスを取得する。
        /// </summary>
        /// <param name="id"></param>
        /// <returns>instance of ErsScheduleBatchLog</returns>
        public virtual ErsScheduleBatchLog getErsScheduleBatchLogWithParameter(Dictionary<string, object> parameters)
        {
            var sched_batch_log = ErsFactory.ersRequestFactory.GetErsScheduleBatchLog();
            sched_batch_log.OverwriteWithParameter(parameters);
            return sched_batch_log;
        }

        /// <summary>
        /// 会員コードをもとに、会員クラスを取得する。
        /// </summary>
        /// <param name="id"></param>
        /// <returns>instance of ErsScheduleBatchLog</returns>
        public virtual ErsScheduleBatchLog getErsScheduleBatchLogWithID(int id)
        {
            var repository = GetErsScheduleBatchLogRepository();

            var criteria = this.GetErsScheduleBatchLogCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);
            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        /// <summary>
        /// 会員コードをもとに、会員クラスを取得する。
        /// </summary>
        /// <param name="id"></param>
        /// <returns>instance of ErsScheduleBatchLog</returns>
        public virtual ErsScheduleBatchLog getErsScheduleBatchLogWithDate(DateTime date)
        {
            var repository = GetErsScheduleBatchLogRepository();
            var criteria = this.GetErsScheduleBatchLogCriteria();
            criteria.execution_date = date;

            var list = repository.Find(criteria);
            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        public virtual ErsScheduleBatchLog getErsScheduleBatchLogWithDateIsNotSuccess(DateTime date)
        {
            var repository = GetErsScheduleBatchLogRepository();
            var criteria = this.GetErsScheduleBatchLogCriteria();
            criteria.execution_date = date;
            criteria.not_status = EnumBatchStatus.Success;

            var list = repository.Find(criteria);
            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        /// <summary>
        /// 会員情報の一覧を取得する。
        /// </summary>
        /// <returns>new instance of MemberSearchSpecification</returns>
        public virtual ScheduleBatchLogSpecification GetErsScheduleBatchLogSpecification()
        {
            return new ScheduleBatchLogSpecification();
        }

        /// <summary>
        /// sets time interval in request module
        /// </summary>                      
        public virtual ScheduleTimeIntervalStgy GetScheduleTimeIntervalStgy()
        {
            return new ScheduleTimeIntervalStgy();
        }

        /// <summary>
        /// gets default time employee schedule in schedule_t
        /// </summary>       
        public virtual SetScheduleDefaultTimeStgy GetSetScheduleDefaultTimeStgy()
        {
            return new SetScheduleDefaultTimeStgy();
        }

        /// <summary>
        /// gets employee details in schedule
        /// </summary>       
        public virtual SetScheduleEmployeeDetailsStgy GetSetScheduleEmployeeDetailsStgy()
        {
            return new SetScheduleEmployeeDetailsStgy();
        }

        public ErsApprover GetErsApprover()
        {
            return new ErsApprover();
        }

        public ErsApproverRepository GetErsApproverRepository()
        {
            return new ErsApproverRepository();
        }

        public ErsApproverCriteria GetErsApproverCriteria()
        {
            return new ErsApproverCriteria();
        }

        public IList<ErsApprover> GetErsApproverListWithRequestID(int? request_id)
        {
            var repo = this.GetErsApproverRepository();
            var criteria = this.GetErsApproverCriteria();

            criteria.request_id = request_id;

            var result = repo.Find(criteria);

            if (result.Count > 0)
            {
                return result;
            }

            return null;
        }

        public ErsLeaveBalance GetErsLeaveBalance()
        {
            return new ErsLeaveBalance();
        }

        public ErsLeaveBalanceRepository GetErsLeaveBalanceRepository()
        {
            return new ErsLeaveBalanceRepository();
        }

        public ErsLeaveBalanceCriteria GetErsLeaveBalanceCriteria()
        {
            return new ErsLeaveBalanceCriteria();
        }

        public ErsLeaveBalance GetErsLeaveBalanceWithEmpNo(string emp_no)
        {
            var repo = this.GetErsLeaveBalanceRepository();
            var criteria = this.GetErsLeaveBalanceCriteria();

            criteria.emp_no = emp_no;
            criteria.AddOrderBy("intime", OrderBy.ORDER_BY_DESC);
            criteria.LIMIT = 1;

            var result = repo.FindSingle(criteria);

            return result;
        }

        /// <summary>
        /// get the cutoff period for the current date
        /// </summary>       
        public virtual CutOffStgy GetCutOffStgy()
        {
            return new CutOffStgy();
        }
    }

}
