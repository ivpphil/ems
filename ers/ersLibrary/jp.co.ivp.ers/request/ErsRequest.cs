﻿using jp.co.ivp.ers.mvc;
using System;

namespace jp.co.ivp.ers.request
{
    public class ErsRequest : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public virtual string emp_no { get; set; }

        public virtual EnumRequestType? request_type { get; set; }

        public virtual DateTime? date_filed { get; set; }

        public virtual EnumStatusRequest? status { get; set; }

        public virtual string reason { get; set; }

        public virtual DateTime? date_start { get; set; }

        public virtual DateTime? date_end { get; set; }

        public virtual DateTime time_start { get; set; }

        public virtual DateTime time_end { get; set; }

        public virtual EnumActive? active { get; set; }

        public virtual DateTime? utime { get; set; }

        public virtual EnumLeaveType? leave_type { get; set; }

        public virtual string cancel_reason { get; set; }

        public virtual double used_leave { get; set; }

        public virtual int? correction_id { get; set; }

        public virtual EnumReceivedRequestFlg received_flg { get; set; }

        public virtual string file_name { get; set; }

    }
}
