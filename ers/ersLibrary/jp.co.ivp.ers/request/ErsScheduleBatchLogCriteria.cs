﻿using jp.co.ivp.ers.db;
using System;

namespace jp.co.ivp.ers.request
{
    public class ErsScheduleBatchLogCriteria : Criteria
    {
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("sched_batch_log_t.id", value, Operation.EQUAL));
            }
        }

        public string emp_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("sched_batch_log_t.emp_no", value, Operation.EQUAL));
            }
        }

        public virtual DateTime execution_date
        {
            set
            {
                Add(Criteria.GetCriterion("sched_batch_log_t.execution_date", value, Operation.EQUAL));
            }
        }

        public virtual DateTime execution_time
        {
            set
            {
                Add(Criteria.GetCriterion("sched_batch_log_t.execution_time", value, Operation.EQUAL));
            }
        }


        public string details
        {
            set
            {
                this.Add(Criteria.GetCriterion("sched_batch_log_t.details", value, Operation.EQUAL));
            }
        }


        public virtual EnumBatchStatus? status
        {
            set
            {
                Add(Criteria.GetCriterion("sched_batch_log_t.status", value, Operation.EQUAL));
            }
        }

        public virtual EnumBatchStatus? not_status
        {
            set
            {
                Add(Criteria.GetCriterion("sched_batch_log_t.status", value, Operation.NOT_EQUAL));
            }
        }

        public void time_range(DateTime? start, DateTime end)
        {
            this.Add(Criteria.JoinWithAnd(
                    new[] {
                                Criteria.GetCriterion("sched_batch_log_t.execution_date", start, Operation.GREATER_EQUAL),
                                 Criteria.GetCriterion("sched_batch_log_t.execution_date", end, Operation.LESS_EQUAL)
                    }));
        }


    }
}
