﻿using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;

namespace jp.co.ivp.ers.request
{
    public class ErsRequestCriteria : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.id", value, Operation.EQUAL));
            }
        }

        public string emp_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.emp_no", value, Operation.EQUAL));
            }
        }

        public EnumRequestType? request_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.request_type", value, Operation.EQUAL));
            }
        }

        public virtual IEnumerable<int?> request_type_in
        {
            set
            {
                Add(Criteria.GetInClauseCriterion("request_t.request_type", value));
            }
        }

        public DateTime? date_filed
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.date_filed", value, Operation.EQUAL));
            }
        }

        public EnumStatusRequest? status
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.status", value, Operation.EQUAL));
            }
        }

        public string reason
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.reason", value, Operation.EQUAL));
            }
        }

        public EnumReceivedRequestFlg? received_flg
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.received_flg", value, Operation.EQUAL));
            }
        }

        public DateTime? date_start
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.date_start", value, Operation.EQUAL));
            }
        }

        public DateTime? date_start_less
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.date_start", value, Operation.LESS_THAN));
            }
        }

        public DateTime? date_end
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.date_end", value, Operation.EQUAL));
            }
        }

        public DateTime time_start
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.time_start", value, Operation.EQUAL));
            }
        }

        public DateTime time_end
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.time_end", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.active", value, Operation.EQUAL));
            }
        }

        public DateTime? utime
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.utime", value, Operation.EQUAL));
            }
        }

        public EnumLeaveType? leave_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.leave_type", value, Operation.EQUAL));
            }
        }

        public string cancel_reason
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.cancel_reason", value, Operation.EQUAL));
            }
        }

        public double used_leave
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.used_leave", value, Operation.EQUAL));
            }
        }
        
        public EnumStatusRequest? status_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("request_t.status", value, Operation.NOT_EQUAL));
            }
        }

        public void getTeamList(string team_leader)
        {
            this.Add(Criteria.GetUniversalCriterion(string.Format(@"emp_no IN (SELECT emp_no FROM  employee_t WHERE team_leader = '{0}' OR emp_no = '{0}')", team_leader)));
        }

        public void getMonitoredRequest(DateTime start_date, DateTime end_date)
        {
            var current_date = DateTime.Now;
            
            this.Add(Criteria.GetUniversalCriterion(
                string.Format(@"CASE WHEN request_type = {2} THEN date_start BETWEEN '{0}' AND '{1}'
                WHEN request_type != {2} THEN received_flg = {3} END"
            , start_date, end_date, (int?)EnumRequestType.Leave, (int?)EnumReceivedRequestFlg.Pending)));
        }

        public void getHROwnRequest()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var current_date = DateTime.Now;

            this.Add(Criteria.GetUniversalCriterion(
                string.Format(@"CASE WHEN request_type = {1}
                    THEN 
                        (CASE WHEN leave_type != {7}
                        THEN 
                        date_start >= '{0}' 
                        WHEN leave_type = {7}
                        THEN
                        date_start <= '{0}' AND
                        '{5}' <= date_filed AND date_filed <= '{6}' 
                        END) 
                    WHEN request_type != {1} 
                        THEN (CASE WHEN emp_no = '{2}'
                            THEN received_flg = {3}
                            ELSE received_flg = {4}
                            END)
                        AND
                        '{5}' <= date_filed AND date_filed <= '{6}' 
                    END"
            , current_date, (int?)EnumRequestType.Leave, setup.hr_emp_no, (int?)EnumReceivedRequestFlg.Received,
            (int?)EnumReceivedRequestFlg.Pending, current_date.AddDays(-5), current_date.AddDays(5), (int?)EnumLeaveType.SickLeave )));
        }

        public void SetOrderByDateFiled(OrderBy orderBy)
        {
            this.AddOrderBy("request_t.date_filed", orderBy);
        }

        public virtual EnumStatusRequest[] status_not_in
        {
            set
            {
                this.Add(Criteria.GetNotInClauseCriterion("request_t.status", value));
            }
        }
    }
}
