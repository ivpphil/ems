﻿using jp.co.ivp.ers.mvc;
using System;

namespace jp.co.ivp.ers.request
{
    public class ErsSchedule : ErsRepositoryEntity
    {
        public ErsSchedule()
        {
        }

        public override int? id { get; set; }
        public string emp_no { get; set; }
        public int desknet_id { get; set; }
        public DateTime? date_start { get; set; }
        public DateTime? date_end { get; set; }
        public DateTime? time_start { get; set; }
        public DateTime? time_end { get; set; }
        public EnumSchedType? sched_type { get; set; }
        public string sched_details { get; set; }
        public string sched_location { get; set; }
        public string sched_location_details { get; set; }
        public string details { get; set; }

    }
}
