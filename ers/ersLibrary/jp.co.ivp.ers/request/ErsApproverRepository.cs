﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;

namespace jp.co.ivp.ers.request
{
    public class ErsApproverRepository : ErsRepository<ErsApprover>
    {
        public ErsApproverRepository() : base("approver_t")
        {

        }

        public ErsApproverRepository(ErsDatabase objDB) : base("approver_t", objDB)
        {

        }

        public override void Insert(ErsApprover obj, bool storeNewIdToObject = false)
        {
            var repo = ErsFactory.ersRequestFactory.GetErsApproverRepository();
            var cri = ErsFactory.ersRequestFactory.GetErsApproverCriteria();
            cri.AddOrderBy("intime", Criteria.OrderBy.ORDER_BY_DESC);
            cri.LIMIT = 1;
            var last = repo.FindSingle(cri);

            obj.intime = DateTime.Now;
            base.Insert(obj, storeNewIdToObject);
        }
    }
}
