﻿using jp.co.ivp.ers.db;
using System;

namespace jp.co.ivp.ers.request
{
    public class ErsApproverCriteria : Criteria
    {
        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("approver_t.id", value, Operation.EQUAL));
            }
        }

        public int? request_id
        {
            set
            {
                Add(Criteria.GetCriterion("approver_t.request_id", value, Operation.EQUAL));
            }
        }

        public string emp_no
        {
            set
            {
                Add(Criteria.GetCriterion("approver_t.emp_no", value, Operation.EQUAL));
            }
        }

        public EnumApprovalStatus? approval_status
        {
            set
            {
                Add(Criteria.GetCriterion("approver_t.approval_status", value, Operation.EQUAL));
            }
        }

        public string reason
        {
            set
            {
                Add(Criteria.GetCriterion("approver_t.reason", value, Operation.EQUAL));
            }
        }

        public DateTime intime
        {
            set
            {
                Add(Criteria.GetCriterion("approver_t.intime", value, Operation.EQUAL));
            }
        }
    }
}
