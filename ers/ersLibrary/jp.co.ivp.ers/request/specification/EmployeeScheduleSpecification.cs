﻿using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.request.specification
{
    public class EmployeeScheduleSpecification : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return @"SELECT   *
                    FROM schedule_t LEFT JOIN employee_t ON schedule_t.desknet_id = employee_t.desknet_id
                    WHERE employee_t.status = 1 ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return @"SELECT COUNT(schedule_t.emp_no) AS " + countColumnAlias + @"
                    FROM schedule_t LEFT JOIN employee_t ON schedule_t.desknet_id = employee_t.desknet_id
                    WHERE employee_t.status = 1 ";
        }
    }
}
