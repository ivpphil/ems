﻿using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.request.specification
{
    public class ScheduleBatchLogSpecification : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return @"SELECT   *
                    FROM sched_batch_log_t LEFT JOIN employee_t ON sched_batch_log_t.emp_no = employee_t.emp_no";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return @"SELECT COUNT(sched_batch_log_t.emp_no) AS " + countColumnAlias + @"
                     FROM sched_batch_log_t LEFT JOIN employee_t ON sched_batch_log_t.emp_no = employee_t.emp_no";
        }
    }
}
