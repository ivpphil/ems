﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;

namespace jp.co.ivp.ers.request.specification
{
    public class EmployeeWeeklyScheduleSpecification : ISpecificationForSQL
    {

        public string time_end { get; set; }
        public string time_start { get; set; }
        public int? id;

        public List<Dictionary<string, object>> ScheduleSearch(Criteria criteria, string[] week, int? id)
        {
            this.week = week;
            this.id = id;

            return this.GetSearchData(criteria, week, id);
        }

        string[] week;

        public List<Dictionary<string, object>> GetSearchData(Criteria criteria, string[] week, int? id)
        {
            return ErsRepository.SelectSatisfying(this, criteria);
        }

        public virtual string asSQL()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            string timeStart = "";
            string timeEnd = "";
            string day1_like = "";
            string withDesknetId = "";

            if (!String.IsNullOrEmpty(time_start))
            {
                timeStart = "AND time_start = '" + time_start + "'";
                day1_like = " WHERE sched.day1 LIKE '%" + time_start + "%' ";
            }
            if (!String.IsNullOrEmpty(time_end))
            {
                timeEnd = "AND time_end = '" + time_end + "'";
                day1_like = " WHERE sched.day1 LIKE '%" + time_end + "%' ";
            }

            if (id != null)
            {
                withDesknetId = " AND desknet_id = '" + id + "'";
            }

            return string.Format(@"SELECT emp.emp_no, emp.lname, emp.fname, emp.birthday,emp.address, emp.contact_no, emp.position, emp.team, emp.status, emp.password,emp.email,emp.mcode,emp.gender,
            emp.image_file, emp.team_leader, emp.m_flg,emp.desknet_id, emp.job_title, 
            sched.*

            FROM
            (SELECT desknet_id,
            MAX(CASE WHEN (date_start='#{0}#' AND sched_type = {7} AND  sched_details IS NULL)  
                THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) 
                WHEN NOT EXISTS(select * from schedule_t where date_start='#{0}#')
                THEN '{8} - {9}' 
                END ) as day1,
            MAX(CASE WHEN (date_start='#{0}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN  sched_details ELSE '' END ) as day1_leave,
            MAX(CASE WHEN (date_start='#{0}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN  sched_type ELSE null END ) as day1_leave_type,
            MAX(CASE WHEN (date_start='#{0}#' AND sched_type = 0 AND (sched_details not like '%ETA%' AND sched_details not like '%MTG%')) THEN details ELSE '' END ) as day1_holiday,
            MAX(CASE WHEN (date_start='#{0}#' AND (sched_type = 0 OR sched_type = 1) AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN sched_details ELSE '' END ) as day1_meeting, 
            MAX(CASE WHEN (date_start='#{0}#' AND sched_details like '%ETA%') THEN sched_details ELSE '' END ) as day1_eta,
            MAX(CASE WHEN (date_start='#{0}#' AND sched_details like '%ETA%') THEN to_char(time_start,'HH12:MI PM') END ) as day1_eta_time,
            MAX(CASE WHEN (date_start='#{0}#' AND sched_details like '%HALF DAY%') THEN sched_details ELSE '' END ) as day1_half_day,
            MAX(CASE WHEN (date_start='#{0}#' AND sched_details like '%HALF DAY%')  THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day1_half_day_time,
            MAX(CASE WHEN (date_start='#{0}#' AND (sched_type = 0 OR sched_type = 1)  AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day1_meeting_time,
           
            MAX(CASE WHEN (date_start='#{1}#' AND sched_type = {7} AND  sched_details IS NULL)  
                THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) 
                WHEN NOT EXISTS(select * from schedule_t where date_start='#{1}#')
                THEN '{8} - {9}'  
                END ) as day2,
            MAX(CASE WHEN (date_start='#{1}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN sched_details ELSE '' END ) as day2_leave,
            MAX(CASE WHEN (date_start='#{1}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN  sched_type ELSE null END ) as day2_leave_type,
            MAX(CASE WHEN (date_start='#{1}#' AND sched_type = 0 AND (sched_details not like '%ETA%' AND sched_details not like '%MTG%')) THEN details ELSE '' END ) as day2_holiday,
            MAX(CASE WHEN (date_start='#{1}#' AND (sched_type = 0 OR sched_type = 1) AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN sched_details ELSE '' END ) as day2_meeting, 
            MAX(CASE WHEN (date_start='#{1}#' AND sched_details like '%ETA%') THEN sched_details ELSE '' END ) as day2_eta,
            MAX(CASE WHEN (date_start='#{1}#' AND sched_details like '%ETA%') THEN to_char(time_start,'HH12:MI PM') END ) as day2_eta_time,
            MAX(CASE WHEN (date_start='#{1}#' AND sched_details like '%HALF DAY%') THEN sched_details ELSE '' END ) as day2_half_day,
            MAX(CASE WHEN (date_start='#{1}#' AND sched_details like '%HALF DAY%')  THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day2_half_day_time,
            MAX(CASE WHEN (date_start='#{1}#' AND (sched_type = 0 OR sched_type = 1)  AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day2_meeting_time,

            MAX(CASE WHEN (date_start='#{2}#' AND sched_type = {7} AND  sched_details IS NULL)  
                THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) 
                WHEN NOT EXISTS(select * from schedule_t where date_start='#{2}#')
                THEN '{8} - {9}'  
                END ) as day3,
            MAX(CASE WHEN (date_start='#{2}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN sched_details ELSE '' END ) as day3_leave,
            MAX(CASE WHEN (date_start='#{2}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN  sched_type ELSE null END ) as day3_leave_type,
            MAX(CASE WHEN (date_start='#{2}#' AND sched_type = 0 AND (sched_details not like '%ETA%' AND sched_details not like '%MTG%')) THEN details ELSE '' END ) as day3_holiday,
            MAX(CASE WHEN (date_start='#{2}#' AND (sched_type = 0 OR sched_type = 1) AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN sched_details ELSE '' END ) as day3_meeting, 
            MAX(CASE WHEN (date_start='#{2}#' AND sched_details like '%ETA%') THEN sched_details ELSE '' END ) as day3_eta,
            MAX(CASE WHEN (date_start='#{2}#' AND sched_details like '%ETA%') THEN to_char(time_start,'HH12:MI PM') END ) as day3_eta_time,
            MAX(CASE WHEN (date_start='#{2}#' AND sched_details like '%HALF DAY%') THEN sched_details ELSE '' END ) as day3_half_day,
            MAX(CASE WHEN (date_start='#{2}#' AND sched_details like '%HALF DAY%')  THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day3_half_day_time,
            MAX(CASE WHEN (date_start='#{2}#' AND (sched_type = 0 OR sched_type = 1)  AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day3_meeting_time,

            MAX(CASE WHEN (date_start='#{3}#' AND sched_type = {7} AND  sched_details IS NULL)  
                THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) 
                WHEN NOT EXISTS(select * from schedule_t where date_start='#{3}#')
                THEN '{8} - {9}'  
                END ) as day4,
            MAX(CASE WHEN (date_start='#{3}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN sched_details ELSE '' END ) as day4_leave,
            MAX(CASE WHEN (date_start='#{3}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN  sched_type ELSE null END ) as day4_leave_type,
            MAX(CASE WHEN (date_start='#{3}#' AND sched_type = 0 AND (sched_details not like '%ETA%' AND sched_details not like '%MTG%')) THEN details ELSE '' END ) as day4_holiday,
            MAX(CASE WHEN (date_start='#{3}#' AND (sched_type = 0 OR sched_type = 1) AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN sched_details ELSE '' END ) as day4_meeting, 
            MAX(CASE WHEN (date_start='#{3}#' AND sched_details like '%ETA%') THEN sched_details ELSE '' END ) as day4_eta,
            MAX(CASE WHEN (date_start='#{3}#' AND sched_details like '%ETA%') THEN to_char(time_start,'HH12:MI PM') END ) as day4_eta_time,
            MAX(CASE WHEN (date_start='#{3}#' AND sched_details like '%HALF DAY%') THEN sched_details ELSE '' END ) as day4_half_day,
            MAX(CASE WHEN (date_start='#{3}#' AND sched_details like '%HALF DAY%')  THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day4_half_day_time,
            MAX(CASE WHEN (date_start='#{3}#' AND (sched_type = 0 OR sched_type = 1)  AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day4_meeting_time,

            MAX(CASE WHEN (date_start='#{4}#' AND sched_type = {7} AND  sched_details IS NULL)  
                THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) 
                WHEN NOT EXISTS(select * from schedule_t where date_start='#{4}#')
                THEN '{8} - {9}'  
                END ) as day5,
            MAX(CASE WHEN (date_start='#{4}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN sched_details ELSE '' END ) as day5_leave,
            MAX(CASE WHEN (date_start='#{4}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN  sched_type ELSE null END ) as day5_leave_type,
            MAX(CASE WHEN (date_start='#{4}#' AND sched_type = 0 AND (sched_details not like '%ETA%' AND sched_details not like '%MTG%')) THEN details ELSE '' END ) as day5_holiday,
            MAX(CASE WHEN (date_start='#{4}#' AND (sched_type = 0 OR sched_type = 1) AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN sched_details ELSE '' END ) as day5_meeting, 
            MAX(CASE WHEN (date_start='#{4}#' AND sched_details like '%ETA%') THEN sched_details ELSE '' END ) as day5_eta,
            MAX(CASE WHEN (date_start='#{4}#' AND sched_details like '%ETA%') THEN to_char(time_start,'HH12:MI PM') END ) as day5_eta_time,
            MAX(CASE WHEN (date_start='#{4}#' AND sched_details like '%HALF DAY%') THEN sched_details ELSE '' END ) as day5_half_day,
            MAX(CASE WHEN (date_start='#{4}#' AND sched_details like '%HALF DAY%')  THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day5_half_day_time,
            MAX(CASE WHEN (date_start='#{4}#' AND (sched_type = 0 OR sched_type = 1)  AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day5_meeting_time,

            MAX(CASE WHEN (date_start='#{5}#' AND sched_type = {7} AND  sched_details IS NULL)  
                THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) 
                WHEN NOT EXISTS(select * from schedule_t where date_start='#{5}#')
                THEN '{8} - {9}'  
                END ) as day6,
            MAX(CASE WHEN (date_start='#{5}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN sched_details ELSE '' END ) as day6_leave,
            MAX(CASE WHEN (date_start='#{5}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN  sched_type ELSE null END ) as day6_leave_type,
            MAX(CASE WHEN (date_start='#{5}#' AND sched_type = 0 AND (sched_details not like '%ETA%' AND sched_details not like '%MTG%')) THEN details ELSE '' END ) as day6_holiday,
            MAX(CASE WHEN (date_start='#{5}#' AND (sched_type = 0 OR sched_type = 1) AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN sched_details ELSE '' END ) as day6_meeting, 
            MAX(CASE WHEN (date_start='#{5}#' AND sched_details like '%ETA%') THEN sched_details ELSE '' END ) as day6_eta,
            MAX(CASE WHEN (date_start='#{5}#' AND sched_details like '%ETA%') THEN to_char(time_start,'HH12:MI PM') END ) as day6_eta_time,
            MAX(CASE WHEN (date_start='#{5}#' AND sched_details like '%HALF DAY%') THEN sched_details ELSE '' END ) as day6_half_day,
            MAX(CASE WHEN (date_start='#{5}#' AND sched_details like '%HALF DAY%')  THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day6_half_day_time,
            MAX(CASE WHEN (date_start='#{5}#' AND (sched_type = 0 OR sched_type = 1)  AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day6_meeting_time,

            MAX(CASE WHEN (date_start='#{6}#' AND sched_type = {7} AND  sched_details IS NULL)  
                THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) 
                WHEN NOT EXISTS(select * from schedule_t where date_start='#{6}#')
                THEN '{8} - {9}'  
                END ) as day7,          
            MAX(CASE WHEN (date_start='#{6}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN sched_details ELSE '' END ) as day7_leave,
            MAX(CASE WHEN (date_start='#{6}#' AND (sched_type = 9 OR sched_type = 10 OR sched_type = 11)) THEN  sched_type ELSE null END ) as day7_leave_type,
            MAX(CASE WHEN (date_start='#{6}#' AND sched_type = 0 AND (sched_details not like '%ETA%' AND sched_details not like '%MTG%')) THEN details ELSE '' END ) as day7_holiday,
            MAX(CASE WHEN (date_start='#{6}#' AND (sched_type = 0 OR sched_type = 1) AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN sched_details ELSE '' END ) as day7_meeting, 
            MAX(CASE WHEN (date_start='#{6}#' AND sched_details like '%ETA%') THEN sched_details ELSE '' END ) as day7_eta,
            MAX(CASE WHEN (date_start='#{6}#' AND sched_details like '%ETA%') THEN to_char(time_start,'HH12:MI PM') END ) as day7_eta_time,
            MAX(CASE WHEN (date_start='#{6}#' AND sched_details like '%HALF DAY%') THEN sched_details ELSE '' END ) as day7_half_day,
            MAX(CASE WHEN (date_start='#{6}#' AND sched_details like '%HALF DAY%')  THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day7_half_day_time,
            MAX(CASE WHEN (date_start='#{6}#' AND (sched_type = 0 OR sched_type = 1)  AND (sched_details like '%MTG%' OR sched_details like '%Meeting%')) THEN CONCAT(to_char(time_start,'HH12:MI PM'),' - ',to_char(time_end,'HH12:MI PM')) ELSE '' END ) as day7_meeting_time
      
            FROM schedule_t 
            WHERE desknet_id is not null " + withDesknetId +
            @"GROUP BY desknet_id) sched 

            INNER JOIN
            (SELECT * FROM employee_t) emp  
             ON  
            sched.desknet_id = emp.desknet_id " + day1_like + " ", week[0], week[1], week[2], week[3], week[4], week[5], week[6], Convert.ToInt32(EnumSchedType.WorkSchedule),setup.def_time_start, setup.def_time_end);
        }
    }
}
