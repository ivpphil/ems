﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merge
{
    public class ErsCtsMergeHistoryRepository : ErsRepository<ErsCtsMergeHistory>
    {
        public ErsCtsMergeHistoryRepository()
            : base("cts_merge_history_t")
        {
        }

        public ErsCtsMergeHistoryRepository(ErsDatabase objDB)
            : base("cts_merge_history_t", objDB)
        {
        }

        public override IList<ErsCtsMergeHistory> Find(db.Criteria criteria)
        {
            var retList = new List<ErsCtsMergeHistory>();

            var list = this.ersDB_table.gSelect(criteria);

            foreach (var dr in list)
            {
                var merge = ErsFactory.ersCtsMergeFactory.GetErsCtsMergeHistoryWithParameters(dr);
                retList.Add(merge);
            }
            return retList;
        }
    }
}
