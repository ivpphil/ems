﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.merge
{
    public class ErsCtsMergeProcessCriteria : Criteria
	{
        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("id", value, Operation.EQUAL));
            }
        }

        public string mcode
        {
            set
            {
                Add(Criteria.GetCriterion("mcode", value, Operation.EQUAL));
            }
        }

        public EnumPaymentType? pay
        {
            set
            {
                Add(Criteria.GetCriterion("pay", (int?)value, Operation.EQUAL));
            }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("id", orderBy);
        }

	}
}
