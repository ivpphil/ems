﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.ctsorder;
using jp.co.ivp.ers.member;
using System.Data.Common;

namespace jp.co.ivp.ers.merge
{
    public class ErsCtsMergeProcessRepository
        : ErsRepository<ErsCtsMergeProcess>
    {
        public ErsCtsMergeProcessRepository()
            : base("cts_merge_process_t")
        {
        }

        public ErsCtsMergeProcessRepository(ErsDatabase objDB)
            : base("cts_merge_process_t", objDB)
        {
        }
    }
}
