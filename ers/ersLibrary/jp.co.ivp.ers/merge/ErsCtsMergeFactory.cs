﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.information.specification;
using jp.co.ivp.ers.merge.strategy;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.ctsorder;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.merge
{
    public class ErsCtsMergeFactory
    {
        public ErsCtsMergeProcessCriteria GetErsCtsMergeProcessCriteria()
        {
            return new ErsCtsMergeProcessCriteria();
        }

        public ErsCtsMergeProcessRepository GetErsCtsMergeProcessRepository()
        {
            return new ErsCtsMergeProcessRepository();
        }

        public ErsCtsMergeProcess GetErsCtsMergeProcess()
        {
            return new ErsCtsMergeProcess();
        }

        public ErsCtsMergeProcess GetErsMergeProcessWithModel(ErsModelBase model)
        {
            var merge = this.GetErsCtsMergeProcess();
            merge.OverwriteWithModel(model);
            return merge;
        }

        public ErsCtsMergeProcess GetErsMergeProcessWithParameters(Dictionary<string, object> parameters)
        {
            var merge = this.GetErsCtsMergeProcess();
            merge.OverwriteWithParameter(parameters);
            return merge;
        }

        public MergeCheckMemberStgy GetMergeCheckMemberStgy()
        {
            return new MergeCheckMemberStgy();
        }

        public MergeCheckInquiryStgy GetMergeCheckInquiryStgy()
        {
            return new MergeCheckInquiryStgy();
        }

        public virtual ErsCtsMergeHistoryRepository GetErsCtsMergeHistoryRepository()
        {
            return new ErsCtsMergeHistoryRepository();
        }

        public virtual ErsCtsMergeHistory GetErsCtsMergeHistory()
        {
            return new ErsCtsMergeHistory();
        }

        public virtual ErsCtsMergeHistory GetErsCtsMergeHistoryWithParameters(Dictionary<string, object> dr)
        {
            var objCtsMergeHistory = this.GetErsCtsMergeHistory();
            objCtsMergeHistory.OverwriteWithParameter(dr);
            return objCtsMergeHistory;
        }
    }
}
