﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merge
{
    public class ErsCtsMergeHistoryCriteria : Criteria
	{
        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_merge_history_t.id", value, Operation.EQUAL));
            }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("id", orderBy);
        }

	}
}
