﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.merge.strategy
{
    /// <summary>
    /// Checks Whether the member already exists.
    /// </summary>
    public class MergeCheckMemberStgy
    {
        /// <summary>
        /// Check member exist
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public virtual ValidationResult CheckMemberExist(string mcode, string fieldNameKey)
        {

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();

            criteria.mcode = mcode;

            if (repository.GetRecordCount(criteria) != 1)
            {
                return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName(fieldNameKey)));
            }

            return null;
        }

        public ValidationResult CheckNotResigned(string mcode)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();

            criteria.mcode = mcode;

            var listMember = repository.Find(criteria);

            if (listMember.Count > 0)
            {
                if (listMember.First().deleted == EnumDeleted.Deleted)
                {
                    return new ValidationResult(ErsResources.GetMessage("20300"));
                }
            }

            return null;
        }
    }
}
