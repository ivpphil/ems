﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merge.strategy
{
    /// <summary>
    /// Checks Whether the member already exists.
    /// </summary>
    public class MergeCheckInquiryStgy
    {
        /// <summary>
        /// Return true if Basket is empty.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public virtual ValidationResult CheckCaseNoExist(int? case_no)
        {

            var inq = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryWithCaseNo(case_no);
            
            if (inq == null)
                return new ValidationResult(ErsResources.GetMessage("default","Case No " + case_no.ToString() + " is invalid or not exist."));
                //TO DO: error message

            return null;
        }

    }
}
