﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.order
{
    public class ErsRegularOrderCriteria
        : Criteria
    {
        public virtual int? id { set { this.Add(Criteria.GetCriterion("regular_t.id", value, Operation.EQUAL)); } }

        public virtual EnumPaymentType? pay { set { this.Add(Criteria.GetCriterion("regular_t.pay", value, Operation.EQUAL)); } }

        public virtual string d_no { set { this.Add(Criteria.GetCriterion("regular_t.d_no", value, Operation.EQUAL)); } }

        public virtual short? send { set { this.Add(Criteria.GetCriterion("regular_t.send", value, Operation.EQUAL)); } }

        public virtual string ransu { set { this.Add(Criteria.GetCriterion("regular_t.ransu", value, Operation.EQUAL)); } }

        public virtual EnumWrap? wrap { set { this.Add(Criteria.GetCriterion("regular_t.wrap", (int?)value, Operation.EQUAL)); } }

        public virtual EnumPmFlg? pm_flg { set { this.Add(Criteria.GetCriterion("regular_t.pm_flg", (int?)value, Operation.EQUAL)); } }

        public virtual DateTime? intime { set { this.Add(Criteria.GetCriterion("regular_t.intime", value, Operation.EQUAL)); } }

        public virtual DateTime? utime { set { this.Add(Criteria.GetCriterion("regular_t.utime", value, Operation.EQUAL)); } }


        public virtual int? detail_id { set { this.Add(Criteria.GetCriterion("regular_detail_t.id", value, Operation.EQUAL)); } }

        public virtual int? detail_pay { set { this.Add(Criteria.GetCriterion("regular_detail_t.pay", value, Operation.EQUAL)); } }

        public virtual string mcode { set { this.Add(Criteria.GetCriterion("regular_detail_t.mcode", value, Operation.EQUAL)); } }

        public virtual DateTime? next_date { set { this.Add(Criteria.GetCriterion("regular_detail_t.next_date", value, Operation.EQUAL)); } }

        public virtual DateTime? next_date_less_equal { set { this.Add(Criteria.GetCriterion("regular_detail_t.next_date", value, Operation.LESS_EQUAL)); } }

        public virtual string mixed_group_code { set { this.Add(Criteria.GetCriterion("regular_detail_t.mixed_group_code", value, Operation.EQUAL)); } }

        public virtual int next_sendtime { set { this.Add(Criteria.GetCriterion("regular_detail_t.next_sendtime", value, Operation.EQUAL)); } }

        public virtual int? member_card_id { set { this.Add(Criteria.GetCriterion("regular_detail_t.member_card_id", value, Operation.EQUAL)); } }

        public virtual EnumWeekendOperation? weekend_operation { set { this.Add(Criteria.GetCriterion("regular_detail_t.weekend_operation", (int?)value, Operation.EQUAL)); } }

        public virtual int? member_add_id { set { this.Add(Criteria.GetCriterion("regular_detail_t.member_add_id", value, Operation.EQUAL)); } }

        public virtual DateTime? delete_date { set { this.Add(Criteria.GetCriterion("regular_detail_t.delete_date", value, Operation.EQUAL)); } }

        public virtual string scode { set { this.Add(Criteria.GetCriterion("regular_detail_t.scode", value, Operation.EQUAL)); } }

        public virtual void SetIntimeOrderBy(OrderBy orderBy)
        {
            this.AddOrderBy("regular_t.intime", orderBy);
        }

        public void SetActiveOnly()
        {
            var criteria_compare = Criteria.GetCriterion("delete_date", ColumnName("next_date"), Operation.GREATER_THAN);
            var criteria_null = Criteria.GetCriterion("delete_date", null, Operation.EQUAL);
            var criteria_lastdate = Criteria.GetCriterion("last_date", DateTime.Now, Operation.GREATER_THAN);
            this.Add(Criteria.JoinWithOR(new[] { criteria_compare, criteria_null, criteria_lastdate }));
        }

        public void SetNonActiveOnly()
        {
            var criteria_compare = Criteria.GetCriterion("delete_date", ColumnName("next_date"), Operation.LESS_EQUAL);
            var criteria_lastdate = Criteria.GetCriterion("last_date", DateTime.Now, Operation.LESS_EQUAL);
            this.Add(Criteria.JoinWithAnd(new[] { criteria_compare, criteria_lastdate }));
        }

        /// <summary>
        /// exclude regular orders that is already canceled
        /// </summary>
        public void SetExcludeCanceledRegularOrder()
        {
            var criteria_compare = Criteria.GetCriterion("delete_date", ColumnName("next_date"), Operation.GREATER_THAN);
            var criteria_null = Criteria.GetCriterion("delete_date", null, Operation.EQUAL);
            this.Add(Criteria.JoinWithOR(new[] { criteria_compare, criteria_null }));
        }

        public EnumConvCode? conv_code { set { this.Add(Criteria.GetCriterion("regular_detail_t.conv_code", value, Operation.EQUAL)); } }

        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("regular_detail_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("regular_detail_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}