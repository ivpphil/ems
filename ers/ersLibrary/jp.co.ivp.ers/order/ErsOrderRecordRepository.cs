﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderRecordRepository
        : ErsRepository<ErsOrderRecord>
    {
        public ErsOrderRecordRepository()
            : base(new ErsDB_ds_master_t())
        {
        }

        /// <summary>
        /// Get Order details for ersOrder. sorted by id
        /// </summary>
        /// <param name="d_no"></param>
        /// <returns></returns>
        public virtual Dictionary<string, ErsOrderRecord> Find(ErsOrder objOrder, Criteria criteria)
        {
            var searchSpec = ErsFactory.ersOrderFactory.GetOrderRecordSearchSpecification();
            var list = searchSpec.GetSearchData(criteria);

            List<ErsOrderRecord> listRecord = new List<ErsOrderRecord>();

            foreach (var dr in list)
            {
                ErsOrderRecord order = ErsFactory.ersOrderFactory.GetErsOrderRecord();
                order.OverwriteWithParameter(dr);

                listRecord.Add(order);
            }

            var returnValue = new Dictionary<string, ErsOrderRecord>();

            var createKeyStgy = ErsFactory.ersOrderFactory.GetCreateOrderRecordKeyStgy();
            foreach (var record in listRecord)
            {
                var key = createKeyStgy.GetKey(record, objOrder.senddate);
                if (returnValue.ContainsKey(key))
                {
                    var message = new StringBuilder();
                    throw new Exception(string.Format("d_no:{0} has duplicated record.", record.d_no));
                }
                returnValue.Add(key, record);
            }
            return returnValue;
        }
    }
}
