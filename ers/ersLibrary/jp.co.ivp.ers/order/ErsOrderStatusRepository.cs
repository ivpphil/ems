﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderStatusRepository
        : ErsRepository<ErsOrderStatus>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsOrderStatusRepository()
            : base("order_status_t")
        {
        }
    }
}