﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order
{
    public class ErsRegularErrLogRepository
        : ErsRepository<ErsRegularErrLog>
    {
        public ErsRegularErrLogRepository()
            : base("regular_error_t")
        {
        }
    }
}
