﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderRecordStatusHistoryRepository
        : ErsRepository<ErsOrderRecordStatusHistory>
    {
        public ErsOrderRecordStatusHistoryRepository()
            : base("ds_status_history_t")
        {
        }
    }
}
