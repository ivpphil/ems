﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order
{
    public interface IManageRegularPatternDatasource
    {
        EnumSendPtn? send_ptn { get; }

        string scode { get; }

        DateTime? next_date { get; }

        short? ptn_interval_day { get; }

        short? ptn_interval_month { get; }

        short? ptn_day { get; }

        short? ptn_interval_week { get; }

        DayOfWeek? ptn_weekday { get; }

        Dictionary<string, object> GetPropertiesAsDictionary();
    }
}
