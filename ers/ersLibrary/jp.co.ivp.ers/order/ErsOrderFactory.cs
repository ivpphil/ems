﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.order.strategy;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.Send;
using jp.co.ivp.ers.order.strategy.status;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.strategy;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.order.ds_set.specification;
using jp.co.ivp.ers.Payment.account_laundering.strategy;
using jp.co.ivp.ers.Send.specification;
using jp.co.ivp.ers.Payment.continual_billing;
using jp.co.ivp.ers.Payment.specification;
using ers.jp.co.ivp.ers.order.specification;

namespace jp.co.ivp.ers.order
{
    /// <summary>
    /// 伝票のファクトリクラス
    /// </summary>
    public class ErsOrderFactory
    {
        /// <summary>
        /// Gets instance of ErsRegularOrder Initialized with the dictionary.
        /// </summary>
        /// <returns></returns>
        public virtual ErsOrder GetErsOrderWithParameters(Dictionary<string, object> parameters)
        {
            var ersOrder = this.GetErsOrder();
            ersOrder.OverwriteWithParameter(parameters);
            return ersOrder;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d_no"></param>
        /// <param name="ersbasket">カート情報</param>
        /// <returns></returns>
        public virtual IDictionary<string, ErsOrderRecord> GetErsOrderRecordList(string d_no, ErsBasket ersbasket, EnumDelvMethod? deliv_method, int? shipping_pref)
        {
            IDictionary<string, ErsOrderRecord> orderRecords = new Dictionary<string, ErsOrderRecord>();

            var strategy = ErsFactory.ersOrderFactory.GetAddOrdinaryOrderStrategy();
            var createKeyStgy = ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy();

            // カート（明細）情報取得
            Dictionary<string, ErsBaskRecord> allrecord = ersbasket.objBasketRecord;
            foreach (var merchandise in allrecord.Values)
            {
                var key = createKeyStgy.GetKey(merchandise);

                // 明細オブジェクト生成
                var record = allrecord[key];
                orderRecords.Add(strategy.GetOrder(d_no, record, record.amount, deliv_method, shipping_pref));
            }

            return orderRecords;
        }

        /// <summary>
        /// Get an instance of ErsOrder using bill number
        /// 伝票番号をキーに、伝票クラスのインスタンスを取得
        /// </summary>
        /// <param name="d_no">伝票番号</param>
        /// <returns>伝票クラスのインスタンス</returns>
        public virtual ErsOrder GetOrderWithD_no(string d_no, int? site_id = null)
        {
            var repository = this.GetErsOrderRepository();
            var criteria = this.GetErsOrderCriteria();

            criteria.d_no = d_no;

            if (site_id.HasValue)
            {
                criteria.site_id = site_id;
            }

            var list = repository.Find(criteria);

            if (list.Count != 1)
            {
                return null;
            }

            return list.First();

        }

        /// <summary>
        /// Get an instance of ErsOrder
        /// </summary>
        /// <returns></returns>
        public virtual ErsOrder GetErsOrder()
        {
            return new ErsOrder();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objOrder"></param>
        /// <returns></returns>
        public Dictionary<string, ErsOrderRecord> GetErsOrderRecordList(ErsOrder objOrder)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
            criteria.d_no = objOrder.d_no;
            criteria.site_id = objOrder.site_id;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            return repository.Find(objOrder, criteria);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objOrder"></param>
        /// <returns></returns>
        public Dictionary<string, ErsOrderRecord> GetErsOrderProductRecordList(ErsOrder objOrder)
        {
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
            criteria.d_no = objOrder.d_no;
            criteria.site_id = objOrder.site_id;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            var searchSpec = ErsFactory.ersOrderFactory.GetObtainProductRecordSpec();
            var list = searchSpec.GetSearchData(criteria);

            List<ErsOrderRecord> listRecord = new List<ErsOrderRecord>();

            foreach (var dr in list)
            {
                ErsOrderRecord order = ErsFactory.ersOrderFactory.GetErsOrderRecord();
                order.OverwriteWithParameter(dr);

                listRecord.Add(order);
            }

            var returnValue = new Dictionary<string, ErsOrderRecord>();

            var createKeyStgy = ErsFactory.ersOrderFactory.GetCreateOrderRecordKeyStgy();
            foreach (var record in listRecord)
            {
                var key = createKeyStgy.GetKey(record, objOrder.senddate);
                if (returnValue.ContainsKey(key))
                {
                    var message = new StringBuilder();
                    throw new Exception(string.Format("d_no:{0} has duplicated record.", record.d_no));
                }
                returnValue.Add(key, record);
            }
            return returnValue;

        }

        /// <summary>
        /// Get an instance of ErsOrder using values of the Dictionary object
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public virtual ErsOrderRecord GetErsOrderRecordWithParameter(Dictionary<string, object> parameter)
        {
            var record = GetErsOrderRecord();
            record.OverwriteWithParameter(parameter);
            return record;
        }

        public virtual ErsOrderRecord GetErsOrderRecordWithId(int? id)
        {
            var repository = this.GetErsOrderRecordRepository();
            var criteria = this.GetErsOrderRecordCriteria();
            criteria.id = id;
            var listRecord = repository.Find(criteria);
            if (listRecord.Count != 1)
            {
                return null;
            }

            return listRecord.First();
        }

        /// <summary>
        /// Get an instance of ErsOrderRecode
        /// </summary>
        /// <returns></returns>
        public virtual ErsOrderRecord GetErsOrderRecord()
        {
            return new ErsOrderRecord();
        }

        /// <summary>
        /// Get an instance of ErsOrderCriteria
        /// </summary>
        /// <returns></returns>
        public virtual ErsOrderCriteria GetErsOrderCriteria()
        {
            return new ErsOrderCriteria();
        }

        public virtual ErsOrderRepository GetErsOrderRepository()
        {
            return new ErsOrderRepository();
        }

        public virtual OrderRecordSearchSpecification GetOrderRecordSearchSpecification()
        {
            return new OrderRecordSearchSpecification();
        }

        public virtual OrderSearchSpecification GetOrderSerachSpecification()
        {
            return new OrderSearchSpecification();
        }

        public virtual NewTop5Specification GetNewTop5Specification()
        {
            return new NewTop5Specification();
        }

        public virtual NewSaleSpecification GetNewSaleSpecification()
        {
            return new NewSaleSpecification();
        }

        public virtual NewOrderSpecification GetNewOrderSpecification()
        {
            return new NewOrderSpecification();
        }

        public virtual NewCancelNumSpecification GetNewCancelNumSpecification()
        {
            return new NewCancelNumSpecification();
        }

        public virtual OrdinaryOrderCancelStrategy GetOrderCancelStrategy()
        {
            return new OrdinaryOrderCancelStrategy();
        }

        public virtual AddOrdinaryOrderStrategy GetAddOrdinaryOrderStrategy()
        {
            return new AddOrdinaryOrderStrategy();
        }

        public virtual OtherModifyOrderPaymentStatusStrategy GetOtherModifyOrderPaymentStatusStrategy()
        {
            return new OtherModifyOrderPaymentStatusStrategy();
        }

        public virtual ErsCalcService GetErsCalcService()
        {
            return new ErsCalcService();
        }

        public virtual CheckValidityStgy GetCheckValidityStgy()
        {
            return new CheckValidityStgy();
        }


        public virtual IErsPaymentBase GetErsPayment(EnumPaymentType? type)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (type == EnumPaymentType.CREDIT_CARD)
            {
                return (IErsPaymentBase)ErsReflection.CreateInstanceFromFullName(setup.CreditCardPaymentClass);
            }
            else if (type == EnumPaymentType.CONVENIENCE_STORE)
            {
                return (IErsPaymentBase)ErsReflection.CreateInstanceFromFullName(setup.ConveniencePaymentClass);
            }
            else if (type == EnumPaymentType.PAYPAL)
            {
                return new ErsPayPal();
            }
            else
            {
                return new ErsPaymentEmpty();
            }
        }

        public virtual HasMonitorMerchandiseSpec GetHasMonitorMerchandiseSpec()
        {
            return new HasMonitorMerchandiseSpec();
        }

        public virtual IsMonitorSpec GetIsMonitorSpec()
        {
            return new IsMonitorSpec();
        }

        public virtual ErsEtc GetErsEtc()
        {
            return new ErsEtc();
        }

        public virtual ErsPayRepository GetErsPayRepository()
        {
            return new ErsPayRepository();
        }

        public virtual ErsPayCriteria GetErsPayCriteria()
        {
            return new ErsPayCriteria();
        }

        public virtual ErsPay GetErsPay()
        {
            return new ErsPay();
        }

        public virtual ErsPay GetErsPayWithParameter(Dictionary<string, object> inputParameters)
        {
            var objPay = this.GetErsPay();
            objPay.OverwriteWithParameter(inputParameters);
            return objPay;
        }

        public virtual ErsPay GetErsPayWithIdAndSiteId(EnumPaymentType? id, int? site_id)
        {
            var repository = this.GetErsPayRepository();
            var criteria = this.GetErsPayCriteria();
            criteria.id = id;
            criteria.site_id = site_id;
            var list = repository.Find(criteria);
            if (list.Count == 1)
                return list[0];

            return null;
        }

        public virtual ErsEtcRepository GetErsEtcRepository()
        {
            return new ErsEtcRepository();
        }

        public virtual ErsEtcCriteria GetErsEtcCriteria()
        {
            return new ErsEtcCriteria();
        }

        public virtual ErsEtc GetErsEtcWithId(int id)
        {
            var repository = this.GetErsEtcRepository();
            var criteria = this.GetErsEtcCriteria();
            criteria.id = id;
            var list = repository.Find(criteria);

            if (list.Count == 1)
                return list[0];

            return null;
        }

        public virtual ErsCard GetErsCardWithId(int id)
        {
            var repository = this.GetErsCardRepository();
            var criteria = this.GetErsCardCriteria();
            criteria.id = id;
            var list = repository.Find(criteria);
            if (list.Count == 1)
                return list[0];

            return null;
        }

        public virtual ErsCard GetErsCardWithParameter(Dictionary<string, object> parameter)
        {
            var objCard = this.GetErsCard();
            objCard.OverwriteWithParameter(parameter);
            return objCard;
        }

        public virtual ErsCard GetErsCard()
        {
            return new ErsCard();
        }

        public virtual ErsCardCriteria GetErsCardCriteria()
        {
            return new ErsCardCriteria();
        }

        public virtual ErsCardRepository GetErsCardRepository()
        {
            return new ErsCardRepository();
        }

        //Validate senddate.
        public virtual ValidateSenddateStgy GetValidateSenddateStgy()
        {
            return new ValidateSenddateStgy();
        }

        public virtual ValidateSendStgy GetValidateSendStgy()
        {
            return new ValidateSendStgy();
        }

        public virtual ValidateSendtimeStgy GetValidateSendtimeStgy()
        {
            return new ValidateSendtimeStgy();
        }

        public virtual UpdateMemberDataStgy GetUpdateMemberDataStgy()
        {
            return new UpdateMemberDataStgy();
        }

        public virtual UpdateMemberPointStgy GetUpdateMemberPointStgy()
        {
            return new UpdateMemberPointStgy();
        }

        public virtual ObtainErsOrderStatusStgy GetObtainErsOrderStatusStgy()
        {
            return new ObtainErsOrderStatusStgy();
        }

        public virtual OrderStatusSetDeliverRequestStgy GetOrderStatusSetDeliverRequestStgy()
        {
            return new OrderStatusSetDeliverRequestStgy();
        }

        public virtual OrderStatusSetDeliverdStgy GetOrderStatusSetDeliverdStgy()
        {
            return new OrderStatusSetDeliverdStgy();
        }

        public virtual ObatainTargetRegularOrderSpec GetObatainTargetRegularOrderSpec()
        {
            return new ObatainTargetRegularOrderSpec();
        }

        public virtual CheckPointsStgy GetCheckPointsStgy()
        {
            return new CheckPointsStgy();
        }

        public virtual LastUsedCardInfoStrategy GetLastUsedCardInfoStrategy()
        {
            return new LastUsedCardInfoStrategy();
        }

        public virtual ErsSalePtnRepository GetErsSalePtnRepository()
        {
            return new ErsSalePtnRepository();
        }

        public virtual ErsSalePtnCriteria GetErsSalePtnCriteria()
        {
            return new ErsSalePtnCriteria();
        }

        public virtual ErsSalePtn GetErsSalePtn()
        {
            return new ErsSalePtn();
        }

        public virtual ErsOrderStatusRepository GetErsOrderStatusRepository()
        {
            return new ErsOrderStatusRepository();
        }

        public virtual ErsOrderStatusCriteria GetErsOrderStatusCriteria()
        {
            return new ErsOrderStatusCriteria();
        }

        public virtual ErsOrderStatus GetErsOrderStatus()
        {
            return new ErsOrderStatus();
        }

        public virtual ErsOrderPaymentStatusRepository GetErsOrderPaymentStatusRepository()
        {
            return new ErsOrderPaymentStatusRepository();
        }

        public virtual ErsOrderPaymentStatusCriteria GetErsOrderPaymentStatusCriteria()
        {
            return new ErsOrderPaymentStatusCriteria();
        }

        public virtual ErsOrderPaymentStatus GetErsOrderPaymentStatus()
        {
            return new ErsOrderPaymentStatus();
        }

        /// <summary>
        /// Obtain instance of ErsSendTimeRepository class
        /// 配送時間リポジトリを取得する
        /// </summary>
        /// <returns>returns instance of ErsSendTimeRepository</returns>
        public virtual ErsSendTimeRepository GetErsSendTimeRepository()
        {
            return new ErsSendTimeRepository();
        }

        /// <summary>
        /// Obtain instance of ErsSendTimeCriteria class 
        /// 配送時間クライテリアを取得する
        /// </summary>
        /// <returns>returns instance of ErsSendTimeCriteria</returns>
        public virtual ErsSendTimeCriteria GetErsSendTimeCriteria()
        {
            return new ErsSendTimeCriteria();
        }

        /// <summary>
        /// Obtain instance of ErsSendTime class 
        /// 配送時間を取得する
        /// </summary>
        /// <returns>returns instance of ErsSendTime</returns>
        public virtual ErsSendTime GetErsSendTime()
        {
            return new ErsSendTime();
        }

        public virtual ErsSendTime GetErsSendTimeWithId(int? id)
        {
            var repository = this.GetErsSendTimeRepository();
            var criteria = this.GetErsSendTimeCriteria();
            criteria.id = id;
            var listObj = repository.Find(criteria);
            if (listObj.Count != 1)
            {
                return null;
            }
            return listObj.First();
        }

        /// <summary>
        /// カレンダーのリポジトリを取得する
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// Repositoryを取得する
        /// </summary>
        /// <returns>returns instance of ErsCalendarRepository</returns>
        public virtual ErsCalendarRepository GetErsCalendarRepository()
        {
            return new ErsCalendarRepository();
        }

        /// <summary>
        /// Obtain instance of ErsCalendarCriteria class 
        /// カレンダーの検索条件を取得する
        /// </summary>
        /// <returns>returns instance of ErsCalendarCriteria</returns>
        public virtual ErsCalendarCriteria GetErsCalendarCriteria()
        {
            return new ErsCalendarCriteria();
        }

        public virtual ErsCalendar GetErsCalendar()
        {
            return new ErsCalendar();
        }

        public virtual ErsCalendar GetErsCalenderWithCloseDate(DateTime close_date)
        {
            var repository = this.GetErsCalendarRepository();
            var criteria = this.GetErsCalendarCriteria();

            criteria.close_date = close_date;

            if (repository.GetRecordCount(criteria) == 0)
            {
                return null;
            }

            return repository.Find(criteria).First();
        }

        public virtual ErsCalendar GetErsCalendarWithParameter(Dictionary<string, object> data)
        {
            var objCalender = this.GetErsCalendar();
            objCalender.OverwriteWithParameter(data);
            return objCalender;
        }

        public virtual ObtainEtcAmountStgy GetObtainEtcAmountStgy()
        {
            return new ObtainEtcAmountStgy();
        }

        public virtual ObtainAmountTotalStgy GetObtainAmountTotalStgy()
        {
            return new ObtainAmountTotalStgy();
        }

        public virtual ObtainOrderTypeStgy GetObtainOrderTypeStgy()
        {
            return new ObtainOrderTypeStgy();
        }

        public virtual ShippingAddressInfo GetShippingAddressInfo()
        {
            return new ShippingAddressInfo();
        }

        public virtual SetD_noStgy GetSetD_noStgy()
        {
            return new SetD_noStgy();
        }

        public virtual ObtainNewSubd_noStgy GetObtainNewSubd_noStgy()
        {
            return new ObtainNewSubd_noStgy();
        }

        /// <summary>
        /// Get an instance of ErsOrder using values of the Dictionary object
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public virtual ErsRegularOrderRecord GetErsRegularOrderRecordWithParameter(ErsRegularOrder objOrder, Dictionary<string, object> parameter)
        {
            var record = this.GetErsRegularOrderRecord(objOrder);
            record.OverwriteWithParameter(parameter);
            return record;
        }

        public virtual ErsRegularOrderRecord GetErsRegularOrderRecord(ErsRegularOrder objOrder)
        {
            return new ErsRegularOrderRecord(objOrder);
        }

        /// <summary>
        /// Gets instance of ErsRegularOrder Initialized with the dictionary.
        /// </summary>
        /// <returns></returns>
        public virtual ErsRegularOrder GetErsRegularOrderWithParameters(Dictionary<string, object> parameters)
        {
            var ersOrder = this.GetErsRegularOrder();
            ersOrder.OverwriteWithParameter(parameters);
            return ersOrder;
        }

        public virtual ErsRegularOrder GetErsRegularOrder()
        {
            return new ErsRegularOrder();
        }

        /// <summary>
        /// Gets instance of ErsRegularOrder Initialized with dictionary. the instance is returned as ErsOrder.
        /// </summary>
        /// <returns></returns>
        public virtual ErsRegularOrder GetRegularOrderWithParameter(IRegistRegularOrderDataSource datasource, ErsBasket ersbasket, int? regular_sendtime)
        {
            if (datasource == null)
            {
                ///何も入力がないときはエラーとしてnullを返す
                return null;
            }

            var retOrder = this.GetErsRegularOrder();

            // ヘッダパラメータセット
            retOrder.OverwriteWithParameter(datasource.GetPropertiesAsDictionary());

            retOrder.member_card_id = datasource.card_id;
            
            retOrder.next_sendtime = regular_sendtime;

            retOrder.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            LoadRegularOrderRecords(ersbasket, retOrder);

            return retOrder;
        }

        public virtual void LoadRegularOrderRecords(ErsBasket ersbasket, ErsRegularOrder retOrder)
        {
            // カート（明細）情報取得
            Dictionary<string, ErsBaskRecord> allrecord = ersbasket.objRegularBasketRecord;
            foreach (string scode in allrecord.Keys)
            {
                var strategy = this.GetAddRegularOrderStrategy();

                // 明細オブジェクト生成
                var record = allrecord[scode];
                strategy.AddOrder(retOrder, record);
            }
        }

        public virtual AddRegularOrderStrategy GetAddRegularOrderStrategy()
        {
            return new AddRegularOrderStrategy();
        }

        /// <summary>
        /// Gets instance of ErsRegularOrderRepository.
        /// </summary>
        /// <returns></returns>
        public virtual ErsRegularOrderRepository GetErsRegularOrderRepository()
        {
            return new ErsRegularOrderRepository();
        }

        /// <summary>
        /// Gets instance of ErsRegularOrderRecord Initialized with ErsRepositoryEntity.
        /// </summary>
        /// <param name="regular_id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual ErsRegularOrderRecord GetErsRegularOrderRecordWithParameteres(int? regular_id, ErsRegularOrder objOrder, ErsRepositoryEntity entity)
        {
            var record = this.GetErsRegularOrderRecord(objOrder);

            var dictionary = entity.GetPropertiesAsDictionary();
            if (dictionary.ContainsKey("id"))
                dictionary.Remove("id");

            record.OverwriteWithParameter(dictionary);
            record.regular_id = regular_id;
            return record;
        }

        public virtual AddBatchOrderStrategy GetAddBatchOrderStrategy()
        {
            return new AddBatchOrderStrategy();
        }

        public virtual RegularRegister GetErsRegularRegister()
        {

            return new RegularRegister();
        }

        public virtual ErsRegularErrLog GetErsRegularErrLog()
        {
            return new ErsRegularErrLog();
        }

        //定期明細取得
        public ErsRegularOrderRecord GetErsRegularOrderRecordWithId(int? id)
        {
            var recRep = this.GetErsRegularOrderRecordRepository();
            var regularCriteria = this.GetErsRegularOrderCriteria();

            //検索条件をクライテリアに保存
            regularCriteria.detail_id = id;
            regularCriteria.AddOrderBy("regular_detail_t.id", Criteria.OrderBy.ORDER_BY_ASC);

            var list = recRep.Find(null, regularCriteria);

            return list[0];
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public virtual ErsRegularOrderRecordRepository GetErsRegularOrderRecordRepository()
        {
            return new ErsRegularOrderRecordRepository();
        }

        /// <summary>
        /// Gets instance of ErsRegularOrderRecordCriteria.
        /// </summary>
        /// <returns></returns>
        public virtual ErsRegularOrderRecordCriteria GetErsRegularOrderRecordCriteria()
        {
            return new ErsRegularOrderRecordCriteria();
        }
        /// <summary>
        /// Gets instance of ErsRegularOrderCriteria.
        /// </summary>
        /// <returns></returns>
        public virtual ErsRegularOrderCriteria GetErsRegularOrderCriteria()
        {
            return new ErsRegularOrderCriteria();
        }

        /// <summary>
        /// Gets instance of ManageRegularPatternService.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual ManageRegularPatternService GetManageRegularPatternService(EnumSendPtn send_ptn)
        {
            return new ManageRegularPatternService(send_ptn);
        }

        /// <summary>
        /// Gets instance of RegularOrderSearchStpec.
        /// </summary>
        /// <returns></returns>
        public virtual RegularOrderSearchSpec GetRegularOrderSearchSpec()
        {
            return new RegularOrderSearchSpec();
        }

        public virtual ErsOrderIntegrated GetErsOrderIntegrated()
        {
            return new ErsOrderIntegrated();
        }

        public virtual RegistRegularOrderStgy GetRegistRegularOrderStgy()
        {
            return new RegistRegularOrderStgy();
        }

        public virtual ErsRegularLog GetErsRegularLog()
        {
            return new ErsRegularLog();
        }

        public virtual ErsRegularLogRepository GetErsRegularLogRepository()
        {
            return new ErsRegularLogRepository();
        }

        public virtual ErsRegularErrLogRepository GetErsRegularErrLogRepository()
        {
            return new ErsRegularErrLogRepository();
        }

        public virtual ErsRegularErrLogCriteria GetErsRegularErrLogCriteria()
        {
            return new ErsRegularErrLogCriteria();
        }

        /// <summary>
        /// セット明細クラスを取得する
        /// </summary>
        /// <returns></returns>
        public virtual ErsDsSet GetErsDsSet()
        {
            return new ErsDsSet();
        }

        /// <summary>
        /// セット商品クラスをセット明細クラスへ
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public virtual ErsDsSet GetErsDsSetWithParameter(ErsSetMerchandise setItem, ErsOrderRecord orderRecords)
        {
            var dsSet = GetErsDsSet();
            dsSet.OverwriteWithParameter(setItem.GetPropertiesAsDictionary());

            dsSet.id = null;
            dsSet.d_no = orderRecords.d_no;
            dsSet.ds_id = orderRecords.id;
            dsSet.intime = null;
            dsSet.utime = null;

            return dsSet;
        }

        /// <summary>
        /// ds_set_t id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual ErsDsSet GetErsDsSetWithId(int id)
        {
            var repository = this.getErsDsSetRepository();
            var criteria = this.getErsDsSetCriteria();
            criteria.id = id;
            var list = repository.Find(criteria);

            if (list.Count == 1)
                return list[0];

            return null;
        }

        /// <summary>
        /// dictionaryの値をセットしたErsDsSetをコピー
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public virtual ErsDsSet GetErsDsSetWithParameter(Dictionary<string, object> parameters)
        {
            var dsSet = GetErsDsSet();
            dsSet.OverwriteWithParameter(parameters);
            return dsSet;
        }

        public virtual ErsDsSetRepository getErsDsSetRepository()
        {
            return new ErsDsSetRepository();
        }

        public virtual ErsDsSetCriteria getErsDsSetCriteria()
        {
            return new ErsDsSetCriteria();
        }

        public virtual DsSetSearchSpec GetDsSetSearchSpec()
        {
            return new DsSetSearchSpec();
        }

        public virtual CheckFloorLimitStgy GetCheckFloorLimitStgy()
        {
            return new CheckFloorLimitStgy();
        }

        public virtual CheckMailDeliveryStgy GetCheckMailDeliveryStgy()
        {
            return new CheckMailDeliveryStgy();
        }

        public virtual CheckCampaignAlreadyPurchasedStgy GetCheckCampaignAlreadyPurchasedStgy()
        {
            return new CheckCampaignAlreadyPurchasedStgy();
        }

        /// <summary>
        /// return strategy for regist set items.
        /// </summary>
        /// <returns></returns>
        public virtual RegistSetMerchandiseStgy GetRegistSetItemsStgy()
        {
            return new RegistSetMerchandiseStgy();
        }

        public virtual ValidatePluralOrderTypeStgy GetValidatePluralOrderTypeStgy()
        {
            return new ValidatePluralOrderTypeStgy();
        }

        public virtual CheckRegularBatchIsPurchaseStgy GetCheckRegularBatchIsPurchaseStgy()
        {
            return new CheckRegularBatchIsPurchaseStgy();
        }

        public virtual ImportAccountLaunderingResultStgyFactory GetImportAccountLaunderingResultStgyFactory()
        {
            return new ImportAccountLaunderingResultStgyFactory();
        }

        public virtual ImportContinualBillingResultStgyFactory GetImportContinualBillingResultStgyFactory()
        {
            return new ImportContinualBillingResultStgyFactory();
        }

        public virtual ValidatePayStgy GetValidatePayStgy()
        {
            return new ValidatePayStgy();
        }

        public virtual IsBasketHasRegularItem GetIsBasketHasRegularItem()
        {
            return new IsBasketHasRegularItem();
        }

        public virtual ErsRegularLog GetErsRegularLogWithParameters(Dictionary<string, object> dr)
        {
            var regularLog = this.GetErsRegularLog();
            regularLog.OverwriteWithParameter(dr);
            return regularLog;
        }

        public virtual ObatainTargetRegularOrderRecordKeysSpec GetObatainTargetRegularOrderRecordKeysSpec()
        {
            return new ObatainTargetRegularOrderRecordKeysSpec();
        }

        public virtual SearchOpenDatesSpec GetSearchOpenDatesSpec()
        {
            return new SearchOpenDatesSpec();
        }

        public virtual ErsEtc GetErsEtcWithParameter(Dictionary<string, object> parameter)
        {
            var objEtc = this.GetErsEtc();
            objEtc.OverwriteWithParameter(parameter);
            return objEtc;
        }

        public virtual ErsTpPaypalRepository GetErsTpPaypalRepository()
        {
            return new ErsTpPaypalRepository();
        }

        public virtual ErsTpPaypalCriteria GetErsTpPaypalCriteria()
        {
            return new ErsTpPaypalCriteria();
        }

        public virtual ErsTpPaypal GetErsTpPaypal()
        {
            return new ErsTpPaypal();
        }

        public virtual ErsOrderRecordRepository GetErsOrderRecordRepository()
        {
            return new ErsOrderRecordRepository();
        }

        public virtual ErsOrderRecordCriteria GetErsOrderRecordCriteria()
        {
            return new ErsOrderRecordCriteria();
        }

        public virtual ErsOrderContainer GetErsOrderContainer()
        {
            return new ErsOrderContainer();
        }

        public virtual IsAllRecordCanceledSpec GetIsAllRecordCanceledSpec()
        {
            return new IsAllRecordCanceledSpec();
        }

        public virtual IsNonNeededPaymentSpec GetIsNonNeededPaymentSpec()
        {
            return new IsNonNeededPaymentSpec();
        }

        public virtual IsAllBasketItemMailDeliverySpec GetIsAllBasketItemMailDeliverySpec()
        {
            return new IsAllBasketItemMailDeliverySpec();
        }

        public virtual ProductRankingSpec GetProductRankingSpec()
        {
            return new ProductRankingSpec();
        }

        public virtual CreateOrderRecordKeyStgy GetCreateOrderRecordKeyStgy()
        {
            return new CreateOrderRecordKeyStgy();
        }

        public virtual CreateRegularOrderRecordKeyStgy GetCreateRegularOrderRecordKeyStgy()
        {
            return new CreateRegularOrderRecordKeyStgy();
        }

        public virtual ObtainOrderSumAmount GetObtainOrderSumAmount()
        {
            return new ObtainOrderSumAmount();
        }

        public virtual ErsOrderMailRepository GetErsOrderMailRepository()
        {
            return new ErsOrderMailRepository();
        }

        public virtual ErsOrderMailCriteria GetErsOrderMailCriteria()
        {
            return new ErsOrderMailCriteria();
        }

        public virtual ErsOrderMail GetErsOrderMail()
        {
            return new ErsOrderMail();
        }

        public virtual ValidateOrderRecordStatusTransition GetValidateOrderRecordStatusTransition()
        {
            return new ValidateOrderRecordStatusTransition();
        }

        public virtual IsValidOrderStatustransitionSpec GetIsValidOrderStatustransitionSpec()
        {
            return new IsValidOrderStatustransitionSpec();
        }

        public virtual IsBeforeDeliverSpec GetIsBeforeDeliverSpec()
        {
            return new IsBeforeDeliverSpec();
        }

        #region ds_status_history_t

        /// <summary>
        /// ErsOrderRecordStatusHistory取得 [Get ErsOrderRecordStatusHistory]
        /// </summary>
        /// <returns>ErsOrderRecordStatusHistory</returns>
        public virtual ErsOrderRecordStatusHistory GetErsOrderRecordStatusHistory()
        {
            return new ErsOrderRecordStatusHistory();
        }

        /// <summary>
        /// ErsOrderRecordStatusHistoryRepository取得 [Get ErsOrderRecordStatusHistoryRepository]
        /// </summary>
        /// <returns>ErsOrderRecordStatusHistory</returns>
        public virtual ErsOrderRecordStatusHistoryRepository GetErsOrderRecordStatusHistoryRepository()
        {
            return new ErsOrderRecordStatusHistoryRepository();
        }

        /// <summary>
        /// ErsOrderRecordStatusHistoryCriteria取得 [Get ErsOrderRecordStatusHistoryCriteria]
        /// </summary>
        /// <returns>ErsOrderRecordStatusHistory</returns>
        public virtual ErsOrderRecordStatusHistoryCriteria GetErsOrderRecordStatusHistoryCriteria()
        {
            return new ErsOrderRecordStatusHistoryCriteria();
        }

        /// <summary>
        /// RegistOrderRecordStatusHistoryStgy取得 [Get RegistOrderRecordStatusHistoryStgy]
        /// </summary>
        /// <returns>RegistOrderRecordStatusHistoryStgy</returns>
        public virtual RegistOrderRecordStatusHistoryStgy GetRegistOrderRecordStatusHistoryStgy()
        {
            return new RegistOrderRecordStatusHistoryStgy();
        }

        #endregion

        public virtual IsPaymentInfoChangableSpec GetIsPaymentInfoChangableSpec()
        {
            return new IsPaymentInfoChangableSpec();
        }

        public virtual ValidateOrderRegistDeliveryMethod ValidateOrderRegistDeliveryMethod()
        {
            return new ValidateOrderRegistDeliveryMethod();
        }
        public virtual IsContinualBillingOrderSpec GetIsContinualBillingOrderSpec()
        {
            return new IsContinualBillingOrderSpec();
        }

        public virtual CountPurchasedPluralOrderAmountSpec GetCountPurchasedPluralOrderAmountSpec()
        {
            return new CountPurchasedPluralOrderAmountSpec();
        }

        /// <summary>
        /// Get instance of PutRegularDateForwardStgy class that puts the next dates forward.
        /// </summary>
        /// <returns></returns>
        public virtual PutRegularDateForwardStgy GetPutRegularDateForwardStgy()
        {
            return new PutRegularDateForwardStgy();
        }

        public virtual ObtainOrderProductRecordSpec GetObtainProductRecordSpec()
        {
            return new ObtainOrderProductRecordSpec();
        }
    }
}
