﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.Payment;

namespace jp.co.ivp.ers.order
{
    /// <summary>
    /// 伝票クラス
    /// </summary>
    public class ErsRegularOrder
        : ErsRepositoryEntity
    {
        /// <summary>
        /// 明細レコードリスト
        /// </summary>
        public virtual Dictionary<string, ErsOrderRecord> orderRecords { get; internal set; }

        //明細詳細
        public virtual IList<ErsRegularOrderRecord> regularOrderRecords { get; internal set; }

        /// <summary>
        /// コンストラクタ（内部用）
        /// </summary>
        protected internal ErsRegularOrder()
        {
            orderRecords = new Dictionary<string, ErsOrderRecord>();
            regularOrderRecords = new List<ErsRegularOrderRecord>();
        }

        /// <summary>
        /// 伝票番号
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 伝票番号
        /// </summary>
        public virtual string d_no { get; set; }

        public string base_d_no { get; set; }

        /// <summary>
        /// 発行日時
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新日時
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// 会員コード
        /// </summary>
        public virtual string mcode { get; set; }

        /// <summary>
        /// メールアドレス
        /// </summary>
        public virtual string email { get; set; }

        /// <summary>
        /// 当店へのコメント
        /// </summary>
        public virtual string memo { get; set; }

        /// <summary>
        /// 管理者コメント
        /// </summary>
        public virtual string memo3 { get; set; }

        /// <summary>
        /// お客様への連絡事項
        /// </summary>
        public virtual string usr_memo { get; set; }

        /// <summary>
        /// ポイント付与済みフラグ
        /// </summary>
        public virtual short? point_ck { get; set; }

        /// <summary>
        /// 購入サイト区分
        /// </summary>
        public virtual EnumPmFlg? pm_flg { get; set; }

        /// <summary>
        /// 乱数
        /// </summary>
        public virtual string ransu { get; set; }

        /// <summary>
        /// 小計
        /// </summary>
        public virtual int subtotal { get; set; }
        /// <summary>
        /// 消費税
        /// </summary>
        public virtual int tax { get; set; }
        /// <summary>
        /// ポイント値引き
        /// </summary>
        public virtual int p_service { get; set; }
        /// <summary>
        /// 送料
        /// </summary>
        public virtual int carriage { get; set; }
        /// <summary>
        /// 手数料
        /// </summary>
        public virtual int etc { get; set; }
        /// <summary>
        /// 合計
        /// </summary>
        public virtual int total { get; set; }

        /// <summary>
        /// クーポン値引き
        /// </summary>
        public virtual int coupon_discount { get; set; }

        /// <summary>
        /// クーポンコード
        /// </summary>
        public virtual string coupon_code { get; set; }

        /// <summary>
        /// 支払い方法ID(pay_tのIDと一緒)
        /// </summary>
        public virtual EnumPaymentType? pay { get; set; }

        #region CyberSource決済
        /// 決済ID
        /// </summary>
        public virtual string c_req_no { get; set; }//PayPalと併用
        #endregion

        #region PayPal決済
        /// <summary>
        /// token
        /// </summary>
        public virtual string token { get; set; }

        /// <summary>
        /// payerID
        /// </summary>
        public virtual string PayerID { get; set; }
        #endregion

        #region GMO決済
        /// <summary>
        /// 支払い回数
        /// </summary>
        public virtual EnumGmoMethod gmoMethod { get { return EnumGmoMethod.Single; } }

        /// <summary>
        /// 決済方法（与信/即時売り上げ）
        /// </summary>
        public virtual EnumGmoJobCd jobCode { get { return EnumGmoJobCd.AUTH; } }

        /// <summary>
        /// 取引ID
        /// </summary>
        public virtual string access_id { get; set; }

        /// <summary>
        /// 取引パスワード
        /// </summary>
        public virtual string access_pass { get; set; }

        /// <summary>
        /// 承認番号
        /// </summary>
        public virtual string approve { get; set; }

        /// <summary>
        /// 支払い方法ID(pay_tのIDと一緒)
        /// </summary>
        public virtual EnumConvCode? conv_code { get; set; }

        #endregion

        /// <summary>
        /// クレジットカード情報
        /// </summary>
        public virtual CreditCardInfo card_info { get; set; }

        /// <summary>
        /// 会員住所
        /// </summary>
        public virtual string shipping_id { get; set; }
        public virtual string lname { get; set; }
        public virtual string fname { get; set; }
        public virtual string lnamek { get; set; }
        public virtual string fnamek { get; set; }
        public virtual string compname { get; set; }
        public virtual string compnamek { get; set; }
        public virtual string division { get; set; }
        public virtual string divisionk { get; set; }
        public virtual string tlname { get; set; }
        public virtual string tfname { get; set; }
        public virtual string tlnamek { get; set; }
        public virtual string tfnamek { get; set; }
        public virtual string tel { get; set; }
        public virtual string fax { get; set; }
        public virtual string zip { get; set; }
        public virtual string address { get; set; }
        public virtual string taddress { get; set; }
        public virtual string maddress { get; set; }
        public virtual int? pref { get; set; }

        /// <summary>
        /// 入金区分（order_payment_status_t.id）
        /// </summary>
        public virtual EnumOrderPaymentStatusType order_payment_status { get; set; }

        /// <summary>
        /// 入金日
        /// </summary>
        public virtual DateTime? paid_date { get; set; }

        /// <summary>
        /// 入金額
        /// </summary>
        public virtual int paid_price { get; set; }

        /// <summary>
        /// 届け先（1 : 本人 / 2 : 別）
        /// </summary>
        public virtual EnumSendTo? send { get; protected set; }

        public virtual string add_lname { get; set; }
        public virtual string add_fname { get; set; }
        public virtual string add_lnamek { get; set; }
        public virtual string add_fnamek { get; set; }
        public virtual string add_compname { get; set; }
        public virtual string add_compnamek { get; set; }
        public virtual string add_division { get; set; }
        public virtual string add_divisionk { get; set; }
        public virtual string add_tlname { get; set; }
        public virtual string add_tfname { get; set; }
        public virtual string add_tlnamek { get; set; }
        public virtual string add_tfnamek { get; set; }
        public virtual string add_tel { get; set; }
        public virtual string add_fax { get; set; }
        public virtual string add_zip { get; set; }
        public virtual string add_address { get; set; }
        public virtual string add_taddress { get; set; }
        public virtual string add_maddress { get; set; }
        public virtual int? add_pref { get; set; }

        /// <summary>
        /// 配送希望日
        /// </summary>
        public virtual DateTime? senddate { get; set; }

        /// <summary>
        /// 配送希望時間情報
        /// </summary>
        public virtual int? sendtime { get; protected set; }

        /// <summary>
        /// Add property to set this value to regularOrderRecords Entity.
        /// </summary>
        public virtual int? next_sendtime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string ccode
        {
            get
            {
                if (string.IsNullOrEmpty(this._ccode))
                {
                    return this._ccode;
                }
                return this._ccode.ToUpper();
            }
            set
            {
                this._ccode = value;
            }
        }
        private string _ccode;

        /// <summary>
        /// ギフトラッピングフラグ
        /// </summary>
        public virtual EnumWrap? wrap { get; protected set; }

        /// <summary>
        /// ギフトラッピングメッセージ
        /// </summary>
        public virtual string memo2 { get; protected set; }

        public virtual int? member_add_id { get; set; }

        public virtual int? member_card_id { get; set; }

        public string user_id { get; set; }

        public virtual int? site_id { get; set; }

        /// <summary>
        /// 合計商品点数(同梱抜き)
        /// </summary>
        public virtual int amounttotal_non_doc_bundling
        {
            get
            {
                var ret = 0;
                foreach (var item in orderRecords.Values)
                {
                    if (item.doc_bundling_flg != EnumDocBundlingFlg.ON)
                    {
                        ret += item.GetAmount();
                    }
                }
                return ret;
            }
        }

        /// <summary>
        /// 配送方法
        /// </summary>
        public virtual EnumDelvMethod? deliv_method
        {
            get
            {
                if (this.orderRecords == null || this.orderRecords.Count == 0)
                {
                    return EnumDelvMethod.Express;
                }

                foreach (var item in orderRecords.Values)
                {
                    if (item.doc_bundling_flg == EnumDocBundlingFlg.ON)
                    {
                        //同梱は判定から除外
                        continue;
                    }

                    if (item.deliv_method == EnumDelvMethod.Express)
                    {
                        return EnumDelvMethod.Express;
                    }
                }
                return EnumDelvMethod.Mail;
            }
        }

        public bool IsAllCanceled
        {
            get
            {
                foreach (var orderRecord in this.orderRecords.Values)
                {
                    if (orderRecord.doc_bundling_flg == EnumDocBundlingFlg.OFF && !orderRecord.IsCanceled.Value)
                    {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
