﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.Payment;

namespace jp.co.ivp.ers.order
{
    public class RegularRegister
        : ErsModelBase, IPaymentInfoGmoServerContainer, IPaymentInfoGmoConvenienceContainer
    {
        //会員情報関連
        [ErsSchemaValidation("member_t.mcode")]
        public virtual string mcode { get; set; }

        [ErsSchemaValidation("member_t.lname")]
        public virtual string lname { get; set; }

        [ErsSchemaValidation("member_t.fname")]
        public virtual string fname { get; set; }

        [ErsSchemaValidation("member_t.lnamek")]
        public virtual string lnamek { get; set; }

        [ErsSchemaValidation("member_t.fnamek")]
        public virtual string fnamek { get; set; }

        [ErsSchemaValidation("member_t.compname")]
        public virtual string compname { get; set; }

        [ErsSchemaValidation("member_t.compnamek")]
        public virtual string compnamek { get; set; }

        [ErsSchemaValidation("member_t.division")]
        public virtual string division { get; set; }

        [ErsSchemaValidation("member_t.divisionk")]
        public virtual string divisionk { get; set; }
        
        [ErsSchemaValidation("member_t.tlname")]
        public virtual string tlname { get; set; }

        [ErsSchemaValidation("member_t.tfname")]
        public virtual string tfname { get; set; }
        
        [ErsSchemaValidation("member_t.tlnamek")]
        public virtual string tlnamek { get; set; }
        
        [ErsSchemaValidation("member_t.tfnamek")]
        public virtual string tfnamek { get; set; }

        [ErsSchemaValidation("member_t.email")]
        public virtual string email { get; set; }

        [ErsSchemaValidation("member_t.zip")]
        public virtual string zip { get; set; }

        [ErsSchemaValidation("member_t.pref")]
        public virtual int? pref { get; set; }

        [ErsSchemaValidation("member_t.address")]
        public virtual string address { get; set; }

        [ErsSchemaValidation("member_t.taddress")]
        public virtual string taddress { get; set; }

        [ErsSchemaValidation("member_t.maddress")]
        public virtual string maddress { get; set; }

        [ErsSchemaValidation("member_t.tel")]
        public virtual string tel { get; set; }

        [ErsSchemaValidation("member_t.fax")]
        public virtual string fax { get; set; }

        [ErsSchemaValidation("member_t.mformat")]
        public EnumMformat? mformat { get; set; }
        //会員情報関連

        [ErsSchemaValidation("addressbook_t.address_name")]
        public virtual string address_name { get; set; }

        //伝票情報関連
        [ErsSchemaValidation("d_master_t.pay")]
        public virtual EnumPaymentType? pay { get; set; }

        [ErsSchemaValidation("d_master_t.p_service")]
        public virtual int p_service { get; set; }

        [ErsSchemaValidation("d_master_t.p_service")]
        public virtual int ent_point
        {
            get
            {
                return _ent_point;
            }
            set
            {
                //入力されたポイントはp_serviceへセット
                p_service = value;
                _ent_point = value;
            }
        }
        private int _ent_point;

        [ErsSchemaValidation("d_master_t.send")]
        public virtual EnumSendTo? send { get; set; }
        
        [ErsSchemaValidation("d_master_t.memo")]
        public virtual string memo { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? member_add_id { get; set; }
     
        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 1, type = CHK_TYPE.Numeric)]
        public virtual EnumAddressAdd address_add { get; set; }

        [ErsSchemaValidation("d_master_t.add_lname")]
        public virtual string add_lname { get; set; }
        
        [ErsSchemaValidation("d_master_t.add_fname")]
        public virtual string add_fname { get; set; }

        [ErsSchemaValidation("d_master_t.add_lnamek")]
        public virtual string add_lnamek { get; set; }
        
        [ErsSchemaValidation("d_master_t.add_fnamek")]
        public virtual string add_fnamek { get; set; }
        
        [ErsSchemaValidation("d_master_t.add_compname")]
        public virtual string add_compname { get; set; }
        
        [ErsSchemaValidation("d_master_t.add_compnamek")]
        public virtual string add_compnamek { get; set; }
        
        [ErsSchemaValidation("d_master_t.add_zip")]
        public virtual string add_zip { get; set; }
        
        [ErsSchemaValidation("d_master_t.add_pref")]
        public virtual int? add_pref { get; set; }
        
        [ErsSchemaValidation("d_master_t.add_address")]
        public virtual string add_address { get; set; }
        
        [ErsSchemaValidation("d_master_t.add_taddress")]
        public virtual string add_taddress { get; set; }
        
        [ErsSchemaValidation("d_master_t.add_maddress")]
        public virtual string add_maddress { get; set; }
        
        [ErsSchemaValidation("d_master_t.add_tel")]
        public virtual string add_tel { get; set; }
        
        [ErsSchemaValidation("d_master_t.add_fax")]
        public virtual string add_fax { get; set; }
        
        [ErsSchemaValidation("d_master_t.wrap")]
        public virtual EnumWrap wrap { get; set; }

        [ErsSchemaValidation("d_master_t.memo2")]
        public virtual string memo2 { get; set; }

        [ErsSchemaValidation("d_master_t.senddate")]
        public virtual DateTime? senddate { get; set; }
        //伝票情報関連

      
        //カード情報関連
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 100)]
        public virtual string card_holder_name { get; set; }
        
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? card { get; set; }
        
        [ErsUniversalValidation(rangeFrom = 13, rangeTo = 16, type = CHK_TYPE.NumericString)]
        public virtual string cardno { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual　int? validity_y { get; set; }
 
        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 12, type = CHK_TYPE.Numeric)]
        public virtual int? validity_m { get; set; }
        
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public virtual EnumCardSave? card_save { get; set; }

        /// <summary>
        /// Holds credit card information for use in settlement
        /// </summary>
        public CreditCardInfo savedCardInfo { get; private set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual EnumConvCode? conv_code { get; set; }

        //カード情報関連



        //定期情報関連
        public virtual EnumSendPtn send_ptn { get; set; }

        [ErsSchemaValidation("regular_detail_t.mixed_group_code")]
        public virtual string mixed_group_code { get; set; }

        [ErsSchemaValidation("regular_t.next_sendtime")]
        public virtual int? sendtime { get; set; }

        public virtual int? member_card_id { get; set; }

        [ErsSchemaValidation("regular_t.weekend_operation")]
        public virtual EnumWeekendOperation? weekend_operation { get; set; }
        //定期情報関連


        /// <summary>
        /// 伝票生成対象会員の定期情報を定期購入モデルに格納
        /// </summary>
        /// <param name="serchResulItem"></param>
        public void LoadValues(Dictionary<string, object> serchResulItem, ErsMember ersMember)
        {
            //会員に関する情報取得   
            this.OverwriteWithParameter(ersMember.GetPropertiesAsDictionary());

            this.mcode = Convert.ToString(serchResulItem["mcode"]);
            this.pay = (EnumPaymentType)Convert.ToInt32(serchResulItem["pay"]);

            if (serchResulItem["member_card_id"] != null)
            {
                this.member_card_id = Convert.ToInt32(serchResulItem["member_card_id"]);
            }

            if (serchResulItem["mixed_group_code"] != null)
            {
                this.mixed_group_code = Convert.ToString(serchResulItem["mixed_group_code"]);
            }

            this.member_add_id = Convert.ToInt32(serchResulItem["member_add_id"]);

            if (this.member_add_id == 0)
            {
                this.send = EnumSendTo.MEMBER_ADDRESS;
            }
            else
            {
                this.send = EnumSendTo.ANOTHER_ADDRESS;
                //配送先に関する情報取得   
                var addressInfo = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(this.member_add_id, this.mcode);
                if (addressInfo == null)
                {
                    throw new Exception(string.Format("配送先データが見つかりません。(会員番号:{0};お届け先ID：{1})", this.mcode, this.member_add_id));
                }
                this.OverwriteWithParameter(addressInfo.GetPropertiesAsDictionary());
            }

            this.senddate = Convert.ToDateTime(serchResulItem["next_date"]);
            this.sendtime = Convert.ToInt16(serchResulItem["next_sendtime"]);
            this.weekend_operation = (EnumWeekendOperation)Convert.ToInt32(serchResulItem["weekend_operation"]);
            if (serchResulItem["conv_code"] != null)
            {
                this.conv_code = (EnumConvCode)Convert.ToInt32(serchResulItem["conv_code"]);
            }
        }
    }
}
