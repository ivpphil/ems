﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order
{
    public class ErsRegularOrderRecordRepository
        : ErsRepository<ErsRegularOrderRecord>
    {
        protected internal ErsRegularOrderRecordRepository()
            : base(new ErsDB_regular_detail_t())
        {
        }

        public override IList<ErsRegularOrderRecord> Find(Criteria criteria)
        {
            return this.Find(null, criteria);
        }

        public virtual IList<ErsRegularOrderRecord> Find(ErsRegularOrder objOrder, Criteria criteria)
        {
            var listRegulerOrderRecord = this.ersDB_table.gSelect(criteria);

            var retList = new List<ErsRegularOrderRecord>();
            foreach (var regularOrderRecordData in listRegulerOrderRecord)
            {
                var regularOrderRecord = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordWithParameter(objOrder, regularOrderRecordData);

                retList.Add(regularOrderRecord);
            }

            return retList;
        }
    }
}