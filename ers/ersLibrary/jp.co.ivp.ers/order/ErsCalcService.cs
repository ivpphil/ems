﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.basket.specification;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order.related;

namespace jp.co.ivp.ers.order
{
    public class ErsCalcService
    {

        CarriageFreeSpecification carriageFreeSpecification;
        public ErsCalcService()
        {
            carriageFreeSpecification = new CarriageFreeSpecification();
        }
        /// <summary>
        /// calculation of tax
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public virtual int calcTax(int amount)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            return (int)Math.Floor(Convert.ToDouble(amount) * (setup.tax / 100D));
        }

        /// <summary>
        /// 伝票計算（送料無料を指定できる版）
        /// </summary>
        /// <param name="order"></param>
        /// <param name="p_service"></param>
        /// <param name="carriageFreeStatus"></param>
        public virtual void calcOrder(ErsOrder order, IEnumerable<ErsOrderRecord> orderRecords, int p_service, EnumCarriageFreeStatus? carriageFreeStatus = null, bool isUpdate = false, bool carriage_recompute = false)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            // 小計
            var subtotal = 0;
            foreach (var record in orderRecords)
            {
                subtotal += record.total.Value;
            }

            if (carriageFreeStatus == null)
            {
                if (ErsFactory.ersBasketFactory.GetRegularShippingFreeSpec().IsSatisfiedBy(orderRecords)
                    || ErsFactory.ersBasketFactory.GetCarriageCostTypeFreeSpec().IsSatisfiedBy(orderRecords))
                {
                    carriageFreeStatus = EnumCarriageFreeStatus.CARRIAGE_FREE;
                }
                else
                {
                    carriageFreeStatus = ErsFactory.ersBasketFactory.GetCarriageFreeSpecification().GetCarriageFreeStatus(subtotal);
                }
            }

            var shippingAddress = ErsFactory.ersOrderFactory.GetShippingAddressInfo();
            shippingAddress.LoadShippingAddress(order);

            int tax;
            int carriage;
            int etc;

            var coupon_discount = order.coupon_discount;

            if (!isUpdate)
            {
                tax = this.calcTax(subtotal);

                carriage = CalclateCarriage(orderRecords, carriageFreeStatus, shippingAddress);

                //送料の税計算
                if (setup.enable_carriage_tax == EnumOnOff.On)
                {
                    tax += this.calcTax(carriage);
                }

                if (p_service + coupon_discount < subtotal + tax + carriage) // do not calculate fee when payment is not needed
                {
                    etc = ErsFactory.ersOrderFactory.GetObtainEtcAmountStgy().Obtain(order.pay, subtotal + tax, order.site_id);

                    //手数料の税計算
                    var objPay = ErsFactory.ersOrderFactory.GetErsPayWithIdAndSiteId(order.pay, order.site_id);
                    if (objPay != null && objPay.enable_tax == EnumOnOff.On)
                    {
                        tax += this.calcTax(etc);
                    }
                }
                else
                {
                    etc = 0;
                }
            }
            else
            {
                tax = order.tax;
                if (carriage_recompute)
                {
                    tax -= this.calcTax(order.carriage);
                    carriage = CalclateCarriage(orderRecords, carriageFreeStatus, shippingAddress);
                    //送料の税計算
                    if (setup.enable_carriage_tax == EnumOnOff.On)
                    {
                        tax += this.calcTax(carriage);
                    }
                }
                else
                {
                    carriage = order.carriage;
                }
                etc = order.etc;
            }

            // 合計
            var pre_total = subtotal + tax + carriage + etc - coupon_discount;
            if (p_service != 0 && p_service > pre_total)
            {
                p_service = pre_total;
            }

            var total = pre_total - p_service - coupon_discount;

            order.subtotal = subtotal;
            order.tax = tax;
            order.p_service = p_service;
            order.carriage = carriage;
            order.etc = etc;
            order.total = total;
            order.coupon_discount = coupon_discount;
        }

        private static int CalclateCarriage(IEnumerable<ErsOrderRecord> orderRecords, EnumCarriageFreeStatus? carriageFreeStatus, Send.ShippingAddressInfo shippingAddress)
        {
            int carriage;
            var isMailDelivery = ErsFactory.ersOrderFactory.GetIsAllBasketItemMailDeliverySpec().Satisfy(orderRecords);
            carriage = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetCarriageFromId(shippingAddress.shipping_pref, carriageFreeStatus, isMailDelivery);
            return carriage;
        }

        /// <summary>
        /// 伝票明細計算
        /// </summary>
        /// <param name="eo">ErsOrderRecord インスタンス</param>
        protected internal virtual void calcOrderRecord(ErsOrderRecord record)
        {
            try
            {
                // 合計 = 単価 * ( 個数 - キャンセル個数 )
                record.total = record.price * (record.GetAmount());
            }
            catch (OverflowException)
            {
                throw new ErsException("10051", String.Format("{0:#,0}", int.MaxValue));
            }
        }

        /// <summary>
        /// 伝票明細計算
        /// </summary>
        /// <param name="eo">ErsOrderRecord インスタンス</param>
        protected internal virtual void calcBaskRecord(ErsBaskRecord record, int amount)
        {
            try
            {
                record.amount = amount;
                record.total = amount * record.price.Value;
            }
            catch (OverflowException)
            {
                throw new ErsException("10051", String.Format("{0:#,0}", int.MaxValue));
            }
        }

        /// <summary>
        /// カート計算
        /// </summary>
        /// <param name="eb">ErsBasket インスタンス</param>
        public virtual void calcBasket(ErsBasket eb)
        {
            try
            {
                // 小計
                eb.subtotal = 0;
                foreach (var ebr in eb.objBasketRecord.Values)
                {
                    eb.subtotal += ebr.total;
                }

                // 消費税
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                eb.tax = this.calcTax(eb.subtotal);

                // 合計
                eb.total = eb.subtotal + eb.tax;

                // 小計
                eb.regular_subtotal = 0;
                foreach (var ebr in eb.objRegularBasketRecord.Values)
                {
                    eb.regular_subtotal += ebr.total;
                }

                // 消費税
                eb.regular_tax = this.calcTax(eb.regular_subtotal);

                // 合計
                eb.regular_total = eb.regular_subtotal + eb.regular_tax;

                // 2回目以降小計
                eb.regular_subtotal_next = 0;
                foreach (var ebr in eb.objRegularBasketRecord.Values)
                {
                    eb.regular_subtotal_next += ebr.regular_total.Value;
                }

                // 2回目以降消費税
                eb.regular_tax_next = this.calcTax(eb.regular_subtotal_next);

                // 2回目以降合計
                eb.regular_total_next = eb.regular_subtotal_next + eb.regular_tax_next;
            
            }
            catch (OverflowException)
            {
                throw new ErsException("10051", String.Format("{0:#,0}", int.MaxValue));
            }
        }

        public void calcOrderTotal(ErsOrder order)
        {
            var p_service = order.p_service;
            var subtotal = order.subtotal;
            var carriage = order.carriage;
            var etc = order.etc;
            var tax = order.tax;
            var coupon_discount = order.coupon_discount;

            // 合計
            var pre_total = subtotal + tax + carriage + etc;
            if (p_service != 0 && p_service > pre_total)
            {
                p_service = pre_total;
            }

            pre_total = pre_total - p_service;

            if(coupon_discount != 0 && coupon_discount > pre_total)
            {
                coupon_discount = pre_total;
            }

            var total = pre_total - coupon_discount;

            order.subtotal = subtotal;
            order.tax = tax;
            order.p_service = p_service;
            order.carriage = carriage;
            order.etc = etc;
            order.total = total;
            order.coupon_discount = coupon_discount;
        }
    }
}
