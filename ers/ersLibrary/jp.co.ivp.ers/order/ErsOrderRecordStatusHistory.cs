﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.Payment;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderRecordStatusHistory
        : ErsRepositoryEntity
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 操作ユーザID [Log user ID]
        /// </summary>
        public virtual string log_user_id { get; set; }

        /// <summary>
        /// 操作日 [Operation date]
        /// </summary>
        public virtual DateTime? tdate { get; set; }

        /// <summary>
        /// 受注日 [Header insert time]
        /// </summary>
        public virtual DateTime? d_intime { get; set; }

        /// <summary>
        /// 伝票番号 [Order no]
        /// </summary>
        public virtual string d_no { get; set; }

        /// <summary>
        /// 明細ID [Record ID]
        /// </summary>
        public virtual int? ds_id { get; set; }

        /// <summary>
        /// 変更前伝票ステータス [Old header order status]
        /// </summary>
        public virtual EnumOrderStatusType? old_d_status { get; set; }

        /// <summary>
        /// 変更後伝票ステータス [New header order status]
        /// </summary>
        public virtual EnumOrderStatusType? new_d_status { get; set; }

        /// <summary>
        /// 変更前販売ステータス [Old order status]
        /// </summary>
        public virtual EnumOrderStatusType? old_order_status { get; set; }

        /// <summary>
        /// 変更後販売ステータス [New order status]
        /// </summary>
        public virtual EnumOrderStatusType? new_order_status { get; set; }

        /// <summary>
        /// 変更前ヘッダ：小計 [Old header subtotal]
        /// </summary>
        public virtual int? old_d_subtotal { get; set; }

        /// <summary>
        /// 変更前ヘッダ：消費税 [Old header tax]
        /// </summary>
        public virtual int? old_d_tax { get; set; }

        /// <summary>
        /// 変更前ヘッダ：送料 [Old header carriage]
        /// </summary>
        public virtual int? old_d_carriage { get; set; }

        /// <summary>
        /// 変更前ヘッダ：ポイント値引き [Old header p_service]
        /// </summary>
        public virtual int? old_d_p_service { get; set; }

        /// <summary>
        /// 変更前ヘッダ：クーポン値引き [Old header coupon_discount]
        /// </summary>
        public virtual int? old_d_coupon_discount { get; set; }

        /// <summary>
        /// 変更前ヘッダ：手数料 [Old header etc]
        /// </summary>
        public virtual int? old_d_etc { get; set; }

        /// <summary>
        /// 変更前ヘッダ：合計 [Old header total]
        /// </summary>
        public virtual int? old_d_total { get; set; }

        /// <summary>
        /// 変更前明細：単価 [Old record price]
        /// </summary>
        public virtual int? old_ds_price { get; set; }

        /// <summary>
        /// 変更前明細：数量 [old record amount]
        /// </summary>
        public virtual int? old_ds_amount { get; set; }

        /// <summary>
        /// 変更前明細：キャンセル数量 [Old record cancel amount]
        /// </summary>
        public virtual int? old_ds_cancel_amount { get; set; }

        /// <summary>
        /// 変更前明細：返品数量 [Old record after cancel amount]
        /// </summary>
        public virtual int? old_ds_after_cancel_amount { get; set; }

        /// <summary>
        /// 変更前明細：合計 [Old record total]
        /// </summary>
        public virtual int? old_ds_total { get; set; }

        /// <summary>
        /// 変更後ヘッダ：小計 [New header subtotal]
        /// </summary>
        public virtual int? new_d_subtotal { get; set; }

        /// <summary>
        /// 変更後ヘッダ：消費税 [New header tax]
        /// </summary>
        public virtual int? new_d_tax { get; set; }

        /// <summary>
        /// 変更後ヘッダ：送料 [New header carriage]
        /// </summary>
        public virtual int? new_d_carriage { get; set; }

        /// <summary>
        /// 変更後ヘッダ：ポイント値引き [New header point service]
        /// </summary>
        public virtual int? new_d_p_service { get; set; }

        /// <summary>
        /// 変更後ヘッダ：クーポン値引き [New header coupon discount]
        /// </summary>
        public virtual int? new_d_coupon_discount { get; set; }

        /// <summary>
        /// 変更後ヘッダ：手数料 [New header etc]
        /// </summary>
        public virtual int? new_d_etc { get; set; }

        /// <summary>
        /// 変更後ヘッダ：合計 [New header total]
        /// </summary>
        public virtual int? new_d_total { get; set; }

        /// <summary>
        /// 変更後明細：単価 [New record price]
        /// </summary>
        public virtual int? new_ds_price { get; set; }

        /// <summary>
        /// 変更後明細：数量 [New record amount]
        /// </summary>
        public virtual int? new_ds_amount { get; set; }

        /// <summary>
        /// 変更後明細：キャンセル数量 [New record cancel amount]
        /// </summary>
        public virtual int? new_ds_cancel_amount { get; set; }

        /// <summary>
        /// 変更後明細：返品数量 [New record after cancel amount]
        /// </summary>
        public virtual int? new_ds_after_cancel_amount { get; set; }

        /// <summary>
        /// 変更後明細：合計 [New record total]
        /// </summary>
        public virtual int? new_ds_total { get; set; }

        /// <summary>
        /// 登録日 [Insert time]
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新日 [Update time]
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// アクティブフラグ [Active flag]
        /// </summary>
        public virtual EnumActive? active { get; set; }
    }
}
