﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderMailCriteria
        : Criteria
    {
        public string d_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_mail_t.d_no", value, Operation.EQUAL));
            }
        }

        public int? ds_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_mail_t.ds_id", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_mail_t.active", value, Operation.EQUAL));
            }
        }
    }
}
