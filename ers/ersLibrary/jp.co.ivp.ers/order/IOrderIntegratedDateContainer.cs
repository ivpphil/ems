﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.Payment;

namespace jp.co.ivp.ers.order
{
    public interface IOrderIntegratedDateContainer
        : IErsModelBase
    {
        DateTime? senddate { get; set; }

        int? sendtime { get; set; }

        int? regular_sendtime { get; set; }

        int? p_service { get; set; }

        EnumPaymentType? pay { get; set; }

        string ent_coupon_code { get; set; }
    }
}
