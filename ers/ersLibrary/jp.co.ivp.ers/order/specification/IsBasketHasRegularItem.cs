﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.specification
{
    public class IsBasketHasRegularItem
    {
        /// <summary>
        /// バスケットの中に定期がある場合True
        /// </summary>
        /// <param name="sendDate"></param>
        /// <param name="strRansu"></param>
        /// <returns></returns>
        public Boolean Satisfy(string strRansu)
        {
            var baskRegCriteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
            var baskRegRep = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            baskRegCriteria.ransu = strRansu;
            baskRegCriteria.order_type = EnumOrderType.Subscription;　//定期製品

            var baskRegular = baskRegRep.Find(baskRegCriteria);

            return (baskRegular.Count != 0);
        }
    }
}
