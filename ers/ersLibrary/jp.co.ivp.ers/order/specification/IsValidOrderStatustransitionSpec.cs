﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order.specification
{
    public class IsValidOrderStatustransitionSpec
    {
        public enum validResult
        {
            Valid,
            Invalid,
            BeforeDeliverReturn
        }

        /// <summary>
        /// 指定された明細を指定されたステータスに更新できるかを判定します。
        /// </summary>
        /// <param name="beforeStatus"></param>
        /// <param name="afterStatus"></param>
        /// <returns></returns>
        public validResult Determine(EnumOrderStatusType? beforeStatus, EnumOrderStatusType? afterStatus, DateTime? ship_date)
        {
            if (!beforeStatus.HasValue)
            {
                return validResult.Valid;
            }

            if (ErsFactory.ersOrderFactory.GetIsBeforeDeliverSpec().Determine(beforeStatus.Value, ship_date) &&
                (afterStatus == EnumOrderStatusType.CANCELED_AFTER_DELIVER))
            {
                return validResult.BeforeDeliverReturn;
            }

            if (!afterStatus.HasValue)
            {
                return validResult.Valid;
            }

            var result = mapping.Value[beforeStatus.Value][afterStatus.Value];

            return result ? validResult.Valid : validResult.Invalid;
        }

        /// <summary>
        /// Singletonなマッピングクラス
        /// </summary>
        private static Lazy<Dictionary<EnumOrderStatusType, Dictionary<EnumOrderStatusType, bool>>> mapping =
            new Lazy<Dictionary<EnumOrderStatusType, Dictionary<EnumOrderStatusType, bool>>>(() => GetTransitionMapping());

        /// <summary>
        /// マッピングクラスの初期化を行う
        /// </summary>
        /// <returns></returns>
        private static Dictionary<EnumOrderStatusType, Dictionary<EnumOrderStatusType, bool>> GetTransitionMapping()
        {
            var resultMapping = new Dictionary<EnumOrderStatusType, Dictionary<EnumOrderStatusType, bool>>();

            foreach (EnumOrderStatusType orderStatus in Enum.GetValues(typeof(EnumOrderStatusType)))
            {
                resultMapping[orderStatus] = new Dictionary<EnumOrderStatusType, bool>();
            }

            //変更前：新着注文
            resultMapping[EnumOrderStatusType.NEW_ORDER][EnumOrderStatusType.NEW_ORDER] = true;
            resultMapping[EnumOrderStatusType.NEW_ORDER][EnumOrderStatusType.DELIVER_WAITING] = true;
            resultMapping[EnumOrderStatusType.NEW_ORDER][EnumOrderStatusType.DELIVER_REQUEST] = true;
            resultMapping[EnumOrderStatusType.NEW_ORDER][EnumOrderStatusType.DELIVERED] = true;
            resultMapping[EnumOrderStatusType.NEW_ORDER][EnumOrderStatusType.CANCELED] = true;
            resultMapping[EnumOrderStatusType.NEW_ORDER][EnumOrderStatusType.CANCELED_AFTER_DELIVER] = false;

            //変更前：出荷待機
            resultMapping[EnumOrderStatusType.DELIVER_WAITING][EnumOrderStatusType.NEW_ORDER] = true;
            resultMapping[EnumOrderStatusType.DELIVER_WAITING][EnumOrderStatusType.DELIVER_WAITING] = true;
            resultMapping[EnumOrderStatusType.DELIVER_WAITING][EnumOrderStatusType.DELIVER_REQUEST] = true;
            resultMapping[EnumOrderStatusType.DELIVER_WAITING][EnumOrderStatusType.DELIVERED] = true;
            resultMapping[EnumOrderStatusType.DELIVER_WAITING][EnumOrderStatusType.CANCELED] = true;
            resultMapping[EnumOrderStatusType.DELIVER_WAITING][EnumOrderStatusType.CANCELED_AFTER_DELIVER] = false;

            //変更前：出荷指示済
            resultMapping[EnumOrderStatusType.DELIVER_REQUEST][EnumOrderStatusType.NEW_ORDER] = true;
            resultMapping[EnumOrderStatusType.DELIVER_REQUEST][EnumOrderStatusType.DELIVER_WAITING] = true;
            resultMapping[EnumOrderStatusType.DELIVER_REQUEST][EnumOrderStatusType.DELIVER_REQUEST] = true;
            resultMapping[EnumOrderStatusType.DELIVER_REQUEST][EnumOrderStatusType.DELIVERED] = true;
            resultMapping[EnumOrderStatusType.DELIVER_REQUEST][EnumOrderStatusType.CANCELED] = true;
            resultMapping[EnumOrderStatusType.DELIVER_REQUEST][EnumOrderStatusType.CANCELED_AFTER_DELIVER] = false;

            //変更前：配送済
            resultMapping[EnumOrderStatusType.DELIVERED][EnumOrderStatusType.NEW_ORDER] = false;
            resultMapping[EnumOrderStatusType.DELIVERED][EnumOrderStatusType.DELIVER_WAITING] = false;
            resultMapping[EnumOrderStatusType.DELIVERED][EnumOrderStatusType.DELIVER_REQUEST] = false;
            resultMapping[EnumOrderStatusType.DELIVERED][EnumOrderStatusType.DELIVERED] = true;
            resultMapping[EnumOrderStatusType.DELIVERED][EnumOrderStatusType.CANCELED] = true;
            resultMapping[EnumOrderStatusType.DELIVERED][EnumOrderStatusType.CANCELED_AFTER_DELIVER] = true;

            //変更前：キャンセル
            resultMapping[EnumOrderStatusType.CANCELED][EnumOrderStatusType.NEW_ORDER] = false;
            resultMapping[EnumOrderStatusType.CANCELED][EnumOrderStatusType.DELIVER_WAITING] = false;
            resultMapping[EnumOrderStatusType.CANCELED][EnumOrderStatusType.DELIVER_REQUEST] = false;
            resultMapping[EnumOrderStatusType.CANCELED][EnumOrderStatusType.DELIVERED] = false;
            resultMapping[EnumOrderStatusType.CANCELED][EnumOrderStatusType.CANCELED] = true;
            resultMapping[EnumOrderStatusType.CANCELED][EnumOrderStatusType.CANCELED_AFTER_DELIVER] = true;

            //変更前：返品
            resultMapping[EnumOrderStatusType.CANCELED_AFTER_DELIVER][EnumOrderStatusType.NEW_ORDER] = false;
            resultMapping[EnumOrderStatusType.CANCELED_AFTER_DELIVER][EnumOrderStatusType.DELIVER_WAITING] = false;
            resultMapping[EnumOrderStatusType.CANCELED_AFTER_DELIVER][EnumOrderStatusType.DELIVER_REQUEST] = false;
            resultMapping[EnumOrderStatusType.CANCELED_AFTER_DELIVER][EnumOrderStatusType.DELIVERED] = false;
            resultMapping[EnumOrderStatusType.CANCELED_AFTER_DELIVER][EnumOrderStatusType.CANCELED] = false;
            resultMapping[EnumOrderStatusType.CANCELED_AFTER_DELIVER][EnumOrderStatusType.CANCELED_AFTER_DELIVER] = true;

            return resultMapping;
        }
    }
}
