﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order.specification
{
    public class ProductRankingSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return " SELECT ds_master_t.gcode as gcode, SUM(amount) AS sum_result ,d_master_t.site_id as site_id "
                    + " FROM ds_master_t "
                    + " INNER JOIN s_master_t ON s_master_t.scode = ds_master_t.scode "
                    + " INNER JOIN d_master_t ON d_master_t.d_no = ds_master_t.d_no "
                    + " INNER JOIN g_master_t ON g_master_t.gcode = s_master_t.gcode ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(ds_master_t.id) as " + countColumnAlias
                    + " FROM ds_master_t "
                    + " INNER JOIN s_master_t ON s_master_t.scode = ds_master_t.scode "
                    + " INNER JOIN d_master_t ON d_master_t.d_no = ds_master_t.d_no "
                    + " INNER JOIN g_master_t ON g_master_t.gcode = s_master_t.gcode ";
        }
    }
}
