﻿using System;
using System.Data;
using System.Collections.Generic;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.specification
{
    public class OrderSearchSpecification
        : SearchSpecificationBase
    {
        /// <summary>
        /// 検索データ取得
        /// </summary>
        /// <param name="crtOrder">クライテリア</param>
        /// <returns>データテーブル</returns>
        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            criteria.AddOrderBy("d_master_t.intime", Criteria.OrderBy.ORDER_BY_DESC);

            return base.GetSearchData(criteria);
        }

        protected override string GetSearchDataSql()
        {
            return string.Format(@" SELECT DISTINCT ON (d_master_t.d_no, d_master_t.intime) *, af_cancel.tdate AS after_cancel_date, cancel.tdate AS cancel_date
                FROM d_master_t 
                INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no 
                LEFT JOIN ds_status_history_t AS af_cancel ON ds_master_t.id = af_cancel.ds_id AND af_cancel.new_order_status = {0} 
                LEFT JOIN ds_status_history_t AS cancel ON ds_master_t.id = cancel.ds_id AND cancel.new_order_status = {1} "
                , (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER
                , (int)EnumOrderStatusType.CANCELED);
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return string.Format(@" SELECT COUNT(DISTINCT d_master_t.d_no) AS count 
                FROM d_master_t 
                INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no 
                LEFT JOIN ds_status_history_t AS af_cancel ON ds_master_t.id = af_cancel.ds_id AND af_cancel.new_order_status = {0} 
                LEFT JOIN ds_status_history_t AS cancel ON ds_master_t.id = cancel.ds_id AND cancel.new_order_status = {1} "
                , (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER
                , (int)EnumOrderStatusType.CANCELED);
        }
    }

}