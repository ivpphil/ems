﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.order.specification
{
    public class IsNonNeededPaymentSpec
    {
        public bool IsSpecified(int? total)
        {
            return (total == 0);
        }
    }
}
