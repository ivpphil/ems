﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.specification
{
    public class IsMonitorSpec
    {
        /// <summary>
        /// judge whether this purchase is Monitoring
        /// </summary>
        /// <param name="basket"></param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        public virtual bool IsSpecified(ErsBasket basket, IIsMonitorSpecDatasource datasource)
        {
            //judge using an basket data
            if (basket == null)
            {
                return false;
            }

            if (ErsFactory.ersOrderFactory.GetHasMonitorMerchandiseSpec().IsSpecified(basket))
            {
                return true;
            }

            //judge using an member data
            if (datasource == null)
            {
                return false;
            }

            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var monitorFirstLname = setup.monitorFirstLname;
            var monitorFirstFname = setup.monitorFirstFname;
            var monitorSecondLname = setup.monitorSecondLname;
            var monitorSecondFname = setup.monitorSecondFname;

            if (datasource.lname == monitorFirstLname && datasource.fname == monitorFirstFname)
            {
                return true;
            }
            if (datasource.lname == monitorSecondLname && datasource.fname == monitorSecondFname)
            {
                return true;
            }

            return false;
        }
    }
}
