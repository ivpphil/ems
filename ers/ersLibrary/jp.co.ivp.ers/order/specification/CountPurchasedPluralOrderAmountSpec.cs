﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order.specification
{
    public class CountPurchasedPluralOrderAmountSpec
        : ISpecificationForSQL
    {
        public int GetCountData(string scode, string mcode, string d_no, int? site_id)
        {
            this.scode = scode;
            this.mcode = mcode;
            this.d_no = d_no;
            this.site_id = site_id;

            var result = ErsRepository.SelectSatisfying(this);

            return Convert.ToInt32(result.First()["result"]);
        }

        public string asSQL()
        {
            var sql = @"
                SELECT
                    COALESCE(SUM(ds_master_t.amount), 0) AS result
                FROM
                    d_master_t
                INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no
		        WHERE d_master_t.mcode = '" + mcode + @"' AND ds_master_t.scode = '" + this.scode + @"'
                    AND d_master_t.site_id = " + this.site_id + 
                    "AND ds_master_t.order_status NOT IN (" + string.Join(",", ErsOrderCriteria.CANCEL_STATUS_ARRAY.Cast<int>()) + @")
                ";

            if (this.d_no.HasValue())
            {
                sql += " AND d_master_t.d_no <> '" + this.d_no + "'";
            }

            return sql;
        }

        public string scode { get; set; }

        public string mcode { get; set; }

        public string d_no { get; set; }

        public int? site_id { get; set; }
    }
}
