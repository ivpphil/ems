﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order.specification
{
    public class IsPaymentInfoChangableSpec
    {
        public bool IsChangable(ErsOrder objOrder)
        {
            if (objOrder == null || !objOrder.pay.HasValue)
            {
                return false;
            }

            return (objOrder.pay == EnumPaymentType.BANK);
        }
    }
}
