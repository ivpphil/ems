﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.order.specification
{
    public class HasMonitorMerchandiseSpec
    {
        public virtual bool IsSpecified(ErsBasket basket)
        {
            if (basket == null)
            {
                throw new ArgumentNullException("basket");
            }

            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var arrGcode = setup.IgnoreGcode;

            foreach (var basketRecord in basket.objBasketRecord.Values)
            {
                if (arrGcode.Contains(basketRecord.gcode))
                {
                    return true;
                }
            }

            return false;
        }

        public virtual bool IsSpecified(string ransu)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var arrGcode = setup.IgnoreGcode;
            var rep = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            var cri = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();

            cri.ransu = ransu;

            var list = rep.Find(cri);

            foreach (var basketRecord in list)
            {
                if (arrGcode.Contains(basketRecord.gcode))
                {
                    return true;
                }
            }

            return false;
        }

    }
}
