﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.specification
{
    public class ObtainOrderSumAmount
        : ISpecificationForSQL
    {
        public virtual int? ObtainOrderRecordSumAmount(Criteria criteria)
        {
            var record = ErsRepository.SelectSatisfying(this, criteria);

            return (int?)Convert.ToInt32(record[0]["sum_amount"]);
        }
        public string asSQL()
        {
            return @"SELECT SUM(ds_master_t.amount) as sum_amount FROM ds_master_t 
                     INNER JOIN d_master_t ON d_master_t.d_no = ds_master_t.d_no ";
        }
    }
}
