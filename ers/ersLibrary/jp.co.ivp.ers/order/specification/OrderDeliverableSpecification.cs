﻿using System;
using System.Data;
using System.Collections.Generic;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.specification
{
    public class OrderDeliverableSpecification
    {
        public virtual bool IsSatisfiedBy(ErsOrder order, IEnumerable<ErsOrderRecord> orderRecords)
        {
            // 新着注文 かつ 未入金以外

            if (order.order_payment_status == EnumOrderPaymentStatusType.NOT_PAID)
                return false;

            var order_status = ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy().ObtainFrom(orderRecords);
            if (order_status != EnumOrderStatusType.NEW_ORDER)
                return false;

            return true;
        }
    }
}