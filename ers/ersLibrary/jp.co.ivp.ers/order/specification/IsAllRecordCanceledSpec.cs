﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order.specification
{
    public class IsAllRecordCanceledSpec
    {
        public bool IsAllCanceled(IEnumerable<ErsOrderRecord> orderRecords)
        {
            foreach (var orderRecord in orderRecords)
            {
                if (orderRecord.doc_bundling_flg == EnumDocBundlingFlg.OFF && !orderRecord.IsCanceled.Value)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
