﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order.specification
{
    public class IsBeforeDeliverSpec
    {
        /// <summary>
        /// 出荷前の伝票のステータスかどうかを判定します
        /// </summary>
        /// <param name="beforeStatus"></param>
        /// <param name="afterStatus"></param>
        /// <param name="ship_date"></param>
        /// <returns></returns>
        public bool Determine(EnumOrderStatusType beforeStatus, DateTime? ship_date)
        {
            switch (beforeStatus)
            {
                case EnumOrderStatusType.NEW_ORDER:
                case EnumOrderStatusType.DELIVER_WAITING:
                case EnumOrderStatusType.DELIVER_REQUEST:
                    return true;
                case EnumOrderStatusType.DELIVERED:
                case EnumOrderStatusType.CANCELED_AFTER_DELIVER:
                    return false;
                case EnumOrderStatusType.CANCELED:
                    return !ship_date.HasValue;
            }

            throw new Exception(beforeStatus + " is not defined in this method.");
        }
    }
}
