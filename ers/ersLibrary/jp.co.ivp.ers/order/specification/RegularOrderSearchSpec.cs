﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order.specification
{
    public class RegularOrderSearchSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return " SELECT DISTINCT regular_t.* FROM regular_t INNER JOIN regular_detail_t ON regular_t.id = regular_detail_t.regular_id ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(DISTINCT regular_t.id) AS count FROM regular_t INNER JOIN regular_detail_t ON regular_t.id = regular_detail_t.regular_id ";
        }
    }
}