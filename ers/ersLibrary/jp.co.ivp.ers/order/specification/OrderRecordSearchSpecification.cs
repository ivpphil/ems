﻿using System;
using System.Data;
using System.Collections.Generic;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using ers.jp.co.ivp.ers;

namespace jp.co.ivp.ers.order.specification
{
    public class OrderRecordSearchSpecification
        : SearchSpecificationBase
    {
        /// <summary>
        /// 検索データ取得
        /// </summary>
        /// <param name="crtOrder">クライテリア</param>
        /// <returns>データテーブル</returns>
        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return this.GetSearchData(criteria, false);
        }

        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria, bool ignore_site_flg)
        {
            return base.GetSearchData(criteria);
        }

        protected override string GetSearchDataSql()
        {
            return string.Format(@" SELECT ds_master_t.*, af_cancel.tdate AS after_cancel_date, cancel.tdate AS cancel_date, c_price.cancel_price AS cancel_price, g.doc_bundling_flg AS g_doc_bundling_flg
                FROM ds_master_t 
                LEFT JOIN g_master_t AS g ON ds_master_t.gcode = g.gcode
                LEFT JOIN ds_status_history_t AS af_cancel ON ds_master_t.id = af_cancel.ds_id AND af_cancel.new_order_status = {0} 
                LEFT JOIN ds_status_history_t AS cancel ON ds_master_t.id = cancel.ds_id AND cancel.new_order_status = {1} 
                LEFT JOIN (select d_no, case when sum( old_d_total - new_d_total) > max(old_d_total )  then sum( old_d_total - new_d_total) - max(old_d_total )  else sum( old_d_total - new_d_total) end  as cancel_price from ds_status_history_t where new_order_status in ({2},{3}) group by d_no ) AS c_price ON ds_master_t.d_no = c_price.d_no "
                , (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER
                , (int)EnumOrderStatusType.CANCELED
                , (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER
                , (int)EnumOrderStatusType.CANCELED);
        }

        public override int GetCountData(Criteria criteria)
        {
            return base.GetCountData(criteria);
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return string.Format(@" SELECT COUNT(DISTINCT ds_master_t.id) AS count 
                FROM ds_master_t 
                LEFT JOIN ds_status_history_t AS af_cancel ON ds_master_t.id = af_cancel.ds_id AND af_cancel.new_order_status = {0} 
                LEFT JOIN ds_status_history_t AS cancel ON ds_master_t.id = cancel.ds_id AND cancel.new_order_status = {1} 
                LEFT JOIN ds_status_history_t AS c_ptice ON ds_master_t.id = c_ptice.ds_id AND c_ptice.old_d_total <> 0 AND c_ptice.new_d_total = 0 "
                , (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER
                , (int)EnumOrderStatusType.CANCELED);
        }
    }

}