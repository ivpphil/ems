﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order.specification
{
    public interface IIsMonitorSpecDatasource
    {
        string lname { get; }
        string fname { get; }
    }
}
