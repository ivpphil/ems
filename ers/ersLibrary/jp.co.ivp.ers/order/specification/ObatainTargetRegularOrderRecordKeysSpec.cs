﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.specification
{
    public class ObatainTargetRegularOrderRecordKeysSpec
              : ISpecificationForSQL
    {
        public virtual List<int> ObtainRecordKeys(Criteria criteria)
        {
            var list = ErsRepository.SelectSatisfying(this, criteria);

            List<int> retList = new List<int>();

            foreach (var val in list)
            {
                retList.Add(int.Parse(val["id"].ToString()));
            }

            return retList;
        }

        /// <summary>
        /// SQL文
        /// </summary>
        /// <returns>SQL文</returns>
        public virtual string asSQL()
        {
            string sql = "SELECT regular_detail_t.id ";
            sql += " FROM regular_detail_t ";

            return sql;
        }
    }
}
