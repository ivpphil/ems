﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.specification
{
    public class IsAllBasketItemMailDeliverySpec
    {
        /// <summary>
        /// カート内が全件メール便かの判定
        /// </summary>
        /// <param name="strRansu"></param>
        /// <returns></returns>
        public Boolean Satisfy(string strRansu)
        {
            var baskCriteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
            var baskRep = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();

            baskCriteria.ransu = strRansu;
            baskCriteria.amount_not_equals = 0;

            var listBaskRecord = baskRep.Find(baskCriteria);

            int mailCount = 0;
            foreach (var record in listBaskRecord)
            {
                if (record.deliv_method == EnumDelvMethod.Mail && record.amount > 0)
                {
                    mailCount++;
                }
            }

            //カートの全件数
            return (mailCount > 0 && listBaskRecord.Count == mailCount);
        }

        /// <summary>
        /// カート内が全件メール便かの判定
        /// </summary>
        /// <param name="strRansu"></param>
        /// <returns></returns>
        public Boolean Satisfy(IEnumerable<ErsOrderRecord> orderRecords)
        {
            int mailCount = 0;
            foreach (var record in orderRecords)
            {
                if (record.deliv_method == EnumDelvMethod.Mail && record.amount > 0)
                {
                    mailCount++;
                }
            }

            //カートの全件数
            return (mailCount > 0 && orderRecords.Count() == mailCount);
        }
    }
}
