﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order
{
    public class ObatainTargetRegularOrderSpec
       : ISpecificationForSQL
    {
        public virtual List<Dictionary<string, object>> Obtain(Criteria criteria)
        {
            return ErsRepository.SelectSatisfying(this, criteria);
        }

        /// <summary>
        /// SQL文
        /// </summary>
        /// <returns>SQL文</returns>
        public virtual string asSQL()
        {
            string sql = @"
                SELECT
                    regular_detail_t.mcode,
                    regular_detail_t.pay, 
                    regular_detail_t.member_card_id, 
                    regular_detail_t.mixed_group_code, 
                    regular_detail_t.member_add_id, 
                    regular_detail_t.next_date, 
                    regular_detail_t.next_sendtime, 
                    regular_detail_t.weekend_operation, 
                    regular_detail_t.conv_code, 
                    regular_detail_t.site_id, 
                    member_card_t.card_mcode 
                FROM regular_detail_t
                LEFT JOIN member_card_t ON member_card_t.id = regular_detail_t.member_card_id";
            return sql;
        }
    }
}