﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderContainer
    {
        public ErsOrder OrderHeader { get; set; }

        public IDictionary<string, ErsOrderRecord> OrderRecords { get; set; }
    }
}
