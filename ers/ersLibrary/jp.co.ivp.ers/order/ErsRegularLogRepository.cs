﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order
{
    public class ErsRegularLogRepository
        : ErsRepository<ErsRegularLog>
    {
        public ErsRegularLogRepository()
            : base("regular_log_t")
        {
        }

        public override IList<ErsRegularLog> Find(db.Criteria criteria)
        {
            var retList = new List<ErsRegularLog>();

            var list = this.ersDB_table.gSelect(criteria);

            if (list.Count == 0)
                return new List<ErsRegularLog>();

            foreach (var dr in list)
            {
                var stepMail = ErsFactory.ersOrderFactory.GetErsRegularLogWithParameters(dr);
                retList.Add(stepMail);
            }
            return retList;
        }
    }
}
