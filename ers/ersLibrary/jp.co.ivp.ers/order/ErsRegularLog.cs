﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order
{
    public class ErsRegularLog : ErsRepositoryEntity
    {

        public override int?  id { get; set; }
        public virtual int? regular_detail_id { get; set; }
        public virtual string  scode { get; set; }
        public virtual int?  amount { get; set; }
        public virtual int?  cancel_amount { get; set; }
        public virtual int?  min_delivery_days { get; set; }
        public virtual EnumSendPtn  send_ptn { get; set; }
        public virtual int?  ptn_interval_month { get; set; }
        public virtual int?  ptn_day { get; set; }
        public virtual int?  ptn_interval_week { get; set; }
        public virtual DayOfWeek? ptn_weekday { get; set; }
        public virtual int?  ptn_interval_day { get; set; }

        public virtual DateTime? last_date_base { get; set; }
        public virtual DateTime? next_date_base { get; set; }
        public virtual DateTime? next2_date_base { get; set; }
        public virtual DateTime? next3_date_base { get; set; }
        public virtual DateTime? next4_date_base { get; set; }
        public virtual DateTime? next5_date_base { get; set; }

        public virtual DateTime? last_date { get; set; }
        public virtual DateTime? next_date { get; set; }
        public virtual DateTime? next2_date { get; set; }
        public virtual DateTime? next3_date { get; set; }
        public virtual DateTime? next4_date { get; set; }
        public virtual DateTime? next5_date { get; set; }

        public virtual int? last_sendtime_base { get; set; }
        public virtual int? next_sendtime_base { get; set; }
        public virtual int? next2_sendtime_base { get; set; }
        public virtual int? next3_sendtime_base { get; set; }
        public virtual int? next4_sendtime_base { get; set; }
        public virtual int? next5_sendtime_base { get; set; }

        public virtual int? last_sendtime { get; set; }
        public virtual int? next_sendtime { get; set; }
        public virtual int? next2_sendtime { get; set; }
        public virtual int? next3_sendtime { get; set; }
        public virtual int? next4_sendtime { get; set; }
        public virtual int? next5_sendtime { get; set; }

        public virtual string mixed_group_code { get; set; }

        public virtual DateTime? delete_date { get; set; }
        public virtual DateTime? skip_date { get; set; }

        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }

    }
}
