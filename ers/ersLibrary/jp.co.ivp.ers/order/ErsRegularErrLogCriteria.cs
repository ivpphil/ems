﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order
{
    public class ErsRegularErrLogCriteria
        : Criteria
    {
        public virtual EnumOnOff? disp_flg
        {
            set
            {
                this.Add(Criteria.GetCriterion("regular_error_t.disp_flg", value, Operation.EQUAL));
            }
        }
        public virtual EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("regular_error_t.active", value, Operation.EQUAL));
            }
        }


        public virtual IEnumerable<string> mcodelist
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("regular_error_t.card_mcode", value));
            }
        }

        public virtual string tel
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_t.tel", value, Operation.EQUAL));
            }
        }

        public EnumDeleted? deleted
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_t.deleted", value, Operation.EQUAL));
            }
        }

        public string email
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_t.email", value, Operation.EQUAL));
            }
        }

        public string lnamek
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_t.lnamek", value, Operation.EQUAL));
            }
        }

        public string fnamek
        {
            set
            {
                this.Add(Criteria.GetCriterion("member_t.fnamek", value, Operation.EQUAL));
            }
        }

        public void SetOccured_dateBetween(DateTime? occured_date_from, DateTime? occured_date_to)
        {
            this.Add(Criteria.GetBetweenCriterion(occured_date_from, occured_date_to, Criteria.ColumnName("regular_error_t.occured_date")));
        }

        public int? regular_order_number
        {
            set
            {
                this.Add(Criteria.GetCriterion("regular_error_t.regular_detail_id", value, Operation.EQUAL));
            }
        }

        public virtual void SetOrderByMcode(OrderBy orderBy)
        {
            this.AddOrderBy("regular_error_t.card_mcode", orderBy);
        }

        public virtual void SetOrderByOccured_date(OrderBy orderBy)
        {
            this.AddOrderBy("regular_error_t.occured_date", orderBy);
        }
    }
}
