﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderRecordCriteria
        : Criteria
    {
        /// <summary>
        /// 伝票番号
        /// </summary>
        public virtual string d_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.d_no", value, Operation.EQUAL));
            }
        }

        public string subd_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.subd_no", value, Operation.EQUAL));
            }
        }

        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.id", value, Operation.EQUAL));
            }
        }

        public EnumDocBundlingFlg? doc_bundling_flg
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.doc_bundling_flg", value, Operation.EQUAL));
            }
        }

        public EnumOrderType? order_type_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.order_type", value, Operation.NOT_EQUAL));
            }
        }

        public EnumOrderType? order_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.order_type", value, Operation.EQUAL));
            }
        }

        public EnumOrderStatusType? order_status
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.order_status", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 対応区分
        /// </summary>
        public virtual IEnumerable<EnumOrderStatusType> order_status_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("ds_master_t.order_status", value));
            }
        }

        public IEnumerable<EnumOrderStatusType> order_status_not_in
        {
            set
            {
                this.Add(Criteria.GetNotInClauseCriterion("ds_master_t.order_status", value));
            }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("ds_master_t.id", Criteria.OrderBy.ORDER_BY_ASC);
        }

        public void SetOrderByPoint(OrderBy orderBy)
        {
            this.AddOrderBy("ds_master_t.point", orderBy);
        }

        public void SetOrderByScode(OrderBy orderBy)
        {
            this.AddOrderBy("ds_master_t.scode", orderBy);
        }

        public EnumSentFlg? shipped_mail
        {
            set
            {
                var sql = "EXISTS(SELECT * FROM ds_mail_t WHERE shipped_mail = :shipped_mail AND d_no = ds_master_t.d_no AND ds_id = ds_master_t.id)";
                this.Add(Criteria.GetUniversalCriterion(sql, new Dictionary<string, object>() { { "shipped_mail", (int)value } }));
            }
        }

        public EnumSentFlg? purchase_mail
        {
            set
            {
                var sql = "EXISTS(SELECT * FROM ds_mail_t WHERE purchase_mail = :purchase_mail AND d_no = ds_master_t.d_no AND ds_id = ds_master_t.id)";
                this.Add(Criteria.GetUniversalCriterion(sql, new Dictionary<string, object>() { { "purchase_mail", value } }));
            }
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("ds_master_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("ds_master_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}
