﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.strategy;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderRepository
        : ErsRepository<ErsOrder>
    {
        protected OrderSearchSpecification searchSpec = ErsFactory.ersOrderFactory.GetOrderSerachSpecification();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsOrderRepository()
            : base(new ErsDB_d_master_t())
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsOrderRepository(ErsDatabase objDB)
            : base(new ErsDB_d_master_t(objDB))
        {
        }

        /// <summary>
        /// Find order
        /// </summary>
        /// <param name="criteria">クライテリア</param>
        /// <returns>検索結果リスト</returns>
        public override IList<ErsOrder> Find(Criteria criteria)
        {
            List<ErsOrder> lstRet = new List<ErsOrder>();

            // 検索
            var list = searchSpec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                ErsOrder order = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(dr);

                lstRet.Add(order);
            }
            return lstRet;
        }

        public override long GetRecordCount(Criteria criteria)
        {
            return searchSpec.GetCountData(criteria);
        }

        public virtual string GetNextD_no()
        {
            return Convert.ToString(this.ersDB_table.GetNextSequence("d_master_t_d_no_seq"));
        }

        public virtual string  GetNextCreditOrderId(string d_no)
        {
            var creditOrderId = this.ersDB_table.GetNextSequence("credit_order_id_seq");
            return string.Format("{0}-{1:D10}", d_no, creditOrderId);
        }

        public virtual int GetNextCreditMcode()
        {
            return this.ersDB_table.GetNextSequence("credit_mcode_seq");
        }
    }
}
