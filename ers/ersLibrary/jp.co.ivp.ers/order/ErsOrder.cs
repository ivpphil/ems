﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.Payment;

namespace jp.co.ivp.ers.order
{
    /// <summary>
    /// 伝票クラス
    /// </summary>
    public class ErsOrder
        : ErsRepositoryEntity, IPaymentInfoGmoServerContainer, IPaymentInfoPayPalContainer, IPaymentInfoGmoConvenienceContainer
    {
        /// <summary>
        /// 伝票番号
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 伝票番号
        /// </summary>
        public virtual string d_no { get; set; }

        public string base_d_no { get; set; }

        /// <summary>
        /// 発行日時
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新日時
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// 会員コード
        /// </summary>
        public virtual string mcode { get; set; }

        /// <summary>
        /// メールアドレス
        /// </summary>
        public virtual string email { get; set; }

        /// <summary>
        /// 当店へのコメント
        /// </summary>
        public virtual string memo
        {
            // モール海外配送にて禁則文字がはいる事があるので置換
            get { return ErsProhibitedCharactersConverter.Convert(_memo); }
            set { _memo = ErsProhibitedCharactersConverter.Convert(value); }
        }
        private string _memo { get; set; }

        /// <summary>
        /// 管理者コメント
        /// </summary>
        public virtual string memo3 { get; set; }

        /// <summary>
        /// お客様への連絡事項
        /// </summary>
        public virtual string usr_memo { get; set; }

        /// <summary>
        /// ポイント付与済みフラグ
        /// </summary>
        public virtual short? point_ck { get; set; }

        /// <summary>
        /// 購入サイト区分
        /// </summary>
        public virtual EnumPmFlg? pm_flg { get; set; }

        /// <summary>
        /// 乱数
        /// </summary>
        public virtual string ransu { get; set; }

        /// <summary>
        /// 小計
        /// </summary>
        public virtual int subtotal { get; set; }
        /// <summary>
        /// 消費税
        /// </summary>
        public virtual int tax { get; set; }
        /// <summary>
        /// ポイント値引き
        /// </summary>
        public virtual int p_service { get; set; }
        /// <summary>
        /// 送料
        /// </summary>
        public virtual int carriage { get; set; }
        /// <summary>
        /// 手数料
        /// </summary>
        public virtual int etc { get; set; }
        /// <summary>
        /// 合計
        /// </summary>
        public virtual int total { get; set; }

        /// <summary>
        /// クーポン値引き
        /// </summary>
        public virtual int coupon_discount { get; set; }

        /// <summary>
        /// クーポンコード
        /// </summary>
        public virtual string coupon_code { get; set; }

        /// <summary>
        /// 支払い方法ID(pay_tのIDと一緒)
        /// </summary>
        public virtual EnumPaymentType? pay { get; set; }

        #region CyberSource決済
        /// 決済ID
        /// </summary>
        public virtual string c_req_no { get; set; }//PayPalと併用
        #endregion

        #region PayPal決済
        /// <summary>
        /// token
        /// </summary>
        public virtual string token { get; set; }

        /// <summary>
        /// payerID
        /// </summary>
        public virtual string PayerID { get; set; }
        #endregion

        #region GMO決済
        /// <summary>
        /// 支払い回数
        /// </summary>
        public virtual EnumGmoMethod gmoMethod { get { return EnumGmoMethod.Single; } }

        /// <summary>
        /// 決済方法（与信/即時売り上げ）
        /// </summary>
        public virtual EnumGmoJobCd jobCode { get { return EnumGmoJobCd.AUTH; } }

        /// <summary>
        /// 取引ID
        /// </summary>
        public virtual string access_id { get; set; }

        /// <summary>
        /// 取引パスワード
        /// </summary>
        public virtual string access_pass { get; set; }

        /// <summary>
        /// 承認番号
        /// </summary>
        public virtual string approve { get; set; }
        #endregion

        /// <summary>
        /// クレジットカード情報
        /// </summary>
        public virtual CreditCardInfo card_info { get; set; }

        /// <summary>
        /// 会員住所
        /// </summary>
        public virtual string shipping_id { get; set; }

        public virtual string lname
        {
            // モール海外配送にて禁則文字がはいる事があるので置換
            get { return ErsProhibitedCharactersConverter.Convert(_lname); }
            set { _lname = ErsProhibitedCharactersConverter.Convert(value); }
        }
        private string _lname { get; set; }

        public virtual string fname
        {
            // モール海外配送にて禁則文字がはいる事があるので置換
            get { return ErsProhibitedCharactersConverter.Convert(_fname); }
            set { _fname = ErsProhibitedCharactersConverter.Convert(value); }
        }
        private string _fname { get; set; }

        public virtual string lnamek { get; set; }
        public virtual string fnamek { get; set; }
        public virtual string compname { get; set; }
        public virtual string compnamek { get; set; }
        public virtual string division { get; set; }
        public virtual string divisionk { get; set; }
        public virtual string tlname { get; set; }
        public virtual string tfname { get; set; }
        public virtual string tlnamek { get; set; }
        public virtual string tfnamek { get; set; }
        public virtual string tel { get; set; }
        public virtual string fax { get; set; }
        public virtual string zip { get; set; }

        public virtual string address
        {
            // モール海外配送にて禁則文字がはいる事があるので置換
            get { return ErsProhibitedCharactersConverter.Convert(_address); }
            set { _address = ErsProhibitedCharactersConverter.Convert(value); }
        }
        private string _address { get; set; }

        public virtual string taddress
        {
            // モール海外配送にて禁則文字がはいる事があるので置換
            get { return ErsProhibitedCharactersConverter.Convert(_taddress); }
            set { _taddress = ErsProhibitedCharactersConverter.Convert(value); }
        }
        private string _taddress { get; set; }

        public virtual string maddress
        {
            // モール海外配送にて禁則文字がはいる事があるので置換
            get { return ErsProhibitedCharactersConverter.Convert(_maddress); }
            set { _maddress = ErsProhibitedCharactersConverter.Convert(value); }
        }
        private string _maddress { get; set; }
 
        public virtual int? pref { get; set; }

        /// <summary>
        /// 入金区分（order_payment_status_t.id）
        /// </summary>
        public virtual EnumOrderPaymentStatusType? order_payment_status { get; set; }

        /// <summary>
        /// 入金日
        /// </summary>
        public virtual DateTime? paid_date { get; set; }

        /// <summary>
        /// 入金額
        /// </summary>
        public virtual int paid_price { get; set; }

        /// <summary>
        /// 届け先（1 : 本人 / 2 : 別）
        /// </summary>
        public virtual EnumSendTo? send { get; set; }

        public virtual string add_lname
        {
            // モール海外配送にて禁則文字がはいる事があるので置換
            get { return ErsProhibitedCharactersConverter.Convert(_add_lname); }
            set { _add_lname = ErsProhibitedCharactersConverter.Convert(value); }
        }
        private string _add_lname { get; set; }

        public virtual string add_fname
        {
            // モール海外配送にて禁則文字がはいる事があるので置換
            get { return ErsProhibitedCharactersConverter.Convert(_add_fname); }
            set { _add_fname = ErsProhibitedCharactersConverter.Convert(value); }
        }
        private string _add_fname { get; set; }

        public virtual string add_lnamek { get; set; }
        public virtual string add_fnamek { get; set; }
        public virtual string add_compname { get; set; }
        public virtual string add_compnamek { get; set; }
        public virtual string add_division { get; set; }
        public virtual string add_divisionk { get; set; }
        public virtual string add_tlname { get; set; }
        public virtual string add_tfname { get; set; }
        public virtual string add_tlnamek { get; set; }
        public virtual string add_tfnamek { get; set; }
        public virtual string add_tel { get; set; }
        public virtual string add_fax { get; set; }
        public virtual string add_zip { get; set; }

        public virtual string add_address
        {
            // モール海外配送にて禁則文字がはいる事があるので置換
            get { return ErsProhibitedCharactersConverter.Convert(_add_address); }
            set { _add_address = ErsProhibitedCharactersConverter.Convert(value); }
        }
        private string _add_address { get; set; }

        public virtual string add_taddress
        {
            // モール海外配送にて禁則文字がはいる事があるので置換
            get { return ErsProhibitedCharactersConverter.Convert(_add_taddress); }
            set { _add_taddress = ErsProhibitedCharactersConverter.Convert(value); }
        }
        private string _add_taddress { get; set; }

        public virtual string add_maddress
        {
            // モール海外配送にて禁則文字がはいる事があるので置換
            get { return ErsProhibitedCharactersConverter.Convert(_add_maddress); }
            set { _add_maddress = ErsProhibitedCharactersConverter.Convert(value); }
        }
        private string _add_maddress { get; set; }

        public virtual int? add_pref { get; set; }

        /// <summary>
        /// 配送希望日
        /// </summary>
        public virtual DateTime? senddate { get; set; }

        /// <summary>
        /// 配送希望時間情報
        /// </summary>
        public virtual int? sendtime { get; set; }

        /// <summary>
        /// Add property to set this value to regularOrderRecords Entity.
        /// </summary>
        public virtual int? next_sendtime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string ccode
        {
            get
            {
                if (string.IsNullOrEmpty(this._ccode))
                {
                    return this._ccode;
                }
                return this._ccode.ToUpper();
            }
            set
            {
                this._ccode = value;
            }
        }
        private string _ccode;

        /// <summary>
        /// ギフトラッピングフラグ
        /// </summary>
        public virtual EnumWrap? wrap { get; set; }

        /// <summary>
        /// ギフトラッピングメッセージ
        /// </summary>
        public virtual string memo2
        {
            // モール海外配送にて禁則文字がはいる事があるので置換
            get { return ErsProhibitedCharactersConverter.Convert(_memo2); }
            set { _memo2 = ErsProhibitedCharactersConverter.Convert(value); }
        }
        private string _memo2 { get; set; }

        public virtual int? member_add_id { get; set; }

        public virtual int? member_card_id { get; set; }

        public string user_id { get; set; }

        public string credit_order_id { get; set; }

        public EnumDocBundlingFlg? doc_bundle_flg { get; set; }

        public EnumConvCode? conv_code { get; set; }

        public string conv_conf_no { get; set; }

        public string conv_receipt_no { get; set; }

        public DateTime? conv_payment_term { get; set; }

        public int? page_id { get; set; }

        public virtual int? site_id { get; set; }

        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        public virtual string mall_d_no { get; set; }

        public int wrap_cost { get; set; }

        public EnumMallOrderStatus? mall_order_status { get; set; }

        public virtual DateTime? after_cancel_date { get; set; }

        public virtual DateTime? cancel_date { get; set; }

        public EnumSentContinualBillingFlg sent_continual_billing { get; set; }

        public int? point_magnification { get; set; }
    }
}
