﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderStatusCriteria
        : Criteria
    {
        internal void SetOrderById(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("order_status_t.id", orderBy);
        }

        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("order_status_t.id", value, Operation.EQUAL));
            }
        }
    }
}
