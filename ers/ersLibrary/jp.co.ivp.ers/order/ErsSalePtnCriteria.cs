﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order
{
    public class ErsSalePtnCriteria
        : Criteria
    {
        internal void SetOrderById(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("sale_ptn_t", orderBy);
        }

        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("sale_ptn_t.id", value, Operation.EQUAL));
            }
        }
    }
}
