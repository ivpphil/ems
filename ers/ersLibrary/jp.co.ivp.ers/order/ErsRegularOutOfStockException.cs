﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.order
{
    public class ErsRegularOutOfStockException : Exception
    {

        public string errMsg { get; protected set; }

        public ErsRegularOutOfStockException(string scode, string mcode)
            : base()
        {

            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            groupCriteria.scode = scode;
            var merchandiseList = groupRepository.FindSkuBaseItemList(groupCriteria, null);
            if (merchandiseList.Count() == 0)
            {
                this.errMsg += "商品が存在しないため伝票発行できませんでした。\r\n";
                this.errMsg += "商品名=該当なし,\r\n";
                this.errMsg += "商品番号=" + scode + "\r\n\r\n";
                return;
            }

            var merchandise = merchandiseList.First();

            var stock = merchandise.stock;
            var date_from = merchandise.date_from;
            var date_to = merchandise.date_to;

            if (!(DateTime.Now >= date_from.Value && DateTime.Now <= date_to.Value))
            {
                this.errMsg += "販売期間外のため伝票発行できませんでした。\r\n";
            }
            else
            {
                this.errMsg += "在庫の状況により伝票発行できませんでした。\r\n";
            }
            this.errMsg += "会員コード=" + mcode + ",\r\n";
            this.errMsg += "商品名=" + merchandise.sname + ",\r\n";
            this.errMsg += "商品番号=" + merchandise.scode + ",\r\n";
            this.errMsg += "販売開始日=" + merchandise.date_from + ",\r\n";
            this.errMsg += "販売終了日=" + merchandise.date_to + ",\r\n";
            this.errMsg += "soldout設定=" + ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.SoldoutFlg,EnumCommonNameColumnName.namename,(int?)merchandise.soldout_flg) + ",\r\n";
            this.errMsg += "在庫数=" + merchandise.stock  + "";
            if (merchandise.set_flg == EnumSetFlg.IsSet)
            {
                this.errMsg += ",\r\nセット商品\r\n\r\n";
            }
            else
            {
                this.errMsg += "\r\n\r\n";
            }
        }

    }
}
