﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderIntegrated
        : ErsOrder
    {

        private int sequence_subd_no = 0;


        /// <summary>
        /// Gets List of ErsRegularOrder that will be used in preparing bills.
        /// </summary>
        protected List<ErsOrderContainer> listOrder = new List<ErsOrderContainer>();

        /// <summary>
        /// Gets Instance of ordinaryOrder
        /// </summary>
        public virtual ErsOrder ordinaryOrderInstance { get; protected set; }

        /// <summary>
        /// Gets instance of Bill that integrate some Bill instances that collect from inner list of ErsOrder
        /// </summary>
        /// <returns></returns>
        protected virtual void GetIntegratedBill()
        {
            var subtotal = 0;
            var tax = 0;
            var p_service = 0;
            var total = 0;
            var carriage = 0;
            var etc = 0;

            foreach (var orderContainer in this.listOrder)
            {
                var order = orderContainer.OrderHeader;
                subtotal += order.subtotal;
                tax += order.tax;
                p_service += order.p_service;
                total += order.total;
                carriage += order.carriage;
                etc += order.etc;
            }

            this.subtotal = subtotal;
            this.tax = tax;
            this.p_service = p_service;
            this.total = total;
            this.carriage = carriage;
            this.etc = etc;
        }

        /// <summary>
        /// Generates a list of ErsOrder, then hold it.
        /// </summary>
        /// <param name="dateContainer"></param>
        /// <param name="basket"></param>
        public void GenerateOrder(IOrderIntegratedDateContainer dateContainer, ErsBasket basket, EnumDelvMethod? deliv_method)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var innerDateContainer = (IOrderIntegratedDateContainer)dateContainer.Clone();

            this.coupon_code = dateContainer.ent_coupon_code;

            var dictionaryBasket = basket.LoadBasketAsList(innerDateContainer.senddate,
                innerDateContainer.sendtime,
                innerDateContainer.regular_sendtime);

            var p_service = innerDateContainer.p_service;

            //Generates the list of ErsOrder.
            foreach (var next_date in dictionaryBasket.Keys)
            {
                var otherBasket = dictionaryBasket[next_date];

                innerDateContainer.senddate = next_date != DateTime.MinValue ? next_date : (DateTime?)null;
                innerDateContainer.p_service = p_service;
                innerDateContainer.sendtime = otherBasket.sendtime;

                var otherOrder = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(innerDateContainer.GetPropertiesAsDictionary());
                otherOrder.site_id = setup.site_id;

                var shipping_pref = otherOrder.send == EnumSendTo.MEMBER_ADDRESS ? otherOrder.pref : otherOrder.add_pref;
                var orderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(otherOrder.d_no, otherBasket, deliv_method, shipping_pref);

                EnumCarriageFreeStatus? status = null;

                if (ErsFactory.ersBasketFactory.GetRegularShippingFreeSpec().IsSatisfiedBy(orderRecords.Values)
                    || ErsFactory.ersBasketFactory.GetCarriageCostTypeFreeSpec().IsSatisfiedBy(orderRecords.Values)                  
                    || ErsFactory.ersBasketFactory.GetLpShippingFreeSpec().IsSatisfiedBy(otherBasket))
                {
                    status = EnumCarriageFreeStatus.CARRIAGE_FREE;
                }
                else
                {
                    status = ErsFactory.ersBasketFactory.GetCarriageFreeSpecification().GetCarriageFreeStatus(otherBasket.subtotal + otherBasket.regular_subtotal);
                }

                ErsFactory.ersOrderFactory.GetErsCalcService().calcOrder(otherOrder, orderRecords.Values, otherOrder.p_service, status);

                //For All item has total of 0 and bask_t.carriageFree == EnumCarriageFreeStatus.CARRIAGE_FREE
                if(ErsFactory.ersOrderFactory.GetIsNonNeededPaymentSpec().IsSpecified(otherOrder.total))                {
                    this.pay = EnumPaymentType.NON_NEEDED_PAYMENT;
                }

                p_service = p_service - otherOrder.p_service;

                var orderContainer = ErsFactory.ersOrderFactory.GetErsOrderContainer();
                orderContainer.OrderHeader = otherOrder;
                orderContainer.OrderRecords = orderRecords;
                listOrder.Add(orderContainer);

                //Hold ordinaryOrder instance.
                if (ordinaryOrderInstance == null)
                    ordinaryOrderInstance = otherOrder;
            }

            //order(登録)オブジェクトに各クーポン割引額按分
            ErsFactory.ersCouponFactory.GetCouponRegistComputeStgy().RegistCompute(this);

            this.GetIntegratedBill();

            //For All item has total of 0 and bask_t.carriageFree == EnumCarriageFreeStatus.CARRIAGE_FREE
            if (ErsFactory.ersOrderFactory.GetIsNonNeededPaymentSpec().IsSpecified(this.total))
            {
                this.pay = EnumPaymentType.NON_NEEDED_PAYMENT;
            }
        }

        /// <summary>
        /// Gets the list of the ErsOrder.
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<ErsOrderContainer> GetListOrder()
        {
            foreach (var orderContainer in listOrder)
            {
                var order = orderContainer.OrderHeader;
                var orderRecords = orderContainer.OrderRecords;
                if (string.IsNullOrEmpty(order.d_no) && !string.IsNullOrEmpty(this.d_no))
                {
                    this.sequence_subd_no++;
                    var d_no = this.d_no + "-" + (sequence_subd_no.ToString("D2"));

                    ErsFactory.ersOrderFactory.GetSetD_noStgy().Set(order, orderRecords.Values, d_no);

                    order.base_d_no = this.d_no;
                }
                order.mcode = this.mcode;

                yield return orderContainer;
            }
        }

        /// <summary>
        /// Set regular_id to ErsRegularOrderRecord.(this method is used when buy.)
        /// </summary>
        /// <param name="regular_id"></param>
        public virtual void SetRegularDetailID(ErsRegularOrder regularOrder)
        {
            foreach (var regularOrderRecord in regularOrder.regularOrderRecords)
            {
                var orderRecord = this.GetSameRecord(regularOrderRecord);

                if (orderRecord == null)
                    throw new Exception("the ErsRegularOrderRecord is not prepared to issue.");

                orderRecord.regular_detail_id = new[] { regularOrderRecord.id.Value };
            }
        }

        /// <summary>
        /// 指定された定期レコードに対応する伝票レコードを取得する。
        /// </summary>
        /// <param name="regularOrderRecord"></param>
        /// <returns></returns>
        protected virtual ErsOrderRecord GetSameRecord(ErsRegularOrderRecord regularOrderRecord)
        {
            foreach (var orderContainer in listOrder)
            {
                var orderRecords = orderContainer.OrderRecords;
                foreach (var orderRecord in orderRecords.Values)
                {
                    var comparePrice = (orderRecord.order_type == EnumOrderType.Subscription) ? orderRecord.regular_price : orderRecord.price;

                    if (regularOrderRecord.price == comparePrice
                        && regularOrderRecord.scode == orderRecord.scode
                        && regularOrderRecord.amount == orderRecord.amount
                        && orderRecord.regular_detail_id == null)
                    {
                        return orderRecord;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// 伝票に対し、使用カードIDを設定する
        /// </summary>
        /// <param name="card_id"></param>
        public void SetMemberCardId(int? card_id)
        {
            foreach (var orderContainer in listOrder)
            {
                var order = orderContainer.OrderHeader;
                order.member_card_id = card_id;
            }
        }

        /// <summary>
        /// 伝票に対し、お届け先IDを設定する
        /// </summary>
        /// <param name="member_add_id"></param>
        public void SetMemberAddId(int? member_add_id)
        {
            foreach (var orderContainer in listOrder)
            {
                var order = orderContainer.OrderHeader;
                order.member_add_id = member_add_id;
            }
        }
    }
}
