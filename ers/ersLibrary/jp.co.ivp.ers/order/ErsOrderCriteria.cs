﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.Diagnostics;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderCriteria
        : Criteria
    {

        protected internal ErsOrderCriteria()
        {
        }

        /// <summary>
        /// 購入日時（開始）
        /// </summary>
        public virtual DateTime intime_from
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.intime", value, Operation.GREATER_EQUAL));
            }
        }

        /// <summary>
        /// 購入日時（終了）
        /// </summary>
        public virtual DateTime intime_to
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.intime", value.AddSeconds(1), Operation.LESS_THAN));
            }
        }

        /// <summary>
        /// 入金日（開始）
        /// </summary>
        public virtual DateTime paid_date1
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.paid_date", value, Operation.GREATER_EQUAL));
            }
        }

        /// <summary>
        /// 入金日（終了）
        /// </summary>
        public virtual DateTime paid_date2
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.paid_date", value, Operation.LESS_EQUAL));
            }
        }

        /// <summary>
        /// 入金金額
        /// </summary>
        public virtual long? paid_price
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.paid_price", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 発送日（開始）
        /// </summary>
        public virtual DateTime shipdate1
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.shipdate", value, Operation.GREATER_EQUAL));
            }
        }

        /// <summary>
        /// 発送日（終了）
        /// </summary>
        public virtual DateTime shipdate2
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.shipdate", value, Operation.LESS_EQUAL));
            }
        }

        /// <summary>
        /// 返品日（開始）
        /// </summary>
        public virtual DateTime af_cancel_date_from
        {
            set
            {
                this.Add(Criteria.GetCriterion("af_cancel.tdate", value, Operation.GREATER_EQUAL));
            }
        }

        /// <summary>
        /// 返品日（終了）
        /// </summary>
        public virtual DateTime af_cancel_date_to
        {
            set
            {
                this.Add(Criteria.GetCriterion("af_cancel.tdate", value, Operation.LESS_EQUAL));
            }
        }

        /// <summary>
        /// キャンセル日（開始）
        /// </summary>
        public virtual DateTime cancel_date_from
        {
            set
            {
                this.Add(Criteria.GetCriterion("cancel.tdate", value, Operation.GREATER_EQUAL));
            }
        }

        /// <summary>
        /// キャンセル日（終了）
        /// </summary>
        public virtual DateTime cancel_date_to
        {
            set
            {
                this.Add(Criteria.GetCriterion("cancel.tdate", value, Operation.LESS_EQUAL));
            }
        }

        /// <summary>
        /// 支払い方法
        /// </summary>
        public virtual EnumPaymentType[] pay_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("d_master_t.pay", value.Cast<int>()));
            }
        }

        /// <summary>
        /// 支払い方法(NOT_IN)
        /// </summary>
        public virtual EnumPaymentType[] pay_not_in
        {
            set
            {
                this.Add(Criteria.GetNotInClauseCriterion("d_master_t.pay", value.Cast<int>()));
            }
        }

        /// <summary>
        /// 対応区分
        /// </summary>
        public virtual IEnumerable<EnumOrderStatusType> order_status_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("ds_master_t.order_status", value.Cast<int>()));
            }
        }

        /// <summary>
        /// 対応区分
        /// </summary>
        public virtual EnumOrderStatusType[] order_status_not_in
        {
            set
            {
                this.Add(Criteria.GetNotInClauseCriterion("ds_master_t.order_status", value.Cast<int>()));
            }
        }

        /// <summary>
        /// 入金区分
        /// </summary>
        public virtual EnumOrderPaymentStatusType[] order_payment_status_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("d_master_t.order_payment_status", value.Cast<int>()));
            }
        }

        /// <summary>
        /// 入金区分
        /// </summary>
        public virtual EnumOrderPaymentStatusType[] order_payment_status_not_in
        {
            set
            {
                this.Add(Criteria.GetNotInClauseCriterion("d_master_t.order_payment_status", value.Cast<int>()));
            }
        }

        /// <summary>
        /// メールアドレス
        /// </summary>
        public virtual string email
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.email", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// メールアドレス
        /// </summary>
        public virtual string email_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.email", value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// 購入サイト
        /// </summary>
        public virtual EnumPmFlg? pm_flg
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.pm_flg", (int?)value, Operation.EQUAL));
            }
        }
        //購入サイトCTS
        public virtual void SetPmFlgCts()
        {
            this.Add(Criteria.JoinWithOR(new[]{Criteria.GetCriterion("d_master_t.pm_flg", (int)EnumPmFlg.TEL, Operation.EQUAL),
                    Criteria.GetCriterion("d_master_t.pm_flg", (int)EnumPmFlg.LETTER, Operation.EQUAL),
                    Criteria.GetCriterion("d_master_t.pm_flg", (int)EnumPmFlg.FAX, Operation.EQUAL)}));
        }

        /// <summary>
        /// イド
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 会員コード
        /// </summary>
        public virtual string mcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.mcode", value, Operation.EQUAL));
            }
        }

        public virtual string mcode_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.mcode", value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// 伝票番号
        /// </summary>
        public virtual string d_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.d_no", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 伝票番号
        /// </summary>
        public virtual string d_no_like
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("d_master_t.d_no", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// 伝票番号
        /// </summary>
        public string d_no_not_equals
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.d_no", value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// 配送枝番
        /// </summary>
        public string subd_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.subd_no", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 伝票番号（明細）
        /// </summary>
        public virtual string ds_d_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.d_no", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 【明細】販売方法
        /// </summary>
        public virtual int s_sale_ptn
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.s_sale_ptn", value, Operation.EQUAL));
            }
        }

        public EnumOrderType? order_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.order_type", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 【明細】商品番号
        /// </summary>
        public virtual string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.scode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 【明細】商品番号(複数)
        /// </summary>
        public virtual string[] scode_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("ds_master_t.scode", value));
            }
        }

        public virtual int? price
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.price", value, Operation.EQUAL));
            }
        }


        /// <summary>
        /// 決済ID
        /// </summary>
        public virtual string c_req_no_not_equals
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.c_req_no", value, Operation.NOT_EQUAL));
            }
        }


        /// <summary>
        /// 姓
        /// </summary>
        public virtual string lname
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.lname", value, Operation.EQUAL));
            }
        }


        /// <summary>
        /// （姓）
        /// </summary>
        public virtual string fname
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.fname", value, Operation.EQUAL));
            }
        }


        /// <summary>
        /// 姓
        /// </summary>
        public virtual string lnamek
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.lnamek", value, Operation.EQUAL));
            }
        }


        /// <summary>
        /// （姓）
        /// </summary>
        public virtual string fnamek
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.fnamek", value, Operation.EQUAL));
            }
        }



        /// <summary>
        /// アドレス
        /// </summary>
        public virtual string address
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.address", value, Operation.EQUAL));
            }
        }


        /// <summary>
        /// 県 
        /// </summary>
        public virtual int? pref
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.pref", value, Operation.EQUAL));
            }
        }


        /// <summary>
        /// 電話
        /// </summary>
        public virtual string tel
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.tel", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// お届け希望日
        /// </summary>
        public virtual DateTime? senddate
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.senddate", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// お届け希望日
        /// </summary>
        public virtual DateTime? senddate_greater_than
        {
            set
            {
                var criteria01 = GetCriterion("d_master_t.senddate", value, Operation.LESS_EQUAL);
                var criteria02 = GetCriterion("d_master_t.senddate", null, Operation.EQUAL);

                this.Add(Criteria.JoinWithOR(new[] { criteria01, criteria02 }));
            }
        }
        /// <summary>
        /// 電話
        /// </summary>
        public virtual DateTime? senddate_nor_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.senddate", value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// ファックス
        /// </summary>
        public virtual string fax
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.fax", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 基幹伝票番号
        /// </summary>
        public virtual string erp_d_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.erp_d_no", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("d_master_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("d_master_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? site_id_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// モールショップ区分
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.mall_shop_kbn", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// モールショップ区分
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.mall_shop_kbn", (int)value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// モール受注番号
        /// </summary>
        public virtual string mall_d_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.mall_d_no", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 楽天ポイント
        /// </summary>
        public virtual int? rakuten_p_service_greater_than
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.rakuten_p_service", value, Operation.GREATER_THAN));
            }
        }

        public virtual string memo_OR_memo3
        {
            set
            {
                this.Add(Criteria.JoinWithOR(
                    new List<CriterionBase> 
                    { 
                        Criteria.GetLikeClauseCriterion("d_master_t.memo", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH), 
                        Criteria.GetLikeClauseCriterion("d_master_t.memo3", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH)
                    }));
            }
        }

        public virtual void SetOrderByIntime(OrderBy orderBy)
        {
            this.AddOrderBy("d_master_t.intime", orderBy);
            this.AddOrderBy("d_master_t.d_no", orderBy);
        }

        //addnl jcb 02-21-12
        public virtual void SetOrderByName()
        {
            this.AddOrderBy("d_master_t.fname", OrderBy.ORDER_BY_ASC);
            this.AddOrderBy("d_master_t.lname", OrderBy.ORDER_BY_ASC);
         }

        /// <summary>
        /// フィールドの比較
        /// </summary>
        public virtual void SetCompare_paid_price_and_total()
        {
            this.Add(Criteria.GetCriterion("d_master_t.paid_price", ColumnName("d_master_t.total"), Operation.NOT_EQUAL));
        }


        public virtual DateTime shipdate
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.shipdate", value, Operation.LESS_EQUAL));
            }
        }

        public virtual int point_ck
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.point_ck", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 出庫許可リスト用の条件をセットする。
        /// </summary>
        /// <param name="crtOrder"></param>
        public virtual void SetConditionsForShipmentList(ErsOrderCriteria crtOrder)
        {
            //クレジット・代引・PayPal・決済なしの場合は、新着注文
            var criteriaForAuth = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteriaForAuth.pay_in = new[] { EnumPaymentType.CREDIT_CARD, EnumPaymentType.CASH_ON_DELIVERY, EnumPaymentType.PAYPAL, EnumPaymentType.NON_NEEDED_PAYMENT };
            criteriaForAuth.order_status_in = new[] { EnumOrderStatusType.NEW_ORDER };

            //それ以外は、入金済み
            var criteriaForElse = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteriaForElse.pay_not_in = new[] { EnumPaymentType.CREDIT_CARD, EnumPaymentType.CASH_ON_DELIVERY, EnumPaymentType.PAYPAL };
            criteriaForElse.order_payment_status_in = new[] { EnumOrderPaymentStatusType.PAID };
            criteriaForElse.order_status_in = new[] { EnumOrderStatusType.NEW_ORDER };

            //criteriaForAuthとcriteriaForElseをORで結合
            crtOrder.Add(Criteria.JoinWithOR(new[] { criteriaForAuth, criteriaForElse }));

            var dic = new Dictionary<string, object>();
            dic.Add("order_status", (int)EnumOrderStatusType.DELIVER_WAITING);

            crtOrder.Add(Criteria.GetUniversalCriterion(" NOT EXISTS (SELECT 1 FROM ds_master_t WHERE ds_master_t.order_status = :order_status AND ds_master_t.d_no = d_master_t.d_no ) ", dic));
        }

        /// <summary>
        /// Sets criterions in order to search data for first puchase monitoring.
        /// </summary>
        public void SetMonitorFirstSearch()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            this.lname = setup.monitorFirstLname;
            this.fname = setup.monitorFirstFname;
        }

        /// <summary>
        /// Sets criterions in order to search data for second puchase monitoring.
        /// </summary>
        public void SetMonitorSecondSearch()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            this.lname = setup.monitorSecondLname;
            this.fname = setup.monitorSecondFname;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual EnumDocBundlingFlg ds_doc_bundling_flg
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.doc_bundling_flg", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 定期購入ID
        /// </summary>
        public int[] regular_detail_id
        {
            set { this.Add(Criteria.GetCriterion("ds_master_t.regular_detail_id", value, Operation.EQUAL)); }
        }

        /// <summary>
        /// 定期購入ID
        /// </summary>
        public int[] regular_detail_id_not
        {
            set { this.Add(Criteria.GetCriterion("ds_master_t.regular_detail_id", value, Operation.NOT_EQUAL)); }
        }


        /// <summary>
        /// 商品送料区分
        /// </summary>
        public virtual EnumCarriageCostType carriage_cost_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.carriage_cost_type", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 商品複数回購入制限区分
        /// </summary>
        public virtual EnumPluralOrderType plural_order_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.plural_order_type", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// クーポンコード
        /// </summary>
        public virtual string coupon_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.coupon_code", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// キャンペーンコード
        /// </summary>
        public virtual string ccode
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.ccode", value.ToUpper(), Operation.EQUAL));
            }
        }

        public virtual IEnumerable<string> gcode_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("ds_master_t.gcode", value));
            }
        }

        /// <summary>
        /// キャンセルの意味を持つステータスの配列
        /// </summary>
        public EnumOrderStatusType[] CancelStatusArray { get { return CANCEL_STATUS_ARRAY; } }


        public static EnumOrderStatusType[] CANCEL_STATUS_ARRAY { get { return new[] { EnumOrderStatusType.CANCELED, EnumOrderStatusType.CANCELED_AFTER_DELIVER }; } }

        public string base_d_no_not_equals
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.base_d_no", value, Operation.NOT_EQUAL));
            }
        }

        public EnumDocBundlingFlg? doc_bundle_flg
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.doc_bundle_flg", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search an order which is specified shipping date.
        /// </summary>
        /// <param name="targetDate"></param>
        public void SetSearchSpecifiedSenddate(DateTime targetDate)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("targetDate", targetDate);
            this.Add(Criteria.GetUniversalCriterion("d_master_t.senddate::date <= :targetDate::date + '7days'::interval", parameters));
        }

        public string ransu
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.ransu", value, Operation.EQUAL));
            }
        }

        public void SetBetweenDSMIntimeByRankingTerm(int ranking_term)
        {
            this.Add(Criteria.GetBetweenCriterion(DateTime.Now.AddDays(-ranking_term), DateTime.Now, ColumnName("ds_master_t.intime")));
        }

        public void AddOrderBySumResult(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("sum_result", orderBy);
        }

        public string access_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.access_id", value, Operation.EQUAL));
            }
        }

        public string credit_order_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.credit_order_id", value, Operation.EQUAL));
            }
        }

        public int? member_card_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.member_card_id", value, Operation.EQUAL));
            }
        }

        public EnumMallOrderStatus mall_order_status
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.mall_order_status", value, Operation.EQUAL));
            }
        }

        public EnumMallOrderStatus mall_order_status_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.mall_order_status", value, Operation.NOT_EQUAL));
            }
        }

        public EnumSentContinualBillingFlg? sent_continual_billing
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.sent_continual_billing", value, Operation.EQUAL));
            }
        }

        public EnumSentFlg? shipped_mail
        {
            set
            {
                var dsCriteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
                dsCriteria.shipped_mail = value;
                this.Add(dsCriteria);
            }
        }

        public EnumSentFlg? purchase_mail
        {
            set
            {
                var dsCriteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
                dsCriteria.purchase_mail = value;
                this.Add(dsCriteria);
            }
        }

        public void SetContinualBilingOnly()
        {
            this.Add(Criteria.GetCriterion("d_master_t.sent_continual_billing", EnumSentContinualBillingFlg.None, Operation.NOT_EQUAL));
        }

        public void ignoreMonitor()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var strSQL = " (d_master_t.lname, d_master_t.fname) NOT IN ((:monitorFirstLname, :monitorFirstFname), (:monitorSecondLname, :monitorSecondFname))";

            this.Add(Criteria.GetUniversalCriterion(strSQL,
                new Dictionary<string, object>() { 
                    { "monitorFirstLname", setup.monitorFirstLname },
                    { "monitorFirstFname", setup.monitorFirstFname },
                    { "monitorSecondLname", setup.monitorSecondLname },
                    { "monitorSecondFname", setup.monitorSecondFname } }));
        }
        //listItemから伝票を検索
        public void SetTargetExistsSpecifiedItem(List<Dictionary<string, object>> listItem)
        {
            

            string SQL = string.Empty;
            IList<string> listItemWhere = new List<string>();

            var dicData = new Dictionary<string, object>();

            // 商品コード AND 販売方法 [Scode and Order type]

            //販売方法の配列を文字列にする
            for (int i = 0; i < listItem.Count; i++)
            {
                string stringoforders = "";
               int[] orders = (int[])(listItem[i]["order_type"]);
               for (int n = 0; n < orders.Length; n++)
               {
                   if (stringoforders != "")
                   {
                       stringoforders += ",";
                   }
                   stringoforders += orders[n];
               }
                // WHERE句 [Statement of WHERE]
               listItemWhere.Add(string.Format("ds_master_t.scode = :scode{0} AND ", i) + "ds_master_t.order_type IN(" + stringoforders + ")");

                // パラメータ [Parameter]
                dicData["scode" + i] = listItem[i]["scode"];

            }

            // OR連結 [Join with "AND"]
            SQL = String.Join(" OR ", listItemWhere);

            Add(Criteria.GetUniversalCriterion(SQL,dicData));

        }

        public void HasDeliveredOrderRecord()
        {
            var strSQL = "EXISTS(SELECT inner_ds.* FROM ds_master_t AS inner_ds WHERE inner_ds.order_status = ANY(:order_status) AND inner_ds.d_no = d_master_t.d_no)";

            this.Add(Criteria.GetUniversalCriterion(strSQL,
                new Dictionary<string, object>()
                {
                    {"order_status", new[]{ (int)EnumOrderStatusType.DELIVERED }}
                }));
        }
    }
}
