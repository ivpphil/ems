﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderRecordStatusHistoryCriteria
        : Criteria
    {
        /// <summary>
        /// 伝票番号 [Order no]
        /// </summary>
        public virtual string d_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_status_history_t.d_no", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 明細ID [Record ID]
        /// </summary>
        public virtual int? ds_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_status_history_t.ds_id", value, Operation.EQUAL));
            }
        }
    }
}