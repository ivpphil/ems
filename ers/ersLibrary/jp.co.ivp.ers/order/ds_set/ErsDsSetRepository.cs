﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.merchandise.specification;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise.strategy;

namespace jp.co.ivp.ers.order
{
    public class ErsDsSetRepository
        : ErsRepository<ErsDsSet>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsDsSetRepository()
            : base(new ErsDB_ds_set_t())
        {
        }

        public override IList<ErsDsSet> Find(Criteria criteria)
        {
            List<ErsDsSet> lstRet = new List<ErsDsSet>();

            var searchSpec = ErsFactory.ersOrderFactory.GetDsSetSearchSpec();
            // 検索
            var list = searchSpec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                var dsSet = ErsFactory.ersOrderFactory.GetErsDsSetWithParameter(dr);
                lstRet.Add(dsSet);
            }
            return lstRet;
        }

        public override long GetRecordCount(Criteria criteria)
        {
            var searchSpec = ErsFactory.ersOrderFactory.GetDsSetSearchSpec();
            return searchSpec.GetCountData(criteria);
        }

    }
}
