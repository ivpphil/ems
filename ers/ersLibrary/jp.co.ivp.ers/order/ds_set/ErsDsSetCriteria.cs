﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order
{
    public class ErsDsSetCriteria
        : Criteria
    {
        public virtual int? id { set { this.Add(Criteria.GetCriterion("ds_set_t.id", value, Operation.EQUAL)); } }

        public virtual string d_no { set { this.Add(Criteria.GetCriterion("ds_set_t.d_no", value, Operation.EQUAL)); } }

        public virtual int? ds_id { set { this.Add(Criteria.GetCriterion("ds_set_t.ds_id", value, Operation.EQUAL)); } }

        public virtual string parent_scode { set { this.Add(Criteria.GetCriterion("ds_set_t.parent_scode", value, Operation.EQUAL)); } }

        public virtual string scode { set { this.Add(Criteria.GetCriterion("ds_set_t.scode", value, Operation.EQUAL)); } }

        public virtual int? amount { set { this.Add(Criteria.GetCriterion("ds_set_t.amount", value, Operation.EQUAL)); } }

        public virtual DateTime? intime { set { this.Add(Criteria.GetCriterion("ds_set_t.intime", value, Operation.EQUAL)); } }

        public virtual DateTime? utime { set { this.Add(Criteria.GetCriterion("ds_set_t.utime", value, Operation.EQUAL)); } }

        public virtual int? price { set { this.Add(Criteria.GetCriterion("ds_set_t.price", value, Operation.EQUAL)); } }

        public virtual int? regular_price { set { this.Add(Criteria.GetCriterion("ds_set_t.regular_price", value, Operation.EQUAL)); } }

        public virtual string[] d_no_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("ds_set_t.d_no", value));
            }
        }

        public virtual string[] scode_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("ds_set_t.scode", value));
            }
        }

        /// <summary>
        /// 商品送料区分
        /// </summary>
        public virtual EnumCarriageCostType carriage_cost_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_set_t.carriage_cost_type", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 商品複数回購入制限区分
        /// </summary>
        public virtual EnumPluralOrderType plural_order_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_set_t.plural_order_type", value, Operation.EQUAL));
            }
        }
        /// <summary>
        /// 会員コード
        /// </summary>
        public virtual string mcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("d_master_t.mcode", value, Operation.EQUAL));
            }
        }

    }
}
