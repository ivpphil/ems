﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.order
{
    /// <summary>
    /// セット明細クラス
    /// </summary>
    public class ErsDsSet
        : ErsRepositoryEntity   
    {

        public override int? id { get; set; }
        public virtual string d_no { get; set; }
        public virtual int? ds_id { get; set; }
        public string parent_scode { get; set; }
        public string scode { get; set; }
        public int? amount { get; set; }
        public virtual EnumCarriageCostType? carriage_cost_type { get; set; }
        public virtual EnumPluralOrderType? plural_order_type { get; set; }

        /// <summary>
        /// 通常用セット内訳価格
        /// </summary>
        public int? price { get; set; }

        /// <summary>
        /// 定期用セット内訳価格
        /// </summary>
        public int? regular_price { get; set; }

        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }

        /// <summary>
        /// 仕入れ価格
        /// </summary>
        public int? cost_price { get; set; }

        /// <summary>
        /// 仕入れ先コード
        /// </summary>
        public string supplier_code { get; set; }

        /// <summary>
        /// 発注区分
        /// </summary>
        public EnumWhOrderType? wh_order_type { get; set; }
    }
}
