﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order.ds_set.specification
{
    public class DsSetSearchSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            string str_sql = "";
            str_sql = " SELECT DISTINCT ds_set_t.* FROM ds_set_t INNER JOIN ds_master_t ON ds_set_t.ds_id = ds_master_t.id INNER JOIN d_master_t ON ds_master_t.d_no = d_master_t.d_no ";
            return str_sql;
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            string str_sql = "";
            str_sql = " SELECT COUNT(DISTINCT ds_set_t.id) AS count ";
            str_sql += "FROM ds_set_t ";
            str_sql += "INNER JOIN ds_master_t ON ds_set_t.ds_id = ds_master_t.id ";
            str_sql += "INNER JOIN d_master_t ON ds_master_t.d_no = d_master_t.d_no ";

            return str_sql;
        }
    }
}
