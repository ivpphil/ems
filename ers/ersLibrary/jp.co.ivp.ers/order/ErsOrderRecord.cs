﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.order
{

    public class ErsOrderRecord
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual string d_no { get; set; }
        public virtual string scode { get; set; }
        public virtual string maker_scode { get; set; }
        public virtual string sname { get; set; }
        public virtual int? price { get; set; }
        public virtual int? regular_price { get; set; }
        public virtual int? amount { get; set; }
        public virtual int? total { get; set; }
        public virtual int? point { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual EnumSalePatternType? s_sale_ptn { get; set; }
        public virtual int? cancel_amount { get; set; }
        public virtual int? after_cancel_amount { get; set; }
        public virtual string gcode { get; set; }
        public virtual string gname { get; set; }
        public virtual string m_gname { get; set; }
        public virtual DateTime? date_from { get; set; }
        public virtual DateTime? date_to { get; set; }
        public virtual EnumStockFlg? stock_flg { get; set; }
        public virtual int? stock_set1 { get; set; }
        public virtual int? stock_set2 { get; set; }
        public virtual int[] cate1 { get; set; }
        public virtual int[] cate2 { get; set; }
        public virtual int[] cate3 { get; set; }
        public virtual int[] cate4 { get; set; }
        public virtual int[] cate5 { get; set; }
        public virtual string recommend1 { get; set; }
        public virtual string recommend2 { get; set; }
        public virtual string recommend3 { get; set; }
        public virtual string recommend4 { get; set; }
        public virtual string recommend5 { get; set; }
        public virtual string jancode { get; set; }
        public virtual string m_sname { get; set; }
        public virtual string attribute1 { get; set; }
        public virtual string attribute2 { get; set; }
        public virtual string mixed_group_code { get; set; }
        public virtual int? max_purchase_count { get; set; }
        public virtual EnumDisp_list_flg? disp_list_flg { get; set; }
        public virtual int? price2 { get; set; }
        public virtual EnumOrderStatusType? order_status { get; set; }
        public virtual string sendno { get; set; }
        public virtual DateTime? shipdate { get; set; }
        public virtual string subd_no { get; set; }
        public virtual int[] regular_detail_id { get; set; }
        public virtual EnumCarriageCostType? carriage_cost_type { get; set; }
        public virtual EnumPluralOrderType? plural_order_type { get; set; }
        public virtual EnumSetFlg? set_flg { get; set; }
        public virtual EnumDocBundlingFlg? doc_bundling_flg { get; set; }
        public virtual EnumDelvMethod? deliv_method { get; set; }
        public virtual string shipping_sname { get; set; }
        public virtual EnumOrderType? order_type { get; set; }
        public virtual EnumSendPtn send_ptn { get; set; }
        public virtual short? ptn_interval_month { get; set; }
        public virtual short? ptn_day { get; set; }
        public virtual short? ptn_interval_week { get; set; }
        public virtual DayOfWeek? ptn_weekday { get; set; }
        public virtual short? ptn_interval_day { get; set; }
        public virtual string cts_sname { get; set; }

        public virtual int? cost_price { get; set; }
        public virtual string supplier_code { get; set; }
        public virtual EnumWhOrderType wh_order_type { get; set; }

        public virtual int? member_rank { get; set; }
        public virtual string shipping_memo { get; set; }

        public virtual string mall_item_id { get; set; }
        public virtual int? mall_quantity { get; set; }

        public virtual DateTime? after_cancel_date { get; set; }
        public virtual DateTime? cancel_date { get; set; }
        public virtual int? cancel_price { get; set; }

        public virtual int? site_id { get; set; }

        public int GetAmount()
        {
            var actualAmount = amount ?? 0;
            var actualCancelAmount = cancel_amount ?? 0;
            var actualAfterCancelAmount = after_cancel_amount ?? 0;

            return actualAmount - actualCancelAmount - actualAfterCancelAmount;
        }

        /// <summary>
        /// 明細がキャンセルステータスであればtrue
        /// </summary>
        public bool? IsCanceled
        {
            get
            {
                if (this.order_status == EnumOrderStatusType.CANCELED
                || this.order_status == EnumOrderStatusType.CANCELED_AFTER_DELIVER)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
