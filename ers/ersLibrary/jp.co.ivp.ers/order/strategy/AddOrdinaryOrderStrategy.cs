﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.order.strategy
{
    public class AddOrdinaryOrderStrategy
    {
        public void AddOrderContact(ErsOrderRecord orderRecord, int amount)
        {
            var calcService = ErsFactory.ersOrderFactory.GetErsCalcService();

            // 伝票明細更新
            orderRecord.amount += amount;
            calcService.calcOrderRecord(orderRecord);
        }

        public virtual KeyValuePair<string, ErsOrderRecord> GetOrder(string d_no, ErsBaskRecord merchandise, int amount, EnumDelvMethod? deliv_method, int? shippng_pref)
        {
            var parameter = merchandise.GetPropertiesAsDictionary();
            parameter.Remove("id");
            parameter.Remove("intime");

            ErsOrderRecord orderRecord;
            if (merchandise.ds_id.HasValue)
            {
                orderRecord = ErsFactory.ersOrderFactory.GetErsOrderRecordWithId(merchandise.ds_id);
                orderRecord.OverwriteWithParameter(parameter);
            }
            else
            {
                orderRecord = ErsFactory.ersOrderFactory.GetErsOrderRecordWithParameter(parameter);
                orderRecord.d_no = d_no;
                orderRecord.id = merchandise.ds_id;
            }

            orderRecord.amount = amount;
            if (deliv_method.HasValue)
            {
                orderRecord.deliv_method = deliv_method;
            }

            // 海外配送の場合は、出荷待機（外部よりshipping_prefが渡されたときのみ）
            if (shippng_pref.HasValue && shippng_pref == (int)EnumPrefecture.OVERSEAS)
            {
                orderRecord.order_status = EnumOrderStatusType.DELIVER_WAITING;
            }

            // 明細リストに追加
            return new KeyValuePair<string, ErsOrderRecord>(ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(merchandise), orderRecord);
        }
    }
}