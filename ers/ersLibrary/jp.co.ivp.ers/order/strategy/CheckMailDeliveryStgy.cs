﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.order.strategy
{
    public class CheckMailDeliveryStgy
    {
        /// <summary>
        /// 代引きでメール便が選択されているかValidate
        /// </summary>
        /// <param name="pay"></param>
        /// <param name="strRansu"></param>
        public virtual ValidationResult Validate(EnumDelvMethod? deliv_method, EnumPaymentType? pay)
        {
            if (deliv_method == EnumDelvMethod.Mail && pay == EnumPaymentType.CASH_ON_DELIVERY)
            {
                return new ValidationResult(ErsResources.GetMessage("20219"));
            }
            return null;
        }

        /// <summary>
        /// メール便発送が選択された場合は、配送日・時間指定不可
        /// </summary>
        /// <param name="senddate"></param>
        /// <param name="sendtime"></param>
        /// <param name="strRansu"></param>
        public ValidationResult CkSendDateTimeBasketMailDelivery(EnumDelvMethod? deliv_method, DateTime? senddate, int? sendtime)
        {
            if (senddate != null || (sendtime != null && sendtime != 0))
            {
                if (deliv_method == EnumDelvMethod.Mail)
                {
                    return new ValidationResult(ErsResources.GetMessage("20224"));
                }
            }
            return null;
        }
    }
}
