﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.strategy
{
    public class UpdateMemberDataStgy
    {
        public void Update(ErsMember member, int addSale, int AddSaleTimes, DateTime? LastSaleDate = null)
        {
            member.sale = (member.sale ?? 0) + addSale;
            member.sale_times = (member.sale_times ?? 0) + AddSaleTimes;
            if (LastSaleDate.HasValue)
            {
                member.last_sale_date = LastSaleDate;
            }
        }
    }
}
