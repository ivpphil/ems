﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.strategy
{
    abstract public class ValidateNextSendStgy
    {
        protected ManageRegularPatternService service;

        public ValidateNextSendStgy(ManageRegularPatternService service)
        {
            this.service = service;
        }

        internal protected abstract IEnumerable<ValidationResult> Validate(IErsModelBase model);
    }
}