﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order.strategy
{
    public class ValidateSendStgy
    {
        /// <summary>
        /// カート内に定期があり、別お届け先指定の場合はお届け先登録必須。
        /// </summary>
        public ValidationResult ValidateAnotherAddressSave(string fieldKey, EnumSendTo? send, EnumAddressAdd? address_add, int? member_add_id, string strRansu)
        {
            Boolean blnRegExist = ErsFactory.ersOrderFactory.GetIsBasketHasRegularItem().Satisfy(strRansu);
            if (blnRegExist)//定期存在
            {
                //カードの場合のみ判定とする
                if (send == EnumSendTo.ANOTHER_ADDRESS)
                {
                    //カード預けがない場合エラー
                    if (address_add == EnumAddressAdd.NotAdd && member_add_id == null)
                    {
                        return new ValidationResult(ErsResources.GetMessage("20237", ErsResources.GetFieldName(fieldKey)), new[] { fieldKey });
                    }
                }
            }

            return null;
        }
    }
}
