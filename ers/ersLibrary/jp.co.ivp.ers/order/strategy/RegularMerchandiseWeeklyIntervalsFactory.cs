﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.order.strategy
{
    public class RegularMerchandiseWeeklyIntervalsFactory
        : RegularMerchandiseFactory
    {
        public RegularMerchandiseWeeklyIntervalsFactory(ManageRegularPatternService service, IManageRegularPatternDatasource datasource)
            : base(service, datasource)
        {
        }

        public override void SetValueToErsMerchandise(ErsBaskRecord merchandise)
        {
            merchandise.ptn_interval_month = datasource.ptn_interval_month;
            merchandise.ptn_interval_week = datasource.ptn_interval_week;
            merchandise.ptn_weekday = datasource.ptn_weekday;
        }
    }
}