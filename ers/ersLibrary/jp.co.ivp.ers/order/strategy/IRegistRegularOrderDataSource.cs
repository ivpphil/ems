﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.strategy
{
    public interface IRegistRegularOrderDataSource
        : IErsModelBase
    {
        int? card_id { get; set; }
    }
}
