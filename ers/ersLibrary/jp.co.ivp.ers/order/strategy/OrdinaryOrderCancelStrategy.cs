﻿using System;
using System.Data;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using System.Collections.Generic;

namespace jp.co.ivp.ers.order.strategy
{
    public class OrdinaryOrderCancelStrategy
    {
        /// <summary>
        /// 明細キャンセル
        /// </summary>
        /// <param name="order">ErsOrder インスタンス</param>
        /// <param name="member">ErsMember インスタンス</param>
        /// <param name="point">ErsPointHistory インスタンス</param>
        /// <param name="scode">キャンセルする商品番号</param>
        /// <param name="cancel_amount">キャンセルする個数</param>
        public virtual void CancelRecord(ErsOrderRecord orderRecord, int cancel_amount, bool wasBeforeDliver)
        {
            // 指定キャンセル数が個数を上回る
            if (IsOverCancelAmount(orderRecord, orderRecord.scode, cancel_amount))
            {
                throw new ErsException("30102", orderRecord.scode);
            }

            var calcService = ErsFactory.ersOrderFactory.GetErsCalcService();

            // 伝票明細のキャンセル個数を加算
            if (wasBeforeDliver)
            {
                orderRecord.cancel_amount += cancel_amount;
            }
            else
            {
                orderRecord.after_cancel_amount += cancel_amount;
            }

            calcService.calcOrderRecord(orderRecord);
        }

        /// <summary>
        /// キャンセル数オーバーチェック
        /// </summary>
        /// <param name="record">ErsOrderRecord インスタンス</param>
        /// <param name="scode">商品番号</param>
        /// <param name="cancel_amount">キャンセルする個数</param>
        /// <returns>true : 存在する / false : 存在しない</returns>
        protected virtual bool IsOverCancelAmount(ErsOrderRecord record, string scode, int cancel_amount)
        {
            return (record.GetAmount() - cancel_amount < 0);
        }
    }
}