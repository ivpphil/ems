﻿using System;
using System.Data;
using jp.co.ivp.ers.mvc;
using System.Collections.Generic;

namespace jp.co.ivp.ers.order.strategy
{
    public class OtherModifyOrderPaymentStatusStrategy
    {
        /// <summary>
        /// ステータスセット（入金ステータス指定）
        /// </summary>
        /// <param name="order">ErsOrder インスタンス</param>
        /// <param name="order_payment_status">入金ステータス</param>
        public virtual void SetStatus(ErsOrder order, IEnumerable<ErsOrderRecord> orderRecords, EnumOrderPaymentStatusType order_payment_status)
        {
            // 変更する必要があるかどうか
            if (order.order_payment_status == EnumOrderPaymentStatusType.PAID)
            {
                return;
            }

            if (order_payment_status == EnumOrderPaymentStatusType.PAID)
            {
                if (order.total > order.paid_price)
                {
                    return;
                }
                else
                {
                    var order_status = ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy().ObtainFrom(orderRecords);
                    if (order_status != EnumOrderStatusType.NEW_ORDER && order_status != EnumOrderStatusType.DELIVERED && order_status != EnumOrderStatusType.DELIVER_WAITING)
                    {
                        return;
                    }
                }
            }

            // 入金ステータスセット
            order.order_payment_status = order_payment_status;
        }
    }
}
