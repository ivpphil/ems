﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.strategy
{
    public class ObtainErsOrderStatusStgy
    {
        /// <summary>
        /// obtain order header status.
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public EnumOrderStatusType? ObtainFrom(IEnumerable<ErsOrderRecord> orderRecords)
        {
            var countDictionary = this.GetCountDictionary();

            this.CountStatus(orderRecords, countDictionary);

            return this.GetOrderHeaderStatus(countDictionary);
        }

        /// <summary>
        /// determins order header status.
        /// </summary>
        /// <param name="countDictionary"></param>
        /// <returns></returns>
        protected virtual EnumOrderStatusType? GetOrderHeaderStatus(Dictionary<EnumOrderStatusType, int> countDictionary)
        {
            //15：出荷待機
            if (countDictionary[EnumOrderStatusType.DELIVER_WAITING] > 0)
                return EnumOrderStatusType.DELIVER_WAITING;
            //10：新着注文
            else if (countDictionary[EnumOrderStatusType.NEW_ORDER] > 0)
                return EnumOrderStatusType.NEW_ORDER;
            //20：出荷指示済み
            else if (countDictionary[EnumOrderStatusType.DELIVER_REQUEST] > 0)
                return EnumOrderStatusType.DELIVER_REQUEST;
            //30：配送済
            else if (countDictionary[EnumOrderStatusType.DELIVERED] > 0)
                return EnumOrderStatusType.DELIVERED;
            //999：出荷前キャンセル
            else if (countDictionary[EnumOrderStatusType.CANCELED] > 0)
                return EnumOrderStatusType.CANCELED;
            //888：出荷後キャンセル
            else if (countDictionary[EnumOrderStatusType.CANCELED_AFTER_DELIVER] > 0)
                return EnumOrderStatusType.CANCELED_AFTER_DELIVER;
            else
                return null;
        }

        /// <summary>
        /// count status of order record in order
        /// </summary>
        /// <param name="order"></param>
        /// <param name="countDictionary"></param>
        protected virtual void CountStatus(IEnumerable<ErsOrderRecord> orderRecords, Dictionary<EnumOrderStatusType, int> countDictionary)
        {
            foreach (var orderRecord in orderRecords)
            {
                countDictionary[orderRecord.order_status.Value]++;
            }
        }

        /// <summary>
        /// create dictionary that contains count of orderrecord status
        /// </summary>
        /// <returns></returns>
        protected virtual Dictionary<EnumOrderStatusType, int> GetCountDictionary()
        {
            var countDictionary = new Dictionary<EnumOrderStatusType, int>();

            var payList = ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().SelectAsList(false);
            foreach (var enumValue in payList)
                countDictionary.Add((EnumOrderStatusType)enumValue["value"], 0);
            return countDictionary;
        }
    }
}
