﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.order.strategy
{
    public class RegistRegularOrderStgy
    {
        /// <summary>
        /// regist ErsOrderRecord.(this method using when buy.)
        /// </summary>
        /// <param name="inputParameters"></param>
        /// <param name="ersbasket"></param>
        /// <param name="orderIntegrated"></param>
        public virtual void Regist(IRegistRegularOrderDataSource datasource, ErsBasket ersbasket, ErsOrderIntegrated orderIntegrated, int? regular_sendtime)
        {
            var regularOrder = ErsFactory.ersOrderFactory.GetRegularOrderWithParameter(datasource, ersbasket, regular_sendtime);

            this.RegistRecord(regularOrder);
            orderIntegrated.SetRegularDetailID(regularOrder);
        }

        /// <summary>
        /// regist to the DB.
        /// </summary>
        /// <param name="inputParameters"></param>
        /// <param name="ersbasket"></param>
        /// <returns></returns>
        protected virtual void RegistRecord(ErsRegularOrder regularOrder)
        {
            var regularRepository = ErsFactory.ersOrderFactory.GetErsRegularOrderRepository();
            regularRepository.Insert(regularOrder, true);
        }

    }
}
