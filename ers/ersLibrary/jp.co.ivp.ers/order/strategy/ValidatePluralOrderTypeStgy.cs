﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.order.strategy
{
    public class ValidatePluralOrderTypeStgy
    {
        public virtual void CheckForCartIn(ErsBaskRecord merchandise, string mcode, string ransu, string d_no = null, bool isReload = false, bool isOrderUpdate = false)
        {
            this.CheckMerchandise(merchandise.scode, merchandise.plural_order_type, mcode, ransu, d_no, isReload, isOrderUpdate);
        }

        private void CheckMerchandise(string scode, EnumPluralOrderType? plural_order_type, string mcode, string ransu, string d_no, bool isReload, bool isOrderUpdate)
        {
            if (plural_order_type == EnumPluralOrderType.NoLimit)
            {
                return;
            }

            //カートの複数回購入制限商品数
            var pastPurchasedQuantity = ErsFactory.ersBasketFactory.GetCountPluralOrderAmountSpec().GetCountData(scode, ransu);

            //過去の購入がないかをチェック(伝票修正時はチェックしない)
            // Check if there is another order that containing the product.(Do not check this when this process is on order update.)
            if (!isOrderUpdate && mcode.HasValue())
            {
                pastPurchasedQuantity += ErsFactory.ersOrderFactory.GetCountPurchasedPluralOrderAmountSpec().GetCountData(scode, mcode, d_no, ErsFactory.ersUtilityFactory.getSetup().site_id);
            }

            if ((isReload && pastPurchasedQuantity > (int?)plural_order_type) ||
                (!isReload && pastPurchasedQuantity >= (int?)plural_order_type))
            {
                var sname = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(scode).sname;

                throw new ErsException("universal", ErsResources.GetMessage("20209", new[] { sname }));
            }
        }

        /// <summary>
        /// カートの複数回購入制限商品数
        /// </summary>
        /// <param name="ransu"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        private IList<ErsSetMerchandise> GetSetItem(string scode)
        {
            var setRep = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseRepository();
            var setCriteria = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseCriteria();

            setCriteria.parent_scode = scode;
            return setRep.Find(setCriteria);
        }
    }
}
