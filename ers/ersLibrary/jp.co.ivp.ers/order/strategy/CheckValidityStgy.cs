﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order.strategy
{
    public class CheckValidityStgy
    {
        public virtual ValidationResult Check(int? validity_y, int? validity_m)
        {
            if (validity_y == null || validity_m == null)
            {
                return null;
            }

            //check date format
            DateTime validity;
            if (DateTime.TryParse(validity_y + "/" + validity_m + "/01", out validity))
            {
                if (DateTime.Now < validity.AddMonths(1))
                {
                    //invalid
                    return null;
                }
            }

            return new ValidationResult(ErsResources.GetMessage("20206"), new[] { "validity_y" });
        }
    }
}
