﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order.strategy
{
    public class ObtainNewSubd_noStgy
    {
        public string Obtain(IEnumerable<ErsOrderRecord> orderRecords)
        {
            var maxSubd_no = 0;
            foreach (var objOrderRecord in orderRecords)
            {
                if (!string.IsNullOrEmpty(objOrderRecord.subd_no))
                {
                    var subd_no = Convert.ToInt32(objOrderRecord.subd_no);
                    if (maxSubd_no < subd_no)
                        maxSubd_no = subd_no;
                }
            }
            return (maxSubd_no + 1).ToString("D2");
        }
    }
}
