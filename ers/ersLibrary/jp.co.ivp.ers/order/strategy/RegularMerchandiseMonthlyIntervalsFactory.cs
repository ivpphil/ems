﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.order.strategy
{
    public class RegularMerchandiseMonthlyIntervalsFactory
        : RegularMerchandiseFactory
    {
        public RegularMerchandiseMonthlyIntervalsFactory(ManageRegularPatternService service, IManageRegularPatternDatasource datasource)
            : base(service, datasource)
        {
        }

        public override void SetValueToErsMerchandise(ErsBaskRecord merchandise)
        {
            merchandise.ptn_interval_month = datasource.ptn_interval_month;
            merchandise.ptn_day = datasource.ptn_day;
            merchandise.next_date = datasource.next_date;
        }
    }
}