﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.order.strategy
{
    public class RegistSetMerchandiseStgy
    {
        /// <summary>
        /// If this order contains a set bill statement, then insert the set bill statement to db.
        /// </summary>
        /// <param name="order"></param>
        public void Regist(IEnumerable<ErsOrderRecord> orderRecords)
        {
            //購入物にセット商品あればセット明細マスタにインサート
            foreach (var orderRecord in orderRecords)
            {
                if (orderRecord.set_flg == EnumSetFlg.IsSet)
                {
                    //子商品リスト取得
                    var DecreaseSetItemList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(orderRecord.scode);

                    //insert
                    foreach (ErsSetMerchandise setItem in DecreaseSetItemList)
                    {
                        if (orderRecord.order_type == EnumOrderType.Subscription)
                            setItem.regular_price = setItem.regular_first_price;

                        ErsDsSet dsSet = ErsFactory.ersOrderFactory.GetErsDsSetWithParameter(setItem, orderRecord);
                        dsSet.amount *= orderRecord.amount;
                        var repo = ErsFactory.ersOrderFactory.getErsDsSetRepository();
                        repo.Insert(dsSet);
                    }
                }
            }
        }

        /// <summary>
        /// 伝票編集時の追加インサート
        /// </summary>
        /// <param name="orderRecords"></param>
        public void UpRegist(ErsOrderRecord orderRecords)
        {
            if (orderRecords.set_flg == EnumSetFlg.IsSet)
            {

                //子商品リスト取得
                var DecreaseSetItemList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(orderRecords.scode);

                //insert
                foreach (ErsSetMerchandise setItem in DecreaseSetItemList)
                {

                    ErsDsSet dsSet = ErsFactory.ersOrderFactory.GetErsDsSetWithParameter(setItem, orderRecords);
                    dsSet.amount *= orderRecords.amount;
                    var repo = ErsFactory.ersOrderFactory.getErsDsSetRepository();
                    repo.Insert(dsSet);
                }
            }
        }


        public void RegistRegularBatch(IEnumerable<ErsOrderRecord> orderRecords)
        {
            //購入物にセット商品あればセット明細マスタにインサート
            foreach (var orderRecord in orderRecords)
            {

                if (orderRecord.set_flg == EnumSetFlg.IsSet)
                {
                    //子商品リスト取得
                    var DecreaseSetItemList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(orderRecord.scode);

                    //insert
                    foreach (ErsSetMerchandise setItem in DecreaseSetItemList)
                    {

                        var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
                        var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
                        groupCriteria.scode = setItem.scode;
                        var merchandiseList = groupRepository.FindGroupBaseItemList(groupCriteria);
                        if (merchandiseList.Count() == 0)
                        {
                            continue;
                        }
                        var merchandise = merchandiseList.First();

                        //定期バッチの場合は複数回購入商品は除外する
                        if (merchandise.plural_order_type == EnumPluralOrderType.NoLimit)
                        {
                            ErsDsSet dsSet = ErsFactory.ersOrderFactory.GetErsDsSetWithParameter(setItem, orderRecord);
                            dsSet.amount *= orderRecord.amount;
                            var repo = ErsFactory.ersOrderFactory.getErsDsSetRepository();
                            repo.Insert(dsSet);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 数量の変更更新
        /// </summary>
        /// <param name="orderRecords"></param>
        /// <param name="new_amount"></param>
        public void UpdateAmount(ErsOrderRecord orderRecords, int new_amount)
        {
            if (orderRecords.set_flg == EnumSetFlg.IsSet)
            {
                var cri = ErsFactory.ersOrderFactory.getErsDsSetCriteria();
                var rep = ErsFactory.ersOrderFactory.getErsDsSetRepository();

                cri.d_no = orderRecords.d_no;
                cri.ds_id = orderRecords.id;

                var dsSetList = rep.Find(cri);

                if (dsSetList == null || dsSetList.Count == 0)
                {
                    throw new ErsException("10200");
                }

                //insert
                foreach (ErsDsSet old_dsSet in dsSetList)
                {
                    ErsDsSet new_dsSet = ErsFactory.ersOrderFactory.GetErsDsSet();
                    new_dsSet.OverwriteWithParameter(old_dsSet.GetPropertiesAsDictionary());

                    new_dsSet.amount = new_amount * old_dsSet.amount;
                    rep.Update(old_dsSet,new_dsSet);
                }
            }
        }
    }
}
