﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.order.strategy
{
    public class AddBatchOrderStrategy
    {
        public virtual KeyValuePair<string, ErsOrderRecord> GetOrderRecord(string d_no, ErsRegularOrderRecord regularOrderRecord)
        {
            var parameter = regularOrderRecord.GetPropertiesAsDictionary();

            parameter.Remove("id");
            parameter.Remove("intime");

            var orderRecord = ErsFactory.ersOrderFactory.GetErsOrderRecordWithParameter(parameter);

            orderRecord.d_no = d_no;
            orderRecord.id = null;
            orderRecord.intime = null;
            orderRecord.utime = null;
            orderRecord.regular_detail_id = new[] { regularOrderRecord.id.Value };

            orderRecord.order_type = EnumOrderType.Subscription;

            // 明細リストに追加
            return new KeyValuePair<string, ErsOrderRecord>(ErsFactory.ersOrderFactory.GetCreateRegularOrderRecordKeyStgy().GetKey(regularOrderRecord), orderRecord);
        }
    }
}