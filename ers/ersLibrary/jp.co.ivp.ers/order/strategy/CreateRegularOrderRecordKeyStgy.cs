﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order.strategy
{
    public class CreateRegularOrderRecordKeyStgy
    {
        /// <summary>
        /// Gets Key of ErsOrderRecord
        /// </summary>
        /// <returns></returns>
        public string GetKey(ErsRegularOrderRecord orderRecord)
        {
            return orderRecord.scode
               + "_" + orderRecord.price;
        }
    }
}
