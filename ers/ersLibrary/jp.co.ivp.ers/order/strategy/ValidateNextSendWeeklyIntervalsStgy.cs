﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order.strategy
{
    public class ValidateNextSendWeeklyIntervalsStgy
        : ValidateNextSendStgy
    {
        public ValidateNextSendWeeklyIntervalsStgy(ManageRegularPatternService service)
            : base(service)
        {
        }

       protected internal override IEnumerable<ValidationResult> Validate(IErsModelBase model)
        {

            var datasource = model as IManageRegularPatternDatasource;

            if (datasource == null)
                throw new Exception(model.GetType().Name + " has to implement IManageRegularPatternDatasource.");

            //○ヶ月ごと第△週目□曜日
            yield return model.CheckRequired("ptn_interval_month");
            yield return model.CheckRequired("ptn_interval_week");
            yield return model.CheckRequired("ptn_weekday");
        }
    }
}