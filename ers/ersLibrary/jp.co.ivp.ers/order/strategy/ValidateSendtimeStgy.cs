﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order.strategy
{
    public class ValidateSendtimeStgy
    {
        public virtual IEnumerable<ValidationResult> Validate(int? sendtime)
        {
            var fieldKey = "sendtime";
            //yield return model.CheckRequired(fieldKey);

            if (sendtime == null)
                yield break;

            if (!ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().ExistValue(sendtime.Value))
            {
                yield return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName(fieldKey)), new[] { fieldKey });
            }
        }
    }
}
