﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.strategy
{
    public class ValidateNextSendDailyIntervalsStgy
        : ValidateNextSendStgy
    {
        public ValidateNextSendDailyIntervalsStgy(ManageRegularPatternService service)
            : base(service)
        {
        }

        protected internal override IEnumerable<ValidationResult> Validate(IErsModelBase model)
        {
            var datasource = model as IManageRegularPatternDatasource;

            if (datasource == null)
                throw new Exception(model.GetType().Name + " has to implement IManageRegularPatternDatasource.");

            //○日ごと
            yield return model.CheckRequired("ptn_interval_day");

            //Checks value
            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("ptn_interval_day", EnumCommonNameType.PtnIntervalDay, (int?)datasource.ptn_interval_day);

            if (datasource.next_date != null && datasource.next_date != DateTime.MinValue)
            {
                var fromDate = ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetRegularFromDate(DateTime.Now);

                if (datasource.next_date < fromDate)
                {
                    yield return new ValidationResult(
                        ErsResources.GetMessage("10045", ErsResources.GetFieldName("senddate"), fromDate.Value.ToString("yyyy/MM/dd")),
                        new[] { "next_date" });
                }

                var toDate = DateTime.Now.AddDays(ErsFactory.ersUtilityFactory.getSetup().sendday_count);

                if (datasource.next_date > toDate)
                    yield return new ValidationResult(ErsResources.GetMessage("10046", ErsResources.GetFieldName("senddate"), toDate.ToString("yyyy/MM/dd"))
                        , new[] { "senddate" });
            }
        }
    }
}