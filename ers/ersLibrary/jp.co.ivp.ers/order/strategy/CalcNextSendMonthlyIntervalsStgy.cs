﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.strategy
{
    public class CalcNextSendMonthlyIntervalsStgy
        : CalcNextSendStgy
    {
        public override DateTime CalculateBase(IManageRegularPatternDatasource datasource, DateTime baseDate)
        {
            baseDate = baseDate.AddMonths(datasource.ptn_interval_month.Value);
            return CalculateNextSend(baseDate, datasource);
        }

        public override DateTime CalculateActual(IManageRegularPatternDatasource datasource, DateTime baseDate, EnumWeekendOperation weekend_operation)
        {
            return CalcNextSendStgy.CalculateWeekendOperation(baseDate, weekend_operation);
        }

        public override DateTime? CalculateFirstTime(IManageRegularPatternDatasource datasource, DateTime baseDate)
        {
            DateTime resultDate;

            var fromDate = ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetRegularFromDate(baseDate);
            resultDate = CalculateNextSend(fromDate, datasource);

            if (resultDate < fromDate)
            {
                resultDate = CalculateNextSend(resultDate.AddMonths(1), datasource);
            }

            return resultDate;
        }

        protected virtual DateTime CalculateNextSend(DateTime? baseDate, IManageRegularPatternDatasource datasource)
        {
            DateTime resultDate;
            if (!DateTime.TryParse(baseDate.Value.ToString("yyyy/MM/" + datasource.ptn_day), out resultDate))
                resultDate = DateTime.Parse(baseDate.Value.AddMonths(1).ToString("yyyy/MM/01")).AddDays(-1);
            return resultDate;
        }
    }
}