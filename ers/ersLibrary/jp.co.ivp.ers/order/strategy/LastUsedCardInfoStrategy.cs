﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.order.strategy
{
    public class LastUsedCardInfoStrategy
    {
        /// <summary>
        /// 過去伝票の合計額比較
        /// （増額時はエラーメッセージ）
        /// </summary>
        /// <param name="order">現在伝票</param>
        /// <param name="d_no"></param>
        public virtual ValidationResult LastUsedCardInfoValidate(ErsOrder order, string d_no)
        {
            var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var orderCriteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

            orderCriteria.d_no = d_no;
            var old_order = orderRepository.Find(orderCriteria);
            if (old_order.Count != 1)
            {
                //該当伝票無し
                return new ValidationResult(ErsResources.GetMessage("29003", d_no), new[] { "order_d_no" });
            }

            //過去合計より増額した場合False
            if (old_order.First().total < order.total && order.pay == EnumPaymentType.CREDIT_CARD)
            {
                //増額時はカード情報の入力必須
                return new ValidationResult(ErsResources.GetMessage("20230"), new[] { "order_d_no" });
            }

            return null;
        }


        /// <summary>
        /// 前回使用カード情報判定
        /// </summary>
        /// <param name="d_no"></param>
        /// <returns>true:前回預け無しでカード使用</returns>
        public virtual bool CheckLastOrderCardStatus(string d_no)
        {
            var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var orderCriteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

            orderCriteria.d_no = d_no;
            var old_order = orderRepository.Find(orderCriteria);
            if (old_order.Count != 1)
            {
                //該当伝票無し
                throw new ErsException("29003", d_no);
            }

            var crnt_order = old_order.First();
            //前回カード払いでカードシーケンス無しの場合
            return (crnt_order.pay == EnumPaymentType.CREDIT_CARD);
        }
    }
}
