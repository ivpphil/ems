﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.order.strategy
{
    public class RegistOrderRecordStatusHistoryStgy
    {
        /// <summary>
        /// 登録 [Regist]
        /// </summary>
        /// <param name="old_record">ErsOrderRecord</param>
        /// <param name="new_record">ErsOrderRecord</param>
        /// <param name="old_order">ErsOrderContainer</param>
        /// <param name="new_order">ErsOrderContainer</param>
        /// <param name="is_batch">bool</param>
        public virtual void Regist(ErsOrderRecord old_record, ErsOrderRecord new_record,
            ErsOrderContainer old_order, ErsOrderContainer new_order, bool is_batch = false)
        {
            var old_records = old_order.OrderRecords.Select(e => e.Value);
            var new_records = new_order.OrderRecords.Select(e => e.Value);
            this.Regist(old_record, new_record, old_order.OrderHeader, old_records, new_order.OrderHeader, new_records, is_batch);
        }

        /// <summary>
        /// 登録 [Regist]
        /// </summary>
        /// <param name="old_record">ErsOrderRecord</param>
        /// <param name="new_record">ErsOrderRecord</param>
        /// <param name="old_header">ErsOrder</param>
        /// <param name="old_records">IEnumerable<ErsOrderRecord></param>
        /// <param name="new_header">ErsOrder</param>
        /// <param name="new_records">IEnumerable<ErsOrderRecord></param>
        /// <param name="is_batch">bool</param>
        public virtual void Regist(ErsOrderRecord old_record, ErsOrderRecord new_record,
            ErsOrder old_header, IEnumerable<ErsOrderRecord> old_records,
            ErsOrder new_header, IEnumerable<ErsOrderRecord> new_records, bool is_batch = false)
        {
            // 受注ステータスに変動があった場合のみ登録を行う [Regist it only when order status includes a change]
            if (old_record.order_status == new_record.order_status)
            {
                return;
            }

            var repository = ErsFactory.ersOrderFactory.GetErsOrderRecordStatusHistoryRepository();
            var entity = ErsFactory.ersOrderFactory.GetErsOrderRecordStatusHistory();

            // 値セット [Set values]
            if (is_batch)
            {
                entity.log_user_id = "batch";
            }
            else
            {
                entity.log_user_id = ErsContext.sessionState.Get("log_user_id");
            }
            entity.tdate = DateTime.Now;
            entity.d_intime = new_header.intime;
            entity.d_no = new_header.d_no;
            entity.ds_id = new_record.id;
            entity.old_d_status = old_records.Min(e => e.order_status);
            entity.new_d_status = new_records.Min(e => e.order_status);
            entity.old_order_status = old_record.order_status;
            entity.new_order_status = new_record.order_status;
            entity.old_d_subtotal = old_header.subtotal;
            entity.old_d_tax = old_header.tax;
            entity.old_d_carriage = old_header.carriage;
            entity.old_d_p_service = old_header.p_service;
            entity.old_d_coupon_discount = old_header.coupon_discount;
            entity.old_d_etc = old_header.etc;
            entity.old_d_total = old_header.total;
            entity.old_ds_price = old_record.price;
            entity.old_ds_amount = old_record.amount;
            entity.old_ds_cancel_amount = old_record.cancel_amount;
            entity.old_ds_after_cancel_amount = old_record.after_cancel_amount;
            entity.old_ds_total = old_record.total;
            entity.new_d_subtotal = new_header.subtotal;
            entity.new_d_tax = new_header.tax;
            entity.new_d_carriage = new_header.carriage;
            entity.new_d_p_service = new_header.p_service;
            entity.new_d_coupon_discount = new_header.coupon_discount;
            entity.new_d_etc = new_header.etc;
            entity.new_d_total = new_header.total;
            entity.new_ds_price = new_record.price;
            entity.new_ds_amount = new_record.amount;
            entity.new_ds_cancel_amount = new_record.cancel_amount;
            entity.new_ds_after_cancel_amount = new_record.after_cancel_amount;
            entity.new_ds_total = new_record.total;
            entity.active = EnumActive.Active;

            // 登録 [Insert]
            repository.Insert(entity, true);
        }
    }
}
