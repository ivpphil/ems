﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.strategy
{
    public class ObtainEtcAmountStgy
    {
        public virtual int Obtain(EnumPaymentType? pay, int subtotal, int? site_id)
        {
            if (!this.CheckPayEtcIsNeeded(pay, site_id))
            {
                return 0;
            }

            var repository = ErsFactory.ersOrderFactory.GetErsEtcRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsEtcCriteria();
            criteria.pay = pay;
            criteria.more_lessEqual = subtotal;
            criteria.down_graterThan = subtotal;
            if (!site_id.HasValue)
            {
                site_id = (int)EnumSiteId.COMMON_SITE_ID;
            }

            criteria.site_id = site_id.Value;

            var listEtc = repository.Find(criteria);

            if (listEtc.Count == 0)
            {
                return 0;
            }

            return listEtc.First().price.Value;
        }

        private bool CheckPayEtcIsNeeded(EnumPaymentType? pay, int? site_id)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsPayRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsPayCriteria();
            criteria.id = pay;
            criteria.site_id = site_id;

            var listPay = repository.Find(criteria);
            if (listPay.Count != 1)
            {
                return false;
            }

            return (listPay.First().etc_flg == EnumOnOff.On);
        }
    }
}
