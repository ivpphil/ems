﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order.strategy
{
    public class UpdateMemberPointStgy
    {
        public void Decrease(string mcode, int p_service, string reason, string d_no, DateTime? odate, int site_id)
        {
            if (p_service == 0)
            {
                return;
            }

            var point = this.GetDecreasedErsPointHistory(mcode, p_service, reason, d_no, odate, site_id);

            var pointRepository = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryRepository();
            pointRepository.Insert(point, true);
        }

        /// <summary>
        /// Based on the input value , to get the class ErsPointHistory for decrease points
        /// </summary>
        /// <param name="member"></param>
        /// <param name="decreasePoints"></param>
        /// <param name="reason"></param>
        /// <returns>new instance of ErsPointHistory</returns>
        protected virtual ErsPointHistory GetDecreasedErsPointHistory(string mcode, int decreasePoints, string reason, string d_no, DateTime? odate, int site_id)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var last_point = 0;

            if (setup.member_centralization)
            {
                last_point = ErsFactory.ersMemberFactory.GetObtainMemberPointStgy().GetPointNotCache(mcode,site_id);
            }
            else
            {
                last_point = ErsFactory.ersMemberFactory.GetObtainMemberPointStgy().GetPointNotCache(mcode, site_id);
            }

            if (last_point - decreasePoints < 0)
                throw new ErsException("20203");

            var pointHistory = ErsFactory.ersPointHistoryFactory.GetErsPointHistory();
            pointHistory.dt = DateTime.Now;
            pointHistory.mcode = mcode;
            pointHistory.last_p = last_point;
            pointHistory.now_p = 0-decreasePoints;
            pointHistory.total_p = last_point - decreasePoints;
            pointHistory.reason = reason;
            pointHistory.d_no = d_no;
            pointHistory.odate = odate;
            pointHistory.site_id = site_id;


            return pointHistory;
        }

        public void Increase(string mcode, int p_service, string reason, string d_no, DateTime? odate ,int site_id)
        {
            if (p_service == 0)
            {
                return;
            }

            var point = this.GetIncreasedErsPointHistory(mcode, p_service, reason, d_no, odate, site_id);

            var pointRepository = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryRepository();
            pointRepository.Insert(point, true);
        }

        /// <summary>
        /// Based on the input value , to get the class ErsPointHistory  for increase points
        /// </summary>
        /// <param name="member"></param>
        /// <param name="increasePoints"></param>
        /// <param name="reason"></param>
        /// <returns>new instance of ErsPointHistory</returns>
        protected virtual ErsPointHistory GetIncreasedErsPointHistory(string mcode, int increasePoints, string reason, string d_no, DateTime? odate, int site_id)
        {
            var last_point = ErsFactory.ersMemberFactory.GetObtainMemberPointStgy().GetPointNotCache(mcode, site_id);

            var pointHistory = ErsFactory.ersPointHistoryFactory.GetErsPointHistory();
            pointHistory.dt = DateTime.Now;
            pointHistory.mcode = mcode;
            pointHistory.last_p = last_point;
            pointHistory.now_p = increasePoints;
            pointHistory.total_p = last_point + increasePoints;
            pointHistory.reason = reason;
            pointHistory.d_no = d_no;
            pointHistory.odate = odate;
            pointHistory.site_id = site_id;

            return pointHistory;
        }
    }
}
