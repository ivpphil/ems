﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.order.strategy
{
    public class CheckCampaignAlreadyPurchasedStgy
    {
        public virtual void Validate(string mcode, string ccode, string scode)
        {
            if (!this.Check(mcode, ccode, scode))
            {
                throw new ErsException("20209", ErsResources.GetFieldName("campaign_products"));
            }
        }

        public virtual ValidationResult ValidateAndGetResult(string mcode, string ccode, string scode, string d_no = null)
        {
            if (!this.Check(mcode, ccode, scode, d_no))
            {
                return new ValidationResult(ErsResources.GetMessage("20209", ErsResources.GetFieldName("campaign_products")));
            }

            return null;
        }

        public virtual Boolean Check(string mcode, string ccode, string scode, string d_no = null)
        {
            var d_master_t_cri = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            var d_master_t_rep = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            d_master_t_cri.mcode = mcode;
            d_master_t_cri.ccode = ccode;
            if (scode != null)
            {
                d_master_t_cri.scode = scode;
            }
            if (d_no != null)
            {
                d_master_t_cri.d_no_not_equals = d_no;
            }

            return (d_master_t_rep.GetRecordCount(d_master_t_cri) == 0);
        }


        public virtual void ValidateBasket(string strRansu, string ccode)
        {
            if (this.CheckBasket(strRansu, ccode))
            {
                throw new ErsException("20221");
            }
        }

        public virtual Boolean CheckBasket(string strRansu, string ccode)
        {
            var baskCriteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
            var baskRep = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();

            baskCriteria.ransu = strRansu;

            baskCriteria.ccode_not_equal = null;

            //カートの全件数
            long baskAllCnt = baskRep.GetRecordCount(baskCriteria);

            if (baskAllCnt != 0)
            {
                return true;
            }
            return false;
        }
    }
}
