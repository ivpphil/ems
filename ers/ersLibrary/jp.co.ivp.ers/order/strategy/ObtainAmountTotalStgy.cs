﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order.strategy
{
    public class ObtainAmountTotalStgy
    {
        public int Obtain(IEnumerable<ErsOrderRecord> orderRecords, bool acceptZeroLess = false)
        {
            if (orderRecords == null)
            {
                return 0;
            }

            var ret = 0;
            foreach (var item in orderRecords)
            {
                if (item.price > 0 || (acceptZeroLess && item.price <= 0))
                    ret += item.GetAmount();
            }
            return ret;
        }
    }
}
