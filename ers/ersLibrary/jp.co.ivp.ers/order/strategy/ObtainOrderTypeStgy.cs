﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order.strategy
{
    public class ObtainOrderTypeStgy
    {
        public virtual EnumOrderType? Obtain(IEnumerable<ErsOrderRecord> orderRecords)
        {
            bool containsUsually = false;
            bool containsSubscription = false;
            foreach (var item in orderRecords)
            {
                switch (item.order_type)
                {
                    case EnumOrderType.Usually:
                        containsUsually = true;
                        break;
                    case EnumOrderType.Subscription:
                        containsSubscription = true;
                        break;
                }
            }

            if (containsUsually && containsSubscription)
            {
                return EnumOrderType.BothUsuallyAndSubscription;
            }
            else if (containsUsually)
            {
                return EnumOrderType.Usually;
            }
            else
            {
                return EnumOrderType.Subscription;
            }
        }
    }
}
