﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order.strategy
{
    public class CheckPointsStgy
    {
        public virtual ValidationResult CheckPoints(int? available, int? ent_point, int? p_service, bool point_recalc)
        {
            if (available < ent_point)
            {
                return new ValidationResult(ErsResources.GetMessage("20203"));
            }
            else if (!point_recalc && (p_service != ent_point))
            {
                return new ValidationResult(ErsResources.GetMessage("20202"));
            }
            return null;
        }
    }
}
