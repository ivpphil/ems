﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order.strategy
{
    public class ValidateSenddateStgy
    {
        public virtual IEnumerable<ValidationResult> Validate(DateTime? baseTime, DateTime? inputValue)
        {
            if (baseTime == null || inputValue == null)
                yield break;

            var fromDate = ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetFromDate(baseTime.Value);

            if (inputValue < fromDate)
                yield return new ValidationResult(ErsResources.GetMessage("10045", ErsResources.GetFieldName("senddate"), fromDate.Value.ToString("yyyy/MM/dd"))
                    , new[] { "senddate" });

            var toDate = baseTime.Value.AddDays(ErsFactory.ersUtilityFactory.getSetup().sendday_count);

            if (inputValue > toDate)
                yield return new ValidationResult(ErsResources.GetMessage("10046", ErsResources.GetFieldName("senddate"), toDate.ToString("yyyy/MM/dd"))
                    , new[] { "senddate" });

            if (ErsFactory.ersOrderFactory.GetErsCalenderWithCloseDate(inputValue.Value.Date) != null)
            {
                yield return new ValidationResult(ErsResources.GetMessage("20242"), new[] { "senddate" });
            }

            yield break;
        }
    }
}
