﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.strategy
{
    public abstract class CalcNextSendStgy
    {

        public abstract DateTime CalculateBase(IManageRegularPatternDatasource datasource, DateTime baseDate);

        public abstract DateTime CalculateActual(IManageRegularPatternDatasource datasource, DateTime baseDate, EnumWeekendOperation weekend_operation);

        public abstract DateTime? CalculateFirstTime(IManageRegularPatternDatasource datasource, DateTime baseDate);

        public static DateTime CalculateWeekendOperation(DateTime baseDate, EnumWeekendOperation weekend_operation)
        {
            DateTime resultDate;
            if (baseDate.DayOfWeek == DayOfWeek.Saturday)
            {
                switch (weekend_operation)
                {
                    case EnumWeekendOperation.AHEAD_OF_SCHEDULE:
                        resultDate = baseDate.AddDays(-1);
                        break;
                    case EnumWeekendOperation.AFTER_OF_SCHEDULE:
                        resultDate = baseDate.AddDays(2);
                        break;
                    default:
                        resultDate = baseDate;
                        break;
                }
            }
            else if (baseDate.DayOfWeek == DayOfWeek.Sunday)
            {
                switch (weekend_operation)
                {
                    case EnumWeekendOperation.AHEAD_OF_SCHEDULE:
                        resultDate = baseDate.AddDays(-2);
                        break;
                    case EnumWeekendOperation.AFTER_OF_SCHEDULE:
                        resultDate = baseDate.AddDays(1);
                        break;
                    default:
                        resultDate = baseDate;
                        break;
                }
            }
            else
            {
                resultDate = baseDate;
            }

            while (true)
            {
                if ((resultDate.DayOfWeek == DayOfWeek.Saturday || resultDate.DayOfWeek == DayOfWeek.Sunday)
                    && (weekend_operation != EnumWeekendOperation.NONE))
                {
                    resultDate = resultDate.AddDays(1);
                    continue;
                }

                //配送日カレンダーを取得
                var repository = ErsFactory.ersOrderFactory.GetErsCalendarRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsCalendarCriteria();
                criteria.close_date = resultDate;
                if (repository.GetRecordCount(criteria) == 0)
                {
                    break;
                }
                //#19292 後ろ倒しから前倒しに変更
                //resultDate = resultDate.AddDays(1);
                resultDate = resultDate.AddDays(-1);
            }

            return resultDate;
        }
    }
}