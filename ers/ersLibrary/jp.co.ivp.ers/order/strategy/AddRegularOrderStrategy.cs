﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.order.strategy
{
    /// <summary>
    /// regular_detail_t用のデータを作成し、ErsRegulrOrderにAddする。
    /// </summary>
    public class AddRegularOrderStrategy
    {
        public virtual void AddOrder(ErsRegularOrder order, ErsBaskRecord merchandise)
        {
            var orderRecord = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordWithParameteres(order.id, order, merchandise);

            if (merchandise.regular_price.HasValue)
            {
                orderRecord.price = merchandise.regular_price.Value;
                orderRecord.total = merchandise.regular_price.Value * merchandise.amount;
            }

            //Nullの場合はNoneを入れる。
            if (orderRecord.weekend_operation == null)
                orderRecord.weekend_operation = EnumWeekendOperation.NONE;

            //Calculate the next shipping date
            var regularPatternService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(merchandise.send_ptn.Value);

            if (orderRecord.next_date_base == null)
            {
                //定期用の最短お届けを設定する
                orderRecord.last_date_base = ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetRegularFromDate(DateTime.Now);

                orderRecord.last_date = regularPatternService.CalculateActual(orderRecord, orderRecord.last_date_base.Value, orderRecord.weekend_operation.Value);
            }
            else
            {
                //already calced in ErsRegularMerchandiseInBasket.CalcRegularFirstDate
                orderRecord.last_date_base = orderRecord.next_date_base;
                orderRecord.last_date = orderRecord.next_date;
            }

            orderRecord.next_date_base = regularPatternService.CalculateBase(merchandise, orderRecord.last_date_base.Value);
            orderRecord.next_date = regularPatternService.CalculateActual(merchandise, orderRecord.next_date_base.Value, orderRecord.weekend_operation.Value);

            orderRecord.next2_date_base = regularPatternService.CalculateBase(merchandise, orderRecord.next_date_base.Value);
            orderRecord.next2_date = regularPatternService.CalculateActual(merchandise, orderRecord.next2_date_base.Value, orderRecord.weekend_operation.Value);

            orderRecord.next3_date_base = regularPatternService.CalculateBase(merchandise, orderRecord.next2_date_base.Value);
            orderRecord.next3_date = regularPatternService.CalculateActual(merchandise, orderRecord.next3_date_base.Value, orderRecord.weekend_operation.Value);

            orderRecord.next4_date_base = regularPatternService.CalculateBase(merchandise, orderRecord.next3_date_base.Value);
            orderRecord.next4_date = regularPatternService.CalculateActual(merchandise, orderRecord.next4_date_base.Value, orderRecord.weekend_operation.Value);

            orderRecord.next5_date_base = regularPatternService.CalculateBase(merchandise, orderRecord.next4_date_base.Value);
            orderRecord.next5_date = regularPatternService.CalculateActual(merchandise, orderRecord.next5_date_base.Value, orderRecord.weekend_operation.Value);

            orderRecord.last_sendtime = order.next_sendtime;
            orderRecord.next_sendtime = order.next_sendtime;
            orderRecord.next2_sendtime = order.next_sendtime;
            orderRecord.next3_sendtime = order.next_sendtime;
            orderRecord.next4_sendtime = order.next_sendtime;
            orderRecord.next5_sendtime = order.next_sendtime;

            orderRecord.last_sendtime_base = order.next_sendtime;
            orderRecord.next_sendtime_base = order.next_sendtime;
            orderRecord.next2_sendtime_base = order.next_sendtime;
            orderRecord.next3_sendtime_base = order.next_sendtime;
            orderRecord.next4_sendtime_base = order.next_sendtime;
            orderRecord.next5_sendtime_base = order.next_sendtime;

            orderRecord.min_delivery_days = ErsFactory.ersUtilityFactory.getSetup().sendday;

            orderRecord.pay = order.pay;
            orderRecord.conv_code = order.conv_code;
            orderRecord.member_card_id = order.member_card_id;
            orderRecord.member_add_id = order.member_add_id;
            orderRecord.mcode = order.mcode;

            orderRecord.intime = DateTime.Now;

            // 明細リストに追加
            order.regularOrderRecords.Add(orderRecord);
        }
    }
}