﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order.strategy
{
    public class ValidateNextSendMonthlyIntervalsStgy
        : ValidateNextSendStgy
    {
        public ValidateNextSendMonthlyIntervalsStgy(ManageRegularPatternService service)
            : base(service)
        {
        }

        internal protected override IEnumerable<ValidationResult> Validate(IErsModelBase model)
        {

            var datasource = model as IManageRegularPatternDatasource;

            if (datasource == null)
                throw new Exception(model.GetType().Name + " has to implement IManageRegularPatternDatasource.");

            //○ヶ月ごと△日
            yield return model.CheckRequired("ptn_interval_month");
            yield return model.CheckRequired("ptn_day");
            //yield return model.CheckRequired("next_date");

            //Checks value
            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("ptn_day", EnumCommonNameType.PtnDay, (int?)datasource.ptn_day);

            DateTime next_date;
            if (datasource.next_date.HasValue)
            {
                next_date = datasource.next_date.Value;
            }
            else
            {
                next_date = DateTime.Now;
            }

            if (datasource.ptn_interval_month != null && datasource.ptn_day != null)
            {
                var next_send_date = service.CalculateFirstTime(datasource, DateTime.Now);
                var fromDate = ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetRegularFromDate(DateTime.Now);

                //最短配送可能日チェック
                if (next_send_date < fromDate)
                {
                    yield return new ValidationResult(
                        ErsResources.GetMessage("10045", ErsResources.GetFieldName("next_date"), fromDate.Value.ToString("yyyy/MM/dd")),
                        new[] { "next_date" });
                }

                var toDate = DateTime.Now.AddDays(ErsFactory.ersUtilityFactory.getSetup().MaxSenddayCount);

                if (next_send_date > toDate)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10046", ErsResources.GetFieldName("senddate"), toDate.ToString("yyyy/MM/dd"))
                        , new[] { "senddate" });
                }
            }
        }
    }
}