﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.strategy
{
    public class CalcNextSendWeeklyIntervalsStgy
        : CalcNextSendStgy
    {
        public override DateTime CalculateBase(IManageRegularPatternDatasource datasource, DateTime baseDate)
        {
            var nextDate = DateTime.Parse(baseDate.AddMonths(datasource.ptn_interval_month.Value).ToString("yyyy/MM/01"));
            return this.CalculateNextSend(nextDate, datasource);
        }

        public override DateTime CalculateActual(IManageRegularPatternDatasource datasource, DateTime baseDate, EnumWeekendOperation weekend_operation)
        {
            return CalcNextSendStgy.CalculateWeekendOperation(baseDate, weekend_operation);
        }

        public override DateTime? CalculateFirstTime(IManageRegularPatternDatasource datasource, DateTime baseDate)
        {
            var nextDate = baseDate;

            nextDate = DateTime.Parse(nextDate.ToString("yyyy/MM/01"));

            var resultDate = this.CalculateNextSend(nextDate, datasource);

            //すでに過ぎている場合は翌月
            var fromDate = ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetRegularFromDate(baseDate);
            if(resultDate < fromDate)
                resultDate = this.CalculateNextSend(nextDate.AddMonths(1), datasource);

            return resultDate;
        }

        protected virtual DateTime CalculateNextSend(DateTime baseDate, IManageRegularPatternDatasource datasource)
        {
            var intWeekday = (int)datasource.ptn_weekday.Value;
            var nextFirstWeekDay = (int)baseDate.DayOfWeek;
            if (intWeekday > nextFirstWeekDay)
            {
                baseDate = baseDate.AddDays(intWeekday - nextFirstWeekDay);
            }
            else if (intWeekday < nextFirstWeekDay)
            {
                baseDate = baseDate.AddDays(intWeekday - nextFirstWeekDay + 7);
            }

            var resultDate = baseDate.AddDays((datasource.ptn_interval_week.Value - 1) * 7);
            while (resultDate.Month != baseDate.Month)
            {
                resultDate = resultDate.AddDays(-7);
            }

            return resultDate;
        }

        protected virtual int GetDate(DateTime baseDate, int ptn_interval_week, DayOfWeek ptn_weekday)
        {
            var sunday = 0 - baseDate.DayOfWeek + 1;
            return sunday + (7 * ptn_interval_week) + (int)ptn_weekday;
        }
    }
}