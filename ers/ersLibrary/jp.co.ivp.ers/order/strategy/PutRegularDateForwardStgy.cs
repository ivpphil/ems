﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order.strategy
{
    /// <summary>
    /// Puts the next dates forward
    /// </summary>
    public class PutRegularDateForwardStgy
    {
        /// <summary>
        /// Puts the next dates of regular order forward
        /// 定期の周期日付の算出
        /// </summary>
        /// <param name="regularRegister"></param>
        /// <param name="detail"></param>
        public virtual void Execute(ErsRegularOrderRecord detail, bool updateLastDate)
        {
            var regularPatternService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(detail.send_ptn.Value);

            // Do not update last_date when just skipping
            if (updateLastDate)
            {
                detail.last_date = detail.next_date;
            }
            detail.last_date_base = detail.next_date_base;

            detail.next_date_base = detail.next2_date_base;
            detail.next_date = detail.next2_date;

            detail.next2_date_base = detail.next3_date_base;
            detail.next2_date = detail.next3_date;

            detail.next3_date_base = detail.next4_date_base;
            detail.next3_date = detail.next4_date;

            detail.next4_date_base = detail.next5_date_base;
            detail.next4_date = detail.next5_date;

            detail.next5_date_base = regularPatternService.CalculateBase(detail, detail.next4_date_base.Value);
            detail.next5_date = regularPatternService.CalculateActual(detail, detail.next5_date_base.Value, detail.weekend_operation.Value);

            detail.last_sendtime = detail.next_sendtime;
            detail.next_sendtime = detail.next2_sendtime;
            detail.next2_sendtime = detail.next3_sendtime;
            detail.next3_sendtime = detail.next4_sendtime;
            detail.next4_sendtime = detail.next5_sendtime;
            detail.next5_sendtime = detail.next5_sendtime_base;

            detail.last_sendtime_base = detail.next_sendtime_base;
            detail.next_sendtime_base = detail.next2_sendtime_base;
            detail.next2_sendtime_base = detail.next3_sendtime_base;
            detail.next3_sendtime_base = detail.next4_sendtime_base;
            detail.next4_sendtime_base = detail.next5_sendtime_base;
            detail.next5_sendtime_base = detail.next5_sendtime_base;//next5_sendtime_base stay as it is

            detail.min_delivery_days = ErsFactory.ersUtilityFactory.getSetup().sendday;
        }
    }
}
