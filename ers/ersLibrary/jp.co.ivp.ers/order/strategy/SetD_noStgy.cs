﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.strategy
{
    public class SetD_noStgy
    {
        /// <summary>
        /// Gets new d_no and Sets it to the d_no of this instance if the d_no of this instance is not set yet.
        /// </summary>
        public void SetNext(ErsOrder objOrder, IEnumerable<ErsOrderRecord> orderRecords)
        {
            if (string.IsNullOrEmpty(objOrder.d_no))
            {
                var d_no = ErsFactory.ersOrderFactory.GetErsOrderRepository().GetNextD_no();
                this.Set(objOrder, orderRecords, d_no);
            }
        }

        /// <summary>
        /// Overwrites the d_no of this instance.
        /// </summary>
        /// <returns>伝票番号</returns>
        public void Set(ErsOrder objOrder, IEnumerable<ErsOrderRecord> orderRecords, string d_no)
        {
            objOrder.d_no = d_no;

            if (orderRecords == null)
            {
                return;
            }

            foreach (var item in orderRecords)
            {
                item.d_no = d_no;
            }
        }
    }
}
