﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order.strategy
{
    public class CreateOrderRecordKeyStgy
    {
        /// <summary>
        /// Gets Key of ErsOrderRecord
        /// </summary>
        /// <returns></returns>
        public string GetKey(ErsOrderRecord orderRecord, DateTime? senddate)
        {
            var regular_detail_id = string.Empty;
            if (orderRecord.regular_detail_id != null)
            {
                regular_detail_id = string.Join("_", orderRecord.regular_detail_id);
            }
            return orderRecord.scode
               + "_" + orderRecord.price
               + "_" + regular_detail_id
               + "_" + senddate
               + "_" + orderRecord.send_ptn
               + "_" + orderRecord.ptn_interval_month
               + "_" + orderRecord.ptn_interval_week
               + "_" + orderRecord.ptn_interval_day
               + "_" + orderRecord.ptn_day
               + "_" + orderRecord.ptn_weekday;
        }
    }
}
