﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.order.strategy
{
    public abstract class RegularMerchandiseFactory
    {
        protected IManageRegularPatternDatasource datasource;
        protected ManageRegularPatternService service;

        public RegularMerchandiseFactory(ManageRegularPatternService service, IManageRegularPatternDatasource datasource)
        {
            this.datasource = datasource;
            this.service = service;
        }

        public virtual ErsBaskRecord GetMerchandise(string ransu, string scode, int? member_rank)
        {
            var merchandise = ErsFactory.ersBasketFactory.GetErsBaskRecordWithScode(ransu, scode, member_rank);

            //定期の初回価格セット
            merchandise.price = merchandise.regular_first_price ?? merchandise.regular_price;
            merchandise.send_ptn = this.service.send_ptn;
            merchandise.intime = null;

            this.SetValueToErsMerchandise(merchandise);

            return merchandise;
        }

        public abstract void SetValueToErsMerchandise(ErsBaskRecord merchandise);
    }
}