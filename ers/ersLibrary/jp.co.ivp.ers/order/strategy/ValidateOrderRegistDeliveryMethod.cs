﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;

namespace jp.co.ivp.ers.order.strategy
{
    public class ValidateOrderRegistDeliveryMethod
    {
        public ValidationResult Validate(EnumDelvMethod? new_deliv_method, IEnumerable<string> basket, IEnumerable<string> regular_basket)
        {
            ValidationResult result = null;

            foreach (var scode in basket)
                result = ExecuteValidation(new_deliv_method, scode.ToString());

            foreach (var scode in regular_basket)
                result = ExecuteValidation(new_deliv_method, scode.ToString());

            return result;

        }

        private ValidationResult ExecuteValidation(EnumDelvMethod? new_deliv_method, string scode)
        {
            var ersMerchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(Convert.ToString(scode), null);

            if (ersMerchandise == null)
                return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("deliv_method", EnumCommonNameType.DelvMethod, (int?)new_deliv_method);

            if ((ersMerchandise.deliv_method != EnumDelvMethod.ExpressAndMail) && ersMerchandise.deliv_method != new_deliv_method)
                return new ValidationResult(ErsResources.GetMessage("10025", new[] { ErsResources.GetFieldName("g_master_t.deliv_method") }));

            return null;
        }
    }
}
