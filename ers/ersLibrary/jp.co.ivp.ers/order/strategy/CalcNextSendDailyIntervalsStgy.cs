﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.strategy
{
    public class CalcNextSendDailyIntervalsStgy
        : CalcNextSendStgy
    {
        public override DateTime CalculateBase(IManageRegularPatternDatasource datasource, DateTime baseDate)
        {
            return baseDate.AddDays(datasource.ptn_interval_day.Value);
        }

        public override DateTime CalculateActual(IManageRegularPatternDatasource datasource, DateTime baseDate, EnumWeekendOperation weekend_operation)
        {
            return CalcNextSendStgy.CalculateWeekendOperation(baseDate, weekend_operation);
        }

        public override DateTime? CalculateFirstTime(IManageRegularPatternDatasource datasource, DateTime baseDate)
        {
            if (datasource.next_date.HasValue)
            {
                return datasource.next_date;
            }

            // 最短日は必ず必要なので直近の配送可能日を算出(1年間配送できないのはありえないので無視)
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            return ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetFromDates(DateTime.Now, setup.sendday, 365);
        }
    }
}