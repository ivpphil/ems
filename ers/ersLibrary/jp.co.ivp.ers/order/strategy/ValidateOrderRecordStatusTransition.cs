﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.order.strategy
{
    public class ValidateOrderRecordStatusTransition
    {
        /// <summary>
        /// 指定された明細を指定されたステータスに更新できるかをチェックする
        /// </summary>
        /// <param name="orderRecord"></param>
        /// <param name="afterStatus"></param>
        /// <returns></returns>
        public ValidationResult Validate(ErsOrderRecord orderRecord, EnumOrderStatusType? afterStatus, string[] memberNames)
        {
            return this.Validate(orderRecord.order_status.Value, afterStatus.Value, orderRecord.shipdate, memberNames);
        }

        /// <summary>
        /// 指定されたステータスを指定されたステータスに更新できるかをチェックする
        /// </summary>
        /// <param name="beforeStatus"></param>
        /// <param name="afterStatus"></param>
        /// <param name="ship_date"></param>
        /// <returns></returns>
        public ValidationResult Validate(EnumOrderStatusType? beforeStatus, EnumOrderStatusType? afterStatus, DateTime? ship_date, string[] memberNames)
        {
            var isValidOrderStatustransitionSpec = ErsFactory.ersOrderFactory.GetIsValidOrderStatustransitionSpec();
            var validateResult = isValidOrderStatustransitionSpec.Determine(beforeStatus, afterStatus, ship_date);

            switch (validateResult)
            {
                case specification.IsValidOrderStatustransitionSpec.validResult.Valid:
                    return null;

                case specification.IsValidOrderStatustransitionSpec.validResult.Invalid:
                    var strBeforeStatus = ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().GetStringFromId(beforeStatus);
                    var strAfterStatus = ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().GetStringFromId(afterStatus);
                    return new ValidationResult(ErsResources.GetMessage("63108", strBeforeStatus, strAfterStatus), memberNames);

                case specification.IsValidOrderStatustransitionSpec.validResult.BeforeDeliverReturn:
                    // 出荷前のキャンセルは返品への変更不可
                    return new ValidationResult(ErsResources.GetMessage("63109"), memberNames);

                default:
                    return null;
            }
        }
    }
}
