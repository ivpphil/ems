﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.order.strategy.status
{
    public class OrderStatusSetDeliverdStgy
    {
        public void SetStatus(ErsOrderRecord orderRecord, string sendno, DateTime? shipdate)
        {
            if (orderRecord.order_status == EnumOrderStatusType.DELIVERED)
                return;

            orderRecord.order_status = EnumOrderStatusType.DELIVERED;
            orderRecord.sendno = sendno;
            orderRecord.shipdate = shipdate;
        }
    }
}
