﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.order.strategy.status
{
    public class OrderStatusSetDeliverRequestStgy
    {
        /// <summary>
        /// Set cancel status to the order
        /// </summary>
        /// <param name="order">ErsOrder インスタンス</param>
        public virtual void SetStatus(ErsOrderRecord orderRecord)
        {
            if (orderRecord.order_status == EnumOrderStatusType.DELIVER_REQUEST)
                return;

            // set cancel status.
            orderRecord.order_status = EnumOrderStatusType.DELIVER_REQUEST;
        }
    }
}
