﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order.related;

namespace jp.co.ivp.ers.order.strategy
{
    public class ValidatePayStgy
    {
        public virtual ValidationResult Validate(string fieldKey, EnumPaymentType? pay, bool regular_order, bool activeOnly = true, int? site_id = null)
        {
            if (!pay.HasValue)
            {
                return null;
            }

            if (site_id == null)
            {
                site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            }

            var objPay = ErsFactory.ersOrderFactory.GetErsPayWithIdAndSiteId(pay.Value, site_id);
            if (objPay == null
                || (activeOnly && objPay.active == EnumActive.NonActive))
            {
                return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName(fieldKey)), new[] { fieldKey });
            }

            if (regular_order && objPay.enable_regular_order == EnumOnOff.Off)
            {
                return new ValidationResult(ErsResources.GetMessage("20240", objPay.pay_name), new[] { fieldKey });
            }

            return null;
        }

        public virtual ValidationResult ValidateOverseas(string fieldKey, EnumPaymentType? pay, int? shipping_pref, int? site_id = null)
        {
            if (!pay.HasValue || !shipping_pref.HasValue)
            {
                return null;
            }

            if (site_id == null)
            {
                site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            }

            var objPay = ErsFactory.ersOrderFactory.GetErsPayWithIdAndSiteId(pay.Value, site_id);
            if (objPay == null)
            {
                return null;
            }

            if (shipping_pref == (int)EnumPrefecture.OVERSEAS && objPay.enable_overseas == EnumOnOff.Off)
            {
                return new ValidationResult(ErsResources.GetMessage("20243", objPay.pay_name), new[] { fieldKey });
            }

            return null;
        }

        /// <summary>
        /// クレジット決済で、依頼主と宛名が異なる場合にエラー
        /// </summary>
        /// <param name="fieldKey"></param>
        /// <param name="pay"></param>
        /// <param name="send"></param>
        /// <param name="lname"></param>
        /// <param name="add_lname"></param>
        /// <param name="fname"></param>
        /// <param name="add_fname"></param>
        /// <param name="lnamek"></param>
        /// <param name="add_lnamek"></param>
        /// <param name="fnamek"></param>
        /// <param name="add_fnamek"></param>
        /// <returns></returns>
        public virtual ValidationResult ValidateRecipientName(string fieldKey, EnumPaymentType? pay, EnumSendTo? send, string lname, string add_lname, string fname, string add_fname, string lnamek, string add_lnamek, string fnamek, string add_fnamek)
        {
            if (pay != null && pay != EnumPaymentType.CREDIT_CARD && send == EnumSendTo.ANOTHER_ADDRESS)
            {
                //依頼主と宛名が同じであればクレジット以外も可
                if (lname != add_lname || 
                    fname != add_fname || 
                    lnamek != add_lnamek || 
                    fnamek != add_fnamek)
                {
                    var w_pay = ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(pay);
                    return new ValidationResult(ErsResources.GetMessage("20210", new[] { w_pay }), new[] { fieldKey });
                }
            }

            return null;
        }

        /// <summary>
        /// カート内に定期があり、支払いがクレジット以外はエラー
        /// </summary>
        public ValidationResult ValidateCardSave(EnumPaymentType? pay, EnumCardSave? card_save, int? card_id, string strRansu)
        {
            Boolean blnRegExist = ErsFactory.ersOrderFactory.GetIsBasketHasRegularItem().Satisfy(strRansu);
            if (blnRegExist)//定期存在
            {
                //カードの場合のみ判定とする
                if (pay == EnumPaymentType.CREDIT_CARD)
                {
                    //カード預けがない場合エラー
                    if (card_save != EnumCardSave.Save && (!card_id.HasValue || card_id == CreditCardInfo.IgnoreCardSequenceValue))
                    {
                        return new ValidationResult(ErsResources.GetMessage("20233"));
                    }
                }
            }
            return null;
        }
    }
}
