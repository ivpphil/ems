﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.Payment;

namespace jp.co.ivp.ers.order.strategy
{
    /// <summary>
    /// 各支払方法に応じて上限閾値金額で判定する
    /// </summary>
    public class CheckFloorLimitStgy
    {
        public int Limit { get; set; }

        public virtual ValidationResult Validate(EnumPaymentType? pay, int total, EnumPmFlg pm_flg)
        {
            if (total > this.getLimit(pay, pm_flg))
            {
                var pay_name = ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(pay);
                return new ValidationResult(ErsResources.GetMessage("20211", pay_name, Limit), new[] { "pay" });
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private int getLimit(EnumPaymentType? pay, EnumPmFlg pm_flg)
        {
            Limit = 0;
            //支払い方法の閾値取得
            var pay_repository = ErsFactory.ersOrderFactory.GetErsPayRepository();
            var pay_criteria = ErsFactory.ersOrderFactory.GetErsPayCriteria();
            pay_criteria.id = pay;
            var pay_list = pay_repository.Find(pay_criteria);
            if (pay_list != null)
            {
                foreach (var pay_obj in pay_list)
                {
                    switch (pm_flg)
                    {
                        case EnumPmFlg.CTS:
                        case EnumPmFlg.FAX:
                        case EnumPmFlg.LETTER:
                        case EnumPmFlg.TEL:
                            Limit = (int)pay_obj.cts_floor_limit;
                            break;

                        default:
                            Limit = (int)pay_obj.floor_limit;
                            break;
                    }
                }
            }
            return Limit;
        }
    }
}
