﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.order.strategy
{
    public class RegularMerchandiseDailyIntervalsFactory
        : RegularMerchandiseFactory
    {
        public RegularMerchandiseDailyIntervalsFactory(ManageRegularPatternService service, IManageRegularPatternDatasource datasource)
            : base(service, datasource)
        {
        }

        public override void SetValueToErsMerchandise(ErsBaskRecord merchandise)
        {
            merchandise.ptn_interval_day = datasource.ptn_interval_day;

            var next_date = datasource.next_date;
            merchandise.next_date = next_date;
        }
    }
}