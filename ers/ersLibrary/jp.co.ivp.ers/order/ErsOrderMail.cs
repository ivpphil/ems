﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderMail
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string d_no { get; set; }
        public int? ds_id { get; set; }
        public EnumSentFlg? purchase_mail { get; set; }
        public EnumSentFlg? shipped_mail { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
        public int? site_id { get; set; }
    }
}
