﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.order
{
    public class ErsRegularOrderRepository
        : ErsRepository<ErsRegularOrder>
    {
        public ErsRegularOrderRepository()
            : base(new ErsDB_regular_t())
        {
        }

        public override void Insert(ErsRegularOrder obj, bool storeNewIdToObject = false)
        {
            base.Insert(obj, storeNewIdToObject);

            var recordRepository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();

            // 明細
            foreach (var record in obj.regularOrderRecords)
            {
                record.regular_id = obj.id;
                recordRepository.Insert(record, true);
            }
        }

        public override IList<ErsRegularOrder> Find(Criteria criteria)
        {
            var searchSpec = ErsFactory.ersOrderFactory.GetRegularOrderSearchSpec();
            var listRegulerOrder = searchSpec.GetSearchData(criteria);

            var retList = new List<ErsRegularOrder>();
            foreach (var regularOrderData in listRegulerOrder)
            {
                var RegularOrder = ErsFactory.ersOrderFactory.GetErsRegularOrderWithParameters(regularOrderData);

                LoadRegularOrderRecord(RegularOrder);

                retList.Add(RegularOrder);
            }

            return retList;
        }

        protected virtual void LoadRegularOrderRecord(ErsRegularOrder regularOrder)
        {
            var detailRepository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            var detailCriteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
            detailCriteria.regular_id = regularOrder.id;
            regularOrder.regularOrderRecords = detailRepository.Find(regularOrder, detailCriteria);
        }

        public override long GetRecordCount(Criteria criteria)
        {
            var searchSpec = ErsFactory.ersOrderFactory.GetRegularOrderSearchSpec();
            return searchSpec.GetCountData(criteria);
        }
    }
}