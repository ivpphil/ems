﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.order.strategy
{
    public class ManageRegularPatternService
    {
        public virtual EnumSendPtn send_ptn { get; protected internal set; }

        public ManageRegularPatternService(EnumSendPtn send_ptn)
        {
            this.send_ptn = send_ptn;
        }

        public virtual IEnumerable<ValidationResult> Validate(IErsModelBase model)
        {
            if (this.send_ptn == EnumSendPtn.NORMAL)
                yield break;

            var validateNextSendStgy = this.GetValidateNextSendStgy();

            foreach (var result in validateNextSendStgy.Validate(model))
            {
                yield return result;
            }
        }

        /// <summary>
        /// Calculate base rule date.
        /// </summary>
        /// <param name="datasource"></param>
        /// <param name="baseDate"></param>
        /// <param name="weekend_operation"></param>
        /// <returns></returns>
        public virtual DateTime CalculateBase(IManageRegularPatternDatasource datasource, DateTime baseDate)
        {
            var calcNextSendStgy = this.GetCalcNextSendStgy();
            return calcNextSendStgy.CalculateBase(datasource, baseDate);
        }

        public virtual DateTime CalculateActual(IManageRegularPatternDatasource datasource, DateTime baseDate, EnumWeekendOperation weekend_operation)
        {
            var calcNextSendStgy = this.GetCalcNextSendStgy();
            return calcNextSendStgy.CalculateActual(datasource, baseDate, weekend_operation);
        }

        public virtual DateTime? CalculateFirstTime(IManageRegularPatternDatasource datasource, DateTime baseDate)
        {
            var calcNextSendStgy = this.GetCalcNextSendStgy();
            return calcNextSendStgy.CalculateFirstTime(datasource, baseDate);
        }

        public virtual ErsBaskRecord GetMerchandise(string ransu, string scode, IManageRegularPatternDatasource datasource, int? member_rank)
        {
            var regularMerchandiseFactory = this.GetRegularMerchandiseFactory(datasource);
            return regularMerchandiseFactory.GetMerchandise(ransu, scode, member_rank);
        }

        protected virtual CalcNextSendStgy GetCalcNextSendStgy()
        {
            switch (this.send_ptn)
            {
                case EnumSendPtn.MONTH_INTERVALS:
                    //○ヶ月ごと△日
                    return new CalcNextSendMonthlyIntervalsStgy();
                case EnumSendPtn.WEEK_INTERVALS:
                    //○ヶ月ごと第△週目□曜日
                    return new CalcNextSendWeeklyIntervalsStgy();
                case EnumSendPtn.DAY_INTERVALS:
                    //○日ごと
                    return new CalcNextSendDailyIntervalsStgy();
                default:
                    throw new ArgumentException("argument 'send_ptn' value must be from 1 to 3");
            }
        }

        protected virtual ValidateNextSendStgy GetValidateNextSendStgy()
        {
            switch (this.send_ptn)
            {
                case EnumSendPtn.MONTH_INTERVALS:
                    //○ヶ月ごと△日
                    return new ValidateNextSendMonthlyIntervalsStgy(this);
                case EnumSendPtn.WEEK_INTERVALS:
                    //○ヶ月ごと第△週目□曜日
                    return new ValidateNextSendWeeklyIntervalsStgy(this);
                case EnumSendPtn.DAY_INTERVALS:
                    //○日ごと
                    return new ValidateNextSendDailyIntervalsStgy(this);
                default:
                    throw new ArgumentException("argument 'send_ptn' value must be from 1 to 3");
            }
        }

        protected virtual RegularMerchandiseFactory GetRegularMerchandiseFactory(IManageRegularPatternDatasource datasource)
        {
            switch (this.send_ptn)
            {
                case EnumSendPtn.MONTH_INTERVALS:
                    //○ヶ月ごと△日
                    return new RegularMerchandiseMonthlyIntervalsFactory(this, datasource);
                case EnumSendPtn.WEEK_INTERVALS:
                    //○ヶ月ごと第△週目□曜日
                    return new RegularMerchandiseWeeklyIntervalsFactory(this, datasource);
                case EnumSendPtn.DAY_INTERVALS:
                    //○日ごと
                    return new RegularMerchandiseDailyIntervalsFactory(this, datasource);
                default:
                    throw new ArgumentException("argument 'send_ptn' value must be from 1 to 3");
            }
        }
    }
}