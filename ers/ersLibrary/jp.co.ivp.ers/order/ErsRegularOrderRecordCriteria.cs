﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order
{
    public class ErsRegularOrderRecordCriteria
        : ErsRegularOrderCriteria
    {
        protected internal ErsRegularOrderRecordCriteria()
        {
        }

        new public virtual int? id { set { this.Add(Criteria.GetCriterion("regular_detail_t.id", value, Operation.EQUAL)); } }

        new public virtual DateTime? intime { set { this.Add(Criteria.GetCriterion("regular_detail_t.intime", value, Operation.EQUAL)); } }

        new public virtual DateTime? utime { set { this.Add(Criteria.GetCriterion("regular_detail_t.utime", value, Operation.EQUAL)); } }

        public virtual int? regular_id { set { this.Add(Criteria.GetCriterion("regular_detail_t.regular_id", value, Operation.EQUAL)); } }

        public virtual short? s_sale_ptn { set { this.Add(Criteria.GetCriterion("regular_detail_t.s_sale_ptn", value, Operation.EQUAL)); } }

        public virtual string gcode { set { this.Add(Criteria.GetCriterion("regular_detail_t.gcode", value, Operation.EQUAL)); } }

        public virtual string jancode { set { this.Add(Criteria.GetCriterion("regular_detail_t.jancode", value, Operation.EQUAL)); } }

        public virtual int? min_delivery_days { set { this.Add(Criteria.GetCriterion("regular_detail_t.min_delivery_days", value, Operation.EQUAL)); } }

        public virtual EnumSendPtn? send_ptn { set { this.Add(Criteria.GetCriterion("regular_detail_t.send_ptn", value, Operation.EQUAL)); } }

        public virtual short? ptn_interval_month { set { this.Add(Criteria.GetCriterion("regular_detail_t.ptn_interval_month", value, Operation.EQUAL)); } }

        public virtual short? ptn_day { set { this.Add(Criteria.GetCriterion("regular_detail_t.ptn_day", value, Operation.EQUAL)); } }

        public virtual short? ptn_interval_week { set { this.Add(Criteria.GetCriterion("regular_detail_t.ptn_interval_week", value, Operation.EQUAL)); } }

        public virtual DayOfWeek? ptn_weekday { set { this.Add(Criteria.GetCriterion("regular_detail_t.ptn_weekday", (int?)value, Operation.EQUAL)); } }

        public virtual short? ptn_interval_day { set { this.Add(Criteria.GetCriterion("regular_detail_t.ptn_interval_day", value, Operation.EQUAL)); } }

        public virtual DateTime? next_date_base { set { this.Add(Criteria.GetCriterion("regular_detail_t.next_date_base", value, Operation.EQUAL)); } }

        public virtual new DateTime? delete_date { set { this.Add(Criteria.GetCriterion("regular_detail_t.delete_date", value, Operation.EQUAL)); } }

        public void SetActiveDetailsOnly()
        {
            var criteria_compare = Criteria.GetCriterion("delete_date", ColumnName("next_date"), Operation.GREATER_THAN);
            var criteria_null = Criteria.GetCriterion("delete_date", null, Operation.EQUAL);
            this.Add(Criteria.JoinWithOR(new[] { criteria_compare, criteria_null }));
        }

        public void SetInactiveDetailsOnly()
        {
            var criteria_compare = Criteria.GetCriterion("delete_date", ColumnName("next_date"), Operation.LESS_EQUAL);
            //var criteria_null = Criteria.GetCriterion("delete_date", null, Operation.NOT_EQUAL);
            this.Add(Criteria.JoinWithOR(new[] { criteria_compare, criteria_compare }));
        }

        public virtual void SetSCodeOrderBy(OrderBy orderBy)
        {
            this.AddOrderBy("regular_detail_t.scode", orderBy);
        }

        public void HasUpdateErrorCard()
        {
            var sql = "EXISTS (SELECT * FROM member_card_t WHERE id = regular_detail_t.member_card_id AND update_status = :update_status)";
            this.Add(Criteria.GetUniversalCriterion(sql, new Dictionary<string, object>() { { "update_status", (int)EnumCardUpdateStatus.Error } }));
        }
    }
}