﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order
{
    public class ErsEtcCriteria
        : Criteria
    {
        public EnumPaymentType? pay
        {
            set
            {
                Add(Criteria.GetCriterion("etc_t.pay", (int?)value, Operation.EQUAL));
            }
        }

        public int id
        {
            set
            {
                Add(Criteria.GetCriterion("etc_t.id", value, Operation.EQUAL));
            }
        }

        public int site_id
        {
            set
            {
                Add(Criteria.GetCriterion("etc_t.site_id", value, Operation.EQUAL));
            }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("etc_t.id", orderBy);
        }

        public long more_lessEqual
        {
            set
            {
                Add(Criteria.GetCriterion("etc_t.more", value, Operation.LESS_EQUAL));
            }
        }

        public long down_graterEqual
        {
            set
            {
                Add(Criteria.GetCriterion("etc_t.down", value, Operation.GREATER_EQUAL));
            }
        }

        public long down_graterThan
        {
            set
            {
                Add(Criteria.GetCriterion("etc_t.down", value, Operation.GREATER_THAN));
            }
        }
    }
}
