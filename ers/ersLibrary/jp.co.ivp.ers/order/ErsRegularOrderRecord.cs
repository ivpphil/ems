﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.order
{
    public class ErsRegularOrderRecord
        : ErsRepositoryEntity, IManageRegularPatternDatasource
    {
        public ErsRegularOrder objOrder;

        public ErsRegularOrderRecord(ErsRegularOrder objOrder)
        {
            this.objOrder = objOrder;
        }

        public override int? id { get; set; }
        public virtual int? regular_id { get; set; }
        public string mcode { get; set; }
        public virtual EnumPaymentType? pay { get; set; }
        public virtual string scode { get; set; }
        public virtual string sname { get; set; }
        public virtual int? price { get; set; }
        public virtual int? amount { get; set; }
        public virtual int? total { get; set; }
        public virtual int? point { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual EnumSalePatternType? s_sale_ptn { get; set; }
        public virtual int? cancel_amount { get; set; }
        public virtual string gcode { get; set; }
        public virtual string gname { get; set; }
        public virtual string m_gname { get; set; }
        public virtual DateTime? date_from { get; set; }
        public virtual DateTime? date_to { get; set; }
        public virtual short? stock_flg { get; set; }
        public virtual int? stock_set1 { get; set; }
        public virtual int? stock_set2 { get; set; }
        public virtual int[] cate1 { get; set; }
        public virtual int[] cate2 { get; set; }
        public virtual int[] cate3 { get; set; }
        public virtual int[] cate4 { get; set; }
        public virtual int[] cate5 { get; set; }
        public virtual string recommend1 { get; set; }
        public virtual string recommend2 { get; set; }
        public virtual string recommend3 { get; set; }
        public virtual string recommend4 { get; set; }
        public virtual string recommend5 { get; set; }
        public virtual string jancode { get; set; }
        public virtual string m_sname { get; set; }
        public virtual string attribute1 { get; set; }
        public virtual string attribute2 { get; set; }
        public virtual string mixed_group_code { get; set; }
        public virtual int? max_purchase_count { get; set; }
        public virtual EnumDisp_list_flg? disp_list_flg { get; set; }
        public virtual int? price2 { get; set; }
        public virtual int? min_delivery_days { get; set; }
        public virtual EnumSendPtn? send_ptn { get; set; }
        public virtual short? ptn_interval_month { get; set; }
        public virtual short? ptn_day { get; set; }
        public virtual short? ptn_interval_week { get; set; }
        public virtual DayOfWeek? ptn_weekday { get; set; }
        public virtual short? ptn_interval_day { get; set; }
        public virtual DateTime? last_date_base { get; set; }
        public virtual DateTime? next_date_base { get; set; }
        public virtual DateTime? next2_date_base { get; set; }
        public virtual DateTime? next3_date_base { get; set; }
        public virtual DateTime? next4_date_base { get; set; }
        public virtual DateTime? next5_date_base { get; set; }
        public virtual DateTime? last_date { get; set; }
        public virtual DateTime? next_date { get; set; }
        public virtual DateTime? next2_date { get; set; }
        public virtual DateTime? next3_date { get; set; }
        public virtual DateTime? next4_date { get; set; }
        public virtual DateTime? next5_date { get; set; }
        public virtual int? last_sendtime_base { get; set; }
        public virtual int? next_sendtime_base { get; set; }
        public virtual int? next2_sendtime_base { get; set; }
        public virtual int? next3_sendtime_base { get; set; }
        public virtual int? next4_sendtime_base { get; set; }
        public virtual int? next5_sendtime_base { get; set; }
        public virtual int? last_sendtime { get; set; }
        public virtual int? next_sendtime { get; set; }
        public virtual int? next2_sendtime { get; set; }
        public virtual int? next3_sendtime { get; set; }
        public virtual int? next4_sendtime { get; set; }
        public virtual int? next5_sendtime { get; set; }
        public virtual DateTime? delete_date { get; set; }
        public virtual DateTime? skip_date { get; set; }
        public virtual EnumWeekendOperation? weekend_operation { get; set; }
        public virtual int? member_add_id { get; set; }
        public virtual int? member_card_id { get; set; }
        public virtual string shipping_memo { get; set; }
        public virtual string cts_sname { get; set; }
        public virtual string ccode
        {
            get
            {
                if (string.IsNullOrEmpty(this._ccode))
                {
                    return this._ccode;
                }
                return this._ccode.ToUpper();
            }
            set
            {
                this._ccode = value;
            }
        }
        private string _ccode;
        public virtual EnumCarriageCostType? carriage_cost_type { get; set; }
        public virtual EnumPluralOrderType? plural_order_type { get; set; }
        public virtual EnumSetFlg? set_flg { get; set; }
        public virtual EnumDocBundlingFlg? doc_bundling_flg { get; set; }
        public virtual EnumDelvMethod? deliv_method { get; set; }
        public virtual string shipping_sname { get; set; }

        public virtual int? member_rank { get; set; }

        public int GetAmount()
        {
            var actualAmount = amount ?? 0;
            var actualCancelAmount = cancel_amount ?? 0;

            return actualAmount - actualCancelAmount;
        }

        protected internal bool IsShortestDelivery { get { return this.next_date_base == null; } }

        public EnumConvCode? conv_code { get; set; }

        public virtual DateTime? after_cancel_date { get; set; }

        public virtual DateTime? cancel_date { get; set; }

        public virtual int? site_id { get; set; }
    }
}
