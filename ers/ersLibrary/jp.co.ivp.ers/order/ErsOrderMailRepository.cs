﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.order
{
    public class ErsOrderMailRepository
        : ErsRepository<ErsOrderMail>
    {
        public ErsOrderMailRepository()
            : base(new ErsDB_ds_mail_t())
        {
        }
    }
}
