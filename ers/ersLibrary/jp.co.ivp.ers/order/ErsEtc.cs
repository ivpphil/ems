﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.order
{
    /// <summary>
    /// 手数料クラス
    /// </summary>
    public class ErsEtc
        : ErsRepositoryEntity
    {

        /// <summary>
        /// クレジットカード情報
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 支払い方法コード
        /// </summary>
        public virtual int? pay { get; set; }

        /// <summary>
        /// 以上
        /// </summary>
        public virtual int? more { get; set; }

        /// <summary>
        /// 未満
        /// </summary>
        public virtual int? down { get; set; }

        /// <summary>
        /// 手数料
        /// </summary>
        public virtual int? price { get; set; }

        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public virtual int? site_id { get; set; }
    }
}
