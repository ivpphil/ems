﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Collections;

namespace jp.co.ivp.ers.order
{
    public class ErsRegularErrLog : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string mcode { get; set; }
        public string card_mcode { get; set; }
        public string card_sequence { get; set; }
        public int[] regular_detail_id { get; set; }
        public string error_description { get; set; }
        public EnumRegularErrLogDispFlg? disp_flg { get; set; }
        public EnumActive? active { get; set; }
        public DateTime? occured_date  { get; set; }
        public string credit_order_id { get; set; }
    }
}
