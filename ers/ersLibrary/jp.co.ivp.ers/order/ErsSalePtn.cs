﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order
{
    public class ErsSalePtn
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string sale_ptn { get; set; }

        public short active { get; set; }
    }
}
