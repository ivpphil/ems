﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.order
{
    public class ErsEtcRepository
        : ErsRepository<ErsEtc>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsEtcRepository()
            : base("etc_t")
        {
        }

        public override IList<ErsEtc> Find(Criteria criteria)
        {
            var list = this.ersDB_table.gSelect(criteria);
            var retList = new List<ErsEtc>();
            foreach (var item in list)
            {
                var etc = ErsFactory.ersOrderFactory.GetErsEtcWithParameter(item);
                retList.Add(etc);
            }
            return retList;
        }
    }
}
