﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using jp.co.ivp.ers.employee;
using System.ComponentModel;
using jp.co.ivp.ers.Pdf.events;

namespace jp.co.ivp.ers.Pdf
{
    /// <summary>
    /// メール送信クラス（ベースクラス）
    /// </summary>
    public abstract class ErsPdf
    {
        public Setup setup { get { return ErsFactory.ersUtilityFactory.getSetup(); } }

        public ErsEmployee employee { get { return ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(this.emp_no); } }

        private string emp_no { get; set; }

        private EnumRequestType? request_type { get; set; }

        public ErsPdfEmployee pdf_employee
        {
            get
            {
                if (employee != null)
                {
                    var temp = new ErsPdfEmployee();
                    temp.OverwriteWithParameter(employee.GetPropertiesAsDictionary());
                    return temp;
                }
                return null;
            }
        }

        public abstract EnumPdfType type { get; }
        protected string path
        {
            get { return ErsCommonContext.MapPath("~/file_download/"); }
        }

        protected string _file_name { get; set; }
        protected string file_name
        {

            get
            {
                return _file_name;
            }
            set
            {
                if (!(value).Contains(".pdf"))
                {
                    _file_name = string.Concat(value, ".pdf");
                }
                else
                {
                    _file_name = value;
                }

            }
        }

        public string file_path
        {
            get
            {
                if (file_name.HasValue() && file_name.Contains(".pdf"))
                {
                    return path + file_name;
                }
                else
                {
                    return null;
                }
            }
        }

        private string[] signatories { get; set; }

        private string _title { get; set; }
        public string title
        {
            get
            {
                var namecode = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType.RequestType, (int?)request_type);
                if (request_type != null && namecode != null)
                {
                    _title = namecode.namename;
                }

                return _title;
            }
            set
            {
                _title = value;
            }

        }

        /*FONTS*/
        private Font font_title { get { return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 18); } }

        private Font font_label { get { return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8); } }

        private Font font_label_white { get { return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, BaseColor.WHITE); } }

        //request information 
        protected List<Dictionary<string, object>> info_table { get; set; }


        /// <summary>
        /// initialize pdf creation
        /// </summary>
        /// <param name="file_name"></param>
        protected void init()
        {
            if (type == EnumPdfType.form)
            {
                this.generatePdfForm();
            }

        }

        private void generatePdfForm()
        {
            var stream = new FileStream(file_path, FileMode.Create);
            var document = new Document(PageSize.A4, 25, 25, 80, 30);

            using (stream)
            {
               
                using (var writer = PdfWriter.GetInstance(document, stream))
                {
                    writer.PageEvent = new ErsPdfTemplate();

                    document.Open();

                    foreach (var form in info_table)
                    {
                        if (form.ContainsKey("emp_no"))
                        {
                            object emp_no;
                            if (form.TryGetValue("emp_no", out emp_no))
                            {
                                this.emp_no = Convert.ToString(emp_no);
                            }
                        }

                        if (form.ContainsKey("request_type"))
                        {
                            object request_type;
                            if (form.TryGetValue("request_type", out request_type))
                            {
                                this.request_type = (EnumRequestType)request_type;
                            }
                        }


                        //PDF title
                        string string_html = string.Format(ErsResources.GetMessage("pdf_title_html"), title);
                        using (var htmlWorker = new HTMLWorker(document))
                        {
                            using (var sr = new StringReader(string_html))
                            {
                                htmlWorker.Parse(sr);
                                document.Add(new Paragraph(" "));
                            }
                        }

                        //Personal Information
                        document.Add(getEmpInfoTable());

                        //Request Information
                        document.Add(getReqInfoTable(form));

                        //Reason
                        if (form.ContainsKey("reason"))
                        {
                            var reasonTable = new PdfPTable(1);

                            reasonTable.AddCell(Cell(new Paragraph(ErsResources.GetFieldName("reason"), font_label_white), reasonTable.NumberOfColumns, BaseColor.GRAY, null, true));
                            object reason;
                            if (form.TryGetValue("reason", out reason))
                            {
                                reasonTable.AddCell(Cell(new Paragraph(Convert.ToString(reason)), reasonTable.NumberOfColumns, null, null, true));
                            }
                            document.Add(new Paragraph(" "));
                            document.Add(reasonTable);
                        }

                        //Signatories
                        signatories = setup.default_signatories;
                        document.Add(new Paragraph(" "));
                        document.Add(getSignatories());

                        if (form != info_table.Last())
                        {
                            document.NewPage();
                        }

                    }

                    document.Close();
                }

            }

        }

        #region EMPLOYEE INFORMATION
        /// <summary>
        /// Employee information table
        /// </summary>
        /// <returns></returns>
        public PdfPTable getEmpInfoTable()
        {
            PdfPTable emp_table = new PdfPTable(4);
            if (pdf_employee == null)
            {
                throw new ErsException(ErsResources.GetMessage("pdf_emp_error"));
            }
            emp_table.AddCell(Cell(new Paragraph(ErsResources.GetFieldName("employee_information"), font_label_white), 4, BaseColor.GRAY, null, true));
            foreach (var property in pdf_employee.GetType().GetProperties())
            {
                if (Attribute.IsDefined(property, typeof(PdfFieldAttribute)))
                {
                    var displayName = string.Empty;
                    if (Attribute.IsDefined(property, typeof(DisplayNameAttribute)))
                    {
                        displayName = ((DisplayNameAttribute)(property.GetCustomAttributes(typeof(DisplayNameAttribute), true).FirstOrDefault())).DisplayName;
                    }
                    else
                    {
                        displayName = property.Name;
                    }

                    if (displayName.Contains("name") || displayName.Contains("contact"))
                        emp_table.AddCell(Cell(new Paragraph(Convert.ToString(property.GetValue(pdf_employee))), 2, null, displayName, true));
                    else
                        emp_table.AddCell(Cell(new Paragraph(Convert.ToString(property.GetValue(pdf_employee))), 1, null, displayName, true));
                }


            }
            emp_table.CompleteRow();
            return emp_table;
        }
        #endregion


        #region REQUEST INFORMATION
        /// <summary>
        /// Employee information table
        /// </summary>
        /// <returns></returns>
        public PdfPTable getReqInfoTable(Dictionary<string, object> dic)
        {
            var emp_table = new PdfPTable(4);
            if (dic.Keys.Count < 4)
            {
                emp_table = new PdfPTable(dic.Keys.Count);
            }

            emp_table.DefaultCell.Border = Rectangle.NO_BORDER;

            emp_table.AddCell(Cell(new Paragraph(ErsResources.GetFieldName("request_information"), this.font_label_white), emp_table.NumberOfColumns, BaseColor.GRAY, null, true));

            foreach (var item in dic)
            {
                string[] arry_irrelevant = setup.irrelevant_fields_info;
                if (!arry_irrelevant.Contains(item.Key))
                {
                    emp_table.AddCell(Cell(new Paragraph(Convert.ToString(item.Value)), 1, null, item.Key, true));
                }
            }

            emp_table.CompleteRow();


            return emp_table;

        }
        #endregion

        #region SIGNATORIES
        /// <summary>
        /// Employee information table
        /// </summary>
        /// <returns></returns>
        public PdfPTable getSignatories()
        {
            PdfPTable signatiesTable = new PdfPTable(2);
            signatiesTable.DefaultCell.Border = Rectangle.NO_BORDER;

            if (signatories != null)
            {
                var employees = ErsFactory.ersEmployeeFactory.GetErsSignatoriesWithEmpNos(signatories);
                if (employees != null && employees.Count > 0)
                {
                    foreach (var employee in employees)
                    {
                        var full_name = string.Join(" ", employee.fname, employee.lname);
                        var cell = Cell(new Paragraph(full_name), 1, null, null, false);
                        var job = ErsFactory.ersJobFactory.GetErsJobTitleWithID(Convert.ToInt32(employee.job_title));
                        if (job != null)
                        {
                            cell.AddElement(new Paragraph(job.job_title, font_label));
                        }
                        cell.Padding = 12;
                        signatiesTable.AddCell(cell);
                    }
                }
            }

            return signatiesTable;
        }

        #endregion


        #region CUSTOM CELL
        /// <summary>
        /// Custom cell for tables
        /// </summary>
        /// <param name="paragraph"></param>
        /// <param name="colspan"></param>
        /// <param name="label"></param>
        /// <param name="hasBorder"></param>
        /// <returns></returns>
        public PdfPCell Cell(Paragraph paragraph, int colspan, BaseColor BackgroundColor, string label, bool hasBorder = true)
        {
            PdfPCell cell = new PdfPCell();
            if (label.HasValue())
            {
                cell.AddElement(new Paragraph(ErsResources.GetFieldName(label), font_label));
            }
            cell.AddElement(paragraph);

            cell.Colspan = colspan;

            if (BackgroundColor != null)
            {
                cell.BackgroundColor = BackgroundColor;
            }

            if (!hasBorder)
            {
                cell.Border = Rectangle.NO_BORDER;
            }
            cell.Padding = 6;
            return cell;

        }
        #endregion

        #region PDF FIELDS
        /// <summary>
        /// Gets propeties with PdfField in a template model class and return as dictionary
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        protected virtual Dictionary<string, object> getPdfField(ErsModelBase template)
        {
            var dic = new Dictionary<string, object>();
            string displayName;

            foreach (var property in template.GetType().GetProperties())
            {
                if (Attribute.IsDefined(property, typeof(DisplayNameAttribute)))
                {
                    displayName = ((DisplayNameAttribute)(property.GetCustomAttributes(typeof(DisplayNameAttribute), true).FirstOrDefault())).DisplayName;
                }
                else
                {
                    displayName = property.Name;
                }

                if (Attribute.IsDefined(property, typeof(PdfFieldAttribute)))
                {
                    dic.Add(displayName, property.GetValue(template));
                }
            }
            return dic;
        }
        #endregion
    }


}
