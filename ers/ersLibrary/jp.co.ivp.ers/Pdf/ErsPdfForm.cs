﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.ComponentModel;

namespace jp.co.ivp.ers.Pdf
{
    /// <summary>
    /// メール送信クラス（ベースクラス）
    /// </summary>
    public class ErsPdfForm : ErsPdf
    {
        public override EnumPdfType type { get { return EnumPdfType.form; } }



        public void init(ErsModelBase fields, string file_name)
        {
            var list = new List<Dictionary<string, object>>();

            list.Add(getPdfField(fields));

            base.info_table = list;

            base.file_name = file_name;

            base.init();

        }


        public void init(List<ErsModelBase> fields_list, string file_name)
        {
           var list = new List<Dictionary<string, object>>();

            foreach (var item in fields_list)
            {
                list.Add(getPdfField(item));
            }

            base.info_table = list;

            base.file_name = file_name;

            base.init();

        }

        /// <summary>
        /// Generic table pdf creation
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="file_name"></param>
        public void init(List<Dictionary<string,object>> fields,string file_name)
        {
            base.file_name = file_name;

            base.init();
        }

        #region PDF FIELDS
        /// <summary>
        /// Gets propeties with PdfField in a template model class and return as dictionary
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        protected override Dictionary<string, object> getPdfField(ErsModelBase template)
        {
            var dic = new Dictionary<string, object>();
            string displayName;
            var request_type = (EnumRequestType)template.GetType().GetProperty("request_type").GetValue(template);

            foreach (var property in template.GetType().GetProperties())
            {
                if (Attribute.IsDefined(property, typeof(DisplayNameAttribute)))
                {
                    displayName = ((DisplayNameAttribute)(property.GetCustomAttributes(typeof(DisplayNameAttribute), true).FirstOrDefault())).DisplayName;
                }
                else
                {
                    displayName = property.Name;
                }

                if (Attribute.IsDefined(property, typeof(PdfFieldAttribute)))
                {
                    var types = ((PdfFieldAttribute)(property.GetCustomAttributes(typeof(PdfFieldAttribute), true).FirstOrDefault())).request_types;

                    if (types == null || types.Contains((int)request_type))
                    {
                        dic.Add(displayName, property.GetValue(template));
                    }
                }
            }
            return dic;
        }
        #endregion
    }


}
