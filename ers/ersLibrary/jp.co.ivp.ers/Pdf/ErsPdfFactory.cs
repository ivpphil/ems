﻿using ers.jp.co.ivp.ers.announcement.specification;
using ers.jp.co.ivp.ers.employee;
using ers.jp.co.ivp.ers.employee.specification;
using ers.jp.co.ivp.ers.employee.strategy;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.employee.specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using jp.co.ivp.ers.Pdf;

namespace jp.co.ivp.ers.Pdf
{
   public class ErsPdfFactory
    {
        public ErsPdfForm GetErsPdfForm()
        {

            return new ErsPdfForm();
        }

        public ErsPdfEmployee GetErsPdfEmployee()
        {

            return new ErsPdfEmployee();
        }
    }
}
