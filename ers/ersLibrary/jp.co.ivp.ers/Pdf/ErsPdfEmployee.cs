﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using jp.co.ivp.ers.employee;
using System.Globalization;
using System.ComponentModel;

namespace jp.co.ivp.ers.Pdf
{
    /// <summary>
    /// メール送信クラス（ベースクラス）
    /// </summary>
    public class ErsPdfEmployee:ErsModelBase
    {
        [PdfField]
        public string full_name
        {
            get
            {
                if (fname.HasValue() && lname.HasValue())
                {
                    return string.Join(" ", fname, lname);
                }
                return null;
            }
        }

        public string _fname { get; set; }

        public string fname
        {
            get { return _fname; }
            set
            {
                if (value.HasValue())
                {
                   _fname = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToLower());
                }
            }
        }

        public string _lname { get; set; }

        public string lname
        {
            get { return _lname; }
            set
            {
                if (value.HasValue())
                {
                    _lname = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToLower());
                }
            }
        }

        [PdfField]
        public string contact_no { get; set; }

        public EnumEmpStatus? status { get; set; }

        public int? job_title { get; set; }

        [PdfField]
        [DisplayName("job_title")]
        public string w_job_title
        {
            get
            {
                if (job_title != null)
                {
                    var job = ErsFactory.ersJobFactory.GetErsJobTitleWithID(Convert.ToInt32(job_title));
                    return job?.job_title;

                }
                return null;
            }
        }

        [PdfField]
        [DisplayName("status")]
        public string w_status {
            get
            {
                if (status != null)
                {
                    var common_name =  ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType.Emp_Status, (int?)status);
                    if (common_name != null)
                    {
                        return common_name.namename;
                    }
                }
                return null;
            }

        }

        [PdfField]
        public DateTime? date_hired { get; set; }

        [PdfField]
        public DateTime? reg_date { get; set; }


    }


}
