﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace jp.co.ivp.ers.Pdf.events
{
    /// <summary>
    /// メール送信クラス（ベースクラス）
    /// </summary>
    public class ErsPdfTemplate : PdfPageEventHelper
    {

        Setup setup = ErsFactory.ersUtilityFactory.getSetup();

        Image header_img = null;
        // This is the contentbyte object of the writer
        PdfContentByte header_cb, footer_cb;

        // we will put the final number of pages in a template
        PdfTemplate headerTemplate, footerTemplate;

        // this is the BaseFont we are going to use for the header / footer
        BaseFont bf = null;


        public string image_path
        {
            get { return string.Concat(setup.image_directory, "\\", setup.ivp_logo_img); }
        }

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            header_img = Image.GetInstance(this.image_path);
            header_img.SetAbsolutePosition(0, 0);
            bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            header_cb = writer.DirectContent;
            footer_cb = writer.DirectContent;
            headerTemplate = header_cb.CreateTemplate(header_img.Width, header_img.Height);
            footerTemplate = footer_cb.CreateTemplate(document.PageSize.Width, 50);

        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            //Set image header
            header_img.ScalePercent(70f);
            headerTemplate.AddImage(header_img);
            header_cb.AddTemplate(headerTemplate, 1f, 0f, 0f, 1f, (document.PageSize.Width / 2) - (header_img.ScaledWidth / 2), document.PageSize.Height - header_img.Height);

            footerTemplate.BeginText();
            footerTemplate.SetFontAndSize(bf, 8);
            footerTemplate.ShowText(string.Join(" ",ErsResources.GetMessage("office_address"),ErsResources.GetMessage("office_contact")));
            footerTemplate.EndText();
            footer_cb.AddTemplate(footerTemplate, 1f, 0f, 0f, 1f, 30, 50);

        }
        




    }


}
