﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Enums for Status (running or stopped)
    /// </summary>
    public enum EnumSeenFlg
    {
        unseen ,
        seen
    }
}
