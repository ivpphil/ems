﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumArticleState
    {
        NonActive = 0,
        Active,
        ActiveWait,
        OutsidePeriod
    }
}
