﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.stepmail
{
    /// <summary>
    /// represents searched conditions for properties that are in ErsStepMail class.
    /// Inherits Criteria class.
    /// </summary>
    public class ErsStepMailCriteria : Criteria
    {
        /// <summary>
        /// sets criteria for ID with the condition of step_mail_t.id = value
        /// </summary>
        public virtual int? id
        {
            set
            {
                Add(Criteria.GetCriterion("id", value, Operation.EQUAL));
            }
        }

        public int? id_not_equal
        {
            set
            {
                Add(Criteria.GetCriterion("id", value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for ID with the condition of step_mail_t.scenario_id = value
        /// </summary>
        public virtual int? scenario_id
        {
            set
            {
                Add(Criteria.GetCriterion("scenario_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for ID with the condition of step_mail_t.step_mail_name = value
        /// </summary>
        public virtual string step_mail_name
        {
            set
            {
                Add(Criteria.GetCriterion("step_mail_name", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for ID with the condition of step_mail_t.pc_mail_title = value
        /// </summary>
        public virtual string pc_mail_title
        {
            set
            {
                Add(Criteria.GetCriterion("pc_mail_title", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for ID with the condition of step_mail_t.pc_mail_body = value
        /// </summary>
        public virtual string pc_mail_body
        {
            set
            {
                Add(Criteria.GetCriterion("pc_mail_body", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for ID with the condition of step_mail_t.mobile_mail_title = value
        /// </summary>
        public virtual string mobile_mail_title
        {
            set
            {
                Add(Criteria.GetCriterion("mobile_mail_title", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for ID with the condition of step_mail_t.mobile_mail_body = value
        /// </summary>
        public virtual string mobile_mail_body
        {
            set
            {
                Add(Criteria.GetCriterion("mobile_mail_body", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for ID with the condition of step_mail_t.active = value
        /// </summary>
        public virtual EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("step_mail_t.active", (int?)value, Operation.EQUAL));
            }
        }

        public virtual EnumStatus? mail_status_kbn
        {
            set
            {
                Add(Criteria.GetCriterion("step_mail_t.mail_status_kbn", value, Operation.EQUAL));
            }
        }


        /// <summary>
        /// Sets order by deliverydate
        /// </summary>
        /// <param name="orderBy">order type</param>
        public virtual void SetOrderByDeliveryDate(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("step_mail_t.elapsed_num", orderBy);
            this.AddOrderBy("step_mail_t.elapsed_kbn", orderBy);
        }
    }
}
