﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.Text.RegularExpressions;
using System.Web;

namespace jp.co.ivp.ers.stepmail.strategy
{
    /// <summary>
    /// HTML本文変換 [ConvertHtmlBodyTag]
    /// </summary>
    public class ConvertHtmlBodyTag
    {
        /// <summary>
        /// 開封率取得画像タグ追加 [dd image tag for open rate]
        /// </summary>
        /// <param name="body">本文 [String of body]</param>
        /// <returns>変換後本文 [Converted body]</returns>
        public string AddImageTag(string body)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            string url = setup.pc_nor_url + setup.open_rate_image_url;

            return body + string.Format("<img src=\"{0}?id=<%=.Model.process_id%>&add=<%=.Model.etc1%>\"/>", url);
        }

        /// <summary>
        /// リダイレクトURLタグ変換 [Convert redirect tag
        /// </summary>
        /// <param name="body">本文 [String of body]</param>
        /// <param name="id">プロセスID [am_process_t.id]</param>
        /// <param name="enc">URLエンコーディング [Encoding for url]</param>
        /// <returns>変換後URL [Converted body]</returns>
        public string ConvertRedirectUrl(string body, int id, Encoding enc)
        {
            if (string.IsNullOrEmpty(body))
            {
                return body;
            }

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            string ptnTag = "<ers:redirectURL>(\\s*?)(\\r\\n|\\n|\\r)?(.*)?(\\r\\n|\\n|\\r)?(\\s*?)</ers:redirectURL>";

            var matches = new Regex(ptnTag, RegexOptions.IgnoreCase).Matches(body);

            foreach (Match m in matches)
            {
                string url = string.Format("{0}?id={1}&ptn={2}", setup.pc_nor_url + setup.click_count_url, id, this.UrlEncodeWithBindVariable(m.Groups[3].Value, enc));
                body = body.Replace(m.Value, url);
            }

            return body;
        }

        /// <summary>
        /// 変数以外をURLエンコード [Urlencode except for Variables]
        /// </summary>
        /// <param name="value">値 [Value]</param>
        /// <param name="enc">URLエンコーディング [Encoding for url]</param>
        /// <returns>エンコード後文字列 [Encoded string]</returns>
        protected string UrlEncodeWithBindVariable(string value, Encoding enc)
        {
            var strTarget = value;

            // 変数以外をURLエンコード [Urlencode except for Variables]
            var matches = new Regex("<%=[^%]+%>").Matches(strTarget);

            foreach (Match m in matches)
            {
                strTarget = strTarget.Replace(m.Groups[0].Value, "|");
            }
            strTarget = strTarget.TrimStart('|');
            strTarget = strTarget.TrimEnd('|');

            if (!string.IsNullOrEmpty(strTarget))
            {
                string escaped = ErsCommon.EscapeForRegex(strTarget);

                matches = new Regex("(" + escaped + ")").Matches(value);
                foreach (Match m in matches)
                {
                    var literal = m.Groups[0].Value;
                    value = value.Replace(literal, HttpUtility.UrlEncode(literal, enc));
                }
            }

            return value;
        }
    }
}
