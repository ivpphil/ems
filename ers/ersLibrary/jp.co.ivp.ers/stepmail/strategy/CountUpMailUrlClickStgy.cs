﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.stepmail.strategy
{
    public class CountUpMailUrlClickStgy
        : ISpecificationForSQL
    {
        /// <summary>
        /// Count up mail_url_click_counter_t.click_count
        /// </summary>
        /// <param name="process_id"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public int CountUp(int? process_id, string url)
        {
            var criteria = ErsFactory.ersStepMailFactory.GetErsMailUrlClickCounterCriteria();
            criteria.process_id = process_id;
            criteria.url = url;

            return ErsRepository.UpdateSatisfying(this, criteria);
        }

        public string asSQL()
        {
            return "UPDATE mail_url_click_counter_t SET click_count = click_count + 1, utime = current_timestamp";
        }
    }
}
