﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.stepmail.strategy
{
    public class UpdateMailToActiveSQLStgy
           : ISpecificationForSQL
    {
        /// <summary>
        /// 無効化 [Set non-active]
        /// </summary>
        /// <param name="listId">IDリスト [List of ID]</param>
        public void SetMailToNonActive(IList<int> listId)
        {
            var criteria = ErsFactory.ErsAtMailFactory.GetErsMailToCriteria();

            criteria.id_in = listId;

            ErsRepository.UpdateSatisfying(this, criteria);
        }

        /// <summary>
        /// SQL文 [SQL statement]
        /// </summary>
        /// <returns>SQL文 [SQL statement]</returns>
        public virtual string asSQL()
        {
            return string.Format("UPDATE am_mailto_t SET active = {0}", Convert.ToInt32(EnumActive.NonActive));
        }
    }
}
