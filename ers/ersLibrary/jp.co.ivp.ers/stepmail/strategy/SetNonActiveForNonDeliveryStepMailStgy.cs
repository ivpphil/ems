﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.stepmail.strategy
{
    /// <summary>
    /// 未配信ステップメール無効化 [Set non-active for non-delivery list of StepMail]
    /// </summary>
    public class SetNonActiveForNonDeliveryStepMailStgy 
    {
        /// <summary>
        /// 無効化 [Set non-active]
        /// </summary>
        /// <param name="listStepMail">ステップメールリスト [List of StepMail]</param>
        /// <param name="listSpecifiedScenarioId">指定ステップメールシナリオIDリスト [List of specified StepMailScenario id]</param>
        /// <param name="listSpecifiedMcode">対象会員コードリスト [DateTime of Now]</param>
        /// <param name="dateTarget">実行対象日時 [DateTime for Execute]</param>
        public void SetNonActive(IList<Dictionary<string, object>> listStepMail, IList<int> listSpecifiedScenarioId, IList<string> listSpecifiedMcode, DateTime dateTarget)
        {
            // ステップメールIDリスト取得 [Get the list of StepMail ID]
            IList<int> listStepMailId = this.GetStepMailIdList(listStepMail, listSpecifiedScenarioId);

            // 未配信IDリスト取得 [Get the list of non-delivery ID]
            IList<int> listMailToId = this.GetNonDeliverlyMailToIdList(listStepMailId, listSpecifiedMcode, dateTarget);

            // 無効化 [Set non-active]
            if (listMailToId != null && listMailToId.Count > 0)
            {
                ErsFactory.ersStepMailFactory.GetUpdateMailToActiveSQLStgy().SetMailToNonActive(listMailToId);
            }

            // 未配信のみ配信管理取得
            IList<int> listProcessId = this.GetNonDeliverlyProcessIdList();

            // 無効化 [Set non-active]
            if (listProcessId != null && listProcessId.Count > 0)
            {
                ErsFactory.ersStepMailFactory.GetUpdateProcessActiveStgy().SetProcessNonActive(listProcessId);
            }
        }

        /// <summary>
        /// ステップメールIDリスト取得 [Get the list of StepMail ID]
        /// </summary>
        /// <param name="listStepMail">ステップメールリスト [List of StepMail]</param>
        /// <param name="listSpecifiedScenarioId">指定ステップメールシナリオIDリスト [List of specified StepMailScenario id]</param>
        /// <returns>ステップメールIDリスト [List of StepMail ID]</returns>
        protected IList<int> GetStepMailIdList(IList<Dictionary<string, object>> listStepMail, IList<int> listSpecifiedScenarioId)
        {
            IList<int> listStepMailId = new List<int>();

            foreach (var data in listStepMail)
            {
                // 指定ステップメールシナリオID [Specified StepMailScenario id]
                if (listSpecifiedScenarioId != null && !listSpecifiedScenarioId.Contains(Convert.ToInt32(data["scenario_id"])))
                {
                    continue;
                }

                listStepMailId.Add(Convert.ToInt32(data["id"]));
            }

            return listStepMailId;
        }

        /// <summary>
        /// 未配信IDリスト取得 [Get the list of non-delivery ID]
        /// </summary>
        /// <param name="listStepMailId">ステップメールIDリスト [List of StepMail ID]</param>
        /// <param name="listSpecifiedMcode">対象会員コードリスト [DateTime of Now]</param>
        /// <param name="dateTarget">実行対象日時 [DateTime for Execute]</param>
        /// <returns>未配信IDリスト [List of non-delivery ID]</returns>
        protected IList<int> GetNonDeliverlyMailToIdList(IList<int> listStepMailId, IList<string> listSpecifiedMcode, DateTime dateTarget)
        {
            // ステップメール未配信ID取得 [Get the list of non-delively id]
            var spec = ErsFactory.ErsAtMailFactory.GetMailToIdSearchSpecification();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();

            // 未配信条件 [Condition for non-delivery]
            criteria.active = EnumActive.Active;
            criteria.up_kind = EnumUpKind.STEP_MAIL;
            criteria.status_in = new EnumAmProcessStatus[] { EnumAmProcessStatus.NotSend, EnumAmProcessStatus.Sending };
            criteria.step_mail_id_in = listStepMailId;

            criteria.mailTo_active = EnumActive.Active;
            criteria.mailTo_scheduled_date_greater = dateTarget;

            // 指定会員コード [Specified mcode]
            if (listSpecifiedMcode != null)
            {
                criteria.mailTo_mcode_in = listSpecifiedMcode;
            }

            // IDリスト取得 [Get the list of ID]
            var listRet = spec.GetSearchData(criteria);

            if (listRet.Count == 0)
            {
                return null;
            }

            IList<int> listId = new List<int>();

            foreach (var data in listRet)
            {
                listId.Add(Convert.ToInt32(data["id"]));
            }

            return listId;
        }

        /// <summary>
        /// 未配信IDリスト取得 [Get the list of non-delivery ID]
        /// </summary>
        /// <returns>未配信IDリスト [List of non-delivery ID]</returns>
        protected IList<int> GetNonDeliverlyProcessIdList()
        {
            // ステップメール未配信ID取得 [Get the list of non-delively id]
            var repository = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
            var criteria = ErsFactory.ErsAtMailFactory.GetErsProcessCriteria();

            // 未配信条件 [Condition for non-delivery]
            criteria.active = EnumActive.Active;
            criteria.up_kind = EnumUpKind.STEP_MAIL;
            criteria.status = EnumAmProcessStatus.NotSend;

            // 有効な配信管理なし [Not exists active mailto]
            criteria.SetExistsActiveMailTo(false);

            // IDリスト取得 [Get the list of ID]
            var listRet = repository.Find(criteria);

            if (listRet.Count == 0)
            {
                return null;
            }

            IList<int> listId = new List<int>();

            foreach (var data in listRet)
            {
                listId.Add(data.id.Value);
            }

            return listId;
        }
    }
}
