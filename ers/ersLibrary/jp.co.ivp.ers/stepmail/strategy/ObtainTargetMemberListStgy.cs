﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.Web;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.step_scenario;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.target;

namespace jp.co.ivp.ers.stepmail.strategy
{
    /// <summary>
    /// 対象会員リスト取得 [ObtainTargetMemberListStgy]
    /// </summary>
    public class ObtainTargetMemberListStgy
    {
        /// <summary>
        /// 対象会員リスト取得 [Obtain]
        /// </summary>
        /// <param name="objStepScenario">ステップメールシナリオ [StepMailScenario]</param>
        /// <param name="objStepMail">ステップメール [StepMail]</param>
        /// <param name="dicTargetItem">ステップメールシナリオ対象商品リスト [List of StepMailScenarioItem]</param>
        /// <param name="dateTarget">実行対象日時 [DateTime for Execute]</param>
        /// <param name="listSpecifiedMcode">対象会員コードリスト [DateTime of Now]</param>
        /// <returns>対象会員リスト [List of target members]</returns>
        public IList<ErsMember> Obtain(ErsTarget objTarget, ErsStepScenario objStepScenario, ErsStepMail objStepMail, IList<ErsTargetItem> listTargetItem, DateTime dateTarget, IList<string> listSpecifiedMcode)
        {
            ErsMemberRepository repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            ErsMemberCriteria criteria = this.GetCriteriaForMember(objTarget, objStepScenario, objStepMail, listTargetItem, dateTarget, listSpecifiedMcode);

            return repository.Find(criteria);
        }

        /// <summary>
        /// クライテリア取得（会員） [GetCriteriaForMember]
        /// </summary>
        /// <param name="objStepScenario">ステップメールシナリオ [StepMailScenario]</param>
        /// <param name="objStepMail">ステップメール [StepMail]</param>
        /// <param name="dicTargetItem">ステップメールシナリオ対象商品リスト [List of StepMailScenarioItem]</param>
        /// <param name="dateTarget">実行対象日時 [DateTime for Execute]</param>
        /// <param name="listSpecifiedMcode">対象会員コードリスト [DateTime of Now]</param>
        /// <returns>会員クライテリア [ErsMemberCriteria]</returns>
        protected ErsMemberCriteria GetCriteriaForMember(ErsTarget objTarget, ErsStepScenario objStepScenario, ErsStepMail objStepMail, IList<ErsTargetItem> listTargetItem, DateTime dateTarget, IList<string> listSpecifiedMcode)
        {
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();

            // 基本 [Base condition]
            criteria.deleted = EnumDeleted.NotDeleted;
            criteria.active = EnumActive.Active;
            criteria.m_flg = EnumMFlg.Deliver;
            criteria.ignoreMonitor();

            var buildCriteriaStgy = ErsFactory.ersTargetFactory.GetBuildTargetMemberCriteriaStgy();
            buildCriteriaStgy.Build(objTarget, criteria, dateTarget, listTargetItem, null);

            // 会員条件
            this.SetMemberCondition(criteria, objStepScenario, dateTarget);

            // 基準日条件 [Base day condition]
            this.SetBaseDayCondition(criteria, objTarget, objStepScenario, objStepMail, listTargetItem, dateTarget);

            // 会員コード指定 [Specified mcode]
            if (listSpecifiedMcode != null)
            {
                criteria.mcode_in = listSpecifiedMcode;
            }

            return criteria;
        }

        #region 会員条件セット [Set condition of order]
        /// <summary>
        /// 会員条件セット [Set condition of member]
        /// </summary>
        /// <param name="criteria">クライテリア（会員） [ErsMemberCriteria]</param>
        /// <param name="objStepScenario">ステップメールシナリオ [StepMailScenario]</param>
        /// <param name="dateTarget">実行対象日時 [DateTime for Execute]</param>
        private void SetMemberCondition(ErsMemberCriteria criteria, ErsStepScenario objStepScenario, DateTime dateTarget)
        {
            criteria.SetBetweenIntime(dateTarget.AddDays(-objStepScenario.reg_elapsed_to.Value).Date, dateTarget.AddDays(-objStepScenario.reg_elapsed_from.Value).Date);
            criteria.deleted = EnumDeleted.NotDeleted;
            criteria.ignoreMonitor();
        }
        #endregion

        #region 基準日条件セット [Set condition of base day]
        /// <summary>
        /// 基準日条件セット [Set condition of base day]
        /// </summary>
        /// <param name="criteria">クライテリア（会員） [ErsMemberCriteria]</param>
        /// <param name="objStepScenario">ステップメールシナリオ [StepMailScenario]</param>
        /// <param name="objStepMail">ステップメール [StepMail]</param>
        /// <param name="dateTarget">実行対象日時 [DateTime for Execute]</param>
        protected void SetBaseDayCondition(ErsMemberCriteria criteria, ErsTarget objTarget, ErsStepScenario objStepScenario, ErsStepMail objStepMail, IList<ErsTargetItem> listTargetItem, DateTime dateTarget)
        {
            if (objStepMail.elapsed_num == null)
            {
                return;
            }

            DateTime? dateTmp = null;

            // 経過日数区分 [Type of elapse days]
            switch (objStepMail.elapsed_kbn)
            {
                // 日前 [Days ago]
                case EnumElapsed.DaysAgo:
                    dateTmp = dateTarget.AddDays(objStepMail.elapsed_num.Value);
                    break;

                // 日後 [Days later]
                case EnumElapsed.DaysLater:
                    dateTmp = dateTarget.AddDays(-objStepMail.elapsed_num.Value);
                    break;

                // 月前 [Months ago]
                case EnumElapsed.MonthsAgo:
                    dateTmp = dateTarget.AddMonths(objStepMail.elapsed_num.Value);
                    break;

                // 月後 [Months later]
                case EnumElapsed.MonthsLater:
                    dateTmp = dateTarget.AddMonths(-objStepMail.elapsed_num.Value);
                    break;
            }

            var buildCriteriaStgy = ErsFactory.ersTargetFactory.GetBuildTargetMemberCriteriaStgy();
            var date_from = dateTmp.Value.GetStartForSearch();
            var date_to = dateTmp.Value.AddDays(1).GetStartForSearch();
            
            // 基準日区分 [Type of base datetime]
            switch (objStepScenario.mail_ref_date_kbn)
            {
                // 誕生日（月日） [Birth date (Month and day)]
                case EnumReferenceDate.Birthdate:
                    criteria.birth_as_mmdd = dateTmp.Value.ToString("MMdd");
                    break;

                // 最終購入日（年月日） [Last sale date (Year and month and day)]
                case EnumReferenceDate.LastPurchasedDate:
                    criteria.SetBetweenLastSaleDate(date_from, date_to, objTarget.site_id);
                    break;

                // 登録日（年月日） [Registration date (Year and month and day)]
                case EnumReferenceDate.RegistrationDate:
                    criteria.intime_as_date = dateTmp;
                    break;

                // 商品購入日
                case EnumReferenceDate.LastItemPurchasedDate:
                    {
                        var additionalQuery = "AND d_master_t.intime >= :date_from"
                            + "     AND d_master_t.intime < :date_to";
                        var additionalQueryParameter = new Dictionary<string, object>()
                        {
                            {"date_from", date_from},
                            {"date_to", date_to}
                        };
                        buildCriteriaStgy.SetOrderPurchaseIncludeItemCondition(criteria, listTargetItem, additionalQuery, additionalQueryParameter);
                        break;
                    }

                // 商品配送日
                case EnumReferenceDate.LastItemShippedDate:
                    {
                        var additionalQuery = "AND ds_master_t.shipdate >= :date_from"
                            + "     AND ds_master_t.shipdate < :date_to";
                        var additionalQueryParameter = new Dictionary<string, object>()
                        {
                            {"date_from", date_from},
                            {"date_to", date_to}
                        };
                        buildCriteriaStgy.SetOrderPurchaseIncludeItemCondition(criteria, listTargetItem, additionalQuery, additionalQueryParameter);
                        break;
                    }
            }
        }
        #endregion
    }
}
