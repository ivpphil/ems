﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.Web;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.step_scenario;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.target;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.stepmail.strategy
{
    /// <summary>
    /// ステップメール登録
    /// </summary>
    public class RegistStepMailToStgy
    {
        #region プロパティ [Properties]
        /// <summary>
        /// 設定 [SetupBatch]
        /// </summary>
        private SetupBatch setup { get; set; }

        /// <summary>
        /// 配信リストリポジトリ [ErsMailToRepository]
        /// </summary>
        protected ErsMailToRepository repositoryMailTo { get; set; }

        /// <summary>
        /// 配信管理リポジトリ [ErsProcessRepository]
        /// </summary>
        protected ErsProcessRepository repositoryProcess { get; set; }
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public RegistStepMailToStgy()
        {
            this.setup = ErsFactory.ersBatchFactory.getSetup();
            this.repositoryMailTo = ErsFactory.ErsAtMailFactory.GetErsMailToRepository();
            this.repositoryProcess = ErsFactory.ErsAtMailFactory.GetErsProcessRepository();
        }
        #endregion

        #region ステップメール登録 [Register StepMail]
        /// <summary>
        /// ステップメール登録 [RegistStepMail]
        /// </summary>
        /// <param name="listMember">会員リスト [List of members]</param>
        /// <param name="objStepScenario">ステップメールシナリオ [StepMailScenario]</param>
        /// <param name="objStepMailFromAddr">ステップメールFROM [StepMailFrom]</param>
        /// <param name="objStepMail">ステップメール [StepMail]</param>
        /// <param name="dateTarget">実行対象日時 [DateTime for Execute]</param>
        /// <param name="dicSendCount">送信数 [Count of send]</param>
        /// <returns>エラーリスト [List of error]</returns>
        public IList<StepMailError> RegistStepMail(IList<ErsMember> listMember, ErsStepScenario objStepScenario, ErsStepMail objStepMail, DateTime dateTarget, Dictionary<string, int> dicSendCount, IList<ErsTargetItem> listTargetItem)
        {
            IList<StepMailError> listError = new List<StepMailError>();

            DateTime? dateMinScheduleDate = null;
            int? process_id = null;
            int cnt = 0;

            try
            {
                // 配信管理ID取得（インサート用） [Get Process id for Insert]
                process_id = this.repositoryProcess.GetNextSequence();

                foreach (var member in listMember)
                {
                    try
                    {
                        // メール送信制限 [Limit for send mail]
                        if (this.setup.stepmail_send_max_count > 0)
                        {
                            if (dicSendCount.ContainsKey(member.mcode))
                            {
                                if (dicSendCount[member.mcode] >= this.setup.stepmail_send_max_count)
                                {
                                    continue;
                                }
                            }
                            else
                            {
                                dicSendCount[member.mcode] = 0;
                            }
                        }

                        // 配信リスト取得 [Get Instance of ErsMailTo]
                        ErsMailTo objMailTo = this.ObtainMailToFromStepMail(objStepScenario, member, process_id, dateTarget, listTargetItem);

                        // 最小送信日時取得 [Get minimum send date]
                        if (dateMinScheduleDate == null)
                        {
                            dateMinScheduleDate = objMailTo.scheduled_date;
                        }
                        else if (dateMinScheduleDate > objMailTo.scheduled_date)
                        {
                            dateMinScheduleDate = objMailTo.scheduled_date;
                        }

                        if (objMailTo.scheduled_date >= dateTarget)
                        {
                            // 配信リストインサート [Insert to am_mailto_t]
                            this.repositoryMailTo.Insert(objMailTo);
                            cnt++;
                        }

                        dicSendCount[member.mcode] = !dicSendCount.ContainsKey(member.mcode) ? 1 : dicSendCount[member.mcode] + 1;
                    }
                    catch (Exception e)
                    {
                        string error_message = ErsResources.GetMessage("65003", objStepScenario.id, objStepMail.id, process_id, member.mcode, e.ToString());
                        string command = string.Format("scenario_id={0} mcode={1}", objStepScenario.id, member.mcode);
                        listError.Add(new StepMailError(error_message, command));
                    }
                }
            }
            catch (Exception e)
            {
                IList<string> listMcode = new List<string>();

                foreach (var member in listMember)
                {
                    listMcode.Add(member.mcode);
                }

                string mcode = String.Join(",", listMcode);
                string error_message = ErsResources.GetMessage("65004", objStepScenario.id, objStepMail.id, process_id != null ? process_id.Value.ToString() : string.Empty, mcode, e.ToString());
                string command = string.Format("scenario_id={0} mcode={1}", objStepScenario.id, mcode);
                listError.Add(new StepMailError(error_message, command));
            }

            if (cnt > 0)
            {
                // 配信管理取得 [Get Instance of ErsProcess]
                ErsProcess objProcess = this.ObtainProcessFromStepMail(objStepScenario, objStepMail, process_id, dateMinScheduleDate.Value);

                // 配信管理インサート [Insert to am_process_t]
                this.repositoryProcess.Insert(objProcess);
            }

            return listError;
        }
        #endregion

        #region 配信リスト取得 [Obtain list of MailTo]
        /// <summary>
        /// 配信リスト取得 [ObtainMailToFromStepMail]
        /// </summary>
        /// <param name="objStepScenario">ステップメールシナリオ [ErsStepScenario]</param>
        /// <param name="objMember">会員 [ErsMember]</param>
        /// <param name="process_id">配信管理ID [am_process_t.id]</param>
        /// <returns>配信リスト [ErsMailTo]</returns>
        protected ErsMailTo ObtainMailToFromStepMail(ErsStepScenario objStepScenario, ErsMember objMember, int? process_id, DateTime dateTarget, IList<ErsTargetItem> listTargetItem)
        {
            ErsMailTo objMailTo = ErsFactory.ErsAtMailFactory.GetErsMailTo();

            // メール受信送信形式取得 [Get mail format]
            var mformat = objMember.mformat;

            if (mformat == null)
            {
                mformat = ErsFactory.ErsAtMailFactory.GetDetermineMformatStgy().WithDomain(objMember.email);
            }

            objMailTo.process_id = process_id;

            objMailTo.mcode = objMember.mcode;
            objMailTo.email = objMember.email;
            objMailTo.lname = objMember.lname;
            objMailTo.fname = objMember.fname;

            // 暗号化メールアドレス [Encoded email address]
            objMailTo.etc1 = ErsFactory.ersUtilityFactory.getErsEncryption().HexEncode(objMember.email);

            // 送信日時取得 [Get datetime for send]
            objMailTo.scheduled_date = this.ObtainScheduledDateForStepMail(objStepScenario, objMember, dateTarget,listTargetItem);

            objMailTo.mformat = mformat.Value;

            objMailTo.sent_flg = EnumSentFlg.NotSent;
            objMailTo.active = EnumActive.Active;
            objMailTo.intime = DateTime.Now;

            return objMailTo;
        }

        /// <summary>
        /// 送信日時取得 [ObtainScheduledDateForStepMail]
        /// </summary>
        /// <param name="objStepScenario">ステップメールシナリオ [ErsStepScenario]</param>
        /// <param name="objMember">会員 [ErsMember]</param>
        /// <returns>送信日時 [Scheduled datetime]</returns>
        protected DateTime ObtainScheduledDateForStepMail(ErsStepScenario objStepScenario, ErsMember objMember, DateTime dateTarget, IList<ErsTargetItem> listTargetItem)
        {
            // 除外時刻の場合 [Not delivery time]
            if (objStepScenario.mail_delv_time_kbn == EnumDeliveryTime.NotDeliveryTime)
            {
                // 時・分を数値変換 [HH:mm -> int(HHmm)]
                DateTime? sendingTime;
                //送信用の時間を保持
                if (objStepScenario.mail_ref_date_kbn == EnumReferenceDate.LastPurchasedDate)
                {
                    sendingTime = objMember.last_sale_date;
                }
                else
                {
                    sendingTime = this.GetItemSoldTime(objMember, listTargetItem);
                }
                //int last_sale_date = Convert.ToInt32(objMember.last_sale_date.Value.ToString("HHmm"));

                int last_sale_date = Convert.ToInt32(sendingTime.Value.ToString("HHmm"));
                int from = Convert.ToInt32(string.Format("{0:00}{1:00}", objStepScenario.mail_delv_out_time_hh_from, objStepScenario.mail_delv_out_time_mm_from));
                int to = Convert.ToInt32(string.Format("{0:00}{1:00}", objStepScenario.mail_delv_out_time_hh_to, objStepScenario.mail_delv_out_time_mm_to));

                bool exclude = false;

                // ex. 10:00 < 14:00
                if (from <= to)
                {
                    if (from <= last_sale_date && last_sale_date <= to)
                    {
                        exclude = true;
                    }
                }
                // ex. 22:00 < 02:00
                else
                {
                    int targetMinutes = (sendingTime.Value.Hour * 60) + sendingTime.Value.Minute;
                    int fromMinutes = (objStepScenario.mail_delv_out_time_hh_from.Value * 60) + objStepScenario.mail_delv_out_time_mm_from.Value;
                    int toMinutes = (objStepScenario.mail_delv_out_time_hh_to.Value * 60) + objStepScenario.mail_delv_out_time_mm_to.Value;

                    if (toMinutes > targetMinutes)
                    {
                        targetMinutes += (24 * 60);
                    }

                    toMinutes += (24 * 60);

                    if (from <= last_sale_date && last_sale_date <= to)
                    {
                        exclude = true;
                    }
                }

                if (exclude)
                {
                    // (システム + 除外日時) - 15分
                    // (yyyy/MM/dd -> System + HH:mm:ss -> scenario.mail_delv_out_time_hh_from:scenario.mail_delv_out_time_mm_from:0) - 15 min
                    return new DateTime(dateTarget.Year, dateTarget.Month, dateTarget.Day, objStepScenario.mail_delv_out_time_hh_from.Value, objStepScenario.mail_delv_out_time_mm_from.Value, 0).AddMinutes(-15);
                }
                else
                {
                    // システム + 最終購入日時
                    // yyyy/MM/dd -> System + HH:mm:ss -> member.las_sale_date.hh:member.las_sale_date.mm:member.las_sale_date.ss
                    return new DateTime(dateTarget.Year, dateTarget.Month, dateTarget.Day, sendingTime.Value.Hour, sendingTime.Value.Minute, sendingTime.Value.Second);
                }
            }

            // システム + 配信日時
            // yyyy/MM/dd -> System + HH:mm:ss -> scenario.mail_delv_time_hh_from:scenario.mail_delv_time_mm_from:0
            return new DateTime(dateTarget.Year, dateTarget.Month, dateTarget.Day, objStepScenario.mail_delv_time_hh.Value, objStepScenario.mail_delv_time_mm.Value, 0);
        }
        #endregion
        //
        DateTime? GetItemSoldTime(ErsMember objMember, IList<ErsTargetItem> listTargetItem)
        {
            
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var crtOrder = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

            List<Dictionary<string, object>> listItem = new List<Dictionary<string, object>>();

            if (listTargetItem.Count == 0)
            {
                crtOrder.mcode = null;
            }
            foreach (var item in listTargetItem)
            {
                // 対象商品 [Item]
                
                
                if (item.target_kbn == EnumTargetKbn.Item)
                {
                    //int order_type = (int)item.order_type;
                    int[] order_type;
                    if (item.order_type == EnumOrderType.BothUsuallyAndSubscription)
                    {
                        order_type = new int[] { (int)EnumOrderType.Usually, (int)EnumOrderType.Subscription };
                    }
                    else
                    {
                        order_type = new int[] { (int)item.order_type };
                    }
                    listItem.Add(new Dictionary<string, object>() { { "scode", item.scode }, { "order_type", order_type } });
                }
            }

            // 指定された会員の伝票
            crtOrder.mcode = objMember.mcode;
            // listTargetItemに指定されている商品名＆購入方法(定期通常)が含まれる伝票
            if (listItem.Count > 0)
            {
                crtOrder.SetTargetExistsSpecifiedItem(listItem);
            }
            // 購入日でソートしてLIMI１で１件取得
            crtOrder.SetOrderByIntime(db.Criteria.OrderBy.ORDER_BY_DESC);
            crtOrder.LIMIT = 1;
            crtOrder.order_status_not_in = (new EnumOrderStatusType[] {EnumOrderStatusType.CANCELED });
            var orderList = repository.Find(crtOrder);
            var bill = orderList.First();
            
            // d_master_t.intime を取得
            return bill.intime;
           
        }

        #region 配信管理取得 [Obtain Process]
        /// <summary>
        /// 配信管理取得 [ObtainProcessFromStepMail]
        /// </summary>
        /// <param name="objStepScenario">ステップメールシナリオ [ErsStepScenario]</param>
        /// <param name="objStepMailFromAddr">ステップメールFROM [StepMailFrom]</param>
        /// <param name="objStepMail">ステップメール [ErsMember]</param>
        /// <param name="process_id">配信管理ID [am_process_t.id]</param>
        /// <param name="scheduled_date">最小送信日時 [MIN(am_mailto_t.scheduled_date)]</param>
        /// <returns>配信リスト [ErsProcess]</returns>
        protected ErsProcess ObtainProcessFromStepMail(ErsStepScenario objStepScenario, ErsStepMail objStepMail, int? process_id, DateTime scheduled_date)
        {
            ErsProcess objProcess = ErsFactory.ErsAtMailFactory.GetErsProcess();

            var stgy = ErsFactory.ersStepMailFactory.GetConvertHtmlBodyTag();
            var enc = ErsEncoding.ShiftJIS;

            // 本文変換 [Convert body]
            string html_body = null;
            string body = null;
            string feature_body = null;

            if (!string.IsNullOrEmpty(objStepMail.pc_mail_body))
            {
                html_body = stgy.ConvertRedirectUrl(objStepMail.pc_mail_body, process_id.Value, enc);
                html_body = stgy.AddImageTag(html_body);
            }

            if (!string.IsNullOrEmpty(objStepMail.pc_mail_body_txt))
            {
                body = stgy.ConvertRedirectUrl(objStepMail.pc_mail_body_txt, process_id.Value, enc);
            }

            if (!string.IsNullOrEmpty(objStepMail.mobile_mail_body))
            {
                feature_body = stgy.ConvertRedirectUrl(objStepMail.mobile_mail_body, process_id.Value, enc);
            }

            objProcess.id = process_id;
            objProcess.step_mail_id = objStepMail.id;
            objProcess.up_kind = EnumUpKind.STEP_MAIL;
            objProcess.status = EnumAmProcessStatus.NotSend;

            objProcess.subject = objStepMail.pc_mail_title;
            objProcess.html_body = html_body;
            objProcess.body = body;
            objProcess.feature_body = feature_body;

            objProcess.from_email = objStepScenario.mail_from_addr;
            objProcess.from_name = objStepScenario.mail_from_name;
            objProcess.reply_email = objStepScenario.mail_reply_addr;

            objProcess.scheduled_date = scheduled_date;
            objProcess.active = EnumActive.Active;
            objProcess.intime = DateTime.Now;

            return objProcess;
        }
        #endregion
    }
}
