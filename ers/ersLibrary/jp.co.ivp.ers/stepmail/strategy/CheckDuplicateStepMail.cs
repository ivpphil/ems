﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.stepmail.strategy
{
    /// <summary>
    /// checking for duplicate step mail name
    /// </summary>
    public class CheckDuplicateStepMail
    {
        /// <summary>
        /// check if the step mail name is already exist
        /// </summary>
        /// <param name="stepmail_name">step mail name</param>
        /// <returns>returns error if already exists and false if not</returns>
        public virtual IEnumerable<ValidationResult> Check(string stepmail_name)
        {

            if (string.IsNullOrEmpty(stepmail_name))
            {
                yield break;
            }

            if (this.CheckStepMailNameExist(null, stepmail_name))
            {
                yield return new ValidationResult(ErsResources.GetMessage(("10103"), ErsResources.GetFieldName("step_mail_name"), stepmail_name), new[] { "step_mail_name" });
            }
        }

        public virtual IEnumerable<ValidationResult> Check(int? id, string stepmail_name)
        {
            if (!id.HasValue || string.IsNullOrEmpty(stepmail_name))
            {
                yield break;
            }

            if (this.CheckStepMailNameExist(id, stepmail_name))
            {
                yield return new ValidationResult(ErsResources.GetMessage(("10103"), ErsResources.GetFieldName("step_mail_name"), stepmail_name), new[] { "step_mail_name" });
            }
        }

        /// <summary>
        /// check if the step mail name is already exist
        /// </summary>
        /// <param name="value">step mail name</param>
        /// <returns>returns true if already exists and false if not</returns>
        public virtual Boolean CheckStepMailNameExist(int? id, string value)
        {
            var repository = ErsFactory.ersStepMailFactory.GetErsStepMailRepository();
            var criteria = ErsFactory.ersStepMailFactory.GetErsStepMailCriteria();
            criteria.step_mail_name = value;
            if (id.HasValue)
            {
                criteria.id_not_equal = id;
            }
            criteria.active = EnumActive.Active;
            return (repository.GetRecordCount(criteria) > 0);
        }
    }
}
