﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.stepmail
{
    public class ErsMailUrlClickCounterRepository
        : ErsRepository<ErsMailUrlClickCounter>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsMailUrlClickCounterRepository()
              : base("mail_url_click_counter_t")
        {
        }

        public ErsMailUrlClickCounterRepository(ErsDatabase objDB)
            : base("mail_url_click_counter_t", objDB)
        {
        }

        public override IList<ErsMailUrlClickCounter> Find(db.Criteria criteria)
        {
            var retList = new List<ErsMailUrlClickCounter>();

            var list = this.ersDB_table.gSelect(criteria);

            if (list.Count == 0)
                return retList;

            foreach (var dr in list)
            {
                var stepMail = ErsFactory.ersStepMailFactory.GetErsMailUrlClickCounterWithParameters(dr);
                retList.Add(stepMail);
            }
            return retList;
        }
    }
}
