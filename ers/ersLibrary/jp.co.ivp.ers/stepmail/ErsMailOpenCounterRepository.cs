﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.stepmail
{
    public class ErsMailOpenCounterRepository
        : ErsRepository<ErsMailOpenCounter>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsMailOpenCounterRepository()
            : base("mail_open_counter_t")
        {
        }

        public ErsMailOpenCounterRepository(ErsDatabase objDB)
            : base("mail_open_counter_t", objDB)
        {
        }

        public override IList<ErsMailOpenCounter> Find(db.Criteria criteria)
        {
            var retList = new List<ErsMailOpenCounter>();

            var list = this.ersDB_table.gSelect(criteria);

            if (list.Count == 0)
                return retList;

            foreach (var dr in list)
            {
                var stepMail = ErsFactory.ersStepMailFactory.GetErsMailOpenCounterWithParameters(dr);
                retList.Add(stepMail);
            }
            return retList;
        }
    }
}
