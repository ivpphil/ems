﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.stepmail
{
    /// <summary>
    /// Provide methods to connect with step_mail_t table. 
    /// Inherits ErsRepository<ErsMerchandiseInBasket>
    /// </summary>
    public class ErsStepMailRepository
        : ErsRepository<ErsStepMail>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsStepMailRepository()
            : base("step_mail_t")
        {
        }

        public ErsStepMailRepository(ErsDatabase objDB)
            : base("step_mail_t", objDB)
        {
        }

        /// <summary>
        /// Gets list of records that have been searched in ErsStepMail group of tables from criteria values using ErsStepMailCriteria
        /// </summary>
        /// <param name="criteria">set of property conditions found in ErsStepMailCriteria</param>
        /// <returns>returns list of records in ErsStepMail based on the criterias given</returns>
        public override IList<ErsStepMail> Find(db.Criteria criteria)
        {
            var retList = new List<ErsStepMail>();

            var list = this.ersDB_table.gSelect(criteria);

            if (list.Count == 0)
                return new List<ErsStepMail>();

            foreach (var dr in list)
            {
                var stepMail = ErsFactory.ersStepMailFactory.GetErsStepMailWithParameters(dr);
                retList.Add(stepMail);
            }
            return retList;
        }
    }
}
