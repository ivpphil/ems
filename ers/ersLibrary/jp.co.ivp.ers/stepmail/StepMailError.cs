﻿
namespace jp.co.ivp.ers.stepmail
{
    /// <summary>
    /// ステップメールエラー [Error for Stepmail]
    /// </summary>
    public struct StepMailError
    {
        /// <summary>
        /// エラーメッセージ [Error message]
        /// </summary>
        public string error_message;

        /// <summary>
        /// コマンドライン引数（リカバリ用） [Argument of command line (for recovery)]
        /// </summary>
        public string command;


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="error_message">エラーメッセージ [Error message]</param>
        /// <param name="command">コマンドライン引数（リカバリ用） [Argument of command line (for recovery)]</param>
        public StepMailError(string _error_message, string _command)
        {
            error_message = _error_message;
            command = _command;
        }
    }
}
