﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.stepmail
{
    public class ErsMailUrlClickCounter
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public int? process_id { get; set; }

        public int? step_mail_id { get; set; }

        public string url { get; set; }

        public int? click_count { get; set; }

        public DateTime? intime { get; set; }

        public DateTime? utime { get; set; }

        public EnumActive? active { get; set; }
    }
}
