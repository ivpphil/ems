﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.stepmail
{
    public class ErsMailOpenCounterCriteria
        : Criteria
    {
        public int? process_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mail_open_counter_t.process_id", value, Operation.EQUAL));
            }
        }

        public string mail_addr
        {
            set
            {
                this.Add(Criteria.GetCriterion("mail_open_counter_t.mail_addr", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("mail_open_counter_t.active", value, Operation.EQUAL));
            }
        }
    }
}
