﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.stepmail.strategy;

namespace jp.co.ivp.ers.stepmail
{
    /// <summary>
    /// カート関連クラス用Factory.
    /// Provide methods that are related in step_mail.
    /// </summary>
    public class ErsStepMailFactory
    {
        /// <summary>
        /// Get the step mail criteria
        /// </summary>
        /// <returns>returns instance of ErsStepMailCriteria</returns>
        public virtual ErsStepMailCriteria GetErsStepMailCriteria()
        {
            return new ErsStepMailCriteria();
        }

        /// <summary>
        /// Get the step mail repository
        /// </summary>
        /// <returns>returns instance of ErsStepMailRepository</returns>
        public virtual ErsStepMailRepository GetErsStepMailRepository()
        {
            return new ErsStepMailRepository();
        }

        /// <summary>
        /// Get the step mail entity
        /// </summary>
        /// <returns>returns instance of ErsStepMail</returns>
        public virtual ErsStepMail GetErsStepMail()
        {
            return new ErsStepMail();
        }

        /// <summary>
        /// Get the step mail entity with model
        /// </summary>
        /// <returns>returns instance of ErsStepMail</returns>
        public virtual ErsStepMail GetErsStepMailWithModel(ErsModelBase model)
        {
            var news = this.GetErsStepMail();
            news.OverwriteWithModel(model);
            return news;
        }

        /// <summary>
        /// Get the step mail entity with parameters
        /// </summary>
        /// <returns>returns instance of ErsStepMail</returns>
        public virtual ErsStepMail GetErsStepMailWithParameters(Dictionary<string, object> parameters)
        {
            var news = this.GetErsStepMail();
            news.OverwriteWithParameter(parameters);
            return news;
        }

        /// <summary>
        /// Get the step mail entity with id
        /// </summary>
        /// <returns>returns instance of ErsStepMail</returns>
        public virtual ErsStepMail GetErsStepMailWithId(int? id)
        {
            var repository = this.GetErsStepMailRepository();
            var criteria = this.GetErsStepMailCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");

            return list[0];
        }

        /// <summary>
        /// Get the check duplicate class 
        /// </summary>
        /// <returns>returns instance of CheckDuplicateStepMail</returns>
        public virtual CheckDuplicateStepMail GetCheckDuplicateStepMail()
        {
            return new CheckDuplicateStepMail();
        }

        /// <summary>
        /// Get the convert html tag class
        /// </summary>
        /// <returns>returns instance of ConvertHtmlBodyTag</returns>
        public virtual ConvertHtmlBodyTag GetConvertHtmlBodyTag()
        {
            return new ConvertHtmlBodyTag();
        }

        /// <summary>
        /// Get the register step mail class
        /// </summary>
        /// <returns>returns instance of RegistStepMailToStgy</returns>
        public virtual RegistStepMailToStgy GetRegistStepMailToStgy()
        {
            return new RegistStepMailToStgy();
        }

        /// <summary>
        /// Get the obtain the list of target members class
        /// </summary>
        /// <returns>returns instance of ObtainTargetMemberListStgy</returns>
        public virtual ObtainTargetMemberListStgy GetObtainTargetMemberListStgy()
        {
            return new ObtainTargetMemberListStgy();
        }

        /// <summary>
        /// Get the set non-active for non-delivery StepMail class
        /// </summary>
        /// <returns>returns instance of SetNonActiveForNonDeliveryStepMailStgy</returns>
        public virtual SetNonActiveForNonDeliveryStepMailStgy GetSetNonActiveForNonDeliveryStepMailStgy()
        {
            return new SetNonActiveForNonDeliveryStepMailStgy();
        }

        /// <summary>
        /// Get instance of ErsMailUrlClickCounterRepository
        /// </summary>
        /// <returns></returns>
        public virtual ErsMailUrlClickCounterRepository GetErsMailUrlClickCounterRepository()
        {
            return new ErsMailUrlClickCounterRepository();
        }

        /// <summary>
        /// Get instance of ErsMailUrlClickCounterCriteria
        /// </summary>
        /// <returns></returns>
        public virtual ErsMailUrlClickCounterCriteria GetErsMailUrlClickCounterCriteria()
        {
            return new ErsMailUrlClickCounterCriteria();
        }

        /// <summary>
        /// Get instance of ErsMailUrlClickCounter
        /// </summary>
        /// <returns></returns>
        public virtual ErsMailUrlClickCounter GetErsMailUrlClickCounter()
        {
            return new ErsMailUrlClickCounter();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public virtual ErsMailUrlClickCounter GetErsMailUrlClickCounterWithParameters(Dictionary<string, object> parameters)
        {
            var obj = this.GetErsMailUrlClickCounter();
            obj.OverwriteWithParameter(parameters);
            return obj;
        }

        /// <summary>
        /// Get instance of CountUpMailUrlClickStgy
        /// </summary>
        /// <returns></returns>
        public virtual CountUpMailUrlClickStgy GetCountUpMailUrlClickStgy()
        {
            return new CountUpMailUrlClickStgy();
        }

        /// <summary>
        /// Get instance of ErsMailOpenCounterRepository
        /// </summary>
        /// <returns></returns>
        public virtual ErsMailOpenCounterRepository GetErsMailOpenCounterRepository()
        {
            return new ErsMailOpenCounterRepository();
        }

        /// <summary>
        /// Get instance of ErsMailOpenCounterCriteria
        /// </summary>
        /// <returns></returns>
        public virtual ErsMailOpenCounterCriteria GetErsMailOpenCounterCriteria()
        {
            return new ErsMailOpenCounterCriteria();
        }

        /// <summary>
        /// Get instance of ErsMailOpenCounter
        /// </summary>
        /// <returns></returns>
        public virtual ErsMailOpenCounter GetErsMailOpenCounter()
        {
            return new ErsMailOpenCounter();
        }

        /// <summary>
        /// Get instance of ErsMailOpenCounter
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public virtual ErsMailOpenCounter GetErsMailOpenCounterWithParameters(Dictionary<string, object> parameters)
        {
            var obj = this.GetErsMailOpenCounter();
            obj.OverwriteWithParameter(parameters);
            return obj;
        }


        public virtual UpdateProcessActiveStgy GetUpdateProcessActiveStgy()
        {
            return new UpdateProcessActiveStgy();
        }

        public virtual UpdateMailToActiveSQLStgy GetUpdateMailToActiveSQLStgy()
        {
            return new UpdateMailToActiveSQLStgy();
        }
    }
}
