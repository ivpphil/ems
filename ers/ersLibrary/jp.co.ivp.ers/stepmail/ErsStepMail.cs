﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.stepmail
{
    /// <summary>
    /// Hold values from step_mail_t table from database.
    /// Inherits ErsRepositoryEntity class.
    /// </summary>    
    public class ErsStepMail : ErsRepositoryEntity
    {
        /// <summary>
        /// id of step_mail_t
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// id of step_scenario_t
        /// </summary>
        public virtual int? scenario_id { get; set; }

        /// <summary>
        /// step mail name
        /// </summary>
        public virtual string step_mail_name { get; set; }

        /// <summary>
        /// elapsed number
        /// </summary>
        public virtual int? elapsed_num { get; set; }

        /// <summary>
        /// elapsed kbn
        /// </summary>
        public virtual EnumElapsed? elapsed_kbn { get; set; }

        /// <summary>
        /// mail title for pc
        /// </summary>
        public virtual string pc_mail_title { get; set; }

        /// <summary>
        /// mail body html type for pc
        /// </summary>
        public virtual string pc_mail_body { get; set; }

        //[ErsSchemaValidation("step_mail_t.mobile_mail_title")]
        //public virtual string mobile_mail_title { get; set; }

        /// <summary>
        /// mail body for mobile
        /// </summary>
        public virtual string mobile_mail_body { get; set; }

        /// <summary>
        /// update time
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// insert time
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// active status
        /// </summary>
        public virtual int? active { get; set; }

        /// <summary>
        /// mail body text type for pc
        /// </summary>
        public virtual string pc_mail_body_txt { get; set; }

        /// <summary>
        /// mail status kbn
        /// </summary>
        public EnumStatus? mail_status_kbn { get; set; }

        /// <summary>
        /// elapsed status name
        /// </summary>
        public virtual string elapsed_kbn_name
        {
            get
            {
                if (!elapsed_kbn.HasValue)
                    return string.Empty;

                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Elapsed, EnumCommonNameColumnName.namename, (int?)this.elapsed_kbn);
            }
        }
    }
}
