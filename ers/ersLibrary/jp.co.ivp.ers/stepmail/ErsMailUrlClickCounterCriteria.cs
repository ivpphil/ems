﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.stepmail
{
    public class ErsMailUrlClickCounterCriteria
        : Criteria
    {
        public int? process_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mail_url_click_counter_t.process_id", value, Operation.EQUAL));
            }
        }

        public string url
        {
            set
            {
                this.Add(Criteria.GetCriterion("mail_url_click_counter_t.url", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("mail_url_click_counter_t.active", value, Operation.EQUAL));
            }
        }

        public void SetOrderByClickCount(OrderBy orderBy)
        {
            this.AddOrderBy("mail_url_click_counter_t.click_count", orderBy);
        }
    }
}
