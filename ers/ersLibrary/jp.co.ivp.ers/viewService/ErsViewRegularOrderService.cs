﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewRegularOrderService
    {

        /// <summary>
        /// Gets a list of months of the merchandise arrivals at first time.
        /// </summary>
        /// <returns></returns>
        public List<DateTime> GetListRegularSenddateMonth()
        {
            var list = new List<DateTime>();

            var sendday = ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetRegularFromDate(DateTime.Now);
            if (sendday.HasValue)
            {
                var min_senddate_day = Convert.ToDateTime(sendday.Value.ToString("yyyy/MM/01"));

                for (var i = 0; i < 3; i++)
                {
                    list.Add(min_senddate_day.AddMonths(i));
                }
            }
            return list;
        }

        public virtual string GetDeliveryTerm(IManageRegularPatternDatasource objDetail)
        {
            string retVal = string.Empty;
            var commonNameService = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService();
            switch (objDetail.send_ptn)
            {
                case EnumSendPtn.MONTH_INTERVALS:
                    if (objDetail.ptn_interval_month != null && objDetail.ptn_day != null)
                    {
                        retVal = string.Format("{0} {1}",
                            ErsResources.GetMessage(commonNameService.GetStringFromId(EnumCommonNameType.PtnIntervalMonth, EnumCommonNameColumnName.namename, objDetail.ptn_interval_month)),
                            ErsResources.GetMessage(commonNameService.GetStringFromId(EnumCommonNameType.PtnDay, EnumCommonNameColumnName.namename, objDetail.ptn_day)));
                    }
                    break;

                case EnumSendPtn.WEEK_INTERVALS:
                    if (objDetail.ptn_interval_month != null && objDetail.ptn_interval_week != null && objDetail.ptn_weekday != null)
                    {
                        retVal = string.Format("{0} {1} {2}",
                            ErsResources.GetMessage(commonNameService.GetStringFromId(EnumCommonNameType.PtnIntervalMonth, EnumCommonNameColumnName.namename, objDetail.ptn_interval_month)),
                            ErsResources.GetMessage(commonNameService.GetStringFromId(EnumCommonNameType.PtnIntervalWeek, EnumCommonNameColumnName.namename, objDetail.ptn_interval_week)),
                            ErsResources.GetMessage(commonNameService.GetStringFromId(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename, (int?)objDetail.ptn_weekday)));
                    }
                    break;
                case EnumSendPtn.DAY_INTERVALS:
                    if (objDetail.ptn_interval_day != null)
                    {
                        retVal = string.Format("{0}",
                            ErsResources.GetMessage(commonNameService.GetStringFromId(EnumCommonNameType.PtnIntervalDay, EnumCommonNameColumnName.namename, objDetail.ptn_interval_day)));
                    }
                    break;
            }

            return retVal;
        }
    }
}