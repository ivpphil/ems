﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of cards from card_t table.
    /// </summary>
    public class ErsViewCardService
        : ErsViewServiceBase
    {
        public const string cacheKey = "card_name-name_id";

        private List<Dictionary<string, object>> GetCachedList(int? site_id = null)
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                if (site_id == null)
                {
                    site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
                }
                var repository = ErsFactory.ersOrderFactory.GetErsCardRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsCardCriteria();
                criteria.site_id = site_id.Value;

                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var objList = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cacheKey];
        }
        /// <summary>
        /// Get list of cards
        /// </summary>
        /// <returns>List (id and card_name)</returns>
        public virtual List<Dictionary<string, object>> SelectAsList(int? site_id = null)
        {
            var list = this.GetCachedList(site_id);

            list = this.GetOnlyActiveRecord(list);

            list = this.GetNameValueList(list, "card_name", "id");

            return list;
        }

        /// <summary>
        /// Get card_t.card_name from supplied ID
        /// </summary>
        /// <param name="id">Card Id number</param>
        /// <returns>Returns card name based on the specified ID</returns>
        public virtual string GetStringFromId(int? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "card_name", "id", id);
        }

        public virtual List<Dictionary<string, object>> GetListYear()
        {
            List<Dictionary<string, object>> retList = new List<Dictionary<string, object>>();

            var repository = ErsFactory.ersCommonFactory.GetErsEraRepository();
            var criteria = ErsFactory.ersCommonFactory.GetErsEraCriteria();
            criteria.SetOrderByYear(Criteria.OrderBy.ORDER_BY_DESC);
            var list = repository.Find(criteria);

            int fromYear = DateTime.Now.Year;
            int toYear = DateTime.Now.AddYears(10).Year;
            int countDt = 0;

            for (int i = fromYear; i < toYear; i++)
            {
                var dr = list[countDt];
                if (list.Count - 1 > countDt && dr.year > i)
                {
                    countDt++;
                }
                var dictionary = new Dictionary<string, object>();
                dictionary["era"] = dr.era;
                dictionary["eraYear"] = i - dr.year + 1;
                dictionary["value"] = i;
                retList.Add(dictionary);
            }

            return retList;
        }

        public virtual List<Dictionary<string, object>> GetListMonth()
        {
            var retList = new List<Dictionary<string, object>>();
            for (int i = 0; i < 12; i++)
            {
                var dictionary = new Dictionary<string, object>();
                dictionary["name"] = i + 1;
                dictionary["value"] = i + 1;
                retList.Add(dictionary);
            }
            return retList;
        }
    }
}
