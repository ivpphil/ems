﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.merchandise.category;
using jp.co.ivp.ers.db;
using System.Data;
using jp.co.ivp.ers.mvc;


namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// カテゴリに関するViewService
    /// </summary>
    public class ErsViewCategoryService
        : ErsViewServiceBase
    {
        internal ErsViewCategoryService()
        {
        }

        /// <summary>
        /// Get list of category by category number
        /// </summary>
        /// <param name="categoryNumber">Category Number to be searched</param>
        /// <param name="checkedValues">Values that are selected (checked)</param>
        /// <param name="displaySiteName">Set as true if site names are needed to be displayed</param>
        /// <returns>List</returns>
        public virtual List<Dictionary<string, object>> GetCategory(int categoryNumber, int[] checkedValues, bool displaySiteName = false)
        {
            var parentCateStgy = ErsFactory.ersMerchandiseFactory.GetObtainParentCateStgy();
            var list = parentCateStgy.SelectSatisfying(categoryNumber,displaySiteName);
            int parentcategoryNumber;

            //convert dictionary to erslist.
            var retList = new List<Dictionary<string, object>>();
            foreach (var item in list)
            {
                //set ”isChecked” when selected
                item["isChecked"] = checkedValues != null && checkedValues.Contains(Convert.ToInt32(item["id"]));

                var ParentNames = string.Empty;
                if (item["parrent_array"] != null)
                {
                    //探索開始親カテゴリ番号
                    parentcategoryNumber = categoryNumber - 1;
                    foreach (int parentId in (IEnumerable)item["parrent_array"])
                    {
                        if (parentId == -1)
                        {
                            break;
                        }

                        var parentName = this.GetStringFromId(parentcategoryNumber, parentId);
                        if (string.IsNullOrEmpty(parentName))
                        {
                            break;
                        }

                        ParentNames += "-" + parentName;
                        parentcategoryNumber = parentcategoryNumber - 1;
                    }
                }
                if (!string.IsNullOrEmpty(ParentNames))
                {
                    item["parentNames"] = ParentNames.Substring(1);
                }

                retList.Add(item);
            }
            return retList;
        }

        /// <summary>
        /// Get list of category by category number
        /// </summary>
        /// <param name="categoryNumber">Category Number to be searched</param>
        /// <param name="checkedValues">Values that are selected (checked)</param>
        /// <returns>List</returns>
        public virtual List<Dictionary<string, object>> GetCategory(int categoryNumber, int[] checkedValues, int site_id)
        {
            return this.GetCategory(categoryNumber, checkedValues).Where(e => Convert.ToInt32(e["site_id"]) == site_id).ToList(); 
        }


        /// <summary>
        /// Get Category List
        /// </summary>
        /// <param name="activeOnly">Active (true), not Active (false)</param>
        /// <param name="filterBySiteId">If filtering by site_id is needed</param>
        /// <returns>List</returns>
        public virtual List<Dictionary<string, object>> GetCategoryList(bool activeOnly, bool filterBySiteId, params int?[] selectedValues)
        {
            var Setlist = new List<Dictionary<string, object>>();

            for (int i = ErsCategory.minCategoryNumber; i <= ErsCategory.maxCategoryNumber; i++)
            {
                var item = new Dictionary<string, object>();
                var dicSetup = getSetup_cate_t();

                //カテゴリ名
                item["cate_t_id"] = i;
                item["cate_t_name"] = dicSetup["cate" + i + ""];
                item["cate_t_active"] = dicSetup["cate" + i + "_active"];

                var selectedValue = selectedValues != null && selectedValues[i - 1].HasValue ? new[] { selectedValues[i - 1].Value } : null;

                //コンボボックス
                var categoryList  = new List<Dictionary<string,object>>();
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                if (!setup.Multiple_sites)
                {
                    categoryList = this.GetCategory(i, selectedValue);
                }
                else
                {
                    if (filterBySiteId)
                    {
                        categoryList = this.GetCategory(i, selectedValue, setup.site_id);
                    }
                    else
                    {
                        categoryList = this.GetCategory(i, selectedValue, setup.Multiple_sites);
                    }
                }

                var firstCategory = categoryList.FirstOrDefault();
                item["rootElement"] = !(firstCategory != null && firstCategory.ContainsKey("parentNames") && firstCategory["parentNames"].ToString().HasValue());

                if (activeOnly)
                {
                    item["list"] = this.GetOnlyActiveRecord(categoryList);
                }
                else
                {
                    item["list"] = categoryList;
                }

                Setlist.Add(item);

            }

            return Setlist;
        }

        /// <summary>
        /// Get the Category 1 in setup_T
        /// </summary>
        /// <returns>Data table</returns>
        public virtual Dictionary<string, object> getSetup_cate_t(int? site_id = null)
        {
            if (site_id == null)
            {
                var objSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSite();
                return objSetup.GetPropertiesAsDictionary();
            }
            else
            {
                var objSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSiteForAdmin(site_id.Value);
                return objSetup.GetPropertiesAsDictionary();
            }
        }
        
        //Category1-5 Name Get
        /// <summary>
        /// Get the Category name from setup_t
        /// </summary>
        /// <param name="catenum">Category Number</param>
        /// <returns>String</returns>
        public string getCateName(int? catenum, int? site_id = null)
        {
            var dicSetup = getSetup_cate_t(site_id);

            return Convert.ToString(dicSetup["cate" + catenum + ""]);
        }

        /// <summary>
        /// Get the category display name for view
        /// </summary>
        /// <param name="catenum"></param>
        /// <returns></returns>
        public string getCateNameView(int catenum)
        {
            return ErsResources.GetMessage("item_cate_name") + catenum;
        }

        //Category1-5 Active Info Get
        /// <summary>
        /// Get value of Active field (1 or 0) from setup_t
        /// </summary>
        /// <param name="catenum">Category number</param>
        /// <returns>Integer</returns>
        public EnumActive getCateActive(int catenum, int? site_id = null)
        {
            EnumActive cateAct = EnumActive.NonActive;
            var dicSetup = getSetup_cate_t(site_id);

            cateAct = (EnumActive)Convert.ToInt16(dicSetup["cate" + catenum + "_active"]);

            return cateAct;
        }

        /// <summary>
        /// Get Category Name by Category Number and by ID (cate{0}_t.name)
        /// </summary>
        /// <param name="categoryNumber">Category Number, this will be use for the table name (cate{0}_t.name)</param>
        /// <param name="id">Category ID</param>
        /// <returns>string</returns>
        public virtual string GetStringFromId(int categoryNumber, int? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList(categoryNumber);

            return this.GetStringFromId(list, "cate_name", "id", id);
        }

        public const string cacheKey = "cate_name-name_id";

        public List<Dictionary<string, object>> GetCachedList(int categoryNumber)
        {
            var cateCacheKey = cacheKey + categoryNumber;
            if (!this.CachedValue.ContainsKey(cateCacheKey))
            {
                var repository = ErsFactory.ersMerchandiseFactory.GetErsCategoryRepository(categoryNumber);
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsCategoryCriteria(categoryNumber);

                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var objList = repository.Find(criteria);

                this.CachedValue[cateCacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cateCacheKey];
        }

        public List<Dictionary<string, object>> GetCachedList(int categoryNumber, int site_id)
        {
            return this.GetCachedList(categoryNumber).Where(e => Convert.ToInt32(e["site_id"]) == site_id).ToList();
        }
    }
}