﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of country from country_t table.
    /// </summary>
    public class ErsViewCountryService
        : ErsViewServiceBase
    {
        public const string cacheKey = "country-name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersCommonFactory.GetErsCountryRepository();
                var criteria = ErsFactory.ersCommonFactory.GetErsCountryCriteria();
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var objList = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// Get the list of all countries.
        /// </summary>
        /// <returns>List (id and value of country)</returns>
        public virtual List<Dictionary<string, object>> SelectAsList()
        {
            var list = this.GetCachedList();

            list = this.GetOnlyActiveRecord(list);

            list = this.GetNameValueList(list, "country", "id");

            return list;
        }

        
        /// <summary>
        /// Get country name by ID
        /// </summary>
        /// <param name="id">country id</param>
        /// <returns>Returns string value of country</returns>
        public virtual string GetStringFromId(int? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "country", "id", id);
        }

        /// <summary>
        /// Check whether country exists by ID
        /// </summary>
        /// <param name="id">country id</param>
        /// <returns>Returns true if the id is existing, returns false if not existing.</returns>
        public virtual bool ExistValue(int? id)
        {
            if (id == null)
            {
                return false;
            }

            var list = this.GetCachedList();

            return this.ExistValue(list, "id", (int?)id);
        }
    }
}
