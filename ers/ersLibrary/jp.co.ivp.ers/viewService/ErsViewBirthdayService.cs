﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of Birthday.
    /// </summary>
    public class ErsViewBirthdayService
    {
        public const int minimumOfSelectyear = 1900;
        public const int maximumOfSelectyear = 3000;

        internal ErsViewBirthdayService()
        {
        }

        /// <summary>
        /// Get List of Years from Now up to 100 years
        /// </summary>
        /// <returns>Returns list of year values</returns>
        public virtual List<Dictionary<string, object>> GetListYear()
        {
            var max_user_age = ErsFactory.ersUtilityFactory.getSetup().max_user_age;
            return GetListYear(max_user_age, 0);
        }

        /// <summary>
        /// Get list of Months in number format (1-12)
        /// </summary>
        /// <returns>Returns list of month values</returns>
        public virtual List<Dictionary<string, object>> GetListMonth()
        {
            var retList = new List<Dictionary<string, object>>();
            for (int i = 0; i < 12; i++)
            {
                var dictionary = new Dictionary<string, object>();
                dictionary["name"] = i + 1;
                dictionary["value"] = i + 1;
                retList.Add(dictionary);
            }
            return retList;
        }

        /// <summary>
        /// Get list of days in number format (1-31)
        /// </summary>
        /// <returns>Returns list of day values</returns>
        public virtual List<Dictionary<string, object>> GetListDay()
        {
            var retList = new List<Dictionary<string, object>>();
            for (int i = 0; i < 31; i++)
            {
                var dictionary = new Dictionary<string, object>();
                dictionary["name"] = i + 1;
                dictionary["value"] = i + 1;
                retList.Add(dictionary);
            }
            return retList;
        }

        /// <summary>
        /// Gets the birthday considering the year, month and day supplied
        /// e.g. 2012/1/21
        /// </summary>
        /// <param name="birthday_y">Year of birthday</param>
        /// <param name="birthday_m">Month of birthday</param>
        /// <param name="birthday_d">Day of birthday</param>
        /// <returns>Returns Date of birth</returns>
        public virtual DateTime? GetBirthDay(int? birthday_y, int? birthday_m, int? birthday_d)
        {
            DateTime retVal;
            if (birthday_y != null && birthday_m != null && birthday_d != null
                && DateTime.TryParse(string.Format("{0}/{1}/{2}", birthday_y, birthday_m, birthday_d), out retVal))
            {
                return retVal;
            }
            return null;
        }
        /// <summary>
        /// 年プルダウンリスト作成
        /// （from toにオフセット年をパラメタ渡しする）
        /// </summary>
        /// <param name="from_offset"></param>
        /// <param name="to_offset"></param>
        /// <returns></returns>
        public virtual List<Dictionary<string, object>> GetListYear(int from_offset, int to_offset)
        {
            List<Dictionary<string, object>> retList = new List<Dictionary<string, object>>();

            var repository = ErsFactory.ersCommonFactory.GetErsEraRepository();
            var criteria = ErsFactory.ersCommonFactory.GetErsEraCriteria();
            criteria.SetOrderByYear(Criteria.OrderBy.ORDER_BY_DESC);
            var list = repository.Find(criteria);

            int toYear = Math.Max(DateTime.Now.AddYears(-from_offset).Year, minimumOfSelectyear);
            int fromYear = DateTime.Now.AddYears(-to_offset).Year;
            int countDt = 0;

            for (int i = fromYear; i >= toYear; i--)
            {
                var dr = list[countDt];
                if (list.Count - 1 > countDt && dr.year >= i)
                {
                    countDt++;
                }
                var dictionary = new Dictionary<string, object>();
                dictionary["era"] = dr.era;
                dictionary["eraYear"] = i - dr.year + 1;
                dictionary["value"] = i;
                retList.Add(dictionary);
            }

            return retList;
        }
    }
}
