﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewLpPageManageViewService
        : ErsViewServiceBase
    {
        public virtual IList<Dictionary<string, object>> GetLpGroupName()
        {
            var repository = ErsFactory.ersLpFactory.GetErsLpPageManageRepository();
            var criteria = ErsFactory.ersLpFactory.GetErsLpPageManageCriteria();
            criteria.SetOrderByMinId(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.AddGroupByLp_group_name();
            criteria.AddGroupByCcode();
            var objList = repository.Find(criteria, new[] { "lp_group_name", "ccode" });

            var retList = new List<Dictionary<string, object>>();
            foreach (var row in objList)
            {
                var dic = new Dictionary<string, object>();
                dic["value"] = row.lp_group_name;
                dic["ccode"] = row.ccode;
                retList.Add(dic);
            }

            return retList;
        }
    }
}
