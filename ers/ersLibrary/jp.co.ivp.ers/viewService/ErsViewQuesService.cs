﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of question from ques_t table. 
    /// </summary>
    public class ErsViewQuesService
        : ErsViewServiceBase
    {
        public const string cacheKey = "ques-name_id";
        
        internal ErsViewQuesService()
        {
        }

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersMemberFactory.GetErsQuesRepository();
                var criteria = ErsFactory.ersMemberFactory.GetErsQuesCriteria();
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                criteria.active = EnumActive.Active;
                var list = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(list);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// 秘密の質問プルダウン
        /// </summary>
        /// <returns>List (id and question name)</returns>
        public virtual List<Dictionary<string, object>> SelectAsList()
        {
            var list = this.GetCachedList();

            list = this.GetOnlyActiveRecord(list);

            return this.GetNameValueList(list, "ques_name", "id");
        }

        /// <summary>
        /// Get ques_name by ID using ErsDB_ques_t
        /// </summary>
        /// <param name="id">question id</param>
        /// <returns>Returns question name</returns>
        public virtual string GetStringFromId(int? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "ques_name", "id", id);
        }

        /// <summary>
        /// Get's boolean result of the specified id if it's existing or not using ErsDB_ques_t
        /// </summary>
        /// <param name="id">question id use for finding id</param>
        /// <returns>returns true if the id is existing, returns false if not existing</returns>
        public virtual bool ExistValue(int? id)
        {
            if (id == null)
            {
                return false;
            }

            var list = this.GetCachedList();

            return this.ExistValue(list, "id", id);
        }
    }
}
