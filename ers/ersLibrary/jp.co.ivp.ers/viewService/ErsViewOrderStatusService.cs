﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of order status from order_status_t table. 
    /// </summary>
    public class ErsViewOrderStatusService
        : ErsViewServiceBase
    {
        public const string cacheKey = "order_status-name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersOrderFactory.GetErsOrderStatusRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsOrderStatusCriteria();
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var list = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(list);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// お支払方法プルダウン
        /// </summary>
        /// <param name="active">active (true or false)</param>
        /// <returns>List (id and order_status)</returns>
        public virtual List<Dictionary<string, object>> SelectAsList(bool IsActiveOnly = true)
        {
            var list = this.GetCachedList();

            if (IsActiveOnly)
            {
                list = this.GetOnlyActiveRecord(list);
            }

            return this.GetNameValueList(list, "order_status", "id");
        }

        /// <summary>
        /// Gets order status according to the specified id using ErsDB_order_status_t.
        /// </summary>
        /// <param name="id">order status id use for finding order status</param>
        /// <returns>Returns value of order status.</returns>
        public virtual string GetStringFromId(EnumOrderStatusType? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "order_status", "id", (int?)id);
        }


        /// <summary>
        /// Get's boolean result of the specified id if it's existing or not using ErsDB_order_status_t.
        /// </summary>
        /// <param name="id">order status id use for finding id</param>
        /// <returns>Returns true if the id is existing, returns false if not existing.</returns>
        public virtual bool ExistValue(EnumOrderStatusType? id)
        {
            if (id == null)
            {
                return false;
            }

            var list = this.GetCachedList();

            return this.ExistValue(list, "id", (int?)id);
        }
    }
}
