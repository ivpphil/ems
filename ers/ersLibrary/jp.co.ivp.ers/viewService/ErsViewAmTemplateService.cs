﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewAmTemplateService
        : ErsViewServiceBase
    {
        public const string cacheKey = "template_name-name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ErsAtMailFactory.GetErsAmTemplateRepository();
                var criteria = ErsFactory.ErsAtMailFactory.GetErsAmTemplateCriteria();
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var objList = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// テンプレートプルダウン
        /// </summary>
        /// <returns>List (id and subject)</returns>
        public virtual List<Dictionary<string, object>> templateList()
        {
            var list = this.GetCachedList();

            list = this.GetOnlyActiveRecord(list);

            list = this.GetNameValueList(list, "template_name", "id");

            return list;
        }

    }
}
