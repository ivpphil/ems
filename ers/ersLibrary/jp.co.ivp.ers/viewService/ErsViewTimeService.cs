﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of Hour and Minutes.
    /// </summary>
    public class ErsViewTimeService
    {
        internal ErsViewTimeService()
        {
        }

        public virtual List<string> GetListHour()
        {
            var list = new List<string>();

            for (int i = 1; i <= 12; i++)
            {
                list.Add(i.ToString("00"));
            }

            return list;
        }

        public virtual List<string> GetListMinutes()
        {
            var list = new List<string>();

            for (int i = 0; i <= 45;)
            {
                list.Add(i.ToString("00"));
                i = i + 15;
            }

            return list;
        }

        //per minute interval used for logsheet
        public virtual List<string> GetListEveryMinute()
        {
            var list = new List<string>();

            for (int i = 0; i <= 59;)
            {
                list.Add(i.ToString("00"));
                i++;
            }

            return list;
        }
    }
}
