﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.administrator.function_group;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewFunctionGroupService
         : ErsViewServiceBase
    {
        public const string cacheKey = "func_name-name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersAdministratorFactory.GetErsFunctionGroupRepository();
                var criteria = ErsFactory.ersAdministratorFactory.GetErsFunctionGroupCriteria();
                criteria.func_id_not_equal = ErsFunctionGroup.GLOBAL_FUNC_ID;
                criteria.AddGroupBy("func_id");
                criteria.AddGroupBy("func_name");
                criteria.AddGroupBy("menu_id");
                criteria.AddGroupBy("menu_name");
                criteria.AddGroupBy("disp_order");
                criteria.AddGroupBy("active");
                criteria.AddOrderBy("disp_order", Criteria.OrderBy.ORDER_BY_ASC);
                var objList = repository.GetFunctionGroupList(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cacheKey];
        }

        public IList<Dictionary<string, object>> SelectAsList(IEnumerable<string> selectedValues, bool activeOnly = true)
        {
            var list = this.GetCachedList();

            if (activeOnly)
            {
                list = this.GetOnlyActiveRecord(list);
            }

            string old_menukey = null;
            foreach (var item in list)
            {
                var menukey = Convert.ToString(item["disp_order"]).Substring(0, 3);
                if (string.IsNullOrEmpty(old_menukey) || old_menukey != menukey)
                {
                    old_menukey = menukey;
                    item["menuName"] = item["menu_name"];
                }
            }

            return this.SetSelected(list, "func_id", selectedValues, "isChecked");
        }

        /// <summary>
        /// Gets job name according to the specified id using ErsDB_job_t.
        /// </summary>
        /// <param name="id">job id use for finding job name</param>
        /// <returns>Returns value of job name</returns>
        public virtual string GetStringFromId(int? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "func_name", "func_id", id);
        }

        /// <summary>
        /// Get's boolean result of the specified id if it's existing or not using ErsDB_job_t.
        /// </summary>
        /// <param name="id">job id use for finding id</param>
        /// <returns>Returns true if the id is existing, returns false if not existing.</returns>
        public virtual bool ExistValue(int? id)
        {
            if (id == null)
            {
                return false;
            }

            var list = this.GetCachedList();

            return this.ExistValue(list, "func_id", id);
        }
    }
}
