﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewCtsClientEscalationService
    {


        /// <summary>
        /// get ClientEscalationList
        /// </summary>
        /// <returns></returns>
        public virtual List<Dictionary<string, object>> GetCtsClientEscalationList()
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationRepository();
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationCriteria();
            criteria.Active = EnumActive.Active;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            var list = repository.Find(criteria);
            var retList = new List<Dictionary<string, object>>();
            foreach (var dr in list)
            {
                var dictionary = new Dictionary<string, object>();
                dictionary["value"] = dr.id;
                dictionary["name"] = dr.esc_name;
                retList.Add(dictionary);
            }

            return retList;
        }
    }
}
