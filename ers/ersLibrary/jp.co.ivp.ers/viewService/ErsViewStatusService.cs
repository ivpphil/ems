﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Class for the Status View Service
    /// </summary>
    public class ErsViewStatusService
    {
        
        /// <summary>
        /// Get the list of status
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetList()
        {
            var lstRet = new List<Dictionary<string, object>>();
            var list = Enum.GetValues(typeof(EnumStatus));


            foreach (var status in list)
            {
                Dictionary<string, object> dicView = new Dictionary<string, object>();

                var commonNameService = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService();

                dicView["value"] = (int)status;
                dicView["name"] = commonNameService.GetStringFromId(EnumCommonNameType.Status, EnumCommonNameColumnName.namename, (int)status);
                lstRet.Add(dicView);
            }

            return lstRet;
        }

        /// <summary>
        /// Gets the string name of Status by ID
        /// Gets from fieldnameresource.config
        /// </summary>
        /// <param name="id">value of enumstatus</param>
        /// <returns></returns>
        public virtual string GetStringFromId(EnumStatus? id)
        {
            var commonNameService = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService();

            return commonNameService.GetStringFromId(EnumCommonNameType.Status, EnumCommonNameColumnName.namename, (int)id);
        }



    }
}
