﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of payment from pay_t table.
    /// </summary>
    public class ErsViewPayService
        : ErsViewServiceBase
    {
        public const string cacheKey = "pay_name-name_id";

        /// <summary>
        /// お支払方法プルダウン
        /// </summary>
        /// <param name="active">active (true or false)</param>
        /// <returns>List (id, pay_name and ispaypal)</returns>
        public virtual List<Dictionary<string, object>> SelectAsList(bool regular_order, bool activeOnly = true, int? site_id = null)
        {
            var list = this.GetCachedList(site_id);

            if (activeOnly)
            {
                list = this.GetOnlyActiveRecord(list);
            }

            if (regular_order)
            {
                var newList = new List<Dictionary<string, object>>();
                foreach (var record in list)
                {
                    if ((EnumOnOff?)record["enable_regular_order"] == EnumOnOff.On)
                    {
                        newList.Add(record);
                    }
                }
                list = newList;
            }

            list = this.GetNameValueList(list, "pay_name", "id");

            return list;
        }

        private List<Dictionary<string, object>> GetCachedList(int? site_id = null)
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                if (site_id == null)
                {
                    site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
                }

                var repository = ErsFactory.ersOrderFactory.GetErsPayRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsPayCriteria();

                criteria.site_id = site_id;
                criteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var objList = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// Gets payment method name according to the specified id using ErsDB_parent
        /// </summary>
        /// <param name="id">question id</param>
        /// <returns>Returns question name</returns>
        public virtual string GetStringFromId(EnumPaymentType? id, int? site_id = null)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList(site_id);

            return this.GetStringFromId(list, "pay_name", "id", id);
        }

        /// <summary>
        /// Gets memo (notation delivery) according to the specified id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetPaymentMessageFromId(EnumPaymentType? id, int? site_id = null)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList(site_id);

            return this.GetStringFromId(list, "memo", "id", id);
        }

        /// <summary>
        /// Gets etc_name (commission name) according to the specified pay id
        /// </summary>
        /// <param name="pay">payment method id, use for finding commission name</param>
        /// <returns>returns commission name using GetStringFromId</returns>
        public string GetEtcNameFromId(EnumPaymentType? pay, int? site_id = null)
        {
            if (pay == null)
            {
                return null;
            }

            var list = this.GetCachedList(site_id);

            return this.GetStringFromId(list, "etc_name", "id", (int?)pay);
        }

        /// <summary>
        /// Get's boolean result of the specified id if it's existing or not using ErsDB_ques_t
        /// </summary>
        /// <param name="id">question id use for finding id</param>
        /// <returns>returns true if the id is existing, returns false if not existing</returns>
        public virtual bool ExistValue(EnumPaymentType? id, int? site_id = null)
        {
            if (id == null)
            {
                return false;
            }

            var list = this.GetCachedList(site_id);

            return this.ExistValue(list, "id", (int?)id);
        }
    }
}
