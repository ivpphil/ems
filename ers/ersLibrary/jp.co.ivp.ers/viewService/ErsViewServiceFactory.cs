﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.viewService;
using ers.jp.co.ivp.ers.viewService;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Factory Class for ViewService
    /// </summary>
    public class ErsViewServiceFactory
    {
        /// <summary>
        /// Obtain instance of ErsViewCategoryService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewCategoryService.</returns>
        public virtual ErsViewCategoryService GetErsViewCategoryService()
        {
            return new ErsViewCategoryService();
        }

        public virtual ErsViewCommonNameCodeService GetErsViewCommonNameCodeService()
        {
            return new ErsViewCommonNameCodeService();
        }

        public virtual ErsViewCtsEnquiryCategoryService GetErsViewCtsEnquiryCategoryService()
        {
            return new ErsViewCtsEnquiryCategoryService();
        }
        /// <summary>
        /// Obtain instance of ErsViewCalendarService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewCalendarService.</returns>
        public virtual ErsViewCalendarService GetErsViewCalendarService()
        {
            return new ErsViewCalendarService();
        }

        /// <summary>
        /// Obtain instance of ErsViewCardService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewCardService</returns>
        public virtual ErsViewCardService GetErsViewCardService()
        {
            return new ErsViewCardService();
        }

        /// <summary>
        /// Obtain instance of ErsViewBirthdayService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewBirthdayService.</returns>
        public virtual ErsViewBirthdayService GetErsViewBirthdayService()
        {
            return new ErsViewBirthdayService();
        }

        /// <summary>
        /// Obtain instance of ErsViewPrefService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewPrefService.</returns>
        public virtual ErsViewPrefService GetErsViewPrefService()
        {
            return new ErsViewPrefService();
        }

        /// <summary>
        /// Obtain instance of ErsViewJobService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewJobService.</returns>
        public virtual ErsViewJobService GetErsViewJobService()
        {
            return new ErsViewJobService();
        }

        /// <summary>
        /// Obtain instance of ErsViewPayService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewPayService.</returns>
        public virtual ErsViewPayService GetErsViewPayService()
        {
            return new ErsViewPayService();
        }

        /// <summary>
        /// Obtain instance of ErsViewSendtimeService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewSendtimeService.</returns>
        public virtual ErsViewSendtimeService GetErsViewSendtimeService()
        {
            return new ErsViewSendtimeService();
        }

        /// <summary>
        /// Obtain instance of ErsViewQuesService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewQuesService.</returns>
        public virtual ErsViewQuesService GetErsViewQuesService()
        {
            return new ErsViewQuesService();
        }

        /// <summary>
        /// Obtain instance of ErsViewOrderPaymentStatusService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewOrderPaymentStatusService.</returns>
        public virtual ErsViewOrderPaymentStatusService GetErsViewOrderPaymentStatusService()
        {
            return new ErsViewOrderPaymentStatusService();
        }

        /// <summary>
        /// Obtain instance of ErsViewOrderStatusService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewOrderStatusService.</returns>
        public virtual ErsViewOrderStatusService GetErsViewOrderStatusService()
        {
            return new ErsViewOrderStatusService();
        }

        /// <summary>
        /// Obtain instance of ErsViewZipService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewZipService.</returns>
        public virtual ErsViewZipService GetErsViewZipService()
        {
            return new ErsViewZipService();
        }

        /// <summary>
        /// Obtain instance of ErsViewSalePtnService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewSalePtnService.</returns>
        public virtual ErsViewSalePtnService GetErsViewSalePtnService()
        {
            return new ErsViewSalePtnService();
        }

        public virtual ErsViewCtsAgentTypeService GetErsViewCtsAgentTypeService()
        {
            return new ErsViewCtsAgentTypeService();
        }

        /// <summary>
        /// Obtain instance of ErsViewCountryService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewCountryService.</returns>
        public virtual ErsViewCountryService GetErsViewCountryService()
        {
            return new ErsViewCountryService();
        }

        /// <summary>
        /// Obtain instance of ErsViewRecommendService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewRecommendService.</returns>
        public virtual ErsViewRecommendService GetErsViewRecommendService()
        {
            return new ErsViewRecommendService();
        }

        /// <summary>
        /// Obtain instance of ErsViewPagerService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewPagerService.</returns>
        public virtual ErsViewPagerService GetErsViewPagerService()
        {
            return new ErsViewPagerService();
        }
        //View Employee team 
        //
        public virtual ErsViewEmpTeamService GetErsViewEmpTeamService()
        {
            return new ErsViewEmpTeamService();
        }

        //View 

        public virtual ErsViewEmpStatusService GetErsEmpStatusService()
        {
            return new ErsViewEmpStatusService();
        }

        public virtual ErsViewEmpReportStatusService GetErsEmpReportStatusService()
        {
            return new ErsViewEmpReportStatusService();
        }

        public virtual ErsViewEmpReportProgService GetErsViewEmpReportProgService()
        {
            return new ErsViewEmpReportProgService();
        }

        public virtual ErsViewEmpPcodeService GetErsViewEmpPcodeService()
        {
            return new ErsViewEmpPcodeService();
        }


        public virtual ErsViewEmpPositionService GetErsViewEmpPositionService()
        {
            return new ErsViewEmpPositionService();
        }

        public virtual ErsViewTargetService GetErsViewTargetService()
        {
            return new ErsViewTargetService();
        }

        public virtual ErsViewOrderPatternService GetErsViewOrderPatternService()
        {
            return new ErsViewOrderPatternService();
        }

        public virtual ErsViewReferenceDateService GetErsViewReferenceDateService()
        {
            return new ErsViewReferenceDateService();
        }

        public virtual ErsViewStatusService GetErsViewStatusService()
        {
            return new ErsViewStatusService();
        }


        public virtual ErsViewElapsedNumService GetErsViewElapsedNumService()
        {
            return new ErsViewElapsedNumService();
        }


        public virtual ErsViewDeliveryTimeService GetErsViewDeliveryTimeService()
        {
            return new ErsViewDeliveryTimeService();
        }

        public virtual ErsViewAmTemplateService GetErsViewAmTemplateService()
        {
            return new ErsViewAmTemplateService();
        }

        public virtual ErsViewCtsAgentService GetErsViewCtsAgentService()
        {
            return new ErsViewCtsAgentService();
        }

        public virtual ErsViewCtsClientEscalationService GetErsViewCtsClientEscalationService()
        {
            return new ErsViewCtsClientEscalationService();
        }

        public virtual ErsViewCmsTemplateService GetErsViewCmsTemplateService()
        {
            return new ErsViewCmsTemplateService();
        }

        public virtual ErsViewRegularOrderService GetErsRegularOrderViewService()
        {
            return new ErsViewRegularOrderService();
        }

        public virtual ErsViewCtsCalenderService GetErsViewCtsCalenderService()
        {
            return new ErsViewCtsCalenderService();
        }

        public virtual ErsViewFunctionGroupService GetErsViewFunctionGroupService()
        {
            return new ErsViewFunctionGroupService();
        }

        public virtual ErsViewRoleGroupService GetErsViewRoleGroupService()
        {
            return new ErsViewRoleGroupService();
        }

        public virtual ErsViewLpPageManageViewService GetErsLpPageManageViewService()
        {
            return new ErsViewLpPageManageViewService();
        }

        public virtual ErsViewMemberRankSetupService GetErsViewMemberRankSetupService()
        {
            return new ErsViewMemberRankSetupService();
        }

        /// <summary>
        /// ErsViewAmSetupService取得 [Get ErsViewAmSetupService]
        /// </summary>
        /// <returns>ErsViewAmSetupService</returns>
        public virtual ErsViewAmSetupService GetErsViewAmSetupService()
        {
            return new ErsViewAmSetupService();
        }

        public virtual ErsViewTimeService GetErsViewTimeService()
        {
            return new ErsViewTimeService();
        }
    }
}
