﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewServiceBase
    {
        private const string cacheKey = "jp.co.ivp.ers.viewService.ErsViewServiceBase.pooledList";

        private string cachedValueKey;

        protected ErsViewServiceBase()
        {
            cachedValueKey =  cacheKey + this.GetType().FullName;
        }

        protected virtual Dictionary<string, List<Dictionary<string, object>>> CachedValue
        {
            get
            {
                var result = (Dictionary<string, List<Dictionary<string, object>>>)ErsCommonContext.GetPooledObject(cachedValueKey);
                if (result == null)
                {
                    result = new Dictionary<string, List<Dictionary<string, object>>>();
                    ErsCommonContext.SetPooledObject(cachedValueKey, result);
                }
                return result;
            }
        }

        /// <summary>
        /// RepositoryEntityのListを、nameとvalueのDictionaryにコンバートする。（表示用）
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected virtual List<Dictionary<string, object>> GetNameValueList(List<Dictionary<string, object>> list, string nameColumn, string valueColumn)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            
            foreach (var item in list)
            {
                var resultValue = Convert.ToString(item[nameColumn]);
                if (setup.site_type == EnumSiteType.MOBILE)
                {
                    resultValue = VBStrings.HalfKana(resultValue);
                }
                item["name"] = resultValue;
                item["value"] = item[valueColumn];
            }

            return list;
        }

        protected virtual List<Dictionary<string, object>> GetOnlyActiveRecord(List<Dictionary<string, object>> list)
        {
            var retList = new List<Dictionary<string, object>>();

            foreach (var row in list)
            {
                if (row.ContainsKey("active") && (EnumActive)Convert.ToInt32(row["active"]) == EnumActive.NonActive)
                {
                    continue;
                }
                retList.Add(row);
            }

            return retList;
        }

        protected virtual string GetStringFromId(List<Dictionary<string, object>> list, string nameColumn, string valueColumn, object value)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (value == null)
            {
                return null;
            }

            var result = list.SingleOrDefault((dictionary) => 
                {
                    var tmpValue = dictionary[valueColumn];
                    if (value is Enum || dictionary[valueColumn] is Enum)
                    {
                        //列挙があの比較を行う
                        return Convert.ToInt32(value).Equals(Convert.ToInt32(tmpValue));
                    }
                    return value.Equals(tmpValue);
                });

            if (result == null)
            {
                return null;
            }
            var resultValue = Convert.ToString(result[nameColumn]);
            if (setup.site_type == EnumSiteType.MOBILE)
            {
                resultValue = VBStrings.HalfKana(resultValue);
            }

            return resultValue;
        }

        protected virtual bool ExistValue(List<Dictionary<string, object>> list, string valueColumn, object id)
        {
            var result = list.SingleOrDefault((dictionary) => dictionary[valueColumn].Equals(id));

            return (result != null);
        }

        protected virtual List<Dictionary<string, object>> SetSelected(List<Dictionary<string, object>> list, string valueColumn, IEnumerable<int> selectedValues, string selectedKey)
        {
            foreach (var item in list)
            {
                if (selectedValues != null)
                {
                    item[selectedKey] = (selectedValues.Contains(Convert.ToInt32(item[valueColumn])));
                }
            }
            return list;
        }

        protected virtual List<Dictionary<string, object>> SetSelected(List<Dictionary<string, object>> list, string valueColumn, IEnumerable<string> selectedValues, string selectedKey)
        {
            foreach (var item in list)
            {
                if (selectedValues != null)
                {
                    item[selectedKey] = (selectedValues.Contains(item[valueColumn]));
                }
            }
            return list;
        }
    }
}
