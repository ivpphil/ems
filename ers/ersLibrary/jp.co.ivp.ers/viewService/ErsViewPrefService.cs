﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.basket.specification;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of prefectures from pref_t table.
    /// </summary>
    public class ErsViewPrefService
        : ErsViewServiceBase
    {
        public const string cacheKey = "pref_name-name_id";

        private List<Dictionary<string, object>> GetCachedList(int? site_id = null)
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                if (site_id == null)
                {
                    site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
                }

                var repository = ErsFactory.ersCommonFactory.GetErsPrefRepository();
                var criteria = ErsFactory.ersCommonFactory.GetErsPrefCriteria();

                criteria.site_id = site_id;
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var list = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(list);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// 都道府県プルダウン
        /// </summary>
        /// <returns>List (id and pref_name)</returns>
        public virtual List<Dictionary<string, object>> SelectAsList(bool activeInFrontOnly = true, int? site_id = null)
        {
            var list = this.GetCachedList(site_id);

            list = this.GetOnlyActiveRecord(list);

            if (activeInFrontOnly)
            {
                list = list.FindAll(dic => (EnumActive)dic["active_in_front"] == EnumActive.Active);
            }

            return this.GetNameValueList(list, "pref_name", "id");
        }

        /// <summary>
        /// 都道府県表示用
        /// </summary>
        /// <param name="pref">prefecture id</param>
        /// <returns>Returns prefecture name</returns>
        public virtual string GetStringFromId(int? id, int? site_id = null)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList(site_id);

            return this.GetStringFromId(list, "pref_name", "id", id);
        }

        /// <summary>
        /// ID取得用
        /// </summary>
        /// <param name="pref">prefecture name</param>
        /// <returns>Returns prefecture id</returns>
        public virtual int? GetIdFromString(string name, int? site_id = null)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            var list = this.GetCachedList(site_id);

            return Convert.ToInt32(this.GetStringFromId(list, "id", "pref_name", name));
        }

        /// <summary>
        /// Idから送料を取得する
        /// </summary>
        /// <param name="id">都道府県ID</param>
        /// <returns>送料</returns>
        public virtual int GetCarriageFromId(int? id, EnumCarriageFreeStatus? carriageFreeStatus, bool isMailDelivery)
        {
            if (carriageFreeStatus == EnumCarriageFreeStatus.CARRIAGE_FREE)
            {
                return 0;
            }

            if (isMailDelivery)
            {
                return ErsFactory.ersUtilityFactory.GetErsSetupOfSite().mail_deliv_carriage;
            }
            else
            {
                if (id == null)
                {
                return 0;
            }

            var list = this.GetCachedList();

            return Convert.ToInt32(this.GetStringFromId(list, "carriage", "id", id));
        }
    }
    }
}
