﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.cts_operators;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewCtsAgentService
         : ErsViewServiceBase
    {
        public const string cacheKey = "ag_name-name_id";
        private List<Dictionary<string, object>> GetCachedList(EnumEnqSituation? enq_situation)
        {
            if (!this.CachedValue.ContainsKey(cacheKey + enq_situation))
            {
                var repository = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorRepository();
                var criteria = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorCriteria();
                criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

                if (enq_situation == EnumEnqSituation.AGEscalation)
                    criteria.authority = CtsAuthorityType.OPERATOR;

                else if (enq_situation == EnumEnqSituation.SVEscalation)
                    criteria.authority = CtsAuthorityType.SUPERVISOR;

                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var objList = repository.Find(criteria);

                this.CachedValue[cacheKey + enq_situation] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cacheKey + enq_situation];
        }

        /// <summary>
        /// get CtsAgentList
        /// </summary>
        /// <param name="enq_situation"></param>
        /// <returns></returns>
        public virtual List<Dictionary<string, object>> SelectAsList(EnumEnqSituation? enq_situation)
        {
            var list = this.GetCachedList(enq_situation);

            list = this.GetOnlyActiveRecord(list);

            list = this.GetNameValueList(list, "ag_name", "id");

            return list;
        }

        /// <summary>
        /// Gets job name according to the specified id using ErsDB_job_t.
        /// </summary>
        /// <param name="id">job id use for finding job name</param>
        /// <returns>Returns value of job name</returns>
        public virtual string GetStringFromId(EnumEnqSituation? enq_situation, int? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList(enq_situation);

            return this.GetStringFromId(list, "ag_name", "id", id);
        }

        /// <summary>
        /// Get's boolean result of the specified id if it's existing or not using ErsDB_job_t.
        /// </summary>
        /// <param name="id">job id use for finding id</param>
        /// <returns>Returns true if the id is existing, returns false if not existing.</returns>
        public virtual bool ExistValue(EnumEnqSituation? enq_situation, int? id)
        {
            if (id == null)
            {
                return false;
            }

            var list = this.GetCachedList(enq_situation);

            return this.ExistValue(list, "id", (int?)id);
        }
    }
}
