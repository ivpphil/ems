﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewTargetService
          : ErsViewServiceBase
    {
        public const string cacheKey = "target_name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersTargetFactory.GetErsTargetRepository();
                var criteria = ErsFactory.ersTargetFactory.GetErsTargetCriteria();
                var list = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(list);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// プルダウン
        /// </summary>
        /// <returns>List (id and question name)</returns>
        public virtual List<Dictionary<string, object>> SelectAsList()
        {
            var list = this.GetCachedList();

            //list = this.GetOnlyActiveRecord(list);

            return this.GetNameValueList(list, "target_name", "id");
        }

        /// <summary>
        /// Get ques_name by ID using ErsDB_ques_t
        /// </summary>
        /// <param name="id">question id</param>
        /// <returns>Returns question name</returns>
        public virtual string GetStringFromId(int? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "target_name", "id", id);
        }
    }
}
