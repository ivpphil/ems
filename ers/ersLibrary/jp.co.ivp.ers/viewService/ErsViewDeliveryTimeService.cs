﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.viewService
{
    
    /// <summary>
    /// Class for delivery time view service
    /// </summary>
    public class ErsViewDeliveryTimeService
    {
        /// <summary>
        /// Gets the list of hours 0 - 2
        /// </summary>
        /// <returns></returns>
        public virtual List<Dictionary<string, object>> GetListHour()
        {
            List<Dictionary<string, object>> retList = new List<Dictionary<string, object>>();

            for (int i = 0; i < 24; i++)
            {
                var dictionary = new Dictionary<string, object>();

                dictionary["name"] = string.Format("{0:D2}", i);
                dictionary["value"] = i;

                retList.Add(dictionary);
            }

            return retList;
        }

        /// <summary>
        /// Gets the list of mins  - 45
        /// </summary>
        /// <returns></returns>
        public virtual List<Dictionary<string, object>> GetListMin()
        {
            List<Dictionary<string, object>> retList = new List<Dictionary<string, object>>();

            for (int i = 0; i < 46; i = i + 15)
            {
                var dictionary = new Dictionary<string, object>();

                dictionary["name"] = string.Format("{0:D2}", i);
                dictionary["value"] = i;

                retList.Add(dictionary);
            }

            return retList;
        }
    }
}
