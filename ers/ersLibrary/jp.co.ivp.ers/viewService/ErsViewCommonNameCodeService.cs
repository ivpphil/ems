﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.Web.Caching;
using System.Web;
using jp.co.ivp.ers.common;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.template;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewCommonNameCodeService
        : ErsViewServiceBase
    {
        public const string cacheKey = "_name_name-name_id";

        private string culture
        {
            get
            {
                var ers = (ErsState)ErsCommonContext.GetPooledObject("_sessionState");
                string culture = string.Empty;
                if (ers != null)
                {
                    culture = ers.Get("culture");
                }

                if (string.IsNullOrEmpty(culture))
                {
                    culture = MessageResourceDictionary.defaultCulture;
                }
                return culture;
            }
        }

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(culture + cacheKey))
                {
                var repository = ErsFactory.ersCommonFactory.GetErsCommonNameCodeRepository();
                var criteria = ErsFactory.ersCommonFactory.GetErsCommonNameCodeCriteria();
                criteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
                criteria.SetOrderByCode(Criteria.OrderBy.ORDER_BY_ASC);
                var objList = repository.Find(criteria);
                //foreach (ErsCommonNameCode code in objList)
                //{
                //    if (MessageResourceDictionary.dicMessageResource[culture].ContainsKey(code.namename))
                //    {
                //        code.namename = ErsResources.GetMessage(code.namename);
                //    }
                //}
                this.CachedValue[culture+cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[culture+cacheKey];
        }

        /// <summary>
        /// gets list from common_namecode_t
        /// </summary>
        public List<Dictionary<string, object>> GetList(EnumCommonNameType type_code, EnumCommonNameColumnName nameColumn, int? opt_flg1 = null, int? opt_flg2 = null, bool IsActiveOnly = true)
        {
            var list = this.GetCachedList();

            if (IsActiveOnly)
            {
                list = this.GetOnlyActiveRecord(list);
            }

            list = this.GetCollectableRecord(list, type_code, opt_flg1, opt_flg2);

            list = this.GetNameValueList(list, nameColumn.ToString(), EnumCommonNameColumnName.code.ToString());

            return list;
        }

        private List<Dictionary<string, object>> GetCollectableRecord(List<Dictionary<string, object>> list, EnumCommonNameType type_code, int? opt_flg1, int? opt_flg2)
        {
            var retList = new List<Dictionary<string, object>>();

            foreach (var record in list)
            {
                if (this.CheckCollectableRecord(record, type_code, opt_flg1, opt_flg2))
                {
                    retList.Add(record);
                }
            }
            return retList;
        }

        /// <summary>
        ///  チェックする。
        /// </summary>
        /// <param name="record"></param>
        /// <param name="type_code"></param>
        /// <param name="opt_flg1"></param>
        /// <param name="opt_flg2"></param>
        /// <returns></returns>
        protected virtual bool CheckCollectableRecord(Dictionary<string, object> record, EnumCommonNameType type_code, int? opt_flg1 = null, int? opt_flg2 = null)
        {
            return type_code.ToString() == Convert.ToString(record["type_code"])
                && (opt_flg1 == null || opt_flg1.Value == Convert.ToInt32(record["opt_flg1"]))
                && (opt_flg2 == null || opt_flg2.Value == Convert.ToInt32(record["opt_flg2"]));
        }

        /// <summary>
        /// gets list from common_namecode_t and checks the values in selectedValues
        /// </summary>
        /// <param name="selectedValues"></param>
        /// <param name="type_code">type_code value which is used to narrow down records of common_namecode_t</param>
        /// <param name="nameColumn"></param>
        /// <returns></returns>
        public IList<Dictionary<string, object>> GetList(IEnumerable<int> selectedValues, EnumCommonNameType type_code, EnumCommonNameColumnName nameColumn, int? opt_flg1 = null, int? opt_flg2 = null)
        {
            var returnList = new List<Dictionary<string, object>>();

            var recordList = GetList(type_code, nameColumn, opt_flg1, opt_flg2);
           
            foreach (var record in recordList)
            {
     
                if (selectedValues != null)
                {
                    if (selectedValues.Contains((int)record["value"]))
                     
                        record["isSelected"] = true;
                    else
                        record["isSelected"] = false;
                }
                returnList.Add(record);

            }

            return returnList;
        }

        /// <summary>
        /// IDから文字列取得
        /// </summary>
        /// <param name="type_code"></param>
        /// <param name="nameColumn"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual string GetStringFromId(EnumCommonNameType type_code, EnumCommonNameColumnName nameColumn, int? value)
        {
            if (value == null)
            {
                return null;
            }

            var list = GetList(type_code, nameColumn);

            var resultValue = this.GetStringFromId(list, "name", "value", value);

            if (!string.IsNullOrEmpty(resultValue))
            {
                return resultValue;
            }

            return null;
        }

        /// <summary>
        /// ID存在チェック
        /// </summary>
        /// <param name="type_code"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool ExistValue(EnumCommonNameType type_code, int? value)
        {
            if (value == null)
            {
                return false;
            }

            var list = this.GetList(type_code, EnumCommonNameColumnName.namename);

            return this.ExistValue(list, "value", value);
        }

        /// <summary>
        /// 文字列からID取得
        /// </summary>
        /// <param name="type_code"></param>
        /// <param name="valueColumn"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual int? GetIdFromString(EnumCommonNameType type_code, EnumCommonNameColumnName valueColumn, string name)
        {
            if (name == null)
            {
                return null;
            }

            var list = GetList(type_code, valueColumn);

            var resultValue = this.GetStringFromId(list, "value", "name", name);

            if (!string.IsNullOrEmpty(resultValue))
            {
                return Convert.ToInt32(resultValue);
            }

            return null;
        }

        public virtual List<int?> GetIdFromStringLike(EnumCommonNameType type_code, string name)
        {
            if (!name.HasValue())
            {
                return null;
            }

            var repository = ErsFactory.ersCommonFactory.GetErsCommonNameCodeRepository();
            var criteria = ErsFactory.ersCommonFactory.GetErsCommonNameCodeCriteria();
            criteria.type_code = type_code;
            criteria.namename_like = name;

            var name_list = repository.Find(criteria);

            var listName = new List<int?>();
            if(name_list != null)
            {
                foreach(var name_code in name_list)
                {
                    listName.Add(name_code.code);
                }
            }

            return listName;
        }

        /// <summary>
        /// 文字列存在チェック
        /// </summary>
        /// <param name="type_code"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool ExistName(EnumCommonNameType type_code, string name)
        {
            if (name == null)
            {
                return false;
            }

            var list = this.GetList(type_code, EnumCommonNameColumnName.namename);

            return this.ExistValue(list, "name", name);
        }
    }
}
