﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of sale method or pattern from sale_ptn_t table. 
    /// </summary>
    public class ErsViewSalePtnService
        : ErsViewServiceBase
    {
        public const string cacheKey = "sale_ptn-name_id";

        internal ErsViewSalePtnService()
        {
        }

        /// <summary>
        /// Gets sale_ptn list using ErsDB_sale_ptn_t
        /// </summary>
        /// <returns>List (id and sale_ptn)</returns>
        public virtual List<Dictionary<string, object>> SelectAsList(bool activeOnly = true)
        {
            var list = this.GetCachedList();

            if (activeOnly)
            {
                list = this.GetOnlyActiveRecord(list);
            }

            list = this.GetNameValueList(list, "sale_ptn", "id");

            return list;
        }

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersOrderFactory.GetErsSalePtnRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsSalePtnCriteria();
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var objList = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// Gets sale_ptn by id using ErsDB_sale_ptn_t
        /// </summary>
        /// <param name="id">sale pattern id</param>
        /// <returns>Returns sale pattern value</returns>
        public virtual string GetStringFromId(EnumSalePatternType? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "sale_ptn", "id", (int?)id);
        }
    }
}
