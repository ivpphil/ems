﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of pager. 
    /// </summary>
    public class ErsViewPagerService
    {
        /// <summary>
        /// Gets pager model
        /// </summary>
        /// <param name="controller">controller for ErsControllerBase</param>
        /// <param name="modelNameInView">model name</param>
        /// <param name="pageCnt">page count</param>
        /// <param name="maxItemCount">maximum item count</param>
        /// <returns>returns model with pager for views</returns>
        public ErsPagerModel GetPagerModel(ErsControllerBase controller, string modelNameInView, int pageCnt, long maxItemCount)
        {
            var pagerModel = new ErsPagerModel();
            pagerModel.Init(controller, modelNameInView, pageCnt, maxItemCount);
            return pagerModel;
        }

        /// <summary>
        /// Gets pager model
        /// </summary>
        /// <param name="pageCnt">page count</param>
        /// <param name="maxItemCount">maximum item count</param>
        /// <returns>returns model with pager for views</returns>
        public ErsPagerModel GetPagerModel(int pageCnt, long maxItemCount)
        {
            var pagerModel = new ErsPagerModel();
            pagerModel.Init(pageCnt, maxItemCount);
            return pagerModel;
        }
    }
}
