﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// This Class is for Order Pattern ViewService
    /// </summary>
    public class ErsViewOrderPatternService
    {
        /// <summary>
        /// Gets the list of the Order Pattern (name and value)
        /// </summary>
        /// <returns></returns>
        public virtual List<Dictionary<string, object>> GetList()
        {
            List<Dictionary<string, object>> retList = new List<Dictionary<string, object>>();

            var listoforderpattern = Enum.GetValues(typeof(EnumOrderPattern));

            var commonNameService = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService();

            foreach (var orderPattern in listoforderpattern)
            {
                var dictionary = new Dictionary<string, object>();


                dictionary["name"] = commonNameService.GetStringFromId(EnumCommonNameType.OrderPattern, EnumCommonNameColumnName.namename, (int)orderPattern);
                dictionary["value"] = (int)orderPattern;

                retList.Add(dictionary);
            }

            return retList;
        }

        
        /// <summary>
        /// Gets the string value of the Order Pattern
        /// Get from FieldNameResource.config
        /// </summary>
        /// <param name="id">EnumOrderPattern that needs the string value</param>
        /// <returns></returns>
        public virtual string GetStringFromId(EnumOrderPattern id)
        {
            var commonNameService = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService();
            return commonNameService.GetStringFromId(EnumCommonNameType.OrderPattern, EnumCommonNameColumnName.namename, (int)id);
        }
    }
}
