﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of order payment status from order_payment_status_t table. 
    /// </summary>
    public class ErsViewOrderPaymentStatusService
        : ErsViewServiceBase
    {
        public const string cacheKey = "order_payment_status-name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersOrderFactory.GetErsOrderPaymentStatusRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsOrderPaymentStatusCriteria();
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var list = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(list);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// お支払方法プルダウン
        /// </summary>
        /// <returns>List (id and order_payment_status)</returns>
        public virtual List<Dictionary<string, object>> SelectAsList(bool IsActiveOnly = true)
        {
            var list = this.GetCachedList();

            if (IsActiveOnly)
            {
                list = this.GetOnlyActiveRecord(list);
            }

            return this.GetNameValueList(list, "order_payment_status", "id");
        }

        /// <summary>
        /// Gets order payment status according to the specified id using ErsDB_order_payment_status_t.
        /// </summary>
        /// <param name="id">id use for finding order payment status</param>
        /// <returns>Returns value of order payment status.</returns>
        public virtual string GetStringFromId(EnumOrderPaymentStatusType? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "order_payment_status", "id", (int?)id);
        }

        /// <summary>
        /// Get's boolean result of the specified id if it's existing or not using ErsDB_order_payment_status_t.
        /// </summary>
        /// <param name="id">id use for finding order payment status id</param>
        /// <returns>Returns true if the id is existing, returns false if not existing.</returns>
        public virtual bool ExistValue(EnumOrderPaymentStatusType? id)
        {
            if (id == null)
            {
                return false;
            }

            var list = this.GetCachedList();

            return this.ExistValue(list, "id", (int?)id);
        }
    }
}
