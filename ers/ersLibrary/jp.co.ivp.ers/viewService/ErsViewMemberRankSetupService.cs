﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of rank from member_rank_setup_t table. 
    /// </summary>
    public class ErsViewMemberRankSetupService
        : ErsViewServiceBase
    {
        public const string cacheKey = "member_rank_name-name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersMemberFactory.GetErsMemberRankSetupRepository();
                var criteria = ErsFactory.ersMemberFactory.GetErsMemberRankSetupCriteria();

                if (ErsFactory.ersUtilityFactory.getSetup().member_rank_centralization)
                {
                    criteria.site_id_for_admin = (int)EnumSiteId.COMMON_SITE_ID;
                }
                else
                {
                    criteria.site_id_not_equal = (int)EnumSiteId.COMMON_SITE_ID;
                }

                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var objList = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// Obtain a list of job names using ErsDB_member_rank_setup_t.
        /// </summary>
        /// <returns>Returns list of rank names</returns>
        public virtual List<Dictionary<string, object>> SelectAsList()
        {
            var list = this.GetCachedList();

            list = this.GetOnlyActiveRecord(list);

            list = this.GetNameValueList(list, "rank_name", "id");

            return list;
        }

        /// <summary>
        /// Gets rank name according to the specified id using ErsDB_member_rank_setup_t.
        /// </summary>
        /// <param name="id">rank id use for finding rank name</param>
        /// <returns>Returns value of rank name</returns>
        public virtual string GetStringFromId(int? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "rank_name", "id", id);
        }

        /// <summary>
        /// Get's boolean result of the specified id if it's existing or not using ErsDB_member_rank_setup_t.
        /// </summary>
        /// <param name="id">rank id use for finding id</param>
        /// <returns>Returns true if the id is existing, returns false if not existing.</returns>
        public virtual bool ExistValue(int? id)
        {
            if (id == null)
            {
                return false;
            }

            var list = this.GetCachedList();

            return this.ExistValue(list, "id", (int?)id);
        }
    }
}
