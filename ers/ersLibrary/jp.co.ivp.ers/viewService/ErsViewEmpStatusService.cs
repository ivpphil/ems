﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.viewService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.viewService
{
   public class ErsViewEmpStatusService : ErsViewServiceBase
    {
        public const string cacheKey = "order_status-name_id";

        private List<Dictionary<string,object>> GetCachedList()
        {
            if(!this.CachedValue.ContainsKey(cacheKey))
            {
                var repo = ErsFactory.ersEmployeeFactory.GetErsEmpStatusRepository();
                var cri = ErsFactory.ersEmployeeFactory.GetErsEmpStatusCriteria();

                var list = repo.Find(cri);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(list);

            }
            return this.CachedValue[cacheKey];

        }
        public virtual List<Dictionary<string, object>> SelectAsList(bool IsActiveOnly = true)
        {
            var list = this.GetCachedList();


            return this.GetNameValueList(list, "emp_status", "id");
        }

        /// <summary>
        /// Gets order status according to the specified id using ErsDB_order_status_t.
        /// </summary>
        /// <param name="id">order status id use for finding order status</param>
        /// <returns>Returns value of order status.</returns>
        public virtual string GetStringFromId(int? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "emp_status", "id", (int?)id);
        }



    }
}
