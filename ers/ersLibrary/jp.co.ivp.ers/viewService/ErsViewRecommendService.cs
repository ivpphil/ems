﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of recommend. 
    /// </summary>
    public class ErsViewRecommendService
    {
        /// <summary>
        /// Get Recommend by Merchandise
        /// </summary>
        /// <param name="merchandise">Merchandise object</param>
        /// <returns>Returns list of values of merchandise from the dictionary</returns>
        public List<Dictionary<string, object>> GetRecommend(ErsMerchandise merchandise, int? member_rank)
        {
            if (merchandise == null)
            {
                return null;
            }

            //recommend作成
            var recommendsStgy = ErsFactory.ersMerchandiseFactory.GetObtainRecommendsStgy();
            var lstRecommends = recommendsStgy.FindRecommends(merchandise);

            var stgy = ErsFactory.ersMerchandiseFactory.GetObtainMerchandisePriceStgy();

            var listRet = new List<Dictionary<string, object>>();
            foreach (var recommendItem in lstRecommends)
            {
                var viewDictionary = recommendItem.GetPropertiesAsDictionary();

                stgy.LoadMaximumAndMinimamPriceOfMerchandise(recommendItem.gcode, member_rank);
                viewDictionary["price"] = stgy.min_price;
                viewDictionary["price2"] = stgy.min_price2;
                viewDictionary["regular_price"] = stgy.min_regular_price;
                viewDictionary["non_member_price"] = stgy.min_non_member_price;
                viewDictionary["non_member_regular_price"] = stgy.min_non_member_regular_price;

                viewDictionary["max_price"] = stgy.max_price;
                viewDictionary["max_regular_price"] = stgy.max_regular_price;

                viewDictionary["onCampaign"] = stgy.isCampaign;

                listRet.Add(viewDictionary);
            }

            return listRet;
        }
    }
}
