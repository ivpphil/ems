﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewElapsedNumService
    {
        public ErsViewElapsedNumService()
        {
        }
        public virtual List<Dictionary<string, object>> GetElapsedNumber()
        {
            List<Dictionary<string, object>> retList = new List<Dictionary<string, object>>();

            for (int i = 0; i < 100; i++)
            {
                var dictionary = new Dictionary<string, object>();
                dictionary["name"] = i + 1;
                dictionary["value"] = i + 1;
                retList.Add(dictionary);
            }

            return retList;
        }
    }
}
