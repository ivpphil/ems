﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of delivery time from sendtime_t table.
    /// </summary>
    public class ErsViewSendtimeService
        : ErsViewServiceBase
    {
        public const string cacheKey = "sendtime-name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersOrderFactory.GetErsSendTimeRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsSendTimeCriteria();
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var list = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(list);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// 配送希望時間プルダウン
        /// </summary>
        /// <returns>List (id and sendtime)</returns>
        public virtual List<Dictionary<string, object>> SelectAsList()
        {
            var list = this.GetCachedList();

            list = this.GetOnlyActiveRecord(list);

            return this.GetNameValueList(list, "sendtime", "id");
        }

        /// <summary>
        /// Get sendtime by ID using ErsDB_sendtime_t.
        /// </summary>
        /// <param name="id">sendtime id</param>
        /// <returns>Returns sendtime value based on the specified ID</returns>
        public virtual string GetStringFromId(int? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "sendtime", "id", id);
        }

        /// <summary>
        /// ID取得用
        /// </summary>
        /// <param name="name">sendtime name</param>
        /// <returns>Returns sendtime id</returns>
        public virtual int? GetIdFromString(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            var list = this.GetCachedList();

            return Convert.ToInt32(this.GetStringFromId(list, "id", "sendtime", name));
        }

        /// <summary>
        /// Check if sendtime is existing by id
        /// </summary>
        /// <param name="id">sendtime id</param>
        /// <returns>true or false</returns>
        public virtual bool ExistValue(int? id)
        {
            if (id == null)
            {
                return false;
            }

            var list = this.GetCachedList();

            return this.ExistValue(list, "id", id);
        }
    }
}
