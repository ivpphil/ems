﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.IO;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.information;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// CTSトップページに表示されるカレンダーのHTMLを生成する
    /// </summary>
    //
    public class ErsViewCtsCalenderService
    {
        /// <summary>
        /// Constructor
        /// </summary>
        internal ErsViewCtsCalenderService()
        {
           
        }

        /// <summary>
        /// </summary>
        /// <param name="zipcode"></param>
        public virtual string GetCalenderHtml(int? agent_id, DateTime calendarMonth,IList<ErsCtsInformation> InformationList)
		{
			string calendarHtml = "";
			int year = calendarMonth.Year;
			int month = calendarMonth.Month;
            
			DateTime curDate = new DateTime(year, month, 1);
			DateTime prevDate = curDate.AddMonths(-1);

            var criteria = GetInformationCriteria((int)agent_id);

			criteria.utimeFrom = prevDate;
			criteria.utimeTo = curDate.AddMonths(1).AddMilliseconds(-1);

            calendarHtml += GetMonthCalendarHtml(curDate, InformationList);
            calendarHtml += GetMonthCalendarHtml(prevDate, InformationList);

			return  calendarHtml;
		}

      

		private string GetMonthCalendarHtml(DateTime dt, IList<ErsCtsInformation> ilist)
		{
                    
			var list = ilist.GroupBy(data => data.utime.Value.Date, (key, elements) => new { Date = key, Count = elements.Count() }).ToList();
			var readList = ilist.Where(data => data.readid > 0).GroupBy(data => data.utime.Value.Date, (key, elements) => new { Date = key, Count = elements.Count() }).ToList();

			string calendarHtml = "";
			int weekStart = 0;

			DateTime startDate = new DateTime(dt.Year, dt.Month, 1);
			DateTime endDate = startDate.AddMonths(1).AddDays(-1);

			calendarHtml += "<div class=\"calendar-month\">";
            
            if (!CheckIfNotJapanese())
            {
                calendarHtml += "<div class=\"month-name\">" + startDate.Year + "/" + startDate.Month + "</div>";
            }
            else { 
                calendarHtml += "<div class=\"month-name\">" + startDate.Year + "年" + startDate.Month + "月</div>";
            }

            string[] weekArray = new string[] { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };

			if (weekStart.Equals(1))
			{
				for (int i = 1; i <= 7; i++)
				{
					calendarHtml += "<div class=\"week-name week"+ (i % 7) + "\">" + weekArray[i] + "</div>";
				}
			}
			else
			{
				for (int i = 0; i <= 6; i++)
				{
					calendarHtml += "<div class=\"week-name week" + i + "\">" + weekArray[i] + "</div>";
				}
			}
			calendarHtml += "<div class=\"weekline\"></div>";

			//前月分空白ボックス
			int pdayTo = Int32.Parse(startDate.DayOfWeek.ToString("d"));
			if (Int32.Parse(startDate.DayOfWeek.ToString("d")) == 0 && weekStart == 1)
				pdayTo = 7;

				for (int i = weekStart; i < pdayTo; i++)
			{
				calendarHtml += "<div class=\"pday week" + i + "\"></div>";
			}

			for (DateTime day = startDate; day <= endDate; day = day.AddDays(1))
			{
				string today = "";
				if (day.Date.Equals(DateTime.Today.Date))
					today = " today";

				if (day.DayOfWeek.ToString("d").Equals(weekStart.ToString()))
					calendarHtml += "<div class=\"weekline\"></div>";

				//calendarHtml += "<div class=\"day week" + day.DayOfWeek.ToString("d") + "\">"+ day.Day + "</div>";

                var findList = list.Where(data => data.Date.ToString("yyyy/MM/dd").StartsWith(day.ToString("yyyy/MM/dd"))).ToList();
                var findReadList = readList.Where(data => data.Date.ToString("yyyy/MM/dd").StartsWith(day.ToString("yyyy/MM/dd"))).ToList();
				int unreadCount = 0;
				string dayClass = "read";
				if (findList.Count > 0)
				{
					if (findReadList.Count > 0)
						unreadCount = findList.First().Count - findReadList.First().Count;
					else
						unreadCount = findList.First().Count;

					if(unreadCount > 0)
						dayClass = "unread";

					calendarHtml += "<div class=\"day week" + day.DayOfWeek.ToString("d") + today + "\"><a href=\"#\" onclick=\"PostDateInfo('" + findList.First().Date.ToString("yyyy/MM/dd") + "');\" class=\"" + dayClass + "\"><div class=\"day-number\">" + day.Day + "</div><div class=\"unread\">" + unreadCount + "</div><div class=\"total\">" + findList.First().Count + "</div></a></div>";
				}
				else
					calendarHtml += "<div class=\"day week" + day.DayOfWeek.ToString("d") + today + "\"><div class=\"day-number\">" + day.Day + "</div></div>";
			}

			//次月分空白ボックス
			if (!(Int32.Parse(endDate.DayOfWeek.ToString("d")) == 0 && weekStart == 1) && Int32.Parse(endDate.DayOfWeek.ToString("d")) + 1 <= (6 + weekStart))
			{
				for (int i = Int32.Parse(endDate.DayOfWeek.ToString("d")) + 1; i <= 6 + weekStart; i++)
				{
					calendarHtml += "<div class=\"nday week" + (i % 7) + "\"></div>";
				}
			}

			calendarHtml += "<div class=\"weekline\"></div>";
			calendarHtml += "</div>"; //close "calendar-month"

			return calendarHtml;
		}

        private ErsCtsInformationCriteria GetInformationCriteria(int agent_id)
        {
            var criteria = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationCriteria();

            criteria.ag_type = ErsFactory.ersCtsOperatorFactory.GetObtainAgentTypeStgy().getAgeType(agent_id);
            criteria.active = EnumActive.Active;
            return criteria;
        }

        private bool CheckIfNotJapanese()
        {
            var ers = (ErsState)ErsCommonContext.GetPooledObject("_sessionState");
            string culture = string.Empty;
            if (ers != null)
            {
                culture = ers.Get("culture");
            }

            if ((ers != null) && (culture != "ja-JP"))
            {
                return false;
            }

            return true;
        }

    }
}
