﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of calendar from calendar_t table.
    /// </summary>    
    public class ErsViewCalendarService
    {
        protected Setup setup;
        
        /// <summary>
        /// Constructor
        /// </summary>
        internal ErsViewCalendarService()
        {
            setup = ErsFactory.ersUtilityFactory.getSetup();
        }

        /// <summary>
        /// Get start date based on baseTime given
        /// </summary>
        /// <param name="baseTime">Value of Date and Time</param>
        /// <returns>Returns start date using GetFromDates method.</returns>
        public virtual DateTime? GetFromDate(DateTime baseTime)
        {
            return GetFromDates(baseTime, setup.sendday, setup.sendday_count);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual DateTime? GetRegularFromDate(DateTime baseTime)
        {
            return GetFromDates(baseTime, setup.sendday, setup.sendday_count);
        }

        /// <summary>
        /// Gets list of date values using GetFromDate and GetList method.
        /// </summary>
        /// <param name="DELIVERY_DAY_COUNT">Count of days delivered</param>
        /// <returns>Returns list of date values based on the count of delivered days and from date.</returns>
        public virtual List<Dictionary<string, object>> SelectAsList(DateTime? fromDate, DateTime? toDate)
        {
            var retList = new List<Dictionary<string, object>>();

            //最短設定可能日を取得
            if (!fromDate.HasValue)
            {
                fromDate = this.GetFromDates(DateTime.Now, setup.sendday, setup.sendday_count);
            }

            if (fromDate.HasValue)
            {
                var list = this.GetList(fromDate.Value, toDate.Value);

                foreach (var date in list)
                {
                    var dic = new Dictionary<string, object>();
                    dic["value"] = date.ToString("yyyy/MM/dd");
                    dic["year"] = date.Year;
                    dic["month"] = date.Month;
                    dic["day"] = date.Day;
                    retList.Add(dic);
                }
            }

            return retList;
        }

        /// <summary>
        /// Gets list of from date using GetFromDate method from ErsDB_calendar_t.
        /// </summary>
        /// <param name="baseTime">Value of Date and Time</param>
        /// <param name="addDays">Number of days given</param>
        /// <returns>Returns list of from date</returns>
        protected internal virtual DateTime? GetFromDates(DateTime baseTime, int addDays, int addToDays)
        {
            //最短設定可能日を取得
            DateTime fromDate = baseTime.AddDays(addDays);
            DateTime toDate = baseTime.AddDays(addToDays);

            var list =  ErsFactory.ersOrderFactory.GetSearchOpenDatesSpec().GetList(fromDate, toDate);
            if (list.Count < 1)
            {
                return null;
            }
            return list.First();
        }

        /// <summary>
        /// Gets list of date values using ErsDB_calendar_t.
        /// </summary>
        /// <param name="DELIVERY_DAY_COUNT">Count of days delivered</param>
        /// <param name="fromDate">From date or start date</param>
        /// <returns>Returns list of date values based on the count of delivered days and from date.</returns>
        protected virtual List<DateTime> GetList(DateTime fromDate, DateTime toDate)
        {
            return ErsFactory.ersOrderFactory.GetSearchOpenDatesSpec().GetList(fromDate, toDate); 
        }

        /// <summary>
        /// Gets list of date values using ErsDB_calendar_t.
        /// </summary>
        /// <param name="DELIVERY_DAY_COUNT">Count of days delivered</param>
        /// <param name="fromDate">From date or start date</param>
        /// <returns>Returns list of date values based on the count of delivered days and from date.</returns>
        public virtual List<int> GetYearList(int fromYear, int YEAR_COUNT)
        {
            var retListYear = new List<int>();
            for (int year = fromYear; year < fromYear + YEAR_COUNT; year++)
            {
                retListYear.Add(year);
            }
            return retListYear;
        }

        /// <summary>
        /// Gets list of date values using ErsDB_calendar_t.
        /// </summary>
        /// <param name="DELIVERY_DAY_COUNT">Count of days delivered</param>
        /// <param name="fromDate">From date or start date</param>
        /// <returns>Returns list of date values based on the count of delivered days and from date.</returns>
        public virtual List<int> GetMonthList(int fromMonth, int MONTH_COUNT)
        {
            var retListYear = new List<int>();
            for (int year = fromMonth; year < fromMonth + MONTH_COUNT; year++)
            {
                retListYear.Add(year);
            }
            return retListYear;
        }


        public virtual List<string> GetScheduleDays()
        {
            List<string> scheduleHeader = new List<string>();

            for (int i = 0; i < 7; i++)
            {
                var date = DateTime.Now.AddDays(i);
                var schedule = date.DayOfWeek.ToString().Substring(0, 3) + " " + date.Day.ToString();
                scheduleHeader.Add(schedule);
            }
            return scheduleHeader;
        }


    }
}
