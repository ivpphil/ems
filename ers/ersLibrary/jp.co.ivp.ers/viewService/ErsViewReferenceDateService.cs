﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Class for Referendate viewServices
    /// </summary>
    public class ErsViewReferenceDateService
    {
        /// <summary>
        /// Gets the list of referencedates (value and name)
        /// </summary>
        /// <returns></returns>
        public virtual List<Dictionary<string, object>> GetList()
        {
            List<Dictionary<string, object>> retList = new List<Dictionary<string, object>>();

            var listofreferencedate = Enum.GetValues(typeof(EnumReferenceDate));            

            foreach (var i in listofreferencedate)
            {
                var dictionary = new Dictionary<string, object>();

                dictionary["name"] = ErsResources.GetFieldName("refDate_" + Convert.ToInt16(i));
                dictionary["value"] = (int)i;

                retList.Add(dictionary);
            }

            return retList;
        }
        
        /// <summary>
        /// Gets the string name of referenceDate by ID
        /// Gets it from fieldNameresource.config
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual string GetStringFromId(EnumReferenceDate id)
        {
            return ErsResources.GetFieldName("refDate_" + (int)id);
        }


    }
}
