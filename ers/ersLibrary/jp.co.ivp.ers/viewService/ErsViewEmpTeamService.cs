﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.viewService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.viewService
{
   public class ErsViewEmpTeamService : ErsViewServiceBase
    {

        public const string cacheKey = "team_name-name_id";

        private List<Dictionary<string,object>> GetCachedList()
        {
            if(!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersEmployeeFactory.GetEmpTeamRepository();
                var cri = ErsFactory.ersEmployeeFactory.GetErsEmpTeamCri();

                var objList = repository.Find(cri);


                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cacheKey];
        }

        public virtual List<Dictionary<string,object>> SelectAsList()
        {
            var list = this.GetCachedList();
            list = this.GetNameValueList(list, "team_name", "id");
            return list;
        }

        public virtual string GetStringFromId(int? id)
        {
            if(id == null)
            {
                return null;
            }
            var list = this.GetCachedList();

            return this.GetStringFromId(list, "team_name", "id", id);
        }



    }
}
