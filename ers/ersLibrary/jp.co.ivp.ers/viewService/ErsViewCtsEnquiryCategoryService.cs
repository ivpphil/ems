﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.Web.Caching;
using System.Web;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewCtsEnquiryCategoryService
        : ErsViewServiceBase
    {
        public const string cacheKey = "name_name-name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryRepository();
                var criteria = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryCriteria();
                criteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
                criteria.SetOrderByCode(Criteria.OrderBy.ORDER_BY_ASC);
                criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
                var objList = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// gets list from common_namecode_t
        /// </summary>
        public List<Dictionary<string, object>> GetList(EnumCtsEnquiryCategoryType type_code, EnumCtsEnquiryCategoryColumnName nameColumn, bool IsActiveOnly = true)
        {
            var list = this.GetCachedList();

            if (IsActiveOnly)
            {
                list = this.GetOnlyActiveRecord(list);
            }

            list = this.GetCollectableRecord(list, type_code);

            list = this.GetNameValueList(list, nameColumn.ToString(), EnumCtsEnquiryCategoryColumnName.code.ToString());

            return list;
        }

        private List<Dictionary<string, object>> GetCollectableRecord(List<Dictionary<string, object>> list, EnumCtsEnquiryCategoryType type_code)
        {
            var retList = new List<Dictionary<string, object>>();

            foreach (var record in list)
            {
                if (this.CheckCollectableRecord(record, type_code))
                {
                    retList.Add(record);
                }
            }
            return retList;
        }

        /// <summary>
        ///  チェックする。
        /// </summary>
        /// <param name="record"></param>
        /// <param name="type_code"></param>
        /// <param name="opt_flg1"></param>
        /// <param name="opt_flg2"></param>
        /// <returns></returns>
        protected virtual bool CheckCollectableRecord(Dictionary<string, object> record, EnumCtsEnquiryCategoryType type_code)
        {
            return type_code.ToString() == Convert.ToString(record["type_code"]);
        }

        /// <summary>
        /// gets list from cts_enquiry_category_t and checks the values in selectedValues
        /// </summary>
        /// <param name="selectedValues"></param>
        /// <param name="type_code">type_code value which is used to narrow down records of cts_enquiry_category_t</param>
        /// <param name="nameColumn"></param>
        /// <returns></returns>
        public IList<Dictionary<string, object>> GetList(IEnumerable<int> selectedValues, EnumCtsEnquiryCategoryType type_code, EnumCtsEnquiryCategoryColumnName nameColumn)
        {
            var returnList = new List<Dictionary<string, object>>();

            var recordList = GetList(type_code, nameColumn);
           
            foreach (var record in recordList)
            {
     
                if (selectedValues != null)
                {
                    if (selectedValues.Contains((int)record["value"]))
                     
                        record["isSelected"] = true;
                    else
                        record["isSelected"] = false;
                }
                returnList.Add(record);

            }

            return returnList;
        }

        /// <summary>
        /// IDから文字列取得
        /// </summary>
        /// <param name="type_code"></param>
        /// <param name="nameColumn"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual string GetStringFromId(EnumCtsEnquiryCategoryType type_code, EnumCtsEnquiryCategoryColumnName nameColumn, int? value)
        {
            if (value == null)
            {
                return ErsResources.GetFieldName("common_undefined");
            }

            var list = GetList(type_code, nameColumn);

            var resultValue = this.GetStringFromId(list, "name", "value", value);

            if (!string.IsNullOrEmpty(resultValue))
            {
                return resultValue;
            }

            return ErsResources.GetFieldName("common_undefined");
        }

        /// <summary>
        /// ID存在チェック
        /// </summary>
        /// <param name="type_code"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool ExistValue(EnumCtsEnquiryCategoryType type_code, int? value)
        {
            if (value == null)
            {
                return false;
            }

            var list = this.GetList(type_code, EnumCtsEnquiryCategoryColumnName.namename);

            return this.ExistValue(list, "value", value);
        }

        /// <summary>
        /// 文字列からID取得
        /// </summary>
        /// <param name="type_code"></param>
        /// <param name="valueColumn"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual int? GetIdFromString(EnumCtsEnquiryCategoryType type_code, EnumCtsEnquiryCategoryColumnName valueColumn, string name)
        {
            if (name == null)
            {
                return null;
            }

            var list = GetList(type_code, valueColumn);

            var resultValue = this.GetStringFromId(list, "value", "name", name);

            if (!string.IsNullOrEmpty(resultValue))
            {
                return Convert.ToInt32(resultValue);
            }

            return null;
        }

        /// <summary>
        /// 文字列存在チェック
        /// </summary>
        /// <param name="type_code"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool ExistName(EnumCtsEnquiryCategoryType type_code, string name)
        {
            if (name == null)
            {
                return false;
            }

            var list = this.GetList(type_code, EnumCtsEnquiryCategoryColumnName.namename);

            return this.ExistValue(list, "name", name);
        }
    }
}
