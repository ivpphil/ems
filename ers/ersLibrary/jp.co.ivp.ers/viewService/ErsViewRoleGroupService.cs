﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewRoleGroupService
         : ErsViewServiceBase
    {
        public const string cacheKey = "role_gname-name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersAdministratorFactory.GetErsRoleGroupRepository();
                var criteria = ErsFactory.ersAdministratorFactory.GetErsRoleGroupCriteria();
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);
                var objList = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cacheKey];
        }

        public virtual IList<Dictionary<string, object>> SelectAsList(IEnumerable<string> selectedValues, bool activeOnly = true)
        {
            var list = this.GetCachedList();

            if (activeOnly)
                list = this.GetOnlyActiveRecord(list);

            list = this.GetNameValueList(list, "role_gname", "role_gcode");

            return SetSelected(list, "role_gcode", selectedValues, "isSelected");
        }

        public virtual string GetStringFromId(string role_gcode)
        {
            if (string.IsNullOrEmpty(role_gcode))
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "role_gname", "role_gcode", role_gcode);
        }

        public virtual bool ExistValue(string role_gcode)
        {
            if (string.IsNullOrEmpty(role_gcode))
            {
                return false;
            }

            var list = this.GetCachedList();

            return this.ExistValue(list, "role_gcode", role_gcode);
        }
    }
}
