﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.IO;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.common;

namespace jp.co.ivp.ers.viewService
{
    /// <summary>
    /// Provides view service to get list or value of zip from zip_t table.
    /// </summary>
    public class ErsViewZipService
    {
        /// <summary>
        /// Constructor
        /// </summary>
        internal ErsViewZipService()
        {
        }

        public string error_message { get; protected set; }
        public int? pref { get; protected set; }
        public string address { get; protected set; }
        public string address2 { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zip"></param>
        /// <returns>エラーメッセージ</returns>
        public virtual void Search(string zip)
        {
            //zipを整形する。
            var searchVal = FormatZip(zip);

            var ZipTableList = this.ZipList(searchVal);

            if (ZipTableList.Count != 1)
            {
                this.error_message =  ErsResources.GetMessage("10201");
                return;
            }

            var Record = ZipTableList[0];
            //結果編集
            this.pref = Record.pref;
            this.address = Record.address;
            this.address2 = Record.address2;
        }

        /// <summary>
        ///Get zip format string using numeric.
        /// </summary>
        /// <param name="zip"></param>
        /// <returns></returns>
        protected string FormatZip(string zip)
        {
            if (zip.Length > 3 && !zip.Contains('-'))
                zip = zip.Substring(0, 3) + "-" + zip.Substring(3);
            return zip;
        }

        /// <summary>
        /// 改ページ版
        /// </summary>
        /// <param name="zipcode">zip code</param>
        /// <returns>Returns list of records from zip_t table based on the given criteria.</returns>
        protected virtual IList<ErsZip> ZipList(string zipcode)
        {
            var repository = ErsFactory.ersCommonFactory.GetErsZipRepository();
            var criteria = ErsFactory.ersCommonFactory.GetErsZipCriteria();
            criteria.zip_prefix_search = zipcode;

            //郵便番号テーブル検索
            return repository.Find(criteria);
        }
    }
}