﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.viewService
{
    public class ErsViewCmsTemplateService
        : ErsViewServiceBase
    {

        public const string cacheKey = "template_code-name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsFactory.ersContentsFactory.GetErsCmsTemplateRepository();
                var criteria = ErsFactory.ersContentsFactory.GetErsCmsTemplateCriteria();
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var objList = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(objList);
            }
            return this.CachedValue[cacheKey];
        }

        public virtual List<Dictionary<string, object>> SelectAsList(string[] selectedValue)
        {
            var list = this.GetCachedList();

            list = this.GetOnlyActiveRecord(list);

            list = this.GetNameValueList(list, "template_type", "id");

            list = this.SetSelected(list, "value", selectedValue, "isChecked");

            return list;
        }
    }
}
