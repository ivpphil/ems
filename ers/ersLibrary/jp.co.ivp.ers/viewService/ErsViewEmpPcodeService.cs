﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.viewService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.viewService
{
   public class ErsViewEmpPcodeService : ErsViewServiceBase
    {
        public const string cacheKey = "pcode-name-id";

        private List<Dictionary<string,object>> GetCachedList()
        {
            if(!this.CachedValue.ContainsKey(cacheKey))
            {
                var repo = ErsFactory.ersPcodeFactory.GetErsPcodeRepository();
                var cri = ErsFactory.ersPcodeFactory.GetErsPcodeCriteria();

                var list = repo.Find(cri);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(list);
            }
            return this.CachedValue[cacheKey];
        }


        public virtual List<Dictionary<string,object>> SelectAsList(bool IsActiveOnly = true)
        {

            var list = this.GetCachedList();

            return this.GetNameValueList(list, "pcode", "id");
        }

        public virtual string GetStringFromId(int? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "pcode", "id", (int?)id);
        }
    }
}
