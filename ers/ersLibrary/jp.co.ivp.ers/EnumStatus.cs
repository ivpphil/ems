﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Enums for Status (running or stopped)
    /// </summary>
    public enum EnumStatus
    {
        /// <summary>
        /// 0 : 未設定
        /// </summary>
        None = 0,

        /// <summary>
        /// 1 : 稼動中 [Running]
        /// </summary>
        Running = 1,

        /// <summary>
        /// 2 : 停止中 [Stopped]
        /// </summary>
        Stopped = 9
    }
}
