﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumEnqSituation
    {
        /// <summary>
        /// 1: 対応中
        /// </summary>
        Correspondence = 1,
        /// <summary>
        /// 2: 自己解決
        /// </summary>
        SelfSolution = 2,
        /// <summary>
        /// 3: AGエスカレーション
        /// </summary>
        AGEscalation = 3,
        /// <summary>
        /// 4: SVエスカレーション
        /// </summary>
        SVEscalation = 4,
        /// <summary>
        /// 5: クライアントエスカレーション
        /// </summary>
        ClientEscalation = 5
    }
}
