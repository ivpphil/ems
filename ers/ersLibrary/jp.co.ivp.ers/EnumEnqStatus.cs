﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumEnqStatus
    {
        /// <summary>
        /// 0: 未設定
        /// </summary>
        None = 0,
        /// <summary>
        /// 1: 問い合わせ
        /// </summary>
        Inquiry = 1,
        /// <summary>
        /// 2: クレーム
        /// </summary>
        Claim,
        /// <summary>
        /// 3: 感謝
        /// </summary>
        Thanks,
        /// <summary>
        /// 4: その他
        /// </summary>
        Other
    }
}
