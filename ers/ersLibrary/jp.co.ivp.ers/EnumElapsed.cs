﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Enums for Elapsed (DaysAgo, DaysLater, MonthsAgo, MonthsLater)
    /// </summary>
    public enum EnumElapsed
    {
        /// <summary>
        /// 1 : 日前 [DaysAgo]
        /// </summary>
        DaysAgo = 1,

        /// <summary>
        /// 2 : 日後 [DaysLater]
        /// </summary>
        DaysLater = 2,

        /// <summary>
        /// 3 : 月前 [MonthsAgo]
        /// </summary>
        MonthsAgo = 3,

        /// <summary>
        /// 4 : 月後 [MonthsLater]
        /// </summary>
        MonthsLater = 4,
    }
}
