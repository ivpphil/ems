﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.step_scenario;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.step_scenario.specification;

namespace jp.co.ivp.ers.step_scenario
{
    public class ErsStepScenarioFactory
    {
        public virtual ErsStepScenarioRepository GetErsStepScenarioRepository()
        {
            return new ErsStepScenarioRepository();
        }

        public virtual ErsStepScenarioCriteria GetErsStepScenarioCriteria()
        {
            return new ErsStepScenarioCriteria();
        }

        public virtual ErsStepScenario GetErsStepScenario()
        {
            return new ErsStepScenario();
        }

        /// <summary>
        /// Retrieves step mail scenario with parameters
        /// </summary>
        /// <param name="parameters">parameters Dictionary</param>
        /// <returns>ErsStepScenario</returns>
        public virtual ErsStepScenario GetErsStepScenarioWithParameters(Dictionary<string, object> parameters)
        {
            var news = this.GetErsStepScenario();
            news.OverwriteWithParameter(parameters);
            return news;
        }

        /// <summary>
        /// Retrieves step mail scenario by id
        /// </summary>
        /// <param name="id">step mail scenario id</param>
        /// <returns></returns>
        public virtual ErsStepScenario GetErsStepScenarioByID(int? id)
        {
            var repository = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioRepository();
            var criteria = ErsFactory.ersStepScenarioFactory.GetErsStepScenarioCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");

            return list[0];
        }

        /// <summary>
        /// Get the Search of StepMail specification
        /// </summary>
        /// <returns>returns instance of StepMailSearchSpec</returns>
        public virtual StepMailSearchSpec GetStepMailSearchSpec()
        {
            return new StepMailSearchSpec();
        }
    }
}
