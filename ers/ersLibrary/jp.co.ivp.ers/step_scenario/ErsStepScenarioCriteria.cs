﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.step_scenario
{
    public class ErsStepScenarioCriteria : Criteria
    {
        public virtual int? id
        {
            set
            {
                Add(Criteria.GetCriterion("step_scenario_t.id", value, Operation.EQUAL));
            }
        }

        public virtual IEnumerable<int> id_in
        {
            set
            {
                Add(Criteria.GetInClauseCriterion("step_scenario_t.id", value));
            }
        }

        public EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("step_scenario_t.active", value, Operation.EQUAL));
            }
        }

        public virtual string scenario_name
        {
            set
            {
                Add(Criteria.GetCriterion("step_scenario_t.scenario_name", value, Operation.EQUAL));
            }
        }

        public virtual string scenario_name_like
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("step_scenario_t.scenario_name", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual EnumOrderPattern? order_ptn_kbn
        {
            set
            {
                Add(Criteria.GetCriterion("step_scenario_t.order_ptn_kbn", value, Operation.EQUAL));
            }
        }

        public virtual EnumStatus? mail_status_kbn
        {
            set
            {
                Add(Criteria.GetCriterion("step_scenario_t.mail_status_kbn", value, Operation.EQUAL));
            }
        }

        public int? target_id
        {
            set
            {
                Add(Criteria.GetCriterion("step_scenario_t.target_id", value, Operation.EQUAL));
            }
        }

        public EnumReferenceDate[] mail_ref_date_kbn_in
        {
            set
            {
                Add(Criteria.GetInClauseCriterion("step_scenario_t.mail_ref_date_kbn", value));
            }
        }

        /// <summary>
        /// Sets order by id
        /// </summary>
        /// <param name="orderBy">order type</param>
        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("step_scenario_t.id", orderBy);
        }

        /// <summary>
        /// Sets order by step_mail_t.id
        /// </summary>
        /// <param name="orderBy">order type</param>
        public virtual void SetOrderByStepId(OrderBy orderBy)
        {
            this.AddOrderBy("step_mail_t.id", orderBy);
        }

        public EnumActive? step_mail_active
        {
            set
            {
                var criteria = ErsFactory.ersStepMailFactory.GetErsStepMailCriteria();
                criteria.active = value;
                this.Add(criteria);
            }
        }

        public EnumStatus step_mail_mail_status_kbn
        {
            set
            {
                var criteria = ErsFactory.ersStepMailFactory.GetErsStepMailCriteria();
                criteria.mail_status_kbn = value;
                this.Add(criteria);
            }
        }

        /// <summary>
        /// set active only
        /// </summary>
        public virtual void SetActiveOnly()
        {
            this.active = EnumActive.Active;
            this.mail_status_kbn = EnumStatus.Running;
            this.step_mail_active = EnumActive.Active;
            this.step_mail_mail_status_kbn = EnumStatus.Running;
        }
    }
}
