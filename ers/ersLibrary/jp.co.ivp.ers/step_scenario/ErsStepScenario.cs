﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.step_scenario
{
    public class ErsStepScenario : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string scenario_name { get; set; }

        public int? target_id { get; set; }

        public int? reg_elapsed_from { get; set; }

        public int? reg_elapsed_to { get; set; }

        public EnumReferenceDate? mail_ref_date_kbn { get; set; }

        public EnumDeliveryTime? mail_delv_time_kbn { get; set; }

        public int? mail_delv_time_hh { get; set; }

        public int? mail_delv_time_mm { get; set; }

        public int? mail_delv_out_time_hh_from { get; set; }

        public int? mail_delv_out_time_mm_from { get; set; }

        public int? mail_delv_out_time_hh_to { get; set; }

        public int? mail_delv_out_time_mm_to { get; set; }

        public string mail_from_addr { get; set; }

        public string mail_from_name { get; set; }

        public string mail_reply_addr { get; set; }

        public EnumStatus? mail_status_kbn { get; set; }

        public virtual DateTime? intime { get; set; }

        public virtual DateTime? utime { get; set; }

        public virtual EnumActive? active { get; set; }
    }
}
