﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.step_scenario
{
    public class ErsStepScenarioRepository
        : ErsRepository<ErsStepScenario>
    {
        public ErsStepScenarioRepository()
            : base("step_scenario_t")
        {
        }

        public ErsStepScenarioRepository(ErsDatabase objDB)
            : base("step_scenario_t", objDB)
        {
        }
    }
}
