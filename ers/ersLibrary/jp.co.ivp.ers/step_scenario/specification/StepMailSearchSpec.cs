﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.step_scenario.specification
{
    /// <summary>
    /// ステップメール検索
    /// </summary>
    public class StepMailSearchSpec
        : ISpecificationForSQL
    {

        /// <summary>
        /// 検索データ取得
        /// </summary>
        /// <param name="crtOrder">クライテリア</param>
        /// <returns>データテーブル</returns>
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return ErsRepository.SelectSatisfying(this, criteria);
        }

        /// <summary>
        /// SELECT句
        /// </summary>
        protected string strSelectFields
        {
            get
            {
                return "step_scenario_t.scenario_name,"
                        + " step_scenario_t.target_id,"
                        + " step_scenario_t.reg_elapsed_from,"
                        + " step_scenario_t.reg_elapsed_to,"
                        + " step_scenario_t.mail_ref_date_kbn,"
                        + " step_scenario_t.mail_delv_time_kbn,"
                        + " step_scenario_t.mail_delv_time_hh,"
                        + " step_scenario_t.mail_delv_time_mm,"
                        + " step_scenario_t.mail_delv_out_time_hh_from,"
                        + " step_scenario_t.mail_delv_out_time_mm_from,"
                        + " step_scenario_t.mail_delv_out_time_hh_to,"
                        + " step_scenario_t.mail_delv_out_time_mm_to,"
                        + " step_scenario_t.mail_from_addr,"
                        + " step_scenario_t.mail_from_name,"
                        + " step_scenario_t.mail_reply_addr,"
                        + " step_scenario_t.mail_status_kbn,"

                        + " step_mail_t.id,"
                        + " step_mail_t.scenario_id,"
                        + " step_mail_t.step_mail_name,"
                        + " step_mail_t.elapsed_num,"
                        + " step_mail_t.elapsed_kbn,"
                        + " step_mail_t.pc_mail_title,"
                        + " step_mail_t.pc_mail_body,"
                        + " step_mail_t.pc_mail_body_txt,"
                        + " step_mail_t.mobile_mail_title,"
                        + " step_mail_t.mobile_mail_body";
            }
        }

        /// <summary>
        /// SQL文
        /// </summary>
        /// <returns>SQL文</returns>
        public string asSQL()
        {
            return "SELECT " + this.strSelectFields
                    + this.SQL;
        }

        /// <summary>
        /// SQL文
        /// </summary>
        protected string SQL
        {
            get
            {
                return " FROM step_scenario_t"
                        + " INNER JOIN step_mail_t"
                        + " ON step_scenario_t.id = step_mail_t.scenario_id AND step_mail_t.active = " + (int)EnumActive.Active;
            }
        }
    }
}