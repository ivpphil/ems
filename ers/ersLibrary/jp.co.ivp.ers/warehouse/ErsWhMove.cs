﻿using System;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhMove : ErsRepositoryEntity
    {

        public override int? id { get; set; }
        public string scode { get; set; }
        public string maker_scode { get; set; }
        public string reason { get; set; }
        public EnumShelfNumber? shelf_from { get; set; }
        public EnumShelfNumber? shelf_to { get; set; }
        public int? amount { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
        public EnumWhMoveType? move_type { get; set; }
    }
}
