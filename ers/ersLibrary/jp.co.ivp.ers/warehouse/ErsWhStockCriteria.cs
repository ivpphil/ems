﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhStockCriteria
        : Criteria
    {
        public string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_stock_t.scode", value, Operation.EQUAL));
            }
        }

        public EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_stock_t.active", value, Operation.EQUAL));
            }
        }

        public virtual DateTime? intime_from
        {
            set
            {
                Add(Criteria.GetCriterion("wh_stock_t.intime", value, Operation.GREATER_EQUAL));
            }
        }

        public virtual DateTime? intime_to
        {
            set
            {
                Add(Criteria.GetCriterion("wh_stock_t.intime", value, Operation.LESS_EQUAL));
            }
        }

        /// <summary>
        /// search condition for supplier_code
        /// </summary>        
        public virtual string supplier_code_prefix
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.supplier_code", value, Criteria.LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual string supplier_code_ambi
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.supplier_code", value, Criteria.LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        /// <summary>
        /// search condition for supplier_name
        /// </summary>        
        public virtual string supplier_name_ambi
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.supplier_name", value, Criteria.LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        /// <summary>
        /// search condition for scode
        /// </summary>        
        public virtual string scode_prefix
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("s_master_t.scode", value, Criteria.LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// search condition for sname
        /// </summary>        
        public virtual string sname_ambi
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("s_master_t.sname", value, Criteria.LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual void SetOrderByIntime(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("wh_stock_t.intime", orderBy);
        }

        /// <summary>
        /// search condition for scode
        /// </summary>        
        public virtual string sm_scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.scode", value, Operation.EQUAL));
            }
        }

        public virtual string sm_scode_ambi
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("s_master_t.scode", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual void SetOrderByID(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("wh_stock_t.id", orderBy);
        }

        public virtual void SetOrderBySID(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("s_master_t.id", orderBy);
        }

        public virtual void SetOrderByScode(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("s_master_t.scode", orderBy);
        }

        public virtual void SetOrderByGcode(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("g_master_t.gcode", orderBy);
        }

        public string scode_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.scode", value, Operation.NOT_EQUAL));
            }
        }

        public virtual string[] ignore_gcode
        {
            set
            {
                this.Add(Criteria.GetNotInClauseCriterion("g_master_t.gcode", value));
            }
        }
    }
}
