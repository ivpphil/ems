﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhOrderInfo
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string order_no { get; set; }

        public string supplier_code { get; set; }

        public EnumWhOrderStatus? wh_order_status { get; set; }

        public string remarks { get; set; }

        public DateTime? intime { get; set; }

        public DateTime? utime { get; set; }

        public EnumActive? active { get; set; }
    }
}
