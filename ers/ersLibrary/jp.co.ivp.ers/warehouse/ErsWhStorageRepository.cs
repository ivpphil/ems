﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhStorageRepository: ErsRepository<ErsWhStorage>
    {
        public ErsWhStorageRepository()
            : base("wh_storage_t")
        {
        }
    }
}
