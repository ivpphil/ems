﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhOrderRepository
        : ErsRepository<ErsWhOrder>
    {
        public ErsWhOrderRepository()
            : base("wh_order_t")
        {
        }

        public IEnumerable<Dictionary<string, object>> FindPastOrderList(ErsWhOrderCriteria criteria)
        {
            var spec = ErsFactory.ersWarehouseFactory.GetSearchPastOrderListSpec();
            return spec.GetSearchData(criteria);
        }

        public int GetPastOrderListCount(ErsWhOrderCriteria criteria)
        {
            var spec = ErsFactory.ersWarehouseFactory.GetSearchPastOrderListSpec();
            return spec.GetCountData(criteria);
        }

        public string GetNextSequenceOrderNo()
        {
            return string.Format("S{0:D19}", this.ersDB_table.GetNextSequence("wh_order_no_seq"));
        }

        public IEnumerable<Dictionary<string, object>> FindPastList(ErsWhOrderCriteria criteria)
        {
            var spec = ErsFactory.ersWarehouseFactory.GetSearchPastOrderSpec();
            return spec.GetSearchData(criteria);
        }

        public int GetPastOrderCount(ErsWhOrderCriteria criteria)
        {
            var spec = ErsFactory.ersWarehouseFactory.GetSearchPastOrderSpec();
            return spec.GetCountData(criteria);
        }
    }
}
