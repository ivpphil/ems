﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.warehouse.strategy
{
    public class ValidateOrderCountStgy
    {
        public virtual IEnumerable<ValidationResult> Validate(string scode, string order_no, int csvShelves)
        {
            ///Acquisition of receipt number of past
            int sumShelves = ErsFactory.ersWarehouseFactory.GetGetSumShelvesStgy().GetSumShelves(scode, order_no, csvShelves);

            int? orderAmount = ErsFactory.ersWarehouseFactory.GetGetOrderAmount().OrderAmount(scode, order_no);
            

            ////Check the number of receipts
            if (orderAmount >= sumShelves)
                yield break;
            else
                yield return new ValidationResult(ErsResources.GetMessage("WHS0002", order_no, scode), new[] { "order_no", "scode" });

          
        }
    }
}
