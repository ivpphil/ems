﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.warehouse.strategy
{
    /// <summary>
    /// Update the stock_t for increased stocks
    /// </summary>
    public class IncreaseWhStockStgy
        : ISpecificationForSQL
    {
        /// <summary>
        /// increasing the stocks
        /// </summary>
        /// <param name="scode"></param>
        /// <param name="amount"></param>
        public virtual void Increase(string scode, int amount, EnumShelfNumber? shelf_number)
        {
            if (shelf_number.HasValue)
            {
                this.shelf_field = (shelf_number == EnumShelfNumber.SHELF001) ? "shelf001" : (shelf_number == EnumShelfNumber.SHELF002) ? "shelf002" : (shelf_number == EnumShelfNumber.SHELF003) ? "shelf003" : null;
            }

            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.scode = scode;
            criteria.g_active = EnumActive.Active;
            criteria.s_active = EnumActive.Active;

            var record = repository.FindSkuBaseItemList(criteria, null);

            if (record.Count == 0)
            {
                throw new ErsException("20000", scode);
            }

            var merchandise = record.First();

            if (merchandise.set_flg != EnumSetFlg.IsSet)
            {
                this.IncreaseNotSet(scode, amount);
            }
            else
            {
                this.IncreaseSet(scode, amount);
            }
        }

        public void IncreaseNotSet(string scode, int amount)
        {
            this.scode = scode;
            this.amount = amount;

            ErsRepository.UpdateSatisfying(this, null);
        }

        /// <summary>
        /// Set increase of set item's stock
        /// </summary>
        /// <param name="scode"></param>
        /// <param name="amount"></param>
        private void IncreaseSet(string scode, int amount)
        {
            //the parent stock is not increased.

            //子商品リスト取得
            var IncreaseSetMerchandiseList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(scode);

            //子商品分在庫減算
            foreach (ErsSetMerchandise item in IncreaseSetMerchandiseList)
            {
                var setMerchandise = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(item.scode);

                if (setMerchandise == null)
                {
                    continue;
                }

                this.IncreaseNotSet(setMerchandise.scode, item.amount.Value * amount);
            }
        }

        protected virtual string scode { get; set; }
        protected virtual int amount { get; set; }
        protected virtual string shelf_field { get; set; }

        public virtual string asSQL()
        {
            if (this.shelf_field != null)
                return string.Format("UPDATE wh_stock_t SET {0} = {0} + {1} WHERE scode = '{2}' ", this.shelf_field, this.amount, this.scode);

            return "UPDATE wh_stock_t SET stock = stock + " + this.amount + " WHERE scode = '" + this.scode + "' ";
        }
    }
}
