﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.warehouse.strategy
{
    public class CheckStatusOrderStgy
    {
        public virtual IEnumerable<ValidationResult> CheckStatusOrder(string order_no)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhOrderInfoRepository();
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhOrderInfoCriteria();
            criteria.active = EnumActive.Active;
            criteria.order_no = order_no;

            if (repository.GetRecordCount(criteria) > 0)
            {
                var list = repository.Find(criteria);
                if (list[0].wh_order_status != EnumWhOrderStatus.NotStorage)
                    yield return new ValidationResult(ErsResources.GetMessage(("WHS0003"), order_no), new[] { "order_no" });
            }
                
        }
    }
}
