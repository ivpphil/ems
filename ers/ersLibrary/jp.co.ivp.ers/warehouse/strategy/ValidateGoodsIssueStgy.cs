﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.warehouse.strategy
{
    public class ValidateGoodsIssueStgy
    {

        public virtual IEnumerable<ValidationResult> Validate(string scode, EnumShelfNumber? shelf_from, int? amount)
        {
            var repo = ErsFactory.ersWarehouseFactory.GetErsWhStockRepository();
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhStockCriteria();
            criteria.scode = scode;

            if (repo.GetRecordCount(criteria) > 0)
            {
                var whStock = repo.Find(criteria)[0];
                var shelfValue = (shelf_from == EnumShelfNumber.SHELF001) ? whStock.shelf001.Value : (shelf_from == EnumShelfNumber.SHELF002) ? whStock.shelf002.Value : (shelf_from == EnumShelfNumber.SHELF003) ? whStock.shelf003.Value : 0;
                if ((shelfValue - amount) < 0)
                    yield return new ValidationResult(ErsResources.GetMessage("MOVCSV0003", shelf_from));
            }
        }

    }
}
