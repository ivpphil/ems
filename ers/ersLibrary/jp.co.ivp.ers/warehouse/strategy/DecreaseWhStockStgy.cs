﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.warehouse.strategy
{
    /// <summary>
    /// Update the stock_t for decreased stocks
    /// </summary>
    public class DecreaseWhStockStgy
        : ISpecificationForSQL
    {
        /// <summary>
        /// セットで無い商品の減算
        /// </summary>
        /// <param name="merchandise"></param>
        /// <param name="amount"></param>
        public virtual void Decrease(string scode, int amount, EnumShelfNumber? shelf_number)
        {

            if (shelf_number.HasValue)
            {
                this.shelf_field = (shelf_number == EnumShelfNumber.SHELF001) ? "shelf001" : (shelf_number == EnumShelfNumber.SHELF002) ? "shelf002" : (shelf_number == EnumShelfNumber.SHELF003) ? "shelf003" : null; ;
            }

            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.scode = scode;
            criteria.g_active = EnumActive.Active;
            criteria.s_active = EnumActive.Active;

            var record = repository.FindSkuBaseItemList(criteria, null);

            if (record.Count == 0)
            {
                throw new ErsException("20000", scode);
            }

            var merchandise = record.First();

            if (merchandise.set_flg != EnumSetFlg.IsSet)
            {
                this.DecreaseNotSet(scode, amount);
            }
            else
            {
                this.DecreaseSet(scode, amount);
            }
        }

        /// <summary>
        /// セットで無い商品の減算
        /// </summary>
        /// <param name="merchandise"></param>
        public void DecreaseNotSet(string scode, int amount)
        {
            this.scode = scode;
            this.amount = amount;

            ErsRepository.UpdateSatisfying(this, null);
        }

        /// <summary>
        /// セット商品の減算
        /// </summary>
        /// <param name="merchandise"></param>
        private void DecreaseSet(string scode, int amount)
        {
            //the parent stock is not decreased.

            //子商品リスト取得
            var DecreaseSetMerchandiseList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(scode);

            //子商品分在庫減算
            foreach (ErsSetMerchandise item in DecreaseSetMerchandiseList)
            {
                var setMerchandise = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(item.scode);

                if (setMerchandise == null)
                {
                    continue;
                }

                this.DecreaseNotSet(setMerchandise.scode, item.amount.Value * amount);
            }
        }

        protected virtual string scode { get; set; }
        protected virtual int amount { get; set; }
        protected virtual string shelf_field { get; set; }

        public virtual string asSQL()
        {
            if (this.shelf_field != null)
                return string.Format("UPDATE wh_stock_t SET {0} = {0} - {1} WHERE scode = '{2}' AND ({0} - {1}) >= 0", this.shelf_field, this.amount, this.scode);

            return "UPDATE wh_stock_t SET stock = stock - " + this.amount + " WHERE scode = '" + this.scode + "' AND (stock - " + this.amount + ") >= 0";

        }
    }
}
