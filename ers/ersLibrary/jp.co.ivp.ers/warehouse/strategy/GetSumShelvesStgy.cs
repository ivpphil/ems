﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.warehouse.strategy
{
    public class GetSumShelvesStgy
    {
        public int GetSumShelves(string scode, string order_no, int csvShelves)
        {
            int sumShelf = 0;

            var specs = ErsFactory.ersWarehouseFactory.GetSumShelvesSpec();
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhStorageCriteria();
            criteria.order_no = order_no;
            criteria.scode = scode;
            criteria.active = EnumActive.Active;

            if (specs.GetCountData(criteria) > 0)
            {
                var list = specs.GetSearchData(criteria);
                sumShelf = Convert.ToInt32(list[0]["sumshelves"]);
            }

            return sumShelf + csvShelves;
        }
    }
}
