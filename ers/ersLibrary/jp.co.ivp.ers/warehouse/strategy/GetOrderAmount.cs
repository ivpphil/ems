﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.warehouse.strategy
{
    public class GetOrderAmount
    {
        public virtual int? OrderAmount(string scode, string order_no)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhOrderRepository();
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhOrderCriteria();

            criteria.order_no = order_no;
            criteria.scode = scode;
            criteria.active = EnumActive.Active;

            if (repository.GetRecordCount(criteria) > 0)
                return repository.Find(criteria)[0].amount;

            return 0;
        }
    }
}
