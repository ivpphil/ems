﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse.strategy
{
    public class UpdateWhStockStgy : ISpecificationForSQL
    {
        public virtual void UpdateStock(ErsWhStorage storage)
        {
            this.scode = storage.scode;
            this.shelf001 = storage.shelf001;
            this.shelf002 = storage.shelf002;
            this.shelf003 = storage.shelf003;
            var result = ErsRepository.UpdateSatisfying(this, null);
        }

        protected virtual string scode { get; set; }
        protected virtual int? shelf001 { get; set; }
        protected virtual int? shelf002 { get; set; }
        protected virtual int? shelf003 { get; set; }

        public virtual string asSQL()
        {
            return "UPDATE wh_stock_t SET stock = stock + " + this.shelf001 +
                " , shelf001 = shelf001 + " + this.shelf001 +
                " , shelf002 = shelf002 + " + this.shelf002 +
                " , shelf003 = shelf003 + " + this.shelf003 +
                " , utime = now() " +
                " WHERE scode = '" + this.scode + "' ";
        }
    }
}
