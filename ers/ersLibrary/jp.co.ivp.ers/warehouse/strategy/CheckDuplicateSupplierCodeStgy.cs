﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.warehouse;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.warehouse.strategy
{
    /// <summary>
    /// Checks Whether the supplier code already exists from wh_supplier_t.
    /// </summary>
    public class CheckDuplicateSupplierCodeStgy
    {
        /// <summary>
        /// check for duplicate supplier code
        /// </summary>
        public virtual ValidationResult CheckDuplicate(int? id,string supplier_code)
        {
            if (string.IsNullOrEmpty(supplier_code))
            {
                return null;
            }

            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhSupplierCriteria();

            if (id.HasValue) criteria.id_not_equal = id;
            criteria.supplier_code = supplier_code;

            var repository = ErsFactory.ersWarehouseFactory.GetErsWhSupplierRepository();

            if (repository.GetRecordCount(criteria) > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("supplier_code"), supplier_code), new[] { "supplier_code" });
            }

            return null;
        }

    }
}
