﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.warehouse.strategy
{
    public class ValidateShelfNumberStgy
    {

        public virtual IEnumerable<ValidationResult> Validate(EnumShelfNumber? shelf_number)
        { 
            if((shelf_number != EnumShelfNumber.SHELF001) && (shelf_number != EnumShelfNumber.SHELF002) && (shelf_number != EnumShelfNumber.SHELF003))
                yield return new ValidationResult(ErsResources.GetMessage("MOVCSV0002", shelf_number));
        }

    }
}
