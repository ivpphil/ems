﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.warehouse.strategy
{
    public class CheckOrderNumberExistStgy
    {
        public virtual IEnumerable<ValidationResult> CheckOrderNumberExist(string scode, string order_no)
        {
            var repository = ErsFactory.ersWarehouseFactory.GetErsWhOrderRepository();
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhOrderCriteria();
            criteria.active = EnumActive.Active;
            criteria.order_no = order_no;
            criteria.scode = scode;

            if (repository.GetRecordCount(criteria) == 0)
                yield return new ValidationResult(ErsResources.GetMessage(("WHS0001"), order_no, scode), new[] { "order_no","scode" });
        }
    }
}
