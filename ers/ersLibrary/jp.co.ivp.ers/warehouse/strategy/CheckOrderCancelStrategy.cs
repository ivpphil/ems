﻿using System;
using System.Data;
using jp.co.ivp.ers.mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using System.Linq;

namespace jp.co.ivp.ers.warehouse.strategy
{
    public class CheckOrderCancelStrategy
    {
        public virtual bool IsOverCancelAmount(string scode, int? cancel_amount)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.scode = scode;
            var listSku = repository.FindSkuBaseItemList(criteria, 1);

            if (listSku.Count == 0)
            {
                return false;
            }

            var objSku = listSku.First();

            if (objSku.soldout_flg == EnumSoldoutFlg.DisableSoldout)
            {
                return false;
            }

            return (objSku.stock - cancel_amount < 0);
        }
    }
}