﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse.strategy
{
    public class UpdateWhOrderInfoStgy : ISpecificationForSQL
    {

        public virtual void UpdateWhOrderInfo(string order_no)
        {
            this.order_no = order_no;
            var result = ErsRepository.UpdateSatisfying(this, null);
        }

        protected virtual string order_no { get; set; }

        public virtual string asSQL()
        {
            return "UPDATE wh_order_info_t SET wh_order_status = " + (int)EnumWhOrderStatus.Storaged + 
                " , utime = now() " +
                " WHERE order_no = '" + this.order_no + "' ";
        }
    }
}
