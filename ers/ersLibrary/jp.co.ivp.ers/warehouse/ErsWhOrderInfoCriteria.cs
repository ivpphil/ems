﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhOrderInfoCriteria
        : Criteria
    {
        public DateTime? intime_less_than { get; set; }

        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_order_info_t.id", value, Operation.EQUAL));
            }
        }

        public string order_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_order_info_t.order_no", value, Operation.EQUAL));
            }
        }

        public string s_supplier_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_supplier_t.supplier_code", value, Operation.EQUAL));
            }
        }

        public DateTime? intime
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_order_info_t.intime", value, Operation.EQUAL));
            }
        }

        public EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_order_info_t.active", value, Operation.EQUAL));
            }
        }
    }
}
