﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhOrderInfoRepository
        : ErsRepository<ErsWhOrderInfo>
    {
        public ErsWhOrderInfoRepository()
            : base("wh_order_info_t")
        {
        }
    }
}
