﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.warehouse.specification;
using jp.co.ivp.ers.warehouse.strategy;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWarehouseFactory
    {
        public virtual ErsWhOrderRepository GetErsWhOrderRepository()
        {
            return new ErsWhOrderRepository();
        }

        public virtual ErsWhOrderCriteria GetErsWhOrderCriteria()
        {
            return new ErsWhOrderCriteria();
        }

        public virtual ErsWhOrder GetErsWhOrder()
        {
            return new ErsWhOrder();
        }

        public virtual ErsWhOrderInfoRepository GetErsWhOrderInfoRepository()
        {
            return new ErsWhOrderInfoRepository();
        }

        public virtual ErsWhOrderInfoCriteria GetErsWhOrderInfoCriteria()
        {
            return new ErsWhOrderInfoCriteria();
        }

        public virtual ErsWhOrderInfo GetErsWhOrderInfo()
        {
            return new ErsWhOrderInfo();
        }

        public virtual SearchOrderNeededSpec GetSearchOrderNeededSpec()
        {
            return new SearchOrderNeededSpec();
        }

        public virtual ErsWhStockRepository GetErsWhStockRepository()
        {
            return new ErsWhStockRepository();
        }

        public virtual ErsWhStockCriteria GetErsWhStockCriteria()
        {
            return new ErsWhStockCriteria();
        }

        public virtual ErsWhStock GetErsWhStock()
        {
            return new ErsWhStock();
        }

        public ErsWhStock GetErsWhStockWithScode(string scode)
        {
            var whStockRepository = this.GetErsWhStockRepository();
            var whStockCriteria = this.GetErsWhStockCriteria();
            whStockCriteria.scode = scode;
            var dataList = whStockRepository.Find(whStockCriteria);

            if (dataList.Count == 1)
                return dataList[0];
            else
                return null;
        }

        public virtual DecreaseWhStockStgy GetDecreaseWhStockStgy()
        {
            return new DecreaseWhStockStgy();
        }

        public virtual IncreaseWhStockStgy GetIncreaseWhStockStgy()
        {
            return new IncreaseWhStockStgy();
        }

        public virtual ErsWhSupplierRepository GetErsWhSupplierRepository()
        {
            return new ErsWhSupplierRepository();
        }

        public virtual ErsWhSupplierCriteria GetErsWhSupplierCriteria()
        {
            return new ErsWhSupplierCriteria();
        }

        public virtual ErsWhSupplier GetErsWhSupplier()
        {
            return new ErsWhSupplier();
        }

        public virtual SearchPastOrderListSpec GetSearchPastOrderListSpec()
        {
            return new SearchPastOrderListSpec();
        }

        public virtual SearchPastOrderSpec GetSearchPastOrderSpec()
        {
            return new SearchPastOrderSpec();
        }

        public virtual CheckDuplicateSupplierCodeStgy GetCheckDuplicateSupplierCodeStgy()
        {
            return new CheckDuplicateSupplierCodeStgy();
        }

        public virtual CheckOrderCancelStrategy GetCheckOrderCancelStrategy()
        {
            return new CheckOrderCancelStrategy();
        }
        

        public virtual ErsWhOrder GetErsWhOrderWithId(int? id)
        {
            var repository = this.GetErsWhOrderRepository();
            var criteria = this.GetErsWhOrderCriteria();
            criteria.id = id;
            var recordCount = repository.GetRecordCount(criteria);
            if (recordCount!=1 )
            {
                return null;
            }

            var listRecord = repository.Find(criteria);
            return listRecord.First();
        }

        public ErsWhSupplier GetErsWhSupplierWithId(int id)
        {
            var supplierRepository = this.GetErsWhSupplierRepository();
            var supplierCriteria = this.GetErsWhSupplierCriteria();
            supplierCriteria.id = id;
            var dataList = supplierRepository.Find(supplierCriteria);

            if (dataList.Count == 1)
                return dataList[0];
            else
                return null;
        }

        public ErsWhSupplier GetErsWhSupplierWithSupplierCode(string supplier_code)
        {
            var supplierRepository = this.GetErsWhSupplierRepository();
            var supplierCriteria = this.GetErsWhSupplierCriteria();
            supplierCriteria.supplier_code = supplier_code;
            var dataList = supplierRepository.Find(supplierCriteria);

            if (dataList.Count == 1)
                return dataList[0];
            else
                return null;
        }

        public virtual ErsWhSupplier GetErsWhSupplierWithParameters(Dictionary<string, object> parameters)
        {
            var entity = this.GetErsWhSupplier();
            entity.OverwriteWithParameter(parameters);
            return entity;
        }

        public virtual ErsWhStorageRepository GetErsWhStorageRepository()
        {
            return new ErsWhStorageRepository();
        }

        public virtual ErsWhStorageCriteria GetErsWhStorageCriteria()
        {
            return new ErsWhStorageCriteria();
        }

        public virtual ErsWhStorage GetErsWhStorage()
        {
            return new ErsWhStorage();
        }

        public ErsWhMove GetErsWhMove()
        {
            return new ErsWhMove();
        }

        public ErsWhMoveCriteria GetErsWhMoveCriteria()
        {
            return new ErsWhMoveCriteria();
        }

        public ErsWhMoveRepository GetErsWhMoveRepository()
        {
            return new ErsWhMoveRepository();
        }

        public UpdateWhStockStgy GetUpdateWhStockStgy()
        {
            return new UpdateWhStockStgy();
        }


        public ValidateShelfNumberStgy GetValidateShelfNumberStgy()
        {
            return new ValidateShelfNumberStgy();
        }

        public ValidateGoodsIssueStgy GetValidateGoodsIssueStgy()
        {
            return new ValidateGoodsIssueStgy();
        }

        public SearchWhMoveSpec GetSearchWhMoveSpec()
        {
            return new SearchWhMoveSpec();
        }

        public virtual ErsWhMove GetErsWhMoveWithParameters(Dictionary<string, object> parameters)
        {
            var entity = this.GetErsWhMove();
            entity.OverwriteWithParameter(parameters);
            return entity;
        }

        public ValidateOrderCountStgy GetValidateOrderCountStgy()
        {
            return new ValidateOrderCountStgy();
        }

        public StorageDownloadListSpec GetStorageDownloadListSpec()
        {
            return new StorageDownloadListSpec();
        }

        public SearchStockSpec GetSearchStockSpec()
        {
            return new SearchStockSpec();
        }

        public CheckOrderNumberExistStgy GetCheckOrderNumberExistStgy()
        {
            return new CheckOrderNumberExistStgy();
        }

        public CheckStatusOrderStgy GetCheckStatusOrderStgy()
        {
            return new CheckStatusOrderStgy();
        }

        public GetSumShelvesStgy GetGetSumShelvesStgy()
        {
            return new GetSumShelvesStgy();
        }

        public UpdateWhOrderInfoStgy GetUpdateWhOrderInfoStgy()
        {
            return new UpdateWhOrderInfoStgy();
        }

        public SumShelvesSpec GetSumShelvesSpec()
        {
            return new SumShelvesSpec();
        }

        public GetOrderAmount GetGetOrderAmount()
        {
            return new GetOrderAmount();
        }
    }
}
