﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhSupplierCriteria
        : Criteria
    {
        public virtual void SetOrderById(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("am_template_t.id", orderBy);
        }

        /// <summary>
        /// search condition for id
        /// </summary>      
        public int id
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_supplier_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for supplier_code
        /// </summary>        
        public virtual string supplier_code
        {
            set
            {
                Add(Criteria.GetCriterion("wh_supplier_t.supplier_code", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for supplier_name
        /// </summary>        
        public virtual string supplier_name
        {
            set
            {
                Add(Criteria.GetCriterion("wh_supplier_t.supplier_name", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for zip
        /// </summary>        
        public virtual string zip
        {
            set
            {
                Add(Criteria.GetCriterion("wh_supplier_t.zip", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for pref
        /// </summary>        
        public virtual int? pref
        {
            set
            {
                Add(Criteria.GetCriterion("wh_supplier_t.pref", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for address
        /// </summary>        
        public virtual string address
        {
            set
            {
                Add(Criteria.GetCriterion("wh_supplier_t.address", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for tel
        /// </summary>        
        public virtual string tel
        {
            set
            {
                Add(Criteria.GetCriterion("wh_supplier_t.tel", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for fax
        /// </summary>        
        public virtual string fax
        {
            set
            {
                Add(Criteria.GetCriterion("wh_supplier_t.fax", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for email address
        /// </summary>        
        public virtual string email
        {
            set
            {
                Add(Criteria.GetCriterion("wh_supplier_t.email", value, Operation.EQUAL));
            }
        }

        public EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_supplier_t.active", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Not equal to id criteria
        /// </summary>        
        public virtual int? id_not_equal
        {
            set
            {
                Add(Criteria.GetCriterion("wh_supplier_t.id", value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// Set Order by supplier code
        /// </summary>
        /// <param name="orderBy"></param>
        public virtual void SetOrderBySupplierCode(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("wh_supplier_t.supplier_code", orderBy);
        }

        /// <summary>
        /// search condition for supplier_code
        /// </summary>        
        public virtual string supplier_code_prefix
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.supplier_code",value,Criteria.LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual string supplier_code_ambi
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.supplier_code", value, Criteria.LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        /// <summary>
        /// search condition for supplier_name
        /// </summary>        
        public virtual string supplier_name_ambi
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.supplier_name", value, Criteria.LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        /// <summary>
        /// search condition for address
        /// </summary>        
        public virtual string address_ambi
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.address", value, Criteria.LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        /// <summary>
        /// search condition for tel
        /// </summary>        
        public virtual string tel_prefix
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.tel", value, Criteria.LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// search condition for fax
        /// </summary>        
        public virtual string fax_prefix
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.fax", value, Criteria.LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// search condition for email address
        /// </summary>        
        public virtual string email_prefix
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.email", value, Criteria.LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }
    }
}
