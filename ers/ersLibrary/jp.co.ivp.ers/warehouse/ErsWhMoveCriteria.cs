﻿using jp.co.ivp.ers.db;
using System;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhMoveCriteria : Criteria
    {

        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("wh_move_t.id", value, Operation.EQUAL));
            }
        }

        public string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_move_t.scode", value, Operation.EQUAL));
            }
        }

        public string maker_scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_move_t.maker_scode", value, Operation.EQUAL));
            }
        }

        public EnumWhMoveType? move_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_move_t.move_type", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_move_t.active", value, Operation.EQUAL));
            }
        }
        public virtual void SetOrderByIntime(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("wh_move_t.intime", orderBy);
        }

        public virtual string supplier_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_supplier_t.supplier_code", value, Operation.EQUAL));
            }
        }

        public virtual string supplier_code_ambi
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.supplier_code", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public string scode_sm
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.scode", value, Operation.EQUAL));
            }
        }

        public string scode_ambi
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("s_master_t.scode", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public string maker_scode_sm
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.maker_scode", value, Operation.EQUAL));
            }
        }

        public string sname
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.sname", value, Operation.EQUAL));
            }
        }

        public string sname_ambi
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("s_master_t.sname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual string supplier_name
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_supplier_t.supplier_name", value, Operation.EQUAL));
            }
        }

        public virtual string supplier_name_ambi
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.supplier_name", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual DateTime? intime_from
        {
            set
            {
                Add(Criteria.GetCriterion("wh_move_t.intime", value, Operation.GREATER_EQUAL));
            }
        }

        public virtual DateTime? intime_to
        {
            set
            {
                Add(Criteria.GetCriterion("wh_move_t.intime", value, Operation.LESS_EQUAL));
            }
        }

        public string maker_scode_sm_prefix
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("s_master_t.maker_scode", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual void SetOrderByID(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("wh_move_t.id", orderBy);
        }
    }
}
