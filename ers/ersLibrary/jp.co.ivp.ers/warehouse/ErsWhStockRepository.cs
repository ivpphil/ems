﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhStockRepository
        : ErsRepository<ErsWhStock>
    {
        public ErsWhStockRepository()
            : base("wh_stock_t")
        {
        }
    }
}
