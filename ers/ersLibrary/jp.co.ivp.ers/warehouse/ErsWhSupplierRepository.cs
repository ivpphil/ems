﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhSupplierRepository
        : ErsRepository<ErsWhSupplier>
    {
        public ErsWhSupplierRepository()
            : base("wh_supplier_t")
        {
        }
    }
}
