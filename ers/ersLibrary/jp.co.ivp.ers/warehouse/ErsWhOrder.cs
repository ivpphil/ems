﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhOrder
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string order_no { get; set; }

        public string supplier_code { get; set; }

        public string scode { get; set; }

        public int? price { get; set; }

        public int? cost_price { get; set; }

        public int? amount { get; set; }

        public EnumWhOrderType? wh_order_type { get; set; }

        public EnumWhUpStock? up_stock { get; set; }

        public DateTime? intime { get; set; }

        public DateTime? utime { get; set; }

        public EnumActive? active { get; set; }

        public DateTime? schedule_date { get; set; }
    }
}
