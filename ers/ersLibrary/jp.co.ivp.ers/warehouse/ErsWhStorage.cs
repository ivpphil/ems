﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhStorage : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string order_no { get; set; }

        public string scode { get; set; }

        public string maker_scode { get; set; }

        public int? shelf001 { get; set; }

        public int? shelf002 { get; set; }

        public int? shelf003 { get; set; }

        public EnumActive? active { get; set; }
    }
}
