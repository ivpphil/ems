﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhOrderCriteria
        : Criteria
    {
        public DateTime? intime_less_than { get; set; }

        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_order_t.id", value, Operation.EQUAL));
            }
        }

        public string order_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_order_t.order_no", value, Operation.EQUAL));
            }
        }

        public IEnumerable<string> order_no_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("wh_order_t.order_no", value));
            }
        }

        public string supplier_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_supplier_t.supplier_code", value, Operation.EQUAL));
            }
        }

        public string supplier_code_ambi
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.supplier_code", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public string sname_ambiguous
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("s_master_t.sname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public EnumWhOrderType? wh_order_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.wh_order_type", value, Operation.EQUAL));
            }
        }

        public DateTime? intime
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_order_t.intime", value, Operation.EQUAL));
            }
        }

        public EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_order_t.active", value, Operation.EQUAL));
            }
        }

        public DateTime? intime_from
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_order_t.intime", value, Operation.GREATER_EQUAL));
            }
        }

        public DateTime? intime_to
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_order_t.intime", value, Operation.LESS_EQUAL));
            }
        }


        public string supplier_name
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_supplier_t.supplier_name", value, Operation.EQUAL));
            }
        }

        public string supplier_name_ambiguous
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("wh_supplier_t.supplier_name", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public void SetOrderBySScode()
        {
            this.AddOrderBy("s_master_t.scode", OrderBy.ORDER_BY_ASC);
        }
        public void SetOrderBySupplierCode()
        {
            this.AddOrderBy("wh_supplier_t.supplier_code", OrderBy.ORDER_BY_DESC);
        }
        public void SetOrderByWhOrderType()
        {
            this.AddOrderBy("s_master_t.wh_order_type", OrderBy.ORDER_BY_DESC);
        }
        public void SetOrderByCostPrice()
        {
            this.AddOrderBy("price_t.cost_price", OrderBy.ORDER_BY_DESC);
        }
        public void SetOrderByOrderNo()
        {
            this.AddOrderBy("wh_order_t.order_no", OrderBy.ORDER_BY_DESC);
        }

        public string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_order_t.scode", value, Operation.EQUAL));
            }
        }

        public string scode_ambi
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("wh_order_t.scode", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public string s_scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.scode", value, Operation.EQUAL));
            }
        }

        public string s_scode_ambi
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("s_master_t.scode", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public void SetBetweenScheduleDate(DateTime from, DateTime to)
        {
            this.Add(Criteria.GetBetweenCriterion(from, to, ColumnName("wh_order_t.schedule_date")));
        }

        public int? amount_greater_than
        {
            set 
            {
                this.Add(Criteria.GetCriterion("wh_order_t.amount", value, Operation.GREATER_THAN));
            }
        }

        public EnumWhOrderStatus? wh_order_status
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_order_info_t.wh_order_status", value, Operation.EQUAL));
            }
        }
    }
}


