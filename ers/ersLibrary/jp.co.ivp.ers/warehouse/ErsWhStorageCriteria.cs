﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhStorageCriteria : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_storage_t.id", value, Operation.EQUAL));
            }
        }

        public string order_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_storage_t.order_no", value, Operation.EQUAL));
            }
        }

        public string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_storage_t.scode", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("wh_storage_t.active", (int)value, Operation.EQUAL));
            }
        }
    }
}
