﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.warehouse.specification
{
    public class SearchWhMoveSpec : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return " SELECT wh_move_t.shelf_from,move_type, wh_move_t.shelf_to,wh_move_t.amount,wh_move_t.reason,wh_move_t.intime, "
                + " s_master_t.scode, s_master_t.maker_scode,s_master_t.sname, "
                + " wh_supplier_t.supplier_code,wh_supplier_t.supplier_name "
                + " FROM wh_move_t "
                + " INNER JOIN s_master_t ON wh_move_t.scode = s_master_t.scode "
                + " LEFT JOIN wh_supplier_t ON s_master_t.supplier_code = wh_supplier_t.supplier_code ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(*) AS " + countColumnAlias + " FROM wh_move_t "
                + " INNER JOIN s_master_t ON wh_move_t.scode = s_master_t.scode "
                + " LEFT JOIN wh_supplier_t ON s_master_t.supplier_code = wh_supplier_t.supplier_code ";
        }
    }
}
