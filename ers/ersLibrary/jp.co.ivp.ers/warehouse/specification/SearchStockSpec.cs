﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.warehouse.specification
{
    public class SearchStockSpec : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return " SELECT DISTINCT ON (s_master_t.id) g_master_t.gcode, s_master_t.scode, s_master_t.sname,price_t.price,price_t.cost_price,wh_stock_t.shelf001,wh_stock_t.shelf002,wh_stock_t.shelf003,amount,stock_t.stock "
                + " FROM g_master_t "
                + " LEFT OUTER JOIN s_master_t ON g_master_t.gcode = s_master_t.gcode "
                + " LEFT JOIN ( SELECT DISTINCT ON (scode) scode, price,cost_price  FROM price_t  where price_kbn = " + (int)EnumPriceKbn.NORMAL + "  ORDER BY scode, COALESCE(member_rank, -1) DESC "
                + " ) AS price_t ON s_master_t.scode = price_t.scode "
                + " LEFT OUTER JOIN stock_t ON s_master_t.scode = stock_t.scode "
                + " LEFT JOIN wh_stock_t ON s_master_t.scode = wh_stock_t.scode "
                + " LEFT JOIN wh_supplier_t ON s_master_t.supplier_code = wh_supplier_t.supplier_code "
                + " LEFT JOIN ( SELECT scode,SUM(amount) as amount  FROM wh_order_t "
                + " INNER JOIN wh_order_info_t ON wh_order_t.order_no = wh_order_info_t.order_no "
                + " WHERE wh_order_info_t.wh_order_status = " + (int)EnumWhOrderStatus.NotStorage + " AND wh_order_t.active = " + (int)EnumActive.Active + " GROUP BY scode ) "
                + " as wh_order_t on s_master_t.scode =wh_order_t.scode WHERE true";																
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(DISTINCT s_master_t.id) AS " + countColumnAlias
                + " FROM g_master_t "
                + " INNER JOIN s_master_t ON g_master_t.gcode = s_master_t.gcode "
                + " LEFT JOIN ( SELECT DISTINCT ON (scode) scode, price,cost_price  FROM price_t  where price_kbn = " + (int)EnumPriceKbn.NORMAL + "  ORDER BY scode, COALESCE(member_rank, -1) DESC "
                + " ) AS price_t ON s_master_t.scode = price_t.scode "
                + " INNER JOIN stock_t ON s_master_t.scode = stock_t.scode "
                + " LEFT JOIN wh_stock_t ON s_master_t.scode = wh_stock_t.scode "
                + " LEFT JOIN wh_supplier_t ON s_master_t.supplier_code = wh_supplier_t.supplier_code "
                + " LEFT JOIN ( SELECT scode,SUM(amount) as amount  FROM wh_order_t "
                + " INNER JOIN wh_order_info_t ON wh_order_t.order_no = wh_order_info_t.order_no "
                + " WHERE wh_order_info_t.wh_order_status = " + (int)EnumWhOrderStatus.NotStorage + " AND wh_order_t.active = " + (int)EnumActive.Active + " GROUP BY scode ) "
                + " as wh_order_t on s_master_t.scode =wh_order_t.scode WHERE true ";
        }
    }
}
