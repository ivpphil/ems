﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.warehouse.specification
{
    public class SumShelvesSpec: SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return " SELECT sum(shelf001+shelf002+shelf003) as sumShelves  FROM wh_storage_t";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(*) AS " + countColumnAlias + " FROM wh_storage_t ";
        }
    }
}
