﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.warehouse.specification
{
    public class SearchPastOrderListSpec
         : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return "SELECT DISTINCT ON (wh_order_t.order_no, wh_order_t.supplier_code, supplier_name) wh_order_t.intime, wh_order_info_t.wh_order_status, * "
                + "FROM wh_order_t "
                + "INNER JOIN wh_order_info_t ON wh_order_t.order_no = wh_order_info_t.order_no "
                + "LEFT JOIN wh_supplier_t ON wh_order_t.supplier_code = wh_supplier_t.supplier_code "
                + "LEFT JOIN s_master_t ON wh_order_t.scode = s_master_t.scode "
                + "LEFT JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(DISTINCT (wh_order_t.order_no, wh_order_t.supplier_code, supplier_name)) AS " + countColumnAlias + " "
                + "FROM wh_order_t "
                + "INNER JOIN wh_order_info_t ON wh_order_t.order_no = wh_order_info_t.order_no "
                + "LEFT JOIN wh_supplier_t ON wh_order_t.supplier_code = wh_supplier_t.supplier_code "
                + "LEFT JOIN s_master_t ON wh_order_t.scode = s_master_t.scode "
                + "LEFT JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode ";
        }
    }
}
