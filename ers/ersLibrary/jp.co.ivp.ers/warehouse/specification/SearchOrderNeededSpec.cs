﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.warehouse.specification
{
    public class SearchOrderNeededSpec
        : SearchSpecificationBase
    {
        string orderNeed { get; set; }

        public virtual List<Dictionary<string, object>> GetSearchData(ErsWhOrderCriteria criteria)
        {
            var orderNeedCriteria = new Criteria();
            orderNeedCriteria.Add(Criteria.GetCriterion("d_master_t.intime", criteria.intime_less_than, Criteria.Operation.LESS_THAN));
            orderNeedCriteria.Add(Criteria.GetInClauseCriterion("order_status", new[] { EnumOrderStatusType.NEW_ORDER, EnumOrderStatusType.DELIVER_WAITING, EnumOrderStatusType.DELIVER_REQUEST }));
            this.orderNeed = orderNeedCriteria.GetWhere();

            orderNeedCriteria.ParamaterOnly = true;
            criteria.Add(orderNeedCriteria);

            return base.GetSearchData(criteria);
        }

        protected override string GetSearchDataSql()
        {
            var strSQL = @"
                            SELECT 
                                s_master_t.scode 
                                ,s_master_t.maker_scode 
                                ,s_master_t.sname 
                                ,s_master_t.stock_alert_amount 
                                ,s_master_t.wh_order_type 
                                ,wh_supplier_t.supplier_code 
                                ,wh_supplier_t.supplier_name 
                                ,price_t.price 
                                ,price_t.cost_price 
                                ,COALESCE(ds_master_t.amount, 0) AS amount 
                                ,stock_t.stock 
                                ,wh_stock_t.stock + COALESCE(wh_order_t.ordered_amount, 0) AS wh_stock 
                                ,wh_order_t.ordered_amount as wh_ordered_amount
                            FROM s_master_t 
                            INNER JOIN price_t ON price_t.cost_price is not null AND s_master_t.scode = price_t.scode AND price_t.price_kbn = " + (int)EnumPriceKbn.NORMAL + @" 
                            LEFT JOIN stock_t ON s_master_t.scode = stock_t.scode 
                            LEFT JOIN wh_stock_t ON s_master_t.scode = wh_stock_t.scode 
                            LEFT JOIN 
                            ( 
                                --受注数の取得
                                SELECT 
                                ds_master_t.scode 
                                ,SUM(ds_master_t.amount) AS amount 
                                FROM 
                                (
                                    ( 
                                        SELECT scode, amount 
                                        FROM ds_master_t 
                                        INNER JOIN d_master_t ON d_master_t.d_no = ds_master_t.d_no 
                                        WHERE " + this.orderNeed + @"
                                    ) 
                                    UNION ALL 
                                    ( 
                                        SELECT ds_set_t.scode, ds_set_t.amount 
                                        FROM ds_set_t 
                                        INNER JOIN ds_master_t ON ds_set_t.ds_id = ds_master_t.id 
                                        INNER JOIN d_master_t ON d_master_t.d_no = ds_master_t.d_no 
                                        WHERE " + this.orderNeed + @"
                                    ) 
                                ) AS ds_master_t 
                                GROUP BY 
                                    ds_master_t.scode 
                            ) AS ds_master_t ON s_master_t.scode = ds_master_t.scode 

                            --未入庫在庫数
                            LEFT JOIN (
                                SELECT 
	                                wh_order_t.scode, 
	                                SUM(wh_order_t.amount) - SUM(COALESCE(wh_storage_t.shelf001, 0)) AS ordered_amount
                                FROM wh_order_t 
                                INNER JOIN wh_order_info_t ON wh_order_t.order_no = wh_order_info_t.order_no
                                LEFT JOIN (
	                                SELECT order_no, scode, SUM(shelf001) AS shelf001
	                                FROM wh_storage_t
	                                WHERE active = 1
	                                GROUP BY order_no, scode
	                                ) AS wh_storage_t ON wh_order_t.order_no = wh_storage_t.order_no AND wh_order_t.scode = wh_storage_t.scode
                                WHERE wh_order_info_t.wh_order_status = " + (int)EnumWhOrderStatus.NotStorage + @"
                                GROUP BY 
	                                wh_order_t.scode
                            ) AS wh_order_t ON s_master_t.scode = wh_order_t.scode 

                            --仕入先マスタ
                            INNER JOIN wh_supplier_t ON s_master_t.supplier_code = wh_supplier_t.supplier_code 

                            --倉庫在庫＋未入庫数よりも受注数が多いものが発注対象
                            --WHERE ds_master_t.amount - (wh_stock_t.stock + COALESCE(wh_order_t.ordered_amount, 0)) > 0 
                            WHERE true
                            ";
            return strSQL;
        }

        public virtual int GetCountData(ErsWhOrderCriteria criteria)
        {
            var orderNeedCriteria = new Criteria();
            orderNeedCriteria.Add(Criteria.GetCriterion("d_master_t.intime", criteria.intime_less_than, Criteria.Operation.LESS_THAN));
            orderNeedCriteria.Add(Criteria.GetInClauseCriterion("order_status", new[] { EnumOrderStatusType.NEW_ORDER, EnumOrderStatusType.DELIVER_WAITING, EnumOrderStatusType.DELIVER_REQUEST }));
            this.orderNeed = orderNeedCriteria.GetWhere();

            orderNeedCriteria.ParamaterOnly = true;
            criteria.Add(orderNeedCriteria);

            return base.GetCountData(criteria);
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            var strSQL = @"
                            SELECT count(*) 
                            FROM s_master_t 
                            INNER JOIN price_t ON price_t.cost_price is not null AND s_master_t.scode = price_t.scode AND price_t.price_kbn = " + (int)EnumPriceKbn.NORMAL + @" 
                            LEFT JOIN stock_t ON s_master_t.scode = stock_t.scode 
                            LEFT JOIN wh_stock_t ON s_master_t.scode = wh_stock_t.scode 
                            LEFT JOIN 
                            ( 
                                --受注数の取得
                                SELECT 
                                ds_master_t.scode 
                                ,SUM(ds_master_t.amount) AS amount 
                                FROM 
                                (
                                    ( 
                                        SELECT scode, amount 
                                        FROM ds_master_t 
                                        INNER JOIN d_master_t ON d_master_t.d_no = ds_master_t.d_no 
                                        WHERE " + this.orderNeed + @"
                                    ) 
                                    UNION ALL 
                                    ( 
                                        SELECT ds_set_t.scode, ds_set_t.amount 
                                        FROM ds_set_t 
                                        INNER JOIN ds_master_t ON ds_set_t.ds_id = ds_master_t.id 
                                        INNER JOIN d_master_t ON d_master_t.d_no = ds_master_t.d_no 
                                        WHERE " + this.orderNeed + @"
                                    ) 
                                ) AS ds_master_t 
                                GROUP BY 
                                    ds_master_t.scode 
                            ) AS ds_master_t ON s_master_t.scode = ds_master_t.scode 

                            --未入庫在庫数
                            LEFT JOIN (
                                SELECT 
	                                wh_order_t.scode, 
	                                SUM(wh_order_t.amount) - SUM(COALESCE(wh_storage_t.shelf001, 0)) AS ordered_amount
                                FROM wh_order_t 
                                INNER JOIN wh_order_info_t ON wh_order_t.order_no = wh_order_info_t.order_no
                                LEFT JOIN (
	                                SELECT order_no, scode, SUM(shelf001) AS shelf001
	                                FROM wh_storage_t
	                                WHERE active = 1
	                                GROUP BY order_no, scode
	                                ) AS wh_storage_t ON wh_order_t.order_no = wh_storage_t.order_no AND wh_order_t.scode = wh_storage_t.scode
                                WHERE wh_order_info_t.wh_order_status = " + (int)EnumWhOrderStatus.NotStorage + @"
                                GROUP BY 
	                                wh_order_t.scode
                            ) AS wh_order_t ON s_master_t.scode = wh_order_t.scode 

                            --仕入先マスタ
                            INNER JOIN wh_supplier_t ON s_master_t.supplier_code = wh_supplier_t.supplier_code 

                            --倉庫在庫＋未入庫数よりも受注数が多いものが発注対象
                            --WHERE ds_master_t.amount - (wh_stock_t.stock + COALESCE(wh_order_t.ordered_amount, 0)) > 0 
                            WHERE true
                            ";
            return strSQL;
        }
    }
}
