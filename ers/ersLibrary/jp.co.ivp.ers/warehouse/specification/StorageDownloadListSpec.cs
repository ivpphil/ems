﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse.specification
{
    public class StorageDownloadListSpec : SearchSpecificationBase
    {

        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return base.GetSearchData(criteria);
        }

        protected override string GetSearchDataSql()
        {
            return "SELECT * " +
                   "FROM wh_order_t " +
                   "INNER JOIN wh_supplier_t ON wh_order_t.supplier_code = wh_supplier_t.supplier_code " +
                   "INNER JOIN s_master_t ON wh_order_t.scode = s_master_t.scode ";
        }

        public override int GetCountData(Criteria criteria)
        {
            return base.GetCountData(criteria);
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(*) " +
                   "FROM wh_order_t " +
                   "INNER JOIN wh_supplier_t ON wh_order_t.supplier_code = wh_supplier_t.supplier_code " +
                   "INNER JOIN s_master_t ON wh_order_t.scode = s_master_t.scode ";
        }

    }
}
