﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.warehouse.specification
{
    public class SearchPastOrderSpec
         : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return @"
                SELECT 
			        wh_order_t.order_no, 
			        wh_order_t.supplier_code, 
			        wh_supplier_t.supplier_name, 
			        wh_order_t.scode, 
			        s_master_t.sname, 
			        s_master_t.wh_order_type, 
			        COALESCE(price_t.cost_price, 0) AS cost_price, 
			        wh_order_t.amount, 
			        wh_order_t.up_stock, 
			        wh_order_t.intime, 
			        wh_order_info_t.wh_order_status, 
			        wh_order_info_t.remarks, 
			        COALESCE(s_master_t.maker_scode, s_master_t.scode) AS maker_scode, 
			        COALESCE(wh_storage_t.shelf001, 0) AS shelf001, 
			        COALESCE(wh_storage_t.shelf002, 0) AS shelf002, 
			        COALESCE(wh_storage_t.shelf003, 0) AS shelf003, 
			        wh_order_t.schedule_date, 
			        wh_storage_t.first_intime, 
			        wh_storage_t.last_intime
                FROM wh_order_t 
                INNER JOIN wh_order_info_t ON wh_order_t.order_no = wh_order_info_t.order_no 
                LEFT JOIN wh_supplier_t ON wh_order_t.supplier_code = wh_supplier_t.supplier_code 
                LEFT JOIN s_master_t ON wh_order_t.scode = s_master_t.scode 
                LEFT JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode 
                LEFT JOIN( 
			        SELECT scode, cost_price 
			        FROM price_t 
                    WHERE price_kbn = " + (int)EnumPriceKbn.NORMAL + @"
                ) AS price_t ON s_master_t.scode = price_t.scode 
                LEFT JOIN (
			        SELECT 
				        order_no, 
				        scode, 
				        SUM(shelf001) AS shelf001, 
				        SUM(shelf002) AS shelf002, 
				        SUM(shelf003) AS shelf003, 
				        MIN(intime) AS first_intime, 
				        MAX(intime) AS last_intime  
			        FROM wh_storage_t 
			        GROUP BY order_no, scode
		        ) AS wh_storage_t ON wh_order_t.order_no = wh_storage_t.order_no AND wh_order_t.scode = wh_storage_t.scode 
                WHERE true
                ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return @"SELECT COUNT(wh_order_t.id) AS " + countColumnAlias + @" 
                FROM wh_order_t 
                INNER JOIN wh_order_info_t ON wh_order_t.order_no = wh_order_info_t.order_no 
                LEFT JOIN wh_supplier_t ON wh_order_t.supplier_code = wh_supplier_t.supplier_code 
                LEFT JOIN s_master_t ON wh_order_t.scode = s_master_t.scode 
                LEFT JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode 
                LEFT JOIN( 
			        SELECT scode, cost_price 
			        FROM price_t 
                    WHERE price_kbn = " + (int)EnumPriceKbn.NORMAL + @"
                ) AS price_t ON s_master_t.scode = price_t.scode 
                LEFT JOIN (
			        SELECT 
				        order_no, 
				        scode, 
				        SUM(shelf001) AS shelf001, 
				        SUM(shelf002) AS shelf002, 
				        SUM(shelf003) AS shelf003, 
				        MIN(intime) AS first_intime, 
				        MAX(intime) AS last_intime  
			        FROM wh_storage_t 
			        GROUP BY order_no, scode
		        ) AS wh_storage_t ON wh_order_t.order_no = wh_storage_t.order_no AND wh_order_t.scode = wh_storage_t.scode 
                WHERE true
                ";
        }
    }
}
