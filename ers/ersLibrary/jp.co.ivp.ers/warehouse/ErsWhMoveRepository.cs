﻿using jp.co.ivp.ers.mvc;
using System.Collections.Generic;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhMoveRepository : ErsRepository<ErsWhMove>
    {

        public ErsWhMoveRepository()
            : base("wh_move_t")
        {
        }

        public IEnumerable<Dictionary<string, object>> FindWhMoveList(ErsWhMoveCriteria criteria)
        {
            var spec = ErsFactory.ersWarehouseFactory.GetSearchWhMoveSpec();
            return spec.GetSearchData(criteria);
        }

        public long GetWhMoveListCount(ErsWhMoveCriteria criteria)
        {
            var spec = ErsFactory.ersWarehouseFactory.GetSearchWhMoveSpec();
            return spec.GetCountData(criteria);
        }
    }
}
