﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.warehouse
{
    public class ErsWhSupplier
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string supplier_code { get; set; }

        public string supplier_name { get; set; }

        public int? pref { get; set; }

        public string zip { get; set; }

        public string address { get; set; }

        public string tel { get; set; }

        public string fax { get; set; }

        public string email { get; set; }

        public DateTime? intime { get; set; }

        public DateTime? utime { get; set; }

        public EnumActive? active { get; set; }

        public string w_pref { get; set; }
    }
}
