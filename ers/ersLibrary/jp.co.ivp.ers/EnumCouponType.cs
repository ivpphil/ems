﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumCouponType
        : short
    {
        /// <summary>
        /// 0: ワンユーザー
        /// </summary>
        OneUser,
        /// <summary>
        /// 1: ワンタイム
        /// </summary>
        OneTime
    }
}
