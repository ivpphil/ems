﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;

namespace jp.co.ivp.ers.coupon
{
    public class ErsCouponFactory
    {

        public ErsCouponRepository GetErsCouponRepository()
        {
            return new ErsCouponRepository();
        }

        public ErsCouponCriteria GetErsCouponCriteria()
        {
            return new ErsCouponCriteria();
        }

        public virtual ErsCoupon GetErsCoupon()
        {
            return new ErsCoupon();
        }

        public virtual ErsValidateCouponStgy GetErsValidateCouponStgy()
        {
            return new ErsValidateCouponStgy();
        }

        public virtual CouponRegistComputeStgy GetCouponRegistComputeStgy()
        {
            return new CouponRegistComputeStgy();
        }

        public virtual ErsCoupon GetErsCouponWithParameter(Dictionary<string, object> parameter)
        {
            var coupon = this.GetErsCoupon();
            coupon.OverwriteWithParameter(parameter);
            return coupon;
        }

        public virtual ErsCoupon GetErsCouponWithId(int id)
        {
            var repository = this.GetErsCouponRepository();
            var criteria = this.GetErsCouponCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);
            return list[0];
        }

        public virtual ErsCoupon GetErsCouponWithCouponCode(string coupon_code, int? site_id = null)
        {
            if (string.IsNullOrEmpty(coupon_code))
            {
                return null;
            }

            var repository = this.GetErsCouponRepository();
            var criteria = this.GetErsCouponCriteria();
            criteria.coupon_code = coupon_code;
            if (site_id.HasValue)
            {
                criteria.site_id = site_id;
            }

            var list = repository.Find(criteria);
            if (list.Count != 0)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }
    }
}
