﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.coupon
{
    public class ErsCouponCriteria : Criteria
	{
       
        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.id", value, Operation.EQUAL));
            }
        }

        public int? id_not_equal
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.id", value, Operation.NOT_EQUAL));
            }
        }

        public string coupon_code
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.coupon_code", value.ToUpper(), Operation.EQUAL));
            }
        }

        public EnumCouponType? coupon_type
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.coupon_type", value, Operation.EQUAL));
            }
        }

        public int price
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.price", value, Operation.EQUAL));
            }
        }

        public int base_price
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.base_price", value, Operation.EQUAL));
            }
        }

        public DateTime ? start_date
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.start_date", value, Operation.EQUAL));
            }
        }

        public DateTime? end_date
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.end_date", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.active", value, Operation.EQUAL));
            }
        }

        public string mcode
        {
            set
            {
                Add(Criteria.GetCriterion("member_t.mcode", value, Operation.EQUAL));
            }
        }


        public int? isGreaterThanBasePrice
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.base_price", value, Operation.LESS_THAN));
            }
        }

        /// <summary>
        /// Added by Sean
        /// </summary>
        /// <param name="orderBy"></param>
        public void SetOrderByProcessID(OrderBy orderBy)
        {
            AddOrderBy("coupon_t.id", orderBy);
        }


        public DateTime? isGreaterEqualIntime
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.intime", value, Operation.GREATER_EQUAL));
            }
        }

        public DateTime? isLessThanEqualIntime
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.intime", value, Operation.LESS_EQUAL));
            }
        }
        public int? isGreaterEqualPrice
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.Price", value, Operation.GREATER_EQUAL));
            }
        }
        public int? isLessThanEqualPrice
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.Price", value, Operation.LESS_EQUAL));
            }
        }

        public int? isGreaterThanEqualBasePrice
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.base_price", value, Operation.GREATER_EQUAL));
            }
        }
        public int? islessThanEqualBasePrice
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.base_price", value, Operation.LESS_EQUAL));
            }
        }
        public DateTime? isGreaterThanEqualStartDate
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.start_date", value, Operation.GREATER_EQUAL));
            }
        }
        public DateTime? islessThanEqualStartDate
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.start_date", value, Operation.LESS_EQUAL));
            }
        }
        public DateTime? isGreaterThanEqualEndDate
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.end_date", value, Operation.GREATER_EQUAL));
            }
        }
        public DateTime? islessThanEqualEndDate
        {
            set
            {
                Add(Criteria.GetCriterion("coupon_t.end_date", value, Operation.LESS_EQUAL));
            }
        }

        public int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("coupon_t.site_id", value, Operation.ANY_EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("coupon_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.ANY_EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}
