﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using System.Data;
using jp.co.ivp.ers.state;
using System.Collections;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.coupon;


namespace jp.co.ivp.ers.coupon
{
    public class ErsCouponRepository
        : ErsRepository<ErsCoupon>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsCouponRepository()
            : base("coupon_t")
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsCouponRepository(ErsDatabase objDB)
            : base("coupon_t", objDB)
        {
        }
    }
}
