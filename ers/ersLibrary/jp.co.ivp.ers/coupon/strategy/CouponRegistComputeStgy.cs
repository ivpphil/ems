﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.coupon
{
    public class CouponRegistComputeStgy
    {
        /// <summary>
        /// 購入直前クーポン按分処理
        /// </summary>
        /// <param name="order"></param>
        public void RegistCompute(ErsOrderIntegrated order)
        {
            if (!order.coupon_code.HasValue())
            {
                return;
            }

            var coupon_code = order.coupon_code.ToUpper();     //クーポンコード(大文字にする)
            var couponObj = ErsFactory.ersCouponFactory.GetErsCouponWithCouponCode(coupon_code);
            if (couponObj == null)
            {
                return;
            }

            //伝票総値引き額
            var coupon_discount = couponObj.price.Value;

            order.coupon_discount = coupon_discount;

            int innerCoupon_discount = coupon_discount;

            foreach (var innerOrderContainer in order.GetListOrder())
            {
                var innerOrder = innerOrderContainer.OrderHeader;

                //各伝票に反映
                innerOrder.coupon_code = coupon_code;
                if (innerOrder.total >= innerCoupon_discount)
                {
                    innerOrder.coupon_discount = innerCoupon_discount;
                    innerOrder.total -= innerCoupon_discount;
                    innerCoupon_discount = 0;
                }
                else
                {
                    innerCoupon_discount -= innerOrder.total;
                    innerOrder.coupon_discount = innerOrder.total;
                    innerOrder.total = 0;
                }

                if (innerOrder.coupon_discount == 0)
                {
                    innerOrder.coupon_code = null;
                }
            }
        }
    }
}
