﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.coupon
{
    /// <summary>
    /// Checks Whether the member already exists.
    /// </summary>
    public class ErsValidateCouponStgy
    {
        /// <summary>
        /// Return true if Basket is empty.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public virtual ValidationResult Validate(string mcode, string ent_coupon_code, int total, string d_no, bool isUpdate, int? site_id = null)
        {
            if (string.IsNullOrEmpty(ent_coupon_code))
                return null;

            //クーポン有効、存在チェック
            var objCoupon = ErsFactory.ersCouponFactory.GetErsCouponWithCouponCode(ent_coupon_code, site_id);
            if (objCoupon == null || objCoupon.active != EnumActive.Active)
            {
                return new ValidationResult(ErsResources.GetMessage("20214"), new[] { "ent_coupon_code" });
            }

            //クーポン期限チェック
            if (objCoupon.start_date > DateTime.Now || objCoupon.end_date < DateTime.Now)
            {
                return new ValidationResult(ErsResources.GetMessage("20213"), new[] { "ent_coupon_code" });
            }

            if (!isUpdate)
            {
                //get tax of an order (subtotal+tax)
                var tax = ErsFactory.ersOrderFactory.GetErsCalcService().calcTax(total);
                total += tax;

                //最低使用金額チェック
                if ((objCoupon.base_price > total))
                {
                    return new ValidationResult(ErsResources.GetMessage("20215", objCoupon.base_price), new[] { "ent_coupon_code" });
                }
            }

            if (objCoupon.coupon_type == EnumCouponType.OneUser)
            {
                //ワンユーザークーポンチェック
                if (!CkCouponOneUser(ent_coupon_code, d_no))
                {
                    return new ValidationResult(ErsResources.GetMessage("20216"), new[] { "ent_coupon_code" });
                }
            }
            else
            {
                //ワンタイムクーポンチェック
                if (!CkCouponOneTime(ent_coupon_code, mcode, d_no))
                {
                    return new ValidationResult(ErsResources.GetMessage("20216"), new[] { "ent_coupon_code" });
                }
            }

            return null;
        }

        /// <summary>
        ///ワンユーザークーポンチェック
        /// </summary>
        /// <param name="mcode"></param>
        /// <param name="coupon_code"></param>
        /// <returns></returns>
        private Boolean CkCouponOneUser(string coupon_code, string d_no)
        {
            var orderRepo = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var orderCriteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            orderCriteria.coupon_code = coupon_code;
            orderCriteria.base_d_no_not_equals = d_no;
            orderCriteria.order_status_not_in = orderCriteria.CancelStatusArray;

            var count = orderRepo.GetRecordCount(orderCriteria);

            return (count == 0);
        }

        /// <summary>
        ///ワンタイムクーポンチェック
        /// </summary>
        /// <param name="mcode"></param>
        /// <param name="coupon_code"></param>
        /// <returns></returns>
        private Boolean CkCouponOneTime(string coupon_code, string mcode, string d_no)
        {
            var orderRepo = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var orderCriteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            orderCriteria.coupon_code = coupon_code;
            orderCriteria.mcode = mcode;
            orderCriteria.base_d_no_not_equals = d_no;
            orderCriteria.order_status_not_in = orderCriteria.CancelStatusArray;

            var count = orderRepo.GetRecordCount(orderCriteria);

            return (count == 0);
        }
    }
}
