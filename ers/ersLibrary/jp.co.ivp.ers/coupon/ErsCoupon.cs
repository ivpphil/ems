﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.coupon
{
    public class ErsCoupon
        : ErsRepositoryEntity
    {

        public override int? id { get; set; }
        public virtual string coupon_code
        {
            get
            {
                if (string.IsNullOrEmpty(this._coupon_code))
                {
                    return this._coupon_code;
                }
                return this._coupon_code.ToUpper();
            }
            set
            {
                this._coupon_code = value;
            }
        }
        private string _coupon_code;

        public virtual EnumCouponType? coupon_type { get; set; }

        public string _coupon_type
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService()
                    .GetStringFromId(EnumCommonNameType.CouponType, EnumCommonNameColumnName.namename, (int?)coupon_type);
            }
        }


        public virtual int? price { get; set; }
        public virtual int? base_price { get; set; }
        public virtual DateTime? start_date { get; set; }
        public virtual DateTime? end_date { get; set; }
        public virtual EnumActive? active { get; set; }
        public string _active
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService()
                    .GetStringFromId(EnumCommonNameType.Active, EnumCommonNameColumnName.namename, (int?)active);
            }
        }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }

        public virtual int[] site_id { get; set; }
    }
}
