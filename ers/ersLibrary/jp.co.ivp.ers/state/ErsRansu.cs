﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.state
{
    /// <summary>
    /// Class for ErsRansu
    /// </summary>
    public class ErsRansu
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string mcode { get; set; }
        public string ssl_ransu { get; set; }
        public string ransu { get; set; }
        public int? site_id { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
    }
}
