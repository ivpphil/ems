﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.state
{
    public class ErsCacheCriteria
        : Criteria
    {
        public string ransu
        {
            set
            {
                this.Add(Criteria.GetCriterion("cache_t.ransu", value, Criteria.Operation.EQUAL));
            }
        }

        public string key
        {
            set
            {
                this.Add(Criteria.GetCriterion("cache_t.key", value, Criteria.Operation.EQUAL));
            }
        }
    }
}
