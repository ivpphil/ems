﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state
{
    public class ErsRansuContactCriteria
        : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("ransu_contact_t.id", value, Operation.EQUAL));
            }
        }

        public void SetCheckDuplicate(string ransu)
        {
            this.Add(Criteria.JoinWithOR(new[]{
                    Criteria.GetCriterion("ransu_contact_t.ransu", ransu, Criteria.Operation.EQUAL),
                    Criteria.GetCriterion("ransu_contact_t.ssl_ransu", ransu, Criteria.Operation.EQUAL)}));
        }

        public string ransu
        {
            set
            {
                this.Add(Criteria.GetCriterion("ransu_contact_t.ransu", value, Operation.EQUAL));
            }
        }

        internal void SetActiveOnly()
        {
            int ransuExpiration = ErsFactory.ersUtilityFactory.getSetup().ransu_expiration;

            var dictionary = new Dictionary<string, object>();
            dictionary.Add("ransuExpiration", ransuExpiration);
            this.Add(Criteria.GetUniversalCriterion("ransu_contact_t.utime > current_timestamp - (:ransuExpiration || ' minute')::interval", dictionary));
        }

        public string ssl_ransu
        {
            set
            {
                this.Add(Criteria.GetCriterion("ransu_contact_t.ssl_ransu", value, Operation.EQUAL));
            }
        }

        public string cts_user_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("ransu_contact_t.cts_user_id", value, Operation.EQUAL));
            }
        }

        public DateTime utime_less_than
        {
            set
            {
                this.Add(Criteria.GetCriterion("ransu_contact_t.utime", value, Criteria.Operation.LESS_THAN));
            }
        }
    }
}
