﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state
{
    public class ErsPassRansuRepository
        : ErsRepository<ErsPassRansu>
    {
        public ErsPassRansuRepository()
            : base("pass_ransu_t")
        {
        }

        public override void Insert(ErsPassRansu obj, bool storeNewIdToObject = false)
        {
            var repository = ErsFactory.ersSessionStateFactory.GetErsPassRansuRepository();
            var cri = ErsFactory.ersSessionStateFactory.GetErsPassRansuCriteria();
            var list = repository.Find(cri);
                if (list.Count() > 0)
                {
                    var ListId = list.Select(x => x.id).OrderBy(y => y);
                    obj.id = ListId.Last() + 1;
                }
                else
                {
                    obj.id = 1;
                }
         
            base.Insert(obj, storeNewIdToObject);
        }
    }
}
