﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.Threading;

namespace jp.co.ivp.ers.state.strategy
{
    /// <summary>
    /// Class for Generating Ransu
    /// Use Generate method.
    /// </summary>
    public class GenerateRansuStringStgy
    {
        /// <summary>
        /// 新しい乱数文字列生成
        /// </summary>
        /// <returns name="sb">Generated string.</returns>
        public virtual string Generate()
        {
            StringBuilder sb = new StringBuilder(ransuLength - 1);
            Random r = new Random();

            for (int i = 0; i < ransuLength; i++)
            {
                //文字の位置をランダムに選択
                int pos = r.Next(passwordChars.Length);
                //選択された位置の文字を取得
                char c = passwordChars[pos];
                //パスワードに追加
                sb.Append(c);
            }

            return VBStrings.Right(sb.ToString() + Thread.CurrentThread.ManagedThreadId, ransuLength);
        }

        /// <summary>
        /// Gets length of random characters based on setup.config.
        /// </summary>
        protected virtual int ransuLength
        {
            get
            {
                return ErsFactory.ersUtilityFactory.getSetup().ransu_length;
            }
        }

        /// <summary>
        /// Gets password random characters based on the setup.config.
        /// </summary>
        protected virtual string passwordChars
        {
            get
            {
                return ErsFactory.ersUtilityFactory.getSetup().ransu_chars;
            }
        }
    }
}
