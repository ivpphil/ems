﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.ctsorder;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state.strategy
{
    /// <summary>
    /// Provides view service to get list or value of random characters from ransu_t and ransu_admin_t table. 
    /// </summary>
    public class RansuDeleteStgy
    {
        private string[] IgnorRansuList { get; set; }

        public virtual void MakeIgnorRansuList()
        {
            //仮受注の一覧取得
            var ctsRepository = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderRepository();
            var ctsCriteria = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderCriteria();
            ctsCriteria.active = EnumActive.Active;
            var ctsorder = ctsRepository.Find(ctsCriteria);
            IgnorRansuList = new string[ctsorder.Count];
            int i = 0;
            foreach (var ctsdata in ctsorder)
            {
                IgnorRansuList[i] = ctsdata.ransu;
                i += 1;
            }
        }

        /// <summary>
        /// Deletes expired ransu (random characters)
        /// </summary>
        public virtual void DeleteExpiredRansu()
        {
            var objDB = ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(ErsWebDatabaseFactory.GetCloudConnectionStrings());
            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuRepository(objDB);
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuCriteria();
            criteria.utime_less_than = DateTime.Now.AddHours(-1);
            var list = repository.Find(criteria);

            var basketRepository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();

            foreach (var record in list)
            {
                //仮受注のバスケットは削除対象外とする
                if (IgnorRansuList.Contains(record.ransu) == false)
                {
                    var basketCriteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
                    basketCriteria.ransu = record.ransu;

                    basketRepository.Delete(basketCriteria);
                }
            }

            repository.Delete(criteria);
        }

        /// <summary>
        /// Deletes expired admin ransu
        /// </summary>
        public virtual void DeleteExpiredAdminRansu()
        {
            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuAdminRepository();
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuAdminCriteria();
            criteria.utime_less_than = DateTime.Now.AddHours(-1);
            repository.Delete(criteria);
        }

        public virtual void DeleteExpiredContactRansu()
        {
            //delete an expired ransu.
            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuContactRepository();
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuContactCriteria();
            criteria.utime_less_than = DateTime.Now.AddHours(-1);
            var list = repository.Find(criteria);

            var basketRepository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();

            foreach (var record in list)
            {
                //仮受注のバスケットは削除対象外とする
                if (!IgnorRansuList.Contains(record.ransu))
                {
                    var basketCriteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
                    basketCriteria.ransu = record.ransu;

                    basketRepository.Delete(basketCriteria);
                }
            }
            repository.Delete(criteria);
        }

    }
}
