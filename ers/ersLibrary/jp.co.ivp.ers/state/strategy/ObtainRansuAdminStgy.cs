﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.state.strategy
{
    public class ObtainRansuAdminStgy
    {
        /// <summary>
        /// Issue a new session of random characters using ErsDB_ransu_admin_t.
        /// </summary>
        public virtual void TryGetNewRansu()
        {
            var admin_ransu = CreateNewRansu();
            ErsContext.sessionState.Add("admin_ransu", admin_ransu);

            var objDB = ErsCommonsSetting.ersDatabaseFactory.GetNewErsDatabase();

            var admin_ssl_ransu = ErsContext.sessionState.Get("admin_ssl_ransu");
            var user_cd = ErsContext.sessionState.Get("user_cd");
            if (string.IsNullOrEmpty(user_cd))
            {
                user_cd = ErsAdministrator.DEFAULT_USER_CODE;
            }
            EnumUserState state = ErsFactory.ersSessionStateFactory.GetUserStateAdminSpecification().getUserState(admin_ransu, admin_ssl_ransu, user_cd);

            //ログイン済みの場合は、SSL乱数は保持する。
            if (state != EnumUserState.LOGIN)
            {
                var repository = ErsFactory.ersSessionStateFactory.GetErsRansuAdminRepository();
                var objRansu = ErsFactory.ersSessionStateFactory.GetErsRansuAdmin();
                objRansu.ransu = admin_ransu;
                objRansu.user_cd = ErsAdministrator.DEFAULT_USER_CODE;
                repository.Insert(objRansu);
            }
            else
            {
                var repository = ErsFactory.ersSessionStateFactory.GetErsRansuAdminRepository();
                var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuAdminCriteria();
                criteria.ssl_ransu = admin_ssl_ransu;
                criteria.user_cd = user_cd;
                var listRansu = repository.Find(criteria);
                if (listRansu.Count == 0)
                {
                    return;
                }
                if (listRansu.Count != 1)
                {
                    throw new Exception(ErsResources.GetMessage("duplicated_ransu_msg"));
                }
                var oldRansu = listRansu.First();
                var newRnasu = ErsFactory.ersSessionStateFactory.GetErsRansuAdminWithId(oldRansu.id);
                newRnasu.ransu = admin_ransu;
                repository.Update(oldRansu, newRnasu);
            }
        }

        /// <summary>
        /// Issue a new secure session of random characters.
        /// </summary>
        /// <param name="code">user code</param>
        public virtual void TryGetNewSSLransu(string user_cd)
        {
            if (user_cd == ErsAdministrator.DEFAULT_USER_CODE)
            {
                throw new Exception("ユーザーコードがセットされていない。");
            }

            this.TryGetNewRansu();

            var admin_ssl_ransu = CreateNewRansu();
            ErsContext.sessionState.Add("admin_ssl_ransu", admin_ssl_ransu);

            ErsContext.sessionState.Add("user_cd", user_cd);

            var admin_ransu = ErsContext.sessionState.Get("admin_ransu");

            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuAdminRepository();
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuAdminCriteria();
            criteria.ransu = admin_ransu;
            criteria.user_cd = ErsAdministrator.DEFAULT_USER_CODE;
            var listRansu = repository.Find(criteria);
            if (listRansu.Count == 0)
            {
                return;
            }
            if (listRansu.Count != 1)
            {
                throw new Exception(ErsResources.GetMessage("duplicated_ransu_msg"));
            }
            var oldRansu = listRansu.First();
            var newRnasu = ErsFactory.ersSessionStateFactory.GetErsRansuAdminWithId(oldRansu.id);
            newRnasu.ssl_ransu = admin_ssl_ransu;
            newRnasu.user_cd = user_cd;
            repository.Update(oldRansu, newRnasu);
        }

        /// <summary>
        /// 新しい乱数文字列取得
        /// </summary>
        /// <returns>Returns newly created random characters using Generate method from GenerateRansuStringStgy</returns>
        protected virtual string CreateNewRansu()
        {
            int stopper = 0;
            var checkRansuDupulicateCountSpec = ErsFactory.ersSessionStateFactory.GetCheckRansuAdminDupulicateSpec();
            while (true)
            {
                stopper++;
                if (stopper > 1000)
                {
                    throw new Exception("重複した乱数が生成されました。");
                }

                string ransu = ErsFactory.ersSessionStateFactory.GetGenerateRansuStringStgy().Generate();

                //重複をチェック
                if (checkRansuDupulicateCountSpec.IsSatisfy(ransu))
                {
                    return ransu;
                }
            }

        }

        /// <summary>
        /// Method Call Retry Action Method
        /// </summary>
        /// <param name="mcode"></param>
        public virtual void GetNewRansu()
        {
            if (!this.RetryActionMethod(() => this.TryGetNewRansu()))
            {
                throw new Exception(ErsResources.GetMessage("duplicated_ransu_msg"));
            }

        }

        /// <summary>
        /// Method Call Retry Action Method
        /// </summary>
        /// <param name="mcode"></param>
        public virtual void GetNewSSLRansu(string user_cd)
        {
            if (!this.RetryActionMethod(() => this.TryGetNewSSLransu(user_cd)))
            {
                throw new Exception(ErsResources.GetMessage("duplicated_ransu_msg"));
            }
        }

        /// <summary>
        /// Retry Action Method for New Ransu and New SSLransu
        /// </summary>
        /// <param name="mcode"></param>
        protected bool RetryActionMethod(Action actionMethod)
        {
            int tryCounter = 0;

            while (tryCounter <= 100)
            {
                try
                {
                    actionMethod();
                    return true;
                }
                catch (ErsDatabaseException e)
                {
                    if (e.Code == ErsFactory.ersUtilityFactory.getSetup().duplicated_ransu_error_code)
                    {
                        tryCounter++;
                        continue;
                    }

                    throw e;
                }
            }

            return false;
        }
    }
}
