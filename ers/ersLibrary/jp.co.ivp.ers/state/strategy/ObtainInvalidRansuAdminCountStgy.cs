﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state.strategy
{
    public class ObtainInvalidRansuAdminCountStgy
        : ISpecificationForSQL
    {
        public int Obtain(string ransu, string user_cd)
        {
            //乱数の期限チェックついでに更新時間をUPDATEする。
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuAdminCriteria();
            criteria.user_cd = user_cd;
            criteria.ransu = ransu;
            criteria.SetActiveOnly();
            return ErsRepository.UpdateSatisfying(this, criteria);
        }

        public string asSQL()
        {
            return "UPDATE ransu_admin_t SET utime = current_timestamp ";
        }
    }
}
