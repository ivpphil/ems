﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.cts_operators;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.state.strategy
{
    public class ObtainRansuContactStgy
    {
        public void TryGetNewRansu()
        {
            EnumUserState state = ErsFactory.ersSessionStateFactory.GetUserStateContactSpecification().getUserState();

            var contact_ransu = CreateNewRansu();
            ErsContext.sessionState.Add("contact_ransu", contact_ransu);

            //ログイン済みの場合は、SSL乱数は保持する。
            if (state != EnumUserState.LOGIN)
            {
                var repository = ErsFactory.ersSessionStateFactory.GetErsRansuContactRepository();
                var objRansu = ErsFactory.ersSessionStateFactory.GetErsRansuContact();
                objRansu.ransu = contact_ransu;
                objRansu.cts_user_id = ErsCtsOperator.DEFAULT_USER_ID;
                objRansu.cts_authority = ErsCtsOperator.DEFAULT_USER_AUTHORITY;
                repository.Insert(objRansu);
            }
            else
            {
                var contact_ssl_ransu = ErsContext.sessionState.Get("contact_ssl_ransu");
                var cts_user_id = ErsContext.sessionState.Get("cts_user_id");
                if (string.IsNullOrEmpty(cts_user_id))
                {
                    //未ログイン会員はゼロ
                    cts_user_id = ErsCtsOperator.DEFAULT_USER_ID;
                }

                var repository = ErsFactory.ersSessionStateFactory.GetErsRansuContactRepository();
                var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuContactCriteria();
                criteria.ssl_ransu = contact_ssl_ransu;
                criteria.cts_user_id = cts_user_id;
                var listRansu = repository.Find(criteria);
                if (listRansu.Count == 0)
                {
                    return;
                }
                if (listRansu.Count != 1)
                {
                    throw new Exception(ErsResources.GetMessage("duplicated_ransu_msg"));
                }
                var oldRansu = listRansu.First();
                var newRnasu = ErsFactory.ersSessionStateFactory.GetErsRansuContactWithId(oldRansu.id);
                newRnasu.ransu = contact_ransu;
                repository.Update(oldRansu, newRnasu);
            }
        }

        public void TryGetNewSSLransu(string cts_user_id, string cts_authority)
        {
            EnumUserState state = ErsFactory.ersSessionStateFactory.GetUserStateContactSpecification().getUserState();

            if (cts_user_id == ErsCtsOperator.DEFAULT_USER_ID)
            {
                throw new Exception("会員コードがセットされていない。");
            }

            if (state == EnumUserState.LOGIN)
            {
                throw new Exception("既にログイン済み");
            }

            //乱数も発行
            if (state != EnumUserState.ASSIGNED_RANSU)
            {
                TryGetNewRansu();
            }

            var contact_ssl_ransu = CreateNewRansu();
            ErsContext.sessionState.Add("contact_ssl_ransu", contact_ssl_ransu);

            ErsContext.sessionState.Add("cts_user_id", cts_user_id);
            ErsContext.sessionState.Add("cts_authority", cts_authority);

            var contact_ransu = ErsContext.sessionState.Get("contact_ransu");

            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuContactRepository();
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuContactCriteria();
            criteria.ransu = contact_ransu;
            var listRansu = repository.Find(criteria);
            if (listRansu.Count == 0)
            {
                return;
            }
            if (listRansu.Count != 1)
            {
                throw new Exception(ErsResources.GetMessage("duplicated_ransu_msg"));
            }
            var oldRansu = listRansu.First();
            var newRnasu = ErsFactory.ersSessionStateFactory.GetErsRansuContactWithId(oldRansu.id);
            newRnasu.ssl_ransu = contact_ssl_ransu;
            newRnasu.cts_user_id = cts_user_id;
            newRnasu.cts_authority = cts_authority;

            repository.Update(oldRansu, newRnasu);
        }

        /// <summary>
        /// 新しい乱数文字列取得
        /// </summary>
        /// <returns></returns>
        protected string CreateNewRansu()
        {
            int stopper = 0;
            var checkRansuDupulicateCountSpec = ErsFactory.ersSessionStateFactory.GetCheckRansuContactDupulicateSpec();
            while (true)
            {
                stopper++;
                if (stopper > 1000)
                {
                    throw new Exception("重複した乱数が生成されました。");
                }

                string ransu = ErsFactory.ersSessionStateFactory.GetGenerateRansuStringStgy().Generate();

                //重複をチェック
                if (checkRansuDupulicateCountSpec.IsSatisfy(ransu))
                {
                    return ransu;
                }
            }
        }

        /// <summary>
        /// Method Call Retry Action Method
        /// </summary>
        /// <param name="mcode"></param>
        public virtual void GetNewRansu()
        {
            if (!this.RetryActionMethod(() => this.TryGetNewRansu()))
            {
                throw new Exception(ErsResources.GetMessage("duplicated_ransu_msg"));
            }

        }

        /// <summary>
        /// Method Call Retry Action Method
        /// </summary>
        /// <param name="mcode"></param>
        public virtual void GetNewSSLRansu(string cts_user_id, string cts_authority)
        {

            if (!this.RetryActionMethod(() => this.TryGetNewSSLransu(cts_user_id, cts_authority)))
            {
                throw new Exception(ErsResources.GetMessage("duplicated_ransu_msg"));
            }
        }

        /// <summary>
        /// Retry Action Method for New Ransu and New SSLransu
        /// </summary>
        /// <param name="mcode"></param>
        protected bool RetryActionMethod(Action actionMethod)
        {
            int tryCounter = 0;

            while (tryCounter <= 100)
            {
                try
                {
                    actionMethod();
                    return true;
                }
                catch (ErsDatabaseException e)
                {
                    if (e.Code == ErsFactory.ersUtilityFactory.getSetup().duplicated_ransu_error_code)
                    {
                        tryCounter++;
                        continue;
                    }

                    throw e;
                }
            }

            return false;
        }
    }
}
