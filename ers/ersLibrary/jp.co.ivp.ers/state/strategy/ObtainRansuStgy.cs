﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.state.strategy
{
    public class ObtainRansuStgy
    {
        /// <summary>
        /// Issue a new session of random characters using ErsDB_ransu_t.
        /// </summary>
        public virtual void TryGetNewRansu()
        {
            EnumUserState state = ErsFactory.ersSessionStateFactory.GetUserStateSpecification().getUserState();

            var ransu = CreateNewRansu();
            ErsContext.sessionState.Add("ransu", ransu);

            var objDB = ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(ErsWebDatabaseFactory.GetCloudConnectionStrings());
            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuRepository(objDB);
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //ログイン済みの場合は、SSL乱数は保持する。
            if (state != EnumUserState.LOGIN)
            {
                var objRansu = ErsFactory.ersSessionStateFactory.GetErsRansu();

                Dictionary<string, object> dic = new Dictionary<string, object>();
                objRansu.ransu = ransu;
                objRansu.mcode = ErsMember.DEFAUTL_MCODE;
                if(setup.Multiple_sites == true){
                    objRansu.site_id = setup.site_id;
                }
                repository.Insert(objRansu);
            }
            else
            {
                var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuCriteria();
                criteria.ssl_ransu = ErsContext.sessionState.Get("ssl_ransu");
                var mcode = ErsContext.sessionState.Get("mcode");
                if (string.IsNullOrEmpty(mcode))
                {
                    mcode = ErsMember.DEFAUTL_MCODE;
                }
                criteria.mcode = mcode;
                if (setup.Multiple_sites == true)
                {
                    criteria.site_id = setup.site_id;
                }

                var listRansu = repository.Find(criteria);
                if (listRansu.Count != 1)
                {
                    throw new Exception(ErsResources.GetMessage("duplicated_ransu_msg"));
                }

                var oldRnasu = listRansu.First();
                var newRansu = ErsFactory.ersSessionStateFactory.GetErsRansuWithId(oldRnasu.id);
                newRansu.ransu = ransu;
                repository.Update(oldRnasu, newRansu);
            }
        }

        /// <summary>
        /// Issue a new secure session of random characters for SSL.
        /// </summary>
        /// <param name="code">member code</param>
        public virtual void TryGetNewSSLransu(string mcode)
        {
            EnumUserState state = ErsFactory.ersSessionStateFactory.GetUserStateSpecification().getUserState();

            if (mcode == ErsMember.DEFAUTL_MCODE)
            {
                throw new Exception("会員コードがセットされていない。");
            }

            if (state == EnumUserState.LOGIN)
            {
                //already login
                return;
            }

            //乱数も発行
            if (state != EnumUserState.ASSIGNED_RANSU)
            {
                TryGetNewRansu();
            }

            var ssl_ransu = CreateNewRansu();
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var enc = ErsFactory.ersUtilityFactory.getErsEncryption();
            var encode_ransu = enc.HexEncode(ssl_ransu);

            var encode_mcode = enc.HexEncode(mcode);

            var ransu = ErsContext.sessionState.Get("ransu");

            var objDB = ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(ErsWebDatabaseFactory.GetCloudConnectionStrings());
            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuRepository(objDB);
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuCriteria();
            criteria.ransu = ransu;
            if (setup.Multiple_sites == true)
            {
                criteria.site_id = setup.site_id;
            }
            var listObj = repository.Find(criteria);
            if (listObj.Count != 1)
            {
                throw new Exception(ErsResources.GetMessage("duplicated_ransu_msg"));
            }
            var oldRansu = listObj.First();
            var newRansu = ErsFactory.ersSessionStateFactory.GetErsRansuWithId(oldRansu.id);
            newRansu.ssl_ransu  = ssl_ransu;
            newRansu.mcode = mcode;
            repository.Update(oldRansu, newRansu);

            ErsContext.sessionState.Add("ssl_ransu", ssl_ransu);
            ErsContext.sessionState.Add("mcode", mcode);
            ErsContext.sessionState.Add("encode_ransu", encode_ransu);
            ErsContext.sessionState.Add("encode_mcode", encode_mcode);
        }

        /// <summary>
        /// 新しい乱数文字列取得
        /// </summary>
        /// <returns>Returns created random characters.</returns>
        public virtual string CreateNewRansu()
        {
            int stopper = 0;
            var checkRansuDupulicateCountSpec = ErsFactory.ersSessionStateFactory.GetCheckRansuDupulicateSpec();
            while (true)
            {
                stopper++;
                if (stopper > 1000)
                {
                    throw new Exception("重複した乱数が生成されました。");
                }

                string ransu = ErsFactory.ersSessionStateFactory.GetGenerateRansuStringStgy().Generate();

                //重複をチェック
                if (checkRansuDupulicateCountSpec.IsSatisfy(ransu))
                {
                    return ransu;
                }
            }
        }

        /// <summary>
        /// Method Call Retry Action Method
        /// </summary>
        /// <param name="mcode"></param>
        public virtual void GetNewRansu()
        {
            if (!this.RetryActionMethod(() => this.TryGetNewRansu()))
            {
                throw new Exception(ErsResources.GetMessage("duplicated_ransu_msg"));
            }
        }

        /// <summary>
        /// Method Call Retry Action Method
        /// </summary>
        /// <param name="mcode"></param>
        public virtual void GetNewSSLRansu(string mcode)
        {

            if (!this.RetryActionMethod(() => this.TryGetNewSSLransu(mcode)))
            {
                throw new Exception(ErsResources.GetMessage("duplicated_ransu_msg"));
            }
        }

        /// <summary>
        /// Retry Action Method for New Ransu and New SSLransu
        /// </summary>
        /// <param name="mcode"></param>
        protected bool RetryActionMethod(Action actionMethod)
        {
            int tryCounter = 0;

            while (tryCounter <= 100)
            {
                try
                {
                    actionMethod();
                    return true;
                }
                catch (ErsDatabaseException e)
                {
                    if (e.Code == ErsFactory.ersUtilityFactory.getSetup().duplicated_ransu_error_code)
                    {
                        tryCounter++;
                        continue;
                    }

                    throw e;
                }
            }

            return false;
        }
    }
}
