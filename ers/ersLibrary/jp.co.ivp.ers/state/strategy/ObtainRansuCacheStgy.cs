﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state.strategy
{
    public class ObtainRansuCacheStgy
    {
        /// <summary>
        /// 乱数を取得
        /// </summary>
        public string GetCacheRansu(string key)
        {
            var stopCount = 0;

            var cacheRepository = ErsFactory.ersSessionStateFactory.GetErsCacheRepository();
            var generateRansuStringStgy = ErsFactory.ersSessionStateFactory.GetGenerateRansuStringStgy();

            while (true)
            {
                stopCount++;

                string cacheRansu = key + generateRansuStringStgy.Generate();
                var cache = ErsFactory.ersSessionStateFactory.GetErsCache();
                cache.ransu = cacheRansu;
                cache.key = key;
                cache.value = "0";
                try
                {
                    cacheRepository.Insert(cache);
                }
                catch
                {
                    if (stopCount > 100)
                    {
                        throw;
                    }
                    continue;
                }
                return cacheRansu;
            }
        }
    }
}
