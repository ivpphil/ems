﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.state
{
    public class ErsSessionStateModelContact
            : ErsSessionStateModelBase
    {
        public override string log_user_id
        {
            get { return this.cts_user_id; }
        }

        [SourceExpiration("cookieExpiration")]
        [ValueSourceCookie(true, IsHttpOnly = true)]
        [ErsSchemaValidation("language_t.culture")]
        [NotLoginData]
        public string culture { get; set; }

        [SourceExpiration("cookieExpiration")]
        [ValueSourceCookie(true, IsHttpOnly = true)]
        [ErsSchemaValidation("ransu_contact_t.ransu")]
        [NotLoginData]
        public string ransu { get; set; }

        [SourceExpiration("cookieExpiration")]
        [ValueSourceCookie(true, IsHttpOnly = true)]
        [ErsSchemaValidation("ransu_contact_t.ssl_ransu")]
        public string ssl_ransu { get; set; }

        [SourceExpiration("cookieExpiration")]
        [ValueSourceCookie(true, IsHttpOnly = true)]
        [ErsSchemaValidation("ransu_contact_t.ransu")]
        [NotLoginData]
        public string contact_ransu { get; set; }

        [SourceExpiration("cookieExpiration")]
        [ValueSourceCookie(true, IsHttpOnly = true)]
        [ErsSchemaValidation("ransu_contact_t.ssl_ransu")]
        public string contact_ssl_ransu { get; set; }

        [SourceExpiration("cookieExpiration")]
        [ValueSourceCookie(true, IsHttpOnly = true)]
        [ErsSchemaValidation("ransu_contact_t.cts_user_id")]
        public string cts_user_id { get; set; }

        [SourceExpiration("cookieExpiration")]
        [ValueSourceCookie(true, IsHttpOnly = true)]
        [ErsSchemaValidation("ransu_contact_t.cts_authority")]
        public string cts_authority { get; set; }

        [ErsSchemaValidation("cts_login_t.ag_name")]
        public string user_name { get; set; }

        [ErsSchemaValidation("administrator_t.role_gcode")]
        public string role_gcode { get; internal set; }

        [ErsSchemaValidation("cts_login_t.ag_type")]
        public EnumAgType? ag_type { get; internal set; }
    }
}
