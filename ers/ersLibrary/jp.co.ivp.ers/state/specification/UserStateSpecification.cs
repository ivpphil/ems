﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.state.specification
{
    /// <summary>
    /// Class for UserState Specification
    /// </summary>
    public class UserStateSpecification
    {
        /// <summary>
        /// Gets different userstates based on the given conditions.
        /// </summary>
        /// <param name="ersRansu">values of ErsRansu</param>
        /// <returns>Returns UserState value</returns>
        public virtual EnumUserState getUserState()
        {
            var ransu = ErsContext.sessionState.Get("ransu");
            var ssl_ransu = ErsContext.sessionState.Get("ssl_ransu");
            var mcode = ErsContext.sessionState.Get("mcode");
            if (string.IsNullOrEmpty(mcode))
            {
                mcode = ErsMember.DEFAUTL_MCODE;
            }

            if (string.IsNullOrEmpty(ransu))
            {
                //ransuが空
                return EnumUserState.NON_ASSIGNED_RANSU;
            }
            else if (!CheckInvalid(ransu))
            {
                //ransuが有効でない
                return EnumUserState.ASSIGNED_RANSU_BUT_INVALID;
            }
            else if (!CheckLogin(ssl_ransu, mcode))
            {
                //ログインしていない
                return EnumUserState.ASSIGNED_RANSU;
            }
            else
            {
                //ログイン済み
                return EnumUserState.LOGIN;
            }
        }

        /// <summary>
        /// 有効なセキュア乱数を保持しているか。
        /// </summary>
        /// <param name="ssl_ransu">Random characters for SSL</param>
        /// <param name="mcode">Member code</param>
        /// <returns>Returns false if the login checking is invalid, true if it's secured.</returns>
        protected virtual bool CheckLogin(string ssl_ransu, string mcode)
        {
            if (string.IsNullOrEmpty(ssl_ransu) || mcode == ErsMember.DEFAUTL_MCODE)
            {
                return false;
            }
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var objDB = ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(ErsWebDatabaseFactory.GetCloudConnectionStrings());
            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuRepository(objDB);
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuCriteria();
            criteria.ssl_ransu = ssl_ransu;
            criteria.mcode = mcode;
            if (setup.Multiple_sites == true)
            {
                criteria.site_id = setup.site_id;
            }

            return repository.GetRecordCount(criteria) == 1;
        }

        /// <summary>
        /// 有効な乱数を保持しているか。
        /// </summary>
        /// <param name="ransu">Random characters</param>
        /// <returns>Returns boolean result whether the ransu is invalid or not.</returns>
        protected virtual bool CheckInvalid(string ransu)
        {
            return ErsFactory.ersSessionStateFactory.GetObtainInvalidRansuCountStgy().Obtain(ransu) == 1;
        }

        /// <summary>
        /// 正常なencode_ransuを保持しているか。
        /// </summary>
        /// <param name="ssl_ransu">Random characters for SSL</param>
        /// <param name="mcode">Member code</param>
        /// <returns>Returns boolean result whether the Login is secured or not.</returns>
        public virtual bool IsLoginButNotSecure(string ssl_ransu, string mcode)
        {
            return CheckLogin(ssl_ransu, mcode);
        }
    }
}
