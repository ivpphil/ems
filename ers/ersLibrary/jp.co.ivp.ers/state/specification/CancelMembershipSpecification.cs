﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.state;
using System.Web.Mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.viewService;
using System.Data;

namespace jp.co.ivp.ers.state.specification
{
    /// <summary>
    /// Class for CancelMembershipSpecification.
    /// Impelements ISpecificationForSQL. 
    /// </summary>
    public class CancelMembershipSpecification : ISpecificationForSQL
    {
        /// <summary>
        /// Status count
        /// </summary>
        public virtual int status_count { get; set; }

        /// <summary>
        /// Query string
        /// </summary>
        protected virtual string StrSql { get; set; }

        /// <summary>
        /// List of ErsOrder
        /// </summary>
        public virtual IList<ErsOrder> list { get; protected set; }

        /// <summary>
        /// Member code
        /// </summary>
        public virtual string mcode { get; set; }

        public virtual int? site_id { get; set; }

        /// <summary>
        /// SQL文
        /// </summary>
        /// <returns>SQL文</returns>
        public virtual string asSQL()
        {
            StrSql += "SELECT COUNT(*) AS count FROM regular_detail_t WHERE mcode = '" + this.mcode + "' AND (delete_date IS NULL OR delete_date > next_date)";

            if (site_id.HasValue && site_id != (int) EnumSiteId.COMMON_SITE_ID)
            {
                StrSql += " AND site_id = " + site_id.Value;
            }
            return StrSql;
        }

        /// <summary>
        /// Gets boolean true.
        /// </summary>
        /// <returns>Returns boolean true.</returns>
        public virtual bool isSatisfiedBy() 
        {
            var dt = ErsRepository.SelectSatisfying(this);

            var result = Convert.ToInt32(dt[0]["count"]);

            if (result == 0)
            {
                return true;
            }
            return false;
        }
    }
}
