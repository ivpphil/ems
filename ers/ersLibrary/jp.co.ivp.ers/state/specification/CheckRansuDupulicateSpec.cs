﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state.specification
{
    public class CheckRansuDupulicateSpec
    {
        public bool IsSatisfy(string ransu)
        {
            var baskRepository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            var baskCriteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
            baskCriteria.ransu = ransu;

            var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var orderCriteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            orderCriteria.ransu = ransu;

            var objDB = ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(ErsWebDatabaseFactory.GetCloudConnectionStrings());
            var ransuRepository = ErsFactory.ersSessionStateFactory.GetErsRansuRepository(objDB);
            var ransuCriteria = ErsFactory.ersSessionStateFactory.GetErsRansuCriteria();
            ransuCriteria.SetCheckDuplicate(ransu);

            return (baskRepository.GetRecordCount(baskCriteria) == 0
                && orderRepository.GetRecordCount(orderCriteria) == 0
                && ransuRepository.GetRecordCount(ransuCriteria) == 0);
        }
    }
}
