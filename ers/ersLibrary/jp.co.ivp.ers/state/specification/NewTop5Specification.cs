﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.state.specification
{
    /// <summary>
    /// Class for Newtop5Specification.
    /// Impelements ISpecificationForSQL.
    /// </summary>
    public class NewTop5Specification : ISpecificationForSQL
    {
        /// <summary>
        /// Query string
        /// </summary>
        protected virtual string strSQL { get; set; }

        /// <summary>
        /// From date
        /// </summary>
        public virtual string date_from { get; set; }

        /// <summary>
        /// To date
        /// </summary>
        public virtual string date_to { get; set; }

        /// <summary>
        /// SQL文
        /// </summary>
        /// <returns>SQL文</returns>
        public virtual string asSQL()
        {
            strSQL += "SELECT scode,sname,count(scode) as scount FROM ds_master_t INNER JOIN d_master_t ON d_master_t.d_no = ds_master_t.d_no ";
            strSQL += "WHERE d_master_t.intime BETWEEN to_timestamp('" + this.date_from + "', 'yyyy/mm/dd hh24:mi:ss') AND to_timestamp('" + this.date_to + "', 'yyyy/mm/dd hh24:mi:ss') ";

            if (this.site_id.HasValue)
            {
                strSQL += "AND d_master_t.site_id = " + this.site_id;
            }

            strSQL += " AND ds_master_t.doc_bundling_flg = " + Convert.ToInt32(EnumDocBundlingFlg.OFF) + " ";
            strSQL += "GROUP BY scode,sname ORDER BY scount desc OFFSET 0 LIMIT 5";

            return strSQL;
        }

        /// <summary>
        /// Gets list of records using the query string asSQL.
        /// </summary>
        /// <returns>Returns list of records based on the output of the query.</returns>
        public virtual List<Dictionary<string, object>> isSatisfiedBy()
        {
            int count = 0;

            var dt = ErsRepository.SelectSatisfying(this);

            var retList = new List<Dictionary<string, object>>();

            foreach (var dtRow in dt)
            {
                dtRow["rank"] = ++count;
                retList.Add(dtRow);
            }

            return retList;
        }

        /// <summary>
        /// Site Id
        /// </summary>
        public int? site_id { get; set; }
    }
}
