﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state.specification
{
    /// <summary>
    /// Class for UserStateAdminSpecification
    /// </summary>
    public class UserStateAdminSpecification
    {
        /// <summary>
        /// Gets different administrator userstates based on the given conditions.
        /// </summary>
        /// <param name="ersRansu">values of ErsRansuAdmin</param>
        /// <returns>Returns UserState value for administrator.</returns>
        public virtual EnumUserState getUserState(string admin_ransu, string admin_ssl_ransu, string user_cd)
        {
            if (string.IsNullOrEmpty(admin_ransu))
            {
                //ransuが空
                return EnumUserState.NON_ASSIGNED_RANSU;
            }
            else if (!CheckInvalid(admin_ransu, user_cd))
            {
                //ransuが有効でない
                return EnumUserState.ASSIGNED_RANSU_BUT_INVALID;
            }
            else if (!CheckLogin(admin_ssl_ransu, user_cd))
            {
                //ログインしていない
                return EnumUserState.ASSIGNED_RANSU;
            }
            else
            {
                //ログイン済みの場合は、newspaper_idをセット(リクエストにつき一度だけ)
                var role_gcode = ErsContext.sessionState.Get("role_gcode");
                if (string.IsNullOrEmpty(role_gcode))
                {
                    var admin = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithUserCode(user_cd);
                    ErsContext.sessionState.Add("role_gcode", admin.role_gcode);
                }

                //ログイン済み
                return EnumUserState.LOGIN;
            }
        }

        /// <summary>
        /// Checks Login of user for administrator.
        /// </summary>
        /// <param name="ssl_ransu">Random characters for SSL</param>
        /// <param name="user_cd">Member code</param>
        /// <returns>Returns false if the login checking is invalid, true if it's secured.</returns>
        protected virtual bool CheckLogin(string ssl_ransu, string user_cd)
        {
            if (string.IsNullOrEmpty(ssl_ransu) || user_cd == ErsAdministrator.DEFAULT_USER_CODE)
            {
                return false;
            }
            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuAdminRepository();
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuAdminCriteria();
            criteria.ssl_ransu = ssl_ransu;
            criteria.user_cd = user_cd;

            return repository.GetRecordCount(criteria) == 1;
        }

        /// <summary>
        /// Checks whether the given ransu and mcode are invalid.
        /// </summary>
        /// <param name="ransu">Random characters</param>
        /// <param name="mcode">Member code</param>
        /// <returns>Returns boolean result whether the ransu and mcode are invalid or not.</returns>
        protected virtual bool CheckInvalid(string ransu, string mcode)
        {
            return ErsFactory.ersSessionStateFactory.GetObtainInvalidRansuAdminCountStgy().Obtain(ransu, mcode) == 1;
        }
    }
}
