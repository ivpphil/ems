﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state.specification
{
    public class CheckRansuContactDupulicateSpec
    {
        public bool IsSatisfy(string ransu)
        {
            var ransuRepository = ErsFactory.ersSessionStateFactory.GetErsRansuContactRepository();
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuContactCriteria();
            criteria.SetCheckDuplicate(ransu);
            return (ransuRepository.GetRecordCount(criteria) == 0);
        }
    }
}
