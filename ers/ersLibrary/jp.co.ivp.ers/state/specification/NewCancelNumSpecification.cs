﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.state.specification
{
    /// <summary>
    /// Class For NewCancelNumSpecification.
    /// Impelements ISpecificationForSQL. 
    /// </summary>
    public class NewCancelNumSpecification : ISpecificationForSQL
    {

        /// <summary>
        /// Query string
        /// </summary>
        protected virtual string strSQL { get; set; }

        /// <summary>
        /// From date
        /// </summary>
        public virtual string date_from { get; set; }

        /// <summary>
        /// To date
        /// </summary>
        public virtual string date_to { get; set; }

        /// <summary>
        /// SQL文
        /// </summary>
        /// <returns>SQL文</returns>
        public virtual string asSQL()
        {
            strSQL = "";
            strSQL += "SELECT count(DISTINCT d_master_t.d_no) as dcount FROM d_master_t ";
            strSQL += "JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no ";
            strSQL += "WHERE order_status IN (" + (int)EnumOrderStatusType.CANCELED + ", " + (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER + ") ";
            strSQL += "AND d_master_t.intime BETWEEN to_timestamp('" + this.date_from + "', 'yyyy/mm/dd hh24:mi:ss') AND to_timestamp('" + this.date_to + "', 'yyyy/mm/dd hh24:mi:ss')";
            return strSQL;
        }

        /// <summary>
        /// Gets count of d_master_t table using the query string asSQL.
        /// </summary>
        /// <returns>Returns count value of dcount.</returns>
        public virtual int isSatisfiedBy()
        {
            int result = 0;

            var dt = ErsRepository.SelectSatisfying(this);

            foreach (var dtRow in dt)
            {

                result =  int.Parse(Convert.ToString(dtRow["dcount"]));

            }
            return result;
        }

    }
}
