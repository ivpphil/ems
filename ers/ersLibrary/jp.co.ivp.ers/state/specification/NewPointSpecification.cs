﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.state.specification
{
    /// <summary>
    /// Class for NewPointSpecification.
    /// Impelements ISpecificationForSQL. 
    /// </summary>
    public class NewPointSpecification : ISpecificationForSQL
    {

        /// <summary>
        /// Query string
        /// </summary>
        protected virtual string strSQL { get; set; }

        public Dictionary<string, Dictionary<string, string>> dict = new Dictionary<string, Dictionary<string, string>>();

        public Dictionary<string, string> subdict1 = new Dictionary<string, string>();

        public Dictionary<string, string> subdict2 = new Dictionary<string, string>();

        /// <summary>
        /// Condition
        /// </summary>
        public virtual string Conditions { get; set; }

        /// <summary>
        /// SQL文
        /// </summary>
        /// <returns>SQL文</returns>
        public virtual string asSQL()
        {
            if (this.Conditions == "all")
            {
                strSQL += "SELECT COALESCE(SUM(GREATEST(0,now_p)),0) as allpcount from point_t ";
            }
            else
            {
                strSQL += "SELECT COALESCE(SUM(p_service), 0)  AS usepcount FROM d_master_t WHERE "
                    + "EXISTS(SELECT * FROM ds_master_t WHERE d_no = d_master_t.d_no "
                    + " AND order_status NOT IN( " + (int)EnumOrderStatusType.CANCELED + ", "
                    +(int)EnumOrderStatusType.CANCELED_AFTER_DELIVER+") )";
            }

            return strSQL;
        }

        /// <summary>
        /// Gets sum of value based on the satisfied condition using the query string asSQL.
        /// </summary>
        /// <returns>Returns value of allpcount if the condition is "all", returns usepcount if not.</returns>
        public virtual long isSatisfiedBy()
        {
            long result = 0;

            if (this.Conditions == "all")
            {

                var dt = ErsRepository.SelectSatisfying(this);

                foreach (var dtRow in dt)
                {
                    result = long.Parse(Convert.ToString(dtRow["allpcount"]));
                }
            }
            else
            {

                var dt = ErsRepository.SelectSatisfying(this);

                foreach (var dtRow in dt)
                {
                    result = long.Parse(Convert.ToString(dtRow["usepcount"]));
                }
            }
            return result;
        }
    }
}
