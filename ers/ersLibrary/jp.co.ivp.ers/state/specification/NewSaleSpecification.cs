﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.state.specification
{
    /// <summary>
    /// Class for NewSaleSpecification.
    /// Impelements ISpecificationForSQL. 
    /// </summary>
    public class NewSaleSpecification : ISpecificationForSQL
    {
        /// <summary>
        /// Query string
        /// </summary>
        protected virtual string strSQL { get; set; }

        /// <summary>
        /// From date
        /// </summary>
        public virtual string date_from { get; set; }

        /// <summary>
        /// To date
        /// </summary>
        public virtual string date_to { get; set; }

        public Dictionary<string, int> dict = new Dictionary<string, int>();

        /// <summary>
        /// SQL文
        /// </summary>
        /// <returns>SQL文</returns>
        public virtual string asSQL()
        {
            strSQL = "";
            //strSQL += "SELECT CASE WHEN SUM(total) IS NULL THEN 0 ELSE SUM(total) END AS stotal, COUNT(d_no) As dcount FROM d_master_t WHERE order_payment_status = " + OrderPaymentStatusType.PAID + " ";
            //strSQL += "AND intime BETWEEN to_timestamp('" + this.date_from + "', 'yyyy/mm/dd hh24:mi:ss') AND to_timestamp('" + this.date_to + "', 'yyyy/mm/dd hh24:mi:ss')";
            strSQL += " SELECT CASE WHEN SUM(A.total) IS NULL THEN 0 ELSE SUM(A.total) END AS stotal, ";
            strSQL += " COUNT(A.d_no) as dcount ";
            strSQL += " FROM ";

            strSQL += " ( ";
            strSQL += " SELECT DISTINCT  d_master_t.*";
            strSQL += " FROM d_master_t";
            strSQL += " INNER JOIN  ds_master_t ON (d_master_t.d_no = ds_master_t.d_no";
            strSQL += " AND ds_master_t.order_status = " + (int)EnumOrderStatusType.DELIVERED + ")";
            strSQL += " ) as A ";

            strSQL += " WHERE ";
            strSQL += " intime BETWEEN to_timestamp('" + this.date_from + "', 'yyyy/mm/dd hh24:mi:ss') AND to_timestamp('" + this.date_to + "', 'yyyy/mm/dd hh24:mi:ss')";

            return strSQL;
        }

        /// <summary>
        /// Gets list of records for sale_stotal and sale_dcount using the query string asSQL given.
        /// </summary>
        public virtual void isSatisfiedBy()
        {

            var dt = ErsRepository.SelectSatisfying(this);

            foreach (var dtRow in dt)
            {

                dict.Add("sale_stotal", Convert.ToInt32(dtRow["stotal"]));
                dict.Add("sale_dcount", Convert.ToInt32(dtRow["dcount"]));

            }
        }
    }
}
