﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.state.specification
{
    /// <summary>
    /// Class for NewMemberAllCountSpecification
    /// </summary>
    public class NewMemberAllCountSpecification
    {

        /// <summary>
        /// Gets the record count in member_t table.
        /// </summary>
        /// <returns>Returns record count in member_t table.</returns>
        public virtual long isSatisfiedBy()
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            criteria.deleted = EnumDeleted.NotDeleted;
            criteria.ignoreMonitor();

            return repository.GetRecordCount(criteria);
        }

    }
}
