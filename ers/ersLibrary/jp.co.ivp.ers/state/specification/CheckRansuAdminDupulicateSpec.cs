﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state.specification
{
    public class CheckRansuAdminDupulicateSpec
    {
        public bool IsSatisfy(string ransu)
        {
            var ransuRepository = ErsFactory.ersSessionStateFactory.GetErsRansuAdminRepository();
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuAdminCriteria();
            criteria.SetCheckDuplicate(ransu);
            return (ransuRepository.GetRecordCount(criteria) == 0);
        }
    }
}
