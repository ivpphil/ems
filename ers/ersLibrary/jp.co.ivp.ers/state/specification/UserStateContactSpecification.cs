﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.cts_operators;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state.specification
{
    public class UserStateContactSpecification
    {
        public EnumUserState getUserState()
        {
            var contact_ransu = ErsContext.sessionState.Get("contact_ransu");
            var contact_ssl_ransu = ErsContext.sessionState.Get("contact_ssl_ransu");
            var cts_user_id =  ErsContext.sessionState.Get("cts_user_id");
            if (string.IsNullOrEmpty(cts_user_id))
            {
                //未ログイン会員はゼロ
                cts_user_id = ErsCtsOperator.DEFAULT_USER_ID;
            }

            if (string.IsNullOrEmpty(contact_ransu))
            {
                //ransuが空
                return EnumUserState.NON_ASSIGNED_RANSU;
            }
            else if (ErsFactory.ersSessionStateFactory.GetObtainInvalidRansuContactCountStgy().Obtain(contact_ransu) != 1)
            {
                //ransuが有効でない
                return EnumUserState.ASSIGNED_RANSU_BUT_INVALID;
            }
            else if (!CheckLogin(contact_ssl_ransu, cts_user_id))
            {
                //ログインしていない
                return EnumUserState.ASSIGNED_RANSU;
            }
            else
            {
                var ag_type = ErsFactory.ersCtsOperatorFactory.GetObtainAgentTypeStgy().getAgeType(Convert.ToInt32(cts_user_id));
                ErsContext.sessionState.Add("ag_type", ag_type);

                //ログイン済み
                return EnumUserState.LOGIN;
            }
        }

        /// <summary>
        /// 有効なセキュア乱数を保持しているか。
        /// </summary>
        /// <param name="ssl_ransu"></param>
        /// <param name="cts_user_id"></param>
        /// <returns></returns>
        private bool CheckLogin(string ssl_ransu, string cts_user_id)
        {
            if (string.IsNullOrEmpty(ssl_ransu) || cts_user_id == ErsCtsOperator.DEFAULT_USER_ID)
            {
                return false;
            }

            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuContactRepository();
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuContactCriteria();
            criteria.ssl_ransu = ssl_ransu;
            criteria.cts_user_id = cts_user_id;

            return repository.GetRecordCount(criteria) == 1;
        }

        /// <summary>
        /// 正常なencode_ransuを保持しているか。
        /// </summary>
        /// <param name="encode_ransu"></param>
        /// <param name="cts_user_id"></param>
        /// <returns></returns>
        public bool IsLoginButNotSecure(string ssl_ransu, string cts_user_id)
        {
            return CheckLogin(ssl_ransu, cts_user_id);
        }

    }
}
