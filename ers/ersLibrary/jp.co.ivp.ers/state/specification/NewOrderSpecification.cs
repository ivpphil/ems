﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.state.specification
{
    /// <summary>
    /// Class for NewOrderSpecification.
    /// Impelements ISpecificationForSQL. 
    /// </summary>
    public class NewOrderSpecification : ISpecificationForSQL
    {
        /// <summary>
        /// Query string
        /// </summary>
        protected virtual string strSQL { get; set; }

        /// <summary>
        /// From date
        /// </summary>
        public virtual string date_from { get; set; }

        /// <summary>
        /// To date
        /// </summary>
        public virtual string date_to { get; set; }

        public Dictionary<string, int> dict = new Dictionary<string, int>();

        /// <summary>
        /// SQL文
        /// </summary>
        /// <returns>SQL文</returns>
        public virtual string asSQL()
        {
            strSQL = "";
            strSQL += "SELECT COALESCE(SUM(d_master_t.total), 0) AS stotal, COUNT(DISTINCT d_master_t.d_no) As dcount FROM d_master_t ";
            strSQL += "WHERE d_master_t.intime BETWEEN to_timestamp('" + this.date_from + "', 'yyyy/mm/dd hh24:mi:ss') AND to_timestamp('" + this.date_to + "', 'yyyy/mm/dd hh24:mi:ss')";
            return strSQL;
        }

        /// <summary>
        /// Gets list of records for order_stotal and order_dcount using the query string asSQL given.
        /// </summary>
        public virtual void isSatisfiedBy()
        {

            var dt = ErsRepository.SelectSatisfying(this);

            foreach (var dtRow in dt)
            {
                dict.Add("order_stotal", Convert.ToInt32(dtRow["stotal"]));
                dict.Add("order_dcount", Convert.ToInt32(dtRow["dcount"]));
            }
        }
    }
}
