﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Web;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.cts_operators;
using System.Collections.Specialized;
using System.Reflection;

namespace jp.co.ivp.ers.state
{
    public class ErsSessionStateContact
        : ErsState, ISession
    {
        protected override ErsBindableModel GetModel()
        {
            return ErsFactory.ersSessionStateFactory.GetErsSessionStateModelContact();
        }

        protected override int? GetCookieExpiration(PropertyInfo prop)
        {
            var sourceExpirationAttr = (SourceExpirationAttribute)prop.GetCustomAttributes(typeof(SourceExpirationAttribute), false).FirstOrDefault();
            if (sourceExpirationAttr == null)
            {
                return null;
            }
            return ErsFactory.ersUtilityFactory.getSetup().GetCookieExpiration(sourceExpirationAttr.configKey);
        }

        protected override string GetCookieDomain()
        {
            return ErsFactory.ersUtilityFactory.getSetup().cookieDomain;
        }

        protected override string GetCookiePath()
        {
            return ErsFactory.ersUtilityFactory.getSetup().cookiePath;
        }

        protected override EnumRequireHttps GetEnableSSL()
        {
            return ErsFactory.ersUtilityFactory.getSetup().enableSSL;
        }

        /// <summary>
        /// Restore session
        /// </summary>
        /// <param name="request"></param>
        public override void Restore(HttpCookieCollection Cookies,
            NameValueCollection Form,
            NameValueCollection QueryString)
        {
            base.Restore(Cookies, Form, QueryString);
        }

        /// <summary>
        /// ログイン処理
        /// </summary>
        public void getNewRansu()
        {
            ErsFactory.ersSessionStateFactory.GetObtainRansuContactStgy().GetNewRansu();
        }

        public void getNewSSLransu(string cts_user_id)
        {
            ErsFactory.ersSessionStateFactory.GetObtainRansuContactStgy().GetNewSSLRansu(cts_user_id, this.GetUserAuthority(cts_user_id));
        }

        public EnumUserState getUserState()
        {
            return ErsFactory.ersSessionStateFactory.GetUserStateContactSpecification().getUserState();
        }

        public string GetUserCode(string user_id, string passwd)
        {
            /// 重複チェック用クライテリア
            var repository = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorRepository();
            var criteria = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorCriteria();
            criteria.passwd = passwd;
            criteria.user_id = user_id;
            criteria.active = EnumActive.Active;

            var list = repository.Find(criteria);
            if (list.Count == 1)
            {
                return list.First().id.ToString();
            }

            return ErsCtsOperator.DEFAULT_USER_ID;
        }

        public string GetUserAuthority(string user_id)
        {
            /// 重複チェック用クライテリア
            var repository = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorRepository();
            var criteria = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorCriteria();
            criteria.id = Convert.ToInt32(user_id);
            criteria.active = EnumActive.Active;

            var list = repository.Find(criteria);
            if (list.Count == 1)
            {
                return list.First().authority;
            }

            return ErsCtsOperator.DEFAULT_USER_AUTHORITY;
        }

        public string GetUserName(int user_id)
        {
            /// 重複チェック用クライテリア
            var repository = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorRepository();
            var criteria = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorCriteria();
            criteria.id = user_id;
            criteria.active = EnumActive.Active;

            var list = repository.Find(criteria);
            if (list.Count == 1)
            {
                return list.First().ag_name;
            }

            return ErsCtsOperator.DEFAULT_USER_NAME;
        }

        public void LogOut()
        {
            var contact_ssl_ransu = ErsContext.sessionState.Get("contact_ssl_ransu");
            var cts_user_id = ErsContext.sessionState.Get("cts_user_id");
            if (!string.IsNullOrEmpty(cts_user_id))
            {
                this.UpdateRansuForLogout(contact_ssl_ransu, cts_user_id);
            }

            
            this.ClearLoginData();
        }

        private void UpdateRansuForLogout(string contact_ssl_ransu, string cts_user_id)
        {
            var repostory = ErsFactory.ersSessionStateFactory.GetErsRansuContactRepository();
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuContactCriteria();
            criteria.ssl_ransu = contact_ssl_ransu;
            criteria.cts_user_id = cts_user_id;
            var listRansu = repostory.Find(criteria);
            if (listRansu.Count == 0)
            {
                return;
            }
            if (listRansu.Count != 1)
            {
                throw new Exception("ssl_ransu is duplicated");
            }
            var oldRansu = listRansu.First();
            var newRansu = ErsFactory.ersSessionStateFactory.GetErsRansuContactWithId(oldRansu.id);
            newRansu.ssl_ransu = null;
            newRansu.cts_user_id = ErsCtsOperator.DEFAULT_USER_ID;
            repostory.Update(oldRansu, newRansu);
        }

        public bool CheckPasswordExpiration()
        {
            throw new NotImplementedException();
        }


        public bool HasPermission(params string[] permissions)
        {
            throw new NotImplementedException();
        }
    }

}
