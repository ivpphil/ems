﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.state
{    
    /// <summary>
    /// Class for ErsSessionStateModelAdmin
    /// </summary>
    public class ErsSessionStateModelAdmin
            : ErsSessionStateModelBase
    {
        public override string log_user_id
        {
            get { return this.user_cd; }
        }

        [SourceExpiration("cookieExpiration")]
        [ValueSourceCookie(true, IsHttpOnly = true)]
        [ErsSchemaValidation("language_t.culture")]
        [NotLoginData]
        public string culture { get; set; }

        /// <summary>
        /// Random characters for administrator
        /// </summary>
        [SourceExpiration("cookieExpiration")]
        [ValueSourceCookie(true, IsHttpOnly = true)]
        [ErsSchemaValidation("ransu_admin_t.ransu")]
        public string admin_ransu { get; set; }

        /// <summary>
        /// ssl random characters for administrator
        /// </summary>
        [SourceExpiration("cookieExpiration")]
        [ValueSourceCookie(true, IsHttpOnly = true)]
        [ErsSchemaValidation("ransu_admin_t.ssl_ransu")]
        public string admin_ssl_ransu { get; set; }

        /// <summary>
        /// User code for administrator
        /// </summary>
        [SourceExpiration("cookieExpiration")]
        [ValueSourceCookie(true, IsHttpOnly = true)]
        [ErsSchemaValidation("ransu_admin_t.user_cd")]
        public string user_cd { get; set; }

        [ErsSchemaValidation("administrator_t.role_gcode")]
        public string role_gcode { get; internal set; }
    }
}
