﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Web.Mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.cts_operators;

namespace jp.co.ivp.ers.state
{
    public class ErsControllerSecureContact
        : ErsControllerBase
    {

        public int cts_User_ID;
        public string cts_Ransu;

        protected override ErsState GetErsSessionState()
        {
            return ErsFactory.ersSessionStateFactory.getErsSessionStateContact();
        }

        public virtual ActionResult ExecuteError()
        {
            if (Convert.ToString(this.RouteData.Values["controller"]) != "Home")
            {
                this.RouteData.Values.Add("sessionError", "1");
            }
            return this.RedirectToAction("login", "login", this.RouteData.Values);
        }

        /// <summary>
        /// Controllerを初期化する。
        /// </summary>
        /// <param name="requestContext"></param>
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            ErsContext.sessionState = this.GetErsSessionState();
            ErsContext.sessionState.Restore(Request.Cookies, Request.Form, Request.QueryString);
        }

        /// <summary>
        /// アクション実行後に実行される。
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            //SessionをViewに出力
            ViewBag.SessionStateHidden = ErsContext.sessionState;

            ErsContext.sessionState.SetCookiesValue();
        }

        public bool IsAuthorized()
        {
            //スーパーユーザは無くす。
            return (ErsContext.sessionState.Get<EnumAgType>("ag_type") == EnumAgType.Admin);
        }

        public bool IsSuperUser()
        {
            //スーパーユーザは無くす。
            return (ErsContext.sessionState.Get("cts_authority") == CtsAuthorityType.SUPERVISOR);
        }

        protected override mvc.ErsState GetErsState()
        {
            return ErsContext.sessionState;
        }

        /// <summary>
        /// セッション情報をViewにセットする
        /// </summary>
        protected override void SetSessionToView()
        {
            //SessionをViewに出力
            ViewBag.SessionStateHidden = ErsContext.sessionState;

            ErsContext.sessionState.SetCookiesValue();
        }
    }

}
