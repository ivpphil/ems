﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Web.Mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.state
{
    /// <summary>
    /// Class for ErsControllerSecure
    /// </summary>
    public class ErsControllerSecure
        : ErsControllerBase
    {
        /// <summary>
        /// セッション情報を取得する。
        /// </summary>
        /// <returns>Returns instance of ErsSessionState</returns>
        [NonAction]
        protected override ErsState GetErsSessionState()
        {
            return ErsFactory.ersSessionStateFactory.getErsSessionState();
        }

        /// <summary>
        /// Controllerを初期化する。
        /// </summary>
        /// <param name="requestContext">Request context</param>
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            ErsContext.sessionState = this.GetErsSessionState();
            ErsContext.sessionState.Restore(Request.Cookies, Request.Form, Request.QueryString);
        }

        /// <summary>
        /// セッション情報をViewにセットする
        /// </summary>
        protected override void SetSessionToView()
        {
            //SessionをViewに出力
            ViewBag.SessionStateHidden = ErsContext.sessionState;

            ErsContext.sessionState.SetCookiesValue();
        }

        protected override mvc.ErsState GetErsState()
        {
            return ErsContext.sessionState;
        }

        /// <summary>
        /// Get System Error Message
        /// </summary>
        /// <returns></returns>
        protected override string GetSystemErrorMessage()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            return ErsResources.GetMessage("system_error", setup.error_message);
        }

        public virtual ActionResult ExecuteError()
        {
            if (Convert.ToString(this.RouteData.Values["controller"]) != "Home")
            {
                this.RouteData.Values.Add("sessionError", "1");
            }
            return this.RedirectToAction("Login", "login", this.RouteData.Values);
        }

        //セッション情報判定＋クリア
        public virtual void validateSession()
        {
            var session = (ISession)ErsContext.sessionState;
            EnumUserState state = session.getUserState();

            if (state == EnumUserState.NON_ASSIGNED_RANSU
                || state == EnumUserState.ASSIGNED_RANSU_BUT_INVALID)
            {
                ((ISession)ErsContext.sessionState).LogOut();
            }

        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (((ISession)ErsContext.sessionState).getUserState() == EnumUserState.LOGIN)
            {
                var emp_no = ErsContext.sessionState.Get("mcode");
                var employee = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(emp_no);

                // Currently login user has been set to invalid
                if (emp_no.HasValue() && employee == null)
                {
                    ((ISession)ErsContext.sessionState).LogOut();
                    throw new ErsException("10203", ErsFactory.ersUtilityFactory.getSetup().sec_url);
                }
                else
                {
                    if (employee.position.HasValue)
                    {
                        ViewData["position"] = (int)employee.position;
                    }
                }

                var batchDetails = ErsFactory.ersRequestFactory.getErsScheduleBatchLogWithDateIsNotSuccess(DateTime.Today);
                if (batchDetails != null)
                {
                    ViewData["batchStatus"] = batchDetails.status;
                }

            }

        }


        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {

            base.OnActionExecuted(filterContext);

            if (((ISession)ErsContext.sessionState).getUserState() == EnumUserState.LOGIN)
            {
                var emp_no = ErsContext.sessionState.Get("mcode");
                var employee = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(emp_no);

                // Set position view data for navigation bar
                if (employee.position.HasValue)
                {
                    ViewData["position"] = (int)employee.position;
                }


            }
        }

    }
}
