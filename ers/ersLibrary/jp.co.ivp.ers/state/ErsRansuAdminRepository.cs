﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state
{
    public class ErsRansuAdminRepository
        : ErsRepository<ErsRansuAdmin>
    {
        public ErsRansuAdminRepository()
            : base("ransu_admin_t")
        {
        }
    }
}
