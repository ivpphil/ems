﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Web;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.state
{
    /// <summary>
    /// Class for ErsSessionStateMobile
    /// </summary>
    public class ErsSessionStateMobile
        : ErsSessionState
    {
        /// <summary>
        /// Obtain an instance of ErsSessionStateModelMobile.
        /// </summary>
        /// <returns>Returns an instance of ErsSessionStateModelMobile.</returns>
        protected override ErsBindableModel GetModel()
        {
            return ErsFactory.ersSessionStateFactory.GetErsSessionStateModelMobile();
        }

    }
}
