﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.state
{
    public class ErsPassRansuCriteria
        : Criteria
    {
        public string ransu
        {
            set
            {
                this.Add(Criteria.GetCriterion("pass_ransu_t.ransu", value, Operation.EQUAL));
            }
        }
    }
}
