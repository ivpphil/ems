﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Class Interface for Isession
    /// </summary>
    public interface ISession
    {
        /// <summary>
        /// Get user login state.
        /// </summary>
        /// <returns>User state</returns>
        EnumUserState getUserState();

        /// <summary>
        /// Issue a new secure session of random characters.
        /// </summary>
        /// <param name="code">SSL ransu code</param>
        void getNewSSLransu(string code);

        /// <summary>
        /// Issue a new session of random characters.
        /// </summary>
        void getNewRansu();

        /// <summary>
        /// Get code that identifies the user.
        /// </summary>
        /// <param name="user_login_id">login id of the user</param>
        /// <param name="passwd">password of the user</param>
        /// <returns>returns user code</returns>
        string GetUserCode(string user_login_id, string passwd);

        /// <summary>
        /// Gets result if Password already expired or not
        /// </summary>
        /// <returns>Returns true if expired, false if not expired</returns>
        bool CheckPasswordExpiration();

        /// <summary>
        /// Logout the user.
        /// </summary>
        void LogOut();
    }
}
