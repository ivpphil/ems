﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.state
{
    /// <summary>
    /// Class for ErsRansuAdmin
    /// </summary>
    public class ErsRansuAdmin
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string user_cd { get; set; }
        public string ssl_ransu { get; set; }
        public string ransu { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
    }
}
