﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.state
{
    /// <summary>
    /// Class for ErsSessionStateModelMobile
    /// </summary>
    public class ErsSessionStateModelMobile
            : ErsSessionStateModelBase
    {
        public override string log_user_id
        {
            get { return this.mcode; }
        }

        /// <summary>
        /// ransu (random characters) from ransu_t table.
        /// </summary>
        [ValueSource(VALUE_SOURCE.FORM_QUERY_STRING, false)]
        [ErsSchemaValidation("ransu_t.ransu")]
        [NotLoginData]
        public string ransu { get; set; }

        /// <summary>
        /// ssl_ransu (secured random characters) from ransu_t table.
        /// </summary>
        [ValueSource(VALUE_SOURCE.FORM_QUERY_STRING, true)]
        [ErsSchemaValidation("ransu_t.ssl_ransu")]
        public string ssl_ransu { get; set; }

        /// <summary>
        /// Member code value from member_t table
        /// </summary>
        [ValueSource(VALUE_SOURCE.FORM_QUERY_STRING, true)]
        [ErsSchemaValidation("member_t.mcode")]
        public string mcode { get; set; }

        /// <summary>
        /// Encoded random characters
        /// </summary>
        [ValueSource(VALUE_SOURCE.FORM_QUERY_STRING, false)]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber)]
        public string encode_ransu { get; set; }

        /// <summary>
        /// Encoded member code
        /// </summary>
        [ValueSource(VALUE_SOURCE.FORM_QUERY_STRING, false)]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber)]
        public string encode_mcode { get; set; }

        /// <summary>
        /// Nickname value from member_t table.
        /// </summary>
        [ValueSource(VALUE_SOURCE.FORM_QUERY_STRING, false)]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string nickname { get; set; }

        [ValueSource(VALUE_SOURCE.FORM_QUERY_STRING, false)]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber )]
        public string guid
        {
            get
            {
                return "on";
            }
        }


    }
}
