﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.state
{
    public class ErsRansuRepository
        : ErsRepository<ErsRansu>
    {
        public ErsRansuRepository(ErsDatabase objDB)
            : base("ransu_t", objDB)
        {
        }
    }
}
