﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace jp.co.ivp.ers.state
{
    /// <summary>
    /// この属性を指定すればセッションチェックは判定しない。
    /// </summary>
    public class NoNeedSessionAttribute
        : FilterAttribute
    {
    }
}
