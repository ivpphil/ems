﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Web;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.administrator;
using System.Collections.Specialized;
using System.Reflection;

namespace jp.co.ivp.ers.state
{

    /// <summary>
    /// Class for ErsSessionStateAdmin.
    /// Implements ErsState and ISession
    /// </summary>
    public class ErsSessionStateAdmin
        : ErsState, ISession
    {
        public ErsSessionStateAdmin()
        {
        }

        /// <summary>
        /// Obtain an instance of ErsSessionStateModelAdmin
        /// </summary>
        /// <returns>Returns an instance of ErsSessionStateModelAdmin</returns>
        protected override ErsBindableModel GetModel()
        {
            return ErsFactory.ersSessionStateFactory.GetErsSessionStateModelAdmin();
        }

        /// <summary>
        /// Gets value of cookie expiration from setup.config.
        /// </summary>
        /// <returns>Returns cookieExpiration value</returns>
        protected override int? GetCookieExpiration(PropertyInfo prop)
        {
            var sourceExpirationAttr = (SourceExpirationAttribute)prop.GetCustomAttributes(typeof(SourceExpirationAttribute), false).FirstOrDefault();
            if (sourceExpirationAttr == null)
            {
                return null;
            }
            return ErsFactory.ersUtilityFactory.getSetup().GetCookieExpiration(sourceExpirationAttr.configKey);
        }

        /// <summary>
        /// Gets value of cookie Domain from setup.config.
        /// </summary>
        /// <returns>Returns cookieDomain value</returns>
        protected override string GetCookieDomain()
        {
            return ErsFactory.ersUtilityFactory.getSetup().cookieDomain;
        }

        /// <summary>
        /// Gets value of cookie path from setup.config.
        /// </summary>
        /// <returns>Returns cookiePath value</returns>
        protected override string GetCookiePath()
        {
            return ErsFactory.ersUtilityFactory.getSetup().cookiePath;
        }

        /// <summary>
        /// Gets boolean value of enableSSL from setup.config
        /// </summary>
        /// <returns>Returns boolean value of enableSSL</returns>
        protected override EnumRequireHttps GetEnableSSL()
        {
            return ErsFactory.ersUtilityFactory.getSetup().enableSSL;
        }

       /// <summary>
        /// Restore session
        /// </summary>
        /// <param name="request">Http request</param>
        public override void Restore(HttpCookieCollection Cookies,
            NameValueCollection Form,
            NameValueCollection QueryString)
        {
            base.Restore(Cookies, Form, QueryString);
        }

        /// <summary>
        /// Issue a new session.
        /// </summary>
        public virtual void getNewRansu()
        {
            ErsFactory.ersSessionStateFactory.GetObtainRansuAdminStgy().GetNewRansu();
        }

        /// <summary>
        /// Issue a new secure session by user code.
        /// </summary>
        /// <param name="user_cd">user code</param>
        public virtual void getNewSSLransu(string user_cd)
        {
            ErsFactory.ersSessionStateFactory.GetObtainRansuAdminStgy().GetNewSSLRansu(user_cd);
        }

        /// <summary>
        /// Get user login state.
        /// </summary>
        /// <returns>Returns user state</returns>
        public virtual EnumUserState getUserState()
        {
            var admin_ransu = this.Get("admin_ransu");
            var admin_ssl_ransu = this.Get("admin_ssl_ransu");
            var user_cd = this.Get("user_cd");
            if (string.IsNullOrEmpty(user_cd))
            {
                user_cd = ErsAdministrator.DEFAULT_USER_CODE;
            }
            return ErsFactory.ersSessionStateFactory.GetUserStateAdminSpecification().getUserState(admin_ransu, admin_ssl_ransu, user_cd);
        }


        /// <summary>
        /// Get user code that identifies the user based on user login and password using ErsDB_administrator_t.
        /// </summary>
        /// <param name="user_login_id">user login id</param>
        /// <param name="passwd">password</param>
        /// <returns>Returns user code based on the specified user login and password</returns>
        public virtual string GetUserCode(string user_login_id, string passwd)
        {
            var repository = ErsFactory.ersAdministratorFactory.GetErsAdministratorRepository();
            var criteria = ErsFactory.ersAdministratorFactory.GetErsAdministratorCriteria();
            criteria.user_login_id = user_login_id;
            criteria.passwd = passwd;
            var listAdmin = repository.Find(criteria);
            if (listAdmin.Count != 1)
            {
                return ErsAdministrator.DEFAULT_USER_CODE;
            }
            return listAdmin.First().user_cd;
        }

        /// <summary>
        /// Checks Password Expiration
        /// </summary>
        /// <returns>Returns boolean result based on the specified conditions for checking of password expiration</returns>
        public virtual bool CheckPasswordExpiration()
        {
            var user_cd = ErsContext.sessionState.Get("user_cd");
            if (string.IsNullOrEmpty(user_cd))
            {
                user_cd = ErsAdministrator.DEFAULT_USER_CODE;
            }
            var objUser = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithUserCode(user_cd);
            if (objUser.pass_date.Value.AddMonths(1) < DateTime.Now)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Logout the user.
        /// </summary>
        public virtual void LogOut()
        {
            var admin_ssl_ransu = ErsContext.sessionState.Get("admin_ssl_ransu");
            var user_cd = ErsContext.sessionState.Get("user_cd");
            if (!string.IsNullOrEmpty(user_cd))
            {
                this.UpdateRansuForLogout(admin_ssl_ransu, user_cd);
            }

            this.ClearLoginData();
        }

        private void UpdateRansuForLogout(string admin_ssl_ransu, string user_cd)
        {
            var repostory = ErsFactory.ersSessionStateFactory.GetErsRansuAdminRepository();
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuAdminCriteria();
            criteria.ssl_ransu = admin_ssl_ransu;
            criteria.user_cd = user_cd;
            var listRansu = repostory.Find(criteria);
            if (listRansu.Count == 0)
            {
                return;
            }
            if (listRansu.Count != 1)
            {
                throw new Exception("ssl_ransu is duplicated");
            }
            var oldRansu = listRansu.First();
            var newRansu = ErsFactory.ersSessionStateFactory.GetErsRansuAdminWithId(oldRansu.id);
            newRansu.ssl_ransu = null;
            newRansu.user_cd = ErsAdministrator.DEFAULT_USER_CODE;
            repostory.Update(oldRansu, newRansu);
        }

        /// <summary>
        /// Gets boolean result with unauthorized message if this user don't have the specified permission.
        /// </summary>
        /// <param name="permissions">permission</param>
        /// <returns>Returns result of permission</returns>
        public bool HasPermission(params string[] permissions)
        {
            var admin = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithUserCode(this.Get("user_cd"));
            return permissions.Contains(admin.role_gcode);
        }
    }
}
