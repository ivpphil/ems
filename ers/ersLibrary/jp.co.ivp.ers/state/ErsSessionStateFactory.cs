﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state.strategy;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.state
{
    /// <summary>
    /// Class for ErsSessionStateFactory
    /// </summary>
    public class ErsSessionStateFactory
    {
        /// <summary>
        /// Get New ErsSessionState Object
        /// </summary>
        /// <returns>ErsSessionState</returns>
        public virtual ErsSessionState getErsSessionState()
        {
            return new ErsSessionState();
        
        }

        /// <summary>
        /// Get New ErsSessionState Object
        /// </summary>
        /// <returns>ErsSessionState</returns>
        public virtual ErsSessionStateAdmin getErsSessionStateAdmin()
        {
            return new ErsSessionStateAdmin();
        }

        public virtual ErsSessionStateContact getErsSessionStateContact()
        {
            return new ErsSessionStateContact();
        }

        /// <summary>
        /// Get New ErsSessionStateModelPc Object
        /// </summary>
        /// <returns>ErsSessionStateModelPc</returns>
        public virtual ErsBindableModel GetErsSessionStateModelPc()
        {
            return new ErsSessionStateModelPc();
        }

        /// <summary>
        /// Get New ErsSessionStateModelMobile Object
        /// </summary>
        /// <returns>ErsSessionStateModelMobile</returns>
        public virtual ErsBindableModel GetErsSessionStateModelMobile()
        {
            return new ErsSessionStateModelMobile();
        }

        /// <summary>
        /// Get New ErsSessionStateModelAdmin Object
        /// </summary>
        /// <returns>ErsSessionStateModelAdmin</returns>
        public virtual ErsBindableModel GetErsSessionStateModelAdmin()
        {
            return new ErsSessionStateModelAdmin();
        }

        /// <summary>
        /// Get New GenerateRansuStringStgy Object
        /// </summary>
        /// <returns>GenerateRansuStringStgy</returns>
        public virtual GenerateRansuStringStgy GetGenerateRansuStringStgy()
        {
            return new GenerateRansuStringStgy();
        }

        public virtual ErsBindableModel GetErsSessionStateModelContact()
        {
            return new ErsSessionStateModelContact();
        }

        public virtual ErsCacheRepository GetErsCacheRepository()
        {
            return new ErsCacheRepository();
        }

        public virtual ErsCacheCriteria GetErsCacheCriteria()
        {
            return new ErsCacheCriteria();
        }

        public virtual ErsCache GetErsCache()
        {
            return new ErsCache();
        }


        public virtual UserStateAdminSpecification GetUserStateAdminSpecification()
        {
            return new UserStateAdminSpecification();
        }

        public virtual ErsRansu GetErsRansu()
        {
            return new ErsRansu();
        }

        public virtual CheckRansuDupulicateSpec GetCheckRansuDupulicateSpec()
        {
            return new CheckRansuDupulicateSpec();
        }

        public virtual ErsPassRansuRepository GetErsPassRansuRepository()
        {
            return new ErsPassRansuRepository();
        }

        public virtual ErsPassRansuCriteria GetErsPassRansuCriteria()
        {
            return new ErsPassRansuCriteria();
        }

        public virtual ErsPassRansu GetErsPassRansu()
        {
            return new ErsPassRansu();
        }

        public virtual UserStateSpecification GetUserStateSpecification()
        {
            return new UserStateSpecification();
        }

        public virtual ObtainRansuStgy GetObtainRansuStgy()
        {
            return new ObtainRansuStgy();
        }

        public virtual ErsRansuRepository GetErsRansuRepository(ErsDatabase objDB)
        {
            return new ErsRansuRepository(objDB);
        }

        public virtual ErsRansuCriteria GetErsRansuCriteria()
        {
            return new ErsRansuCriteria();
        }

        public virtual ErsRansu GetErsRansuWithId(int? id)
        {
            var objDB = ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(ErsWebDatabaseFactory.GetCloudConnectionStrings());
            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuRepository(objDB);
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuCriteria();
            criteria.id = id;
            var listObj = repository.Find(criteria);
            if (listObj.Count != 1)
            {
                return null;
            }
            return listObj.First();
        }

        public virtual ObtainInvalidRansuCountStgy GetObtainInvalidRansuCountStgy()
        {
            return new ObtainInvalidRansuCountStgy();
        }

        public virtual ObtainRansuAdminStgy GetObtainRansuAdminStgy()
        {
            return new ObtainRansuAdminStgy();
        }

        public virtual ErsRansuAdminRepository GetErsRansuAdminRepository()
        {
            return new ErsRansuAdminRepository();
        }

        public virtual ErsRansuAdminCriteria GetErsRansuAdminCriteria()
        {
            return new ErsRansuAdminCriteria();
        }

        public virtual CheckRansuAdminDupulicateSpec GetCheckRansuAdminDupulicateSpec()
        {
            return new CheckRansuAdminDupulicateSpec();
        }

        public virtual ObtainInvalidRansuAdminCountStgy GetObtainInvalidRansuAdminCountStgy()
        {
            return new ObtainInvalidRansuAdminCountStgy();
        }

        public virtual ErsRansuAdmin GetErsRansuAdmin()
        {
            return new ErsRansuAdmin();
        }

        public virtual ErsRansuAdmin GetErsRansuAdminWithId(int? id)
        {
            var repository = this.GetErsRansuAdminRepository();
            var criteria = this.GetErsRansuAdminCriteria();
            criteria.id = id;
            var listRansu = repository.Find(criteria);
            if (listRansu.Count != 1)
            {
                return null;
            }
            return listRansu.First();
        }

        public virtual UserStateContactSpecification GetUserStateContactSpecification()
        {
            return new UserStateContactSpecification();
        }

        public virtual ObtainRansuContactStgy GetObtainRansuContactStgy()
        {
            return new ObtainRansuContactStgy();
        }

        public virtual ErsRansuContactRepository GetErsRansuContactRepository()
        {
            return new ErsRansuContactRepository();
        }

        public virtual ErsRansuContactCriteria GetErsRansuContactCriteria()
        {
            return new ErsRansuContactCriteria();
        }

        public virtual ErsRansuContact GetErsRansuContact()
        {
            return new ErsRansuContact();
        }

        public virtual ErsRansuContact GetErsRansuContactWithId(int? id)
        {
            var repository = this.GetErsRansuContactRepository();
            var criteria = this.GetErsRansuContactCriteria();
            criteria.id = id;
            var listRansu = repository.Find(criteria);
            if (listRansu.Count != 1)
            {
                return null;
            }
            return listRansu.First();
        }

        public virtual CheckRansuContactDupulicateSpec GetCheckRansuContactDupulicateSpec()
        {
            return new CheckRansuContactDupulicateSpec();
        }

        public virtual ObtainInvalidRansuContactCountStgy GetObtainInvalidRansuContactCountStgy()
        {
            return new ObtainInvalidRansuContactCountStgy();
        }

        /// <summary>
        /// Obtain instance of ErsViewRansuService class.
        /// </summary>
        /// <returns>Returns instance of ErsViewRansuService</returns>
        public virtual RansuDeleteStgy GetRansuDeleteStgy()
        {
            return new RansuDeleteStgy();
        }

        public virtual ObtainRansuCacheStgy GetObtainRansuCacheStgy()
        {
            return new ObtainRansuCacheStgy();
        }
    }
}