﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Web;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using System.Reflection;
using System.Collections.Specialized;

namespace jp.co.ivp.ers.state
{
    /// <summary>
    /// Class for ErsSessionState
    /// </summary>
    public class ErsSessionState
        : ErsState, ISession
    {
        public const string nickNameKey = "nickname";

        public ErsSessionState()
        {
        }

        /// <summary>
        /// Obtain an instance of ErsSessionStateModelPc
        /// </summary>
        /// <returns>Returns an instance of ErsSessionStateModelPc</returns>
        protected override ErsBindableModel GetModel()
        {
            return ErsFactory.ersSessionStateFactory.GetErsSessionStateModelPc();
        }

        /// <summary>
        /// Gets value of cookie expiration from setup.config.
        /// </summary>
        /// <returns>Returns cookieExpiration value</returns>
        protected override int? GetCookieExpiration(PropertyInfo prop)
        {
            var sourceExpirationAttr = (SourceExpirationAttribute)prop.GetCustomAttributes(typeof(SourceExpirationAttribute), false).FirstOrDefault();
            if (sourceExpirationAttr == null)
            {
                return null;
            }
            return ErsFactory.ersUtilityFactory.getSetup().GetCookieExpiration(sourceExpirationAttr.configKey);
        }

        /// <summary>
        /// Gets value of cookie Domain from setup.config.
        /// </summary>
        /// <returns>Returns cookieDomain value</returns>
        protected override string GetCookieDomain()
        {
            return ErsFactory.ersUtilityFactory.getSetup().cookieDomain;
        }

        /// <summary>
        /// Gets value of cookie path from setup.config.
        /// </summary>
        /// <returns>Returns cookiePath value</returns>
        protected override string GetCookiePath()
        {
            return ErsFactory.ersUtilityFactory.getSetup().cookiePath;
        }

        /// <summary>
        /// Gets boolean value of enableSSL from setup.config
        /// </summary>
        /// <returns>Returns boolean value of enableSSL</returns>
        protected override EnumRequireHttps GetEnableSSL()
        {
            return ErsFactory.ersUtilityFactory.getSetup().enableSSL;
        }

        /// <summary>
        /// Restore session
        /// </summary>
        /// <param name="request">Http request</param>
        public override void Restore(HttpCookieCollection Cookies,
            NameValueCollection Form,
            NameValueCollection QueryString)
        {
            base.Restore(Cookies, Form, QueryString);
        }

        /// <summary>
        /// Issue a new session of random characters.
        /// </summary>
        public virtual void getNewRansu()
        {
            ErsFactory.ersSessionStateFactory.GetObtainRansuStgy().GetNewRansu();
        }

        /// <summary>
        /// Issue a new secure session of random characters by member code.
        /// </summary>
        /// <param name="user_cd">member code</param>
        public virtual void getNewSSLransu(string mcode)
        {
            ErsFactory.ersSessionStateFactory.GetObtainRansuStgy().GetNewSSLRansu(mcode);
        }

        /// <summary>
        /// Get user login state.
        /// </summary>
        /// <returns>Returns user state</returns>
        public virtual EnumUserState getUserState()
        {
            return ErsFactory.ersSessionStateFactory.GetUserStateSpecification().getUserState();
        }

        /// <summary>
        /// Checks Password Expiration
        /// </summary>
        /// <returns>Returns throw exception</returns>
        public virtual bool CheckPasswordExpiration()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 正常なencode_ransuを保持しているか。
        /// </summary>
        /// <returns>Returns boolean result if the login is secured or not.</returns>
        public virtual bool IsLoginButNotSecure()
        {
            var decode_ransu = string.Empty;
            try
            {
                var encode_ransu = ErsContext.sessionState.Get("encode_ransu");
                var enc = ErsFactory.ersUtilityFactory.getErsEncryption();
                decode_ransu = enc.HexDecode(encode_ransu);
            }
            catch
            {
                //DecodeできないときはEmpty
            }

            var decode_mcode = this.GetDecodedMcode();

            return new specification.UserStateSpecification()
                .IsLoginButNotSecure(decode_ransu, decode_mcode);
        }

        /// <summary>
        /// encode_mcodeをデコードした値を返す
        /// </summary>
        /// <returns>Returns decoded member code.</returns>
        public virtual string GetDecodedMcode()
        {
            var decode_mcode = string.Empty;
            try
            {
                var encode_mcode = ErsContext.sessionState.Get("encode_mcode");
                var enc = ErsFactory.ersUtilityFactory.getErsEncryption();
                decode_mcode = enc.HexDecode(encode_mcode);
            }
            catch
            {
                //DecodeできないときはEmpty
            }

            return decode_mcode;
        }

        /// <summary>
        /// Gets member code based on email and password using ersMemberFactory.
        /// </summary>
        /// <param name="email">email address</param>
        /// <param name="passwd">password</param>
        /// <returns>Returns member code based on the specified criteria by email and password.</returns>
        public virtual string GetUserCode(string email, string passwd)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            criteria.email = email;
            criteria.passwd = passwd;
            criteria.deleted = EnumDeleted.NotDeleted;
            criteria.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();
            var list = repository.Find(criteria);
            if(list.Count != 1)
                return ErsMember.DEFAUTL_MCODE;

            return list[0].mcode;
        }

        /// <summary>
        /// Logout the user.
        /// </summary>
        public virtual void LogOut()
        {
            var ssl_ransu = ErsContext.sessionState.Get("ssl_ransu");
            var mcode = ErsContext.sessionState.Get("mcode");
            if (!string.IsNullOrEmpty(mcode))
            {
                this.UpdateRansuForLogout(ssl_ransu, mcode);
            }

            this.ClearLoginData();
        }

        private void UpdateRansuForLogout(string ssl_ransu, string mcode)
        {
            var objDB = ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(ErsWebDatabaseFactory.GetCloudConnectionStrings());
            var repository = ErsFactory.ersSessionStateFactory.GetErsRansuRepository(objDB);
            var criteria = ErsFactory.ersSessionStateFactory.GetErsRansuCriteria();
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            criteria.ssl_ransu = ssl_ransu;
            criteria.mcode = mcode;
            if (setup.Multiple_sites == true)
            {
                criteria.site_id = setup.site_id;
            }
            var listObj = repository.Find(criteria);
            if (listObj.Count == 0)
            {
                return;
            }
            if (listObj.Count != 1)
            {
                throw new Exception("ransu is duplicated");
            }
            var oldRansu = listObj.First();
            var newRansu = ErsFactory.ersSessionStateFactory.GetErsRansuWithId(oldRansu.id);
            newRansu.ssl_ransu = null;
            newRansu.mcode =ErsMember.DEFAUTL_MCODE;
            repository.Update(oldRansu, newRansu);
        }

        /// <summary>
        /// Throws ErsException with unauthorized message if this user don't have the specified permission.
        /// </summary>
        /// <param name="permissions">permission</param>
        /// <returns>Returns Exception</returns>
        public bool HasPermission(params string[] permissions)
        {
            throw new NotImplementedException();
        }
    }
}
