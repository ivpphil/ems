﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state
{
    /// <summary>
    /// Class for ErsSessionStateMobileFactory
    /// </summary>
    public class ErsSessionStateMobileFactory
        : ErsSessionStateFactory
    {
        /// <summary>
        /// Obtain an instance of ErsSessionStateMobile
        /// </summary>
        /// <returns>Returns an instance of ErsSessionStateMobile</returns>
        public override ErsSessionState getErsSessionState()
        {
            return new ErsSessionStateMobile();

        }
    }
}