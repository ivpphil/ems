﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.state
{
    public class ErsCache
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string ransu { get; set; }

        public string key { get; set; }

        public string value { get; set; }
    }
}
