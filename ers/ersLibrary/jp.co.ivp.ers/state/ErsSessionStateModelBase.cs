﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state
{
    public abstract class ErsSessionStateModelBase
            : ErsBindableModel
    {
        public abstract string log_user_id { get; }
    }
}
