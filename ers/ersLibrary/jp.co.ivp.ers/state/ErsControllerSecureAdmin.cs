﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Web.Mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.state
{
    /// <summary>
    /// Class for ErsControllerSecureAdmin
    /// </summary>
    public class ErsControllerSecureAdmin
        : ErsControllerBase
    {

        /// <summary>
        /// Obtain instance of ErsSessionStateAdmin
        /// </summary>
        /// <returns>Returns instance of ErsSessionStateAdmin</returns>
        protected override ErsState GetErsSessionState()
        {
            return ErsFactory.ersSessionStateFactory.getErsSessionStateAdmin();
        }

        /// <summary>
        /// Checks controller for the error execution.
        /// </summary>
        /// <returns>Returns Action to login with specified RouteData values.</returns>
        public virtual ActionResult ExecuteError()
        {
            //throw new ErsException("10203", ErsFactory.ersUtilityFactory.getSetup().sec_url);
            if (Convert.ToString(this.RouteData.Values["controller"]) != "Home")
            {
                this.RouteData.Values.Add("sessionError", "1");
            }
            return this.RedirectToAction("login", "login", this.RouteData.Values);
        }

        /// <summary>
        /// Controllerを初期化する。
        /// </summary>
        /// <param name="requestContext">Request context</param>
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            ErsContext.sessionState = this.GetErsSessionState();
            ErsContext.sessionState.Restore(Request.Cookies, Request.Form, Request.QueryString);
        }

        /// <summary>
        /// Gets user login state
        /// </summary>
        /// <returns>Returns user state</returns>
        protected override mvc.ErsState GetErsState()
        {
            return ErsContext.sessionState;
        }

        /// <summary>
        /// セッション情報をViewにセットする
        /// </summary>
        protected override void SetSessionToView()
        {
            //SessionをViewに出力
            ViewBag.SessionStateHidden = ErsContext.sessionState;

            ErsContext.sessionState.SetCookiesValue();
        }
    }

}
