﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state
{
    public class ErsPassRansu
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string mcode { get; set; }
        public string ransu { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
    }
}
