﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state
{
    public class ErsRansuCriteria
        : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("ransu_t.id", value, Operation.EQUAL));
            }
        }

        public string ssl_ransu
        {
            set
            {
                this.Add(Criteria.GetCriterion("ransu_t.ssl_ransu", value, Operation.EQUAL));
            }
        }

        public string mcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("ransu_t.mcode", value, Operation.EQUAL));
            }
        }
        public int site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("ransu_t.site_id", value, Operation.EQUAL));
            }
        }
        public string ransu
        {
            set
            {
                this.Add(Criteria.GetCriterion("ransu_t.ransu", value, Operation.EQUAL));
            }
        }

        public DateTime utime_grater_than
        {
            set
            {
                this.Add(Criteria.GetCriterion("ransu_t.utime", value, Operation.GREATER_THAN));
            }
        }

        public DateTime utime_less_than
        {
            set
            {
                this.Add(Criteria.GetCriterion("ransu_t.utime", value, Criteria.Operation.LESS_THAN));
            }
        }

        internal void SetActiveOnly()
        {
            int ransuExpiration = ErsFactory.ersUtilityFactory.getSetup().ransu_expiration;

            var dictionary = new Dictionary<string, object>();
            dictionary.Add("ransuExpiration", ransuExpiration);
            this.Add(Criteria.GetUniversalCriterion("ransu_t.utime > current_timestamp - (:ransuExpiration || ' minute')::interval", dictionary));
        }

        internal void SetCheckDuplicate(string ransu)
        {
            this.Add(Criteria.JoinWithOR(new[]{
                    Criteria.GetCriterion("ransu_t.ransu", ransu, Criteria.Operation.EQUAL),
                    Criteria.GetCriterion("ransu_t.ssl_ransu", ransu, Criteria.Operation.EQUAL)}));
        }
    }
}
