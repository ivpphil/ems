﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.state
{
    public class ErsRansuContactRepository
        : ErsRepository<ErsRansuContact>
    {
        public ErsRansuContactRepository()
            : base("ransu_contact_t")
        {
        }
    }
}
