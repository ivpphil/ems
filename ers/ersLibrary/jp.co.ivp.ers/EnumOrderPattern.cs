﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Enums for OrderPatter (Usual, Subscription, Purchase Item)
    /// Using [step_scenario_t.order_ptn_kbn]
    /// </summary>
    public enum EnumOrderPattern
    {
        /// <summary>
        /// 1 : 通常商品 [Usually]
        /// </summary>
        Usually = 1,

        /// <summary>
        /// 2 : 定期商品 [Subscription]
        /// </summary>
        Subscription = 2,

        /// <summary>
        /// 3 : 対象商品 [PurchaseItem]
        /// </summary>
        PurchaseItem = 3
    }
}
