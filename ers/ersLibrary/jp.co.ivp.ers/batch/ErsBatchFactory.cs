﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.batch
{
    /// <summary>
    /// Factory
    /// </summary>
    public class ErsBatchFactory
    {
        protected static SetupBatch _setup
        {
            get
            {
                return (SetupBatch)ErsCommonContext.GetPooledObject("_setupBatch");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_setupBatch", value);
            }
        }

        /// <summary>
        /// 設定情報を格納したクラスを取得する（参照用）
        /// </summary>
        /// <returns></returns>
        public virtual SetupBatch getSetup()
        {
            if (_setup == null)
                _setup = new SetupBatch();

            return _setup;
        }

        public virtual ErsSendMailBatch GetErsSendMailBatch()
        {
            return new ErsSendMailBatch();
        }

        public virtual ErsBatchScheduleRepository GetErsBatchScheduleRepository()
        {
            return new ErsBatchScheduleRepository();
        }

        public virtual ErsBatchScheduleRepository GetErsBatchScheduleRepository(db.ErsDatabase objDB)
        {
            return new ErsBatchScheduleRepository(objDB);
        }

        public virtual ErsBatchScheduleCriteria GetErsBatchScheduleCriteria()
        {
            return new ErsBatchScheduleCriteria();
        }

        public virtual ErsBatchSchedule GetErsBatchSchedule()
        {
            return new ErsBatchSchedule();
        }

        public virtual ErsBatchSchedule GetErsBatchScheduleWithParameter(Dictionary<string, object> dr)
        {
            var objBatchSchedule = this.GetErsBatchSchedule();
            objBatchSchedule.OverwriteWithParameter(dr);
            return objBatchSchedule;
        }

        public virtual ErsBatchSequenceRepository GetErsBatchSequenceRepository()
        {
            return new ErsBatchSequenceRepository();
        }

        public virtual ErsBatchSequenceCriteria GetErsBatchSequenceCriteria()
        {
            return new ErsBatchSequenceCriteria();
        }

        public virtual ErsBatchSequence GetErsBatchSequence()
        {
            return new ErsBatchSequence();
        }

        public virtual ErsBatchSequence GetErsBatchSequenceWithParameter(Dictionary<string, object> dr)
        {
            var objBatchSequence = this.GetErsBatchSequence();
            objBatchSequence.OverwriteWithParameter(dr);
            return objBatchSequence;
        }

        public virtual ErsBatchSchedule GetErsBatchScheduleWithBatchId(string batchId)
        {
            var repository = this.GetErsBatchScheduleRepository();
            var criteria = this.GetErsBatchScheduleCriteria();
            criteria.batch_id = batchId;
            var listBatchSchedule = repository.Find(criteria);
            if (listBatchSchedule.Count == 0)
            {
                return null;
            }
            return listBatchSchedule.First();
        }

        public virtual ErsBatchImmediateRepository GetErsBatchImmediateRepository()
        {
            return new ErsBatchImmediateRepository();
        }

        public virtual ErsBatchImmediateRepository GetErsBatchImmediateRepository(db.ErsDatabase objDB)
        {
            return new ErsBatchImmediateRepository(objDB);
        }

        public virtual ErsBatchImmediateCriteria GetErsBatchImmediateCriteria()
        {
            return new ErsBatchImmediateCriteria();
        }

        public virtual ErsBatchImmediate GetErsBatchImmediate()
        {
            return new ErsBatchImmediate();
        }
    }
}
