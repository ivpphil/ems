﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.template;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.common;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers.coupon;
using jp.co.ivp.ers.ctsorder;
using jp.co.ivp.ers.Inquiry;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc.compile;
using jp.co.ivp.ers.mvc.compile.entity;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.step_scenario;
using jp.co.ivp.ers.stepmail;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.warehouse;
using jp.co.ivp.ers.summary;
using jp.co.ivp.ers.ranking;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers.target;
using jp.co.ivp.ers.doc_bundle;
using jp.co.ivp.ers.update_specified_column;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.request;

namespace jp.co.ivp.ers.batch
{
    public class ErsBatchEnvironment
        : IErsBatchEnvironment
    {
        public void Initialization(string applicationRootPath)
        {
            ErsCommonContext.Initialize(null, applicationRootPath);

            MessageResourceDictionary.Reload();
            FieldNameResourceDictionary.Reload();

            SetFactory();

            ErsTemplateParser.ParserList = GetErsTagParserList();
        }

        /// <summary>
        /// ファクトリーセット
        /// </summary>
        protected void SetFactory()
        {
            ErsFactory.ersBasketFactory = new ErsBasketFactory();

            ErsFactory.ersMerchandiseFactory = new ErsMerchandiseFactory();

            ErsFactory.ersMemberFactory = new ErsMemberFactory();

            ErsFactory.ersMailFactory = new ErsMailFactory();

            ErsFactory.ersContentsFactory = new ErsContentsFactory();

            ErsFactory.ersSessionStateFactory = new ErsSessionStateFactory();

            ErsFactory.ersViewServiceFactory = new ErsViewServiceFactory();

            ErsFactory.ersUtilityFactory = new ErsUtilityFactory();

            ErsFactory.ersAddressInfoFactory = new ErsAddressInfoFactory();

            ErsFactory.ersPointHistoryFactory = new ErsPointHistoryFactory();

            ErsFactory.ersAdministratorFactory = new ErsAdministratorFactory();

            ErsFactory.ErsAtMailFactory = new ErsAtMailFactory();

            ErsFactory.ersTargetFactory = new ErsTargetFactory();

            ErsFactory.ersDocBundleFactory = new ErsDocBundleFactory();

            ErsFactory.ersCouponFactory = new ErsCouponFactory();

            ErsFactory.ersBatchFactory = new ErsBatchFactory();

            ErsFactory.ersOrderFactory = new ErsOrderFactory();

            ErsFactory.ersCtsOrderFactory = new ErsCtsOrderFactory();

            ErsFactory.ersStepScenarioFactory = new ErsStepScenarioFactory();

            ErsFactory.ersStepMailFactory = new ErsStepMailFactory();

            ErsFactory.ersCtsInquiryFactory = new ErsCtsInquiryFactory();

            ErsFactory.ersCommonFactory = new ErsCommonFactory();

            ErsFactory.ersWarehouseFactory = new ErsWarehouseFactory();

            ErsFactory.ersSummaryFactory = new ErsSummaryFactory();

            ErsFactory.ersRankingFactory = new ErsRankingFactory();

            ErsFactory.ersLpFactory = new ErsLpFactory();

            ErsFactory.ersUpdateSpecifiedColumnFactory = new ErsUpdateSpecifiedColumnFactory();

            ErsFactory.ersTableSequenceFactory = new ErsTableSequenceFactory();

            ErsFactory.ersEmployeeFactory = new ErsEmployeeFactory();

            ErsFactory.ersRequestFactory = new ErsRequestFactory();
        }

        /// <summary>
        /// ERS独自タグのパーサーのリストを取得
        /// </summary>
        /// <returns>パーサーのリスト</returns>
        public List<ErsTemplateEntityBase> GetErsTagParserList()
        {
            var retVal = new List<ErsTemplateEntityBase>();

            retVal.Add(new ErsTagOutputQuery());
            retVal.Add(new ErsTemplateForm());
            retVal.Add(new ErsTemplateAnchor());
            retVal.Add(new ErsTagPartial());
            retVal.Add(new ErsTagDispError());
            retVal.Add(new ErsTagIsError());
            retVal.Add(new ErsTagForeach());
            retVal.Add(new ErsTagIsEqual());
            retVal.Add(new ErsTagIsNotEqual());
            retVal.Add(new ErsTagIsElse());
            retVal.Add(new ErsTagFormatString());
            retVal.Add(new ErsTagFormatCurrency());
            retVal.Add(new ErsTagFormatNumber());
            retVal.Add(new ErsTagFormatDate());
            retVal.Add(new ErsTagMultiLine());
            retVal.Add(new ErsTagTable());
            retVal.Add(new ErsTagNonEncode());
            retVal.Add(new ErsRemoveInvalidInputAttributes());

            return retVal;
        }
    }
}
