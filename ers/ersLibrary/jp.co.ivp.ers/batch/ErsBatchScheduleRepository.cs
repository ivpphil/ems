﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.batch
{
    public class ErsBatchScheduleRepository
        : ErsRepository<ErsBatchSchedule>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsBatchScheduleRepository()
            : base("batch_schedule_t")
        {
        }

        public ErsBatchScheduleRepository(ErsDatabase objDB)
            : base("batch_schedule_t", objDB)
        {
        }
    }
}
