﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.batch
{
    public class ErsBatchImmediateCriteria : Criteria
    {
        public string batch_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("batch_immediate_t.batch_id", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("batch_immediate_t.active", value, Operation.EQUAL));
            }
        }

        public DateTime? execute_date_less_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("batch_immediate_t.execute_date", value, Operation.LESS_EQUAL));
            }
        }

        public void SetOrderByid(OrderBy orderBy)
        {
            this.AddOrderBy("batch_immediate_t.id", orderBy);
        }

        public void SetOrderByExecute_date(OrderBy orderBy)
        {
            this.AddOrderBy("batch_immediate_t.execute_date", orderBy);
        }
    }
}
