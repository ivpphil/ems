﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.batch
{
    public class ErsBatchScheduleCriteria
        : Criteria
    {
        public string batch_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("batch_schedule_t.batch_id", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("batch_schedule_t.active", value, Operation.EQUAL));
            }
        }

        public void SetOrderByid(OrderBy orderBy)
        {
            this.AddOrderBy("batch_schedule_t.id", orderBy);
        }

        public void SetOrderByLastDate(OrderBy orderBy)
        {
            this.AddOrderBy("batch_schedule_t.last_date", orderBy);
        }

        public void SetOrderByBatch_id(OrderBy orderBy)
        {
            this.AddOrderBy("batch_schedule_t.batch_id", orderBy);
        }
    }
}
