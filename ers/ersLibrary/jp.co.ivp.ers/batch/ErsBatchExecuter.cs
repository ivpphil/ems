﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.batch.util;

namespace jp.co.ivp.ers.batch
{
    public class ErsBatchExecuter
        : IErsBatchExecuter
    {
        #region IErsBatchScheduler メンバー

        /// <summary>
        /// バッチの一覧を取得
        /// </summary>
        /// <param name="executeDate"></param>
        /// <returns></returns>
        public IList<BatchDataContainer> GetBatchDataList(DateTime executeDate)
        {
            var repository = ErsFactory.ersBatchFactory.GetErsBatchScheduleRepository();
            var criteria = ErsFactory.ersBatchFactory.GetErsBatchScheduleCriteria();
            criteria.active = EnumActive.Active;
            criteria.SetOrderByLastDate(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByid(Criteria.OrderBy.ORDER_BY_ASC);
            var listBatchSchedule = repository.Find(criteria);

            var listBatchContainer = new List<BatchDataContainer>();
            foreach (var batchSchedule in listBatchSchedule)
            {
                var batchContainer = new BatchDataContainer();
                batchContainer.batchId = batchSchedule.batch_id;
                batchContainer.batchName = batchSchedule.batch_name;
                batchContainer.schedule = batchSchedule.schedule;
                batchContainer.lastDate = batchSchedule.last_date ?? batchSchedule.intime;
                batchContainer.lastSuccessDate = batchSchedule.last_success_date;
                listBatchContainer.Add(batchContainer);
            }
            return listBatchContainer;
        }

        public void ExecuteClass(BatchDataContainer batchContainer)
        {
            var executeDate = batchContainer.executeDate;

            try
            {
                //バッチの実行
                var objCommand = (IErsBatchCommand)ErsReflection.CreateInstanceFromFullName(batchContainer.class_name);

                if (objCommand == null)
                {
                    throw new Exception(batchContainer.class_name + " is not found.");
                }

                var options = ErsObtainOption.Obtain(ErsObtainOption.SplitArgs(batchContainer.options), false);

                options["last_success_date"] = batchContainer.lastSuccessDate;

                objCommand.Run(batchContainer.batchName, executeDate, options, batchContainer.lastDate, batchContainer.batchLocation);
            }
            catch (Exception ex)
            {
                //クラス名を保持するために、ErsBatchStepExceptionでラップする
                throw new ErsBatchStepException(batchContainer.class_name, ex);
            }
        }

        /// <summary>
        /// バッチの実行を行う
        /// </summary>
        /// <param name="batchContainer"></param>
        public void Execute(BatchDataContainer batchContainer)
        {
            var executeDate = batchContainer.executeDate;

            // バッチが成功したか否か。finally句にて使用
            bool isError = false;
            try
            {
                using (var objDB = ErsCommonsSetting.ersDatabaseFactory.GetNewErsDatabase())
                {
                    // アプリケーション名セット
                    this.SetApplicationName(batchContainer.batchId, objDB);

                    using (var transaction = ErsDB_parent.BeginTransaction(objDB))
                    {
                        //対象のバッチのレコードをロックする
                        if (!this.LockBatchRecord(batchContainer.batchId, objDB, executeDate))
                        {
                            return;
                        }

                        //バッチ実行テーブルより、実行クラス一覧を取得
                        var listErsBatchSequence = this.GetListErsBatchSequence(batchContainer);

                        foreach (var ersBatchSequence in listErsBatchSequence)
                        {
                            try
                            {
                                //バッチの実行
                                var objCommand = (IErsBatchCommand)ErsReflection.CreateInstanceFromFullName(ersBatchSequence.class_name);

                                if (objCommand == null)
                                {
                                    throw new Exception(ersBatchSequence.class_name + " is not found.");
                                }

                                var options = ErsObtainOption.Obtain(ErsObtainOption.SplitArgs(ersBatchSequence.options), false);

                                options["last_success_date"] = batchContainer.lastSuccessDate;

                                objCommand.Run(batchContainer.batchName, executeDate, options, batchContainer.lastDate, batchContainer.batchLocation);
                            }
                            catch (Exception ex)
                            {
                                //クラス名を保持するために、ErsBatchStepExceptionでラップする
                                throw new ErsBatchStepException(ersBatchSequence.class_name, ex);
                            }
                        }

                        //トランザクションはレコードをロックするためだけなので、コミットは不要
                        //transaction.Commit();
                    }
                }
            }
            catch
            {
                isError = true;
                throw;
            }
            finally
            {
                try
                {
                    //エラーがあったかどうかにかかわらず、最終実行時間更新を行う
                    this.UpdateLastDate(batchContainer.batchId, executeDate, isError);

                    this.UpdateImmediateDone(batchContainer.executeDate, batchContainer.batchId);
                }
                catch (Exception)
                {
                    //エラー時の最終実行時間更新でエラーが有った場合は、そのエラーはもみ消す。
                }
            }
        }

        /// <summary>
        /// 最終実行時間更新を行う
        /// </summary>
        /// <param name="batchId"></param>
        /// <param name="executeDate"></param>
        private void UpdateLastDate(string batchId, DateTime? executeDate, bool isError)
        {
            using (var objDB = ErsCommonsSetting.ersDatabaseFactory.GetNewErsDatabase())
            {
                var repository = ErsFactory.ersBatchFactory.GetErsBatchScheduleRepository(objDB);

                var newBatchSchedule = ErsFactory.ersBatchFactory.GetErsBatchScheduleWithBatchId(batchId);
                newBatchSchedule.last_date = executeDate;
                if (!isError)
                {
                    //エラーがない場合は最終成功日時を更新
                    newBatchSchedule.last_success_date = executeDate;
                }

                var oldBatchSchedule = ErsFactory.ersBatchFactory.GetErsBatchScheduleWithBatchId(batchId);

                repository.Update(oldBatchSchedule, newBatchSchedule);
            }
        }

        /// <summary>
        /// バッチ実行テーブルより、実行クラス一覧を取得
        /// </summary>
        /// <param name="batchContainer"></param>
        /// <returns></returns>
        private IList<ErsBatchSequence> GetListErsBatchSequence(BatchDataContainer batchContainer)
        {
            var repository = ErsFactory.ersBatchFactory.GetErsBatchSequenceRepository();
            var criteria = ErsFactory.ersBatchFactory.GetErsBatchSequenceCriteria();
            criteria.batch_id = batchContainer.batchId;
            criteria.active = EnumActive.Active;
            criteria.SetOrderByExecuteOrder(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByid(Criteria.OrderBy.ORDER_BY_ASC);
            return repository.Find(criteria);
        }

        /// <summary>
        /// アプリケーション名セット
        /// </summary>
        /// <param name="batchId"></param>
        /// <param name="objDB"></param>
        private void SetApplicationName(string batchId, ErsDatabase objDB)
        {
            var sql = string.Format("SET application_name = 'ERS.batch.{0}'", batchId);
            ErsDB_universal.GetInstance(objDB).ExecuteQuery(sql);
        }

        /// <summary>
        /// 対象のバッチのレコードをロックする
        /// </summary>
        /// <param name="batchId"></param>
        private bool LockBatchRecord(string batchId, ErsDatabase objDB, DateTime? executeDate)
        {
            var repository = ErsFactory.ersBatchFactory.GetErsBatchScheduleRepository(objDB);
            var criteria = ErsFactory.ersBatchFactory.GetErsBatchScheduleCriteria();
            criteria.batch_id = batchId;
            criteria.ForUpdate = true;
            var listRecord = repository.Find(criteria);
            if (listRecord.Count > 0)
            {
                //ロック解除されたあとに、実行対象外となる可能性があるので、最終実行日が、「バッチ実行時」より前であれば処理を続行。
                var record = listRecord.First();
                if (record.last_date < executeDate)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 即時実行の指示があるかをチェックする。
        /// </summary>
        /// <param name="executeDate"></param>
        /// <param name="batch_id"></param>
        /// <returns></returns>
        public bool CheckExistsImmediate(DateTime? executeDate, string batch_id)
        {
            var repository = ErsFactory.ersBatchFactory.GetErsBatchImmediateRepository();
            var criteria = ErsFactory.ersBatchFactory.GetErsBatchImmediateCriteria();
            criteria.batch_id = batch_id;
            criteria.execute_date_less_equal = executeDate;
            criteria.active = EnumActive.Active;

            return (repository.GetRecordCount(criteria) > 0);
        }

        /// <summary>
        /// 実行済みの即時実行の指示を無効化する。
        /// </summary>
        /// <param name="nullable"></param>
        public void UpdateImmediateDone(DateTime? executeDate, string batch_id)
        {
            using (var objDB = ErsCommonsSetting.ersDatabaseFactory.GetNewErsDatabase())
            {
                var repository = ErsFactory.ersBatchFactory.GetErsBatchImmediateRepository(objDB);
                var criteria = ErsFactory.ersBatchFactory.GetErsBatchImmediateCriteria();
                criteria.batch_id = batch_id;
                criteria.execute_date_less_equal = executeDate;
                criteria.active = EnumActive.Active;
                var listImmediate = repository.Find(criteria);
                foreach (var objImmediate in listImmediate)
                {
                    var newImmediate = ErsFactory.ersBatchFactory.GetErsBatchImmediate();
                    newImmediate.OverwriteWithParameter(objImmediate.GetPropertiesAsDictionary());
                    newImmediate.active = EnumActive.NonActive;
                    repository.Update(objImmediate, newImmediate);
                }
            }
        }
        #endregion
    }
}
