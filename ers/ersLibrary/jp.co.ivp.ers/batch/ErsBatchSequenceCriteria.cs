﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.batch
{
    public class ErsBatchSequenceCriteria
        : Criteria
    {
        public string batch_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("batch_sequence_t.batch_id", value, Operation.EQUAL));
            }
        }

        public EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("batch_sequence_t.active", value, Operation.EQUAL));
            }
        }

        internal void SetOrderByExecuteOrder(OrderBy orderBy)
        {
            this.AddOrderBy("batch_sequence_t.execute_order", orderBy);
        }

        internal void SetOrderByid(OrderBy orderBy)
        {
            this.AddOrderBy("batch_sequence_t.id", orderBy);
        }
    }
}
