﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch
{
    public class ErsBatchImmediate : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string batch_id { get; set; }

        public DateTime? execute_date { get; set; }

        public DateTime? intime { get; set; }

        public DateTime? utime { get; set; }

        public EnumActive? active { get; set; }
    }
}
