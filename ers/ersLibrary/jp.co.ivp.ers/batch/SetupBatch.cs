﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise.stock;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.batch
{
    public class SetupBatch
        : Setup
    {
        #region "コンタクトメール設定"
        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        /// </summary>
        public virtual string contactMailReceptionMailServer
        {
            get
            {
                return GetSiteConfigFileValue("contactMailReceptionMailServer");
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        /// </summary>
        public virtual string contactMailReceptionMailServerAccount
        {
            get
            {
                return GetSiteConfigFileValue("contactMailReceptionMailServerAccount");
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        /// </summary>
        public virtual string contactMailReceptionMailServerPass
        {
            get
            {
                return GetSiteConfigFileValue("contactMailReceptionMailServerPass");
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        /// </summary>
        public virtual int contactMailReceptionMailServerPort
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("contactMailReceptionMailServerPort"));
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        ///  SSL接続か否か
        /// </summary>
        public virtual Boolean contactMailReceptionMailServerSslConnection
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("contactMailReceptionMailServerSslConnection"));
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        ///  1=POP or 2=IMAP
        /// </summary>
        public virtual EnumMailerType contactMailReceptionMailServerReceptionType
        {
            get
            {
                return (EnumMailerType)Convert.ToInt32(GetSiteConfigFileValue("contactMailReceptionMailServerReceptionType"));
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        ///  IMAP用　既読メールのフォルダ名 POP時は記入は無視
        /// </summary>
        public virtual string contactMailReceptionReadMailDirName
        {
            get
            {
                return GetSiteConfigFileValue("contactMailReceptionReadMailDirName");
            }
        }

        #region multiple

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        /// </summary>
        public virtual string multiple_contactMailReceptionMailServer(int? site_id)
        {
            return GetSiteConfigFileValue("site" + site_id + "_contactMailReceptionMailServer");
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        /// </summary>
        public virtual string multiple_contactMailReceptionMailServerAccount(int? site_id)
        {
            return GetSiteConfigFileValue("site" + site_id + "_contactMailReceptionMailServerAccount");
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        /// </summary>
        public virtual string multiple_contactMailReceptionMailServerPass(int? site_id)
        {
            return GetSiteConfigFileValue("site" + site_id + "_contactMailReceptionMailServerPass");
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        /// </summary>
        public virtual int multiple_contactMailReceptionMailServerPort(int? site_id)
        {
            return Convert.ToInt32(GetSiteConfigFileValue("site" + site_id + "_contactMailReceptionMailServerPort"));
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        ///  SSL接続か否か
        /// </summary>
        public virtual Boolean multiple_contactMailReceptionMailServerSslConnection(int? site_id)
        {
            return Convert.ToBoolean(GetSiteConfigFileValue("site" + site_id + "_contactMailReceptionMailServerSslConnection"));
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        ///  1=POP or 2=IMAP
        /// </summary>
        public virtual EnumMailerType multiple_contactMailReceptionMailServerReceptionType(int? site_id)
        {
            return (EnumMailerType)Convert.ToInt32(GetSiteConfigFileValue("site" + site_id + "_contactMailReceptionMailServerReceptionType"));
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        ///  IMAP用　既読メールのフォルダ名 POP時は記入は無視
        /// </summary>
        public virtual string multiple_contactMailReceptionReadMailDirName(int? site_id)
        {
            return GetSiteConfigFileValue("site" + site_id + "_contactMailReceptionReadMailDirName");
        }

        #endregion

        #endregion

        #region "CreateListForStepMail ステップメール配信リスト生成"
        /// <summary>
        /// 配信数制限
        /// </summary>
        public virtual int stepmail_send_max_count
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("stepmail_send_max_count"));
            }
        }
        #endregion

        #region "creditAccountLaunderingDownload GMO洗い替えファイルダウンロード"

        public string creditAccountLaunderingDownloadTempFilePath
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingDownloadTempFilePath");
            }
        }

        public string creditAccountLaunderingDownloadSftpHost
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingDownloadSftpHost");
            }
        }

        public string creditAccountLaunderingDownloadSftpUser
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingDownloadSftpUser");
            }
        }

        public string creditAccountLaunderingDownloadSftpPass
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingDownloadSftpPass");
            }
        }

        public int? creditAccountLaunderingDownloadSftpPort
        {
            get
            {
                var strPort = GetSiteConfigFileValue("creditAccountLaunderingDownloadSftpPort");
                if (string.IsNullOrEmpty(strPort))
                {
                    return null;
                }
                else
                {
                    return Convert.ToInt32(strPort);
                }
            }
        }

        public string creditAccountLaunderingDownloadSftpSshKeyPath
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingDownloadSftpSshKeyPath");
            }
        }

        public string creditAccountLaunderingDownloadSftpSshPassPhrase
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingDownloadSftpSshPassPhrase");
            }
        }

        public string creditAccountLaunderingDownloadSftpUserSftpPutPath
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingDownloadSftpUserSftpPutPath");
            }
        }
        #endregion

        #region "CreditAccountLaunderingUpload GMO洗い替えファイルアップロード"

        public string creditAccountLaunderingUploadTempFilePath
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingUploadTempFilePath");
            }
        }

        public string creditAccountLaunderingUploadSftpHost
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingUploadSftpHost");
            }
        }

        public string creditAccountLaunderingUploadSftpUser
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingUploadSftpUser");
            }
        }

        public string creditAccountLaunderingUploadSftpPass
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingUploadSftpPass");
            }
        }

        public int? creditAccountLaunderingUploadSftpPort
        {
            get
            {
                var strPort = GetSiteConfigFileValue("creditAccountLaunderingUploadSftpPort");
                if (string.IsNullOrEmpty(strPort))
                {
                    return null;
                }
                else
                {
                    return Convert.ToInt32(strPort);
                }
            }
        }

        public string creditAccountLaunderingUploadSftpSshKeyPath
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingUploadSftpSshKeyPath");
            }
        }

        public string creditAccountLaunderingUploadSftpSshPassPhrase
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingUploadSftpSshPassPhrase");
            }
        }

        public string creditAccountLaunderingUploadSftpUserSftpPutPath
        {
            get
            {
                return GetSiteConfigFileValue("creditAccountLaunderingUploadSftpUserSftpPutPath");
            }
        }
        #endregion

        #region "CreditContinualBillingUpload 継続課金CSV送信"

        public string creditContinualBillingUploadTempFilePath
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingUploadTempFilePath");
            }
        }

        public string creditContinualBillingUploadSftpHost
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingUploadSftpHost");
            }
        }

        public string creditContinualBillingUploadSftpUser
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingUploadSftpUser");
            }
        }

        public string creditContinualBillingUploadSftpPass
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingUploadSftpPass");
            }
        }

        public int? creditContinualBillingUploadSftpPort
        {
            get
            {
                var strPort = GetSiteConfigFileValue("creditContinualBillingUploadSftpPort");
                if (string.IsNullOrEmpty(strPort))
                {
                    return null;
                }
                else
                {
                    return Convert.ToInt32(strPort);
                }
            }
        }

        public string creditContinualBillingUploadSftpSshKeyPath
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingUploadSftpSshKeyPath");
            }
        }

        public string creditContinualBillingUploadSftpSshPassPhrase
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingUploadSftpSshPassPhrase");
            }
        }

        public string creditContinualBillingUploadSftpUserSftpPutPath
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingUploadSftpUserSftpPutPath");
            }
        }
        #endregion

        #region "creditContinualBillingDownload 継続課金CSVダウンロード"

        public string creditContinualBillingDownloadTempFilePath
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingDownloadTempFilePath");
            }
        }

        public string creditContinualBillingDownloadSftpHost
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingDownloadSftpHost");
            }
        }

        public string creditContinualBillingDownloadSftpUser
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingDownloadSftpUser");
            }
        }

        public string creditContinualBillingDownloadSftpPass
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingDownloadSftpPass");
            }
        }

        public int? creditContinualBillingDownloadSftpPort
        {
            get
            {
                var strPort = GetSiteConfigFileValue("creditContinualBillingDownloadSftpPort");
                if (string.IsNullOrEmpty(strPort))
                {
                    return null;
                }
                else
                {
                    return Convert.ToInt32(strPort);
                }
            }
        }

        public string creditContinualBillingDownloadSftpSshKeyPath
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingDownloadSftpSshKeyPath");
            }
        }

        public string creditContinualBillingDownloadSftpSshPassPhrase
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingDownloadSftpSshPassPhrase");
            }
        }

        public string creditContinualBillingDownloadSftpUserSftpPutPath
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingDownloadSftpUserSftpPutPath");
            }
        }

        public string creditContinualBillingDownloadGmoErrMailTitle
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingDownloadGmoErrMailTitle");
            }
        }

        public string creditContinualBillingDownloadGmoErrMailTo
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingDownloadGmoErrMailTo");
            }
        }

        public string creditContinualBillingDownloadGmoErrMailFrom
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingDownloadGmoErrMailFrom");
            }
        }

        public string creditContinualBillingDownloadGmoErrMailCc
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingDownloadGmoErrMailCc");
            }
        }

        public string creditContinualBillingDownloadGmoErrMailBcc
        {
            get
            {
                return GetSiteConfigFileValue("creditContinualBillingDownloadGmoErrMailBcc");
            }
        }
        #endregion

        #region "＠メール"
        public string MassSend_hostname
        {
            get
            {
                return GetSiteConfigFileValue("MassSend_hostname");
            }
        }

        public int? MassSend_port
        {
            get
            {
                int resultValue;
                if (Int32.TryParse(GetSiteConfigFileValue("MassSend_port"), out resultValue))
                {
                    return resultValue;
                }

                return null;
            }
        }

        public int? MassSend_sendAtOnce
        {
            get
            {
                int resultValue;
                if (Int32.TryParse(GetSiteConfigFileValue("MassSend_sendAtOnce"), out resultValue))
                {
                    return resultValue;
                }

                return null;
            }
        }

        public int? MassSend_restInterval
        {
            get
            {
                int resultValue;
                if (Int32.TryParse(GetSiteConfigFileValue("MassSend_restInterval"), out resultValue))
                {
                    return resultValue;
                }

                return null;
            }
        }

        public int? MassSendManager_ProcessesNumber
        {
            get
            {
                int resultValue;
                if (Int32.TryParse(GetSiteConfigFileValue("MassSendManager_ProcessesNumber"), out resultValue))
                {
                    return resultValue;
                }

                return null;
            }
        }

        public int? MassSendManager_stopTime
        {
            get
            {
                int resultValue;
                if (Int32.TryParse(GetSiteConfigFileValue("MassSendManager_stopTime"), out resultValue))
                {
                    return resultValue;
                }

                return null;
            }
        }

        /// <summary>
        /// 送信されていないメールの監視時間
        /// </summary>
        public int MonitorMassSendPastMinutes
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("MonitorMassSendPastMinutes"));
            }
        }
        #endregion

        #region "ポイント付与/失効"
        /// <summary>
        /// Gets number of months until expiration point using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual int monthsExpirationPoint
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("monthsExpirationPoint"));
            }
        }

        /// <summary>
        /// Gets Number of days until the grant point using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual int daysGivePoint
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("daysGivePoint"));
            }
        }
        #endregion

        #region "RegularOrderBilling クレジット決済処理"

        public string regularOrderBillingResultMailTitle
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingResultMailTitle");
            }
        }

        public string regularOrderBillingResultMailTo
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingResultMailTo");
            }
        }

        public string regularOrderBillingResultMailFrom
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingResultMailFrom");
            }
        }

        public string regularOrderBillingResultMailBcc
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingResultMailBcc");
            }
        }

        public string regularOrderBillingResultMailCc
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingResultMailCc");
            }
        }

        public string regularOrderBillingGmoErrMailTo
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingGmoErrMailTo");
            }
        }

        public string regularOrderBillingGmoErrMailCc
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingGmoErrMailCc");
            }
        }

        public string regularOrderBillingGmoErrMailBcc
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingGmoErrMailBcc");
            }
        }

        public string regularOrderBillingGmoErrMailFrom
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingGmoErrMailFrom");
            }
        }

        public string regularOrderBillingGmoErrMailTitle
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingGmoErrMailTitle");
            }
        }

        public string regularOrderBillingStockErrMailTitle
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingStockErrMailTitle");
            }
        }

        public string regularOrderBillingStockErrMailTo
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingStockErrMailTo");
            }
        }

        public string regularOrderBillingStockErrMailFrom
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingStockErrMailFrom");
            }
        }

        public string regularOrderBillingStockErrMailCc
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingStockErrMailCc");
            }
        }

        public string regularOrderBillingStockErrMailBcc
        {
            get
            {
                return GetSiteConfigFileValue("regularOrderBillingStockErrMailBcc");
            }
        }
        #endregion

        #region Ranking of units sold(ランキング生成バッチ)
        public virtual int rankingTerm
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("rankingTerm"));
            }
        }

        public virtual int rankingLimit
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("rankingLimit"));
            }
        }
        #endregion

        #region ProductStockAlertail(在庫アラートメール)
        public string ProductStockAlertMail_email_address
        {
            get
            {
                return GetSiteConfigFileValue("stock_recipient_email_address");

            }
        }

        public string ProductStockAlertMail_mail_title
        {
            get
            {
                return GetSiteConfigFileValue("stock_recipient_mail_title");

            }
        }
        #endregion

        #region CtsStockRelease
        public int stock_reservation_period
        {
            get
            {
                int resultValue;
                if (Int32.TryParse(GetSiteConfigFileValue("stock_reservation_period"), out resultValue))
                {
                    return resultValue;
                }

                return 0;
            }
        }
        #endregion

        #region アップデートバッチ

        public string updateSpecifiedColumnAlertMailTitle
        {
            get
            {
                return GetSiteConfigFileValue("updateSpecifiedColumnAlertMailTitle");

            }
        }

        public string updateSpecifiedColumnAlertMailTo
        {
            get
            {
                return GetSiteConfigFileValue("updateSpecifiedColumnAlertMailTo");

            }
        }

        #endregion アップデートバッチ

        #region ダウンロードバッチ

        public string downloadSpecifiedColumnAlertMailTitle
        {
            get
            {
                return GetSiteConfigFileValue("downloadSpecifiedColumnAlertMailTitle");

            }
        }

        public string downloadSpecifiedColumnAlertMailTo
        {
            get
            {
                return GetSiteConfigFileValue("downloadSpecifiedColumnAlertMailTo");

            }
        }

        #endregion ダウンロードバッチ

        #region インサートバッチ

        public string insertSpecifiedColumnAlertMailTitle
        {
            get
            {
                return GetSiteConfigFileValue("insertSpecifiedColumnAlertMailTitle");

            }
        }

        public string insertSpecifiedColumnAlertMailTo
        {
            get
            {
                return GetSiteConfigFileValue("insertSpecifiedColumnAlertMailTo");

            }
        }

        #endregion インサートバッチ

        #region インサートバッチ

        public string deleteSpecifiedColumnAlertMailTitle
        {
            get
            {
                return GetSiteConfigFileValue("deleteSpecifiedColumnAlertMailTitle");

            }
        }

        public string deleteSpecifiedColumnAlertMailTo
        {
            get
            {
                return GetSiteConfigFileValue("deleteSpecifiedColumnAlertMailTo");

            }
        }

        public string defaultCaseInquiryMailTitle
        {
            get
            {
                return GetSiteConfigFileValue("defaultCaseInquiryMailTitle");
            }
        }

        #endregion インサートバッチ
    }
}
