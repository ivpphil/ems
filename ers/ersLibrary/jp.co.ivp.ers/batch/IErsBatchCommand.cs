﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.batch
{
    public interface IErsBatchCommand
    {
        /// <summary>
        /// バッチの実行を行う
        /// </summary>
        /// <param name="batchName"></param>
        /// <param name="executeDate"></param>
        /// <param name="options"></param>
        /// <param name="lastDate"></param>
        void Run(string batchName, DateTime? executeDate, IDictionary<string, object> options, DateTime? lastDate, string batchLocation);
    }
}
