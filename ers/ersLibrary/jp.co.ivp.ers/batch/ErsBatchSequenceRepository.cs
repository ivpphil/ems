﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.batch
{
    public class ErsBatchSequenceRepository
        : ErsRepository<ErsBatchSequence>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsBatchSequenceRepository()
            : base("batch_sequence_t")
        {
        }

        public ErsBatchSequenceRepository(ErsDatabase objDB)
            : base("batch_sequence_t", objDB)
        {
        }
    }
}
