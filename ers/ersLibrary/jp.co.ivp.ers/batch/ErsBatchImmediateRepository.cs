﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.batch
{
    public class ErsBatchImmediateRepository : ErsRepository<ErsBatchImmediate>
    {
        public ErsBatchImmediateRepository()
            : base("batch_immediate_t")
        {
        }

        public ErsBatchImmediateRepository(ErsDatabase objDB)
            : base("batch_immediate_t", objDB)
        {
        }
    }
}
