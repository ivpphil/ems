﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.batch
{
    public class ErsBatchSequence
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string batch_id { get; set; }

        public int? execute_order { get; set; }

        public string class_name { get; set; }

        public string options { get; set; }

        public DateTime? intime { get; set; }

        public DateTime? utime { get; set; }

        public EnumActive? active { get; set; }
    }
}
