﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.ctsorder
{
    public class ErsCtsOrderCriteria : Criteria
	{

        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_Order_t.id", value, Operation.EQUAL));
            }
        }

        public string ransu
        {
            set
            {
                Add(Criteria.GetCriterion("cts_Order_t.ransu", value, Operation.EQUAL));
            }
        }

        public string mcode
        {
            set
            {
                Add(Criteria.GetCriterion("cts_Order_t.mcode", value, Operation.EQUAL));
            }
        }

        public string temp_d_no
        {
            set
            {
                Add(Criteria.GetCriterion("cts_Order_t.temp_d_no", value, Operation.EQUAL));
            }
        }


        public int status
        {
            set
            {
                Add(Criteria.GetCriterion("cts_Order_t.status", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("cts_Order_t.active", value, Operation.EQUAL));
            }
        }

        public EnumStockReservation stock_reservation
        {
          set
            {
                this.Add(GetCriterion("bask_t.stock_reservation", value, Operation.EQUAL));
            }
        }

        public void SetCoalesceIntimeAndUtime(DateTime? current_date)
        {
            var param = new Dictionary<string, object>();
            param.Add("coalesce_intime_utime", current_date);
            this.Add(GetUniversalCriterion("COALESCE(cts_order_t.utime, cts_order_t.intime) < :coalesce_intime_utime", param));
        }

        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("id", orderBy);
        }

        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("cts_Order_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_Order_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

	}
}
