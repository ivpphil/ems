﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.ctsorder
{
    public class ErsCtsOrder : ErsRepositoryEntity
	{
        public override int? id { get; set; }

        public virtual int? temp_d_no { get; set; }

        public virtual DateTime? temp_odate { get; set; }

        public virtual DateTime? work_time { get; set; }

        public virtual string ransu { get; set; }

        public virtual string mcode { get; set; }

        public virtual string lname { get; set; }

        public virtual string fname { get; set; }

        public virtual string lnamek { get; set; }

        public virtual string fnamek { get; set; }

        public virtual string memo { get; set; }

        public virtual string charge1 { get; set; }

        public virtual string charge2 { get; set; }

        public virtual string charge3 { get; set; }

        public virtual int status { get; set; }

        public virtual string user_id { get; set; }

        public virtual string tel { get; set; }

        public virtual int subtotal { get; set; }

        public virtual EnumAgType? charge1_ag_type { get; set; }

        public virtual string charge1_ag_name { get; set; }

        public virtual DateTime? utime { get; set; }

        public virtual DateTime? intime { get; set; }

        public virtual EnumActive? active { get; set; }

        public virtual EnumPmFlg? pm_flg { get; set; }

        public virtual int? addressbook_id { get; set; }

        public virtual int? site_id { get; set; }
	}
}
