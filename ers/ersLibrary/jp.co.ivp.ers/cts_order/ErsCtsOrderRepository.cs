﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.ctsorder
{
    public class ErsCtsOrderRepository
        : ErsRepository<ErsCtsOrder>
    {
        public ErsCtsOrderRepository()
            : base(new ErsDB_cts_order_t())
        {
        }

        public ErsCtsOrderRepository(ErsDatabase objDB)
            : base(new ErsDB_cts_order_t(objDB))
        {
        }

        public override void Insert(ErsCtsOrder obj, bool storeNewIdToObject = false)
        {
            if (storeNewIdToObject == true)
            {
                obj.id = this.ersDB_table.GetNextSequence();
                obj.temp_d_no = this.ersDB_table.GetNextSequence("cts_order_t_temp_d_no_seq");
            }
            base.Insert(obj);
        }
    }
}
