﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.ctsorder.strategy;
using jp.co.ivp.ers.cts_order.specification;

namespace jp.co.ivp.ers.ctsorder
{
    public class ErsCtsOrderFactory
    {

        public ErsCtsOrderCriteria GetErsCtsOrderCriteria()
        {
            return new ErsCtsOrderCriteria();
        }

        public ErsCtsOrderRepository GetErsCtsOrderRepository()
        {
            return new ErsCtsOrderRepository();
        }

        public ErsCtsOrder GetErsCtsOrder()
        {
            return new ErsCtsOrder();
        }

        public virtual CheckDuplicateCtsOrderStgy GetCheckDuplicateCtsOrderStgy()
        {
            return new CheckDuplicateCtsOrderStgy();
        }

        public ErsCtsOrder GetErsOrderWithModel(ErsModelBase model)
        {
            var Order = this.GetErsCtsOrder();
            Order.OverwriteWithModel(model);
            return Order;
        }

        public ErsCtsOrder GetErsCtsOrderWithParameters(Dictionary<string, object> parameters)
        {
            var Order = this.GetErsCtsOrder();
            Order.OverwriteWithParameter(parameters);
            return Order;
        }

        public ErsCtsOrder GetErsCtsOrderWithId(int id)
        {
            var repository = this.GetErsCtsOrderRepository();
            var criteria = this.GetErsCtsOrderCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");

            return list[0];
        }

        public ErsCtsOrder GetErsCtsOrderWithTempDNo(int temp_d_no, int? site_id = null)
        {
            var repository = this.GetErsCtsOrderRepository();
            var criteria = this.GetErsCtsOrderCriteria();
            criteria.temp_d_no = temp_d_no.ToString();
            criteria.active = EnumActive.Active;
            if (site_id.HasValue)
            {
                criteria.site_id = site_id;
            }
            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");

            return list[0];
        }

        public SearchCtsStockReleaseSpec GetSearchCtsStockReleaseSpec()
        {
            return new SearchCtsStockReleaseSpec();
        }

        public CheckMemberEmailExist GetCheckMemberEmailExist()
        {
            return new CheckMemberEmailExist();
        }

    }
}
