﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.ctsorder.strategy
{
    /// <summary>
    /// Checks Whether the member already exists.
    /// </summary>
    public class CheckDuplicateCtsOrderStgy
    {
        /// <summary>
        /// Return true if the same member exists.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public virtual ValidationResult CheckDuplicate(string mcode, string ransu)
        {
            var criteria = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderCriteria();

            criteria.mcode = mcode;
            criteria.ransu = ransu;

            var repository = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderRepository();

            if (repository.GetRecordCount(criteria) > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("default", "Duplicate Temporary Order for MCode=" + mcode + " and ransu =" + ransu + "."));
            }

            return null;
        }

    }
}
