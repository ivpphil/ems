﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace jp.co.ivp.ers.ctsorder.strategy
{
    public class CheckMemberEmailExist
    {
        public virtual ValidationResult ValidateExistEmail(string email,EnumPaymentType? paymenttype)
        {
            if (!email.HasValue())
                if (paymenttype == EnumPaymentType.CONVENIENCE_STORE)
                {
                    return new ValidationResult(ErsResources.GetMessage("20238"));
                }
                else if (paymenttype == EnumPaymentType.BANK)
                {
                    return new ValidationResult(ErsResources.GetMessage("20244"));
                }
                

            return null;
        }
    }
}
