﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.cts_order.specification
{
    public class SearchCtsStockReleaseSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            return "SELECT bask_t.id AS bask_id, * FROM cts_order_t "
                + " INNER JOIN bask_t ON cts_order_t.ransu = bask_t.ransu ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            return "SELECT COUNT(cts_order_t.id) AS " + countColumnAlias
                + " FROM cts_order_t "
                + " INNER JOIN bask_t ON cts_order_t.ransu = bask_t.ransu ";
        }
    }
}
