﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// 商品送料区分
    /// </summary>
    public enum EnumCarriageCostType
    {
        /// <summary>
        /// 1: 通常
        /// </summary>
        Normal = 1,

        /// <summary>
        /// 2: 無料
        /// </summary>
        Free = 2
    }
}
