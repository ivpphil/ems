﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumDisp_list_flg
        : short
    {
        Invisible,
        Visible
    }
}
