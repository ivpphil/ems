﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumFrontOrderStatus
    {
        /// <summary>
        /// 新着注文
        /// </summary>
        NEW_ORDER = 10,

        /// <summary>
        /// 配送済
        /// </summary>
        DELIVERED = 30,

        /// <summary>
        /// 出荷前キャンセル
        /// </summary>
        CANCELED = 999,
    }
}
