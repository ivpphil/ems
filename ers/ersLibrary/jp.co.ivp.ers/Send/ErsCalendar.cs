﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Send
{
    public class ErsCalendar
        : ErsRepositoryEntity
    {
        public ErsCalendar()
        {
        }
        public override int? id { get; set; }
        public virtual DateTime? close_date { get; set; }
    }
}
