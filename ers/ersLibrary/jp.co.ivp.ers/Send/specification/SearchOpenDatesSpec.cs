﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.Send.specification
{
    public class SearchOpenDatesSpec
        : ISpecificationForSQL
    {
        public List<DateTime> GetList(DateTime fromDate, DateTime toDate)
        {
            var key = new KeyValuePair<string, DateTime>(fromDate.ToString("yyyy/MM/dd"), toDate);
            if (_poolOpenCalendar.ContainsKey(key))
            {
                return _poolOpenCalendar[key];
            }

            this.fromDate = fromDate;
            this.toDate = toDate;

            var searchResult = ErsRepository.SelectSatisfying(this);

            var retList = new List<DateTime>();


            foreach (var item in searchResult)
            {
                retList.Add((DateTime)item["open_date"]);
            }

            _poolOpenCalendar.Add(key, retList);
            return retList;
        }

        public string asSQL()
        {
            return "SELECT open_date FROM "
                    + "(SELECT '" + fromDate + "'::date + s.i AS open_date FROM generate_series(0,365) AS s(i)) AS date_table "
                    + "WHERE open_date NOT IN (SELECT close_date FROM calendar_t) AND open_date <= '" + toDate + "'::date "
                    + "ORDER BY open_date ASC";
        }

        private DateTime fromDate { get; set; }

        private DateTime toDate { get; set; }


        /// <summary>
        /// リストプール用
        /// </summary>
        private static Dictionary<KeyValuePair<string, DateTime>, List<DateTime>> _poolOpenCalendar
        {
            get
            {
                if (ErsCommonContext.GetPooledObject("_poolOpenCalendar") == null)
                    _poolOpenCalendar = new Dictionary<KeyValuePair<string, DateTime>, List<DateTime>>();
                return (Dictionary<KeyValuePair<string, DateTime>, List<DateTime>>)ErsCommonContext.GetPooledObject("_poolOpenCalendar");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_poolOpenCalendar", value);
            }
        }
    }
}
