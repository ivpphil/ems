﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Send
{
    public class ErsSendTime
         : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public virtual string sendtime { get; set; }

        public EnumActive? active { get; set; }

        public DateTime? intime { get; set; }

        public DateTime? utime { get; set; }
    }
}
