﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.Send
{
    public class ShippingAddressInfo
    {
        private ErsOrder objOrder;

        public void LoadShippingAddress(ErsOrder objOrder)
        {
            this.objOrder = objOrder;
        }

        public virtual string shipping_lname { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.lname:this.objOrder.add_lname; } }
        public virtual string shipping_fname { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.fname:this.objOrder.add_fname; } }
        public virtual string shipping_lnamek { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.lnamek:this.objOrder.add_lnamek; } }
        public virtual string shipping_fnamek { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.fnamek:this.objOrder.add_fnamek; } }
        public virtual string shipping_compname { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.compname:this.objOrder.add_compname; } }
        public virtual string shipping_compnamek { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.compnamek:this.objOrder.add_compnamek; } }
        public virtual string shipping_division { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.division:this.objOrder.add_division; } }
        public virtual string shipping_divisionk { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.divisionk:this.objOrder.add_divisionk; } }
        public virtual string shipping_tlname { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.tlname:this.objOrder.add_tlname; } }
        public virtual string shipping_tfname { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.tfname:this.objOrder.add_tfname; } }
        public virtual string shipping_tlnamek { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.tlnamek:this.objOrder.add_tlnamek; } }
        public virtual string shipping_tfnamek { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.tfnamek:this.objOrder.add_tfnamek; } }
        public virtual string shipping_tel { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.tel:this.objOrder.add_tel; } }
        public virtual string shipping_fax { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.fax:this.objOrder.add_fax; } }
        public virtual string shipping_zip { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.zip:this.objOrder.add_zip; } }
        public virtual string shipping_address { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.address:this.objOrder.add_address; } }
        public virtual string shipping_taddress { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.taddress:this.objOrder.add_taddress; } }
        public virtual string shipping_maddress { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.maddress:this.objOrder.add_maddress; } }
        public virtual int? shipping_pref { get { return (this.objOrder.send == EnumSendTo.MEMBER_ADDRESS)?this.objOrder.pref:this.objOrder.add_pref; } }
    }
}
