﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.Send
{
    public class ErsCalendarCriteria
        : Criteria
    {
        protected internal ErsCalendarCriteria()
        {
        }

        /// <summary>
        /// Set calendar_t.id. 
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("calendar_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 配送不可（開始）
        /// </summary>
        public virtual DateTime? close_date_from
        {
            set
            {
                this.Add(Criteria.GetCriterion("calendar_t.close_date", value, Operation.GREATER_EQUAL));
            }
        }

        /// <summary>
        /// 配送不可（終了）
        /// </summary>
        public virtual DateTime? close_date_to
        {
            set
            {
                this.Add(Criteria.GetCriterion("calendar_t.close_date", value, Operation.LESS_EQUAL));
            }
        }

        public DateTime close_date
        {
            set
            {
                this.Add(Criteria.GetCriterion("calendar_t.close_date", value.Date, Operation.EQUAL));
            }
        }
    }
}
