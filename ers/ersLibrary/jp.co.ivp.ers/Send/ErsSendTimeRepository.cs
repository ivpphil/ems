﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.Send
{
    public class ErsSendTimeRepository
        : ErsRepository<ErsSendTime>
    {
        public ErsSendTimeRepository()
            : base("sendtime_t")
        {
        }

        /// <summary>
        /// Deletes record in sendtime_t.
        /// </summary>
        /// <param name="obj">ErsSendTime object.</param>
        public override void Delete(ErsSendTime obj)
        {
            obj.active = EnumActive.NonActive;

            var oldObj = ErsFactory.ersOrderFactory.GetErsSendTimeWithId(obj.id);

            this.Update(oldObj, obj);
        }
    }
}
