﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.Send
{
    public class ErsSendTimeCriteria
        : Criteria
    {
        protected internal ErsSendTimeCriteria()
        {
        }

        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("sendtime_t.id", value, Operation.EQUAL));
            }
        }

        public virtual string sendtime
        {
            set
            {
                this.Add(Criteria.GetCriterion("sendtime_t.sendtime", value, Operation.EQUAL));
            }
        }

        public virtual EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("sendtime_t.active", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Set orderBy sendtime_t.id.
        /// </summary>
        /// <param name="orderBy">OrderBy ascending(asc) or descending(desc).</param>
        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("sendtime_t.id", orderBy);
        }
   }
}
