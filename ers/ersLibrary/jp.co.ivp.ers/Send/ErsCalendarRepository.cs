﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.Send;

namespace jp.co.ivp.ers.Send
{
    public class ErsCalendarRepository
        : ErsRepository<ErsCalendar>
    {
        public ErsCalendarRepository()
            : base("calendar_t")
        {
        }

        /// <summary>
        /// Gets IList ErsCalendar in calendar_t.
        /// </summary>
        /// <param name="criteria">Criteria for getting Ilist ErsCalendar in calendar_t.</param>
        /// <returns>Retruns List ErsCalendar.</returns>
        public override IList<ErsCalendar> Find(Criteria criteria)
        {
            List<Dictionary<string, object>> allData = this.ersDB_table.gSelect(criteria);
            List<ErsCalendar> retList = new List<ErsCalendar>();
            foreach (var data in allData)
            {
                ErsCalendar tmpCalendar = ErsFactory.ersOrderFactory.GetErsCalendarWithParameter(data);
                retList.Add(tmpCalendar);
            }
            return retList;
        }
    }
}
