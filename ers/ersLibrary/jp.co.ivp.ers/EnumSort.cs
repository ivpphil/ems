﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumSort
    {
        NotSorted = 0,
        Ascending = 1,
        Descending = 2,
    }
}
