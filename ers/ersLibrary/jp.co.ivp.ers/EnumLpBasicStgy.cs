﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumLpBasicStgy
    {
        /// <summary>
        /// 1: 本商品単体
        /// </summary>
        Simple = 1,

        /// <summary>
        /// 2: アップセル
        /// </summary>
        UpSell,

        /// <summary>
        /// 3: 無料サンプル
        /// </summary>
        FreeSample
    }
}
