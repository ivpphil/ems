﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumPriceKbn
    {
        NORMAL = 1,
        REGULAR_FIRST = 2,
        REGULAR = 3,
        RANK_NORMAL = 31,
        RANK_REGULAR_FIRST = 32,
        RANK_REGULAR = 33,
        SALE = 21
    }
}
