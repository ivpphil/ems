﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// 支払い方法
    /// </summary>
    public enum EnumPaymentType
    {
        /// <summary>
        /// 1: クレジットカード
        /// </summary>
        CREDIT_CARD = 1,

        /// <summary>
        /// 2: 銀行
        /// </summary>
        BANK = 2,

        /// <summary>
        /// 4: 代引き
        /// </summary>
        CASH_ON_DELIVERY = 4,

        /// <summary>
        /// 5: コンビニ
        /// </summary>
        CONVENIENCE_STORE = 5,

        /// <summary>
        /// 9: PayPal
        /// </summary>
        PAYPAL = 9,

        /// <summary>
        /// 900: 決済なし
        /// </summary>
        NON_NEEDED_PAYMENT = 900,
    }
}
