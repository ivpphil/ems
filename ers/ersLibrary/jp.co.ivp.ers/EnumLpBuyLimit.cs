﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumLpBuyLimit
    {
        /// <summary>
        /// 1: お一人様1個まで
        /// </summary>
        OneTime = 1,

        /// <summary>
        /// 2: 複数回申込み可能
        /// </summary>
        MultiTime
    }
}
