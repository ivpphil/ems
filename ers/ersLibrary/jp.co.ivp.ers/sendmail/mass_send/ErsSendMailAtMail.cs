﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.sendmail.mass_send
{
    /// <summary>
    /// メール送信クラス（ベースクラス）
    /// </summary>
    public class ErsSendMailAtMail
    {
        public string pathKey { get; set; }

        /// <summary>
        /// メール送信先
        /// </summary>
        public virtual string mail_to { get; set; }

        /// <summary>
        /// メール送信元
        /// </summary>
        public virtual string mail_from { get; set; }

        /// <summary>
        /// 返信先
        /// </summary>
        public virtual string reply_to { get; set; }

        /// <summary>
        /// BCC
        /// </summary>
        public virtual string[] bcc_email { get; set; }

        /// <summary>
        /// CC
        /// </summary>
        public virtual string[] cc_email { get; set; }

        /// <summary>
        /// メールタイトル
        /// </summary>
        public virtual string title { get; protected set; }

        /// <summary>
        /// メールタイトル
        /// </summary>
        public virtual string body { get; set; }

        /// <summary>
        /// HTML BODY
        /// </summary>
        public virtual string html_body { get; protected set; }

        /// <summary>
        /// Model
        /// </summary>
        protected virtual IErsAtmailModelBase model { get; set; }

        /// <summary>
        /// Subject from am_process_t
        /// </summary>
        public virtual string subject { get; set; }

        /// <summary>
        ///  初期化を行う
        /// </summary>
        /// <param name="model">Model</param>
        /// <param name="format"> Mobile</param>
        public virtual void Bind(IErsAtmailModelBase model, string body, string pathKey)
        {
            this.model = model;
            this.body = body;
            this.pathKey = pathKey;

            this.RenderBody();
        }

        /// <param name="format">Pc</param>
        public virtual void Bind(IErsAtmailModelBase model, string body, string html_body, string pathKey)
        {
            this.model = model;
            this.body = body;
            this.html_body = html_body;
            this.pathKey = pathKey;

            this.RenderBody();
        }


        /// <summary>
        /// メールを送信する
        /// </summary>
        /// <param name="mail_title">Title of Mail</param>
        /// <param name="mail_body">Body of Mail</param>
        public virtual void SendMail(string hostname, int port)
        {
            var retryProperty = ErsFactory.ersMailFactory.GetErsSmtpRetryProperty();
            retryProperty.SetDisableRetry();

            //D3)Use the method of ErsSmtp SendSynchronous class to send an email.
            ErsSmtp smtp = new ErsSmtp(hostname, port, null, null, null, null, retryProperty);
            smtp.SendSynchronous(this.mail_from, this.reply_to, mail_to, cc_email, bcc_email, subject,this.body, this.html_body);
        }
        /// <summary>
        /// bodyのersタグを処理する
        /// </summary>
        /// <returns></returns>
        protected virtual void RenderBody()
        {
            //メッセージ取得
            if (model == null)
            {
                throw new Exception("ViewがModelで初期化されていません。");
            }
            //テンプレート置換（Viewを利用）
            var context = ErsFactory.ersMailFactory.getErsMailViewContext();
            context.Init(model, null);

            var pcPathKey = this.pathKey;
            var htmlPathKey = this.pathKey;
            if (htmlPathKey.HasValue())
            {
                htmlPathKey += "html";
            }

            this.body = Render(context, this.body, pcPathKey);
            if (!string.IsNullOrEmpty(this.html_body))
            {
                this.html_body = Render(context, this.html_body, htmlPathKey);
            }
        }

        /// <summary>
        /// render body
        /// </summary>
        /// <param name="context"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        private string Render(ErsMailViewContext context, string body, string pathKey)
        {
            var emv = ErsFactory.ersMailFactory.getErsMailView();
            emv.Init(pathKey, context, body);

            var sw = new StringWriter();
            emv.Render(sw);
            return sw.ToString();
        }
    }
}
