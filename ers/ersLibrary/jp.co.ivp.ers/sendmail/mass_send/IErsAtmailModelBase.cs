﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.sendmail.mass_send
{
    public interface IErsAtmailModelBase
        : IErsModelBase
    {
        string mcode { get; }
        string email { get; }
        string lname { get; }
        string fname { get; }
        string etc1 { get; }
        string etc2 { get; }
    }
}
