﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSendMailUpCreditAccountUpdate
        :ErsSendMail
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsSendMailUpCreditAccountUpdate(int? siteId)
            : base(siteId)
        {
        }

        protected override string key
        {
            get { return "up_creditaccountupdate"; }
        }
    }
}
