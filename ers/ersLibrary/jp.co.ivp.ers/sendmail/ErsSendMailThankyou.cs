﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSendMailThankyou
        :ErsSendMail
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsSendMailThankyou(int? siteId)
            : base(siteId)
        {
        }

        protected override string key
        {
            get { return "register"; }
        }

        public void Send(string d_no, string mcode, string email, EnumMformat mformat, ErsModelBase model, ErsModelBase cartModel, bool isMonitor)
        {
            this.Init(model, mformat);
            this.AddModelToMailView("CartModel", cartModel);
            this.d_no_forLog = d_no;
            this.mcode_forLog = mcode;
            this.mail_to = email;
            if (isMonitor)
            {
                this.sendToAdmin = false;
            }
            this.Send();
        }
    }
}
