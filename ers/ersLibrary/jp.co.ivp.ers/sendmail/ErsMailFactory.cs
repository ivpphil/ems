﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail.mass_send;
using ers.jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsMailFactory
    {
        /// <summary>
        /// Instantiate ErsMailView
        /// </summary>
        /// <returns>new ErsMailView</returns>
        public virtual ErsMailView getErsMailView()
        {
            return new ErsMailView();
        }

        /// <summary>
        /// Instantiate ErsMailViewContext
        /// </summary>
        /// <returns>new ErsMailViewContext</returns>
        public virtual ErsMailViewContext getErsMailViewContext()
        {
            return new ErsMailViewContext();
        }

        /// <summary>
        /// Instantiate ErsSendMailThankyou
        /// </summary>
        /// <returns>new ErsSendMailThankyou</returns>
        public virtual ErsSendMailThankyou getErsSendMailThankyou()
        {
            return new ErsSendMailThankyou(ErsFactory.ersUtilityFactory.getSetup().site_id);
        }

        /// <summary>
        /// Instantiate ErsSendMailMemberRegistered
        /// </summary>
        /// <returns>new ErsSendMailMemberRegistered</returns>
        public virtual ErsSendMailMemberRegistered getErsSendMailMemberRegistered()
        {
            return new ErsSendMailMemberRegistered(ErsFactory.ersUtilityFactory.getSetup().site_id);
        }

        /// <summary>
        /// Instantiate ErsSendMailQuest
        /// </summary>
        /// <returns>new ErsSendMailQuest</returns>
        public virtual ErsSendMailQuest getErsSendMailQuest()
        {
            return new ErsSendMailQuest(ErsFactory.ersUtilityFactory.getSetup().site_id);
        }

        /// <summary>
        /// Instantiate ErsSendMailQuit
        /// </summary>
        /// <returns>new ErsSendMailQuit</returns>
        public virtual ErsSendMailQuit getErsSendMailQuit()
        {
            return new ErsSendMailQuit(ErsFactory.ersUtilityFactory.getSetup().site_id);
        }

        /// <summary>
        /// Instantiate ErsSendMailPassrim
        /// </summary>
        /// <returns>new ErsSendMailPassrim</returns>
        public virtual ErsSendMailPassrim getErsSendMailPassrim()
        {
            return new ErsSendMailPassrim(ErsFactory.ersUtilityFactory.getSetup().site_id);
        }

        public virtual ErsSendMailEmp getErsSendmailEmp()
        {
            return new ErsSendMailEmp(ErsFactory.ersUtilityFactory.getSetup().site_id);
        }

        /// Instantiate ErsSendMailForgetPass
        /// </summary>
        /// <returns></returns>
        public virtual ErsSendMailForgetPass getErsSendMailForgetPass()
        {
            return new ErsSendMailForgetPass(ErsFactory.ersUtilityFactory.getSetup().site_id);
        }

        /// <summary>
        /// Instantiate ErsSendMailAdminIndividual
        /// </summary>
        /// <returns>new ErsSendMailAdminIndividual</returns>
        public virtual ErsSendMailAdminIndividual getErsSendMailAdminIndividual(int? site_id = null)
        {
            if (!site_id.HasValue)
            {
                site_id = (int)EnumSiteId.COMMON_SITE_ID;
            }
            return new ErsSendMailAdminIndividual(site_id);
        }

        /// <summary>
        /// Instantiate ErsSendMailDelivered
        /// </summary>
        /// <returns>new ErsSendMailDelivered</returns>
        public virtual ErsSendMailDelivered GetErsSendMailDelivered(int? site_id)
        {
            return new ErsSendMailDelivered(site_id);
        }

        public virtual ErsSmtp GetErsSmtp()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            
            var logFileUserName = setup.logFileUserName;
            var logFileUserPassword = setup.logFileUserPassword;

            var smtpHostName = setup.smtpHostName;
            var smtpPort = setup.smtpPort;
            var smtpOperationLogPath = setup.smtpOperationLogPath;
            var smtpErrorLogPath = setup.smtpErrorLogPath;

            var retryProperty = this.GetErsSmtpRetryProperty();
            retryProperty.SetEnableRetry(setup.smtpRetryTermMinutes, setup.smtpRetryIntervalMinutes, setup.smtpRetryTextPath);

            return new ErsSmtp(smtpHostName, smtpPort, logFileUserName, logFileUserPassword, smtpOperationLogPath, smtpErrorLogPath, retryProperty);
        }

        public virtual ErsSmtpRetryProperty GetErsSmtpRetryProperty()
        {
            return new ErsSmtpRetryProperty();
        }

        public virtual ErsSendMailAtMail GetErsSendMailAtMail()
        {
            return new ErsSendMailAtMail();
        }

        public virtual ErsTpMailRepository GetErsTpMailRepository()
        {
            return new ErsTpMailRepository();
        }

        public virtual ErsTpMail GetErsTpMail()
        {
            return new ErsTpMail();
        }

        public virtual ErsSendMailRegularOrder GetErsSendMailRegularOrder(int? site_id)
        {
            return new ErsSendMailRegularOrder(site_id);
        }

        public virtual ErsSendMailQuestAccept GetErsSendMailQuestAccept()
        {
            return new ErsSendMailQuestAccept(ErsFactory.ersUtilityFactory.getSetup().site_id);
        }
    }
}
