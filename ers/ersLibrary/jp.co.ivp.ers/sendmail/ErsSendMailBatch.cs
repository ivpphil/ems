﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.IO;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSendMailBatch
    {
        /// <summary>
        /// メールタイトル
        /// </summary>
        public virtual string body { get; set; }

        /// <summary>
        /// Model
        /// </summary>
        protected virtual IErsModelBase model { get; set; }

        /// <summary>
        /// メール送信
        /// </summary>
        /// <param name="mail_title">件名</param>
        /// <param name="mail_body">本文</param>
        /// <param name="mailPath">送信先</param>
        /// <param name="tp_mail_memo">実行パス</param>
        public virtual void MailSend(string mail_title, string mail_body, string tp_mail_memo, string mailTo, string mailFrom, string mailCc, string mailBcc)
        {
            if (string.IsNullOrEmpty(mailFrom))
                throw new Exception("mail_from is not specified.");
            if (string.IsNullOrEmpty(mailTo))
                throw new Exception("mail_to is not specified.");

            List<string> cc_email = null;
            if (mailCc != null && mailCc != string.Empty)
                cc_email = mailCc.Split(',').ToList();

            //cc
            string strCc = string.Empty;
            if (cc_email != null)
            {
                foreach (var val in cc_email)
                {
                    if (!string.IsNullOrEmpty(val))
                        strCc += val + Environment.NewLine;
                }
            }

            List<string> bcc_email = null;
            if (mailBcc != null && mailBcc != string.Empty)
                bcc_email  = mailBcc.Split(',').ToList();

            //bcc
            string strBcc = string.Empty;
            var enumerableBcc = (IEnumerable<string>)bcc_email;
            if (enumerableBcc != null)
            {
                foreach (var val in enumerableBcc)
                {
                    if (!string.IsNullOrEmpty(val))
                        strBcc += val + Environment.NewLine;
                }
            }

            var logData = ErsFactory.ersMailFactory.GetErsTpMail();
            logData.mailfrom=mailFrom;
            logData.mailto=mailTo;
            logData.mailbcc=strBcc;
            logData.mailcc=strCc;
            logData.subject=mail_title;
            logData.message=mail_body;
            logData.memo=tp_mail_memo;
            logData.mail_flg=0;
            logData.sdate = DateTime.Now;

            var repository = ErsFactory.ersMailFactory.GetErsTpMailRepository();
            repository.Insert(logData);

            //VEXの場合はスキップ
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            if (!setup.onVEX)
            {
                ErsFactory.ersMailFactory.GetErsSmtp().SendSynchronous(mailFrom, null, mailTo, cc_email, bcc_email, mail_title, mail_body, null);
            }
        }

        /// <summary>
        ///  初期化を行う
        /// </summary>
        /// <param name="model">Model</param>
        /// <param name="format"> Mobile</param>
        public virtual void Bind(IErsModelBase model, string body)
        {
            this.model = model;
            this.body = body;

            this.RenderBody();
        }

        /// <summary>
        /// bodyのersタグを処理する
        /// </summary>
        /// <returns></returns>
        protected virtual void RenderBody()
        {
            //メッセージ取得
            if (model == null)
            {
                throw new Exception("ViewがModelで初期化されていません。");
            }
            //テンプレート置換（Viewを利用）
            var context = ErsFactory.ersMailFactory.getErsMailViewContext();
            context.Init(model, null);
            this.body = Render(context, this.body);
        }

        /// <summary>
        /// render body
        /// </summary>
        /// <param name="context"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        private string Render(ErsMailViewContext context, string body)
        {
            var emv = ErsFactory.ersMailFactory.getErsMailView();
            emv.Init(null, context, body);

            var sw = new StringWriter();
            emv.Render(sw);
            return sw.ToString();
        }
    }
}
