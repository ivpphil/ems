﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSendMailQuit : ErsSendMail
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsSendMailQuit(int? siteId)
            : base(siteId)
        {
        }

        protected override string key
        {
            get { return "quit"; }
        }

        /// <summary>
        /// メール送信
        /// </summary>
        public void Send(ErsModelBase model, string email, EnumMformat? mformat)
        {
            this.mail_to = email;
            this.Init(model, mformat.Value);
            this.Send();
        }
    }
}
