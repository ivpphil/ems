﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSendMailMemberRegistered
        :ErsSendMail
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsSendMailMemberRegistered(int? siteId)
            : base(siteId)
        {
        }

        protected override string key
        {
            get { return "entry"; }
        }


        public void Send(string d_no, string mcode, string email, EnumMformat? mformat, ErsModelBase model, bool isMonitor)
        {
            this.Init(model, mformat.Value);
            if (!string.IsNullOrEmpty(d_no))
            {
                this.d_no_forLog = d_no;
            }
            this.mcode_forLog = mcode;
            this.mail_to = email;
            if (isMonitor)
            {
                this.sendToAdmin = false;
            }
            this.Send();
        }
    }
}
