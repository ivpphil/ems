﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.sendmail
{
    /// <summary>
    /// メール送信クラス（ベースクラス）
    /// </summary>
    public abstract class ErsSendMail
    {
        /// <summary>
        /// 管理者にメールをおくるか否か
        /// </summary>
        public virtual bool sendToAdmin { get; set; }

        /// <summary>
        /// メール送信先
        /// </summary>
        public virtual string mail_to { get; set; }

        /// <summary>
        /// メール送信元
        /// </summary>
        public virtual string mail_from { get; set; }

        /// <summary>
        /// 返信先
        /// </summary>
        public virtual string reply_to { get; set; }

        /// <summary>
        /// BCC
        /// </summary>
        public virtual List<string> bcc_email { get; set; }

        /// <summary>
        /// CC
        /// </summary>
        public virtual List<string> cc_email { get; set; }

        /// <summary>
        /// ログに出力する伝票番号
        /// </summary>
        public virtual string d_no_forLog { get; set; }

        /// <summary>
        /// ログに出力する会員コード
        /// </summary>
        public virtual string mcode_forLog { get; set; }

        /// <summary>
        /// ログに出力するメモ
        /// </summary>
        public virtual string memo_forLog { get; set; }




        /// <summary>
        /// 管理者メールアドレス
        /// </summary>
        public virtual string admin_email { get; protected set; }





        /// <summary>
        /// mail_template_tのキー名
        /// </summary>
        protected abstract string key { get; }

        /// <summary>
        /// メール送信先
        /// </summary>
        protected virtual ErsMailView emv { get; set; }

        /// <summary>
        /// メールタイトル
        /// </summary>
        public virtual string title { get; protected set; }

        /// <summary>
        /// メールタイトル
        /// </summary>
        public virtual string body { get; protected set; }

        /// <summary>
        /// BCC(sendToAdminがtrueの場合にBCCにいれる。)
        /// </summary>
        protected virtual List<string> admin_bcc { get; set; }

        /// <summary>
        /// メール種別
        /// </summary>
        protected virtual int? send_type { get; set; }

        /// <summary>
        /// Model
        /// </summary>
        protected virtual IErsModelBase model { get; set; }
        
        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public virtual int? siteId { get; set; }


        /// <summary>
        /// constractor
        /// </summary>
        public ErsSendMail(int? siteId)
        {
            this.bcc_email = new List<string>();
            this.cc_email = new List<string>();
            this.sendToAdmin = true;
            this.siteId = siteId;
        }

        /// <summary>
        ///  初期化を行う
        /// </summary>
        /// <param name="model">Model</param>
        /// <param name="format">Pc or Mobile</param>
        public virtual void Init(IErsModelBase model, EnumMformat format)
        {
            this.model = model;
            this.GetMailData(format);
        }

        /// <summary>
        /// 追加のモデルを追加します。
        /// </summary>
        /// <param name="templateModelName"></param>
        /// <param name="model"></param>
        public void AddModelToMailView(string templateModelName, IErsModelBase model)
        {
            this.additionalModels.Add(templateModelName, model);
        }

        Dictionary<string, IErsModelBase> additionalModels = new Dictionary<string, IErsModelBase>();

        /// <summary>
        /// メールを送信する（非同期）
        /// </summary>
        public virtual void Send()
        {
            this.RenderBody();

            this.MailSend(true);
        }

        /// <summary>
        /// メールを送信する（同期）
        /// </summary>
        public virtual void SendSynchronous()
        {
            this.RenderBody();

            this.MailSend(false);
        }

        /// <summary>
        /// メールを送信する（非同期）
        /// </summary>
        /// <param name="mail_title">Title of Mail</param>
        /// <param name="mail_body">Body of Mail</param>
        public virtual void Send(string mail_title, string mail_body)
        {
            this.title = mail_title;
            this.body = mail_body;
            this.Send();
        }

        /// <summary>
        /// メールを送信する（同期）
        /// </summary>
        /// <param name="mail_title">Title of Mail</param>
        /// <param name="mail_body">Body of Mail</param>
        public virtual void SendSynchronous(string mail_title, string mail_body)
        {
            this.title = mail_title;
            this.body = mail_body;
            this.SendSynchronous();
        }

        /// <summary>
        /// bodyのersタグを処理する
        /// </summary>
        /// <returns></returns>
        protected virtual void RenderBody()
        {

            //メッセージ取得
            if (model == null)
            {
                throw new Exception("ViewがModelで初期化されていません。");
            }
            var sw = new StringWriter();
            //テンプレート置換（Viewを利用）
            var context = ErsFactory.ersMailFactory.getErsMailViewContext();
            context.Init(model, this.additionalModels);
            emv = ErsFactory.ersMailFactory.getErsMailView();
            emv.Init(null, context, body);
            emv.Render(sw);
            this.body = sw.ToString();
        }

        /// <summary>
        /// テンプレート情報をDBから取得
        /// </summary>
        protected virtual void GetMailData(EnumMformat format)
        {
            var keyString = GetMailKeyString(format, this.key);

            //DBから取得

            this.title = ErsFactory.ersAdministratorFactory.GetErsMailTemplateWithKey(keyString + "_title", this.siteId).value;
            this.body = ErsFactory.ersAdministratorFactory.GetErsMailTemplateWithKey(keyString + "_body", this.siteId).value;
            var send_type = ErsFactory.ersAdministratorFactory.GetErsMailTemplateWithKey(key + "_sendtype", this.siteId).value;

            //携帯用のメールテンプレートの登録がない場合は、PC用でメール送信する。
            if (format == EnumMformat.MOBILE)
            {
                if (string.IsNullOrEmpty(this.title) || string.IsNullOrEmpty(this.body))
                {
                    keyString = GetMailKeyString(EnumMformat.PC, this.key);
                    this.title = ErsFactory.ersAdministratorFactory.GetErsMailTemplateWithKey(keyString + "_title", this.siteId).value;
                    this.body = ErsFactory.ersAdministratorFactory.GetErsMailTemplateWithKey(keyString + "_body", this.siteId).value;
                }
            }

            if (string.IsNullOrEmpty(this.title))
                throw new Exception(string.Format("'{0}' key is not defined in mail_template_t.", keyString + "_title"));
            if (string.IsNullOrEmpty(this.body))
                throw new Exception(string.Format("'{0}' key is not defined in mail_template_t.", keyString + "_body"));
            if (string.IsNullOrEmpty(send_type))
                throw new Exception(string.Format("'{0}' key is not defined in mail_template_t.", key + "_sendtype"));

            //設定ファイルから取得
            var setup = ErsFactory.ersUtilityFactory.GetErsSetupOfSite();
            this.admin_email = setup.r_email;
            this.admin_bcc = this.GetBccEmailFromSetup(setup);

            if (string.IsNullOrEmpty(this.mail_from))
            {
                this.mail_from = setup.r_email;
            }
            this.send_type = Convert.ToInt32(send_type);
        }

        private List<string> GetBccEmailFromSetup(ErsSetup setup)
        {
            var setup_tValue = setup.GetPropertiesAsDictionary();

            var ret = new List<string>();
            for (var i = 1; i <= 3; i++)
            {
                if (!setup_tValue.ContainsKey("f_email" + (i)))
                {
                    ret.Add(string.Empty);
                }
                else
                {
                    ret.Add(Convert.ToString(setup_tValue["f_email" + (i)]));
                }
            }
            return ret;
        }

        /// <summary>
        /// Gets key of mailtemplate_t.
        /// </summary>
        /// <param name="format">EnumMformat object.</param>
        /// <returns>Returns String keystring.</returns>
        protected virtual string GetMailKeyString(EnumMformat format, string key)
        {
            var keyString = key;

            if (format == EnumMformat.PC)
                keyString = keyString + "_pc";
            if (format == EnumMformat.MOBILE)
                keyString = keyString + "_mobile";

            return keyString;
        }

        /// <summary>
        /// メール送信する
        /// </summary>
        protected virtual void MailSend(bool async)
        {
            if (string.IsNullOrEmpty(this.mail_from))
                throw new Exception("mail_from is not specified.");
            if (string.IsNullOrEmpty(this.mail_to))
                throw new Exception("mail_to is not specified.");

            //bcc
            string strBcc = string.Empty;
            if (sendToAdmin)
            {
                //管理者あてにメール送信
                if (this.bcc_email == null)
                {
                    this.bcc_email = this.admin_bcc;
                }
                else
                {
                    this.bcc_email.AddRange(this.admin_bcc);
                }
            }
            if (this.bcc_email != null)
            {
                foreach (var val in this.bcc_email)
                {
                    if (!string.IsNullOrEmpty(val))
                        strBcc += val + Environment.NewLine;
                }
            }

            //cc
            string strCc = string.Empty;
            if (cc_email != null)
            {
                foreach (var val in this.cc_email)
                {
                    if (!string.IsNullOrEmpty(val))
                        strCc += val + Environment.NewLine;
                }
            }

            //tp_mail_tへログを落とす
            var logData = ErsFactory.ersMailFactory.GetErsTpMail();
            logData.mailfrom = this.mail_from;
            logData.mailto= this.mail_to;
            logData.mailcc = strCc;
            logData.mailbcc = strBcc;
            logData.subject = title;
            logData.message = this.body;
            logData.d_no = this.d_no_forLog;
            logData.mcode = this.mcode_forLog;
            logData.memo = this.memo_forLog;
            logData.mail_flg = this.send_type;
            logData.sdate = DateTime.Now;
            logData.site_id = this.siteId;

            var repository = ErsFactory.ersMailFactory.GetErsTpMailRepository();
            repository.Insert(logData);

            this.SendSmtpMail(async);
        }

        /// <summary>
        /// メール送信処理 [Process for send mail]
        /// </summary>
        protected virtual void SendSmtpMail(bool async)
        {
            //VEXの場合はスキップ
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            if (!setup.onVEX)
            {
                if (async)
                {
                    ErsFactory.ersMailFactory.GetErsSmtp().Send(this.mail_from, this.reply_to, this.mail_to, this.cc_email, this.bcc_email, this.title, this.body, null);
                }
                else
                {
                    ErsFactory.ersMailFactory.GetErsSmtp().SendSynchronous(this.mail_from, this.reply_to, this.mail_to, this.cc_email, this.bcc_email, this.title, this.body, null);
                }
            }
        }
    }
}
