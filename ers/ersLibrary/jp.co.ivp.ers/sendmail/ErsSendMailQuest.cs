﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSendMailQuest
        :ErsSendMail
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsSendMailQuest(int? siteId)
            : base(siteId)
        {
        }

        protected override string key
        {
            get { return "quest"; }
        }

        public void Send(ErsModelBase model, string email)
        {
            // 設定オブジェクト取得
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            // 初期化
            this.Init(model, EnumMformat.PC);

            // 送信元・送信先セット
            this.reply_to = email;
            this.mail_to = setup.quest_email_to;

            // 送信
            this.SendSynchronous();
        }
    }
}
