﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSendMailPassrim
        :ErsSendMail
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsSendMailPassrim(int? siteId)
            : base(siteId)
        {
        }

        protected override string key
        {
            get { return "passrim"; }
        }

        /// <summary>
        /// メール配信
        /// </summary>
        public void Send(ErsModelBase model, string email, EnumMformat? mformat)
        {
            this.mail_to = email;
            this.sendToAdmin = false;
            this.Init(model, mformat.Value);
            this.SendSynchronous();
        }
    }
}
