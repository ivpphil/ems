﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSendMailRegularOrder
        : ErsSendMail
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsSendMailRegularOrder(int? siteId)
            : base(siteId)
        {
        }

        protected override string key
        {
            get { return "regular_order"; }
        }

        public void SendMail(string d_no, string mcode, string email, EnumMformat mformat, ErsModelBase model)
        {
            this.Init(model, mformat);
            this.d_no_forLog = d_no;
            this.mcode_forLog = mcode;
            this.mail_to = email;
            this.SendSynchronous();
        }
    }
}
