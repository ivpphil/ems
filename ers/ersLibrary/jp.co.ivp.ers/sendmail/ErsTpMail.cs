﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsTpMail
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string d_no { get; set; }

        public string mcode { get; set; }

        public DateTime? sdate { get; set; }

        public string mailfrom { get; set; }

        public string mailto { get; set; }

        public string mailcc { get; set; }

        public string mailbcc { get; set; }

        public string subject { get; set; }

        public string message { get; set; }

        public int? mail_flg { get; set; }

        public string memo { get; set; }

        public int? site_id { get; set; }
    }
}
