﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.sendmail
{
    public class ErsSendMailForgetPass:ErsSendMail
    {


        public ErsSendMailForgetPass(int? siteId)
            :base(siteId)
        {

        }


        protected override string key
        {
            get
            {
                return "passforget";
            }
        }


        public void Send(ErsModelBase model, string email, EnumMformat? mformat)
        {
            this.mail_to = email;
            this.sendToAdmin = false;
            this.Init(model, mformat.Value);
            this.SendSynchronous();
        }
    }
}
