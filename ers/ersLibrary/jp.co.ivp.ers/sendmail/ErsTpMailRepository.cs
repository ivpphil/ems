﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsTpMailRepository
        : ErsRepository<ErsTpMail>
    {
        public ErsTpMailRepository()
            : base("tp_mail_t")
        {
        }
    }
}
