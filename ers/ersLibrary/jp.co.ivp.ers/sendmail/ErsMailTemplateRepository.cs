﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using System.Collections;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsMailTemplateRepository
        : ErsRepository<ErsMailTemplate>
    {
        public ErsMailTemplateRepository()
            : base("mail_template_t")
        {
        }

        /// <summary>
        /// Gets IList ErsMailTemplate in mail_templates_t.
        /// </summary>
        /// <returns> Returns IList ErsMailTemplate.</returns>
        public override IList<ErsMailTemplate> Find(Criteria criteria)
        {
            var tmpList = this.ersDB_table.gSelect(criteria);

            List<ErsMailTemplate> retList = new List<ErsMailTemplate>();
            foreach (Dictionary<string, object> data in tmpList)
            {
                var objTemplate = ErsFactory.ersAdministratorFactory.GetErsMailTemplate();
                objTemplate.OverwriteWithParameter(data);
                retList.Add(objTemplate);
            }
            return retList;
        }
    }
}
