﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsMailTemplate
        : ErsRepositoryEntity
    {
        public ErsMailTemplate()
        {
        }
        public override int? id { get; set; }
        public virtual string key { get; set; }
        public virtual string value { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public int? site_id { get; set; }
    }
}
