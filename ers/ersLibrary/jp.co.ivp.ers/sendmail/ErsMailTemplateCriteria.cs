﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsMailTemplateCriteria
        : Criteria
    {
        public virtual string key { set { this.Add(Criteria.GetCriterion("mail_template_t.key", value, Operation.EQUAL)); } }
        public virtual new string value { set { this.Add(Criteria.GetCriterion("mail_template_t.value", value, Operation.EQUAL)); } }

        public int? site_id { set { this.Add(Criteria.GetCriterion("mail_template_t.site_id", value, Operation.EQUAL)); } }
    }
}
