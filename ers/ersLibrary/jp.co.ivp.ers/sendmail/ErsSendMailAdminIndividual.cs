﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSendMailAdminIndividual
        : ErsSendMail
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsSendMailAdminIndividual(int? siteId)
            : base(siteId)
        {
        }

        protected override string key
        {
            get { return "individual"; }
        }

        public virtual void SendMail(ErsModelBase model, string mail_from, string mail_to, bool sendToAdmin, string mail_title, string mail_body, EnumMformat? mformat)
        {
            this.mail_from = mail_from;
            this.mail_to = mail_to;
            this.sendToAdmin = sendToAdmin;
            this.Init(model, mformat.Value);
            this.SendSynchronous(mail_title, mail_body);
        }

    }
}
