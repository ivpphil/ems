﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.atmail;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSendMailQuestAccept
        : ErsSendMail
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsSendMailQuestAccept(int? siteId)
            : base(siteId)
        {
        }

        protected override string key
        {
            get { return "quest_accept"; }
        }

        public void Send(ErsModelBase model, string email, EnumMformat? mformat = null)
        {
            // 設定オブジェクト取得
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (mformat == null)
            {
                 mformat = new DetermineMformatStgy().WithDomain(email);
            }

            // 初期化
            this.Init(model, mformat.Value);

            // 送信元・送信先セット
            this.mail_from = setup.quest_email_to;
            this.mail_to = email;

            // 送信
            this.SendSynchronous();
        }
    }
}
