﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumAgType
    {
        /// <summary>
        /// 1: 管理者
        /// </summary>
        Admin = 1,

        /// <summary>
        /// 2: コールセンター
        /// </summary>
        CallCenter
    }
}
