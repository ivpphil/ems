﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumDeleted
    {
        /// <summary>
        /// 0: 退会していない会員(A member who is not resigned.)
        /// </summary>
        NotDeleted = 0,

        /// <summary>
        /// 1: 退会済みの会員(A member who have resigned.)
        /// </summary>
        Deleted
    }
}
