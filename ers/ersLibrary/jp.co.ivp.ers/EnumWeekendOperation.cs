﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumWeekendOperation
    {
        /// <summary>
        /// 指定なし
        /// </summary>
        NONE = 0,
        /// <summary>
        /// 前倒し
        /// </summary>
        AHEAD_OF_SCHEDULE,
        /// <summary>
        /// 後ろ倒し
        /// </summary>
        AFTER_OF_SCHEDULE
    }
}
