﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Class for UserState
    /// </summary>
    public enum EnumUserState
    {
        LOGIN = 1,                      //有効なログイン乱数割り当て済み
        ASSIGNED_RANSU_BUT_INVALID, //無効な乱数
        ASSIGNED_RANSU,             //有効なセッション乱数
        NON_ASSIGNED_RANSU         //セッション乱数未割り当て
    }
}
