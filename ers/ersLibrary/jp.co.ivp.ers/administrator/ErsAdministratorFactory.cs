﻿using System.Collections.Generic;
using System.Linq;
using jp.co.ivp.ers.administrator.function_group;
using jp.co.ivp.ers.administrator.function_group.specification;
using jp.co.ivp.ers.administrator.function_group.strategy;
using jp.co.ivp.ers.administrator.role_group;
using jp.co.ivp.ers.administrator.role_group.specification;
using jp.co.ivp.ers.administrator.role_group.strategy;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.administrator
{
    /// <summary>
    /// Factory class for ErsAdministrator.
    /// Provide methods that are related in administrator.
    /// </summary>
    public class ErsAdministratorFactory
    {
        protected static ErsAdministratorRepository _ErsAdministratorRepository
        {
            get
            {
                return (ErsAdministratorRepository)ErsCommonContext.GetPooledObject("_ErsAdministratorRepository");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_ErsAdministratorRepository", value);
            }
        }

        /// <summary>
        /// 管理者情報リポジトリを取得する
        /// </summary>
        /// <returns>returns instance of ErsAdministratorRepository.</returns>
        public virtual ErsAdministratorRepository GetErsAdministratorRepository()
        {
            if (_ErsAdministratorRepository == null)
                _ErsAdministratorRepository = new ErsAdministratorRepository();
            return _ErsAdministratorRepository;
        }

        /// <summary>
        /// Obtain instance of ErsAdministratorCriteria class.
        /// </summary>
        /// <returns>returns instance of ErsAdministratorCriteria.</returns>
        public ErsAdministratorCriteria GetErsAdministratorCriteria()
        {
            return new ErsAdministratorCriteria();
        }


        /// <summary>
        /// Obtain instance of ErsMailTemplateRepository class 
        /// メールテンプレートを取得する
        /// </summary>
        /// <returns>returns instance of ErsMailTemplateRepository</returns>
        public virtual ErsMailTemplateRepository GetErsMailTemplateRepository()
        {
            return new ErsMailTemplateRepository();
        }

        public virtual ErsMailTemplateCriteria GetErsMailTemplateCriteria()
        {
            return new ErsMailTemplateCriteria();
        }

        public virtual ErsMailTemplate GetErsMailTemplate()
        {
            return new ErsMailTemplate();
        }

        public virtual ErsMailTemplate GetErsMailTemplateWithKey(string key, int? site_id)
        {
            var repository = this.GetErsMailTemplateRepository();
            var criteria = this.GetErsMailTemplateCriteria();
            criteria.key = key;
            criteria.site_id = site_id;
            var listTemplate = repository.Find(criteria);
            if (listTemplate.Count != 1)
            {
                return null;
            }

            return listTemplate.First();
        }

        /// <summary>
        /// Obtain instance of ErsAdministrator class 
        /// </summary>
        /// <returns>returns instance of ErsAdministrator</returns>
        public virtual ErsAdministrator GetErsAdministrator()
        {
            return new ErsAdministrator();
        }

        /// <summary>
        /// Get records from administrator_t using OverwriteWithParameter function of ErsAdministrator
        /// </summary>
        /// <param name="data">values of administrator saved in dictionary</param>
        /// <returns>return values from ErsAdministrator</returns>
        public virtual ErsAdministrator GetErsAdministratorWithParameter(Dictionary<string, object> data)
        {
            ErsAdministrator tmpData = this.GetErsAdministrator();
            tmpData.OverwriteWithParameter(data);
            return tmpData;
        }

        /// <summary>
        /// Gets list of searched records according to the criteria using user code
        /// </summary>
        /// <param name="user_code">user code</param>
        /// <returns>returns record from administrator_t table when there's record found, returns null if it's not existing.</returns>
        public virtual ErsAdministrator GetErsAdministratorWithUserCode(string user_code)
        {
            var adminRepository = this.GetErsAdministratorRepository();
            var adminCriteria = this.GetErsAdministratorCriteria();
            adminCriteria.user_cd = user_code;
            var dataList = adminRepository.Find(adminCriteria);

            if (dataList.Count == 1)
            {
                return dataList[0];
            }

            return null;
        }

        /// <summary>
        /// Gets list of searched records according to the criteria using id
        /// </summary>
        /// <param name="id">administrator ID use for finding of record using ErsAdministratorCriteria</param>
        /// <returns>returns data from administrator_t table, returns null if not existing</returns>
        public ErsAdministrator GetErsAdministratorWithId(int? id)
        {
            if (!id.HasValue)
            {
                return null;
            }

            var adminRepository = this.GetErsAdministratorRepository();
            var adminCriteria = this.GetErsAdministratorCriteria();
            adminCriteria.id = id.Value;
            var dataList = adminRepository.Find(adminCriteria);

            if (dataList.Count == 1)
            {
                return dataList[0];
            }

            return null;
        }

        public ErsFunctionGroup GetErsFunctionGroup()
        {
            return new ErsFunctionGroup();
        }

        public ErsFunctionGroupRepository GetErsFunctionGroupRepository()
        {
            return new ErsFunctionGroupRepository();
        }

        public ErsFunctionGroupCriteria GetErsFunctionGroupCriteria()
        {
            return new ErsFunctionGroupCriteria();
        }

        public virtual FunctionNameListSearchSpec GetFunctionNameListSearchSpec()
        {
            return new FunctionNameListSearchSpec();
        }

        public virtual MenuNameListSearchSpec GetMenuNameListSearchSpec()
        {
            return new MenuNameListSearchSpec();
        }

        public virtual FunctionListSearchSpec GetFunctionListSpec()
        {
            return new FunctionListSearchSpec();
        }

        public virtual ErsFunctionGroup GetErsFunctionGroupWithId(int id)
        {
            var repository = this.GetErsFunctionGroupRepository();
            var criteria = ErsFactory.ersAdministratorFactory.GetErsFunctionGroupCriteria();

            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");

            return list[0];
        }

        public virtual ErsFunctionGroup GetErsFunctionGroupWithParameters(Dictionary<string, object> parameters)
        {
            var interest = this.GetErsFunctionGroup();
            interest.OverwriteWithParameter(parameters);
            return interest;
        }

        public virtual ErsRoleGroup GetErsRoleGroup()
        {
            return new ErsRoleGroup();
        }

        public virtual ErsRoleGroupRepository GetErsRoleGroupRepository()
        {
            return new ErsRoleGroupRepository();
        }

        /// <summary>
        /// Get Criteria of ErsRoleGroupRepository.
        /// </summary>
        /// <returns></returns>
        public ErsRoleGroupCriteria GetErsRoleGroupCriteria()
        {
            return new ErsRoleGroupCriteria();
        }

        public virtual ErsRoleGroup GetErsRoleGroupWithModel(ErsModelBase model)
        {
            var group = this.GetErsRoleGroup();
            group.OverwriteWithModel(model);
            return group;
        }

        public ErsRoleGroup GetErsRoleGroupWithId(int id)
        {
            var adminRepository = this.GetErsRoleGroupRepository();
            var adminCriteria = this.GetErsRoleGroupCriteria();
            adminCriteria.id = id;
            var dataList = adminRepository.Find(adminCriteria);

            if (dataList.Count == 1)
                return dataList[0];
            else
                return null;
        }

        public virtual UserRoleSpecification GetUserRoleSpecification()
        {
            return new UserRoleSpecification();
        }

        public virtual CheckRoleActionStgy GetCheckRoleActionStgy()
        {
            return new CheckRoleActionStgy();
        }

        public virtual CheckDuplicateRoleGnameStgy GetCheckDuplicateRoleGnameStgy()
        {
            return new CheckDuplicateRoleGnameStgy();
        }

        public virtual CheckRoleGroupStgy GetCheckRoleGroupStgy()
        {
            return new CheckRoleGroupStgy();
        }

        public virtual ErsRoleGroup GetErsRoleGroupWithParameters(Dictionary<string, object> parameters)
        {
            var newsrolegroup = this.GetErsRoleGroup();
            newsrolegroup.OverwriteWithParameter(parameters);
            return newsrolegroup;
        }

        public virtual ValidateRoleGroupStgy GetValidateRoleGroupStgy()
        {
            return new ValidateRoleGroupStgy();
        }
    }
}
