﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;

namespace jp.co.ivp.ers.administrator.role_group
{
    public class ErsRoleGroup
        : ErsRepositoryEntity
    {
        public const string DEFAULT_USER_CODE = "0";

        public override int? id { get; set; }
        public virtual string role_gcode { get; set; }
        public virtual string role_gname { get; set; }
        public virtual string[] role_action { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual DateTime? intime { get; set; }
    }
}
