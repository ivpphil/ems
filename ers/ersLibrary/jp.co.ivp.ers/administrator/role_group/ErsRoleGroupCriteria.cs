﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.administrator.role_group
{
    public class ErsRoleGroupCriteria
        : Criteria
    {
        public int id
        {
            set
            {
                this.Add(Criteria.GetCriterion("role_group_t.id", value, Operation.EQUAL));
            }
        }

        public virtual int not_id
        {
            set
            {
                Add(Criteria.GetCriterion("role_group_t.id", value, Criteria.Operation.NOT_EQUAL));
            }
        }

        public string role_action
        {
            set
            {
                this.Add(Criteria.GetCriterion("role_group_t.role_action", value, Operation.ANY_EQUAL));
            }
        }

        public virtual string user_cd
        {
            set
            {
                Add(Criteria.GetCriterion("administrator_t.user_cd", value, Criteria.Operation.EQUAL));
            }
        }

        public virtual string role_gname
        {
            set
            {
                Add(Criteria.GetCriterion("role_group_t.role_gname", value, Criteria.Operation.EQUAL));
            }
        }
        
        public string role_gcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("role_group_t.role_gcode", value, Operation.EQUAL));
            }
        }

        public EnumActive? active 
        {
            set
            {
                Add(Criteria.GetCriterion("role_group_t.active", (int)EnumActive.Active, Operation.EQUAL));
            }
        }

        public virtual void SetActiveOnly()
        {
            this.active = EnumActive.Active;
        }

        public void SetOrderById(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("role_group_t.id", orderBy);
        }

        public void SetOrderByIntime(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("role_group_t.intime", orderBy);
        }
    }
}
