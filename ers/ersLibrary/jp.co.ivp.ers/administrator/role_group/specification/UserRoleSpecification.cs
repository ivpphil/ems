﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.administrator.role_group.specification
{
    public class UserRoleSpecification
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            string ret = "SELECT DISTINCT role_group_t.* FROM administrator_t ";
            ret += "INNER JOIN role_group_t   ON administrator_t.role_gcode = role_group_t.role_gcode";

            return ret;
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            string ret = "SELECT COUNT(DISTINCT role_group_t.id) AS " + countColumnAlias + " FROM administrator_t ";
            ret += "INNER JOIN role_group_t   ON administrator_t.role_gcode = role_group_t.role_gcode";

            return ret;
        }
    }
}
