﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.administrator.role_group.strategy
{
    public class ValidateRoleGroupStgy
    {
        public virtual IEnumerable<ValidationResult> Validate(IEnumerable<string> role_gcode_list)
        {
            var fieldKey = "role_gcode";

            foreach (var value in role_gcode_list)
            {
                if (string.IsNullOrEmpty(value))
                {
                    continue;
                }

                var NewsRolegroup = ErsFactory.ersViewServiceFactory.GetErsViewRoleGroupService();

                if (!NewsRolegroup.ExistValue(value))
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName(fieldKey)), new[] { fieldKey });
                }
            }
        }
    }
}
