﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.administrator.role_group.strategy
{

    public class CheckRoleGroupStgy
    {
        
        /// <summary>
        /// Return true if the same RoleGname exists.
        /// </summary>
        /// <param name="role_gname"></param>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual ValidationResult CheckId(int id)
        {
            var repository = ErsFactory.ersAdministratorFactory.GetErsRoleGroupRepository();

            var criteria = ErsFactory.ersAdministratorFactory.GetErsRoleGroupCriteria();

            criteria.id = id;

            if (repository.GetRecordCount(criteria) == 0)
            {
                return new ValidationResult(ErsResources.GetMessage("10207", new[] { ErsResources.GetFieldName("role_group_t.id") }), new[] { "id" });
            }

            return null;

        }

        /// <summary>
        /// Return true if the same RoleGname exists.
        /// </summary>
        /// <param name="role_gname"></param>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual ValidationResult CheckRoleGcode(string role_gcode)
        {
            var repository = ErsFactory.ersAdministratorFactory.GetErsRoleGroupRepository();

            var criteria = ErsFactory.ersAdministratorFactory.GetErsRoleGroupCriteria();

            criteria.role_gcode = role_gcode;

            if (repository.GetRecordCount(criteria) == 0)
            {
                return new ValidationResult(ErsResources.GetMessage("10207", new[] { ErsResources.GetFieldName("role_group_t.role_gcode") }), new[] { "role_gcode" });
            }

            return null;

        }
    }
}   
