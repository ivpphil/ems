﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.administrator.role_group.strategy
{

    public class CheckDuplicateRoleGnameStgy
    {
        
        /// <summary>
        /// Return true if the same RoleGname exists.
        /// </summary>
        /// <param name="role_gname"></param>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual Boolean  CheckDuplicate(string role_gname, int id)
        {
            var repository = ErsFactory.ersAdministratorFactory.GetErsRoleGroupRepository();

            var criteria = ErsFactory.ersAdministratorFactory.GetErsRoleGroupCriteria();
            criteria.role_gname = role_gname;

            if (id != 0)
                criteria.not_id = id;

            if (repository.GetRecordCount(criteria)==0)
                return true;

            return false;

        }
    }
}   
