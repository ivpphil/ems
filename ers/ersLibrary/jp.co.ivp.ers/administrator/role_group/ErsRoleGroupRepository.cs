﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.util;
using System.Collections;

namespace jp.co.ivp.ers.administrator.role_group
{
    public class ErsRoleGroupRepository
        : ErsRepository<ErsRoleGroup>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsRoleGroupRepository()
            : base("role_group_t")
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsRoleGroupRepository(ErsDatabase objDB)
            : base("role_group_t", objDB)
        {
        }

        public override void Insert(ErsRoleGroup obj, bool storeNewIdToObject = false)
        {
            if (storeNewIdToObject == true)
            {
                obj.id = this.ersDB_table.GetNextSequence();
                obj.role_gcode = obj.id.ToString();
            }
            base.Insert(obj);
        }
    }
}
