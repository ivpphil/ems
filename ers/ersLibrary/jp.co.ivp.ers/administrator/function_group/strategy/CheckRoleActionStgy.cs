﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.administrator.function_group.strategy
{

    public class CheckRoleActionStgy
    {
        
        /// <summary>
        /// Return true if the same RoleGname exists.
        /// </summary>
        /// <param name="role_gname"></param>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual ValidationResult Check(string role_action)
        {
            var repository = ErsFactory.ersAdministratorFactory.GetErsFunctionGroupRepository();

            var criteria = ErsFactory.ersAdministratorFactory.GetErsFunctionGroupCriteria();
            criteria.func_id = role_action;

            if (repository.GetRecordCount(criteria) == 0)
            {
                return new ValidationResult(ErsResources.GetMessage("10207", new[] { ErsResources.GetFieldName("role_group_t.role_action") }), new[] { "role_action" });
            }

            return null;

        }
    }
}   
