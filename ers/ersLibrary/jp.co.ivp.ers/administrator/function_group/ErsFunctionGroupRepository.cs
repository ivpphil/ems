﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.administrator.function_group
{
    public class ErsFunctionGroupRepository
        :ErsRepository<ErsFunctionGroup>
    {
        public ErsFunctionGroupRepository()
            : base("function_group_t")
        {
        }

        public ErsFunctionGroupRepository(ErsDatabase objDB)
            : base("function_group_t", objDB)
        {
        }

        public IList<ErsFunctionGroup> GetFunctionGroupList(Criteria criteria)
        {
            var retList = new List<ErsFunctionGroup>();

            var specSearchData = ErsFactory.ersAdministratorFactory.GetFunctionListSpec();
            var list = specSearchData.GetSearchData(criteria);

            if (list.Count == 0)
                return new List<ErsFunctionGroup>();

            foreach (var dr in list)
            {
                var functionGroup = ErsFactory.ersAdministratorFactory.GetErsFunctionGroupWithParameters(dr);
                retList.Add(functionGroup);
            }
            return retList;
        }
    }
}
