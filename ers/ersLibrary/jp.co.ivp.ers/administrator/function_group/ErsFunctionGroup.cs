﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.administrator.function_group
{
    public class ErsFunctionGroup  : ErsRepositoryEntity
    {
        public const string GLOBAL_FUNC_ID = "global";

        public override int? id { get; set; }
        public virtual EnumActive? active { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual DateTime? intime { get; set; }  
        public virtual string func_id { get; set; }  
        public virtual string func_name { get; set; }
        public virtual string menu_id { get; set; }
        public virtual string menu_name { get; set; }
        public virtual string controller_name { get; set; }
        public virtual string method_name { get; set; }
        public virtual int? disp_order { get; set; }
    }
}
