﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.administrator.function_group.specification
{
    public class FunctionListSearchSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return " SELECT func_id, func_name, menu_id, menu_name, disp_order, active FROM function_group_t ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(*) AS " + countColumnAlias + " FROM function_group_t  ";
        }
    }
}
