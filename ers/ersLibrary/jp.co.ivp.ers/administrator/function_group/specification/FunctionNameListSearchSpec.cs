﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.administrator.function_group.specification
{
    public class FunctionNameListSearchSpec
           : ISpecificationForSQL
    {
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return ErsRepository.SelectSatisfying(this, criteria);
        }

        public string asSQL()
        {
            return "SELECT DISTINCT ON (function_group_t.disp_order, function_group_t.func_id, function_group_t.func_name) function_group_t.* "
                    + "FROM function_group_t ";
        }
    }
}
