﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.administrator.function_group.specification
{
    public class MenuNameListSearchSpec
           : ISpecificationForSQL
    {
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return ErsRepository.SelectSatisfying(this, criteria);
        }

        public string asSQL()
        {
            return "SELECT menu_id, menu_name "
                    + "FROM function_group_t ";
        }
    }
}
