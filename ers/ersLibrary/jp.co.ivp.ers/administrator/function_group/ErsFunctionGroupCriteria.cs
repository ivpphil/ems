﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.administrator.function_group
{
    public class ErsFunctionGroupCriteria
        : Criteria
    {
        public virtual int? id
        {
            set
            {
                Add(Criteria.GetCriterion("function_group_t.id", value, Operation.EQUAL));
            }
        }

        public virtual string menu_id
        {
            set
            {
                Add(Criteria.GetCriterion("function_group_t.menu_id", value, Operation.EQUAL));
            }
        }

        public virtual string controller_name
        {
            set
            {
                Add(Criteria.GetCriterion("function_group_t.controller_name", value, Operation.EQUAL));
            }
        }

        public virtual string method_name
        {
            set
            {
                Add(Criteria.GetCriterion("function_group_t.method_name", value, Operation.EQUAL));
            }
        }

        public virtual void SetActiveOnly()
        {
            Add(Criteria.GetCriterion("function_group_t.active", (int)EnumActive.Active, Operation.EQUAL));
        }

        public void SetOrderByDispOrder(OrderBy orderBy)
        {
            this.AddOrderBy("function_group_t.disp_order", orderBy);
        }

        public void SetOrderByFunc_id(OrderBy orderBy)
        {
            this.AddOrderBy("function_group_t.func_id", orderBy);
        }

        public void SetOrderByFunc_name(OrderBy orderBy)
        {
            this.AddOrderBy("function_group_t.func_name", orderBy);
        }

        // administrator_t
        public virtual string user_cd
        {
            set
            {
                Add(Criteria.GetCriterion("administrator_t.user_cd", value, Operation.EQUAL));
            }
        }

        public string func_id
        {
            set
            {
                Add(Criteria.GetCriterion("function_group_t.func_id", value, Operation.EQUAL));
            }
        }

        public string func_id_not_equal
        {
            set
            {
                Add(Criteria.GetCriterion("function_group_t.func_id", value, Operation.NOT_EQUAL));
            }
        }

        public void SetAuthedFunctionUser_cd(string user_cd)
        {
            var dictionary = new Dictionary<string, object>()
            {
                {"user_cd", user_cd}
            };
            var sql = "function_group_t.func_id IN (SELECT unnest(role_group_t.role_action) FROM administrator_t INNER JOIN role_group_t ON administrator_t.role_gcode = role_group_t.role_gcode WHERE administrator_t.user_cd = :user_cd)";
            this.Add(Criteria.GetUniversalCriterion(sql, dictionary));
        }

        public void SetAuthedMenuUser_cd(string user_cd)
        {
            var dictionary = new Dictionary<string, object>()
            {
                {"user_cd", user_cd}
            };
            var sql = "function_group_t.menu_id IN ("
                + "SELECT DISTINCT menu_id "
                + "FROM function_group_t "
                + "WHERE "
                + "func_id IN (SELECT unnest(role_group_t.role_action) FROM administrator_t INNER JOIN role_group_t ON administrator_t.role_gcode = role_group_t.role_gcode WHERE administrator_t.user_cd = :user_cd))";
            this.Add(Criteria.GetUniversalCriterion(sql, dictionary));
        }

        public void SetRequestedAction(string controllerName, string actionName)
        {
            var dictionary = new Dictionary<string, object>()
            {
                {"controller_name", controllerName},
                {"method_name", actionName},
                {"active", EnumActive.Active},
            };
            var sql = "function_group_t.menu_id IN (SELECT function_group_t.menu_id FROM function_group_t WHERE function_group_t.controller_name = :controller_name AND  function_group_t.method_name = :method_name AND function_group_t.active = :active)";
            this.Add(Criteria.GetUniversalCriterion(sql, dictionary));
        }

        public void SetGroupByMenu_id()
        {
            this.AddGroupBy("function_group_t.menu_id");
        }

        public void SetGroupByMenu_name()
        {
            this.AddGroupBy("function_group_t.menu_name");
        }

        public void SetOrderByMinDispOrder(OrderBy orderBy)
        {
            this.AddOrderBy("MIN(function_group_t.disp_order)", orderBy);
        }

        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("function_group_t.id", orderBy);
        }
    }
}
