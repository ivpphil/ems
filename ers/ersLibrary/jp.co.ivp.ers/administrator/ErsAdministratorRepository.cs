﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.util;
using System.Collections;

namespace jp.co.ivp.ers.administrator
{
    /// <summary>
    /// Provide methods to connect with adminstrator_t table. 
    /// Inherits ErsRepository<ErsAdministrator>
    /// </summary>
    public class ErsAdministratorRepository
        : ErsRepository<ErsAdministrator>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsAdministratorRepository()
            : base("administrator_t")
        {
        }
    }
}
