﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.state;

namespace jp.co.ivp.ers.administrator
{
 
    /// <summary>
    /// Hold values from administrator_t table.
    /// Inherits ErsRepositoryEntity class.
    /// Log management master screen.
    /// </summary>
    public class ErsAdministrator
        : ErsRepositoryEntity
    {
        /// <summary>
        /// default user code
        /// </summary>
        public const string DEFAULT_USER_CODE = "0";

        /// <summary>
        /// Unique key ID
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// user code
        /// </summary>
        public virtual string user_cd { get; set; }

        /// <summary>
        /// user log in 
        /// this property will be use for log in
        /// </summary>
        public virtual string user_login_id { get; set; }

        /// <summary>
        /// name of the user
        /// </summary>
        public virtual string user_name { get; set; }

        /// <summary>
        /// password of the user
        /// </summary>
        public virtual string passwd { get; set; }

        /// <summary>
        /// password modify date
        /// </summary>
        public virtual DateTime? pass_date { get; set; }

        /// <summary>
        /// password modify date
        /// </summary>
        public virtual string role_gcode { get; set; }

        /// <summary>
        /// agent_id
        /// </summary>
        public virtual int? agent_id { get; set; }

        /// <summary>
        /// intime
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// utime
        /// </summary>
        public virtual DateTime? utime { get; set; }
    }
}
