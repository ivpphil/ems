﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.administrator
{
    /// <summary>
    /// represents searched conditions for properties that are in ErsAdministrator class.
    /// Inherits Criteria class.
    /// </summary>
    public class ErsAdministratorCriteria
        : Criteria
    {
        /// <summary>
        /// sets criteria for user code with the condition of administrator_t.user_cd = value
        /// </summary>
        public string user_cd
        {
            set
            {
                this.Add(Criteria.GetCriterion("administrator_t.user_cd", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for id with the condition of administrator_t.id = value
        /// </summary>
        public int id
        {
            set
            {
                this.Add(Criteria.GetCriterion("administrator_t.id", value, Operation.EQUAL));
            }
        }

        public int? id_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("administrator_t.id", value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// sets criteria for user_login_id with the condition of administrator_t.user_login_id = value
        /// </summary>
        public string user_login_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("administrator_t.user_login_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// gets sort order by id
        /// </summary>
        public void SetOrderById(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("administrator_t.id", orderBy);
        }

        public string passwd
        {
            set
            {
                this.Add(Criteria.GetCriterion("administrator_t.passwd", value, Criteria.Operation.EQUAL));
            }
        }
    }
}
