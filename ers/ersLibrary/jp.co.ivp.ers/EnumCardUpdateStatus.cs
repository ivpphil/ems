﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// カード洗い替えステータス
    /// </summary>
    public enum EnumCardUpdateStatus
    {
        /// <summary>
        /// 0: 正常
        /// </summary>
        Success = 0,

        /// <summary>
        /// 1: エラー
        /// </summary>
        Error
    }
}
