﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// 販売区分
    /// </summary>
    public enum EnumSalePatternType
        : short
    {
        NORMAL = 1,
        REGULAR,
        ALL
    }
}
