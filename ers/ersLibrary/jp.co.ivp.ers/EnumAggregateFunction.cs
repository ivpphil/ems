﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumAggregateFunction
    {
        Sum = 1,
        Count = 2,
        Average = 3,
        Minimum = 4,
        Maximum = 5,
    }
}
