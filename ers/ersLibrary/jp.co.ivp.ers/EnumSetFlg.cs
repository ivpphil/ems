﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumSetFlg
        : short
    {
        IsNotSet = 0,
        IsSet
    }
}
