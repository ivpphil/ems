﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.information
{
    public class ErsCtsInformationCriteria : Criteria
	{

        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_information_t.id", value, Operation.EQUAL));
            }
        }

        public string ag_name
        {
            set
            {
                Add(Criteria.GetCriterion("cts_information_t.ag_name", value, Operation.EQUAL));
            }
        }


        public string agent_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_information_t.agent_id", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("cts_information_t.active", value, Operation.EQUAL));
            }
        }


        public int readid
        {
            set
            {
                Add(Criteria.GetCriterion("cts_information_t.readid", value, Operation.EQUAL));
            }
        }

        public DateTime intimeFrom
        {
            set
            {
                Add(Criteria.GetCriterion("cts_information_t.utime", value, Operation.GREATER_EQUAL));
            }
        }

        public DateTime intimeTo
        {
            set
            {
                Add(Criteria.GetCriterion("cts_information_t.utime", value, Operation.LESS_EQUAL));
            }
        }

        public int? exclude_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_information_t.id", value, Operation.NOT_EQUAL));
            }
        }

		public DateTime utimeFrom
		{
			set
			{
				Add(Criteria.GetCriterion("cts_information_t.utime", value, Operation.GREATER_EQUAL));
			}
		}

		public DateTime utimeTo
		{
			set
			{
				Add(Criteria.GetCriterion("cts_information_t.utime", value, Operation.LESS_EQUAL));
			}
		}


        public int read_Info_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_information_read_t.information_id", value, Operation.EQUAL));
            }
        }

        public int? read_user_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_information_read_t.user_id", Convert.ToString(value), Operation.EQUAL));
            }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("id", orderBy);
        }

        public void SetOrderByUTime(OrderBy orderBy)
        {
            AddOrderBy("utime", orderBy);
        }

        public EnumAgType? ag_type
        {
            set
            {
                Add(Criteria.GetCriterion("cts_login_t.ag_type", value, Operation.LESS_EQUAL));
            }
        }

        public EnumAgType? read_ag_type
        {
            set
            {
                Add(Criteria.GetCriterion("cts_information_t.ag_type", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("cts_information_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_information_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? site_id_tb
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("tb.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("tb.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? site_id_of_read
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("cts_information_read_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_information_read_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
	}
}
