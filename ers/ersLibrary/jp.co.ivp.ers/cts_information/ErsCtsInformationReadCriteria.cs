﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.cts_information
{
    public class ErsCtsInformationReadCriteria
        : Criteria
    {
        public int? information_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_information_read_t.information_id", value, Operation.EQUAL));
            }
        }
    }
}
