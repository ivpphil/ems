﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.cts_information
{
    public class ErsCtsInformationRead : ErsRepositoryEntity
	{
        public override int? id { get; set; }

        public int? user_id { get; set; }

        public int? information_id { get; set; }

        public DateTime? intime { get; set; }

        public int? site_id { get; set; }
	}
}
