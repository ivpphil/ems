﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.information.specification;
using jp.co.ivp.ers.cts_information;

namespace jp.co.ivp.ers.information
{
    public class ErsCtsInformationFactory
    {

        public ErsCtsInformationCriteria GetErsCtsInformationCriteria()
        {
            return new ErsCtsInformationCriteria();
        }

        public ErsCtsInformationRepository GetErsCtsInformationRepository()
        {
            return new ErsCtsInformationRepository();
        }

        public ErsCtsInformation GetErsCtsInformation()
        {
            return new ErsCtsInformation();
        }

        public ErsCtsInformation GetErsInformationWithModel(ErsModelBase model)
        {
            var Information = this.GetErsCtsInformation();
            Information.OverwriteWithModel(model);
            return Information;
        }

        public ErsCtsInformation GetErsCtsInformationWithParameters(Dictionary<string, object> parameters)
        {
            var Information = this.GetErsCtsInformation();
            Information.OverwriteWithParameter(parameters);
            return Information;
        }

        public ErsCtsInformation GetErsCtsInformationWithId(int id)
        {
            var repository = this.GetErsCtsInformationRepository();
            var criteria = this.GetErsCtsInformationCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");

            return list[0];
        }

        public virtual CtsInformationSearchSpecification GetCtsInformationSearchSpecification()
        {
            return new CtsInformationSearchSpecification();
        }

        public virtual ErsCtsInformationUnreadSpecification GetCtsInformationUnreadSpecification()
        {
            return new ErsCtsInformationUnreadSpecification();
        }

        public virtual ErsCtsInformationReadRepository GetErsCtsInformationReadRepository()
        {
            return new ErsCtsInformationReadRepository();
        }

        public virtual ErsCtsInformationReadCriteria GetErsCtsInformationReadCriteria()
        {
            return new ErsCtsInformationReadCriteria();
        }

        public virtual ErsCtsInformationRead GetErsCtsInformationRead()
        {
            return new ErsCtsInformationRead();
        }

        public virtual ErsCtsInformationRead GetErsCtsInformationReadWithParameters(Dictionary<string, object> dr)
        {
            var objCtsInformationRead = this.GetErsCtsInformationRead();
            objCtsInformationRead.OverwriteWithParameter(dr);
            return objCtsInformationRead;
        }
    }
}
