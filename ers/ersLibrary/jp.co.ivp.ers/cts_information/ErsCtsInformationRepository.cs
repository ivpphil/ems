﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.information
{
    public class ErsCtsInformationRepository
        : ErsRepository<ErsCtsInformation>
    {
        public ErsCtsInformationRepository()
            : base("cts_information_t")
        {
        }

        public ErsCtsInformationRepository(ErsDatabase objDB)
            : base("cts_information_t", objDB)
        {
        }

        public IList<ErsCtsInformation> FindUnread(db.Criteria criteria, int infoid, EnumAgType? agType = null)
        {
            var spec = ErsFactory.ersCtsInformationFactory.GetCtsInformationUnreadSpecification();
            
            List<ErsCtsInformation> lstRet = new List<ErsCtsInformation>();
            var list = spec.GetUnreadSearchData(criteria,  infoid, agType);
            foreach (var dr in list)
            {
                var info = ErsFactory.ersCtsInformationFactory .GetErsCtsInformationWithParameters(dr);
                lstRet.Add(info);
            }
            return lstRet;
        }
    }
}
