﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.information.specification
{
    public class ErsCtsInformationUnreadSpecification
        : ISpecificationForSQL
    {
        public virtual List<Dictionary<string, object>> GetUnreadSearchData(Criteria criteria, int infoid, EnumAgType? agType)
        {
            this.infoid = infoid;
            this.agType = agType;

            return ErsRepository.SelectSatisfying(this, criteria);
        }

        public virtual int infoid { get; set; }
        public virtual EnumAgType? agType { get; set; }

        /// <summary>
        /// SQL文
        /// </summary>
        /// <returns>SQL文</returns>
        public virtual string asSQL()
        {
            string strQuery = @"select tb.* from (select user_id,ag_name,site_id, ";
            strQuery += " (select intime from cts_information_read_t ";
            strQuery += "   where cts_information_read_t.information_id = " +  this.infoid;
            strQuery += "     and to_number(cts_information_read_t.user_id,'0000000000') = cts_login_t.id) as intime ";
            strQuery += "    from cts_login_t ";

            if (agType.HasValue)
            {
                strQuery += "   where active = " + (int)EnumActive.Active + " and ag_type = " + (int)this.agType + ") as tb ";
            }
            else
            {
                strQuery += "   where active = " + (int)EnumActive.Active + ") as tb ";
            }
            
            //strQuery += "   where tb.intime is null ";
            return strQuery;
        }
    }
}
