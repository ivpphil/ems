﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.information.specification
{
    [Obsolete("分割する")]
    public class CtsInformationSearchSpecification
    {
        public virtual List<Dictionary<string, object>> GetInfoData(Criteria criteria, int user_id)
        {
            var specificationForSQL = new InformationSearchRecordSpecification();
            specificationForSQL.user_id = user_id;
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        /// <summary>
        /// 検索データ取得
        /// </summary>
        /// <param name="crtOrder">クライテリア</param>
        /// <returns>データテーブル</returns>
        public virtual int GetCountData(Criteria criteria)
        {
            var specificationForSQL = new InformationSearchCountSpecification();

            var record = ErsRepository.SelectSatisfying(specificationForSQL, criteria);

            if (record.Count == 0)
                return 0;

            return Convert.ToInt32(record[0]["count"]);
        }


        public virtual int GetUnreadCountData(Criteria criteria, int user_id)
        {
            var specificationForSQL = new UnreadInformationSearchCountSpecification();
            specificationForSQL.user_id = user_id;
            var record = ErsRepository.SelectSatisfying(specificationForSQL, criteria);

            if (record.Count == 0)
                return 0;

            return Convert.ToInt32(record[0]["count"]);
        }

        public virtual List<Dictionary<string, object>> GetCountReadInfoData(Criteria criteria)
        {
            var specificationForSQL = new InformationReadRecordSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetCalendarInfoData(Criteria criteria)
        {
            var specificationForSQL = new InformationCalendarSearchSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }



        internal protected class InformationSearchRecordSpecification
           : ISpecificationForSQL
        {
            public virtual int user_id { get; set; }

            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                return " SELECT DISTINCT cts_information_t.*, cts_login_t.ag_type, COALESCE(cts_information_read_t.id, 0) as readid, cts_information_read_t.user_id FROM cts_information_t "
                    + " INNER JOIN cts_login_t ON to_number(cts_information_t.agent_id,'0000000000') = cts_login_t.id "
                    + " LEFT JOIN cts_information_read_t ON cts_information_t.id = cts_information_read_t.information_id AND cts_information_read_t.user_id = '" + this.user_id + "'";
            }
        }

        internal protected class InformationSearchCountSpecification
            : ISpecificationForSQL
        {

            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {

                return " SELECT COUNT(DISTINCT(cts_information_t.ID)) FROM cts_information_t "
                    + " INNER JOIN cts_login_t ON to_number(cts_information_t.agent_id,'0000000000') = cts_login_t.id ";
            }
        }


        internal protected class UnreadInformationSearchCountSpecification
        : ISpecificationForSQL
        {
             public virtual int user_id { get; set; }
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                return " SELECT COUNT(cts_information_t.readid) FROM ( "
                     + " SELECT COALESCE(cts_information_read_t.id, 0) as readid, cts_information_t.active, cts_login_t.ag_type, cts_information_t.utime, cts_information_t.site_id FROM cts_information_t "
                     + "  INNER JOIN cts_login_t ON to_number(cts_information_t.agent_id,'0000000000') = cts_login_t.id "
                     + "   LEFT JOIN cts_information_read_t ON cts_information_t.id = cts_information_read_t.information_id AND cts_information_read_t.user_id = '" + this.user_id + "') AS cts_information_t ";

            }
        }

        internal protected class InformationCalendarSearchSpecification
        : ISpecificationForSQL
        {

            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                return " SELECT cts_information_t.utime, count(cts_information_t.id) as cnt FROM ( "
										 + " SELECT date_trunc('day',cts_information_t.utime) as utime, cts_information_t.id, cts_information_t.agent_id, cts_information_t.active "
                     + "   FROM cts_information_t) as cts_information_t  ";


            }
        }

        internal protected class InformationReadRecordSpecification
        : ISpecificationForSQL
        {
            public virtual string user_id { get; set; }

            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                return " SELECT COUNT(cts_information_read_t.id)  FROM cts_information_read_t ";
                     
            }
        }
    }
}
