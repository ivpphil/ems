﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.cts_information
{
    public class ErsCtsInformationReadRepository
        : ErsRepository<ErsCtsInformationRead>
    {
        public ErsCtsInformationReadRepository()
            : base("cts_information_read_t")
        {
        }
    }
}
