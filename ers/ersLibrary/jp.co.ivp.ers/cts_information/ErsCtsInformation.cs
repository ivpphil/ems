﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.information
{
    public class ErsCtsInformation : ErsRepositoryEntity
	{
        public override int? id { get; set; }

        public string ag_name { get; set; }

        public string agent_id { get; set; }

        public DateTime? utime { get; set; }

        public DateTime? intime { get; set; }

        public string title { get; set; }

        public string contents { get; set; }

        public EnumActive? active { get; set; }

        public int? readid { get; set; }

        public string user_id { get; set; }

        public int? site_id { get; set; }
	}
}
