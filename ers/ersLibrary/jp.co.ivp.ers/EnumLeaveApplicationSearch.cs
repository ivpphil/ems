﻿namespace jp.co.ivp.ers
{
    public enum EnumLeaveApplicationSearch
    {
        PendingApplicationList = 1,
        MyApplicationList,
        EmployeeOrTeamApplicationList
    }
}
