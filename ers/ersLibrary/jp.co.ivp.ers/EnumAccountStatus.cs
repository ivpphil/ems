﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumAccountStatus
    {
        /// <summary>
        /// 0: Account who is not lock
        /// </summary>
        NotLocked = 0,

        /// <summary>
        /// 1: Account is locked
        /// </summary>
        Locked
    }
}
