﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Enums for OrderType (Usually, Subscription, Both Usually And Subscription)
    /// Using [target_item_t.order_type]
    /// </summary>
    public enum EnumOrderType
    {
        /// <summary>
        /// 1 : 通常 [Usually]
        /// </summary>
        Usually = 1,

        /// <summary>
        /// 2 : 定期 [Subscription]
        /// </summary>
        Subscription = 2,

        /// <summary>
        /// 9 : 通常＋定期 [Both Usually And Subscription]
        /// </summary>
        BothUsuallyAndSubscription = 9,

        /// <summary>
        /// 10 : 同梱処理
        /// </summary>
        DocBundled = 10
    }
}
