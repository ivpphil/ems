﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsKeywordsCriteria
        : Criteria
    {
        public string keyword
        {
            set
            {
                this.Add(Criteria.GetCriterion("keywords_t.keyword", value, Operation.EQUAL));
            }
        }

        public string keyword_prefix_search
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("keywords_t.keyword", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }
        public string keyword_search
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("keywords_t.keyword", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public string disp_keyword
        {
            set
            {
                this.Add(Criteria.GetCriterion("keywords_t.disp_keyword", value, Operation.EQUAL));
            }
        }

        public void SetOrderByKeywordOctetLength(OrderBy orderBy)
        {
            this.AddOrderBy("OCTET_LENGTH(keyword)", orderBy);
        }

        public void SetOrderByIntime(OrderBy orderBy)
        {
            this.AddOrderBy("keywords_t.intime", orderBy);
        }
 
        public void SetIsolatedKeywordOnly()
        {
            this.Add(Criteria.GetUniversalCriterion("NOT EXISTS (SELECT * FROM keywords_relation_t WHERE keywords_relation_t.keyword_id = keywords_t.id)"));
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("keywords_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("keywords_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}
