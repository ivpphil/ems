﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsSetMerchandiseRepository
        : ErsRepository<ErsSetMerchandise>
    {
        /// <summary>
        /// 
        /// </summary>
        protected internal ErsSetMerchandiseRepository()
            : base("set_master_t")
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDB"></param>
        public ErsSetMerchandiseRepository(ErsDatabase objDB)
            : base("set_master_t", objDB)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public override IList<ErsSetMerchandise> Find(Criteria criteria)
        {
            var retList = new List<ErsSetMerchandise>();

            var searchSpec = ErsFactory.ersMerchandiseFactory.GetSerMerchandiseSearchSpec();
            var list = searchSpec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                var news = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseWithParameters(dr);
                retList.Add(news);
            }
            return retList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public override long GetRecordCount(Criteria criteria)
        {
            var searchSpec = ErsFactory.ersMerchandiseFactory.GetSerMerchandiseSearchSpec();
            return searchSpec.GetCountData(criteria);
        }
    }
}
