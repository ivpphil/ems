﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsPriceCriteria
        : Criteria
    {

        public virtual int? id
        {
            set { this.Add(Criteria.GetCriterion("price_t.id", value, Operation.EQUAL)); }
        }

        public virtual int? not_id
        {
            set { this.Add(Criteria.GetCriterion("price_t.id", value, Operation.NOT_EQUAL)); }
        }

        public virtual string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("price_t.scode", value, Operation.EQUAL));
            }
        }

        public virtual string s_master_scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.scode", value, Operation.EQUAL));
            }
        }

        public string sname_ambiguous
        {
            set { this.Add(Criteria.GetLikeClauseCriterion("s_master_t.sname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH)); }
        }

        public string scode_prefix
        {
            set { this.Add(Criteria.GetLikeClauseCriterion("price_t.scode", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH)); }
        }

        public virtual string icode
        {
            set { this.Add(Criteria.GetCriterion("price_t.icode", value, Operation.EQUAL)); }
        }

        public virtual DateTime? date_from
        {
            set { this.Add(Criteria.GetCriterion("price_t.date_from", value, Operation.EQUAL)); }
        }

        public virtual DateTime? date_to
        {
            set { this.Add(Criteria.GetCriterion("price_t.date_to", value, Operation.EQUAL)); }
        }

        public virtual DateTime? date_from_not_eq
        {
            set
            {
                this.Add(Criteria.GetCriterion("price_t.date_from", value, Operation.NOT_EQUAL));
            }
        }

        public virtual DateTime? date_to_not_eq
        {
            set
            {
                this.Add(Criteria.GetCriterion("price_t.date_to", value, Operation.NOT_EQUAL));
            }
        }

        public int? status
        {
            set
            {
                this.Add(Criteria.GetCriterion("price_t.status", value, Operation.EQUAL));
            }
        }

        public EnumPriceKbn? price_kbn
        {
            set
            {
                this.Add(Criteria.GetCriterion("price_t.price_kbn", value, Operation.EQUAL));
            }
        }

        public IEnumerable<EnumPriceKbn> price_kbn_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("price_t.price_kbn", value));
            }
        }

        /// <summary>
        /// Sets criterion to criteria for getting active record only.
        /// </summary>
        public virtual void SetActiveOnly()
        {
            var timeCriterion = Criteria.GetBetweenCriterion(ColumnName("price_t.date_from"), ColumnName("price_t.date_to"), ColumnName("current_timestamp"));
            var nullCriterion = Criteria.GetCriterion("price_t.date_from", null, Operation.EQUAL);
            this.Add(Criteria.JoinWithOR(new[] { timeCriterion, nullCriterion }));
        }

        /// <summary>
        /// Sets Criterions for sorting by minimum 'date_from'.
        /// </summary>
        /// <param name="orderBy">ORDER_BY_ASC or ORDER_BY_DESC</param>
        public virtual void SetOrderByDateFrom(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("price_t.date_from", orderBy);
        }

        /// <summary>
        /// Sets Criterions for sorting by 'scode'.
        /// </summary>
        /// <param name="orderBy">ORDER_BY_ASC or ORDER_BY_DESC</param>
        public virtual void SetOrderByScode(OrderBy orderBy)
        {
            this.AddOrderBy("price_t.scode", orderBy);
        }

        /// <summary>
        /// Sets the BETWEEN Criterion.
        /// </summary>
        /// <param name="date_from">The <i>(criteria) start date timestamp with time zone for date_from</i> in price_t of BETWEEN Criterion.</param>
        /// <param name="date_to">The <i>(criteria) end date timestamp with time zone for date_to</i> in price_t of BETWEEN Criterion.</param>
        public void SetValidateDateFromTo(DateTime date_from, DateTime date_to)
        {
            var criteriaThisDate_from = Criteria.GetBetweenCriterion(ColumnName("price_t.date_from"), ColumnName("price_t.date_to"), date_from);
            var criteriaThisDate_To = Criteria.GetBetweenCriterion(ColumnName("price_t.date_from"), ColumnName("price_t.date_to"), date_to);
            var criteriaOtherDate_from = Criteria.GetBetweenCriterion(date_from, date_to, ColumnName("price_t.date_from"));
            var criteriaOtherDate_To = Criteria.GetBetweenCriterion(date_from, date_to, ColumnName("price_t.date_to"));

            this.Add(Criteria.JoinWithOR(new[] { criteriaThisDate_from, criteriaThisDate_To, criteriaOtherDate_from, criteriaOtherDate_To }));
        }

        public virtual string ccode
        {
            set { this.Add(Criteria.GetCriterion("price_t.ccode", value.ToUpper(), Operation.EQUAL)); }
        }

        public int? price
        {
            set { this.Add(Criteria.GetCriterion("price_t.price", value, Operation.EQUAL)); }
        }

        public int? member_rank
        {
            set { this.Add(Criteria.GetCriterion("price_t.member_rank", value, Operation.EQUAL)); }
        }

        /// <summary>
        /// set order by g_master_t.sort
        /// </summary>
        public virtual void SetOrderBySort(OrderBy orderBy)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            criteria.SetOrderBySort(orderBy);
            this.AddOrderBy(criteria);
        }

        /// <summary>
        /// set order by g_master_t.gcode
        /// </summary>
        public virtual void SetOrderByGcode(OrderBy orderBy)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            criteria.SetOrderByGcode(orderBy);
            this.AddOrderBy(criteria);
        }

        public string gcode
        {
            set { this.Add(Criteria.GetCriterion("price_t.gcode", value, Operation.EQUAL)); }
        }

        public string gcode_prefix
        {
            set { this.Add(Criteria.GetLikeClauseCriterion("g_master_t.gcode", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH)); }
        }

        public IEnumerable<string> gcode_in
        {
            set { this.Add(Criteria.GetInClauseCriterion("g_master_t.gcode", value)); }
        }

        public IEnumerable<string> scode_in
        {
            set { this.Add(Criteria.GetInClauseCriterion("s_master_t.scode", value)); }
        }


        public string gname_ambiguous
        {
            set { this.Add(Criteria.GetLikeClauseCriterion("g_master_t.gname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH)); }
        }

        public void SetOrderByPrice_for_search(OrderBy orderBy)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            criteria.SetOrderByPrice_for_search(orderBy);
            this.AddOrderBy(criteria);
        }

        public void SetOrderByDisp_order(OrderBy orderBy)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.SetOrderByDisp_order(orderBy);
            this.AddOrderBy(criteria);
        }

        public void SetOrderByPriceKbn(OrderBy orderBy)
        {
            this.AddOrderBy("price_t.price_kbn", orderBy);
        }

        public void SetOrderByMemberRank(OrderBy orderBy)
        {
            this.AddOrderBy("price_t.member_rank", orderBy);
        }

        /// <summary>
        /// set order by array
        /// </summary>
        public void SetOrderByArrayIndex(string field_name,string[] string_array, OrderBy orderBy)
        {
            this.AddOrderByArrayIndex(field_name, string_array, orderBy);
        }

    }
}
