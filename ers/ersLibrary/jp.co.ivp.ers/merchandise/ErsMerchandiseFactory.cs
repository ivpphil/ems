﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.merchandise.category;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise.specification;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise.strategy;
using jp.co.ivp.ers.merchandise.stock;

namespace jp.co.ivp.ers.merchandise
{
    /// <summary>
    /// 商品関連クラス用Factory
    /// </summary>
    public class ErsMerchandiseFactory
    {
        /// <summary>
        /// Get empty instance of ErsMerchandise.
        /// </summary>
        /// <returns></returns>
        public virtual ErsMerchandise GetErsMerchandise()
        {
            return new ErsMerchandise();
        }

        /// <summary>
        /// カテゴリクラス用Repository
        /// </summary>
        /// <returns></returns>
        public virtual ErsCategoryRepository GetErsCategoryRepository(int categoryNumber)
        {
            return new ErsCategoryRepository(categoryNumber);
        }

        /// <summary>
        /// ErsCategoryRepository用Criteria
        /// </summary>
        /// <returns></returns>
        public virtual ErsCategoryCriteria GetErsCategoryCriteria(int categoryNumber)
        {
            return new ErsCategoryCriteria(categoryNumber);
        }

        /// <summary>
        /// カテゴリクラスを取得する
        /// </summary>
        /// <returns></returns>
        public virtual ErsCategory GetErsCategory()
        {
            return new ErsCategory();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual ErsCategory GetErsCategoryWithModel(ErsModelBase model)
        {
            var category = this.GetErsCategory();
            category.OverwriteWithModel(model);
            return category;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryNumber"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public virtual ErsCategory GetErsCategoryWithParameters(int categoryNumber, Dictionary<string, object> dic)
        {
            var category = this.GetErsCategory();

            category.OverwriteWithParameter(dic);

            category.categoryNumber = categoryNumber;

            return category;
        }

        /// <summary>
        /// Get the class of StockStatusSpecification
        /// </summary>
        /// <returns>new instance of StockStatusSpecification</returns>
        public virtual StockStatusSpecification GetStockStatusSpecification()
        {
            return new StockStatusSpecification();
        }
        /// <summary>
        /// Get the class of OnPointCampaignSpecification
        /// </summary>
        /// <returns>new instance of OnPointCampaignSpecification</returns>
        public virtual OnPointCampaignSpecification GetOnPointCampaignSpecification()
        {
            return new OnPointCampaignSpecification();
        }

        /// <summary>
        /// Get the class of OnCampaignSpecification
        /// </summary>
        /// <returns>new instance of OnCampaignSpecification</returns>
        public virtual OnCampaignSpecification GetOnCampaignSpecification()
        {
            return new OnCampaignSpecification();
        }

        /// <summary>
        /// Get the class of OnSaleSpecification
        /// </summary>
        /// <returns>new instance of OnSaleSpecification</returns>
        public virtual OnSaleSpecification GetOnSaleSpecification()
        {
            return new OnSaleSpecification();
        }

        /// <summary>
        /// Get the class of OutOfStockAtBasketSpecification
        /// </summary>
        /// <returns>new instance of OutOfStockAtBasketSpecification</returns>
        public virtual OutOfStockAtBasketSpecification GetOutOfStockAtBasketSpecification()
        {
            return new OutOfStockAtBasketSpecification();
        }

        /// <summary>
        /// Get the class of OutOfStockSpecification
        /// </summary>
        /// <returns>new instance of OutOfStockSpecification</returns>
        public virtual OutOfStockSpecification GetOutOfStockSpecification()
        {
            return new OutOfStockSpecification();
        }

        /// <summary>
        /// Get the class of ObtainMerchandisePriceStgy
        /// </summary>
        /// <returns>new instance of ObtainMerchandisePriceStgy</returns>
        public virtual strategy.ObtainMerchandisePriceStgy GetObtainMerchandisePriceStgy()
        {
            return new strategy.ObtainMerchandisePriceStgy();
        }

        /// <summary>
        /// Get the class of ErsStockFactory
        /// </summary>
        /// <returns>new instance of ErsStockFactory</returns>
        public virtual stock.ErsStockFactory GetErsStockFactory()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            switch (setup.StockManagementType)
            {
                case EnumStockManagementType.INSERT:
                    return new stock.insert_ver.ErsStockInsertFactory(); //insert version
                default:
                    return new stock.ErsStockFactory();//update version
            }
        }
        /// <summary>
        /// Get the class of CheckDuplicateMerchandiseGroupStgy
        /// </summary>
        /// <returns>new instance of CheckDuplicateMerchandiseGroupStgy</returns>
        public virtual CheckDuplicateMerchandiseGroupStgy GetCheckDuplicateMerchandiseGroupStgy()
        {
            return new CheckDuplicateMerchandiseGroupStgy();
        }

        /// <summary>
        /// Get the class of CheckStock_flgStgy
        /// </summary>
        /// <returns>new instance of CheckStock_flgStgy</returns>
        public virtual CheckStock_flgStgy GetCheckStock_flgStgy()
        {
            return new CheckStock_flgStgy();
        }
        /// <summary>
        /// Get the class of CheckDuplicateMerchandiseStgy
        /// </summary>
        /// <returns>new instance of CheckDuplicateMerchandiseStgy</returns>
        public virtual CheckDuplicateMerchandiseStgy GetCheckDuplicateMerchandiseStgy()
        {
            return new CheckDuplicateMerchandiseStgy();
        }

        /// <summary>
        /// get category value and parent category id chain;
        /// </summary>
        /// <returns>new instance of ObtainParentCateStgy</returns>
        public virtual ObtainParentCateStgy GetObtainParentCateStgy()
        {
            return new ObtainParentCateStgy();
        }
        /// <summary>
        /// Get the class of CheckMerchandiseExistStgy
        /// </summary>
        /// <returns>new instance of CheckMerchandiseExistStgy</returns>
        public virtual CheckMerchandiseExistStgy GetCheckMerchandiseExistStgy()
        {
            return new CheckMerchandiseExistStgy();
        }

        /// <summary>
        /// Get the class of ObtainRecommendsStgy
        /// </summary>
        /// <returns>new instance of ObtainRecommendsStgy</returns>
        public virtual ObtainRecommendsStgy GetObtainRecommendsStgy()
        {
            return new ObtainRecommendsStgy();
        }

        public virtual ErsSetMerchandiseRepository GetErsSetMerchandiseRepository()
        {
            return new ErsSetMerchandiseRepository();
        }

        public virtual ErsSetMerchandiseCriteria GetErsSetMerchandiseCriteria()
        {
            return new ErsSetMerchandiseCriteria();
        }

        public virtual ErsSetMerchandise GetErsSetMerchandise()
        {
            return new ErsSetMerchandise();
        }

        public virtual ErsSetMerchandise GetErsSetMerchandiseWithModel(ErsModelBase item)
        {
            var esm = GetErsSetMerchandise();
            esm.OverwriteWithModel(item);
            return esm;
        }

        public virtual ErsSetMerchandise GetErsSetMerchandiseWithID(int? id)
        {
            var repository = this.GetErsSetMerchandiseRepository();
            var criteria = this.GetErsSetMerchandiseCriteria();
            criteria.id = id;
            return repository.FindSingle(criteria);

        }

        /// <summary>
        /// 商品グループコードを元にセット商品クラスを取得する
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public virtual ErsSetMerchandise GetErsSetMerchandiseWithParameters(Dictionary<string, object> parameter)
        {
            var setItem = GetErsSetMerchandise();
            setItem.OverwriteWithParameter(parameter);
            return setItem;
        }

        /// <summary>
        /// カートのscodeに紐づくset_master_t情報を取得
        /// </summary>
        /// <param name="scode"></param>
        /// <param name="amount"></param>
        /// <param name="soldout_flg"></param>
        public IList<ErsSetMerchandise> GetSetMerchandiseList(string scode)
        {

            var repo = this.GetErsSetMerchandiseRepository();
            var criteria = this.GetErsSetMerchandiseCriteria();
            criteria.parent_scode = scode;
            return repo.Find(criteria);

        }

        public virtual SetMerchandiseSearchSpec GetSerMerchandiseSearchSpec()
        {
            return new SetMerchandiseSearchSpec();
        }

        public virtual ErsGroupRepository GetErsGroupRepository()
        {
            return new ErsGroupRepository();
        }

        public virtual ErsGroupCriteria GetErsGroupCriteria()
        {
            return new ErsGroupCriteria();
        }

        public virtual ErsGroup GetErsGroup()
        {
            return new ErsGroup();
        }

        public virtual ErsGroup GetErsGroupWithGcode(string gcode)
        {
            var groupRepository = this.GetErsGroupRepository();
            var groupCriteria = this.GetErsGroupCriteria();
            groupCriteria.gcode = gcode;
            var listGroup = groupRepository.Find(groupCriteria);
            if (listGroup.Count != 1)
            {
                return null;
            }

            return listGroup.First();
        }

        public virtual ErsGroup GetErsGroupWithParameter(Dictionary<string, object> dicGroup)
        {
            var objGroup = this.GetErsGroup();
            objGroup.OverwriteWithParameter(dicGroup);
            return objGroup;
        }

        public virtual ErsSkuRepository GetErsSkuRepository()
        {
            return new ErsSkuRepository();
        }

        public virtual ErsSkuCriteria GetErsSkuCriteria()
        {
            return new ErsSkuCriteria();
        }

        public virtual ErsSku GetErsSku()
        {
            return new ErsSku();
        }

        public virtual ErsSku GetErsSkuWithScode(string scode)
        {
            var repository = this.GetErsSkuRepository();
            var criteria = this.GetErsSkuCriteria();
            criteria.scode = scode;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            var listSku = repository.Find(criteria);
            if (listSku.Count != 1)
            {
                return null;
            }
            return listSku.First();
        }

        public virtual ErsSku GetErsSkuWithParameter(Dictionary<string, object> dicSku)
        {
            var objSku = this.GetErsSku();
            objSku.OverwriteWithParameter(dicSku);
            return objSku;
        }

        public virtual UpdatePriceForSearchSpec GetUpdatePriceForSearchSpec()
        {
            return new UpdatePriceForSearchSpec();
        }

        /// <summary>
        /// Instantiates the strategy class that update g_master_t.price_for_search.
        /// </summary>
        /// <returns>Returns new strategy class that update g_master_t.price_for_search.</returns>
        public virtual UpdatePriceForSearchStgy GetUpdatePriceForSearchStgy()
        {
            return new UpdatePriceForSearchStgy();
        }

        /// <summary>
        /// Repositoryを取得する
        /// </summary>
        /// <returns></returns>
        public virtual ErsPriceRepository GetErsPriceRepository()
        {
            return new ErsPriceRepository();
        }

        /// <summary>
        /// ErsPriceRespository用Criteria
        /// </summary>
        /// <returns>New ErsPriceCriteria</returns>
        public virtual ErsPriceCriteria GetErsPriceCriteria()
        {
            return new ErsPriceCriteria();
        }

        /// <summary>
        /// 価格クラスを取得する
        /// </summary>
        /// <returns>New ErsPrice</returns>
        public virtual ErsPrice GetErsPrice()
        {
            return new ErsPrice();
        }

        /// <summary>
        /// Get specification object for search prices.
        /// </summary>
        /// <returns>New PriceSearchSpecification</returns>
        public virtual PriceSearchSpecification GetPriceSerachSpecification()
        {
            return new PriceSearchSpecification();
        }

        /// <summary>
        /// 入力値をもとに、価格クラスを取得する。
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>Return ErsPrice with parameter values</returns>
        public virtual ErsPrice getErsPriceWithParameter(Dictionary<string, object> parameters)
        {
            var price = this.GetErsPrice();
            price.OverwriteWithParameter(parameters);
            return price;
        }


        /// <summary>
        /// IDをもとに、価格クラスを取得する。
        /// </summary>
        /// <param name="id">Id of Price to be searched</param>
        /// <returns>searched price</returns>
        public virtual ErsPrice getErsPriceWithId(int? id)
        {
            var repository = GetErsPriceRepository();

            var c = this.GetErsPriceCriteria();
            c.id = id;
            var list = repository.Find(c);
            if (list.Count != 1)
                return null;

            return list[0];
        }

        /// <summary>
        /// 商品番号から価格リスト一覧用のリスト取得
        /// </summary>
        /// <param name="scode">scode of price to be searched</param>
        /// <returns>searched price</returns>
        public virtual ErsMerchandise GetErsDefaultPriceWithScode(string scode)
        {
            var repository = GetErsPriceRepository();
            var criteria = GetErsPriceCriteria();

            criteria.scode = scode;
            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

            var list = repository.FindPriceSkuBaseItemList(criteria);

            if (list.Count != 1)
                return null;

            return list[0];
        }

        /// <summary>
        /// Overwrite publicly writable properties of this Entity using Dictionary values.
        /// </summary>
        /// <param name="parameter">Dictionary values to ovewrite publicly writeable values.</param>
        /// <returns name="papa">returns the class price.</returns>
        public virtual ErsPrice GetErsPriceWithParameter(Dictionary<string, object> parameter)
        {
            var price = this.GetErsPrice();
            price.OverwriteWithParameter(parameter);
            return price;
        }

        /// <summary>
        /// Instantiates the strategy class that checks duplicates in price_t.
        /// </summary>
        /// <returns>Returns new strategy class that checks duplicates in price_t.</returns>
        public virtual CheckDuplicatePriceStgy GetCheckDuplicatePriceStgy()
        {
            return new CheckDuplicatePriceStgy();
        }

        public virtual SearchGroupBaseItemListSpec GetSearchGroupBaseItemListSpec()
        {
            return new SearchGroupBaseItemListSpec();
        }

        public virtual SearchSkuBaseItemListSpec GetSearchSkuBaseItemListSpec()
        {
            return new SearchSkuBaseItemListSpec();
        }

        public virtual ErsMerchandise GetActiveErsMerchandiseWithScode(string scode, int? member_rank, int? site_id= null)
        {
            var repository = this.GetErsGroupRepository();
            var criteria = this.GetErsSkuCriteria();
            criteria.scode = scode;
            criteria.SetActiveOnly(DateTime.Now);
            if (site_id.HasValue)
            {
                criteria.site_id = site_id;
            }
            var listMerchandise = repository.FindSkuBaseItemList(criteria, member_rank);
            if (listMerchandise.Count == 0)
            {
                return null;
            }

            return listMerchandise.First();
        }

        public virtual ErsMerchandise GetErsMerchandiseWithScode(string scode, int? member_rank)
        {
            var repository = this.GetErsGroupRepository();
            var criteria = this.GetErsSkuCriteria();
            criteria.scode = scode;

            var listMerchandise = repository.FindSkuBaseItemList(criteria, member_rank);
            if (listMerchandise.Count == 0)
            {
                return null;
            }

            return listMerchandise.First();
        }

        public virtual CheckPriceForSalePtnRegisteredStgy GetCheckPriceForSalePtnRegisteredStgy()
        {
            return new CheckPriceForSalePtnRegisteredStgy();
        }

        public virtual ErsStockLog GetErsStockLog()
        {
            return new ErsStockLog();
        }

        public virtual ErsStockLogRepository GetErsStockLogRepository()
        {
            return new ErsStockLogRepository();
        }

        public virtual SearchCategoryListSpec GetSearchCategoryListSpec()
        {
            return new SearchCategoryListSpec();
        }

        public virtual SearchPriceSkuBaseItemListSpec GetSearchPriceSkuBaseItemListSpec()
        {
            return new SearchPriceSkuBaseItemListSpec();
        }

        public virtual SearchPriceMemberRankItemListSpec GetSearchPriceMemberRankItemListSpec()
        {
            return new SearchPriceMemberRankItemListSpec();
        }

        public virtual SearchPriceGroupBaseItemListSpec GetSearchPriceGroupBaseItemListSpec()
        {
            return new SearchPriceGroupBaseItemListSpec();
        }

        public virtual RegistPriceStgy GetRegistPriceStgy()
        {
            return new RegistPriceStgy();
        }

        public virtual RegistPriceOfSaleStgy GetRegistPriceOfSaleStgy()
        {
            return new RegistPriceOfSaleStgy();
        }

        public virtual RegistPriceOfMemberRankStgy GetRegistPriceOfMemberRankStgy()
        {
            return new RegistPriceOfMemberRankStgy();
        }

        public virtual ErsKeywordsRepository GetErsKeywordsRepository()
        {
            return new ErsKeywordsRepository();
        }

        public virtual ErsKeywordsCriteria GetErsKeywordsCriteria()
        {
            return new ErsKeywordsCriteria();
        }

        public virtual ErsKeywords GetErsKeywords()
        {
            return new ErsKeywords();
        }

        public virtual ErsKeywordsRelationRepository GetErsKeywordsRelationRepository()
        {
            return new ErsKeywordsRelationRepository();
        }

        public virtual ErsKeywordsRelationCriteria GetErsKeywordsRelationCriteria()
        {
            return new ErsKeywordsRelationCriteria();
        }

        public virtual ErsKeywordsRelation GetErsKeywordsRelation()
        {
            return new ErsKeywordsRelation();
        }

        public virtual SearchKeywordListSpec GetSearchKeywordListSpec()
        {
            return new SearchKeywordListSpec();
        }

        public virtual RegistKeywordStgy GetRegistKeywordStgy()
        {
            return new RegistKeywordStgy();
        }

        public virtual UpdateRelationalProductCodeStgy GetUpdateRelationalProductCodeStgy()
        {
            return new UpdateRelationalProductCodeStgy();
        }

        public virtual SearchDailyRankingListSpec GetSearchDailyRankingListSpec()
        {
            return new SearchDailyRankingListSpec();
        }

        public virtual ProductKeywordConstracterStgy GetProductKeywordConstracterStgy()
        {
            return new ProductKeywordConstracterStgy();
        }

        public virtual ValidateDispSendPtnStgy GetValidateDispSendPtnStgy()
        {
            return new ValidateDispSendPtnStgy();
        }

        public ChecksActivenessOfMerchandiseSpec GetChecksActivenessOfMerchandiseSpec()
        {
            return new ChecksActivenessOfMerchandiseSpec();
        }
    }
}
