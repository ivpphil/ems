﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsSkuRepository
        : ErsRepository<ErsSku>
    {
        public ErsSkuRepository()
            : base("s_master_t")
        {
        }

        public override IList<ErsSku> Find(db.Criteria criteria)
        {
            var ret = new List<ErsSku>();

            var listRecord = ersDB_table.gSelect(criteria);

            foreach (var dicGroup in listRecord)
            {
                var objGroup = ErsFactory.ersMerchandiseFactory.GetErsSkuWithParameter(dicGroup);
                ret.Add(objGroup);
            }
            return ret;
        }
    }
}
