﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise
{
    /// <summary>
    /// セット商品マスタクラス
    /// </summary>
    public class ErsSetMerchandise
        : ErsRepositoryEntity
    {
        protected internal ErsSetMerchandise() { }

        public override int? id { get; set; }

        /// <summary>
        /// 親商品コード
        /// </summary>
        public string parent_scode { get; set; }

        /// <summary>
        /// 子商品コード
        /// </summary>
        public string scode { get; set; }

        /// <summary>
        /// 子商品名
        /// </summary>
        public string sname { get; set; }

        /// <summary>
        /// 個数
        /// </summary>
        public int? amount { get; set; }

        /// <summary>
        /// 通常用セット内訳価格
        /// </summary>
        public int? price { get; set; }

        /// <summary>
        /// 定期用セット内訳価格
        /// </summary>
        public int? regular_price { get; set; }

        /// <summary>
        /// </summary>
        public DateTime? intime { get; set; }

        /// <summary>
        /// </summary>
        public DateTime? utime { get; set; }

        /// <summary>
        /// The flug of carriage_cost_type merchandise
        /// </summary>
        public EnumCarriageCostType? carriage_cost_type { get; set; }

        /// <summary>
        /// The flug of plural_order_type merchandise
        /// </summary>
        public EnumPluralOrderType? plural_order_type { get; set; }

        /// <summary>
        /// 定期用セット内訳初回価格
        /// </summary>
        public int? regular_first_price { get; set; }

        /// <summary>
        /// 仕入れ価格
        /// </summary>
        public int? cost_price { get; set; }

        /// <summary>
        /// 仕入れ先コード
        /// </summary>
        public string supplier_code { get; set; }

        /// <summary>
        /// 発注区分
        /// </summary>
        public EnumWhOrderType? wh_order_type { get; set; }
    }
}
