﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsSetMerchandiseCriteria
        : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("set_master_t.id", value, Operation.EQUAL));
            }
        }

        protected internal ErsSetMerchandiseCriteria()
        {
        }

        public virtual string parent_scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("set_master_t.parent_scode", value, Operation.EQUAL));
            }
        }

        public virtual string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("set_master_t.scode", value, Operation.EQUAL));
            }
        }

        public virtual int amount
        {
            set
            {
                this.Add(Criteria.GetCriterion("set_master_t.amount", value, Operation.EQUAL));
            }
        }

        public virtual string[] scode_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("set_master_t.scode", value));
            }
        }

        public void SetOrderByScode(OrderBy orderBy)
        {
            this.AddOrderBy("set_master_t.scode", orderBy);
        }

        public EnumSetFlg? parent_set_flg 
        {
            set
            {
                this.Add(Criteria.GetCriterion("parent_g_master_t.set_flg", value, Operation.EQUAL));
            }
        }

    }
}
