﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.strategy
{
    public class ProductKeywordConstracterStgy
    {
        /// <summary>
        /// グループ用のキーワードをセット
        /// </summary>
        /// <param name="objGroup"></param>
        /// <returns></returns>
        public string CreateGroupKeyword(ErsGroup objGroup)
        {
            var baseKeyword = string.Join(",", objGroup.gcode, objGroup.gname);

            if (objGroup.disp_keyword != null)
            {
                var gkeyword = string.Join(",", objGroup.disp_keyword);
                baseKeyword = string.Join(",", baseKeyword, gkeyword);
            }

            return baseKeyword;
        }

        /// <summary>
        /// SKU用のキーワードをセット
        /// </summary>
        /// <param name="objSku"></param>
        /// <returns></returns>
        public string CreateSkuKeyword(ErsSku objSku)
        {
            return string.Join(",", objSku.scode, objSku.sname);
        }
    }
}
