﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.strategy
{  
    public class CheckDuplicatePriceStgy
    {
        /// <summary>
        /// Checks if duplicate record exists in price_t.
        /// </summary>
        /// <param name="id">The <i>(criteria) serial to check</i> in price_t.</param>
        /// <param name="scode">The <i>(criteria) character varying to check</i> in price_t.</param>
        /// <param name="date_from">The <i>(criteria) timestamp with time zone for date_from</i> in price_t.</param>
        /// <param name="date_to">The <i>(criteria) timestamp with time zone for date_to</i> in price_t.</param>
        /// <param name="lineName"> The <i>string</i> to be concatenaded with Validation error message.</param>
        /// <returns>Returns lineName + Validation error message if duplicate record is found.</returns>
        public virtual IEnumerable<ValidationResult> CheckDuplicate(string scode, DateTime? date_from, DateTime? date_to)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();
            var crtPrice = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
            crtPrice.scode = scode;
            crtPrice.price_kbn = EnumPriceKbn.SALE;
            crtPrice.date_from_not_eq = date_from;
            crtPrice.SetValidateDateFromTo(date_from.Value, date_to.Value);

            if (repository.GetRecordCount(crtPrice) != 0)
            {
                yield return new ValidationResult(
                   ErsResources.GetMessage("30200", scode),
                   new[] { "scode", "date_from", "date_to" });
            }
        }
    }
}
