﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.merchandise.strategy
{
    public class RegistPriceOfMemberRankStgy
    {
        ErsPriceRepository repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();

        public void Regist(string scode, string gcode, int? member_rank, int? normal_price, int? regular_price, int? regular_first_price)
        {
            foreach (EnumPriceKbn price_kbn in Enum.GetValues(typeof(EnumPriceKbn)))
            {
                int? price;
                switch (price_kbn)
                {
                    case EnumPriceKbn.RANK_NORMAL:
                        price = normal_price;
                        break;
                    case EnumPriceKbn.RANK_REGULAR:
                        price = regular_price;
                        break;
                    case EnumPriceKbn.RANK_REGULAR_FIRST:
                        price = regular_first_price;
                        break;
                    default:
                        continue;
                }
                var objPrice = this.GetErsPrice(scode, member_rank, price_kbn);
                if (objPrice == null)
                {
                    this.RegistNewPrice(scode, gcode, price, member_rank, price_kbn);
                }
                else
                {
                    this.UpdatePrice(objPrice, price);
                }
            }
        }

        private void UpdatePrice(ErsPrice objPrice, int? price)
        {
            if (price.HasValue)
            {
                objPrice.price = price;
                var oldPrice = ErsFactory.ersMerchandiseFactory.getErsPriceWithId(objPrice.id);
                repository.Update(oldPrice, objPrice);
            }
            else
            {
                repository.Delete(objPrice);
            }
        }

        private void RegistNewPrice(string scode, string gcode, int? price, int? member_rank, EnumPriceKbn? price_kbn)
        {
            if (!price.HasValue)
            {
                return;
            }

            var objPrice = ErsFactory.ersMerchandiseFactory.GetErsPrice();
            objPrice.scode = scode;
            objPrice.gcode = gcode;
            objPrice.price_kbn = price_kbn;
            objPrice.member_rank = member_rank;
            objPrice.price = price;
            repository.Insert(objPrice);
        }

        private ErsPrice GetErsPrice(string scode, int? member_rank, EnumPriceKbn? price_kbn)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
            criteria.scode = scode;
            criteria.member_rank = member_rank;
            criteria.price_kbn = price_kbn;
            var listPrice = repository.Find(criteria);
            if (listPrice.Count == 0)
            {
                return null;
            }

            return listPrice.First();
        }
    }
}
