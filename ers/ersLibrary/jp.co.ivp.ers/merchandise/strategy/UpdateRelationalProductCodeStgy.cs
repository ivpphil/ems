﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.strategy
{
    public class UpdateRelationalProductCodeStgy
    {
        /// <summary>
        /// 関連テーブルのSCODEを更新する。
        /// </summary>
        /// <param name="item"></param>
        /// <param name="command"></param>
        public void UpdateOtherScode(string old_scode, string scode)
        {
            if (old_scode == scode)
            {
                return;
            }

            //Update bask_t.scode
            this.UpdateSetSkuScode(old_scode, scode);

            //Update stock_t.scode
            this.UpdateStockScode(old_scode, scode);

            //Update price_t.scode
            this.UpdatePriceScode(old_scode, scode);
        }

        /// <summary>
        /// セット商品の商品コードをアップデート
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        /// <param name="scode"></param>
        private void UpdateSetSkuScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseCriteria();
            criteria.scode = old_scode;

            var repository = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseRepository();
            var new_setsku = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandise();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_setsku.OverwriteWithModel(record);
                new_setsku.scode = scode;
                repository.Update(record, new_setsku);
            }
        }

        private void UpdateStockScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockCriteria();
            criteria.scode = old_scode;

            var repository = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockRepository();
            var new_stock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStock();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_stock.OverwriteWithModel(record);
                new_stock.scode = scode;
                repository.Update(record, new_stock);
            }
        }

        #region Update Other Gcode
        public void UpdateOtherGcode(string old_gcode, string gcode)
        {
            //Update bask_t.gcode
            this.UpdateBaskGcode(old_gcode, gcode);

            //Update s_master_t.gcode
            this.UpdateSkuGcode(old_gcode, gcode);

            //Update g_master_t.recommend1 to g_master_t.recommend5
            this.UpdateGroupRecommend(old_gcode, gcode);

            //Update regular_detail_t.gcode
            this.UpdateRegularDetailGcode(old_gcode, gcode);

            //Update price_t.gcode
            this.UpdatePriceGcode(old_gcode, gcode);

            //Update target_item_t.gcode
            this.UpdateStepScenarioItemGcode(old_gcode, gcode);
        }

        /// <summary>
        /// バスケット内の商品コードを更新
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        /// <param name="gcode"></param>
        private void UpdateBaskGcode(string old_gcode, string gcode)
        {
            var criteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
            criteria.gcode = old_gcode;

            var repository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            var new_bask = ErsFactory.ersBasketFactory.GetErsBaskRecord();

            var list = repository.Find(criteria);
            foreach (var record in list)
            {
                new_bask.OverwriteWithModel(record);
                new_bask.gcode = gcode;
                repository.Update(record, new_bask);
            }

        }

        private void UpdatePriceGcode(string old_gcode, string gcode)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
            criteria.gcode = old_gcode;

            var repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();
            var new_price = ErsFactory.ersMerchandiseFactory.GetErsPrice();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_price.OverwriteWithModel(record);
                new_price.gcode = gcode;
                repository.Update(record, new_price);
            }
        }

        private void UpdateSkuGcode(string old_gcode, string gcode)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.gcode = old_gcode;

            var repository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var new_sku = ErsFactory.ersMerchandiseFactory.GetErsSku();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_sku.OverwriteWithModel(record);
                new_sku.gcode = gcode;
                repository.Update(record, new_sku);
            }
        }

        private void UpdateGroupRecommend(string old_gcode, string gcode)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            criteria.recommend_JoinWithOr = old_gcode;

            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var new_group = ErsFactory.ersMerchandiseFactory.GetErsGroup();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_group.OverwriteWithModel(record);
                new_group.recommend1 = this.GetNewRecommend(record.recommend1, old_gcode, gcode);
                new_group.recommend2 = this.GetNewRecommend(record.recommend2, old_gcode, gcode);
                new_group.recommend3 = this.GetNewRecommend(record.recommend3, old_gcode, gcode);
                new_group.recommend4 = this.GetNewRecommend(record.recommend4, old_gcode, gcode);
                new_group.recommend5 = this.GetNewRecommend(record.recommend5, old_gcode, gcode);
                repository.Update(record, new_group);
            }
        }

        private string GetNewRecommend(string recommend_gcode, string old_gcode, string new_gcode)
        {
            if (recommend_gcode == old_gcode)
                return new_gcode;

            return recommend_gcode;
        }

        /// <summary>
        /// 定期明細の商品コードをアップデート
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        /// <param name="gcode"></param>
        private void UpdateRegularDetailGcode(string old_gcode, string gcode)
        {
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
            criteria.gcode = old_gcode;

            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            var new_regular_detail = ErsFactory.ersOrderFactory.GetErsRegularOrderRecord(ErsFactory.ersOrderFactory.GetErsRegularOrder());
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_regular_detail.OverwriteWithModel(record);
                new_regular_detail.gcode = gcode;
                repository.Update(record, new_regular_detail);
            }
        }

        /// <summary>
        /// ステップメールのシナリオの商品コードを更新する。
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        /// <param name="gcode"></param>
        private void UpdateStepScenarioItemGcode(string old_gcode, string gcode)
        {
            var criteria = ErsFactory.ersTargetFactory.GetErsTargetItemCriteria();
            criteria.gcode = old_gcode;

            var repository = ErsFactory.ersTargetFactory.GetErsTargetItemRepository();
            var new_step_scenario_item = ErsFactory.ersTargetFactory.GetErsTargetItem();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_step_scenario_item.OverwriteWithModel(record);
                new_step_scenario_item.gcode = gcode;
                repository.Update(record, new_step_scenario_item);
            }
        }
        #endregion

        #region Update Other Scode

        /// <summary>
        /// 関連テーブルのSCODEを更新する。
        /// </summary>
        /// <param name="item"></param>
        /// <param name="command"></param>
        public void UpdateIsolationOtherScode(string old_scode, string scode)
        {
            if (old_scode == scode || string.IsNullOrEmpty(old_scode))
            {
                return;
            }

            //Update bask_t.scode
            this.UpdateBaskScode(old_scode, scode);

            //Update set_master_t.parent_scode 
            this.UpdateSetSkuParentScode(old_scode, scode);

            //Update regular_detail_t.scode
            this.UpdateRegularDetailScode(old_scode, scode);

            //Update wishlist_t.scode
            this.UpdateWishlistScode(old_scode, scode);

            //Update doc_bundling_t.scode
            this.UpdateDocBundlingScode(old_scode, scode);

            //Updat doc_target_t.scode
            this.UpdateDocTargetScode(old_scode, scode);

            //Update target_item_t.scode
            this.UpdateStepScenarioItemScode(old_scode, scode);

            //Update campaign_t.scode
            this.UpdateCampaignScode(old_scode, scode);

            //Update tg_target_t.scode
            this.UpdateTGTargetScode(old_scode, scode);

            //Update wh_stock_t.scode
            this.UpdateWhStockScode(old_scode, scode);
        }

        /// <summary>
        /// バスケット内の商品コードを更新
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        /// <param name="scode"></param>
        private void UpdateBaskScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
            criteria.scode = old_scode;

            var repository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            var new_bask = ErsFactory.ersBasketFactory.GetErsBaskRecord();

            var list = repository.Find(criteria);
            foreach (var record in list)
            {
                new_bask.OverwriteWithModel(record);
                new_bask.scode = scode;
                repository.Update(record, new_bask);
            }
        }

        private void UpdatePriceScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
            criteria.scode = old_scode;

            var repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();
            var new_price = ErsFactory.ersMerchandiseFactory.GetErsPrice();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_price.OverwriteWithModel(record);
                new_price.scode = scode;
                repository.Update(record, new_price);
            }
        }

        /// <summary>
        /// セット商品の親商品コードをアップデート
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        /// <param name="parent_scode"></param>
        private void UpdateSetSkuParentScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseCriteria();
            criteria.parent_scode = old_scode;

            var repository = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandiseRepository();
            var new_setsku = ErsFactory.ersMerchandiseFactory.GetErsSetMerchandise();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_setsku.OverwriteWithModel(record);
                new_setsku.parent_scode = scode;
                repository.Update(record, new_setsku);
            }
        }

        /// <summary>
        /// 定期明細の商品コードをアップデート
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        /// <param name="scode"></param>
        private void UpdateRegularDetailScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
            criteria.scode = old_scode;

            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            var new_regular_detail = ErsFactory.ersOrderFactory.GetErsRegularOrderRecord(ErsFactory.ersOrderFactory.GetErsRegularOrder());
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_regular_detail.OverwriteWithModel(record);
                new_regular_detail.scode = scode;
                repository.Update(record, new_regular_detail);
            }
        }

        /// <summary>
        /// Wishlistの商品コードを更新
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        private void UpdateWishlistScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersMemberFactory.GetErsWishlistCriteria();
            criteria.scode = old_scode;

            var repository = ErsFactory.ersMemberFactory.GetErsWishlistRepository();
            var new_wishlist = ErsFactory.ersMemberFactory.GetErsWishlist();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_wishlist.OverwriteWithModel(record);
                new_wishlist.scode = scode;
                repository.Update(record, new_wishlist);
            }
        }

        /// <summary>
        /// 同梱商品の商品コードを更新する
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        private void UpdateDocBundlingScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersDocBundleFactory.GetErsDocBundlingCriteria();
            criteria.scode = old_scode;

            var repository = ErsFactory.ersDocBundleFactory.GetErsDocBundlingRepository();
            var new_doc_bundling = ErsFactory.ersDocBundleFactory.GetErsDocBundling();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_doc_bundling.OverwriteWithModel(record);
                new_doc_bundling.scode = scode;
                repository.Update(record, new_doc_bundling);
            }
        }

        /// <summary>
        /// 同梱対象商品の商品コードを更新する。
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        private void UpdateDocTargetScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersDocBundleFactory.GetErsDocTargetCriteria();
            criteria.scode = old_scode;

            var repository = ErsFactory.ersDocBundleFactory.GetErsDocTargetRepository();
            var new_doc_target = ErsFactory.ersDocBundleFactory.GetErsDocTarget();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_doc_target.OverwriteWithModel(record);
                new_doc_target.scode = scode;
                repository.Update(record, new_doc_target);
            }
        }

        /// <summary>
        /// ステップメールのシナリオの商品コードを更新する。
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        /// <param name="scode"></param>
        private void UpdateStepScenarioItemScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersTargetFactory.GetErsTargetItemCriteria();
            criteria.scode = old_scode;

            var repository = ErsFactory.ersTargetFactory.GetErsTargetItemRepository();
            var new_step_scenario_item = ErsFactory.ersTargetFactory.GetErsTargetItem();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_step_scenario_item.OverwriteWithModel(record);
                new_step_scenario_item.scode = scode;
                repository.Update(record, new_step_scenario_item);
            }
        }

        /// <summary>
        /// キャンペーン商品の商品コードを更新する。
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        private void UpdateCampaignScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersDocBundleFactory.GetErsCampaignCriteria();
            criteria.scode = old_scode;

            var repository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();
            var new_campaign = ErsFactory.ersDocBundleFactory.GetErsCampaign();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_campaign.OverwriteWithModel(record);
                new_campaign.scode = scode;
                repository.Update(record, new_campaign);
            }
        }

        /// <summary>
        /// 同梱ターゲットのターゲットの商品コードを更新する。
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        private void UpdateTGTargetScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersTargetFactory.GetErsTargetItemCriteria();
            criteria.scode = old_scode;

            var repository = ErsFactory.ersTargetFactory.GetErsTargetItemRepository();
            var new_tg_target = ErsFactory.ersTargetFactory.GetErsTargetItem();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_tg_target.OverwriteWithModel(record);
                new_tg_target.scode = scode;
                repository.Update(record, new_tg_target);
            }
        }

        /// <summary>
        /// 棚在庫マスタの商品コードを更新する。
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="item"></param>
        private void UpdateWhStockScode(string old_scode, string scode)
        {
            var criteria = ErsFactory.ersWarehouseFactory.GetErsWhStockCriteria();
            criteria.scode = old_scode;

            var repository = ErsFactory.ersWarehouseFactory.GetErsWhStockRepository();
            var new_wh_stock = ErsFactory.ersWarehouseFactory.GetErsWhStock();
            var list = repository.Find(criteria);

            foreach (var record in list)
            {
                new_wh_stock.OverwriteWithModel(record);
                new_wh_stock.scode = scode;
                repository.Update(record, new_wh_stock);
            }
        }

        #endregion
    }
}
