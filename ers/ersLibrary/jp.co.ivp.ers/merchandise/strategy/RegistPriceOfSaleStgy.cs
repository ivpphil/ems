﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.merchandise.strategy
{
    public class RegistPriceOfSaleStgy
    {
        ErsPriceRepository repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();

        public virtual void Regist(int? id, string scode, string gcode, int? price, DateTime? date_from, DateTime? date_to)
        {
            if (id != null)
            {
                this.UpdateSale(id, price, date_from, date_to);
            }
            else
            {
                this.InsertSale(gcode, scode, price, date_from, date_to);
            }
        }

        private void UpdateSale(int? id, int? price, DateTime? date_from, DateTime? date_to)
        {
            var new_price = ErsFactory.ersMerchandiseFactory.getErsPriceWithId(id);
            new_price.price = price;
            new_price.date_from = date_from;
            new_price.date_to = date_to;

            //変更前データ取得
            var old_price = ErsFactory.ersMerchandiseFactory.getErsPriceWithId(id);

            repository.Update(old_price, new_price);
        }

        private void InsertSale(string gcode, string scode, int? price, DateTime? date_from, DateTime? date_to)
        {
            var new_price = ErsFactory.ersMerchandiseFactory.GetErsPrice();
            new_price.gcode = gcode;
            new_price.scode = scode;
            new_price.price = price;
            new_price.date_from = date_from;
            new_price.date_to = date_to;
            new_price.price_kbn = EnumPriceKbn.SALE;

            repository.Insert(new_price);
        }
    }
}
