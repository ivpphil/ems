﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.strategy
{
    /// <summary>
    /// The strategy class that Provides methods to get price of the merchandise.
    /// </summary>
    public class ObtainMerchandisePriceStgy
    {
        public int? min_price { get; protected set; }
        public int? min_price2 { get; protected set; }
        public int? min_default_price { get; protected set; }
        public int? min_regular_price { get; protected set; }
        public int? min_regular_first_price { get; protected set; }
        public int? min_non_member_price { get; protected set; }
        public int? min_non_member_regular_price { get; protected set; }

        public int? max_price { get; protected set; }
        public int? max_price2 { get; protected set; }
        public int? max_regular_price { get; protected set; }
        public int? max_default_price { get; protected set; }
        public int? max_regular_first_price { get; protected set; }
        public int? max_non_member_price { get; protected set; }
        public int? max_non_member_regular_price { get; protected set; }

        public bool isCampaign { get; protected set; }

        /// <summary>
        /// Get value that indicates minimum price of this merchandise group.
        /// </summary>
        public virtual void LoadMaximumAndMinimamPriceOfMerchandise(string gcode, int? member_rank)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();

            criteria.gcode = gcode;
            criteria.s_active = EnumActive.Active;

            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
            var listPrice = repository.FindSkuBaseItemList(criteria, member_rank);
            
            var campaignSpec = ErsFactory.ersMerchandiseFactory.GetOnCampaignSpecification();

            this.isCampaign = campaignSpec.IsSatisfiedByGroup(listPrice);
            
            this.GetMinPrice(listPrice);

            this.GetMaxPrice(listPrice);

            var minRegularPrice = this.GetMinimumPriceMerchandise(listPrice, this.isCampaign, EnumSalePatternType.REGULAR);
            if (minRegularPrice != null)
            {
                this.min_regular_price = minRegularPrice.regular_price;
                this.min_regular_first_price = minRegularPrice.regular_first_price;
                this.min_non_member_regular_price = minRegularPrice.non_member_regular_price;
            }

            var maxRegularPrice = this.GetMaximumPriceMerchandise(listPrice, this.isCampaign, EnumSalePatternType.REGULAR);
            if (maxRegularPrice != null)
            {
                this.max_regular_price = maxRegularPrice.regular_price;
                this.max_regular_first_price = maxRegularPrice.regular_first_price;
                this.max_non_member_regular_price = maxRegularPrice.non_member_regular_price;
            }
        }

        /// <summary>
        ///  Loads maximum default price of this merchandise group.(for product search of front site)
        /// </summary>
        private void GetMaxPrice(IList<ErsMerchandise> listPrice)
        {
            var maxPrice = this.GetMaximumPriceMerchandise(listPrice, false, EnumSalePatternType.NORMAL);
            if (maxPrice != null)
            {
                if (this.isCampaign)
                {
                    this.max_default_price = maxPrice.price;
                }
                else
                {
                    this.max_price = maxPrice.price;
                }
                this.max_price2 = maxPrice.price2;
                this.max_non_member_price = maxPrice.non_member_price;
            }

            if (this.isCampaign)
            {
                var maxCampaignPrice = this.GetMaximumPriceMerchandise(listPrice, true, EnumSalePatternType.NORMAL);
                if (maxCampaignPrice != null)
                {
                    this.max_price = maxCampaignPrice.sale_price;
                }
            }
        }

        /// <summary>
        ///  Loads maximum default price of this merchandise group.(for product search of front site)
        /// </summary>
        protected virtual ErsMerchandise GetMaximumPriceMerchandise(IEnumerable<ErsMerchandise> listMerchandise, bool isCampaign, EnumSalePatternType sale_ptn)
        {
            var onCampaignSpec = ErsFactory.ersMerchandiseFactory.GetOnCampaignSpecification();
            int max_price = int.MinValue;
            ErsMerchandise max_price_Merchandise = null;
            foreach (var item in listMerchandise)
            {
                var price = this.GetComparePrice(sale_ptn, item, int.MinValue, isCampaign);

                if ((sale_ptn == EnumSalePatternType.REGULAR || onCampaignSpec.IsSatisfiedBy(item.p_date_from, item.p_date_to) == isCampaign) && max_price < price)
                {
                    max_price = price.Value;
                    max_price_Merchandise = item;
                }
            }
            return max_price_Merchandise;
        }

        /// <summary>
        ///  Loads minimum default price of this merchandise group.(for product search of front site)
        /// </summary>
        private void GetMinPrice(IList<ErsMerchandise> listPrice)
        {
            var minPrice = this.GetMinimumPriceMerchandise(listPrice, false, EnumSalePatternType.NORMAL);
            if (minPrice != null)
            {
                if (this.isCampaign)
                {
                    this.min_default_price = minPrice.price;
                }
                else
                {
                    this.min_price = minPrice.price;
                }
                this.min_price2 = minPrice.price2;
                this.min_non_member_price = minPrice.non_member_price;
            }

            if (this.isCampaign)
            {
                var minCampaignPrice = this.GetMinimumPriceMerchandise(listPrice, true, EnumSalePatternType.NORMAL);
                if (minCampaignPrice != null)
                {
                    this.min_price = minCampaignPrice.sale_price;
                }
            }
        }

        /// <summary>
        ///  Loads minimum default price of this merchandise group.(for product search of front site)
        /// </summary>
        protected virtual ErsMerchandise GetMinimumPriceMerchandise(IEnumerable<ErsMerchandise> listMerchandise, bool isCampaign, EnumSalePatternType sale_ptn)
        {
            var min_price = int.MaxValue;
            ErsMerchandise min_price_Merchandise = null;
            foreach (var item in listMerchandise)
            {
                var price = this.GetComparePrice(sale_ptn, item, int.MaxValue, isCampaign);

                if (price.HasValue && min_price >= price)
                {
                    min_price = price.Value;
                    min_price_Merchandise = item;
                }
            }
            return min_price_Merchandise;
        }


        /// <summary>
        /// Get Price for compare
        /// </summary>
        /// <param name="sale_ptn"></param>
        /// <param name="item"></param>
        /// <param name="defaultValue"></param>
        /// <param name="isCampaign"></param>
        /// <returns></returns>
        private int? GetComparePrice(EnumSalePatternType sale_ptn, ErsMerchandise item, int defaultValue, bool isCampaign)
        {
            var onCampaignSpec = ErsFactory.ersMerchandiseFactory.GetOnCampaignSpecification();
            
            if (sale_ptn == EnumSalePatternType.NORMAL)
            {
                if (isCampaign)
                {
                    if (!onCampaignSpec.IsSatisfiedBy(item.p_date_from, item.p_date_to))
                    {
                        return null;
                    }

                    return item.sale_price ?? defaultValue;
                }

                return item.price ?? defaultValue;
            }
            else
            {
                return item.regular_price ?? defaultValue;
            }
        }
    }
}
