﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.strategy
{
    public class CheckRegularBatchIsPurchaseStgy
    {
        public virtual bool Check(string scode)
        {

            var merchandise = ErsFactory.ersMerchandiseFactory.GetActiveErsMerchandiseWithScode(scode, null);

            if (merchandise == null)
                return false;
            
            //販売期間チェック    
            var datePeriod = ErsFactory.ersMerchandiseFactory.GetOnSaleSpecification().GetDatePeriod(merchandise.date_from, merchandise.date_to, DateTime.Now);

            //在庫チェック
            Boolean stockFlg = ErsFactory.ersMerchandiseFactory.GetOutOfStockSpecification().IsSatisfiedBy(merchandise.set_flg, merchandise.scode, merchandise.stock, merchandise.soldout_flg);

            if (datePeriod == EnumDatePeriod.INSIDE && !stockFlg)
            {
                return true;
            }

            return false;


        }

    }
}
