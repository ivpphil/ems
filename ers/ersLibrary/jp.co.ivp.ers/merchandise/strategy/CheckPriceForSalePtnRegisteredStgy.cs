﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.strategy
{
    public class CheckPriceForSalePtnRegisteredStgy
    {
        public virtual IEnumerable<ValidationResult> Validate(string gcode, EnumSalePatternType? s_sale_ptn)
        {
            if (string.IsNullOrEmpty(gcode) || !s_sale_ptn.HasValue)
            {
                yield break;
            }

            var priceRepository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();
            if (s_sale_ptn == EnumSalePatternType.ALL
                        || s_sale_ptn == EnumSalePatternType.NORMAL)
            {
                var priceCriteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
                priceCriteria.gcode = gcode;
                priceCriteria.price = null;
                priceCriteria.price_kbn = EnumPriceKbn.NORMAL;
                if (priceRepository.GetRecordCount(priceCriteria) > 0)
                {
                    var w_s_sale_ptn_value = ErsFactory.ersViewServiceFactory.GetErsViewSalePtnService().GetStringFromId(s_sale_ptn);
                    var w_s_sale_ptn = ErsResources.GetFieldName("s_sale_ptn");
                    var w_price = ErsResources.GetFieldName("price");
                    yield return new ValidationResult(ErsResources.GetMessage("20004", w_price, w_s_sale_ptn, w_s_sale_ptn_value), new[] { "s_sale_ptn" });
                }
            }
            if (s_sale_ptn == EnumSalePatternType.ALL
                        || s_sale_ptn == EnumSalePatternType.REGULAR)
            {
                var priceCriteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
                priceCriteria.gcode = gcode;
                priceCriteria.price = null;
                priceCriteria.price_kbn = EnumPriceKbn.REGULAR;
                if (priceRepository.GetRecordCount(priceCriteria) > 0)
                {
                    var w_s_sale_ptn_value = ErsFactory.ersViewServiceFactory.GetErsViewSalePtnService().GetStringFromId(s_sale_ptn);
                    var w_s_sale_ptn = ErsResources.GetFieldName("s_sale_ptn");
                    var w_regular_price = ErsResources.GetFieldName("regular_price");
                    yield return new ValidationResult(ErsResources.GetMessage("20004", w_regular_price, w_s_sale_ptn, w_s_sale_ptn_value), new[] { "s_sale_ptn" });
                }
            }
        }
    }
}
