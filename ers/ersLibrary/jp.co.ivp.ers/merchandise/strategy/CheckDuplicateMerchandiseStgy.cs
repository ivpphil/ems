﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.strategy
{
    /// <summary>
    /// Checks for duplicate merchandise 
    /// </summary>
    public class CheckDuplicateMerchandiseStgy
    {
        /// <summary>
        /// check duplicate for gcode,scode and jancode
        /// </summary>
        /// <param name="gcode"></param>
        /// <param name="scode"></param>
        /// <param name="jancode"></param>
        /// <returns>if exists, error occured</returns>
        public virtual IEnumerable<ValidationResult> CheckDuplicate(string gcode, string scode, string jancode)
        {
            if (string.IsNullOrEmpty(gcode) || (string.IsNullOrEmpty(scode) && string.IsNullOrEmpty(jancode)))
                yield break;

            var repository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();

            if (!string.IsNullOrEmpty(scode))
            {
                criteria.gcode_not_equals = gcode;
                criteria.scode = scode;

                if (repository.GetRecordCount(criteria) > 0)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("scode"), scode), new[] { "scode" });
                }
            }
            if (!string.IsNullOrEmpty(jancode))
            {
                criteria.Clear();
                criteria.gcode_not_equals = gcode;
                criteria.jancode = jancode;

                if (repository.GetRecordCount(criteria) > 0)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("jancode"), jancode), new[] { "jancode" });
                }
            }
        }
    }
}
