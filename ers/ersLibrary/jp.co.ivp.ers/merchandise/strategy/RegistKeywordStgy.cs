﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.strategy
{
    public class RegistKeywordStgy
    {
        public void Insert(string gcode, EnumKeywordType keyword_type, string keyword)
        {
            ErsKeywords objKeywords;
            //キーワードが登録済みかチェック
            var keywordsRepository = ErsFactory.ersMerchandiseFactory.GetErsKeywordsRepository();
            var keywordsCriteria = ErsFactory.ersMerchandiseFactory.GetErsKeywordsCriteria();
            keywordsCriteria.disp_keyword = keyword;

            var listKeywords = keywordsRepository.Find(keywordsCriteria);
            if (listKeywords.Count > 0)
            {
                //登録済みの場合は、キーワードのactiveをON
                var oldKeywords = listKeywords.First();
                var newKeywords = ErsFactory.ersMerchandiseFactory.GetErsKeywords();
                newKeywords.OverwriteWithParameter(oldKeywords.GetPropertiesAsDictionary());
                newKeywords.active = EnumActive.Active;
                keywordsRepository.Update(oldKeywords, newKeywords);

                objKeywords = newKeywords;
            }
            else
            {
                //新規にキーワードを登録
                var newKeywords = ErsFactory.ersMerchandiseFactory.GetErsKeywords();
                newKeywords.keyword = ErsKanaConverter.String_conversion(keyword);
                newKeywords.disp_keyword = keyword;

                keywordsRepository.Insert(newKeywords, true);

                objKeywords = newKeywords;
            }

            var keywordsRelationRepository = ErsFactory.ersMerchandiseFactory.GetErsKeywordsRelationRepository();
            var keywordsRelation = ErsFactory.ersMerchandiseFactory.GetErsKeywordsRelation();
            keywordsRelation.keyword_id = objKeywords.id;
            keywordsRelation.gcode = gcode;
            keywordsRelation.keyword_type = keyword_type;
            keywordsRelationRepository.Insert(keywordsRelation);
        }

        /// <summary>
        /// グループのキーワードを更新します。（削除→再登録）
        /// </summary>
        /// <param name="command"></param>
        public void UpdateGroupKeyword(string gcode, string[] keywords)
        {
            this.Delete(gcode, EnumKeywordType.Group);
            this.DeleteIsolated();

            foreach (var keyword in keywords)
            {
                this.Insert(gcode, EnumKeywordType.Group, keyword);
            }
        }

        /// <summary>
        /// Update keyword_updated from g_master_t
        /// </summary>
        /// <param name="gcode"></param>
        /// <param name="executeDate"></param>
        public void UpdateGroupKeywordUpdated(string gcode, DateTime? executeDate)
        {
            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var oldGroup = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(gcode);
            var newGroup = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(gcode);

            newGroup.keyword_updated = executeDate;
            newGroup.utime = executeDate;
            groupRepository.Update(oldGroup, newGroup);
        }

        /// <summary>
        /// Update keyword_updated from s_master_t
        /// </summary>
        /// <param name="gcode"></param>
        /// <param name="executeDate"></param>
        public void UpdateDetailKeywordUpdated(string gcode, DateTime? executeDate)
        {
            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();

            var skuCriteira = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            skuCriteira.gcode = gcode;
            var listSku = skuRepository.Find(skuCriteira);
            foreach (var objSku in listSku)
            {
                var newSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(objSku.scode);
                newSku.keyword_updated = executeDate;
                newSku.utime = executeDate;
                skuRepository.Update(objSku, newSku);
            }
        }

        /// <summary>
        /// 明細のキーワードを更新します。（削除→再登録）
        /// </summary>
        /// <param name="command"></param>
        /// <param name="skuRepository"></param>
        public void UpdateDetailKeyword(string gcode)
        {
            var skuRepository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();

            //キーワードを更新(削除→登録)
            this.Delete(gcode, EnumKeywordType.Sku);

            var skuCriteira = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            skuCriteira.gcode = gcode;
            var listSku = skuRepository.Find(skuCriteira);
            foreach (var objSku in listSku)
            {
                this.Insert(gcode, EnumKeywordType.Sku, objSku.scode);
                this.Insert(gcode, EnumKeywordType.Sku, objSku.sname);
            }
        }

        /// <summary>
        /// キーワードを削除します
        /// </summary>
        /// <param name="gcode"></param>
        /// <param name="keyword_type"></param>
        public void Delete(string gcode, EnumKeywordType keyword_type)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsKeywordsRelationRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsKeywordsRelationCriteria();
            criteria.gcode = gcode;
            criteria.keyword_type = keyword_type;
            repository.Delete(criteria);
        }

        /// <summary>
        /// どの商品グループとも紐付いていないキーワードを削除
        /// </summary>
        public void DeleteIsolated()
        {
            var keywordsRepository = ErsFactory.ersMerchandiseFactory.GetErsKeywordsRepository();
            var keywordsCriteria = ErsFactory.ersMerchandiseFactory.GetErsKeywordsCriteria();
            keywordsCriteria.SetIsolatedKeywordOnly();
            keywordsRepository.Delete(keywordsCriteria);
        }
    }
}
