﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.merchandise.strategy
{
    /// <summary>
    /// Validate Display Send Pattern
    /// </summary>
    public class ValidateDispSendPtnStgy
    {
        /// <summary>
        /// checks for invalid pattern
        /// </summary>
        /// <param name="disp_send_ptn"></param>
        /// <returns></returns>
        public virtual ValidationResult Validate(string ptn)
        {
            if (!ptn.HasValue())
            {
                return null;
            }

            if (Regex.IsMatch(ptn, @"^[0-1]{3}$"))
            {
                return null;
            }

            return new ValidationResult(ErsResources.GetMessage("10025", ErsResources.GetFieldName("disp_send_ptn")));
        }
    }
}
