﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.strategy
{
    /// <summary>
    /// Checks for duplicate in merchandise with gcode 
    /// </summary>
    public class CheckDuplicateMerchandiseGroupStgy
    {
        /// <summary>
        /// checks for duplicate gcode
        /// </summary>
        /// <param name="gcode"></param>
        /// <returns></returns>
        public virtual ValidationResult CheckDuplicate(string gcode)
        {
            if (string.IsNullOrEmpty(gcode))
                return null;

            var objGroup = ErsFactory.ersMerchandiseFactory.GetErsGroupWithGcode(gcode);
            if (objGroup != null)
            {
                return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("gcode"), gcode), new[] { "gcode" });
            }

            return null;
        }
    }
}
