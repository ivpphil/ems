﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise.strategy
{
    public class UpdatePriceForSearchStgy
    {
        /// <summary>
        /// update merchandise prices according to the current time.
        /// </summary>
        /// <param name="gcode">Gcode for searching merchandise</param>
        public virtual void Update(string gcode = "")
        {
            var updatePriceForSearchSpec = ErsFactory.ersMerchandiseFactory.GetUpdatePriceForSearchSpec();

            var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();

            if (!string.IsNullOrEmpty(gcode))
            {
                groupCriteria.gcode = gcode;
            }

            ErsRepository.UpdateSatisfying(updatePriceForSearchSpec, groupCriteria);
        }
    }
}
