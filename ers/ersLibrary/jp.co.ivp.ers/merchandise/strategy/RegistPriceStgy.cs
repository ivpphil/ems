﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.merchandise.strategy
{
    public class RegistPriceStgy
    {
        ErsPriceRepository repository = ErsFactory.ersMerchandiseFactory.GetErsPriceRepository();

        public virtual void Regist(string scode, string gcode, int? price, int? price2, int? cost_price, int? regular_price, int? regular_first_price)
        {
            //Update Normal
            var objPrice = this.GetErsPrice(scode, EnumPriceKbn.NORMAL);
            if (objPrice == null)
            {
                this.InsertPrice(scode, gcode, EnumPriceKbn.NORMAL, price, price2, cost_price);
            }
            else
            {
                this.UpdatePrice(objPrice, price, price2, cost_price);
            }


            //Update Regular
            objPrice = this.GetErsPrice(scode, EnumPriceKbn.REGULAR);
            if (objPrice == null)
            {
                this.InsertPrice(scode, gcode, EnumPriceKbn.REGULAR, regular_price, null, null);
            }
            else
            {
                if (regular_price.HasValue)
                {
                    this.UpdatePrice(objPrice, regular_price, null, null);
                }
                else
                {
                    repository.Delete(objPrice);
                }
            }

            //Update Regular First
            objPrice = this.GetErsPrice(scode, EnumPriceKbn.REGULAR_FIRST);
            if (objPrice == null)
            {
                this.InsertPrice(scode, gcode, EnumPriceKbn.REGULAR_FIRST, regular_first_price, null, null);
            }
            else
            {
                if (regular_first_price.HasValue)
                {
                    this.UpdatePrice(objPrice, regular_first_price, null, null);
                }
                else
                {
                    repository.Delete(objPrice);
                }
            }
        }

        private void UpdatePrice(ErsPrice objPrice, int? price, int? price2, int? cost_price)
        {
            objPrice.price = price;
            objPrice.price2 = price2;
            objPrice.cost_price = cost_price;
            var oldPrice = ErsFactory.ersMerchandiseFactory.getErsPriceWithId(objPrice.id);

            repository.Update(oldPrice, objPrice);
        }

        private void InsertPrice(string scode, string gcode, EnumPriceKbn price_kbn, int? price, int? price2, int? cost_price)
        {
            if (!price.HasValue && price_kbn != EnumPriceKbn.NORMAL)
            {
                return;
            }

            var objPrice = ErsFactory.ersMerchandiseFactory.GetErsPrice();
            objPrice.scode = scode;
            objPrice.gcode = gcode;
            objPrice.price_kbn = price_kbn;
            objPrice.price = price;
            objPrice.price2 = price2;
            objPrice.cost_price = cost_price;
            repository.Insert(objPrice);
        }

        private ErsPrice GetErsPrice(string scode, EnumPriceKbn? price_kbn)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria();
            criteria.scode = scode;
            criteria.price_kbn = price_kbn;
            var listPrice = repository.Find(criteria);
            if (listPrice.Count == 0)
            {
                return null;
            }

            return listPrice.First();
        }
    }
}
