﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.strategy
{
    /// <summary>
    /// Cheks for stock flag Validation
    /// </summary>
    public class CheckStock_flgStgy
    {
        /// <summary>
        /// check for stock flag
        /// </summary>
        /// <param name="model"></param>
        /// <param name="stock_flg"></param>
        /// <param name="stock_set1"></param>
        /// <param name="stock_set2"></param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> Check(EnumStockFlg? stock_flg, int? stock_set1, int? stock_set2)
        {
            foreach (var result in Check(stock_flg, stock_set1, stock_set2, string.Empty))
            {
                yield return result;
            }
        }


        public virtual IEnumerable<ValidationResult> Check(EnumStockFlg? stock_flg, int? stock_set1, int? stock_set2, string lineName)
        {
            if (stock_set1 == null || stock_set2 == null)
            {
                yield break;
            }

            if (stock_flg != EnumStockFlg.DisplaySymbol)
            {
                yield break;
            }

            if (stock_set1 >= stock_set2)
                yield return new ValidationResult(
                    lineName + ErsResources.GetMessage("10052",
                        ErsResources.GetFieldName("g_master_t.stock_set2"),
                        ErsResources.GetFieldName("g_master_t.stock_set1")),
                    new[] { "stock_set1", "stock_set2" });

            yield break;
        }
    }
}
