﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.strategy
{
    /// <summary>
    /// Checks if the product is already exist
    /// </summary>
    public class CheckMerchandiseExistStgy
    {
        /// <summary>
        /// checks for gcode,scode
        /// </summary>
        /// <param name="gcode"></param>
        /// <param name="scode"></param>
        /// <param name="lineName"></param>
        /// <returns>if not exists, error occured</returns>
        public virtual IEnumerable<ValidationResult> Check(string gcode, string scode)
        {
            if (string.IsNullOrEmpty(gcode) || string.IsNullOrEmpty(scode))
            {
                yield break;
            }

            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var crt = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();

            if (!string.IsNullOrEmpty(gcode))
            {
                crt.gcode = gcode;
                if (repository.GetRecordCount(crt) == 0)
                {
                    yield return new ValidationResult(ErsResources.GetMessage(("20003"), gcode), new[] { "gcode" });
                }

                if (!string.IsNullOrEmpty(scode))
                {
                    crt.scode = scode;
                    if (repository.GetRecordCountSkuBase(crt) == 0)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage(("20000"), scode), new[] { "scode" });
                    }
                }
            }
        }

        /// <summary>
        /// checks for gcode,scode
        /// </summary>
        /// <param name="gcode"></param>
        /// <param name="scode"></param>
        /// <param name="lineName"></param>
        /// <returns>if not exists, error occured</returns>
        public virtual IEnumerable<ValidationResult> CheckScodeExist(string scode)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var crt = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            
            if (!string.IsNullOrEmpty(scode))
            {
                crt.scode = scode;
                if (repository.GetRecordCountSkuBase(crt) == 0)
                {
                    yield return new ValidationResult(ErsResources.GetMessage(("20000"), scode), new[] { "scode" });
                }

            }
        }
    }
}
