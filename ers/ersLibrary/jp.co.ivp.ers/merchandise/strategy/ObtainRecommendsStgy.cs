﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.strategy
{
    /// <summary>
    /// Search for Merchandise
    /// </summary>
    public class ObtainRecommendsStgy
    {
        /// <summary>
        /// リコメンドのリストを取得する
        /// </summary>
        /// <param name="merchandise">values from ErsMerchandise</param>
        /// <returns></returns>
        public virtual IList<ErsMerchandise> FindRecommends(ErsMerchandise merchandise)
        {
            var t = merchandise.GetType();

            var recommendList = new string[ErsMerchandise.RECOMEND_ITEM_COUNT];
            for (int i = 0; i < ErsMerchandise.RECOMEND_ITEM_COUNT; i++)
            {
                recommendList[i] = ErsExpressionAccessor<ErsMerchandise, string>.GetPropertyValue(merchandise, "recommend" + (i + 1));
            }

            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();

            criteria.gcode_in = recommendList;
            criteria.g_active = EnumActive.Active;
            criteria.s_active = EnumActive.Active;
            criteria.disp_list_flg = EnumDisp_list_flg.Visible;
            criteria.price_kbn = EnumPriceKbn.NORMAL;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            return repository.FindGroupBaseItemList(criteria);
        }
    }
}
