﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.merchandise.specification;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise.category;
using jp.co.ivp.ers.util;
using System.Reflection;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsMerchandise
        : IErsCollectable, IOverwritable
    {
        /// <summary>
        /// リコメンドの数
        /// </summary>
        public static int RECOMEND_ITEM_COUNT { get { return 5; } }
        
        //Fields for g_master_t
        public virtual int? id { get; set; }
        public virtual string gcode { get; set; }
        public virtual string gname { get; set; }
        public virtual string m_gname { get; set; }
        public virtual DateTime? date_from { get; set; }
        public virtual DateTime? date_to { get; set; }
        public virtual EnumSalePatternType? s_sale_ptn { get; set; }
        public virtual EnumStockFlg? stock_flg { get; set; }
        public virtual int? stock_set1 { get; set; }
        public virtual int? stock_set2 { get; set; }
        public virtual int[] cate1 { get; set; }
        public virtual int[] cate2 { get; set; }
        public virtual int[] cate3 { get; set; }
        public virtual int[] cate4 { get; set; }
        public virtual int[] cate5 { get; set; }
        public virtual string recommend1 { get; set; }
        public virtual string recommend2 { get; set; }
        public virtual string recommend3 { get; set; }
        public virtual string recommend4 { get; set; }
        public virtual string recommend5 { get; set; }
        public virtual string metatitle { get; set; }
        public virtual string metadescription { get; set; }
        public virtual string metawords { get; set; }
        public virtual string metaalt { get; set; }
        public virtual string link_url { get; set; }
        public virtual string description { get; set; }
        public virtual string m_description { get; set; }
        public virtual string description_for_list { get; set; }
        public virtual EnumActive? active { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual EnumDisp_list_flg? disp_list_flg { get; set; }
        public virtual int? price_for_search { get; set; }
        public virtual int? sort { get; set; }
        public virtual string disp_send_ptn { get; set; }
        public virtual EnumCarriageCostType? carriage_cost_type { get; set; }
        public virtual EnumPluralOrderType? plural_order_type { get; set; }
        public virtual EnumSetFlg? set_flg { get; set; }
        public virtual int? doc_bundling_flg { get; set; }
        public virtual EnumDelvMethod? deliv_method { get; set; }
        public virtual string keyword { get; set; }
        public virtual string[] disp_keyword { get; set; }

        //Fields for s_master_t
        public virtual string scode { get; set; }
        public virtual string jancode { get; set; }
        public virtual string sname { get; set; }
        public virtual string m_sname { get; set; }
        public virtual string cts_sname { get; set; }
        public virtual string shipping_sname { get; set; }
        public virtual int? point { get; set; }
        public virtual EnumSoldoutFlg? soldout_flg { get; set; }
        public virtual string attribute1 { get; set; }
        public virtual string attribute2 { get; set; }
        public virtual int? disp_order { get; set; }
        public virtual DateTime? s_intime { get; set; }
        public virtual string mixed_group_code { get; set; }
        public virtual int? max_purchase_count { get; set; }
        public virtual DateTime? point_campaign_from { get; set; }
        public virtual DateTime? point_campaign_to { get; set; }
        public virtual int? campaign_point { get; set; }
        public virtual EnumActive? s_active { get; set; }
        public virtual DateTime? s_utime { get; set; }
        public virtual string supplier_code { get; set; }
        public virtual EnumWhOrderType? wh_order_type { get; set; }
        public virtual string s_keyword { get; set; }

        public virtual EnumOnOff? h_mall_flg { get; set; }
        //Fields for price_t
        public virtual DateTime? p_date_from { get; set; }
        public virtual DateTime? p_date_to { get; set; }
        public virtual int? price { get; set; }
        public virtual int? price2 { get; set; }
        public virtual DateTime? p_utime { get; set; }
        public virtual DateTime? p_intime { get; set; }
        public virtual int? regular_price { get; set; }
        public virtual int? regular_first_price { get; set; }
        public virtual int? sale_price { get; set; }
        public virtual int? member_rank { get; set; }
        public virtual int? cost_price { get; set; }
        public virtual int? non_member_price { get; set; }
        public virtual int? non_member_regular_price { get; set; }
        public virtual int? non_member_regular_first_price { get; set; }
        public virtual string maker_scode { get; set; }
        public virtual int[] site_id { get; set; }

        //Fields for stock_t
        public virtual int? stock { get; set; }

        //Display values for fileds
        public string w_sale_ptn { get { return ErsFactory.ersViewServiceFactory.GetErsViewSalePtnService().GetStringFromId(this.s_sale_ptn); } }

        public Dictionary<string, object> GetPropertiesAsDictionary()
        {
            return ErsReflection.GetPropertiesAsDictionary(this, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        }

        public void OverwriteWithParameter(IDictionary<string, object> dictionary)
        {
            var dic = ErsCommon.OverwriteDictionary(this.GetPropertiesAsDictionary(), dictionary);
            ErsReflection.SetPropertyAll(this, dic);
        }
    }
}
