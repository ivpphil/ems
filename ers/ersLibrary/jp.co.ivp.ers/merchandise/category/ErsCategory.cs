﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.category
{
    /// <summary>
    /// Holds values of cate1_t,cate2_t,cate3_t,cate4_t,cate5_t record.
    /// Inherit from repository
    /// </summary>
    public class ErsCategory
        : ErsRepositoryEntity
    {
        /// <summary>
        /// カテゴリの下限
        /// </summary>
        public const int minCategoryNumber = 1;

        /// <summary>
        /// カテゴリの上限
        /// </summary>
        public const int maxCategoryNumber = 5;

        /// <summary>
        /// カテゴリ番号
        /// </summary>
        public virtual int categoryNumber { get; set; }

        /// <summary>
        /// catex_t.id
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// catex_t.cate_name
        /// </summary>
        public virtual string cate_name { get; set; }

        /// <summary>
        /// catex_t.parent_id
        /// </summary>
        public virtual int? parent_id { get; set; }

        /// <summary>
        /// catex_t.active
        /// </summary>
        public virtual EnumActive? active { get; set; }

        /// <summary>
        /// catex_t.disp_order
        /// </summary>
        public int? disp_order { get; set; }

        /// <summary>
        /// catex_t.intime
        /// </summary>
        public DateTime? intime { get; set; }

        /// <summary>
        /// catex_t.utime
        /// </summary>
        public DateTime? utime { get; set; }

        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public int? site_id { get; set; }
    }
}