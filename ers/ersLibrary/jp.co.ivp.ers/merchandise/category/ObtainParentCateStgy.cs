﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.category
{
    /// <summary>
    /// Get the parent category
    /// </summary>
    public class ObtainParentCateStgy
        : ISpecificationForSQL
    {
        /// <summary>
        /// Get Select Data
        /// </summary>
        /// <param name="categoryNumber">integer value for vategory number</param>
        /// <param name="joinWithSiteTable">set include join with site_t</param>
        /// <returns>data table</returns>
        public virtual List<Dictionary<string, object>> SelectSatisfying(int categoryNumber, bool joinWithSiteTable = false)
        {
            this.categoryNumber = categoryNumber;
            this.joinWithSiteTable = joinWithSiteTable;
            return ErsRepository.SelectSatisfying(this);

        }

        protected virtual int categoryNumber { get; set; }
        protected virtual bool joinWithSiteTable { get; set; }
        public virtual string asSQL()
        {
            var fromStatement = string.Empty;
            var arrayStatement = string.Empty;
            for (var i = categoryNumber ; i > ErsCategory.minCategoryNumber; i--)
            {
                var parentCateTableName = "cate" + (i - 1) + "_t";
                var cateTableName = "cate" + i + "_t";
                fromStatement += " LEFT JOIN " + parentCateTableName + " ON " + cateTableName + ".parent_id = " + parentCateTableName + ".id ";

                arrayStatement += ",COALESCE(" + parentCateTableName + ".id, -1)";
            }

            if (!string.IsNullOrEmpty(arrayStatement))
                arrayStatement = "ARRAY[" + arrayStatement.Substring(1) + "]";
            else
                arrayStatement = "null";

            var tableName = "cate" + categoryNumber + "_t";
            
            var site_name = "";
            if (this.joinWithSiteTable)
            {
                fromStatement += " LEFT JOIN site_t  ON site_t.id = " + tableName + ".site_id ";
                site_name = ",site_t.site_name ";
            }

            return "SELECT " + tableName + ".*, " + arrayStatement + " AS parrent_array " + site_name + "  FROM " + tableName + " " + fromStatement + " ORDER BY " + tableName + ".site_id," + tableName + ".disp_order, " + tableName + ".id ";
        }
    }
}
