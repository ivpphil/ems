﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using System.Data;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.category
{
    /// <summary>
    /// Get the list of cate
    /// </summary>
    public class ErsCategoryTree
        : List<ErsCategory>
    {

        protected ErsCategoryTree(List<ErsCategory> categories)
            : base(categories)
        {
        }

        /// <summary>
        /// 指定したカテゴリを取得する(1 ～ 5)
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public new List<ErsCategory> this[int index]
        {
            get { return this[index - 1]; }
        }

        public static int categoryDepth
        {
            get
            {
                if (ErsCommonContext.GetPooledObject("categoryDepth") == null)
                    ErsCommonContext.SetPooledObject("categoryDepth", GetCategoryDepth());
                return (int)ErsCommonContext.GetPooledObject("categoryDepth");
            }
        }

        protected static int GetCategoryDepth()
        {
            for (var i = ErsCategory.maxCategoryNumber; i >= ErsCategory.minCategoryNumber; i--)
            {
                var repository = ErsFactory.ersMerchandiseFactory.GetErsCategoryRepository(i);
                var c = ErsFactory.ersMerchandiseFactory.GetErsCategoryCriteria(i);
                c.LIMIT = 1;
                var cateList = repository.Find(c);
                if (cateList.Count == 0)
                {
                    continue;
                }

                if (i != 1 && cateList[0].parent_id == null)
                {
                    return i - 1;
                }
            }
            return ErsCategory.maxCategoryNumber;
        }

        /// <summary>
        /// get the category in view tree
        /// </summary>
        /// <param name="merchandise">values from ErsMerchandise</param>
        /// <returns>list of catgory with subs category</returns>
        public static IEnumerable<ErsCategoryTree> GetTree(ErsMerchandise merchandise)
        {
            foreach (var cateRootValue in GetCategoryList(1, merchandise.cate1))
            {
                List<ErsCategory> retList = new List<ErsCategory>(ErsCategoryTree.categoryDepth);
                for (var i = 0; i < categoryDepth; i++)
                {
                    retList.Add(null);
                }

                foreach (var list in GetChildCategories(merchandise, cateRootValue, 1, retList))
                {
                    yield return new ErsCategoryTree(list);
                }
            }
        }

        protected static List<ErsCategory> GetCategoryList(int categoryNumber, int[] arrCategory, bool asSingleton = false)
        {
            var listCategory = new List<ErsCategory>();

            if (arrCategory == null || arrCategory.Length == 0)
                return listCategory;

            var repository = ErsFactory.ersMerchandiseFactory.GetErsCategoryRepository(categoryNumber);
            var c = ErsFactory.ersMerchandiseFactory.GetErsCategoryCriteria(categoryNumber);
            c.ids = arrCategory;
            var result = repository.Find(c, asSingleton);

            foreach (var category in result)
            {
                listCategory.Add(category);
            }

            return listCategory;
        }

        protected static IEnumerable<List<ErsCategory>> GetChildCategories(ErsMerchandise merchandise, ErsCategory parentCategory, int parentCategoryNumber, List<ErsCategory> retList)
        {
            retList[parentCategoryNumber - 1] = parentCategory;

            if (parentCategoryNumber == ErsCategoryTree.categoryDepth)
            {
                yield return retList;
                yield break;
            }

            if (parentCategory == null)
            {
                foreach (var list in GetChildCategories(merchandise, null, parentCategoryNumber + 1, retList))
                {
                    yield return retList;
                    yield break;
                }
            }
            else
            {
                //カテゴリーツリーを作成
                var cateValue = (int[])ErsReflection.GetPropertyValue(merchandise, "cate" + (parentCategoryNumber + 1));
                var childCategories = GetCategoryList(parentCategoryNumber + 1, cateValue);

                bool hasChild = false;
                foreach (var childCategory in childCategories)
                {
                    if (parentCategory.id == childCategory.parent_id)
                    {
                        foreach (var list in GetChildCategories(merchandise, childCategory, parentCategoryNumber + 1, retList))
                        {
                            yield return retList;
                            hasChild = true;
                        }
                    }
                }
                if (!hasChild)
                {
                    foreach (var list in GetChildCategories(merchandise, null, parentCategoryNumber + 1, retList))
                    {
                        yield return retList;
                        yield break;
                    }
                }
            }
        }
    }
}