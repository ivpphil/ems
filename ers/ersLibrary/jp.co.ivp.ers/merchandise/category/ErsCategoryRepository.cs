﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.category
{
    /// <summary>
    /// Provides methods to cate with CRUD
    /// inherit from ErsRepository
    /// </summary>
    public class ErsCategoryRepository
        : ErsRepository<ErsCategory>
    {
        protected int categoryNumber { get; set; }

        protected const string tableName = "cate{0}_t";
        public ErsCategoryRepository(int categoryNumber)
            : base(string.Format(tableName, categoryNumber))
        {
            this.categoryNumber = categoryNumber;
        }

        /// <summary>
        /// search records from cate{0}_t
        /// </summary>
        /// <param name="criteria">ErsCategoryCriteria</param>
        /// <returns>return a list of result</returns>
        public override IList<ErsCategory> Find(Criteria criteria)
        {
            return this.Find(criteria, false);
        }

        public virtual IList<ErsCategory> Find(Criteria criteria, bool asSingleton)
        {
            var retVal = new List<ErsCategory>();

            var listDic = this.ersDB_table.gSelect(criteria);
            foreach (var dic in listDic)
            {
                if (asSingleton)
                {
                    retVal.Add(this.CategoryAsSingleton(this.categoryNumber, dic));
                }
                else
                {
                    retVal.Add(this.GetCategoryFromDB(this.categoryNumber, dic));
                }
            }
            return retVal;
        }

        public virtual ErsCategory CategoryAsSingleton(int categoryNumber, Dictionary<string, object> dic)
        {
            var list = CategorySingletons();

            var key = Convert.ToString(categoryNumber) + "\\" + Convert.ToString(dic["id"]);

            if (!list.ContainsKey(key))
                list.Add(key, this.GetCategoryFromDB(categoryNumber, dic));

            return list[key];

        }

        protected virtual ErsCategory GetCategoryFromDB(int categoryNumber, Dictionary<string, object> dic)
        {
            return ErsFactory.ersMerchandiseFactory.GetErsCategoryWithParameters(categoryNumber, dic);
        }

        protected static Dictionary<string, ErsCategory> CategorySingletons()
        {
            var list = (Dictionary<string, ErsCategory>)ErsCommonContext.GetPooledObject("CategoryAsSingleton");
            if (list == null)
            {
                list = new Dictionary<string, ErsCategory>();
                ErsCommonContext.SetPooledObject("CategoryAsSingleton", list);
            }
            return list;
        }
    }
}
