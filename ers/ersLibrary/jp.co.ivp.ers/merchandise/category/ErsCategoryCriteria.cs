﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.category
{
    /// <summary>
    /// Represents the search condition of cate1_t,cate2_t,cate3_t,cate4_t,cate5_t
    /// inherit from criteria
    /// </summary>
    public class ErsCategoryCriteria
        : Criteria
    {
        public ErsCategoryCriteria(int categoryNumber)
        {
            this.categoryNumber = categoryNumber;
        }

        protected virtual int categoryNumber { get; set; }

        protected string tableName { get { return string.Format("cate{0}_t", this.categoryNumber); } }

        /// <summary>
        /// search condition for id
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion(this.tableName + ".id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for id
        /// </summary>
        public virtual int? id_not_eq
        {
            set
            {
                this.Add(Criteria.GetCriterion(this.tableName + ".id", value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// search condition for ids
        /// </summary>
        public virtual int[] ids
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion(this.tableName + ".id", value));
            }
        }

        /// <summary>
        /// search condition for parent_id
        /// </summary>
        public virtual int? parent_id
        {
            set
            {
                this.Add(Criteria.GetCriterion(this.tableName + ".parent_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for active
        /// </summary>
        public virtual EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion(this.tableName + ".active", (int?)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion(this.tableName + ".site_id", (int?)value, Operation.EQUAL));
            }
        }

        public virtual int? site_id_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion(this.tableName + ".site_id", (int?)value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// set order by disp_order
        /// </summary>
        public void SetOrderByDispOrder(OrderBy orderBy)
        {
            AddOrderBy(this.tableName + ".disp_order", orderBy);
        }

        /// <summary>
        /// set order by id
        /// </summary>
        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy(this.tableName + ".id", orderBy);
        }

        internal void SetNoParent()
        {
            this.Add(Criteria.GetCriterion(string.Format("cate{0}_t.id", this.categoryNumber - 1), null, Operation.EQUAL));
        }
    }
}
