﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.stock
{
    /// <summary>
    /// Represents the search condition of stock_t
    /// inherit from criteria
    /// </summary>
    public class ErsStockCriteria
        : Criteria
    {
        /// <summary>
        /// add criteria for scode
        /// </summary>
        public virtual string scode
        {
            set
            {
                Add(Criteria.GetCriterion("stock_t.scode", value, Operation.EQUAL));
            }
        }

        public string jancode
        {
            set
            {
                Add(Criteria.GetCriterion("s_master_t.jancode", value, Operation.EQUAL));
            }
        }

        public int? stock_less_than
        {
            set
            {
                Add(Criteria.GetCriterion("stock_t.stock", value, Operation.LESS_THAN));
            }
        }


        public void SetOrderByScode(OrderBy orderby)
        {
            this.AddOrderBy("stock_t.scode", orderby);
        }
    }
}
