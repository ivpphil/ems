﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.Data;

namespace jp.co.ivp.ers.merchandise.stock.insert_ver
{
    /// <summary>
    /// Checks if the item/product is out of stock
    /// </summary>
    public class OutOfStockOnPurchaseInsertSpecification
        : OutOfStockOnPurchaseSpecification, ISpecificationForSQL
    {
        protected internal OutOfStockOnPurchaseInsertSpecification() { }

        /// <summary>
        /// 在庫減算後の在庫チェック
        /// </summary>
        /// <param name="scode"></param>
        /// <param name="id"></param>
        /// <returns>在庫がなければtrue</returns>
        public override bool IsSatisfiedBy(string scode, int stock_id)
        {
            this.scode = scode;
            this.stock_id = stock_id;

            var dt = ErsRepository.SelectSatisfying(this);
            if (dt.Count <= 0)
                return true;

            return false;
        }

        protected string scode;
        protected int stock_id;

        public virtual string asSQL()
        {
            return " SELECT id AS count"
                + " FROM s_master_t s WHERE s.scode = '" + scode + "'"
                + " AND CASE WHEN soldout_flg = " + (int)EnumSoldoutFlg.DisableSoldout + " THEN true"
                + " ELSE (s.stock -"
                + "  (SELECT CASE WHEN SUM(stock) IS NULL THEN 0 ELSE SUM(stock) END FROM stock_t WHERE scode = s.scode AND id <= " + stock_id + ")"
                + " ) >= 0"
                + " END";
        }
    }
}