﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.stock.insert_ver
{
    /// <summary>
    /// Integrates stock_t.stock and s_master_t.stock.
    /// </summary>
    public class StockIntegrationInsertSpec
        : StockIntegrationSpec, ISpecificationForSQL
    {
        /// <summary>
        /// updating the record from s_master_t
        /// </summary>
        public override void IntegrateStock()
        {
            var stockRepository = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockRepository();
            var currentSequence = stockRepository.GetNextSequence();

            var criteria = new Criteria();
            criteria.Add(Criteria.GetCriterion("stock_t.id", currentSequence, Criteria.Operation.LESS_THAN));
            criteria.AddGroupBy("s_master_t.scode");
            criteria.AddGroupBy("s_master_t.stock");

            var list = ErsRepository.SelectSatisfying(this, criteria);

            var repository = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockRepository();
            foreach (var item in list)
            {
                var scode = Convert.ToString(item["scode"]);
                var oldStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(scode);
                var newStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(scode);
                newStock.stock = Convert.ToInt32(item["stock"]);
                repository.Update(oldStock, newStock);
            }

            //過去のものを削除
            criteria.Clear();
            criteria.Add(Criteria.GetCriterion("stock_t.id", currentSequence, Criteria.Operation.LESS_THAN));
            stockRepository.Delete(criteria);
        }

        public virtual string asSQL()
        {
            return "SELECT s_master_t.scode, s_master_t.stock - SUM(stock_t.stock) AS stock FROM s_master_t INNER JOIN stock_t ON s_master_t.scode = stock_t.scode ";
        }
    }
}
