﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.db.table;

namespace jp.co.ivp.ers.merchandise.stock.insert_ver
{
    /// <summary>
    /// Insert the item into stock_t and stock_log_t for decreased stocks
    /// </summary>
    public class DecreaseStockInsertStgy
        : DecreaseStockStgy
    {
        /// <summary>
        /// Insert the item into stock_t and stock_log_t
        /// </summary>
        /// <param name="merchandise">values from ErsMerchandiseInBasket</param>
        public override void Decrease(string scode, int amount)
        {
            var objStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(scode);
            objStock.stock = amount;

            var repository = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockRepository();
            repository.Insert(objStock);

            var stock_id = repository.GetCurrentSequence();

            //在庫切れチェック
            if (ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetOutOfStockOnPurchaseSpecification().IsSatisfiedBy(scode, stock_id))
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                throw new ErsException("20205", scode, setup.sec_url + setup.cart_url);
            }
        }
    }
}
