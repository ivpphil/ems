﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.db.table;

namespace jp.co.ivp.ers.merchandise.stock.insert_ver
{
    /// <summary>
    /// Insert the item into stock_t and stock_log_t for Increased stocks
    /// </summary>
    public class IncreaseStockInsertStgy
        : IncreaseStockStgy
    {
        /// <summary>
        /// Insert the item into stock_t and stock_log_t
        /// </summary>
        /// <param name="scode">Product code</param>
        /// <param name="amount">price of an item/product</param>
        public override void Increase(string scode, int amount)
        {
            var calcService = ErsFactory.ersOrderFactory.GetErsCalcService();

            var merchandise = ErsFactory.ersBasketFactory.GetErsBaskRecordWithScode(null, scode, null);
            calcService.calcBaskRecord(merchandise, amount * -1);

            var objStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(scode);

            var repository = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockRepository();
            repository.Insert(objStock);
        }
    }
}
