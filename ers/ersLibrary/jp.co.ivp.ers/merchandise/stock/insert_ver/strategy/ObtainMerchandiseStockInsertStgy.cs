﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.stock.insert_ver
{
    /// <summary>
    /// The strategy class that Provides methods to get stock of the merchandise.
    /// </summary>
    public class ObtainMerchandiseStockInsertStgy
        : ObtainMerchandiseStockStgy, ISpecificationForSQL
    {
        /// <summary>
        /// 何度も在庫を取得するので、キャッシュするように修正
        /// </summary>
        protected virtual Dictionary<string, int> StockCache
        {
            get
            {
                if (ErsCommonContext.GetPooledObject("StockCache") == null)
                    StockCache = new Dictionary<string, int>();

                return (Dictionary<string, int>)ErsCommonContext.GetPooledObject("StockCache");
            }
            set
            {
                ErsCommonContext.SetPooledObject("StockCache", value);
            }
        }

        /// <summary>
        /// Gets stock of the merchandise by considering the stock_t.
        /// </summary>
        public override int GetStock(string scode, int? stock)
        {
            if (StockCache.ContainsKey(scode))
                return StockCache[scode];

            this.scode = scode;

            var dt = ErsRepository.SelectSatisfying(this);

            StockCache[scode] = Convert.ToInt32(dt[0]["stock"]);
            return StockCache[scode];
        }

        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public virtual int GetCountData(Criteria criteria)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Bridges the scode to the asSQL method.
        /// </summary>
        protected virtual string scode { get; set; }

        /// <summary>
        /// this query is executed by repository.
        /// </summary>
        /// <returns></returns>
        public virtual string asSQL()
        {
            return " SELECT s_master_t.stock - COALESCE(SUM(stock_t.stock), 0) AS stock"
                + " FROM s_master_t "
                + " LEFT JOIN stock_t ON s_master_t.scode = stock_t.scode "
                + " WHERE s_master_t.scode = '" + scode + "'"
                + " GROUP BY s_master_t.scode, s_master_t.stock";
        }
    }
}