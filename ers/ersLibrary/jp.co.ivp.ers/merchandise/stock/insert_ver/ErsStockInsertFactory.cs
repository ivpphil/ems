﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.merchandise.stock.insert_ver
{
    /// <summary>
    /// Provides for class stocks related
    /// </summary>
    public class ErsStockInsertFactory
        : ErsStockFactory
    {
        /// <summary>
        /// Get the class of OutOfStockOnPurchaseSpecification
        /// </summary>
        /// <returns>new instance of OutOfStockOnPurchaseSpecification</returns>
        public override OutOfStockOnPurchaseSpecification GetOutOfStockOnPurchaseSpecification()
        {
            return new OutOfStockOnPurchaseInsertSpecification();
        }

        /// <summary>
        /// Get the class of DecreaseStockStgy
        /// </summary>
        /// <returns>new instance of DecreaseStockStgy</returns>
        public override DecreaseStockStgy GetDecreaseStockStgy()
        {
            return new DecreaseStockInsertStgy();
        }
        /// <summary>
        /// Get the class of IncreaseStockStgy
        /// </summary>
        /// <returns>new instance of IncreaseStockStgy</returns>
        public override IncreaseStockStgy GetIncreaseStockStgy()
        {
            return new IncreaseStockInsertStgy();
        }
        /// <summary>
        /// Get the class of ObtainMerchandiseStockInsertStgy
        /// </summary>
        /// <returns>new instance of ObtainMerchandiseStockInsertStgy</returns>
        public override ObtainMerchandiseStockStgy GetObtainMerchandiseStockStgy()
        {
            return new ObtainMerchandiseStockInsertStgy();
        }
        /// <summary>
        /// Get the class of StockIntegrationInsertSpec
        /// </summary>
        /// <returns>new instance of StockIntegrationInsertSpec</returns>
        public override StockIntegrationSpec GetStockIntegrationSpec()
        {
            return new StockIntegrationInsertSpec();
        }
    }
}
