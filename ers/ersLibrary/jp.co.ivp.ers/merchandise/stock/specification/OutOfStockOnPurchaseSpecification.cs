﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.Data;

namespace jp.co.ivp.ers.merchandise.stock
{
    /// <summary>
    /// Checks if the item/product is out of stock before purchase
    /// </summary>
    public class OutOfStockOnPurchaseSpecification
    {
        protected internal OutOfStockOnPurchaseSpecification() { }

        /// <summary>
        /// 在庫減算後の在庫チェック
        /// </summary>
        /// <param name="scode">product code</param>
        /// <param name="id">product id</param>
        /// <returns>在庫がなければtrue</returns>
        public virtual bool IsSatisfiedBy(string scode, int stock_id)
        {
            return false;
        }

    }
}