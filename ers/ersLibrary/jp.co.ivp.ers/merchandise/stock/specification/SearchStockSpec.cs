﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.stock.specification
{
    public class SearchStockSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return "SELECT DISTINCT stock_t.* FROM stock_t INNER JOIN s_master_t ON s_master_t.scode = stock_t.scode";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(DISTINCT stock_t.id) AS count FROM stock_t INNER JOIN s_master_t ON s_master_t.scode = stock_t.scode";
        }
    }
}
