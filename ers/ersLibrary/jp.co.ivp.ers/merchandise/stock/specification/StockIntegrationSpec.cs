﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.stock
{
    /// <summary>
    /// Integrates stock_t.stock and s_master_t.stock.
    /// </summary>
    public class StockIntegrationSpec
    {

        public virtual void IntegrateStock()
        {
            return;
        }
    }
}
