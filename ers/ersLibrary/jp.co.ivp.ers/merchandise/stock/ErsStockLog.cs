﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise.stock
{
    /// <summary>
    /// Holds values of  stock_t ,stock_log_t record.
    /// Inherit from repository
    /// </summary>
    public class ErsStockLog
        : ErsRepositoryEntity
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// product code
        /// </summary>
        public virtual string scode { get; set; }

        /// <summary>
        /// count of before stocks
        /// </summary>
        public virtual int? before_stock { get; set; }

        /// <summary>
        /// count of after stocks
        /// </summary>
        public virtual int? after_stock { get; set; }
    }
}
