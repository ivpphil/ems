﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise.stock.specification;

namespace jp.co.ivp.ers.merchandise.stock
{
    /// <summary>
    /// Provides methods to obtain instance of Stock-related classes.
    /// </summary>
    public class ErsStockFactory
    {
        /// <summary>
        /// Get the class of OutOfStockOnPurchaseSpecification
        /// </summary>
        /// <returns>new instance of OutOfStockOnPurchaseSpecification</returns>
        public virtual OutOfStockOnPurchaseSpecification GetOutOfStockOnPurchaseSpecification()
        {
            return new OutOfStockOnPurchaseSpecification();
        }

        /// <summary>
        /// Get the class of DecreaseStockStgy
        /// </summary>
        /// <returns>new instance of DecreaseStockStgy</returns>
        public virtual DecreaseStockStgy GetDecreaseStockStgy()
        {
            return new DecreaseStockStgy();
        }

        /// <summary>
        /// Get the class of IncreaseStockStgy
        /// </summary>
        /// <returns>new instance of IncreaseStockStgy</returns>
        public virtual IncreaseStockStgy GetIncreaseStockStgy()
        {
            return new IncreaseStockStgy();
        }

        /// <summary>
        /// Get the class of ObtainMerchandiseStockStgy
        /// </summary>
        /// <returns>new instance of ObtainMerchandiseStockStgy</returns>
        public virtual ObtainMerchandiseStockStgy GetObtainMerchandiseStockStgy()
        {
            return new ObtainMerchandiseStockStgy();
        }

        /// <summary>
        /// Get the class of StockIntegrationSpec
        /// </summary>
        /// <returns>new instance of StockIntegrationSpec</returns>
        public virtual StockIntegrationSpec GetStockIntegrationSpec()
        {
            return new StockIntegrationSpec();
        }

        /// <summary>
        /// Get the class of ErsStockRepository
        /// </summary>
        /// <returns>new instance of ErsStockRepository</returns>
        public virtual ErsStockRepository GetErsStockRepository()
        {
            return new ErsStockRepository();
        }

        /// <summary>
        /// Get the class of ErsStockCriteria
        /// </summary>
        /// <returns>new instance of ErsStockCriteria</returns>
        public virtual ErsStockCriteria GetErsStockCriteria()
        {
            return new ErsStockCriteria();
        }

        /// <summary>
        /// Get the class of ErsStock
        /// </summary>
        /// <returns>new instance of ErsStock</returns>
        public virtual ErsStock GetErsStock()
        {
            return new ErsStock();
        }

        /// <summary>
        /// Get the class of ErsStock based on the product code
        /// </summary>
        /// <param name="scode">product code</param>
        /// <returns>new instance of ErsStock</returns>
        public virtual ErsStock GetErsStockWithScode(string scode)
        {
            var repository = this.GetErsStockRepository();
            var criteria = this.GetErsStockCriteria();
            criteria.scode = scode;
            var list = repository.Find(criteria);
            if (list.Count != 1)
                return null;

            return list[0];
        }
        /// <summary>
        /// Get the class of ErsStock based on the values of entity of stocks
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>new instance of ErsStock</returns>
        public virtual ErsStock GetErsStockWithJancode(string jancode)
        {
            var repository = this.GetErsStockRepository();
            var criteria = this.GetErsStockCriteria();
            criteria.jancode = jancode;
            var count = repository.GetRecordCount(criteria);
            var list = repository.Find(criteria);
            if (list.Count != 1)
                return null;

            return list[0];
        }

        public virtual SearchStockSpec GetSearchStockSpec()
        {
            return new SearchStockSpec();
        }
    }
}
