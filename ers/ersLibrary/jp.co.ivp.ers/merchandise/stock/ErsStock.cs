﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise.stock
{   
    /// <summary>
    /// Holds values of stock_t record.
    /// Inherit from repository
    /// </summary>
    public class ErsStock
        : ErsRepositoryEntity
    {

        public override int? id { get; set; }

        public virtual string scode { get; set; }

        public virtual int? stock { get; set; }
    }
}
