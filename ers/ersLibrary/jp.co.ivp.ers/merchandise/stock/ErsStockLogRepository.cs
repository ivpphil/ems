﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise.stock
{
    public class ErsStockLogRepository
        : ErsRepository<ErsStockLog>
    {
        public ErsStockLogRepository()
            : base("stock_log_t")
        {
        }
    }
}
