﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.stock
{
    /// <summary>
    /// Provides methods to access stock_t,stock_log_t
    /// </summary>
    public class ErsStockRepository
        : ErsRepository<ErsStock>
    {
        protected internal ErsStockRepository()
            : base("stock_t")
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDB"></param>
        public ErsStockRepository(ErsDatabase objDB)
            : base("stock_t", objDB)
        {
        }

        /// <summary>
        /// insert records into stock_t,stock_log_t
        /// </summary>
        /// <param name="obj">values from ErsStock</param>
        public override void Insert(ErsStock obj, bool storeNewIdToObject = false)
        {
            base.Insert(obj, storeNewIdToObject);

            ErsStockLog objStockLog = ErsFactory.ersMerchandiseFactory.GetErsStockLog();

            objStockLog.scode = obj.scode;
            objStockLog.after_stock = obj.stock;
            var repository = ErsFactory.ersMerchandiseFactory.GetErsStockLogRepository();
            repository.Insert(objStockLog);
        }

        /// <summary>
        /// update records of stock_t,stock_log_t
        /// </summary>
        /// <param name="old_obj">values from ErsStock</param>
        /// <param name="new_obj">values from ErsStock</param>
        public override void Update(ErsStock old_obj, ErsStock new_obj)
        {
            base.Update(old_obj, new_obj);

            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            if (setup.site_type == EnumSiteType.ADMIN)
            {
                if (old_obj.stock != new_obj.stock)
                {
                    ErsStockLog objStockLog = ErsFactory.ersMerchandiseFactory.GetErsStockLog();
                    objStockLog.scode = new_obj.scode;
                    objStockLog.after_stock = new_obj.stock;
                    objStockLog.before_stock = old_obj.stock;

                    var repository = ErsFactory.ersMerchandiseFactory.GetErsStockLogRepository();
                    repository.Insert(objStockLog);
                }
            }
        }

        /// <summary>
        /// search records from stock_t,stock_log_t
        /// </summary>
        /// <param name="criteria">criteria from ErsStockCriteria</param>
        /// <returns>list of result or data table</returns>
        public override IList<ErsStock> Find(db.Criteria criteria)
        {
            var searchSpec = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetSearchStockSpec();
            var list = searchSpec.GetSearchData(criteria);

            var retList = new List<ErsStock>();

            foreach (var item in list)
            {
                var objStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStock();
                objStock.OverwriteWithParameter(item);
                retList.Add(objStock);
            }
            return retList;
        }
        /// <summary>
        /// get record count of stock_t,stock_log_t
        /// </summary>
        /// <param name="criteria">criteria from ErsStockCriteria</param>
        /// <returns>data count</returns>
        public override long GetRecordCount(db.Criteria criteria)
        {
            var searchSpec = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetSearchStockSpec();
            return searchSpec.GetCountData(criteria);
        }
    }
}
