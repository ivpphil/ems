﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.merchandise.stock
{
    /// <summary>
    /// Update the stock_t for increased stocks
    /// </summary>
    public class IncreaseStockStgy
        : ISpecificationForSQL
    {
        /// <summary>
        /// increasing the stocks
        /// </summary>
        /// <param name="scode"></param>
        /// <param name="amount"></param>
        public virtual void Increase(string scode, int amount)
        {

            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.scode = scode;
            criteria.g_active = EnumActive.Active;
            criteria.s_active = EnumActive.Active;

            var record = repository.FindSkuBaseItemList(criteria, null);

            if (record.Count == 0)
            {
                throw new ErsException("20000", scode);
            }

            var merchandise = record.First();

            if (merchandise.set_flg != EnumSetFlg.IsSet)
            {
                this.IncreaseNotSet(scode, amount, merchandise.soldout_flg.Value);
            }
            else
            {
                this.IncreaseSet(scode, amount);
            }
           // var result = ErsRepository.UpdateSatisfying(this, null);
        }

        public void IncreaseNotSet(string scode, int amount, EnumSoldoutFlg soldout_flg)
        {
            this.scode = scode;
            this.amount = amount;
            this.soldout_flg = soldout_flg;

            ErsRepository.UpdateSatisfying(this, null);          
        }

        /// <summary>
        /// Set increase of set item's stock
        /// </summary>
        /// <param name="merchandise"></param>
        private void IncreaseSet(string scode, int amount)
        {
            //the parent stock is not Increased.

            //子商品リスト取得
            var IncreaseSetMerchandiseList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(scode);

            //子商品分在庫減算
            foreach (ErsSetMerchandise item in IncreaseSetMerchandiseList)
            {
                var setMerchandise = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(item.scode);

                if (setMerchandise == null)
                {
                    continue;
                }

                this.IncreaseNotSet(setMerchandise.scode, item.amount.Value * amount, setMerchandise.soldout_flg.Value);
            }
        }

        protected virtual string scode { get; set; }
        protected virtual int amount { get; set; }
        protected virtual EnumSoldoutFlg soldout_flg { get; set; }

        public virtual string asSQL()
        {
            var retString = "UPDATE stock_t SET stock = stock + " + amount + " WHERE scode = '" + scode + "' ";

            return retString;
        }
    }
}
