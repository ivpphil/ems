﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise.stock
{
    /// <summary>
    /// The strategy class that Provides methods to get stock of the merchandise.
    /// </summary>
    public class ObtainMerchandiseStockStgy
    {
        /// <summary>
        /// Gets stock of the merchandise by considering the stock_t.
        /// </summary>
        public virtual int GetStock(string scode, int? stock)
        {
            if (stock == null)
            {
                return 0;
            }
            else
            {
                return stock.Value;
            }
        }
    }
}
