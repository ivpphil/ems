﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.merchandise.stock
{
    /// <summary>
    /// Update the stock_t for decreased stocks
    /// </summary>
    public class DecreaseStockStgy
        : ISpecificationForSQL
    {
        /// <summary>
        /// updating the record to decrease stocks
        /// </summary>
        /// <param name="merchandise">values from ErsMerchandiseInBasket</param>
        public virtual void Decrease(string scode, int amount)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.scode = scode;
            criteria.g_active = EnumActive.Active;
            criteria.s_active = EnumActive.Active;

            var record = repository.FindSkuBaseItemList(criteria, null);

            if (record.Count == 0)
            {
                throw new ErsException("20000", scode);
            }

            var merchandise = record.First();

            if (merchandise.set_flg != EnumSetFlg.IsSet)
            {
                this.DecreaseNotSet(scode, amount, merchandise.soldout_flg.Value);
            }
            else
            {
                this.DecreaseSet(scode, amount);
            }
        }

        /// <summary>
        /// セットで無い商品の減算
        /// </summary>
        /// <param name="merchandise"></param>
        public void DecreaseNotSet(string scode, int amount, EnumSoldoutFlg soldout_flg)
        {
            this.scode = scode;
            this.amount = amount;
            this.soldout_flg = soldout_flg;

            var result = ErsRepository.UpdateSatisfying(this, null);

            if (result != 1)
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                throw new ErsException("20205", scode, setup.sec_url + setup.cart_url);
            }
        }

        /// <summary>
        /// セット商品の減算
        /// </summary>
        /// <param name="merchandise"></param>
        private void DecreaseSet(string scode, int amount)
        {
            //the parent stock is not decreased.

            //子商品リスト取得
            var DecreaseSetMerchandiseList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(scode);

            //子商品分在庫減算
            foreach (ErsSetMerchandise item in DecreaseSetMerchandiseList)
            {
                var setMerchandise = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(item.scode);

                if (setMerchandise == null)
                {
                    continue;
                }

                this.DecreaseNotSet(setMerchandise.scode, item.amount.Value * amount, setMerchandise.soldout_flg.Value);
            }
        }

        protected virtual string scode { get; set; }
        protected virtual int amount { get; set; }
        protected virtual EnumSoldoutFlg soldout_flg { get; set; }

        public virtual string asSQL()
        {
            var retString = "UPDATE stock_t SET stock = stock - " + this.amount + " WHERE scode = '" + this.scode + "' ";

            if (this.soldout_flg != EnumSoldoutFlg.DisableSoldout)
                retString += "AND stock >= " + this.amount;

            return retString;
        }
    }
}
