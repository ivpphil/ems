﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsKeywords
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string keyword { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
        public string disp_keyword { get; set; }
    }
}
