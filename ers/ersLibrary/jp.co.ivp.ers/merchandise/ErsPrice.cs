﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsPrice
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual string gcode { get; set; }
        public virtual string scode { get; set; }
        public virtual DateTime? date_from { get; set; }
        public virtual DateTime? date_to { get; set; }
        public virtual int? price { get; set; }
        public virtual int? price2 { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual int? member_rank { get; set; }
        public virtual EnumPriceKbn? price_kbn { get; set; }
        public virtual int? cost_price { get; set; }
    }
}
