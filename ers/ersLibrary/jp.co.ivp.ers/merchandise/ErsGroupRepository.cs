﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsGroupRepository
        : ErsRepository<ErsGroup>
    {
        public ErsGroupRepository()
            : base("g_master_t")
        {
        }

        /// <summary>
        /// g_master_tとs_master_tのjoinでデータ取得
        /// </summary>
        /// <param name="emCri"></param>
        /// <returns></returns>
        public virtual IList<ErsMerchandise> FindGroupBaseItemList(ErsGroupCriteria groupCriteria)
        {
            var ret = new List<ErsMerchandise>();

            var searchGroupBaseItemListSpec = ErsFactory.ersMerchandiseFactory.GetSearchGroupBaseItemListSpec();
            var listRecord = searchGroupBaseItemListSpec.GetSearchData(groupCriteria);

            foreach (var record in listRecord)
            {
                var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandise();
                merchandise.OverwriteWithParameter(record);
                ret.Add(merchandise);
            }

            return ret;
        }

        /// <summary>
        /// g_master_tとs_master_tのjoinでデータ取得
        /// </summary>
        /// <param name="emCri"></param>
        /// <returns></returns>
        public long GetRecordCountGroupBase(Criteria groupCriteria)
        {
            return GetRecordCount(groupCriteria);
        }

        /// <summary>
        /// g_master_tとs_master_tのjoinでデータ取得
        /// </summary>
        /// <param name="emCri"></param>
        /// <returns></returns>
        public virtual IList<ErsMerchandise> FindSkuBaseItemList(ErsSkuCriteria groupCriteria, int? member_rank)
        {
            var ret = new List<ErsMerchandise>();

            var searchSkuBaseItemListSpec = ErsFactory.ersMerchandiseFactory.GetSearchSkuBaseItemListSpec();
            var listRecord = searchSkuBaseItemListSpec.GetSearchData(groupCriteria, member_rank);

            foreach (var record in listRecord)
            {
                var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandise();
                merchandise.OverwriteWithParameter(record);
                ret.Add(merchandise);
            }

            return ret;
        }

        public long GetRecordCountSkuBase(Criteria groupCriteria)
        {
            var searchSkuBaseItemListSpec = ErsFactory.ersMerchandiseFactory.GetSearchSkuBaseItemListSpec();
            return searchSkuBaseItemListSpec.GetCountData(groupCriteria);
        }
    }
}
