﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using System.Data;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsPriceRepository
        : ErsRepository<ErsPrice>
    {

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsPriceRepository()
            : base("price_t")
        {
        }

        /// <summary>
        /// Gets IList ErsPrice in price_t.
        /// </summary>
        /// <param name="criteria">Criteria for getting IList ErsPrice in price_t.</param>
        /// <returns>Returns List ErsPrice.</returns>
        public override IList<ErsPrice> Find(Criteria criteria)
        {
            List<ErsPrice> lstRet = new List<ErsPrice>();

            // 検索
            var priceSpe = ErsFactory.ersMerchandiseFactory.GetPriceSerachSpecification();
            var list = priceSpe.GetSearchData(criteria);

            foreach (var dr in list)
            {
                ErsPrice price = ErsFactory.ersMerchandiseFactory.getErsPriceWithParameter(dr);
                lstRet.Add(price);
            }
            return lstRet;
        }

        /// <summary>
        /// g_master_tとs_master_tのjoinでデータ取得
        /// </summary>
        /// <param name="emCri"></param>
        /// <returns></returns>
        public virtual IList<ErsMerchandise> FindPriceSkuBaseItemList(Criteria priceCriteria)
        {
            var ret = new List<ErsMerchandise>();

            var searchPriceSkuBaseItemListSpec = ErsFactory.ersMerchandiseFactory.GetSearchPriceSkuBaseItemListSpec();
            var listRecord = searchPriceSkuBaseItemListSpec.GetSearchData(priceCriteria);

            foreach (var record in listRecord)
            {
                var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandise();
                merchandise.OverwriteWithParameter(record);
                ret.Add(merchandise);
            }

            return ret;
        }

        public long GetRecordCountPriceSkuBase(Criteria priceCriteria)
        {
            var searchSkuBaseItemListSpec = ErsFactory.ersMerchandiseFactory.GetSearchPriceSkuBaseItemListSpec();
            return searchSkuBaseItemListSpec.GetCountData(priceCriteria);
        }

        public IList<ErsMerchandise> FindPriceGroupBaseItemList(ErsPriceCriteria priceCriteria)
        {
            var ret = new List<ErsMerchandise>();

            var searchPriceSkuBaseItemListSpec = ErsFactory.ersMerchandiseFactory.GetSearchPriceGroupBaseItemListSpec();
            var listRecord = searchPriceSkuBaseItemListSpec.GetSearchData(priceCriteria);

            foreach (var record in listRecord)
            {
                var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandise();
                merchandise.OverwriteWithParameter(record);
                ret.Add(merchandise);
            }

            return ret;
        }

        public long GetRecordCountPriceGroupBase(ErsPriceCriteria priceCriteria)
        {
            var searchSkuBaseItemListSpec = ErsFactory.ersMerchandiseFactory.GetSearchPriceGroupBaseItemListSpec();
            return searchSkuBaseItemListSpec.GetCountData(priceCriteria);
        }

        /// <summary>
        /// g_master_tとs_master_tのjoinでデータ取得
        /// </summary>
        /// <param name="emCri"></param>
        /// <returns></returns>
        public virtual IList<ErsMerchandise> FindPriceMemberRankItemList(Criteria priceCriteria)
        {
            var ret = new List<ErsMerchandise>();

            var searchPriceSkuBaseItemListSpec = ErsFactory.ersMerchandiseFactory.GetSearchPriceMemberRankItemListSpec();
            var listRecord = searchPriceSkuBaseItemListSpec.GetSearchData(priceCriteria);

            foreach (var record in listRecord)
            {
                var merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandise();
                merchandise.OverwriteWithParameter(record);
                ret.Add(merchandise);
            }

            return ret;
        }

        public long GetRecordCountPriceMemberRank(Criteria groupCriteria)
        {
            var searchSkuBaseItemListSpec = ErsFactory.ersMerchandiseFactory.GetSearchPriceMemberRankItemListSpec();
            return searchSkuBaseItemListSpec.GetCountData(groupCriteria);
        }
    }
}
