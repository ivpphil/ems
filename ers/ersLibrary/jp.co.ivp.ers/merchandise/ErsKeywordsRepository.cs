﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsKeywordsRepository
        : ErsRepository<ErsKeywords>
    {
        public ErsKeywordsRepository()
            : base("keywords_t")
        {
        }

        public IList<ErsKeywords> FindKeyword(ErsKeywordsRelationCriteria keywordRelationCriteria)
        {
            var ret = new List<ErsKeywords>();

            var searchKeywordListSpec = ErsFactory.ersMerchandiseFactory.GetSearchKeywordListSpec();
            var listRecord = searchKeywordListSpec.GetSearchData(keywordRelationCriteria);

            foreach (var record in listRecord)
            {
                var keyword = ErsFactory.ersMerchandiseFactory.GetErsKeywords();
                keyword.OverwriteWithParameter(record);
                ret.Add(keyword);
            }

            return ret;
        }
    }
}
