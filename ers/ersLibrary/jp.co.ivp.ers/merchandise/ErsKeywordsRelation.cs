﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsKeywordsRelation
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string gcode { get; set; }
        public int? keyword_id { get; set; }
        public EnumKeywordType? keyword_type { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
    }
}
