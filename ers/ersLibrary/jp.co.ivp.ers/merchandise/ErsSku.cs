﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsSku
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual string scode { get; set; }
        public virtual string gcode { get; set; }
        public virtual string jancode { get; set; }
        public virtual string sname { get; set; }
        public virtual string m_sname { get; set; }
        public virtual string cts_sname { get; set; }
        public virtual string shipping_sname { get; set; }
        public virtual int? point { get; set; }
        public virtual EnumSoldoutFlg? soldout_flg { get; set; }
        public virtual string attribute1 { get; set; }
        public virtual string attribute2 { get; set; }
        public virtual int? disp_order { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual string mixed_group_code { get; set; }
        public virtual int? max_purchase_count { get; set; }
        public virtual DateTime? point_campaign_from { get; set; }
        public virtual DateTime? point_campaign_to { get; set; }
        public virtual int? campaign_point { get; set; }
        public virtual EnumActive? active { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual string supplier_code { get; set; }
        public virtual EnumWhOrderType? wh_order_type { get; set; }
        public virtual int? stock_alert_amount { get; set; }
        public virtual EnumOnOff h_mall_flg { get; set; }
        public virtual string maker_scode { get; set; }
        public virtual string keyword { get; set; }
        public virtual DateTime? keyword_updated { get; set; }
        public virtual int? site_id { get; set; }
    }
}
