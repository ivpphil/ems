﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsGroup
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual string gcode { get; set; }
        public virtual string gname { get; set; }
        public virtual string m_gname { get; set; }
        public virtual DateTime? date_from { get; set; }
        public virtual DateTime? date_to { get; set; }
        public virtual EnumSalePatternType? s_sale_ptn { get; set; }
        public virtual EnumStockFlg? stock_flg { get; set; }
        public virtual int? stock_set1 { get; set; }
        public virtual int? stock_set2 { get; set; }
        public virtual int[] cate1 { get; set; }
        public virtual int[] cate2 { get; set; }
        public virtual int[] cate3 { get; set; }
        public virtual int[] cate4 { get; set; }
        public virtual int[] cate5 { get; set; }
        public virtual string recommend1 { get; set; }
        public virtual string recommend2 { get; set; }
        public virtual string recommend3 { get; set; }
        public virtual string recommend4 { get; set; }
        public virtual string recommend5 { get; set; }
        public virtual string metatitle { get; set; }
        public virtual string metadescription { get; set; }
        public virtual string metawords { get; set; }
        public virtual string metaalt { get; set; }
        public virtual string link_url { get; set; }
        public virtual string description { get; set; }
        public virtual string m_description { get; set; }
        public virtual string description_for_list { get; set; }
        public virtual EnumActive? active { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual EnumDisp_list_flg? disp_list_flg { get; set; }
        public virtual int? price_for_search { get; set; }
        public virtual int? sort { get; set; }
        public virtual string disp_send_ptn { get; set; }
        public virtual EnumCarriageCostType? carriage_cost_type { get; set; }
        public virtual EnumPluralOrderType? plural_order_type { get; set; }
        public virtual EnumSetFlg? set_flg { get; set; }
        public virtual int? doc_bundling_flg { get; set; }
        public virtual EnumDelvMethod? deliv_method { get; set; }
        public virtual string keyword { get; set; }
        public virtual string[] disp_keyword { get; set; }
        public virtual DateTime? keyword_updated { get; set; }
        public virtual int[] site_id { get; set; }
    }
}
