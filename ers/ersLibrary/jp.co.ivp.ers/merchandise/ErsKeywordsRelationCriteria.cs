﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsKeywordsRelationCriteria
        : Criteria
    {
        public string gcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("keywords_relation_t.gcode", value, Operation.EQUAL));
            }
        }

        public EnumKeywordType? keyword_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("keywords_relation_t.keyword_type", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("keywords_relation_t.active", value, Operation.EQUAL));
            }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("keywords_relation_t.id", orderBy);
        }
    }
}
