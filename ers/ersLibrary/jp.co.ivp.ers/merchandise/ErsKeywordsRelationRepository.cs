﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsKeywordsRelationRepository
        : ErsRepository<ErsKeywordsRelation>
    {
        public ErsKeywordsRelationRepository()
            : base("keywords_relation_t")
        {
        }
    }
}
