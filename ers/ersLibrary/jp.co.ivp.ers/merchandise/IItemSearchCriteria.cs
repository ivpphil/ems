﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise
{
    public interface IItemSearchCriteria
        : ICriteria
    {
        string[] ignore_gcode { set; }

        string gcode { set; }

        string jancode { set; }

        string scode { set; }

        string sname_and_gname { set; }

        int? cate1 { set; }

        int? cate2 { set; }

        int? cate3 { set; }

        int? cate4 { set; }

        int? cate5 { set; }
    }
}
