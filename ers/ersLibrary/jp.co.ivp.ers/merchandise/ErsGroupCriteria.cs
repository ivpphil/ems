﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsGroupCriteria
        : Criteria, IItemSearchCriteria
    {
        Lazy<ErsSkuCriteria> _skuCriteria = new Lazy<ErsSkuCriteria>(() => ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria());
        ErsSkuCriteria skuCriteria { get { return _skuCriteria.Value; } }

        Lazy<ErsPriceCriteria> _priceCriteria = new Lazy<ErsPriceCriteria>(() => ErsFactory.ersMerchandiseFactory.GetErsPriceCriteria());
        ErsPriceCriteria priceCriteria { get { return _priceCriteria.Value; } }

        public string gcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.gcode", value, Operation.EQUAL));
            }
        }

        public string scode
        {
            set
            {
                skuCriteria.scode = value;
            }
        }

        /// <summary>
        /// search condition for ignore_gcode ( not in )
        /// </summary>
        public virtual string[] ignore_gcode
        {
            set
            {
                this.Add(Criteria.GetNotInClauseCriterion("g_master_t.gcode", value));
            }
        }

        /// <summary>
        /// search condition for jancode
        /// </summary>
        public virtual string jancode
        {
            set
            {
                skuCriteria.jancode = value;
            }
        }

        /// <summary>
        /// search condition for sname_and_gname (or)
        /// </summary>
        public virtual string sname_and_gname
        {
            set
            {
                skuCriteria.sname_and_gname = value;
            }
        }

        /// <summary>
        /// search condition for cate1 ( any )
        /// </summary>
        public virtual int? cate1
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.cate1", value, Operation.ANY_EQUAL));
            }
        }

        /// <summary>
        /// search condition for cate2 ( any )
        /// </summary>
        public virtual int? cate2
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.cate2", value, Operation.ANY_EQUAL));
            }
        }

        /// <summary>
        /// search condition for cate3 ( any )
        /// </summary>
        public virtual int? cate3
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.cate3", value, Operation.ANY_EQUAL));
            }
        }

        /// <summary>
        /// search condition for cate4 ( any )
        /// </summary>
        public virtual int? cate4
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.cate4", value, Operation.ANY_EQUAL));
            }
        }

        /// <summary>
        /// search condition for cate5 ( any )
        /// </summary>        
        public virtual int? cate5
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.cate5", value, Operation.ANY_EQUAL));
            }
        }

        /// <summary>
        /// set order by g_master_t.gcode
        /// </summary>
        public virtual void SetOrderByGcode(OrderBy orderBy)
        {
            this.AddOrderBy("g_master_t.gcode", orderBy);
        }

        /// <summary>
        /// set order by g_master_t.gcode
        /// </summary>
        public virtual void SetOrderByS_Gcode(OrderBy orderBy)
        {
            this.AddOrderBy("g_master_t.gcode", orderBy);
        }

        public EnumActive? s_active
        {
            set
            {
                skuCriteria.s_active = value;
            }
        }

        public void SetOrderByPrice_for_search(OrderBy orderBy)
        {
            this.AddOrderBy("g_master_t.price_for_search", orderBy);
        }

        /// <summary>
        /// set order by g_master_t.sort
        /// </summary>
        public virtual void SetOrderBySort(OrderBy orderBy)
        {
            this.AddOrderBy("g_master_t.sort", orderBy);
        }

        /// <summary>
        /// set order by s_master_t.disp_order
        /// </summary>
        public void SetOrderByDisp_order(OrderBy orderBy)
        {
            this.AddOrderBy("s_master_t.disp_order", orderBy);
        }

        /// <summary>
        /// set order by g_master_t.date_from
        /// </summary>
        public virtual void SetOrderByDateFrom(OrderBy orderBy)
        {
            this.AddOrderBy("g_master_t.date_from", orderBy);
        }

        /// <summary>
        /// search condition for scode_and_gcode (or)
        /// </summary>
        public virtual string scode_and_gcode
        {
            set
            {
                skuCriteria.scode_and_gcode = value;
            }
        }

        public virtual string keyword
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.keyword", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for keywords ( like )
        /// </summary>      
        public virtual string keyword_prefix_search
        {
            set
            {
                skuCriteria.keyword_prefix_search = value;
            }
        }

        /// <summary>
        /// search condition for price_lower ( greater than equal)
        /// </summary>  
        public virtual long price_lower
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.price_for_search", value, Operation.GREATER_EQUAL));
            }
        }

        /// <summary>
        /// search condition for price_lower_less ( less than equal)
        /// </summary>  
        public virtual long price_lower_less
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.price_for_search", value, Operation.LESS_EQUAL));
            }
        }

        /// <summary>
        /// search condition for price_upper ( less than equal)
        /// </summary>  
        public virtual long price_upper
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.price_for_search_max", value, Operation.LESS_EQUAL));
            }
        }

        /// <summary>
        /// search condition for disp_list_flg
        /// </summary>
        public virtual EnumDisp_list_flg? disp_list_flg
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.disp_list_flg", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// search condition for g_active
        /// </summary>  
        public virtual EnumActive? g_active
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.active", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 同梱商品以外
        /// </summary>
        public virtual EnumDocBundlingFlg? doc_bundling_flg_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.doc_bundling_flg", value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// search condition for gcode_in
        /// </summary>
        public virtual IEnumerable<string> gcode_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("g_master_t.gcode", value));
            }
        }

        /// <summary>
        /// search condition for sname_and_gname (or)
        /// </summary>
        public virtual string recommend_JoinWithOr
        {
            set
            {
                this.Add(Criteria.JoinWithOR(
                        new[] {
                            Criteria.GetCriterion("g_master_t.recommend1", value, Operation.EQUAL),
                            Criteria.GetCriterion("g_master_t.recommend2", value, Operation.EQUAL),
                            Criteria.GetCriterion("g_master_t.recommend3", value, Operation.EQUAL),
                            Criteria.GetCriterion("g_master_t.recommend4", value, Operation.EQUAL),
                            Criteria.GetCriterion("g_master_t.recommend5", value, Operation.EQUAL)
                        }));
            }
        }

        public IEnumerable<string> scode_in
        {
            set
            {
                skuCriteria.scode_in = value;
            }
        }

        public void SetActiveOnly(DateTime? currentDateTime)
        {
            this.g_active = EnumActive.Active;
            this.s_active = EnumActive.Active;
            this.SetDateFromTo(currentDateTime);
        }

        public DateTime? DateTo
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.date_to", value, Operation.GREATER_THAN));
            }
        }


        public void SetDateFromTo(DateTime? currentDateTime)
        {
            this.Add(Criteria.GetBetweenCriterion(ColumnName("g_master_t.date_from"), ColumnName("g_master_t.date_to"), currentDateTime));
        }

        public EnumCarriageCostType? carriage_cost_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.carriage_cost_type", value, Operation.EQUAL));
            }
        }

        public EnumPluralOrderType? plural_order_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.plural_order_type", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 在庫なしの商品を含まない
        /// </summary>
        public void SetExcludeOutOfStock()
        {
            skuCriteria.SetExcludeOutOfStock();
        }

        /// <summary>
        /// 同梱商品以外
        /// </summary>
        public virtual EnumDocBundlingFlg? doc_bundling_flg
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.doc_bundling_flg", value, Operation.EQUAL));
            }
        }

        public EnumPriceKbn? price_kbn
        {
            set
            {
                priceCriteria.price_kbn = value;
            }
        }

        /// <summary>
        /// Check keyword update status
        /// </summary>
        public void ChangedIn()
        {
            skuCriteria.ChangedIn();
        }

        /// <summary>
        /// filter records using s_master_t.scode is not null
        /// </summary>
        public string scode_not_equal
        {
            set
            {
                skuCriteria.scode_not_equal = value;
            }
        }

        /// <summary>
        /// Sets the criteria for g_master_t.site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("g_master_t.site_id", value, Operation.ANY_EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("g_master_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.ANY_EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

        public EnumSetFlg? set_flg
        {
            set
            {
                this.Add(Criteria.GetCriterion("g_master_t.set_flg", value, Operation.EQUAL));
            }
        }

        public virtual string gcodeLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("g_master_t.gcode", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual void SetPriceSearch(long? price_min, long? price_max)
        {
            var list = new List<CriterionBase>();
            if(!price_min.HasValue && !price_max.HasValue)
            {
                return;
            }

            this.AddSearchPriceList(list, price_min);
            this.AddSearchPriceList(list, price_max);

            this.Add(JoinWithOR(list));
        }

        private void AddSearchPriceList(IList<CriterionBase> list, long? price_value)
        {
            if (price_value.HasValue)
            {
                var criteria = GetBetweenCriterion(ColumnName("price_for_search"), ColumnName("price_for_search_max"), price_value);
                list.Add(criteria);
            }
        }

        public override IEnumerable<CriterionBase> GetCriterionList(IEnumerable<CriterionBase> value)
        {
            foreach(var criterion in base.GetCriterionList(value))
            {
                yield return criterion;
            }

            if (_skuCriteria.IsValueCreated)
            {
                var strWhere = skuCriteria.GetWhere();
                yield return GetUniversalCriterion("EXISTS(SELECT * FROM s_master_t WHERE gcode = g_master_t.gcode AND " + strWhere + ")", skuCriteria);
            }

            if (_priceCriteria.IsValueCreated)
            {
                var strWhere = priceCriteria.GetWhere();
                yield return GetUniversalCriterion("EXISTS(SELECT * FROM price_t WHERE gcode = g_master_t.gcode AND " + strWhere + ")", priceCriteria);
            }
        }
    }
}
