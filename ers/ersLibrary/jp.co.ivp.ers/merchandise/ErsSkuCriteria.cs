﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.stock;

namespace jp.co.ivp.ers.merchandise
{
    public class ErsSkuCriteria
        : Criteria, IItemSearchCriteria
    {
        Lazy<ErsGroupCriteria> _groupCriteria = new Lazy<ErsGroupCriteria>(() => ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria());
        ErsGroupCriteria groupCriteria { get { return _groupCriteria.Value; } }

        public string gcode
        {
            set { this.Add(Criteria.GetCriterion("s_master_t.gcode", value, Operation.EQUAL)); }
        }

        public string gcode_not_equals
        {
            set { this.Add(Criteria.GetCriterion("s_master_t.gcode", value, Operation.NOT_EQUAL)); }
        }

        public virtual string scode
        {
            set { this.Add(Criteria.GetCriterion("s_master_t.scode", value, Operation.EQUAL)); }
        }

        public string scode_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.scode", value, Criteria.Operation.NOT_EQUAL));
            }
        }

        public string scode_like
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("s_master_t.scode", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public string jancode
        {
            set { this.Add(Criteria.GetCriterion("s_master_t.jancode", value, Operation.EQUAL)); }
        }

        public void SetOrderByScode(OrderBy orderBy)
        {
            this.AddOrderBy("s_master_t.scode", orderBy);
        }

        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("s_master_t.id", orderBy);
        }

        public EnumSoldoutFlg soldout_flg
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.soldout_flg", value, Operation.EQUAL));
            }
        }

        public int greater_stock_alert
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.stock_alert_amount", value, Operation.GREATER_THAN));
            }
        }

        /// <summary>
        /// 商品コードIN [Scode IN]
        /// </summary>
        public virtual IEnumerable<string> scode_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("s_master_t.scode", value));
            }
        }

        public virtual string keyword
        {
            set { this.Add(Criteria.GetCriterion("s_master_t.keyword", value, Operation.EQUAL)); }
        }

        public virtual int max_purchase_count_not_eq
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.max_purchase_count", value, Operation.NOT_EQUAL));
            }
        }

        public void HasSetChild(params string[] child_scode)
        {
            this.Add(Criteria.GetUniversalCriterion("EXISTS (SELECT * FROM set_master_t WHERE :scode = scode AND parent_scode = s_master_t.scode)", new Dictionary<string, object>() { { "scode", child_scode } }));
        }

        public EnumSetFlg? set_flg
        {
            set
            {
                groupCriteria.set_flg = value;
            }
        }
        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("s_master_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("s_master_t.site_id",(int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

        /// <summary>
        /// Sets the criteria for g_master_t.site_id
        /// </summary>
        public virtual int? g_site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("g_master_t.site_id", value, Operation.ANY_EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("g_master_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.ANY_EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }



        public void SetActiveOnly(DateTime? currentDateTime)
        {
            this.g_active = EnumActive.Active;
            this.s_active = EnumActive.Active;
            this.SetDateFromTo(currentDateTime);
        }

        /// <summary>
        /// search condition for g_active
        /// </summary>  
        public virtual EnumActive? g_active
        {
            set
            {
                groupCriteria.g_active = value;
            }
        }

        public EnumActive? s_active
        {
            set
            {
                this.Add(Criteria.GetCriterion("s_master_t.active", value, Operation.EQUAL));
            }
        }

        public void SetDateFromTo(DateTime? currentDateTime)
        {
            groupCriteria.SetDateFromTo(currentDateTime);
        }

        /// <summary>
        /// search condition for ignore_gcode ( not in )
        /// </summary>
        public virtual string[] ignore_gcode
        {
            set
            {
                groupCriteria.ignore_gcode = value;
            }
        }

        public void SetOrderByDisp_order(OrderBy orderBy)
        {
            this.AddOrderBy("s_master_t.disp_order", orderBy);
        }

        /// <summary>
        /// search condition for cate1 ( any )
        /// </summary>
        public virtual int? cate1
        {
            set
            {
                groupCriteria.cate1 = value;
            }
        }

        /// <summary>
        /// search condition for cate2 ( any )
        /// </summary>
        public virtual int? cate2
        {
            set
            {
                groupCriteria.cate2 = value;
            }
        }

        /// <summary>
        /// search condition for cate3 ( any )
        /// </summary>
        public virtual int? cate3
        {
            set
            {
                groupCriteria.cate3 = value;
            }
        }

        /// <summary>
        /// search condition for cate4 ( any )
        /// </summary>
        public virtual int? cate4
        {
            set
            {
                groupCriteria.cate4 = value;
            }
        }

        /// <summary>
        /// search condition for cate5 ( any )
        /// </summary>        
        public virtual int? cate5
        {
            set
            {
                groupCriteria.cate5 = value;
            }
        }

        public virtual string gcodeLikePrefix
        {
            set
            {
                groupCriteria.gcodeLikePrefix = value;
            }
        }

        /// <summary>
        /// search condition for scode_and_gcode (or)
        /// </summary>
        public virtual string scode_and_gcode
        {
            set
            {
                this.Add(Criteria.JoinWithOR(
                        new[] {
                            Criteria.GetCriterion("s_master_t.scode", value, Operation.EQUAL),
                            Criteria.GetCriterion("g_master_t.gcode", value, Operation.EQUAL)
                        }));
            }
        }

        /// <summary>
        /// search condition for sname_and_gname (or)
        /// </summary>
        public virtual string sname_and_gname
        {
            set
            {
                this.Add(Criteria.JoinWithOR(
                        new[] {
                            Criteria.GetLikeClauseCriterion("s_master_t.sname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH),
                            Criteria.GetLikeClauseCriterion("g_master_t.gname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH)
                        }));
            }
        }

        /// <summary>
        /// set order by g_master_t.sort
        /// </summary>
        public virtual void SetOrderBySort(OrderBy orderBy)
        {
            this.AddOrderBy("g_master_t.sort", orderBy);
        }

        /// <summary>
        /// set order by g_master_t.date_from
        /// </summary>
        public virtual void SetOrderByDateFrom(OrderBy orderBy)
        {
            this.AddOrderBy("g_master_t.date_from", orderBy);
        }

        /// <summary>
        /// set order by g_master_t.gcode
        /// </summary>
        public virtual void SetOrderByGcode(OrderBy orderBy)
        {
            this.AddOrderBy("g_master_t.gcode", orderBy);
        }

        /// <summary>
        /// 同梱商品以外
        /// </summary>
        public virtual EnumDocBundlingFlg? doc_bundling_flg
        {
            set
            {
                groupCriteria.doc_bundling_flg = value;
            }
        }

        /// <summary>
        /// search condition for keywords ( like )
        /// </summary>      
        public virtual string keyword_prefix_search
        {
            set
            {
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsKeywordsCriteria();

                var keywordArray = value.Split(new[] { " ", "　" }, StringSplitOptions.RemoveEmptyEntries);

                var criteriaList = new List<Criteria>();

                foreach (var keyword in keywordArray)
                {
                    criteriaList.Add(Criteria.JoinWithOR(new[]{
                        Criteria.GetLikeClauseCriterion("g_master_t.keyword", keyword, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH),
                        Criteria.GetLikeClauseCriterion("s_master_t.keyword", keyword, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH),
                    }));
                }

                this.Add(Criteria.JoinWithAnd(criteriaList));
            }
        }

        /// <summary>
        /// 在庫なしの商品を含まない
        /// </summary>
        public void SetExcludeOutOfStock()
        {
            var strSQL = "EXISTS (SELECT * FROM stock_t WHERE s_master_t.scode = stock_t.scode AND (s_master_t.soldout_flg = :s_master_t.soldout_flg OR stock_t.stock > 0))";
            var enable_soldout_criteria = Criteria.GetUniversalCriterion(strSQL,
                new Dictionary<string, object>() { { "s_master_t.soldout_flg", EnumSoldoutFlg.DisableSoldout } });

            var disable_soldout_criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            disable_soldout_criteria.soldout_flg = EnumSoldoutFlg.DisableSoldout;

            this.Add(Criteria.JoinWithOR(new[] { enable_soldout_criteria, disable_soldout_criteria }));
        }

        /// <summary>
        /// Check keyword update status
        /// </summary>
        public void ChangedIn()
        {
            var sql = "(g_master_t.keyword_updated IS null "
                 + "OR  g_master_t.utime - INTERVAL '1 minute'  > g_master_t.keyword_updated "
                 + "OR s_master_t.keyword_updated IS null "
                 + "OR  s_master_t.utime -  INTERVAL '1 minute' > s_master_t.keyword_updated) ";
            this.Add(Criteria.GetUniversalCriterion(sql));
        }

        public override IEnumerable<CriterionBase> GetCriterionList(IEnumerable<CriterionBase> value)
        {
            foreach (var criterion in base.GetCriterionList(value))
            {
                yield return criterion;
            }

            if (_groupCriteria.IsValueCreated)
            {
                var strWhere = groupCriteria.GetWhere();
                yield return GetUniversalCriterion("EXISTS(SELECT * FROM g_master_t WHERE gcode = s_master_t.gcode AND " + strWhere + ")", groupCriteria);
            }
        }

        public void SetHasActiveMallFlg()
        {
            var sql = "EXISTS(SELECT * FROM mall_s_master_t WHERE scode = s_master_t.scode AND mall_flg = :mall_flg)";
            var values = new Dictionary<string, object>() {
                { "mall_flg", EnumOnOff.On }};
            this.Add(Criteria.GetUniversalCriterion(sql, values));
        }
    }
}
