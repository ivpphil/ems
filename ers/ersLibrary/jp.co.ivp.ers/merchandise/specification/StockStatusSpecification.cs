﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise.specification
{
    /// <summary>
    /// The specification class that determines what is the stock status of an item
    /// </summary>
    public class StockStatusSpecification
    {
        protected internal StockStatusSpecification() { }

        /// <summary>
        /// Get stock status of the merchandise.
        /// </summary>
        /// <param name="em">values from ErsMerchandise</param>
        /// <returns></returns>
        public virtual EnumStockStatus GetStockStatusOfMerchandise(ErsMerchandise merchandise)
        {
            if (ErsFactory.ersMerchandiseFactory.GetOutOfStockSpecification().IsSatisfiedBy(merchandise.set_flg, merchandise.scode, merchandise.stock, merchandise.soldout_flg))
            {
                return EnumStockStatus.OUT_OF_STOCK;
            }

            var stock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetObtainMerchandiseStockStgy().GetStock(merchandise.scode, merchandise.stock);

            if (merchandise.stock_set1 >= stock)
            {
                return EnumStockStatus.LESS_THAN_STOCK_SET1;
            }
            else if (merchandise.stock_set2 < stock)
            {
                return EnumStockStatus.MORE_THAN_STOCK_SET2;
            }
            else
            {
                return EnumStockStatus.INSIDE_STOCK_SET;
            }
        }
    }
}