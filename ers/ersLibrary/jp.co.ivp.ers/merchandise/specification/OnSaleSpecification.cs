﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jp.co.ivp.ers.merchandise.specification
{
    /// <summary>
    /// The specification class that determines whether the item is in sale term.
    /// </summary>
    public class OnSaleSpecification
    {
        protected internal OnSaleSpecification() { }

        /// <summary>
        /// 販売期間ステータスを取得
        /// </summary>
        /// <param name="em">values from ErsMerchandise</param>
        /// <returns></returns>
        public virtual EnumDatePeriod GetDatePeriod(DateTime? date_from, DateTime? date_to, DateTime time)
        {
            if (date_from > time)
            {
                return EnumDatePeriod.BEFORE;
            }
            else if (date_to < time)
            {
                return EnumDatePeriod.AFTER;
            }
            else
            {
                return EnumDatePeriod.INSIDE;
            }
        }
    }

}