﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise.specification
{
    public class SearchSkuBaseItemListSpec
         : SearchSpecificationBase
    {
        private string orderByValue { get; set; }
        private int? member_rank { get; set; }

        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return base.GetSearchData(criteria);
        }

        public List<Dictionary<string, object>> GetSearchData(Criteria criteria, int? member_rank)
        {
            this.member_rank = member_rank;
            this.orderByValue = criteria.GetDistinctOn("g_master_t", "s_master_t");
            if (string.IsNullOrEmpty(this.orderByValue))
            {
                criteria.InsertOrderByToFirst("s_master_t.scode", Criteria.OrderBy.ORDER_BY_ASC);                
                this.orderByValue = "s_master_t.scode";
            }
            return base.GetSearchData(criteria);
        }

        protected override string GetSearchDataSql()
        {
            var memberRank = (this.member_rank.HasValue) ? this.member_rank.ToString() : "NULL";

            return "SELECT DISTINCT ON (" + orderByValue + ") *, "
                + "s_master_t.intime AS s_intime, s_master_t.utime AS s_utime, s_master_t.active AS s_active, s_master_t.keyword AS s_keyword, "
                + "(SELECT ARRAY(SELECT keyword FROM keywords_t INNER JOIN keywords_relation_t ON keywords_t.id = keywords_relation_t.keyword_id WHERE keywords_relation_t.keyword_type = " + (int)EnumKeywordType.Group + " AND gcode = g_master_t.gcode)) AS keyword, g_master_t.site_id "
                + "FROM g_master_t "
                + "LEFT OUTER JOIN s_master_t ON g_master_t.gcode = s_master_t.gcode "
                + "LEFT OUTER JOIN stock_t ON s_master_t.scode = stock_t.scode "
                + "LEFT JOIN( "
                + "	    SELECT DISTINCT ON (scode) scode, price, price2, cost_price "
                + "     FROM price_t "
                + "     WHERE (price_kbn = " + (int)EnumPriceKbn.NORMAL + " OR (price_kbn = " + (int)EnumPriceKbn.RANK_NORMAL + " AND member_rank = " + memberRank + ")) "
                + "     ORDER BY scode, COALESCE(member_rank, -1) DESC "
                + ") AS price_t ON s_master_t.scode = price_t.scode "
                + "LEFT JOIN( "
                + "	    SELECT DISTINCT ON (scode) scode, price AS non_member_price "
                + "     FROM price_t "
                + "     WHERE (price_kbn = " + (int)EnumPriceKbn.NORMAL + ") "
                + "     ORDER BY scode, COALESCE(member_rank, -1) DESC "
                + ") AS non_member_price_t ON s_master_t.scode = non_member_price_t.scode "
                + "LEFT JOIN( "
                + "	    SELECT DISTINCT ON (scode) scode, price AS regular_first_price "
                + "     FROM price_t "
                + "     WHERE (price_kbn = " + (int)EnumPriceKbn.REGULAR_FIRST + " OR (price_kbn = " + (int)EnumPriceKbn.RANK_REGULAR_FIRST + " AND member_rank = " + memberRank + ")) "
                + "     ORDER BY scode, COALESCE(member_rank, -1) DESC "
                + ") AS regular_first_price ON s_master_t.scode = regular_first_price.scode "
                + "LEFT JOIN( "
                + "	    SELECT DISTINCT ON (scode) scode, price AS non_member_regular_first_price "
                + "     FROM price_t "
                + "     WHERE (price_kbn = " + (int)EnumPriceKbn.REGULAR_FIRST + ") "
                + "     ORDER BY scode, COALESCE(member_rank, -1) DESC "
                + ") AS non_member_regular_first_price ON s_master_t.scode = non_member_regular_first_price.scode "
                + "LEFT JOIN( "
                + "	    SELECT DISTINCT ON (scode) scode, price AS regular_price "
                + "     FROM price_t "
                + "     WHERE (price_kbn = " + (int)EnumPriceKbn.REGULAR + " OR (price_kbn = " + (int)EnumPriceKbn.RANK_REGULAR + " AND member_rank = " + memberRank + ")) "
                + "     ORDER BY scode, COALESCE(member_rank, -1) DESC "
                + ") AS regular_price ON s_master_t.scode = regular_price.scode "
                + "LEFT JOIN( "
                + "	    SELECT DISTINCT ON (scode) scode, price AS non_member_regular_price "
                + "     FROM price_t "
                + "     WHERE (price_kbn = " + (int)EnumPriceKbn.REGULAR + ") "
                + "     ORDER BY scode, COALESCE(member_rank, -1) DESC "
                + ") AS non_member_regular_price_t ON s_master_t.scode = non_member_regular_price_t.scode "
                + "LEFT JOIN( "
                + "	    SELECT scode, price AS sale_price, date_from AS p_date_from, date_to AS p_date_to "
                + "     FROM price_t "
                + "     WHERE current_timestamp BETWEEN date_from AND date_to AND price_kbn = " + (int)EnumPriceKbn.SALE + " "
                + ") AS sale_price ON s_master_t.scode = sale_price.scode "
                + "WHERE true ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(DISTINCT s_master_t.id) AS " + countColumnAlias + " "
                + "FROM g_master_t "
                + "INNER JOIN s_master_t ON g_master_t.gcode = s_master_t.gcode "
                + "LEFT JOIN price_t ON s_master_t.scode = price_t.scode "
                + "INNER JOIN stock_t ON s_master_t.scode = stock_t.scode "
                +"WHERE true ";
        }
    }
}
