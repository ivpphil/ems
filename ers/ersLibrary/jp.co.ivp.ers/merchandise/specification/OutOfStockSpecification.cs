﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.Data;

namespace jp.co.ivp.ers.merchandise.specification
{
    /// <summary>
    /// The specification class that determines whether the item is out of stock.
    /// </summary>
    public class OutOfStockSpecification
    {
        protected internal OutOfStockSpecification() { }

        /// <summary>
        /// checking for satisfied
        /// </summary>
        /// <param name="merchandise">values from ErsMerchandise</param>
        /// <returns></returns>
        public virtual bool IsSatisfiedBy(EnumSetFlg? set_flg, string scode, int? stock, EnumSoldoutFlg? soldout_flg)
        {
            if (set_flg != EnumSetFlg.IsSet)
            {
                return CheckNotSet(scode, stock, soldout_flg);
            }
            else
            {
                return CheckSet(scode);
            }
        }

        private bool CheckNotSet(string scode, int? stock, EnumSoldoutFlg? soldout_flg)
        {
            var currentStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetObtainMerchandiseStockStgy().GetStock(scode, stock);

            return (currentStock <= 0 && soldout_flg == EnumSoldoutFlg.EnableSoldout);
        }

        private bool CheckSet(string scode)
        {
            //子商品リスト取得
            var DecreaseSetMerchandiseList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(scode);

            if (DecreaseSetMerchandiseList.Count == 0)
            {
                return true;
            }

            //子商品分在庫減算
            foreach (ErsSetMerchandise item in DecreaseSetMerchandiseList)
            {
                var merchandise = ErsFactory.ersMerchandiseFactory.GetActiveErsMerchandiseWithScode(item.scode, null);

                if (merchandise == null)
                {
                    return true;
                }
                else
                {
                    if (this.CheckNotSet(merchandise.scode, merchandise.stock, merchandise.soldout_flg))
                    {
                        return true;
                    }

                }
            }

            return false;
        }
    }
}