﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.specification
{
    public class SearchPriceGroupBaseItemListSpec
        : SearchPriceSkuBaseItemListSpec
    {
        protected override void SetOrderByValue(Criteria criteria)
        {
            this.orderByValue = criteria.GetDistinctOn("g_master_t");
            if (string.IsNullOrEmpty(this.orderByValue))
            {
                criteria.InsertOrderByToFirst("g_master_t.gcode", Criteria.OrderBy.ORDER_BY_ASC);
                this.orderByValue = "g_master_t.gcode";
            }
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(DISTINCT g_master_t.id) AS " + countColumnAlias + " "
                + "FROM price_t "
                + "INNER JOIN s_master_t ON price_t.scode = s_master_t.scode "
                + "INNER JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode "
                +"WHERE true ";
        }
    }
}
