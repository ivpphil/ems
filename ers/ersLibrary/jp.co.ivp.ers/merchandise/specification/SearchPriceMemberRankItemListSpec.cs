﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise.specification
{
    public class SearchPriceMemberRankItemListSpec
         : SearchSpecificationBase
    {
        private string orderByValue { get; set; }

        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            this.orderByValue = criteria.GetDistinctOn("price_t");
            if (string.IsNullOrEmpty(this.orderByValue))
            {
                criteria.InsertOrderByToFirst("price_t.scode", Criteria.OrderBy.ORDER_BY_ASC);
                criteria.InsertOrderByToFirst("price_t.member_rank", Criteria.OrderBy.ORDER_BY_ASC);
                this.orderByValue = "price_t.scode, price_t.member_rank";
            }

            return base.GetSearchData(criteria);
        }

        protected override string GetSearchDataSql()
        {
            return "SELECT DISTINCT ON (" + orderByValue + ") * "
                + "FROM member_rank_setup_t "
                + "LEFT JOIN( "
                + "	SELECT scode, member_rank FROM price_t GROUP BY scode, member_rank "
                + ") AS price_t ON member_rank_setup_t.rank = price_t.member_rank "
                + "LEFT JOIN( "
                + "	SELECT scode, member_rank, price FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.RANK_NORMAL + " "
                + ") AS normal_price_t ON price_t.scode = normal_price_t.scode AND member_rank_setup_t.rank = normal_price_t.member_rank "
                + "LEFT JOIN( "
                + "	SELECT scode, member_rank, price AS regular_first_price FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.RANK_REGULAR_FIRST + " "
                + ") AS regular_first_price ON price_t.scode = regular_first_price.scode AND member_rank_setup_t.rank = regular_first_price.member_rank "
                + "LEFT JOIN( "
                + "	SELECT scode, member_rank, price AS regular_price FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.RANK_REGULAR + " "
                + ") AS regular_price ON price_t.scode = regular_price.scode AND member_rank_setup_t.rank = regular_price.member_rank "
                + "LEFT JOIN( "
                + "	SELECT scode, price AS sale_price, date_from AS p_date_from, date_to AS p_date_to FROM price_t WHERE current_timestamp BETWEEN date_from AND date_to AND price_kbn = " + (int)EnumPriceKbn.SALE + " "
                + ") AS sale_price ON price_t.scode = sale_price.scode "
                + "WHERE true ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(price_t.id) AS " + countColumnAlias + " "
                + "FROM member_rank_setup_t "
                + "LEFT JOIN( "
                + "	SELECT scode, member_rank FROM price_t GROUP BY scode, member_rank "
                + ") AS price_t ON member_rank_setup_t.rank = price_t.member_rank ";
        }
    }
}
