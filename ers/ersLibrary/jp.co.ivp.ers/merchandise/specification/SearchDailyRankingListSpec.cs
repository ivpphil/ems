﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise.specification
{
    public class SearchDailyRankingListSpec
         : SearchSpecificationBase
    {
        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            criteria.AddOrderBy("ranking_t.sum_result", Criteria.OrderBy.ORDER_BY_DESC);

            return base.GetSearchData(criteria);
        }

        protected override string GetSearchDataSql()
        {
            return "SELECT ranking_t.gcode FROM ranking_t INNER JOIN g_master_t ON g_master_t.gcode = ranking_t.gcode";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(ranking_t.id) AS count FROM ranking_t INNER JOIN g_master_t ON g_master_t.gcode = ranking_t.gcode";
        }
    }
}
