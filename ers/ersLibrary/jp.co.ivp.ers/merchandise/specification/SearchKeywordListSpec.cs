﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.specification
{
    public class SearchKeywordListSpec
         : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return "SELECT keywords_t.* "
                + "FROM keywords_t "
                + "INNER JOIN keywords_relation_t ON keywords_t.id = keywords_relation_t.keyword_id ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(keywords_relation_t.id) AS " + countColumnAlias + " "
                + "FROM keywords_t "
                + "INNER JOIN keywords_relation_t ON keywords_t.id = keywords_relation_t.keyword_id ";
        }
    }
}
