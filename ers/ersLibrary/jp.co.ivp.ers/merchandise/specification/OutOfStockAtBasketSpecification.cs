﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.basket;

namespace jp.co.ivp.ers.merchandise.specification
{
    /// <summary>
    /// The specification class that determines whether the item is out of stock at the basket.
    /// </summary>
    public class OutOfStockAtBasketSpecification
    {
        protected internal OutOfStockAtBasketSpecification() { }

        /// <summary>
        /// check is it satisfied
        /// </summary>
        /// <param name="merchandise">values from ErsMerchandiseInBasket</param>
        /// <returns></returns>
        public virtual bool IsSatisfiedBy(string ransu, string scode, int? amount, int? stock, EnumSoldoutFlg? soldout_flg, EnumSetFlg? set_flg, bool IsOrderUpdate)
        {
            if (set_flg == EnumSetFlg.IsSet)
            {
                return this.CheckSet(ransu, scode, amount, IsOrderUpdate);
            }
            else
            {
                return this.CheckNotSet(scode, stock, soldout_flg, IsOrderUpdate);
            }
        }

        private bool CheckNotSet(string scode, int? amount, EnumSoldoutFlg? soldout_flg, bool IsOrderUpdate)
        {
            var stock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetObtainMerchandiseStockStgy().GetStock(scode, amount);
            return (stock < 0 && soldout_flg != EnumSoldoutFlg.DisableSoldout);
        }

        private bool CheckSet(string ransu, string scode, int? amount, bool IsOrderUpdate)
        {
            //子商品リスト取得
            var DecreaseSetMerchandiseList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(scode);

            if (DecreaseSetMerchandiseList.Count == 0)
            {
                if (IsOrderUpdate)
                {
                    //データ更新時は、子供データがなくても問題ないこととする
                    return false;
                }
                else
                {
                    return true;
                }
            }

            var calcService = ErsFactory.ersOrderFactory.GetErsCalcService();

            foreach (ErsSetMerchandise item in DecreaseSetMerchandiseList)
            {
                //商品情報取得
                var setMerchandise = ErsFactory.ersBasketFactory.GetErsBaskRecordWithScode(ransu, item.scode, null);

                if (setMerchandise == null)
                {
                    continue;
                }

                calcService.calcBaskRecord(setMerchandise, item.amount.Value * amount.Value);

                if (this.CheckNotSet(setMerchandise.scode, setMerchandise.stock, setMerchandise.soldout_flg, IsOrderUpdate))
                {
                    return true;
                }
            }
            return false;
        }

    }
}