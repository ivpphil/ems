﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.merchandise.specification
{
    public class ChecksActivenessOfMerchandiseSpec
    {
        public bool Check(ErsMerchandise merchandise)
        {
            return merchandise.active == EnumActive.Active
                && merchandise.s_active == EnumActive.Active
                && merchandise.date_from <= DateTime.Now
                && merchandise.date_to >= DateTime.Now;
        }
    }
}
