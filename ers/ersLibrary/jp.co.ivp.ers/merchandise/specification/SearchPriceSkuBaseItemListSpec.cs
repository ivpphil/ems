﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise.specification
{
    public class SearchPriceSkuBaseItemListSpec
         : SearchSpecificationBase
    {
        protected string orderByValue { get; set; }

        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            this.SetOrderByValue(criteria);

            return base.GetSearchData(criteria);
        }

        protected virtual void SetOrderByValue(Criteria criteria)
        {
            this.orderByValue = criteria.GetDistinctOn("s_master_t");
            if (string.IsNullOrEmpty(this.orderByValue))
            {
                criteria.InsertOrderByToFirst("s_master_t.scode", Criteria.OrderBy.ORDER_BY_ASC);
                this.orderByValue = "s_master_t.scode";
            }
        }

        protected override string GetSearchDataSql()
        {
            return "SELECT DISTINCT ON (" + orderByValue + ") *, "
                + "s_master_t.intime AS s_intime, s_master_t.utime AS s_utime, s_master_t.active AS s_active "
                + "FROM s_master_t "
                + "INNER JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode "
                +"LEFT JOIN( "
                +"	SELECT scode, price, price2, cost_price FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.NORMAL + " "
                +") AS price_t ON s_master_t.scode = price_t.scode "
                +"LEFT JOIN( "
                +"	SELECT scode, price AS regular_first_price FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.REGULAR_FIRST + " "
                +") AS regular_first_price ON s_master_t.scode = regular_first_price.scode "
                +"LEFT JOIN( "
                +"	SELECT scode, price AS regular_price FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.REGULAR + " "
                +") AS regular_price ON s_master_t.scode = regular_price.scode "
                +"LEFT JOIN( "
                +"	SELECT scode, price AS sale_price, date_from AS p_date_from, date_to AS p_date_to FROM price_t WHERE current_timestamp BETWEEN date_from AND date_to AND price_kbn = " + (int)EnumPriceKbn.SALE + " "
                +") AS sale_price ON s_master_t.scode = sale_price.scode "
                + "WHERE true ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(DISTINCT s_master_t.id) AS " + countColumnAlias + " "
                + "FROM price_t "
                + "INNER JOIN s_master_t ON price_t.scode = s_master_t.scode "
                + "INNER JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode "
                +"WHERE true ";
        }
    }
}
