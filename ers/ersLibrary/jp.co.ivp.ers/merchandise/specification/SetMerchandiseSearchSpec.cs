﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.specification
{
    public class SetMerchandiseSearchSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            var strSql = " SELECT DISTINCT set_master_t.*, g_master_t.carriage_cost_type, g_master_t.plural_order_type, s_master_t.supplier_code, s_master_t.wh_order_type, price_t.cost_price, s_master_t.sname ";
            strSql += "FROM set_master_t ";
            strSql += "INNER JOIN s_master_t ON set_master_t.scode = s_master_t.scode ";
            strSql += "INNER JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode ";
            strSql += "RIGHT JOIN s_master_t AS parent_s_master_t ON set_master_t.parent_scode = parent_s_master_t.scode ";
            strSql += "INNER JOIN g_master_t AS parent_g_master_t ON parent_s_master_t.gcode = parent_g_master_t.gcode ";
            strSql += "LEFT JOIN( ";
            strSql += "	SELECT scode, price, price2, cost_price FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.NORMAL + " ";
            strSql += ") AS price_t ON set_master_t.scode = price_t.scode ";
            strSql += "WHERE true ";
            return strSql;
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            var strSql = " SELECT COUNT(DISTINCT set_master_t.id) AS " + countColumnAlias + " ";
            strSql += "FROM set_master_t ";
            strSql += "INNER JOIN s_master_t ON set_master_t.scode = s_master_t.scode ";
            strSql += "INNER JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode ";
            strSql += "RIGHT JOIN s_master_t parent_s_master_t ON set_master_t.parent_scode = parent_s_master_t.scode ";
            strSql += "INNER JOIN g_master_t AS parent_g_master_t ON parent_s_master_t.gcode = parent_g_master_t.gcode ";
            strSql += "LEFT JOIN( ";
            strSql += "	SELECT scode, price, price2, cost_price FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.NORMAL + " ";
            strSql += ") AS price_t ON set_master_t.scode = price_t.scode ";
            strSql += "WHERE true ";
            return strSql;
        }
    }
}
