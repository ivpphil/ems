﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise.specification
{
    public class SearchGroupBaseItemListSpec
        : SearchSpecificationBase
    {
        private string orderByValue { get; set; }

        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            this.orderByValue = criteria.GetDistinctOn("g_master_t");
            if (string.IsNullOrEmpty(this.orderByValue))
            {
                criteria.InsertOrderByToFirst("g_master_t.gcode", Criteria.OrderBy.ORDER_BY_ASC);
                this.orderByValue = "g_master_t.gcode";
            }

            return base.GetSearchData(criteria);
        }

        protected override string GetSearchDataSql()
        {
            return "SELECT DISTINCT ON (" + orderByValue + ") * "
                + "FROM g_master_t "
                + "LEFT JOIN s_master_t ON g_master_t.gcode = s_master_t.gcode "
                + "LEFT JOIN price_t ON s_master_t.scode = price_t.scode "
                + "LEFT JOIN stock_t ON s_master_t.scode = stock_t.scode ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(DISTINCT g_master_t.gcode) AS " + countColumnAlias + " "
                + "FROM g_master_t "
                + "LEFT JOIN s_master_t ON g_master_t.gcode = s_master_t.gcode "
                + "LEFT JOIN price_t ON s_master_t.scode = price_t.scode "
                + "LEFT JOIN stock_t ON s_master_t.scode = stock_t.scode ";
        }
    }
}
