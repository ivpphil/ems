﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.merchandise.specification
{
    /// <summary>
    /// The specification class that determines whether the merchandise is in campaign term.
    /// </summary>
    public class OnCampaignSpecification
    {
        protected internal OnCampaignSpecification() { }

        /// <summary>
        /// Determines whether the merchandise is in campaign term.
        /// </summary>
        /// <param name="merchandise">value from ErsMerchandise</param>
        /// <returns></returns>
        public virtual bool IsSatisfiedBy(DateTime? campaign_from, DateTime? campaign_to)
        {
            return campaign_from != null && campaign_to != null;
        }

        /// <summary>
        /// Determines whether the group of the merchandise is in campaign term.
        /// </summary>
        /// <param name="em">value from ErsMerchandise</param>
        /// <returns></returns>
        public virtual bool IsSatisfiedByGroup(IEnumerable<ErsMerchandise> listMerchandise)
        {
            foreach (var item in listMerchandise)
            {
                if (IsSatisfiedBy(item.p_date_from, item.p_date_to))
                    return true;
            }
            return false;
        }
    }
}
