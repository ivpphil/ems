﻿using System;
using System.Data;
using System.Collections.Generic;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise.specification
{
    public class PriceSearchSpecification
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            string ret = "SELECT price_t.* FROM price_t ";
            ret += "LEFT JOIN g_master_t ON price_t.gcode = g_master_t.gcode ";
            ret += "LEFT JOIN s_master_t ON price_t.scode = s_master_t.scode ";

            return ret;
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            string ret = "SELECT count(price_t.*) AS " + countColumnAlias + " FROM price_t ";
            ret += "LEFT JOIN g_master_t ON price_t.gcode = g_master_t.gcode ";
            ret += "LEFT JOIN s_master_t ON price_t.scode = s_master_t.scode ";

            return ret;
        }
    }

}