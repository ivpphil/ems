﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.merchandise.specification
{
    public class UpdatePriceForSearchSpec
        : ISpecificationForSQL
    {
        public string asSQL()
        {
            return @"UPDATE g_master_t SET price_for_search = tp.price_for_search  , price_for_search_max = tp.price_for_search_max
                    FROM  
                    ( 
                    	SELECT gcode, MIN(price) AS price_for_search, MAX(price) AS price_for_search_max FROM 
                    	( 
                    		SELECT g_master_t.gcode, price_t.price
                    		FROM g_master_t
                    		INNER JOIN s_master_t ON g_master_t.gcode = s_master_t.gcode 
                    		LEFT JOIN price_t ON s_master_t.scode = price_t.scode
                    		WHERE ((price_t.date_from IS NULL AND price_t.date_to IS NULL) OR current_timestamp BETWEEN price_t.date_from AND price_t.date_to) 
                    		AND s_master_t.active = " + (int)EnumActive.Active + @" 
                    		AND price_t.member_rank IS NULL 
                    		AND price_t.price_kbn IN (" + (int)EnumPriceKbn.NORMAL + ", " + (int)EnumPriceKbn.SALE + ", " + (int)EnumPriceKbn.REGULAR + @")
                    		ORDER BY price_t.scode, CASE WHEN current_timestamp BETWEEN price_t.date_from AND price_t.date_to THEN 0 ELSE 1 END, price_t.date_to 
                    	) AS tp 
                    	GROUP BY gcode 
                    )AS tp 
                    WHERE 
                    g_master_t.gcode = tp.gcode 
                    AND ((g_master_t.price_for_search IS NULL OR tp.price_for_search IS NULL OR g_master_t.price_for_search <> tp.price_for_search)
                    OR (g_master_t.price_for_search_max IS NULL OR tp.price_for_search_max IS NULL OR g_master_t.price_for_search_max <> tp.price_for_search_max))";
        }
    }
}
