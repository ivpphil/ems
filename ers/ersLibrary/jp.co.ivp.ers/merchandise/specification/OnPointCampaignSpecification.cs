﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jp.co.ivp.ers.merchandise.specification
{
    /// <summary>
    /// The specification class that determines whether the point is in campaign term.
    /// </summary>
    public class OnPointCampaignSpecification
    {

        protected internal OnPointCampaignSpecification() { }

        /// <summary>
        /// ポイントキャンペーン期間か否か
        /// </summary>
        /// <param name="em">values from ErsMerchandise</param>
        /// <returns></returns>
        public virtual bool IsSatisfiedBy(DateTime? point_campaign_from, DateTime? point_campaign_to, DateTime time)
        {
            return point_campaign_from <= time && point_campaign_to >= time;
        }
    }
}