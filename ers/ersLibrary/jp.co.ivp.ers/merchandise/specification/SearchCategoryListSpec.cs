﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.merchandise.specification
{
    public class SearchCategoryListSpec
        : ISpecificationForSQL
    {
        private int categoryNumber { get; set; }

        /// <summary>
        /// 親を持たないカテゴリのリストを取得する。
        /// </summary>
        /// <param name="activeOnly">active=true</param>
        /// <returns>returns list of records that has no parent record and with 
        /// active only sorted by disp_order and id</returns>
        public virtual List<Dictionary<string, object>> SelectAsListNoParent(int categoryNumber, bool activeOnly, int site_id)
        {
            this.categoryNumber = categoryNumber;

            var retList = new List<Dictionary<string, object>>();

            var criteria = ErsFactory.ersMerchandiseFactory.GetErsCategoryCriteria(categoryNumber);
            criteria.SetNoParent();
            if (activeOnly)
            {
                criteria.active = EnumActive.Active;
            }
            criteria.site_id = site_id;

            criteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            return ErsRepository.SelectSatisfying(this, criteria);
        }

        public string asSQL()
        {
            return string.Format("SELECT * FROM cate{0}_t LEFT JOIN cate{1}_t ON (cate{0}_t.parent_id = cate{1}_t.id)", categoryNumber, categoryNumber - 1);
        }
    }
}
