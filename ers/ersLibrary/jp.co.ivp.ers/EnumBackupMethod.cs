﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumBackupMethod
    {
        /// <summary>
        /// 1: Insert
        /// </summary>
        INSERT = 1,

        /// <summary>
        /// 2: Update
        /// </summary>
        UPDATE,

        /// <summary>
        /// 3: Delete
        /// </summary>
        DELETE
    }
}
