﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;

namespace jp.co.ivp.ers.cts_Inquiry
{
    public class ErsCtsEnquiryDetailRepository
        : ErsRepository<ErsCtsEnquiryDetail>
    {
        public ErsCtsEnquiryDetailRepository()
            : base(new ErsDB_cts_enquiry_detail_t())
        {
        }

        public override void Insert(ErsCtsEnquiryDetail obj, bool storeNewIdToObject = false)
        {
            if (storeNewIdToObject == true)
            {
                obj.sub_no = this.ersDB_table.GetNextSequence();
                obj.id = obj.sub_no;
            }
            base.Insert(obj);
        }
    }
}
