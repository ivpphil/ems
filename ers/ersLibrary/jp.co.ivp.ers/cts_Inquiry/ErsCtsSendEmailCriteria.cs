﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.cts_Inquiry
{
    public class ErsCtsSendEmailCriteria
        : Criteria
    {
        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_send_email_t.id", value, Operation.EQUAL));
            }
        }

        public virtual EnumEnqEmailStatus? email_status
        {
            set
            {
                Add(Criteria.GetCriterion("cts_send_email_t.email_status", value, Operation.EQUAL));
            }
        }
    }
}
