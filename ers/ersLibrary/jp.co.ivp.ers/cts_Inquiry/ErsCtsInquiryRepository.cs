﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.Inquiry
{
    public class ErsCtsInquiryRepository
        : ErsRepository<ErsCtsInquiry>
    {
        public ErsCtsInquiryRepository()
            : base(new ErsDB_cts_enquiry_t())
        {
        }

        public ErsCtsInquiryRepository (ErsDatabase objDB)
            : base(new ErsDB_cts_enquiry_t(objDB))
        {
        }

        public override void Insert(ErsCtsInquiry obj, bool storeNewIdToObject = false)
        {
            if (storeNewIdToObject == true)
            {
                var caseno = this.ersDB_table.GetNextSequence();
                obj.case_no = caseno;
                obj.id = obj.case_no;
            }

            base.Insert(obj);
        }

        public void UpdateCardErr(ErsCtsInquiryScreen objnew, ErsCtsInquiryScreen objold)
        {
            var oldDic = objold.GetPropertiesAsDictionary();
            var newDic = objnew.GetPropertiesAsDictionary();

            var changedColumns = ErsCommon.GetChangedKeys(oldDic, newDic);
            if (changedColumns.Length > 0)
                this.ersDB_table.gUpdateColumn(changedColumns, newDic, "WHERE id = " + objold.id + "");
        }

        public virtual IList<ErsCtsInquiryScreen> FindHeader(db.Criteria criteria)
        {
            var spec = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquirySearchSpec();
            List<ErsCtsInquiryScreen> lstRet = new List<ErsCtsInquiryScreen>();
            var list = spec.GetSearchData(criteria);
            foreach (var dr in list)
            {
                var inqrec = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryScreenWithParameters(dr);
                lstRet.Add(inqrec);
            }
            return lstRet;
        }

        public virtual long GetRecordCountHeader(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquirySearchSpec();
            return spec.GetCountData(criteria);
        }

        public IList<ErsCtsInquiryScreen> FindDetail(db.Criteria criteria)
        {
            var spec = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryDetailSearchSpec();
            List<ErsCtsInquiryScreen> lstRet = new List<ErsCtsInquiryScreen>();
            var list = spec.GetSearchData(criteria);
            foreach (var dr in list)
            {
                var inqrec = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryScreenWithParameters(dr);
                lstRet.Add(inqrec);
            }
            return lstRet;
        }

        public IList<ErsCtsInquiryScreen> FindInqCase(db.Criteria criteria)
        {
            var spec = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryCaseSearchSpec();
            List<ErsCtsInquiryScreen> lstRet = new List<ErsCtsInquiryScreen>();
            var list = spec.GetSearchData(criteria);
            foreach (var dr in list)
            {
                var inqrec = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryScreenWithParameters(dr);
                lstRet.Add(inqrec);
            }
            return lstRet;
        }

        public long GetRecordCountInqCase(ErsCtsInquiryCriteria criteria)
        {
            var spec = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryCaseSearchSpec();
            return spec.GetCountData(criteria);
        }

        public IList<ErsCtsInquiryScreen> FindMailApprove(db.Criteria criteria)
        {
            var spec = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryMailApproveSearchSpec();
            List<ErsCtsInquiryScreen> lstRet = new List<ErsCtsInquiryScreen>();
            var list = spec.GetSearchData(criteria);
            foreach (var dr in list)
            {
                var inqrec = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryScreenWithParameters(dr);
                lstRet.Add(inqrec);
            }
            return lstRet;
        }
    }
}
