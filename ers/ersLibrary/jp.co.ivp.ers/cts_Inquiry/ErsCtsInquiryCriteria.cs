﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.viewService;

namespace jp.co.ivp.ers.Inquiry
{
    public class ErsCtsInquiryCriteria : Criteria
	{
        public virtual string mcode
        {
            set 
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.mcode", value, Operation.EQUAL));
            }
        }

        public virtual int? case_no 
        { 
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.case_no", value, Operation.EQUAL));
            }
        }

        public virtual int? sub_no
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_detail_t.sub_no", value, Operation.EQUAL));
            }
        }

        public virtual string user_id
        {
            set 
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.user_id", value, Operation.EQUAL));
            }
        }

        public virtual int? esc_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.esc_id", Convert.ToString(value), Operation.EQUAL));
            }
        }

        public virtual IEnumerable<string> esc_id_in
        {
            set
            {
                Add(Criteria.GetInClauseCriterion("cts_enquiry_t.esc_id", value));
            }
        }

        public virtual EnumEnqStatus? enq_status
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.enq_status", value, Operation.EQUAL));
            }
        }

        public virtual EnumEnqProgress? enq_progress 
        { 
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.enq_progress", value, Operation.EQUAL));
            }
        }

        public virtual EnumEnqProgress? not_enq_progress
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.enq_progress", value, Operation.NOT_EQUAL));
            }
        }

        public virtual EnumEnqCorresponding? enq_corresponding
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_detail_t.enq_corresponding", value, Operation.EQUAL));
            }
        }

        public virtual int active
        {
            set
            {
                Add(Criteria.GetCriterion("cts_login_t.active", value, Operation.EQUAL));
            }
        }

        public virtual int? id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.id", value, Operation.EQUAL));
            }
        }

        public virtual int? dtl_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_login_t.id", value, Operation.EQUAL));
            }
        }

        public virtual int cate1
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.cate1", value, Operation.EQUAL));
            }
        }

        public virtual int cate2
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.cate2", value, Operation.EQUAL));
            }
        }

        public virtual int cate3
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.cate3", value, Operation.EQUAL));
            }
        }

        public virtual int cate4
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.cate4", value, Operation.EQUAL));
            }
        }

        public virtual int cate5
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.cate5", value, Operation.EQUAL));
            }
        }

        public virtual EnumEnqEmailStatus? email_status
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_detail_t.email_status", value, Operation.EQUAL));
            }
        }

        public virtual EnumEnqEmailType? email_type
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_detail_t.email_type", value, Operation.EQUAL));
            }
        }

        public virtual EnumEnqSituation? enq_situation
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.enq_situation", value, Operation.EQUAL));
            }
        }

        public virtual EnumCardErrFlg? card_error_flg
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.card_error_flg", value, Operation.EQUAL));
            }
        }

        public virtual string keywords_search
        {
            set
            {
                this.Add(Criteria.JoinWithOR(
                        new[] { 
                            Criteria.GetLikeClauseCriterion("cts_enquiry_detail_t.enq_detail", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH), 
                            Criteria.GetLikeClauseCriterion("cts_enquiry_detail_t.ans_detail", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH),
                            Criteria.GetLikeClauseCriterion("cts_enquiry_detail_t.email_body", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH),
                            Criteria.GetLikeClauseCriterion("cts_enquiry_detail_t.memo", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH)
                        }));
            }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("cts_enquiry_t.id", orderBy);
        }

        public void SetOrderBySubNo(OrderBy orderBy)
        {
            AddOrderBy("cts_enquiry_detail_t.sub_no", orderBy);
        }

        public void SetOrderByCaseNo(OrderBy orderBy)
        {
            AddOrderBy("cts_enquiry_detail_t.case_no", orderBy);
        }

        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("cts_enquiry_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_enquiry_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}
