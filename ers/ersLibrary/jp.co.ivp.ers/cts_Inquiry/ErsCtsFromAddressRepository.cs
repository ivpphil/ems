﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.fromaddress
{
    public class ErsCtsFromAddressRepository
        : ErsRepository<ErsCtsFromAddress>
    {
        public ErsCtsFromAddressRepository()
            : base("cts_from_address_t")
        {
        }

        public ErsCtsFromAddressRepository(ErsDatabase objDB)
            : base("cts_from_address_t", objDB)
        {
        }
    }
}
