﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.clientescalation
{
    public class ErsCtsClientEscalationRepository
        : ErsRepository<ErsCtsClientEscalation>
    {
        public ErsCtsClientEscalationRepository()
            : base("cts_client_escalation_t")
        {
        }
    }
}
