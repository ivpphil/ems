﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.fromaddress;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.fromaddress.strategy
{
    /// <summary>
    /// Checks for Cts Common Type data validity.
    /// </summary>
    public class CheckValidCtsFromAddressStgy
    {
        /// <summary>
        /// Return true if the same email from name exists.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public virtual ValidationResult CheckDuplicate(int? id, string email_from_name)
        {
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsFromAddressCriteria();

            criteria.exclude_id = id;
            criteria.email_from_name = email_from_name;

            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsFromAddressRepository();

            if (repository.GetRecordCount(criteria) > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("email_from_name"), email_from_name), new[] { "email_from_name" });
            }

            return null;
        }
    }
}
