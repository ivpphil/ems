﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.fromaddress;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.fromaddress.strategy
{
    /// <summary>
    /// Checks for Cts Common Type data validity.
    /// </summary>
    public class CheckValidCtsClientEscalationStgy
    {
        /// <summary>
        /// Return true if the same escalation name exists.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public virtual ValidationResult CheckDuplicate(int? id, string esc_name)
        {
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationCriteria();

            criteria.exclude_id = id;
            criteria.esc_name = esc_name;

            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationRepository();

            if (repository.GetRecordCount(criteria) > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("10103", ErsResources.GetFieldName("esc_name"), esc_name), new[] { "esc_name" });
            }

            return null;
        }
    }
}
