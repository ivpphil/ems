﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ers.jp.co.ivp.ers.cts_Inquiry.strategy
{
    public class ObtainLockControl
    {

        /// <summary>
        /// 排他
        /// </summary>
        public void Lock(int? case_no, int ctsUserID)
        {
            var rep = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
            var new_inq = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryWithCaseNo(case_no.Value);

            new_inq.lockid = Convert.ToInt32(ctsUserID);

            var old_inq = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryWithCaseNo(case_no.Value);
            rep.Update(new_inq, old_inq);
        }


        /// <summary>
        /// 排他解除
        /// </summary>
        /// <param name="case_no"></param>
        /// <param name="ctsUserID"></param>
        public void Release(int? case_no, int ctsUserID)
        {
            var rep = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
            var new_inq = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryWithCaseNo(case_no.Value);

            new_inq.lockid = null;

            var old_inq = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryWithCaseNo(case_no.Value);
            rep.Update(new_inq, old_inq);
        }
    }
}
