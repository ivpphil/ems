﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.cts_Inquiry
{
    public class ErsCtsEnquiryDetailCriteria
        : Criteria
    {
        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_detail_t.id", value, Operation.EQUAL));
            }
        }

        public int? case_no
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_detail_t.case_no", value, Operation.EQUAL));
            }
        }

        public int? sub_no
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_detail_t.sub_no", value, Operation.EQUAL));
            }
        }
    }
}
