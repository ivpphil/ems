﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.cts_Inquiry
{
    public class ErsCtsEnquiryDetail
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public int? case_no { get; set; }
        public int? sub_no { get; set; }
        public EnumEnqCorresponding? enq_corresponding { get; set; }
        public EnumCorresponding? corresponding { get; set; }
        public DateTime? starttime { get; set; }
        public DateTime? finishtime { get; set; }
        public string email_title { get; set; }
        public string email_from_name { get; set; }
        public string email_from { get; set; }
        public string[] email_to { get; set; }
        public string[] email_cc { get; set; }
        public string[] email_bcc { get; set; }
        public string enq_detail { get; set; }
        public string ans_detail { get; set; }
        public string email_header { get; set; }
        public string email_body { get; set; }
        public string email_fotter { get; set; }
        public string memo { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumEnqSaveMode? save_mode { get; set; }
        public EnumActive active { get; set; }
        public EnumEnqEmailStatus? email_status { get; set; }
        public EnumEnqEmailType? email_type { get; set; }

        public int? email_id { get; set; }
        public int? site_id { get; set; }
    }
}
