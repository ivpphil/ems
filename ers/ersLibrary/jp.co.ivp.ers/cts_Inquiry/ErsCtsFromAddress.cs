﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.fromaddress
{
    public class ErsCtsFromAddress : ErsRepositoryEntity
	{
        public override int? id { get; set; }

        public string email_from_name { get; set; }

        public virtual string email_from { get; set; }

        public virtual string[] email_cc { get; set; }

        public virtual string[] email_bcc { get; set; }

        public virtual string email_header { get; set; }

        public virtual string email_body { get; set; }

        public virtual string email_fotter { get; set; }

        public virtual DateTime? utime { get; set; }

        //public virtual DateTime? intime { get; set; }

        public virtual EnumActive? active { get; set; }

        public virtual int? site_id { get; set; }
	}
}
