﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;
using System.Reflection;

namespace jp.co.ivp.ers.Inquiry
{
    public class ErsCtsInquiryScreen
        : IErsCollectable, IOverwritable
    {
        public virtual int? id { get; set; }
        public virtual int? case_no { get; set; }
        public virtual string mcode { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? detail_intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual DateTime? recepttime { get; set; }
        public virtual EnumEnqType? enq_type { get; set; }
        public virtual EnumEnqStatus? enq_status { get; set; }
        public virtual EnumEnqProgress? enq_progress { get; set; }
        public virtual EnumEnqSituation? enq_situation { get; set; }
        public virtual EnumEnqPriority? enq_priorty { get; set; }
        public virtual string enq_casename { get; set; }
        public virtual string enq_progress_name { get; set; }
        public virtual string ag_name { get; set; }
        public virtual string user_ag_name { get; set; }
        public virtual string esc_ag_name { get; set; }
        public virtual int cate1 { get; set; }
        public virtual int cate2 { get; set; }
        public virtual int cate3 { get; set; }
        public virtual int cate4 { get; set; }
        public virtual int cate5 { get; set; }
        public virtual string user_id { get; set; }
        public virtual EnumEnqSaveMode? save_mode { get; set; }
        public virtual string esc_id { get; set; }
        public virtual int? lockid { get; set; }

        public virtual int dtl_id { get; set; }
        public virtual string enq_status_name { get; set; }
        public virtual string enq_situation_name { get; set; }
        public virtual string enq_priorty_name { get; set; }
        public virtual string cate1_name { get; set; }
        public virtual string cate2_name { get; set; }
        public virtual string cate3_name { get; set; }
        public virtual string cate4_name { get; set; }
        public virtual string cate5_name { get; set; }
        public virtual int sub_no { get; set; }
        public virtual EnumEnqCorresponding? enq_corresponding { get; set; }
        public virtual EnumCorresponding? corresponding { get; set; }
        public virtual DateTime? starttime { get; set; }
        public virtual DateTime? finishtime { get; set; }
        public virtual string email_title { get; set; }
        public virtual string email_from_name { get; set; }
        public virtual string email_from { get; set; }
        public virtual string[] email_to { get; set; }
        public virtual string[] email_cc { get; set; }
        public virtual string[] email_bcc { get; set; }
        public virtual string enq_detail { get; set; }
        public virtual string ans_detail { get; set; }
        public virtual string email_header { get; set; }
        public virtual string email_body { get; set; }
        public virtual string email_fotter { get; set; }
        public virtual string memo { get; set; }
        public virtual EnumActive? active { get; set; }
        public virtual int reccnt { get; set; }
        public virtual EnumEnqEmailStatus? email_status { get; set; }
        public virtual EnumEnqEmailType? email_type { get; set; }
        public virtual bool mailescsend { get; set; }
        public virtual bool mailsent { get; set; }

        public Dictionary<string, object> GetPropertiesAsDictionary()
        {
            return ErsReflection.GetPropertiesAsDictionary(this, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        }

        public void OverwriteWithParameter(IDictionary<string, object> dictionary)
        {
            var dic = ErsCommon.OverwriteDictionary(this.GetPropertiesAsDictionary(), dictionary);
            ErsReflection.SetPropertyAll(this, dic);
        }
    }
}
