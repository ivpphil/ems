﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.incomingmail
{
    public class ErsCtsIncomingMailCriteria : Criteria
	{
        public virtual int? id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_incoming_mail_t.id", value, Operation.EQUAL));
            }
        }

        public int? exclude_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_incoming_mail_t.id", value, Operation.NOT_EQUAL));
            }
        }

        public virtual EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("cts_incoming_mail_t.active", value, Operation.EQUAL));
            }
        }

        public string message_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_incoming_mail_t.message_id", value, Operation.EQUAL));
            }
        }

        public string u_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_incoming_mail_t.u_id", value, Operation.EQUAL));
            }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("id", orderBy);
        }


        public int? cts_enquiry_detail_t_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_detail_t.id", value, Operation.EQUAL));
            }
        }

        public int? site_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_incoming_mail_t.site_id", value, Operation.EQUAL));
            }
        }
    }
}
