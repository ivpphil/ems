﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.cts_Inquiry
{
    public class ErsCtsSendEmail
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public int? case_no { get; set; }
        public int? sub_no { get; set; }
        public string email_title { get; set; }
        public string email_from_name { get; set; }
        public string email_from { get; set; }
        public string[] email_to { get; set; }
        public string[] email_cc { get; set; }
        public string[] email_bcc { get; set; }
        public string email_header { get; set; }
        public string email_body { get; set; }
        public string email_fotter { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }

        public EnumEnqEmailStatus? email_status { get; set; }
        public EnumActive? active { get; set; }
    }
}
