﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.Inquiry.specification;
using jp.co.ivp.ers.fromaddress;
using jp.co.ivp.ers.fromaddress.strategy;
using jp.co.ivp.ers.clientescalation;
using jp.co.ivp.ers.incomingmail;
using ers.jp.co.ivp.ers.cts_Inquiry.strategy;
using jp.co.ivp.ers.cts_Inquiry;
using jp.co.ivp.ers.cts_Inquiry.specification;

namespace jp.co.ivp.ers.Inquiry
{
    public class ErsCtsInquiryFactory
    {

#region " Inquiry "

        public ErsCtsInquiryCriteria GetErsCtsInquiryCriteria()
        {
            return new ErsCtsInquiryCriteria();
        }

        public ErsCtsInquiryRepository GetErsCtsInquiryRepository()
        {
            return new ErsCtsInquiryRepository();
        }

        public ErsCtsInquiry GetErsCtsInquiry()
        {
            return new ErsCtsInquiry();
        }

        public ErsCtsInquiryScreen GetErsCtsInquiryScreenWithID(int? ID)
        {
            var repository = this.GetErsCtsInquiryRepository();
            var criteria = this.GetErsCtsInquiryCriteria();
            criteria.id = ID;

            var list = repository.FindHeader(criteria);

            if (list.Count == 0)
                throw new ErsException("29002");
            return list[0];
        }

        public ErsCtsInquiryScreen GetErsCtsInquiryScreen()
        {
            return new ErsCtsInquiryScreen();
        }

        public ErsCtsInquirySearchSpec GetErsCtsInquirySearchSpec()
        {
            return new ErsCtsInquirySearchSpec();
        }

        public virtual ErsCtsInquiryDetailSearchSpec GetErsCtsInquiryDetailSearchSpec()
        {
            return new ErsCtsInquiryDetailSearchSpec();
        }

        public virtual ErsCtsInquiryCaseSearchSpec GetErsCtsInquiryCaseSearchSpec()
        {
            return new ErsCtsInquiryCaseSearchSpec();
        }

        public virtual ErsCtsInquiryMailApproveSearchSpec GetErsCtsInquiryMailApproveSearchSpec()
        {
            return new ErsCtsInquiryMailApproveSearchSpec();
        }

        public ErsCtsInquiry GetErsCtsInquiryWithParameters(Dictionary<string, object> parameters)
        {
            var Inquiry = this.GetErsCtsInquiry();
            Inquiry.OverwriteWithParameter(parameters);
            return Inquiry;
        }
       
        public ErsCtsInquiryScreen GetErsCtsInquiryScreenWithParameters(Dictionary<string, object> parameters)
        {
            var Inquiry = this.GetErsCtsInquiryScreen();
            Inquiry.OverwriteWithParameter(parameters);
            return Inquiry;
        }

        public ErsCtsInquiry GetErsCtsInquiryWithCaseNo(int? case_no)
        {
            var repository = this.GetErsCtsInquiryRepository();
            var criteria = this.GetErsCtsInquiryCriteria();
            criteria.case_no = case_no;
            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");
            return list[0];
        }

        public ErsCtsInquiryScreen GetErsCtsInquiryScreenWithCaseNo(int? case_no)
        {
            var repository = this.GetErsCtsInquiryRepository();
            var criteria = this.GetErsCtsInquiryCriteria();
            criteria.case_no = case_no;
            var list = repository.FindHeader(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");
            return list[0];
        }

        public ObtainLockControl GetObtainLockControl()
        {
            return new ObtainLockControl();
        }

#endregion

#region " From Address "

        public ErsCtsFromAddressCriteria GetErsCtsFromAddressCriteria()
        {
            return new ErsCtsFromAddressCriteria();
        }

        public ErsCtsFromAddressRepository GetErsCtsFromAddressRepository()
        {
            return new ErsCtsFromAddressRepository();
        }

        public ErsCtsFromAddress GetErsCtsFromAddress()
        {
            return new ErsCtsFromAddress();
        }

        public ErsCtsFromAddress GetErsCtsFromAddressWithModel(ErsModelBase model)
        {
            var FromAddress = this.GetErsCtsFromAddress();
            FromAddress.OverwriteWithModel(model);
            return FromAddress;
        }

        public ErsCtsFromAddress GetErsCtsFromAddressWithParameters(Dictionary<string, object> parameters)
        {
            var FromAddress = this.GetErsCtsFromAddress();
            FromAddress.OverwriteWithParameter(parameters);
            return FromAddress;
        }

        public ErsCtsFromAddress GetErsCtsFromAddressWithID(int ID)
        {
            var repository = this.GetErsCtsFromAddressRepository();
            var criteria = this.GetErsCtsFromAddressCriteria();

            criteria.id = ID;
            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");
            return list[0];
        }

        public virtual CheckValidCtsFromAddressStgy GetValidCtsFromAddressStgy()
        {
            return new CheckValidCtsFromAddressStgy();
        }

#endregion

#region " Client Escalation "

        public ErsCtsClientEscalationCriteria GetErsCtsClientEscalationCriteria()
        {
            return new ErsCtsClientEscalationCriteria();
        }

        public ErsCtsClientEscalationRepository GetErsCtsClientEscalationRepository()
        {
            return new ErsCtsClientEscalationRepository();
        }

        public ErsCtsClientEscalation GetErsCtsClientEscalation()
        {
            return new ErsCtsClientEscalation();
        }

        public ErsCtsClientEscalation GetErsCtsClientEscalationWithModel(ErsModelBase model)
        {
            var ClientEscalation = this.GetErsCtsClientEscalation();
            ClientEscalation.OverwriteWithModel(model);
            return ClientEscalation;
        }

        public ErsCtsClientEscalation GetErsCtsClientEscalationWithParameters(Dictionary<string, object> parameters)
        {
            var ClientEscalation = this.GetErsCtsClientEscalation();
            ClientEscalation.OverwriteWithParameter(parameters);
            return ClientEscalation;
        }

        public ErsCtsClientEscalation GetErsCtsClientEscalationWithID(int ID)
        {
            var repository = this.GetErsCtsClientEscalationRepository();
            var criteria = this.GetErsCtsClientEscalationCriteria();

            criteria.id = ID;
            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");
            return list[0];
        }

        public virtual CheckValidCtsClientEscalationStgy GetValidCtsClientEscalationStgy()
        {
            return new CheckValidCtsClientEscalationStgy();
        }

#endregion

#region " Incoming Mail "

        public ErsCtsIncomingMailCriteria GetErsCtsIncomingMailCriteria()
        {
            return new ErsCtsIncomingMailCriteria();
        }

        public ErsCtsIncomingMailRepository GetErsCtsIncomingMailRepository()
        {
            return new ErsCtsIncomingMailRepository();
        }

        public ErsCtsIncomingMail GetErsCtsIncomingMail()
        {
            return new ErsCtsIncomingMail();
        }

        public ErsCtsIncomingMail GetErsCtsIncomingMailWithModel(ErsModelBase model)
        {
            var IncomingMail = this.GetErsCtsIncomingMail();
            IncomingMail.OverwriteWithModel(model);
            return IncomingMail;
        }

        public ErsCtsIncomingMail GetErsCtsIncomingMailWithParameters(Dictionary<string, object> parameters)
        {
            var IncomingMail = this.GetErsCtsIncomingMail();
            IncomingMail.OverwriteWithParameter(parameters);
            return IncomingMail;
        }

        public ErsCtsIncomingMail GetErsCtsIncomingMailWithID(int ID)
        {
            var repository = this.GetErsCtsIncomingMailRepository();
            var criteria = this.GetErsCtsIncomingMailCriteria();

            criteria.id = ID;
            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");
            return list[0];
        }

#endregion


        public virtual ErsCtsSendEmailRepository GetErsCtsSendEmailRepository()
        {
            return new ErsCtsSendEmailRepository();
        }

        public virtual ErsCtsSendEmailCriteria GetErsCtsSendEmailCriteria()
        {
            return new ErsCtsSendEmailCriteria();
        }

        public virtual ErsCtsSendEmail GetErsCtsSendEmail()
        {
            return new ErsCtsSendEmail();
        }

        public virtual ErsCtsSendEmail GetErsCtsSendEmailWithParameter(Dictionary<string, object> parameter)
        {
            var objCtsSendEmail = this.GetErsCtsSendEmail();
            objCtsSendEmail.OverwriteWithParameter(parameter);
            return objCtsSendEmail;
        }

        public virtual ErsCtsSendEmail GetErsCtsSendEmailWithId(int? id)
        {
            var repository = this.GetErsCtsSendEmailRepository();
            var criteria = this.GetErsCtsSendEmailCriteria();
            criteria.id = id;
            var listResult = repository.Find(criteria);
            if (listResult.Count == 0)
            {
                return null;
            }

            return listResult.First();
        }

        public virtual ErsCtsEnquiryDetailRepository GetErsCtsEnquiryDetailRepository()
        {
            return new ErsCtsEnquiryDetailRepository();
        }

        public virtual ErsCtsEnquiryDetailCriteria GetErsCtsEnquiryDetailCriteria()
        {
            return new ErsCtsEnquiryDetailCriteria();
        }

        public virtual ErsCtsEnquiryDetail GetErsCtsEnquiryDetail()
        {
            return new ErsCtsEnquiryDetail();
        }

        public virtual ErsCtsEnquiryDetail GetErsCtsEnquiryDetailWithParameter(Dictionary<string, object> parameter)
        {
            var objCtsEnquiryDetail = this.GetErsCtsEnquiryDetail();
            objCtsEnquiryDetail.OverwriteWithParameter(parameter);
            return objCtsEnquiryDetail;
        }

        public virtual ErsCtsEnquiryDetail GetErsCtsEnquiryDetailWithId(int? id)
        {
            var repository = this.GetErsCtsEnquiryDetailRepository();
            var criteria = this.GetErsCtsEnquiryDetailCriteria();
            criteria.id = id;
            var listResult = repository.Find(criteria);
            if (listResult.Count == 0)
            {
                return null;
            }

            return listResult.First();
        }

        public virtual SearchIncomingMailListSpec GetSearchIncomingMailListSpec()
        {
            return new SearchIncomingMailListSpec();
        }
    }
}
