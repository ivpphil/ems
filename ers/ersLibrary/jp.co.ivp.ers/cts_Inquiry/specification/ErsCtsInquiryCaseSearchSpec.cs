﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Inquiry.specification
{
    public class ErsCtsInquiryCaseSearchSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return " SELECT DISTINCT cts_enquiry_t.* "
                    + "  ,cts_login_t.ag_name AS user_ag_name "
                    + "FROM cts_enquiry_t LEFT JOIN cts_enquiry_detail_t ON cts_enquiry_t.case_no = cts_enquiry_detail_t.case_no "
                    + "LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(cts_enquiry_t.*) AS " + countColumnAlias + " "
                    + "FROM cts_enquiry_t LEFT JOIN cts_enquiry_detail_t ON cts_enquiry_t.case_no = cts_enquiry_detail_t.case_no "
                    + "LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id ";
        }
    }
}
