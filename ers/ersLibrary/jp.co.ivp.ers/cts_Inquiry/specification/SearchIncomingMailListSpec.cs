﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.cts_Inquiry.specification
{
    public class SearchIncomingMailListSpec
         : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return "SELECT DISTINCT cts_incoming_mail_t.* FROM cts_incoming_mail_t "
                + "LEFT JOIN cts_enquiry_detail_t ON cts_incoming_mail_t.id = cts_enquiry_detail_t.email_id ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(cts_incoming_mail_t.id) FROM cts_incoming_mail_t "
                + "LEFT JOIN cts_enquiry_detail_t ON cts_incoming_mail_t.id = cts_enquiry_detail_t.email_id ";
        }
    }
}
