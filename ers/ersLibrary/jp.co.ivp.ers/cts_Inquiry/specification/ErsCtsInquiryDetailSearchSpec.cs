﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Inquiry.specification
{
    public class ErsCtsInquiryDetailSearchSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            var site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            var addWhere = " where cts_enquiry_category_t.site_id = " + site_id + " ";

            return " SELECT * "
                    + " ,cts_enquiry_detail_t.id AS dtl_id "
                    + " ,(SELECT common_namecode_t.namename FROM common_namecode_t INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON common_namecode_t.code = tp_cts_enquiry_t.enq_priorty AND tp_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND common_namecode_t.type_code = 'ENQPRY') AS enq_priorty_name "
                    + " ,(SELECT common_namecode_t.namename FROM common_namecode_t INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON common_namecode_t.code = tp_cts_enquiry_t.enq_status AND tp_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND common_namecode_t.type_code = 'ENQSTS') AS enq_status_name "
                    + " ,(SELECT common_namecode_t.namename FROM common_namecode_t INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON common_namecode_t.code = tp_cts_enquiry_t.enq_progress AND tp_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND common_namecode_t.type_code = 'ENQPGR') AS enq_progress_name "
                    + " ,(SELECT common_namecode_t.namename FROM common_namecode_t INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON common_namecode_t.code = tp_cts_enquiry_t.enq_situation AND tp_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND common_namecode_t.type_code = 'ENQSIT') AS enq_situation_name "
                    + " ,(SELECT cts_enquiry_category_t.namename FROM cts_enquiry_category_t INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON cts_enquiry_category_t.code = tp_cts_enquiry_t.cate1 AND tp_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND cts_enquiry_category_t.type_code = 'ENQCT1' " + addWhere + ") AS cate1_name "
                    + " ,(SELECT cts_enquiry_category_t.namename FROM cts_enquiry_category_t INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON cts_enquiry_category_t.code = tp_cts_enquiry_t.cate2 AND tp_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND cts_enquiry_category_t.type_code = 'ENQCT2' " + addWhere + ") AS cate2_name "
                    + " ,(SELECT cts_enquiry_category_t.namename FROM cts_enquiry_category_t INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON cts_enquiry_category_t.code = tp_cts_enquiry_t.cate3 AND tp_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND cts_enquiry_category_t.type_code = 'ENQCT3' " + addWhere + ") AS cate3_name "
                    + " ,(SELECT cts_enquiry_category_t.namename FROM cts_enquiry_category_t INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON cts_enquiry_category_t.code = tp_cts_enquiry_t.cate4 AND tp_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND cts_enquiry_category_t.type_code = 'ENQCT4' " + addWhere + ") AS cate4_name "
                    + " ,(SELECT cts_enquiry_category_t.namename FROM cts_enquiry_category_t INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON cts_enquiry_category_t.code = tp_cts_enquiry_t.cate5 AND tp_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND cts_enquiry_category_t.type_code = 'ENQCT5' " + addWhere + ") AS cate5_name "
                    + " ,cts_login_t.ag_name as ag_name "
                    + "FROM cts_enquiry_t "
                    + "INNER JOIN cts_enquiry_detail_t ON cts_enquiry_t.case_no = cts_enquiry_detail_t.case_no "
                    + "LEFT JOIN  cts_login_t on cts_login_t.id = CAST(cts_enquiry_t.user_id AS INTEGER)  ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(distinct cts_enquiry_detail_t.sub_no) AS " + countColumnAlias + " "
                        + "      FROM cts_enquiry_detail_t "
                        + "      INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON cts_enquiry_detail_t.case_no = tp_cts_enquiry_t.case_no ";
        }
    }
}
