﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Inquiry.specification
{
    public class ErsCtsInquirySearchSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return "  SELECT cts_enquiry_t.* "
                        + "  ,(SELECT common_namecode_t.namename "
                        + "      FROM common_namecode_t "
                        + "      INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON common_namecode_t.code = tp_cts_enquiry_t.enq_status AND cts_enquiry_t.case_no = tp_cts_enquiry_t.case_no AND common_namecode_t.type_code = 'ENQSTS' "
                        + "      ) AS enq_status_name "
                        + "  ,(SELECT common_namecode_t.namename "
                        + "      FROM common_namecode_t "
                        + "      INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON common_namecode_t.code = tp_cts_enquiry_t.enq_progress AND cts_enquiry_t.case_no = tp_cts_enquiry_t.case_no AND common_namecode_t.type_code = 'ENQPGR' "
                        + "      ) AS enq_progress_name "
                        + "  ,(SELECT COUNT(distinct cts_enquiry_detail_t.sub_no) "
                        + "      FROM cts_enquiry_detail_t "
                        + "      INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON cts_enquiry_detail_t.case_no = tp_cts_enquiry_t.case_no AND cts_enquiry_t.case_no = tp_cts_enquiry_t.case_no "
                        + "      ) AS reccnt "
                        + "  ,(SELECT MAX(cts_enquiry_detail_t.sub_no) "
                        + "      FROM cts_enquiry_detail_t  "
                        + "      INNER JOIN cts_enquiry_t AS tp_cts_enquiry_t ON cts_enquiry_detail_t.case_no = tp_cts_enquiry_t.case_no AND cts_enquiry_t.case_no = tp_cts_enquiry_t.case_no "
                        + "      ) AS sub_no "
                        + "  ,lock_cts_login_t.ag_name "
                        + "  ,cts_login_t.ag_name AS user_ag_name "
                        + "  ,esc_cts_login_t.ag_name AS esc_ag_name "
                        + "FROM cts_enquiry_t "
                        + "LEFT JOIN cts_login_t AS lock_cts_login_t ON cts_enquiry_t.lockid = lock_cts_login_t.id "
                        + "LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id "
                        + "LEFT JOIN cts_login_t AS esc_cts_login_t ON (esc_cts_login_t.id || '') = cts_enquiry_t.esc_id ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT COUNT(DISTINCT cts_enquiry_t.case_no) AS count FROM cts_enquiry_t "
                        +"LEFT JOIN cts_login_t AS lock_cts_login_t ON cts_enquiry_t.lockid = lock_cts_login_t.id "
                        + "LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id ";
        }
    }
}
