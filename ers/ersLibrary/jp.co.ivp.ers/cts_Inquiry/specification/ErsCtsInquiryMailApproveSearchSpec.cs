﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Inquiry.specification
{
    public class ErsCtsInquiryMailApproveSearchSpec
        : ISpecificationForSQL
    {
        public List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return ErsRepository.SelectSatisfying(this, criteria);
        }

        /// <summary>
        /// SQL文 
        /// </summary>
        /// <returns>SQL文</returns>
        public virtual string asSQL()
        {
            return " SELECT *, cts_enquiry_detail_t.intime AS detail_intime FROM cts_enquiry_t INNER JOIN cts_enquiry_detail_t ON cts_enquiry_t.case_no = cts_enquiry_detail_t.case_no ";

        }
    }
}
