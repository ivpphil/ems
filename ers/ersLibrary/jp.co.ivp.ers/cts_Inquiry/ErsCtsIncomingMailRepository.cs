﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.incomingmail
{
    public class ErsCtsIncomingMailRepository
        : ErsRepository<ErsCtsIncomingMail>
    {
        public ErsCtsIncomingMailRepository()
            : base("cts_incoming_mail_t")
        {
        }

        public override IList<ErsCtsIncomingMail> Find(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsInquiryFactory.GetSearchIncomingMailListSpec();
            var listValue = spec.GetSearchData(criteria);
            return this.CreateList(listValue);
        }

        public override long GetRecordCount(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsInquiryFactory.GetSearchIncomingMailListSpec();
            return spec.GetCountData(criteria);
        }
    }
}
