﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.Inquiry
{
    public class ErsCtsInquiry: ErsRepositoryEntity
	{
        public override int? id { get; set; }
        public virtual int? case_no { get; set; }
        public virtual string mcode { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual DateTime? recepttime { get; set; }
        public virtual EnumEnqType? enq_type { get; set; }
        public virtual EnumEnqStatus? enq_status { get; set; }
        public virtual EnumEnqProgress? enq_progress { get; set; }
        public virtual EnumEnqSituation? enq_situation { get; set; }
        public virtual EnumEnqPriority? enq_priorty { get; set; }
        public virtual string enq_casename { get; set; }
        public virtual int cate1 { get; set; }
        public virtual int cate2 { get; set; }
        public virtual int cate3 { get; set; }
        public virtual int cate4 { get; set; }
        public virtual int cate5 { get; set; }
        public virtual string user_id { get; set; }
        public virtual EnumEnqSaveMode? save_mode { get; set; }
        public virtual string esc_id { get; set; }
        public virtual int? lockid { get; set; }
        public virtual EnumCardErrFlg card_error_flg { get; set; }
        public virtual int? site_id { get; set; }
    }
}
