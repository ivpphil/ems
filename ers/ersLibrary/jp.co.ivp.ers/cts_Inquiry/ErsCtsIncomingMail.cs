﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.incomingmail
{
    public class ErsCtsIncomingMail : ErsRepositoryEntity
	{
        public override int? id { get; set; }

        public virtual string mail_from { get; set; }

        public virtual string[] mail_to { get; set; }
        public virtual string mail_to_string { get; set; }
        
        public virtual string[] mail_cc { get; set; }
        public virtual string mail_cc_string { get; set; }

        public virtual string[] mail_bcc { get; set; }
        public virtual string mail_bcc_string { get; set; }

        public virtual string mail_title { get; set; }

        public virtual string mail_body { get; set; }

        public virtual DateTime? utime { get; set; }

        public virtual DateTime? intime { get; set; }

        public virtual EnumActive? active { get; set; }

        public virtual string mcode { get; set; }

        public virtual string u_id { get; set; }

        public virtual DateTime? received_date { get; set; }
        
        public virtual string message_id { get; set; }

        public virtual string mail_from_name { get; set; }

        public virtual int? site_id { get; set; }
    }
}
