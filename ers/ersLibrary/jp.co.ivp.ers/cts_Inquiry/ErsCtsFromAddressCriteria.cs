﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.fromaddress
{
    public class ErsCtsFromAddressCriteria : Criteria
	{

        public int id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_from_address_t.id", value, Operation.EQUAL));
            }
        }

        public int? exclude_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_from_address_t.id", value, Operation.NOT_EQUAL));
            }
        }

        public string email_from_name
        {
            set
            {
                Add(Criteria.GetCriterion("cts_from_address_t.email_from_name", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("cts_from_address_t.active", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("cts_from_address_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_from_address_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("id", orderBy);
        }

        public void SetOrderByFromName(OrderBy orderBy)
        {
            AddOrderBy("email_from_name", orderBy);
        }

	}
}
