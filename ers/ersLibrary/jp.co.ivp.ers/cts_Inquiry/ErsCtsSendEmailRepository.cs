﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.cts_Inquiry
{
    public class ErsCtsSendEmailRepository
        : ErsRepository<ErsCtsSendEmail>
    {
        public ErsCtsSendEmailRepository()
            : base("cts_send_email_t")
        {
        }
    }
}
