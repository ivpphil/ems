﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumAccountLaunderingResult
    {
        /// <summary>
        /// 0: 有効性OK
        /// </summary>
        Success = 0,

        /// <summary>
        /// 1: 有効性NG
        /// </summary>
        Fail = 1,

        /// <summary>
        /// 2: 照合エラー
        /// </summary>
        MatchingError = 2,

        /// <summary>
        /// 3: 入力データエラー
        /// </summary>
        ValueError = 3,
    }
}
