﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    //use for getting mailto sent_flg
    public enum EnumSentFlg
    {
        /// <summary>
        /// 0 : 未送信 [NotSent]
        /// </summary>
        NotSent = 0,

        /// <summary>
        /// 1 : 送信済み [Sent]
        /// </summary>
        Sent = 1,

        /// <summary>
        /// 2 : データエラー
        /// </summary>
        DataError = 2,
    }
}
