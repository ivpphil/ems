﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumAuthorityType
    {
        /// <summary>
        /// 1: リーダ
        /// </summary>
        SUPERVISOR = 1,
        /// <summary>
        /// 2: オペレータ
        /// </summary>
        OPERATOR = 2
    }
}
