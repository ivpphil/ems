﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Enums for Active (nonActive or Active)
    /// </summary>
    public enum EnumActive
        : short
    {
        NonActive = 0,
        Active = 1,
    }
}
