﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumLpPageTypeCode
    {
        /// <summary>
        /// LPページ
        /// </summary>
        Sell,

        /// <summary>
        /// アップセルページ
        /// </summary>
        Upsell,

        /// <summary>
        /// 確認ページ
        /// </summary>
        Confirm,

        /// <summary>
        /// 完了ページ
        /// </summary>
        Complete,
    }
}
