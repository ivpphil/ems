﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumEnqEmailStatus
        : short
    {
        //0:e-mail tray 1:e-mail regist 2:send finish 
        /// <summary>
        /// 0: e-mail tray
        /// </summary>
        EmailTray,
        /// <summary>
        /// 1: e-mail regist
        /// </summary>
        EmailRegist,
        /// <summary>
        /// 2: send finish
        /// </summary>
        SendFinish
    }
}
