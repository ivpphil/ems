﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Enums for target_kbn (item or excludingitem)
    /// </summary>
    public enum EnumTargetKbn
    {
        /// <summary>
        /// 1 : 対象商品 [Item]
        /// </summary>
        Item = 1,

        /// <summary>
        /// 2 : 除外商品 [ExcludingItem]
        /// </summary>
        ExcludingItem = 2,
    }
}
