﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumWhOrderType
    {
        /// <summary>
        /// 在庫
        /// </summary>
        Stock = 1,

        /// <summary>
        /// 受注発注
        /// </summary>
        Order
    }
}
