﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumSummaryWhereOparation
    {
        /// <summary>
        /// 0: field = value
        /// </summary>
        EQUAL,

        /// <summary>
        /// 1: field &lt;&gt; value
        /// </summary>
        NOT_EQUAL,

        /// <summary>
        /// 2: field &gt; value
        /// </summary>
        GREATER_THAN,

        /// <summary>
        /// 3: field &gt;= value
        /// </summary>
        GREATER_EQUAL,

        /// <summary>
        /// 4: field &lt; value
        /// </summary>
        LESS_THAN,

        /// <summary>
        /// 5: field &lt;= value
        /// </summary>
        LESS_EQUAL,

        /// <summary>
        /// 6: field = ANY(value)
        /// </summary>
        ANY_EQUAL,

        /// <summary>
        /// 7: field &lt;&gt; ANY(value)
        /// </summary>
        ANY_NOT_EQUAL,

        /// <summary>
        /// 8: field && ARRAY(value)
        /// </summary>
        ARRAY_AMBERSAND,

        /// <summary>
        /// 9: field IN(value)
        /// </summary>
        IN,

        /// <summary>
        /// 10: field NOT IN(value)
        /// </summary>
        NOT_IN,

        /// <summary>
        /// 11: field LIKE(value)
        /// </summary>
        LIKE,

        /// <summary>
        /// 12: field NOT LIKE(value)
        /// </summary>
        NOT_LIKE,

        /// <summary>
        /// 13: field BETWEEN value_from AND value_to
        /// </summary>
        BETWEEN_DATE,
    }
}
