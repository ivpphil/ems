﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers
{
    public enum EnumReportSummary : short
    {
        FirstQuarter = 1,
        SecondQuarter = 2,
        ThirdQuarter = 3,
        FourthQuarter = 4,
        FirstHalf = 5,
        SecondHalf = 6,
        Today = 7,
        Week = 8,
        Month = 9,
        Year = 10
    }
}
