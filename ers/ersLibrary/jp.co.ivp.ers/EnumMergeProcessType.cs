﻿
namespace jp.co.ivp.ers
{
    public enum EnumMergeProcessType
        : short
    {
        CUSTOMER = 1,
        ENQUIRY = 2
    }
}
