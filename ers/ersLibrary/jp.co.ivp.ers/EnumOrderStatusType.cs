﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// 対応区分定義
    /// </summary>
    public enum EnumOrderStatusType
    {
        /*if you add this item, should add GetOrderHeaderStatus item */
        /*ここに項目を足した場合、ObtainErsOrderStatusStgyのGetOrderHeaderStatusにも設定を足す事！！*/
        /// <summary>
        /// 10: 新着注文
        /// </summary>
        NEW_ORDER = 10,

        /// <summary>
        /// 15: 出荷待機
        /// </summary>
        DELIVER_WAITING = 15,

        /// <summary>
        /// 20: 出荷指示済み
        /// </summary>
        DELIVER_REQUEST = 20,

        /// <summary>
        /// 30: 配送済
        /// </summary>
        DELIVERED = 30,

        /// <summary>
        /// 999: 出荷前キャンセル
        /// </summary>
        CANCELED = 999,

        /// <summary>
        /// 888: 出荷後キャンセル
        /// </summary>
        CANCELED_AFTER_DELIVER = 888
    }
}
