﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumGmoPayType
    {
        /// <summary>
        /// 0：クレジット
        /// </summary>
        Credit,

        /// <summary>
        /// 1：モバイルSuica
        /// </summary>
        Suica,

        /// <summary>
        /// 2：Mobile Edy
        /// </summary>
        Edy,
        
        /// <summary>
        /// 3：コンビニ
        /// </summary>
        Convenience,

        /// <summary>
        /// 4：Pay-easy
        /// </summary>
        PayEasy,

        /// <summary>
        /// 5：PayPal
        /// </summary>
        PayPal,

        /// <summary>
        /// 6：ｉＤネット
        /// </summary>
        iDnet,

        /// <summary>
        /// 7：WebMoney
        /// </summary>
        WebMoney
    }
}
