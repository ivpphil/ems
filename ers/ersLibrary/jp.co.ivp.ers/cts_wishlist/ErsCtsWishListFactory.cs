﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.cts_wishlist.specification;

namespace jp.co.ivp.ers.cts_wishlist
{
    public class ErsCtsWishListFactory
    {
        public virtual ErsCtsWishList GetErsCtsWishList()
        {
            return new ErsCtsWishList();
        }

        public virtual ErsCtsWishListRepository GetErsCtsWishListRepository()
        {
            return new ErsCtsWishListRepository();
        }

        public virtual ErsCtsWishListCritera GetErsCtsWishListCritera()
        {
            return new ErsCtsWishListCritera();
        }

        public ErsCtsWishList GetErsCtsWishListWithModel(ErsModelBase model)
        {
            var wishlist = this.GetErsCtsWishList();
            wishlist.OverwriteWithModel(model);
            return wishlist;
        }

        public ErsCtsWishList GetErsCtsWishListWithParameters(Dictionary<string, object> parameters)
        {
            var wishlist = this.GetErsCtsWishList();
            wishlist.OverwriteWithParameter(parameters);
            return wishlist;
        }

        public ErsCtsWishList GetErsCtsWishListWithId(int id)
        {
            var repository = this.GetErsCtsWishListRepository();
            var criteria = this.GetErsCtsWishListCritera();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count != 1)
                return null;

            return list[0];
        }

        public SearchCtsWishlistWithMerchandiseSpec GetSearchCtsWishlistWithMerchandiseSpec()
        {
            return new SearchCtsWishlistWithMerchandiseSpec();
        }
    }
}
