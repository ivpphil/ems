﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.cts_wishlist
{
    public class ErsCtsWishListRepository
        : ErsRepository<ErsCtsWishList>
    {
        public ErsCtsWishListRepository()
            : base("cts_wishlist_t")
        {
        }

        public ErsCtsWishListRepository(ErsDatabase objDB)
            : base("cts_wishlist_t", objDB)
        {
        }
    }
}
