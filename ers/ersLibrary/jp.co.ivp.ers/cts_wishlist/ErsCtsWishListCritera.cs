﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.cts_wishlist
{
    public class ErsCtsWishListCritera
        : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_wishlist_t.id", value, Operation.EQUAL));
            }
        }


        public EnumCtsWishlistType wishlist_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_wishlist_t.wishlist_type", (int)value, Operation.EQUAL));
            }
        }

        public string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_wishlist_t.scode", value, Operation.EQUAL));
            }
        }

        public string user_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_wishlist_t.user_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_wishlist_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? g_site_id
        {
            set
            {
                var gCriteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
                gCriteria.site_id = value;
                this.Add(gCriteria);
            }
        }

        /// <summary>
        /// Sets order by cts_wishlist_t.utime
        /// </summary>
        /// <param name="orderBy">order type</param>
        public virtual void SetCtsWishListByUtime(OrderBy orderBy)
        {
            this.AddOrderBy("cts_wishlist_t.utime", orderBy);
        }

        /// <summary>
        /// Sets order by cts_wishlist_t.intime
        /// </summary>
        /// <param name="orderBy">order type</param>
        public virtual void SetCtsWishListByIntime(OrderBy orderBy)
        {
            this.AddOrderBy("cts_wishlist_t.intime", orderBy);
        }

        /// <summary>
        /// Sets order by cts_wishlist_t.scode
        /// </summary>
        /// <param name="orderBy"order type></param>
        public virtual void SetCtsWishListByScode(OrderBy orderBy)
        {
            this.AddOrderBy("cts_wishlist_t.scode", orderBy);
        }
    }
}
