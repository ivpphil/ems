﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.cts_wishlist
{
    public class ErsCtsWishList
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public virtual EnumCtsWishlistType wishlist_type { get; set; }

        public virtual string user_id { get; set; }

        public virtual string scode { get; set; }

        public virtual DateTime? intime { get; set; }

        public virtual DateTime? utime { get; set; }

        public virtual EnumActive? active { get; set; }

        public virtual EnumSiteId? site_id { get; set; }
    }
}
