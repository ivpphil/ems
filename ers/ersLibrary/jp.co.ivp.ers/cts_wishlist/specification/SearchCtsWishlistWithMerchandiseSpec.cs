﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.cts_wishlist.specification
{
    public class SearchCtsWishlistWithMerchandiseSpec
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return "SELECT * FROM cts_wishlist_t "
                + " INNER JOIN s_master_t ON s_master_t.scode = cts_wishlist_t.scode AND s_master_t.active = " + (int)EnumActive.Active
                + " INNER JOIN g_master_t ON g_master_t.gcode = s_master_t.gcode AND g_master_t.active = " + (int)EnumActive.Active;
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(DISTINCT cts_wishlist_t.id) AS " + countColumnAlias + " "
                + " INNER JOIN s_master_t ON s_master_t.scode = cts_wishlist_t.scode AND s_master_t.active = " + (int)EnumActive.Active
                + " INNER JOIN g_master_t ON g_master_t.gcode = s_master_t.gcode AND g_master_t.active = " + (int)EnumActive.Active;
        }
    }
}
