﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment
{
    public interface IPaymentInfoGmoServerContainer
        : IPaymentInfoContainer
    {
        int? member_card_id { get; set; }
    }
}
