﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.Net;

namespace jp.co.ivp.ers.Payment
{
    public class GmoException : Exception, IErsHandlableException
    {

        private IEnumerable<string> ErrCodes;
        private string messagePrefix;

        public GmoException(IEnumerable<string> ErrCodes, string messagePrefix = null)
            : base()
        {
            this.ErrCodes = ErrCodes;
            this.messagePrefix = messagePrefix;
        }

        protected GmoException()
            : base()
        {
        }

        /// <summary>
        /// エラーコードを取得する。
        /// </summary>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public virtual string GetErrorCodes(string delimiter)
        {
            return string.Join(delimiter, ErrCodes);
        }

        /// <summary>
        /// エラーメッセージを取得する。
        /// </summary>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public virtual string GetErrorMessages(string delimiter)
        {
            //メッセージ作成
            var message = string.Empty;
            foreach (var errorCode in ErrCodes)
            {
                message += delimiter + ErsResources.GetMessage(errorCode);
            }

            if (string.IsNullOrEmpty(message))
            {
                return message;
            }

            return this.messagePrefix + message.Substring(delimiter.Length);
        }

        /// <summary>
        /// エラーメッセージを取得する。
        /// </summary>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public override string Message
        {
            get
            {
                return this.GetErrorMessages(Environment.NewLine);
            }
        }

        /// <summary>
        /// return Url
        /// </summary>
        public string returnUrl
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// return null
        /// </summary>
        public ErsModelBase model
        {
            get { return null; }
        }

        /// <summary>
        /// DBインサート用
        /// </summary>
        public virtual string errInfoMsg
        {
            get
            {
                //メッセージ作成
                var message = string.Empty;
                foreach (var errorCode in ErrCodes)
                {
                    message += "[" + errorCode + "]" + ErsResources.GetMessage(errorCode) + "$";
                }

                if (message.EndsWith("$")) message = message.Substring(0, message.Length - 1);

                return message;
            }
        }

        /// <summary>
        /// HttpStatusCode
        /// </summary>
        public HttpStatusCode? httpStatus { get; set; }


        public bool isNoBackTo { get; set; }
    }
}
