﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Payment
{
    public class ErsTpPaypal
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public DateTime? edate { get; set; }
        public string token { get; set; }
        public string ransu { get; set; }
        public string form_values { get; set; }
    }
}
