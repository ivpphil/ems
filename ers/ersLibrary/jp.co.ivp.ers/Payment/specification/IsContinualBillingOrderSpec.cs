﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.Payment.specification
{
    public class IsContinualBillingOrderSpec
    {
        public bool Satisfy(ErsOrder ersOrder)
        {
            var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(ersOrder.pay);
            return objPayment is ErsGmoCard && ersOrder.access_id == null && ersOrder.access_pass == null;
        }
    }
}
