﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment
{
    public interface IGmoResultReceive
    {
        /// <summary>
        /// CHAR 13 ショップID
        /// </summary>
        string ShopID { get; set; }

        /// <summary>
        /// CHAR 10 ショップパスワード “*” 10桁固定
        /// </summary>
        string ShopPass { get; set; }

        /// <summary>
        /// CHAR 32 取引ID
        /// </summary>
        string AccessID { get; set; }

        /// <summary>
        /// CHAR 32 取引パスワード “*” 32桁固定
        /// </summary>
        string AccessPass { get; set; }

        /// <summary>
        /// CHAR 27 オーダーID
        /// </summary>
        string OrderID { get; set; }

        /// <summary>
        /// CHAR - 現状態
        /// </summary>
        EnumGmoResultStatus? Status { get; set; }

        /// <summary>
        /// CHAR - 処理区分
        /// </summary>
        EnumGmoJobCd? JobCd { get; set; }

        /// <summary>
        /// NUMBER 10 利用金額
        /// </summary>
        int? Amount { get; set; }

        /// <summary>
        /// NUMBER 10 税送料
        /// </summary>
        int? Tax { get; set; }

        /// <summary>
        /// CHAR 3 通貨コード決済に利用された通貨を返却します。
        /// </summary>
        string Currency { get; set; }

        /// <summary>
        /// CHAR 7 仕向先会社コードカード・ｉＤネット決済時のみ返却
        /// </summary>
        string Forward { get; set; }

        /// <summary>
        /// CHAR 1 支払方法
        /// </summary>
        EnumGmoMethod? Method { get; set; }

        /// <summary>
        /// NUMBER 2 支払回数カード・ｉＤネット決済時のみ返却
        /// </summary>
        int? PayTimes { get; set; }

        /// <summary>
        /// CHAR 28 トランザクションID
        /// </summary>
        string TranID { get; set; }

        /// <summary>
        /// CHAR 7 承認番号カード・ｉＤネット決済時のみ返却
        /// </summary>
        string Approve { get; set; }

        /// <summary>
        /// CHAR 14 処理日付 yyyyMMddHHmmss書式
        /// </summary>
        string TranDate { get; set; }

        /// <summary>
        /// CHAR 3 エラーコードエラー発生時のみ ※2
        /// </summary>
        string ErrCode { get; set; }

        /// <summary>
        /// CHAR 9 エラー詳細コードエラー発生時のみ ※2
        /// </summary>
        string ErrInfo { get; set; }

        /// <summary>
        /// CHAR 1 決済方法
        /// </summary>
        EnumGmoPayType? PayType { get; set; }

        /// <summary>
        /// CHAR 5 支払先コンビニコード
        /// </summary>
        EnumConvCode? CvsCode { get; set; }

        /// <summary>
        /// CHAR 20 コンビニ確認番号
        /// </summary>
        string CvsConfNo { get; set; }

        /// <summary>
        /// CHAR 32 コンビニ受付番号
        /// </summary>
        string CvsReceiptNo { get; set; }

        /// <summary>
        /// CHAR 16 Edy受付番号
        /// </summary>
        string EdyReceiptNo { get; set; }

        /// <summary>
        /// CHAR 40 Edy注文番号
        /// </summary>
        string EdyOrderNo { get; set; }

        /// <summary>
        /// CHAR 9 Suica受付番号
        /// </summary>
        string SuicaReceiptNo { get; set; }

        /// <summary>
        /// CHAR 40 Suica注文番号
        /// </summary>
        string SuicaOrderNo { get; set; }

        /// <summary>
        /// CHAR 11 Pay-easyお客様番号
        /// </summary>
        string CustID { get; set; }

        /// <summary>
        /// CHAR 5 Pay-easy収納機関番号
        /// </summary>
        string BkCode { get; set; }

        /// <summary>
        /// CHAR 20 Pay-easy確認番号
        /// </summary>
        string ConfNo { get; set; }

        /// <summary>
        /// CHAR 14 Pay-easy支払期限日時(yyyyMMddHHmmss書式)
        /// </summary>
        string PaymentTerm { get; set; }

        /// <summary>
        /// CHAR 128 Pay-easy暗号化決済番号
        /// </summary>
        string EncryptReceiptNo { get; set; }

        /// <summary>
        /// CHAR 14 入金確定日時(yyyyMMddHHmmss書式)(モバイルSuica・Edy・コンビニ・Pay-easy)
        /// </summary>
        string FinishDate { get; set; }

        /// <summary>
        /// CHAR 14 受付日時(yyyyMMddHHmmss書式)(モバイルSuica・Edy・コンビニ・Pay-easy)
        /// </summary>
        string ReceiptDate { get; set; }

        /// <summary>
        /// CHAR 16 購入に使用されたWebMoneyの管理番号
        /// </summary>
        string WebMoneyManagementNo { get; set; }

        /// <summary>
        /// CHAR 25 WebMoneyセンターが返却した決済コード
        /// </summary>
        string WebMoneySettleCode { get; set; }
    }
}
