﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment
{
    public interface IPaymentInfoGmoConvenienceContainer
        : IPaymentInfoContainer
    {
        EnumConvCode? conv_code { get; set; }
    }
}
