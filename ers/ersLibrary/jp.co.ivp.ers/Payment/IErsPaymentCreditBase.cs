﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.Payment
{
    public interface IErsPaymentCreditBase
        : IErsPaymentBase
    {
        List<CreditCardInfo> ObtainMemberCardInfo(ErsMember member);

        CreditCardInfo ObtainMemberCardInfo(ErsMember member, int? card_id);

        ErsMemberCard SaveMemberCardInfo(CreditCardInfo card_info, ErsMember member);

        ErsMemberCard deleteCardInfo(CreditCardInfo card_info, ErsMember member);
    }
}
