﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.Payment
{
    public class ErsPay
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual string pay_name { get; set; }
        public virtual string memo { get; set; }
        public virtual DateTime intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual EnumActive? active { get; set; }
        public virtual string etc_name { get; set; }
        public virtual EnumOnOff? etc_flg { get; set; }

        public virtual int? floor_limit { get; set; }

        public virtual int? cts_floor_limit { get; set; }

        public virtual EnumOnOff? enable_tax { get; set; }
        public virtual EnumOnOff? enable_regular_order { get; set; }
        public virtual EnumOnOff? enable_overseas { get; set; }
        public virtual int? site_id { get; set; }
    }
}
