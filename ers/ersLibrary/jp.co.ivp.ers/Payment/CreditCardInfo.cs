﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.order.related
{
    /// <summary>
    /// クレジットカード情報
    /// </summary>
    public class CreditCardInfo
        : IErsCollectable
    {
        public const int IgnoreCardSequenceValue = 9999;

        /// <summary>
        /// preserve
        /// </summary>
        public virtual EnumCardInfoPreserve preserve { get; protected set; }

        /// <summary>
        /// カード契約者名
        /// </summary>
        public virtual string card_holder_name { get; protected set; }
        /// <summary>
        /// カード会社種別
        /// </summary>
        public virtual int? card_type { get; protected set; }

        /// <summary>
        /// カード会社名
        /// </summary>
        public virtual string card_name { get; protected set; }

        /// <summary>
        /// カード番号
        /// </summary>
        public virtual string card_no { get; protected set; }

        /// <summary>
        /// カード番号(下４桁)
        /// </summary>
        public virtual string card_no_u4 { get; protected set; }

        /// <summary>
        /// 有効期限(年)
        /// </summary>
        public virtual int? validity_y { get; protected set; }

        /// <summary>
        /// 有効期限(月)
        /// </summary>
        public virtual int? validity_m { get; protected set; }

        /// <summary>
        /// セキュリティーコード
        /// </summary>
        public virtual string security_no { get; protected set; }

        /// <summary>
        /// ERS会員番号（カード預け時）
        /// </summary>
        public virtual string member_id { get; protected set; }

        /// <summary>
        /// カード識別会員番号（カード預け時）
        /// </summary>
        public string card_mcode { get; set; }

        /// <summary>
        /// ERSカードID
        /// </summary>
        public int? card_id { get; set; }

        /// <summary>
        /// カード識別番号（カード預け時）
        /// </summary>
        public virtual string card_sequence { get; protected set; }

        /// <summary>
        /// コンストラクタ（カード情報入力時）
        /// </summary>
        /// <param name="card_holder_name">カード契約者名</param>
        /// <param name="card_type">カード会社種別</param>
        /// <param name="card_no">カード番号</param>
        /// <param name="validity_y">有効期限(年)</param>
        /// <param name="validity_m">有効期限(月)</param>
        /// <param name="security_no">セキュリティーコード</param>
        public CreditCardInfo(string card_holder_name,
            int? card_type,
            string card_no,
            int? validity_y,
            int? validity_m,
            string security_no)
        {
            this.card_holder_name = card_holder_name;
            this.card_type = card_type;
            this.card_no = card_no;
            this.validity_y = validity_y;
            this.validity_m = validity_m;
            this.security_no = security_no;

            if (!string.IsNullOrEmpty(this.card_no))
            {
                if (card_no.Length > 4)
                {
                    this.card_no_u4 = card_no.Substring(card_no.Length - 4);
                }
            }

            //カード会社名を取得
            if (this.card_type != null)
                this.card_name = ErsFactory.ersViewServiceFactory.GetErsViewCardService().GetStringFromId(this.card_type.Value);

            this.preserve = EnumCardInfoPreserve.OnServer;

        }

        /// <summary>
        /// Accepts Credit Card info
        /// </summary>
        /// <param name="member_id"></param>
        /// <param name="card_sequence"></param>
        /// <param name="card_holder_name"></param>
        /// <param name="card_type"></param>
        /// <param name="card_no"></param>
        /// <param name="validity_y"></param>
        /// <param name="validity_m"></param>
        /// <param name="security_no"></param>
        public CreditCardInfo(
            string member_id,
            string card_mcode,
            int? card_id,
            string card_sequence,
            string card_holder_name,
            int? card_type,
            string card_no,
            int? validity_y,
            int? validity_m,
            string security_no)
            : this(card_holder_name, card_type, card_no, validity_y, validity_m, security_no)
        {
            this.member_id = member_id;
            this.card_mcode = card_mcode;
            this.card_id = card_id;
            this.card_sequence = card_sequence;
            this.preserve = EnumCardInfoPreserve.OnService;
        }

        #region IErsCollectable メンバー

        public Dictionary<string, object> GetPropertiesAsDictionary()
        {
            return ErsReflection.GetPropertiesAsDictionary(this);
        }

        #endregion
    }
}
