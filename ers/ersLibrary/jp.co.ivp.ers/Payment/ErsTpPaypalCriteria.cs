﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.Payment
{
    public class ErsTpPaypalCriteria
        : Criteria
    {
        public string ransu
        {
            set
            {
                this.Add(Criteria.GetCriterion("tp_paypal_t.ransu", value, Operation.EQUAL));
            }
        }

        public string token
        {
            set
            {
                this.Add(Criteria.GetCriterion("tp_paypal_t.token", value, Operation.EQUAL));
            }
        }
    }
}
