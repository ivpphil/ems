﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment
{
    public interface IPaymentInfoPayPalContainer
        : IPaymentInfoContainer
    {
        string token { get; set; }

        string PayerID { get; set; }

        string c_req_no { get; set; }
    }
}
