﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using System.Net;
using System.IO;
using jp.co.ivp.ers.db.table;
using System.Web;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.related;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.Payment
{
    /// <summary>
    /// Provides the methods that controll GMO transaction.
    /// </summary>
    public class ErsGmoCard
        : ErsGmoBase, IErsPaymentCreditBase
    {
        protected static int seq_mode = 1;//0論理モード 1物理モード

        /// <summary>
        /// Send auth request to GMO.
        /// </summary>
        /// <param name="order">ErsOrder</param>
        /// <param name="member">ErsMember</param>
        /// <param name="isMonitor">isMonitor</param>
        public void SendAuth(ErsOrder order, ErsMember member, bool isMonitor)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            this.InitConnection(setup, isMonitor);

            //受注番号の重複を避けるため、伝票番号にシーケンスを付加します。
            order.credit_order_id = ErsFactory.ersOrderFactory.GetErsOrderRepository().GetNextCreditOrderId(order.d_no);

            while (true)
            {
                try
                {
                    this.SendEntryTransaction(order);
                }
                catch (GmoRetryableException gmoException)
                {
                    throw new GmoException(gmoException.ErrorInfoList);
                }
                break;
            }

            while (true)
            {
                try
                {
                    this.SendAuthTransaction(order);
                }
                catch (GmoRetryableException gmoException)
                {
                    throw new GmoException(gmoException.ErrorInfoList);
                }
                break;
            }
        }

        /// <summary>
        /// Get entry data for this transaction.
        /// </summary>
        /// <param name="order">ErsOrder</param>
        /// <returns></returns>
        protected virtual void SendEntryTransaction(ErsOrder order)
        {
            var SendValues = GetBaseDictionary();

            SendValues.Add("OrderID", order.credit_order_id);
            SendValues.Add("JobCd", order.jobCode);
            SendValues.Add("Amount", Convert.ToString(order.total));

            var dicRet = this.HashCall(connect_url + "payment/EntryTran.idPass", SendValues);

            if (dicRet.ContainsKey("errcode") || dicRet.ContainsKey("errinfo"))
            {
                throw new GmoRetryableException(Convert.ToString(dicRet["errcode"]), Convert.ToString(dicRet["errinfo"]));
            }

            order.access_id = Convert.ToString(dicRet["accessid"]);
            order.access_pass = Convert.ToString(dicRet["accesspass"]);

            if (order.jobCode == EnumGmoJobCd.SALES)
            {
                order.order_payment_status = EnumOrderPaymentStatusType.PAID;
                order.paid_date = DateTime.Now;
                order.paid_price = order.total;
            }
            else
            {
                order.order_payment_status = EnumOrderPaymentStatusType.NOT_PAID;
            }
        }

        /// <summary>
        /// Send Authorize transaction.
        /// </summary>
        /// <param name="order">ErsOrder</param>
        protected virtual void SendAuthTransaction(ErsOrder order)
        {
            var SendValues = GetBaseDictionary();

            SendValues.Add("AccessID", order.access_id);
            SendValues.Add("AccessPass", order.access_pass);
            SendValues.Add("OrderID", order.credit_order_id);
            SendValues.Add("Method", (int)order.gmoMethod);

            if (order.card_info.preserve == EnumCardInfoPreserve.OnServer)
            {
                SendValues.Add("CardNo", order.card_info.card_no);
                SendValues.Add("Expire", Convert.ToString(order.card_info.validity_y).Substring(2, 2) + string.Format("{0:D2}", order.card_info.validity_m));
            }
            else
            {
                SendValues.Add("MemberID", order.card_info.card_mcode);
                SendValues.Add("CardSeq", order.card_info.card_sequence);
                SendValues.Add("SeqMode", seq_mode);
            }

            var dicRet = this.HashCall(connect_url + "payment/ExecTran.idPass", SendValues);

            if (dicRet.ContainsKey("errcode") || dicRet.ContainsKey("errinfo"))
            {
                throw new GmoRetryableException(Convert.ToString(dicRet["errcode"]), Convert.ToString(dicRet["errinfo"]));
            }

            string approve = null;
            if (dicRet.ContainsKey("approve"))
                approve = (string)dicRet["approve"];

            order.approve = approve;

            // After taked auth, the order will not be target of continual billing function anymore.
            order.sent_continual_billing = EnumSentContinualBillingFlg.None;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        /// <param name="member"></param>
        public void SendAlterAuth(ErsOrder order, int amount)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            this.InitConnection(setup, false);

            while (true)
            {
                try
                {
                    order.approve = this.SendAlterAuthTransaction(order.access_id, order.access_pass, amount);
                }
                catch (GmoRetryableException gmoException)
                {
                    throw new GmoException(gmoException.ErrorInfoList);
                }
                break;
            }
        }

        /// <summary>
        /// 与信枠変更
        /// </summary>
        /// <param name="order"></param>
        protected virtual string SendAlterAuthTransaction(string access_id, string access_pass, int amount)
        {
            var SendValues = GetBaseDictionary();

            SendValues.Add("AccessID", access_id);
            SendValues.Add("AccessPass", access_pass);

            SendValues.Add("JobCd", EnumGmoJobCd.AUTH);
            SendValues.Add("Amount", amount);

            var dicRet = this.HashCall(connect_url + "payment/ChangeTran.idPass", SendValues);

            if (dicRet.ContainsKey("errcode") || dicRet.ContainsKey("errinfo"))
            {
                throw new GmoRetryableException(Convert.ToString(dicRet["errcode"]), Convert.ToString(dicRet["errinfo"]));
            }

            string approve = null;
            if (dicRet.ContainsKey("approve"))
                approve = (string)dicRet["approve"];

            return approve;
        }

        /// <summary>
        /// Captures an authorized payment.
        /// </summary>
        /// <param name="order">ErsOrder</param>
        public void SendCapture(ErsOrder order, int total)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            this.InitConnection(setup, false);

            while (true)
            {
                try
                {
                    this.SendCaptureTransaction(order, total);
                }
                catch (GmoRetryableException gmoException)
                {
                    throw new GmoException(gmoException.ErrorInfoList);
                }
                break;
            }
        }

        /// <summary>
        /// Send captured transaction 
        /// </summary>
        /// <param name="order">ErsOrder</param>
        protected virtual void SendCaptureTransaction(ErsOrder order, int total)
        {

            var SendValues = GetBaseDictionary();

            SendValues.Add("AccessID", order.access_id);
            SendValues.Add("AccessPass", order.access_pass);

            SendValues.Add("JobCd", EnumGmoJobCd.SALES);
            SendValues.Add("Amount", total);

            var dicRet = this.HashCall(connect_url + "payment/AlterTran.idPass", SendValues);

            if (dicRet.ContainsKey("errcode") || dicRet.ContainsKey("errinfo"))
            {
                throw new GmoRetryableException(Convert.ToString(dicRet["errcode"]), Convert.ToString(dicRet["errinfo"]));
            }
        }

        public void SetPaymentMethod(ErsOrder order, IPaymentInfoContainer container)
        {
            var paymentInputInfo = container as IPaymentInfoGmoInputContainer;
            if (paymentInputInfo != null)
            {
                this.SetPaymentMethod(order, paymentInputInfo);
                return;
            }
            var paymentInputServerInfo = container as IPaymentInfoGmoServerContainer;
            if (paymentInputServerInfo != null)
            {
                this.SetPaymentMethod(order, paymentInputServerInfo);
                return;
            }

            //不正な引数なのでエラー
            throw new Exception(container.GetType().FullName + " has to implement IPaymentInfoGmoInputContainer or IPaymentInfoGmoServerContainer.");
        }

        private void SetPaymentMethod(ErsOrder order, IPaymentInfoGmoInputContainer paymentInfo)
        {
            CreditCardInfo card_info = null;
            if (paymentInfo.card_id.HasValue && paymentInfo.card_id != CreditCardInfo.IgnoreCardSequenceValue)
            {
                var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(order.mcode, true);
                card_info = this.ObtainMemberCardInfo(member, paymentInfo.card_id);
            }
            else
            {
                card_info = new CreditCardInfo(paymentInfo.card_holder_name, paymentInfo.card, paymentInfo.cardno, paymentInfo.validity_y, paymentInfo.validity_m, paymentInfo.security_no);
            }

            order.card_info = card_info;
        }

        private void SetPaymentMethod(ErsOrder order, IPaymentInfoGmoServerContainer paymentInfo)
        {
            //reset member_add_id
            if (paymentInfo.member_card_id == CreditCardInfo.IgnoreCardSequenceValue)
            {
                paymentInfo.member_card_id = null;
            }

            CreditCardInfo card_info = null;
            if (paymentInfo.member_card_id.HasValue)
            {
                var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(order.mcode, true);
                card_info = this.ObtainMemberCardInfo(member, paymentInfo.member_card_id);
            }

            order.card_info = card_info;
        }

        /// <summary>
        /// Get token of the transaction for view;
        /// </summary>
        /// <param name="order">ErsOrder</param>
        /// <returns></returns>
        public string GetTransactionStringForView(ErsOrder order)
        {
            return order.access_id;
        }

        /// <summary>
        /// Get credit card information
        /// </summary>
        /// <param name="member">ErsMember</param>
        /// <param name="card_id">card_id</param>
        /// <returns></returns>
        public CreditCardInfo ObtainMemberCardInfo(ErsMember member, int? card_id)
        {
            if (member != null)
            {
                var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
                memberCardCriteria.active = EnumActive.Active;
                memberCardCriteria.mcode = member.mcode;
                memberCardCriteria.id = card_id;
                memberCardCriteria.site_id = member.site_id;
                var isMonitor = ErsFactory.ersOrderFactory.GetIsMonitorSpec().IsSpecified(null, member);

                var memberCardList = memberCardRepository.Find(memberCardCriteria);
                if (memberCardList.Count == 1)
                {
                    var memberCard = memberCardList[0];
                    return this.ObtainCardInfoFromCardSequence(memberCard.id, memberCard.mcode, memberCard.card_mcode, memberCard.card_sequence, isMonitor);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get Member card information
        /// </summary>
        /// <param name="member">ErsMember</param>
        /// <returns></returns>
        public List<CreditCardInfo> ObtainMemberCardInfo(ErsMember member)
        {
            var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
            var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
            memberCardCriteria.active = EnumActive.Active;
            memberCardCriteria.mcode = member.mcode;
            memberCardCriteria.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();
            var memberCardList = memberCardRepository.Find(memberCardCriteria);
            var retList = new List<CreditCardInfo>();

            var isMonitor = ErsFactory.ersOrderFactory.GetIsMonitorSpec().IsSpecified(null, member);

            foreach (var memberCard in memberCardList)
            {
                var card_info = this.ObtainCardInfoFromCardSequence(memberCard.id, memberCard.mcode, memberCard.card_mcode, memberCard.card_sequence, isMonitor);
                if (card_info == null)
                {
                    continue;
                }
                retList.Add(card_info);
            }
            return retList;
        }

        /// <summary>
        /// Get Card information sequence 
        /// </summary>
        /// <param name="mcode">mcode</param>
        /// <param name="card_sequence">card_sequence</param>
        /// <param name="isMonitor">isMonitor</param>
        /// <returns></returns>
        protected virtual CreditCardInfo ObtainCardInfoFromCardSequence(int? card_id, string mcode, string card_mcode, string card_sequence, bool isMonitor)
        {
            while (true)
            {
                try
                {
                    return SendGetCardInfo(card_id, mcode, card_mcode, card_sequence, isMonitor);
                }
                catch (GmoRetryableException gmoException)
                {
                    throw new GmoException(gmoException.ErrorInfoList, string.Format("[card_id={0}][mcode={1}]", card_id, mcode));
                }
            }
        }


        /// <summary>
        /// Send Card information 
        /// </summary>
        /// <param name="mcode">mcode</param>
        /// <param name="card_sequence">card_sequence</param>
        /// <param name="isMonitor">isMonitor</param>
        /// <returns></returns>
        protected virtual CreditCardInfo SendGetCardInfo(int? card_id, string mcode, string card_mcode, string card_sequence, bool isMonitor)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            this.InitConnection(setup, isMonitor);

            var SendValues = GetBaseDictionary();

            SendValues.Add("MemberID", card_mcode);
            SendValues.Add("CardSeq", card_sequence);
            SendValues.Add("SeqMode", seq_mode);

            var dicRet = this.HashCall(connect_url + "payment/SearchCard.idPass", SendValues);

            if (dicRet.ContainsKey("errcode") || dicRet.ContainsKey("errinfo"))
            {
                throw new GmoRetryableException(Convert.ToString(dicRet["errcode"]), Convert.ToString(dicRet["errinfo"]));
            }

            //物理モードの場合deleteflagでも有効無効チェック
            if (dicRet.ContainsKey("deleteflag") && dicRet["deleteflag"].Equals("1"))
            {
                //無効データの場合はスルー
                return null;
            }

            if (Convert.ToString(dicRet["cardname"]) == "")
                dicRet["cardname"] = null;

            return new CreditCardInfo(
                mcode,
                card_mcode,
                card_id,
                card_sequence,
                Convert.ToString(dicRet["holdername"]),
                Convert.ToInt32(dicRet["cardname"]),
                Convert.ToString(dicRet["CardNo"]),
                Convert.ToInt32("20" + (Convert.ToString(dicRet["Expire"])).Substring(0, 2)),
                Convert.ToInt32((Convert.ToString(dicRet["Expire"])).Substring(2, 2)),
                string.Empty);
        }

        /// <summary>
        /// Save Memeber Card information by card_info and member
        /// </summary>
        /// <param name="card_info">CreditCardInfo</param>
        /// <param name="member">ErsMember</param>
        /// <returns></returns>
        public ErsMemberCard SaveMemberCardInfo(CreditCardInfo card_info, ErsMember member)
        {
            string card_mcode;
            if (!card_info.card_mcode.HasValue()
                    || (card_info.card_id.HasValue && ErsFactory.ersMemberFactory.GetHasNotYetSaledIssuedRegularOrderSpec().Has(member.mcode, card_info.card_id, member.site_id)))
            {
                // 新規にカード登録
                // 継続課金ファイル送信済み定期購入伝票は新規にカードを登録。
                card_mcode = member.mcode + string.Format("{0:D10}", ErsFactory.ersOrderFactory.GetErsOrderRepository().GetNextCreditMcode());
            }
            else
            {
                card_mcode = card_info.card_mcode;
            }

            while (true)
            {
                try
                {
                    this.SendMemberSave(card_mcode);
                }
                catch (GmoRetryableException gmoException)
                {
                    if (gmoException.ErrorInfoList.Contains("E01390010"))
                    {
                        //already exist member
                        break;
                    }
                    else
                    {
                        throw new GmoException(gmoException.ErrorInfoList);
                    }
                }
                break;
            }

            while (true)
            {
                try
                {
                    return SendCardSave(card_info, member, card_mcode);
                }
                catch (GmoRetryableException gmoException)
                {
                    throw new GmoException(gmoException.ErrorInfoList);
                }
            }
        }

        /// <summary>
        /// Send member save
        /// </summary>
        /// <param name="member">ErsMember</param>
        /// <returns></returns>
        protected virtual void SendMemberSave(string card_mcode)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            this.InitConnection(setup, false);

            var SendValues = GetBaseDictionary();

            SendValues.Add("MemberID", card_mcode);

            var dicRet = this.HashCall(connect_url + "payment/SaveMember.idPass", SendValues);

            if (dicRet.ContainsKey("errcode") || dicRet.ContainsKey("errinfo"))
            {
                throw new GmoRetryableException(Convert.ToString(dicRet["errcode"]), Convert.ToString(dicRet["errinfo"]));
            }
        }


        /// <summary>
        /// returns the Membercard 
        /// </summary>
        /// <param name="card_info">CreditCardInfo</param>
        /// <param name="member">ErsMember</param>
        /// <returns></returns>
        protected virtual ErsMemberCard SendCardSave(CreditCardInfo card_info, ErsMember member, string card_mcode)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            this.InitConnection(setup, false);

            var SendValues = GetBaseDictionary();

            SendValues.Add("MemberID", card_mcode);
            SendValues.Add("CardNo", card_info.card_no);
            SendValues.Add("Expire", Convert.ToString(card_info.validity_y).Substring(2, 2) + string.Format("{0:D2}", card_info.validity_m));
            SendValues.Add("CardName", card_info.card_type);
            SendValues.Add("HolderName", card_info.card_holder_name);
            SendValues.Add("SecurityNo", card_info.security_no);
            SendValues.Add("SeqMode", seq_mode);

            var dicRet = this.HashCall(connect_url + "payment/SaveCard.idPass", SendValues);

            if (dicRet.ContainsKey("errcode") || dicRet.ContainsKey("errinfo"))
            {
                throw new GmoRetryableException(Convert.ToString(dicRet["errcode"]), Convert.ToString(dicRet["errinfo"]));
            }

            var memberCard = ErsFactory.ersMemberFactory.GetErsMemberCard();
            memberCard.mcode = member.mcode;
            memberCard.card_mcode = card_mcode;
            memberCard.card_sequence = Convert.ToString(dicRet["CardSeq"]);
            memberCard.site_id = member.site_id;
            return memberCard;
        }

        /// <summary>
        /// Delete Card by card info, and member info
        /// </summary>
        /// <param name="card_info">CreditCardInfo</param>
        /// <param name="member">ErsMember</param>
        /// <returns></returns>
        protected virtual ErsMemberCard deleteCard(CreditCardInfo card_info, ErsMember member)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            this.InitConnection(setup, false);

            var SendValues = GetBaseDictionary();

            SendValues.Add("MemberID", card_info.card_mcode);
            SendValues.Add("CardSeq", card_info.card_sequence);
            SendValues.Add("SeqMode", seq_mode);

            var dicRet = this.HashCall(connect_url + "payment/DeleteCard.idPass", SendValues);

            if (dicRet.ContainsKey("errcode") || dicRet.ContainsKey("errinfo"))
            {
                throw new GmoRetryableException(Convert.ToString(dicRet["errcode"]), Convert.ToString(dicRet["errinfo"]));
            }

            var ersMemberCard = this.GetMemberCard(member, card_info);

            ersMemberCard.active = EnumActive.NonActive;

            return ersMemberCard;
        }

        private ErsMemberCard GetMemberCard(ErsMember member, CreditCardInfo card_info)
        {
            var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
            var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
            memberCardCriteria.active = EnumActive.Active;
            memberCardCriteria.mcode = member.mcode;
            memberCardCriteria.id = card_info.card_id;
            var ersMemberCard = memberCardRepository.Find(memberCardCriteria).First();
            return ersMemberCard;
        }

        /// <summary>
        /// Delete the card information by card info and member 
        /// </summary>
        /// <param name="card_info">CreditCardInfo</param>
        /// <param name="member">ErsMember</param>
        /// <returns></returns>
        public ErsMemberCard deleteCardInfo(CreditCardInfo card_info, ErsMember member)
        {
            while (true)
            {
                try
                {
                    if (ErsFactory.ersMemberFactory.GetIsCardHasRegularOrderSpec().Has(member.mcode, card_info.card_id)
                        || (card_info.card_id.HasValue && ErsFactory.ersMemberFactory.GetHasNotYetSaledIssuedRegularOrderSpec().Has(member.mcode, card_info.card_id, member.site_id)))
                    {
                        //定期購入伝票は削除できない。
                        return this.GetMemberCard(member, card_info);
                    }

                    return this.deleteCard(card_info, member);
                }
                catch (GmoRetryableException gmoException)
                {
                    throw new GmoException(gmoException.ErrorInfoList);
                }
            }
        }

        /// <summary>
        /// Send void authentication 
        /// </summary>
        /// <param name="order">ErsOrder</param>
        public void SendVoidAuth(ErsOrder order)
        {
            while (true)
            {
                try
                {
                    // 継続課金の場合は与信破棄の必要がない
                    if (!order.access_id.HasValue() && !order.access_pass.HasValue())
                    {
                        return;
                    }

                    this.SendVoidAuthActually(order.access_id, order.access_pass);
                    return;
                }
                catch (GmoRetryableException gmoException)
                {
                    throw new GmoException(gmoException.ErrorInfoList);
                }
            }

        }

        /// <summary>
        /// Send Void Authentication
        /// </summary>
        /// <param name="order">ErsOrder</param>
        protected virtual void SendVoidAuthActually(string access_id, string access_pass)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            this.InitConnection(setup, false);

            var SendValues = GetBaseDictionary();

            SendValues.Add("AccessID", access_id);
            SendValues.Add("AccessPass", access_pass);

            SendValues.Add("JobCd", EnumGmoJobCd.VOID);

            var dicRet = this.HashCall(connect_url + "payment/AlterTran.idPass", SendValues);

            if (dicRet.ContainsKey("errcode") || dicRet.ContainsKey("errinfo"))
            {
                throw new GmoRetryableException(Convert.ToString(dicRet["errcode"]), Convert.ToString(dicRet["errinfo"]));
            }
        }
    }
}
