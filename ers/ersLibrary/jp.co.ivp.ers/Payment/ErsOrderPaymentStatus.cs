﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Payment
{
    public class ErsOrderPaymentStatus
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string order_payment_status { get; set; }

        public EnumActive? active { get; set; }
    }
}
