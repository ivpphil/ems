﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using System.Net;
using System.IO;
using jp.co.ivp.ers.db.table;
using System.Web;
using jp.co.ivp.ers.member;
using System.Web.Mvc;

namespace jp.co.ivp.ers.Payment
{
    /// <summary>
    /// Provides the methods that controll PayPal transaction.
    /// </summary>
    public class ErsPayPal
        : IErsPaymentBase
    {
        protected virtual string delimiter { get { return "<$:>"; } }
        protected virtual string valueDelimiter { get { return "<$=>"; } }

        protected virtual string ecUrl { get; set; }
        protected virtual string userName { get; set; }
        protected virtual string password { get; set; }
        protected virtual string signature { get; set; }
        protected virtual string endPoint { get; set; }

        protected virtual string _additionalQuery { get; set; }

        /// <summary>
        /// initialize
        /// </summary>
        internal ErsPayPal()
        {
        }

        /// <summary>
        /// Configure a connection to PayPal.
        /// </summary>
        /// <param name="setup">Setup</param>
        /// <param name="isMonitor">isMonitor</param>
        protected virtual void InitConnection(Setup setup, bool isMonitor)
        {
            if (!isMonitor)
            {
                ecUrl = setup.paypal_ec_url;
                userName = setup.paypal_api_username;
                password = setup.paypal_api_password;
                signature = setup.paypal_api_signature;
                endPoint = setup.paypal_api_endpoint;
            }
            else
            {
                ecUrl = setup.paypal_ec_url_monitor;
                userName = setup.paypal_api_username_monitor;
                password = setup.paypal_api_password_monitor;
                signature = setup.paypal_api_signature_monitor;
                endPoint = setup.paypal_api_endpoint_monitor;
            }

        }

        /// <summary>
        /// Send request to PayPal.
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="SendValues">SendValues dictionary</param>
        /// <returns></returns>
        protected virtual IDictionary<string, object> HashCall(string url, Dictionary<string, object> SendValues)
        {
            //値をチェック
            if (SendValues["METHOD"] == null || string.IsNullOrEmpty(Convert.ToString(SendValues["METHOD"])))
                throw new Exception(ErsResources.GetMessage("53200"));

            var retDic = ErsHttpClient.Post(url, SendValues);

            this.OutputPayPalLog(SendValues, retDic);

            return retDic;

        }

        /// <summary>
        ///  Logging result of request.
        /// </summary>
        /// <param name="SendValues">SendValues dictionary</param>
        /// <param name="retDic">retDic dictionary</param>
        protected virtual void OutputPayPalLog(IDictionary<string, object> SendValues, IDictionary<string, object> retDic)
        {

            var logText = new StringBuilder();

            logText.AppendFormat("Date:{0}\r\n", DateTime.Now);

            foreach (var key in SendValues.Keys)
            {
                logText.AppendFormat("{0}:{1}\r\n", key, SendValues[key]);
            }

            foreach (var key in retDic.Keys)
            {
                logText.AppendFormat("{0}:{1}\r\n", key, retDic[key]);
            }

            logText.Append("\r\n");

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var dir = new Uri(new Uri(setup.log_path), setup.paypal_log_directory + "\\");

            ErsDirectory.CreateDirectories(dir.LocalPath);

            //'# ファイル名の作成
            var paypaltxt_pass_file = new Uri(dir, DateTime.Now.ToString("yyyyMMdd") + "_" + SendValues["METHOD"] + "log.inc").LocalPath;

            //'# PayPal決済情報の送受信ログ書き込み(同ファイル名があれば追記、無ければ新規作成)
            ErsFile.WriteAll(logText.ToString(), paypaltxt_pass_file, ErsEncoding.ShiftJIS);
        }

        /// <summary>
        /// Initiates an Express Checkout transaction.
        /// </summary>
        /// <param name="model">ErsModelBase</param>
        /// <param name="order">ErsOrder</param>
        /// <param name="k_flg">k_flg</param>
        /// <param name="isMonitor">isMonitor</param>
        /// <returns>RedirectResult</returns>
        public virtual ActionResult SetExpressCheckout(ErsModelBase model, ErsOrder order, int k_flg, bool isMonitor, string returnURL, string cancelURL)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();

            if (isMonitor)
            {
                returnURL += "$monitor_flg=1";
                cancelURL += "$monitor_flg=1";
            }

            this.InitConnection(setup, isMonitor);

            var SendValues = new Dictionary<string, object>();

            SendValues.Add("METHOD", "SetExpressCheckout");
            SendValues.Add("L_NAME0", setup.paypal_item_name);
            SendValues.Add("L_AMT0", order.total.ToString());
            SendValues.Add("L_QTY0", "1");
            SendValues.Add("AMT", order.total.ToString());
            SendValues.Add("RETURNURL", returnURL + _additionalQuery);
            SendValues.Add("CANCELURL", cancelURL + _additionalQuery);
            SendValues.Add("CURRENCYCODE", "JPY");
            SendValues.Add("PAYMENTACTION", setup.paypal_paymenttype);
            SendValues.Add("USER", userName);
            SendValues.Add("PWD", password);
            SendValues.Add("SIGNATURE", signature);
            SendValues.Add("VERSION", setup.paypal_api_version);
            SendValues.Add("ALLOWNOTE", "0");

            SendValues.Add("ADDROVERRIDE", "1"); //PayPalページでの編集不可
            SendValues.Add("SHIPTOCOUNTRYCODE", "JP");

            var shippingAddress = ErsFactory.ersOrderFactory.GetShippingAddressInfo();
            shippingAddress.LoadShippingAddress(order);

            SendValues.Add("SHIPTOZIP", shippingAddress.shipping_zip);//  '20
            SendValues.Add("SHIPTOSTATE", ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(shippingAddress.shipping_pref));// '40
            SendValues.Add("SHIPTOCITY", shippingAddress.shipping_address);// '40
            SendValues.Add("SHIPTOSTREET", shippingAddress.shipping_taddress);//  '100
            SendValues.Add("SHIPTOSTREET2", shippingAddress.shipping_maddress);// '100
            SendValues.Add("SHIPTONAME", shippingAddress.shipping_lname + " " + shippingAddress.shipping_fname);// '100

            if (!setup.onVEX)
            {
                var dicRet = this.HashCall(endPoint, SendValues);

                if ("SUCCESS" != Convert.ToString(dicRet["ack"]).ToUpper())
                {
                    throw new Exception(ErsResources.GetMessage("53201", DateTime.Now, SendValues["METHOD"], dicRet["token"]));
                }

                this.TemporarySaveModel(model, Convert.ToString(dicRet["token"]));

                return new RedirectResult(ecUrl + "?cmd=_express-checkout&token=" + dicRet["token"] + "&locale.x=ja_JP");
            }
            else
            {
                //If this process is for vex, redirects to returnUrl directly.
                this.TemporarySaveModel(model, "vex");

                return new RedirectResult(returnURL + "&token=vex&PayerID=playerVex");
            }
        }

        /// <summary>
        /// Initiates an Express Checkout transaction.
        /// </summary>
        /// <param name="model">ErsModelBase</param>
        /// <param name="order">ErsOrder</param>
        /// <param name="k_flg">k_flg</param>
        /// <param name="isMonitor">isMonitor</param>
        /// <param name="_returnControllerName">String Controller Name</param>
        /// <param name="_cancelControllerName">String Controller Name</param>
        /// <param name="_returnResultName">String ActionResult Name</param>
        /// <param name="_cancelResultName">String ActionResult Name</param>
        /// <param name="addtion_queryString">Dictionary<stining, object></param>
        /// <returns>RedirectResult</returns>
        public virtual ActionResult SetExpressCheckout(ErsModelBase model, ErsOrder order, int k_flg, bool isMonitor,
                                                  string _returnControllerName, string _cancelControllerName,
                                                  string _returnResultName, string _cancelResultName,
                                                  Dictionary<string, object> addtion_queryString = null)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();

            string additional_string = string.Empty;

            string reutrn_queryString = @"?val=" + "ssl_ransu=" + ErsContext.sessionState.Get("ssl_ransu") + "$mcode=" + ErsContext.sessionState.Get("mcode");
            string cancel_queryString = @"?val=" + "ssl_ransu=" + ErsContext.sessionState.Get("ssl_ransu") + "$mcode=" + ErsContext.sessionState.Get("mcode");

            if (addtion_queryString != null)
            {
                foreach (var query in addtion_queryString)
                {
                    string formatQuery = "&{0}={1}";
                    this._additionalQuery += String.Format(formatQuery, query.Key, query.Value);
                }
            }

            var returnURL = setup.sec_url + "top/" + _returnControllerName + "/asp/" + _returnResultName + ".asp" + reutrn_queryString;

            var cancelURL = setup.sec_url + "top/" + _cancelControllerName + "/asp/" + _cancelResultName + ".asp" + cancel_queryString;

            return this.SetExpressCheckout(model, order, k_flg, isMonitor, returnURL, cancelURL);
        }

        /// <summary>
        /// Save the model data to table temporarily.
        /// </summary>
        /// <param name="model">ErsModelBase</param>
        /// <param name="token">token</param>
        protected virtual void TemporarySaveModel(ErsModelBase model, string token)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();

            var repository = ErsFactory.ersOrderFactory.GetErsTpPaypalRepository();
            if (setup.onVEX)
            {
                //If this process is for vex, prepares form_values for vex.
                var criteria = ErsFactory.ersOrderFactory.GetErsTpPaypalCriteria();
                criteria.token = token;
                repository.Delete(criteria);
            }

            var dic = model.GetPropertiesAsDictionary();
            var saveValue = string.Empty;
            //文字列を連結する
            saveValue = ErsCommon.HttpJoinDictionary(dic, delimiter, valueDelimiter);
            //BinableTable連結文字列をくわえる
            string bindJoinValue = ErsCommon.HttpJoinBindTableDictionary(model, delimiter, valueDelimiter);
            saveValue += bindJoinValue.HasValue() ? bindJoinValue.Insert(0, delimiter) : bindJoinValue;

            var tpPaypal = ErsFactory.ersOrderFactory.GetErsTpPaypal();
            tpPaypal.token=token;
            tpPaypal.edate=DateTime.Now;
            tpPaypal.ransu=ErsContext.sessionState.Get("ransu");
            tpPaypal.form_values= saveValue;
            repository.Insert(tpPaypal);
        }

        /// <summary>
        /// Obtains information about an Express Checkout transaction.
        /// </summary>
        /// <param name="models">ErsModelBase</param>
        /// <param name="token">token</param>
        /// <param name="isMonitor">isMonitor</param>
        public virtual void GetExpressCheckout(ErsModelBase[] models, string token, bool isMonitor)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            this.InitConnection(setup, isMonitor);

            var SendValues = new Dictionary<string, object>();

            SendValues.Add("METHOD", "GetExpressCheckoutDetails");
            SendValues.Add("PAYMENTACTION", setup.paypal_paymenttype);
            SendValues.Add("TOKEN", token);
            SendValues.Add("CURRENCYCODE", "JPY");
            SendValues.Add("USER", userName);
            SendValues.Add("PWD", password);
            SendValues.Add("SIGNATURE", signature);
            SendValues.Add("VERSION", setup.paypal_api_version);

            if (!setup.onVEX)
            {
                var dicRet = this.HashCall(endPoint, SendValues);

                if ("SUCCESS" != Convert.ToString(dicRet["ack"]).ToUpper())
                {
                    throw new Exception(ErsResources.GetMessage("53201", DateTime.Now, SendValues["METHOD"], token));
                }
            }

            //テンポラリテーブルからデータを取得
            var repository = ErsFactory.ersOrderFactory.GetErsTpPaypalRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsTpPaypalCriteria();
            criteria.ransu = ErsContext.sessionState.Get("ransu");
            criteria.token = token;
            var listTpPaypal = repository.Find(criteria);
            if (listTpPaypal.Count == 0 || string.IsNullOrEmpty(listTpPaypal.First().form_values))
            {
                throw new ErsException("53203");
            }
            var tpPaypal = listTpPaypal.First();
            var dicForm = ErsCommon.GetDictionaryWithHttpString(tpPaypal.form_values, delimiter, valueDelimiter);
            if (models != null)
            {
                foreach (var model in models)
                {
                    model.OverwriteWithParameter(dicForm);

                    //Overwrite dicForm in bindable model
                    ErsCommon.SetBindTableWithPaypalFormValues(model, dicForm);
                }
            }
        }

        /// <summary>
        /// Completes an Express Checkout transaction.(DoExpressCheckout)
        /// </summary>
        /// <param name="order">ErsOrder</param>
        /// <param name="member">ErsMember</param>
        /// <param name="isMonitor">isMonitor</param>
        public void SendAuth(ErsOrder order, ErsMember member, bool isMonitor)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            this.InitConnection(setup, isMonitor);

            var SendValues = new Dictionary<string, object>();

            SendValues.Add("METHOD", "DoExpressCheckoutPayment");
            SendValues.Add("PAYMENTACTION", setup.paypal_paymenttype);
            SendValues.Add("TOKEN", order.token);
            SendValues.Add("PAYERID", order.PayerID);
            SendValues.Add("AMT", order.total);
            SendValues.Add("CURRENCYCODE", "JPY");
            SendValues.Add("BUTTONSOURCE", "IVP_cart_EC_JP");//  '# IVP用のBNコードをセット
            SendValues.Add("INVNUM", order.d_no);
            SendValues.Add("USER", userName);
            SendValues.Add("PWD", password);
            SendValues.Add("SIGNATURE", signature);
            SendValues.Add("VERSION", setup.paypal_api_version);

            string transactionid = string.Empty;
            if (!setup.onVEX)
            {
                var dicRet = this.HashCall(endPoint, SendValues);

                if ("SUCCESS" != Convert.ToString(dicRet["ack"]).ToUpper())
                {
                    throw new Exception(ErsResources.GetMessage("53201", DateTime.Now, SendValues["METHOD"], order.token));
                }

                transactionid = Convert.ToString(dicRet["transactionid"]);
            }
            else
            {
                transactionid = "onVEX";
            }

            order.c_req_no = transactionid;
        }

        /// <summary>
        /// Captures an authorized payment.
        /// </summary>
        /// <param name="order">ErsOrder</param>
        public void SendCapture(ErsOrder order, int total)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            this.InitConnection(setup, false);

            var SendValues = new Dictionary<string, object>();

            SendValues.Add("METHOD", "DoCapture");
            SendValues.Add("USER", userName);
            SendValues.Add("PWD", password);
            SendValues.Add("SIGNATURE", signature);
            SendValues.Add("VERSION", setup.paypal_api_version);
            SendValues.Add("AUTHORIZATIONID", order.c_req_no);
            SendValues.Add("AMT", total);
            SendValues.Add("COMPLETETYPE", "Complete");
            SendValues.Add("CURRENCYCODE", "JPY");
            SendValues.Add("INVNUM", order.d_no);

            var dicRet = this.HashCall(endPoint, SendValues);

            if ("SUCCESS" != Convert.ToString(dicRet["ack"]).ToUpper())
            {
                throw new Exception(ErsResources.GetMessage("53202", order.d_no, order.c_req_no));
            }

        }

        public void SetPaymentMethod(ErsOrder order, IPaymentInfoContainer container)
        {
            var paymentInfo = container as IPaymentInfoPayPalContainer;
            if (paymentInfo == null)
            {
                throw new Exception(container.GetType().FullName + " has to implement IPaymentInfoPayPalContainer.");
            }

            order.token = paymentInfo.token;
            order.PayerID = paymentInfo.PayerID;
            order.c_req_no = paymentInfo.c_req_no;
        }

        /// <summary>
        /// returns string c_req_no
        /// </summary>
        /// <param name="order">ErsOrder</param>
        /// <returns></returns>
        public string GetTransactionStringForView(ErsOrder order)
        {
            return order.c_req_no;
        }

        /// <summary>
        /// throws new NotImplementedException()
        /// </summary>
        /// <param name="order"></param>
        public void SendAlterAuth(ErsOrder order, int amount)
        {
        }

        /// <summary>
        /// throws new NotImplementedException()
        /// </summary>
        /// <param name="order"></param>
        public void SendVoidAuth(ErsOrder order)
        {
        }
    }
}
