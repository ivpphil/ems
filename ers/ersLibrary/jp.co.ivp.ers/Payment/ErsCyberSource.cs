﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.related;

namespace jp.co.ivp.ers.Payment
{
    public class ErsCyberSource
        : ErsPaymentCreditBase
    {
        protected ICSCOMLib.ICS ics;
        protected Setup setup;

        /// <summary>
        /// initialize
        /// </summary>
        public ErsCyberSource()
        {
            ics = new ICSCOMLib.ICS();
            setup = ErsFactory.ersUtilityFactory.getSetup();
        }

        /// <summary>
        /// Auth information set 
        /// </summary>
        /// <param name="order">ErsOrder</param>
        /// <param name="member">ErsMember</param>
        /// <param name="isMonitor">isMonitor</param>
        public override void SendAuth(ErsOrder order, ErsMember member, bool isMonitor)
        {

            //初期設定
            ics.Merchant = setup.cybersource_merchant;
            ics.ServerHost = (!isMonitor ? setup.cybersource_server : setup.cybersource_server_monitor);

            //与信設定
            ics.SetValue("ics_applications", "ics_auth");

            this.SetOrderValue(order, member);

            //実行処理
            ics.Send();

            //エラーハンドリング
            if (Convert.ToInt64(ics.GetResponseValue("ics_rcode")) > 0)
            {
                this.CatchError();
            }
            else
            {
                //成功時
                var request_id = ics.GetResponseValue("request_id");
                var request_token = ics.GetResponseValue("request_token");

                order.c_req_no = request_id + "-" + request_token;
            }
        }

        /// <summary>
        /// 伝票情報セット
        /// </summary>
        /// <param name="order">ErsOrder</param>
        /// <param name="member">ErsMember</param>
        protected virtual void SetOrderValue(ErsOrder order, ErsMember member)
        {
            //決済情報埋め込み
            ics.SetValue("merchant_ref_number", order.d_no);

            //アドレス情報
            ics.SetValue("bill_address1", member.taddress + member.maddress);
            ics.SetValue("bill_city", member.address);
            ics.SetValue("bill_state", ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(member.pref));
            ics.SetValue("bill_zip", member.zip);
            ics.SetValue("bill_country", "jp");
            ics.SetValue("customer_firstname", member.lname);
            ics.SetValue("customer_lastname", member.fname);
            ics.SetValue("customer_email", member.email);
            ics.SetValue("customer_phone", member.tel);

            //カード情報
            ics.SetValue("customer_cc_number", order.card_info.card_no);
            ics.SetValue("customer_cc_cv_number", order.card_info.security_no);
            ics.SetValue("customer_cc_expmo", order.card_info.validity_m.Value.ToString("{0:D4}"));
            ics.SetValue("customer_cc_expyr", order.card_info.validity_y.Value.ToString());
            ics.SetValue("currency", "JPY");

            //伝票情報
            ics.AddToOffer(0, "offer_id", order.d_no);
            ics.AddToOffer(0, "product_name", "商品一式");
            ics.AddToOffer(0, "product_code", "default");
            ics.AddToOffer(0, "amount", order.subtotal.ToString());
            ics.AddToOffer(0, "quantity", "1");
            ics.AddToOffer(0, "tax_amount", order.tax.ToString());
            ics.AddToOffer(0, "merchant_product_sku", order.d_no);

            ics.AddToOffer(0, "offer_id", "CARRY");
            ics.AddToOffer(0, "product_name", "送料");
            ics.AddToOffer(0, "product_code", "default");
            ics.AddToOffer(0, "amount", order.carriage.ToString());
            ics.AddToOffer(0, "quantity", "1");
            ics.AddToOffer(0, "tax_amount", "0");
            ics.AddToOffer(0, "merchant_product_sku", "CARRY");

            ics.AddToOffer(0, "offer_id", "FEE");
            ics.AddToOffer(0, "product_name", "手数料");
            ics.AddToOffer(0, "product_code", "default");
            ics.AddToOffer(0, "amount", order.etc.ToString());
            ics.AddToOffer(0, "quantity", "1");
            ics.AddToOffer(0, "tax_amount", "0");
            ics.AddToOffer(0, "merchant_product_sku", "FEE");
        }

        /// <summary>
        /// エラー処理
        /// </summary>
        protected virtual void CatchError()
        {
            var rcode2 = ics.GetResponseValue("ics_rflag");
            var rcode5 = ics.GetResponseValue("auth_auth_response");
            var rcode6 = ics.GetResponseValue("ics_rmsg");

            switch (rcode2)
            {
                case "DCARDEXPIRED":
                    throw new ErsException("20206");
                case "DCARDREFUSED":
                    if (rcode5 == "G02")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53002"), rcode5, rcode6);
                    else if (rcode5 == "G03")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53003"), rcode5, rcode6);
                    else if (rcode5 == "G04")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53004"), rcode5, rcode6);
                    else if (rcode5 == "G05")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53005"), rcode5, rcode6);
                    else if (rcode5 == "G22")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53006"), rcode5, rcode6);
                    else if (rcode5 == "G42")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53007"), rcode5, rcode6);
                    else if (rcode5 == "G44")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53045"), rcode5, rcode6);
                    else if (rcode5 == "G56")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53008"), rcode5, rcode6);
                    else if (rcode5 == "G60")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53009"), rcode5, rcode6);
                    else if (rcode5 == "G61")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53010"), rcode5, rcode6);
                    else if (rcode5 == "G65")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53011"), rcode5, rcode6);
                    else if (rcode5 == "G67")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53012"), rcode5, rcode6);
                    else if (rcode5 == "G68")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53013"), rcode5, rcode6);
                    else if (rcode5 == "G69")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53014"), rcode5, rcode6);
                    else if (rcode5 == "G70")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53015"), rcode5, rcode6);
                    else if (rcode5 == "G71")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53016"), rcode5, rcode6);
                    else if (rcode5 == "G72")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53017"), rcode5, rcode6);
                    else if (rcode5 == "G73")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53018"), rcode5, rcode6);
                    else if (rcode5 == "G74")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53019"), rcode5, rcode6);
                    else if (rcode5 == "G75")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53020"), rcode5, rcode6);
                    else if (rcode5 == "G77")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53021"), rcode5, rcode6);
                    else if (rcode5 == "G78")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53022"), rcode5, rcode6);
                    else if (rcode5 == "G79")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53023"), rcode5, rcode6);
                    else if (rcode5 == "G80")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53024"), rcode5, rcode6);
                    else if (rcode5 == "G81")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53025"), rcode5, rcode6);
                    else if (rcode5 == "G96")
                        throw new ErsException("53000", rcode2, ErsResources.GetMessage("53026"), rcode5, rcode6);
                    else
                        throw new Exception(ErsResources.GetMessage("53043", rcode2, rcode5, rcode6));
                case "DINVALIDCARD":
                    throw new ErsException("53047", rcode2, rcode5, rcode6);
                case "DINVALIDDATA":
                    throw new Exception(ErsResources.GetMessage("53028", rcode2, rcode5, rcode6));
                case "DCALL":
                    if (rcode5 == "C78")
                        throw new ErsException("53001", rcode2, ErsResources.GetMessage("53030"), rcode5, rcode6);
                    else if (rcode5 == "G12")
                        throw new ErsException("53001", rcode2, ErsResources.GetMessage("53031"), rcode5, rcode6);
                    else if (rcode5 == "G30")
                        throw new ErsException("53001", rcode2, ErsResources.GetMessage("53032"), rcode5, rcode6);
                    else if (rcode5 == "G54")
                        throw new ErsException("53001", rcode2, ErsResources.GetMessage("53033"), rcode5, rcode6);
                    else if (rcode5 == "G55")
                        throw new ErsException("53001", rcode2, ErsResources.GetMessage("53034"), rcode5, rcode6);
                    else if (rcode5 == "G83")
                        throw new ErsException("53001", rcode2, ErsResources.GetMessage("53035"), rcode5, rcode6);
                    else if (rcode5 == "G95")
                        throw new ErsException("53001", rcode2, ErsResources.GetMessage("53036"), rcode5, rcode6);
                    else if (rcode5 == "G97")
                        throw new ErsException("53001", rcode2, ErsResources.GetMessage("53037"), rcode5, rcode6);
                    else if (rcode5 == "G98")
                        throw new ErsException("53001", rcode2, ErsResources.GetMessage("53038"), rcode5, rcode6);
                    else if (rcode5 == "G99")
                        throw new ErsException("53001", rcode2, ErsResources.GetMessage("53039"), rcode5, rcode6);
                    else
                        throw new Exception(ErsResources.GetMessage("53043", rcode2, rcode5, rcode6));
                case "DMISSINGFIELD":
                    throw new Exception(ErsResources.GetMessage("53040", rcode2, rcode5, rcode6));
                case "ETIMEOUT":
                    throw new Exception(ErsResources.GetMessage("53041", rcode2, rcode5, rcode6));
                case "ESYSTEM":
                    throw new Exception(ErsResources.GetMessage("53042", rcode2, rcode5, rcode6));
                default:
                    throw new Exception(ErsResources.GetMessage("53043", rcode2, rcode5, rcode6));
            }
        }

        public override void SetPaymentMethod(ErsOrder order, IPaymentInfoContainer container)
        {
            var paymentInfo = container as IPaymentInfoCyberSourceContainer;
            if (paymentInfo == null)
            {
                throw new Exception(container.GetType().FullName + " has to implement IPaymentInfoCyberSourceContainer.");
            }

            order.card_info = new CreditCardInfo(paymentInfo.card_holder_name, paymentInfo.card, paymentInfo.cardno, paymentInfo.validity_y, paymentInfo.validity_m, paymentInfo.security_no);

            order.c_req_no = paymentInfo.c_req_no;
        }

        /// <summary>
        /// throws new NotImplementedException
        /// </summary>
        /// <param name="order"></param>
        public override void SendCapture(ErsOrder order, int total)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// returns string c_req_no
        /// </summary>
        /// <param name="order">ErsOrder</param>
        /// <returns></returns>
        public override string GetTransactionStringForView(ErsOrder order)
        {
            return order.c_req_no;
        }

        /// <summary>
        /// throws new NotImplementedException("Cybersource does not have this service").
        /// </summary>
        /// <param name="member">ErsMember</param>
        /// <returns></returns>
        public override List<CreditCardInfo> ObtainMemberCardInfo(ErsMember member)
        {
            throw new NotImplementedException("Cybersource does not have this service.");
        }

        /// <summary>
        ///  throws new NotImplementedException("Cybersource does not have this service").
        /// </summary>
        /// <param name="member">ErsMember</param>
        /// <param name="card_sequence">card_sequence</param>
        /// <returns></returns>
        public override CreditCardInfo ObtainMemberCardInfo(ErsMember member, string card_sequence)
        {
            throw new NotImplementedException("Cybersource does not have this service.");
        }

        /// <summary>
        /// throws new NotImplementedException("Cybersource does not have this service").
        /// </summary>
        /// <param name="card_info">CreditCardInfo</param>
        /// <param name="member">ErsMember</param>
        /// <returns></returns>
        public override ErsMemberCard SaveMemberCardInfo(CreditCardInfo card_info, ErsMember member)
        {
            throw new NotImplementedException("Cybersource does not have this service.");
        }

        /// <summary>
        /// throws new NotImplementedException
        /// </summary>
        /// <param name="card_info">CreditCardInfo</param>
        /// <param name="member">ErsMember</param>
        /// <returns></returns>
        public override ErsMemberCard deleteCardInfo(CreditCardInfo card_info, ErsMember member)
        {
            throw new NotImplementedException("Cybersource does not have this service.");
        }

        /// <summary>
        /// throws new NotImplementedException
        /// </summary>
        /// <param name="order">ErsOrder</param>
        public override void SendAlterAuth(ErsOrder order, int amount)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// throws new NotImplementedException
        /// </summary>
        /// <param name="order">ErsOrder</param>
        public override void SendVoidAuth(ErsOrder order)
        {
            throw new NotImplementedException();
        }
    }
}
