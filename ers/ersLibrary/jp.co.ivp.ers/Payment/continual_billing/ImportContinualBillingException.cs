﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment.continual_billing
{
    public class ImportContinualBillingException
        : Exception
    {
        public ImportContinualBillingException(string message)
            : base(message)
        {

        }
    }
}
