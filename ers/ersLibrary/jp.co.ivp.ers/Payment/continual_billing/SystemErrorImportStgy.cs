﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.Payment.continual_billing
{
    public class SystemErrorImportStgy : IImportContinualBillingResultStgy
    {
        /// <summary>
        /// 失敗を取り込む
        /// </summary>
        /// <param name="model"></param>
        public virtual void Import(IContinualBillingResult model)
        {
            this.InsertRegularError(model);
        }

        /// <summary>
        /// エラーをDBへ保持する。
        /// </summary>
        /// <param name="model"></param>
        /// <param name="memberCard"></param>
        private void InsertRegularError(IContinualBillingResult model)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.credit_order_id = model.credit_order_id;

            var objOrder = repository.FindSingle(criteria);

            if (objOrder == null)
            {
                throw new Exception("Specified credit_order_id is not defined.[" + model.credit_order_id + "]");
            }

            var regularErrorRepository = ErsFactory.ersOrderFactory.GetErsRegularErrLogRepository();

            var regularError = ErsFactory.ersOrderFactory.GetErsRegularErrLog();

            regularError.mcode = objOrder.mcode;
            regularError.card_mcode = model.card_mcode;
            regularError.error_description = "result = " + model.process_result + "; authory_result=" + model.authory_result;
            regularError.disp_flg = EnumRegularErrLogDispFlg.SystemError;
            regularError.active = EnumActive.Active;
            regularError.occured_date = DateTime.Now;
            regularError.credit_order_id = model.credit_order_id;

            regularErrorRepository.Insert(regularError, true);

            throw new Exception("不正な入力値エラー(" + regularError.error_description + ")");
        }
    }
}
