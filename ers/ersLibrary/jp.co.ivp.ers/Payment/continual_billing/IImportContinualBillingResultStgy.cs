﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment.continual_billing
{
    public interface IImportContinualBillingResultStgy
    {
        void Import(IContinualBillingResult model);
    }
}
