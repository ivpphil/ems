﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.Payment.continual_billing
{
    public class FailedImportStgy : IImportContinualBillingResultStgy
    {
        /// <summary>
        /// 失敗を取り込む
        /// </summary>
        /// <param name="model"></param>
        public virtual void Import(IContinualBillingResult model)
        {
            var mcode = this.GetMcode(model);

            var description = string.Format("継続課金エラー：{0}（会員番号：{1}）",
                ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ContinualBilling, EnumCommonNameColumnName.namename, (int)model.process_result),
                mcode);

            this.InsertRegularError(model, mcode, description);

            this.InsertCtsEnquiry(model, mcode, description);

            throw new ImportContinualBillingException("課金処理の失敗(" + description + ")");
        }

        private string GetMcode(IContinualBillingResult model)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.credit_order_id = model.credit_order_id;

            var objOrder = repository.FindSingle(criteria);

            if (objOrder == null)
            {
                throw new Exception("Specified credit_order_id is not defined.[" + model.credit_order_id + "]");
            }
            return objOrder.mcode;
        }

        /// <summary>
        /// エラーをDBへ保持する。
        /// </summary>
        /// <param name="model"></param>
        /// <param name="memberCard"></param>
        private void InsertRegularError(IContinualBillingResult model, string mcode, string description)
        {
            var regularErrorRepository = ErsFactory.ersOrderFactory.GetErsRegularErrLogRepository();

            var regularError = ErsFactory.ersOrderFactory.GetErsRegularErrLog();

            regularError.mcode = mcode;
            regularError.card_mcode = model.card_mcode;
            regularError.error_description = description + "; オーソリー結果=" + model.authory_result;
            regularError.disp_flg = EnumRegularErrLogDispFlg.PaymentError;
            regularError.active = EnumActive.Active;
            regularError.occured_date = DateTime.Now;
            regularError.credit_order_id = model.credit_order_id;

            regularErrorRepository.Insert(regularError, true);
        }

        private void InsertCtsEnquiry(IContinualBillingResult model, string mcode, string description)
        {
            var ctsEnquiryRepository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();

            var ctsEnquiry = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiry();

            ctsEnquiry.mcode = mcode;
            ctsEnquiry.intime = DateTime.Now;
            ctsEnquiry.enq_casename = description;
            ctsEnquiry.card_error_flg = EnumCardErrFlg.Err;

            ctsEnquiryRepository.Insert(ctsEnquiry, true);

            var ctsEnquiryDetailRepository = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetailRepository();
            var ctsEnquiryDetail = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetail();
            ctsEnquiryDetail.case_no = ctsEnquiry.case_no;
            ctsEnquiryDetail.enq_detail = description + "; オーソリー結果=" + model.authory_result;

            ctsEnquiryDetailRepository.Insert(ctsEnquiryDetail, true);
        }
    }
}
