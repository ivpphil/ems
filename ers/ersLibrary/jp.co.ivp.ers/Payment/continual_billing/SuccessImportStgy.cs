﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment.continual_billing
{
    public class SuccessImportStgy : IImportContinualBillingResultStgy
    {
        public virtual void Import(IContinualBillingResult model)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.credit_order_id = model.credit_order_id;

            var objOrder = repository.FindSingle(criteria);

            if (objOrder == null)
            {
                throw new Exception("Specified credit_order_id is not defined.[" + model.credit_order_id + "]");
            }

            var objOldOrder = ErsFactory.ersOrderFactory.GetErsOrder();
            objOldOrder.OverwriteWithParameter(objOrder.GetPropertiesAsDictionary());

            objOrder.order_payment_status = EnumOrderPaymentStatusType.PAID;
            objOrder.paid_date = DateTime.Now;
            objOrder.paid_price = Convert.ToInt32(model.total);

            repository.Update(objOldOrder, objOrder);
        }

    }
}
