﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment.continual_billing
{
    public class ImportContinualBillingResultStgyFactory
    {
        public IImportContinualBillingResultStgy GetImporter(IContinualBillingResult model)
        {

            switch (model.process_result)
            {
                case EnumContinualBillingResult.AuthoryError:
                case EnumContinualBillingResult.SaleError:
                    return new FailedImportStgy();

                case EnumContinualBillingResult.ValueError:
                    return new SystemErrorImportStgy();

                case EnumContinualBillingResult.Success:
                    return new SuccessImportStgy();

                default:
                    throw new Exception("Specified process_result code is not defined.[" + model.process_result + "]");
            }
        }

    }
}
