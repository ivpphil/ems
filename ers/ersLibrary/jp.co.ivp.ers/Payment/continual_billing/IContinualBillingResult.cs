﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Payment.continual_billing
{
    public interface IContinualBillingResult
        : IErsModelBase
    {
        /// <summary>
        /// 会員ID
        /// </summary>
        string card_mcode { get; set; }

        /// <summary>
        /// カード登録連番
        /// </summary>
        string card_sequence { get; set; }

        /// <summary>
        /// オーダーID
        /// </summary>
        string credit_order_id { get; }

        /// <summary>
        /// 処理結果
        /// </summary>
        EnumContinualBillingResult? process_result { get; }

        /// <summary>
        /// 利用金額
        /// </summary>
        int? total { get; set; }

        /// <summary>
        /// オーソリ結果
        /// </summary>
        string authory_result { get; set; }
    }
}
