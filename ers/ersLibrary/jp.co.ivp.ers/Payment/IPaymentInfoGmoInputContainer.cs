﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment
{
    public interface IPaymentInfoGmoInputContainer
        : IPaymentInfoContainer
    {
        int? card_id { get; set; }

        string card_holder_name { get; set; }

        int? card { get; set; }

        string cardno { get; set; }

        int? validity_y { get; set; }

        int? validity_m { get; set; }

        string security_no { get; set; }
    }
}
