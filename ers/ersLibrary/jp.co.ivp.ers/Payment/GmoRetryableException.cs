﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment
{
    public class GmoRetryableException
        : Exception
    {
        public virtual List<string> ErrorInfoList { get; protected set; }

        /// <summary>
        /// populates ErrorInfoList
        /// </summary>
        /// <param name="ErrCode">Error</param>
        /// <param name="ErrInfo">Error Description</param>
        public GmoRetryableException(string ErrCode, string ErrInfo)
        {
            var arrErrInfo = ErrInfo.Split(new[] { '|' });

            this.ErrorInfoList = new List<string>();

            for (var i = 0; i < arrErrInfo.Length; i++)
            {
                ErrorInfoList.Add(arrErrInfo[i]);
            }
        }
    }
}
