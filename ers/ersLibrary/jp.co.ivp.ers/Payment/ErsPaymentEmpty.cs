﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.Payment
{
    //何もしないPaymentクラス
    public class ErsPaymentEmpty
        : IErsPaymentBase
    {
        /// <summary>
        /// 与信取得
        /// </summary>
        /// <param name="order">ErsOrder</param>
        /// <param name="member">ErsMember</param>
        /// <param name="isMonitor">isMonitor</param>
        public void SendAuth(ErsOrder order, ErsMember member, bool isMonitor)
        {
        }

        /// <summary>
        /// 与信取得
        /// </summary>
        /// <param name="order">ErsOrder</param>
        public void SendCapture(ErsOrder order, int total)
        {
        }

        public void SetPaymentMethod(ErsOrder order, IPaymentInfoContainer container)
        {

        }

        /// <summary>
        /// returns empty string
        /// </summary>
        /// <param name="order">ErsOrder</param>
        /// <returns></returns>
        public string GetTransactionStringForView(ErsOrder order)
        {
            return string.Empty;
        }

        /// <summary>
        /// throws new NotImplementedException
        /// </summary>
        /// <param name="order">ErsOrder</param>
        public void SendAlterAuth(ErsOrder order, int amount)
        {
        }

        /// <summary>
        /// throws new NotImplementedException
        /// </summary>
        /// <param name="order">ErsOrder</param>
        public void SendVoidAuth(ErsOrder order)
        {
        }
    }
}
