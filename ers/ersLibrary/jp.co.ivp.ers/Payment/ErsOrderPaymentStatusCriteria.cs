﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.Payment
{
    public class ErsOrderPaymentStatusCriteria
        : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("order_payment_status_t.id", value, Operation.EQUAL));
            }
        }

        internal void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("order_payment_status_t.id", orderBy);
        }
    }
}
