﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.related;

namespace jp.co.ivp.ers.Payment
{
    public interface IErsPaymentBase
    {
        /// <summary>
        /// 与信取得
        /// </summary>
        /// <param name="order"></param>
        /// <param name="member"></param>
        /// <param name="isMonitor"></param>
        void SendAuth(ErsOrder order, ErsMember member, bool isMonitor);

        /// <summary>
        /// 与信情報変更
        /// </summary>
        /// <param name="order"></param>
        void SendAlterAuth(ErsOrder order, int amount);

        /// <summary>
        /// 与信取消
        /// </summary>
        /// <param name="order"></param>
        void SendVoidAuth(ErsOrder order);

        /// <summary>
        /// 売上を立てる
        /// </summary>
        /// <param name="order"></param>
        /// <param name="type"></param>
        /// <param name="inputParameters"></param>
        void SendCapture(ErsOrder order, int total);

        /// <summary>
        /// 決済情報取得
        /// </summary>
        /// <param name="order"></param>
        /// <param name="type"></param>
        /// <param name="inputParameters"></param>
        void SetPaymentMethod(ErsOrder order, IPaymentInfoContainer container);

        /// <summary>
        /// 表示用の決済IDを取得する。
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        string GetTransactionStringForView(ErsOrder order);
    }
}
