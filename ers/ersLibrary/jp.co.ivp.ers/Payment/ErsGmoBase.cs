﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using System.Net;
using System.IO;
using jp.co.ivp.ers.db.table;
using System.Web;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order.related;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.Payment
{
    /// <summary>
    /// Provides the methods that controll GMO transaction.
    /// </summary>
    public class ErsGmoBase
    {
        protected virtual string connect_url { get; set; }
        protected virtual string shop_id { get; set; }
        protected virtual string shop_password { get; set; }
        protected virtual string site_id { get; set; }
        protected virtual string site_password { get; set; }

        /// <summary>
        /// Configure a connection to GMO.
        /// </summary>
        /// <param name="setup">Setup</param>
        /// <param name="isMonitor">isMonitor</param>
        protected virtual void InitConnection(Setup setup, bool isMonitor)
        {
            if (!isMonitor)
            {
                this.connect_url = setup.gmo_connect_url;
                this.shop_id = setup.gmo_shop_id;
                this.shop_password = setup.gmo_shop_password;
                this.site_id = setup.gmo_site_id;
                this.site_password = setup.gmo_site_password;
            }
            else
            {
                this.connect_url = setup.gmo_connect_url_monitor;
                this.shop_id = setup.gmo_shop_id_monitor;
                this.shop_password = setup.gmo_shop_password_monitor;
                this.site_id = setup.gmo_site_id_monitor;
                this.site_password = setup.gmo_site_password_monitor;
            }

        }

        /// <summary>
        /// Send request to GMO.
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="SendValues">SendValues dictionary</param>
        /// <returns></returns>
        protected virtual IDictionary<string, object> HashCall(string url, Dictionary<string, object> SendValues, Encoding encoding = null)
        {
            if (encoding == null)
            {
                encoding = System.Text.Encoding.UTF8;
            }

            //値をチェック
            var retDic = ErsHttpClient.Post(url, SendValues, encoding);

            ErsGmoBase.OutputGmoLog(SendValues, retDic);

            return retDic;

        }

        /// <summary>
        /// Logging result of request.
        /// </summary>
        /// <param name="SendValues">SendValues dictionary</param>
        /// <param name="retDic">retDic dictionary</param>
        public static void OutputGmoLog(IDictionary<string, object> SendValues, IDictionary<string, object> retDic)
        {

            var logText = new StringBuilder();

            logText.AppendFormat("Date:{0}\r\n", DateTime.Now);

            if (SendValues != null)
            {
                logText.AppendFormat("Send\r\n", DateTime.Now);

                foreach (var key in SendValues.Keys)
                {
                    //CardNoは落とさない
                    if (key != "CardNo")
                    {
                        logText.AppendFormat("{0}:{1}\r\n", key, SendValues[key]);
                    }
                }
            }

            if (retDic != null)
            {
                logText.AppendFormat("Receive\r\n", DateTime.Now);

                foreach (var key in retDic.Keys)
                {
                    logText.AppendFormat("{0}:{1}\r\n", key, retDic[key]);
                }
            }

            logText.Append("\r\n");

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var dir = new Uri(new Uri(setup.log_path), setup.gmo_log_directory + "\\");

            ErsDirectory.CreateDirectories(dir.LocalPath);

            //'# ファイル名の作成
            var gmotxt_pass_file = new Uri(dir, DateTime.Now.ToString("yyyyMMdd") + "_log.inc").LocalPath;

            //'# PayPal決済情報の送受信ログ書き込み(同ファイル名があれば追記、無ければ新規作成)
            ErsFile.WriteAll(logText.ToString(), gmotxt_pass_file, ErsEncoding.ShiftJIS);
        }

        /// <summary>
        /// Return Base Dictionary 
        /// </summary>
        /// <returns></returns>
        protected virtual Dictionary<string, object> GetBaseDictionary()
        {
            var SendValues = new Dictionary<string, object>();

            SendValues.Add("ShopID", this.shop_id);
            SendValues.Add("ShopPass", this.shop_password);
            SendValues.Add("SiteID", this.site_id);
            SendValues.Add("SitePass", this.site_password);

            return SendValues;
        }
    }
}
