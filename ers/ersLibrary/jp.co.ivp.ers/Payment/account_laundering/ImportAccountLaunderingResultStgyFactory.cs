﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment.account_laundering.strategy
{
    public class ImportAccountLaunderingResultStgyFactory
    {
        public IImportAccountLaunderingResultStgy GetImporter(IAccountLaunderingResult model)
        {

            switch (model.result)
            {
                case EnumAccountLaunderingResult.Fail:
                case EnumAccountLaunderingResult.MatchingError:
                    return new FailedImportStgy();
                
                case EnumAccountLaunderingResult.ValueError:
                    return new SystemErrorImportStgy();
                
                default:
                    //何もしない
                    return new SuccessImportStgy();
            }
        }

    }
}
