﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment.account_laundering.strategy
{
    public class SuccessImportStgy : IImportAccountLaunderingResultStgy
    {
        public virtual void Import(IAccountLaunderingResult model)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
            criteria.card_mcode = model.card_mcode;
            criteria.card_sequence = model.card_sequence;

            var objMemberCard = repository.FindSingle(criteria);
            if (objMemberCard == null)
            {
                throw new Exception("Specified card_mcode and card_sequence is not defined.[" + model.card_mcode + ", " + model.card_sequence + "]");
            }

            var oldMemberCard = ErsFactory.ersMemberFactory.GetErsMemberCard();
            oldMemberCard.OverwriteWithParameter(objMemberCard.GetPropertiesAsDictionary());

            objMemberCard.update_status = EnumCardUpdateStatus.Success;

            repository.Update(oldMemberCard, objMemberCard);
        }

    }
}
