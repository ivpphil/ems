﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.Payment.account_laundering.strategy
{
    public interface IImportAccountLaunderingResultStgy
    {
        void Import(IAccountLaunderingResult model);
    }
}
