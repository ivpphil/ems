﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.Payment.account_laundering.strategy
{
    public class SystemErrorImportStgy : IImportAccountLaunderingResultStgy
    {
        /// <summary>
        /// 失敗を取り込む
        /// </summary>
        /// <param name="model"></param>
        public virtual void Import(IAccountLaunderingResult model)
        {
            this.InsertRegularError(model);
        }

        /// <summary>
        /// エラーをDBへ保持する。
        /// </summary>
        /// <param name="model"></param>
        /// <param name="memberCard"></param>
        private void InsertRegularError(IAccountLaunderingResult model)
        {
            var mcode = this.UpdateMemberCard(model);

            var regularErrorRepository = ErsFactory.ersOrderFactory.GetErsRegularErrLogRepository();

            var regularError = ErsFactory.ersOrderFactory.GetErsRegularErrLog();

            var description = string.Format("カード洗い替えエラー：{0}（会員番号：{1}）",
                ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.AccountLaundering, EnumCommonNameColumnName.namename, (int)model.result),
                mcode);

            regularError.mcode = mcode;
            regularError.card_mcode = model.card_mcode;
            regularError.error_description = description;
            regularError.card_sequence = model.card_sequence;
            regularError.disp_flg = EnumRegularErrLogDispFlg.SystemError;
            regularError.active = EnumActive.Active;
            regularError.occured_date = DateTime.Now;

            regularErrorRepository.Insert(regularError, true);

            throw new Exception("Receipt Invalid input result.(" + regularError.error_description + ")");
        }

        private string UpdateMemberCard(IAccountLaunderingResult model)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
            criteria.card_mcode = model.card_mcode;
            criteria.card_sequence = model.card_sequence;

            var objMemberCard = repository.FindSingle(criteria);
            if (objMemberCard == null)
            {
                throw new Exception("Specified card_mcode and card_sequence is not defined.[" + model.card_mcode + ", " + model.card_sequence + "]");
            }

            var oldMemberCard = ErsFactory.ersMemberFactory.GetErsMemberCard();
            oldMemberCard.OverwriteWithParameter(objMemberCard.GetPropertiesAsDictionary());

            objMemberCard.update_status = EnumCardUpdateStatus.Error;

            repository.Update(oldMemberCard, objMemberCard);

            return objMemberCard.mcode;
        }
    }
}
