﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Payment.account_laundering
{
    public interface IAccountLaunderingResult
        : IErsModelBase
    {
        EnumAccountLaunderingResult? result { get; }

        string card_mcode { get; }

        string card_sequence { get; }
    }
}
