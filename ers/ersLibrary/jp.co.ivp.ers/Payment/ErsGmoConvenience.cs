﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.Payment
{
    public class ErsGmoConvenience
        : ErsGmoBase, IErsPaymentBase
    {
        public void SendAuth(ErsOrder order, ErsMember member, bool isMonitor)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            this.InitConnection(setup, isMonitor);

            //受注番号の重複を避けるため、伝票番号にシーケンスを付加します。
            order.credit_order_id = ErsFactory.ersOrderFactory.GetErsOrderRepository().GetNextCreditOrderId(order.d_no);

            while (true)
            {
                try
                {
                    this.SendEntryTransaction(order);
                }
                catch (GmoRetryableException gmoException)
                {
                    throw new GmoException(gmoException.ErrorInfoList);
                }
                break;
            }

            while (true)
            {
                try
                {
                    this.SendAuthTransaction(order);
                }
                catch (GmoRetryableException gmoException)
                {
                    throw new GmoException(gmoException.ErrorInfoList);
                }
                break;
            }
        }

        /// <summary>
        /// Get entry data for this transaction.
        /// </summary>
        /// <param name="order">ErsOrder</param>
        /// <returns></returns>
        protected virtual void SendEntryTransaction(ErsOrder order)
        {
            var SendValues = GetBaseDictionary();

            SendValues.Add("OrderID", order.credit_order_id);
            SendValues.Add("Amount", Convert.ToString(order.total));

            var dicRet = this.HashCall(connect_url + "payment/EntryTranCvs.idPass", SendValues);

            if (dicRet.ContainsKey("errcode") || dicRet.ContainsKey("errinfo"))
            {
                throw new GmoRetryableException(Convert.ToString(dicRet["errcode"]), Convert.ToString(dicRet["errinfo"]));
            }

            order.access_id = Convert.ToString(dicRet["accessid"]);
            order.access_pass = Convert.ToString(dicRet["accesspass"]);
        }

        /// <summary>
        /// Send Authorize transaction.
        /// </summary>
        /// <param name="order">ErsOrder</param>
        protected virtual void SendAuthTransaction(ErsOrder order)
        {
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();

            var SendValues = GetBaseDictionary();

            SendValues.Add("AccessID", order.access_id);
            SendValues.Add("AccessPass", order.access_pass);
            SendValues.Add("OrderID", order.credit_order_id);
            SendValues.Add("Convenience", string.Format("{0:D5}", (int?)order.conv_code));
            SendValues.Add("CustomerName", VBStrings.Left(ErsCommon.ConvertToFull(order.lname + " " + order.fname), 40));
            SendValues.Add("CustomerKana", VBStrings.Left(ErsCommon.ConvertToFull(order.lnamek + " " + order.fnamek), 40));
            SendValues.Add("TelNo", order.tel);
            SendValues.Add("ReceiptsDisp11", setup.ContactInformationName);//お問合せ先(42byte)
            SendValues.Add("ReceiptsDisp12", setup.ContactInformationTel);//お問合せ先電話番号(12byte)
            SendValues.Add("ReceiptsDisp13", setup.ContactInformationTime);//お問合せ先受付時間(11byte)

            var dicRet = this.HashCall(connect_url + "payment/ExecTranCvs.idPass", SendValues, ErsEncoding.ShiftJIS);

            if (dicRet.ContainsKey("errcode") || dicRet.ContainsKey("errinfo"))
            {
                throw new GmoRetryableException(Convert.ToString(dicRet["errcode"]), Convert.ToString(dicRet["errinfo"]));
            }

            if (dicRet.ContainsKey("ConfNo"))
                order.conv_conf_no = (string)dicRet["ConfNo"];

            if (dicRet.ContainsKey("ReceiptNo"))
                order.conv_receipt_no = (string)dicRet["ReceiptNo"];

            if (dicRet.ContainsKey("PaymentTerm") && !string.IsNullOrEmpty((string)dicRet["PaymentTerm"]))
                order.conv_payment_term = DateTime.ParseExact((string)dicRet["PaymentTerm"], "yyyyMMddHHmmss", null);
        }

        public void SendAlterAuth(ErsOrder order, int amount)
        {
            //キャンセル不可
        }

        public void SendVoidAuth(ErsOrder order)
        {
            //キャンセル不可
        }

        public void SendCapture(ErsOrder order, int total)
        {
            //売上は通知にて伝送されてくる
        }

        public void SetPaymentMethod(ErsOrder order, IPaymentInfoContainer container)
        {
            var paymentInfo = container as IPaymentInfoGmoConvenienceContainer;
            if (paymentInfo == null)
            {
                throw new Exception(container.GetType().FullName + " has to implement IPaymentInfoGmoConvenienceContainer.");
            }

            order.conv_code = paymentInfo.conv_code;
        }

        public string GetTransactionStringForView(ErsOrder order)
        {
            return order.access_id;
        }
    }
}
