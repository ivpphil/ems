﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Payment
{
    public class ErsCard
        : ErsRepositoryEntity
    {
        /// <summary>
        /// Initialize ErsCard
        /// </summary>
        public ErsCard()
        {
        }
        public override int? id { get; set; }
        public virtual string card_name { get; set; }
        public virtual EnumActive? active { get; set; }
        public virtual int? site_id { get; set; }

        /// <summary>
        /// Return ErsCard Dictionary 
        /// </summary>
        /// <returns></returns>
        public virtual Dictionary<string, object> getDictionary()
        {
            Dictionary<string, object> tmpDic = new Dictionary<String, object>();
            tmpDic.Add("id", id);
            tmpDic.Add("card_name", card_name);
            tmpDic.Add("active", (int?)active);
            return tmpDic;
        }
    }
}
