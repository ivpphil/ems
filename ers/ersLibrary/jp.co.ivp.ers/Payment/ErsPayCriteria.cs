﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.Payment
{
    public class ErsPayCriteria
        : Criteria
    {
        /// <summary>
        /// Active Criteria
        /// </summary>
        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("active", (int?)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Id Criteria
        /// </summary>
        public EnumPaymentType? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("id", (int?)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Not equal to id criteria
        /// </summary>
        public EnumPaymentType? id_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("id", (int?)value, Operation.NOT_EQUAL));
            }
        }

        public int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("site_id", (int?)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Sort record by Id 
        /// </summary>
        /// <param name="orderBy"></param>
        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("id", orderBy);
        }

        /// <summary>
        /// Sort record by disp_order 
        /// </summary>
        /// <param name="orderBy"></param>
        public void SetOrderByDispOrder(OrderBy orderBy)
        {
            this.AddOrderBy("disp_order", orderBy);
        }
    }
}
