﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Payment
{
    public class ErsTpPaypalRepository
        : ErsRepository<ErsTpPaypal>
    {
        public ErsTpPaypalRepository()
            : base("tp_paypal_t")
        {
        }
    }
}
