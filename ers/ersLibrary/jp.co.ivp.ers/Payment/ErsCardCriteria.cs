﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Payment
{
    public class ErsCardCriteria
        : Criteria
    {
        /// <summary>
        /// Crtiria id for ErsCard
        /// </summary>
        public int id
        {
            set
            {
                this.Add(Criteria.GetCriterion("id", value, Operation.EQUAL));
            }
        }

        public int site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Criteria not equal to Id
        /// </summary>
        public int id_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("id", value, Operation.NOT_EQUAL));
            }
        }

        /// <summary>
        /// Sort record by Id 
        /// </summary>
        /// <param name="orderBy"></param>
        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("id", orderBy);
        }
    }
}
