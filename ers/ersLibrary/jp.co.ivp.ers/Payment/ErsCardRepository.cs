﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.Payment
{
    public class ErsCardRepository : ErsRepository<ErsCard>
    {
        /// <summary>
        /// initialize ErsCardRepository
        /// </summary>
        public ErsCardRepository()
            : base("card_t")
        {
        }

        /// <summary>
        /// Get record by criteria and return  by Ilist 
        /// </summary>
        /// <param name="criteria">Criteria</param>
        /// <returns></returns>
        public override IList<ErsCard> Find(Criteria criteria)
        {
            //返却するリスト
            IList<ErsCard> retList = new List<ErsCard>();
            //データ取得
            var allData = this.ersDB_table.gSelect(criteria);
            //ディクショナリをもとに戻す
            foreach (var data in allData)
            {
                var tmpData = ErsFactory.ersOrderFactory.GetErsCardWithParameter(data);
                retList.Add(tmpData);
            }
            return retList;
        }
    }
}
