﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.Payment
{
    public class ErsOrderPaymentStatusRepository
        : ErsRepository<ErsOrderPaymentStatus>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsOrderPaymentStatusRepository()
            : base("order_payment_status_t")
        {
        }
    }
}
