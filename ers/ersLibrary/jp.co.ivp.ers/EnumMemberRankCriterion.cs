﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumMemberRankCriterion
    {
        /// <summary>
        /// 0: the number of purchases
        /// </summary>
        PurchaseTimes = 0,

        /// <summary>
        /// 1: the total of purchases
        /// </summary>
        PuchaseTotal = 1
    }
}
