﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers
{
    public enum EnumBatchStatus : short
    {
        Success = 0,
        Executed = 1,
        Error = 2
    }
}
