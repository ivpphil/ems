﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.ranking
{
    public class ErsRankingRepository
        : ErsRepository<ErsRanking>
    {
        public ErsRankingRepository()
            : base("ranking_t") 
        { 
        }

        public ErsRankingRepository(ErsDatabase objDB)
            : base("ranking_t", objDB)
        {
        }
    }
}
