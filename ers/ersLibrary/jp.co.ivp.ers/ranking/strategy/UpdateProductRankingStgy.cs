﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.ranking.strategy
{
    public class UpdateProductRankingStgy
        : ISpecificationForSQL
    {
        /// <summary>
        /// Update Product Ranking Into NonActive
        /// </summary>
        /// <param name="EnumActive"></param>
        /// <param name="rank_type "></param>
        public virtual void NonActive(EnumActive active, EnumRankType? rank_type)
        {
            this.setActive = EnumActive.NonActive;
            this.active = active;
            this.rank_type = rank_type;
            ErsRepository.UpdateSatisfying(this, null);
        }

        /// <summary>
        /// Update Product Ranking Into Active
        /// </summary>
        /// <param name="EnumActive"></param>
        /// <param name="rank_type "></param>
        public virtual void Active(EnumActive active, EnumRankType? rank_type)
        {
            this.setActive = EnumActive.Active;
            this.active = active;
            this.rank_type = rank_type;
            ErsRepository.UpdateSatisfying(this, null);
        }

        protected virtual EnumActive setActive { get; set; }
        protected virtual EnumActive active { get; set; }
        protected virtual EnumRankType? rank_type { get; set; }

        public virtual string asSQL()
        {
            var retString = "UPDATE ranking_t SET active = " + (int)setActive + " WHERE rank_type = '" + (int)rank_type + "' AND active = " + (int)active;

            return retString;
        }
    }
}
