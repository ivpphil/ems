﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.ranking
{
    public class ErsRankingCriteria
        : Criteria
    {
        public int id
        {
            set
            {
                Add(Criteria.GetCriterion("ranking_t.id", value, Operation.EQUAL));
            }
        }

        public EnumActive? rank_active
        {
            set
            {
                this.Add(Criteria.GetCriterion("ranking_t.active", value, Operation.EQUAL));
            }
        }

        public EnumRankType? rank_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("ranking_t.rank_type", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Sets the criteria for g_master_t.site_id
        /// </summary>
        public virtual int? g_site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("g_master_t.site_id", value, Operation.ANY_EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("g_master_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.ANY_EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

        /// <summary>
        /// Sets the criteria for ranking_t.site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("ranking_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("ranking_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

        public void SetOrderBySum_result(OrderBy orderBy)
        {
            this.AddOrderBy("ranking_t.sum_result", orderBy);
        }
    }
}
