﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.ranking
{
    public class ErsRanking
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public virtual DateTime? sum_date { get; set; }

        public virtual string gcode { get; set; }

        public virtual int? sum_result { get; set; }

        public virtual DateTime? intime { get; set; }

        public virtual DateTime? utime { get; set; }

        public virtual EnumActive? active { get; set; }

        public virtual EnumRankType? rank_type { get; set; }

        public virtual int? site_id { get; set; }
    }
}
