﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.ranking.strategy;

namespace jp.co.ivp.ers.ranking
{
    public class ErsRankingFactory
    {
        public ErsRanking GetErsRanking()
        {
            return new ErsRanking();
        }

        public ErsRankingRepository GetErsRankingRepository()
        {
            return new ErsRankingRepository();
        }

        public ErsRankingCriteria GetErsRankingCriteria()
        {
            return new ErsRankingCriteria();
        }


        public virtual ErsRanking GetErsRankingWithParameter(Dictionary<string, object> parameter)
        {
            var ranking = this.GetErsRanking();
            ranking.OverwriteWithParameter(parameter);
            return ranking;
        }

        public virtual ErsRanking GetErsRankingWithId(int id)
        {
            var repository = this.GetErsRankingRepository();
            var criteria = this.GetErsRankingCriteria();
            criteria.id = id;

            if (repository.GetRecordCount(criteria) != 1)
                return null;

            var list = repository.Find(criteria);
            return list[0];
        }

        public UpdateProductRankingStgy GetUpdateProductRankingStgy()
        {
            return new UpdateProductRankingStgy();
        }
    }
}
