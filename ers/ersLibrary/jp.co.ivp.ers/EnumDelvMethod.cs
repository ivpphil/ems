﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumDelvMethod
        : short
    {
        Express = 1,
        Mail,
        ExpressAndMail
    }
}
