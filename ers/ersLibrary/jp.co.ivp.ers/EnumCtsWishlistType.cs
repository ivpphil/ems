﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// 1:AG, 2:SV
    /// </summary>
    public enum EnumCtsWishlistType
    {
        /// <summary>
        /// 1
        /// </summary>
        AG = 1,

        /// <summary>
        /// 2
        /// </summary>
        SV = 2
    }
}
