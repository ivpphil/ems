﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// DM要不要
    /// </summary>
    public enum EnumOutBoundFlg
        : short
    {
        NoNeed = 0,
        Need = 1
    }
}
