﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace ers.jp.co.ivp.ers.employee
{
    public  class ErsDReportRepository:ErsRepository<ErsDReport>
    {
        public ErsDReportRepository() : base("dailyreport_details_t")
        {

        }

        public ErsDReportRepository(ErsDatabase objDB) : base("dailyreport_details_t", objDB)
        {

        }
    }
}
