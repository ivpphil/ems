﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
  public class ErsEmpSkillsRepository:ErsRepository<ErsEmpSkills>
    {

        public ErsEmpSkillsRepository():base("emp_skills_t")
        {

        }

        public ErsEmpSkillsRepository(ErsDatabase objDB) : base("emp_skills_t",objDB)
        {

        }
    }
}
