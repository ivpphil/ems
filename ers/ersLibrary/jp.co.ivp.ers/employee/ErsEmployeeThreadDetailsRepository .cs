﻿
using ers.jp.co.ivp.ers.employee;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.employee
{
   public class ErsEmployeeThreadDetailsRepository : ErsRepository<ErsEmployeeThreadDetails>
    {

        public ErsEmployeeThreadDetailsRepository() : base("emp_thread_details_t")
        {

        }

        public ErsEmployeeThreadDetailsRepository(ErsDatabase objDB) : base("emp_thread_details_t", objDB)
        {

        }

    }

}

