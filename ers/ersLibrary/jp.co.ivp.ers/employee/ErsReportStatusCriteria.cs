﻿using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
   public class ErsReportStatusCriteria: Criteria
    {

        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("report_status_t.id", value, Operation.EQUAL));
            }
        }


        public string report_status
        {
            set
            {
                this.Add(Criteria.GetCriterion("report_status_t.report_status", value, Operation.EQUAL));
            }
        }
    }
}
