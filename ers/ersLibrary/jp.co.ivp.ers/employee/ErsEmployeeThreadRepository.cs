﻿
using ers.jp.co.ivp.ers.employee;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.employee
{
   public class ErsEmployeeThreadRepository : ErsRepository<ErsEmployeeThread>
    {

        public ErsEmployeeThreadRepository() : base("emp_thread_t")
        {

        }

        public ErsEmployeeThreadRepository(ErsDatabase objDB) : base("emp_thread_t", objDB)
        {

        }


    }
}
