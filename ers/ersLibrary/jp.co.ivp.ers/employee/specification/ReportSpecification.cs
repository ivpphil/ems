﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee.specification
{
    public class ReportSpecification:ISpecificationForSQL
    {
        public bool isCSV { get; set; }
        public List<Dictionary<string,object>> GetSearchData(Criteria criteria)
        {
            return ErsRepository.SelectSatisfying(this, criteria);
        }
        

        public virtual string asSQL()
        {

            if (isCSV)
            {
                return string.Format(@"select report_code, dailyreport_details_t.emp_no,
                                  (select lname from employee_t where emp_no = dailyreport_details_t.emp_no)as lname,
                                  (select fname from employee_t where emp_no = dailyreport_details_t.emp_no) as fname,
                                  (select team from employee_t where emp_no = dailyreport_details_t.emp_no)as team, 
                                  dailyreport_details_t.report_date,* FROM dailyreport_details_t 
                                  LEFT JOIN employee_t on employee_t.emp_no = dailyreport_details_t.emp_no");
            }
             return string.Format(@"select DISTINCT(report_code), dailyreport_details_t.emp_no,
                                  (select lname from employee_t where emp_no = dailyreport_details_t.emp_no)as lname,
                                  (select fname from employee_t where emp_no = dailyreport_details_t.emp_no) as fname,
                                  (select team from employee_t where emp_no = dailyreport_details_t.emp_no)as team, 
                                  dailyreport_details_t.report_date, dailyreport_details_t.in_time, dailyreport_details_t.utime FROM dailyreport_details_t 
                                  LEFT JOIN employee_t on employee_t.emp_no = dailyreport_details_t.emp_no");
        }
    }
}
