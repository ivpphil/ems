﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee.specification
{
    public class TeamUpdateSpecification:ISpecificationForSQL
    {

        public List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            return ErsRepository.SelectSatisfying(this, criteria);
        }

      public virtual string asSQL()
        {
            return @"UPDATE employee_t set team_leader =''";
        }
    }
}
