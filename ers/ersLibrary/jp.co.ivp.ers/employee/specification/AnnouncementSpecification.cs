﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.announcement.specification
{
    public class AnnouncementSpecification : SearchSpecificationBase
    {

        protected override string GetSearchDataSql()
        {
            return @"SELECT *,
                     CASE WHEN announcement_t.u_time > announcement_t.in_time THEN announcement_t.u_time  
                          WHEN announcement_t.u_time <> null then announcement_t.in_time  ELSE announcement_t.in_time end as latest 
                     FROM announcement_t 	               
                     LEFT OUTER JOIN employee_t ON announcement_t.emp_no = employee_t.emp_no";

        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return String.Format(@"
                        SELECT COUNT(announcement_t.id) AS {0} 
                        FROM announcement_t 	               
                        LEFT OUTER JOIN employee_t ON announcement_t.emp_no = employee_t.emp_no", countColumnAlias);
        }


    }
}
