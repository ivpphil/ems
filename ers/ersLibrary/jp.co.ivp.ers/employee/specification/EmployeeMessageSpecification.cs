﻿using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee.specification
{
    public class EmployeeMessageSpecification : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return @"SELECT emp_message_t.id,employee_t.emp_no, CONCAT(employee_t.fname ,' ', employee_t.lname) AS Name, emp_message_t.subject, emp_message_t.message, emp_message_t.intime, emp_message_t.thread_email,emp_message_t.sender_emp_no,
                     (SELECT CONCAT(fname ,' ', lname) FROM employee_t where emp_no = emp_message_t.sender_emp_no) AS sender ,(SELECT CONCAT(fname ,' ', lname) FROM employee_t where emp_no = emp_message_t.emp_no) AS recipient,
                     emp_message_t.seen_flag 
                     FROM emp_message_t 
                     JOIN employee_t 
                     ON emp_message_t.emp_no = employee_t.emp_no ";

        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(id) AS " + countColumnAlias + " "
                + "FROM employee_t ";
        }
    }
}
