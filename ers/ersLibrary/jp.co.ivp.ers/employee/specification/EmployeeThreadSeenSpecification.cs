﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee.specification
{
    public class EmployeeThreadSeenSpecification : ISpecificationForSQL
    {
        public void update(Criteria criteria)
        {
            ErsRepository.UpdateSatisfying(this, criteria);
        }

        public string asSQL()
        {
            return "UPDATE emp_thread_details_t SET seen_flg = " + (int)EnumSeenFlg.seen + " ";
        }
    }
}
