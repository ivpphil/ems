﻿using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.employee.specification
{
    public class SearchReportDateSpecification : SearchSpecificationBase
    {
        private string orderByValue { get; set; }

        protected override string GetSearchDataSql()
        {
            return @"SELECT * 
                 FROM dailyreport_details_t join employee_t on employee_t.emp_no = dailyreport_details_t.emp_no";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return null;
        }
    }
}
