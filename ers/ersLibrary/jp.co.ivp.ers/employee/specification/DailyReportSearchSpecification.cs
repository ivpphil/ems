﻿using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee.specification
{
    public class DailyReportSearchSpecification : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return "SELECT DISTINCT(report_code),dailyreport_details_t.emp_no,(select lname from employee_t where emp_no = dailyreport_details_t.emp_no)as lname, "
                   + "(select fname from employee_t where emp_no = dailyreport_details_t.emp_no) as fname, "
                   + "(select team from employee_t where emp_no = dailyreport_details_t.emp_no)as team, "
                   + "report_date FROM dailyreport_details_t ";
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(id) AS " + countColumnAlias + " "
                + "FROM dailyreport_details_t ";
        }
    }
}