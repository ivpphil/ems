﻿using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee.specification
{
    public class EmployeeThreadSpecification : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return @"select (select Concat(fname, ' ' , lname) as name from employee_t where emp_no = emp_thread_t.sender_emp_no) as sender_name,
                    (select Concat(fname, ' ' , lname) as name from employee_t where emp_no = emp_thread_t.recipient_emp_no) as recipient_name,
                    (select intime from emp_thread_details_t where thread_no = emp_thread_t.id order by emp_thread_details_t.intime desc limit 1 ) as recent_log,
		            (select count(*) from emp_thread_details_t where thread_no = emp_thread_t.id  and seen_flg = 0 and maker_emp_no = emp_thread_t.sender_emp_no) as sender_unseen_cnt,
	                (select count(*) from emp_thread_details_t where thread_no = emp_thread_t.id  and seen_flg = 0 and maker_emp_no = emp_thread_t.recipient_emp_no) as recipient_unseen_cnt,* 
                    from emp_thread_t ";

        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(id) AS " + countColumnAlias + " "
                + "FROM emp_thread_t ";
        }
    }
}
