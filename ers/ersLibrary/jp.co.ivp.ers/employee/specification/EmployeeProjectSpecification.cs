﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee.specification
{
    public class EmployeeProjectSpecification : SearchSpecificationBase
    {
        public string emp_no { get; set; }
        protected override string GetSearchDataSql()
        {
            return @"select distinct (dailyreport_details_t.pcode), dailyreport_details_t.proj_desc, MAX(dailyreport_details_t.report_date) as report_date from dailyreport_details_t "; 
            
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return @"select distinct count(dailyreport_details_t.pcode), dailyreport_details_t.proj_desc, MAX(dailyreport_details_t.report_date) as report_date from dailyreport_details_t ";
        }
    }
}
