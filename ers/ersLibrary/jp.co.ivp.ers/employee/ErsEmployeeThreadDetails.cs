﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
    public class ErsEmployeeThreadDetails: ErsRepositoryEntity
    {

        public override int? id { get; set; }

        public int thread_no { get; set; }

        public string message { get; set; }

        public EnumActive? active_sender { get; set; }

        public EnumActive? active_recipient { get; set;}
    
        public DateTime intime { get; set; }

        public string maker_emp_no { get; set; }

        public EnumSeenFlg? seen_flg { get; set;}


    }
}
