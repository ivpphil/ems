﻿using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
    public class ErsEmpStatusCriteria:Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_status_t.id", value, Operation.EQUAL));
            }
        }

        public string emp_status
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_status_t.emp_status", value, Operation.EQUAL));
            }
        }
    }
}
