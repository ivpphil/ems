﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
    public class ErsEmployeeThread : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        
        public string sender_emp_no { get; set; }

        public string recipient_emp_no { get; set; }

        public DateTime intime { get; set; }

        public string subject { get; set; }

        public DateTime sender_last_active { get; set; }

        public DateTime recipient_last_active { get; set; }

    }
}
