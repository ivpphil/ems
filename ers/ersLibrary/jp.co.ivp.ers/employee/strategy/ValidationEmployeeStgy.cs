﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ers.jp.co.ivp.ers.employee.strategy
{
   public  class ValidationEmployeeStgy
    {
        public virtual ValidationResult CheckPasswordConfirm( string password, string password_confirm)
        {
            if (password != password_confirm)
            {
                return new ValidationResult(ErsResources.GetMessage("MisMatch", new[] { ErsResources.GetFieldName("passwd"), ErsResources.GetFieldName("passwd_confirm") }));
            }

            return null;
        }

        public virtual ValidationResult CheckCorrectPass(string emp_no,string password)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            cri.emp_no = emp_no;
            cri.password = password;
            var result = repo.GetRecordCount(cri);

            if (result == 0)
            {
                return new ValidationResult(ErsResources.GetMessage("10025", new[] { ErsResources.GetFieldName("passwd") }));
            }
            return null;
        }

        public virtual ValidationResult CheckDuplicateEmpNo(string emp_no)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            cri.emp_no = emp_no;

            var result = repo.GetRecordCount(cri);

            if(result > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("Duplicate", ErsResources.GetFieldName("emp_no"), emp_no));
            }
            return null;
        }

        public virtual ValidationResult CheckDuplicateDesknetID(int desknet_id)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            cri.desknet_id = desknet_id;

            var result = repo.GetRecordCount(cri);

            if (result > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("Duplicate", ErsResources.GetFieldName("desknet_id"), desknet_id));
            }
            return null;
        }


        public virtual ValidationResult ChecDuplicateEmail(string email, string emp_no)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            cri.email = email;
            cri.empno_not_equal = emp_no;
            var result = repo.GetRecordCount(cri);

            if(result > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("Duplicate", ErsResources.GetFieldName("email"), email));
            }
            return null;
        }

        public virtual ValidationResult ChecDuplicateRegister(string email)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            cri.email = email;
            var result = repo.GetRecordCount(cri);

            if (result > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("Duplicate", ErsResources.GetFieldName("email"), email));
            }
            return null;
        }

        public virtual bool CheckDuplicateEmailRegister(string email)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            cri.email = email;
            var result = repo.GetRecordCount(cri);

            if (result > 0)
            {
                return true;
            }
            return false;
        }

        public virtual bool CheckDuplicateEmpNoRegister(string emp_no)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            cri.emp_no = emp_no;
            var result = repo.GetRecordCount(cri);

            if (result > 0)
            {
                return true;
            }
            return false;
        }

        public virtual bool CheckDuplicateDesknet_ID(int desknet_id)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            cri.desknet_id = desknet_id;
            var result = repo.GetRecordCount(cri);

            if (result > 0)
            {
                return true;
            }
            return false;
        }

        public virtual ValidationResult CheckEmailConfirm(string email, string email_confirm)
        {
            if(email != email_confirm)
            {
                return new ValidationResult(ErsResources.GetMessage("MisMatch",new[] { ErsResources.GetFieldName("email"),ErsResources.GetFieldName("email_confirm")}));
            }
            return null;
        }


        public virtual ValidationResult CheckEmpNo(string emp_no,bool chkEmpStatus = false)
        {
            if(string.IsNullOrEmpty(emp_no))
            {
                throw new ErsException(ErsResources.GetMessage("CheckEmpNoErrorMessage1"));
            }

            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();

            cri.emp_no = emp_no;


            long emp_noCount = repo.GetRecordCount(cri);
            if(emp_noCount == 0)
            {
                throw new ErsException(ErsResources.GetMessage("CheckEmpNoErrorMessage2",emp_no));
            }

            if(chkEmpStatus)
            {
                cri.status = EnumEmpStatus.Employed;
                emp_noCount =  repo.GetRecordCount(cri);
                if (emp_noCount == 0)
                {
                    throw new ErsException(ErsResources.GetMessage("CheckEmpNoErrorMessage3",emp_no));
                }

            }

            
            return null;
        }


        public virtual ValidationResult CheckValidBirtday(DateTime birthday)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - birthday.Year;

            if (now < birthday.AddYears(age)) age--;

            if(age < 19)
            {
                return new ValidationResult(ErsResources.GetMessage("BirthdayErrMessage", ErsResources.GetFieldName("birthday"), birthday));
            }
            return null;
        }

        public virtual ValidationResult check_empNoFormat(string emp_no)
        {
            int n;
            bool isNumeric = int.TryParse(emp_no, out n);

            if(isNumeric)
            {
                return null;
            }
            else
            {
                return new ValidationResult(ErsResources.GetMessage("Employee Number is Invalid, please check format", ErsResources.GetFieldName("emp_no"), emp_no));
            }
        }


        public virtual ValidationResult CheckEmail(string email)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();

            cri.email = email;
            cri.status = EnumEmpStatus.Employed;
            var count = repo.GetRecordCount(cri);

            if(count == 0)
            {
                return new ValidationResult(ErsResources.GetMessage("NotExist", ErsResources.GetFieldName("email")));
            }

            return null;

        }

        public virtual ValidationResult CheckOtherEmail(string emp_no,string email)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
            cri.empno_not_equal = emp_no;
            cri.email = email;
            cri.status = EnumEmpStatus.Employed;
            var count = repo.GetRecordCount(cri);

            if (count > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("ExistOtherRecord", ErsResources.GetFieldName("email")));
            }

            return null;

        }

        public virtual ValidationResult CheckPositionIfExist(string position)
        {
            var position_list = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetIdFromStringLike(EnumCommonNameType.position, position);

            if (position_list.Count() == 0)
            {
                return new ValidationResult(ErsResources.GetMessage("position_select_err"));
            }

            return null;

        }

        public virtual ValidationResult CheckTeamIfExist(string team)
        {
            var team_list = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetIdFromStringLike(EnumCommonNameType.Team, team);

            if (team_list.Count() == 0)
            {
                return new ValidationResult(ErsResources.GetMessage("team_select_err"));
            }

            return null;

        }

        public virtual ValidationResult CheckEmpStatusExisting(EnumEmpStatus? status)
        {
            var common_status = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType.Emp_Status, (int)status);
            if (common_status == null)
            {
                return new ValidationResult(ErsResources.GetMessage("not_valid_status"));
            }
            return null;
        }

        public virtual ValidationResult CheckEmpPositionExisting(EnumPosition? position)
        {
            var common_status = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType.position, (int)position);
            if (common_status == null)
            {
                return new ValidationResult(ErsResources.GetMessage("not_valid_position"));
            }
            return null;
        }

     

        public virtual ValidationResult CheckJobTitleExisting(string w_job_title)
        {
            var job = ErsFactory.ersJobFactory.GetErsJobIdWithJobTitle(w_job_title);

            if (job == null)
            {
                return new ValidationResult(ErsResources.GetMessage("NotExist",ErsResources.GetFieldName("job_title"), w_job_title));
            }
            return null;
        }    

        public virtual ValidationResult CheckEmpTeamExisting(EnumTeam? team)
        {
            var common_status = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType.Team, (int)team);
            if (common_status == null)
            {
                return new ValidationResult(ErsResources.GetMessage("not_valid_team"));
            }
            return null;
        }
        public virtual ValidationResult CheckEmpLeader(string emp_no)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
            cri.emp_no =emp_no;

            var member = repo.Find(cri);
            foreach(var emp in member)
            {
                if (emp.team_leader.HasValue())
                {
                    return new ValidationResult(ErsResources.GetMessage("teamlead_err"));
                }
            }
            return null;
        }
        public virtual ValidationResult CheckRemoveEmpLeader(string emp_no,string teamlead)
        {
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
            cri.emp_no = emp_no;

            var member = repo.Find(cri);
            foreach (var emp in member)
            {
                if (teamlead != emp.team_leader)
                {
                    return new ValidationResult(ErsResources.GetMessage("remove_teamlead_err"));
                }
            }
            return null;
        }
    }
}
