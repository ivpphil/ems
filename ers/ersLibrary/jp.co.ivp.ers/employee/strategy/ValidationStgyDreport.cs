﻿using jp.co.ivp.ers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee.strategy
{
    public class ValidationStgyDreport
    {

        public virtual ValidationResult Check_start_due_date(string start_date, string due_date)
        {
            DateTime sd = Convert.ToDateTime(start_date);
            DateTime dd = Convert.ToDateTime(due_date);


            if (sd != null && dd != null)
            {
                if (sd > dd)
                {
                    return new ValidationResult(string.Format(ErsResources.GetMessage("CheckDateErrMessage"), ErsResources.GetFieldName("start_date"), ErsResources.GetFieldName("due_date")), new[] { "start_date", "due_date" });
                }
            }
            return null;
        }


        public virtual ValidationResult Check_reportDate(string emp_no, DateTime drdate)
        {

            var repo = ErsFactory.ersEmployeeFactory.GetErsDreportRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsDreportCriteria();

            cri.emp_no = emp_no;
            cri.report_date = drdate;

            var result = repo.Find(cri).Count;


            if (result > 0)
            {
                return new ValidationResult(string.Format(ErsResources.GetMessage("CheckDateRecordExistsErrMessage"), ErsResources.GetFieldName("report_date"), new[] { "report_date" }));

            }

            return null;


        }


        public virtual ValidationResult CheckDate(DateTime? dreport_date)
        {
            DateTime dateTime;
            if (dreport_date != null)
            {
                if (!DateTime.TryParse(dreport_date.ToString(), out dateTime))
                {
                    return new ValidationResult(string.Format(ErsResources.GetMessage("ReportDateInvalidErrMessage"), ErsResources.GetFieldName("report_date"), new[] { "report_date" }));
                }
            }


            return null;
        }

        public virtual ValidationResult CheckLessThanReportDate(DateTime? dreport_date)
        {
            DateTime dateNow = DateTime.Now.Date;

            if (dreport_date != null)
            {
                if (CountWorkingDays(Convert.ToDateTime(dreport_date), dateNow) > 3)
                {
                    return new ValidationResult(string.Format(ErsResources.GetMessage("ReportDateInvalidErrMessage"), ErsResources.GetFieldName("report_date"), new[] { "report_date" }));
                }
            }


            return null;
        }


        public virtual ValidationResult checkTeam(int? team)
        {

            var repo = ErsFactory.ersEmployeeFactory.GetEmpTeamRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsEmpTeamCri();

            cri.id = team;

            var result = repo.Find(cri);

            if (result.Count <= 0)
            {
                return new ValidationResult(string.Format(ErsResources.GetMessage("The Team ID you entered does not exist."), ErsResources.GetFieldName("team"), new[] { "team" }));
            }
            return null;

        }

        public virtual ValidationResult checkpCodeExist(string pcode, string desc)
        {

            var repo = ErsFactory.ersPcodeFactory.GetErsPcodeRepository();
            var cri = ErsFactory.ersPcodeFactory.GetErsPcodeCriteria();

            cri.pcode = pcode;
            cri.pcode_desc = desc;
            var result = repo.GetRecordCount(cri);

            if (result <= 0)
            {
                return new ValidationResult(string.Format(ErsResources.GetMessage("The Pcode and description you entered does not exist."), ErsResources.GetFieldName("pcode"), new[] { "pcode" }));
            }

            return null;
        }


        public virtual ValidationResult checkProgress(int? progress, EnumReportStatus? status)
        {

            if ((status == EnumReportStatus.CompletedTask && progress != 100) || ((status == EnumReportStatus.OngoingTask || status == EnumReportStatus.OngoingTaskWithProblem) && progress == 100))
            {
                return new ValidationResult(string.Format(ErsResources.GetMessage("Please check your project status and progress."), ErsResources.GetFieldName("status"), new[] { "status" }));
            }
            return null;

        }

        public virtual ValidationResult checkTotalUsedManHours(double? um_hours)
        {
            if (um_hours > 24)
            {
                return new ValidationResult(string.Format(ErsResources.GetMessage("Total report manhour must not exceed 24 hours per day.")));
            }

            return null;
        }

        public virtual ValidationResult checkUsedManHours(double? um_hours)
        {
            if (um_hours > 24)
            {
                return new ValidationResult(string.Format(ErsResources.GetMessage("Report manhour must not exceed 24 hours.")));
            }

            return null;
        }


        public virtual ValidationResult checkDreportEdit(DateTime? dreport_date)
        {
           DateTime report_date = (DateTime)dreport_date;
            report_date = report_date.AddDays(3);

            if(report_date < DateTime.Now)
            {
                return new ValidationResult(string.Format(ErsResources.GetMessage("Report cannot be edited"), ErsResources.GetFieldName("status"), new[] { "status" }));
            }

            return null;
        }


        public virtual ValidationResult CheckReportCode(string report_code)
        {

            var repo = ErsFactory.ersEmployeeFactory.GetErsDreportRepository();
            var cri = ErsFactory.ersEmployeeFactory.GetErsDreportCriteria();

            cri.report_code = report_code;

            if (repo.GetRecordCount(cri) <= 0)
            {
                return new ValidationResult(string.Format(ErsResources.GetMessage("Report Code does not exist."), ErsResources.GetFieldName("report_code"), new[] { "report_code" }));

            }

            return null;
        }

        public int CountWorkingDays(DateTime start_date, DateTime end_date)
        {
            int countDays = 0;

            DateTime dateIterator = start_date;

            while (dateIterator < end_date.AddDays(1))
            {
                if (dateIterator.DayOfWeek != DayOfWeek.Saturday && dateIterator.DayOfWeek != DayOfWeek.Sunday)
                {
                    countDays++;
                }

                dateIterator = dateIterator.AddDays(1);
            }

            return countDays;
        }
    }
}
