﻿using System;
using System.Globalization;

namespace ers.jp.co.ivp.ers.employee.strategy
{
    public class FormatFnameAndLnameStgy
    {
        public string FormatFnameAndLname(string fname,string lname)
        {
            //fname & lname capitalization
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            string firstname = textInfo.ToTitleCase(fname.ToLower());
            string lastname = textInfo.ToTitleCase(lname.ToLower());

          return String.Format("{0} {1}", firstname, lastname);
        }

    }
}
