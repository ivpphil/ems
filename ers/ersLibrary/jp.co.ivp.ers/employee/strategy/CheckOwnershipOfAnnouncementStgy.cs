﻿using jp.co.ivp.ers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee.strategy
{
    public class CheckOwnershipOfAnnouncementStgy
    {
        public virtual ValidationResult Validate(string emp_no, int id)
        {
            var repository = ErsFactory.ersEmployeeFactory.getAnnouncementRepository();
            var criteria = ErsFactory.ersEmployeeFactory.getAnnouncementCriteria();

            criteria.id = id;
            criteria.emp_no = emp_no;
            criteria.not_status = EnumAnnouncementStatus.Deleted;

            var annnouncements = repository.GetRecordCount(criteria);

            if (annnouncements == 0)
            {
                return new ValidationResult(ErsResources.GetMessage("announcement_unowned"));
            }

            return null;
        }
    }
}
