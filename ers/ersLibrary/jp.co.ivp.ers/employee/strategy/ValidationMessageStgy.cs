﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ers.jp.co.ivp.ers.employee.strategy
{
   public  class ValidationMessageStgy
    {
        public virtual ValidationResult checkThreadExist(int? thread_no)
        {
            var thread = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadWithId(thread_no);
            if(thread == null)
            { 
                return new ValidationResult(ErsResources.GetMessage("thread_not_exist"));
            }

            return null;
        }

        public virtual ValidationResult CheckRecipientExist(string emp_no)
        {
            var recipient = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(emp_no);
            if (recipient == null)
            {
                return new ValidationResult(ErsResources.GetMessage("recipient_invalid"));
            }

            return null;
        }

        public virtual bool checkRecipientIfExist(string recipient)
        {
            var recp_Details = ErsFactory.ersEmployeeFactory.GetErsEmployeeWithEmpNo(recipient);
            if (recp_Details == null)
            {
                return false;
            }

            return true;
        }

        public virtual ValidationResult checkThreadPersonsInvolve(int? thread_no, string sender, string recipient)
        {
            var thread = ErsFactory.ersEmployeeFactory.GetErsEmployeeThreadWithId(thread_no);
            if (thread == null)
            {
                return new ValidationResult(ErsResources.GetMessage("thread_not_exist"));
            }
            else
            {
                bool isSenderInvolve = (thread.sender_emp_no == sender || thread.recipient_emp_no == sender);
                bool isRecipientInvolve = (thread.sender_emp_no == recipient || thread.recipient_emp_no == recipient);

                if (!isSenderInvolve || !isRecipientInvolve)
                {
                    return new ValidationResult(ErsResources.GetMessage("thread_involve_tampered"));
                }
            }

            return null;
        }


    }
}
