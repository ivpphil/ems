﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
 public  class ErsEmployeeRoleCriteria:Criteria
    {

        public int id
        {
            set
            {
                this.Add(Criteria.GetCriterion("role_ems_t.id", value, Operation.EQUAL));
            }
        }

        public virtual int not_id
        {
            set
            {
                Add(Criteria.GetCriterion("role_ems_t.id", value, Criteria.Operation.NOT_EQUAL));
            }
        }

        public string role_action
        {
            set
            {
                this.Add(Criteria.GetCriterion("role_ems_t.role_action", value, Operation.ANY_EQUAL));
            }
        }

        public virtual string user_cd
        {
            set
            {
                Add(Criteria.GetCriterion("administrator_t.user_cd", value, Criteria.Operation.EQUAL));
            }
        }

        public virtual string role_posname
        {
            set
            {
                Add(Criteria.GetCriterion("role_ems_t.role_posname", value, Criteria.Operation.EQUAL));
            }
        }

        public int? role_poscode
        {
            set
            {
                this.Add(Criteria.GetCriterion("role_ems_t.role_poscode", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("role_ems_t.active", (int)EnumActive.Active, Operation.EQUAL));
            }
        }

        public virtual void SetActiveOnly()
        {
            this.active = EnumActive.Active;
        }

        public void SetOrderById(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("role_ems_t.id", orderBy);
        }

        public void SetOrderByIntime(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("role_ems_t.intime", orderBy);
        }
    }
}
