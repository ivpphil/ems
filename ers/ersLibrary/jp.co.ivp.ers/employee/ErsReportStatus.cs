﻿using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
   public class ErsReportStatus:ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual string report_status { get; set; }

    }
}
