﻿using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
   public class ErsEmployeePostionRepository : ErsRepository<ErsEmployeePosition>
    {

        public ErsEmployeePostionRepository() : base("emp_pos_t")
        {

        }
    }
}
