﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;

namespace ers.jp.co.ivp.ers.employee
{
    public class ErsDReportCriteria:Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("dailyreport_details_t.id", value, Operation.EQUAL));
            }
        }

        public string report_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("dailyreport_details_t.report_code", value, Operation.EQUAL));
            }
        }

        public string proj_desc
        {
            set
            {
                this.Add(Criteria.GetCriterion("dailyreport_details_t.proj_desc", value, Operation.EQUAL));
            }
        }


        public DateTime? report_date
        {
            set
            {
                this.Add(Criteria.GetCriterion("dailyreport_details_t.report_date", value, Operation.EQUAL));
            }
        }

        public string emp_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("dailyreport_details_t.emp_no", value, Operation.EQUAL));
            }
        }

        public string fname
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.fname", value, Operation.EQUAL));
            }
        }
        
        public void fname_like(string fname)
        {
            string query = "employee_t.fname ilike '%"+fname+"%'";
            this.Add(Criteria.GetUniversalCriterion(query));
        }

        public string lname
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.lname", value, Operation.EQUAL));
            }
        }

        public void lname_like(string lname)
        {
            string query = "employee_t.lname ilike '%"+lname+"%'";
            this.Add(Criteria.GetUniversalCriterion(query));
        }

        public EnumTeam? team
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.team", value, Operation.EQUAL));
            }
        }

        public string team_leader
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.team_leader", value, Operation.EQUAL));
            }
        }

        public void setSearchByTeam (IEnumerable<string> team_leaders)
        {
            List<CriterionBase> CriteriaList = new List<CriterionBase>();
            var leaders = Criteria.GetInClauseCriterion("employee_t.emp_no", team_leaders);
            CriteriaList.Add(leaders);
            var members = Criteria.GetInClauseCriterion("employee_t.team_leader", team_leaders);
            CriteriaList.Add(members);

            this.Add(Criteria.JoinWithOR(CriteriaList));
        }
        
        public virtual DateTime? from_date
        {
            set
            {
                this.Add(Criteria.GetCriterion("dailyreport_details_t.report_date", value, Operation.GREATER_EQUAL));
            }
        }

        public string pcode
        {
            set
            {
                Add(Criteria.GetCriterion("dailyreport_details_t.pcode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 配送不可（終了）
        /// </summary>
        public virtual DateTime? to_date
        {
            set
            {
                this.Add(Criteria.GetCriterion("dailyreport_details_t.report_date", value, Operation.LESS_EQUAL));
            }
        }

        public void SetOrderByProjReport_date(OrderBy orderBy)
        {
            this.AddOrderBy("report_date", orderBy);
        }

        public void SetOrderByReport_date(OrderBy orderBy)
        {
            this.AddOrderBy("dailyreport_details_t.report_date", orderBy);
        }

        public void SetOrderByEmpNo(OrderBy orderBy)
        {
            this.AddOrderBy("dailyreport_details_t.emp_no", orderBy);
        }


        public void InReportCodeList(IEnumerable<string> list)
        {
            this.Add(Criteria.GetInClauseCriterion("dailyreport_details_t.report_code", list));
        }
        
        public void ignorePcodes()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var strSQL = " dailyreport_details_t.pcode NOT IN ('80049','80048','80047','80046','80045','80044','80043','80042')";
            this.Add(Criteria.GetUniversalCriterion(strSQL));
        }

        public void setActivePcode()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var strSQL = "dailyreport_details_t.report_date > current_date - interval '1 month' ";
            this.Add(Criteria.GetUniversalCriterion(strSQL));
        }

        public void SetGroupByPcode()
        {
            this.AddGroupBy("dailyreport_details_t.pcode");
        }

        public void SetGroupByPcodeDescription()
        {
            this.AddGroupBy("dailyreport_details_t.proj_desc");
        }
        public void SetGroupByPcodeReportDate()
        {
            this.AddGroupBy("dailyreport_details_t.report_date");
        }

        public EnumEmpStatus? status
        {
            set
            {
                Add(Criteria.GetCriterion("employee_t.status", value, Operation.EQUAL));
            }
        }
    }
}
