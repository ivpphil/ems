﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.employee;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
  public  class ErsAnnouncementRepository:ErsRepository<ErsAnnouncement>
    {
        public ErsAnnouncementRepository() : base("announcement_t")
        {

        }

        public ErsAnnouncementRepository(ErsDatabase objDB) : base("announcement_t", objDB)
        {

        }

    }
}
