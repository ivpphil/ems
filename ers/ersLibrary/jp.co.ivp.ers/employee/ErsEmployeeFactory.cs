﻿using ers.jp.co.ivp.ers.announcement.specification;
using ers.jp.co.ivp.ers.employee;
using ers.jp.co.ivp.ers.employee.specification;
using ers.jp.co.ivp.ers.employee.strategy;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.employee.specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using ers.jp.co.ivp.ers.reports.specification;

namespace jp.co.ivp.ers.employee
{
   public class ErsEmployeeFactory
    {
        public ErsEmployeeRepository GetErsEmployeeRepository()
        {
            return new ErsEmployeeRepository();
        }

        public ErsEmployee GetErsEmployee()
        {
            return new ErsEmployee();
        }

        public ErsEmployeeCriteria GetErsEmployeeCriteria()
        {
            return new ErsEmployeeCriteria();
        }

        public ErsEmployeeCorrectionRepository GetErsEmployeeCorrectionRepository()
        {
            return new ErsEmployeeCorrectionRepository();
        }

        public ErsEmployeeCorrection GetErsEmployeeCorrection()
        {
            return new ErsEmployeeCorrection();
        }

        public ErsEmployeeCorrectionCriteria GetErsEmployeeCorrectionCriteria()
        {
            return new ErsEmployeeCorrectionCriteria();
        }

        public virtual ErsEmployeeCorrection getErsEmployeeCorrectionWithParameter(Dictionary<string, object> parameters)
        {
            var correction = GetErsEmployeeCorrection();
            correction.OverwriteWithParameter(parameters);
            return correction;
        }

        public ValidationEmployeeStgy GetErsStgyEmployee()
        {
            return new ValidationEmployeeStgy();
        }

        public ErsEmployee GetErsEmployeeWithEmpNo(string emp_no)
        {
            var repo = this.GetErsEmployeeRepository();
            var criteria = this.GetErsEmployeeCriteria();
            criteria.emp_no = emp_no;
            var emp = repo.FindSingle(criteria);

            if(emp != null)
            {
                emp.fname = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(emp.fname.ToLower());
                emp.lname = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(emp.lname.ToLower());
                return emp;
            }
            return null;

        }
        public IList<ErsEmployee> GetErsSignatoriesWithEmpNos(string[] emp_no)
        {
            var repo = this.GetErsEmployeeRepository();
            var criteria = this.GetErsEmployeeCriteria();
            criteria.emp_nos = emp_no;
            var employees = repo.Find(criteria);

            foreach(var emp in employees )
            { 
                emp.fname = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(emp.fname.ToLower());
                emp.lname = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(emp.lname.ToLower());
            }
            return employees;

        }

        public ErsEmployee GetErsEmployeeWithDesknetID(int id)
        {
            var repo = this.GetErsEmployeeRepository();
            var criteria = this.GetErsEmployeeCriteria();
            criteria.desknet_id = id;
            var emp = repo.Find(criteria);


            if (emp.Count > 0)
            {
                return emp.First();
            }
            return null;

        }


        public bool GetChkEmployeeIdExist(int? id)
        {
            var repo = this.GetErsEmployeeRepository();
            var criteria = this.GetErsEmployeeCriteria();
            criteria.id = id;
            var emp = repo.FindSingle(criteria);

            if (emp != null)
            {
                return emp != null;
            }
            return emp == null;
        }

        public string generateRandomPassword()
        {
            var random = new Random();
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var chars = setup.ransu_chars;
            return new string(Enumerable.Repeat(chars, 10).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public string getNextEmpId()
        {
            var repo = this.GetErsEmployeeRepository();
            var cri = this.GetErsEmployeeCriteria();
            var emp_list = repo.Find(cri);
            if (emp_list == null)
            {
                return "1";
            }
            var EmpNoList = emp_list.Select(x => Convert.ToInt32(x.emp_no));
            var list = EmpNoList.OrderBy(x => x);
            return null;
        }

        public ErsEmpTeamRepository GetEmpTeamRepository()
        {
            return new ErsEmpTeamRepository();
        }

        public IList<ErsEmployee> GetOtherTeamLeadList()
        {
            var repo = this.GetErsEmployeeRepository();
            var cri = this.GetErsEmployeeCriteria();
            cri.position = (int?)EnumPosition.TeamLeader;
            cri.status = EnumEmpStatus.Employed;
            cri.other_team_leader_except = ErsContext.sessionState.Get("mcode");
            var list = repo.Find(cri);

            if (list != null && list.Count > 0)
            {
                return list;
            }
            return null;
        }
        public ErsEmpTeam GetEmpTeam()
        {
            return new ErsEmpTeam();
        }

        public bool GetChkEmployeeTeamExist(int? team_int)
        {
            var repository = GetEmpTeamRepository();

            var criteria = this.GetErsEmpTeamCri();
            criteria.id = team_int;

            var result = repository.GetRecordCount(criteria);
            if (result > 0)
            {
                return true;
            }
            return false;
        }

        public ErsEmpTeamCriteria GetErsEmpTeamCri()
        {
            return new ErsEmpTeamCriteria();
        }

        public ErsEmpStatus GetErsEmpStatus()
        {
            return new ErsEmpStatus();
        }

        public ErsEmpStatusRepository GetErsEmpStatusRepository()
        {
            return new ErsEmpStatusRepository();
        }

        public ErsEmpStatusCriteria GetErsEmpStatusCriteria()
        {
            return new ErsEmpStatusCriteria();
        }

        public ErsDReport GetErsDreport()
        {
            return new ErsDReport();

        }

        public ErsDReportRepository GetErsDreportRepository()
        {
            return new ErsDReportRepository();
        }

        public ErsDReportCriteria GetErsDreportCriteria()
        {
            return new ErsDReportCriteria();
        }

        public ErsReportStatus GetErsReportStatus()
        {
            return new ErsReportStatus();
        }

        public ErsReportStatusRepository GetErsReportStatusRepository()
        {
            return new ErsReportStatusRepository();
        }

        public ErsReportStatusCriteria GetErsReportStatusCriteria()
        {
            return new ErsReportStatusCriteria();
        }

        public ErsReportProg GetErsReportProg()
        {
            return new ErsReportProg();
        }

        public ErsReportProgRepository GetErsReportProgRepository()
        {
            return new ErsReportProgRepository();
        }

        public ErsReportProgCriteria GetErsReportProgCriteria()
        {
            return new ErsReportProgCriteria();
        }

        public virtual ValidationStgyDreport GetErsStgyDreport()
        {
            return new ValidationStgyDreport();
        }

        public virtual ReportSpecification GetReportSpecification()
        {
            return new ReportSpecification();
        }

        public ErsEmployeePosition GetErsEmployeePosition()
        {
            return new ErsEmployeePosition();
        }

        public bool GetChkEmployeePosExist(int? pos_int)
        {
            var repository = GetErsEmployeePositionRepository();

            var criteria = this.GetErsEmployeePositionCriteria();
            criteria.id = pos_int;

            var result = repository.GetRecordCount(criteria);
            if (result > 0)
            {
                return true;
            }
            return false;
        }

        public ErsEmployeePositionCriteria GetErsEmployeePositionCriteria()
        {
            return new ErsEmployeePositionCriteria();
        }

        public ErsEmployeePostionRepository GetErsEmployeePositionRepository()
        {
            return new ErsEmployeePostionRepository();
        }

        public virtual SearchReportDateSpecification getSearchReportDateSpecification()
        {
            return new SearchReportDateSpecification();
        }

        public virtual ErsAnnouncementCriteria getAnnouncementCriteria()
        {
            return new ErsAnnouncementCriteria();
        }

        public virtual ErsAnnouncement getAnnouncement()
        {
            return new ErsAnnouncement();
        }

        public virtual ErsAnnouncementRepository getAnnouncementRepository()
        {
            return new ErsAnnouncementRepository();
        }

        public virtual ErsEmpSkillsCriteria getErsEmpSkillsCriteria()
        {
            return new ErsEmpSkillsCriteria();
        }
        
        public virtual EmployeeMessageSpecification GetEmployeeMessageSpecification()
        {
            return new EmployeeMessageSpecification();
        }
        
        public virtual ErsEmpSkills getErsEmpSkills()
        {
            return new ErsEmpSkills();
        }

        public virtual ErsEmpSkillsRepository getErsEmpSkillsRepository()
        {
            return new ErsEmpSkillsRepository();
        }

        public virtual AnnouncementSpecification GetAnnouncementSpecification()
        {
            return new AnnouncementSpecification();
        }

        public virtual DailyReportsDataForGraphsSpecification GetDailyReportsDataForGraphsSpecification()
        {
            return new DailyReportsDataForGraphsSpecification();
        }


        public virtual ErsEmployeeRole getErsEmployeeRole()
        {
            return new ErsEmployeeRole();
        }

        public virtual ErsEmployeeRole getRoleWithPosition(EnumPosition position)
        {
            var repo = this.getErsEmployeeRoleRepository();
            var cri = this.getErsEmployeeRoleCriteria();

            cri.role_poscode = (int?)position;
            var role = repo.FindSingle(cri);
            if (role != null)
            { 
                return role;
            }
            return null;
        }
        public virtual ErsEmployeeRoleRepository getErsEmployeeRoleRepository()
        {
            return new ErsEmployeeRoleRepository();
        }

        public virtual ErsEmployeeRoleCriteria getErsEmployeeRoleCriteria()
        {
            return new ErsEmployeeRoleCriteria();
        }

        public virtual TeamUpdateSpecification getErsTeamUpdateSpecification()
        {
            return new TeamUpdateSpecification();
        }

        public virtual ErsAnnouncement getAnnouncementWithID(int id) 
        {
            var repository = getAnnouncementRepository();

            var criteria = this.getAnnouncementCriteria();
            criteria.id = id;


            var list = repository.Find(criteria);
            if (list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        public virtual ErsDReport getDreportId(int? id)
        {
            var repo = GetErsDreportRepository();
            var cri = GetErsDreportCriteria();

            cri.id = id;
            var list = repo.Find(cri);

            if(list.Count != 1)
            {
                return null;
            }

            return list.First();
        }

        public virtual EmployeeProjectSpecification GetEmployeeProjectSpecification()
        {
            return new EmployeeProjectSpecification();
        }

        public virtual IList<ErsEmployee> Getm_flg()
        {
            var criteria = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
            var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
            criteria.m_flg = 0;
            
            var list = repo.Find(criteria);
            return list;

        }


        public ValidationMessageStgy GetValidationMessageStgy()
        {
            return new ValidationMessageStgy();
        }

        public virtual ErsEmployee getErsEmployeeWithParameter(Dictionary<string, object> parameters)
        {
            var employee = GetErsEmployee();
            employee.OverwriteWithParameter(parameters);
            return employee;
        }

        public ErsEmployeeThread GetErsEmployeeThreadWithId(int? id)
        {
            if (!id.HasValue)
            {
                return null;
            }
            var repo = GetErsEmployeeThreadRepository();
            var cri = GetErsEmployeeThreadCriteria();
            cri.id = id;
            return repo.FindSingle(cri);
        }
        public ErsEmployeeThread GetErsEmployeeThread()
        {
            return new ErsEmployeeThread();
        }

        public bool CheckThreadDetailsExist(int? thread_no,string emp_no)
        {
            if (!thread_no.HasValue || !emp_no.HasValue())
            {
                return false;
            }
            var repo = GetErsEmployeeThreadDetailsRepository();
            var cri = GetErsEmployeeThreadDetailsCriteria();
            cri.thread_no = thread_no;
            cri.maker_emp_no_not_equal = emp_no;

            return repo.GetRecordCount(cri) > 0;
        }

        public ErsEmployeeThreadDetails GetErsEmployeeThreadDetails()
        {
            return new ErsEmployeeThreadDetails();
        }

        public ErsEmployeeThreadRepository GetErsEmployeeThreadRepository()
        {
            return new ErsEmployeeThreadRepository();
        }

        public ErsEmployeeThreadCriteria GetErsEmployeeThreadCriteria()
        {
            return new ErsEmployeeThreadCriteria();
        }

        public ErsEmployeeThreadDetailsRepository GetErsEmployeeThreadDetailsRepository()
        {
            return new ErsEmployeeThreadDetailsRepository();
        }

        public ErsEmployeeThreadDetailsCriteria GetErsEmployeeThreadDetailsCriteria()
        {
            return new ErsEmployeeThreadDetailsCriteria();
        }

        public EmployeeThreadSeenSpecification GetEmployeeThreadSeenSpecification()
        {
            return new EmployeeThreadSeenSpecification();
        }

        public EmployeeThreadSpecification GetEmployeeThreadSpecification()
        {
            return new EmployeeThreadSpecification();
        }

        public List<Dictionary<string, object>> GetEmployeeListForDropDown()
        {          
            var returnList = new List<Dictionary<string, object>>();
            var repo = this.GetErsEmployeeRepository();
            var cri = this.GetErsEmployeeCriteria();
            cri.status = EnumEmpStatus.Employed;
            cri.SetOrderByFirstName(Criteria.OrderBy.ORDER_BY_ASC);

            var list = repo.Find(cri);

            if (list != null && list.Count() > 0)
            {
                foreach (var item in list)
                {   
                    if(item.emp_no!=ErsContext.sessionState.Get("mcode"))
                    {
                        if (item.fname.HasValue())
                        {
                            item.fname = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(item.fname.ToLower());
                        }
                        if (item.lname.HasValue())
                        {
                            item.lname = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(item.lname.ToLower());
                        }

                        var dic = new Dictionary<string, object>();
                        dic.Add("name", string.Concat(item.fname, " ", item.lname));
                        dic.Add("emp_no", item.emp_no);
                        returnList.Add(dic);
                    }
                }
                return returnList;
            }
            return null;

        }

        public bool CheckEmpImageExist(string emp_no)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var file_path = setup.image_directory + emp_no + "\\" + emp_no + Path.GetExtension(emp_no);

            if (File.Exists(file_path))
            {
                return true;
            }

            return false;
        }

        public ErsEmployeeThread GetRecentThread(string[] sender_recipient,int? thread_no)
        {
            var repo = GetErsEmployeeThreadRepository();

            var cri = GetErsEmployeeThreadCriteria();

            cri.conversation_thread = sender_recipient;

            if (thread_no.HasValue)
            {
                cri.id = thread_no;

            }
         
            cri.SetOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);

            cri.LIMIT = 1;

            return repo.FindSingle(cri);

        }

        public CheckOwnershipOfAnnouncementStgy GetCheckOwnershipOfAnnouncementStgy()
        {
            return new CheckOwnershipOfAnnouncementStgy();
        }

        public virtual ErsAnnouncement getAnnouncementWithParameter(Dictionary<string, object> parameters)
        {
            var announcement = getAnnouncement();
            announcement.OverwriteWithParameter(parameters);
            return announcement;
        }

        public FormatFnameAndLnameStgy GetFormatFnameAndLnameStgy()
        {
            return new FormatFnameAndLnameStgy();
        }

        public ErsEmployeeCorrection GetErsEmployeeCorrectionWithID(int? id)
        {
            var repo = this.GetErsEmployeeCorrectionRepository();
            var criteria = this.GetErsEmployeeCorrectionCriteria();
            criteria.id = id;
            var emp = repo.Find(criteria);


            if (emp.Count > 0)
            {
                return emp.First();
            }
            return null;

        }


    }
}
