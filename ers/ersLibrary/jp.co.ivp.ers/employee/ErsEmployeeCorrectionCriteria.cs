﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
   public class ErsEmployeeCorrectionCriteria : Criteria
    {
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_correction_t.id", value, Operation.EQUAL));
            }
        }

        public virtual string emp_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_correction_t.emp_no", value, Operation.EQUAL));
            }
        }



        public virtual string email
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_correction_t.email", value, Operation.EQUAL));
            }
        }
        
        public virtual string fname
        {
            set
            {

                Add(Criteria.GetLikeClauseCriterion("employee_correction_t.fname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual string lname
        {
            set
            {

                Add(Criteria.GetLikeClauseCriterion("employee_correction_t.lname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual DateTime? birthday
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_correction_t.birthday", value, Operation.EQUAL));
            }
        }

        public virtual string contact_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_correction_t.contact_no", value, Operation.EQUAL));
            }
        }

        public virtual int? gender
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_correction_t.gender", value, Operation.EQUAL));
            }
        }

        public virtual string address
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_correction_t.address", value, Operation.EQUAL));
            }
        }

       
    }
}
