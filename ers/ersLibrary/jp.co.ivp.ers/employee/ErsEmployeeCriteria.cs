﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.employee
{
   public class ErsEmployeeCriteria : Criteria
    {
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.id", value, Operation.EQUAL));
            }
        }

        public virtual string emp_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.emp_no", value, Operation.EQUAL));
            }
        }


        public virtual string[] emp_nos
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("employee_t.emp_no", value));
            }
        }
        
        public virtual string password
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.password", value, Operation.EQUAL));
            }
        }

        public virtual string email
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.email", value, Operation.EQUAL));
            }
        }

        public virtual string email_like
        {
            set
            {

                Add(Criteria.GetLikeClauseCriterion("employee_t.email", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual string fname
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.fname", value, Operation.EQUAL));
            }
        }

        public virtual string fname_like
        {
            set
            {

                Add(Criteria.GetLikeClauseCriterion("employee_t.fname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }


        public virtual string lname
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.fname", value, Operation.EQUAL)); ;
            }
        }

        public virtual string lname_like
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("employee_t.lname", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual EnumTeam? team
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.team", value, Operation.EQUAL));
            }
        }

        public virtual IEnumerable<int?> team_in
        {
            set
            {

                this.Add(Criteria.GetInClauseCriterion("employee_t.team", value));
            }
        }

        public string empno_not_equal
        {
            set
            {
                Add(Criteria.GetCriterion("employee_t.emp_no", value, Operation.NOT_EQUAL));
            }
        }

        public virtual string team_leader
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.team_leader", value, Operation.EQUAL));
            }
        }

        public virtual string other_team_leader_except
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.emp_no",value ,Operation.NOT_EQUAL));
            }
        }

        public virtual string name
        {
            set
            {
                Add(Criteria.GetUniversalCriterion("(LOWER(fname|| ' ' ||lname) LIKE :name)", new Dictionary<string, object> { { "name", "%" + value.ToLower() + "%" } }));
            }
        }

        public virtual string fname_lower_like
        {
            set
            {
                Add(Criteria.GetUniversalCriterion("(LOWER(fname) LIKE :name)", new Dictionary<string, object> { { "name", "%" + value.ToLower() + "%" } }));
            }
        }

        public virtual string lname_lower_like
        {
            set
            {
                Add(Criteria.GetUniversalCriterion("(LOWER(lname) LIKE :name)", new Dictionary<string, object> { { "name", "%" + value.ToLower() + "%" } }));
            }
        }

        public virtual int? position
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.position", value, Operation.EQUAL));
            }
        }

        public virtual EnumPositionTitle? position_title
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.position_title", value, Operation.EQUAL));
            }
        }

        public virtual int? job_title
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.job_title", value, Operation.EQUAL));
            }
        }

        public virtual int? job_title_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.job_title", value, Operation.NOT_EQUAL));
            }
        }


        public virtual IEnumerable<int?> position_in
        {
            set
            {

                this.Add(Criteria.GetInClauseCriterion("employee_t.position", value));
            }
        }

        public virtual EnumEmpStatus status
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.status", value, Operation.EQUAL));
            }
        }

        public void team_leader_isNull()
        {
                Add(Criteria.GetUniversalCriterion("employee_t.team_leader is null"));
        }

        public virtual string image_file
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.image_file", value, Operation.EQUAL));
            }
        }

        public virtual DateTime? birthday
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.birthday", value, Operation.EQUAL));
            }
        }

        public void getBirthdayMonthRange()
        {
            this.Add(Criteria.GetUniversalCriterion(" to_char(employee_t.birthday, 'MM') = to_char(CURRENT_DATE, 'MM') "));
        }

        public virtual string contact_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.contact_no", value, Operation.EQUAL));
            }
        }

        public virtual string contact_no_empty
        {
            set
            {
                this.Add(Criteria.GetUniversalCriterion(" ( employee_t.contact_no <> '') IS NOT TRUE"));
            }
        }

        public virtual int? gender
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.gender", value, Operation.EQUAL));
            }
        }

        public virtual string address
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.address", value, Operation.EQUAL));
            }
        }

        public virtual string address_empty
        {
            set
            {
                this.Add(Criteria.GetUniversalCriterion(" ( employee_t.address <> '') IS NOT TRUE"));
            }
        }

        public void SetOrderByid(OrderBy orderBy)
        {
            AddOrderBy("employee_t.id", orderBy);
        }

        public void SetOrderByLastName(OrderBy orderBy)
        {
            AddOrderBy("employee_t.lname", orderBy);
        }

        public void SetOrderByEmployeeNo(OrderBy orderBy)
        {
            AddOrderBy("employee_t.emp_no", orderBy);
        }
        
        public void SetorderByStatus(OrderBy orderBy)
        {
            AddOrderBy("employee_t.status", orderBy);
        }

        public void SetOrderByFirstName(OrderBy orderBy)
        {
            AddOrderBy("employee_t.fname", orderBy);
        }

        public void SortEmployeeNo(OrderBy orderBy)
        {
            AddOrderBy("CAST(employee_t.emp_no AS FLOAT)", orderBy);
        }

        public virtual int m_flg
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.m_flg",value ,Operation.EQUAL));
            }
        }

        public virtual int desknet_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.desknet_id", value, Operation.EQUAL));
            }
        }

        public virtual int not_desknet_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("employee_t.desknet_id", value, Operation.NOT_EQUAL));
            }
        }

        public void SearchCriteria(string s_keyword)
        {
            var list = new List<CriterionBase>();
            list.Add(Criteria.GetLikeClauseCriterion("employee_t.emp_no", s_keyword, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            list.Add(Criteria.GetLikeClauseCriterion("employee_t.email", s_keyword, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            list.Add(Criteria.GetLikeClauseCriterion("employee_t.fname", s_keyword, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            list.Add(Criteria.GetLikeClauseCriterion("employee_t.lname", s_keyword, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            this.Add(Criteria.JoinWithOR(list));
        }

    }
}
