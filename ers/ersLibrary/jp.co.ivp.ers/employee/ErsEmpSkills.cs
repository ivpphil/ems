﻿using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
   public class ErsEmpSkills:ErsRepositoryEntity
    {

        public override int? id { get; set; }

        public virtual string emp_no { get; set; }

        public virtual string skill_desc { get; set; }

        public virtual int years_exp { get; set; }

        public virtual string remarks { get; set; }

    }
}
