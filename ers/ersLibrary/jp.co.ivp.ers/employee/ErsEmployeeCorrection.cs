﻿using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.employee
{
    public class ErsEmployeeCorrection : ErsRepositoryEntity
    {

       
        public override int? id { get; set; }

        public virtual string emp_no { get; set; }

        public virtual string lname { get; set; }

        public virtual string fname { get; set; }

        public virtual DateTime? birthday { get; set; }

        public virtual string address { get; set; }

        public virtual string contact_no { get; set; }

        public virtual string password { get; set; }

        public virtual string email { get; set; }

        public virtual int? gender { get; set; }

        public virtual DateTime intime { get; set; }
        
        public virtual DateTime? utime { get; set; }

    }
}
