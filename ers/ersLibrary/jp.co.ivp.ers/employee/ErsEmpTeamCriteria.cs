﻿using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
   public class ErsEmpTeamCriteria : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_team_t.id", value, Operation.EQUAL));
            }
        }


        public string team_name
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_team_t.team_name", value, Operation.EQUAL));
            }
        }
    }
}
