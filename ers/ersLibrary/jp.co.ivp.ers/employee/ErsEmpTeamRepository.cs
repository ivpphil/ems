﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
    public class ErsEmpTeamRepository:ErsRepository<ErsEmpTeam>
    {

        public ErsEmpTeamRepository() : base("emp_team_t")
        {

        }

        public ErsEmpTeamRepository(ErsDatabase objDB) : base("emp_team_t", objDB)
        {

        }
    }
}
