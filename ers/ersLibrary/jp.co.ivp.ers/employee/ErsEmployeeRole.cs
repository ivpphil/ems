﻿using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
  public  class ErsEmployeeRole:ErsRepositoryEntity
    {

        public const string DEFAULT_USER_CODE = "0";

        public override int? id { get; set; }
        public virtual string role_poscode { get; set; }
        public virtual string role_posname { get; set; }
        public virtual string[] role_action { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual DateTime? intime { get; set; }
    }
}
