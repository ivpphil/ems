﻿
using ers.jp.co.ivp.ers.employee;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.employee
{
   public class ErsEmployeeCorrectionRepository : ErsRepository<ErsEmployeeCorrection>
    {

        public ErsEmployeeCorrectionRepository() : base("employee_correction_t")
        {

        }

        public ErsEmployeeCorrectionRepository(ErsDatabase objDB) : base("employee_correction_t", objDB)
        {

        }

        /// <summary>
        /// insert records to employee_correction_t and save it to request_t to for validation of admin account
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="updateReason"></param>
        /// <param name="storeNewIdToObject"></param>
        public void InsertEmployeeCorrectionRequest(ErsEmployeeCorrection obj, string updateReason, IEnumerable<string> employee_correction_list, bool storeNewIdToObject = false)
        {
            var new_obj = ErsFactory.ersEmployeeFactory.GetErsEmployeeCorrection();

            this.InsertRequest(obj, new_obj, employee_correction_list, storeNewIdToObject);

            var requestRepo = ErsFactory.ersRequestFactory.GetErsRequestRepository();
            var request = ErsFactory.ersRequestFactory.GetErsRequest();

            request.correction_id = new_obj.id;
            request.emp_no = new_obj.emp_no;
            request.request_type = EnumRequestType.InfoCorrection;
            request.status = EnumStatusRequest.Pending;
            request.active = EnumActive.Active;
            request.reason = updateReason;

            var short_date_now = DateTime.Now.ToShortDateString();
            request.date_filed = Convert.ToDateTime(short_date_now);

            requestRepo.Insert(request, true);
        }

        public void InsertRequest(ErsEmployeeCorrection obj, ErsEmployeeCorrection new_obj, IEnumerable<string> employee_correction_list, bool storeNewIdToObject = false)
        {
            //transfer correction to new object
            foreach (var prop_name in employee_correction_list)
            {
                var propertyInfo = new_obj.GetType().GetProperty(prop_name);
                var objValue = obj.GetType().GetProperty(prop_name).GetValue(obj, null);
                propertyInfo.SetValue(new_obj, objValue, null);
            }
            new_obj.emp_no = obj.emp_no;
            new_obj.intime = DateTime.Now;

            base.Insert(new_obj, storeNewIdToObject);

        }

    }
}
