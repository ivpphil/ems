﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
    public class ErsEmployeeThreadDetailsCriteria : Criteria
    {
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_thread_details_t.id", value, Operation.EQUAL));
            }
        }
        public virtual int? thread_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_thread_details_t.thread_no", value, Operation.EQUAL));
            }
        }


        public virtual DateTime? intime
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_thread_details_t.intime", value, Operation.EQUAL));
            }
        }



        public virtual DateTime? intime_greater_than
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_thread_details_t.intime", value, Operation.GREATER_EQUAL));
            }
        }


        public virtual string message
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_thread_details_t.message", value, Operation.EQUAL));
            }
        }

        public virtual EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_thread_details_t.active", value, Operation.EQUAL));
            }
        }

        public void SetOrderByIntime(OrderBy orderBy)
        {
            AddOrderBy("emp_thread_details_t.intime", orderBy);
        }

        public virtual string maker_emp_no_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_thread_details_t.maker_emp_no", value, Operation.NOT_EQUAL));
            }
        }
    }
}
