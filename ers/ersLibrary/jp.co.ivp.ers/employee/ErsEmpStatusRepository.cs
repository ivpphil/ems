﻿using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
    public class ErsEmpStatusRepository:ErsRepository<ErsEmpStatus>
    {
        public ErsEmpStatusRepository() : base("emp_status_t")
        {
        }
    }
}
