﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
    public class ErsEmployeeThreadCriteria : Criteria
    {
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_thread_t.id", value, Operation.EQUAL));
            }
        }
        public virtual string sender_emp_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_thread_t.sender_emp_no", value, Operation.EQUAL));
            }
        }

        public virtual string recipient_emp_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_thread_t.recipient_emp_no", value, Operation.EQUAL));
            }
        }

        public virtual string sender_or_recipient
        {
            set
            {
                var sender = Criteria.GetCriterion("emp_thread_t.sender_emp_no", value, Operation.EQUAL);
                var recipient = Criteria.GetCriterion("emp_thread_t.recipient_emp_no", value, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { sender,recipient}));
            }
        }

        public virtual string[] conversation_thread
        {
            set
            {
                var critList = new List<CriterionBase>();
                foreach (var item in value)
                {
                    if (!item.HasValue())
                    {
                        continue;
                    }
                    var sender = Criteria.GetCriterion("emp_thread_t.sender_emp_no", item, Operation.EQUAL);
                    var recipient = Criteria.GetCriterion("emp_thread_t.recipient_emp_no", item, Operation.EQUAL);
                    critList.Add(Criteria.JoinWithOR(new[] { sender, recipient }));
                }

                this.Add(Criteria.JoinWithAnd(critList));
            }
        }

        public virtual DateTime? intime
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_thread_t.intime", value, Operation.EQUAL));
            }
        }

        public virtual string subject
        {
            set
            {
                this.Add(Criteria.GetCriterion("emp_thread_t.subject", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual string getThreadActiveOnly
        {
            set
            {
                string stringQry = @"case 
		                        when sender_emp_no = '{0}'
		                        then
		                        (select count(*) from emp_thread_details_t where intime > sender_last_active and thread_no = emp_thread_t.id)> 0	
		                        when recipient_emp_no ='{0}'
		                        then
		                        (select count(*) from emp_thread_details_t where intime > recipient_last_active and thread_no = emp_thread_t.id)> 0 
		                        else
		                        false
			                    end";
                string query = string.Format(stringQry, value);
                this.Add(Criteria.GetUniversalCriterion(query));
            }
        }

        public void SetOrderByIntime(OrderBy orderBy)
        {
            AddOrderBy("emp_thread_t.intime", orderBy);
        }

        public void SetOrderByRecentLog(OrderBy orderBy)
        {
            AddOrderBy("recent_log", orderBy);
        }

    }
}
