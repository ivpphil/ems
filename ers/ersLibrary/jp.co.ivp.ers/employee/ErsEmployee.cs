﻿using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.employee
{
    public class ErsEmployee : ErsRepositoryEntity
    {

       
        public override int? id { get; set; }

        public virtual string emp_no { get; set; }

        public virtual string lname { get; set; }

        public virtual string fname { get; set; }

        public virtual DateTime? birthday { get; set; }

        public virtual string address { get; set; }

        public virtual string contact_no { get; set; }

        public virtual EnumPosition? position { get; set; }

        public virtual EnumTeam? team { get; set; }

        public virtual EnumEmpStatus? status { get; set; }

        public virtual string password { get; set; }

        public virtual string email { get; set; }

        public virtual string mcode { get; set; }

        public virtual int? gender { get; set; }

        public virtual EnumMformat? mformat { get; set; }

        public virtual string image_file { get; set; }

        public virtual string team_leader { get; set; }

        public virtual DateTime intime { get; set; }
        
        public virtual DateTime? utime { get; set; }

        public virtual int m_flg { get; set; }

        public virtual int desknet_id { get; set; }

        public virtual DateTime? date_hired { get; set; }

        public virtual DateTime? reg_date { get; set; }

        public virtual int? job_title { get; set; }

        public virtual string sss_no { get; set; }

        public virtual string tin_no { get; set; }

        public string w_job_title
        {
            get
            {
                if (job_title != null)
                {
                    var job = ErsFactory.ersJobFactory.GetErsJobTitleWithID(Convert.ToInt32(job_title));
                    return job?.job_title;

                }
                return "No Job Title Set";
            }
        }

        public string w_team
        {
            get
            {
                if (team != null)
                {
                    var namecode = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType.Team, (int?)team);
                    if (namecode != null)
                    {
                        return namecode.namename;
                    }
                }
                return null;
            }
        }


        public string w_status
        {
            get
            {
                if (status != null)
                {
                    var namecode = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType.Emp_Status, (int?)status);
                    if (namecode != null)
                    {
                        return namecode.namename;
                    }
                }
                return null;
            }
        }

        public string w_gender
        {
            get
            {
                if (gender != null)
                {
                    var namecode = ErsFactory.ersCommonFactory.GetErsCommonNameCodeWithTypeCodeAndCode(EnumCommonNameType.Gender, (int?)gender);
                    if (namecode != null)
                    {
                        return namecode.namename;
                    }
                }
                return null;
            }
        }

        public string w_birthday
        {
            get
            {
                if (birthday != null)
                {
                    return ((DateTime)birthday).ToString("MM/dd/yyyy");
                }
                return null;
            }
        }


    }
}
