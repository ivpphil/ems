﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
   public class ErsAnnouncementCriteria : Criteria
    {
       
        public virtual int id
        {
            set
            {
                this.Add(Criteria.GetCriterion("announcement_t.id", value, Operation.EQUAL));
            }
        }

        public virtual string emp_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("announcement_t.emp_no", value, Operation.EQUAL));
            }
        }

        public virtual DateTime in_time
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("announcement_t.in_time", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public virtual EnumActive? status
        {
            set
            {
                this.Add(Criteria.GetCriterion("announcement_t.status", value, Operation.EQUAL));
            }
        }

        public virtual EnumAnnouncementStatus? not_status
        {
            set
            {
                this.Add(Criteria.GetCriterion("announcement_t.status", value, Operation.NOT_EQUAL));
            }
        }


        public void LatestPost(OrderBy OrderBy)
        {
            this.AddOrderBy("latest", OrderBy);
        }



    }
}
