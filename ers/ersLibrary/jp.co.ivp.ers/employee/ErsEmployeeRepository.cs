﻿
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.employee
{
   public class ErsEmployeeRepository:ErsRepository<ErsEmployee>
    {

        public ErsEmployeeRepository() : base("employee_t")
        {

        }

        public ErsEmployeeRepository(ErsDatabase objDB) : base("employee_t", objDB)
        {

        }

        public override void Insert(ErsEmployee obj, bool storeNewIdToObject = false)
        {
            if (!obj.emp_no.HasValue())
            {
                var repo = ErsFactory.ersEmployeeFactory.GetErsEmployeeRepository();
                var cri = ErsFactory.ersEmployeeFactory.GetErsEmployeeCriteria();
                cri.AddOrderBy("intime",Criteria.OrderBy.ORDER_BY_DESC);
                cri.LIMIT = 1;
                var last = repo.FindSingle(cri);
                obj.emp_no = last.emp_no + 1;
            }
            obj.intime = DateTime.Now;
            obj.mcode =  obj.emp_no;
            obj.status = EnumEmpStatus.Employed;
            if (!obj.password.HasValue())
            {
                obj.password = ErsFactory.ersEmployeeFactory.generateRandomPassword();
            }
            base.Insert(obj, storeNewIdToObject);
        }
    }
}
