﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using System;

namespace ers.jp.co.ivp.ers.employee
{
    public class ErsDReport : ErsRepositoryEntity
    {
        public override int? id { get; set; }
              
        public virtual DateTime report_date { get; set; }
               
        public virtual string emp_no { get; set; }
            
        public virtual string report_code { get; set; }
              
        public virtual string pcode { get; set; }
          
        public virtual string proj_desc { get; set; }
           
        public virtual string ref_no { get; set; }
             
        public virtual int progress { get; set; }
               
        public virtual EnumReportStatus? status { get; set; }
        
        public virtual string summary { get; set; }

        public virtual string start_date { get; set; }

        public virtual string due_date { get; set; }

        public virtual double um_hours { get; set; }

        public virtual int? team { get; set; }

        public virtual string w_team { get; set; }

        public virtual DateTime? utime { get; set; }

        public virtual EnumOnOff downloaded { get; set; }
    }
}
