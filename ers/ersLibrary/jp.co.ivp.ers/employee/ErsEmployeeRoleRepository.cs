﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
    public class ErsEmployeeRoleRepository:ErsRepository<ErsEmployeeRole>
    {


        public ErsEmployeeRoleRepository()
            : base("role_ems_t")
        {
        }

        public ErsEmployeeRoleRepository(ErsDatabase objDB)
            : base("role_ems_t",objDB)
        {

        }

        public override void Insert(ErsEmployeeRole obj, bool storeNewIdToObject = false)
        {
           if(storeNewIdToObject == true)
            {
                obj.id = this.ersDB_table.GetNextSequence();

            }
            base.Insert(obj);
        }

    }
}
