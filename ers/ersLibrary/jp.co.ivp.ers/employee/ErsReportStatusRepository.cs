﻿using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
   public class ErsReportStatusRepository:ErsRepository<ErsReportStatus>
    {
        public ErsReportStatusRepository() : base("report_status_t")
        {
        }
    }
}
