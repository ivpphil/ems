﻿using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers.employee
{
    public class ErsAnnouncement: ErsRepositoryEntity
    {

        public override int? id { get; set; }

        public virtual string emp_no { get; set; }

        public virtual string news { get; set; }

        public virtual DateTime in_time { get; set; }

        public virtual DateTime? u_time { get; set; }

        public virtual EnumAnnouncementStatus? status { get; set; }

    }
}
