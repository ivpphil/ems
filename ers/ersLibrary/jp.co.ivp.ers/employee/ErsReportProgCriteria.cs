﻿using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
   public class ErsReportProgCriteria: Criteria
    {

        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("report_progress_t.id", value, Operation.EQUAL));
            }
        }


        public string report_progress
        {
            set
            {
                this.Add(Criteria.GetCriterion("report_progress_t.report_progress", value, Operation.EQUAL));
            }
        }
    }
}
