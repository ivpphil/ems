﻿using jp.co.ivp.ers.mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.employee
{
  public class ErsEmpTeam : ErsRepositoryEntity
    {

        public override int? id { get; set; }

        public string team_name { get; set; }

        public DateTime? in_time { get; set; }
    }
}
