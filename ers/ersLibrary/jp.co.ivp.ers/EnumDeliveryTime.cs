﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// Enums for DeliveryTime (Delivery or NotDelivery)
    /// </summary>
    public enum EnumDeliveryTime
    {
        /// <summary>
        /// 0 : 配信時刻 [DeliveryTime]
        /// </summary>
        DeliveryTime = 0,

        /// <summary>
        /// 1 : 配信除外時刻 [NotDeliveryTime]
        /// </summary>
        NotDeliveryTime = 1
    }
}
