﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers
{
    public enum EnumAnnouncementCmdtype
    {
        Create = 1,
        Modify = 2,
        Delete = 3,
    }
}
