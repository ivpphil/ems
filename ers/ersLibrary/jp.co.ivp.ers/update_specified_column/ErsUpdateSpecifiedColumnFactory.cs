﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.update_specified_column
{
    public class ErsUpdateSpecifiedColumnFactory
    {
        public ErsUpdateSpecifiedColumn GetErsUpdateSpecifiedColumn()
        {
            return new ErsUpdateSpecifiedColumn();
        }

        public ErsUpdateSpecifiedColumnRepository GetErsUpdateSpecifiedColumnRepository(string tableName)
        {
            return new ErsUpdateSpecifiedColumnRepository(tableName);
        }

        public ErsUpdateSpecifiedColumnCriteria GetErsUpdateSpecifiedColumnCritera()
        {
            return new ErsUpdateSpecifiedColumnCriteria();
        }

        public ErsUpdateSpecifiedColumn GetErsUpdateSpecifiedColumnWithParameters(Dictionary<string, object> param)
        {
            var entity = this.GetErsUpdateSpecifiedColumn();
            entity.OverwriteWithParameter(param);
            return entity;
        }
    }
}
