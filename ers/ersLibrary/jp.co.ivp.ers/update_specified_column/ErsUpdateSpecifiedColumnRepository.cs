﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using System.Data;

namespace jp.co.ivp.ers.update_specified_column
{
    public class ErsUpdateSpecifiedColumnRepository
        : ErsRepository<ErsUpdateSpecifiedColumn>
    {
        public ErsUpdateSpecifiedColumnRepository(string tableName)
            : base(tableName)
        {    
        }

        public ErsUpdateSpecifiedColumnRepository(string tableName, db.ErsDatabase objDB)
            : base(tableName, objDB)
        {
        }

        public DataColumnCollection GetTableSchema()
        {
            return this.ersDB_table.Schema;
        }
    }
}
