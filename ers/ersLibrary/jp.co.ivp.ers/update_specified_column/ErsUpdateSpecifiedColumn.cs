﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.Reflection;
using jp.co.ivp.ers.db;
using System.Data;

namespace jp.co.ivp.ers.update_specified_column
{
    public class ErsUpdateSpecifiedColumn
        : jp.co.ivp.ers.util.ErsReflection.ErsModelDictionary<string, object>, IErsRepositoryEntity
    {
        public ErsUpdateSpecifiedColumn()
            : base()
        {
        }
        public ErsUpdateSpecifiedColumn(IDictionary<string, object> baseObject)
            : base(baseObject)
        {
        }

        public int? id { get; set; }


        #region IErsRepositoryEntity interface

        /// <summary>
        /// プロパティをDictionaryにセットして返す
        /// <para>Returns a set of properties to a dictionary</para>
        /// </summary>
        /// <returns></returns>
        public virtual Dictionary<string, object> GetPropertiesAsDictionary()
        {
            return this;
        }

        /// <summary>
        /// プロパティをDictionaryにセットして返す
        /// <para>Returns a set of properties to a dictionary</para>
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> GetPropertiesAsDictionary(ErsDB_parent table)
        {
            var dicModel = this.GetPropertiesAsDictionary();

            var retVal = new Dictionary<string, object>();

            foreach (DataColumn column in table.Schema)
            {
                var key = column.ColumnName;
                if (dicModel.ContainsKey(key))
                {
                    retVal.Add(key, ErsBindModel.GetConcreteValue(column.DataType, dicModel[key]));
                }
            }

            return retVal;
        }

        /// <summary>
        /// Overwrite publicly writable properties of this Entity using publicly readable properties of model.
        /// </summary>
        /// <param name="model"></param>
        public virtual void OverwriteWithModel(IErsCollectable model)
        {
            var sorceDic = this.GetPropertiesAsDictionary();

            var destDic = model.GetPropertiesAsDictionary();

            this.OverwriteWithParameter(sorceDic);
        }

        /// <summary>
        /// Overwrite publicly writable properties of this Entity using dictionary values.
        /// </summary>
        /// <param name="model"></param>
        public void OverwriteWithParameter(IDictionary<string, object> dictionary)
        {
            var newDictionary = ErsCommon.OverwriteDictionary(this, dictionary);

            foreach (var key in newDictionary.Keys)
            {
                this[key] = newDictionary[key];
            }

            ErsReflection.SetPropertyAll(this, newDictionary);
        }

        #endregion IErsRepositoryEntity

        #region Specified Column Functions

        public Dictionary<string, object> GetNotNullPropertiesAsDictionary(DataColumnCollection Schema)
        {
            var dicModel = this.GetPropertiesAsDictionary();

            var retVal = new Dictionary<string, object>();

            foreach (DataColumn column in Schema)
            {
                var key = column.ColumnName;
                if (dicModel.ContainsKey(key))
                {
                    object convertValue = null;

                    if (dicModel[key] is String || dicModel[key] is string)
                    {
                        if (Convert.ToString(dicModel[key]).HasValue())
                            convertValue = ErsBindModel.GetConcreteValue(column.DataType, dicModel[key]);
                    }
                    else
                        convertValue = ErsBindModel.GetConcreteValue(column.DataType, dicModel[key]);

                    if (!Convert.ToString(convertValue).HasValue())
                        continue;

                    convertValue = ErsBindModel.GetConcreteValue(column.DataType, dicModel[key]);
                    retVal.Add(key, convertValue);
                }
            }

            return retVal;
        }

        public Dictionary<string, object> GetConcreteValuePropertiesAsDictionary(DataColumnCollection Schema)
        {
            var dicModel = this.GetPropertiesAsDictionary();

            var retVal = new Dictionary<string, object>();

            foreach (DataColumn column in Schema)
            {
                var key = column.ColumnName;
                if (dicModel.ContainsKey(key))
                {
                    object convertValue = null;

                    if (dicModel[key] is String || dicModel[key] is string)
                    {
                        if (Convert.ToString(dicModel[key]).HasValue())
                            convertValue = ErsBindModel.GetConcreteValue(column.DataType, dicModel[key]);
                    }
                    else
                        convertValue = ErsBindModel.GetConcreteValue(column.DataType, dicModel[key]);

                    if (convertValue is DateTime)
                        if ((DateTime)convertValue == DateTime.MinValue)
                            convertValue = null;

                    retVal.Add(key, convertValue);
                }
            }

            return retVal;
        }

        public Dictionary<string, Type> GetPropertiesTypeAsDictionary(DataColumnCollection Schema)
        {
            var dicModel = this.GetPropertiesAsDictionary();

            return this.GetPropertiesTypeAsDictionary(dicModel, Schema);
        }

        public Dictionary<string, Type> GetPropertiesTypeAsDictionary(Dictionary<string, object> dicModel, DataColumnCollection Schema)
        {
            var retVal = new Dictionary<string, Type>();

            foreach (DataColumn column in Schema)
            {
                var key = column.ColumnName;
                if (dicModel.ContainsKey(key))
                {
                    retVal.Add(key, column.DataType);
                }
            }

            return retVal;
        }

        #endregion Specified Column Functions
    }
}
