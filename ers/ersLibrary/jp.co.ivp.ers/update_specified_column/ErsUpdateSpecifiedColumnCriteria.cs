﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.update_specified_column
{
    public class ErsUpdateSpecifiedColumnCriteria
        : Criteria
    {
        public object this[string tableName, string columnName, Operation operation]
        {
            set
            {
                var fieldName = columnName;

                if(tableName.HasValue())
                    fieldName = tableName + "." + columnName;

                this[fieldName, operation] = value;

            }
        }

        public object this[string fieldName, Operation operation]
        {
            set
            {
                this.Add(GetCriterion(fieldName, value, operation));

            }
        }
    }
}
