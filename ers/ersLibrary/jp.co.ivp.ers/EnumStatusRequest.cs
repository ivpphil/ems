﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.ivp.ers
{
    public enum EnumStatusRequest
    {
        Pending = 0,
        Approved,
        Declined,
        Cancelled
    }
}
