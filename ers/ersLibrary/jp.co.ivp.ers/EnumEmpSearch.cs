﻿namespace jp.co.ivp.ers
{
    /// <summary>
    /// Enums for emp search  (running or stopped)
    /// </summary>
    public enum EnumEmpSearch
    {
        EmpNo = 1,
        EmpEmail,
        FirstName,
        LastName,
        Schedule
    }
}
