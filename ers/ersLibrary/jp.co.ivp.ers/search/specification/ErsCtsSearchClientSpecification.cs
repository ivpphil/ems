﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
 
namespace jp.co.ivp.ers.search.specification
{
    public class ErsCtsSearchClientSpecification
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            var strSQL = " SELECT  * FROM member_t"
                        + " LEFT JOIN pref_t ON member_t.pref = pref_t.id and pref_t.site_id = member_t.site_id "
                        + " LEFT JOIN campaign_t ON UPPER(member_t.ccode) = UPPER(campaign_t.ccode) AND campaign_t.active = " + (int)EnumActive.Active + " ";
            return strSQL;
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT DISTINCT  COUNT(member_t.mcode) AS " + countColumnAlias + " FROM member_t"
                        + " LEFT JOIN pref_t ON member_t.pref = pref_t.id and pref_t.site_id = member_t.site_id  "
                        + " LEFT JOIN campaign_t ON UPPER(member_t.ccode) = UPPER(campaign_t.ccode) AND campaign_t.active = " + (int)EnumActive.Active + " ";
        }
    }
}
