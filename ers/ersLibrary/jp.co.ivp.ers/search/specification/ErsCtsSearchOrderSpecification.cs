﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
 
namespace jp.co.ivp.ers.search.specification
{
    public class ErsCtsSearchOrderSpecification
        : SearchSpecificationBase
    {
        string strWhereDmaster { get; set; }
        string strWhereCts { get; set; }

        /// <summary>
        /// データ検索
        /// </summary>
        /// <param name="tempCriteria"></param>
        /// <returns></returns>
        public override List<Dictionary<string, object>> GetSearchData(Criteria tempCriteria)
        {
            var criteria = tempCriteria as ErsCtsSearchCriteria;
            if (criteria == null)
            {
                return null;
            }

            this.SetDnoToCriteria(criteria);

            return base.GetSearchData(criteria);
        }

        /// <summary>
        /// 件数取得
        /// </summary>
        /// <param name="tempCriteria"></param>
        /// <returns></returns>
        public override int GetCountData(Criteria tempCriteria)
        {
            var criteria = tempCriteria as ErsCtsSearchCriteria;
            if (criteria == null)
            {
                return 0;
            }

            this.SetDnoToCriteria(criteria);

            return base.GetCountData(criteria);
        }

        /// <summary>
        /// 伝票番号セレクトのCriteriaをセットする
        /// </summary>
        /// <param name="criteria"></param>
        private void SetDnoToCriteria(ErsCtsSearchCriteria criteria)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            //d_master_t
            if (criteria.d_master_t || criteria.regular_t)
            {
                var d_criteria = this.SetOrderTypeDmaster(criteria.d_master_t, criteria.regular_t, criteria.d_no);
                this.strWhereDmaster = " WHERE " + d_criteria.GetWhere();

                d_criteria.ParamaterOnly = true;
                criteria.Add(d_criteria);

                var d_site_criteria = this.SetDSiteIDCriteria(setup);
                 this.strWhereDmaster += " AND " + d_site_criteria.GetWhere();
                d_site_criteria.ParamaterOnly = true;
                criteria.Add(d_site_criteria);
            }
            else
            {
                this.strWhereDmaster = " WHERE false ";
            }

            //cts_order_t
            if (criteria.cts_order_t)
            {
                if (criteria.d_no.HasValue())
                {
                    //CTS伝票検索クライテリア
                    //for Parameter
                    var cts_criteria = new Criteria();
                    cts_criteria.Add(Criteria.GetLikeClauseCriterion("cts_order_t.temp_d_no", criteria.d_no, Criteria.LIKE_SEARCH_TYPE.PREFIX_SEARCH));
                    this.strWhereCts = " WHERE " + cts_criteria.GetWhere();

                    cts_criteria.ParamaterOnly = true;
                    criteria.Add(cts_criteria);
                }

                //add site_id condition
                var cts_site_id_criteria = new Criteria();
                var site_id_criteria = Criteria.GetCriterion("cts_order_t.site_id", setup.site_id, Criteria.Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_order_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Criteria.Operation.EQUAL);
                cts_site_id_criteria.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
                this.strWhereCts += " AND " + cts_site_id_criteria.GetWhere();

                cts_site_id_criteria.ParamaterOnly = true;
                criteria.Add(cts_site_id_criteria);
            }
            else
            {
                this.strWhereCts = " WHERE false ";
            }
        }

        /// <summary>
        /// 伝票番号のタイプをセットする
        /// And add site_id as criteria
        /// </summary>
        /// <param name="d_master_t"></param>
        /// <param name="regular_t"></param>
        /// <param name="d_no"></param>
        /// <returns></returns>
        private Criteria SetOrderTypeDmaster(bool d_master_t, bool regular_t, string d_no)
        {
            var resultCriteria = new Criteria();
            
            var listCriteria = new List<CriterionBase>();

            //伝票検索クライテリア
            if (d_master_t)
            {
                //for Parameter
                var d_criteria = new Criteria();
                d_criteria.Add(Criteria.GetCriterion("ds_master_t.order_type", EnumOrderType.Usually, Criteria.Operation.EQUAL));
                if (d_no.HasValue())
                {
                    d_criteria.Add(Criteria.GetLikeClauseCriterion("d_master_t.d_no", d_no, Criteria.LIKE_SEARCH_TYPE.PREFIX_SEARCH));
                }
                listCriteria.Add(d_criteria);
            }

            //定期伝票検索クライテリア
            if (regular_t)
            {
                //for Parameter
                var regular_criteria = new Criteria();
                regular_criteria.Add(Criteria.GetCriterion("ds_master_t.order_type", EnumOrderType.Subscription, Criteria.Operation.EQUAL));
                if (d_no.HasValue())
                {
                    regular_criteria.Add(Criteria.GetLikeClauseCriterion("d_master_t.d_no", d_no, Criteria.LIKE_SEARCH_TYPE.PREFIX_SEARCH));
                }
                listCriteria.Add(regular_criteria);
            }

            resultCriteria.Add(Criteria.JoinWithOR(listCriteria));

            return resultCriteria;
        }

        /// <summary>
        /// Add site_id to d_master_t
        /// </summary>
        /// <returns></returns>
        private Criteria SetDSiteIDCriteria(Setup setup)
        {
            var resultCriteria = new Criteria();

            //add site_id condition
            var site_id_criteria = Criteria.GetCriterion("d_master_t.site_id", setup.site_id, Criteria.Operation.EQUAL);
            var common_site_id_criteria = Criteria.GetCriterion("d_master_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Criteria.Operation.EQUAL);
            resultCriteria.Add(Criteria.JoinWithOR(new[] { site_id_criteria,common_site_id_criteria }));

            return resultCriteria;
        }

        protected override string GetSearchDataSql()
        {
            return " SELECT DISTINCT ON (d_no) * FROM (SELECT d_master_t.d_no, d_master_t.mcode, d_master_t.lname, d_master_t.fname, d_master_t.lnamek, d_master_t.fnamek, "
                     + "        member_t.address,member_t.taddress,member_t.maddress, member_t.email, d_master_t.tel, member_t.fax, member_t.zip, d_master_t.intime as orderdate, d_master_t.subtotal as orderamt, "
                     + "        pay_t.pay_name as paymenttype, d_master_t.pref, d_master_t.ccode, campaign_t.id as campaign_id, 1 as active, 1 as src ,member_t.blacklist, member_t.deleted, d_master_t.mall_d_no, "
                     + "        af_cancel.tdate AS after_cancel_date, cancel.tdate AS cancel_date, d_master_t.site_id  "
                     + "    FROM d_master_t LEFT JOIN pay_t ON d_master_t.pay = pay_t.id "
                     + "    LEFT JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no "
                     + "    LEFT JOIN ds_status_history_t AS af_cancel ON ds_master_t.id = af_cancel.ds_id AND af_cancel.new_order_status = " + (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER + " "
                     + "    LEFT JOIN ds_status_history_t AS cancel ON ds_master_t.id = cancel.ds_id AND cancel.new_order_status = " + (int)EnumOrderStatusType.CANCELED + " "
                     + "    LEFT JOIN member_t ON d_master_t.mcode = member_t.mcode "
                     + "    LEFT JOIN campaign_t ON UPPER(d_master_t.ccode) = UPPER(campaign_t.ccode) AND campaign_t.active = " + (int)EnumActive.Active + " "
                     + this.strWhereDmaster
                     + " UNION "
                     + " SELECT cts_order_t.temp_d_no, cts_order_t.mcode, cts_order_t.lname, cts_order_t.fname, cts_order_t.lnamek, cts_order_t.fnamek, "
                     + "        member_t.address,member_t.taddress,member_t.maddress, member_t.email, cts_order_t.tel, member_t.fax, member_t.zip,cts_order_t.temp_odate as orderdate, cts_order_t.subtotal as orderamt, "
                     + "        NULL AS paymenttype, member_t.pref, member_t.ccode as ccode, campaign_t.id as campaign_id, cts_order_t.active, 2 as src,member_t.blacklist, member_t.deleted, null as mall_d_no, "
                     + "        NULL AS after_cancel_date, NULL AS cancel_date, cts_order_t.site_id  "
                     + "    FROM cts_order_t "
                     + "    LEFT JOIN member_t ON cts_order_t.mcode = member_t.mcode "
                     + "    LEFT JOIN campaign_t ON UPPER(member_t.ccode) = UPPER(campaign_t.ccode) AND campaign_t.active = " + (int)EnumActive.Active
                     + this.strWhereCts + " ) as searchData WHERE true ";
       }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return " SELECT DISTINCT COUNT(d_no) AS count FROM (SELECT d_master_t.d_no, d_master_t.mcode, d_master_t.lname, d_master_t.fname, d_master_t.lnamek, d_master_t.fnamek, "
                     + "        member_t.address,member_t.taddress,member_t.maddress, member_t.email, d_master_t.tel, member_t.fax, member_t.zip, d_master_t.intime as orderdate, d_master_t.subtotal as orderamt, "
                     + "        pay_t.pay_name as paymenttype, d_master_t.pref, d_master_t.ccode, campaign_t.id as campaign_id, 1 as active, 1 as src ,member_t.blacklist, member_t.deleted, d_master_t.mall_d_no, "
                     + "        af_cancel.tdate AS after_cancel_date, cancel.tdate AS cancel_date, d_master_t.site_id "
                     + "    FROM d_master_t LEFT JOIN pay_t ON d_master_t.pay = pay_t.id "
                     + "    LEFT JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no "
                     + "    LEFT JOIN ds_status_history_t AS af_cancel ON ds_master_t.id = af_cancel.ds_id AND af_cancel.new_order_status = " + (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER + " "
                     + "    LEFT JOIN ds_status_history_t AS cancel ON ds_master_t.id = cancel.ds_id AND cancel.new_order_status = " + (int)EnumOrderStatusType.CANCELED + " "
                     + "    LEFT JOIN member_t ON d_master_t.mcode = member_t.mcode "
                     + "    LEFT JOIN campaign_t ON UPPER(d_master_t.ccode) = UPPER(campaign_t.ccode) AND campaign_t.active = " + (int)EnumActive.Active + " "
                     + this.strWhereDmaster
                     + " UNION "
                     + " SELECT cts_order_t.temp_d_no, cts_order_t.mcode, cts_order_t.lname, cts_order_t.fname, cts_order_t.lnamek, cts_order_t.fnamek, "
                     + "        member_t.address,member_t.taddress,member_t.maddress, member_t.email, cts_order_t.tel, member_t.fax, member_t.zip,cts_order_t.temp_odate as orderdate, cts_order_t.subtotal as orderamt, "
                     + "        NULL AS paymenttype, member_t.pref, member_t.ccode as ccode, campaign_t.id as campaign_id, cts_order_t.active, 2 as src,member_t.blacklist, member_t.deleted, null as mall_d_no, "
                     + "        NULL AS after_cancel_date, NULL AS cancel_date, cts_order_t.site_id "
                     + "    FROM cts_order_t "
                     + "    LEFT JOIN member_t ON cts_order_t.mcode = member_t.mcode "
                     + "    LEFT JOIN campaign_t ON UPPER(member_t.ccode) = UPPER(campaign_t.ccode) AND campaign_t.active = " + (int)EnumActive.Active
                     + this.strWhereCts + " ) as searchData WHERE true ";
        }
    }
}
