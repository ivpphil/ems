﻿using System;
using System.Data;
using System.Collections.Generic;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.search.specification
{
    public class OrderItemSpecification
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            return string.Format(@" SELECT *, af_cancel.tdate AS after_cancel_date, cancel.tdate AS cancel_date FROM d_master_t
                INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no 
                LEFT JOIN ds_status_history_t AS af_cancel ON ds_master_t.id = af_cancel.ds_id AND af_cancel.new_order_status = {0} 
                LEFT JOIN ds_status_history_t AS cancel ON ds_master_t.id = cancel.ds_id AND cancel.new_order_status = {1} "
                , (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER
                , (int)EnumOrderStatusType.CANCELED);
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return string.Format(@" SELECT COUNT(d_master_t.d_no) AS {0} FROM d_master_t
                INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no 
                LEFT JOIN ds_status_history_t AS af_cancel ON ds_master_t.id = af_cancel.ds_id AND af_cancel.new_order_status = {1} 
                LEFT JOIN ds_status_history_t AS cancel ON ds_master_t.id = cancel.ds_id AND cancel.new_order_status = {2} "
                , countColumnAlias
                , (int)EnumOrderStatusType.CANCELED_AFTER_DELIVER
                , (int)EnumOrderStatusType.CANCELED);
        }
    }

}