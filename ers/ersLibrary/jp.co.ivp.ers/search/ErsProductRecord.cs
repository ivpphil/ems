﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using System.Reflection;

namespace jp.co.ivp.ers.search
{

    public class ErsProductRecord
           : IErsCollectable, IOverwritable
    {
        public virtual int? id { get; set; }
        public virtual string d_no { get; set; }
        public virtual string gcode { get; set; }
        public virtual string scode { get; set; }
        public virtual string sname { get; set; }
        public virtual int? price { get; set; }
        public virtual int? amount { get; set; }
        public virtual int? cancel_amount { get; set; }
        public virtual int? after_cancel_amount { get; set; }
        public virtual int? total { get; set; }
        public virtual EnumOrderStatusType? order_status { get; set; }
        public virtual EnumDelvMethod? deliv_method { get; set; }
        public virtual string sendno { get; set; }
        public virtual DateTime? after_cancel_date { get; set; }
        public virtual DateTime? cancel_date { get; set; }

        public Dictionary<string, object> GetPropertiesAsDictionary()
        {
            return ErsReflection.GetPropertiesAsDictionary(this, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        }

        public void OverwriteWithParameter(IDictionary<string, object> dictionary)
        {
            var dic = ErsCommon.OverwriteDictionary(this.GetPropertiesAsDictionary(), dictionary);
            ErsReflection.SetPropertyAll(this, dic);
        }
    }
}
