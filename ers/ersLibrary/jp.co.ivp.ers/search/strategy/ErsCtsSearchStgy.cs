﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.search.specification; 
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.search;

namespace jp.co.ivp.ers.search.strategy
{
    public class ErsCtsSearchStgy
    {
        /// <summary>
        /// 検索（クライテリア）
        /// </summary>
        /// <param name="criteria">クライテリア</param>
        /// <returns>検索結果リスト</returns>
        public IList<ErsCtsSearch> FindOrder(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsSearchFactory.GetErsCtsSearchOrderSpecification();
            //criteria.Add(Criteria.GetCriterion("searchData.active", 1, Criteria.Operation.EQUAL));

            List<ErsCtsSearch> lstRet = new List<ErsCtsSearch>();
            var list = spec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                ErsCtsSearch search = ErsFactory.ersCtsSearchFactory.getOrderWithParameter(dr, this);
                lstRet.Add(search);
            }
            return lstRet;
        }

        public long GetRecordCountOrder(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsSearchFactory.GetErsCtsSearchOrderSpecification();

            //criteria.Add(Criteria.GetCriterion("searchData.active", 1, Criteria.Operation.EQUAL));

            return spec.GetCountData(criteria);
        }

        /// <summary>
        /// 検索（クライテリア）
        /// </summary>
        /// <param name="criteria">クライテリア</param>
        /// <returns>検索結果リスト</returns>
        public IList<ErsCtsSearch> FindClient(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsSearchFactory.GetErsCtsSearchClientSpecification();

            List<ErsCtsSearch> lstRet = new List<ErsCtsSearch>();
            var list = spec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                ErsCtsSearch search = ErsFactory.ersCtsSearchFactory.getOrderWithParameter(dr, this);
                lstRet.Add(search);
            }
            return lstRet;

        }

        public long GetRecordCountClient(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsSearchFactory.GetErsCtsSearchClientSpecification();

            //criteria.Add(Criteria.GetCriterion("searchData.active", 1, Criteria.Operation.EQUAL));

            return spec.GetCountData(criteria);
        }

        public IList<ErsProductRecord> FindProduct(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsSearchFactory.GetOrderItemSpecification();

            List<ErsProductRecord> lstRet = new List<ErsProductRecord>();
            var list = spec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                ErsProductRecord search = ErsFactory.ersCtsSearchFactory.getProductWithParameter(dr, this);
                lstRet.Add(search);
            }
            return lstRet;

        }
    }
}
