﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.search
{
    public class ErsCtsSearchCriteria
        : Criteria
    {
        /// <summary>
        /// 顧客番号（前方一致）
        /// </summary>
        public virtual string mcodeLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.mcode", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// 姓（前方一致）
        /// </summary>
        public virtual string lnameLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.lname", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// 名（前方一致）
        /// </summary>
        public virtual string fnameLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.fname", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// 姓カナ（前方一致）
        /// </summary>
        public virtual string lnamekLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.lnamek", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// 名（前方一致）
        /// </summary>
        public virtual string fnamekLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.fnamek", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// アドレス（前方一致）
        /// </summary>
        public virtual string addressLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.address", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual string taddressLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.taddress", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// 県 
        /// </summary>
        public virtual int? pref
        {
            set
            {
                this.Add(Criteria.GetCriterion("searchData.pref", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 退会状況による検索 (Fileter for  )
        /// </summary>
        public EnumDeleted deleted
        {
            set
            {
                this.Add(Criteria.GetUniversalCriterion("COALESCE(searchData.deleted, 0) = :deleted", new Dictionary<string, object>() { { "deleted", value } }));
            }
        }

        /// <summary>
        /// 電話
        /// </summary>
        public virtual string tel
        {
            set
            {
                this.Add(Criteria.GetCriterion("searchData.tel", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 電話（前方一致）
        /// </summary>
        public virtual string telLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.tel", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        /// <summary>
        /// ファックス（前方一致）
        /// </summary>
        public virtual string faxLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.fax", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual string zipLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.zip", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }
 
        //for wildcard "_"       **************
        public virtual string zipnthPrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.zip", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH, "_____"));
            }
        }

        /// <summary>
        /// メールアドレス（前方一致）
        /// </summary>
        public virtual string emailLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.email", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public bool d_master_t { get; set; }

        public bool regular_t { get; set; }

        public bool cts_order_t { get; set; }

        public string d_no { get; set; }

        public string mall_d_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("searchData.mall_d_no", value, Operation.EQUAL));
            }
        }


        /// <summary>
        /// 媒体番号（前方一致）
        /// </summary>
        public virtual string ccodeLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("searchData.ccode", value.ToUpper(), LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual int? campaign_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("searchData.campaign_id", value, Operation.EQUAL));
            }
        }
        
        public virtual string ordno
        {
            set
            {
                this.Add(Criteria.GetCriterion("ds_master_t.d_no", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 返品日（開始）
        /// </summary>
        public virtual DateTime af_cancel_date_from
        {
            set
            {
                this.Add(Criteria.GetCriterion("after_cancel_date", value, Operation.GREATER_EQUAL));
            }
        }

        /// <summary>
        /// 返品日（終了）
        /// </summary>
        public virtual DateTime af_cancel_date_to
        {
            set
            {
                this.Add(Criteria.GetCriterion("after_cancel_date", value, Operation.LESS_EQUAL));
            }
        }

        /// <summary>
        /// キャンセル日（開始）
        /// </summary>
        public virtual DateTime cancel_date_from
        {
            set
            {
                this.Add(Criteria.GetCriterion("cancel_date", value, Operation.GREATER_EQUAL));
            }
        }

        /// <summary>
        /// キャンセル日（終了）
        /// </summary>
        public virtual DateTime cancel_date_to
        {
            set
            {
                this.Add(Criteria.GetCriterion("cancel_date", value, Operation.LESS_EQUAL));
            }
        }

        //addnl jcb 02-21-12
        public virtual void SetOrderByName()
        {
            this.AddOrderBy("searchData.lnamek", OrderBy.ORDER_BY_ASC);
            this.AddOrderBy("searchData.fnamek", OrderBy.ORDER_BY_ASC);
            this.AddOrderBy("searchData.pref", OrderBy.ORDER_BY_ASC);
            this.AddOrderBy("searchData.mcode", OrderBy.ORDER_BY_ASC);
        }
        public virtual void SetOrderByOrderNo()
        {
            this.AddOrderBy("searchData.d_no", OrderBy.ORDER_BY_ASC);
        }

        public void ignoreMonitor()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var strSQL = " (lname, fname) NOT IN ((:monitorFirstLname, :monitorFirstFname), (:monitorSecondLname, :monitorSecondFname))";

            this.Add(Criteria.GetUniversalCriterion(strSQL,
                new Dictionary<string, object>() { 
                    { "monitorFirstLname", setup.monitorFirstLname },
                    { "monitorFirstFname", setup.monitorFirstFname },
                    { "monitorSecondLname", setup.monitorSecondLname },
                    { "monitorSecondFname", setup.monitorSecondFname } }));
        }

        public void SetLikeAddress(string address)
        {
            var SQL = " searchData.address||searchData.taddress||searchData.maddress LIKE '%" + address + "%'";

            Add(Criteria.GetUniversalCriterion(SQL));
        }


        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("d_master_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("d_master_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? searchData_site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("searchData.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("searchData.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}
