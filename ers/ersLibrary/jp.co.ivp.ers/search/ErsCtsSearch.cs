﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.IO;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.search
{
    public class ErsCtsSearch
        : ErsRepositoryEntity 
    { 

        public virtual string mcode { get; set; }
        public virtual string lname { get; set; }
        public virtual string fname { get; set; }
        public virtual string lnamek { get; set; }
        public virtual string fnamek { get; set; }
        public virtual string zip1 { get; set; }
        public virtual int? pref { get; set; }
        public virtual string pref_name { get; set; }
        public virtual string address { get; set; }
        public virtual string email { get; set; }
        public virtual string tel { get; set; }
        public virtual string fax { get; set; }
        public virtual string d_no { get; set; }
        
        public virtual DateTime? orderdate { get; set; }
        public virtual int orderamt { get; set; }
        public virtual string orderstatus { get; set; }
        public virtual string paymenttype { get; set; }
        public virtual int src { get; set; }
        public override int? id { get; set; }

        public virtual int order { get; set; }
        public virtual int temporder { get; set; }
        public virtual int regorder { get; set; }
        public virtual int targetsearch { get; set; }

        public virtual string ccode { get; set; }
        public virtual string campaign_name { get; set; }
        public virtual int? blacklist { get; set; }
        public virtual int? site_id { get; set; }

        public virtual EnumDeleted? deleted { get; set; }
    }
}
