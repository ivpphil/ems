﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.search.specification;
using jp.co.ivp.ers.search.strategy; 

namespace jp.co.ivp.ers.search
{
    public class ErsCtsSearchFactory
    {
        public virtual ErsCtsSearchStgy GetErsCtsSearchStgy()
        {
            return new ErsCtsSearchStgy();
        }

        public virtual ErsCtsSearchCriteria GetErsCtsSearchCriteria()
        {
            return new ErsCtsSearchCriteria();
        }
        
        public virtual ErsCtsSearchOrderSpecification GetErsCtsSearchOrderSpecification()
        {
            return new ErsCtsSearchOrderSpecification();
        }


        public virtual ErsCtsSearchClientSpecification GetErsCtsSearchClientSpecification()
        {
            return new ErsCtsSearchClientSpecification();
        }

        public virtual OrderItemSpecification GetOrderItemSpecification() 
        {
            return new OrderItemSpecification();
        }

        public virtual ErsCtsSearch getOrderWithParameter(Dictionary<string, object> inputParameters, ErsCtsSearchStgy repository)
        {

            var objOrder = new ErsCtsSearch();

            objOrder.OverwriteWithParameter(inputParameters);

            return objOrder;
        }

        public virtual ErsProductRecord getProductWithParameter(Dictionary<string, object> inputParameters, ErsCtsSearchStgy repository)
        {

            var objOrder = new ErsProductRecord();

            objOrder.OverwriteWithParameter(inputParameters);

            return objOrder;
        }

    }
}
