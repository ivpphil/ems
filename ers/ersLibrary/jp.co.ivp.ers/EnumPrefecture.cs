﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumPrefecture
    {
        /// <summary>
        /// 北海道
        /// </summary>
        HOKKAIDO = 1,
        /// <summary>
        /// 青森県
        /// </summary>
        AOMORI,
        /// <summary>
        /// 岩手県
        /// </summary>
        IWATE,
        /// <summary>
        /// 宮城県
        /// </summary>
        MIYAGI,
        /// <summary>
        /// 秋田県
        /// </summary>
        AKITA,
        /// <summary>
        /// 山形県
        /// </summary>
        YAMAGATA,
        /// <summary>
        /// 福島県
        /// </summary>
        FUKUSHIMA,
        /// <summary>
        /// 茨城県
        /// </summary>
        IBARAKI,
        /// <summary>
        /// 栃木県
        /// </summary>
        TOCHIGI,
        /// <summary>
        /// 群馬県
        /// </summary>
        GUNMA,
        /// <summary>
        /// 埼玉県
        /// </summary>
        SAITAMA,
        /// <summary>
        /// 千葉県
        /// </summary>
        CHIBA,
        /// <summary>
        /// 東京都
        /// </summary>
        TOKYO,
        /// <summary>
        /// 神奈川県
        /// </summary>
        KANAGAWA,
        /// <summary>
        /// 新潟県
        /// </summary>
        NIIGATA,
        /// <summary>
        /// 富山県
        /// </summary>
        TOYAMA,
        /// <summary>
        /// 石川県
        /// </summary>
        ISHIKAWA,
        /// <summary>
        /// 福井県
        /// </summary>
        FUKUI,
        /// <summary>
        /// 山梨県
        /// </summary>
        YAMANASHI,
        /// <summary>
        /// 長野県
        /// </summary>
        NAGANO,
        /// <summary>
        /// 岐阜県
        /// </summary>
        GIFU,
        /// <summary>
        /// 静岡県
        /// </summary>
        SHIZUOKA,
        /// <summary>
        /// 愛知県
        /// </summary>
        AICHI,
        /// <summary>
        /// 三重県
        /// </summary>
        MIE,
        /// <summary>
        /// 滋賀県
        /// </summary>
        SHIGA,
        /// <summary>
        /// 京都府
        /// </summary>
        KYOTO,
        /// <summary>
        /// 大阪府
        /// </summary>
        OSAKA,
        /// <summary>
        /// 兵庫県
        /// </summary>
        HYOGO,
        /// <summary>
        /// 奈良県
        /// </summary>
        NARA,
        /// <summary>
        /// 和歌山県
        /// </summary>
        WAKAYAMA,
        /// <summary>
        /// 鳥取県
        /// </summary>
        TOTTORI,
        /// <summary>
        /// 島根県
        /// </summary>
        SHIMANE,
        /// <summary>
        /// 岡山県
        /// </summary>
        OKAYAMA,
        /// <summary>
        /// 広島県
        /// </summary>
        HIROSHIMA,
        /// <summary>
        /// 山口県
        /// </summary>
        YAMAGUCHI,
        /// <summary>
        /// 徳島県
        /// </summary>
        TOKUSHIMA,
        /// <summary>
        /// 香川県
        /// </summary>
        KAGAWA,
        /// <summary>
        /// 愛媛県
        /// </summary>
        EHIME,
        /// <summary>
        /// 高知県
        /// </summary>
        KOCHI,
        /// <summary>
        /// 福岡県
        /// </summary>
        FUKUOKA,
        /// <summary>
        /// 佐賀県
        /// </summary>
        SAGA,
        /// <summary>
        /// 長崎県
        /// </summary>
        NAGASAKI,
        /// <summary>
        /// 熊本県
        /// </summary>
        KUMAMOTO,
        /// <summary>
        /// 大分県
        /// </summary>
        OITA,
        /// <summary>
        /// 宮崎県
        /// </summary>
        MIYAZAKI,
        /// <summary>
        /// 鹿児島県
        /// </summary>
        KAGOSHIMA,
        /// <summary>
        /// 沖縄県
        /// </summary>
        OKINAWA,
        /// <summary>
        /// 海外
        /// </summary>
        OVERSEAS
    }
}
