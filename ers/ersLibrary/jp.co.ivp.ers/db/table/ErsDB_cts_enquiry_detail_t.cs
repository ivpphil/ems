﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using jp.co.ivp.ers.member;
using System.Data.Common;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.db.table
{
    /// <summary>
    /// Obtain table for member master
    /// </summary>
    internal class ErsDB_cts_enquiry_detail_t
        : ErsDB_parent_withLog
    {
        protected const string tableName = "cts_enquiry_detail_t";
        protected const string logTableName = "tp_cts_enquiry_detail_t";

        /// <summary>
        /// コンストラクタ宣言
        /// </summary>
        public ErsDB_cts_enquiry_detail_t()
            : base(tableName, logTableName)
        {
        }

        /// <summary>
        /// テスト用コンストラクタ
        /// </summary>
        /// <param name="objDB">database used</param>
        public ErsDB_cts_enquiry_detail_t(ErsDatabase objDB)
            : base(tableName, logTableName, objDB)
        {
        }
    }
}
