﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using jp.co.ivp.ers.mvc;
using System.Data.Common;

namespace jp.co.ivp.ers.db.table
{
    /// <summary>
    /// Obtain table for document master
    /// </summary>
    internal class ErsDB_regular_t
        : ErsDB_parent_withLog
    {
        protected const string tableName = "regular_t";
        protected const string logTableName = "old_regular_t";

        /// <summary>
        /// コンストラクタ宣言
        /// </summary>
        public ErsDB_regular_t()
            : base(tableName, logTableName)
        {
        }

        /// <summary>
        ///  テスト用コンストラクタ
        /// </summary>
        /// <param name="objDB">database used</param>
        public ErsDB_regular_t(ErsDatabase objDB)
            : base(tableName, logTableName, objDB)
        {
        }
    }
}
