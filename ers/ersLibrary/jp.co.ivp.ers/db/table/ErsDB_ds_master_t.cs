﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;

namespace jp.co.ivp.ers.db.table
{
    /// <summary>
    /// Obtain table for Document items master
    /// </summary>
    internal class ErsDB_ds_master_t
        : ErsDB_parent_withLog
    {
        protected const string tableName = "ds_master_t";
        protected const string logTableName = "old_ds_master_t";

        /// <summary>
        /// コンストラクタ宣言
        /// </summary>
        public ErsDB_ds_master_t()
            : base(tableName, logTableName)
        {
        }

        //テスト用コンストラクタ
        public ErsDB_ds_master_t(ErsDatabase objDB)
            : base(tableName, logTableName, objDB)
        {
        }
    }
}
