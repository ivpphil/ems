﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace jp.co.ivp.ers.db.table
{
    public class ErsDB_parent_withLog
        : ErsDB_parent
    {
        ErsTextBackup textBackup;
        ErsDbRecordBackUp recordBackup;

        string logTableName;

        /// <summary>
        /// コンストラクタ宣言
        /// </summary>
        public ErsDB_parent_withLog(string tableName, string logTableName)
            : base(tableName)
        {
            textBackup = new ErsTextBackup();
            recordBackup = new ErsDbRecordBackUp();

            this.logTableName = logTableName;
        }

        /// <summary>
        /// テスト用コンストラクタ
        /// </summary>
        /// <param name="objDB">database used</param>
        public ErsDB_parent_withLog(string tableName, string logTableName, ErsDatabase objDB)
            : base(tableName, objDB)
        {
            textBackup = new ErsTextBackup();
            recordBackup = new ErsDbRecordBackUp();

            this.logTableName = logTableName;
        }

        /// <summary>
        /// Inserts record in member_t table
        /// </summary>
        /// <param name="dic">values saved in dictionary</param>
        /// <returns>returns inserted values in member_t table</returns>
        public override int gInsert(Dictionary<string, object> dic, bool skipColumn = true)
        {
            int result;

            using (var tx = recordBackup.BeginTransaction(this.objDB))
            {
                //IDは強制でここで取得します。
                if (dic["id"] == null)
                {
                    dic["id"] = this.GetNextSequence();
                }

                //テキストバックアップ処理を差し込むためオーバーライド
                textBackup.BackUp(this.myTableName, EnumBackupMethod.INSERT, dic);

                //テンポラリバックアップ
                var logDic = new Dictionary<string, object>() { { "id", dic["id"] } };
                recordBackup.BackUp(this.objDB, this.logTableName, logDic, EnumBackupMethod.INSERT);

                result = base.gInsert(dic, skipColumn);

                recordBackup.Commit(tx);
            }

            return result;
        }

        /// <summary>
        /// Updates record in member_t table
        /// </summary>
        /// <param name="columns">specific columns to be updated</param>
        /// <param name="dic">specific values of columns to be updated</param>
        /// <param name="strWhere">specifies where new record is to updated</param>
        /// <param name="parameters">specifies parameter to be updated</param>
        /// <returns>returns update record</returns>
        public override int gUpdateColumn(string[] columns, Dictionary<string, object> dic, string strWhere, IEnumerable<DbParameter> parameters = null)
        {
            int result;

            using (var tx = recordBackup.BeginTransaction(this.objDB))
            {
                var targetList = this.gSelect(strWhere, parameters);
                foreach (var logDic in targetList)
                {
                    //テキストバックアップ処理を差し込むためオーバーライド
                    textBackup.BackUp(this.myTableName, EnumBackupMethod.UPDATE, dic);

                    //テンポラリバックアップ
                    recordBackup.BackUp(this.objDB, this.logTableName, logDic, EnumBackupMethod.UPDATE);
                }

                result = base.gUpdateColumn(columns, dic, strWhere, parameters);

                recordBackup.Commit(tx);
            }

            return result;
        }

        /// <summary>
        /// Deletes record in member_t table
        /// </summary>
        /// <param name="strWhere">specifies which record to be deleted</param>
        /// <param name="parameters">specifies parameter to be updated</param>
        /// <returns>returns delete record</returns>
        public override int gDelete(string strWhere, IEnumerable<DbParameter> parameters)
        {
            int result;

            using (var tx = recordBackup.BeginTransaction(this.objDB))
            {
                //テキストバックアップ処理を差し込むためオーバーライド
                var targetList = this.gSelect(strWhere, parameters);
                foreach (var dic in targetList)
                {
                    var logDic = new Dictionary<string, object>() { { "id", dic["id"] } };
                    textBackup.BackUp(this.myTableName, EnumBackupMethod.DELETE, logDic);

                    //テンポラリバックアップ
                    recordBackup.BackUp(this.objDB, this.logTableName, dic, EnumBackupMethod.DELETE);
                }

                result = base.gDelete(strWhere, parameters);

                recordBackup.Commit(tx);
            }

            return result;
        }
    }
}
