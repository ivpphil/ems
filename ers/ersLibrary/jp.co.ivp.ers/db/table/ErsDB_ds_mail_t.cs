﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db.table
{
    internal class ErsDB_ds_mail_t
         : ErsDB_parent_withLog
    {
        protected const string tableName = "ds_mail_t";
        protected const string logTableName = "old_ds_mail_t";

        /// <summary>
        /// コンストラクタ宣言
        /// </summary>
        public ErsDB_ds_mail_t()
            : base(tableName, logTableName)
        {
        }

        /// <summary>
        ///  テスト用コンストラクタ
        /// </summary>
        /// <param name="objDB">database used</param>
        public ErsDB_ds_mail_t(ErsDatabase objDB)
            : base(tableName, logTableName, objDB)
        {
        }
    }
}
