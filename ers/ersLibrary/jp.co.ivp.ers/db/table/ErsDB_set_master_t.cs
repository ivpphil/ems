﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace ers.jp.co.ivp.ers.db.table
{
    internal class ErsDB_set_master_t : ErsDB_parent
    {
        protected const string tableName = "set_master_t";

        public ErsDB_set_master_t()
            : base(tableName)
        { 
        }

        public ErsDB_set_master_t(ErsDatabase objDB)
            : base(tableName, objDB)
        { 
        
        }

        public virtual Dictionary<string, object> SelectById(int id)
        {
            var list = gSelect("Where id = '" + id + "'");
            if (list.Count == 0)
            {
                return null;
            }
            return list[0];
        }
        public override int GetNextSequence()
        {
            return GetNextSequence("set_master_t_id_seq");
        }

        public override int GetCurrentSequence()
        {
            return GetCurrentSequence("set_master_t_id_seq");
        }


    }
}
