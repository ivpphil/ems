﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using Npgsql;
using jp.co.ivp.ers.util;
using System.Web;
using System.Data;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.state;


namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// Factory class for ErsWebDatabase,
    /// implements AbstractErsDatabaseFactory
    /// </summary>
    public class ErsWebDatabaseFactory
        : AbstractErsDatabaseFactory
    {

        /// <summary>
        /// DB接続のプール
        /// </summary>
        protected static Dictionary<string, ErsDatabase> _DbPool
        {
            get
            {
                return (Dictionary<string, ErsDatabase>)ErsCommonContext.GetPooledObject("_DbPool");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_DbPool", value);
            }
        }

        /// <summary>
        /// DB接続のプール(connection string毎にプール)
        /// </summary>
        public static Dictionary<string, ErsDatabase> DbPool
        {
            get
            {
                if (_DbPool == null)
                {
                    _DbPool = new Dictionary<string, ErsDatabase>();
                }
                return _DbPool;
            }
        }

        /// <summary>
        /// Gets a Connection Object which uses a default connection string.
        /// </summary>
        /// <returns>returns the connectionstring based on the setup</returns>
        public override ErsDatabase GetErsDatabase()
        {
            return GetErsDatabase(ErsFactory.ersUtilityFactory.getSetup().getConnectionStrings());
        }

        /// <summary>
        /// Gets a Connection Object which uses the specified connection string.
        /// </summary>
        /// <param name="connectionString">connection string based on the setup</param>
        /// <returns>returns pool connection string</returns>
        public override ErsDatabase GetErsDatabase(string connectionString)
        {

            if (!DbPool.ContainsKey(connectionString))
            {

                DbPool[connectionString] = GetNewErsDatabase(connectionString);
            }
            return DbPool[connectionString];
        }

        /// <summary>
        /// Gets new connection string
        /// </summary>
        /// <returns>returns new connection string</returns>
        public override ErsDatabase GetNewErsDatabase()
        {
            return GetNewErsDatabase(ErsCommonsSetting.strConnectionString);
        }

        /// <summary>
        /// Gets new connection string
        /// </summary>
        /// <param name="connectionString">connection string</param>
        /// <returns>returns created new connection</returns>
        public override ErsDatabase GetNewErsDatabase(string connectionString)
        {
            ErsDatabase objDB = new NpgsqlErsDatabase();
            objDB.connection = objDB.CreateConnection(connectionString);
            return objDB;
        }

        /// <summary>
        /// Gets a connection string of cloud database.
        /// </summary>
        /// <returns>returns cloud connection string</returns>
        public static string GetCloudConnectionStrings()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var ransuChar = setup.ransu_chars;

            int index = -1;

            if (ErsContext.sessionState != null)
            {
                var ransu = ErsContext.sessionState.Get("ransu");
                if (!string.IsNullOrEmpty(ransu))
                {
                    index = ransuChar.IndexOf(ransu.Substring(0, 1)) % 10;
                }
            }

            if (index < 0)
            {
                //If there is not a random string on session, get a new one.
                Random r = new Random();
                index = r.Next(9);
            }

            //乱数の１文字目を１桁の数字に変換
            //Uses first character of the random string as index.
            return setup.getCloudConnectionStrings()[index];
        }
    }
}
