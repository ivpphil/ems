﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.db
{
    public abstract class SearchSpecificationBase
        : ISearchSpecification, ISpecificationForSQL
    {
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            this._sql = this.GetSearchDataSql();
            return ErsRepository.SelectSatisfying(this, criteria);
        }

        public virtual IEnumerable<T> GetSearchData<T>(Criteria criteria)
        {
            this._sql = this.GetSearchDataSql();
            return ErsRepository.SelectSatisfying<T>(this, criteria);
        }

        public virtual int GetCountData(Criteria criteria)
        {
            var countColumnAlias = "count";

            this._sql = this.GetCountDataDataSql(countColumnAlias);
            var record = ErsRepository.SelectSatisfying(this, criteria);

            if (record.Count == 0)
                return 0;

            return Convert.ToInt32(record[0][countColumnAlias]);
        }

        private string _sql;

        public string asSQL()
        {
            return _sql;
        }

        protected abstract string GetSearchDataSql();

        protected abstract string GetCountDataDataSql(string countColumnAlias);
    }
}
