﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db
{
    public class ErsDbRecordBackUp
    {
        bool inTransaction { get; set; }

        internal ErsConnection BeginTransaction(ErsDatabase objDb)
        {
            this.inTransaction = ErsDB_parent.InTransaction;

            if (!this.inTransaction)
            {
                return ErsDB_parent.BeginTransaction(objDb);
            }
            else
            {
                return ErsDB_parent.OpenConnection(objDb);
            }
        }

        internal void Commit(ErsConnection tx)
        {
            if (!this.inTransaction)
            {
                tx.Commit();
            }
        }

        public void BackUp(ErsDatabase objDb, string tableName, Dictionary<string, object> dic, EnumBackupMethod method)
        {
            var log_table = new ErsDB_parent(tableName, objDb);

            dic["method"] = method;
            dic["site_type"] = ErsFactory.ersUtilityFactory.getSetup().site_type;
            if (ErsContext.sessionState != null)
            {
                dic["log_user_id"] = ErsContext.sessionState.Get("log_user_id");
            }

            log_table.gInsert(dic, false);
        }
    }
}
