﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using jp.co.ivp.ers.mvc;
using System.IO;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// Text Backup
    /// </summary>
    public class ErsTextBackup
    {
        /// <summary>
        /// DataRowの値のバックアップを作成する。
        /// </summary>
        /// <param name="tableName">name of the table</param>
        /// <param name="method">method</param>
        /// <param name="dic">values saved in dictionary</param>
        public virtual void BackUp(string tableName, EnumBackupMethod method, Dictionary<string, object> dic)
        {
            //Body作成
            string textBody = string.Empty;

            var table = new ErsDB_parent(tableName);
            foreach (DataColumn dc in table.Schema)
            {
                textBody += ",";
                var key = dc.ColumnName;

                if (!dic.ContainsKey(key))
                {
                    continue;
                }
                if (dic[key] == DBNull.Value)
                {
                    continue;
                }

                if (dic[key] is Array)
                {
                    var arrStr = new List<string>();
                    foreach (var value in (Array)dic[key])
                    {
                        if (value is Enum)
                        {
                            arrStr.Add(Convert.ToString((int)value));
                        }
                        else
                        {
                            arrStr.Add(value.ToString());
                        }
                    }
                    textBody += "{" + string.Join(",", arrStr.ToArray()) + "}";
                }
                else
                {
                    if (dic[key] is Enum)
                    {
                        textBody += Convert.ToString(Convert.ChangeType(dic[key], ((Enum)dic[key]).GetTypeCode()));
                    }
                    else
                    {
                        textBody += dic[key];
                    }
                }
            }

            textBody = textBody.Substring(1) + Environment.NewLine;

            //書き込みパス作成
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var d_back_path = setup.d_back_path;
            if(!d_back_path.EndsWith("\\"))
            {
                d_back_path += "\\";
            }
            if (!System.IO.Directory.Exists(d_back_path))
            {
                throw new Exception("The path of d_back_path is not prepared.");
            }

            var path = d_back_path + DateTime.Now.ToString("yyyyMMdd") + "_" + tableName + "_" + method.ToString().ToLower() + ".inc";

            //暗号化
            var objEnc = ErsFactory.ersUtilityFactory.getErsEncryption();

            if (!objEnc.EncodeBuckup(textBody, path))
            {
                throw new Exception("The process of backup was fail.");
            }
        }
    }
}
