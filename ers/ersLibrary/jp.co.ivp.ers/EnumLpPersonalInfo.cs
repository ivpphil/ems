﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumLpPersonalInfo
    {
        /// <summary>
        /// 1: 登録済みの個人情報を指定する
        /// </summary>
        UseRegisterd = 1,

        /// <summary>
        /// 2: 外部URLを指定する
        /// </summary>
        UseUrl
    }
}
