﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// モール伝票ステータス [Mall order status]
    /// </summary>
    public enum EnumMallOrderStatus
    {
        /// <summary>
        /// 1 : 新規 [New]
        /// </summary>
        New = 1,

        /// <summary>
        /// 2 : 処理済み [Done]
        /// </summary>
        Done = 2,

        /// <summary>
        /// 3 : 処理中 [Processing]
        /// </summary>
        Processing = 3,

        /// <summary>
        /// 4 : 発送待ち [Waiting shipment]
        /// </summary>
        WaitingShipment = 4,

        /// <summary>
        /// 5 : 発送後入金待ち [Waiting payment]
        /// </summary>
        WaitingPayment = 5,

        /// <summary>
        /// -1 : キャンセル [Canceled]
        /// </summary>
        Canceled = -1,

        /// <summary>
        /// -2 : 保留 [Pending]
        /// </summary>
        Pending = -2,

        /// <summary>
        /// -3 : 出荷不可 [Unfulfillable]
        /// </summary>
        Unfulfillable = -3
    }
}
