﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumSendTo
        : short
    {
        MEMBER_ADDRESS = 1,
        ANOTHER_ADDRESS
    }
}
