﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumEnqCorresponding
        : short
    {
        /// <summary>
        /// 0
        /// </summary>
        Phone,
        /// <summary>
        /// 1
        /// </summary>
        Email,
        /// <summary>
        /// 2
        /// </summary>
        Memo
    }
}
