﻿
namespace jp.co.ivp.ers
{
    public enum EnumHistoryProcessType
    {
        /// <summary>
        /// 問い合わせ
        /// </summary>
        ENQ,

        /// <summary>
        /// 伝票
        /// </summary>
        DMT,

        /// <summary>
        /// 仮受注
        /// </summary>
        CTS,

        /// <summary>
        /// 別お届け先
        /// </summary>
        ADD,

        /// <summary>
        /// 定期
        /// </summary>
        RET,

        /// <summary>
        /// 定期明細
        /// </summary>
        RDT,

        /// <summary>
        /// ポイント
        /// </summary>
        PNT,

        /// <summary>
        /// 会員
        /// </summary>
        MEM,

        /// <summary>
        /// カード
        /// </summary>
        MCD,
    }
}
