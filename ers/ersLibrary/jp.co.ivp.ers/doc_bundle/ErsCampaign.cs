﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.doc_bundle
{
    public class ErsCampaign
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public virtual string ccode { get; set; }

        public virtual int? target_id { get; set; }

        public virtual EnumActive? active { get; set; }

        public virtual string campaign_name { get; set; }

        public virtual DateTime? term_from { get; set; }

        public virtual DateTime? term_to { get; set; }

        public virtual string scode { get; set; }

        public virtual string advertise_name { get; set; }

        public virtual string advertise_code { get; set; }

        public virtual int? advertise_cost { get; set; }

        public virtual string advertise_area { get; set; }

        public virtual string remarks { get; set; }

        public virtual DateTime? intime { get; set; }

        public virtual DateTime? utime { get; set; }

        public virtual string w_active { get; set; }

        public virtual string gcode { get; set; }

        public virtual int? price { get; set; }

        public virtual int campaign_type { get; set; }

        public virtual int[] site_id { get; set; }
    }
}
