﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.doc_bundle
{
    public class ErsDocBundling
        : ErsRepositoryEntity
    {

        public override int? id { get; set; }

        public virtual string ccode { get; set; }

        public virtual string scode { get; set; }

        public virtual int? amount { get; set; }

        public virtual DateTime? intime { get; set; }

        public virtual DateTime? utime { get; set; }

        public virtual EnumDocDuplicate? duplicate { get; set; }
    }
}
