﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.doc_bundle
{
    public class ErsDocTarget
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public virtual string ccode { get; set; }

        public virtual string scode { get; set; }

        public virtual EnumOrderType? order_type { get; set; }

        public virtual DateTime? intime { get; set; }

        public virtual DateTime? utime { get; set; }

    }
}
