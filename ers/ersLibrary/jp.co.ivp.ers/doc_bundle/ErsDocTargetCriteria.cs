﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.doc_bundle
{
    public class ErsDocTargetCriteria
        :Criteria
    {
        public virtual int? id { set { this.Add(Criteria.GetCriterion("doc_target_t.id", value, Operation.EQUAL)); } }

        public virtual string ccode { set { this.Add(Criteria.GetCriterion("doc_target_t.ccode", value, Operation.EQUAL)); } }

        public virtual string scode { set { this.Add(Criteria.GetCriterion("doc_target_t.scode", value, Operation.EQUAL)); } }

        public virtual int? s_sale_ptn { set { this.Add(Criteria.GetCriterion("doc_target_t.s_sale_ptn", value, Operation.EQUAL)); } }

        public virtual IEnumerable<int> s_sale_ptn_in { set { this.Add(Criteria.GetInClauseCriterion("doc_target_t.s_sale_ptn", value)); } }

        public virtual DateTime? intime { set { this.Add(Criteria.GetCriterion("doc_target_t.intime", value, Operation.EQUAL)); } }

        public virtual DateTime? utime { set { this.Add(Criteria.GetCriterion("doc_target_t.utime", value, Operation.EQUAL)); } }

        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("doc_target_t.id", orderBy);
        }
    }
}
