﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.doc_bundle.strategy
{
    public class CampaignCheckExistStrategy
    {
        public virtual IEnumerable<ValidationResult> Check(string ccode)
        {

            if (string.IsNullOrEmpty(ccode))
            {
                yield break;
            }

            if (!this.CheckCcodeExist(ccode))
            {
                yield return new ValidationResult(ErsResources.GetMessage(("30301"), ccode), new[] { "ccode" });
            }

        }

        /// <summary>
        /// ccode存在チェック
        /// </summary>
        /// <param name="ccode"></param>
        /// <returns></returns>
        public virtual Boolean CheckCcodeExist(string ccode)
        {
            var repository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();
            var criteria = ErsFactory.ersDocBundleFactory.GetErsCampaignCriteria();
            criteria.ccode = ccode;
            return (repository.GetRecordCount(criteria) > 0);
        }

    }
}
