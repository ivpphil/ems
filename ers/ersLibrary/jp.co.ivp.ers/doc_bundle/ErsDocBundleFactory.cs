﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.doc_bundle.strategy;



namespace jp.co.ivp.ers.doc_bundle
{
    /// <summary>
    /// Factory
    /// </summary>
    public class ErsDocBundleFactory
    {

        protected static ErsCampaignRepository _ErsCampaignRepository
        {
            get
            {
                return (ErsCampaignRepository)ErsCommonContext.GetPooledObject("_ErsCampaignRepository");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_ErsCampaignRepository", value);
            }
        }

        /// <summary>
        /// Gets an instance of ErsCampaignCriteria.
        /// </summary>
        /// <returns></returns>
        public virtual ErsCampaignCriteria GetErsCampaignCriteria()
        {
            return new ErsCampaignCriteria();
        }

        /// <summary>
        /// Gets an instance of ErsCampaignRepository.
        /// </summary>
        /// <returns></returns>
        public virtual ErsCampaignRepository GetErsCampaignRepository()
        {
            return new ErsCampaignRepository();
        }

        /// <summary>
        /// Gets an instance of ErsDocBundlingCriteria.
        /// </summary>
        /// <returns></returns>
        public virtual ErsDocBundlingCriteria GetErsDocBundlingCriteria()
        {
            return new ErsDocBundlingCriteria();
        }

        /// <summary>
        /// Gets an instance of ErsDocBundlingRepository.
        /// </summary>
        /// <returns></returns>
        public virtual ErsDocBundlingRepository GetErsDocBundlingRepository()
        {
            return new ErsDocBundlingRepository();
        }

        /// <summary>
        /// Gets an instance of ErsCampaign.
        /// </summary>
        /// <returns></returns>
        public virtual ErsCampaign GetErsCampaign()
        {
            return new ErsCampaign();
        }

        /// <summary>
        /// Gets an instance of ErsCampaign which is overwritten with parameters.
        /// </summary>
        /// <returns></returns>
        public virtual ErsCampaign GetErsCampaignWithParameter(Dictionary<string, object> parameters)
        {
            var campaign = this.GetErsCampaign();
            campaign.OverwriteWithParameter(parameters);
            return campaign;
        }

        /// <summary>
        /// Gets an instance of ErsDocBundling.
        /// </summary>
        /// <returns></returns>
        public virtual ErsDocBundling GetErsDocBundling()
        {
            return new ErsDocBundling();
        }

        /// <summary>
        /// Gets an instance of ErsDocTarget.
        /// </summary>
        /// <returns></returns>
        public virtual ErsDocTarget GetErsDocTarget()
        {
            return new ErsDocTarget();
        }

        /// <summary>
        /// Gets an instance of ErsDocBundlingCriteria.
        /// </summary>
        /// <returns></returns>
        public virtual ErsDocTargetCriteria GetErsDocTargetCriteria()
        {
            return new ErsDocTargetCriteria();
        }

        /// <summary>
        /// Gets an instance of ErsDocBundlingRepository.
        /// </summary>
        /// <returns></returns>
        public virtual ErsDocTargetRepository GetErsDocTargetRepository()
        {
            return new ErsDocTargetRepository();
        }

        /// <summary>
        /// Gets an instance of ErsDocBundling which is overwritten with parameters.
        /// </summary>
        /// <returns></returns>
        public virtual ErsDocBundling GetErsDocBundlingWithParameter(Dictionary<string, object> parameters)
        {
            var docBundling = this.GetErsDocBundling();
            docBundling.OverwriteWithParameter(parameters);
            return docBundling;
        }

        public virtual ErsDocTarget GetErsDocTargetWithParameter(Dictionary<string, object> parameters)
        {
            var docTarget = this.GetErsDocTarget();
            docTarget.OverwriteWithParameter(parameters);
            return docTarget;
        }

        public virtual ErsDocTarget GetErsDocTargetWithModel(ErsModelBase model)
        {
            var docTarget = this.GetErsDocTarget();
            docTarget.OverwriteWithModel(model);
            return docTarget;
        }

        /// <summary>
        ///  Gets instance of CampaignCheckExistStrategy
        /// </summary>
        /// <returns></returns>
        public virtual CampaignCheckExistStrategy GetCampaignCheckExistStrategy()
        {
            return new CampaignCheckExistStrategy();
        }

        /// <summary>
        /// IDをもとに、クラスを取得する。
        /// </summary>
        /// <param name="mcode"></param>
        /// <returns></returns>
        public virtual ErsCampaign getErsCampaignWithId(int? id)
        {
            var repository = GetErsCampaignRepository();

            var c = this.GetErsCampaignCriteria();
            c.id = id;
            var list = repository.Find(c);
            if (list.Count != 1)
                return null;

            return list[0];
        }

        public virtual ErsCampaign GetErsCampaignWithModel(ErsModelBase model)
        {
            var Campaign = this.GetErsCampaign();
            Campaign.OverwriteWithModel(model);
            return Campaign;
        }
    }
}
