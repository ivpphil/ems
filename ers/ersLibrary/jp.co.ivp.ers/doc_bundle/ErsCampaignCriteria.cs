﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.member;

namespace jp.co.ivp.ers.doc_bundle
{
    public class ErsCampaignCriteria
        : Criteria
    {

        public virtual int? id { set { this.Add(Criteria.GetCriterion("campaign_t.id", value, Operation.EQUAL)); } }

         public virtual int? id_not_equal { set { this.Add(Criteria.GetCriterion("campaign_t.id", value, Operation.NOT_EQUAL)); } }
        
        public virtual string ccode
        {
            set
            {
                if (value.HasValue())
                {
                    value = value.ToUpper();
                }

                var parameters = new Dictionary<string, object>();
                parameters.Add("ccode",value);
                var strSQL = "UPPER(campaign_t.ccode) = :ccode";
                this.Add(Criteria.GetUniversalCriterion(strSQL, parameters));
            } 
        }

        public virtual int? target_id { set { this.Add(Criteria.GetCriterion("campaign_t.target_id", value, Operation.EQUAL)); } }

        public virtual EnumActive? active { set { this.Add(Criteria.GetCriterion("campaign_t.active", value, Operation.EQUAL)); } }

        public virtual string campaign_name { set { this.Add(Criteria.GetCriterion("campaign_t.campaign_name", value, Operation.EQUAL)); } }

        public virtual string scode { set { this.Add(Criteria.GetCriterion("campaign_t.scode", value, Operation.EQUAL)); } }

        public virtual string remarks { set { this.Add(Criteria.GetCriterion("campaign_t.remarks", value, Operation.EQUAL)); } }

        public virtual DateTime? intime { set { this.Add(Criteria.GetCriterion("campaign_t.intime", value, Operation.EQUAL)); } }

        public virtual DateTime? utime { set { this.Add(Criteria.GetCriterion("campaign_t.utime", value, Operation.EQUAL)); } }

        public virtual int? additional_type { set { this.Add(Criteria.GetCriterion("campaign_t.additional_type", value, Operation.EQUAL)); } }

        public virtual int? additional_item_kbn { set { this.Add(Criteria.GetCriterion("campaign_t.additional_item_kbn", value, Operation.EQUAL)); } }

        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("campaign_t.id", orderBy);
        }

        /// <summary>
        /// 同梱名称(Like)
        /// </summary>
        public virtual string campaign_name_like
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("campaign_t.campaign_name", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        /// <summary>
        /// 実施期間
        /// </summary>
        public virtual DateTime? term_from
        {
            set { this.Add(Criteria.GetCriterion("campaign_t.term_from", value, Operation.GREATER_EQUAL)); }
        }
        public virtual DateTime? term_to
        {
            set { this.Add(Criteria.GetCriterion("campaign_t.term_to", value, Operation.LESS_EQUAL)); }
        }

        /// <summary>
        /// 実施期間(指定日が期間内を取得)
        /// </summary>
        public virtual DateTime? term_from_in_range
        {
            set { this.Add(Criteria.GetCriterion("campaign_t.term_from", value, Operation.LESS_EQUAL)); }
        }
        public virtual DateTime? term_to_in_range
        {
            set { this.Add(Criteria.GetCriterion("campaign_t.term_to", value, Operation.GREATER_EQUAL)); }
        }

        public void SetTargetCondition(IEnumerable<ErsOrderRecord> orderRecords)
        {
            var listCriteria = new List<CriterionBase>();
            foreach (var orderRecord in orderRecords)
            {
                if (orderRecord.order_type == EnumOrderType.DocBundled)
                {
                    continue;
                }

                var parameters = new Dictionary<string, object>();
                parameters.Add("scode", orderRecord.scode);
                parameters.Add("order_type", (int?)orderRecord.order_type);
                parameters.Add("common_order_type", (int?)EnumOrderType.BothUsuallyAndSubscription);
                var strSQL = "EXISTS(SELECT * FROM doc_target_t WHERE scode = :scode AND order_type IN (:order_type, :common_order_type) AND ccode = campaign_t.ccode)";
                listCriteria.Add(Criteria.GetUniversalCriterion(strSQL, parameters));
            }

            // 対象商品が設定されていない場合も対象とする
            listCriteria.Add(Criteria.GetUniversalCriterion("NOT EXISTS(SELECT * FROM doc_target_t WHERE ccode = campaign_t.ccode)"));

            this.Add(Criteria.JoinWithOR(listCriteria));
        }

        public void SetActiveOnly()
        {
            this.active = EnumActive.Active;
            this.Add(Criteria.GetBetweenCriterion(ColumnName("campaign_t.term_from"), ColumnName("campaign_t.term_to"), ColumnName("current_timestamp")));
        }

        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("campaign_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("campaign_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}
