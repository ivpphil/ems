﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.doc_bundle
{
    public class ErsDocTargetRepository
        : ErsRepository<ErsDocTarget>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsDocTargetRepository()
            : base("doc_target_t")
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsDocTargetRepository(ErsDatabase objDB)
            : base("doc_target_t", objDB)
        {
        }

        [Obsolete("idベースでUPDATEするように修正したら削除")]
        public override void Update(ErsDocTarget old_obj, ErsDocTarget new_obj)
        {
            var newDic = new_obj.GetPropertiesAsDictionary(this.ersDB_table);
            var oldDic = old_obj.GetPropertiesAsDictionary(this.ersDB_table);
            oldDic.Remove("intime");
            newDic.Remove("intime");
            oldDic.Remove("id");
            newDic.Remove("id");
            var changedColumns = ErsCommon.GetChangedKeys(oldDic, newDic);
            if (changedColumns.Length > 0)
            {
                this.ersDB_table.gUpdateColumn(changedColumns, newDic, "WHERE ccode = '" + old_obj.ccode + "' AND scode = '" + old_obj.scode + "'");
            }
        }
    }
}
