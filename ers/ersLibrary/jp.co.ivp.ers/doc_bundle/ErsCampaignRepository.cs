﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.doc_bundle
{
    public class ErsCampaignRepository
        : ErsRepository<ErsCampaign>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsCampaignRepository()
            : base("campaign_t")
        {
        }

        /// <summary>
        /// コンストラクタ(ユニットテスト用)
        /// </summary>
        public ErsCampaignRepository(ErsDatabase objDB)
            : base("campaign_t", objDB)
        {
        }
    }
}
