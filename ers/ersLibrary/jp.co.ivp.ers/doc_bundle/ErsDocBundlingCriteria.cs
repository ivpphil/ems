﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.doc_bundle
{
    public class ErsDocBundlingCriteria
        : Criteria
    {

        public virtual int? id { set { this.Add(Criteria.GetCriterion("doc_bundling_t.id", value, Operation.EQUAL)); } }

        public virtual string ccode { set { this.Add(Criteria.GetCriterion("doc_bundling_t.ccode", value, Operation.EQUAL)); } }

        public virtual string scode { set { this.Add(Criteria.GetCriterion("doc_bundling_t.scode", value, Operation.EQUAL)); } }

        public virtual int? amount { set { this.Add(Criteria.GetCriterion("doc_bundling_t.amount", value, Operation.EQUAL)); } }

        public virtual DateTime? intime { set { this.Add(Criteria.GetCriterion("doc_bundling_t.intime", value, Operation.EQUAL)); } }

        public virtual DateTime? utime { set { this.Add(Criteria.GetCriterion("doc_bundling_t.utime", value, Operation.EQUAL)); } }

        public void SetDupplicate(string mcode)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("mcode", mcode);
            parameters.Add("EnumDocDuplicateOn", (int)EnumDocDuplicate.On);
            
            EnumOrderStatusType[] CancelStatus = ErsFactory.ersOrderFactory.GetErsOrderCriteria().CancelStatusArray;
            string cancelstatus = "";

            for (int i = 0; i < CancelStatus.Count(); i++)
            {
                cancelstatus = cancelstatus + ((int)CancelStatus[i]).ToString();

                if (i < CancelStatus.Count() - 1)
                {
                    cancelstatus = cancelstatus + ",";
                }
            }

            var strSQL = "CASE WHEN doc_bundling_t.duplicate = :EnumDocDuplicateOn THEN true ";
            strSQL += "ELSE NOT EXISTS(SELECT * FROM ds_master_t INNER JOIN d_master_t ON ds_master_t.d_no = d_master_t.d_no WHERE mcode = :mcode AND order_status not in (" + cancelstatus + ") AND scode = doc_bundling_t.scode) END ";

            this.Add(Criteria.GetUniversalCriterion(strSQL, parameters));
        }

        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("doc_bundling_t.id", orderBy);
        }
    }
}
