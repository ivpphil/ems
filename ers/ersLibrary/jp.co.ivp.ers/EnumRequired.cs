﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumRequired
    {
        /// <summary>
        /// 0: 非必須
        /// </summary>
        NonRequired = 0,

        /// <summary>
        /// 1: 必須
        /// </summary>
        Required
    }
}
