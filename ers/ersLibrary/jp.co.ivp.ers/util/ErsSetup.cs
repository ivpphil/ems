﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.util
{

    /// <summary>
    /// Hold values from setup_t table.
    /// Inherits ErsRepositoryEntity class.
    /// Initial Setting master.
    /// </summary>/// 
    public class ErsSetup
        :ErsRepositoryEntity
    {

        /// <summary>
        /// Constructor method
        /// </summary>
        public ErsSetup()
        {
        }

        /// <summary>
        /// Primary key ID
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// Reply to email address
        /// </summary>
        public virtual string r_email { get; set; }

        /// <summary>
        /// Forward email address at 1st time
        /// </summary>
        public virtual string f_email1 { get; set; }

        /// <summary>
        /// Forward email address at 2nd time
        /// </summary>
        public virtual string f_email2 { get; set; }

        /// <summary>
        /// Forward email address at 3rd time
        /// </summary>
        public virtual string f_email3 { get; set; }

        /// <summary>
        /// Free shipping
        /// </summary>
        public virtual int? free { get; set; }

        /// <summary>
        /// Set delivery date (can be set from days)
        /// </summary>
        public virtual int? sendday { get; set; }

        /// <summary>
        /// Set delivery date (can be set to days)
        /// </summary>
        public virtual int? sendday_count { get; set; }

        /// <summary>
        /// Delivery
        /// </summary>
        public virtual string delivery { get; set; }

        /// <summary>
        /// URL
        /// </summary>
        public virtual string url { get; set; }

        /// <summary>
        /// Tax rate
        /// </summary>
        public virtual int? tax { get; set; }

        /// <summary>
        /// Tax rate
        /// </summary>
        public virtual EnumOnOff? enable_carriage_tax { get; set; }

        /// <summary>
        /// 1st Category
        /// </summary>
        public virtual string cate1 { get; set; }

        /// <summary>
        /// Show/Hide 1st Category
        /// </summary>
        public virtual EnumActive? cate1_active { get; set; }

        /// <summary>
        /// 2nd Category
        /// </summary>
        public virtual string cate2 { get; set; }

        /// <summary>
        /// Show/Hide 2nd Category
        /// </summary>
        public virtual EnumActive? cate2_active { get; set; }

        /// <summary>
        /// 3rd Category
        /// </summary>
        public virtual string cate3 { get; set; }

        /// <summary>
        /// Show/Hide 3rd Category
        /// </summary>
        public virtual EnumActive? cate3_active { get; set; }

        /// <summary>
        /// 4th Category
        /// </summary>
        public virtual string cate4 { get; set; }

        /// <summary>
        /// Show/Hide 4th Category
        /// </summary>
        public virtual EnumActive? cate4_active { get; set; }

        /// <summary>
        /// 5th Category
        /// </summary>
        public virtual string cate5 { get; set; }

        /// <summary>
        /// Show/Hide 5th Category
        /// </summary>
        public virtual EnumActive? cate5_active { get; set; }

        /// <summary>
        /// Privacy policy memo
        /// </summary>
        public virtual string pri_memo { get; set; }

        /// <summary>
        /// Insert time
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// Update time
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// Contact Email destination
        /// </summary>
        public virtual string quest_email_to { get; set; }

        /// <summary>
        /// UL, DL file format
        /// </summary>
        public virtual short? filekind { get; set; }

        /// <summary>
        /// Checkout screen note
        /// </summary>
        public virtual string regi_memo { get; set; }

        /// <summary>
        /// Error message
        /// </summary>
        public virtual string error_message { get; set; }

        /// <summary>
        /// Error message (for mobile)
        /// </summary>
        public virtual string m_error_message { get; set; }

        /// <summary>
        /// ランク振り分け基準
        /// </summary>
        public virtual EnumMemberRankCriterion? member_rank_criterion { get; set; }

        /// <summary>
        /// 対象期間
        /// </summary>
        public int? member_rank_term { get; set; }

        public virtual string mail_delivery { get; set; }

        public virtual string mail_url { get; set; }

        /// <summary>
        /// メール便送料
        /// </summary>
        public int mail_deliv_carriage { get; set; }

        /// <summary>
        /// 発祥元
        /// </summary>
        public virtual string wh_owner_description { get; set; }

        public virtual int? login_try_limit { get; set; }
        public virtual int shipping_csv_output_min_days { get; set; }
        public virtual int create_regular_order_days { get; set; }


    }
}
