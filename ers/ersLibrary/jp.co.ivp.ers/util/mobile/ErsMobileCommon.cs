﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.util.mobile
{
    public class ErsMobileCommon
    {
        public string userAgent { get; private set; }
        public EnumMobileCarrier? Carrier { get; private set; }
        public string Device { get; private set; }
        public string Uid { get; private set; }


        public ErsMobileCommon()
        {
            userAgent = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
            this.GetCarrier();
            this.GetDevice();
            this.GetUid();

        }

        /// <summary>
        /// キャリア判別
        /// </summary>
        private void GetCarrier()
        {
            this.Carrier = EnumMobileCarrier.OTHER;

            if (userAgent != null)
            {
                //docomo?
                if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "^DoCoMo"))
                    this.Carrier = EnumMobileCarrier.DOCOMO;

                //au?
                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "^UP.Browser"))
                    this.Carrier = EnumMobileCarrier.AU;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "^KDDI"))
                    this.Carrier = EnumMobileCarrier.AU;

                //SoftBank?
                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "^J-PHONE"))
                    this.Carrier = EnumMobileCarrier.SOFTBANK;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "^Vodafone"))
                    this.Carrier = EnumMobileCarrier.SOFTBANK;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "^SoftBank"))
                    this.Carrier = EnumMobileCarrier.SOFTBANK;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "^MOT-"))
                    this.Carrier = EnumMobileCarrier.SOFTBANK;


                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "^MOT-"))
                    this.Carrier = EnumMobileCarrier.SOFTBANK;

                //# start AU0066 2010/04/13 M.Nakagawa Google クローラ対策
                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "GOOGLEBOT"))
                    this.Carrier = EnumMobileCarrier.GoogleBot;

                //# start 2010/05/07 D.Onoe Yahoo クローラ対策
                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "Y!J-SRD"))
                    this.Carrier = EnumMobileCarrier.YahooBot;

                //#start 2010/05/07 D.Onoe Yahoo クローラ対策
                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "Y!J-MBS"))
                    this.Carrier = EnumMobileCarrier.YahooBot;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "iPhone"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "iPod"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "Android"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "dream"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "CUPCAKE"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "blackberry9500"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "blackberry9530"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "blackberry9520"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "blackberry9550"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "blackberry9800"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "webOS"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "incognito"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

                else if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "webmate"))
                    this.Carrier = EnumMobileCarrier.SmartPhone;

            }
        }


        /// <summary>
        /// 端末名称取得
        /// </summary>
        private void GetDevice()
        {

            switch (Carrier)
            {
                case EnumMobileCarrier.DOCOMO:

                    if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "/1.0"))
                    {
                        string[] ary = userAgent.Split(new string[] { "/" }, StringSplitOptions.None);
                        this.Device = ary[2];
                        break;
                    }

                    if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "/2.0"))
                    {
                        string[] tmp = userAgent.Split(new string[] { "/2.0" }, StringSplitOptions.None);
                        string[] ary = tmp[1].Split('(');
                        this.Device = ary[0];
                        break;
                    }

                    break;

                //auは機種名じゃなくデバイスIDなので注意
                case EnumMobileCarrier.AU:

                    if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "-"))
                    {
                        string[] tmp = userAgent.Split(new string[] { "-" }, StringSplitOptions.None);
                        string[] ary = tmp[1].Split(' ');
                        this.Device = ary[0];
                    }

                    break;

                case EnumMobileCarrier.SOFTBANK:

                    Device = HttpContext.Current.Request.ServerVariables["HTTP_X_JPHONE_MSNAME"];
                    break;

                default:
                    break;
            }
        }


        /// <summary>
        /// UIDを取得
        /// </summary>
        private void GetUid()
        {
            //strC_ransu = Trim(Request("c_ransu"))

            //// c_ransu がある場合は、それを機種IDとする
            //IF nkg_isEmpty(strC_ransu) = False Then
            //    Uid = strC_ransu
            //    Exit Function
            //End IF


            switch (Carrier)
            {
                case EnumMobileCarrier.DOCOMO:

                    if (string.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables["HTTPS"]) ||
                        HttpContext.Current.Request.ServerVariables["HTTPS"].Equals("off"))
                    {
                        this.Uid = HttpContext.Current.Request.ServerVariables["HTTP_X_DCMGUID"];
                    }
                    break;

                case EnumMobileCarrier.AU:
                    this.Uid = HttpContext.Current.Request.ServerVariables["HTTP_X_UP_SUBNO"];
                    break;


                case EnumMobileCarrier.SOFTBANK:
                    this.Uid = HttpContext.Current.Request.ServerVariables["HTTP_X_JPHONE_UID"];
                    break;

            }

            //Buffer Overflowを防ぐため、32文字以上を切り捨て
            if (this.Uid != null && this.Uid.Length > 32) this.Uid = this.Uid.Substring(0, 32);

            if (userAgent != null)
            {
                //クローラー対応
                if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "GOOGLEBOT"))
                    this.Uid = "Googlebot";

                if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "Y!J-SRD"))
                    this.Uid = "Yahoobot";

                if (System.Text.RegularExpressions.Regex.IsMatch(userAgent, "Y!J-MBS"))
                    this.Uid = "Yahoobot";
            }
        }

        public virtual EnumSiteType GetSiteType()
        {
            if (this.isMobile())
            {
                return EnumSiteType.MOBILE;
            }

            if (this.isSmartPhone())
            {
                return EnumSiteType.SMARTPHONE;
            }

            return EnumSiteType.PC;
        }

        /// <summary>
        /// モバイルからのアクセスか
        /// </summary>
        /// <returns></returns>
        private Boolean isMobile()
        {
            if (this.isSmartPhone())
            {
                return false;
            }

            if (Carrier == EnumMobileCarrier.GoogleBot)
            {
                return false;
            }

            if (Carrier == EnumMobileCarrier.OTHER)
            {
                return false;
            }

            return true;
        }


        /// <summary>
        /// スマホからのアクセスか
        /// </summary>
        /// <returns></returns>
        private Boolean isSmartPhone()
        {
            if (this.Carrier == EnumMobileCarrier.SmartPhone)
                return true;
            else
                return false;
        }
    }
}
