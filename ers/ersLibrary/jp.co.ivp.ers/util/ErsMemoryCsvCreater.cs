﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.util
{
    public class ErsMemoryCsvCreater
        : ErsCsvCreater
    {
        public ErsMemoryCsvCreater()
        { 
        }

        bool HasEmptyHeader = false;

        public static string DEFAULT_NULL = Npgsql.NpgsqlCopySerializer.DEFAULT_NULL; //Default Postgres Null Value

        public static string DEFAULT_DELIMITER = Npgsql.NpgsqlCopySerializer.DEFAULT_DELIMITER; //Default Postgres tab csv delimiter

        public string NonQuotedNullString
        {
            get
            {
                return DEFAULT_NULL;
            }
            set
            {
                DEFAULT_NULL = value;
            }
        }

        public string delimiter
        {
            get
            {
                return DEFAULT_DELIMITER;
            }
            set
            {
                DEFAULT_DELIMITER = value;
            }
        }

        /// <summary>
        /// ファイルのStreamWriterを取得する。（Shift-JIS）
        /// <para>Retreive the file using StreamWriter（Shift-JIS）</para>
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="mode">true:追記(省略時)/false:上書き</param>
        /// <returns></returns>
        public virtual System.IO.StreamWriter GetWriter(System.IO.Stream stream, Encoding enc = null)
        {
            if (enc == null)
            {
                enc = ErsEncoding.ShiftJIS;
            }

            return new System.IO.StreamWriter(stream, enc);
        }


        public virtual void writeEmptyHeader(System.IO.StreamWriter writer)
        {
            if (!this.HasEmptyHeader)
            {
                writer.WriteLine(string.Empty);
            }
        }

        /// <summary>
        /// CSVボディを出力する。
        /// <para>Output the CSV body</para>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="writer"></param>
        public override void WriteBody(Dictionary<string, object> dictionary, System.IO.StreamWriter writer)
        {
            this.WriteBody(dictionary, writer, (value) => (value == DEFAULT_NULL) ? value : "\"" + value.Replace("\"", "\"\"") + "\"");
        }

        /// <summary>
        /// CSVボディを出力する。
        /// <para>Output the CSV body</para>
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="writer"></param>
        public override void WriteBody(Dictionary<string, object> dictionary, System.IO.StreamWriter writer, Func<string, string> formatter)
        {
            var body = string.Empty;
            foreach (var key in dictionary.Keys)
            {
                body += DEFAULT_DELIMITER;
                var value = dictionary[key];
                if (value != null)
                {
                    var val = string.Empty;
                    if (value is Array)
                    {
                        foreach (object o in (Array)value)
                        {
                            val += "," + o.ToString();
                        }

                        if (!string.IsNullOrEmpty(val))
                            val = val.Substring(1);
                    }
                    else if (value is Enum)
                    {
                        val = Convert.ToString(Convert.ChangeType(value, ((Enum)value).GetTypeCode()));
                    }
                    else
                    {
                        val = Convert.ToString(value);
                    }
                    body += formatter(val);
                }
            }

            foreach (var arrchar in replaceChar)
            {
                body = body.Replace(arrchar[0], arrchar[1]);
            }

            writer.WriteLine(body.Substring(1));
        }

        char[][] replaceChar = new[] { 
            new[] { (char)0x2212, (char)0xFF0D },
            new[] { (char)0x301C, (char)0xFF5E },
            new[] { (char)0x00A2, (char)0xFFE0 },
            new[] { (char)0x00A3, (char)0xFFE1 },
            new[] { (char)0x00AC, (char)0xFFE2 },
            new[] { (char)0x2014, (char)0x2015 },
            new[] { (char)0x2016, (char)0x2225 },
        };
    }
}
