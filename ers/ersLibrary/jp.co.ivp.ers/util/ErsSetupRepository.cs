﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.util
{
    /// <summary>
    /// Provide methods to connect with setup_t table. 
    /// Inherits ErsRepository<ErsSetup>
    /// </summary>/// 
    public class ErsSetupRepository
        : ErsRepository<ErsSetup>
    {
        public ErsSetupRepository()
            : base("setup_t")
        {
        }
    }
}
