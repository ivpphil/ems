﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.util
{
    public class ErsCsvCreater
    {
        public static string DOWNLOAD_FILE_PATH;

        public ErsCsvCreater()
        {
            if (ErsCommonContext.IsBatch)
            {
                DOWNLOAD_FILE_PATH = string.Empty;
            }
            else
            {
                DOWNLOAD_FILE_PATH = ErsCommonContext.MapPath("~/file_download/");
            }
        }

        /// <summary>
        /// 出力されたCSVのファイルパス
        /// <para>Returns the File Path of CSV file</para>
        /// </summary>
        public string filePath { get; protected set; }

        /// <summary>
        /// フィールドタイプ [Field type]
        /// </summary>
        protected virtual Type fieldAttributeType
        {
            get
            {
                return typeof(CsvFieldAttribute);
            }
        }

        /// <summary>
        /// ファイルのStreamWriterを取得する。（Shift-JIS）
        /// <para>Retreive the file using StreamWriter（Shift-JIS）</para>
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="mode">true:追記(省略時)/false:上書き</param>
        /// <returns></returns>
        public virtual StreamWriter GetWriter(string fileName, bool mode = true, Encoding enc = null)
        {
            return this.GetWriter(ErsCsvCreater.DOWNLOAD_FILE_PATH, fileName, mode, enc);
        }

        /// <summary>
        /// ファイルのStreamWriterを取得する。（Shift-JIS）
		/// <para>Retrieve the file on StreamWriter（Shift-JIS）</para>
        /// </summary>
        /// <param name="path">path</param>
        /// <param name="fileName">fileName</param>
        /// <param name="mode">true:追記(省略時)/false:上書き</param>
        /// <returns></returns>
        public virtual StreamWriter GetWriter(string path, string fileName, bool mode = true, Encoding enc = null)
        {
            if (enc == null)
            {
                enc = ErsEncoding.ShiftJIS;
            }
            
            this.filePath = path + fileName;

            return new System.IO.StreamWriter(this.filePath, mode, enc);
        }

        /// <summary>
        /// CSVヘッダーを出力する。
        /// <para>Output the CSV Header</para>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="writer"></param>
        public void WriteCsvHeader<T>(StreamWriter writer)
            where T : ErsModelBase
        {
            var columns = this.getHeaderColumnsFromModel(typeof(T));
            this.WriteCsvHeader(columns, writer, value => "\"" + value + "\"");
        }

        /// <summary>
        /// CSVヘッダーを出力する。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="writer"></param>
        /// <param name="formatter"></param>
        public void WriteCsvHeader<T>(StreamWriter writer, Func<string, string> formatter)
            where T : ErsModelBase
        {
            var columns = this.getHeaderColumnsFromModel(typeof(T));
            this.WriteCsvHeader(columns, writer, formatter);
        }

        /// <summary>
        /// モデルからヘッダの一覧を取得
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private List<string> getHeaderColumnsFromModel(Type t)
        {
            var columns = new List<string>();

            var properties = t.GetProperties()
                .Where(prop => prop.GetCustomAttributes(this.fieldAttributeType, false).Length > 0)
                .OrderBy(
                    prop => prop.GetCustomAttributes(false).OfType<ISortableAttribute>().First().Order
                    );

            foreach (var property in properties)
            {
                if (property.GetCustomAttributes(this.fieldAttributeType, false).Length > 0)
                {
                    var FieldNameKey = ErsResources.GetDisplayName(property.GetCustomAttributes(true).OfType<Attribute>());
                    if (string.IsNullOrEmpty(FieldNameKey))
                        FieldNameKey = property.Name;
                    columns.Add(ErsResources.GetFieldName(FieldNameKey));
                }
            }
            return columns;
        }

        /// <summary>
        /// CSVヘッダーを出力する。
        /// <para>Output the CSV Header</para>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="writer"></param>
        public void WriteCsvHeader(IEnumerable<string> columns, StreamWriter writer)
        {
            this.WriteCsvHeader(columns, writer, value => "\"" + value + "\"");
        }

        /// <summary>
        /// CSVヘッダーを出力する。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="writer"></param>
        /// <param name="formatter"></param>
        public virtual void WriteCsvHeader(IEnumerable<string> columns, StreamWriter writer, Func<string, string> formatter)
        {
            var header = string.Empty;
            foreach (var column in columns)
            {
                header += "," + formatter(column);
            }
            if (!string.IsNullOrEmpty(header))
            {
                writer.WriteLine(header.Substring(1));
            }
        }

        /// <summary>
        /// CSVボディを出力する。
        /// <para>Output the CSV body</para>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="writer"></param>
        public virtual void WriteBody<T>(T model, StreamWriter writer)
            where T : ErsModelBase
        {
            var dictionary = model.GetPropertiesAsDictionary(this.fieldAttributeType);

            this.WriteBody(dictionary, writer, (value) => "\"" + value.Replace("\"", "\"\"") + "\"");
        }

        /// <summary>
        /// CSVボディを出力する。
        /// <para>Output the CSV body</para>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="writer"></param>
        public virtual void WriteBody<T>(T model, StreamWriter writer, Func<string, string> formatter)
            where T : ErsModelBase
        {
            var dictionary = model.GetPropertiesAsDictionary(this.fieldAttributeType);

            this.WriteBody(dictionary, writer, formatter);
        }

        /// <summary>
        /// CSVボディを出力する。
        /// <para>Output the CSV body</para>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="writer"></param>
        public virtual void WriteBody(Dictionary<string, object> dictionary, StreamWriter writer)
        {
            this.WriteBody(dictionary, writer, (value) => "\"" + value.Replace("\"", "\"\"") + "\"");
        }

        /// <summary>
        /// CSVボディを出力する。
        /// <para>Output the CSV body</para>
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="writer"></param>
        public virtual void WriteBody(Dictionary<string, object> dictionary, StreamWriter writer, Func<string, string> formatter)
        {
            var body = string.Empty;
            foreach (var key in dictionary.Keys)
            {
                body += ",";
                var value = dictionary[key];
                if (value != null)
                {
                    var val = string.Empty;
                    if (value is Array)
                    {
                        foreach (object o in (Array)value)
                        {
                            val += "," + o.ToString();
                        }

                        if (!string.IsNullOrEmpty(val))
                            val = val.Substring(1);
                    }
                        else if(value is Enum)
                    {
                        val = Convert.ToString(Convert.ChangeType(value, ((Enum)value).GetTypeCode()));
                    }
                    else
                    {
                        val = Convert.ToString(value);
                    }
                    body += formatter(val);
                }
            }

            foreach (var arrchar in replaceChar)
            {
                body = body.Replace(arrchar[0], arrchar[1]);
            }

            writer.WriteLine(body.Substring(1));
        }

        char[][] replaceChar = new[] { 
            new[] { (char)0x2212, (char)0xFF0D },
            new[] { (char)0x301C, (char)0xFF5E },
            new[] { (char)0x00A2, (char)0xFFE0 },
            new[] { (char)0x00A3, (char)0xFFE1 },
            new[] { (char)0x00AC, (char)0xFFE2 },
            new[] { (char)0x2014, (char)0x2015 },
            new[] { (char)0x2016, (char)0x2225 },
        };
    }
}
