﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.merchandise.stock;

namespace jp.co.ivp.ers.util
{
    /// <summary>
    /// Class for Setup.
    /// Implements SetupConfigReader
    /// </summary>
    public class Setup
        : SetupConfigReader
    {

        internal Setup()
        {
        }

        /// <summary>
        /// Gets and sets pooled setup
        /// </summary>
        protected static Dictionary<string, object> _setup_tValue
        {
            get
            {
                return (Dictionary<string, object>)ErsCommonContext.GetPooledObject("_setupValue");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_setupValue", value);
            }
        }

        /// <summary>
        /// Gets list of records from setup_t table using ErsDB_setup_t based on the site_id.
        /// </summary>
        protected virtual Dictionary<string, object> setup_tValue
        {
            get
            {
                if (_setup_tValue == null)
                {
                    var objSetup = ErsFactory.ersUtilityFactory.GetErsSetupOfSite();
                    _setup_tValue = objSetup.GetPropertiesAsDictionary();
                }
                return _setup_tValue;
            }
        }

        /// <summary>
        /// Get class name of credit card payment. 
        /// </summary>
        public virtual string CreditCardPaymentClass
        {
            get
            {
                return GetCommonConfigFileValue("CreditCardPaymentClass");
            }
        }

        /// <summary>
        /// Get class name of credit card payment. 
        /// </summary>
        public virtual string ConveniencePaymentClass
        {
            get
            {
                return GetCommonConfigFileValue("ConveniencePaymentClass");
            }
        }

        /// <summary>
        /// Gets value of r_email (Reply to email address) in setup_t table using setup_tValue method.
        /// </summary>
        public virtual string r_email
        {
            get
            {
                if (!setup_tValue.ContainsKey("r_email"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["r_email"]);
            }
        }

        /// <summary>
        /// Gets value of f_email (Forward email address) in setup_t table using setup_tValue method.
        /// </summary>
        public virtual List<string> f_email
        {
            get
            {
                var ret = new List<string>();
                for (var i = 1; i <= 3; i++)
                {
                    if (!setup_tValue.ContainsKey("f_email" + (i)))
                    {
                        ret.Add(string.Empty);
                    }
                    else
                    {
                        ret.Add(Convert.ToString(setup_tValue["f_email" + (i)]));
                    }
                }
                return ret;
            }
        }

        /// <summary>
        /// Gets value of f_email1 (Forward email address at 1st time) in setup_t table using setup_tValue method.
        /// </summary>
        public virtual string f_email1
        {
            get
            {
                if (!setup_tValue.ContainsKey("f_email1"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["f_email1"]);
            }
        }

        /// <summary>
        /// Gets value of f_email2 (Forward email address at 2nd time) in setup_t table using setup_tValue method.
        /// </summary>
        public virtual string f_email2
        {
            get
            {
                if (!setup_tValue.ContainsKey("f_email2"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["f_email2"]);
            }
        }

        /// <summary>
        /// Gets value of f_email3 (Forward email address at 3rd time) in setup_t table using setup_tValue method.
        /// </summary>
        public virtual string f_email3
        {
            get
            {
                if (!setup_tValue.ContainsKey("f_email3"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["f_email3"]);
            }
        }

        /// <summary>
        /// Gets value of free (Free shipping) in setup_t table using setup_tValue method.
        /// </summary>
        public virtual int? free
        {
            get
            {
                if (!setup_tValue.ContainsKey("free"))
                {
                    return null;
                }
                return Convert.ToInt32(setup_tValue["free"]);
            }
        }

        /// <summary>
        /// Gets value of  sendday (delivery date (can be set from days)) in setup_t table using setup_tValue method.
        /// </summary>
        public virtual int sendday
        {
            get
            {
                if (!setup_tValue.ContainsKey("sendday"))
                {
                    return 0;
                }
                return Convert.ToInt32(setup_tValue["sendday"]);
            }
        }

        /// <summary>
        /// Gets value of  sendday (delivery date (can be set from days)) in setup_t table using setup_tValue method.
        /// </summary>
        public virtual int sendday_count
        {
            get
            {
                if (!setup_tValue.ContainsKey("sendday_count"))
                {
                    return 0;
                }
                return Convert.ToInt32(setup_tValue["sendday_count"]);
            }
        }

        /// <summary>
        /// Gets value of delivery in setup_t table using setup_tValue method.
        /// </summary>
        public virtual string delivery
        {
            get
            {
                if (!setup_tValue.ContainsKey("delivery"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["delivery"]);
            }
        }

        /// <summary>
        /// Gets value of URL in setup_t table using setup_tValue method.
        /// </summary>
        public virtual string url
        {
            get
            {
                if (!setup_tValue.ContainsKey("url"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["url"]);
            }
        }

        /// <summary>
        /// Gets value of delivery in setup_t table using setup_tValue method.
        /// </summary>
        public virtual string mail_delivery
        {
            get
            {
                if (!setup_tValue.ContainsKey("mail_delivery"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["mail_delivery"]);
            }
        }

        /// <summary>
        /// Gets value of URL in setup_t table using setup_tValue method.
        /// </summary>
        public virtual string mail_url
        {
            get
            {
                if (!setup_tValue.ContainsKey("mail_url"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["mail_url"]);
            }
        }

        /// <summary>
        /// Gets value of tax (Tax rate) in setup_t table using setup_tValue method.
        /// </summary>
        public virtual int tax
        {
            get
            {
                if (!setup_tValue.ContainsKey("tax"))
                {
                    return 0;
                }
                return Convert.ToInt32(setup_tValue["tax"]);
            }
        }

        /// <summary>
        /// Gets On/OFF value of carriage_tax (Tax rate) in setup_t table using setup_tValue method.
        /// </summary>
        public virtual EnumOnOff enable_carriage_tax
        {
            get
            {
                if (!setup_tValue.ContainsKey("enable_carriage_tax"))
                {
                    return EnumOnOff.Off;
                }
                return (EnumOnOff)Convert.ToInt32(setup_tValue["enable_carriage_tax"]);
            }
        }

        /// <summary>
        /// Gets value of cate1 (1st category) in setup_t table using setup_tValue method.
        /// </summary
        public virtual string cate1
        {
            get
            {
                if (!setup_tValue.ContainsKey("cate1"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["cate1"]);
            }
        }

        /// <summary>
        /// Gets value of cate1_active (show/hide 1st category) in setup_t table using setup_tValue method.
        /// </summary
        public virtual short? cate1_active
        {
            get
            {
                if (!setup_tValue.ContainsKey("cate1_active"))
                {
                    return null;
                }
                return Convert.ToInt16(setup_tValue["cate1_active"]);
            }
        }

        /// <summary>
        /// Gets value of cate2 (2nd category) in setup_t table using setup_tValue method.
        /// </summary
        public virtual string cate2
        {
            get
            {
                if (!setup_tValue.ContainsKey("cate2"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["cate2"]);
            }
        }

        /// <summary>
        /// Gets value of cate2_active (show/hide 2nd category) in setup_t table using setup_tValue method.
        /// </summary
        public virtual short? cate2_active
        {
            get
            {
                if (!setup_tValue.ContainsKey("cate2_active"))
                {
                    return null;
                }
                return Convert.ToInt16(setup_tValue["cate2_active"]);
            }
        }

        /// <summary>
        /// Gets value of cate3 (3rd category) in setup_t table using setup_tValue method.
        /// </summary
        public virtual string cate3
        {
            get
            {
                if (!setup_tValue.ContainsKey("cate3"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["cate3"]);
            }
        }

        /// <summary>
        /// Gets value of cate3_active (show/hide 3rd category) in setup_t table using setup_tValue method.
        /// </summary
        public virtual short? cate3_active
        {
            get
            {
                if (!setup_tValue.ContainsKey("cate3_active"))
                {
                    return null;
                }
                return Convert.ToInt16(setup_tValue["cate3_active"]);
            }
        }

        /// <summary>
        /// Gets value of cate4 (4th category) in setup_t table using setup_tValue method.
        /// </summary
        public virtual string cate4
        {
            get
            {
                if (!setup_tValue.ContainsKey("cate4"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["cate4"]);
            }
        }

        /// <summary>
        /// Gets value of cate4_active (show/hide 4th category) in setup_t table using setup_tValue method.
        /// </summary
        public virtual short? cate4_active
        {
            get
            {
                if (!setup_tValue.ContainsKey("cate4_active"))
                {
                    return null;
                }
                return Convert.ToInt16(setup_tValue["cate4_active"]);
            }
        }

        /// <summary>
        /// Gets value of cate5 (5th category) in setup_t table using setup_tValue method.
        /// </summary
        public virtual string cate5
        {
            get
            {
                if (!setup_tValue.ContainsKey("cate5"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["cate5"]);
            }
        }

        /// <summary>
        /// Gets value of cate5_active (show/hide 5th category) in setup_t table using setup_tValue method.
        /// </summary
        public virtual short? cate5_active
        {
            get
            {
                if (!setup_tValue.ContainsKey("cate5_active"))
                {
                    return null;
                }
                return Convert.ToInt16(setup_tValue["cate5_active"]);
            }
        }

        /// <summary>
        /// Gets value of regi_memo (Checkout screen note) in setup_t table using setup_tValue method.
        /// </summary
        public virtual string regi_memo
        {
            get
            {
                if (!setup_tValue.ContainsKey("regi_memo"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["regi_memo"]);
            }
        }

        /// <summary>
        /// Gets value of pri_memo (Privacy policy memo) in setup_t table using setup_tValue method.
        /// </summary
        public virtual string pri_memo
        {
            get
            {
                if (!setup_tValue.ContainsKey("pri_memo"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["pri_memo"]);
            }
        }

        /// <summary>
        /// Gets value of pri_URL (Privacy policy URL) in setup_t table using setup_tValue method.
        /// </summary
        public virtual string pri_url
        {
            get
            {
                if (!setup_tValue.ContainsKey("pri_url"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["pri_url"]);
            }
        }

        /// <summary>
        /// Gets value of m_pri_url1 (Privacy policy URL for mobile) in setup_t table using setup_tValue method.
        /// </summary
        public virtual string m_pri_url1
        {
            get
            {
                if (!setup_tValue.ContainsKey("m_pri_url1"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["m_pri_url1"]);
            }
        }

        /// <summary>
        /// Gets value of m_pri_url2 (Privacy policy URL for mobile) in setup_t table using setup_tValue method.
        /// </summary
        public virtual string m_pri_url2
        {
            get
            {
                if (!setup_tValue.ContainsKey("m_pri_url2"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["m_pri_url2"]);
            }
        }

        /// <summary>
        /// Gets value of quest_email_to (Contact Email destination) in setup_t table using setup_tValue method.
        /// </summary
        public virtual string quest_email_to
        {
            get
            {
                if (!setup_tValue.ContainsKey("quest_email_to"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["quest_email_to"]);
            }
        }

        /// <summary>
        /// Gets value of error_message (error message) in setup_t table using setup_tValue method.
        /// </summary
        public string error_message
        {
            get
            {
                if (!setup_tValue.ContainsKey("error_message"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["error_message"]);
            }
        }

        /// <summary>
        /// Gets value of error_message (error message for mobile) in setup_t table using setup_tValue method.
        /// </summary
        public string m_error_message
        {
            get
            {
                if (!setup_tValue.ContainsKey("m_error_message"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["m_error_message"]);
            }
        }

        /// <summary>
        /// Gets value shipping_csv_output_min_days in setup_t table using setup_tValue method.
        /// </summary
        public int shipping_csv_output_min_days
        {
            get
            {
                if (!setup_tValue.ContainsKey("shipping_csv_output_min_days"))
                {
                    return 0;
                }
                return Convert.ToInt32(setup_tValue["shipping_csv_output_min_days"]);
            }
        }

        
        /// <summary>
        /// Gets value create_regular_order_days in setup_t table using setup_tValue method.
        /// <!--next_dateから何日前のデータの伝票生成を実行するか-->
        /// </summary
        public int create_regular_order_days
        {
            get
            {
                if (!setup_tValue.ContainsKey("create_regular_order_days"))
                {
                    return 0;
                }
                return Convert.ToInt32(setup_tValue["create_regular_order_days"]);
            }
        }
        

        /// <summary>
        /// Gets value of site_id (id) in setup_t table using setup_tValue method.
        /// </summary
        public virtual int site_id
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("site_id"));
            }
        }

        /// <summary>
        /// DB接続文字列を取得する
        /// </summary>
        /// <returns>Returns ConnectionString using GetConfigFileValue method based on the setup configuration. </returns>
        public virtual string getConnectionStrings()
        {
            return GetCommonConfigFileValue("ConnectionStrings");
        }


        protected string[] cloudConnectionStringsValues;

        /// <summary>
        /// Gets Cloud ConnectionString
        /// </summary>
        /// <returns>Returns value of Cloud ConnectionString</returns>
        public virtual string[] getCloudConnectionStrings()
        {
            if (cloudConnectionStringsValues == null)
            {
                cloudConnectionStringsValues = new string[10];

                string preValue = getConnectionStrings();

                for (int i = 0; i < 10; i++)
                {
                    string val = GetCommonConfigFileValue("ConnectionStrings" + i);
                    if (string.IsNullOrEmpty(val))
                    {
                        val = preValue;
                    }
                    else
                    {
                        preValue = val;
                    }
                    cloudConnectionStringsValues[i] = val;
                }
            }
            return cloudConnectionStringsValues;
        }

        /// <summary>
        /// Gets browser title using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string browser_title
        {
            get
            {
                return GetSiteConfigFileValue("browser_title");
            }
        }

        /// <summary>
        /// Gets Cart URL using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string cart_url
        {
            get
            {
                return GetCommonConfigFileValue("cart_url");
            }
        }

        /// <summary>
        /// Gets value of change URL using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string change_url
        {
            get
            {
                return GetCommonConfigFileValue("change_url");
            }
        }

        /// <summary>
        /// Gets mypage URL using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string mypage_url
        {
            get
            {
                return GetCommonConfigFileValue("mypage_url");
            }
        }

        /// <summary>
        /// Gets lenght of ransu (random characters) using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual int ransu_length
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("ransu_length"));
            }
        }

        /// <summary>
        /// Gets expiration of ransu (random characters) using GetConfigFileValue method based on the setup configuration
        /// </summary>
        public virtual int ransu_expiration
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("ransu_expiration"));
            }
        }

        /// <summary>
        /// Gets ransu characters using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string ransu_chars
        {
            get
            {
                return GetCommonConfigFileValue("ransu_chars");
            }
        }

        /// <summary>
        /// 画像パス
        /// </summary>
        public virtual string gs_img_url
        {
            get
            {
                return GetCommonConfigFileValue("nor_url") + "images/simg/";
            }
        }

        /// <summary>
        /// 伝票バックアップパス
        /// </summary>
        public virtual string d_back_path
        {
            get
            {
                return GetCommonConfigFileValue("d_back_path");
            }
        }

        /// <summary>
        /// サイバーソース・マーチャントID
        /// </summary>
        public virtual string cybersource_merchant
        {
            get
            {
                return GetCommonConfigFileValue("cybersource_merchant");
            }
        }

        /// <summary>
        /// サイバーソース・通信先サーバー
        /// </summary>
        public virtual string cybersource_server
        {
            get
            {
                return GetCommonConfigFileValue("cybersource_server");
            }
        }

        /// <summary>
        /// サイバーソース・通信先サーバー（購入監視用）
        /// </summary>
        public virtual string cybersource_server_monitor
        {
            get
            {
                return GetCommonConfigFileValue("cybersource_server_monitor");
            }
        }

        /// <summary>
        /// PayPal・API接続先
        /// </summary>
        public virtual string paypal_api_endpoint
        {
            get
            {
                return GetCommonConfigFileValue("paypal_api_endpoint");
            }
        }
        /// <summary>
        /// PayPal・遷移先URL
        /// </summary>
        public virtual string paypal_ec_url
        {
            get
            {
                return GetCommonConfigFileValue("paypal_ec_url");
            }
        }
        /// <summary>
        /// PayPal・ユーザ名
        /// </summary>
        public virtual string paypal_api_username
        {
            get
            {
                return GetCommonConfigFileValue("paypal_api_username");
            }
        }

        /// <summary>
        /// PayPal・パスワード
        /// </summary>
        public virtual string paypal_api_password
        {
            get
            {
                return GetCommonConfigFileValue("paypal_api_password");
            }
        }
        /// <summary>
        /// PayPal・シグネチャ
        /// </summary>
        public virtual string paypal_api_signature
        {
            get
            {
                return GetCommonConfigFileValue("paypal_api_signature");
            }
        }
        /// <summary>
        /// PayPal・subject
        /// </summary>
        public virtual string paypal_api_subject
        {
            get
            {
                return GetCommonConfigFileValue("paypal_api_subject");
            }
        }
        /// <summary>
        /// PayPal・バージョン
        /// </summary>
        public virtual string paypal_api_version
        {
            get
            {
                return GetCommonConfigFileValue("paypal_api_version");
            }
        }
        /// <summary>
        /// PayPal・表示商品名
        /// </summary>
        public virtual string paypal_item_name
        {
            get
            {
                return GetCommonConfigFileValue("paypal_item_name");
            }
        }
        /// <summary>
        /// PayPal・決済方法(Sale/Authorization)
        /// </summary>
        public virtual string paypal_paymenttype
        {
            get
            {
                return GetCommonConfigFileValue("paypal_paymenttype");
            }
        }
        /// <summary>
        /// PayPal・API接続先・監視用
        /// </summary>
        public virtual string paypal_api_endpoint_monitor
        {
            get
            {
                return GetCommonConfigFileValue("paypal_api_endpoint_monitor");
            }
        }
        /// <summary>
        /// PayPal・遷移先URL・監視用
        /// </summary>
        public virtual string paypal_ec_url_monitor
        {
            get
            {
                return GetCommonConfigFileValue("paypal_ec_url_monitor");
            }
        }
        /// <summary>
        /// PayPal・ユーザ名・監視用
        /// </summary>
        public virtual string paypal_api_username_monitor
        {
            get
            {
                return GetCommonConfigFileValue("paypal_api_username_monitor");
            }
        }
        /// <summary>
        /// PayPal・パスワード・監視用
        /// </summary>
        public virtual string paypal_api_password_monitor
        {
            get
            {
                return GetCommonConfigFileValue("paypal_api_password_monitor");
            }
        }
        /// <summary>
        /// PayPal・シグネチャ・監視用
        /// </summary>
        public virtual string paypal_api_signature_monitor
        {
            get
            {
                return GetCommonConfigFileValue("paypal_api_signature_monitor");
            }
        }
        /// <summary>
        /// PayPal・subject・監視用
        /// </summary>
        public virtual string paypal_api_subject_monitor
        {
            get
            {
                return GetCommonConfigFileValue("paypal_api_subject_monitor");
            }
        }

        /// <summary>
        /// モバイルサイト有効/無効
        /// </summary>
        public virtual bool enableMobile
        {
            get
            {
                return bool.Parse(GetCommonConfigFileValue("enableMobile"));
            }
        }

        /// <summary>
        /// attribute of Expir that is set to Cookie
        /// </summary>
        /// <summary>
        /// Gets cookie expiration using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public int? GetCookieExpiration(string configKey)
        {
            var strRet = GetCommonConfigFileValue(configKey);
            if (string.IsNullOrEmpty(strRet))
            {
                return null;
            }
            return Convert.ToInt32(strRet);
        }

        /// <summary>
        /// attribute of Domain that is set to Cookie
        /// </summary>
        public virtual string cookieDomain
        {
            get
            {
                return GetCommonConfigFileValue("cookieDomain");
            }
        }

        /// <summary>
        /// attribute of Path that is set to Cookie
        /// </summary>
        public virtual string cookiePath
        {
            get
            {
                return GetCommonConfigFileValue("cookiePath");
            }
        }

        /// <summary>
        /// Gets Ignore group code using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string[] IgnoreGcode
        {
            get
            {
                return GetCommonConfigFileValue("IgnoreGcode").Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        /// <summary>
        /// Get days of requested delivery date for view using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual int MaxSenddayCount
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("MaxSenddayCount"));
            }
        }

        /// <summary>
        /// Get number of items on a merchandise list page using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual int MerchandiseListItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("MerchandiseListItemNumberOnPage"));
            }
        }

        public virtual int AnnouncementListItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("AnnouncementListItemNumberOnPage"));
            }
        }

        public virtual int AnnouncementListItemNumberOnOneLine
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("AnnouncementListItemNumberOnOneLine"));
            }
        }

        /// <summary>
        /// Gets numbers of Merchandise Item Number per line display using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public long MerchandiseListItemNumberOnOneLine
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("MerchandiseListItemNumberOnOneLine"));
            }
        }

        /// <summary>
        /// Gets GMO Payment settings url connection using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        /// <summary>
        /// Get number of item on a Cts Information list page 
        /// </summary>
        public int CtsInformationListItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("CtsInformationListItemNumberOnPage"));
            }
        }

        /// <summary>
        /// Get number of item on a Cts Direction Management list page 
        /// </summary>
        public int CtsInquiryCaseListItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("CtsInquiryCaseListItemNumberOnPage"));
            }
        }

        /// <summary>
        /// Get number of item on a Cts Inquiry Case List page 
        /// </summary>
        public int CtsDirectionsmanagementListItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("CtsDirectionsmanagementListItemNumberOnPage"));
            }
        }

        /// <summary>
        /// Get number of item on a Cts Direction CardErr List page 
        /// </summary>
        public int CtsDirectionscarderrListItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("CtsDirectionscarderrListItemNumberOnPage"));
            }
        }

        /// <summary>
        /// Get number of item on a Cts Search list page 
        /// </summary>
        public int CtsSearchListItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("CtsSearchListItemNumberOnPage"));
            }
        }

        public int CtsRepContactLogsItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("CtsRepContactLogsItemNumberOnPage"));
            }
        }

        public virtual string gmo_connect_url
        {
            get
            {
                return GetCommonConfigFileValue("gmo_connect_url");
            }
        }

        /// <summary>
        /// Gets GMO Payment settings shop id using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string gmo_shop_id
        {
            get
            {
                return GetCommonConfigFileValue("gmo_shop_id");
            }
        }

        /// <summary>
        /// Gets GMO Payment settings password shop using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string gmo_shop_password
        {
            get
            {
                return GetCommonConfigFileValue("gmo_shop_password");
            }
        }

        /// <summary>
        /// Gets GMO Payment settings site id using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string gmo_site_id
        {
            get
            {
                return GetCommonConfigFileValue("gmo_site_id");
            }
        }

        /// <summary>
        /// Gets GMO Payment settings site password using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string gmo_site_password
        {
            get
            {
                return GetCommonConfigFileValue("gmo_site_password");
            }
        }

        /// <summary>
        /// Gets GMO connection url monitor using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string gmo_connect_url_monitor
        {
            get
            {
                return GetCommonConfigFileValue("gmo_connect_url_monitor");
            }
        }

        /// <summary>
        /// Gets GMO shop monitor using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string gmo_shop_id_monitor
        {
            get
            {
                return GetCommonConfigFileValue("gmo_shop_id_monitor");
            }
        }

        /// <summary>
        /// Gets GMO shop password monitor using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string gmo_shop_password_monitor
        {
            get
            {
                return GetCommonConfigFileValue("gmo_shop_password_monitor");
            }
        }

        /// <summary>
        /// Gets GMO site id monitor using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string gmo_site_id_monitor
        {
            get
            {
                return GetCommonConfigFileValue("gmo_site_id_monitor");
            }
        }

        /// <summary>
        /// Gets GMO site password monitor using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string gmo_site_password_monitor
        {
            get
            {
                return GetCommonConfigFileValue("gmo_site_password_monitor");
            }
        }

        /// <summary>
        /// Get total of bill that could be compensated by GMO.
        /// </summary>
        public int gmo_compensable_total
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("gmo_compensable_total"));
            }
        }

        /// <summary>
        /// ERS_CMS_News
        /// upload image and file
        /// </summary>
        public virtual string news_image_path
        {
            get
            {
                var directory = GetCommonConfigFileValue("news_image_path");
                if (!directory.EndsWith("\\") && !directory.EndsWith("/"))
                {
                    directory += "\\";
                }

                return this.root_path + directory;
            }
        }
        public virtual string multiple_news_image_path(int site_id)
        {
            {
                var directory = GetCommonConfigFileValue("news_" + site_id.ToString() + "_image_path");
                if (!directory.EndsWith("\\") && !directory.EndsWith("/"))
                {
                    directory += "\\";
                }

                return this.root_path + directory;
            }
        }


        public virtual string news_image_temp_path
        {
            get
            {
                var directory = GetCommonConfigFileValue("news_image_temp_path");
                if (!directory.EndsWith("\\") && !directory.EndsWith("/"))
                {
                    directory += "\\";
                }

                return this.root_path + directory;
            }
        }

        public virtual int cms_news_thumbnail_image_width
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("cms_news_thumbnail_image_width"));
            }
        }

        /// <summary>
        /// Gets image directory using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual string image_directory
        {
            get
            {
                var directory = GetCommonConfigFileValue("image_directory");
                if (!directory.EndsWith("\\") && !directory.EndsWith("/"))
                {
                    directory += "\\";
                }

                return directory;
            }
        }

        /// <summary>
        /// Gets image temporary directory using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public string image_temp_directory
        {
            get
            {
                var directory = GetCommonConfigFileValue("image_temp_directory");
                if (!directory.EndsWith("\\") && !directory.EndsWith("/"))
                {
                    directory += "\\";
                }

                return directory;
            }
        }
        /// <summary>
        /// Gets the default image file name on the setup configuration.
        /// </summary>
        public string default_name_image
        {
            get
            {
                return GetCommonConfigFileValue("default_name_image");
            }
        }

        /// <summary>
        /// Gets Stock management type using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual EnumStockManagementType StockManagementType
        {
            get
            {
                return (EnumStockManagementType)Enum.Parse(typeof(EnumStockManagementType), GetCommonConfigFileValue("StockManagementType"));
            }
        }

        /// <summary>
        /// Gets Maximum number of credit card registration using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual int entryCardInfoCount
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("entryCardInfoCount"));
            }
        }

        #region "年齢関連"
        /// <summary>
        ///  プルダウン表示時最小年齢（より大）
        /// </summary>
        public virtual int min_user_age
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("min_user_age"));
            }
        }
        /// <summary>
        ///  プルダウン表示時最大年齢（より大）
        /// </summary>
        public virtual int max_user_age
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("max_user_age"));
            }
        }
        /// <summary>
        ///  プルダウンデフォルト設定年齢(フォーカス時)
        /// </summary>
        public virtual int tgt_user_born
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("tgt_user_born"));
            }
        }

        #endregion

        /// <summary>
        /// email保存クッキー期限（分）
        /// </summary>
        public virtual int? cookieTimer
        {
            get
            {
                if (GetCommonConfigFileValue("cookieTimer").HasValue())
                {
                    return Convert.ToInt16(GetCommonConfigFileValue("cookieTimer"));
                }

                return null;
            }
        }

        /// <summary>
        /// 初回購入監視用会員姓
        /// </summary>
        public string monitorFirstLname
        {
            get
            {
                return GetCommonConfigFileValue("monitorFirstLname");
            }
        }

        /// <summary>
        /// 初回購入監視用会員名
        /// </summary>
        public string monitorFirstFname
        {
            get
            {
                return GetCommonConfigFileValue("monitorFirstFname");
            }
        }

        /// <summary>
        /// 2回目購入監視用会員姓
        /// </summary>
        public string monitorSecondLname
        {
            get
            {
                return GetCommonConfigFileValue("monitorSecondLname");
            }
        }

        /// <summary>
        /// 2回目購入監視用会員名
        /// </summary>
        public string monitorSecondFname
        {
            get
            {
                return GetCommonConfigFileValue("monitorSecondFname");
            }
        }

        #region "SMTP"
        public string smtpHostName
        {
            get
            {
                return GetCommonConfigFileValue("smtpHostName");
            }
        }

        public int smtpPort
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("smtpPort"));
            }
        }

        public string smtpOperationLogPath
        {
            get
            {
                var value = GetCommonConfigFileValue("smtpOperationLogPath");
                if (!value.EndsWith("\\"))
                {
                    value += "\\";
                }
                return value;
            }
        }

        public string smtpErrorLogPath
        {
            get
            {
                var value = GetCommonConfigFileValue("smtpErrorLogPath");
                if (!value.EndsWith("\\"))
                {
                    value += "\\";
                }
                return value;
            }
        }

        public int smtpRetryTermMinutes
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("smtpRetryTermMinutes"));
            }
        }

        public int smtpRetryIntervalMinutes
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("smtpRetryIntervalMinutes"));
            }
        }

        public string smtpRetryTextPath
        {
            get
            {
                return GetCommonConfigFileValue("smtpRetryTextPath");
            }
        }
        #endregion

        /// <summary>
        /// DOMAIN for mobile
        /// </summary>
        public virtual string[] DomainMobile
        {
            get
            {
                if (!string.IsNullOrEmpty(GetCommonConfigFileValue("domain_mobile")))
                {
                    if (_DomainMobile == null)
                    {
                        _DomainMobile = GetCommonConfigFileValue("domain_mobile").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (var i = 0; i < _DomainMobile.Length; i++)
                        {
                            _DomainMobile[i] = _DomainMobile[i].ToLower();
                        }
                    }
                }
                return _DomainMobile;
            }
        }

        protected static string[] _DomainMobile
        {
            get
            {
                return (string[])ErsCommonContext.GetPooledObject("_DomainMobile");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_DomainMobile", value);
            }
        }

        /// <summary>
        /// URL for open rate
        /// </summary>
        public virtual string open_rate_image_url
        {
            get
            {
                return GetCommonConfigFileValue("open_rate_image_url");
            }
        }

        /// <summary>
        /// URL for click count
        /// </summary>
        public virtual string click_count_url
        {
            get
            {
                return GetCommonConfigFileValue("click_count_url");
            }
        }

        public virtual string gmo_log_directory
        {
            get
            {
                return GetCommonConfigFileValue("gmo_log_directory");
            }
        }

        public string paypal_log_directory
        {
            get
            {
                return GetCommonConfigFileValue("paypal_log_directory");
            }
        }

        public string cts_highlightcolor
        {
            get
            {
                return GetSiteConfigFileValue("cts_highlightcolor");
            }
        }

        public string BasicAuthUserName
        {
            get
            {
                return GetCommonConfigFileValue("BasicAuthUserName");
            }
        }

        public string BasicAuthPassword
        {
            get
            {
                return GetCommonConfigFileValue("BasicAuthPassword");
            }
        }

        /// <summary>
        ///  ランキング表示件数
        /// </summary>
        public virtual int FrontRankingDispCount
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("FrontRankingDispCount"));
            }
        }
        /// <summary>
        /// 検索キーワード数
        /// </summary>
        public virtual long Keyword_num
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("Keyword_num"));
            }
        }

        /// <summary>
        /// setup_tValueメソッドを使用して、setup_tテーブルのwh_owner_description （発注元）の値を取得します。
        /// </summary
        public virtual string wh_owner_description
        {
            get
            {
                if (!setup_tValue.ContainsKey("wh_owner_description"))
                {
                    return null;
                }
                return Convert.ToString(setup_tValue["wh_owner_description"]);
            }
        }

        #region ＠メール監視
        /// <summary>
        /// 監視用＠メールプロセスID
        /// </summary>
        public int? monitorMassSendProcessID
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("monitorMassSendProcessID"));
            }
        }

        public string monitorMassSendMailAddress
        {
            get
            {
                return GetCommonConfigFileValue("monitorMassSendMailAddress");
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        /// </summary>
        public virtual string monitorMassSendMailServer
        {
            get
            {
                return GetCommonConfigFileValue("monitorMassSendMailServer");
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        /// </summary>
        public virtual string monitorMassSendMailServerAccount
        {
            get
            {
                return GetCommonConfigFileValue("monitorMassSendMailServerAccount");
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        /// </summary>
        public virtual string monitorMassSendMailServerPass
        {
            get
            {
                return GetCommonConfigFileValue("monitorMassSendMailServerPass");
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        /// </summary>
        public virtual int monitorMassSendMailServerPort
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("monitorMassSendMailServerPort"));
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        ///  SSL接続か否か
        /// </summary>
        public virtual Boolean monitorMassSendMailServerSslConnection
        {
            get
            {
                return Convert.ToBoolean(GetCommonConfigFileValue("monitorMassSendMailServerSslConnection"));
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        ///  1=POP or 2=IMAP
        /// </summary>
        public virtual EnumMailerType monitorMassSendMailServerReceptionType
        {
            get
            {
                return (EnumMailerType)Convert.ToInt32(GetCommonConfigFileValue("monitorMassSendMailServerReceptionType"));
            }
        }

        /// <summary>
        ///  コンタクトメール受信　メールサーバー情報
        ///  IMAP用　既読メールのフォルダ名 POP時は記入は無視
        /// </summary>
        public virtual string monitorMassSendReadMailDirName
        {
            get
            {
                return GetCommonConfigFileValue("monitorMassSendReadMailDirName");
            }
        }
        #endregion

        #region "お問い合わせ先"
        public string ContactInformationName
        {
            get
            {
                return GetCommonConfigFileValue("ContactInformationName");
            }
        }

        public string ContactInformationTel
        {
            get
            {
                return GetCommonConfigFileValue("ContactInformationTel");
            }
        }

        public string ContactInformationTime
        {
            get
            {
                return GetCommonConfigFileValue("ContactInformationTime");
            }
        }
        #endregion

        #region update table

        public string uploadedFilePath
        {
            get
            {
                return GetCommonConfigFileValue("uploadedFilePath");
            }
        }

        public string uploadedFileErrorPath
        {
            get
            {
                return GetCommonConfigFileValue("uploadedFileErrorPath");
            }
        }

        public string uploadedFileBackupPath
        {
            get
            {
                return GetCommonConfigFileValue("uploadedFileBackupPath");
            }
        }

        public string updateSpecifiedColumnAlertMailTo
        {
            get
            {
                return GetCommonConfigFileValue("updateSpecifiedColumnAlertMailTo");
            }
        }

        #endregion

        #region download table

        public string downloadFilePath
        {
            get
            {
                return GetCommonConfigFileValue("downloadFilePath");
            }
        }

        public string downloadFileResultPath
        {
            get
            {
                return GetCommonConfigFileValue("downloadFileResultPath");
            }
        }

        #endregion

        #region Insert table

        public string insertTableFilePath
        {
            get
            {
                return GetCommonConfigFileValue("insertTableFilePath");
            }
        }

        public string insertTableFileErrorPath
        {
            get
            {
                return GetCommonConfigFileValue("insertTableFileErrorPath");
            }
        }

        public string insertTableFileBackupPath
        {
            get
            {
                return GetCommonConfigFileValue("insertTableFileBackupPath");
            }
        }

        public string insertTableSpecifiedColumnAlertMailTo
        {
            get
            {
                return GetCommonConfigFileValue("insertTableSpecifiedColumnAlertMailTo");
            }
        }

        #endregion

        #region delete table

        public string deleteFilePath
        {
            get
            {
                return GetCommonConfigFileValue("deleteFilePath");
            }
        }

        public string deleteFileErrorPath
        {
            get
            {
                return GetCommonConfigFileValue("deleteFileErrorPath");
            }
        }

        public string deleteFileBackupPath
        {
            get
            {
                return GetCommonConfigFileValue("deleteFileBackupPath");
            }
        }

        public string deleteSpecifiedColumnAlertMailTo
        {
            get
            {
                return GetCommonConfigFileValue("deleteSpecifiedColumnAlertMailTo");
            }
        }

        #endregion

        /// <summary>
        /// リダイレクト先のサイト。RedirectSiteAttributeにて使用
        /// </summary>
        public IEnumerable<EnumSiteType> redirect_site_types
        {
            get
            {
                var value = GetSiteConfigFileValue("redirect_site_types");
                if (!value.HasValue())
                {
                    yield break;
                }

                var arrValue = value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var strSite_type in arrValue)
                {
                    EnumSiteType site_type;
                    if (Enum.TryParse<EnumSiteType>(strSite_type, out site_type))
                    {
                        yield return site_type;
                    }
                }
            }
        }

        public virtual int max_purchase_count_mail_per_item
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("max_purchase_count_mail_per_item"));
            }
        }

        public virtual int common_max_purchase_count
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("common_max_purchase_count"));
            }
        }

        public string conveniencePaidGmoErrMailTo
        {
            get
            {
                return GetSiteConfigFileValue("conveniencePaidGmoErrMailTo");
            }
        }

        public string conveniencePaidGmoErrMailCc
        {
            get
            {
                return GetSiteConfigFileValue("conveniencePaidGmoErrMailCc");
            }
        }

        public string conveniencePaidGmoErrMailBcc
        {
            get
            {
                return GetSiteConfigFileValue("conveniencePaidGmoErrMailBcc");
            }
        }

        public string conveniencePaidGmoErrMailFrom
        {
            get
            {
                return GetSiteConfigFileValue("conveniencePaidGmoErrMailFrom");
            }
        }

        public string conveniencePaidGmoErrMailTitle
        {
            get
            {
                return GetSiteConfigFileValue("conveniencePaidGmoErrMailTitle");
            }
        }

        /// <summary>
        /// Get number of item on a page 
        /// </summary>
        public int OrderItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("OrderItemNumberOnPage"));
            }
        }

        /// <summary>
        /// ERS_CMS_LP
        /// upload image and file
        /// </summary>
        public virtual string lp_image_path
        {
            get
            {
                var directory = GetCommonConfigFileValue("lp_image_path");
                if (!directory.EndsWith("\\") && !directory.EndsWith("/"))
                {
                    directory += "\\";
                }

                return this.root_path + directory;
            }
        }


        /// <summary>
        /// 顧客一元化
        /// Customer unification
        /// </summary>
        public bool member_centralization
        {
            get
            {
                var config_value = GetCommonConfigFileValue("member_centralization");
                if (config_value == null)
                {
                    return false;
                }
                else
                {
                    return bool.Parse(config_value);
                }
            }
        }

        /// <summary>
        /// 会員ランク一元化
        /// Member rank unification
        /// </summary>
        public bool member_rank_centralization
        {
            get
            {
                var config_value = GetCommonConfigFileValue("member_rank_centralization");
                if (config_value == null)
                {
                    return false;
                }
                else
                {
                    return bool.Parse(config_value);
                }
            }
        }

        public virtual string multiple_image_directory(int site_id)
        {
            
            {
                var directory = GetCommonConfigFileValue("image_" + site_id.ToString() + "_directory");
                if (!directory.EndsWith("\\") && !directory.EndsWith("/"))
                {
                    directory += "\\";
                }

                return directory;
            }
        }

        public virtual int summary_commandtimeout
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("summary_commandtimeout"));
            }
        }

        public virtual int merchandise_disp_keyword_limit
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("MerchandiseDispKeywordLimit"));
            }
        }
        
        public virtual string hr_emp_no
        {
            get
            {
                return GetSiteConfigFileValue("hr_emp_no");
            }
        }

        public virtual string def_time_start
        {
            get
            {
                return GetSiteConfigFileValue("def_time_start");
            }
        }

        public virtual string def_time_end
        {
            get
            {
                return GetSiteConfigFileValue("def_time_end");
            }
        }

        public virtual int? max_vl_sl
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("max_vl_sl"));
            }
        }
    }
}