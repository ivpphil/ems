﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.IO;
using System.Web;

namespace jp.co.ivp.ers.util
{
    public class SaveUploadedFileHelper
    {

        protected string randomKey;
        protected string random;

        public SaveUploadedFileHelper()
        {
            //get random string used to specify temporary file name
            randomKey = ErsContext.sessionState.Get("admin_ransu");
        }

        /// <summary>
        /// get temporary file name used in confirmation page
        /// </summary>
        /// <param name="postedFile"></param>
        /// <returns></returns>
        public virtual string GetTempFileName(HttpPostedFileBase postedFile)
        {
            if (postedFile == null)
                return "";

            byte[] b = new byte[4];
            new System.Security.Cryptography.RNGCryptoServiceProvider().GetBytes(b);
            Random r = new Random(BitConverter.ToInt32(b, 0));
            string s = null;
            string str = "0123456789";
            str += "abcdefghijklmnopqrstuvwxyz";
            str += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            for (int i = 0; i < 32; i++)
            {
                s += str.Substring(r.Next(0, str.Length - 1), 1);
            }
            random = s;
            return randomKey + random + Path.GetExtension(postedFile.FileName);
        }

        /// <summary>
        /// save temporary file used in confirmation page
        /// </summary>
        /// <param name="postedFile"></param>
        /// <param name="savedDirectory"></param>
        public virtual void SaveTempFile(HttpPostedFileBase postedFile, string outputDirectory, string temp_file_name)
        {
            //create target directory if it doesn't exist
            if (!Directory.Exists(outputDirectory))
            {
                Directory.CreateDirectory(outputDirectory);
            }

            postedFile.SaveAs(outputDirectory + temp_file_name);
        }


        public virtual void SavePdfFile(string tempDirectory, string tempFileName, string outputDirectory, string outputFileName)
        {
            //enable override
            File.Copy(tempDirectory + tempFileName, outputDirectory + outputFileName, true);
        }

        public virtual void SaveResizedImage(string tempDirectory, string tempFileName, string outputDirectory, string outputFileName, int width)
        {
            var resizeImage = ErsFactory.ersUtilityFactory.getErsResizeImage();

            resizeImage.InitSize(width);
            resizeImage.CreateResizedImage(tempDirectory + tempFileName, outputDirectory, outputFileName);
        }

        public virtual void DeleteOldImageFile( string ImageFile)
        {
            File.Delete(ImageFile);
        }

        public virtual void DeleteFile(string outputPath, string filename)
        {
            File.Delete(outputPath + filename);
        }

        public virtual void DeleteTempFile(string outputPath)
        {
            Directory.CreateDirectory(outputPath);

            foreach (var path in Directory.GetFiles(outputPath, this.randomKey + "*"))
            {
                try
                {
                    File.Delete(path);
                }
                catch(IOException)
                {
                    continue;
                }
            }
        }
    }
}
