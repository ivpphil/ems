﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using System.IO;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.util
{
    public class ErsTsvCreater
        : ErsCsvCreater
    {
        /// <summary>
        /// フィールドタイプ [Field type]
        /// </summary>
        protected override Type fieldAttributeType
        {
            get
            {
                return typeof(TsvFieldAttribute);
            }
        }


        /// <summary>
        /// CSVヘッダーを出力する。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="writer"></param>
        /// <param name="formatter"></param>
        public override void WriteCsvHeader(IEnumerable<string> columns, StreamWriter writer, Func<string, string> formatter)
        {
            var header = string.Empty;
            foreach (var column in columns)
            {
                header += "\t" + formatter(column);
            }
            if (!string.IsNullOrEmpty(header))
            {
                writer.WriteLine(header.Substring(1));
            }
        }

        /// <summary>
        /// CSVボディを出力する。
		/// <para>Output a CSV body</para>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="writer"></param>
        public override void WriteBody<T>(T model, StreamWriter writer)
        {
            var body = this.CreateBody<T>(model);

            writer.WriteLine(body);
        }

        public string CreateBody<T>(T model)
            where T : ErsModelBase
        {
            var dic = model.GetPropertiesAsDictionary(this.fieldAttributeType);

            var type = model.GetType();

            var body = string.Empty;
            foreach (var key in dic.Keys)
            {
                body += "\t";

                var val = string.Empty;

                if (dic[key] == null)
                {
                    val = string.Empty;
                }
                else if (dic[key] is Array)
                {
                    foreach (object o in (Array)dic[key])
                    {
                        val += "," + o.ToString();
                    }

                    if (!string.IsNullOrEmpty(val))
                        val = val.Substring(1);
                }
                else
                {
                    val = Convert.ToString(dic[key]);
                }

                //固定長を取得
                var fixedLength = ((TsvFieldAttribute)type.GetProperty(key).GetCustomAttributes(this.fieldAttributeType, false)[0]).fixedLength;
                if (fixedLength != TsvFieldAttribute.NON_FIXED)
                {
                    val = GetFixedString(val, fixedLength);
                }

                if (val.HasValue() && val.Contains(Environment.NewLine))
                {
                    body += "\"" + val + "\"";
                }
                else
                {
                    body += val;
                }
            }

            return body.Substring(1);
        }

        public string GetFixedString(string sorceString, int fixedLength)
        {
            var resultString = sorceString.PadRight(fixedLength);
            for (var i = 1; ErsCommon.GetByteCount(resultString) > fixedLength; i++)
            {
                resultString = VBStrings.Left(resultString, resultString.Length - 1);
            }
            return resultString;
        }
    }
}
