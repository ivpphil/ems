﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.util
{
    public class ErsSetupCriteria
        : Criteria
    {
        public int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("setup_t.id", value, Operation.EQUAL));
            }
        }

        public int? site_id_for_admin
        {
            set
            {
                this.Add(Criteria.GetCriterion("setup_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("setup_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("setup_t.site_id",(int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

        public void SetOrderBySiteId(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("setup_t.site_id", orderBy);
        }
    }
}
