﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util.mobile;
using jp.co.ivp.ers.util.mail;

namespace jp.co.ivp.ers.util
{
    /// <summary>
    /// Class for ErsUtilityFactory.
    /// Provide methods that are related in setup.
    /// </summary>
    public class ErsUtilityFactory
    {

        /// <summary>
        /// Gets and sets pooled setup
        /// </summary>
        protected static Setup _setup
        {
            get
            {
                return (Setup)ErsCommonContext.GetPooledObject("_setup");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_setup", value);
            }
        }

        /// <summary>
        /// 設定情報を格納したクラスを取得する（参照用）
        /// </summary>
        /// <returns>Returns instance of Setup</returns>
        public virtual Setup getSetup()
        {
            if (_setup == null)
                _setup = new Setup();

            return _setup;
        }

        /// <summary>
        /// 設定情報を格納するクラスを取得する（更新用）
        /// </summary>
        /// <returns>Returns instance of ErsSetup</returns>
        public virtual ErsSetup getErsSetup()
        {
            return new ErsSetup();
        }

        /// <summary>
        /// 暗号化オブジェクトを取得する。
        /// </summary>
        /// <returns>Returns instance of ErsEncryption</returns>
        public virtual ErsEncryption getErsEncryption()
        {
            return new ErsEncryption();
        }

        /// <summary>
        /// ErsSetupクラス用リポジトリ
        /// </summary>
        /// <returns>Returns instance of ErsSetupRepository</returns>
        public virtual ErsSetupRepository GetErsSetupRepository()
        {
            return new ErsSetupRepository();
        }

        /// <summary>
        /// ErsSetupクラス用リポジトリ
        /// </summary>
        /// <returns>Returns instance of ErsSetupRepository</returns>
        public virtual ErsSetupCriteria GetErsSetupCriteria()
        {
            return new ErsSetupCriteria();
        }

        /// <summary>
        /// Get records from setup_t table using OverwriteWithParameter function of ErsSetup
        /// </summary>
        /// <param name="parameters">values of setup saved in dictionary</param>
        /// <returns>return values from ErsSetup</returns>
        internal virtual ErsSetup getErsSetupWithParameter(Dictionary<string, object> parameters)
        {
            var setup = this.getErsSetup();
            setup.OverwriteWithParameter(parameters);
            return setup;
        }

        public ErsSetup GetErsSetupOfSite()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var repository = ErsFactory.ersUtilityFactory.GetErsSetupRepository();
            var criteria = ErsFactory.ersUtilityFactory.GetErsSetupCriteria();
            criteria.site_id = setup.site_id;
            criteria.SetOrderBySiteId(db.Criteria.OrderBy.ORDER_BY_DESC);
            var listSetup = repository.Find(criteria);
            if (listSetup.Count <= 0)
            {
                return null;
            }
            return listSetup.First();
        }

        public ErsSetup GetErsSetupOfSiteForAdmin(int? site_id)
        {
            var repository = ErsFactory.ersUtilityFactory.GetErsSetupRepository();
            var criteria = ErsFactory.ersUtilityFactory.GetErsSetupCriteria();
            criteria.site_id = site_id;
            criteria.SetOrderBySiteId(db.Criteria.OrderBy.ORDER_BY_DESC);
            var listSetup = repository.Find(criteria);
            return listSetup.First();
        }

        public ErsSetup GetErsSetupWithId(int? id)
        {
            var repository = ErsFactory.ersUtilityFactory.GetErsSetupRepository();
            var criteria = ErsFactory.ersUtilityFactory.GetErsSetupCriteria();
            criteria.id = id;
            var listSetup = repository.Find(criteria);
            if (listSetup.Count != 1)
            {
                return null;
            }
            return listSetup.First();
        }

        public virtual ErsCsvCreater GetErsCsvCreater()
        {
            return new ErsCsvCreater();
        }

        public virtual ErsTsvCreater GetErsTsvCreater()
        {
            return new ErsTsvCreater();
        }

        public virtual ErsMemoryCsvCreater GetErsMemoryCsvCreater()
        {
            return new ErsMemoryCsvCreater();
        }

        public virtual FTPClient GetFTPClient()
        {
            return new FTPClient();
        }

        /// <summary>
        /// Obtain object for resizing image
        /// </summary>
        /// <returns>Returns instance of ErsResizeImage</returns>
        public virtual ErsResizeImage getErsResizeImage()
        {
            return new ErsResizeImage();
        }

        /// <summary>
        /// upload image
        /// </summary>
        /// <returns></returns>
        public virtual SaveUploadedFileHelper GetSaveUploadedFileHelper()
        {
            return new SaveUploadedFileHelper();
        }

        public virtual ErsMobileCommon getErsMobileCommon()
        {
            return new ErsMobileCommon();
        }

        /// <summary>
        /// Tar/Zipファイル操作クラス
        /// </summary>
        /// <returns></returns>
        public virtual TarZipFileManager GetTarZipFileManager()
        {
            return new TarZipFileManager();
        }

        public ErsImailer GetErsImailer(EnumMailerType type)
        {

            if (type == EnumMailerType.IMAP)
            {
                return this.GetErsMailerIMAP();
            }
            else
            {
                return this.GetErsMailerPOP();
            }
        }

        public virtual ErsMailerIMAP GetErsMailerIMAP()
        {
            return new ErsMailerIMAP();
        }

        public virtual ErsMailerPOP GetErsMailerPOP()
        {
            return new ErsMailerPOP();
        }

        public virtual ErsCsvContainer<T> GetErsCsvContainer<T>()
            where T : ErsBindableModel
        {
            return new ErsCsvContainer<T>();
        }

        public virtual ErsCsvSchemaContainer<TContainer> GetErsCsvSchemaContainer<TContainer>()
            where TContainer : ErsBindableModel
        {
            return new ErsCsvSchemaContainer<TContainer>();
        }
    }
}
