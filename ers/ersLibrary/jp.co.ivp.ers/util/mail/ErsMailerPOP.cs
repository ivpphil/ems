﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using TKMP.Net;
using TKMP.Reader;
using jp.co.ivp.ers.util;
using System.Collections;

namespace jp.co.ivp.ers.util.mail
{
    public class ErsMailerPOP : ErsImailer 
    {
        TKMP.Net.IPopLogon logonPop;
        TKMP.Net.PopClient pop;

        private string communicationLog = string.Empty;
        private string mailSetErrLog = string.Empty;

        protected override Boolean OpenConnectionActually(string server, string mailServerAccount, string mailServerPass, int mailServerPort, bool mailServerSslConnection)
        {
            logonPop = new TKMP.Net.BasicPopLogon(mailServerAccount, mailServerPass);
            pop = new TKMP.Net.PopClient(logonPop, server, mailServerPort);

            pop.KeepAlive = true;

            if (mailServerSslConnection)
            {
                pop.AuthenticationProtocol = AuthenticationProtocols.SSL;
            }
            else
            {
                pop.AuthenticationProtocol = AuthenticationProtocols.None;
            }

            //受信イベントを登録
            pop.MessageReceive += new TKMP.Net.MessageReceiveHandler(ServerMessageReceive);

            //送信イベントを登録
            pop.MessageSend += new TKMP.Net.MessageSendHandler(ServerMessageSend);

            return pop.Connect();
        }

        /// <summary>
        /// クローズ
        /// </summary>
        protected override void CloseConnection()
        {
            pop.Close();
        }

        /// <summary>
        /// サーバーより指定されたメールBOXのメールをErsMailDetail[]
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public override List<IDictionary<string, object>> GetMailList()
        {
            var mailList = pop.GetMailList ();

            var ret = new List<IDictionary<string, object>>();

            foreach (var mail in mailList)
            {
                try
                {
                    var ersMailDetail = this.GetMailReaderInfo(mail);
                }
                catch (Exception ex)
                {
                    mailSetErrLog += "GetMailList()エラー, uid= " + mail.UID + System.Environment.NewLine
                        + ex.ToString() + System.Environment.NewLine + System.Environment.NewLine;
                }
            }
            return ret;
        }

        /// <summary>
        /// メール削除
        /// </summary>
        /// <param name="mail"></param>
        public override void MailMoveDelete(string ReadMailDirName, IEnumerable<string> arr)
        {
            List<string> errLogArr = new List<string>();

            var mailList = pop.GetMailList();
            if (mailList != null)
            {
                return;
            }

            var arrCnt = mailList.Count();

            for (int cnt = 0; cnt < arrCnt; cnt++)
            {
                foreach (string UID in arr)
                {
                    try
                    {
                        if (UID.Equals (pop.MailDatas[cnt].UID ))
                        {
                            pop.MailDatas[cnt].Delete();
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        string errLog = string.Empty;
                        errLog += "mail-move-err :uid=" + UID + Environment.NewLine;
                        errLog += ex.ToString() + Environment.NewLine; ;
                        errLogArr.Add(errLog);
                        break;
                    }
                }
            }

            //1件でもエラーがあれば
            if (errLogArr.Count > 0)
            {
                throw new Exception(string.Join(",", errLogArr));
            }
        }

        private IDictionary<string, object> GetMailReaderInfo(IMailData mail)
        {
            //メール内容取得
            mail.ReadHeader();
            mail.ReadBody();
            var read_date = mail.DataStream;
            var mailContnts = new TKMP.Reader.MailReader(read_date, false);

            //メール情報をハッシュへ
            return this.SetMailReaderInfo(mailContnts, mail.UID);
        }

        /// <summary>
        /// メール情報をハッシュへ
        /// </summary>
        /// <param name="mailContnts"></param>
        /// <returns></returns>
        private IDictionary<string, object> SetMailReaderInfo(TKMP.Reader.MailReader mailReader, string uid)
        {
            ErsIgnoreCaseDictionary mailDic = new ErsIgnoreCaseDictionary();

            //ヘッダー配列取得
            var mailHeaderArr = mailReader.HeaderCollection.GetEnumerator();
            mailHeaderArr.MoveNext();

            for (int cnt = 0; cnt < mailReader.HeaderCollection.Count; cnt++)
            {
                string key = ((TKMP.Reader.Header.HeaderString)(mailHeaderArr.Current)).Name;
                string value = ((TKMP.Reader.Header.HeaderString)(mailHeaderArr.Current)).Data;

                mailDic.Add(key, value);
                mailHeaderArr.MoveNext();
            }

            mailDic.Add("uid", uid);
            mailDic.Add("body", mailReader.MainText);

            return mailDic;
        }

        /// <summary>
        /// ログ内容取得
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override string GetMailSetErrLog()
        {
            return this.mailSetErrLog;
        }

        /// <summary>
        /// サーバーへメッセージを受信したときのイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ServerMessageSend(object sender, TKMP.Net.MessageArgs e)
        {
            communicationLog += DateTime.Now + ":send," + (e.Message);
        }

        /// <summary>
        /// サーバーへメッセージを送信したときのイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void ServerMessageReceive(object sender, TKMP.Net.MessageArgs e)
        {
            communicationLog += DateTime.Now + ":receive," + (e.Message);
        }

        /// <summary>
        /// ログ内容取得
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected override string GetCommunicationLog()
        {
            return this.communicationLog;
        }
    }
}
