﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using TKMP.Net;
using TKMP.Reader;
using jp.co.ivp.ers.util;
using Smdn.Net.Imap4;
using Smdn.Net.Imap4.Protocol.Client;
using Smdn.Net.Imap4.Client;
using System.Collections;
using jp.co.ivp.ers.batch;


namespace jp.co.ivp.ers.util.mail
{
    public class ErsMailerIMAP : ErsImailer 
    {
        TKMP.Net.IImapLogon logonImap;
        TKMP.Net.ImapClient imap;

        private string communicationLog = string.Empty;
        private string mailSetErrLog = string.Empty;
        private string mailBoxName = "INBOX";

        /// <summary>
        /// 接続
        /// </summary>
        protected override Boolean OpenConnectionActually(string server, string mailServerAccount, string mailServerPass, int mailServerPort, bool mailServerSslConnection)
        {
            logonImap = new TKMP.Net.BasicImapLogon(mailServerAccount, mailServerPass);
            imap = new TKMP.Net.ImapClient(logonImap, server, mailServerPort);

            if (mailServerSslConnection)
            {
                imap.AuthenticationProtocol = AuthenticationProtocols.SSL;
            }
            else
            {
                imap.AuthenticationProtocol = AuthenticationProtocols.None;
            }

            imap.KeepAlive = true;

            //受信イベントを登録（ログ出力）
            imap.MessageReceive += new TKMP.Net.MessageReceiveHandler(ServerMessageReceive);

            //送信イベントを登録（ログ出力）
            imap.MessageSend += new TKMP.Net.MessageSendHandler(ServerMessageSend);

            return imap.Connect();
        }

        /// <summary>
        /// クローズ
        /// </summary>
        protected override void CloseConnection()
        {
            imap.Close();
        }

        /// <summary>
        /// サーバーより指定されたメールBOXのメールをErsMailDetail[]に格納
        /// </summary>
        /// <param name="mailBoxName"></param>
        /// <returns></returns>
        public override List<IDictionary<string, object>> GetMailList()
        {
            var ret = new List<IDictionary<string, object>>();

            var mailList = this.GetselectMailBoxMails(mailBoxName);
            if (mailList == null)
            {
                return null;
            }

            foreach (var mail in mailList)
            {
                try
                {
                    var mailDetail = this.GetMailReaderInfo(mail);
                    ret.Add(mailDetail);
                }
                catch(Exception ex)
                {
                    mailSetErrLog += "GetMailList()エラー, uid= " + mail.UID + System.Environment.NewLine
                        + ex.ToString() + System.Environment.NewLine + System.Environment.NewLine;
                }
            }

            return ret;

        }

        /// <summary>
        /// メール移動処理
        /// </summary>
        /// <param name="mail"></param>
        public override void MailMoveDelete(string ReadMailDirName, IEnumerable<string> arr)
        {
            //メール移動フォルダの確認
            this.CkExistenceServerDir(ReadMailDirName);

            List<string> errLogArr = new List<string>();

            var mailList = this.GetselectMailBoxMails(mailBoxName);
            if (mailList == null)
            {
                return;
            }

            //受信箱取得
            foreach (var mail in this.GetselectMailBoxMails(mailBoxName))
            {
                foreach (string UID in arr)
                {
                    try
                    {
                        //サーバーメールと削除対象メールのUIDが一致すれば処理開始
                        if (mail.UID.Equals(UID))
                        {
                            mail.Copy(imap.FindMailbox(ReadMailDirName));
                            mail.Delete ();
                            break;
                        }
                    }
                    catch(Exception ex)
                    {
                        string errLog = string.Empty;
                        errLog += "mail-move-err :uid=" + UID + Environment.NewLine;
                        errLog += ex.ToString();
                        errLogArr.Add(errLog);
                        break;
                    }
                }
            }

            //1件でもエラーがあれば
            if (errLogArr.Count > 0)
            {
                throw new Exception(string.Join(",", errLogArr));
            }
        }

        /// <summary>
        /// 指定したメールboxのメール取得
        /// </summary>
        /// <param name="mailBoxName"></param>
        /// <returns></returns>
        private MailData_Imap[] GetselectMailBoxMails(string mailBoxName)
        {
            var ret = imap.FindMailbox(mailBoxName);

            if (ret != null)
            {
                return ret.MailDatas;
            }

            return null;
        }

        private IDictionary<string, object> GetMailReaderInfo(IMailData mail)
        {
            //メール内容取得
            mail.ReadHeader();
            mail.ReadBody();
            var read_date = mail.DataStream;
            var mailContnts = new TKMP.Reader.MailReader(read_date, false);

            //メール情報をハッシュへ
            return this.SetMailReaderInfo(mailContnts, mail.UID);
        }

        /// <summary>
        /// メール情報をハッシュへ
        /// </summary>
        /// <param name="mailContnts"></param>
        /// <returns></returns>
        private IDictionary<string, object> SetMailReaderInfo(TKMP.Reader.MailReader mailReader, string uid)
        {
            ErsIgnoreCaseDictionary mailDic = new ErsIgnoreCaseDictionary();

            //ヘッダー配列取得
            var mailHeaderArr = mailReader.HeaderCollection.GetEnumerator();
            mailHeaderArr.MoveNext();

            for (int cnt = 0; cnt < mailReader.HeaderCollection.Count; cnt++)
            {
                string key = ((TKMP.Reader.Header.HeaderString)(mailHeaderArr.Current)).Name;
                string value = ((TKMP.Reader.Header.HeaderString)(mailHeaderArr.Current)).Data;

                mailDic.Add(key, value);
                mailHeaderArr.MoveNext();
            }

            mailDic.Add("uid", uid);
            mailDic.Add("body", mailReader.MainText);

            return mailDic;
        }

        /// <summary>
        /// サーバー側のフォルダ存在チェック、なければ作成
        /// </summary>
        /// <param name="mailBoxName"></param>
        private void CkExistenceServerDir(string mailBoxName)
        {
            var seenDir = imap.FindMailbox(mailBoxName);

            if (seenDir == null)
            {
                imap.CreateMailbox(mailBoxName);
            }
        }

        /// <summary>
        /// ログ内容取得
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        public override string GetMailSetErrLog()
        {
            return this.mailSetErrLog;
        }

        /// <summary>
        /// サーバーへメッセージを受信したときのイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ServerMessageSend(object sender, TKMP.Net.MessageArgs e)
        {
            communicationLog+= DateTime.Now  + ":send," + (e.ToString());
        }

        /// <summary>
        /// サーバーへメッセージを送信したときのイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ServerMessageReceive(object sender, TKMP.Net.MessageArgs e)
        {
            communicationLog += DateTime.Now + ":receive," + (e.Message);
        }

        /// <summary>
        /// ログ内容取得
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override string GetCommunicationLog()
        {
            return this.communicationLog;
        }
    }
}
