﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Collections;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.util.mail
{
    public abstract class ErsImailer
        : IDisposable
    {
        public abstract List<IDictionary<string, object>> GetMailList();

        public abstract void MailMoveDelete(string ReadMailDirName, IEnumerable<string> arr);

        public abstract string GetMailSetErrLog();

        protected abstract bool OpenConnectionActually(string server, string mailServerAccount, string mailServerPass, int mailServerPort, bool mailServerSslConnection);

        protected abstract void CloseConnection();

        protected abstract string GetCommunicationLog();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="server"></param>
        /// <param name="mailServerAccount"></param>
        /// <param name="mailServerPass"></param>
        /// <param name="mailServerPort"></param>
        /// <param name="mailServerSslConnection"></param>
        /// <returns></returns>
        public bool OpenConnection(string server, string mailServerAccount, string mailServerPass, int mailServerPort, bool mailServerSslConnection)
        {
            var result = this.OpenConnectionActually(server, mailServerAccount, mailServerPass, mailServerPort, mailServerSslConnection);

            _disposed = false;

            return result;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            // If you need thread safety, use a lock around these 
            // operations, as well as in your methods that use the resource.
            if (!_disposed)
            {
                if (disposing)
                {

                    //切断
                    this.CloseConnection();

                    //通信ログ内容
                    ErsDebug.WriteLine(this.GetCommunicationLog());
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
        }

        bool _disposed { get; set; }
    }
}
