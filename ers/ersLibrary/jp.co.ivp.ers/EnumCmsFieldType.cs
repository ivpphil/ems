﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumCmsFieldType
    {
        /// <summary>
        /// 0: 非表示
        /// </summary>
        Hide = 0,

        /// <summary>
        /// 1: 表示
        /// </summary>
        Visible,

        /// <summary>
        /// 必須
        /// </summary>
        Required
    }
}
