﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumEnqSaveMode
        : short
    {
        /// <summary>
        /// 0:temporary
        /// </summary>
        Temporary,
        /// <summary>
        /// 1:submit
        /// </summary>
        Submit
    }
}
