﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumConvCode
        : short
    {
        /// <summary>
        /// 1: ローソン
        /// </summary>
        LAWSON = 1,

        /// <summary>
        /// 2: ファミリーマート
        /// </summary>
        FamilyMart,

        /// <summary>
        /// 3: サンクス
        /// </summary>
        Sunkus,

        /// <summary>
        /// 4: サークルK
        /// </summary>
        CircleK,

        /// <summary>
        /// 5: ミニストップ
        /// </summary>
        MINISTOP,

        /// <summary>
        /// 6: デイリーヤマザキ
        /// </summary>
        DailyYamazaki,

        /// <summary>
        /// 7: セブンイレブン
        /// </summary>
        SevenEleven,
    }
}
