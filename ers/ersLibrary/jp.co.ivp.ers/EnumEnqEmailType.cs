﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumEnqEmailType
        : short
    {
        /// <summary>
        /// 0: end user
        /// </summary>
        EndUser,
        /// <summary>
        /// 1: client
        /// </summary>
        Client
    }
}
