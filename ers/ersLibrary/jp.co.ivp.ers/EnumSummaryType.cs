﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumSummaryType
    {
        /// <summary>
        /// 0: 集計
        /// </summary>
        Aggregate,

        /// <summary>
        /// 1: データ抽出
        /// </summary>
        Extract,
    }
}
