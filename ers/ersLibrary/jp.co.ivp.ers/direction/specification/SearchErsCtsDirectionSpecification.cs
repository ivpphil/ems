﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.direction.specification
{
    public class SearchErsCtsDirectionSpecification
        : SearchSpecificationBase
    {
        protected override string GetSearchDataSql()
        {
            string strQuery = @"SELECT  
                                           cts_directions_management_t.id, 
                                           cts_directions_management_t.intime, 
                                           cts_directions_management_t.utime,
                                           CASE WHEN cts_directions_management_t.check_s = 1 THEN 'Already Read' ELSE 'Unread' END AS check_desc,
                                           cts_directions_management_t.to_user_id, 
                                           cts_directions_management_t.from_user_id, 
                                           cts_directions_management_t.dmemo,
                                           cts_enquiry_t.case_no,
                                           cts_enquiry_t.enq_casename,
                                           cts_login_t.ag_name AS to_ag_name,  
                                           from_cts_login_t.ag_name AS from_ag_name,

                                           (SELECT common_namecode_t.namename FROM common_namecode_t INNER JOIN cts_enquiry_t AS inner_cts_enquiry_t ON common_namecode_t.code = inner_cts_enquiry_t.enq_status AND inner_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND common_namecode_t.type_code = 'ENQSTS') AS w_status,
                                           (SELECT common_namecode_t.namename FROM common_namecode_t INNER JOIN cts_enquiry_t AS inner_cts_enquiry_t ON common_namecode_t.code = inner_cts_enquiry_t.enq_situation AND inner_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND common_namecode_t.type_code = 'ENQSIT') AS w_situation,
                                           (SELECT common_namecode_t.namename FROM common_namecode_t INNER JOIN cts_enquiry_t AS inner_cts_enquiry_t ON common_namecode_t.code = inner_cts_enquiry_t.enq_progress AND inner_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND common_namecode_t.type_code = 'ENQPGR') AS w_progress,
                                           (SELECT common_namecode_t.namename FROM common_namecode_t INNER JOIN cts_enquiry_t AS inner_cts_enquiry_t ON common_namecode_t.code = inner_cts_enquiry_t.enq_priorty AND inner_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND common_namecode_t.type_code = 'ENQPRY') AS w_priority,
                                           (SELECT common_namecode_t.namename FROM common_namecode_t INNER JOIN cts_enquiry_t AS inner_cts_enquiry_t ON common_namecode_t.code = inner_cts_enquiry_t.enq_type AND inner_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND common_namecode_t.type_code = 'ENQTYP') AS w_type,

                                           (SELECT cts_enquiry_category_t.namename FROM cts_enquiry_category_t INNER JOIN cts_enquiry_t AS inner_cts_enquiry_t ON cts_enquiry_category_t.code = inner_cts_enquiry_t.cate1 AND inner_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND cts_enquiry_category_t.type_code = 'ENQCT1' AND cts_enquiry_category_t.site_id = cts_login_t.site_id) AS cate1,
                                           (SELECT cts_enquiry_category_t.namename FROM cts_enquiry_category_t INNER JOIN cts_enquiry_t AS inner_cts_enquiry_t ON cts_enquiry_category_t.code = inner_cts_enquiry_t.cate2 AND inner_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND cts_enquiry_category_t.type_code = 'ENQCT2' AND cts_enquiry_category_t.site_id = cts_login_t.site_id) AS cate2,
                                           (SELECT cts_enquiry_category_t.namename FROM cts_enquiry_category_t INNER JOIN cts_enquiry_t AS inner_cts_enquiry_t ON cts_enquiry_category_t.code = inner_cts_enquiry_t.cate3 AND inner_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND cts_enquiry_category_t.type_code = 'ENQCT3' AND cts_enquiry_category_t.site_id = cts_login_t.site_id) AS cate3,
                                           (SELECT cts_enquiry_category_t.namename FROM cts_enquiry_category_t INNER JOIN cts_enquiry_t AS inner_cts_enquiry_t ON cts_enquiry_category_t.code = inner_cts_enquiry_t.cate4 AND inner_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND cts_enquiry_category_t.type_code = 'ENQCT4' AND cts_enquiry_category_t.site_id = cts_login_t.site_id) AS cate4,
                                           (SELECT cts_enquiry_category_t.namename FROM cts_enquiry_category_t INNER JOIN cts_enquiry_t AS inner_cts_enquiry_t ON cts_enquiry_category_t.code = inner_cts_enquiry_t.cate5 AND inner_cts_enquiry_t.case_no = cts_enquiry_t.case_no AND cts_enquiry_category_t.type_code = 'ENQCT5' AND cts_enquiry_category_t.site_id = cts_login_t.site_id) AS cate5

                                      FROM cts_directions_management_t
                                     INNER JOIN cts_enquiry_t ON cts_directions_management_t.case_no = cts_enquiry_t.case_no
                                     INNER JOIN cts_login_t ON cts_directions_management_t.to_user_id = cts_login_t.id
                                     INNER JOIN cts_login_t AS from_cts_login_t ON cts_directions_management_t.from_user_id = from_cts_login_t.id ";
            return strQuery;
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            string strQuery = @"SELECT COUNT(DISTINCT cts_directions_management_t.id) AS " + countColumnAlias + " "
                                     + "FROM cts_directions_management_t "
                                     + "INNER JOIN cts_enquiry_t ON cts_directions_management_t.case_no = cts_enquiry_t.case_no "
                                     + "INNER JOIN cts_login_t ON cts_directions_management_t.to_user_id = cts_login_t.id ";
            return strQuery;
        }
    }
}
