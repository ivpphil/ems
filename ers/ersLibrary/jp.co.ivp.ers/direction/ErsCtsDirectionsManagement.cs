﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.direction
{
    public class ErsCtsDirectionsManagement
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual int? case_no { get; set; }
        public virtual int? sub_no { get; set; }
        public virtual int? to_user_id { get; set; }
        public virtual int? from_user_id { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual short check_s { get; set; }
        public virtual string dmemo { get; set; }
        public virtual int? site_id { get; set; }
    }
}
