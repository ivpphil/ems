﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.direction
{
    public class ErsCtsDirectionsManagementCriteria : Criteria
	{
        public virtual int? id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_directions_management_t.id", value, Operation.EQUAL));
            }
        }

        public virtual int? case_no 
        { 
            set
            {
                Add(Criteria.GetCriterion("cts_directions_management_t.case_no", value, Operation.EQUAL));
            }
        }

        public virtual DateTime? datefrom
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_directions_management_t.intime", value, Operation.GREATER_EQUAL));
            }
        }
        public virtual DateTime? dateto
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_directions_management_t.intime", value, Operation.LESS_EQUAL));
            }
        }

        public virtual EnumAgType? ag_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_login_t.ag_type", value, Operation.EQUAL));
            }
        }

        public virtual short check_s
        {
            set
            {
                Add(Criteria.GetCriterion("cts_directions_management_t.check_s", value, Operation.EQUAL));
            }
        }

        public virtual int? to_user_id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_directions_management_t.to_user_id", value, Operation.EQUAL));
            }
        }

        public void AddOrderByIntime(OrderBy orderBy)
        {
            this.AddOrderBy("cts_directions_management_t.intime", orderBy);
        }

        public virtual EnumEnqProgress? enq_progress
        {
            set
            {
                Add(Criteria.GetCriterion("cts_enquiry_t.enq_progress", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("cts_directions_management_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_directions_management_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}
