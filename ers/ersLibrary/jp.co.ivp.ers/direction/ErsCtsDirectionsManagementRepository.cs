﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.direction
{
    public class ErsCtsDirectionsManagementRepository
        : ErsRepository<ErsCtsDirectionsManagement>
    {
        public ErsCtsDirectionsManagementRepository()
            : base("cts_directions_management_t")
        {
        }

        public ErsCtsDirectionsManagementRepository(ErsDatabase objDB)
            : base("cts_directions_management_t", objDB)
        {
        }

        public IList<ErsCtsDirection> FindInstruction(db.Criteria criteria)
        {
            var spec = ErsFactory.ersCtsDirectionFactory.GetSearchErsCtsDirectionSpecification();
            List<ErsCtsDirection> lstRet = new List<ErsCtsDirection>();
            var list = spec.GetSearchData(criteria);
            foreach (var dr in list)
            {
                var dir = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionWithParameters(dr);
                lstRet.Add(dir);
            }
            return lstRet;
        }

        public long GetRecordCountInstruction(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsDirectionFactory.GetSearchErsCtsDirectionSpecification();
            return spec.GetCountData(criteria);
        }
    }
}
