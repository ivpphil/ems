﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.Inquiry.specification;
using jp.co.ivp.ers.fromaddress;
using jp.co.ivp.ers.fromaddress.strategy;
using jp.co.ivp.ers.clientescalation;
using jp.co.ivp.ers.incomingmail;
using jp.co.ivp.ers.direction.specification;

namespace jp.co.ivp.ers.direction
{
    public class ErsCtsDirectionFactory
    {
        public ErsCtsDirectionsManagementCriteria GetErsCtsDirectionsManagementCriteria()
        {
            return new ErsCtsDirectionsManagementCriteria();
        }

        public ErsCtsDirectionsManagementRepository GetErsCtsDirectionsManagementRepository()
        {
            return new ErsCtsDirectionsManagementRepository();
        }

        public ErsCtsDirection GetErsCtsDirection()
        {
            return new ErsCtsDirection();
        }

        public SearchErsCtsDirectionSpecification GetSearchErsCtsDirectionSpecification()
        {
            return new SearchErsCtsDirectionSpecification();
        }

        public ErsCtsDirection GetErsCtsDirectionWithParameters(Dictionary<string, object> parameters)
        {
            var Direction = this.GetErsCtsDirection();
            Direction.OverwriteWithParameter(parameters);
            return Direction;
        }

        public ErsCtsDirection GetErsCtsDirectionWithCaseNo(int? case_no)
        {
            var repository = this.GetErsCtsDirectionsManagementRepository();
            var criteria = this.GetErsCtsDirectionsManagementCriteria();
            criteria.case_no = case_no;
            var list = repository.FindInstruction(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");
            return list[0];
        }

        public ErsCtsDirection GetErsCtsDirectionWithID(int? ID)
        {
            var repository = this.GetErsCtsDirectionsManagementRepository();
            var criteria = this.GetErsCtsDirectionsManagementCriteria();
            criteria.id = ID;
            var list = repository.FindInstruction(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");
            return list[0];
        }

        public virtual ErsCtsDirectionsManagement GetErsCtsDirectionsManagement()
        {
            return new ErsCtsDirectionsManagement();
        }
    }
}
