﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;
using System.Reflection;

namespace jp.co.ivp.ers.direction
{
    public class ErsCtsDirection
        : IErsCollectable, IOverwritable
    {
        public virtual int? id { get; set; }
        public virtual int? case_no { get; set; }
        public virtual int? sub_no { get; set; }
        public virtual int? to_user_id { get; set; }
        public virtual int? from_user_id { get; set; }
        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual short check_s { get; set; }
        public virtual string check_desc { get; set; }
        public virtual string dmemo { get; set; }
        public virtual EnumEnqType? type { get; set; }
        public virtual string enq_casename { get; set; }
        
        public virtual EnumEnqStatus? status { get; set; }
        public virtual EnumEnqProgress? progress { get; set; }
        public virtual EnumEnqSituation? situation { get; set; }
        public virtual EnumEnqPriority? priority { get; set; }
        public virtual string cate1 { get; set; }
        public virtual string cate2 { get; set; }
        public virtual string cate3 { get; set; }
        public virtual string cate4 { get; set; }
        public virtual string cate5 { get; set; }
        public virtual string ag_name { get; set; }
        public virtual string to_ag_name { get; set; }
        public virtual string from_ag_name { get; set; }

        public virtual string w_status { get; set; }
        public virtual string w_situation { get; set; }
        public virtual string w_progress { get; set; }
        public virtual string w_priority { get; set; }
        public virtual string w_type { get; set; }

        public Dictionary<string, object> GetPropertiesAsDictionary()
        {
            return ErsReflection.GetPropertiesAsDictionary(this, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        }

        public void OverwriteWithParameter(IDictionary<string, object> dictionary)
        {
            var dic = ErsCommon.OverwriteDictionary(this.GetPropertiesAsDictionary(), dictionary);
            ErsReflection.SetPropertyAll(this, dic);
        }
    }
}
