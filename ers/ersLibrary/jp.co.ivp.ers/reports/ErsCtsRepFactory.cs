﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.reports.specification;
using jp.co.ivp.ers.reports.strategy;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepFactory
    {
        public virtual ErsCtsRepAgeStgy GetErsCtsRepAgeStgy()
        {
            return new ErsCtsRepAgeStgy();
        }

        public virtual ErsCtsRepAgeSpecification GetErsCtsRepAgeSpecification()
        {
            return new ErsCtsRepAgeSpecification();
        }

        public virtual ErsCtsRepAge GetErsCtsRepAgeWithParameter(Dictionary<string, object> inputParameters)
        {
            var objAge = this.GetErsCtsRepAge();

            objAge.OverwriteWithParameter(inputParameters);

            return objAge;
        }

        public virtual ErsCtsRepAge GetErsCtsRepAge()
        {
            return new ErsCtsRepAge();
        }

        public virtual ErsCtsRepCallStgy GetErsCtsRepCallStgy()
        {
            return new ErsCtsRepCallStgy();
        }

        public virtual ErsCtsRepCallCriteria GetErsCtsRepCallCriteria()
        {
            return new ErsCtsRepCallCriteria();
        }

        public virtual ErsCtsRepCallSpecification GetErsCtsRepCallSpecification()
        {
            return new ErsCtsRepCallSpecification();
        }

        public virtual ErsCtsRepCall GetErsCtsRepCallWithParameter(Dictionary<string, object> inputParameters)
        {

            var objCall = new ErsCtsRepCall();

            objCall.OverwriteWithParameter(inputParameters);

            return objCall;
        }

        public virtual ErsCtsRepCategoryStgy GetErsCtsRepCategoryStgy()
        {
            return new ErsCtsRepCategoryStgy();
        }

        public virtual ErsCtsRepCategoryCriteria GetErsCtsRepCategoryCriteria()
        {
            return new ErsCtsRepCategoryCriteria();
        }

        public virtual ErsCtsRepCategorySpecification GetErsCtsRepCategorySpecification()
        {
            return new ErsCtsRepCategorySpecification();
        }

        public virtual ErsCtsRepCategory GetErsCtsRepCategoryWithParameter(Dictionary<string, object> inputParameters)
        {

            var objProd = this.GetErsCtsRepCategory();

            objProd.OverwriteWithParameter(inputParameters);

            return objProd;
        }

        public virtual ErsCtsRepCategory GetErsCtsRepCategory()
        {
            return new ErsCtsRepCategory();
        }

        public virtual ErsCtsRepContactLogStgy GetErsCtsRepContactLogStgy()
        {
            return new ErsCtsRepContactLogStgy();
        }

        public virtual ErsCtsRepContactLogCriteria GetErsCtsRepContactLogCriteria()
        {
            return new ErsCtsRepContactLogCriteria();
        }

        public virtual ErsCtsRepContactLogSpecification GetErsCtsRepContactLogSpecification()
        {
            return new ErsCtsRepContactLogSpecification();
        }

        public virtual ErsCtsRepContactLog GetErsCtsRepContactLogWithParameter(Dictionary<string, object> inputParameters)
        {

            var objProd = new ErsCtsRepContactLog();

            objProd.OverwriteWithParameter(inputParameters);

            return objProd;
        }

        public virtual ErsCtsRepInboundStgy GetErsCtsRepInboundStgy()
        {
            return new ErsCtsRepInboundStgy();
        }

        public virtual ErsCtsRepInboundCriteria GetErsCtsRepInboundCriteria()
        {
            return new ErsCtsRepInboundCriteria();
        }

        public virtual ErsCtsRepInboundSpecification GetErsCtsRepInboundSpecification()
        {
            return new ErsCtsRepInboundSpecification();
        }

        public virtual ErsCtsRepInbound GetErsCtsRepInboundWithParameter(Dictionary<string, object> inputParameters)
        {

            var objInbound = new ErsCtsRepInbound();

            objInbound.OverwriteWithParameter(inputParameters);

            return objInbound;
        }

        public virtual ErsCtsRepOrderStgy GetErsCtsRepOrderStgy()
        {
            return new ErsCtsRepOrderStgy();
        }

        public virtual ErsCtsRepOrderCriteria GetErsCtsRepOrderCriteria()
        {
            return new ErsCtsRepOrderCriteria();
        }

        public virtual ErsCtsRepOrderSpecification GetErsCtsRepOrderSpecification()
        {
            return new ErsCtsRepOrderSpecification();
        }

        public virtual ErsCtsRepOrder GetErsCtsRepOrderWithParameter(Dictionary<string, object> inputParameters)
        {

            var objProd = new ErsCtsRepOrder();

            objProd.OverwriteWithParameter(inputParameters);

            return objProd;
        }

        public virtual ErsCtsRepProdStgy GetErsCtsRepProdStgy()
        {
            return new ErsCtsRepProdStgy();
        }

        public virtual ErsCtsRepProdCriteria GetErsCtsRepProdCriteria()
        {
            return new ErsCtsRepProdCriteria();
        }

        public virtual ErsCtsRepProdSpecification GetErsCtsRepProdSpecification()
        {
            return new ErsCtsRepProdSpecification();
        }

        public virtual ErsCtsRepProd GetErsCtsRepProdWithParameter(Dictionary<string, object> inputParameters)
        {

            var objProd = new ErsCtsRepProd();

            objProd.OverwriteWithParameter(inputParameters);

            return objProd;
        }

        public virtual ErsCtsRepSalesStgy GetErsCtsRepSalesStgy()
        {
            return new ErsCtsRepSalesStgy();
        }

        public virtual ErsCtsRepSalesCriteria GetErsCtsRepSalesCriteria()
        {
            return new ErsCtsRepSalesCriteria();
        }

        public virtual ErsCtsRepSalesSpecification GetErsCtsRepSalesSpecification()
        {
            return new ErsCtsRepSalesSpecification();
        }

        public virtual ErsCtsRepSales GetErsCtsRepSalesWithParameter(Dictionary<string, object> inputParameters)
        {

            var objProd = new ErsCtsRepSales();

            objProd.OverwriteWithParameter(inputParameters);

            return objProd;
        }
    }
}
