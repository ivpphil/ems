﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.IO;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepInbound:ErsRepositoryEntity
    {

        public override int? id { get; set; }
        public virtual string namename { get; set; }
        public virtual EnumEnqType? enq_type { get; set; }
        public virtual EnumEnqProgress? enq_progress { get; set; }
        public virtual EnumEnqSituation? enq_situation { get; set; }
        public virtual int enq_type_count { get; set; }
        public virtual int enq_progress_count { get; set; }
        public virtual int enq_situation_count { get; set; }

        public virtual int case_no { get; set; }
        public virtual string mcode { get; set; }
        public virtual DateTime intime { get; set; }
        public virtual DateTime utime { get; set; }
        public virtual string lname { get; set; }
        public virtual string fname { get; set; }
        public virtual EnumEnqProgress? progress { get; set; }
        public virtual EnumEnqStatus? status { get; set; }
        public virtual string situation { get; set; }
        public virtual string enq_casename { get; set; }
        public virtual EnumEnqPriority? priority { get; set; }
        public virtual string ag_name { get; set; }
        public virtual EnumAgType? ag_type { get; set; }
        public virtual string agent { get; set; }

        public virtual string cate1 { get; set; }
        public virtual string cate2 { get; set; }
        public virtual string cate3 { get; set; }
        public virtual string cate4 { get; set; }
        public virtual string cate5 { get; set; }
        public virtual string type { get; set; }
        public virtual string priorty { get; set; }

        public virtual string enq_detail { get; set; }
        public virtual string ans_detail { get; set; }
        public virtual string email_header { get; set; }
        public virtual string email_body { get; set; }
        public virtual string email_fotter { get; set; }
        public virtual string memo { get; set; }

 
        public virtual int rawspan_typ { get; set; }
        public virtual int rawspan_prg { get; set; }

        public virtual string w_status { get; set; }
        public virtual string w_situation { get; set; }
        public virtual string w_progress { get; set; }
        public virtual string w_priority { get; set; }
        public virtual string w_type { get; set; }
    }
}
