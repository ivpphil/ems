﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.IO;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepOrder
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual string agent { get; set; }
        public virtual int amount { get; set; }
        public virtual int price { get; set; }
        public virtual int temp_amount { get; set; }
        public virtual int temp_price { get; set; }
        public virtual int total_amount { get; set; }
        public virtual int total_price { get; set; }

        public virtual DateTime? datefrom { get; set; }
        public virtual DateTime? dateto { get; set; }
        public virtual string agentid { get; set; }
        public virtual EnumAgType? ag_type { get; set; }
    }
}
