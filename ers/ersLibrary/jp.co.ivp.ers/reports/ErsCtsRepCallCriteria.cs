﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepCallCriteria
        :Criteria
    {
        protected internal ErsCtsRepCallCriteria()
        {
        }
        public virtual DateTime? datefrom
        {
            set
            {
            this.Add(Criteria.GetCriterion("t.intime", value, Operation.GREATER_EQUAL));
            }
        }

        public virtual DateTime? dateto
        {
            set
            {
                this.Add(Criteria.GetCriterion("t.intime", value, Operation.LESS_EQUAL));
            }
        }

        public virtual EnumAgType? ag_type
        {
            set
            { this.Add(Criteria.GetCriterion("t.ag_type", value, Operation.EQUAL)); }
        }

        public virtual string scale_code
        {
            set
            { this.Add(Criteria.GetCriterion("t.scale_code", value, Operation.EQUAL)); }
        }

        public virtual string days
        {
            set
            {
                this.Add(Criteria.GetCriterion("t.byhalfhour", value, Operation.NOT_EQUAL));
            }
        }
    }
}
