﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports.specification
{
    [Obsolete("分割する")]
    public class ErsCtsRepSalesSpecification
        : ISearchSpecification
    {
        public virtual List<Dictionary<string, object>> GetInterval(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepSalesIntervalSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        // Regular Orders
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepSalesRecordSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetTotalByInterval(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepSalesTotalByIntervalSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetTotalByDNo(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepSalesTotalByDNoSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        // Temporary Orders
        public virtual List<Dictionary<string, object>> GetSearchTempData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepSalesTempRecordSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetTempTotalByInterval(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepSalesTempTotalByIntervalSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetTempTotalByDNo(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepSalesTempTotalByDNoSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetProductList(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepSalesProductSearch();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual int GetCountData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepSalesCountSpecification();

            var record = ErsRepository.SelectSatisfying(specificationForSQL, criteria);
            if (record.Count == 0)
                return 0;

            return Convert.ToInt32(record[0]["count"]);
        }


        internal protected class ErsCtsRepSalesIntervalSpecification
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                return @"SELECT DISTINCT scale_value AS byhalfhour FROM s_scale WHERE scale_code='byhalfhour'";
            }
        }

        //Regular Orders
        internal protected class ErsCtsRepSalesRecordSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = "SELECT DISTINCT(byhalfhour), scode, sum(amount) as amount FROM ( ";
                strQuery += "SELECT  DISTINCT(s_scale.scale_value) as byhalfhour, s_scale.scale_code, scode, intime, ";
                strQuery += "(CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE sum(amount) END) as amount ";
                strQuery += "FROM s_scale CROSS JOIN ";
                strQuery += "(select ds_master_t.d_no, ds_master_t.scode, ds_master_t.amount, ";
                strQuery += "(to_char(EXTRACT(hour from d_master_t.intime), '00dS')) || ':' ||  ";
                strQuery += "(CASE WHEN EXTRACT(minute from d_master_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END) as recordtime, d_master_t.intime ";
                strQuery += "from ds_master_t join d_master_t ";
                strQuery += "on ds_master_t.d_no = d_master_t.d_no " + site_id_condition_sql("d_master_t") +") as salesamount ";
                strQuery += "GROUP BY scode, recordtime, byhalfhour, intime, scale_code order by amount) as repsales";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepSalesTotalByIntervalSpecification
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                string strQuery = "SELECT DISTINCT(byhalfhour), sum(amount) as amount FROM ( ";
                strQuery += "SELECT  DISTINCT(s_scale.scale_value) as byhalfhour, s_scale.scale_code, scode, intime, ";
                strQuery += "(CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE sum(amount) END) as amount  ";
                strQuery += "FROM s_scale CROSS JOIN ";
                strQuery += "(select ds_master_t.d_no, ds_master_t.scode, ds_master_t.amount, ";
                strQuery += "(to_char(EXTRACT(hour from d_master_t.intime), '00dS')) || ':' ||  ";
                strQuery += "(CASE WHEN EXTRACT(minute from d_master_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END) as recordtime, d_master_t.intime ";
                strQuery += "from ds_master_t join d_master_t ";
                strQuery += "on ds_master_t.d_no = d_master_t.d_no " + site_id_condition_sql("d_master_t") +") as salesamount ";
                strQuery += "GROUP BY scode, recordtime, byhalfhour, intime, scale_code order by amount) as repsales ";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepSalesTotalByDNoSpecification
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                string strQuery = "SELECT scode, sum(amount) as amount FROM ( ";
                strQuery += "SELECT  DISTINCT(s_scale.scale_value) as byhalfhour, s_scale.scale_code, scode, intime, ";
                strQuery += "(CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE sum(amount) END) as amount ";
                strQuery += "FROM s_scale CROSS JOIN ";
                strQuery += "(select ds_master_t.d_no, ds_master_t.scode, ds_master_t.amount, ";
                strQuery += "(to_char(EXTRACT(hour from d_master_t.intime), '00dS')) || ':' ||  ";
                strQuery += "(CASE WHEN EXTRACT(minute from d_master_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END) as recordtime, d_master_t.intime ";
                strQuery += "from ds_master_t join d_master_t ";
                strQuery += "on ds_master_t.d_no = d_master_t.d_no " + site_id_condition_sql("d_master_t") +") as salesamount ";
                strQuery += "GROUP BY scode, recordtime, byhalfhour, intime, scale_code) as repsales ";
                return strQuery;
            }
        }

        // Temporary Orders
        internal protected class ErsCtsRepSalesTempRecordSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = "SELECT DISTINCT (byhalfhour), scode, sum(amount) as amount ";
                strQuery += "FROM ( SELECT DISTINCT (s_scale.scale_value) as byhalfhour, s_scale.scale_code, scode, intime, ";
                strQuery += "(CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE sum(amount) END) as amount ";
                strQuery += "FROM s_scale CROSS JOIN ";
                strQuery += "(SELECT bask_t.ransu, bask_t.scode, bask_t.amount, ";
                strQuery += "(to_char(EXTRACT(hour from cts_order_t.intime), '00dS')) || ':' ||  ";
                strQuery += "(CASE WHEN EXTRACT(minute from cts_order_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END) as recordtime, cts_order_t.intime ";
                strQuery += "FROM bask_t JOIN cts_order_t ";
                strQuery += "ON bask_t.ransu = cts_order_t.ransu " + site_id_condition_sql("cts_order_t") +") as repamount ";
                strQuery += "GROUP BY scode, recordtime, byhalfhour, intime, scale_code) as repsales";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepSalesTempTotalByIntervalSpecification
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                string strQuery = " SELECT DISTINCT (byhalfhour), sum(amount) as amount ";
                strQuery += "FROM (SELECT DISTINCT (s_scale.scale_value) as byhalfhour, s_scale.scale_code, scode, intime, ";
                strQuery += "(CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE sum(amount) END) as amount ";
                strQuery += "FROM s_scale CROSS JOIN ";
                strQuery += "(SELECT bask_t.ransu, bask_t.scode, bask_t.amount, ";
                strQuery += "(to_char(EXTRACT(hour from cts_order_t.intime), '00dS')) || ':' ||  ";
                strQuery += "(CASE WHEN EXTRACT(minute from cts_order_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END) as recordtime, cts_order_t.intime ";
                strQuery += "FROM bask_t JOIN cts_order_t ";
                strQuery += "ON bask_t.ransu = cts_order_t.ransu " + site_id_condition_sql("cts_order_t") +") as repamount ";
                strQuery += "GROUP BY scode, recordtime, byhalfhour, intime, scale_code) as repsales ";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepSalesTempTotalByDNoSpecification
           : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                string strQuery = "SELECT DISTINCT (scode), sum(amount) as amount ";
                strQuery += "FROM (SELECT DISTINCT (s_scale.scale_value) as byhalfhour, s_scale.scale_code, scode, intime, ";
                strQuery += "(CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE sum(amount) END) as amount ";
                strQuery += "FROM s_scale CROSS JOIN ";
                strQuery += "(SELECT bask_t.ransu, bask_t.scode, bask_t.amount, ";
                strQuery += "(to_char(EXTRACT(hour from cts_order_t.intime), '00dS')) || ':' ||  ";
                strQuery += "(CASE WHEN EXTRACT(minute from cts_order_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END) as recordtime, cts_order_t.intime ";
                strQuery += "FROM bask_t JOIN cts_order_t ";
                strQuery += "ON bask_t.ransu = cts_order_t.ransu " + site_id_condition_sql("cts_order_t") +") as repamount ";
                strQuery += "GROUP BY scode, recordtime, byhalfhour, intime, scale_code) as repsales ";
                return strQuery;
            }
        }


        internal protected class ErsCtsRepSalesCountSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                return @"SELECT COUNT(scode) FROM s_master_t";
            }
        }

        internal protected class ErsCtsRepSalesProductSearch
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                return @"SELECT scode, cts_sname, sname FROM s_master_t";
            }
        }

        /// <summary>
        /// Returns sql condition for the site_id
        /// </summary>
        /// <param name="table">table name for site_id condition</param>
        /// <returns></returns>
        internal static string site_id_condition_sql(string table)
        {
            int? site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            if (site_id.HasValue)
            {
                return string.Format(" AND ({0}.site_id = {1} OR {0}.site_id = {2}) ", table, site_id, (int)EnumSiteId.COMMON_SITE_ID);
            }
            return string.Empty;
        }
    }
}
