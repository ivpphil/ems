﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ers.jp.co.ivp.ers.reports.specification
{
    public class DailyReportsDataForGraphsSpecification : SearchSpecificationBase
    {
        public EnumReportSummary? report_summary { get; set; }
        public bool isPersonal { get; set; }

        string SQL { get; set; }
        string emp_no { get { return ErsContext.sessionState.Get("mcode"); } }

        protected override string GetSearchDataSql()
        {
            if (isPersonal)
            {
                GetDateReportFilter();
                GetUserDailyReportData();
            }
            else
            {
                GetDateReportFilter();
            }

            return String.Format(@"
                        SELECT pcode,proj_desc, SUM(um_hours) AS total_hours 
                        FROM dailyreport_details_t
                        WHERE {0}
                        GROUP BY pcode,proj_desc
                        UNION ALL
                        SELECT 'SUM' pcode,'Total Hours' proj_desc, SUM(um_hours)
                        FROM dailyreport_details_t
                        WHERE {0}", SQL);
        }

        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return null;
        }

        internal void GetUserDailyReportData()
        {
            SQL = String.Format((SQL + " AND emp_no = '{0}' "), emp_no);
        }

        internal void GetDateReportFilter()
        {
            DateTime DateToday = DateTime.Now;
            string date1 = "";
            string date2 = "";

            switch (report_summary)
            {
                case EnumReportSummary.Today:

                    SQL = String.Format(" report_date = '#{0}#' ", DateTime.Now.Date.ToString("yyyy-MM-dd"));
                    break;
                case EnumReportSummary.Week:

                    date1 = DateToday.AddDays(-(int)DateToday.DayOfWeek).ToString("yyyy-MM-dd");
                    date2 = DateToday.AddDays(-(int)DateToday.DayOfWeek).AddDays(1).ToString("yyyy-MM-dd");
                    break;
                case EnumReportSummary.Month:

                    date1 = new DateTime(DateToday.Year, DateToday.Month, 1).AddDays(-1).ToString("yyyy-MM-dd");
                    date2 = new DateTime(DateToday.Year, DateToday.Month, 1).AddMonths(1).ToString("yyyy-MM-dd");
                    break;

                case EnumReportSummary.Year:

                    date1 = new DateTime(DateToday.Year, 1, 1).AddDays(-1).ToString("yyyy-MM-dd");
                    date2 = new DateTime(DateToday.Year, 1, 1).AddYears(1).ToString("yyyy-MM-dd");
                    break;

                default:

                    if (report_summary.Equals(EnumReportSummary.FirstHalf) || report_summary.Equals(EnumReportSummary.SecondHalf))
                    {
                        if (report_summary.Equals(EnumReportSummary.FirstHalf))
                        {
                            date1 = new DateTime(DateToday.Year, 1, 1).AddDays(-1).ToString("yyyy-MM-dd");
                            date2 = new DateTime(DateToday.Year, 7, 1).ToString("yyyy-MM-dd");
                        }
                        else if (report_summary.Equals(EnumReportSummary.SecondHalf))
                        {
                            date1 = new DateTime(DateToday.Year, 7, 1).AddDays(-1).ToString("yyyy-MM-dd");
                            date2 = new DateTime(DateToday.Year, 1, 1).AddYears(1).ToString("yyyy-MM-dd");
                        }
                    }
                    else if (report_summary.Equals(EnumReportSummary.FirstQuarter) || report_summary.Equals(EnumReportSummary.SecondQuarter) || report_summary.Equals(EnumReportSummary.ThirdQuarter) || report_summary.Equals(EnumReportSummary.FourthQuarter))
                    {
                        if (report_summary.Equals(EnumReportSummary.FirstQuarter))
                        {
                            date1 = new DateTime(DateToday.Year, 1, 1).AddDays(-1).ToString("yyyy-MM-dd");
                            date2 = new DateTime(DateToday.Year, 4, 1).ToString("yyyy-MM-dd");
                        }
                        else if (report_summary.Equals(EnumReportSummary.SecondQuarter))
                        {
                            date1 = new DateTime(DateToday.Year, 4, 1).AddDays(-1).ToString("yyyy-MM-dd");
                            date2 = new DateTime(DateToday.Year, 7, 1).ToString("yyyy-MM-dd");
                        }
                        else if (report_summary.Equals(EnumReportSummary.ThirdQuarter))
                        {
                            date1 = new DateTime(DateToday.Year, 7, 1).AddDays(-1).ToString("yyyy-MM-dd");
                            date2 = new DateTime(DateToday.Year, 10, 1).ToString("yyyy-MM-dd");
                        }
                        else if (report_summary.Equals(EnumReportSummary.FourthQuarter))
                        {
                            date1 = new DateTime(DateToday.Year, 10, 1).AddDays(-1).ToString("yyyy-MM-dd");
                            date2 = new DateTime(DateToday.Year, 1, 1).AddYears(1).ToString("yyyy-MM-dd");
                        }
                    }
                    break;
            }
            if (report_summary != EnumReportSummary.Today)
            {
                SQL = String.Format(" report_date BETWEEN '#{0}#' AND '#{1}#' ", date1, date2);
            }
        }

    }
}
