﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports.specification
{
    [Obsolete("分割する")]
    public class ErsCtsRepContactLogSpecification
        : ISearchSpecification
    {
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepContactLogRecordSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetDetailData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepContactLogDetailSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetBillData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepContactLogBillDetail();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual int GetCountData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepContactLogCountSpecification();

            var record = ErsRepository.SelectSatisfying(specificationForSQL, criteria);
            if (record.Count == 0)
                return 0;

            return Convert.ToInt32(record[0]["count"]);
        }

        public virtual List<Dictionary<string, object>> GetCSVSearchData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepContactLogCSVSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        internal protected class ErsCtsRepContactLogRecordSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = @"SELECT cts_enquiry_t.case_no, 
                                           cts_enquiry_t.intime, 
                                           member_t.lname, 
                                           member_t.fname, 
                                           (SELECT e.namename from common_namecode_t e inner join cts_enquiry_t f on e.code = f.enq_status and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQSTS') as w_status,
	                                       (SELECT e.namename from common_namecode_t e inner join cts_enquiry_t f on e.code = f.enq_situation and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQSIT') as w_situation,
                                           cts_enquiry_t.enq_casename, 
                                           cts_login_t.ag_name
                                      FROM cts_enquiry_t JOIN member_t ON cts_enquiry_t.mcode = member_t.mcode
                                      LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id ";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepContactLogCountSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = @"SELECT COUNT(cts_enquiry_t.case_no)
                                      FROM cts_enquiry_t JOIN member_t ON cts_enquiry_t.mcode = member_t.mcode
                                      LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id  ";

                return strQuery;
            }
        }

        internal protected class ErsCtsRepContactLogDetailSpecification
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                string strQuery = @"SELECT cts_login_t.ag_name, 
                                           cts_enquiry_t.enq_casename, 
       
                                           (SELECT DISTINCT e.namename from cts_enquiry_category_t e inner join cts_enquiry_t f on e.code = f.cate1 and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQCT1') as cate1,
                                           (SELECT DISTINCT e.namename from cts_enquiry_category_t e inner join cts_enquiry_t f on e.code = f.cate2 and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQCT2') as cate2,
                                           (SELECT DISTINCT e.namename from cts_enquiry_category_t e inner join cts_enquiry_t f on e.code = f.cate3 and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQCT3') as cate3,
                                           (SELECT DISTINCT e.namename from cts_enquiry_category_t e inner join cts_enquiry_t f on e.code = f.cate4 and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQCT4') as cate4,
                                           (SELECT DISTINCT e.namename from cts_enquiry_category_t e inner join cts_enquiry_t f on e.code = f.cate5 and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQCT5') as cate5,

                                           (SELECT DISTINCT e.namename from common_namecode_t e inner join cts_enquiry_t f on e.code = f.enq_progress and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQPGR') as w_enq_progress,
                                           (SELECT DISTINCT e.namename from common_namecode_t e inner join cts_enquiry_t f on e.code = f.enq_status and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQSTS') as w_enq_status,
                                           (SELECT DISTINCT e.namename from common_namecode_t e inner join cts_enquiry_t f on e.code = f.enq_situation and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQSIT') as w_enq_situation,
                                           (SELECT DISTINCT e.namename from common_namecode_t e inner join cts_enquiry_t f on e.code = f.enq_type and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQTYP') as w_enq_type,
                                           (SELECT DISTINCT e.namename from common_namecode_t e inner join cts_enquiry_t f on e.code = f.enq_priorty and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQPRY') as w_enq_priorty

                                      FROM cts_enquiry_t 
                                      LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id ";
                
                return strQuery;
            }
        }

        internal protected class ErsCtsRepContactLogBillDetail
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                string strQuery = "SELECT cts_enquiry_detail_t.sub_no, cts_enquiry_detail_t.enq_corresponding, cts_enquiry_detail_t.enq_detail, cts_enquiry_detail_t.ans_detail, ";
                strQuery += "cts_enquiry_detail_t.email_header, cts_enquiry_detail_t.email_body, cts_enquiry_detail_t.email_fotter, cts_enquiry_detail_t.memo ";
                strQuery += "FROM cts_enquiry_detail_t";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepContactLogCSVSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                //先方送付フォーマットの都合により、固定値設定アリ
                string strQuery = "SELECT DISTINCT '' AS Affiliate, ";
                strQuery += "        cts_enquiry_t.enq_casename AS ProdIDCode, ";
                strQuery += "        cts_enquiry_t.case_no, ";
                strQuery += "        cat2.namename as cat2, ";
                strQuery += "        cat3.namename as cat3, ";
                strQuery += "        cts_login_t.ag_name AS PersonName, ";
                strQuery += "        CASE WHEN member_t.sex = 1 THEN '男性' WHEN member_t.sex = 2 THEN '女性' ELSE '' END AS Gender, ";
                strQuery += "        member_t.birth, ";
                strQuery += "        member_t.lnamek, ";
                strQuery += "        member_t.fnamek, ";
                strQuery += "        pref_t.pref_name, ";
                strQuery += "        cat1.namename as cat1, ";
                strQuery += "        CASE WHEN dtl1.email_type_cnt  > 0  OR cts_enquiry_t.enq_situation IN (3,4,5) THEN 'エスカレーション' ELSE '' END  AS escalation, ";
                strQuery += "        cts_enquiry_t.intime, ";
                strQuery += "        cts_enquiry_t.utime, ";
                strQuery += "        cts_enquiry_t.enq_progress, ";
                strQuery += "        cts_enquiry_t.enq_situation, ";
                strQuery += "        dtl5.content as content , ";
                strQuery += "        regexp_split_to_array(cts_enquiry_t.enq_casename, E'/') AS prodcode ";
                strQuery += "   FROM cts_enquiry_t ";

                strQuery += "   INNER JOIN member_t ON cts_enquiry_t.mcode = member_t.mcode ";
                strQuery += "   LEFT JOIN (SELECT cts_enquiry_detail_t .case_no, Count(cts_enquiry_detail_t.sub_no) as email_type_cnt FROM cts_enquiry_detail_t  where cts_enquiry_detail_t.email_type = 1 Group by case_no) ";
                strQuery += "   AS dtl1 ON cts_enquiry_t.case_no = dtl1.case_no ";

                strQuery += "   LEFT JOIN (SELECT  ";
                strQuery += "   dtl3.case_no, ";
                strQuery += "   array_to_string ";
                strQuery += "   (array(SELECT  ";
                strQuery += "   '【' || dtl4.sub_no || '】' || dtl4.content ";
                strQuery += "   FROM  ";
                strQuery += "   ( ";
                strQuery += "   SELECT dtl2.case_no, ";
                strQuery += "   dtl2.sub_no, ";
                strQuery += "   dtl2.enq_corresponding, ";
                strQuery += "   CASE WHEN dtl2.enq_corresponding = 0 ";
                strQuery += "   	THEN '\n【問】' || dtl2.enq_detail || '\n【答】' || dtl2.ans_detail ";
                strQuery += "   WHEN dtl2.enq_corresponding = 1 ";
                strQuery += "   	THEN '\n【ヘッダー】' || dtl2.email_header || '\n【本文】' || dtl2.email_body || '\n【フッター】' || dtl2.email_fotter ";
                strQuery += "   WHEN dtl2.enq_corresponding = 2 ";
                strQuery += "   	THEN dtl2.memo ";
                strQuery += "   END AS content ";
                strQuery += "   FROM cts_enquiry_detail_t as dtl2 ";
                strQuery += "   order by sub_no ";
                strQuery += "   ) as dtl4 ";
                strQuery += "           where dtl4.case_no = dtl3.case_no ";
                strQuery += "          ORDER BY dtl4.sub_no),'\n') as content ";
                strQuery += "   FROM cts_enquiry_detail_t as dtl3 ";
                strQuery += "   GROUP BY dtl3.case_no ORDER BY dtl3.case_no) ";
                strQuery += "   AS dtl5 ON cts_enquiry_t.case_no = dtl5.case_no ";

                strQuery += "   LEFT JOIN pref_t ON member_t.pref = pref_t.id and pref_t.site_id = member_t.site_id ";
                strQuery += "   LEFT JOIN (SELECT cts_enquiry_category_t.type_code, cts_enquiry_category_t.namename, cts_enquiry_category_t.code FROM cts_enquiry_category_t) AS cat3 ON cat3.code = cts_enquiry_t.cate3 AND cat3.type_code = 'ENQCT3' ";
                strQuery += "   LEFT JOIN (SELECT cts_enquiry_category_t.type_code, cts_enquiry_category_t.namename, cts_enquiry_category_t.code FROM cts_enquiry_category_t) AS cat2 ON cat2.code = cts_enquiry_t.cate2 AND cat2.type_code = 'ENQCT2' ";
                strQuery += "   LEFT JOIN (SELECT cts_enquiry_category_t.type_code, cts_enquiry_category_t.namename, cts_enquiry_category_t.code FROM cts_enquiry_category_t) AS cat1 ON cat1.code = cts_enquiry_t.cate1 AND cat1.type_code = 'ENQCT1' ";
                strQuery += "LEFT JOIN cts_login_t ";
                strQuery += "ON (cts_login_t.id || '') = cts_enquiry_t.user_id ";
                return strQuery;
            }
        }
    }
}
