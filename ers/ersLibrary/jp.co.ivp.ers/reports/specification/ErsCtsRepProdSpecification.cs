﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
 
namespace jp.co.ivp.ers.reports.specification
{
    [Obsolete("分割する")]
    public class ErsCtsRepProdSpecification
        : ISearchSpecification
    {
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {

            var specificationForSQL = new ErsCtsRepProdRecordSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetSearchDataList(ErsCtsRepProdCriteria criteria)
        {

            var specificationForSQL = new ErsCtsRepProdRecordSpecification();
            specificationForSQL.SetProp(criteria);
            criteria.Clear();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTotalPrice(Criteria criteria)
        {

            var specificationForSQL = new ErsCtsRepProdTotalPrice();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetTotalNumber(Criteria criteria)
        {

            var specificationForSQL = new ErsCtsRepProdTotalNumber();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual int GetCountData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepProdCountSpecification();

            var record = ErsRepository.SelectSatisfying(specificationForSQL, criteria);
            if (record.Count == 0)
                return 0;

            return Convert.ToInt32(record[0]["count"]);
        }
         
        internal protected class ErsCtsRepProdRecordSpecification
            : ISpecificationForSQL
        {
            public virtual string prodcode { get; set; }

            public virtual string prodname { get; set; }

            public virtual string agentid { get; set; }

            public virtual EnumAgType? ag_type { get; set; }

            public virtual DateTime? datefrom { get; set; }

            public virtual DateTime? dateto { get; set; }

            public virtual string TargetOrder { get; set; }

            public virtual int? site_id { get; set; }

            public void SetProp(ErsCtsRepProdCriteria criteria)
            {
                this.prodcode = criteria.param_prodcode;
                this.prodname = criteria.param_prodname;
                this.agentid = criteria.param_agentid;
                this.ag_type = criteria.param_ag_type;

                this.datefrom = criteria.param_datefrom;
                this.dateto = criteria.param_dateto;
                this.TargetOrder = criteria.param_TargetOrder;
                this.site_id = criteria.param_site_id;
            }
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                if (this.TargetOrder == "RegOrder")
                {
                    return GetRegOrderQuery();
                }
                else
                { 
                    return GetTempOrderQuery();  
                }
            }

            private string GetRegOrderQuery()
            { 
                string strQuery = "";

                strQuery += "SELECT cts_sname, sname, gcode, scode, price,";
                strQuery += " SUM(tempord) AS tempord, ";
                strQuery += " SUM(cts) AS cts, ";
                strQuery += " SUM(tel) AS tel, ";
                strQuery += " SUM(let) AS let, ";
                strQuery += " SUM(fax) AS fax, ";
                strQuery += " SUM(mb) AS mb, ";
                strQuery += " SUM(pc) AS pc, ";
                strQuery += " SUM(amount) AS totnum, ";
                strQuery += " SUM(amount) * price AS totamt,";
                strQuery += " stock ";
                strQuery += " FROM ( SELECT DISTINCT ds_master_t.amount, ds_master_t.price, ds_master_t.cts_sname, ds_master_t.sname, ";
                strQuery += "       ds_master_t.gcode, ds_master_t.scode, ";
                strQuery += "       CASE WHEN d_master_t.pm_flg = " + (int)EnumPmFlg.REGULAR + " THEN ds_master_t.amount ELSE 0 END AS tempord, ";
                strQuery += "       CASE WHEN d_master_t.pm_flg = " + (int)EnumPmFlg.CTS + " THEN ds_master_t.amount ELSE 0 END AS cts, ";
                strQuery += "       CASE WHEN d_master_t.pm_flg = " + (int)EnumPmFlg.MOBILE + " THEN ds_master_t.amount ELSE 0 END AS mb, ";
                strQuery += "       CASE WHEN d_master_t.pm_flg = " + (int)EnumPmFlg.PC + " THEN ds_master_t.amount ELSE 0 END AS pc, ";
                strQuery += "       CASE WHEN d_master_t.pm_flg = " + (int)EnumPmFlg.SmartPhone + " THEN ds_master_t.amount ELSE 0 END AS smp, ";
                strQuery += "       CASE WHEN d_master_t.pm_flg = " + (int)EnumPmFlg.TEL + " THEN ds_master_t.amount ELSE 0 END AS tel, ";
                strQuery += "       CASE WHEN d_master_t.pm_flg = " + (int)EnumPmFlg.LETTER + " THEN ds_master_t.amount ELSE 0 END AS let, ";
                strQuery += "       CASE WHEN d_master_t.pm_flg = " + (int)EnumPmFlg.FAX + " THEN ds_master_t.amount ELSE 0 END AS fax, ";
                strQuery += "       stock_t.stock, d_master_t.intime, cts_login_t.user_id ";
                strQuery += " FROM d_master_t ";
                strQuery += " INNER JOIN ds_master_t ON (d_master_t.d_no = ds_master_t.d_no AND ds_master_t.doc_bundling_flg = 0) ";
                strQuery += " LEFT JOIN cts_login_t ON d_master_t.user_id = (cts_login_t.id || '') ";
                strQuery += " LEFT JOIN stock_t ON ds_master_t.scode = stock_t.scode";

                strQuery += " WHERE 1 = 1 ";
                if( !string.IsNullOrEmpty(this.prodcode))
                {
                    strQuery += " AND ds_master_t.scode = '" + this.prodcode + "' ";
                }
                if (!string.IsNullOrEmpty(this.prodname))
                {
                    strQuery += " AND ds_master_t.cts_sname like '" + this.prodname + "%' ";
                }
                if (!string.IsNullOrEmpty(this.agentid))
                {
                    strQuery += " AND cts_login_t.user_id = '" + this.agentid + "' ";
                }
                if (this.ag_type.HasValue)
                {
                    strQuery += " AND cts_login_t.ag_type = " + (int)this.ag_type + " ";
                }
                if (this.datefrom.HasValue)
                {
                    strQuery += " AND d_master_t.intime >= '" + String.Format("{0:yyyy/MM/dd HH:mm:ss}", this.datefrom) + "' ";
                }
                if (this.dateto.HasValue)
                {
                    strQuery += " AND d_master_t.intime <= '" + String.Format("{0:yyyy/MM/dd HH:mm:ss}", this.dateto) + "' ";
                }

                if (this.site_id.HasValue)
                {
                    strQuery += " AND (d_master_t.site_id = " + (int)this.site_id + " OR d_master_t.site_id = " + (int)EnumSiteId.COMMON_SITE_ID + ") ";
                }

                strQuery += ") AS rep ";
                //strQuery += " INNER JOIN cts_login_t ON rep.user_id = cts_login_t.user_id ";
                strQuery += " GROUP BY cts_sname,sname,gcode,scode,stock,price";
                strQuery += " ORDER BY scode";

                    return strQuery;
            }

            private string GetTempOrderQuery()
            {
                string strQuery = "";

                strQuery += "SELECT cts_sname, sname, gcode, scode, price,";
                strQuery += " SUM(tempord) AS tempord, ";
                strQuery += " SUM(cts) AS cts, ";
                strQuery += " SUM(tel) AS tel, ";
                strQuery += " SUM(let) AS let, ";
                strQuery += " SUM(fax) AS fax, ";
                strQuery += " SUM(mb) AS mb, ";
                strQuery += " SUM(pc) AS pc, ";
                strQuery += " SUM(amount) AS totnum, ";
                strQuery += " SUM(amount) * price AS totamt,";
                strQuery += " stock ";
                strQuery += " FROM ( SELECT DISTINCT bask_t.amount, bask_t.price, bask_t.cts_sname, bask_t.sname, ";
                strQuery += "       bask_t.gcode, bask_t.scode, ";
                strQuery += "       CASE WHEN cts_order_t.pm_flg = " + (int)EnumPmFlg.REGULAR + " THEN bask_t.amount ELSE 0 END AS tempord, ";
                strQuery += "       CASE WHEN cts_order_t.pm_flg = " + (int)EnumPmFlg.CTS + " THEN bask_t.amount ELSE 0 END AS cts, ";
                strQuery += "       CASE WHEN cts_order_t.pm_flg = " + (int)EnumPmFlg.MOBILE + " THEN bask_t.amount ELSE 0 END AS mb, ";
                strQuery += "       CASE WHEN cts_order_t.pm_flg = " + (int)EnumPmFlg.PC + " THEN bask_t.amount ELSE 0 END AS pc, ";
                strQuery += "       CASE WHEN cts_order_t.pm_flg = " + (int)EnumPmFlg.SmartPhone + " THEN bask_t.amount ELSE 0 END AS smp, ";
                strQuery += "       CASE WHEN cts_order_t.pm_flg = " + (int)EnumPmFlg.TEL + " THEN bask_t.amount ELSE 0 END AS tel, ";
                strQuery += "       CASE WHEN cts_order_t.pm_flg = " + (int)EnumPmFlg.LETTER + " THEN bask_t.amount ELSE 0 END AS let, ";
                strQuery += "       CASE WHEN cts_order_t.pm_flg = " + (int)EnumPmFlg.FAX + " THEN bask_t.amount ELSE 0 END AS fax, ";
                strQuery += "       stock_t.stock, cts_order_t.intime, cts_login_t.user_id ";
                strQuery += " FROM cts_order_t ";
                strQuery += " INNER JOIN bask_t ON (bask_t.ransu = cts_order_t.ransu AND bask_t.doc_bundling_flg = 0) ";
                strQuery += " LEFT JOIN cts_login_t ON cts_order_t.user_id = (cts_login_t.id || '') ";
                strQuery += " LEFT JOIN stock_t ON bask_t.scode = stock_t.scode";

                strQuery += " WHERE 1 = 1 ";
                if (!string.IsNullOrEmpty(this.prodcode))
                {
                    strQuery += " AND bask_t.scode = '" + this.prodcode + "' ";
                }
                if (!string.IsNullOrEmpty(this.prodname))
                {
                    strQuery += " AND bask_t.cts_sname like '" + this.prodname + "%' ";
                }
                if (!string.IsNullOrEmpty(this.agentid))
                {
                    strQuery += " AND cts_login_t.user_id = '" + this.agentid + "' ";
                }
                if (this.ag_type.HasValue)
                {
                    strQuery += " AND cts_login_t.ag_type = " + (int)this.ag_type + " ";
                }
                if (!this.datefrom.HasValue)
                {
                    strQuery += " AND cts_order_t.intime >= '" + String.Format("{0:yyyy/MM/dd HH:mm:ss}", this.datefrom) + "' ";
                }
                if (!this.dateto.HasValue)
                {
                    strQuery += " AND cts_order_t.intime <= '" + String.Format("{0:yyyy/MM/dd HH:mm:ss}", this.dateto) + "' ";
                }
                if (this.site_id.HasValue)
                {
                    strQuery += " AND (cts_order_t.site_id = " + (int)this.site_id + " OR cts_order_t.site_id = " + (int)EnumSiteId.COMMON_SITE_ID + ") ";
                }

                strQuery += ") AS rep ";
                //strQuery += " INNER JOIN cts_login_t ON rep.user_id = cts_login_t.user_id ";
                strQuery += " GROUP BY cts_sname,sname,gcode,scode,stock,price";
                strQuery += " ORDER BY scode";

                return strQuery;
            }
        }

        internal protected class ErsCtsRepProdTotalPrice
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = @"SELECT SUM(price) AS totalprice FROM
                                (SELECT cts_sname, gcode, scode, SUM(price) AS price, SUM(tempord) AS tempord, SUM(cts) AS cts, SUM(mb) AS mb,
                                        SUM(pc) AS pc, SUM(tempord + mb) AS totnum, SUM(price) AS totamt, stock, rep.user_id, ag_type, rep.intime
                                   FROM (SELECT DISTINCT ds_master_t.amount, ds_master_t.price, ds_master_t.cts_sname, ds_master_t.gcode, ds_master_t.scode,d_master_t.site_id, 
                                                        CASE WHEN d_master_t.pm_flg = 3 THEN ds_master_t.amount ELSE 0 END AS tempord,
                                                        CASE WHEN d_master_t.pm_flg = 2 THEN ds_master_t.amount ELSE 0 END AS cts,
                                                        CASE WHEN d_master_t.pm_flg = 1 THEN ds_master_t.amount ELSE 0 END AS mb,
                                                        CASE WHEN d_master_t.pm_flg = 0 THEN ds_master_t.amount ELSE 0 END AS pc,
                                                        stock_t.stock, d_master_t.intime, cts_login_t.user_id 
                                    FROM d_master_t
                                    INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no
                                    LEFT JOIN cts_login_t ON d_master_t.user_id = (cts_login_t.id || '') 
                                    LEFT JOIN stock_t ON ds_master_t.scode = stock_t.scode) AS rep LEFT JOIN cts_login_t ON rep.user_id=cts_login_t.user_id
                                    GROUP BY cts_sname,gcode,scode,stock,rep.user_id, ag_type, rep.intime) AS repprod";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepProdTotalNumber
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = @"SELECT COUNT(cts_sname) as totalnumber FROM
                                (SELECT cts_sname, gcode, scode, SUM(price) AS price, SUM(tempord) AS tempord, SUM(cts) AS cts, SUM(mb) AS mb,
                                        SUM(pc) AS pc, SUM(tempord + mb) AS totnum, SUM(price) AS totamt, stock, rep.user_id, ag_type, rep.intime
                                   FROM (SELECT DISTINCT ds_master_t.amount, ds_master_t.price, ds_master_t.cts_sname, ds_master_t.gcode, ds_master_t.scode,d_master_t.site_id, 
                                                        CASE WHEN d_master_t.pm_flg = 3 THEN ds_master_t.amount ELSE 0 END AS tempord,
                                                        CASE WHEN d_master_t.pm_flg = 2 THEN ds_master_t.amount ELSE 0 END AS cts,
                                                        CASE WHEN d_master_t.pm_flg = 1 THEN ds_master_t.amount ELSE 0 END AS mb,
                                                        CASE WHEN d_master_t.pm_flg = 0 THEN ds_master_t.amount ELSE 0 END AS pc,
                                                        stock_t.stock, d_master_t.intime, cts_login_t.user_id
                                    FROM d_master_t
                                    INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no
                                    LEFT JOIN cts_login_t ON d_master_t.user_id = (cts_login_t.id || '') 
                                    LEFT JOIN stock_t ON ds_master_t.scode = stock_t.scode) AS rep LEFT JOIN cts_login_t ON rep.user_id=cts_login_t.user_id
                                    GROUP BY cts_sname,gcode,scode,stock,rep.user_id, ag_type, rep.intime) AS repprod";
                return strQuery;
            }
        }
        internal protected class ErsCtsRepProdCountSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                return " SELECT DISTINCT COUNT(mcode) AS count FROM (SELECT mcode, lname, fname, lnamek, fnamek, zip, pref, pref_name, address, email, tel, fax, 1 as active, 1 as src, site_id FROM member_t LEFT JOIN pref_t on member_t.pref = pref_t.id UNION SELECT mcode, lname, fname, lnamek, fnamek, '' as zip, 0 as pref, '' as pref_name, '' as address, '' as email, tel, '' as fax, active,  2 as src, site_id FROM cts_order_t) as searchData ";
            }
        }

    }
}
