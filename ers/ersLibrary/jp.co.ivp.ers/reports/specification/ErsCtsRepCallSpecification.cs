﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
 
namespace jp.co.ivp.ers.reports.specification
{
    [Obsolete("分割できるか、、")]
    public class ErsCtsRepCallSpecification
    {

        #region Time
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            // Total Per User
            var specificationForSQL = new ErsCtsRepCallRecordSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTotalTimeData(Criteria criteria)
        {
            // Total Time
            var specificationForSQL = new ErsCtsRepCallRecordTotalTimeSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTotalUserTimeData(Criteria criteria)
        {
            // Total Per User Per Time
            var specificationForSQL = new ErsCtsRepCallRecordTotalUserTimeSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetIntervalData(Criteria criteria)
        {

            var specificationForSQL = new ErsCtsRepCallRecordInterval();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTempTotalUserTimeData(Criteria criteria)
        {
            // TEMP Total Per User Per Time
            var specificationForSQL = new ErsCtsRepCallRecordTempTotalUserTimeSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTempTotalTimeData(Criteria criteria)
        {
            // TEMP Total Per Time
            var specificationForSQL = new ErsCtsRepCallRecordTempTotalTimeSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTempTotalUserData(Criteria criteria)
        {
            // TEMP Total Per User
            var specificationForSQL = new ErsCtsRepCallRecordTempSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        #endregion

        #region Day
        public virtual List<Dictionary<string, object>> GetSearchDataDay(Criteria criteria)
        {
            // Total Per User
            var specificationForSQL = new ErsCtsRepCallRecordDaySpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTotalTimeDataDay(Criteria criteria)
        {
            // Total Time
            var specificationForSQL = new ErsCtsRepCallRecordTotalDaySpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTotalUserTimeDataDay(Criteria criteria)
        {
            // Total Per User Per Time
            var specificationForSQL = new ErsCtsRepCallRecordTotalUserDaySpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTempTotalUserTimeDataDay(Criteria criteria)
        {
            // TEMP Total Per User Per Time
            var specificationForSQL = new ErsCtsRepCallRecordTempTotalUserDaySpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTempTotalTimeDataDay(Criteria criteria)
        {
            // TEMP Total Per Time
            var specificationForSQL = new ErsCtsRepCallRecordTempTotalDaySpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTempTotalUserDataDay(Criteria criteria)
        {
            // TEMP Total Per User
            var specificationForSQL = new ErsCtsRepCallRecordDayTempSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        #endregion
        #region Month
        public virtual List<Dictionary<string, object>> GetSearchDataMonth(Criteria criteria)
        {
            // Total Per User
            var specificationForSQL = new ErsCtsRepCallRecordMonthSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTotalTimeDataMonth(Criteria criteria)
        {
            // Total Time
            var specificationForSQL = new ErsCtsRepCallRecordTotalMonthSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTotalUserTimeDataMonth(Criteria criteria)
        {
            // Total Per User Per Time
            var specificationForSQL = new ErsCtsRepCallRecordTotalUserMonthSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTempTotalUserTimeDataMonth(Criteria criteria)
        {
            // TEMP Total Per User Per Time
            var specificationForSQL = new ErsCtsRepCallRecordTempTotalUserMonthSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTempTotalTimeDataMonth(Criteria criteria)
        {
            // TEMP Total Per Time
            var specificationForSQL = new ErsCtsRepCallRecordTempTotalMonthSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetTempTotalUserDataMonth(Criteria criteria)
        {
            // TEMP Total Per User
            var specificationForSQL = new ErsCtsRepCallRecordMonthTempSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        #endregion

        #region Time
        internal protected class ErsCtsRepCallRecordSpecification
    : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = String.Format(@"SELECT user_id, ag_name, sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, ag_name, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                 FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, cts_login_t.ag_name, d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(hour from d_master_t.intime), '00dS')) || ':' || 
			                (CASE WHEN EXTRACT(minute from d_master_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END) as 				recordtime, d_master_t.intime
			                FROM d_master_t join cts_login_t
			                ON d_master_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, ag_name, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("d_master_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTotalUserTimeSpecification
    : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Per User Per Time
                string strQuery = String.Format(@"
                                SELECT DISTINCT(byhalfhour), user_id, sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                    FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(hour from d_master_t.intime), '00dS')) || ':' || 
			                (CASE WHEN EXTRACT(minute from d_master_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END) as 				recordtime, d_master_t.intime
			                FROM d_master_t join cts_login_t
			                ON d_master_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("d_master_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTotalTimeSpecification
    : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Time
                //                string strQuery = String.Format(@"SELECT count(d_no) as d_no_count, 
                //                (EXTRACT(hour from d_master_t.intime) || ':' || 
                //                (CASE WHEN EXTRACT(minute from d_master_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END)) AS recordtime FROM d_master_t";
                string strQuery = String.Format(@"
                SELECT DISTINCT(byhalfhour), sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                 FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(hour from d_master_t.intime), '00dS')) || ':' || 
			                (CASE WHEN EXTRACT(minute from d_master_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END) as 				recordtime, d_master_t.intime
			                FROM d_master_t join cts_login_t
			                ON d_master_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("d_master_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordInterval
    : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Per User Per Time
                string strQuery =@"SELECT byhalfhour FROM (SELECT DISTINCT scale_value AS byhalfhour, scale_code FROM s_scale) as t ";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTempTotalUserTimeSpecification
    : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Per User Per Time
                string strQuery = String.Format(@"SELECT DISTINCT(byhalfhour), user_id, sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                 FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, temp_d_no as d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(hour from cts_order_t.intime), '00dS')) || ':' || 
			                (CASE WHEN EXTRACT(minute from cts_order_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END) as 				recordtime, cts_order_t.intime
			                FROM cts_order_t join cts_login_t
			                ON cts_order_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("cts_order_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTempTotalTimeSpecification
    : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Time
                //                string strQuery = String.Format(@"SELECT count(d_no) as d_no_count, 
                //                (EXTRACT(hour from d_master_t.intime) || ':' || 
                //                (CASE WHEN EXTRACT(minute from d_master_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END)) AS recordtime FROM d_master_t";
                string strQuery = String.Format(@"SELECT DISTINCT(byhalfhour), sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour,  s_scale.scale_code, user_id, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                 FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, temp_d_no as d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(hour from cts_order_t.intime), '00dS')) || ':' || 
			                (CASE WHEN EXTRACT(minute from cts_order_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END) as 				recordtime, cts_order_t.intime
			                FROM cts_order_t join cts_login_t
			                ON cts_order_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("cts_order_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTempSpecification
      : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = String.Format(@"SELECT user_id, ag_name, sum(d_no_count) as d_no_count
            FROM (
	            SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, ag_name, intime, ag_type,
		            (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		             FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			            (SELECT cts_login_t.user_id, cts_login_t.ag_name, temp_d_no as d_no, cts_login_t.ag_type,
			            (to_char(EXTRACT(hour from cts_order_t.intime), '00dS')) || ':' || 
			            (CASE WHEN EXTRACT(minute from cts_order_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END) as 				recordtime, cts_order_t.intime
			            FROM cts_order_t join cts_login_t
			            ON cts_order_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	            GROUP BY user_id, ag_name, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("cts_order_t"));
                return strQuery;
            }
        }
        
        #endregion

        #region Day
        internal protected class ErsCtsRepCallRecordDaySpecification
: ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = String.Format(@"SELECT user_id, ag_name, sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, ag_name, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                 FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, cts_login_t.ag_name, d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(day from d_master_t.intime), '00日ds')) as recordtime, d_master_t.intime
			                FROM d_master_t join cts_login_t
			                ON d_master_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, ag_name, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("d_master_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTotalUserDaySpecification
: ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Per User Per Time
                string strQuery = String.Format(@"
                                SELECT DISTINCT(byhalfhour), user_id, sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                    FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(day from d_master_t.intime), '00日ds')) as recordtime, d_master_t.intime
			                FROM d_master_t join cts_login_t
			                ON d_master_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("d_master_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTotalDaySpecification
    : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Time
                //                string strQuery = String.Format(@"SELECT count(d_no) as d_no_count, 
                //                (EXTRACT(hour from d_master_t.intime) || ':' || 
                //                (CASE WHEN EXTRACT(minute from d_master_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END)) AS recordtime FROM d_master_t";
                string strQuery = String.Format(@"
                SELECT DISTINCT(byhalfhour), sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                 FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(day from d_master_t.intime), '00日ds')) as recordtime, d_master_t.intime
			                FROM d_master_t join cts_login_t
			                ON d_master_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("d_master_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTempTotalUserDaySpecification
: ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Per User Per Time
                string strQuery = String.Format(@"SELECT DISTINCT(byhalfhour), user_id, sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                 FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, temp_d_no as d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(day from cts_order_t.intime), '00日ds')) as recordtime, cts_order_t.intime
			                FROM cts_order_t join cts_login_t
			                ON cts_order_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("cts_order_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTempTotalDaySpecification
: ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Time
                //                string strQuery = String.Format(@"SELECT count(d_no) as d_no_count, 
                //                (EXTRACT(hour from d_master_t.intime) || ':' || 
                //                (CASE WHEN EXTRACT(minute from d_master_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END)) AS recordtime FROM d_master_t";
                string strQuery = String.Format(@"SELECT DISTINCT(byhalfhour), sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour,  s_scale.scale_code, user_id, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                 FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, temp_d_no as d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(day from cts_order_t.intime), '00日ds')) as recordtime, cts_order_t.intime
			                FROM cts_order_t join cts_login_t
			                ON cts_order_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("cts_order_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordDayTempSpecification
: ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = String.Format(@"SELECT user_id, ag_name, sum(d_no_count) as d_no_count
            FROM (
	            SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, ag_name, intime, ag_type,
		            (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		             FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			            (SELECT cts_login_t.user_id, cts_login_t.ag_name, temp_d_no as d_no, cts_login_t.ag_type,
			            (to_char(EXTRACT(day from cts_order_t.intime), '00日ds')) as recordtime, cts_order_t.intime
			            FROM cts_order_t join cts_login_t
			            ON cts_order_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	            GROUP BY user_id, ag_name, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("cts_order_t"));
                return strQuery;
            }
        } 
        #endregion

        #region Month
        internal protected class ErsCtsRepCallRecordMonthSpecification
: ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = String.Format(@"SELECT user_id, ag_name, sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, ag_name, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                 FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, cts_login_t.ag_name, d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(month from d_master_t.intime), '00月ds')) as recordtime, d_master_t.intime
			                FROM d_master_t join cts_login_t
			                ON d_master_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, ag_name, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("d_master_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTotalUserMonthSpecification
: ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Per User Per Time
                string strQuery = String.Format(@"
                                SELECT DISTINCT(byhalfhour), user_id, sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                    FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(month from d_master_t.intime), '00月ds')) as recordtime, d_master_t.intime
			                FROM d_master_t join cts_login_t
			                ON d_master_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("d_master_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTotalMonthSpecification
    : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Time
                //                string strQuery = String.Format(@"SELECT count(d_no) as d_no_count, 
                //                (EXTRACT(hour from d_master_t.intime) || ':' || 
                //                (CASE WHEN EXTRACT(minute from d_master_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END)) AS recordtime FROM d_master_t";
                string strQuery = String.Format(@"
                SELECT DISTINCT(byhalfhour), sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                 FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(month from d_master_t.intime), '00月ds')) as recordtime, d_master_t.intime
			                FROM d_master_t join cts_login_t
			                ON d_master_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("d_master_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTempTotalUserMonthSpecification
: ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Per User Per Time
                string strQuery = String.Format(@"SELECT DISTINCT(byhalfhour), user_id, sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                 FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, temp_d_no as d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(month from cts_order_t.intime), '00月ds')) as recordtime, cts_order_t.intime
			                FROM cts_order_t join cts_login_t
			                ON cts_order_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("cts_order_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordTempTotalMonthSpecification
: ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                // Total Time
                //                string strQuery = String.Format(@"SELECT count(d_no) as d_no_count, 
                //                (EXTRACT(hour from d_master_t.intime) || ':' || 
                //                (CASE WHEN EXTRACT(minute from d_master_t.intime)>=30 THEN '30' ELSE substr(to_char(00,'00'), 2,3) END)) AS recordtime FROM d_master_t";
                string strQuery = String.Format(@"SELECT DISTINCT(byhalfhour), sum(d_no_count) as d_no_count
                FROM (
	                SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour,  s_scale.scale_code, user_id, intime, ag_type,
		                (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		                 FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			                (SELECT cts_login_t.user_id, temp_d_no as d_no, cts_login_t.ag_type,
			                (to_char(EXTRACT(month from cts_order_t.intime), '00月ds')) as recordtime, cts_order_t.intime
			                FROM cts_order_t join cts_login_t
			                ON cts_order_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	                GROUP BY user_id, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("cts_order_t"));
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCallRecordMonthTempSpecification
: ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = String.Format(@"SELECT user_id, ag_name, sum(d_no_count) as d_no_count
            FROM (
	            SELECT  DISTINCT(s_scale.scale_value) AS byhalfhour, s_scale.scale_code, user_id, ag_name, intime, ag_type,
		            (CASE WHEN recordtime!=s_scale.scale_value THEN '0' ELSE count(d_no) END) as d_no_count 
		             FROM (SELECT DISTINCT * FROM s_scale) AS s_scale CROSS JOIN
			            (SELECT cts_login_t.user_id, cts_login_t.ag_name, temp_d_no as d_no, cts_login_t.ag_type,
			            (to_char(EXTRACT(month from cts_order_t.intime), '00月ds')) as recordtime, cts_order_t.intime
			            FROM cts_order_t join cts_login_t
			            ON cts_order_t.user_id = (cts_login_t.id || '') {0}) AS usertime
	            GROUP BY user_id, ag_name, recordtime, byhalfhour, intime, ag_type, scale_code order by d_no_count) as t ", site_id_condition_sql("cts_order_t"));
                return strQuery;
            }
        }
        #endregion

        /// <summary>
        /// Returns sql condition for the site_id
        /// </summary>
        /// <param name="table">table name for site_id condition</param>
        /// <returns></returns>
        internal static string site_id_condition_sql(string table)
        {
            int? site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            if (site_id.HasValue)
            {
                return string.Format(" AND ({0}.site_id = {1} OR {0}.site_id = {2}) ", table, site_id, (int)EnumSiteId.COMMON_SITE_ID);
            }
            return string.Empty;
        }
    }
}