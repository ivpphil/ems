﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports.specification
{
    public class ErsCtsRepAgeSpecification
    {
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria, string where, string whereTemp)
        {
            var specificationForSQL = new ErsCtsRepAgeRecordSpecification(where, whereTemp);

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        /// <summary>
        /// 伝票数取得
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public virtual int GetDCount(Criteria criteria, string where, string whereTemp)
        {
            var specificationForSQL = new ErsCtsRepAllRecordSpecification(where, whereTemp);

            var record = ErsRepository.SelectSatisfying(specificationForSQL, criteria);
            if (record.Count == 0)
                return 0;

            return Convert.ToInt32(record[0]["dno_count"]);
        }


        internal protected class ErsCtsRepAgeRecordSpecification
            : ISpecificationForSQL
        {
            public string where;
            public string whereTemp;

            public ErsCtsRepAgeRecordSpecification(string where, string whereTemp)
            {
                this.where = where;
                this.whereTemp = whereTemp;
            }

            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = @"
                    SELECT * FROM
                    (
                        SELECT
                            code AS age_code,
                            namename,
                            " + this.GetSexCount(EnumSex.Male, this.where, this.whereTemp) + @" AS mcount,
                            " + this.GetSexCount(EnumSex.Female, this.where, this.whereTemp) + @" AS fcount
                        FROM
                        (
                            SELECT
                                * 
                            FROM
                                (SELECT code, namename FROM common_namecode_t WHERE type_code = 'ORDAGE' ORDER BY disp_order, code) AS ORDAGE
                        ) AS BASE
                        ORDER BY CASE code WHEN 0 THEN 1000 ELSE code END
                    ) AS member_t
                    WHERE true";

                return strQuery;
            }

            private string GetSexCount(EnumSex sex, string where, string whereTemp)
            {
                return @" 
                            COALESCE((
                                SELECT
                                    COUNT(mcode)
                                FROM
                                    (
                                        SELECT
                                            COALESCE((
                                            SELECT code FROM common_namecode_t WHERE type_code = 'ORDAGE' AND extract(year from age(current_date,member_t.birth)) BETWEEN opt_num1 AND opt_num2 AND active = 1
                                            ), 0) as auto_age_code, *
                                        FROM member_t
                                    )member_t
                                WHERE 
                                    sex = " + (int)sex + @"
                                    AND BASE.code = member_t.auto_age_code
                                    AND (EXISTS(SELECT 1 FROM d_master_t LEFT JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no WHERE mcode = member_t.mcode " + where + @")
                                    OR EXISTS(SELECT 1 FROM cts_order_t LEFT JOIN bask_t ON cts_order_t.ransu = bask_t.ransu WHERE mcode = member_t.mcode " + whereTemp + @"))
                            ), 0)";
            }
        }

        internal protected class ErsCtsRepAllRecordSpecification
        : ISpecificationForSQL
        {
            public string where;
            public string whereTemp;

            public ErsCtsRepAllRecordSpecification(string where, string whereTemp)
            {
                this.where = where;
                this.whereTemp = whereTemp;
            }

            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = @"SELECT COUNT((d_no)) as dno_count  
                FROM(
                SELECT d_master_t.d_no ,d_master_t.intime, member_t.sex FROM member_t INNER JOIN d_master_t ON d_master_t.mcode = member_t.mcode INNER JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no "
                + where
                + " UNION SELECT cts_order_t.temp_d_no:: character varying as d_no,cts_order_t.intime, member_t.sex FROM member_t INNER JOIN cts_order_t ON cts_order_t.mcode = member_t.mcode LEFT JOIN bask_t ON cts_order_t.ransu = bask_t.ransu "
                + whereTemp
                + " ) as allcount WHERE sex IN (" + (int)EnumSex.Male + ", " + (int)EnumSex.Female + ")";

                return strQuery;
            }
        }
    }
}
