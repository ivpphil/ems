﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports.specification
{
    [Obsolete("分割する")]
    public class ErsCtsRepInboundSpecification
        :ISearchSpecification
    {
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {

            var specificationForSQL = new ErsCtsRepInboundTypeSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetProgress(Criteria criteria)
        {

            var specificationForSQL = new ErsCtsRepInboundProgressSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetSituation(Criteria criteria)
        {

            var specificationForSQL = new ErsCtsRepInboundSituationSpecification();

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        internal protected class ErsCtsRepInboundTypeSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = @"SELECT cts_enquiry_t.enq_type, common_namecode_t.namename, COUNT(cts_enquiry_t.enq_type) as enq_type_count 
                                      FROM cts_enquiry_t 
                                     INNER JOIN common_namecode_t ON cts_enquiry_t.enq_type = common_namecode_t.code
                                     LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id ";
                
                return strQuery;
            }
        }

        internal protected class ErsCtsRepInboundProgressSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = @"SELECT cts_enquiry_t.enq_type, CASE WHEN common_namecode_t.code = 1 THEN 'Open' WHEN common_namecode_t.code = 2 THEN 'Close' END AS namename, COUNT(cts_enquiry_t.enq_progress) AS enq_progress_count 
                                      FROM cts_enquiry_t INNER JOIN common_namecode_t ON cts_enquiry_t.enq_progress = common_namecode_t.code
                                     LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id ";
                return strQuery;
            }
        }


        internal protected class ErsCtsRepInboundSituationSpecification
        : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = @"SELECT cts_enquiry_t.enq_type, namename, COUNT(enq_situation) as enq_situation_count, cts_enquiry_t.enq_situation
                                      FROM cts_enquiry_t INNER JOIN common_namecode_t ON cts_enquiry_t.enq_situation = common_namecode_t.code
                                     LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id ";
                return strQuery;
            }
        }
        public virtual int GetCountData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepInboundCountSpecification();

            var record = ErsRepository.SelectSatisfying(specificationForSQL, criteria);
            if (record.Count == 0)
                return 0;

            return Convert.ToInt32(record[0]["count"]);
        }
        internal protected class ErsCtsRepInboundCountSpecification
         : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                return @"SELECT COUNT(cts_enquiry_t.id)  FROM cts_enquiry_t 
                        LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id ";
            }
        }

        public virtual List<Dictionary<string, object>> GetListData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepInboundListSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        internal protected class ErsCtsRepInboundListSpecification
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                var site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
                var addWhere = " where e.site_id = " + site_id + " ";

                string strQuery = @"SELECT cts_enquiry_t.case_no, 
                                           cts_enquiry_t.intime, 
                                           cts_enquiry_t.utime, 
                                           member_t.mcode, 
                                           member_t.lname, 
                                           member_t.fname, 
                                           cts_enquiry_t.enq_casename, 
                                           cts_login_t.ag_name,
                                           cts_login_t.ag_type,
                                           cts_agetype_t.agent,
                                           cts_enquiry_t.enq_type,

                                           (SELECT e.namename from common_namecode_t e inner join cts_enquiry_t f on e.code = f.enq_status and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQSTS') as w_status,
                                           (SELECT e.namename from common_namecode_t e inner join cts_enquiry_t f on e.code = f.enq_situation and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQSIT') as w_situation,
                                           (SELECT e.namename from common_namecode_t e inner join cts_enquiry_t f on e.code = f.enq_progress and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQPGR') as w_progress,
                                           (SELECT e.namename from common_namecode_t e inner join cts_enquiry_t f on e.code = f.enq_priorty and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQPRY') as w_priority,

                                           (SELECT e.namename from cts_enquiry_category_t e inner join cts_enquiry_t f on e.code = f.cate1 and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQCT1' " + addWhere + @") as cate1,
                                           (SELECT e.namename from cts_enquiry_category_t e inner join cts_enquiry_t f on e.code = f.cate2 and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQCT2' " + addWhere + @") as cate2,
                                           (SELECT e.namename from cts_enquiry_category_t e inner join cts_enquiry_t f on e.code = f.cate3 and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQCT3' " + addWhere + @") as cate3,
                                           (SELECT e.namename from cts_enquiry_category_t e inner join cts_enquiry_t f on e.code = f.cate4 and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQCT4' " + addWhere + @") as cate4,
                                           (SELECT e.namename from cts_enquiry_category_t e inner join cts_enquiry_t f on e.code = f.cate5 and f.case_no = cts_enquiry_t.case_no and e.type_code = 'ENQCT5' " + addWhere + @") as cate5

                                      FROM cts_enquiry_t LEFT JOIN member_t ON cts_enquiry_t.mcode = member_t.mcode
                                      LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id 
                                      LEFT JOIN cts_agetype_t on cts_login_t.ag_type = cts_agetype_t.id";
                                      
                return strQuery;
            }
        }
    }
}
