﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports.specification
{
    [Obsolete("分割する")]
    public class ErsCtsRepCategorySpecification
        : ISearchSpecification
    {
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepCategorySelectParent();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetChild(Criteria criteria, int? site_id = null)
        {
            var specificationForSQL = new ErsCtsRepCategorySelectChild();
            specificationForSQL.site_id = site_id;
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetRowSpan(Criteria criteria, int? site_id = null)
        {
            var specificationForSQL = new ErsCtsRepCategoryCountRowSpan();
            specificationForSQL.site_id = site_id;
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetChildCount(Criteria criteria, string where)
        {
            var specificationForSQL = new ErsCtsRepCategoryChildCount(where);
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetParentCount(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepCategoryParentCount();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual int GetCountData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepCategoryCountSpecification();

            var record = ErsRepository.SelectSatisfying(specificationForSQL, criteria);
            if (record.Count == 0)
                return 0;

            return Convert.ToInt32(record[0]["count"]);
        }
        public virtual List<Dictionary<string, object>> GetCategoryList(Criteria criteria, int? site_id = null)
        {
            var specificationForSQL = new ErsCtsRepCategoryListSpecification();
            specificationForSQL.site_id = site_id;
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetDetailData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepCategoryDetailSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }
        public virtual List<Dictionary<string, object>> GetBillData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepCategoryBillDetail();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        internal protected class ErsCtsRepCategorySelectParent
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = @"SELECT * FROM (SELECT type_code, namename, code, active, site_id from cts_enquiry_category_t) AS p";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCategorySelectChild
            : ISpecificationForSQL
        {
            public int? site_id { get; set; }

            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string whereQuery = "";

                if (this.site_id.HasValue)
                {
                    whereQuery = "where (children.site_id = " + site_id + " OR children.site_id = 0) ";
                }

                string strQuery = @"SELECT * FROM (select parent.type_code as parent_type_code, parent.code as parent_code, parent.namename as parent_namename , parent.active as parent_active,parent.site_id, 
                substring(children.type_code from 6 for 1) as child_code,
                children.code, children.namename, children.disp_order, children.active
                from cts_enquiry_category_t parent 
                inner join cts_enquiry_category_t children
                on to_char(parent.code,'9') = ' ' || substring(children.type_code from 6 for 1) " + whereQuery + ") as c";


                return strQuery;
            }
        }

        internal protected class ErsCtsRepCategoryCountRowSpan
            : ISpecificationForSQL
        {
            public int? site_id { get; set; }
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string whereQuery = "";

                if (this.site_id.HasValue)
                {
                    whereQuery = "where (children.site_id = " + site_id + " OR children.site_id = 0) ";
                }
                string strQuery = @"SELECT child_code, COUNT(child_code)+1 as rowcount FROM (select parent.type_code as parent_type_code, parent.code as parent_code, parent.namename as parent_namename , parent.site_id, 
                    substring(children.type_code from 6 for 1) as child_code,
                    children.code, children.namename
                    from cts_enquiry_category_t parent 
                    inner join cts_enquiry_category_t children
                    on to_char(parent.code,'9') = ' ' || substring(children.type_code from 6 for 1) " + whereQuery + ") as c";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCategoryCountSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL? Must be same query as in ErsCtsRepCategoryListSpecification to match the page count list
            /// </summary>
            /// <returns>SQL?</returns>
            public virtual string asSQL()
            {
                //string strQuery = "SELECT COUNT(cts_enquiry_t.id)  FROM cts_enquiry_t";
                //return strQuery;

                
                string strQuery = @"SELECT COUNT(cts_enquiry_t.case_no)
                                FROM cts_enquiry_t INNER JOIN member_t
                                ON cts_enquiry_t.mcode = member_t.mcode
                                LEFT JOIN (SELECT common_namecode_t.namename, common_namecode_t.code
                                FROM common_namecode_t where common_namecode_t.type_code = 'ENQSTS') as status
                                ON status.code = cts_enquiry_t.enq_status
                                LEFT JOIN (SELECT common_namecode_t.namename, common_namecode_t.code
                                FROM common_namecode_t where common_namecode_t.type_code = 'ENQPGR') as progress
                                ON progress.code = cts_enquiry_t.enq_progress
                                LEFT JOIN (SELECT common_namecode_t.namename, common_namecode_t.code
                                FROM common_namecode_t where common_namecode_t.type_code = 'ENQSIT') as situation
                                ON situation.code = cts_enquiry_t.enq_situation
                                LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id";
                return strQuery;
            }
        }
        
        internal protected class ErsCtsRepCategoryChildCount
           : ISpecificationForSQL
        {
            /// <summary>
            /// SQL?
            /// </summary>
            /// <returns>SQL?</returns>
            /// 
            public string where;

            public ErsCtsRepCategoryChildCount(string where)
            {
                if (!string.IsNullOrEmpty(where))
                    this.where = where;
                else
                    this.where = "";
            }

            public virtual string asSQL()
            {
                string strQuery = @"select p.type_code as parent_type_code, p.code as parent_code, p.namename as parent_namename, 
                    c.c_tcode as type_code, c.c_code as code, c.c_tcname as namename,
                    case 
                    when p.code = '1' then c.cate1
                    when p.code = '2' then c.cate2
                    when p.code = '3' then c.cate3
                    when p.code = '4' then c.cate4
                    when p.code = '5' then c.cate5
                    end as childcount
                    from
                    (select 
                    c.type_code as c_tcode,
                    c.code as c_code,
                    c.namename as c_tcname,
                    case when r.cate1 is null then 0 else r.cate1 end,
                    case when r.cate2 is null then 0 else r.cate2 end,
                    case when r.cate3 is null then 0 else r.cate3 end,
                    case when r.cate4 is null then 0 else r.cate4 end,
                    case when r.cate5 is null then 0 else r.cate5 end
                    from (select type_code, code, namename from cts_enquiry_category_t
                    union (select 'ENQCT1' as type_code, 0 as code, '未設定' as namename)
                    union (select 'ENQCT2' as type_code, 0 as code, '未設定' as namename)
                    union (select 'ENQCT3' as type_code, 0 as code, '未設定' as namename)
                    union (select 'ENQCT4' as type_code, 0 as code, '未設定' as namename)
                    union (select 'ENQCT5' as type_code, 0 as code, '未設定' as namename)
                    ) as c
                    full outer join (
                    (select 'ENQCT1' as c_tcode, cate1 as c_code,
                    cnt as cate1, 0 as cate2, 0 as cate3, 0 as cate4, 0 as cate5
                    from (select count(*) as cnt, cate1 from cts_enquiry_t e " +
                    where + @"
                    group by cate1) as c1
                    left join cts_enquiry_category_t c on c.code = c1.cate1 and c.type_code = 'ENQCT1'
                    )
                    union (select 'ENQCT2' as c_tcode, cate2 as c_code,
                    0 as cate1, cnt as cate2, 0 as cate3, 0 as cate4, 0 as cate5
                    from (select count(*) as cnt, cate2 from cts_enquiry_t e " +
                    where + @"
                    group by cate2) as c2
                    left join cts_enquiry_category_t c on c.code = c2.cate2 and c.type_code = 'ENQCT2'
                    )
                    union (select 'ENQCT3' as c_tcode, cate3 as c_code,
                    0 as cate1, 0 as cate2, cnt as cate3, 0 as cate4, 0 as cate5
                    from (select count(*) as cnt, cate3 from cts_enquiry_t e " +
                    where + @"
                    group by cate3) as c3
                    left join cts_enquiry_category_t c on c.code = c3.cate3 and c.type_code = 'ENQCT3'
                    )
                    union (select 'ENQCT4' as c_tcode, cate4 as c_code,
                    0 as cate1, 0 as cate2, 0 as cate3, cnt as cate4, 0 as cate5
                    from (select count(*) as cnt, cate4 from cts_enquiry_t e " +
                    where + @"
                    group by cate4) as c4
                    left join cts_enquiry_category_t c on c.code = c4.cate4 and c.type_code = 'ENQCT4'
                    )
                    union (select 'ENQCT5' as c_tcode, cate5 as c_code,
                    0 as cate1, 0 as cate2, 0 as cate3, 0 as cate4, cnt as cate5
                    from (select count(*) as cnt, cate5 from cts_enquiry_t e " +
                    where + @"
                    group by cate5) as c5
                    left join cts_enquiry_category_t c on c.code = c5.cate5 and c.type_code = 'ENQCT5'
                    )
                    ) as r
                    on c.code = r.c_code and c.type_code = r.c_tcode
                    where (c.type_code != 'ENQCT_NAME' or r.c_tcode != 'ENQCT_NAME') and (substring(c.type_code from 1 for 5) = 'ENQCT' or substring(r.c_tcode from 1 for 5) = 'ENQCT')
                    ) as c
                    left join cts_enquiry_category_t p on cast(substring(c.c_tcode from 6 for 1) as integer) = p.code and p.type_code = 'ENQCT_NAME'";

                return strQuery;
            }
        }

        internal protected class ErsCtsRepCategoryParentCount
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = @"SELECT parent_code, sum(childcount) as parentcount FROM(
                    select 
                     report.p_tcode as parent_type_code, report.p_code as parent_code, report.p_tcname as parent_namename, report.c_tcode as type_code, report.c_code as code, report.c_tcname as namename,
                     case 
                      when report.p_code = '1' then report.cate1
                      when report.p_code = '2' then report.cate2
                      when report.p_code = '3' then report.cate3
                      when report.p_code = '4' then report.cate4
                      when report.p_code = '5' then report.cate5
                     end as childcount
                    from
                     (
                     select 
                      parent.type_code as p_tcode, 
                      parent.code as p_code, 
                      parent.namename as p_tcname, 
                      children.type_code as c_tcode, 
                      children.code as c_code, 
                      children.namename as c_tcname,
                      (select count(*) from cts_enquiry_t where cate1 = children.code and children.type_code = 'ENQCT1' and cate1>1) cate1,
                      (select count(*) from cts_enquiry_t where cate2 = children.code and children.type_code = 'ENQCT2' and cate2>1) cate2,
                      (select count(*) from cts_enquiry_t where cate3 = children.code and children.type_code = 'ENQCT3' and cate3>1) cate3,
                      (select count(*) from cts_enquiry_t where cate4 = children.code and children.type_code = 'ENQCT4' and cate4>1) cate4,
                      (select count(*) from cts_enquiry_t where cate5 = children.code and children.type_code = 'ENQCT5' and cate5>1) cate5
                     from cts_enquiry_category_t parent 
                     inner join cts_enquiry_category_t children
                     on to_char(parent.code,'9') = ' ' || substring(children.type_code from 6 for 1)
                     where parent.type_code = 'ENQCT_NAME') report
                    order by p_code, c_code) as parent";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCategoryListSpecification
            :ISpecificationForSQL
        {
            public int? site_id { get; set; }

            public virtual string asSQL()
            {
                string whereQuery = "";

                if(this.site_id.HasValue)
                {
                    whereQuery = "and (site_id = " + site_id + " or site_id = 0 )";
                }

                string strQuery = @"SELECT cts_enquiry_t.case_no, cts_enquiry_t.intime, member_t.lname, member_t.fname, status.namename as w_status, situation.namename as w_situation, cts_enquiry_t.enq_casename, cts_login_t.ag_name, member_t.pref,cts_enquiry_t.enq_progress,
                                (Select namename from cts_enquiry_category_t where type_code = 'ENQCT1' and code = cts_enquiry_t.cate1 " + whereQuery + ")cate1, " +
					            "(Select namename from cts_enquiry_category_t where type_code = 'ENQCT2' and code = cts_enquiry_t.cate2 " + whereQuery + ")cate2, " +
					            "(Select namename from cts_enquiry_category_t where type_code = 'ENQCT3' and code = cts_enquiry_t.cate3 " + whereQuery + ")cate3, " +
					            "(Select namename from cts_enquiry_category_t where type_code = 'ENQCT4' and code = cts_enquiry_t.cate4 " + whereQuery + ")cate4, " +    
                                "(Select CASE WHEN enq_corresponding = 0 " + 
                                "THEN '\n【問】' || enq_detail || '\n【答】' || ans_detail " + 
                                "WHEN enq_corresponding = 1 " + 
                                "THEN '\n【ヘッダー】' || email_header || '\n【本文】' || email_body || '\n【フッター】' || email_fotter " + 
                                "WHEN enq_corresponding = 2 " + 
                                "THEN memo " +
                                "END AS content " + 
                                "FROM cts_enquiry_detail_t where case_no = cts_enquiry_t.case_no limit 1)inquiry " +

                                "FROM cts_enquiry_t INNER JOIN member_t " +
                                "ON cts_enquiry_t.mcode = member_t.mcode " +
                                "LEFT JOIN (SELECT common_namecode_t.namename, common_namecode_t.code " +
                                "FROM common_namecode_t where common_namecode_t.type_code = 'ENQSTS') as status " +
                                "ON status.code = cts_enquiry_t.enq_status " +
                                "LEFT JOIN (SELECT common_namecode_t.namename, common_namecode_t.code " +
                                "FROM common_namecode_t where common_namecode_t.type_code = 'ENQPGR') as progress " +
                                "ON progress.code = cts_enquiry_t.enq_progress " +
                                "LEFT JOIN (SELECT common_namecode_t.namename, common_namecode_t.code " +
                                "FROM common_namecode_t where common_namecode_t.type_code = 'ENQSIT') as situation " +
                                "ON situation.code = cts_enquiry_t.enq_situation " +
                                "LEFT JOIN cts_login_t ON (cts_login_t.id || '') = cts_enquiry_t.user_id ";
                return strQuery;
            }

        }

        internal protected class ErsCtsRepCategoryDetailSpecification
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                string strQuery = "SELECT cts_login_t.ag_name, cts_enquiry_t.enq_casename, cate1.namename AS cate1, cate2.namename AS cate2, cate3.namename AS cate3, ";
                strQuery += "cate4.namename AS cate4, cate5.namename AS cate5, progress.namename AS w_progress, status.namename AS w_status,  ";
                strQuery += "situation.namename as w_situation, type.namename AS w_type, priorty.namename AS w_priority ";
                strQuery += "FROM cts_enquiry_t LEFT JOIN cts_login_t ";
                strQuery += "ON (cts_login_t.id || '') = cts_enquiry_t.user_id ";
                strQuery += "LEFT JOIN (SELECT common_namecode_t.namename, common_namecode_t.code ";
                strQuery += "FROM common_namecode_t where common_namecode_t.type_code = 'ENQPGR') as progress ";
                strQuery += "ON progress.code = cts_enquiry_t.enq_progress ";
                strQuery += "LEFT JOIN (SELECT common_namecode_t.namename, common_namecode_t.code ";
                strQuery += "FROM common_namecode_t where common_namecode_t.type_code = 'ENQSTS') as status ";
                strQuery += "ON status.code = cts_enquiry_t.enq_status ";
                strQuery += "LEFT JOIN (SELECT common_namecode_t.namename, common_namecode_t.code ";
                strQuery += "FROM common_namecode_t where common_namecode_t.type_code = 'ENQSIT') as situation ";
                strQuery += "ON situation.code = cts_enquiry_t.enq_situation ";
                strQuery += "LEFT JOIN (SELECT common_namecode_t.namename, common_namecode_t.code ";
                strQuery += "FROM common_namecode_t where common_namecode_t.type_code = 'ENQTYP') as type ";
                strQuery += "ON type.code = cts_enquiry_t.enq_type ";
                strQuery += "LEFT JOIN (SELECT common_namecode_t.namename, common_namecode_t.code ";
                strQuery += "FROM common_namecode_t where common_namecode_t.type_code = 'ENQPRY') as priorty ";
                strQuery += "ON priorty.code = cts_enquiry_t.enq_priorty ";
                strQuery += "LEFT JOIN (SELECT cts_enquiry_category_t.namename, cts_enquiry_category_t.code ";
                strQuery += "FROM cts_enquiry_category_t where cts_enquiry_category_t.type_code = 'ENQCT1') as cate1 ";
                strQuery += "ON cate1.code = cts_enquiry_t.cate1 ";
                strQuery += "LEFT JOIN (SELECT cts_enquiry_category_t.namename, cts_enquiry_category_t.code ";
                strQuery += "FROM cts_enquiry_category_t where cts_enquiry_category_t.type_code = 'ENQCT2') as cate2 ";
                strQuery += "ON cate2.code = cts_enquiry_t.cate2 ";
                strQuery += "LEFT JOIN (SELECT cts_enquiry_category_t.namename, cts_enquiry_category_t.code ";
                strQuery += "FROM cts_enquiry_category_t where cts_enquiry_category_t.type_code = 'ENQCT3') as cate3 ";
                strQuery += "ON cate3.code = cts_enquiry_t.cate3 ";
                strQuery += "LEFT JOIN (SELECT cts_enquiry_category_t.namename, cts_enquiry_category_t.code ";
                strQuery += "FROM cts_enquiry_category_t where cts_enquiry_category_t.type_code = 'ENQCT4') as cate4 ";
                strQuery += "ON cate4.code = cts_enquiry_t.cate4 ";
                strQuery += "LEFT JOIN (SELECT cts_enquiry_category_t.namename, cts_enquiry_category_t.code ";
                strQuery += "FROM cts_enquiry_category_t where cts_enquiry_category_t.type_code = 'ENQCT5') as cate5 ";
                strQuery += "ON cate5.code = cts_enquiry_t.cate5 ";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepCategoryBillDetail
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                string strQuery = "SELECT cts_enquiry_detail_t.sub_no, cts_enquiry_detail_t.enq_corresponding, cts_enquiry_detail_t.enq_detail, cts_enquiry_detail_t.ans_detail, ";
                strQuery += "cts_enquiry_detail_t.email_header, cts_enquiry_detail_t.email_body, cts_enquiry_detail_t.email_fotter, cts_enquiry_detail_t.memo ";
                strQuery += "FROM cts_enquiry_detail_t ";
                return strQuery;
            }
        }
    }
}
