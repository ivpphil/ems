﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports.specification
{
    [Obsolete("分割する")]
    public class ErsCtsRepOrderSpecification
        : ISearchSpecification
    {
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepOrderRecordSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetTotalData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepOrderTotalSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetTempData(Criteria criteria)
        {
            var specificationForSQL = new ErsRepOrderTempRecordSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetTempTotalData(Criteria criteria)
        {
            var specificationForSQL = new ErsRepOrderTempTotalSpecification();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual List<Dictionary<string, object>> GetTemp(Criteria criteria)
        {
            var specificationForSQL = new ErsRepOrderTemp();
            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        public virtual int GetCountData(Criteria criteria)
        {
            var specificationForSQL = new ErsCtsRepOrderCountSpecification();

            var record = ErsRepository.SelectSatisfying(specificationForSQL, criteria);
            if (record.Count == 0)
                return 0;

            return Convert.ToInt32(record[0]["count"]);
        }

        internal protected class ErsCtsRepOrderRecordSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                string strQuery = "SELECT agent, ag_type, Count(*) as amount, SUM(total) as price ";
                strQuery += "FROM (	SELECT cts_agetype_t.agent, cts_login_t.user_id, d_master_t.total, cts_login_t.ag_type, d_master_t.intime, d_master_t.site_id ";
                strQuery += "FROM cts_agetype_t JOIN cts_login_t ON cts_agetype_t.id = cts_login_t.ag_type ";
                strQuery += "JOIN d_master_t ON (cts_login_t.id || '') = d_master_t.user_id ";
                strQuery += ") AS reporder ";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepOrderCountSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL?
            /// </summary>
            /// <returns>SQL?</returns>
            public virtual string asSQL()
            {
                string strQuery = "SELECT COUNT(reporder.user_id) FROM cts_login_t as reporder";
                return strQuery;
            }
        }

        internal protected class ErsCtsRepOrderTotalSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL
            /// </summary>
            /// <returns>SQL</returns>
            /// 

            public virtual string asSQL()
            {
                string strQuery = "SELECT Count(*) as amount, SUM(total) as price ";
                strQuery += "FROM (SELECT cts_agetype_t.agent, cts_login_t.user_id, d_master_t.total, cts_login_t.ag_type, d_master_t.intime,d_master_t.site_id  ";
                strQuery += "FROM cts_agetype_t JOIN cts_login_t ON cts_agetype_t.id = cts_login_t.ag_type ";
                strQuery += "JOIN d_master_t ON (cts_login_t.id || '') = d_master_t.user_id ";
                strQuery += ") AS reporder ";
                /*
                string strQuery = "SELECT SUM(amount) as amount, SUM(total) as price ";
                strQuery += "FROM (SELECT cts_agetype_t.agent, cl.user_id, ds_master_t.amount, ds_master_t.total, cts_login_t.ag_type, d_master_t.intime ";
                strQuery += "FROM cts_agetype_t JOIN cts_login_t ON cts_agetype_t.id = cts_login_t.ag_type ";
                strQuery += "JOIN d_master_t ON cts_login_t.user_id = d_master_t.user_id ";
                strQuery += "JOIN ds_master_t ON d_master_t.d_no = ds_master_t.d_no ";
                strQuery += "LEFT JOIN cts_login_t cl on d_master_t.user_id = (cl.id || '') ";
                strQuery += ") AS reporder ";
                */
                return strQuery;
            }
        }

        internal protected class ErsRepOrderTempRecordSpecification
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                string strQuery = "SELECT agent, ag_type, SUM(amount) as temp_amount, SUM(total) as temp_price ";
                strQuery += "FROM (	SELECT cts_agetype_t.agent, cl.user_id, bask_t.amount, bask_t.total, cts_order_t.charge1_ag_type as ag_type, cts_order_t.intime,cts_order_t.site_id  ";
                strQuery += "FROM cts_agetype_t JOIN cts_order_t ON cts_agetype_t.id = cts_order_t.charge1_ag_type ";
                strQuery += "JOIN bask_t ON cts_order_t.ransu = bask_t.ransu ";
                strQuery += "JOIN cts_login_t cl on cts_order_t.user_id = (cl.id || '') ";
                strQuery += ") as reporder";
                return strQuery;
            }
        }

        internal protected class ErsRepOrderTempTotalSpecification
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                string strQuery = "SELECT 'TOTAL' as agent, NULL as ag_type, SUM(amount) as temp_amount, SUM(total) as temp_price ";
                strQuery += "FROM (	SELECT cts_agetype_t.agent, cl.user_id, bask_t.amount, bask_t.total, cts_order_t.charge1_ag_type as ag_type, cts_order_t.intime,cts_order_t.site_id  ";
                strQuery += "FROM cts_agetype_t JOIN cts_order_t ON cts_agetype_t.id = cts_order_t.charge1_ag_type ";
                strQuery += "JOIN bask_t ON cts_order_t.ransu = bask_t.ransu ";
                strQuery += "JOIN cts_login_t cl on cts_order_t.user_id = (cl.id || '') ";
                strQuery += ") as reporder";
                return strQuery;
            }
        }

        internal protected class ErsRepOrderTemp
            : ISpecificationForSQL
        {
            public virtual string asSQL()
            {
                string strQuery = "SELECT DISTINCT(cts_agetype_t.agent) as agent, cts_agetype_t.id as ag_type, '0' as amount, '0' as price ";
                strQuery += "FROM cts_agetype_t JOIN cts_login_t ";
                strQuery += "ON cts_agetype_t.id = cts_login_t.ag_type";
                return strQuery;
            }
        }
    }
}
