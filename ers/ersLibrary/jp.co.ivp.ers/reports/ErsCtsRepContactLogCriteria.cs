﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepContactLogCriteria
        : Criteria
    {

        protected internal ErsCtsRepContactLogCriteria()
        {
        }

        public virtual DateTime? datefrom
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.intime", value, Operation.GREATER_EQUAL));
            }
        }

        public virtual DateTime? dateto
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.intime", value, Operation.LESS_EQUAL));
            }
            
        }

        public virtual int? typcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.enq_type", value, Operation.EQUAL));
            }
        }

        public virtual int? prycode
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.enq_priorty", value, Operation.EQUAL));
            }
        }

        public virtual int? stscode
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.enq_status", value, Operation.EQUAL));
            }
        }

        public virtual int? pgrcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.enq_progress", value, Operation.EQUAL));
            }
        }

        public virtual int? sitcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.enq_situation", value, Operation.EQUAL));
            }
        }

        public virtual int? ct1code
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.cate1", value, Operation.EQUAL));
            }
        }

        public virtual int? ct2code
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.cate2", value, Operation.EQUAL));
            }
        }

        public virtual int? ct3code
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.cate3", value, Operation.EQUAL));
            }
        }

        public virtual int? ct4code
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.cate4", value, Operation.EQUAL));
            }
        }

        public virtual int? ct5code
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.cate5", value, Operation.EQUAL));
            }
        }

        public virtual string enq_casename
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.enq_casename", value, Operation.EQUAL));
            }
        }

        public virtual int? case_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.case_no", value, Operation.EQUAL));
            }
        }


        public virtual EnumEnqCorresponding? enq_corresponding
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_detail_t.enq_corresponding", value, Operation.EQUAL));
            }
        }

        public virtual int case_num
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_detail_t.case_no", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("cts_enquiry_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_enquiry_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}
