﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.IO;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;


namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepProd
        : ErsRepositoryEntity 
    {

        public override int? id { get; set; }
        public virtual string sname { get; set; }
        public virtual string gcode { get; set; }
        public virtual string scode { get; set; }
        public virtual int price { get; set; }

        public virtual int tempord { get; set; }
        public virtual int cts { get; set; }
        public virtual int tel { get; set; }
        public virtual int let { get; set; }
        public virtual int fax { get; set; }
        public virtual int mb { get; set; }
        public virtual int pc { get; set; }
        public virtual int totnum { get; set; }
        public virtual int totamt { get; set; }
        public virtual int stock { get; set; }

        public virtual int totprodno { get; set; }
        public virtual int totprodamt { get; set; }

        public virtual int totalnumber { get; set; }
        public virtual int totalprice { get; set; }

        public virtual string cts_sname { get; set; }
    }
}
