﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepOrderCriteria
        : Criteria
    {

        protected internal ErsCtsRepOrderCriteria()
        {
        }

        public virtual DateTime? datefrom
        {
            set
            {
                this.Add(Criteria.GetCriterion("reporder.intime", value, Operation.GREATER_EQUAL));
            }
        }

        public virtual DateTime? dateto
        {
            set
            {
                this.Add(Criteria.GetCriterion("reporder.intime", value, Operation.LESS_EQUAL));
            }

        }

        public virtual string agentid
        {
            set
            {
                //this.Add(Criteria.GetLikeClauseCriterion("reporder.user_id", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
                this.Add(Criteria.GetCriterion("reporder.user_id", value, Operation.EQUAL));
            }
        }

        public virtual EnumAgType? ag_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("reporder.ag_type", value, Operation.EQUAL));
            }
        }

        public virtual string user_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("user_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("reporder.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("reporder.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

    }
}
