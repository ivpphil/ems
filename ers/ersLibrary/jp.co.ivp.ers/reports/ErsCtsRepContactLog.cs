﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.IO;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepContactLog
        : ErsRepositoryEntity 
    {
        public override int? id { get; set; }
        public virtual int case_no { get; set; }
        public virtual DateTime intime { get; set; }
        public virtual string lname { get; set; }
        public virtual string fname { get; set; }
        public virtual string enq_casename { get; set; }
        public virtual string ag_name { get; set; }

        public virtual string type { get; set; }
        public virtual string priorty { get; set; }
        public virtual string w_status { get; set; }
        public virtual string progress { get; set; }
        public virtual string w_situation { get; set; }

        public virtual string w_enq_progress { get; set; }
        public virtual string w_enq_status { get; set; }
        public virtual string w_enq_situation { get; set; }
        public virtual string w_enq_type { get; set; }
        public virtual string w_enq_priorty { get; set; }

        public virtual string cate1 { get; set; }
        public virtual string cate2 { get; set; }
        public virtual string cate3 { get; set; }
        public virtual string cate4 { get; set; }
        public virtual string cate5 { get; set; }

        public virtual EnumEnqCorresponding? enq_corresponding { get; set; }
        public virtual string enq_detail { get; set; }
        public virtual string ans_detail { get; set; }
        public virtual string email_header { get; set; }
        public virtual string email_body { get; set; }
        public virtual string email_fotter { get; set; }
        public virtual string memo { get; set; }

        public virtual string personname { get; set; }
        public virtual string prodidcode { get; set; }
        public virtual string prodname { get; set; }
        public virtual string cat2 { get; set; }
        public virtual string cat3 { get; set; } 
        public virtual string gender { get; set; }
        public virtual string birth { get; set; }
        public virtual string lnamek { get; set; }
        public virtual string fnamek { get; set; }
        public virtual string initials { get; set; }

        public virtual string pref_name { get; set; }
        public virtual string cat1 { get; set; }
        public virtual string escalation { get; set; }
        public virtual string content { get; set; }
        public virtual DateTime utime { get; set; }
        public virtual string[] prodcode { get; set; }
        public virtual int? enq_progress { get; set; }
        public virtual int? enq_situation { get; set; }
    }
}
