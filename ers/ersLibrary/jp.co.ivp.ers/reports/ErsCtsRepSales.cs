﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.IO;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepSales
        : ErsRepositoryEntity 
    {
        public override int? id { get; set; }
        public virtual string scode { get; set; }
        public virtual string byhalfhour { get; set; }
        public virtual int amount { get; set; }
        public virtual int d_no_count { get; set; }

        public virtual string[] scodes { get; set; }
        public virtual string sname { get; set; }

        public virtual string cts_sname { get; set; }
    }
}
