﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.IO;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepCall
        :ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual string user_id { get; set; }
        public virtual string ag_name { get; set; }
        public virtual int d_no_count { get; set; }
        public virtual DateTime intime { get; set; }
        public virtual DateTime? datefrom { get; set; }
        public virtual DateTime? dateto { get; set; }
        public virtual DateTime? timefrom { get; set; }
        public virtual DateTime? timeto { get; set; }
        public virtual EnumAgType? ag_type { get; set; }
        public virtual string TargetSearch { get; set; }
        public virtual string byhalfhour { get; set; }
        public virtual string recordtime { get; set; }
    }
}
