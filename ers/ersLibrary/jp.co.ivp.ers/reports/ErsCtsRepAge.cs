﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.IO;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepAge
        :ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual DateTime intime { get; set; }
        public virtual DateTime? datefrom { get; set; }
        public virtual DateTime? dateto { get; set; }
        public virtual DateTime? timefrom { get; set; }
        public virtual DateTime? timeto { get; set; }

        public virtual string sex { get; set; }
        public virtual string age_code { get; set; }
        
        public virtual int mcount { get; set; }
        public virtual int fcount { get; set; }
        public virtual int total { get { return mcount + fcount; } }
        public virtual int percentage_male { get { return (int)Math.Round(total == 0 ? (mcount == 0 ? 0 : 100) : ((double)mcount / (double)total) * 100); } }
        public virtual int percentage_female { get { return (int)Math.Round(total == 0 ? (fcount == 0 ? 0 : 100) : ((double)fcount / (double)total) * 100); } }
        public virtual int percentage_total { get { return (int)Math.Round(entire_total == 0 ? (total == 0 ? 0 : 100) : ((double)total / (double)entire_total) * 100); } }
        public virtual int entire_total { get; set; }

        public virtual string code { get; set; }
        public virtual string namename { get; set; }
        public virtual int agecountmale { get; set; }
        public virtual int agecountfemale { get; set; }
        public virtual int age { get; set; }
        public virtual int agesubtotal { get; set; }

        public virtual string scode { get; set; }
        public virtual string sname { get; set; }

        public virtual string cts_sname { get; set; }
    }
}
