﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.search.specification; 
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.reports;

namespace jp.co.ivp.ers.reports.strategy
{
    public class ErsCtsRepContactLogStgy
    {
        /// <summary>
        /// 検索（クライテリア）
        /// </summary>
        /// <param name="criteria">クライテリア</param>
        /// <returns>検索結果リスト</returns>
        public IList<ErsCtsRepContactLog> Find(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogSpecification();

            List<ErsCtsRepContactLog> lstRet = new List<ErsCtsRepContactLog>();
            var list = spec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepContactLog repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public IList<ErsCtsRepContactLog> FindDetail(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogSpecification();

            List<ErsCtsRepContactLog> lstRet = new List<ErsCtsRepContactLog>();
            var list = spec.GetDetailData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepContactLog repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public IList<ErsCtsRepContactLog> FindBillDetail(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogSpecification();
            List<ErsCtsRepContactLog> lstRet = new List<ErsCtsRepContactLog>();
            var list = spec.GetBillData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepContactLog repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public IList<ErsCtsRepContactLog> GetCSVContactLog(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogSpecification();
            List<ErsCtsRepContactLog> lstRet = new List<ErsCtsRepContactLog>();
            var list = spec.GetCSVSearchData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepContactLog repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public long GetRecordCount(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogSpecification();
            return spec.GetCountData(criteria);
        }
    }
}
