﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.search.specification; 
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.reports;

namespace jp.co.ivp.ers.reports.strategy
{
    public class ErsCtsRepProdStgy
    {
        /// <summary>
        /// 検索（クライテリア）
        /// </summary>
        /// <param name="criteria">クライテリア</param>
        /// <returns>検索結果リスト</returns>
        public IList<ErsCtsRepProd> Find(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepProdSpecification();

            List<ErsCtsRepProd> lstRet = new List<ErsCtsRepProd>();
            var list = spec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepProd repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepProdWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }

        public virtual IList<ErsCtsRepProd> FindList(ErsCtsRepProdCriteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepProdSpecification();

            List<ErsCtsRepProd> lstRet = new List<ErsCtsRepProd>();
            var list = spec.GetSearchDataList(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepProd repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepProdWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }

        public IList<ErsCtsRepProd> FindPrice(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepProdSpecification();

            List<ErsCtsRepProd> lstRet = new List<ErsCtsRepProd>();
            var list = spec.GetTotalPrice(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepProd repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepProdWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        public IList<ErsCtsRepProd> FindNumber(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepProdSpecification();

            List<ErsCtsRepProd> lstRet = new List<ErsCtsRepProd>();
            var list = spec.GetTotalNumber(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepProd repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepProdWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
    }
}
