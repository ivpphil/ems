﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.search.specification;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.reports;

namespace jp.co.ivp.ers.reports.strategy
{
    public class ErsCtsRepSalesStgy
    {
        public IList<ErsCtsRepSales> Find(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesSpecification();

            List<ErsCtsRepSales> lstRet = new List<ErsCtsRepSales>();
            var list = spec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepSales repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }

        public long GetRecordCount(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesSpecification();
            return spec.GetCountData(criteria);
        }

        /// <summary>
        /// 検索（クライテリア）
        /// </summary>
        /// <param name="criteria">クライテリア</param>
        /// <returns>検索結果リスト</returns>
        /// 
        public IList<ErsCtsRepSales> FindInterval(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesSpecification();

            List<ErsCtsRepSales> lstRet = new List<ErsCtsRepSales>();
            var list = spec.GetInterval(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepSales repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public IList<ErsCtsRepSales> FindTotalByInterval(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesSpecification();

            List<ErsCtsRepSales> lstRet = new List<ErsCtsRepSales>();
            var list = spec.GetTotalByInterval(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepSales repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public IList<ErsCtsRepSales> FindTotalByDNo(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesSpecification();

            List<ErsCtsRepSales> lstRet = new List<ErsCtsRepSales>();
            var list = spec.GetTotalByDNo(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepSales repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesWithParameter(dr);
                if (dr["amount"] != null)
                {
                    repList.d_no_count = Convert.ToInt32(dr["amount"]);
                }
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public IList<ErsCtsRepSales> FindTemp(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesSpecification();

            List<ErsCtsRepSales> lstRet = new List<ErsCtsRepSales>();
            var list = spec.GetSearchTempData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepSales repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }

        public IList<ErsCtsRepSales> FindTempTotalByInterval(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesSpecification();

            List<ErsCtsRepSales> lstRet = new List<ErsCtsRepSales>();
            var list = spec.GetTempTotalByInterval(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepSales repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public IList<ErsCtsRepSales> FindTempTotalByDNo(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesSpecification();

            List<ErsCtsRepSales> lstRet = new List<ErsCtsRepSales>();
            var list = spec.GetTempTotalByDNo(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepSales repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public IList<ErsCtsRepSales> FindProducts(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesSpecification();
            List<ErsCtsRepSales> lstRet = new List<ErsCtsRepSales>();
            var list = spec.GetProductList(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepSales repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }
    }
}
