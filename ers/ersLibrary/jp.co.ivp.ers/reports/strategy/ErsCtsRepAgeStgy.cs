﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.search.specification;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.reports;

namespace jp.co.ivp.ers.reports.strategy
{
    public class ErsCtsRepAgeStgy
    {
        /// <summary>
        /// 検索（クライテリア）
        /// </summary>
        /// <param name="criteria">クライテリア</param>
        /// <returns>検索結果リスト</returns>
        public  IList<ErsCtsRepAge> Find(Criteria criteria, string where, string whereTemp)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepAgeSpecification();

            List<ErsCtsRepAge> lstRet = new List<ErsCtsRepAge>();
            var list = spec.GetSearchData(criteria, where, whereTemp);

            foreach (var dr in list)
            {
                ErsCtsRepAge repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepAgeWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public int FindDList(Criteria criteria, string where, string whereTemp)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepAgeSpecification();

            where = " WHERE TRUE " + where;
            whereTemp = " WHERE TRUE " + whereTemp;
            int listcount = spec.GetDCount(criteria, where, whereTemp);
            return listcount;
        }
    }
}
