﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.search.specification;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.reports.specification;

namespace jp.co.ivp.ers.reports.strategy
{
    public class ErsCtsRepOrderStgy
    {
        /// <summary>
        /// 検索（クライテリア）
        /// </summary>
        /// <param name="criteria">クライテリア</param>
        /// <returns>検索結果リスト</returns>
        public IList<ErsCtsRepOrder> Find(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderSpecification();

            List<ErsCtsRepOrder> lstRet = new List<ErsCtsRepOrder>();
            var list = spec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepOrder repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }

        public IList<ErsCtsRepOrder> FindTotal(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderSpecification();
            List<ErsCtsRepOrder> lstRet = new List<ErsCtsRepOrder>();
            var list = spec.GetTotalData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepOrder repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }

        public IList<ErsCtsRepOrder> FindTemp(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderSpecification();
            List<ErsCtsRepOrder> lstRet = new List<ErsCtsRepOrder>();
            var list = spec.GetTempData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepOrder repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public IList<ErsCtsRepOrder> FindTempTotal(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderSpecification();
            List<ErsCtsRepOrder> lstRet = new List<ErsCtsRepOrder>();
            var list = spec.GetTempTotalData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepOrder repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public IList<ErsCtsRepOrder> FindList(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderSpecification();
            List<ErsCtsRepOrder> lstRet = new List<ErsCtsRepOrder>();
            var list = spec.GetTemp(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepOrder repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public long GetRecordCount(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderSpecification();
            return spec.GetCountData(criteria);
        }
    }
}
