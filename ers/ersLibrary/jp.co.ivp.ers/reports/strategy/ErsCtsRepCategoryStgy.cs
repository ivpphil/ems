﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.search.specification; 
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.reports;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepCategoryStgy
    {
        /// <summary>
        /// 検索（クライテリア）
        /// </summary>
        /// <param name="criteria">クライテリア</param>
        /// <returns>検索結果リスト</returns>
        public IList<ErsCtsRepCategory> Find(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategorySpecification();

            List<ErsCtsRepCategory> lstRet = new List<ErsCtsRepCategory>();
            var list = spec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCategory repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public IList<ErsCtsRepCategory> FindChild(Criteria criteria, int? site_id = null)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategorySpecification();
            
            List<ErsCtsRepCategory> lstRet = new List<ErsCtsRepCategory>();
            var list = spec.GetChild(criteria, site_id);

            foreach (var dr in list)
            {
                ErsCtsRepCategory repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public IList<ErsCtsRepCategory> FindRowSpan(Criteria criteria, int? site_id = null)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategorySpecification();

            List<ErsCtsRepCategory> lstRet = new List<ErsCtsRepCategory>();
            var list = spec.GetRowSpan(criteria, site_id);

            foreach (var dr in list)
            {
                ErsCtsRepCategory repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }
        public IList<ErsCtsRepCategory> FindChildCount(Criteria criteria, string where)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategorySpecification();

            List<ErsCtsRepCategory> lstRet = new List<ErsCtsRepCategory>();
            var list = spec.GetChildCount(criteria, where);

            foreach (var dr in list)
            {
                ErsCtsRepCategory repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }
        public IList<ErsCtsRepCategory> FindParentCount(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategorySpecification();

            List<ErsCtsRepCategory> lstRet = new List<ErsCtsRepCategory>();
            var list = spec.GetParentCount(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCategory repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }
        public IList<ErsCtsRepCategory> FindList(Criteria criteria, int? site_id = null)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategorySpecification();
            List<ErsCtsRepCategory> lstRet = new List<ErsCtsRepCategory>();
            var list = spec.GetCategoryList(criteria, site_id);

            foreach (var dr in list)
            {
                ErsCtsRepCategory repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }
        public IList<ErsCtsRepCategory> FindDetail(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategorySpecification();
            List<ErsCtsRepCategory> lstRet = new List<ErsCtsRepCategory>();
            var list = spec.GetDetailData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCategory repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }
        public IList<ErsCtsRepCategory> FindBillDetail(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategorySpecification();
            List<ErsCtsRepCategory> lstRet = new List<ErsCtsRepCategory>();
            var list = spec.GetBillData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCategory repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }
        public long GetRecordCount(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategorySpecification();
            return spec.GetCountData(criteria);
        }
    }
}
