﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.search.specification;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.reports;

namespace jp.co.ivp.ers.reports.strategy
{
    public class ErsCtsRepInboundStgy
    {
        /// <summary>
        /// 検索（クライテリア）
        /// </summary>
        /// <param name="criteria">クライテリア</param>
        /// <returns>検索結果リスト</returns>
        public IList<ErsCtsRepInbound> Find(Criteria criteria)
        {   // Find Type
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundSpecification();

            List<ErsCtsRepInbound> lstRet = new List<ErsCtsRepInbound>();
            var list = spec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepInbound repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        public IList<ErsCtsRepInbound> FindProgress(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundSpecification();

            List<ErsCtsRepInbound> lstRet = new List<ErsCtsRepInbound>();
            var list = spec.GetProgress(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepInbound repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundWithParameter(dr);
                lstRet.Add(repList);
            }
            
            return lstRet;
        }
        public IList<ErsCtsRepInbound> FindSituation(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundSpecification();

            List<ErsCtsRepInbound> lstRet = new List<ErsCtsRepInbound>();
            var list = spec.GetSituation(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepInbound repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundWithParameter(dr);
                lstRet.Add(repList);

            }
            return lstRet;
        }

        public IList<ErsCtsRepInbound> FindInboundList(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundSpecification();

            List<ErsCtsRepInbound> lstRet = new List<ErsCtsRepInbound>();
            var list = spec.GetListData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepInbound repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundWithParameter(dr);
                lstRet.Add(repList);
            }

            return lstRet;
        }

        public long GetRecordCount(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundSpecification();
            return spec.GetCountData(criteria);
        }
    }
}
