﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.search.specification;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.reports;


namespace jp.co.ivp.ers.reports.strategy
{
    public class ErsCtsRepCallStgy
    {
        /// <summary>
        /// 検索（クライテリア）
        /// </summary>
        /// <param name="criteria">クライテリア</param>
        /// <returns>検索結果リスト</returns>
        ///
        #region Time
        // Total User
        public IList<ErsCtsRepCall> Find(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetSearchData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        // Total Time
        public IList<ErsCtsRepCall> FindTotalTime(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTotalTimeData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        // Total Per user Per Time
        public IList<ErsCtsRepCall> FindTotalUserTime(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTotalUserTimeData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        //

        // TEMP Total Per user Per Time
        public IList<ErsCtsRepCall> FindTempTotalUserTime(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTempTotalUserTimeData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        // TEMP Total Per Time
        public IList<ErsCtsRepCall> FindTempTotalTime(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTempTotalTimeData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        // TEMP Total Per User
        public IList<ErsCtsRepCall> FindTempTotalUser(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTempTotalUserData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        // Interval
        public IList<ErsCtsRepCall> FindInterval(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetIntervalData(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        } 
        #endregion

        #region Day
        // Total User
        public IList<ErsCtsRepCall> FindDay(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetSearchDataDay(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        // Total Time
        public IList<ErsCtsRepCall> FindTotalTimeDay(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTotalTimeDataDay(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        // Total Per user Per Time
        public IList<ErsCtsRepCall> FindTotalUserTimeDay(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTotalUserTimeDataDay(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        //

        // TEMP Total Per user Per Time
        public IList<ErsCtsRepCall> FindTempTotalUserTimeDay(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTempTotalUserTimeDataDay(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        // TEMP Total Per Time
        public IList<ErsCtsRepCall> FindTempTotalTimeDay(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTempTotalTimeDataDay(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        // TEMP Total Per User
        public IList<ErsCtsRepCall> FindTempTotalUserDay(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTempTotalUserDataDay(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        #endregion


        #region Month
        public IList<ErsCtsRepCall> FindMonth(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetSearchDataMonth(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        public IList<ErsCtsRepCall> FindTotalTimeMonth(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTotalTimeDataMonth(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        public IList<ErsCtsRepCall> FindTotalUserTimeMonth(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTotalUserTimeDataMonth(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        public IList<ErsCtsRepCall> FindTempTotalUserTimeMonth(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTempTotalUserTimeDataMonth(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        public IList<ErsCtsRepCall> FindTempTotalTimeMonth(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTempTotalTimeDataMonth(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        public IList<ErsCtsRepCall> FindTempTotalUserMonth(Criteria criteria)
        {
            var spec = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallSpecification();

            List<ErsCtsRepCall> lstRet = new List<ErsCtsRepCall>();
            var list = spec.GetTempTotalUserDataMonth(criteria);

            foreach (var dr in list)
            {
                ErsCtsRepCall repList = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallWithParameter(dr);
                lstRet.Add(repList);
            }
            return lstRet;
        }
        #endregion
    }
}
