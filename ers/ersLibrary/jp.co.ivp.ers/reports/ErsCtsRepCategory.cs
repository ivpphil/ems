﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using System.IO;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepCategory
        : ErsRepositoryEntity 
    {
        public override int? id { get; set; }
        public virtual int case_no { get; set; }
        public virtual DateTime intime { get; set; }
        public virtual string lname { get; set; }
        public virtual string fname { get; set; }
        public virtual string enq_casename { get; set; }
        public virtual string ag_name { get; set; }

        public virtual EnumEnqType? type { get; set; }
        public virtual string priorty { get; set; }
        public virtual EnumEnqStatus? status { get; set; }
        public virtual EnumEnqProgress? progress { get; set; }
        public virtual EnumEnqSituation? situation { get; set; }
        public virtual EnumPrefecture? pref { get; set; }
        public virtual EnumEnqProgress? enq_progress { get; set; }

        public virtual string cate1 { get; set; }
        public virtual string cate2 { get; set; }
        public virtual string cate3 { get; set; }
        public virtual string cate4 { get; set; }
        public virtual string cate5 { get; set; }

        public virtual EnumEnqCorresponding? enq_corresponding { get; set; }
        public virtual string enq_detail { get; set; }
        public virtual string ans_detail { get; set; }
        public virtual string email_header { get; set; }
        public virtual string email_body { get; set; }
        public virtual string email_fotter { get; set; }
        public virtual string memo { get; set; }
        public virtual string inquiry { get; set; }


        public virtual string parent_type_code { get; set; }
        public virtual string parent_code { get; set; }
        public virtual string parent_namename { get; set; }
        public virtual string child_code { get; set; }
        public virtual string code { get; set; }
        public virtual string type_code { get; set; }
        public virtual string namename { get; set; }
        public virtual EnumActive? active { get; set; }

        public virtual int rowcount { get; set; }

        public virtual int childcount { get; set; }
        public virtual int parentcount { get; set; }

        public virtual string w_status { get; set; }
        public virtual string w_situation { get; set; }
        public virtual string w_progress { get; set; }
        public virtual string w_priority { get; set; }
        public virtual string w_type { get; set; }

        public bool hasChild { get; set; }
    }
}
