﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepInboundCriteria:Criteria
    {
        protected internal ErsCtsRepInboundCriteria()
        { }
        public virtual DateTime? datefrom
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.intime", value, Operation.GREATER_EQUAL));
            }
        }
        public virtual DateTime? dateto
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.intime", value, Operation.LESS_EQUAL));
            }
        }
        public virtual string type_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("common_namecode_t.type_code", value, Operation.EQUAL));
            }
        }

        public virtual EnumEnqType? enq_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.enq_type", value, Operation.EQUAL));
            }
        }

        public virtual EnumEnqProgress? enq_progress
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.enq_progress", value, Operation.EQUAL));
            }
        }

        public virtual EnumEnqSituation? enq_situation
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.enq_situation", value, Operation.EQUAL));
            }
        }

        public virtual int? case_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_t.case_no", value, Operation.EQUAL));
            }
        }

        public virtual int case_num
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_enquiry_detail_t.case_no", value, Operation.EQUAL));
            }
        }

        public virtual EnumAgType? ag_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_login_t.ag_type", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("cts_enquiry_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_enquiry_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }
    }
}
