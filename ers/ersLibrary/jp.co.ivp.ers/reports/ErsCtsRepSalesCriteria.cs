﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepSalesCriteria
        : Criteria
    {

        protected internal ErsCtsRepSalesCriteria()
        {
        }

        public virtual DateTime? datefrom
        {
            set
            {
                this.Add(Criteria.GetCriterion("repsales.intime", value, Operation.GREATER_EQUAL));
            }
        }

        public virtual DateTime? dateto
        {
            set
            {
                this.Add(Criteria.GetCriterion("repsales.intime", value, Operation.LESS_EQUAL));
            }

        }

        public virtual string[] scodes
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("repsales.scode", value));
            }
        }

        public virtual string scale_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("repsales.scale_code", value, Operation.EQUAL));
            }
        }

        public virtual string scode
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("s_master_t.scode", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }
    }
}
