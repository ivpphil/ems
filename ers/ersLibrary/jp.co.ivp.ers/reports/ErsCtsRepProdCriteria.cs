﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.reports
{
    public class ErsCtsRepProdCriteria
        : Criteria
    {

        protected internal ErsCtsRepProdCriteria()
        {
        }

        public virtual DateTime? datefrom
        {
            set
            {
                this.Add(Criteria.GetCriterion("repprod.intime", value, Operation.GREATER_EQUAL));
            }
        }

        public virtual DateTime? dateto
        {
            set
            {
                this.Add(Criteria.GetCriterion("repprod.intime", value, Operation.LESS_EQUAL));
            }
        }

        public virtual string agentid
        {
            set
            {
                this.Add(Criteria.GetCriterion("repprod.user_id", value, Operation.EQUAL));
            }
        }

        public virtual string agentidLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("repprod.user_id", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual string prodcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("repprod.scode", value, Operation.EQUAL));
            }
        }

        public virtual string prodcodeLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("repprod.scode", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual string prodname
        {
            set
            {
                this.Add(Criteria.GetCriterion("repprod.sname", value, Operation.EQUAL));
            }
        }

        public virtual string prodnameLikePrefix
        {
            set
            {
                this.Add(Criteria.GetLikeClauseCriterion("repprod.sname", value, LIKE_SEARCH_TYPE.PREFIX_SEARCH));
            }
        }

        public virtual EnumAgType? ag_type
        {
            set
            { this.Add(Criteria.GetCriterion("repprod.ag_type", value, Operation.EQUAL)); }
        }

        public DateTime? param_datefrom { get; set; }

        public DateTime? param_dateto { get; set; }

        public string param_prodcode { get; set; }

        public string param_prodname { get; set; }

        public string param_agentid { get; set; }

        public EnumAgType? param_ag_type { get; set; }

        public string param_TargetOrder { get; set; }

        public int? param_site_id { get; set; }
    }
}
