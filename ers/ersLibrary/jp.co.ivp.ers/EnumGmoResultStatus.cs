﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumGmoResultStatus
    {
        /// <summary>
        /// 未決済
        /// </summary>
        UNPROCESSED,

        /// <summary>
        /// 未決済(3D登録済)
        /// </summary>
        AUTHENTICATED,

        /// <summary>
        /// 有効性チェック
        /// </summary>
        CHECK,

        /// <summary>
        /// 即時売上
        /// </summary>
        CAPTURE,

        /// <summary>
        /// 仮売上
        /// </summary>
        AUTH,

        /// <summary>
        /// 実売上
        /// </summary>
        SALES,

        /// <summary>
        /// 取消
        /// </summary>
        VOID,

        /// <summary>
        /// 返品
        /// </summary>
        RETURN,

        /// <summary>
        /// 月跨り返品
        /// </summary>
        RETURNX,

        /// <summary>
        /// 簡易オーソリ
        /// </summary>
        SAUTH,

        /// <summary>
        /// 要求成功
        /// </summary>
        REQSUCCESS,

        /// <summary>
        /// キャンセル
        /// </summary>
        CANCEL,

        /// <summary>
        /// 決済失敗
        /// </summary>
        PAYFAIL,

        /// <summary>
        /// 期限切れ
        /// </summary>
        EXPIRED,


        /// <summary>
        /// 決済完了
        /// </summary>
        PAYSUCCESS,
    }
}
