﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumOnOff
    {
        /// <summary>
        /// 0 : 無効 [Off]
        /// </summary>
        Off = 0,

        /// <summary>
        /// 1 : 有効 [On]
        /// </summary>
        On = 1, 
    }
}
