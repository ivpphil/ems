﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// 送料に関するステータス.
    /// Inherits ErsEnum<CarriageFreeStatus>
    /// </summary>
    public enum EnumCarriageFreeStatus
    {
        CARRIAGE_FREE = 1,          //送料なし
        CARRIAGE_NOT_FREE = 2,      //送料あり
        FREE_SETTING_NOTHING = 3   //設定なし
    }
}
