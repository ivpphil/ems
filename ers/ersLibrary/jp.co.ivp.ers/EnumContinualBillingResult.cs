﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumContinualBillingResult
    {
        /// <summary>
        /// 0: 成功
        /// </summary>
        Success = 0,

        /// <summary>
        /// 1: アップロード時のエラー（フォーマットエラー、会員 ID なしエラーなど）
        /// </summary>
        ValueError = 1,

        /// <summary>
        /// 2: オーソリ時エラー
        /// </summary>
        AuthoryError = 2,

        /// <summary>
        /// 3: 売上処理時エラー
        /// </summary>
        SaleError = 3,
    }
}
