﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// 入金区分定義
    /// </summary>
    public enum EnumOrderPaymentStatusType
    {
        /// <summary>
        /// 10: 未入金
        /// </summary>
        NOT_PAID = 10,

        /// <summary>
        /// 20: 与信未取得
        /// </summary>
        NO_AUTH = 20,

        /// <summary>
        /// 30: 入金済
        /// </summary>
        PAID = 30
    }
}
