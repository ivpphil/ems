﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.faq
{
    public class ErsCtsFAQCriteria : Criteria
	{

        public int? id
        {
            set
            {
                Add(Criteria.GetCriterion("cts_faq_template_t.id", value, Operation.EQUAL));
            }
        }

        public string faq_casename
        {
            set
            {
                Add(Criteria.GetCriterion("cts_faq_template_t.faq_casename", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// カテゴリ1
        /// </summary>
        public virtual int? cate1
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_faq_template_t.cate1", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// カテゴリ2
        /// </summary>
        public virtual int? cate2
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_faq_template_t.cate2", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// カテゴリ3
        /// </summary>
        public virtual int? cate3
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_faq_template_t.cate3", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// カテゴリ4
        /// </summary>
        public virtual int? cate4
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_faq_template_t.cate4", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// カテゴリ5
        /// </summary>
        public virtual int? cate5
        {
            set
            {
                this.Add(Criteria.GetCriterion("cts_faq_template_t.cate5", value, Operation.EQUAL));
            }
        }

        public virtual string keywords
        {
            set
            {
                this.Add(Criteria.JoinWithOR(
                        new[] { 
                            Criteria.GetLikeClauseCriterion("cts_faq_template_t.faq_casename", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH), 
                            Criteria.GetLikeClauseCriterion("cts_faq_template_t.faq_text", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH), 
                            Criteria.GetLikeClauseCriterion("cts_faq_template_t.faq_text2", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH),
                        }));
            }
        }

        /// <summary>
        /// Sets the criteria for site_id
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                var site_id_criteria = Criteria.GetCriterion("cts_faq_template_t.site_id", value, Operation.EQUAL);
                var common_site_id_criteria = Criteria.GetCriterion("cts_faq_template_t.site_id", (int)EnumSiteId.COMMON_SITE_ID, Operation.EQUAL);
                this.Add(Criteria.JoinWithOR(new[] { site_id_criteria, common_site_id_criteria }));
            }
        }

        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("id", orderBy);
        }

	}
}
