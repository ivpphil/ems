﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.information.specification;

namespace jp.co.ivp.ers.faq
{
    public class ErsCtsFAQFactory
    {

        public ErsCtsFAQCriteria GetErsCtsFAQCriteria()
        {
            return new ErsCtsFAQCriteria();
        }

        public ErsCtsFAQRepository GetErsCtsFAQRepository()
        {
            return new ErsCtsFAQRepository();
        }

        public ErsCtsFAQ GetErsCtsFAQ()
        {
            return new ErsCtsFAQ();
        }

        public ErsCtsFAQ GetErsFAQWithModel(ErsModelBase model)
        {
            var FAQ  = this.GetErsCtsFAQ();
            FAQ.OverwriteWithModel(model);
            return FAQ;
        }

        public ErsCtsFAQ GetErsFAQWithParameters(Dictionary<string, object> parameters)
        {
            var FAQ = this.GetErsCtsFAQ();
            FAQ.OverwriteWithParameter(parameters);
            return FAQ;
        }

        public ErsCtsFAQ GetErsCtsFAQWithID(int? id)
        {
            var repository = this.GetErsCtsFAQRepository ();
            var criteria = this.GetErsCtsFAQCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");

            return list[0];
        }

    }
}
