﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.faq
{
    public class ErsCtsFAQRepository
        : ErsRepository<ErsCtsFAQ>
    {
        public ErsCtsFAQRepository()
            : base("cts_faq_template_t")
        {
        }

        public ErsCtsFAQRepository (ErsDatabase objDB)
            : base("cts_faq_template_t", objDB)
        {
        }
    }
}
