﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Data;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;

namespace jp.co.ivp.ers.faq
{
    public class ErsCtsFAQ: ErsRepositoryEntity
	{
        /// <summary>
        /// 伝票番号
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// インタイム
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 時間
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// ケース名
        /// </summary>
        public virtual string faq_casename { get; set; }

        /// <summary>
        /// カテゴリ1
        /// </summary>
        public virtual int? cate1 { get; set; }

        /// <summary>
        /// カテゴリ2
        /// </summary>
        public virtual int? cate2 { get; set; }

        /// <summary>
        /// カテゴリ3
        /// </summary>
        public virtual int? cate3 { get; set; }

        /// <summary>
        /// カテゴリ4
        /// </summary>
        public virtual int? cate4 { get; set; }

        /// <summary>
        /// カテゴリ5
        /// </summary>
        public virtual int? cate5 { get; set; }


        /// <summary>
        /// FAQのテキスト
        /// </summary>
        public virtual string faq_text { get; set; }

        /// <summary>
        /// FAQ1のテキスト
        /// </summary>
        public virtual string faq_text2 { get; set; }

        /// <summary>
        /// ユーザーID
        /// </summary>
        public virtual int? user_id { get; set; }

        /// <summary>
        /// アクティブ
        /// </summary>
        public virtual EnumActive? active { get; set; }

        /// <summary>
        /// サイト　ID
        /// </summary>
        public virtual int? site_id { get; set; }
	}
}
