﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumQuestPtn
    {
        AboutOrder = 1,
        AboutDelivery,
        AboutPay,
        AboutItem,
        AboutOther
    }
}
