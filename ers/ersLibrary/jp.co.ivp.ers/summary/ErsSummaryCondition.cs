﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummaryCondition
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string group_code { get; set; }
        public string item_name { get; set; }
        public string column_name { get; set; }
        public string template_name { get; set; }
        public EnumSummaryWhereOparation where_oparation { get; set; }
        public string validation_comment { get; set; }
        public EnumOnOff is_required { get; set; }
        public int? disp_order { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
    }
}
