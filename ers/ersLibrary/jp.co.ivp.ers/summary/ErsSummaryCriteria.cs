﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummaryCriteria
        : Criteria
    {
        public string group_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("summary_t.group_code", value, Operation.EQUAL));
            }
        }

        public string summary_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("summary_t.summary_code", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("summary_t.active", value, Operation.EQUAL));
            }
        }

        public void SetOrderByDisp_order(OrderBy orderBy)
        {
            this.AddOrderBy("summary_t.disp_order", orderBy);
        }

        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("summary_t.id", orderBy);
        }
    }
}
