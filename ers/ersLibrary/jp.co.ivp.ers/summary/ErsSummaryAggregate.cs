﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummaryAggregate
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string summary_code { get; set; }
        public string row_key_name { get; set; }
        public string row_name { get; set; }
        public string column_name { get; set; }
        public string value_name { get; set; }
        public EnumOnOff? display_row_total { get; set; }
        public EnumOnOff? display_column_total { get; set; }
        public EnumOnOff? display_graph { get; set; }
        public EnumSummaryGraphType? graph_type { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
    }
}
