﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummaryAggregateRepository
        : ErsRepository<ErsSummaryAggregate>
    {
        public ErsSummaryAggregateRepository()
            : base("summary_aggregate_t")
        {
        }
    }
}
