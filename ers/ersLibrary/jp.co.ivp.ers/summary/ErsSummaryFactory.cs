﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.summary.specification;
using jp.co.ivp.ers.summary.strategy;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummaryFactory
    {
        public virtual ErsSummaryGroupRepositroy GetErsSummaryGroupRepositroy()
        {
            return new ErsSummaryGroupRepositroy();
        }

        public virtual ErsSummaryGroupCriteria GetErsSummaryGroupCriteria()
        {
            return new ErsSummaryGroupCriteria();
        }

        public virtual ErsSummaryGroup GetErsSummaryGroup()
        {
            return new ErsSummaryGroup();
        }

        public virtual ErsSummaryConditionRepositroy GetErsSummaryConditionRepositroy()
        {
            return new ErsSummaryConditionRepositroy();
        }

        public virtual ErsSummaryConditionCriteria GetErsSummaryConditionCriteria()
        {
            return new ErsSummaryConditionCriteria();
        }

        public virtual ErsSummaryCondition GetErsSummaryCondition()
        {
            return new ErsSummaryCondition();
        }

        public virtual ErsSummaryCondition GetErsSummaryConditionWithId(int? id)
        {
            var repository = this.GetErsSummaryConditionRepositroy();
            var criteria = this.GetErsSummaryConditionCriteria();
            criteria.id = id;
            criteria.active = EnumActive.Active;
            var listRecord = repository.Find(criteria);
            if (listRecord.Count == 0)
            {
                return null;
            }

            return listRecord.First();
        }

        public virtual ErsSummaryRepository GetErsSummaryRepository()
        {
            return new ErsSummaryRepository();
        }

        public virtual ErsSummaryCriteria GetErsSummaryCriteria()
        {
            return new ErsSummaryCriteria();
        }

        public virtual ErsSummary GetErsSummary()
        {
            return new ErsSummary();
        }

        public virtual SummaryAggregateSearchSpec GetSummaryAggregateSearchSpec()
        {
            return new SummaryAggregateSearchSpec();
        }

        public virtual ErsSummaryAggregateRepository GetErsSummaryAggregateRepository()
        {
            return new ErsSummaryAggregateRepository();
        }

        public virtual ErsSummaryAggregateCriteria GetErsSummaryAggregateCriteria()
        {
            return new ErsSummaryAggregateCriteria();
        }

        public virtual ErsSummaryAggregate GetErsSummaryAggregate()
        {
            return new ErsSummaryAggregate();
        }

        public virtual SummaryConditionContainer GetSummaryConditionContainer()
        {
            return new SummaryConditionContainer();
        }

        public virtual CreateSummaryConditionStgy GetCreateSummaryConditionStgy()
        {
            return new CreateSummaryConditionStgy();
        }

        public virtual ReplaceSummaryConditionStgy GetReplaceSummaryConditionStgy()
        {
            return new ReplaceSummaryConditionStgy();
        }

        public virtual CreateSummaryResultAggregateStgy GetCreateSummaryResultAggregateStgy()
        {
            return new CreateSummaryResultAggregateStgy();
        }

        public virtual CreateSummaryResultExtractStgy GetCreateSummaryResultExtractStgy()
        {
            return new CreateSummaryResultExtractStgy();
        }

        public virtual ErsSummaryExtractRepository GetErsSummaryExtractRepository()
        {
            return new ErsSummaryExtractRepository();
        }

        public virtual ErsSummaryExtractCriteria GetErsSummaryExtractCriteria()
        {
            return new ErsSummaryExtractCriteria();
        }

        public virtual ErsSummaryExtract GetErsSummaryExtract()
        {
            return new ErsSummaryExtract();
        }

        public virtual SummaryExtractSearchSpec GetSummaryExtractSearchSpec()
        {
            return new SummaryExtractSearchSpec();
        }
    }
}
