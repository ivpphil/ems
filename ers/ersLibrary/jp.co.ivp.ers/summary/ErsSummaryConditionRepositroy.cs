﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummaryConditionRepositroy
        : ErsRepository<ErsSummaryCondition>
    {
        public ErsSummaryConditionRepositroy()
            : base("summary_condition_t")
        {
        }
    }
}
