﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummaryExtract
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string group_code { get; set; }
        public string summary_code { get; set; }
        public string item_name { get; set; }
        public string column_name { get; set; }
        public int? disp_order { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
    }
}
