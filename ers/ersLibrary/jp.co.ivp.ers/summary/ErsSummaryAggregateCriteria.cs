﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummaryAggregateCriteria
        : Criteria
    {
        public string group_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("summary_aggregate_t.group_code", value, Operation.EQUAL));
            }
        }

        public string summary_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("summary_aggregate_t.summary_code", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("summary_aggregate_t.active", value, Operation.EQUAL));
            }
        }
    }
}
