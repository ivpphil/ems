﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.summary.strategy;

namespace jp.co.ivp.ers.summary.specification
{
    public class SummaryAggregateSearchSpec
            : ISpecificationForSQL
    {
        public string strSql { get; set; }

        /// <summary>
        /// 集計用クエリを発行する。
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="summary_conditions"></param>
        /// <returns></returns>
        public IEnumerable<IDictionary<string, object>> Select(string sql, IEnumerable<ISummaryConditionValue> summary_conditions)
        {
            var listConditions = ErsFactory.ersSummaryFactory.GetCreateSummaryConditionStgy().GetConditions(summary_conditions);

            Criteria criteria;
            this.strSql = ErsFactory.ersSummaryFactory.GetReplaceSummaryConditionStgy().Replace(out criteria, sql, listConditions);

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            foreach (var record in ErsRepository.SelectSatisfying<object>(this, criteria, setup.summary_commandtimeout))
            {
                var ret = record as IDictionary<string, object>;
                yield return ret;
            }
        }

        public string asSQL()
        {
            return "SELECT * FROM (" + this.strSql + ") AS smry WHERE true";
        }
    }
}
