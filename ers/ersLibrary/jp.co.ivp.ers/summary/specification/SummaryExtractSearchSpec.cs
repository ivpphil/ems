﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.summary.strategy;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.summary.specification
{
    public class SummaryExtractSearchSpec
        : ISpecificationForSQL
    {
        public string strSql { get; set; }

        public string strColumns { get; set; }

        /// <summary>
        /// 抽出用クエリを発行する。
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="listSummaryExtract"></param>
        /// <param name="summary_conditions"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> Select(string sql, IEnumerable<ErsSummaryExtract> listSummaryExtract, IEnumerable<ISummaryConditionValue> summary_conditions)
        {
            var listConditions = ErsFactory.ersSummaryFactory.GetCreateSummaryConditionStgy().GetConditions(summary_conditions);

            Criteria criteria;
            this.strSql = ErsFactory.ersSummaryFactory.GetReplaceSummaryConditionStgy().Replace(out criteria, sql, listConditions);

            this.strColumns = this.GetColumns(listSummaryExtract);

            return ErsRepository.SelectSatisfying(this, criteria);
        }

        private string GetColumns(IEnumerable<ErsSummaryExtract> listSummaryExtract)
        {
            var listColumns = new List<string>();
            foreach (var summaryExtract in listSummaryExtract)
            {
                listColumns.Add(string.Format("{0} AS \"{1}\"", summaryExtract.column_name, summaryExtract.item_name));
            }
            return string.Join(", ", listColumns);
        }

        public string asSQL()
        {
            return "SELECT " + strColumns + " FROM (" + this.strSql + ") AS smry WHERE true";
        }
    }
}
