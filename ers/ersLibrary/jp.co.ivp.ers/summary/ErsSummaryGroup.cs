﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummaryGroup
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string group_code { get; set; }
        public string group_name { get; set; }
        public int? disp_order { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive? active { get; set; }
    }
}
