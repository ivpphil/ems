﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummaryExtractRepository
        : ErsRepository<ErsSummaryExtract>
    {
        public ErsSummaryExtractRepository()
            : base("summary_extract_t")
        {
        }
    }
}
