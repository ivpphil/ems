﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummaryRepository
        : ErsRepository<ErsSummary>
    {
        public ErsSummaryRepository()
            : base("summary_t")
        {
        }
    }
}
