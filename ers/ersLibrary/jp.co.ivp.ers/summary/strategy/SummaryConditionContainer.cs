﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.summary.strategy
{
    public class SummaryConditionContainer
    {
        public string column_name { get; set; }

        public string value { get; set; }

        public string value_from { get; set; }

        public string value_to { get; set; }

        public Criteria criteria { get; set; }
    }
}
