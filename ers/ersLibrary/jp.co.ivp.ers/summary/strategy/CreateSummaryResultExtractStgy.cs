﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.summary.strategy
{
    public class CreateSummaryResultExtractStgy
    {
        private const string CACHE_KEY = "jp.co.ivp.ers.summary.strategy.CreateSummaryResultExtractStgy.";

        public IEnumerable<Dictionary<string, object>> GetResultWithCache(ErsSummary summary, IEnumerable<ErsSummaryExtract> summaryExtract, IEnumerable<ISummaryConditionValue> summary_conditions)
        {
            string fullKey = CACHE_KEY + summary.id;
            var cacheList = (List<Dictionary<string, object>>)ErsCommonContext.GetPooledObject(fullKey);

            if (cacheList == null)
            {
                cacheList = new List<Dictionary<string, object>>();

                foreach (var result in GetResult(summary, summaryExtract, summary_conditions))
                {
                    cacheList.Add(result);
                    yield return result;
                }

                ErsCommonContext.SetPooledObject(fullKey, cacheList);
            }
            else
            {
                foreach (var result in cacheList)
                {
                    yield return result;
                }
            }
        }

        /// <summary>
        /// 抽出クエリの結果を返却します
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="summaryExtract"></param>
        /// <param name="summary_conditions"></param>
        /// <returns></returns>
        /// <remarks>冗長ですが、CreateSummaryResultAggreateStgyと同じになるようにこのクラスを作成</remarks>
        public IEnumerable<Dictionary<string, object>> GetResult(ErsSummary summary, IEnumerable<ErsSummaryExtract> summaryExtract, IEnumerable<ISummaryConditionValue> summary_conditions)
        {
            return ErsFactory.ersSummaryFactory.GetSummaryExtractSearchSpec().Select(summary.sql, summaryExtract, summary_conditions);
        }
    }
}
