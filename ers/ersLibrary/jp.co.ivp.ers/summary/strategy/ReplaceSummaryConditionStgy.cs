﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.summary.strategy
{
    public class ReplaceSummaryConditionStgy
    {
        public string Replace(out Criteria criteria, string sql, List<SummaryConditionContainer> listConditions)
        {
            //{where}用のCriteria
            criteria = new Criteria();

            var valueCriteria = new Criteria();

            var sqlBuilder = new StringBuilder(sql);
            foreach (var condition in listConditions)
            {
                //whereとparameterを取得
                //{****.value}
                CriterionBase criterion;
                criterion = Criteria.GetUniversalCriterion(":" + condition.column_name + ".value", new Dictionary<string, object>() { { condition.column_name + ".value", condition.value } });
                sqlBuilder.Replace("{" + condition.column_name + ".value}", criterion.GetWhere());
                valueCriteria.Add(criterion);

                //{****.value_from}
                criterion = Criteria.GetUniversalCriterion(":" + condition.column_name + ".value_from", new Dictionary<string, object>() { { condition.column_name + ".value_from", condition.value_from } });
                sqlBuilder.Replace("{" + condition.column_name + ".value_from}", criterion.GetWhere());
                valueCriteria.Add(criterion);

                //{****.value_to}
                criterion = Criteria.GetUniversalCriterion(":" + condition.column_name + ".value_to", new Dictionary<string, object>() { { condition.column_name + ".value_to", condition.value_to } });
                sqlBuilder.Replace("{" + condition.column_name + ".value_to}", criterion.GetWhere());
                valueCriteria.Add(criterion);

                //{****.criteria}
                var param = condition.criteria.GetWhere();
                sqlBuilder.Replace("{" + condition.column_name + ".criteria}", param.HasValue() ? param : "true");
                valueCriteria.Add(condition.criteria);

                criteria.Add(condition.criteria);
            }

            var where = criteria.GetWhere();
            sqlBuilder.Replace("{where}", where.HasValue() ? where : "true");

            criteria.Add(valueCriteria);

            criteria.ParamaterOnly = true;
            return sqlBuilder.ToString();
        }
    }
}
