﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.summary.strategy
{
    public class CreateSummaryConditionStgy
    {

        public List<SummaryConditionContainer> GetConditions(IEnumerable<ISummaryConditionValue> summary_conditions)
        {
            var listContainer = new List<SummaryConditionContainer>();
            foreach (var item in summary_conditions)
            {
                var objCondition = ErsFactory.ersSummaryFactory.GetErsSummaryConditionWithId(item.id);
                
                Criteria criteria = null;
                switch (objCondition.where_oparation)
                {
                    case EnumSummaryWhereOparation.BETWEEN_DATE:
                        if (item.value_from.HasValue() && item.value_to.HasValue())
                        {
                            criteria = new Criteria();
                            criteria.Add(this.GetBetweenCriterion(objCondition.column_name, item.value_from, item.value_to));
                        }
                        break;
                    default:
                        if (item.value.HasValue())
                        {
                            criteria = new Criteria();
                            criteria.Add(this.GetCriterion(objCondition.column_name, item.value, objCondition.where_oparation));
                        }
                        break;
                }

                if (criteria != null)
                {
                    var container = ErsFactory.ersSummaryFactory.GetSummaryConditionContainer();
                    container.column_name = objCondition.column_name;
                    container.value = item.value;
                    container.value_from = item.value_from;
                    container.value_to = item.value_to;
                    container.criteria = criteria;
                    listContainer.Add(container);
                }
            }
            return listContainer;
        }

        /// <summary>
        /// 集計用のWHEREを作成
        /// </summary>
        /// <param name="column_name"></param>
        /// <param name="value_from"></param>
        /// <param name="value_to"></param>
        private CriterionBase GetBetweenCriterion(string column_name, string value_from, string value_to)
        {
            return Criteria.GetBetweenCriterion(value_from, value_to, Criteria.ColumnName(column_name));
        }

        /// <summary>
        /// 集計用のWHEREを作成
        /// </summary>
        /// <param name="column_name"></param>
        /// <param name="value"></param>
        /// <param name="where_oparation"></param>
        private CriterionBase GetCriterion(string column_name, string value, EnumSummaryWhereOparation where_oparation)
        {
            return Criteria.GetCriterion(column_name, value, (Criteria.Operation)where_oparation);
        }
    }
}
