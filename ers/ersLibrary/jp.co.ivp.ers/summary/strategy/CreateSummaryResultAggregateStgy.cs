﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.summary.strategy
{
    public class CreateSummaryResultAggregateStgy
    {
        private const string ROW_NAME_KEY = "row_name";

        private const string CACHE_KEY = "jp.co.ivp.ers.summary.strategy.CreateSummaryResultAggregateStgy.";

        /// <summary>
        /// 集計クエリの結果を、テーブル組に変換して返却します
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="summaryAggregate"></param>
        /// <param name="objMappable"></param>
        /// <returns></returns>
        /// <remarks>
        ///  {行名}|{列名}|{値}　のように取得したデータを、以下のテーブル組みに変換する
        ///                      | {列名1} | {列名2}
        /// ------------------------------------------
        /// {行名データ1}  |{値1}      | {値2}
        /// {行名データ2}  |{値1}      | {値2}
        /// {行名データ3}  |{値1}      | {値2}
        /// ....
        /// </remarks>
        public IEnumerable<Dictionary<string, object>> GetResultWithCache(ErsSummary summary, ErsSummaryAggregate summaryAggregate, IEnumerable<ISummaryConditionValue> summary_conditions)
        {
            string fullKey = CACHE_KEY + summary.id;
            var cacheList = (List<Dictionary<string, object>>)ErsCommonContext.GetPooledObject(fullKey);

            if (cacheList == null)
            {
                cacheList = new List<Dictionary<string, object>>();

                foreach (var result in GetResult(summary, summaryAggregate, summary_conditions))
                {
                    cacheList.Add(result);
                    yield return result;
                }

                ErsCommonContext.SetPooledObject(fullKey, cacheList);
            }
            else
            {
                foreach (var result in cacheList)
                {
                    yield return result;
                }
            }
        }

        /// <summary>
        /// 集計クエリの結果を、テーブル組に変換して返却します
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="summaryAggregate"></param>
        /// <param name="objMappable"></param>
        /// <returns></returns>
        /// <remarks>
        ///  {行名}|{列名}|{値}　のように取得したデータを、以下のテーブル組みに変換する
        ///                      | {列名1} | {列名2}
        /// ------------------------------------------
        /// {行名データ1}  |{値1}      | {値2}
        /// {行名データ2}  |{値1}      | {値2}
        /// {行名データ3}  |{値1}      | {値2}
        /// ....
        /// </remarks>
        public IEnumerable<Dictionary<string, object>> GetResult(ErsSummary summary, ErsSummaryAggregate summaryAggregate, IEnumerable<ISummaryConditionValue> summary_conditions)
        {
            var listSearchResult = ErsFactory.ersSummaryFactory.GetSummaryAggregateSearchSpec().Select(summary.sql, summary_conditions);

            var listResult = new Dictionary<string, Dictionary<string, object>>();

            foreach (var result in listSearchResult)
            {
                Dictionary<string, object> record;
                //キーに指定されたカラムを元にレコードを判定
                var existRecord = listResult.Where((pair) => pair.Key == Convert.ToString(result[summaryAggregate.row_key_name]));
                if (existRecord.Count() == 0)
                {
                    //新規行を作成
                    record = new Dictionary<string, object>();
                    record[ROW_NAME_KEY] = result[summaryAggregate.row_name];
                    listResult.Add(Convert.ToString(result[summaryAggregate.row_key_name]), record);
                }
                else
                {
                    record = existRecord.First().Value;
                }

                //行にカラムと値を追加
                record[Convert.ToString(result[summaryAggregate.column_name])] = result[summaryAggregate.value_name];
            }

            foreach (var pair in listResult)
            {
                var dictionary = pair.Value;
                if (summaryAggregate.display_row_total == EnumOnOff.On)
                {
                    //行の合計値を出力するように指定されている場合は、C#コードにて計算
                    var total = 0;
                    foreach (var column in dictionary)
                    {
                        if (column.Key != ROW_NAME_KEY)
                        {
                            total += Convert.ToInt32(column.Value);
                        }
                    }
                    dictionary[ErsResources.GetFieldName("aggregate_total")] = total;
                }

                yield return dictionary;
            }

            //列の合計値を出力するように指定されている場合は、C#コードにて計算
            if (summaryAggregate.display_column_total == EnumOnOff.On)
            {
                yield return this.GetTotalResult(listResult);
            }
        }

        /// <summary>
        /// 合計行の取得
        /// </summary>
        /// <param name="listResult"></param>
        /// <returns></returns>
        private Dictionary<string, object> GetTotalResult(Dictionary<string, Dictionary<string, object>> listResult)
        {
            var totalDictionary = new Dictionary<string, object>();
            totalDictionary[ROW_NAME_KEY] = ErsResources.GetFieldName("aggregate_total");
            if (listResult.Values.Count != 0)
            {
                foreach (var columnRecord in listResult.Values.First())
                {
                    if (columnRecord.Key == ROW_NAME_KEY)
                    {
                        continue;
                    }

                    var total = 0;
                    foreach (var record in listResult)
                    {
                        total += Convert.ToInt32(record.Value[columnRecord.Key]);
                    }
                    totalDictionary[columnRecord.Key] = total;
                }
            }
            return totalDictionary;
        }

        /// <summary>
        /// 合計行以外を出力（グラフ用）
        /// </summary>
        /// <param name="listResult"></param>
        /// <returns></returns>
        public IEnumerable<IEnumerable<KeyValuePair<string, object>>> GetResultOfGraph(IEnumerable<Dictionary<string, object>> listResult)
        {
            foreach (var result in listResult)
            {
                if (Convert.ToString(result[ROW_NAME_KEY]) == ErsResources.GetFieldName("aggregate_total"))
                {
                    continue;
                }

                yield return this.GetFirstResultOfGraph(result);
            }
        }

        /// <summary>
        /// 合計行以外を出力（グラフ用）
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<string, object>> GetFirstResultOfGraph(Dictionary<string, object> result)
        {
            foreach (var data in result)
            {
                if (data.Key == ErsResources.GetFieldName("aggregate_total"))
                {
                    continue;
                }
                yield return data;
            }
        }
    }
}
