﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.summary.strategy
{
    public interface ISummaryConditionValue
        : IOverwritable
    {
        int? id { get; set; }

        string value { get; set; }

        string value_from { get; set; }

        string value_to { get; set; }
    }
}
