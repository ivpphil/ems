﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummaryGroupRepositroy
        : ErsRepository<ErsSummaryGroup>
    {
        public ErsSummaryGroupRepositroy()
            : base("summary_group_t")
        {
        }
    }
}
