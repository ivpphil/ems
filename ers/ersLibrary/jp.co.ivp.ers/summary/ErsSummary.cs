﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.summary
{
    public class ErsSummary
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string group_code { get; set; }
        public string summary_code { get; set; }
        public EnumSummaryType? summary_type { get; set; }
        public string summary_name { get; set; }
        public EnumOnOff? display_title { get; set; }
        public EnumOnOff? display_table { get; set; }
        public EnumOnOff? display_csv_dl { get; set; }
        public string sql { get; set; }
        public int? disp_order { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
        public EnumActive active { get; set; }
    }
}
