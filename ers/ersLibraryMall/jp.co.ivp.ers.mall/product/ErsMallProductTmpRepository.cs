﻿using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品テンポラリリポジトリ [Repository for mall product temporary table]
    /// </summary>
    public class ErsMallProductTmpRepository
        : ErsRepository<ErsMallProductTmp>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="type">DBタイプ [Type of DB]</param>
        public ErsMallProductTmpRepository()
            : base("mall_product_tmp_t")
        {
        }
    }
}
