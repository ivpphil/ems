﻿using System;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品ファイルアップロード管理エンティティ [Entity for mall product upload management]
    /// </summary>
    public class ErsMallProductFileUploadManage
        : ErsRepositoryEntity
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 登録日時 [Inserted datetime]
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新日時 [Updated datetime]
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// アクティブ [Active]
        /// </summary>
        public virtual EnumActive? active { get; set; }

        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        public virtual int? site_id { get; set; }

        /// <summary>
        /// モール店舗区分 [Mall shop type]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// モール商品アップロードファイルタイプ [Mall product upload file type]
        /// </summary>
        public virtual EnumMallProductUploadFileType? file_type { get; set; }

        /// <summary>
        /// ファイル名 [File name]
        /// </summary>
        public virtual string file_name { get; set; }

        /// <summary>
        /// 画像ディレクトリID [Image directory id]
        /// </summary>
        public virtual int? image_directory_id { get; set; }
    }
}
