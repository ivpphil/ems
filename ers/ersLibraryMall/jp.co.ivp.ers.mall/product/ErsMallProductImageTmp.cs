﻿using System;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品画像テンポラリエンティティ [Entity for mall product image temporary]
    /// </summary>
    public class ErsMallProductImageTmp
        : ErsRepositoryEntity, IErsMallProductTmp
    {
        #region 非カラム [Not column]
        /// <summary>
        /// モール用商品コード [Product code for Mall]
        /// </summary>
        public string mall_scode
        {
            get
            {
                return ErsMallCommonService.GetMallSkuFromErs(this.scode);
            }
        }

        /// <summary>
        /// 画像ディレクトリID [Image directory id]
        /// </summary>
        public virtual int? image_directory_id { get; set; }
        #endregion


        /// <summary>
        /// id : id
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// intime : 登録日時
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// utime : 更新日時
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// active : アクティブ
        /// </summary>
        public virtual EnumActive? active { get; set; }

        /// <summary>
        /// site_id : サイトID
        /// </summary>
        public virtual int? site_id { get; set; }

        /// <summary>
        /// mall_shop_kbn : モール店舗区分
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// image_type : 画像タイプ
        /// </summary>
        public virtual EnumMallProductImageType? image_type { get; set; }

        /// <summary>
        /// scode : SKUコード
        /// </summary>
        public virtual string scode { get; set; }

        /// <summary>
        /// gcode : ITEMコード
        /// </summary>
        public virtual string gcode { get; set; }

        /// <summary>
        /// image_index : 画像インデックス
        /// </summary>
        public virtual int? image_index { get; set; }
    }
}
