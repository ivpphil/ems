﻿using System;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品画像ディレクトリエンティティ [Entity for mall product image directory]
    /// </summary>
    public class ErsMallProductImageDirectory
        : ErsRepositoryEntity
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 登録日時 [Inserted datetime]
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新日時 [Updated datetime]
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// アクティブ [Active]
        /// </summary>
        public virtual EnumActive? active { get; set; }

        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        public virtual int? site_id { get; set; }

        /// <summary>
        /// モール店舗区分 [Mall shop type]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// SKUコード [SKU code]
        /// </summary>
        public virtual string scode { get; set; }

        /// <summary>
        /// ディレクトリID [Directory id]
        /// </summary>
        public virtual int? directory_id { get; set; }
    }
}
