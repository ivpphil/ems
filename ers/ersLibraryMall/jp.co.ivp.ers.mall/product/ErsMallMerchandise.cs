﻿using System;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    public class ErsMallMerchandise : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public virtual string gcode { get; set; }
        public virtual string scode { get; set; }
        public virtual string jancode { get; set; }
        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }
        public virtual int? price { get; set; }
        public virtual int? manage_id { get; set; }

        #region amazon
        /// <summary>
        /// 商品タイプ [Product type]
        /// </summary>
        public virtual string product_type { get; set; }

        /// <summary>
        /// ブランド [Brand]
        /// </summary>
        public virtual string brand { get; set; }

        /// <summary>
        /// メーカー名 [Maker name]
        /// </summary>
        public virtual string maker_name { get; set; }

        /// <summary>
        /// 型式 [Sname]
        /// </summary>
        public virtual string model_number { get; set; }

        /// <summary>
        /// product_description : 商品説明文
        /// </summary>
        public string product_description { get; set; }

        /// <summary>
        /// item_package_quantity : パッケージ商品数
        /// </summary>
        public int? item_package_quantity { get; set; }

        /// <summary>
        /// condition_note : 商品のコンディション説明
        /// </summary>
        public string condition_note { get; set; }

        /// <summary>
        /// sale_price : セール価格
        /// </summary>
        public int? sale_price { get; set; }

        /// <summary>
        /// sale_from_date : セール開始日
        /// </summary>
        public DateTime? sale_from_date { get; set; }

        /// <summary>
        /// sale_end_date : セール終了日
        /// </summary>
        public DateTime? sale_end_date { get; set; }

        /// <summary>
        /// offering_can_be_gift_messaged : ギフトメッセージ
        /// </summary>
        public virtual EnumOnOff offering_can_be_gift_messaged { get; set; }

        /// <summary>
        /// offering_can_be_giftwrapped : ギフト包装可
        /// </summary>
        public virtual EnumOnOff offering_can_be_giftwrapped { get; set; }

        /// <summary>
        /// bullet_point1 : 商品説明の箇条書き1
        /// </summary>
        public string bullet_point1 { get; set; }

        /// <summary>
        /// bullet_point2 : 商品説明の箇条書き2
        /// </summary>
        public string bullet_point2 { get; set; }

        /// <summary>
        /// bullet_point3 : 商品説明の箇条書き3
        /// </summary>
        public string bullet_point3 { get; set; }

        /// <summary>
        /// bullet_point4 : 商品説明の箇条書き4
        /// </summary>
        public string bullet_point4 { get; set; }

        /// <summary>
        /// bullet_point5 : 商品説明の箇条書き5
        /// </summary>
        public string bullet_point5 { get; set; }

        /// <summary>
        /// generic_keywords1 : 検索キーワード1
        /// </summary>
        public string generic_keywords1 { get; set; }

        /// <summary>
        /// generic_keywords2 : 検索キーワード2
        /// </summary>
        public string generic_keywords2 { get; set; }

        /// <summary>
        /// generic_keywords3 : 検索キーワード3
        /// </summary>
        public string generic_keywords3 { get; set; }

        /// <summary>
        /// generic_keywords4 : 検索キーワード4
        /// </summary>
        public string generic_keywords4 { get; set; }

        /// <summary>
        /// generic_keywords5 : 検索キーワード5
        /// </summary>
        public string generic_keywords5 { get; set; }

        /// <summary>
        /// recommended_browse_nodes1 : 推奨ブラウズノード1
        /// </summary>
        public virtual string recommended_browse_nodes1 { get; set; }

        /// <summary>
        /// recommended_browse_nodes2 : 推奨ブラウズノード2
        /// </summary>
        public virtual string recommended_browse_nodes2 { get; set; }

        /// <summary>
        /// specific_uses_keywords1 : 用途1
        /// </summary>
        public virtual string specific_uses_keywords1 { get; set; }

        /// <summary>
        /// specific_uses_keywords2 : 用途2
        /// </summary>
        public virtual string specific_uses_keywords2 { get; set; }

        /// <summary>
        /// target_audience_keywords : 対象
        /// </summary>
        public virtual string target_audience_keywords { get; set; }

        /// <summary>
        /// safety_warning : 警告
        /// </summary>
        public virtual string safety_warning { get; set; }

        /// <summary>
        /// legal_disclaimer_description : 法規上の免責条項
        /// </summary>
        public virtual string legal_disclaimer_description { get; set; }

        /// <summary>
        /// ingredients1 : 原材料・成分1
        /// </summary>
        public virtual string ingredients1 { get; set; }

        /// <summary>
        /// ingredients2 : 原材料・成分2
        /// </summary>
        public virtual string ingredients2 { get; set; }

        /// <summary>
        /// ingredients3 : 原材料・成分3
        /// </summary>
        public virtual string ingredients3 { get; set; }

        /// <summary>
        /// special_ingredients : 特別成分
        /// </summary>
        public virtual string special_ingredients { get; set; }

        /// <summary>
        /// indications : 使用上の注意
        /// </summary>
        public virtual string indications { get; set; }

        /// <summary>
        /// directions : 商品の利用(調理)方法
        /// </summary>
        public virtual string directions { get; set; }

        /// <summary>
        /// style_name : スタイル名
        /// </summary>
        public string style_name { get; set; }

        /// <summary>
        /// flavor_name : フレーバー
        /// </summary>
        public virtual string flavor_name { get; set; }

        /// <summary>
        /// size_name : サイズ
        /// </summary>
        public virtual string size_name { get; set; }

        /// <summary>
        /// color_name : カラー
        /// </summary>
        public virtual string color_name { get; set; }

        /// <summary>
        /// color_map : カラーマップ
        /// </summary>
        public virtual string color_map { get; set; }

        /// <summary>
        /// scent_name : 香り
        /// </summary>
        public virtual string scent_name { get; set; }

        /// <summary>
        /// item_form : 商品の形状
        /// </summary>
        public virtual string item_form { get; set; }

        /// <summary>
        /// special_features1 : 特殊機能1
        /// </summary>
        public virtual string special_features1 { get; set; }

        /// <summary>
        /// special_features2 : 特殊機能2
        /// </summary>
        public virtual string special_features2 { get; set; }

        /// <summary>
        /// special_features3 : 特殊機能3
        /// </summary>
        public virtual string special_features3 { get; set; }

        /// <summary>
        /// minimum_weight_recommendation : 推奨最小重量
        /// </summary>
        public virtual string minimum_weight_recommendation { get; set; }

        /// <summary>
        /// maximum_weight_recommendation : 推奨最大重量
        /// </summary>
        public virtual string maximum_weight_recommendation { get; set; }

        /// <summary>
        /// weight_recommendation_unit_of_measure : 推奨重量の単位
        /// </summary>
        public virtual string weight_recommendation_unit_of_measure { get; set; }
        #endregion

        #region yahoo
        /// <summary>
        /// headline : キャッチコピー
        /// </summary>
        public string headline { get; set; }

        /// <summary>
        /// caption : 商品説明
        /// </summary>
        public string caption { get; set; }

        /// <summary>
        /// abstract : ひと言コメント
        /// </summary>
        public string abstract_ { get; set; }

        /// <summary>
        /// explanation : 商品情報
        /// </summary>
        public string explanation { get; set; }

        /// <summary>
        /// additional1 : フリースペース
        /// </summary>
        public string additional1 { get; set; }

        /// <summary>
        /// additional2 : フリースペース
        /// </summary>
        public string additional2 { get; set; }

        /// <summary>
        /// additional3 : フリースペース
        /// </summary>
        public string additional3 { get; set; }

        /// <summary>
        /// relevant-links : おすすめ商品
        /// </summary>
        public string relevant_links { get; set; }

        /// <summary>
        /// release-date : 発売日
        /// </summary>
        public string release_date { get; set; }

        /// <summary>
        /// point-code : ポイント倍率
        /// </summary>
        public int? point_code { get; set; }

        /// <summary>
        /// meta-key : META keywords
        /// </summary>
        public string meta_key { get; set; }

        /// <summary>
        /// meta-desc : META description
        /// </summary>
        public string meta_desc { get; set; }

        /// <summary>
        /// template : 使用中のテンプレート
        /// </summary>
        public string template { get; set; }

        /// <summary>
        /// sale-period-start : 販売期間（開始日）
        /// </summary>
        public DateTime? sale_period_start { get; set; }

        /// <summary>
        /// sale-period-end : 販売期間（終了日）
        /// </summary>
        public DateTime? sale_period_end { get; set; }

        /// <summary>
        /// sp-code : 販促コード
        /// </summary>
        public string sp_code { get; set; }

        /// <summary>
        /// brand-code : ブランドコード
        /// </summary>
        public string brand_code { get; set; }

        /// <summary>
        /// yahoo-product-code : Yahoo!ショッピング製品コード
        /// </summary>
        public string yahoo_product_code { get; set; }

        /// <summary>
        /// product-code : 製品コード
        /// </summary>
        public string product_code { get; set; }

        /// <summary>
        /// product-category : プロダクトカテゴリ
        /// </summary>
        public string product_category { get; set; }

        /// <summary>
        /// spec1 : スペック
        /// </summary>
        public string spec1 { get; set; }

        /// <summary>
        /// spec2 : スペック
        /// </summary>
        public string spec2 { get; set; }

        /// <summary>
        /// spec3 : スペック
        /// </summary>
        public string spec3 { get; set; }

        /// <summary>
        /// spec4 : スペック
        /// </summary>
        public string spec4 { get; set; }

        /// <summary>
        /// spec5 : スペック
        /// </summary>
        public string spec5 { get; set; }
        #endregion

        #region 楽天
        /// <summary>
        /// tag_id : タグＩＤ
        /// </summary>
        public string tag_id { get; set; }

        /// <summary>
        /// pc_slogan : PC用キャッチコピー
        /// </summary>
        public string pc_slogan { get; set; }

        /// <summary>
        /// mobile_slogan : モバイル用キャッチコピー
        /// </summary>
        public string mobile_slogan { get; set; }

        /// <summary>
        /// product_layout : 商品情報レイアウト
        /// </summary>
        public int? product_layout { get; set; }

        /// <summary>
        /// noshi : のし対応
        /// </summary>
        public int? noshi { get; set; }

        /// <summary>
        /// pc_description : PC用商品説明文
        /// </summary>
        public string pc_description { get; set; }

        /// <summary>
        /// mobile_description : モバイル用商品説明文
        /// </summary>
        public string mobile_description { get; set; }

        /// <summary>
        /// smartphone_description : スマートフォン用商品説明文
        /// </summary>
        public string smartphone_description { get; set; }

        /// <summary>
        /// pc_sale_description : PC用販売説明文
        /// </summary>
        public string pc_sale_description { get; set; }

        /// <summary>
        /// movie : 動画
        /// </summary>
        public string movie { get; set; }

        /// <summary>
        /// stock_display : 在庫数表示
        /// </summary>
        public int? stock_display { get; set; }

        /// <summary>
        /// black_market_pass : 闇市パスワード
        /// </summary>
        public string black_market_pass { get; set; }

        /// <summary>
        /// point_scale_rate : ポイント変倍率
        /// </summary>
        public int? point_scale_rate { get; set; }

        /// <summary>
        /// point_scale_rate_period : ポイント変倍率適用期間
        /// </summary>
        public DateTime? point_scale_rate_period { get; set; }

        /// <summary>
        /// header_footer_leftnavi : ヘッダー・フッター・レフトナビ
        /// </summary>
        public string header_footer_leftnavi { get; set; }

        /// <summary>
        /// display_order : 表示項目の並び順
        /// </summary>
        public string display_order { get; set; }

        /// <summary>
        /// common_description_small : 共通説明文（小）
        /// </summary>
        public string common_description_small { get; set; }

        /// <summary>
        /// deature_product : 目玉商品
        /// </summary>
        public string deature_product { get; set; }

        /// <summary>
        /// common_description_large : 共通説明文（大）
        /// </summary>
        public string common_description_large { get; set; }

        /// <summary>
        /// display_review_test : レビュー本文表示
        /// </summary>
        public int? display_review_test { get; set; }

        /// <summary>
        /// size_chart_link : サイズ表リンク
        /// </summary>
        public string size_chart_link { get; set; }

        /// <summary>
        /// drug_description : 医薬品説明文
        /// </summary>
        public string drug_description { get; set; }

        /// <summary>
        /// drug_notes : 医薬品注意事項
        /// </summary>
        public string drug_notes { get; set; }

        /// <summary>
        /// control_number_dual_price_word : 二重価格文言管理番号
        /// </summary>
        public string control_number_dual_price_word { get; set; }
        #endregion

        public virtual DateTime? intime { get; set; }
        public virtual DateTime? utime { get; set; }
        public virtual EnumDeleted deleted { get; set; }
        public virtual int? site_id { get; set; }
        public virtual EnumOnOff? mall_flg { get; set; }
        public virtual DateTime? last_image_upload_time { get; set; }
        public virtual EnumOnOff? sales_period_flg { get; set; }
    }
}

