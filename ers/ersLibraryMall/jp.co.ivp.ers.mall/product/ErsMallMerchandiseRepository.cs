﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.db;
using System.Data;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.product
{
    public class ErsMallMerchandiseRepository : ErsRepository<ErsMallMerchandise>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErsMallMerchandiseRepository()
            : base("mall_s_master_t")
        {
        }

        public IList<ErsMallMerchandise> FindWithMerchandise(ErsMallMerchandiseCriteria mallCriteria)
        {
            var searchMallMerchandiseSpec = ErsMallFactory.ersMallProductFactory.GetSearchMallMerchandiseSpec();
            var listRecord = searchMallMerchandiseSpec.GetSearchData(mallCriteria);

            return base.CreateList(listRecord);
        }
    }
}
