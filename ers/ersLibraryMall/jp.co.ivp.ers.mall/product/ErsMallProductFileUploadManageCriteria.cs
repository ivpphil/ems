﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品ファイルアップロード管理クライテリア [Criteria for mall product file upload management table]
    /// </summary>
    public class ErsMallProductFileUploadManageCriteria
        : Criteria
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_file_upload_manage_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// アクティブ[Active]
        /// </summary>
        public virtual EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_file_upload_manage_t.active", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_file_upload_manage_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// モール店舗区分 [Mall shop type]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_file_upload_manage_t.mall_shop_kbn", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// モール商品アップロードファイルタイプ [Mall product upload file type]
        /// </summary>
        public virtual EnumMallProductUploadFileType? file_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_file_upload_manage_t.file_type", (int)value, Operation.EQUAL));
            }
        }


        /// <summary>
        /// IDソート [For sorting of ID]
        /// </summary> 
        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("mall_product_file_upload_manage_t.id", orderBy);
        }
    }
}
