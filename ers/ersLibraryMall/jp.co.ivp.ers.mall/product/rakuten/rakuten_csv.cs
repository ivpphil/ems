﻿using System.ComponentModel;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.product.rakuten
{
    /// <summary>
    /// 楽天用CSVモデル [CSV Model for Rakuten]
    /// </summary>
    public class rakuten_csv
        : ErsModelBase
    {
        /// <summary>
        /// control_column : コントロールカラム
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.control_column")]
        public string control_column { get; set; }

        /// <summary>
        /// product_url : 商品管理番号（商品URL）
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.product_url")]
        public string product_url { get; set; }

        /// <summary>
        /// product_number : 商品番号
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.product_number")]
        public string product_number { get; set; }

        /// <summary>
        /// all_products_dir_id : 全商品ディレクトリID
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.all_products_dir_id")]
        public int? all_products_dir_id { get; set; }

        /// <summary>
        /// tag_id : タグＩＤ
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.tag_id")]
        public string tag_id { get; set; }

        /// <summary>
        /// pc_slogan : PC用キャッチコピー
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.pc_slogan")]
        public string pc_slogan { get; set; }

        /// <summary>
        /// mobile_slogan : モバイル用キャッチコピー
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.mobile_slogan")]
        public string mobile_slogan { get; set; }

        /// <summary>
        /// product_name : 商品名
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.product_name")]
        public string product_name { get; set; }

        /// <summary>
        /// selling_price : 販売価格
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.selling_price")]
        public int? selling_price { get; set; }

        /// <summary>
        /// display_price : 表示価格
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.display_price")]
        public int? display_price { get; set; }

        /// <summary>
        /// tax : 消費税
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.tax")]
        public int? tax { get; set; }

        /// <summary>
        /// shipping_cost : 送料
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.shipping_cost")]
        public int? shipping_cost { get; set; }

        /// <summary>
        /// extra_shipping_cost : 個別送料
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.extra_shipping_cost")]
        public string extra_shipping_cost { get; set; }

        /// <summary>
        /// shipping_cost_cate1 : 送料区分1
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.shipping_cost_cate1")]
        public string shipping_cost_cate1 { get; set; }

        /// <summary>
        /// shipping_cost_cate2 : 送料区分2
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.shipping_cost_cate2")]
        public string shipping_cost_cate2 { get; set; }

        /// <summary>
        /// cod_cost : 代引料
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.cod_cost")]
        public int? cod_cost { get; set; }

        /// <summary>
        /// warehouse : 倉庫指定
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.warehouse")]
        public int? warehouse { get; set; }

        /// <summary>
        /// product_layout : 商品情報レイアウト
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.product_layout")]
        public int? product_layout { get; set; }

        /// <summary>
        /// order_button : 注文ボタン
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.order_button")]
        public int? order_button { get; set; }

        /// <summary>
        /// request_button : 資料請求ボタン
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.request_button")]
        public int? request_button { get; set; }

        /// <summary>
        /// product_inquiry_button : 商品問い合わせボタン
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.product_inquiry_button")]
        public int? product_inquiry_button { get; set; }

        /// <summary>
        /// restock_button : 再入荷お知らせボタン
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.restock_button")]
        public int? restock_button { get; set; }

        /// <summary>
        /// mobile_diplay : モバイル表示
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.mobile_diplay")]
        public int? mobile_diplay { get; set; }

        /// <summary>
        /// noshi : のし対応
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.noshi")]
        public int? noshi { get; set; }

        /// <summary>
        /// pc_description : PC用商品説明文
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.pc_description")]
        public string pc_description { get; set; }

        /// <summary>
        /// mobile_description : モバイル用商品説明文
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.mobile_description")]
        public string mobile_description { get; set; }

        /// <summary>
        /// smartphone_description : スマートフォン用商品説明文
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.smartphone_description")]
        public string smartphone_description { get; set; }

        /// <summary>
        /// pc_sale_description : PC用販売説明文
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.pc_sale_description")]
        public string pc_sale_description { get; set; }

        /// <summary>
        /// image_url : 商品画像URL
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.image_url")]
        public string image_url { get; set; }

        /// <summary>
        /// image_alt : 商品画像名（ALT）
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.image_alt")]
        public string image_alt { get; set; }

        /// <summary>
        /// movie : 動画
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.movie")]
        public string movie { get; set; }

        /// <summary>
        /// sales_period : 販売期間指定
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.sales_period")]
        public string sales_period { get; set; }

        /// <summary>
        /// order_accept_count : 注文受付数
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.order_accept_count")]
        public int? order_accept_count { get; set; }

        /// <summary>
        /// stock_type : 在庫タイプ
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.stock_type")]
        public int? stock_type { get; set; }

        /// <summary>
        /// stock : 在庫数
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.stock")]
        public int? stock { get; set; }

        /// <summary>
        /// stock_display : 在庫数表示
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.stock_display")]
        public int? stock_display { get; set; }

        /// <summary>
        /// item_h_axis_name : 項目選択肢別在庫用横軸項目名
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.item_h_axis_name")]
        public string item_h_axis_name { get; set; }

        /// <summary>
        /// item_v_axis_name : 項目選択肢別在庫用縦軸項目名
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.item_v_axis_name")]
        public string item_v_axis_name { get; set; }

        /// <summary>
        /// tem_remain_display_threshold : 項目選択肢別在庫用残り表示閾値
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.tem_remain_display_threshold")]
        public string tem_remain_display_threshold { get; set; }

        /// <summary>
        /// rac_number : RAC番号
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.rac_number")]
        public string rac_number { get; set; }

        /// <summary>
        /// search_hide : サーチ非表示
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.search_hide")]
        public int? search_hide { get; set; }

        /// <summary>
        /// black_market_pass : 闇市パスワード
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.black_market_pass")]
        public string black_market_pass { get; set; }

        /// <summary>
        /// caralog_id : カタログID
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.caralog_id")]
        public string caralog_id { get; set; }

        /// <summary>
        /// restore_stock_flg : 在庫戻しフラグ
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.restore_stock_flg")]
        public int? restore_stock_flg { get; set; }

        /// <summary>
        /// order_reception_out_of_stock : 在庫切れ時の注文受付
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.order_reception_out_of_stock")]
        public string order_reception_out_of_stock { get; set; }

        /// <summary>
        /// delivery_control_number_in_stock : 在庫あり時納期管理番号
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.delivery_control_number_in_stock")]
        public string delivery_control_number_in_stock { get; set; }

        /// <summary>
        /// delivery_control_number_out_of_stock : 在庫切れ時納期管理番号
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.delivery_control_number_out_of_stock")]
        public string delivery_control_number_out_of_stock { get; set; }

        /// <summary>
        /// reserve_release_date : 予約商品発売日
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.reserve_release_date")]
        public string reserve_release_date { get; set; }

        /// <summary>
        /// point_scale_rate : ポイント変倍率
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.point_scale_rate")]
        public int? point_scale_rate { get; set; }

        /// <summary>
        /// point_scale_rate_period : ポイント変倍率適用期間
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.point_scale_rate_period")]
        public string point_scale_rate_period { get; set; }

        /// <summary>
        /// header_footer_leftnavi : ヘッダー・フッター・レフトナビ
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.header_footer_leftnavi")]
        public string header_footer_leftnavi { get; set; }

        /// <summary>
        /// display_order : 表示項目の並び順
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.display_order")]
        public string display_order { get; set; }

        /// <summary>
        /// common_description_small : 共通説明文（小）
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.common_description_small")]
        public string common_description_small { get; set; }

        /// <summary>
        /// deature_product : 目玉商品
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.deature_product")]
        public string deature_product { get; set; }

        /// <summary>
        /// common_description_large : 共通説明文（大）
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.common_description_large")]
        public string common_description_large { get; set; }

        /// <summary>
        /// display_review_test : レビュー本文表示
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.display_review_test")]
        public int? display_review_test { get; set; }

        /// <summary>
        /// control_number_asuraku : あす楽配送管理番号
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.control_number_asuraku")]
        public int? control_number_asuraku { get; set; }

        /// <summary>
        /// control_number_delivery_overseas : 海外配送管理番号
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.control_number_delivery_overseas")]
        public int? control_number_delivery_overseas { get; set; }

        /// <summary>
        /// size_chart_link : サイズ表リンク
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.size_chart_link")]
        public string size_chart_link { get; set; }

        /// <summary>
        /// drug_description : 医薬品説明文
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.drug_description")]
        public string drug_description { get; set; }

        /// <summary>
        /// drug_notes : 医薬品注意事項
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.drug_notes")]
        public string drug_notes { get; set; }

        /// <summary>
        /// control_number_dual_price_word : 二重価格文言管理番号
        /// </summary>
        [CsvField]
        [DisplayName("rakuten.control_number_dual_price_word")]
        public string control_number_dual_price_word { get; set; }
    }
}
