﻿using System.ComponentModel;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.product.rakuten
{
    /// <summary>
    /// 楽天カテゴリ用CSVモデル [CSV Model for Rakuten category]
    /// </summary>
    public class rakuten_cat_csv
        : ErsModelBase
    {
        /// <summary>
        /// control_column : コントロールカラム
        /// </summary>
        [CsvField]
        [DisplayName("rakuten_cat.control_column")]
        public string control_column { get; set; }

        /// <summary>
        /// product_url : 商品管理番号（商品URL）
        /// </summary>
        [CsvField]
        [DisplayName("rakuten_cat.product_url")]
        public string product_url { get; set; }

        /// <summary>
        /// product_name : 商品名
        /// </summary>
        [CsvField]
        [DisplayName("rakuten_cat.product_name")]
        public string product_name { get; set; }

        /// <summary>
        /// display_category : 表示先カテゴリ
        /// </summary>
        [CsvField]
        [DisplayName("rakuten_cat.display_category")]
        public string display_category { get; set; }

        /// <summary>
        /// prioroty : 優先度
        /// </summary>
        [CsvField]
        [DisplayName("rakuten_cat.prioroty")]
        public string prioroty { get; set; }

        /// <summary>
        /// url : URL
        /// </summary>
        [CsvField]
        [DisplayName("rakuten_cat.url")]
        public string url { get; set; }

        /// <summary>
        /// multi_style_per_page : 1ページ複数形式
        /// </summary>
        [CsvField]
        [DisplayName("rakuten_cat.multi_style_per_page")]
        public string multi_style_per_page { get; set; }
    }
}
