﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.common;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.merchandise.category;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.product.rakuten
{
    /// <summary>
    /// 楽天CSVマッパー [Rakuten TSV mapper]
    /// </summary>
    public class RakutenTsvMapper
    {
        #region 定数 [Constant]
        /// <summary>
        /// 楽天CSV分割数 [Rakuten CSV divide count]
        /// </summary>
        protected const int RAKUTEN_CSV_DIVIDE_COUNT = 50000;
        #endregion


        #region 商品情報 [Product information]
        /// <summary>
        /// マップ [Map]
        /// </summary>
        /// <param name="dirPath">出力ディレクトリパス [Output directory path]</param>
        /// <param name="delGetFileName">ファイル名取得デリゲート [Delegate for Get file name]</param>
        /// <param name="listMallProductTmp">商品リスト [The list of products]</param>
        /// <param name="extractDateTime">抽出日時 [Extract datetime]</param>
        /// <returns>出力ファイルパスリスト [The list of output file path]</returns>
        public IEnumerable<string> Map(string dirPath, GetFileName delGetFileName, IList<ErsMallProductTmp> listMallProductTmp, ErsCommonStruct.DateTimeStartEnd extractDateTime)
        {
            var creater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();

            int count = listMallProductTmp.Count();

            // 分割数 [Divide count]
            int divide = (int)Math.Ceiling(count / (double)RAKUTEN_CSV_DIVIDE_COUNT);

            for (var i = 0; i < divide; i++)
            {
                var fileName = delGetFileName(listMallProductTmp.First().site_id.Value, i);

                // CSVファイル生成 [Create the CSV file]
                using (var writer = creater.GetWriter(dirPath, fileName))
                {
                    var csv = ErsMallFactory.ersMallProductFactory.GetRakutenCsvModel();

                    // ヘッダ [Header]
                    creater.WriteCsvHeader<rakuten_csv>(writer);

                    for (var j = i * RAKUTEN_CSV_DIVIDE_COUNT; j < (i + 1) * RAKUTEN_CSV_DIVIDE_COUNT; j++)
                    {
                        if (j >= count)
                        {
                            break;
                        }

                        // 楽天CSVデータセット [Get the CSV for Rakuten]
                        csv = this.SetRakutenCsv(csv, listMallProductTmp[j], extractDateTime);

                        // ボディ [Body]
                        creater.WriteBody(csv, writer);
                    }
                }

                yield return fileName;
            }
        }

        /// <summary>
        /// ファイル名取得（デリゲート） [Get file name (Delegate)]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="divide">分割カウント [divide count]</param>
        /// <returns>ファイル名 [File name]</returns>
        public delegate string GetFileName(int siteId, int divide);

        /// <summary>
        /// 楽天CSVデータセット [Get the CSV for Rakuten]
        /// </summary>
        /// <param name="csv">CSVモデル [CSV model]</param>
        /// <param name="product">商品 [Product]</param>
        /// <param name="extractDateTime">抽出日時 [Extract datetime]</param>
        /// <returns>CSVモデル [CSV model]</returns>
        protected virtual rakuten_csv SetRakutenCsv(rakuten_csv csv, ErsMallProductTmp product, ErsCommonStruct.DateTimeStartEnd extractDateTime)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var operationType = ErsMallFactory.ersMallProductFactory.GetObtainProductOperationTypeStgy().Obtain(product, extractDateTime.dateFrom.Value, extractDateTime.dateTo.Value);

            var deletedValue = " ";
            
            csv.control_column = operationType == EnumMallProductOperationType.REGIST ? "n" : "u";

            csv.product_url = product.mall_scode.ToLower();
            csv.product_number = product.mall_scode;

            csv.all_products_dir_id = product.manage_id;
            csv.tag_id = product.tag_id;

            csv.pc_slogan = product.pc_slogan ?? deletedValue;
            csv.mobile_slogan = product.mobile_slogan ?? deletedValue;

            csv.product_name = product.sname;

            csv.selling_price = product.price;
            csv.display_price = product.p_price2.HasValue ? product.p_price2 : null;
            csv.tax = setup.tax == 0 ? 1 : 0;
            csv.shipping_cost = 0;
            csv.cod_cost = 0;
            csv.warehouse = operationType != EnumMallProductOperationType.DELETE ? 0 : 1;

            csv.product_layout = product.product_layout;
            csv.order_button = 1;
            csv.request_button = 0;
            csv.product_inquiry_button = 0;
            csv.restock_button = 0;
            csv.mobile_diplay = 1;
            csv.noshi = product.noshi; //チェックボックス

            // HTML レベル２
            var pc_description = product.pc_description;
            csv.pc_description = pc_description.HasValue() ? pc_description.Replace(Environment.NewLine, ErsViewHelper.TAG_NEW_LINE) : deletedValue;
            csv.mobile_description = product.mobile_description ?? deletedValue;
            csv.smartphone_description = product.smartphone_description ?? deletedValue;

            // HTML レベル２
            var pc_sale_description = product.pc_sale_description;
            csv.pc_sale_description = pc_sale_description.HasValue() ? pc_sale_description.Replace(Environment.NewLine, ErsViewHelper.TAG_NEW_LINE) : deletedValue;

            csv.image_url = this.GetImageUrl(product.site_id, product.gcode, product.scode);
            csv.movie = product.movie;

            csv.sales_period = product.date_from.HasValue && product.date_to.HasValue ? string.Format("{0} {1}", product.date_from.Value.ToString("yyyy/MM/dd HH:mm"), product.date_to.Value.ToString("yyyy/MM/dd HH:mm")) : string.Empty;
            csv.order_accept_count = product.max_purchase_count;

            csv.stock_type = 1;
            var objStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(product.scode);
            csv.stock = objStock != null ? objStock.stock : 0;
            csv.stock_display = product.stock_display;//必須

            csv.search_hide = 0;
            csv.black_market_pass = product.black_market_pass;
            csv.caralog_id = product.jancode;

            csv.restore_stock_flg = 0;

            csv.point_scale_rate = product.point_scale_rate;
            csv.point_scale_rate_period = product.point_scale_rate_period.HasValue ? product.point_scale_rate_period.Value.ToString("ddHH_yyyyMM") : null;
            csv.header_footer_leftnavi = product.header_footer_leftnavi;
            csv.display_order = product.display_order;
            csv.common_description_small = product.common_description_small;
            csv.deature_product = product.deature_product;
            csv.common_description_large = product.common_description_large;
            csv.display_review_test = product.display_review_test;

            //csv.control_number_asuraku = 0;
            //csv.control_number_delivery_overseas = 0;

            csv.size_chart_link = product.size_chart_link;
            //以下は医薬品販売の許可が必要
            //csv.drug_description = product.drug_description ?? deletedValue;
            //csv.drug_notes = product.drug_notes ?? deletedValue;
            csv.control_number_dual_price_word = product.control_number_dual_price_word;

            return csv;
        }
        #endregion

        #region カテゴリ情報 [Category information]
        /// <summary>
        /// カテゴリマップ [Map category]
        /// </summary>
        /// <param name="dirPath">出力ディレクトリパス [Output directory path]</param>
        /// <param name="delGetFileName">ファイル名取得デリゲート [Delegate for Get file name]</param>
        /// <param name="listMallProductTmp">商品リスト [The list of products]</param>
        /// <param name="extractDateTime">抽出日時 [Extract datetime]</param>
        /// <returns>出力ファイルパスリスト [The list of output file path]</returns>
        public IEnumerable<string> MapCategory(string dirPath, GetFileNameCategory delGetFileName, IList<ErsMallProductTmp> listMallProductTmp, ErsCommonStruct.DateTimeStartEnd extractDateTime)
        {
            var creater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();

            int count = listMallProductTmp.Count();

            // 分割数 [Divide count]
            int divide = (int)Math.Ceiling(count / (double)RAKUTEN_CSV_DIVIDE_COUNT);

            for (var i = 0; i < divide; i++)
            {
                var fileName = delGetFileName(listMallProductTmp.First().site_id.Value, i);

                using (var writer = creater.GetWriter(dirPath, fileName))
                {
                    var csv = ErsMallFactory.ersMallProductFactory.GetRakutenCategoryCsvModel();

                    // ヘッダ [Header]
                    creater.WriteCsvHeader<rakuten_cat_csv>(writer);

                    for (var j = i * RAKUTEN_CSV_DIVIDE_COUNT; j < (i + 1) * RAKUTEN_CSV_DIVIDE_COUNT; j++)
                    {
                        if (j >= count)
                        {
                            break;
                        }

                        // 楽天カテゴリCSVデータセット [Get the CSV for Rakuten category]
                        csv = this.SetRakutenCategoryCsv(csv, listMallProductTmp[j], extractDateTime);

                        // ボディ [Body]
                        creater.WriteBody(csv, writer);
                    }
                }

                yield return creater.filePath;
            }
        }

        /// <summary>
        /// カテゴリファイル名取得（デリゲート） [Get file name category (Delegate)]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="divide">分割カウント [divide count]</param>
        /// <returns>ファイル名 [File name]</returns>
        public delegate string GetFileNameCategory(int siteId, int divide);

        /// <summary>
        /// 楽天カテゴリCSVデータセット [Get the CSV for Rakuten category]
        /// </summary>
        /// <param name="csv">CSVモデル [CSV model]</param>
        /// <param name="product">商品 [Product]</param>
        /// <param name="extractDateTime">抽出日時 [Extract datetime]</param>
        /// <returns>CSVモデル [CSV model]</returns>
        protected virtual rakuten_cat_csv SetRakutenCategoryCsv(rakuten_cat_csv csv, ErsMallProductTmp product, ErsCommonStruct.DateTimeStartEnd extractDateTime)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var setupMall = ErsMallFactory.ersMallBatchFactory.getSetup();
            var operationType = ErsMallFactory.ersMallProductFactory.GetObtainProductOperationTypeStgy().Obtain(product, extractDateTime.dateFrom.Value, extractDateTime.dateTo.Value);

            csv.control_column = operationType == EnumMallProductOperationType.REGIST ? "n" : "u";

            csv.product_url = product.mall_scode;

            var separator = "\\";
            var viewService = ErsFactory.ersViewServiceFactory.GetErsViewCategoryService();
            string display_category = string.Empty;
            //デバッグ時は、IVP用のルートディレクトリを付与
            if (setup.debug)
            {
                display_category = separator + setupMall.ivpProductCategoryPath;
            }

            for (var i = ErsCategory.minCategoryNumber; i <= ErsCategory.maxCategoryNumber; i++)
            {
                var cate = ErsExpressionAccessor<ErsMallProductTmp, int[]>.GetPropertyValue(product, "cate" + i);
                if (cate != null)
                {
                    display_category += separator + viewService.GetStringFromId(1, cate[0]);
                }
            }

            if (!string.IsNullOrEmpty(display_category))
            {
                csv.display_category = display_category.Substring(separator.Length);
            }

            csv.prioroty = "999999999";

            return csv;
        }
        #endregion

        #region 画像URL取得 [Get image URL]
        /// <summary>
        /// 画像URL取得 [Get image URL]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="gcode">グループコード [ITEM code]</param>
        /// <param name="scode">商品コード [SKU code]</param>
        /// <returns>画像URL [Image URL]</returns>
        protected virtual string GetImageUrl(int? siteId, string gcode, string scode)
        {
            var listRet = new List<string>();
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var setupMall = ErsMallFactory.ersMallBatchFactory.getSetup();
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();

            var shopUrl = mallSetup.GetRakutenMallShopUrl(siteId.Value);

            // SKU [SKU]
            for (var i = 0; i < ErsCommonConst.MERCHANDISE_IMAGE_NUM; i++)
            {
                var index = i + 1;

                var filePath = string.Format(ErsCommonConst.SKU_IMAGE_PATH_FORMAT, setup.image_directory, scode, index);

                if (!File.Exists(filePath))
                {
                    continue;
                }

                var dirName = !setupMall.rakutenFTPImageDir.HasValue() ? string.Empty : setupMall.rakutenFTPImageDir + "/";
                var fileName = Path.GetFileName(filePath).Replace(scode, ErsMallCommonService.GetMallSkuFromErs(scode)).ToLower();

                // [URL]/[User ID]/cabinet/([Directory]/)[File name]
                listRet.Add(string.Format(setupMall.rakutenImageUrlFormat, shopUrl) + dirName + fileName);
            }

            return listRet.Count > 0 ? String.Join(" ", listRet) : null;
        }
        #endregion
    }
}
