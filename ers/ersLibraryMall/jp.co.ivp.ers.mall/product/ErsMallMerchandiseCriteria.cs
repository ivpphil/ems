﻿using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;
using System;

namespace jp.co.ivp.ers.mall.product
{
    public class ErsMallMerchandiseCriteria
        : Criteria, IItemSearchCriteria
    {
        public int? id
        {
            set { this.Add(Criteria.GetCriterion("mall_s_master_t.id", value, Operation.EQUAL)); }
        }

        public string gcode
        {
            set { this.Add(Criteria.GetCriterion("mall_s_master_t.gcode", value, Operation.EQUAL)); }
        }

        public string scode
        {
            set { this.Add(Criteria.GetCriterion("mall_s_master_t.scode", value, Operation.EQUAL)); }
        }

        public string scode_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_s_master_t.scode", value, Criteria.Operation.NOT_EQUAL));
            }
        }

        public int? site_id
        {
            set { this.Add(Criteria.GetCriterion("mall_s_master_t.site_id", value, Operation.EQUAL)); }
        }

        public EnumMallShopKbn? mall_shop_kbn
        {
            set { this.Add(Criteria.GetCriterion("mall_s_master_t.mall_shop_kbn", (int)value, Operation.EQUAL)); }
        }

        public EnumOnOff? mall_flg
        {
            set { this.Add(Criteria.GetCriterion("mall_s_master_t.mall_flg", value, Operation.EQUAL)); }
        }

        public EnumDeleted deleted
        {
            set { this.Add(Criteria.GetCriterion("mall_s_master_t.deleted", value, Operation.EQUAL)); }
        }

        public IEnumerable<EnumMallShopKbn> mall_shop_kbn_in
        {
            set { this.Add(Criteria.GetInClauseCriterion("mall_s_master_t.mall_shop_kbn", value)); }
        }

        public IEnumerable<string> scode_in
        {
            set { this.Add(Criteria.GetInClauseCriterion("mall_s_master_t.scode", value)); }
        }

        /// <summary>
        /// 楽天キャンペーン対象 [Rakuten campaign]
        /// </summary>
        public void SetRakutenCampign()
        {
            this.Add(Criteria.GetCriterion("mall_s_master_t.sku_name", null, Operation.NOT_EQUAL));
            this.Add(Criteria.GetCriterion("mall_s_master_t.opt_date_1", null, Operation.NOT_EQUAL));
            this.Add(Criteria.GetCriterion("mall_s_master_t.opt_date_2", null, Operation.NOT_EQUAL));

            this.Add(Criteria.GetCriterion("mall_s_master_t.mall_flg", (int)EnumOnOff.On, Criteria.Operation.EQUAL));
        }

        /// <summary>
        /// モール販売期間商品更新対象（UpdateMallSalesPeriodProducts 専用） [Mall sales period (Use for UpdateMallSalesPeriodProducts only)]
        /// </summary>
        public void SetMallSalesEnd()
        {
            var listMallCriteria = new List<CriterionBase>();
            var listRakutenCriteria = new List<CriterionBase>();
            var listAmazonCriteria = new List<CriterionBase>();

            listRakutenCriteria.Add(Criteria.GetCriterion("mall_s_master_t.mall_flg", (int)EnumOnOff.On, Operation.EQUAL));
            listRakutenCriteria.Add(Criteria.GetCriterion("mall_s_master_t.mall_shop_kbn", EnumMallShopKbn.RAKUTEN, Operation.EQUAL));
            listMallCriteria.Add(Criteria.JoinWithAnd(listRakutenCriteria));

            listAmazonCriteria.Add(Criteria.GetCriterion("mall_s_master_t.mall_flg", (int)EnumOnOff.On, Operation.EQUAL));
            listAmazonCriteria.Add(Criteria.GetCriterion("mall_s_master_t.mall_shop_kbn", EnumMallShopKbn.AMAZON, Operation.EQUAL));
            listMallCriteria.Add(Criteria.JoinWithAnd(listAmazonCriteria));

            this.Add(Criteria.JoinWithOR(listMallCriteria));

            var sqlTarget =
                "CASE WHEN now() BETWEEN g_master_t.date_from AND g_master_t.date_to THEN 0 "
                + "ELSE 1 END <> COALESCE(mall_s_master_t.sales_period_flg, 0)";
            this.Add(Criteria.GetUniversalCriterion(sqlTarget));
        }

        public void SetOrderByScode(OrderBy orderBy)
        {
            this.AddOrderBy("mall_s_master_t.scode", orderBy);
        }

        public void SetOrderBySiteId(OrderBy orderBy)
        {
            this.AddOrderBy("mall_s_master_t.site_id", orderBy);
        }

        /// <summary>
        ///  ソート：id [Sorting : id]
        /// </summary>
        /// <param name="orderBy">ソート [Sorting]</param>
        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("mall_s_master_t.id", orderBy);
        }

        public void SetGroupByScode()
        {
            this.AddGroupBy("mall_s_master_t.scode");
        }

        public string[] ignore_gcode
        {
            set
            {
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
                criteria.ignore_gcode = value;
                this.Add(criteria);
            }
        }

        public string jancode
        {
            set
            {
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
                criteria.jancode = value;
                this.Add(criteria);
            }
        }

        public string sname_and_gname
        {
            set
            {
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
                criteria.sname_and_gname = value;
                this.Add(criteria);
            }
        }

        public int? cate1
        {
            set
            {
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
                criteria.cate1 = value;
                this.Add(criteria);
            }
        }

        public int? cate2
        {
            set
            {
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
                criteria.cate2 = value;
                this.Add(criteria);
            }
        }

        public int? cate3
        {
            set
            {
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
                criteria.cate3 = value;
                this.Add(criteria);
            }
        }

        public int? cate4
        {
            set
            {
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
                criteria.cate4 = value;
                this.Add(criteria);
            }
        }

        public int? cate5
        {
            set
            {
                var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
                criteria.cate5 = value;
                this.Add(criteria);
            }
        }

        /// <summary>
        /// モール商品登録用 [For register mall products]
        /// SearchMallProductsSpec用
        /// </summary>
        /// <param name="from">検索日時FROM [Search datetime from]</param>
        /// <param name="to">検索日時TO [Search datetime from]</param>
        public void SetSearchMallProductForRegister(DateTime from, DateTime to)
        {
            var dicDateTime = new Dictionary<string, object>();

            dicDateTime.Add("intime_utime_from", from);
            dicDateTime.Add("intime_utime_to", to);

            var listDateTimeCriteria = new List<CriterionBase>();

            // s_master_t.intime or utime
            var sqlSDateTime = "CASE WHEN s_master_t.utime IS NULL THEN s_master_t.intime ELSE s_master_t.utime END BETWEEN :intime_utime_from AND :intime_utime_to";
            listDateTimeCriteria.Add(Criteria.GetUniversalCriterion(sqlSDateTime, dicDateTime));

            // g_master_t.intime or utime
            var sqlGDateTime = "CASE WHEN g_master_t.utime IS NULL THEN g_master_t.intime ELSE g_master_t.utime END BETWEEN :intime_utime_from AND :intime_utime_to";
            listDateTimeCriteria.Add(Criteria.GetUniversalCriterion(sqlGDateTime, dicDateTime));

            // mall_s_master_t.intime or utime
            var sqlMsDateTime = "mall_s_master_t.id IS NOT NULL AND (CASE WHEN mall_s_master_t.utime IS NULL THEN mall_s_master_t.intime ELSE mall_s_master_t.utime END BETWEEN :intime_utime_from AND :intime_utime_to)";
            listDateTimeCriteria.Add(Criteria.GetUniversalCriterion(sqlMsDateTime, dicDateTime));

            // price_t.intime or utime
            var sqlPDateTime = "price_t.scode IS NOT NULL AND (CASE WHEN price_t.utime IS NULL THEN price_t.intime ELSE price_t.utime END BETWEEN :intime_utime_from AND :intime_utime_to)";
            listDateTimeCriteria.Add(Criteria.GetUniversalCriterion(sqlPDateTime, dicDateTime));

            this.Add((Criteria.JoinWithOR(listDateTimeCriteria)));


            // 基本条件 [Basic condition]
            var dicBasicParam = new Dictionary<string, object>();

            dicBasicParam.Add("mall_flg_on", EnumOnOff.On);

            var sqlBasicCondition =
                "(CASE WHEN mall_product_tmp_t.id IS NULL THEN "
                    + "CASE WHEN mall_s_master_t.mall_flg = :mall_flg_on THEN true ELSE false END "
                + "ELSE "
                    + "true "
                + "END)";

            this.Add(Criteria.GetUniversalCriterion(sqlBasicCondition, dicBasicParam));
        }

        /// <summary>
        /// モール販売判定 [Judgment mall selling]
        /// </summary>
        public virtual void mall_site_id(int? site_id)
        {
            var sql = "EXISTS(SELECT * FROM mall_s_master_t WHERE scode = s_master_t.scode AND mall_flg = :mall_flg AND site_id = :site_id)";
            var values = new Dictionary<string, object>() {
                { "mall_flg", EnumOnOff.On },
                { "site_id", site_id }};

            this.Add(Criteria.GetUniversalCriterion(sql, values));
        }
    }
}
