﻿using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品画像抽出管理リポジトリ [Repository for extract mall product image]
    /// </summary>
    public class ErsMallProductImageExtractRepository
        : ErsRepository<ErsMallProductImageExtract>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="type">DBタイプ [Type of DB]</param>
        public ErsMallProductImageExtractRepository()
            : base("mall_product_image_extract_t")
        {
        }
    }
}
