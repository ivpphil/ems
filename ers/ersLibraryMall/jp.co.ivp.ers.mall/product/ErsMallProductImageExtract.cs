﻿using System;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品画像抽出管理エンティティ [Entity for extract mall product image]
    /// </summary>
    public class ErsMallProductImageExtract
        : ErsRepositoryEntity
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 登録日時 [Inserted datetime]
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新日時 [Updated datetime]
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// アクティブ [Active]
        /// </summary>
        public virtual EnumActive? active { get; set; }

        /// <summary>
        /// モール店舗区分 [Mall shop type]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        public virtual int? site_id { get; set; }

        /// <summary>
        /// 抽出日時 [Extract date]
        /// </summary>
        public virtual DateTime? extract_date { get; set; }
    }
}
