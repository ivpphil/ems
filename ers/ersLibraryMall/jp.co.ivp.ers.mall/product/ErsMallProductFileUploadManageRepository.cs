﻿using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品ファイルアップロード管理リポジトリ [Repository for mall product file upload management table]
    /// </summary>
    public class ErsMallProductFileUploadManageRepository
        : ErsRepository<ErsMallProductFileUploadManage>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public ErsMallProductFileUploadManageRepository()
            : base("mall_product_file_upload_manage_t")
        {
        }
    }
}
