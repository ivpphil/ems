﻿using System.ComponentModel;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.product.yahoo
{
    /// <summary>
    /// Yahoo!用CSVモデル [CSV Model for Yahoo!]
    /// </summary>
    public class yahoo_csv
        : ErsModelBase
    {
        /// <summary>
        /// path : パス
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.path")]
        public string path { get; set; }

        /// <summary>
        /// name : 商品名
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.name")]
        public string name { get; set; }

        /// <summary>
        /// code : 商品コード
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.code")]
        public string code { get; set; }

        /// <summary>
        /// sub-code : 個別商品コード
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.sub_code")]
        public string sub_code { get; set; }

        /// <summary>
        /// original-price : 定価
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.original_price")]
        public int? original_price { get; set; }

        /// <summary>
        /// price : 通常販売価格
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.price")]
        public int? price { get; set; }

        /// <summary>
        /// sale-price : 特価
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.sale_price")]
        public int? sale_price { get; set; }

        /// <summary>
        /// options : オプション
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.options")]
        public string options { get; set; }

        /// <summary>
        /// headline : キャッチコピー
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.headline")]
        public string headline { get; set; }

        /// <summary>
        /// caption : 商品説明
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.caption")]
        public string caption { get; set; }

        /// <summary>
        /// abstract : ひと言コメント
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.abstract")]
        public string abstract_ { get; set; }

        /// <summary>
        /// explanation : 商品情報
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.explanation")]
        public string explanation { get; set; }

        /// <summary>
        /// additional1 : フリースペース
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.additional1")]
        public string additional1 { get; set; }

        /// <summary>
        /// additional2 : フリースペース
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.additional2")]
        public string additional2 { get; set; }

        /// <summary>
        /// additional3 : フリースペース
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.additional3")]
        public string additional3 { get; set; }

        /// <summary>
        /// relevant-links : おすすめ商品
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.relevant_links")]
        public string relevant_links { get; set; }

        /// <summary>
        /// ship-weight : 重量
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.ship_weight")]
        public string ship_weight { get; set; }

        /// <summary>
        /// taxable : 課税対象
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.taxable")]
        public string taxable { get; set; }

        /// <summary>
        /// release-date : 発売日
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.release_date")]
        public string release_date { get; set; }

        /// <summary>
        /// temporary-point-term : 仮ポイント期間
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.temporary_point_term")]
        public string temporary_point_term { get; set; }

        /// <summary>
        /// point-code : ポイント倍率
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.point_code")]
        public int? point_code { get; set; }

        /// <summary>
        /// meta-key : META keywords
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.meta_key")]
        public string meta_key { get; set; }

        /// <summary>
        /// meta-desc : META description
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.meta_desc")]
        public string meta_desc { get; set; }

        /// <summary>
        /// template : 使用中のテンプレート
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.template")]
        public string template { get; set; }

        /// <summary>
        /// sale-period-start : 販売期間（開始日）
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.sale_period_start")]
        public string sale_period_start { get; set; }

        /// <summary>
        /// sale-period-end : 販売期間（終了日）
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.sale_period_end")]
        public string sale_period_end { get; set; }

        /// <summary>
        /// sale-limit : 購入数制限
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.sale_limit")]
        public int? sale_limit { get; set; }

        /// <summary>
        /// sp-code : 販促コード
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.sp_code")]
        public string sp_code { get; set; }

        /// <summary>
        /// brand-code : ブランドコード
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.brand_code")]
        public string brand_code { get; set; }

        /// <summary>
        /// person-code : 人物コード
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.person_code")]
        public string person_code { get; set; }

        /// <summary>
        /// yahoo-product-code : Yahoo!ショッピング製品コード
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.yahoo_product_code")]
        public string yahoo_product_code { get; set; }

        /// <summary>
        /// product-code : 製品コード
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.product_code")]
        public string product_code { get; set; }

        /// <summary>
        /// jan : JANコード/ISBNコード
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.jan")]
        public string jan { get; set; }

        /// <summary>
        /// isbn : 2013年8月「jan」に統合。
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.isbn")]
        public string isbn { get; set; }

        /// <summary>
        /// delivery : 送料無料
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.delivery")]
        public int? delivery { get; set; }

        /// <summary>
        /// astk-code : 翌日配達「あすつく」
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.astk_code")]
        public int? astk_code { get; set; }

        /// <summary>
        /// condition : 商品の状態
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.condition")]
        public int? condition { get; set; }

        /// <summary>
        /// taojapan : 淘日本への掲載
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.taojapan")]
        public string taojapan { get; set; }

        /// <summary>
        /// product-category : プロダクトカテゴリ
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.product_category")]
        public string product_category { get; set; }

        /// <summary>
        /// spec1 : スペック
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.spec1")]
        public string spec1 { get; set; }

        /// <summary>
        /// spec2 : スペック
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.spec2")]
        public string spec2 { get; set; }

        /// <summary>
        /// spec3 : スペック
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.spec3")]
        public string spec3 { get; set; }

        /// <summary>
        /// spec4 : スペック
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.spec4")]
        public string spec4 { get; set; }

        /// <summary>
        /// spec5 : スペック
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.spec5")]
        public string spec5 { get; set; }

        /// <summary>
        /// display : ページ公開
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.display")]
        public int? display { get; set; }

        /// <summary>
        /// sort : 商品表示順序
        /// </summary>
        [CsvField]
        [DisplayName("yahoo.sort")]
        public string sort { get; set; }
    }
}
