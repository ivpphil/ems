﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.merchandise.category;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.product.yahoo
{
    /// <summary>
    /// Yahoo!CSVマッパー [Yahoo! CSV mapper]
    /// </summary>
    public class YahooTsvMapper
    {
        #region 定数 [Constant]
        /// <summary>
        /// Yahoo!商品並び順最大値 [Yahoo! Max display order]
        /// </summary>
        protected const int YAHOO_SORT_MAX = 999999999;

        /// <summary>
        /// Yahoo!CSV分割数 [Yahoo! CSV divide count]
        /// </summary>
        protected const int YAHOO_CSV_DIVIDE_COUNT = 100000;
        #endregion


        #region マップ [Map]
        /// <summary>
        /// マップ [Map]
        /// </summary>
        /// <param name="dirPath">出力ディレクトリパス [Output directory path]</param>
        /// <param name="delGetFileName">ファイル名取得デリゲート [Delegate for Get file name]</param>
        /// <param name="listMallProductTmp">商品リスト [The list of products]</param>
        /// <param name="extractDateTime">抽出日時 [Extract datetime]</param>
        /// <returns>出力ファイルパスリスト [The list of output file path]</returns>
        public IEnumerable<string> Map(string dirPath, GetFileName delGetFileName, IList<ErsMallProductTmp> listMallProductTmp, ErsCommonStruct.DateTimeStartEnd extractDateTime)
        {
            var creater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();

            int count = listMallProductTmp.Count();

            // 分割数 [Divide count]
            int divide = (int)Math.Ceiling(count / (double)YAHOO_CSV_DIVIDE_COUNT);

            for (var i = 0; i < divide; i++)
            {
                var fileName = delGetFileName(listMallProductTmp.First().site_id.Value, i);

                // CSVファイル生成 [Create the CSV file]
                using (var writer = creater.GetWriter(dirPath, fileName))
                {
                    var csv = ErsMallFactory.ersMallProductFactory.GetYahooCsvModel();

                    // ヘッダ [Header]
                    creater.WriteCsvHeader<yahoo_csv>(writer);

                    for (var j = i * YAHOO_CSV_DIVIDE_COUNT; j < (i + 1) * YAHOO_CSV_DIVIDE_COUNT; j++)
                    {
                        if (j >= count)
                        {
                            break;
                        }

                        // Yahoo!CSVデータセット [Get the CSV for Yahoo!]
                        csv = this.SetYahooCsv(csv, listMallProductTmp[j], extractDateTime);

                        // ボディ [Body]
                        creater.WriteBody(csv, writer);
                    }
                }

                yield return fileName;
            }
        }

        /// <summary>
        /// ファイル名取得（デリゲート） [Get file name (Delegate)]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="divide">分割カウント [divide count]</param>
        /// <returns>ファイル名 [File name]</returns>
        public delegate string GetFileName(int siteId, int divide);
        #endregion

        #region Yahoo!CSVデータセット [Get the CSV for Yahoo!]
        /// <summary>
        /// Yahoo!CSVデータセット [Get the CSV for Yahoo!]
        /// </summary>
        /// <param name="csv">CSVモデル [CSV model]</param>
        /// <param name="product">商品 [Product]</param>
        /// <param name="extractDateTime">抽出日時 [Extract datetime]</param>
        /// <returns>CSVモデル [CSV model]</returns>
        protected virtual yahoo_csv SetYahooCsv(yahoo_csv csv, ErsMallProductTmp product, ErsCommonStruct.DateTimeStartEnd extractDateTime)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var setupMall = ErsMallFactory.ersMallBatchFactory.getSetup();
            var operationType = ErsMallFactory.ersMallProductFactory.GetObtainProductOperationTypeStgy().Obtain(product, extractDateTime.dateFrom.Value, extractDateTime.dateTo.Value);

            float tax = setup.tax;

            var viewService = ErsFactory.ersViewServiceFactory.GetErsViewCategoryService();
            var separator = ":";
            string display_category = string.Empty;
            //デバッグ時は、IVP用のルートディレクトリを付与
            if (setup.debug)
            {
                display_category = separator + setupMall.ivpProductCategoryPath;
            }

            for (var i = ErsCategory.minCategoryNumber; i <= ErsCategory.maxCategoryNumber; i++)
            {
                var cate = ErsExpressionAccessor<ErsMallProductTmp, int[]>.GetPropertyValue(product, "cate" + i);
                if (cate == null)
                {
                    break;
                }

                var cateValue = viewService.GetStringFromId(1, cate[0]);
                if (!cateValue.HasValue())
                {
                    break;
                }

                display_category += separator + cateValue;
            }

            if (!string.IsNullOrEmpty(display_category))
            {
                csv.path = display_category.Substring(separator.Length);
            }

            csv.name = product.sname;
            csv.code = product.mall_scode;
            csv.sub_code = null;
            csv.original_price = product.p_price2.HasValue ? product.p_price2 * ((setup.tax / 100) + 1) : 0;
            csv.price = product.price;
            //csv.options = product.option;//28文字
            csv.headline = product.headline;//30文字
            // HTML可
            var caption = product.caption;//5000文字
            csv.caption = caption.HasValue() ? caption.Replace(Environment.NewLine, ErsViewHelper.TAG_NEW_LINE) : null;

            csv.abstract_ = product.abstract_;//500文字

            // HTML不可
            csv.explanation = product.explanation;//500文字

            csv.additional1 = product.additional1;//5000
            csv.additional2 = product.additional2;//5000
            csv.additional3 = product.additional3;//5000
            csv.relevant_links = product.relevant_links;//99
            //csv.ship_weight = null;
            csv.taxable = "1";//伝票取り込みの為1固定
            csv.release_date = product.release_date;//YYYYMMDDHH
            //csv.temporary_point_term = null;
            csv.point_code = product.point_code;
            csv.meta_key = product.meta_key;//80
            csv.meta_desc = product.meta_desc;//80

            csv.template = product.template;//4

            csv.sale_price = product.sale_price;//YYYYMMDDHH
            csv.sale_period_start = product.sale_period_start.HasValue ? product.sale_period_start.Value.ToString("yyyyMMddHH") : string.Empty;//YYYYMMDDHH
            csv.sale_period_end = product.sale_period_start.HasValue ? product.sale_period_start.Value.ToString("yyyyMMddHH") : string.Empty;//YYYYMMDDHH
            csv.sale_limit = product.max_purchase_count;
            csv.sp_code = product.sp_code;//10
            csv.brand_code = product.brand_code;//10
            //csv.person_code = null;
            csv.yahoo_product_code = product.yahoo_product_code;//32
            csv.product_code = product.product_code;//50
            csv.jan = product.jancode;
            //csv.isbn = null;
            csv.delivery = 0;
            csv.astk_code = 0;
            csv.condition = 0;
            //csv.taojapan = null;
            csv.product_category = product.product_category;//10
            csv.spec1 = product.spec1;//10
            csv.spec2 = product.spec2;//10
            csv.spec3 = product.spec3;//10
            csv.spec4 = product.spec4;//10
            csv.spec5 = product.spec5;//10

            csv.display = operationType != EnumMallProductOperationType.DELETE ? 1 : 0;

            csv.sort = product.disp_order.HasValue ? Convert.ToString(product.disp_order) : null;

            return csv;
        }
        #endregion
    }
}
