﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    public class ErsMallAmazonProductType
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }
        public string type_code { get; set; }
        public string type_name { get; set; }
        public EnumActive? active { get; set; }
        public int? disp_order { get; set; }
        public DateTime? intime { get; set; }
        public DateTime? utime { get; set; }
    }
}
