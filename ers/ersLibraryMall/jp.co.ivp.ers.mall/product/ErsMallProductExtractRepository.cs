﻿using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品抽出管理リポジトリ [Repository for extract mall product]
    /// </summary>
    public class ErsMallProductExtractRepository
        : ErsRepository<ErsMallProductExtract>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="type">DBタイプ [Type of DB]</param>
        public ErsMallProductExtractRepository()
            : base("mall_product_extract_t")
        {
        }
    }
}
