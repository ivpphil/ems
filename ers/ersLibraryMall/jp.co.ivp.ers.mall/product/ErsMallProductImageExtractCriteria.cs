﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品画像抽出管理クライテリア [Criteria for extract mall product image]
    /// </summary>
    public class ErsMallProductImageExtractCriteria
        : Criteria
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_extract_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// アクティブ[Active]
        /// </summary>
        public virtual EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_extract_t.active", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_extract_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// モール店舗区分 [Mall shop type]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_extract_t.mall_shop_kbn", (int)value, Operation.EQUAL));
            }
        }


        /// <summary>
        /// 抽出日時ソート [For sorting of extract datetime]
        /// </summary> 
        public void SetOrderByExtractDate(OrderBy orderBy)
        {
            AddOrderBy("mall_product_image_extract_t.extract_date", orderBy);
        }
    }
}
