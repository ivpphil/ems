﻿using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品画像ディレクトリリポジトリ [Repository for mall product image directory table]
    /// </summary>
    public class ErsMallProductImageDirectoryRepository
        : ErsRepository<ErsMallProductImageDirectory>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public ErsMallProductImageDirectoryRepository()
            : base("mall_product_image_directory_t")
        {
        }
    }
}
