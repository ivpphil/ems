﻿
namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品テンポラリエンティティインターフェース [Interface for mall product temporary]
    /// </summary>
    public interface IErsMallProductTmp
    {
        /// <summary>
        /// site_id : サイトID
        /// </summary>
        int? site_id { get; set; }

        /// <summary>
        /// mall_shop_kbn : モール店舗区分
        /// </summary>
        EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// scode : SKUコード
        /// </summary>
        string scode { get; set; }
    }
}
