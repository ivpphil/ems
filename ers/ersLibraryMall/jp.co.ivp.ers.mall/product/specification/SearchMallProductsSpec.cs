﻿using System.Collections.Generic;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.product.specification
{
    public class SearchMallProductsSpec
         : SearchSpecificationBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string GetSearchDataSql()
        {
            #region operation_type
            var operation_type = string.Format(
                "(CASE WHEN mall_product_tmp_t.id IS NULL THEN "
                    + "CASE WHEN mall_s_master_t.mall_flg = {0} THEN {1} ELSE NULL END "
                + "ELSE "
                    + "CASE WHEN mall_s_master_t.mall_flg = {0} THEN {2} ELSE {3} END "
                + "END) AS operation_type",
                (int)EnumOnOff.On,
                (int)EnumMallProductOperationType.REGIST, (int)EnumMallProductOperationType.UPDATE, (int)EnumMallProductOperationType.DELETE);
            #endregion

            #region columns
            var columns =
                "mall_s_master_t.site_id, "
                + "mall_s_master_t.mall_shop_kbn, "
                + "mall_s_master_t.mall_flg, "
                + "mall_s_master_t.scode, "
                + "mall_s_master_t.gcode, "
                + "mall_s_master_t.jancode, "
                + "mall_s_master_t.price, "
                + "mall_s_master_t.manage_id, "
                + "mall_s_master_t.product_type, "
                + "mall_s_master_t.brand, "
                + "mall_s_master_t.maker_name, "
                + "mall_s_master_t.model_number, "
                + "mall_s_master_t.product_description, "
                + "mall_s_master_t.item_package_quantity, "
                + "mall_s_master_t.condition_note, "
                + "mall_s_master_t.sale_price, "
                + "mall_s_master_t.sale_from_date, "
                + "mall_s_master_t.sale_end_date, "
                + "mall_s_master_t.offering_can_be_gift_messaged, "
                + "mall_s_master_t.offering_can_be_giftwrapped, "
                + "mall_s_master_t.bullet_point1, "
                + "mall_s_master_t.bullet_point2, "
                + "mall_s_master_t.bullet_point3, "
                + "mall_s_master_t.bullet_point4, "
                + "mall_s_master_t.bullet_point5, "
                + "mall_s_master_t.generic_keywords1, "
                + "mall_s_master_t.generic_keywords2, "
                + "mall_s_master_t.generic_keywords3, "
                + "mall_s_master_t.generic_keywords4, "
                + "mall_s_master_t.generic_keywords5, "
                + "mall_s_master_t.recommended_browse_nodes1, "
                + "mall_s_master_t.recommended_browse_nodes2, "
                + "mall_s_master_t.specific_uses_keywords1, "
                + "mall_s_master_t.specific_uses_keywords2, "
                + "mall_s_master_t.target_audience_keywords, "
                + "mall_s_master_t.safety_warning, "
                + "mall_s_master_t.legal_disclaimer_description, "
                + "mall_s_master_t.ingredients1, "
                + "mall_s_master_t.ingredients2, "
                + "mall_s_master_t.ingredients3, "
                + "mall_s_master_t.special_ingredients, "
                + "mall_s_master_t.indications, "
                + "mall_s_master_t.directions, "
                + "mall_s_master_t.style_name, "
                + "mall_s_master_t.flavor_name, "
                + "mall_s_master_t.size_name, "
                + "mall_s_master_t.color_name, "
                + "mall_s_master_t.color_map, "
                + "mall_s_master_t.scent_name, "
                + "mall_s_master_t.item_form, "
                + "mall_s_master_t.special_features1, "
                + "mall_s_master_t.special_features2, "
                + "mall_s_master_t.special_features3, "
                + "mall_s_master_t.minimum_weight_recommendation, "
                + "mall_s_master_t.maximum_weight_recommendation, "
                + "mall_s_master_t.weight_recommendation_unit_of_measure, "
                + "mall_s_master_t.headline, "
                + "mall_s_master_t.caption, "
                + "mall_s_master_t.abstract_, "
                + "mall_s_master_t.explanation, "
                + "mall_s_master_t.additional1, "
                + "mall_s_master_t.additional2, "
                + "mall_s_master_t.additional3, "
                + "mall_s_master_t.relevant_links, "
                + "mall_s_master_t.release_date, "
                + "mall_s_master_t.point_code, "
                + "mall_s_master_t.meta_key, "
                + "mall_s_master_t.meta_desc, "
                + "mall_s_master_t.template, "
                + "mall_s_master_t.sale_period_start, "
                + "mall_s_master_t.sale_period_end, "
                + "mall_s_master_t.sp_code, "
                + "mall_s_master_t.brand_code, "
                + "mall_s_master_t.yahoo_product_code, "
                + "mall_s_master_t.product_code, "
                + "mall_s_master_t.product_category, "
                + "mall_s_master_t.spec1, "
                + "mall_s_master_t.spec2, "
                + "mall_s_master_t.spec3, "
                + "mall_s_master_t.spec4, "
                + "mall_s_master_t.spec5, "
                + "mall_s_master_t.tag_id, "
                + "mall_s_master_t.pc_slogan, "
                + "mall_s_master_t.mobile_slogan, "
                + "mall_s_master_t.product_layout, "
                + "mall_s_master_t.noshi, "
                + "mall_s_master_t.pc_description, "
                + "mall_s_master_t.mobile_description, "
                + "mall_s_master_t.smartphone_description, "
                + "mall_s_master_t.pc_sale_description, "
                + "mall_s_master_t.movie, "
                + "mall_s_master_t.stock_display, "
                + "mall_s_master_t.black_market_pass, "
                + "mall_s_master_t.point_scale_rate, "
                + "mall_s_master_t.point_scale_rate_period, "
                + "mall_s_master_t.header_footer_leftnavi, "
                + "mall_s_master_t.display_order, "
                + "mall_s_master_t.common_description_small, "
                + "mall_s_master_t.deature_product, "
                + "mall_s_master_t.common_description_large, "
                + "mall_s_master_t.display_review_test, "
                + "mall_s_master_t.size_chart_link, "
                + "mall_s_master_t.drug_description, "
                + "mall_s_master_t.drug_notes, "
                + "mall_s_master_t.control_number_dual_price_word, "
                + "mall_s_master_t.deleted, "
                + "mall_s_master_t.last_image_upload_time, "
                + "s_master_t.sname, "
                + "s_master_t.disp_order, "
                + "s_master_t.max_purchase_count, "
                + "g_master_t.date_from, "
                + "g_master_t.date_to, "
                + "g_master_t.cate1, "
                + "g_master_t.cate2, "
                + "g_master_t.cate3, "
                + "g_master_t.cate4, "
                + "g_master_t.cate5, "
                + "price_t.price AS p_price, "
                + "price_t.price2 AS p_price2 ";
            #endregion

            return "SELECT "
                + operation_type + ", " + columns
                + "FROM s_master_t "
                + "INNER JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode "
                + "INNER JOIN mall_s_master_t ON s_master_t.scode = mall_s_master_t.scode "
                + "LEFT JOIN (SELECT scode, price, price2, cost_price, intime, utime FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.NORMAL
                + ") AS price_t ON s_master_t.scode = price_t.scode "
                + "LEFT JOIN mall_product_tmp_t ON mall_s_master_t.scode = mall_product_tmp_t.scode AND mall_s_master_t.site_id = mall_product_tmp_t.site_id "
                + "WHERE true ";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countColumnAlias"></param>
        /// <returns></returns>
        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(*) AS " + countColumnAlias + " "
                + "FROM s_master_t "
                + "INNER JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode "
                + "INNER JOIN mall_s_master_t ON s_master_t.scode = mall_s_master_t.scode "
                + "LEFT JOIN (SELECT scode, price, price2, cost_price, intime, utime FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.NORMAL
                + ") AS price_t ON s_master_t.scode = price_t.scode "
                + "LEFT JOIN mall_product_tmp_t ON mall_s_master_t.scode = mall_product_tmp_t.scode AND mall_s_master_t.site_id = mall_product_tmp_t.site_id "
                + "WHERE true ";
        }
    }
}
