﻿using System.Collections.Generic;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.product.specification
{
    public class SearchMallMerchandiseSpec
         : SearchSpecificationBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected string orderByValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public override List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            this.SetOrderByValue(criteria);

            return base.GetSearchData(criteria);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        protected virtual void SetOrderByValue(Criteria criteria)
        {
            this.orderByValue = criteria.GetDistinctOn("mall_s_master_t");

            if (string.IsNullOrEmpty(this.orderByValue))
            {
                criteria.InsertOrderByToFirst("mall_s_master_t.scode", Criteria.OrderBy.ORDER_BY_ASC);
                this.orderByValue = "mall_s_master_t.scode";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string GetSearchDataSql()
        {
            return "SELECT  DISTINCT ON (" + orderByValue + ") mall_s_master_t.* "
                + "FROM s_master_t "
                + "INNER JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode "
                + "INNER JOIN mall_s_master_t ON s_master_t.scode = mall_s_master_t.scode "
                + "LEFT JOIN (SELECT scode, price, price2, cost_price, intime, utime FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.NORMAL
                + ") AS price_t ON s_master_t.scode = price_t.scode "
                + "WHERE true ";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countColumnAlias"></param>
        /// <returns></returns>
        protected override string GetCountDataDataSql(string countColumnAlias)
        {
            return "SELECT COUNT(mall_s_master_t.id) AS " + countColumnAlias + " "
                + "FROM s_master_t "
                + "INNER JOIN g_master_t ON s_master_t.gcode = g_master_t.gcode "
                + "INNER JOIN mall_s_master_t ON s_master_t.scode = mall_s_master_t.scode "
                + "LEFT JOIN (SELECT scode, price, price2, cost_price, intime, utime FROM price_t WHERE price_kbn = " + (int)EnumPriceKbn.NORMAL
                + ") AS price_t ON s_master_t.scode = price_t.scode "
                + "WHERE true ";
        }
    }
}
