﻿using System.ComponentModel;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.product.harc
{
    /// <summary>
    /// Amazon用CSVモデル [CSV Model for Amazon]
    /// </summary>
    public class harc_amazon_csv
        : ErsModelBase
    {
        /// <summary>
        /// product code [scode]
        /// </summary>
        [CsvField]
        [DisplayName("harc.product.productCode")]
        public string product_code { get; set; }

        /// <summary>
        /// product SkuCode [scode]
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.productSkuCode")]
        public string sku_code { get; set; }

        /// <summary>
        /// shop product code [scode]
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductParam.shopProductCode")]
        public string shop_product_code { get; set; }

        /// <summary>
        /// shop product name [sname]
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductParam.shopProductName")]
        public string shop_product_name { get; set; }

        /// <summary>
        /// shop product price [price]
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductParam.salePrice")]
        public int shop_product_price { get; set; }

        /// <summary>
        /// shop product points
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductParam.shopPoint")]
        public int shop_product_point { get; set; }

        /// <summary>
        /// active flag : 0/1
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductParam.activeFlag")]
        public EnumActive shop_product_active_flg { get; set; }

        /// <summary>
        /// shop skucode [scode]
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.shopProductSkuCode")]
        public string shop_sku_code { get; set; }

        /// <summary>
        /// shop sku name [sname]
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.shopProductSkuName")]
        public string shop_sku_name { get; set; }

        /// <summary>
        /// shop sku price [price]
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.salePrice")]
        public int shop_sku_price { get; set; }

        /// <summary>
        /// shop sku points
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.shopPoint")]
        public int shop_sku_point { get; set; }

        /// <summary>
        /// shop sku col name
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.shopPostingParam.skuColName")]
        public string shop_sku_col_name { get; set; }

        /// <summary>
        /// shop sku col code
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.shopPostingParam.skuColCode")]
        public string shop_sku_col_code { get; set; }

        /// <summary>
        /// shop sku col type
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.shopPostingParam.skuColType")]
        public string shop_sku_col_type { get; set; }

        /// <summary>
        /// shop sku row name
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.shopPostingParam.skuRowName")]
        public string shop_sku_row_name { get; set; }

        /// <summary>
        /// shop sku row code
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.shopPostingParam.skuRowCode")]
        public string shop_sku_row_code { get; set; }

        /// <summary>
        /// shop sku row type
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.shopPostingParam.skuRowType")]
        public string shop_sku_row_type { get; set; }

        /// shop sku lead time
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.shopPostingParam.leadTime")]
        public int? shop_sku_lead_time { get; set; }

        /// <summary>
        /// shop sku restock date
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.shopPostingParam.restockDate")]
        public string shop_sku_restock_date { get; set; }

        /// <summary>
        /// <summary>
        /// shop sku order name
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.orderName")]
        public string shop_sku_order_name { get; set; }

        /// <summary>
        /// active flag : 0/1
        /// </summary>
        [CsvField]
        [DisplayName("harc.shopProductSkuParam.activeFlag")]
        public EnumActive shop_sku_active_flg { get; set; }
    }
}
