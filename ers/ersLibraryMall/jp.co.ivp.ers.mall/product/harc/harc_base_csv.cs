﻿using System.ComponentModel;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.product.harc
{
    /// <summary>
    /// 基本CSVモデル [CSV Model for Base]
    /// </summary>
    public class harc_base_csv
        : ErsModelBase
    {
        /// <summary>
        /// product code [scode]
        /// </summary>
        [CsvField]
        [DisplayName("harc.product.productCode")]
        public string product_code { get; set; }

        /// <summary>
        /// product name [sname]
        /// </summary>
        [CsvField]
        [DisplayName("harc.product.productName")]
        public string product_name { get; set; }

        /// <summary>
        /// product price [price]
        /// </summary>
        [CsvField]
        [DisplayName("harc.product.listPrice")]
        public int product_price { get; set; }

        /// <summary>
        /// active flag : 0/1
        /// </summary>
        [CsvField]
        [DisplayName("harc.product.activeFlag")]
        public int active_flg { get; set; }

        /// <summary>
        /// sku code [scode]
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.productSkuCode")]
        public string sku_code { get; set; }

        /// <summary>
        /// sku name [sname]
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.productSkuName")]
        public string sku_name { get; set; }

        /// <summary>
        /// sku price [price]
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.listPrice")]
        public int sku_price { get; set; }

        /// <summary>
        /// sku unit price [price]
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.unitPrice")]
        public int sku_unit_price { get; set; }

        /// <summary>
        /// stock alert 
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.stockAlertThreshold")]
        public int stock_alert { get; set; }

        /// <summary>
        /// head name 0
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.variationParam.0.headName")]
        public string sku_head_name0 { get; set; }

        /// <summary>
        /// value name 0
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.variationParam.0.valueName")]
        public string sku_value_name0 { get; set; }

        /// <summary>
        /// value code 0
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.variationParam.0.valueCode")]
        public string sku_value_code0 { get; set; }

        /// <summary>
        /// head name 1
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.variationParam.1.headName")]
        public string sku_head_name1 { get; set; }

        /// <summary>
        /// value name 1
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.variationParam.1.valueName")]
        public string sku_value_name1 { get; set; }

        /// <summary>
        /// value code 1
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.variationParam.1.valueCode")]
        public string sku_value_code1 { get; set; }

        /// <summary>
        /// active flag : 0/1
        /// </summary>
        [CsvField]
        [DisplayName("harc.productSku.activeFlag")]
        public int sku_active_flg { get; set; }

    }
}
