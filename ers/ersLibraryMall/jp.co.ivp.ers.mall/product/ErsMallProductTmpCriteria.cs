﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品テンポラリクライテリア [Criteria for mall product temporary table]
    /// </summary>
    public class ErsMallProductTmpCriteria
        : Criteria
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_tmp_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// アクティブ[Active]
        /// </summary>
        public virtual EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_tmp_t.active", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_tmp_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// モール店舗区分 [Mall shop type]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_tmp_t.mall_shop_kbn", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 商品コード [Product code]
        /// </summary>
        public virtual string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_tmp_t.scode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 商品コードIN [Scode IN]
        /// </summary>
        public virtual IEnumerable<string> scode_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("mall_product_tmp_t.scode", value));
            }
        }


        /// <summary>
        /// ソート：商品コード [Sorting : Product code]
        /// </summary>
        /// <param name="orderBy"></param>
        public void SetOrderByScode(OrderBy orderBy)
        {
            this.AddOrderBy("mall_product_tmp_t.scode", orderBy);
        }


        /// <summary>
        /// モール商品登録用 [For register mall products]
        /// </summary>
        /// <param name="from">検索日時FROM [Search datetime from]</param>
        /// <param name="to">検索日時TO [Search datetime from]</param>
        public void SetSearchForRegister(DateTime from, DateTime to)
        {
            var dicDateTime = new Dictionary<string, object>();

            dicDateTime.Add("intime_utime_from", from);
            dicDateTime.Add("intime_utime_to", to);

            // mall_product_tmp_t.intime or utime
            var sql = "CASE WHEN mall_product_tmp_t.utime IS NULL THEN mall_product_tmp_t.intime ELSE mall_product_tmp_t.utime END BETWEEN :intime_utime_from AND :intime_utime_to";

            this.Add(Criteria.GetUniversalCriterion(sql, dicDateTime));
        }

        /// <summary>
        /// モール商品削除用 [For delete mall products]
        /// </summary>
        /// <param name="listMallProductTmp">モール商品テンポラリリスト [The list of mall product temporary]</param>
        public void SetSearchForDelete(IList<IErsMallProductTmp> listMallProductTmp)
        {
            var dicParam = new Dictionary<string, object>();
            var listParamName = new List<string>();

            for (var i = 0; i < listMallProductTmp.Count; i++)
            {
                var site_id_name = string.Format("site_id{0}", i);
                var mall_shop_kbn_name = string.Format("mall_shop_kbn{0}", i);
                var scode_name = string.Format("scode{0}", i);

                dicParam[site_id_name] = listMallProductTmp[i].site_id;
                dicParam[mall_shop_kbn_name] = listMallProductTmp[i].mall_shop_kbn;
                dicParam[scode_name] = listMallProductTmp[i].scode;

                listParamName.Add(string.Format("(:{0}, :{1}, :{2})", site_id_name, mall_shop_kbn_name, scode_name));
            }

            // mall_product_tmp_t.mall_shop_kbn & scode
            var sql = string.Format("(mall_product_tmp_t.site_id, mall_product_tmp_t.mall_shop_kbn, mall_product_tmp_t.scode) IN ({0})", String.Join(", ", listParamName));

            this.Add(Criteria.GetUniversalCriterion(sql, dicParam));

            this.active = EnumActive.Active;
        }
    }
}
