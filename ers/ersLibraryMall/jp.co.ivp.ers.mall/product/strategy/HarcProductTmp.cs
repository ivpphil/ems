﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mall.common;

namespace jp.co.ivp.ers.mall.product.strategy
{
    public struct HarcProductTmp
    {
        /// <summary>
        /// モール用商品コード [Product code for Mall]
        /// </summary>
        public string mall_scode
        {
            get
            {
                return ErsMallCommonService.GetMallSkuFromErs(this.scode);
            }
        }

        /// <summary>
        /// 商品コード [Product code]
        /// </summary>
        public string scode { get; set; }

        public string sname { get; set; }

        public int? price { get; set; }

        public int? site_id { get; set; }

        public EnumActive? active { get; set; }

        public EnumMallShopKbn mall_shop_kbn { get; set; }
    }
}
