﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.mall.product.harc;

namespace jp.co.ivp.ers.mall.product.strategy
{
    /// <summary>
    /// 在庫平準化設定CSV生成サービス（EC-CUBE） [CSV creating service for stock leveling settings (EC-CUBE)]
    /// </summary>
    public class ProductLevelingSettingsCsvServiceEcCube
        : ProductLevelingSettingsCsvService
    {
        /// <summary>
        /// ファイル名取得 [Get th file name]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <returns>ファイル名 [File name]</returns>
        protected override string GetFileName(int? siteId, EnumMallShopKbn? shopKbn)
        {
            return string.Format("harc_{0}_{1}.csv", shopKbn.ToString().ToLower(), DateTime.Now.ToString("yyyyMMddHHmmssfffffff"));
        }

        /// <summary>
        /// CSV生成 [Create the CSV file]
        /// </summary>
        /// <param name="listMallProductTmp">モール商品テンポラリリスト [The list of mall product temporary]</param>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Mall shop type]</param>
        /// <returns>生成したファイルパス [Created CSV file path]</returns>
        public override string CreateCsvFile(IList<HarcProductTmp> listHarcProductTmp, int? siteId, EnumMallShopKbn? shopKbn)
        {
            // 書き込みインデックスリスト取得 [Get the list of index for writing]
            var listIndex = this.GetIndexList(listHarcProductTmp, siteId);

            if (listIndex.Count == 0)
            {
                return null;
            }

            harc_ec_cube_csv csv = ErsMallFactory.ersMallProductFactory.GetHarcEccubeCsvModel();

            var creater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            var mallSetup = ErsMallFactory.ersMallUtilityFactory.getSetup();

            var dirPath = mallSetup.harcProductCsvOutputPath;
            var filename = this.GetFileName(siteId, shopKbn);

            // CSVファイル生成 [Create the CSV file]
            using (var writer = creater.GetWriter(dirPath, filename))
            {
                // ヘッダ [Header]
                creater.WriteCsvHeader<harc_ec_cube_csv>(writer);

                // ボディ [Body]
                foreach (var index in listIndex)
                {
                    var data = listHarcProductTmp[index];

                    csv.product_code = data.mall_scode;
                    csv.sku_code = data.mall_scode;
                    csv.shop_product_code = data.mall_scode;
                    csv.shop_product_name = data.sname;
                    csv.shop_product_price = data.price != null ? data.price.Value : 0;

                    csv.shop_product_active_flg = EnumActive.Active;//data.active.Value;
                    csv.shop_sku_active_flg = EnumActive.Active;//data.active.Value;

                    csv.shop_sku_name = data.sname;
                    csv.shop_sku_price = csv.shop_product_price;
                    csv.shop_sku_code = data.mall_scode;

                    csv.shop_sku_order_name = data.mall_scode;

                    creater.WriteBody(csv, writer);
                }
            }

            return creater.filePath;
        }

        /// <summary>
        /// CSV書き込みインデックスリスト取得 [Get the list of index for wrting CSV]
        /// </summary>
        /// <param name="listMallProductTmp">モール商品テンポラリリスト [The list of mall product temporary]</param>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <returns>CSV書き込みインデックスリスト [The list of index for wrting CSV]]</returns>
        protected override IList<int> GetIndexList(IList<HarcProductTmp> listHarcProductTmp, int? siteId, EnumMallShopKbn? shopKbn = null)
        {
            IList<int> listIndex = new List<int>();
            IList<string> listScode = new List<string>();

            for (var i = 0; i < listHarcProductTmp.Count; i++)
            {
                var data = listHarcProductTmp[i];

                if (!listScode.Contains(data.scode))
                {
                    listScode.Add(data.scode);
                    listIndex.Add(i);
                }
            }

            return listIndex;
        }
    }
}
