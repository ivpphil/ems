﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.product;

namespace jp.co.ivp.ers.mall.product.strategy
{
    /// <summary>
    /// 在庫平準化設定CSV生成サービス [CSV creating service for stock leveling settings]
    /// </summary>
    public abstract class ProductLevelingSettingsCsvService
    {
        /// <summary>
        /// ファイル名取得 [Get th file name]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <returns>ファイル名 [File name]</returns>
        protected virtual string GetFileName(int? siteId, EnumMallShopKbn? shopKbn)
        {
            return string.Format("harc_{0}_{1}_{2}.csv", siteId, shopKbn.ToString().ToLower(), DateTime.Now.ToString("yyyyMMddHHmmssfffffff"));
        }

        /// <summary>
        /// CSV生成 [Create the CSV file]
        /// </summary>
        /// <param name="listMallProductTmp">モール商品テンポラリリスト [The list of mall product temporary]</param>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <returns>生成したファイルパス [Created CSV file path]</returns>
        public abstract string CreateCsvFile(IList<HarcProductTmp> listHarcProductTmp, int? siteId, EnumMallShopKbn? shopKbn);

        /// <summary>
        /// CSV書き込みインデックスリスト取得 [Get the list of index for wrting CSV]
        /// </summary>
        /// <param name="listMallProductTmp">モール商品テンポラリリスト [The list of mall product temporary]</param>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <returns>CSV書き込みインデックスリスト [The list of index for wrting CSV]]</returns>
        protected virtual IList<int> GetIndexList(IList<HarcProductTmp> listHarcProductTmp, int? siteId, EnumMallShopKbn? shopKbn = null)
        {
            IList<int> listIndex = new List<int>();

            for (var i = 0; i < listHarcProductTmp.Count; i++)
            {
                var data = listHarcProductTmp[i];

                if (siteId.HasValue && siteId == data.site_id &&
                    shopKbn.HasValue && shopKbn.Value == data.mall_shop_kbn)
                {
                    listIndex.Add(i);
                }
            }

            return listIndex;
        }
    }
}
