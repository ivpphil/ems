﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.merchandise;

namespace jp.co.ivp.ers.mall.product.strategy
{
    public class CheckMallFlgStgy
    {
        public ValidationResult CheckSku(string gcode, EnumSalePatternType? s_sale_ptn)
        {
            if (s_sale_ptn != EnumSalePatternType.REGULAR)
            {
                return null;
            }
            var repositroy = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();

            criteria.gcode = gcode;
            criteria.SetHasActiveMallFlg();

            if (repositroy.GetRecordCount(criteria) > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("70200"));
            }

            return null;
        }

        public ValidationResult CheckGroup(string scode)
        {
            var objMerchandise = this.GetErsMerchandiseWithScode(scode);
            if (objMerchandise != null)
            {
                if (objMerchandise.s_sale_ptn == EnumSalePatternType.REGULAR)
                {
                    return new ValidationResult(ErsResources.GetMessage("70200"));
                }
            }
            return null;
        }

        public virtual ErsMerchandise GetErsMerchandiseWithScode(string scode)
        {
            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            groupCriteria.scode = scode;
            var listGroup = groupRepository.FindGroupBaseItemList(groupCriteria);
            if (listGroup.Count != 0)
            {
                return listGroup.First();
            }
            else
            {
                return null;
            }
        }

        public ValidationResult CheckGroupAndSku(string gcode, string scode, EnumSalePatternType? s_sale_ptn)
        {
            if (s_sale_ptn != EnumSalePatternType.REGULAR)
            {
                return null;
            }

            var repositroy = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();

            criteria.gcode = gcode;
            criteria.scode_not_equal = scode;
            criteria.SetHasActiveMallFlg();

            if (repositroy.GetRecordCount(criteria) > 0)
            {
                return new ValidationResult(ErsResources.GetMessage("70200"));
            }

            return null;
        }
    }
}
