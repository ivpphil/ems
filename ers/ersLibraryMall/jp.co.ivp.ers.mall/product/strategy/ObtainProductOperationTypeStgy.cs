﻿using System;

namespace jp.co.ivp.ers.mall.product.strategy
{
    /// <summary>
    /// モール商品操作タイプ取得 [Get operation type for mall product]
    /// </summary>
    public class ObtainProductOperationTypeStgy
    {
        /// <summary>
        /// モール商品操作タイプ取得 [Get operation type for mall product]
        /// </summary>
        /// <param name="product">商品 [Product]</param>
        /// <param name="dateFrom">検索日時FROM [Search datetime (FROM)]</param>
        /// <param name="dateTo">検索日時TO [Search datetime (TO)]</param>
        /// <returns>モール商品操作タイプ [Operation type for mall product]</returns>
        public virtual EnumMallProductOperationType Obtain(ErsMallProductTmp product, DateTime dateFrom, DateTime dateTo)
        {
            if (product.deleted == EnumDeleted.Deleted)
            {
                return EnumMallProductOperationType.DELETE;
            }

            if (!product.utime.HasValue)
            {
                return EnumMallProductOperationType.REGIST;
            }
            if (product.intime >= dateFrom)
            {
                return EnumMallProductOperationType.REGIST;
            }

            var dateNow = DateTime.Now;

            if (product.active == EnumActive.NonActive)
            {
                return EnumMallProductOperationType.DELETE;
            }
            if (!(product.date_from <= dateNow && dateNow <= product.date_to))
            {
                return EnumMallProductOperationType.DELETE;
            }

            return EnumMallProductOperationType.UPDATE;
        }
    }
}
