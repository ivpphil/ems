﻿using System.Collections.Generic;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.mall.product.harc;

namespace jp.co.ivp.ers.mall.product.strategy
{
    /// <summary>
    /// 在庫平準化設定CSV生成サービス（楽天） [CSV creating service for stock leveling settings (Rakuten)]
    /// </summary>
    public class ProductLevelingSettingsCsvServiceRakuten
        : ProductLevelingSettingsCsvService
    {
        /// <summary>
        /// CSV生成 [Create the CSV file]
        /// </summary>
        /// <param name="listMallProductTmp">モール商品テンポラリリスト [The list of mall product temporary]</param>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Mall shop type]</param>
        /// <returns>生成したファイルパス [Created CSV file path]</returns>
        public override string CreateCsvFile(IList<HarcProductTmp> listHarcProductTmp, int? siteId, EnumMallShopKbn? shopKbn)
        {
            // 書き込みインデックスリスト取得 [Get the list of index for writing]
            var listIndex = this.GetIndexList(listHarcProductTmp, siteId, shopKbn);

            if (listIndex.Count == 0)
            {
                return null;
            }

            harc_rakuten_csv csv = ErsMallFactory.ersMallProductFactory.GetHarcRakutenCsvModel();

            var creater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            var mallSetup = ErsMallFactory.ersMallUtilityFactory.getSetup();

            var dirPath = mallSetup.harcProductCsvOutputPath;
            var filename = this.GetFileName(siteId, shopKbn);

            // CSVファイル生成 [Create the CSV file]
            using (var writer = creater.GetWriter(dirPath, filename))
            {
                // ヘッダ [Header]
                creater.WriteCsvHeader<harc_rakuten_csv>(writer);

                // ボディ [Body]
                foreach (var index in listIndex)
                {
                    var data = listHarcProductTmp[index];

                    csv.product_code = data.mall_scode;
                    csv.sku_code = data.mall_scode;
                    csv.shop_product_code = data.mall_scode;
                    csv.shop_product_name = data.sname;
                    csv.shop_product_price = data.price != null ? data.price.Value : 0;

                    csv.shop_product_url = data.mall_scode.ToLower();

                    csv.shop_product_active_flg = data.active.Value;
                    csv.shop_sku_active_flg = data.active.Value;

                    csv.shop_sku_name = data.sname;
                    csv.shop_sku_price = csv.shop_product_price;
                    csv.shop_sku_code = data.mall_scode;

                    csv.shop_sku_order_name = data.mall_scode;

                    creater.WriteBody(csv, writer);
                }
            }

            return creater.filePath;
        }
    }
}
