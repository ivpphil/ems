﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using com.hunglead.harc;
using jp.co.ivp.ers.mall.api;
using jp.co.ivp.ers.mall.product;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using System.IO;

namespace jp.co.ivp.ers.mall.product.strategy
{
    /// <summary>
    /// モール商品操作クラス（HARC） [Class for operate mall product (HARC)]
    /// </summary>
    public class OperateMallProductHarc
    {
        public struct CsvFiles
        {
            public string path { get; set; }
            public int? shop_id { get; set; }
        }

        #region 実行 [Execute]
        /// <summary>
        /// 実行 [Execute]
        /// </summary>
        /// <param name="dateExecute">実行日時 [Execute datetime]</param>
        public string Execute(IList<HarcProductTmp> listHarcProductTmp)
        {
            if (listHarcProductTmp == null || listHarcProductTmp.Count == 0)
            {
                return string.Empty;
            }

            var setup = ErsMallFactory.ersMallUtilityFactory.getSetup();

            if (!setup.enableMall)
            {
                return null;
            }

            if (!setup.harcProductCsvOutputPath.HasValue())
            {
                throw new Exception("Not set harcProductCsvOutputPath.");
            }

            // CSV出力ディレクトリ作成 [Create directory for output CSV file]
            Directory.CreateDirectory(setup.harcProductCsvOutputPath);

            var objDB = ErsCommonsSetting.ersDatabaseFactory.GetNewErsDatabase();
            using (var otherTx = ErsDB_parent.BeginTransaction(objDB))
            {
                this.CreateErsMallStock(objDB, listHarcProductTmp);
                // HARCログイン [Login to HARC]
                var request = ErsMallFactory.ersMallCommonFactory.GetHarcLoginStgy().HarcLogin();

                // 平準化設定 [Leveling settings]
                var result = this.LevelingSettings(request, listHarcProductTmp);

                otherTx.Commit();

                return result;
            }
        }
        #endregion

        #region 平準化設定 [Leveling settings]
        /// <summary>
        /// 平準化設定 [Leveling settings]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="listHarcProductTmp">モール商品テンポラリリスト [The list of mall product temporary]</param>
        /// <returns>エラーメッセージ [Error message]</returns>
        private string LevelingSettings(HarcApiRequest request, IList<HarcProductTmp> listHarcProductTmp)
        {
            IList<CsvFiles> listParam = new List<CsvFiles>();

            // CSV生成（ベース） [Create the CSV file (for Base)]
            var filePathBase = (new ProductLevelingSettingsCsvServiceBase()).CreateCsvFile(listHarcProductTmp, null, null);

            if (string.IsNullOrEmpty(filePathBase))
            {
                return null;
            }

            var siteData = ErsMallFactory.ersSiteFactory.GetSiteData();
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();

            // CSV生成（モール） [Create the CSV files (for Mall shops)]
            foreach (var siteId in siteData.dicSiteData.Keys)
            {
                if (siteData.dicSiteData[siteId].harc_flg == EnumOnOff.Off)
                {
                    continue;
                }

                // モール店舗タイプ取得（サイトIDから） [Get mall shop type (from Site id)]
                var shopKbn = siteData.GetMallShopKbnFromSiteId(siteId);

                var service = EnumMallShopKbnExtension.GetProductLevelingSettingsCsvService(shopKbn);

                if (service == null)
                {
                    continue;
                }

                var filePath = service.CreateCsvFile(listHarcProductTmp, siteId, shopKbn);

                if (!string.IsNullOrEmpty(filePath))
                {
                    var csvFile = default(CsvFiles);
                    csvFile.path = filePath;
                    csvFile.shop_id = mallSetup.GetShopId(siteId);
                    listParam.Add(csvFile);
                }
            }

            // HARCへCSV登録 [Register the CSV to HARC]
            var resultBase = this.importProductLeveling(request, null, filePathBase);
            var resultShop = this.importProductLevelingShop(request, listParam);

            return resultBase + (!string.IsNullOrEmpty(resultShop) ? Environment.NewLine : string.Empty) + resultShop;
        }

        /// <summary>
        /// CSV登録（モール） [Register the CSV file [for Mall shops]]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="listParam">パラメーラリスト [List of parameters]</param>
        /// <returns>エラーメッセージ [Error message]</returns>
        protected string importProductLevelingShop(HarcApiRequest request, IList<CsvFiles> listParam)
        {
            var listError = new ConcurrentQueue<string>();

            // 並列処理 [Parallel processing]
            Parallel.ForEach(listParam, param =>
            {
                var result = this.importProductLeveling(request, param.shop_id, param.path);

                if (!string.IsNullOrEmpty(result))
                {
                    listError.Enqueue(result);
                }
            });

            // エラーが発生していた場合は連結する [When there are any error, then join the error message]
            if (listError.Count > 0)
            {
                return String.Join(Environment.NewLine, listError);
            }

            return string.Empty;
        }

        /// <summary>
        /// CSV登録 [Register the CSV file]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="shop_id">店舗ID [Shop id]</param>
        /// <param name="filename">CSVファイル名 [SCV file name]</param>
        /// <returns>エラーメッセージ [Error message]</returns>
        protected string importProductLeveling(HarcApiRequest request, int? shop_id, string filename)
        {
            var paramApi = ErsMallFactory.ersMallAPIFactory.GetImportRawProductCsvAPIParam();
            paramApi.shop_id = shop_id;

            try
            {
                // CSV登録 [Register the CSV file]
                var dicResult = ErsMallFactory.ersMallAPIFactory.GetImportRawProductCsvAPI(paramApi).Import(request, filename);
            }
            catch (APIFailedException e)
            {
                return ErsResources.GetMessage(shop_id != null ? "102004" : "102003", filename, e.ToString());
            }

            return string.Empty;
        }
        #endregion

        /// <summary>
        /// ERSでHarcの在庫数を保持する
        /// </summary>
        /// <param name="objDB"></param>
        /// <param name="listParam"></param>
        private void CreateErsMallStock(ErsDatabase objDB, IList<HarcProductTmp> listHarcProductTmp)
        {
            var repository = ErsMallFactory.ersMallStockFactory.GetErsMallStockRepository(objDB);

            foreach (var product in listHarcProductTmp)
            {
                var criteria = ErsMallFactory.ersMallStockFactory.GetErsMallStockCriteria();
                criteria.scode = product.scode;
                var objMallStock = repository.FindSingle(criteria);
                if (objMallStock == null)
                {
                    objMallStock = ErsMallFactory.ersMallStockFactory.GetErsMallStock();
                    objMallStock.scode = product.scode;
                    objMallStock.stock = 0;
                    repository.Insert(objMallStock);
                }
            }
        }
    }
}
