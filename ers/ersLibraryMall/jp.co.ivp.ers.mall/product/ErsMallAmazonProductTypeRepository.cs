﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    public class ErsMallAmazonProductTypeRepository
        : ErsRepository<ErsMallAmazonProductType>
    {
        public ErsMallAmazonProductTypeRepository()
            : base("mall_amazon_product_type_t")
        {
        }
    }
}
