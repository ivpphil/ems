﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品画像テンポラリクライテリア [Criteria for mall product image temporary table]
    /// </summary>
    public class ErsMallProductImageTmpCriteria
        : Criteria
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_tmp_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// アクティブ[Active]
        /// </summary>
        public virtual EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_tmp_t.active", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_tmp_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// モール店舗区分 [Mall shop type]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_tmp_t.mall_shop_kbn", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 画像タイプ [Image type]
        /// </summary>
        public virtual EnumMallProductImageType? image_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_tmp_t.image_type", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 画像インデックス [Image index]
        /// </summary>
        public virtual int? image_index
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_tmp_t.image_index", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// グループコード [Group code]
        /// </summary>
        public virtual string gcode
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_tmp_t.gcode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 商品コード [Product code]
        /// </summary>
        public virtual string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_tmp_t.scode", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 商品コードIN [Scode IN]
        /// </summary>
        public virtual IEnumerable<string> scode_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("mall_product_image_tmp_t.scode", value));
            }
        }



        /// <summary>
        /// ソート：ID [ID]
        /// </summary>
        /// <param name="orderBy"></param>
        public void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("mall_product_image_tmp_t.id", orderBy);
        }


        /// <summary>
        /// モール商品画像登録用 [For register mall product images]
        /// </summary>
        /// <param name="from">検索日時FROM [Search datetime from]</param>
        /// <param name="to">検索日時TO [Search datetime from]</param>
        public void SetSearchForRegister(DateTime from, DateTime to)
        {
            var dicDateTime = new Dictionary<string, object>();

            dicDateTime.Add("intime_utime_from", from);
            dicDateTime.Add("intime_utime_to", to);

            // mall_product_tmp_t.intime or utime
            var sqlIntimeUtime = "CASE WHEN mall_product_image_tmp_t.utime IS NULL THEN mall_product_image_tmp_t.intime ELSE mall_product_image_tmp_t.utime END BETWEEN :intime_utime_from AND :intime_utime_to";

            this.Add(Criteria.GetUniversalCriterion(sqlIntimeUtime, dicDateTime));
        }

        #region mall_product_image_directory_t
        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? image_directory_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_directory_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? image_directory_id_not_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_product_image_directory_t.id", value, Operation.NOT_EQUAL));
            }
        }
        #endregion
    }
}
