﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.product
{
    public class ErsMallAmazonProductTypeCriteria
        : Criteria
    {
        public void SetOrderByDispOrder(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("mall_amazon_product_type_t.disp_order", orderBy);
        }
    }
}
