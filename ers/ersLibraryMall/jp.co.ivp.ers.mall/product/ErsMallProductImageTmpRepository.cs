﻿using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品画像テンポラリリポジトリ [Repository for mall product image temporary table]
    /// </summary>
    public class ErsMallProductImageTmpRepository
        : ErsRepository<ErsMallProductImageTmp>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="type">DBタイプ [Type of DB]</param>
        public ErsMallProductImageTmpRepository()
            : base("mall_product_image_tmp_t")
        {
        }
    }
}
