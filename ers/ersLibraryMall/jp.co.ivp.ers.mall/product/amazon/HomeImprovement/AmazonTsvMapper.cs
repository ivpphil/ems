﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product.amazon.HomeImprovement
{
    /// <summary>
    /// AmazonTSVマッパー [Amazon TSV mapper]
    /// </summary>
    public class AmazonTsvMapper
        : AmazonTsvMapperBase
    {
        #region マップ [Map]
        /// <summary>
        /// マップ [Map]
        /// </summary>
        /// <param name="dirPath">出力ディレクトリパス [Output directory path]</param>
        /// <param name="delGetFileName">ファイル名取得デリゲート [Delegate for Get file name]</param>
        /// <param name="listMallProductTmp">商品リスト [The list of products]</param>
        /// <param name="extractDateTime">抽出日時 [Extract datetime]</param>
        /// <returns>出力ファイルパスリスト [The list of output file path]</returns>
        public override IEnumerable<string> Map(string dirPath, GetFileName delGetFileName, IList<ErsMallProductTmp> listMallProductTmp, ErsCommonStruct.DateTimeStartEnd extractDateTime)
        {
            int i = 0;

            int count = listMallProductTmp.Count();
            int divide = 0;

            while (i < count)
            {
                var creater = new AmazonTsvCreater();
                var fileName = delGetFileName(listMallProductTmp.First().site_id.Value, divide);

                using (var writer = creater.GetWriter(dirPath, fileName))
                {
                    var tsv = new amazon_tsv();

                    // ヘッダ [Header]
                    creater.WriteLine(writer, "TemplateType=HomeImprovement	Version=2014.0123");    // テンプレート情報 [Template information]
                    creater.WriteCsvHeader<amazon_jp_tsv>(writer, value => value);                  // 日本語ヘッダ名 [Japanese header name]
                    creater.WriteCsvHeader<amazon_tsv>(writer, value => value);                     // 英語ヘッダ名 [English header name]

                    for (; i < count; i++)
                    {
                        // AmazonTSVデータセット [Get the TSV for Amazon]
                        tsv = this.SetAmazonTsv(tsv, listMallProductTmp[i], extractDateTime);

                        var body = creater.CreateBody(tsv);

                        if (creater.CheckWriteLength(body, AMAZON_TSV_DIVIDE_BYTE))
                        {
                            // ボディ [Body]
                            creater.WriteLine(writer, body);
                        }
                        else
                        {
                            break;
                        }
                    }

                    divide++;
                }

                yield return fileName;
            }
        }
        #endregion

        #region AmazonTSVデータセット [Get the TSV for Amazon]
        /// <summary>
        /// AmazonTSVデータセット [Get the TSV for Amazon]
        /// </summary>
        /// <param name="tsv">TSVモデル [TSV model]</param>
        /// <param name="product">商品 [Product]</param>
        /// <param name="extractDateTime">抽出日時 [Extract datetime]</param>
        /// <returns>TSVモデル [TSV model]</returns>
        protected virtual amazon_tsv SetAmazonTsv(amazon_tsv tsv, ErsMallProductTmp product, ErsCommonStruct.DateTimeStartEnd extractDateTime)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var operationType = ErsMallFactory.ersMallProductFactory.GetObtainProductOperationTypeStgy().Obtain(product, extractDateTime.dateFrom.Value, extractDateTime.dateTo.Value);

            tsv.item_sku = product.mall_scode;
            //tsv.external_product_id = product.scode;
            //tsv.external_product_id_type = "EAN";

            tsv.item_name = product.sname;

            tsv.brand_name = string.Empty;
            tsv.manufacturer = string.Empty;

            tsv.feed_product_type = product.product_type;

            tsv.part_number = product.sname;

            var product_description = product.product_description;

            // HTML可
            tsv.product_description = product_description.HasValue() ? product_description.Replace(Environment.NewLine, ErsViewHelper.TAG_NEW_LINE) : null;

            tsv.standard_price = Convert.ToString(product.price);
            tsv.currency = "JPY";

            tsv.item_package_quantity = "1";

            var objStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(product.scode);
            tsv.quantity = objStock != null ? Convert.ToString(objStock.stock) : "0";

            tsv.fulfillment_latency = setup.sendday.ToString();    // リードタイムを設定する場合在庫数が必須となる。

            tsv.condition_type = "New";
            tsv.condition_note = string.Empty;

            var product_site_launch_date = string.Empty;

            if (operationType != EnumMallProductOperationType.DELETE)
            {
                product_site_launch_date = product.date_from.HasValue ? product.date_from.Value.ToString("yyyy-MM-ddTHH:mm:ss+09:00") : null;
            }
            else
            {
                product_site_launch_date = AMAZON_FUTURE_DATETIME_FOR_DELETE;
            }

            tsv.product_site_launch_date = product_site_launch_date;

            tsv.list_price = Convert.ToString(product.p_price2);

            tsv.offering_can_be_gift_messaged = "false";
            tsv.offering_can_be_giftwrapped = "false";
            tsv.is_discontinued_by_manufacturer = "false";
            tsv.missing_keyset_reason = "PrivateLabel";

            tsv.bullet_point1 = string.Empty;
            tsv.bullet_point2 = product.sname;
            tsv.bullet_point3 = string.Empty;
            tsv.bullet_point4 = string.Empty;
            tsv.bullet_point5 = string.Empty;

            tsv.recommended_browse_nodes1 = string.Empty;

            this.SetImageUrl(tsv, product.gcode, product.scode);

            return tsv;
        }
        #endregion
    }
}
