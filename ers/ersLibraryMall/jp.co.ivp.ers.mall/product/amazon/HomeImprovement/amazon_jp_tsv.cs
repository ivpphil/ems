﻿using System.ComponentModel;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.product.amazon.HomeImprovement
{
    /// <summary>
    /// Amazon用TSVモデル（日本語） [TSV Model for Amazon (Japanese)]
    /// </summary>
    public class amazon_jp_tsv
        : amazon_tsv
    {
        #region 商品基本情報 - すべての商品に必須の項目
        /// <summary>
        /// item_sku : 商品管理番号
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_sku")]
        public override string item_sku { get; set; }

        /// <summary>
        /// external_product_id : 商品コード(JANコード等)
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.external_product_id")]
        public override string external_product_id { get; set; }

        /// <summary>
        /// external_product_id_type : 商品コードのタイプ
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.external_product_id_type")]
        public override string external_product_id_type { get; set; }

        /// <summary>
        /// item_name : 商品名
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_name")]
        public override string item_name { get; set; }

        /// <summary>
        /// brand_name : ブランド名
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.brand_name")]
        public override string brand_name { get; set; }

        /// <summary>
        /// manufacturer : メーカー名
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.manufacturer")]
        public override string manufacturer { get; set; }

        /// <summary>
        /// feed_product_type : 商品タイプ
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.feed_product_type")]
        public override string feed_product_type { get; set; }

        /// <summary>
        /// part_number : メーカー型番
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.part_number")]
        public override string part_number { get; set; }

        /// <summary>
        /// part_number : 商品説明文
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.product_description")]
        public override string product_description { get; set; }

        /// <summary>
        /// update_delete : アップデート・削除
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.update_delete")]
        public override string update_delete { get; set; }
        #endregion

        #region 販売情報 - 商品をサイト上で販売可能にする際に必要な項目
        /// <summary>
        /// standard_price : 商品の販売価格
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.standard_price")]
        public override string standard_price { get; set; }

        /// <summary>
        /// currency : 通貨コード
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.currency")]
        public override string currency { get; set; }

        /// <summary>
        /// item_package_quantity : パッケージ商品数
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_package_quantity")]
        public override string item_package_quantity { get; set; }

        /// <summary>
        /// quantity : 在庫数
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.quantity")]
        public override string quantity { get; set; }

        /// <summary>
        /// fulfillment_latency : リードタイム(出荷までにかかる作業日数)
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.fulfillment_latency")]
        public override string fulfillment_latency { get; set; }

        /// <summary>
        /// condition_type : 商品のコンディション
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.condition_type")]
        public override string condition_type { get; set; }

        /// <summary>
        /// condition_note : 商品のコンディション説明
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.condition_note")]
        public override string condition_note { get; set; }

        /// <summary>
        /// product_site_launch_date : 商品の公開日
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.product_site_launch_date")]
        public override string product_site_launch_date { get; set; }

        /// <summary>
        /// merchant_release_date : 予約商品の販売開始日
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.merchant_release_date")]
        public override string merchant_release_date { get; set; }

        /// <summary>
        /// restock_date : 商品の入荷予定日
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.restock_date")]
        public override string restock_date { get; set; }

        /// <summary>
        /// list_price : メーカー希望小売価格
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.list_price")]
        public override string list_price { get; set; }

        /// <summary>
        /// optional_payment_type_exclusion : 使用しない支払い方法
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.optional_payment_type_exclusion")]
        public override string optional_payment_type_exclusion { get; set; }

        /// <summary>
        /// sale_price : セール価格
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.sale_price")]
        public override string sale_price { get; set; }

        /// <summary>
        /// delivery_schedule_group_id : 配送日時指定SKUリスト
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.delivery_schedule_group_id")]
        public override string delivery_schedule_group_id { get; set; }

        /// <summary>
        /// sale_from_date : セール開始日
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.sale_from_date")]
        public override string sale_from_date { get; set; }

        /// <summary>
        /// sale_end_date : セール終了日
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.sale_end_date")]
        public override string sale_end_date { get; set; }

        /// <summary>
        /// max_order_quantity : 最大注文個数
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.max_order_quantity")]
        public override string max_order_quantity { get; set; }

        /// <summary>
        /// offering_can_be_gift_messaged : ギフトメッセージ
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.offering_can_be_gift_messaged")]
        public override string offering_can_be_gift_messaged { get; set; }

        /// <summary>
        /// offering_can_be_giftwrapped : ギフト包装可
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.offering_can_be_giftwrapped")]
        public override string offering_can_be_giftwrapped { get; set; }

        /// <summary>
        /// is_discontinued_by_manufacturer : メーカー製造中止
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.is_discontinued_by_manufacturer")]
        public override string is_discontinued_by_manufacturer { get; set; }

        /// <summary>
        /// missing_keyset_reason : 商品コードなしの理由
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.missing_keyset_reason")]
        public override string missing_keyset_reason { get; set; }
        #endregion

        #region 寸法 - 商品のサイズや重量を入力する項目
        /// <summary>
        /// website_shipping_weight : 配送重量
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.website_shipping_weight")]
        public override string website_shipping_weight { get; set; }

        /// <summary>
        /// website_shipping_weight_unit_of_measure : 配送重量の単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.website_shipping_weight_unit_of_measure")]
        public override string website_shipping_weight_unit_of_measure { get; set; }

        /// <summary>
        /// item_weight : 商品の重量
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_weight")]
        public override string item_weight { get; set; }

        /// <summary>
        /// item_weight_unit_of_measure : 商品の重量の単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_weight_unit_of_measure")]
        public override string item_weight_unit_of_measure { get; set; }

        /// <summary>
        /// item_length : 商品の長さ
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_length")]
        public override string item_length { get; set; }

        /// <summary>
        /// item_height : 商品の高さ
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_height")]
        public override string item_height { get; set; }

        /// <summary>
        /// item_width : 商品の幅
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_width")]
        public override string item_width { get; set; }

        /// <summary>
        /// item_length_unit_of_measure : 1ユニットの内容量
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_length_unit_of_measure")]
        public override string item_length_unit_of_measure { get; set; }

        /// <summary>
        /// item_display_volume : 1ユニットの内容量
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_display_volume")]
        public override string item_display_volume { get; set; }

        /// <summary>
        /// item_display_volume_unit_of_measure : 1ユニットの内容量の単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_display_volume_unit_of_measure")]
        public override string item_display_volume_unit_of_measure { get; set; }

        /// <summary>
        /// item_display_weight : 1ユニットの重量
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_display_weight")]
        public override string item_display_weight { get; set; }

        /// <summary>
        /// item_display_weight_unit_of_measure : 1ユニットの重量の単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_display_weight_unit_of_measure")]
        public override string item_display_weight_unit_of_measure { get; set; }

        /// <summary>
        /// item_display_length : 1ユニットの長さ
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_display_length")]
        public override string item_display_length { get; set; }

        /// <summary>
        /// item_display_length_unit_of_measure : 1ユニットの長さの単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_display_length_unit_of_measure")]
        public override string item_display_length_unit_of_measure { get; set; }

        /// <summary>
        /// volume_capacity_name : 容積容量
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.volume_capacity_name")]
        public override string volume_capacity_name { get; set; }

        /// <summary>
        /// item_display_diameter : 商品の直径
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_display_diameter")]
        public override string item_display_diameter { get; set; }

        /// <summary>
        /// item_display_diameter_unit_of_measure : 商品の直径の単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_display_diameter_unit_of_measure")]
        public override string item_display_diameter_unit_of_measure { get; set; }

        /// <summary>
        /// item_width_unit_of_measure : 商品の幅の単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_width_unit_of_measure")]
        public override string item_width_unit_of_measure { get; set; }

        /// <summary>
        /// item_height_unit_of_measure : 商品の高さの単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.item_height_unit_of_measure")]
        public override string item_height_unit_of_measure { get; set; }
        #endregion

        #region 商品検索情報 - サーチ上で商品を検索されやすくするために必要な項目
        /// <summary>
        /// bullet_point1 : 商品説明の箇条書き1
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.bullet_point1")]
        public override string bullet_point1 { get; set; }

        /// <summary>
        /// bullet_point2 : 商品説明の箇条書き2
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.bullet_point2")]
        public override string bullet_point2 { get; set; }

        /// <summary>
        /// bullet_point3 : 商品説明の箇条書き3
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.bullet_point3")]
        public override string bullet_point3 { get; set; }

        /// <summary>
        /// bullet_point4 : 商品説明の箇条書き4
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.bullet_point4")]
        public override string bullet_point4 { get; set; }

        /// <summary>
        /// bullet_point5 : 商品説明の箇条書き5
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.bullet_point5")]
        public override string bullet_point5 { get; set; }

        /// <summary>
        /// generic_keywords1 : 検索キーワード1
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.generic_keywords1")]
        public override string generic_keywords1 { get; set; }

        /// <summary>
        /// generic_keywords2 : 検索キーワード2
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.generic_keywords2")]
        public override string generic_keywords2 { get; set; }

        /// <summary>
        /// generic_keywords3 : 検索キーワード3
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.generic_keywords3")]
        public override string generic_keywords3 { get; set; }

        /// <summary>
        /// generic_keywords4 : 検索キーワード4
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.generic_keywords4")]
        public override string generic_keywords4 { get; set; }

        /// <summary>
        /// generic_keywords5 : 検索キーワード5
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.generic_keywords5")]
        public override string generic_keywords5 { get; set; }

        /// <summary>
        /// recommended_browse_nodes1 : 推奨ブラウズノード1
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.recommended_browse_nodes1")]
        public override string recommended_browse_nodes1 { get; set; }

        /// <summary>
        /// recommended_browse_nodes2 : 推奨ブラウズノード2
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.recommended_browse_nodes2")]
        public override string recommended_browse_nodes2 { get; set; }

        /// <summary>
        /// catalog_number : 出品者カタログ番号
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.catalog_number")]
        public override string catalog_number { get; set; }

        /// <summary>
        /// specific_uses_keywords1 : 用途1
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.specific_uses_keywords1")]
        public override string specific_uses_keywords1 { get; set; }

        /// <summary>
        /// specific_uses_keywords2 : 用途2
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.specific_uses_keywords2")]
        public override string specific_uses_keywords2 { get; set; }

        /// <summary>
        /// specific_uses_keywords3 : 用途3
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.specific_uses_keywords3")]
        public override string specific_uses_keywords3 { get; set; }

        /// <summary>
        /// specific_uses_keywords4 : 用途4
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.specific_uses_keywords4")]
        public override string specific_uses_keywords4 { get; set; }

        /// <summary>
        /// specific_uses_keywords5 : 用途5
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.specific_uses_keywords5")]
        public override string specific_uses_keywords5 { get; set; }

        /// <summary>
        /// platinum_keywords1 : プラチナキーワード1
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.platinum_keywords1")]
        public override string platinum_keywords1 { get; set; }

        /// <summary>
        /// platinum_keywords2 : プラチナキーワード2
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.platinum_keywords2")]
        public override string platinum_keywords2 { get; set; }

        /// <summary>
        /// platinum_keywords3 : プラチナキーワード3
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.platinum_keywords3")]
        public override string platinum_keywords3 { get; set; }

        /// <summary>
        /// platinum_keywords4 : プラチナキーワード4
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.platinum_keywords4")]
        public override string platinum_keywords4 { get; set; }

        /// <summary>
        /// platinum_keywords5 : プラチナキーワード5
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.platinum_keywords5")]
        public override string platinum_keywords5 { get; set; }
        #endregion

        #region 画像 - 商品画像を表示させるために必要な項目。詳しくは画像説明タブを参照
        /// <summary>
        /// main_image_url : 商品メイン画像URL
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.main_image_url")]
        public override string main_image_url { get; set; }

        /// <summary>
        /// swatch_image_url : カラーサンプル画像URL
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.swatch_image_url")]
        public override string swatch_image_url { get; set; }

        /// <summary>
        /// other_image_url1  : 商品のサブ画像URL1
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.other_image_url1")]
        public override string other_image_url1 { get; set; }

        /// <summary>
        /// other_image_url2 : 商品のサブ画像URL2
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.other_image_url2")]
        public override string other_image_url2 { get; set; }

        /// <summary>
        /// other_image_url3 : 商品のサブ画像URL3
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.other_image_url3")]
        public override string other_image_url3 { get; set; }

        /// <summary>
        /// other_image_url4 : 商品のサブ画像URL4
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.other_image_url4")]
        public override string other_image_url4 { get; set; }

        /// <summary>
        /// other_image_url5 : 商品のサブ画像URL5
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.other_image_url5")]
        public override string other_image_url5 { get; set; }

        /// <summary>
        /// other_image_url6 : 商品のサブ画像URL6
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.other_image_url6")]
        public override string other_image_url6 { get; set; }

        /// <summary>
        /// other_image_url7 : 商品のサブ画像URL7
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.other_image_url7")]
        public override string other_image_url7 { get; set; }

        /// <summary>
        /// other_image_url8 : 商品のサブ画像URL8
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.other_image_url8")]
        public override string other_image_url8 { get; set; }
        #endregion

        #region 出荷関連情報 - フルフィルメント by Amazon (FBA) の利用、あるいは自社出荷の注文に関する出荷関連情報を、この項目に記入してください。FBA を利用する場合には必須の項目。
        /// <summary>
        /// fulfillment_center_id : フルフィルメントセンターID
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.fulfillment_center_id")]
        public override string fulfillment_center_id { get; set; }

        /// <summary>
        /// package_length : 商品パッケージの長さ
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.package_length")]
        public override string package_length { get; set; }

        /// <summary>
        /// package_length_unit_of_measure : 商品パッケージの長さの単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.package_length_unit_of_measure")]
        public override string package_length_unit_of_measure { get; set; }

        /// <summary>
        /// package_width : 商品パッケージの幅
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.package_width")]
        public override string package_width { get; set; }

        /// <summary>
        /// package_width_unit_of_measure : 商品パッケージの幅の単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.package_width_unit_of_measure")]
        public override string package_width_unit_of_measure { get; set; }

        /// <summary>
        /// package_height : 商品パッケージの高さ
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.package_height")]
        public override string package_height { get; set; }

        /// <summary>
        /// package_height_unit_of_measure : 商品パッケージの高さの単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.package_height_unit_of_measure")]
        public override string package_height_unit_of_measure { get; set; }

        /// <summary>
        /// package_weight : 商品パッケージの重量
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.package_weight")]
        public override string package_weight { get; set; }

        /// <summary>
        /// package_weight_unit_of_measure : 商品パッケージの重量の単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.package_weight_unit_of_measure")]
        public override string package_weight_unit_of_measure { get; set; }
        #endregion

        #region バリエーション情報 - 商品の色・サイズなどのバリエーションを作成する際に必須の項目
        /// <summary>
        /// parent_child : 親子関係の指定
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.parent_child")]
        public override string parent_child { get; set; }

        /// <summary>
        /// parent_sku : 親商品のSKU(商品管理番号)
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.parent_sku")]
        public override string parent_sku { get; set; }

        /// <summary>
        /// relationship_type : 親子関係のタイプ
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.relationship_type")]
        public override string relationship_type { get; set; }

        /// <summary>
        /// variation_theme : バリエーションテーマ
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.variation_theme")]
        public override string variation_theme { get; set; }
        #endregion

        #region コンプライアンス情報 - 商品を販売する国または地域の特定商取引に遵守するために利用される項目
        /// <summary>
        /// legal_disclaimer_description : 法規上の免責条項
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.legal_disclaimer_description")]
        public override string legal_disclaimer_description { get; set; }

        /// <summary>
        /// country_of_origin : 生産国
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.country_of_origin")]
        public override string country_of_origin { get; set; }
        #endregion

        #region ProductTypeによって必須となる項目
        /// <summary>
        /// seller_warranty_description : 出品者保証の説明
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.seller_warranty_description")]
        public override string seller_warranty_description { get; set; }

        /// <summary>
        /// warranty_description : メーカー保証の説明
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.warranty_description")]
        public override string warranty_description { get; set; }

        /// <summary>
        /// mfg_warranty_description_type : 製品保証タイプ
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.mfg_warranty_description_type")]
        public override string mfg_warranty_description_type { get; set; }

        /// <summary>
        /// size_name : サイズ
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.size_name")]
        public override string size_name { get; set; }

        /// <summary>
        /// color_name : カラー
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.color_name")]
        public override string color_name { get; set; }

        /// <summary>
        /// color_map1 : カラーマップ1
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.color_map1")]
        public override string color_map1 { get; set; }

        /// <summary>
        /// color_map2 : カラーマップ2
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.color_map2")]
        public override string color_map2 { get; set; }

        /// <summary>
        /// grit_number : 粒度
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.grit_number")]
        public override string grit_number { get; set; }

        /// <summary>
        /// wattage : ワット数
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.wattage")]
        public override string wattage { get; set; }

        /// <summary>
        /// voltage : 電圧
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.voltage")]
        public override string voltage { get; set; }

        /// <summary>
        /// battery_capacity : バッテリ容量
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.battery_capacity")]
        public override string battery_capacity { get; set; }

        /// <summary>
        /// power_source_type : 電源
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.power_source_type")]
        public override string power_source_type { get; set; }

        /// <summary>
        /// minimum : 最少年齢
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.minimum")]
        public override string minimum { get; set; }

        /// <summary>
        /// minimum_unit_of_measure : 推奨最少年齢の単位
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.minimum_unit_of_measure")]
        public override string minimum_unit_of_measure { get; set; }

        /// <summary>
        /// material_type : 素材
        /// </summary>
        [TsvField(TsvFieldAttribute.NON_FIXED)]
        [DisplayName("amazon.jp.material_type")]
        public override string material_type { get; set; }
        #endregion
    }
}
