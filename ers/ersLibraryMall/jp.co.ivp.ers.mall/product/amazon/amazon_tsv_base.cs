﻿using System.ComponentModel;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.product.amazon
{
    /// <summary>
    /// Amazon用TSVモデル [TSV Model for Amazon]
    /// </summary>
    public class amazon_tsv_base
        : ErsModelBase
    {
        /// <summary>
        /// main_image_url : 商品メイン画像URL
        /// </summary>
        public virtual string main_image_url { get; set; }

        /// <summary>
        /// other_image_url1  : 商品のサブ画像URL1
        /// </summary>
        public virtual string other_image_url1 { get; set; }

        /// <summary>
        /// other_image_url2 : 商品のサブ画像URL2
        /// </summary>
        public virtual string other_image_url2 { get; set; }
    }
}
