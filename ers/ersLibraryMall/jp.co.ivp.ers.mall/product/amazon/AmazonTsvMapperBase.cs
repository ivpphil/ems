﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.common;
using jp.co.ivp.ers.mall.common;

namespace jp.co.ivp.ers.mall.product.amazon
{
    /// <summary>
    /// AmazonTSVマッパー [Amazon TSV mapper]
    /// </summary>
    public abstract class AmazonTsvMapperBase
    {
        #region 定数 [Constant]
        /// <summary>
        /// AmazonTSV分割容量（バイト） [Amazon TSV divide size (Byte)]
        /// </summary>
        protected const int AMAZON_TSV_DIVIDE_BYTE = 5242880;

        /// <summary>
        /// 商品削除（非表示）用未来日時 [Dateime for delete (hide) product]
        /// </summary>
        protected const string AMAZON_FUTURE_DATETIME_FOR_DELETE = "2200-01-01T00:00:00+09:00";
        #endregion


        #region マップ [Map]
        /// <summary>
        /// マップ [Map]
        /// </summary>
        /// <param name="dirPath">出力ディレクトリパス [Output directory path]</param>
        /// <param name="delGetFileName">ファイル名取得デリゲート [Delegate for Get file name]</param>
        /// <param name="listMallProductTmp">商品リスト [The list of products]</param>
        /// <param name="extractDateTime">抽出日時 [Extract datetime]</param>
        /// <returns>出力ファイルパスリスト [The list of output file path]</returns>
        public abstract IEnumerable<string> Map(string dirPath, GetFileName delGetFileName, IList<ErsMallProductTmp> listMallProductTmp, ErsCommonStruct.DateTimeStartEnd extractDateTime);

        /// <summary>
        /// ファイル名取得（デリゲート） [Get file name (Delegate)]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="divide">分割カウント [divide count]</param>
        /// <returns>ファイル名 [File name]</returns>
        public delegate string GetFileName(int siteId, int divide);
        #endregion

        #region 画像URLセット [Set image URL]
        /// <summary>
        /// 画像URLセット [Set image URL]
        /// </summary>
        /// <param name="tsv">TSVモデル [TSV model]</param>
        /// <param name="gcode">グループコード [ITEM code]</param>
        /// <param name="scode">商品コード [SKU code]</param>
        /// <returns>画像URL [Image URL]</returns>
        protected virtual void SetImageUrl(amazon_tsv_base tsv, string gcode, string scode)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            // SKU [SKU]
            var filePath = string.Format(ErsCommonConst.SKU_IMAGE_PATH_FORMAT, setup.image_directory, scode, 1);

            if (File.Exists(filePath))
            {
                tsv.main_image_url = string.Format(ErsCommonConst.SKU_IMAGE_URL_FORMAT, setup.pc_nor_url, gcode, scode, 1);
            }

            filePath = string.Format(ErsCommonConst.SKU_IMAGE_PATH_FORMAT, setup.image_directory, scode, 2);

            if (File.Exists(filePath))
            {
                tsv.other_image_url1 = string.Format(ErsCommonConst.SKU_IMAGE_URL_FORMAT, setup.pc_nor_url, gcode, scode, 2);
            }

            filePath = string.Format(ErsCommonConst.SKU_IMAGE_PATH_FORMAT, setup.image_directory, scode, 3);

            if (File.Exists(filePath))
            {
                tsv.other_image_url2 = string.Format(ErsCommonConst.SKU_IMAGE_URL_FORMAT, setup.pc_nor_url, gcode, scode, 3);
            }
        }
        #endregion
    }
}
