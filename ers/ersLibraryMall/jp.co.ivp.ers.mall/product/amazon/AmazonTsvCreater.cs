﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.product.amazon
{
    /// <summary>
    /// AmazonTSVクリエイター [Amazon TSV creater]
    /// </summary>
    public class AmazonTsvCreater
        : ErsTsvCreater
    {
        /// <summary>
        /// エンコーディング [Encoding]
        /// </summary>
        protected virtual Encoding enc { get; set; }

        /// <summary>
        /// 書き込みバイト数 [Writing byte count]
        /// </summary>
        public virtual long length { get; protected set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public AmazonTsvCreater()
            : base()
        {
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="enc"></param>
        /// <returns></returns>
        public override StreamWriter GetWriter(string path, string fileName, bool mode = true, Encoding enc = null)
        {
            this.enc = enc;

            if (this.enc == null)
            {
                this.enc = ErsEncoding.ShiftJIS;
            }

            this.filePath = Path.Combine(path, fileName);

            return new System.IO.StreamWriter(this.filePath, mode, this.enc);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="writer"></param>
        /// <param name="formatter"></param>
        public override void WriteCsvHeader(IEnumerable<string> columns, StreamWriter writer, Func<string, string> formatter)
        {
            var header = string.Empty;
            foreach (var column in columns)
            {
                header += "\t" + formatter(column);
            }
            if (!string.IsNullOrEmpty(header))
            {
                this.WriteLine(writer, header.Substring(1));
            }
        }

        /// <summary>
        /// 書き込み
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="str"></param>
        public virtual void WriteLine(StreamWriter writer, string str)
        {
            this.length += this.enc.GetByteCount(str);
            writer.WriteLine(str);
        }

        /// <summary>
        /// 書き込みチェック
        /// </summary>
        /// <param name="str"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public virtual bool CheckWriteLength(string str, int length)
        {
            return this.length + this.enc.GetByteCount(str) < length;
        }
    }
}
