﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.product.amazon.Health
{
    /// <summary>
    /// AmazonTSVマッパー [Amazon TSV mapper]
    /// </summary>
    public class AmazonTsvMapper
        : AmazonTsvMapperBase
    {
        #region マップ [Map]
        /// <summary>
        /// マップ [Map]
        /// </summary>
        /// <param name="dirPath">出力ディレクトリパス [Output directory path]</param>
        /// <param name="delGetFileName">ファイル名取得デリゲート [Delegate for Get file name]</param>
        /// <param name="listMallProductTmp">商品リスト [The list of products]</param>
        /// <param name="extractDateTime">抽出日時 [Extract datetime]</param>
        /// <returns>出力ファイルパスリスト [The list of output file path]</returns>
        public override IEnumerable<string> Map(string dirPath, GetFileName delGetFileName, IList<ErsMallProductTmp> listMallProductTmp, ErsCommonStruct.DateTimeStartEnd extractDateTime)
        {
            int i = 0;

            int count = listMallProductTmp.Count();
            int divide = 0;

            while (i < count)
            {
                var creater = new AmazonTsvCreater();
                var fileName = delGetFileName(listMallProductTmp.First().site_id.Value, divide);

                using (var writer = creater.GetWriter(dirPath, fileName))
                {
                    var tsv = new amazon_tsv();

                    // ヘッダ [Header]
                    creater.WriteLine(writer, "TemplateType=Health	Version=2014.0212");    // テンプレート情報 [Template information]
                    creater.WriteCsvHeader<amazon_jp_tsv>(writer, value => value);          // 日本語ヘッダ名 [Japanese header name]
                    creater.WriteCsvHeader<amazon_tsv>(writer, value => value);             // 英語ヘッダ名 [English header name]

                    for (; i < count; i++)
                    {
                        // AmazonTSVデータセット [Get the TSV for Amazon]
                        tsv = this.SetAmazonTsv(tsv, listMallProductTmp[i], extractDateTime);

                        var body = creater.CreateBody(tsv);

                        if (creater.CheckWriteLength(body, AMAZON_TSV_DIVIDE_BYTE))
                        {
                            // ボディ [Body]
                            creater.WriteLine(writer, body);
                        }
                        else
                        {
                            break;
                        }
                    }

                    divide++;
                }

                yield return fileName;
            }
        }
        #endregion

        #region AmazonTSVデータセット [Get the TSV for Amazon]
        /// <summary>
        /// AmazonTSVデータセット [Get the TSV for Amazon]
        /// </summary>
        /// <param name="tsv">TSVモデル [TSV model]</param>
        /// <param name="product">商品 [Product]</param>
        /// <param name="extractDateTime">抽出日時 [Extract datetime]</param>
        /// <returns>TSVモデル [TSV model]</returns>
        protected virtual amazon_tsv SetAmazonTsv(amazon_tsv tsv, ErsMallProductTmp product, ErsCommonStruct.DateTimeStartEnd extractDateTime)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var operationType = ErsMallFactory.ersMallProductFactory.GetObtainProductOperationTypeStgy().Obtain(product, extractDateTime.dateFrom.Value, extractDateTime.dateTo.Value);

            // 商品基本情報 - すべての商品に必須の項目
            tsv.item_sku = product.mall_scode;
            tsv.item_name = product.sname;
            tsv.external_product_id = product.jancode;
            tsv.external_product_id_type = "EAN";
            tsv.brand_name = product.brand;
            tsv.manufacturer = product.maker_name;
            tsv.feed_product_type = product.product_type;
            tsv.part_number = product.model_number;
            // HTML可
            tsv.product_description = product.product_description.HasValue() ? product.product_description.Replace(Environment.NewLine, ErsViewHelper.TAG_NEW_LINE) : null;

            // 販売情報 - 商品をサイト上で販売可能にする際に必要な項目
            tsv.item_package_quantity = product.item_package_quantity;
            var objStock = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetErsStockWithScode(product.scode);
            tsv.quantity = objStock != null ? objStock.stock : 0;
            tsv.fulfillment_latency = setup.sendday.ToString();    // リードタイムを設定する場合在庫数が必須となる。
            tsv.standard_price = product.price * ((setup.tax / 100) + 1);
            tsv.currency = "JPY";
            tsv.condition_type = "New";
            tsv.condition_note = product.condition_note;
            var product_site_launch_date = string.Empty;
            if (operationType != EnumMallProductOperationType.DELETE)
            {
                product_site_launch_date = product.date_from.HasValue ? product.date_from.Value.ToString("yyyy-MM-ddTHH:mm:ss+09:00") : null;
            }
            else
            {
                product_site_launch_date = AMAZON_FUTURE_DATETIME_FOR_DELETE;
            }
            tsv.product_site_launch_date = product_site_launch_date;
            tsv.list_price = product.p_price2 * ((setup.tax / 100) + 1);
            tsv.sale_price = product.sale_price;
            tsv.sale_from_date = product.sale_from_date.HasValue ? product.sale_from_date.Value.ToString("yyyy-MM-ddTHH:mm:ss+09:00") : null;
            tsv.sale_end_date = product.sale_end_date.HasValue ? product.sale_end_date.Value.ToString("yyyy-MM-ddTHH:mm:ss+09:00") : null;
            tsv.max_order_quantity = product.max_purchase_count == 0 ? null : product.max_purchase_count;
            tsv.offering_can_be_gift_messaged = product.offering_can_be_gift_messaged == EnumOnOff.On ? "True" : "False";
            tsv.offering_can_be_giftwrapped = product.offering_can_be_giftwrapped == EnumOnOff.On ? "True" : "False";
            tsv.missing_keyset_reason = null; // 独自SKUコードのみで出品可能な申請契約をしている場合に設定
            // 寸法 - 商品のサイズや重量を入力する項目

            // 商品検索情報 - サーチ上で商品を検索されやすくするために必要な項目
            tsv.bullet_point1 = product.bullet_point1;
            tsv.bullet_point2 = product.bullet_point2;
            tsv.bullet_point3 = product.bullet_point3;
            tsv.bullet_point4 = product.bullet_point4;
            tsv.bullet_point5 = product.bullet_point5;
            tsv.generic_keywords1 = product.generic_keywords1;
            tsv.generic_keywords2 = product.generic_keywords2;
            tsv.generic_keywords3 = product.generic_keywords3;
            tsv.generic_keywords4 = product.generic_keywords4;
            tsv.generic_keywords5 = product.generic_keywords5;
            tsv.recommended_browse_nodes1 = product.recommended_browse_nodes1;
            tsv.recommended_browse_nodes2 = product.recommended_browse_nodes2;
            tsv.specific_uses_keywords1 = product.specific_uses_keywords1;
            tsv.specific_uses_keywords2 = product.specific_uses_keywords2;
            tsv.target_audience_keywords = product.target_audience_keywords;

            //コンプライアンス情報 - 商品を販売する国または地域の特定商取引に遵守するために利用される項目
            tsv.safety_warning = product.safety_warning;
            tsv.legal_disclaimer_description = product.legal_disclaimer_description;

            //HEALTH_PERSONAL_CARE
            tsv.ingredients1 = product.ingredients1;
            tsv.ingredients2 = product.ingredients2;
            tsv.ingredients3 = product.ingredients3;
            tsv.special_ingredients = product.special_ingredients;
            tsv.indications = product.indications;
            tsv.directions = product.directions;
            tsv.style_name = product.style_name;
            tsv.flavor_name = product.flavor_name;
            tsv.size_name = product.size_name;
            tsv.color_name = product.color_name;
            tsv.color_map = product.color_map;
            tsv.scent_name = product.scent_name;
            tsv.item_form = product.item_form;
            tsv.special_features1 = product.special_features1;
            tsv.special_features2 = product.special_features2;
            tsv.special_features3 = product.special_features3;
            tsv.minimum_weight_recommendation = product.minimum_weight_recommendation;
            tsv.maximum_weight_recommendation = product.maximum_weight_recommendation;
            tsv.weight_recommendation_unit_of_measure = product.weight_recommendation_unit_of_measure;

            this.SetImageUrl(tsv, product.gcode, product.scode);

            return tsv;
        }
        #endregion
    }
}
