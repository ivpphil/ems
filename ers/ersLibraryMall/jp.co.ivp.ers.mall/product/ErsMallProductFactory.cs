﻿using jp.co.ivp.ers.mall.product.amazon;
using jp.co.ivp.ers.mall.product.harc;
using jp.co.ivp.ers.mall.product.rakuten;
using jp.co.ivp.ers.mall.product.specification;
using jp.co.ivp.ers.mall.product.yahoo;
using jp.co.ivp.ers.mall.product.strategy;

namespace jp.co.ivp.ers.mall.product
{
    /// <summary>
    /// モール商品情報関連ファクトリ [Factory for product information]
    /// </summary>
    public class ErsMallProductFactory
    {
        #region HARC [HARC]
        /// <summary>
        /// ベースCSVモデル（HARC）取得 [Get the instance of base csv model (HARC)]
        /// </summary>
        /// <returns></returns>
        public harc_base_csv GetHarcBaseCsvModel()
        {
            return new harc_base_csv();
        }

        /// <summary>
        /// 楽天CSVモデル（HARC）取得 [Get the instance of rakuten csv model (HARC)]
        /// </summary>
        /// <returns></returns>
        public harc_rakuten_csv GetHarcRakutenCsvModel()
        {
            return new harc_rakuten_csv();
        }

        /// <summary>
        /// Yahoo!CSVモデル（HARC）取得 [Get the instance of yahoo csv model (HARC)]
        /// </summary>
        /// <returns></returns>
        public harc_yahoo_csv GetHarcYahooCsvModel()
        {
            return new harc_yahoo_csv();
        }

        /// <summary>
        /// AmazonCSVモデル（HARC）取得 [Get the instance of amazon csv model (HARC)]
        /// </summary>
        /// <returns></returns>
        public harc_amazon_csv GetHarcAmazonCsvModel()
        {
            return new harc_amazon_csv();
        }

        /// <summary>
        /// EcCubeCSVモデル（HARC）取得 [Get the instance of ec cube csv model (HARC)]
        /// </summary>
        /// <returns></returns>
        public harc_ec_cube_csv GetHarcEccubeCsvModel()
        {
            return new harc_ec_cube_csv();
        }
        #endregion

        #region 楽天 [Rakuten]
        /// <summary>
        /// 楽天CSVモデル取得 [Get the instance of csv model (Rakuten)]
        /// </summary>
        /// <returns></returns>
        public rakuten_csv GetRakutenCsvModel()
        {
            return new rakuten_csv();
        }

        /// <summary>
        /// 楽天カテゴリCSVモデル取得 [Get the instance of csv model (Rakuten category)]
        /// </summary>
        /// <returns></returns>
        public rakuten_cat_csv GetRakutenCategoryCsvModel()
        {
            return new rakuten_cat_csv();
        }
        #endregion

        #region Yahoo! [Yahoo!]
        /// <summary>
        /// Yahoo!CSVモデル取得 [Get the instance of csv model (Yahoo!)]
        /// </summary>
        /// <returns></returns>
        public yahoo_csv GetYahooCsvModel()
        {
            return new yahoo_csv();
        }
        #endregion

        #region モール商品 [Mall product]
        /// <summary>
        /// モール商品エンティティ取得 [Get mall product]
        /// </summary>
        /// <returns>Instance of ErsMallMerchandise</returns>
        public virtual ErsMallMerchandise GetErsMallMerchandise()
        {
            return new ErsMallMerchandise();
        }

        /// <summary>
        /// モール商品リポジトリ取得 [Get mall product repository]
        /// </summary>
        /// <returns>Instance of ErsMallMerchandiseRepository</returns>
        public virtual ErsMallMerchandiseRepository GetErsMallMerchandiseRepository()
        {
            return new ErsMallMerchandiseRepository();
        }

        /// <summary>
        /// モール商品クライテリア取得 [Get mall product criteria]
        /// </summary>
        /// <returns>Instance of ErsMallMerchandiseCriteria</returns>
        public virtual ErsMallMerchandiseCriteria GetErsMallMerchandiseCriteria()
        {
            return new ErsMallMerchandiseCriteria();
        }

        /// <summary>
        /// モール商品エンティティ取得（IDから） [Get mall product (from ID)]
        /// </summary>
        /// <returns>Instance of ErsMallMerchandise</returns>
        public virtual ErsMallMerchandise GetErsMallMerchandiseById(int? id)
        {
            var repository = this.GetErsMallMerchandiseRepository();
            var criteria = this.GetErsMallMerchandiseCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count == 0)
                return null;

            return list[0];
        }
        #endregion

        #region モール商品抽出管理 [Extract mall product]
        /// <summary>
        /// モール商品抽出管理エンティティ取得 [Get extract mall product]
        /// </summary>
        /// <returns>Instance of ErsMallProductExtract</returns>
        public virtual ErsMallProductExtract GetErsMallProductExtract()
        {
            return new ErsMallProductExtract();
        }

        /// <summary>
        /// モール商品抽出管理リポジトリ取得 [Get extract mall product repository]
        /// </summary>
        /// <returns>Instance of ErsMallProductExtractRepository</returns>
        public virtual ErsMallProductExtractRepository GetErsMallProductExtractRepository()
        {
            return new ErsMallProductExtractRepository();
        }

        /// <summary>
        /// モール商品抽出管理クライテリア取得 [Get extract mall product criteria]
        /// </summary>
        /// <returns>Instance of ErsMallProductExtractCriteria</returns>
        public virtual ErsMallProductExtractCriteria GetErsMallProductExtractCriteria()
        {
            return new ErsMallProductExtractCriteria();
        }
        #endregion

        #region モール商品テンポラリ [Mall product temporary]
        /// <summary>
        /// モール商品テンポラリエンティティ取得 [Get mall product temporary]
        /// </summary>
        /// <returns>Instance of ErsMallOrderImport</returns>
        public virtual ErsMallProductTmp GetErsMallProductTmp()
        {
            return new ErsMallProductTmp();
        }

        /// <summary>
        /// モール商品テンポラリリポジトリ取得 [Get mall product temporary repository]
        /// </summary>
        /// <returns>Instance of ErsMallProductTmpRepository</returns>
        public virtual ErsMallProductTmpRepository GetErsMallProductTmpRepository()
        {
            return new ErsMallProductTmpRepository();
        }

        /// <summary>
        /// モール商品テンポラリクライテリア取得 [Get mall product temporary criteria]
        /// </summary>
        /// <returns>Instance of ErsMallProductTmpCriteria</returns>
        public virtual ErsMallProductTmpCriteria GetErsMallProductTmpCriteria()
        {
            return new ErsMallProductTmpCriteria();
        }
        #endregion

        #region モール商品画像抽出管理 [Extract mall product image]
        /// <summary>
        /// モール商品画像抽出管理エンティティ取得 [Get extract mall product image]
        /// </summary>
        /// <returns>Instance of ErsMallProductImageExtract</returns>
        public virtual ErsMallProductImageExtract GetErsMallProductImageExtract()
        {
            return new ErsMallProductImageExtract();
        }

        /// <summary>
        /// モール商品画像抽出管理リポジトリ取得 [Get extract mall product image repository]
        /// </summary>
        /// <returns>Instance of ErsMallProductImageExtractRepository</returns>
        public virtual ErsMallProductImageExtractRepository GetErsMallProductImageExtractRepository()
        {
            return new ErsMallProductImageExtractRepository();
        }

        /// <summary>
        /// モール商品画像抽出管理クライテリア取得 [Get extract mall product image criteria]
        /// </summary>
        /// <returns>Instance of ErsMallProductImageExtractCriteria</returns>
        public virtual ErsMallProductImageExtractCriteria GetErsMallProductImageExtractCriteria()
        {
            return new ErsMallProductImageExtractCriteria();
        }
        #endregion

        #region モール商品画像テンポラリ [Mall product image temporary]
        /// <summary>
        /// モール商品画像テンポラリエンティティ取得 [Get mall product image temporary]
        /// </summary>
        /// <returns>Instance of ErsMallProductImageTmp</returns>
        public virtual ErsMallProductImageTmp GetErsMallProductImageTmp()
        {
            return new ErsMallProductImageTmp();
        }

        /// <summary>
        /// モール商品画像テンポラリリポジトリ取得 [Get mall product image temporary repository]
        /// </summary>
        /// <returns>Instance of ErsMallProductImageTmpRepository</returns>
        public virtual ErsMallProductImageTmpRepository GetErsMallProductImageTmpRepository()
        {
            return new ErsMallProductImageTmpRepository();
        }

        /// <summary>
        /// モール商品画像テンポラリクライテリア取得 [Get mall product image temporary criteria]
        /// </summary>
        /// <returns>Instance of ErsMallProductImageTmpCriteria</returns>
        public virtual ErsMallProductImageTmpCriteria GetErsMallProductImageTmpCriteria()
        {
            return new ErsMallProductImageTmpCriteria();
        }
        #endregion

        #region モール商品ファイルアップロード管理 [Mall product file upload management]
        /// <summary>
        /// モール商品ファイルアップロード管理エンティティ取得 [Get mall product file upload management]
        /// </summary>
        /// <returns>Instance of ErsMallProductFileUploadManage</returns>
        public virtual ErsMallProductFileUploadManage GetErsMallProductFileUploadManage()
        {
            return new ErsMallProductFileUploadManage();
        }

        /// <summary>
        /// モール商品ファイルアップロード管理リポジトリ取得 [Get mall product file upload management repository]
        /// </summary>
        /// <returns>Instance of ErsMallProductFileUploadManageRepository</returns>
        public virtual ErsMallProductFileUploadManageRepository GetErsMallProductFileUploadManageRepository()
        {
            return new ErsMallProductFileUploadManageRepository();
        }

        /// <summary>
        /// モール商品ファイルアップロード管理クライテリア取得 [Get mall product file upload management criteria]
        /// </summary>
        /// <returns>Instance of ErsMallProductFileUploadManageCriteria</returns>
        public virtual ErsMallProductFileUploadManageCriteria GetErsMallProductFileUploadManageCriteria()
        {
            return new ErsMallProductFileUploadManageCriteria();
        }
        #endregion

        #region モール商品画像ディレクトリ [Mall product image directory]
        /// <summary>
        /// モール商品画像ディレクトリエンティティ取得 [Get mall product image directory]
        /// </summary>
        /// <returns>Instance of ErsMallProductImageDirectory</returns>
        public virtual ErsMallProductImageDirectory GetErsMallProductImageDirectory()
        {
            return new ErsMallProductImageDirectory();
        }

        /// <summary>
        /// モール商品画像ディレクトリリポジトリ取得 [Get mall product image directory repository]
        /// </summary>
        /// <returns>Instance of ErsMallProductImageDirectoryRepository</returns>
        public virtual ErsMallProductImageDirectoryRepository GetErsMallProductImageDirectoryRepository()
        {
            return new ErsMallProductImageDirectoryRepository();
        }

        /// <summary>
        /// モール商品画像ディレクトリクライテリア取得 [Get mall product image directory criteria]
        /// </summary>
        /// <returns>Instance of ErsMallProductImageDirectoryCriteria</returns>
        public virtual ErsMallProductImageDirectoryCriteria GetErsMallProductImageDirectoryCriteria()
        {
            return new ErsMallProductImageDirectoryCriteria();
        }
        #endregion

        #region Amazon商品タイプ [Amazon product type]
        /// <summary>
        /// Amazon商品タイプエンティティ取得 [Get amazon product type]
        /// </summary>
        /// <returns>Instance of ErsMallAmazonProductType</returns>
        public virtual ErsMallAmazonProductType GetErsMallAmazonProductType()
        {
            return new ErsMallAmazonProductType();
        }

        /// <summary>
        /// Amazon商品タイプリポジトリ取得 [Get amazon product type repository]
        /// </summary>
        /// <returns>Instance of ErsMallAmazonProductTypeRepository</returns>
        public virtual ErsMallAmazonProductTypeRepository GetErsMallAmazonProductTypeRepository()
        {
            return new ErsMallAmazonProductTypeRepository();
        }

        /// <summary>
        /// Amazon商品タイプクライテリア取得 [Get amazon product type criteria]
        /// </summary>
        /// <returns>Instance of ErsMallAmazonProductTypeCriteria</returns>
        public virtual ErsMallAmazonProductTypeCriteria GetErsMallAmazonProductTypeCriteria()
        {
            return new ErsMallAmazonProductTypeCriteria();
        }
        #endregion

        /// <summary>
        /// モール商品検索取得 [Get search mall products]
        /// </summary>
        /// <returns>Instance of SearchMallProductsSpec</returns>
        public virtual SearchMallProductsSpec GetSearchMallProductsSpec()
        {
            return new SearchMallProductsSpec();
        }

        /// <summary>
        /// モール商品検索取得 [Get search mall products]
        /// </summary>
        /// <returns>Instance of SearchMallProductsSpec</returns>
        public virtual SearchMallMerchandiseSpec GetSearchMallMerchandiseSpec()
        {
            return new SearchMallMerchandiseSpec();
        }

        /// <summary>
        /// モール商品操作クラス（HARC）取得 [Get class for operate mall product (HARC)]
        /// </summary>
        /// <returns>Instance of OperateMallProductHarc</returns>
        public virtual OperateMallProductHarc GetOperateMallProductHarc()
        {
            return new OperateMallProductHarc();
        }

        /// <summary>
        /// モールフラグチェック処理クラス取得 [Get class for check mall flag]
        /// </summary>
        /// <returns>Instance of CheckMallFlgStgy</returns>
        public virtual CheckMallFlgStgy GetCheckMallFlgStgy()
        {
            return new CheckMallFlgStgy();
        }

        /// <summary>
        /// モール商品操作タイプ取得クラス取得 [Get class for get operation type for mall product]
        /// </summary>
        /// <returns>Instance of ObtainProductOperationTypeStgy</returns>
        public virtual ObtainProductOperationTypeStgy GetObtainProductOperationTypeStgy()
        {
            return new ObtainProductOperationTypeStgy();
        }
    }
}
