﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// Enums for merge status
    /// </summary>
    public enum EnumMergeStatus
    {
        /// <summary>
        /// 0 : マージ準備中 [Ready]
        /// </summary>
        Ready = 0,

        /// <summary>
        /// 1 : 要マージ [Need merge]
        /// </summary>
        NeedMerge = 1,

        /// <summary>
        /// 2 : マージ済み／マージ不要 [Merged / No need merge]
        /// </summary>
        Merged = 2,

        /// <summary>
        /// 3 : 保留 [Pending]
        /// </summary>
        Pending = 3,
    }
}
