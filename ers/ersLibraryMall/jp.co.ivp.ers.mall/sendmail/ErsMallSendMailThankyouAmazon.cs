﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.mall.sendmail
{
    /// <summary>
    /// 注文完了メール（Amazon） [Order completion mail (Amazon)]
    /// </summary>
    public class ErsMallSendMailThankyouAmazon
        : ErsMallSendMailAmazon
    {
        /// <summary>
        /// キー [Key]
        /// </summary>
        protected override string key
        {
            get { return "register"; }
        }

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsMallSendMailThankyouAmazon(int? siteId)
            : base(siteId)
        {
        }
    }
}
