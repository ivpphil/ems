﻿using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.mall.sendmail.strategy;

namespace jp.co.ivp.ers.mall.sendmail
{
    /// <summary>
    /// モールメール送信関連ファクトリ [Factory for mail of mall]
    /// </summary>
    public class ErsMallMailFactory
    {
        /// <summary>
        /// SMTPクラス取得（楽天） [Get the instance of smtp class (Rakuten)]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>SMTPクラスインスタンス [Instance of SMTP class]</returns>
        public virtual ErsSmtp GetErsSmtpRakuten(int? siteId)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var setupMall = ErsMallFactory.ersMallUtilityFactory.getSetup();
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();

            var logFileUserName = setupMall.logFileUserName;
            var logFileUserPassword = setupMall.logFileUserPassword;

            var smtpHostName = mallSetup.GetRakutenMailHost(siteId.Value);
            var smtpPort = mallSetup.GetRakutenMailPort(siteId.Value).Value;
            var smtpAuthId = mallSetup.GetRakutenMailAuthId(siteId.Value);
            var smtpAuthPass = mallSetup.GetRakutenMailAuthPass(siteId.Value);

            var smtpOperationLogPath = setup.smtpOperationLogPath;
            var smtpErrorLogPath = setup.smtpErrorLogPath;

            var retryProperty = ErsFactory.ersMailFactory.GetErsSmtpRetryProperty();
            retryProperty.SetDisableRetry();

            return new ErsSmtp(smtpHostName, smtpPort, logFileUserName, logFileUserPassword, smtpOperationLogPath, smtpErrorLogPath, retryProperty, smtpAuthId, smtpAuthPass);
        }


        #region MallSendMailThankyou
        /// <summary>
        /// Instantiate IErsMallSendMailOrder
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>new IErsMallSendMailOrder</returns>
        public virtual IErsMallSendMailOrder GetErsMallSendMailThankyou(int? siteId)
        {
            var shopKbn = ErsMallFactory.ersSiteFactory.GetSiteData().GetMallShopKbnFromSiteId(siteId.Value);

            switch (shopKbn)
            {
                case EnumMallShopKbn.RAKUTEN:
                    return this.GetErsMallSendMailThankyouRakuten(siteId);

                case EnumMallShopKbn.YAHOO:
                    return this.GetErsMallSendMailThankyouYahoo(siteId);

                case EnumMallShopKbn.AMAZON:
                    return this.GetErsMallSendMailThankyouAmazon(siteId);
            }

            return null;
        }

        /// <summary>
        /// Instantiate ErsMallSendMailThankyouRakuten
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>new ErsMallSendMailThankyouRakuten</returns>
        public virtual ErsMallSendMailThankyouRakuten GetErsMallSendMailThankyouRakuten(int? siteId)
        {
            return new ErsMallSendMailThankyouRakuten(siteId);
        }

        /// <summary>
        /// Instantiate ErsMallSendMailThankyouYahoo
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>new ErsMallSendMailThankyouYahoo</returns>
        public virtual ErsMallSendMailThankyouYahoo GetErsMallSendMailThankyouYahoo(int? siteId)
        {
            return new ErsMallSendMailThankyouYahoo(siteId);
        }

        /// <summary>
        /// Instantiate ErsMallSendMailThankyouAmazon
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>new ErsMallSendMailThankyouAmazon</returns>
        public virtual ErsMallSendMailThankyouAmazon GetErsMallSendMailThankyouAmazon(int? siteId)
        {
            return new ErsMallSendMailThankyouAmazon(siteId);
        }
        #endregion

        #region MallSendMailDelivered
        /// <summary>
        /// Instantiate IErsMallSendMailOrder
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>new IErsMallSendMailOrder</returns>
        public virtual IErsMallSendMailOrder GetErsMallSendMailDelivered(int? siteId)
        {
            var shopKbn = ErsMallFactory.ersSiteFactory.GetSiteData().GetMallShopKbnFromSiteId(siteId.Value);

            switch (shopKbn)
            {
                case EnumMallShopKbn.RAKUTEN:
                    return this.GetErsMallSendMailDeliveredRakuten(siteId);

                case EnumMallShopKbn.YAHOO:
                    return this.GetErsMallSendMailDeliveredYahoo(siteId);

                case EnumMallShopKbn.AMAZON:
                    return this.GetErsMallSendMailDeliveredAmazon(siteId);
            }

            return null;
        }

        /// <summary>
        /// Instantiate ErsMallSendMailDeliveredRakuten
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>new ErsMallSendMailDeliveredRakuten</returns>
        public virtual ErsMallSendMailDeliveredRakuten GetErsMallSendMailDeliveredRakuten(int? siteId)
        {
            return new ErsMallSendMailDeliveredRakuten(siteId);
        }

        /// <summary>
        /// Instantiate ErsMallSendMailDeliveredYahoo
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>new ErsMallSendMailDeliveredYahoo</returns>
        public virtual ErsMallSendMailDeliveredYahoo GetErsMallSendMailDeliveredYahoo(int? siteId)
        {
            return new ErsMallSendMailDeliveredYahoo(siteId);
        }

        /// <summary>
        /// Instantiate ErsMallSendMailDeliveredAmazon
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>new ErsMallSendMailDeliveredAmazon</returns>
        public virtual ErsMallSendMailDeliveredAmazon GetErsMallSendMailDeliveredAmazon(int? siteId)
        {
            return new ErsMallSendMailDeliveredAmazon(siteId);
        }

        /// <summary>
        /// Instantiate SendDeliveredMailStgy
        /// </summary>
        /// <returns>new SendDeliveredMailStgy</returns>
        public virtual SendDeliveredMailStgy GetSendDeliveredMailStgy()
        {
            return new SendDeliveredMailStgy();
        }
        #endregion

        #region MallSendMailIndividual
        /// <summary>
        /// Instantiate IErsMallSendMailOrder
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>new IErsMallSendMailOrder</returns>
        public virtual IErsMallSendMailIndividual GetErsMallSendMailIndividual(int? siteId)
        {
            var shopKbn = ErsMallFactory.ersSiteFactory.GetSiteData().GetMallShopKbnFromSiteId(siteId.Value);

            switch (shopKbn)
            {
                case EnumMallShopKbn.RAKUTEN:
                    return this.GetErsMallSendMailIndividualRakuten(siteId);

                case EnumMallShopKbn.YAHOO:
                    return this.GetErsMallSendMailIndividualYahoo(siteId);

                case EnumMallShopKbn.AMAZON:
                    return this.GetErsMallSendMailIndividualAmazon(siteId);
            }

            return null;
        }

        /// <summary>
        /// Instantiate ErsMallSendMailIndividualRakuten
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>new ErsMallSendMailIndividualRakuten</returns>
        public virtual ErsMallSendMailIndividualRakuten GetErsMallSendMailIndividualRakuten(int? siteId)
        {
            return new ErsMallSendMailIndividualRakuten(siteId);
        }

        /// <summary>
        /// Instantiate ErsMallSendMailIndividualYahoo
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>new ErsMallSendMailIndividualYahoo</returns>
        public virtual ErsMallSendMailIndividualYahoo GetErsMallSendMailIndividualYahoo(int? siteId)
        {
            return new ErsMallSendMailIndividualYahoo(siteId);
        }

        /// <summary>
        /// Instantiate ErsMallSendMailIndividualAmazon
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>new ErsMallSendMailIndividualAmazon</returns>
        public virtual ErsMallSendMailIndividualAmazon GetErsMallSendMailIndividualAmazon(int? siteId)
        {
            return new ErsMallSendMailIndividualAmazon(siteId);
        }

        /// <summary>
        /// Instantiate SendIndividualMailStgy
        /// </summary>
        /// <returns>new SendIndividualMailStgy</returns>
        public virtual SendIndividualMailStgy GetSendIndividualMailStgy()
        {
            return new SendIndividualMailStgy();
        }

        /// <summary>
        /// Instantiate ValidateMallMailFromStgy
        /// </summary>
        /// <returns>new ValidateMallMailFromStgy</returns>
        public virtual ValidateMallMailFromStgy GetValidateMallMailFromStgy()
        {
            return new ValidateMallMailFromStgy();
        }
        #endregion
    }
}
