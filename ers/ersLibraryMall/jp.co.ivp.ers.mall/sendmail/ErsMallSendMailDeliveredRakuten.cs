﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.mall.sendmail
{
    /// <summary>
    /// 配送完了メール（楽天） [Shipping completion mail (Rakuten)]
    /// </summary>
    public class ErsMallSendMailDeliveredRakuten
        : ErsMallSendMailRakuten
    {
        /// <summary>
        /// キー [Key]
        /// </summary>
        protected override string key
        {
            get { return "delivery"; }
        }

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsMallSendMailDeliveredRakuten(int? siteId)
            : base(siteId)
        {
        }
    }
}
