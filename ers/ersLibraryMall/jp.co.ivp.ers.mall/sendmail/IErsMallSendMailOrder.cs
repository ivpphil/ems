﻿using System;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.mall.sendmail
{
    /// <summary>
    /// モール注文メール送信インターフェース [Mall send order mail interface]
    /// </summary>
    public interface IErsMallSendMailOrder
    {
        /// <summary>
        /// メール送信処理 [Process for send mail]
        /// </summary>
        /// <param name="d_no">伝票番号 [Order number]</param>
        /// <param name="email">メールアドレス [E-mail address]</param>
        /// <param name="mformat">送信フォーマット [Send format]</param>
        /// <param name="model">モデル [Model]</param>
        void Send(string d_no, string email, EnumMformat mformat, IErsModelBase model);
    }
}
