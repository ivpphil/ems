﻿using System;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.mall.sendmail
{
    /// <summary>
    /// モール個別メール送信インターフェース [Mall send individual mail interface]
    /// </summary>
    public interface IErsMallSendMailIndividual
    {
        /// <summary>
        /// メール送信処理 [Process for send mail]
        /// </summary>
        /// <param name="model">モデル [Model]</param>
        /// <param name="mail_from">From [From]</param>
        /// <param name="mail_to">To [To]</param>
        /// <param name="sendToAdmin">管理者送信 [Send to administrator]</param>
        /// <param name="mail_title">件名 [Subject]</param>
        /// <param name="mail_body">本文 [Body]</param>
        /// <param name="mformat">メールフォーマット [Mail format]</param>
        void Send(IErsModelBase model, string mail_from, string mail_to, bool sendToAdmin, string mail_title, string mail_body, EnumMformat? mformat);
    }
}
