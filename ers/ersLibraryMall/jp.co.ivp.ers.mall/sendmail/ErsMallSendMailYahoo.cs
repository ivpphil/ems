﻿using System;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.mall.sendmail
{
    /// <summary>
    /// メール送信（Yahoo!） [Send mail (Yahoo!)]
    /// </summary>
    public class ErsMallSendMailYahoo
        : ErsSendMail, IErsMallSendMailOrder
    {
        /// <summary>
        /// キー [Key]
        /// </summary>
        protected override string key
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsMallSendMailYahoo(int? siteId)
            : base(siteId)
        {
        }

        /// <summary>
        /// 初期化 [Initialize]
        /// </summary>
        /// <param name="model">モデル [Model]</param>
        /// <param name="format">送信フォーマット [Send format]</param>
        public override void Init(IErsModelBase model, EnumMformat format)
        {
            base.Init(model, format);

            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();

            this.mail_from = mallSetup.GetYahooMailFrom(this.siteId.Value);
        }

        /// <summary>
        /// メール送信処理 [Process for send mail]
        /// </summary>
        protected override void SendSmtpMail(bool async)
        {
            var setupMall = ErsMallFactory.ersMallUtilityFactory.getSetup();

            if (!setupMall.enableSendMailMall)
            {
                return;
            }

            base.SendSmtpMail(async);
        }

        /// <summary>
        /// 送信 [Send mail]
        /// </summary>
        /// <param name="d_no">伝票番号 [Order number]</param>
        /// <param name="email">メールアドレス [E-mail address]</param>
        /// <param name="mformat">メールフォーマット [Format of mail]</param>
        /// <param name="model">モデル [Model]</param>
        public virtual void Send(string d_no, string email, EnumMformat mformat, IErsModelBase model)
        {
            this.Init(model, mformat);

            if (!string.IsNullOrEmpty(d_no))
            {
                this.d_no_forLog = d_no;
            }

            this.mail_to = email;

            this.SendSynchronous();
        }
    }
}
