﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.mall.sendmail
{
    /// <summary>
    /// 注文完了メール（Yahoo!） [Order completion mail (Yahoo!)]
    /// </summary>
    public class ErsMallSendMailThankyouYahoo
        : ErsMallSendMailYahoo
    {
        /// <summary>
        /// キー [Key]
        /// </summary>
        protected override string key
        {
            get { return "register"; }
        }

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsMallSendMailThankyouYahoo(int? siteId)
            : base(siteId)
        {
        }
    }
}
