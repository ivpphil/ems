﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.mall.sendmail
{
    /// <summary>
    /// 注文完了メール（楽天） [Order completion mail (Rakuten)]
    /// </summary>
    public class ErsMallSendMailThankyouRakuten
        : ErsMallSendMailRakuten
    {
        /// <summary>
        /// キー [Key]
        /// </summary>
        protected override string key
        {
            get { return "register"; }
        }

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsMallSendMailThankyouRakuten(int? siteId)
            : base(siteId)
        {
        }
    }
}
