﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.mall.sendmail.strategy
{
    /// <summary>
    /// モールFROMメールアドレスチェック [Validate for from mail address of mall]
    /// </summary>
    public class ValidateMallMailFromStgy
    {
        /// <summary>
        /// モールFROMメールアドレスチェック [Validate for from mail address of mall]
        /// </summary>
        /// <param name="shopKbn">モール店舗タイプ [Mall shop type]</param>
        /// <param name="from">From [From]</param>
        /// <param name="fieldKey">フィールドキー [Field key]</param>
        /// <returns>結果 [Result]</returns>
        public ValidationResult Validate(int? siteId, string from, string fieldKey)
        {
            if (!from.HasValue())
            {
                return null;
            }

            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var shopKbn = ErsMallFactory.ersSiteFactory.GetSiteData().GetMallShopKbnFromSiteId(siteId.Value);
            var fromMall = string.Empty;

            switch (shopKbn)
            {
                case EnumMallShopKbn.RAKUTEN:
                    fromMall = mallSetup.GetRakutenMailFrom(siteId.Value);
                    break;

                case EnumMallShopKbn.AMAZON:
                    fromMall = mallSetup.GetAmazonMailFrom(siteId.Value);
                    break;

                default:
                    return null;
            }

            if (from != fromMall)
            {
                return new ValidationResult(ErsResources.GetMessage("108000", fromMall), new[] { fieldKey });
            }

            return null;
        }
    }
}
