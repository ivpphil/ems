﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.sendmail.strategy
{
    /// <summary>
    /// 個別メール送信 [Send individual mail]
    /// </summary>
    public class SendIndividualMailStgy
    {
        /// <summary>
        /// 個別メール送信 [Send individual mail]
        /// </summary>
        /// <param name="model">モデル [Model]</param>
        /// <param name="mail_from">From [From]</param>
        /// <param name="mail_to">To [To]</param>
        /// <param name="sendToAdmin">管理者送信 [Send to administrator]</param>
        /// <param name="mail_title">件名 [Subject]</param>
        /// <param name="mail_body">本文 [Body]</param>
        /// <param name="mformat">メールフォーマット [Mail format]</param>
        /// <param name="shopKbn">モール店舗タイプ [Mall shop type]</param>
        public void SendMail(IErsModelBase model, string from, string to, bool sendToAdmin, string title, string body, EnumMformat? mformat, int? siteId)
        {
            var factory = ErsMallFactory.ersMallMailFactory;

            var objSendMail = ErsMallFactory.ersMallMailFactory.GetErsMallSendMailIndividual(siteId);

            objSendMail.Send(model, from, to, sendToAdmin, title, body, mformat);
        }
    }
}
