﻿using System.Collections.Generic;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using System;

namespace jp.co.ivp.ers.mall.sendmail.strategy
{
    /// <summary>
    /// 配送完了メール送信 [Send shipping completion mail]
    /// </summary>
    public class SendDeliveredMailStgy
    {
        /// <summary>
        /// 配送完了メール送信 [Send shipping completion mail]
        /// </summary>
        /// <param name="objOrder">伝票ヘッダ [The order header]</param>
        /// <param name="listOrderRecord">伝票ボディリスト [The list of order body]</param>
        public void SendMail(ErsOrder objOrder, IErsModelBase model)
        {
            var factory = ErsMallFactory.ersMallMailFactory;

            var objSendMail = ErsMallFactory.ersMallMailFactory.GetErsMallSendMailDelivered(objOrder.site_id);

            objSendMail.Send(objOrder.d_no, objOrder.email, EnumMformat.PC, model);
        }
    }
}
