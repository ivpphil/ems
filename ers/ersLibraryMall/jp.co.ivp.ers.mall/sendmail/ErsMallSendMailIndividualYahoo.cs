﻿using System;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.mall.sendmail
{
    /// <summary>
    /// 個別メール（Yahoo!） [Individual mail (Yahoo!)]
    /// </summary>
    public class ErsMallSendMailIndividualYahoo
        : ErsMallSendMailYahoo, IErsMallSendMailIndividual
    {
        /// <summary>
        /// キー [Key]
        /// </summary>
        protected override string key
        {
            get
            {
                // 未使用 [Not use]
                return "individual";
            }
        }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsMallSendMailIndividualYahoo(int? siteId)
            : base(siteId)
        {
        }

        /// <summary>
        /// メール送信 [Send en mail]
        /// </summary>
        /// <param name="model">モデル [Model]</param>
        /// <param name="mail_from">From [From]</param>
        /// <param name="mail_to">To [To]</param>
        /// <param name="sendToAdmin">管理者送信 [Send to administrator]</param>
        /// <param name="mail_title">件名 [Subject]</param>
        /// <param name="mail_body">本文 [Body]</param>
        /// <param name="mformat">メールフォーマット [Mail format]</param>
        public virtual void Send(IErsModelBase model, string mail_from, string mail_to, bool sendToAdmin, string mail_title, string mail_body, EnumMformat? mformat)
        {
            this.mail_from = mail_from;
            this.mail_to = mail_to;
            this.sendToAdmin = sendToAdmin;
            this.Init(model, mformat.Value);
            this.SendSynchronous(mail_title, mail_body);
        }
    }
}
