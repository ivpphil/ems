﻿using System;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.mall.sendmail
{
    /// <summary>
    /// メール送信（楽天） [Send mail (Rakuten)]
    /// </summary>
    public class ErsMallSendMailRakuten
        : ErsSendMail, IErsMallSendMailOrder
    {
        /// <summary>
        /// キー [Key]
        /// </summary>
        protected override string key
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsMallSendMailRakuten(int? siteId)
            : base(siteId)
        {
        }

        /// <summary>
        /// 初期化 [Initialize]
        /// </summary>
        /// <param name="model">モデル [Model]</param>
        /// <param name="format">送信フォーマット [Send format]</param>
        public override void Init(IErsModelBase model, EnumMformat format)
        {
            base.Init(model, format);

            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();

            this.mail_from = mallSetup.GetRakutenMailFrom(this.siteId.Value);
        }

        /// <summary>
        /// メール送信処理 [Process for send mail]
        /// </summary>
        protected override void SendSmtpMail(bool async)
        {
            var setupMall = ErsMallFactory.ersMallUtilityFactory.getSetup();

            if (!setupMall.enableSendMailMall)
            {
                return;
            }

            // VEXの場合はスキップ [If on VEX, skip this process]
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (!setup.onVEX)
            {
                var smtp = ErsMallFactory.ersMallMailFactory.GetErsSmtpRakuten(this.siteId);
                var smtpAdmin = ErsFactory.ersMailFactory.GetErsSmtp();

                if (async)
                {
                    smtp.Send(this.mail_from, this.reply_to, this.mail_to, this.cc_email, null, this.title, this.body, null);

                    // 管理者宛 [To Administrator]
                    if (this.sendToAdmin)
                    {
                        smtpAdmin.Send(this.admin_email, this.reply_to, this.admin_email, null, this.bcc_email, this.title, this.body, null);
                    }
                }
                else
                {
                    smtp.SendSynchronous(this.mail_from, this.reply_to, this.mail_to, this.cc_email, null, this.title, this.body, null);

                    // 管理者宛 [To Administrator]
                    if (this.sendToAdmin)
                    {
                        smtpAdmin.SendSynchronous(this.admin_email, this.reply_to, this.admin_email, null, this.bcc_email, this.title, this.body, null);
                    }
                }
            }
        }

        /// <summary>
        /// メール送信処理 [Process for send mail]
        /// </summary>
        /// <param name="d_no">伝票番号 [Order number]</param>
        /// <param name="email">メールアドレス [E-mail address]</param>
        /// <param name="mformat">送信フォーマット [Send format]</param>
        /// <param name="model">モデル [Model]</param>
        public virtual void Send(string d_no, string email, EnumMformat mformat, IErsModelBase model)
        {
            this.Init(model, mformat);

            if (!string.IsNullOrEmpty(d_no))
            {
                this.d_no_forLog = d_no;
            }

            this.mail_to = email;

            this.SendSynchronous();
        }
    }
}
