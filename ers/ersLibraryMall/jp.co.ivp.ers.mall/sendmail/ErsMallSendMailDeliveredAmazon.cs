﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.sendmail;

namespace jp.co.ivp.ers.mall.sendmail
{
    /// <summary>
    /// 配送完了メール（Amazon） [Shipping completion mail (Amazon)]
    /// </summary>
    public class ErsMallSendMailDeliveredAmazon
        : ErsMallSendMailAmazon
    {
        /// <summary>
        /// キー [Key]
        /// </summary>
        protected override string key
        {
            get { return "delivery"; }
        }

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        public ErsMallSendMailDeliveredAmazon(int? siteId)
            : base(siteId)
        {
        }
    }
}
