﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mall.site.strategy;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.site
{
    public class ErsSiteFactory
    {
        #region サイトデータ [Site data]
        /// <summary>
        /// サイトデータ [Site data]
        /// </summary>
        protected static SiteData _siteData
        {
            get
            {
                return (SiteData)jp.co.ivp.ers.util.ErsCommonContext.GetPooledObject("_siteData");
            }
            set
            {
                jp.co.ivp.ers.util.ErsCommonContext.SetPooledObject("_siteData", value);
            }
        }

        /// <summary>
        /// サイトデータインスタンス取得（参照用） [Get the instance of site data (for Reference)]
        /// </summary>
        /// <returns></returns>
        public virtual SiteData GetSiteData()
        {
            if (_siteData == null)
            {
                _siteData = new SiteData();
            }
            return _siteData;
        }
        #endregion

        #region モール設定 [Mall settings]
        /// <summary>
        /// モール設定実体 [Mall settings object]
        /// </summary>
        protected static MallSetup _mallSetup
        {
            get
            {
                return (MallSetup)jp.co.ivp.ers.util.ErsCommonContext.GetPooledObject("_mallSetup");
            }
            set
            {
                jp.co.ivp.ers.util.ErsCommonContext.SetPooledObject("_mallSetup", value);
            }
        }

        /// <summary>
        /// モール設定インスタンス取得（参照用） [Get the instance of mall settings (for Reference)]
        /// </summary>
        /// <returns></returns>
        public virtual MallSetup GetMallSetup()
        {
            if (_mallSetup == null)
            {
                _mallSetup = new MallSetup();
            }
            return _mallSetup;
        }
        #endregion

        #region ErsSite
        public virtual ErsSiteRepository GetErsSiteRepository()
        {
            return new ErsSiteRepository();
        }

        public virtual ErsSiteCriteria GetErsSiteCriteria()
        {
            return new ErsSiteCriteria();
        }

        public virtual ErsSite GetErsSite()
        {
            return new ErsSite();
        }

        /// <summary>
        /// Retrieves site with parameters
        /// </summary>
        /// <param name="parameters">parameters Dictionary</param>
        /// <returns>ErsSite</returns>
        public virtual ErsSite GetErsSiteWithParameters(Dictionary<string, object> parameters)
        {
            var entity = this.GetErsSite();
            entity.OverwriteWithParameter(parameters);
            return entity;
        }

        /// <summary>
        /// Retrieves step mail scenario by id
        /// </summary>
        /// <param name="id">step mail scenario id</param>
        /// <returns></returns>
        public virtual ErsSite GetErsSiteByID(int? id)
        {
            var repository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var criteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count == 0)
                throw new ErsException("10200");

            return list[0];
        }
        #endregion

        #region ErsMallSetup
        /// <summary>
        /// モール設定リポジトリ取得 [Get mall settings repository]
        /// </summary>
        /// <returns>ErsMallSetupRepository</returns>
        public virtual ErsMallSetupRepository GetErsMallSetupRepository()
        {
            return new ErsMallSetupRepository();
        }

        /// <summary>
        /// モール設定クライテリア取得 [Get mall settings criteria]
        /// </summary>
        /// <returns>ErsMallSetupCriteria</returns>
        public virtual ErsMallSetupCriteria GetErsMallSetupCriteria()
        {
            return new ErsMallSetupCriteria();
        }

        /// <summary>
        /// モール設定エンティティ取得 [Get mall settings]
        /// </summary>
        /// <returns>ErsMallSetup</returns>
        public virtual ErsMallSetup GetErsMallSetup()
        {
            return new ErsMallSetup();
        }

        /// <summary>
        /// モール設定エンティティ取得（パラメータから） [Get mall settings entity (from parameters)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsMallSetup</returns>
        public virtual ErsMallSetup GetErsMallSetupWithParameters(Dictionary<string, object> dicParams)
        {
            var obj = this.GetErsMallSetup();
            obj.OverwriteWithParameter(dicParams);
            return obj;
        }

        /// <summary>
        /// モール設定エンティティ取得（IDから） [Get mall settings entity (from ID)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsMallSetup</returns>
        public virtual ErsMallSetup GetErsMallSetupWithID(int id)
        {
            var repository = this.GetErsMallSetupRepository();

            var criteria = this.GetErsMallSetupCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            return list.Count > 0 ? list[0] : null;
        }
        #endregion

        #region Stragey
        /// <summary>
        /// モール設定リポジトリ取得 [Get mall settings repository]
        /// </summary>
        /// <returns>ObtainRakutenPasswordValidityMessageStgy</returns>
        public ObtainRakutenPasswordValidityMessageStgy GetObtainRakutenPasswordValidityMessageStgy()
        {
            return new ObtainRakutenPasswordValidityMessageStgy();
        }
        #endregion
    }
}
