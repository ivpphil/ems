﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.site.strategy
{
    /// <summary>
    /// 楽天パスワード有効期限メッセージ取得 [Get the message for Rakuten password validity]
    /// </summary>
    public class ObtainRakutenPasswordValidityMessageStgy
    {
        /// <summary>
        /// FTPパスワード有効期限月数 [Expiration months for FTP password]
        /// </summary>
        protected const int FTP_PASSWORD_EXPIRATION_MONTH = 3;

        /// <summary>
        /// WEBAPI認証キー有効期限月数 [Expiration months for WebAPI authorization key]
        /// </summary>
        protected const int WEBAPI_AUTHORIZATION_KEY_EXPIRATION_MONTH = 3;

        /// <summary>
        /// R-Login パスワード有効期限日数 [Expiration days for R-Login password]
        /// </summary>
        protected const int RLOGIN_PASSWORD_EXPIRATION_DAYS = 14;


        /// <summary>
        /// 楽天パスワード有効期限メッセージ取得 [Get the message for Rakuten password validity]
        /// </summary>
        /// <returns>メッセージ [Message]</returns>
        public string Obtain()
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();

            // 楽天サイトリスト取得 [Get the list of Rakuten site]
            var listRakutenSite = this.GetRakutenSiteList();

            if (listRakutenSite.Count == 0)
            {
                return string.Empty;
            }

            var listMessage = new List<string>();

            foreach (var objSite in listRakutenSite)
            {
                var strMessage = string.Empty;

                // 時刻部分を初期化 [Initialize time part]
                var dateNow = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd"));

                // FTPパスワード最終更新日時取得 [Get Last updated datetime of FTP password]
                var strFTPPassUtime = mallSetup.GetRakutenFtpPassUtime(objSite.id.Value);

                // メッセージ取得 [Get the message]
                strMessage = this.GetMessage(strFTPPassUtime, dateNow.AddMonths(-FTP_PASSWORD_EXPIRATION_MONTH), objSite.site_name, "rakuten_ftp_pass");

                if (strMessage.HasValue())
                {
                    listMessage.Add(strMessage);
                }

                // WEBAPI認証キー最終更新日時取得 [Get Last updated datetime of WebAPI authorization key]
                var strWebAPIAuthorizationKeyUtime = mallSetup.GetRakutenWebApiAuthorizationKeyUtime(objSite.id.Value);

                // メッセージ取得 [Get the message]
                strMessage = this.GetMessage(strWebAPIAuthorizationKeyUtime, dateNow.AddMonths(-WEBAPI_AUTHORIZATION_KEY_EXPIRATION_MONTH), objSite.site_name, "rakuten_webapi_auth_key");

                if (strMessage.HasValue())
                {
                    listMessage.Add(strMessage);
                }

                // R-Login パスワード最終更新日時取得 [Get Last updated datetime of R-Login password]
                var strRLoginPassUtime = mallSetup.GetRakutenRLoginPassUtime(objSite.id.Value);

                // メッセージ取得 [Get the message]
                strMessage = this.GetMessage(strRLoginPassUtime, dateNow.AddDays(-RLOGIN_PASSWORD_EXPIRATION_DAYS), objSite.site_name, "rakuten_rlogin_pass");

                if (strMessage.HasValue())
                {
                    listMessage.Add(strMessage);
                }
            }

            return listMessage.Count == 0 ? string.Empty : String.Join(string.Empty, listMessage);
        }

        /// <summary>
        /// 楽天サイトリスト取得 [Get the list of Rakuten site]
        /// </summary>
        /// <returns>楽天サイトリスト [The list of Rakuten site]</returns>
        protected IList<ErsSite> GetRakutenSiteList()
        {
            var repository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var criteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();

            criteria.mall_shop_kbn = EnumMallShopKbn.RAKUTEN;
            criteria.SetActiveOnly();

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            return repository.Find(criteria);
        }

        /// <summary>
        /// メッセージ取得 [Get the message]
        /// </summary>
        /// <param name="strUtime">更新日時文字列 [Updated datetime string]</param>
        /// <param name="dateCompare">比較日時 [Datetime for compare]</param>
        /// <param name="siteName">サイト名 [Site name]</param>
        /// <param name="fieldKey">フィールドキー [Field key]</param>
        /// <returns>メッセージ</returns>
        protected string GetMessage(string strUtime, DateTime dateCompare, string siteName, string fieldKey)
        {
            if (!strUtime.HasValue())
            {
                return string.Empty;
            }

            DateTime dateUtime;

            if (!DateTime.TryParse(strUtime, out dateUtime))
            {
                return string.Empty;
            }

            // 時刻部分を初期化 [Initialize time part]
            dateUtime = Convert.ToDateTime(dateUtime.ToString("yyyy/MM/dd"));

            var span = dateUtime - dateCompare;

            // 期限切れ [Expired]
            if (span.TotalSeconds < 0)
            {
                return ErsResources.GetMessage("110000", siteName, ErsResources.GetFieldName(fieldKey));
            }
            // 期限近い [Expiration is near]
            else if (span.TotalDays < 7)
            {
                return ErsResources.GetMessage("110001", siteName, ErsResources.GetFieldName(fieldKey), Math.Ceiling(span.TotalDays) + 1);
            }

            return string.Empty;
        }
    }
}
