﻿using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.site
{
    /// <summary>
    /// モール設定リポジトリ [Repository for Mall settings]
    /// </summary>
    public class ErsMallSetupRepository
        : ErsRepository<ErsMallSetup>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public ErsMallSetupRepository()
            : base("mall_setup_t")
        {
        }
    }
}
