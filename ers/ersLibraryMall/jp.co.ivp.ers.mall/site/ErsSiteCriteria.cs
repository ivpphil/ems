﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.site
{
    public class ErsSiteCriteria : Criteria
    {
        public virtual int? id
        {
            set
            {
                Add(Criteria.GetCriterion("site_t.id", value, Operation.EQUAL));
            }
        }

        public EnumActive? active
        {
            set
            {
                Add(Criteria.GetCriterion("site_t.active", (int)value, Operation.EQUAL));
            }
        }

        public virtual string site_name
        {
            set
            {
                Add(Criteria.GetCriterion("site_t.site_name", value, Operation.EQUAL));
            }
        }

        public virtual string site_name_like
        {
            set
            {
                Add(Criteria.GetLikeClauseCriterion("site_t.site_name", value, LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH));
            }
        }

        public EnumMallShopKbn? mall_shop_kbn
        {
            set
            {
                Add(Criteria.GetCriterion("site_t.mall_shop_kbn", (int)value, Operation.EQUAL));
            }
        }

        public EnumMallShopKbn?[] mall_shop_kbn_not_in
        {
            set
            {
                Add(Criteria.GetNotInClauseCriterion("site_t.mall_shop_kbn", value));
            }
        }

        public EnumMallShopKbn? mall_shop_kbn_not_equal
        {
            set
            {
                Add(Criteria.GetCriterion("site_t.mall_shop_kbn", value, Operation.NOT_EQUAL));
            }
        }

        public virtual int? site_cate
        {
            set
            {
                Add(Criteria.GetCriterion("site_t.site_cate", value, Operation.EQUAL));
            }
        }


        /// <summary>
        /// set active only
        /// </summary>
        public virtual void SetActiveOnly()
        {
            this.active = EnumActive.Active;
        }

        /// <summary>
        /// Sets order by id
        /// </summary>
        /// <param name="orderBy">order type</param>
        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("site_t.id", orderBy);
        }
    }
}
