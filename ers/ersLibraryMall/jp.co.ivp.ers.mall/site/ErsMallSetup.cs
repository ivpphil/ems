﻿using System;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.site
{
    /// <summary>
    /// モール設定エンティティ [Entity for Mall settings]
    /// </summary>
    public class ErsMallSetup
        : ErsRepositoryEntity
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 登録日時 [Datetime of register]
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新日時 [Datetime of update]
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// アクティブ [Active]
        /// </summary>
        public virtual EnumActive? active { get; set; }

        /// <summary>
        /// サイト設定ID [ID of site settings]
        /// </summary>
        public virtual int? site_id { get; set; }

        /// <summary>
        /// キー [Key]
        /// </summary>
        public virtual string key { get; set; }

        /// <summary>
        /// 値 [Value]
        /// </summary>
        public virtual string value { get; set; }
    }
}
