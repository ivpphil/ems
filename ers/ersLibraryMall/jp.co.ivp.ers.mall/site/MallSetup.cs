﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.site
{
    /// <summary>
    /// モール設定 [Mall settings]
    /// </summary>
    public class MallSetup
    {
        #region モール設定データ [Mall settings data object]
        /// <summary>
        /// モール設定データ実体 [Mall settings data object]
        /// </summary>
        protected static IDictionary<int, IDictionary<string, object>> _mallSetupValues
        {
            get
            {
                return (IDictionary<int, IDictionary<string, object>>)ErsCommonContext.GetPooledObject("_mallSetupValues");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_mallSetupValues", value);
            }
        }

        /// <summary>
        /// モール設定データ（参照用） [Mall settings data object (for Reference)]
        /// </summary>
        protected virtual IDictionary<int, IDictionary<string, object>> mallSetupValues
        {
            get
            {
                if (_mallSetupValues == null)
                {
                    _mallSetupValues = this.GetMallSetupValues();
                }
                return _mallSetupValues;
            }
        }

        /// <summary>
        /// モール設定データ取得 [Get mall settings data]
        /// </summary>
        /// <returns>モール設定データ [Mall settings data]</returns>
        protected virtual IDictionary<int, IDictionary<string, object>> GetMallSetupValues()
        {
            var repository = ErsMallFactory.ersSiteFactory.GetErsMallSetupRepository();

            var listFind = repository.Find(null);

            var dicRet = new Dictionary<int, IDictionary<string, object>>();

            foreach (var data in listFind)
            {
                if (!dicRet.ContainsKey(data.site_id.Value))
                {
                    dicRet[data.site_id.Value] = new Dictionary<string, object>();
                }

                dicRet[data.site_id.Value].Add(data.key, data.value);
            }

            return dicRet;
        }
        #endregion

        #region 共通 [Common]
        /// <summary>
        /// キー取得 [Get key]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="key">キー [Key]</param>
        /// <returns>キー [Key]</returns>
        protected virtual string GetKey(int siteId, string key)
        {
            var shopKbn = ErsMallFactory.ersSiteFactory.GetSiteData().GetMallShopKbnFromSiteId(siteId);
            return string.Format("{0}_{1}", shopKbn.ToString().ToLower(), key);
        }

        /// <summary>
        /// ショップID取得 [Get shop id]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>ショップID [Shop id]</returns>
        public virtual int? GetShopId(int siteId)
        {
            var key = this.GetKey(siteId, "shop_id");
            if (this.mallSetupValues.ContainsKey(siteId))
            {

                if (this.mallSetupValues[siteId].ContainsKey(key))
                {
                    return Convert.ToInt32(this.mallSetupValues[siteId][key]);
                }
            }

            return null;
        }

        /// <summary>
        /// ショップ名取得 [Get shop name]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>ショップ名 [Shop name]</returns>
        public virtual string GetShopName(int siteId)
        {
            var key = this.GetKey(siteId, "shop_name");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }
        #endregion

        #region HARC [HARC]
        /// <summary>
        /// サイトID取得（ショップIDから） [Get site id (from Shop id)]
        /// </summary>
        /// <param name="shopId">ショップID [Shop id]</param>
        /// <returns>サイトID [Site id]</returns>
        public virtual int? GetSiteIdFromShopId(int? shopId)
        {
            foreach (var key in this.mallSetupValues.Keys)
            {
                var shopIdTmp = this.GetShopId(key);

                if (shopIdTmp.HasValue && shopIdTmp == shopId)
                {
                    return key;
                }
            }

            return null;
        }

        /// <summary>
        /// モール店舗タイプ取得（ショップIDから） [Get mall shop type (from Shop id)]
        /// </summary>
        /// <param name="shopId">ショップID [Shop id]</param>
        /// <returns>モール店舗タイプ [Mall shop tyep]</returns>
        public virtual EnumMallShopKbn? GetMallShopKbnFromShopId(int? shopId)
        {
            var siteId = this.GetSiteIdFromShopId(shopId);

            if (siteId.HasValue)
            {
                return ErsMallFactory.ersSiteFactory.GetSiteData().GetMallShopKbnFromSiteId(siteId.Value);
            }

            return null;
        }
        #endregion

        #region 楽天 [Rakuten]
        /// <summary>
        /// 店舗URL取得 [Get Mall shop url]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>店舗URL [Mall shop url]</returns>
        public virtual string GetRakutenMallShopUrl(int siteId)
        {
            var key = this.GetKey(siteId, "mall_shop_url");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// FTPホスト取得 [Get FTP host]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>FTPホスト [FTP host]</returns>
        public virtual string GetRakutenFtpHost(int siteId)
        {
            var key = this.GetKey(siteId, "ftp_host");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// FTPユーザ取得 [Get FTP user]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>FTPユーザ [FTP user]</returns>
        public virtual string GetRakutenFtpUser(int siteId)
        {
            var key = this.GetKey(siteId, "ftp_user");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// FTPパスワード取得 [Get FTP password]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>FTPパスワード [FTP password]</returns>
        public virtual string GetRakutenFtpPass(int siteId)
        {
            var key = this.GetKey(siteId, "ftp_pass");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// メールFROMアドレス取得 [Get Mail from address]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>メールFROM [Mail from address]</returns>
        public virtual string GetRakutenMailFrom(int siteId)
        {
            var key = this.GetKey(siteId, "mail_from");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// メールホスト取得 [Get Mail host]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>メールホスト [Mail host]</returns>
        public virtual string GetRakutenMailHost(int siteId)
        {
            var key = this.GetKey(siteId, "mail_host");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// メールポート取得 [Get Mail port]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>メールポート [Mail port]</returns>
        public virtual int? GetRakutenMailPort(int siteId)
        {
            var key = this.GetKey(siteId, "mail_port");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToInt32(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// メール認証ID取得 [Get Mail authentication id]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>メール認証ID [Mail authentication id]</returns>
        public virtual string GetRakutenMailAuthId(int siteId)
        {
            var key = this.GetKey(siteId, "mail_auth_id");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// メール認証パスワード取得 [Get Mail authentication password]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>メール認証パスワード [Mail authentication password]</returns>
        public virtual string GetRakutenMailAuthPass(int siteId)
        {
            var key = this.GetKey(siteId, "mail_auth_pass");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// FTPパスワード最終更新日時取得 [Get Last updated datetime of FTP password]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>FTPパスワード最終更新日時 [Last updated datetime of FTP password]</returns>
        public virtual string GetRakutenFtpPassUtime(int siteId)
        {
            var key = this.GetKey(siteId, "ftp_pass_utime");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// WEBAPI認証キー最終更新日時取得 [Get Last updated datetime of WebAPI authorization key]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>WEBAPI認証キー最終更新日時 [Last updated datetime of WebAPI authorization key]</returns>
        public virtual string GetRakutenWebApiAuthorizationKeyUtime(int siteId)
        {
            var key = this.GetKey(siteId, "webapi_auth_key_utime");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// R-Login パスワード最終更新日時取得 [Get Last updated datetime of R-Login password]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>R-Login パスワード最終更新日時 [Last updated datetime of R-Login password]</returns>
        public virtual string GetRakutenRLoginPassUtime(int siteId)
        {
            var key = this.GetKey(siteId, "rlogin_pass_utime");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }
        #endregion

        #region Yahoo! [Yahoo!]
        /// <summary>
        /// FTPホスト取得 [Get FTP host]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>FTPホスト [FTP host]</returns>
        public virtual string GetYahooFtpHost(int siteId)
        {
            var key = this.GetKey(siteId, "ftp_host");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// FTPユーザ取得 [Get FTP user]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>FTPユーザ [FTP user]</returns>
        public virtual string GetYahooFtpUser(int siteId)
        {
            var key = this.GetKey(siteId, "ftp_user");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// FTPパスワード取得 [Get FTP password]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>FTPパスワード [FTP password]</returns>
        public virtual string GetYahooFtpPass(int siteId)
        {
            var key = this.GetKey(siteId, "ftp_pass");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// メールFROMアドレス取得 [Get Mail from address]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>メールFROM [Mail from address]</returns>
        public virtual string GetYahooMailFrom(int siteId)
        {
            var key = this.GetKey(siteId, "mail_from");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }
        #endregion

        #region Amazon [Amazon]
        /// <summary>
        /// 出品者ID取得 [Get merchant id]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>出品者ID [Merchant id]</returns>
        public virtual string GetAmazonMerchantId(int siteId)
        {
            var key = this.GetKey(siteId, "merchant_id");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// マーケットプレイスID取得 [Get marketplace id]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>マーケットプレイスID取得 [Marketplace id]</returns>
        public virtual string GetAmazonMarketplaceId(int siteId)
        {
            var key = this.GetKey(siteId, "marketplace_id");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// アクセスキーID取得 [Get accesskey id]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>アクセスキーID [Accesskey id]</returns>
        public virtual string GetAmazonAccesskeyId(int siteId)
        {
            var key = this.GetKey(siteId, "accesskey_id");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// シークレットアクセスキー取得 [Get secret accesskey]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>シークレットアクセスキー [Secret accesskey]</returns>
        public virtual string GetAmazonSecretAccesskey(int siteId)
        {
            var key = this.GetKey(siteId, "secret_accesskey");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// メールFROMアドレス取得 [Get Mail from address]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>メールFROM [Mail from address]</returns>
        public virtual string GetAmazonMailFrom(int siteId)
        {
            var key = this.GetKey(siteId, "mail_from");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }
        #endregion

        #region ERS [ERS]
        /// <summary>
        /// アカウントユーザ取得 [Get account user]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>アカウントユーザ [Account user]</returns>
        public virtual string GetErsAccountUser()
        {
            int siteId = ErsMallFactory.ersSiteFactory.GetSiteData().GetErsSiteId();

            var key = this.GetKey(siteId, "account_user");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }

        /// <summary>
        /// アカウントパスワード取得 [Get account password]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <returns>アカウントパスワード取得 [Account password]</returns>
        public virtual string GetErsAccountPass()
        {
            int siteId = ErsMallFactory.ersSiteFactory.GetSiteData().GetErsSiteId();

            var key = this.GetKey(siteId, "account_pass");

            if (this.mallSetupValues[siteId].ContainsKey(key))
            {
                return Convert.ToString(this.mallSetupValues[siteId][key]);
            }

            return null;
        }
        #endregion
    }
}
