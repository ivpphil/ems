﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.site
{
    public class ErsSite : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public virtual string site_name { get; set; }

        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        public virtual int? site_cate { get; set; }

        public virtual EnumActive? active { get; set; }

        public virtual DateTime? intime { get; set; }

        public virtual DateTime? utime { get; set; }

        public virtual EnumOnOff? harc_flg { get; set; }
    }
}
