﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.site
{
    public class ErsSiteRepository: ErsRepository<ErsSite>
    {
        public ErsSiteRepository()
            : base("site_t")
        {
        }

        public ErsSiteRepository(ErsDatabase objDB)
            : base("site_t", objDB)
        {
        }

        public override long GetRecordCount(Criteria criteria)
        {
            return ersDB_table.gSelectCount("id", criteria);
        }

        public override IList<ErsSite> Find(Criteria criteria)
        {
            return this.Find(criteria, new[] { "*" });
        }

        public override IList<ErsSite> Find(Criteria criteria, IEnumerable<string> columns)
        {
            var objList = this.ersDB_table.gSelect(columns, criteria);

            return CreateList(objList);
        }
    }
}
