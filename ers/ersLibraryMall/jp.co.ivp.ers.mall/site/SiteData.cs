﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.site
{
    /// <summary>
    /// サイトデータ [Site data]
    /// </summary>
    public class SiteData
    {
        #region サイトデータ [Site data]
        /// <summary>
        /// サイトデータ実体 [Site data object]
        /// </summary>
        protected static IDictionary<int, ErsSite> _dicSiteData
        {
            get
            {
                return (IDictionary<int, ErsSite>)ErsCommonContext.GetPooledObject("_dicSiteData");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_dicSiteData", value);
            }
        }

        /// <summary>
        /// サイトデータ（参照用） [Site data object (for Reference)]
        /// </summary>
        public virtual IDictionary<int, ErsSite> dicSiteData
        {
            get
            {
                if (_dicSiteData == null)
                {
                    _dicSiteData = this.GetSiteData();
                }
                return _dicSiteData;
            }
        }

        /// <summary>
        /// サイトデータ取得 [Get site data]
        /// </summary>
        /// <returns>サイトデータ [Site data]</returns>
        protected virtual IDictionary<int, ErsSite> GetSiteData()
        {
            var repository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
            var criteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();

            criteria.SetActiveOnly();
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            var listFind = repository.Find(criteria);

            var dicRet = new Dictionary<int, ErsSite>();

            foreach (var data in listFind)
            {
                dicRet[data.id.Value] = data;
            }

            return dicRet;
        }
        #endregion

        /// <summary>
        /// モール店舗タイプ取得（サイトIDから） [Get mall shop type (from Site id)]
        /// </summary>
        /// <param name="site_id">サイトID [Site id]</param>
        /// <returns>モール店舗タイプ [Mall shop type]</returns>
        public virtual EnumMallShopKbn GetMallShopKbnFromSiteId(int site_id)
        {
            if (!this.dicSiteData.ContainsKey(site_id))
            {
                throw new Exception("Bad site id.");
            }

            return this.dicSiteData[site_id].mall_shop_kbn.Value;
        }

        /// <summary>
        /// ERSサイトID取得 [Get ERS site id]
        /// </summary>
        /// <param name="site_id">サイトID [Site id]</param>
        /// <returns>ERSサイトID [ERS site id]</returns>
        public virtual int GetErsSiteId()
        {
            foreach (var objSiteData in this.dicSiteData.Values)
            {
                if (objSiteData.mall_shop_kbn == EnumMallShopKbn.ERS &&
                    objSiteData.harc_flg == EnumOnOff.On)
                {
                    return objSiteData.id.Value;
                }
            }

            throw new Exception("ERS site data was not found.");
        }
    }
}
