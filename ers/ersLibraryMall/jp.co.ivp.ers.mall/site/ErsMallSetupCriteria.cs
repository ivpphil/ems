﻿using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.site
{
    /// <summary>
    /// モール設定クライテリア [Criteria for Mall settings]
    /// </summary>
    public class ErsMallSetupCriteria
        : Criteria
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? id
        {
            set
            {
                Add(Criteria.GetCriterion("mall_setup_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイト設定ID [ID of site settings]
        /// </summary>
        public int? site_id
        {
            set
            {
                Add(Criteria.GetCriterion("mall_setup_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// キー [Key]
        /// </summary>
        public string key
        {
            set
            {
                Add(Criteria.GetCriterion("mall_setup_t.key", value, Operation.EQUAL));
            }
        }
    }
}
