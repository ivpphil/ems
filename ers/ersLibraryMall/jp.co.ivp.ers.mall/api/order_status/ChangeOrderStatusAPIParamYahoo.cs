﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.order_status
{
    /// <summary>
    /// 支店受注データ ステータス変更APIパラメタ [Change order status API param base]
    /// </summary> 
    public class ChangeOrderStatusAPIParamYahoo
        : ChangeOrderStatusAPIParamBase
    {
        /// <summary>
        /// Yahoo!管理ツールタイプ [Type of management tool (Yahoo!)]
        /// </summary>
        public virtual EnumMallYahooManagementToolType? managementTool { get; set; }


        /// <summary>
        /// 複数受注 HARC API パラメタ取得 [Get HARC API param orders]
        /// </summary>
        /// <param name="api_name">string</param>
        /// <param name="listParam">IList&lt;ChangeOrderStatusParam&gt;</param>
        /// <param name="update_status">EnumMoleOrderStatus?</param>
        /// <returns>HarcApiParam</returns>
        public override HarcApiParam GetHarcApiParamOrders(string api_name, IList<UpdateOrderStatusParam> listParam, EnumMallOrderStatus? update_status)
        {
            var apiParam = base.GetHarcApiParamOrders(api_name, listParam, update_status);

            if (this.managementTool != null)
            {
                apiParam.Param.Add("managementTool", this.managementTool.ToString());
            }

            return apiParam;
        }

        /// <summary>
        /// HARC API パラメタ取得 [Get HARC API param]
        /// </summary>
        /// <param name="api_name">string</param>
        /// <param name="order">ErsBranchOrder</param>
        /// <param name="update_status">EnumMoleOrderStatus?</param>
        /// <returns>HarcApiParam</returns>
        public override HarcApiParam GetHarcApiParam(string api_name, UpdateOrderStatusParam param, EnumMallOrderStatus? update_status)
        {
            var apiParam = base.GetHarcApiParam(api_name, param, update_status);

            if (this.managementTool != null)
            {
                apiParam.Param.Add("managementTool", this.managementTool.ToString());
            }

            return apiParam;
        }
    }
}
