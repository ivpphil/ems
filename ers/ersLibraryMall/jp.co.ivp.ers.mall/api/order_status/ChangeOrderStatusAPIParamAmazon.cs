﻿using System;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.order_status
{
    /// <summary>
    /// モール伝票データ ステータス変更APIパラメタ [Change order status API param base]
    /// </summary> 
    public class ChangeOrderStatusAPIParamAmazon
        : ChangeOrderStatusAPIParamBase
    {
        /// <summary>
        /// HARC API パラメタ取得 [Get HARC API param]
        /// </summary>
        /// <param name="api_name">string</param>
        /// <param name="order">UpdateOrderStatusParam</param>
        /// <param name="update_status">EnumMoleOrderStatus?</param>
        /// <returns>HarcApiParam</returns>
        public override HarcApiParam GetHarcApiParam(string api_name, UpdateOrderStatusParam order, EnumMallOrderStatus? update_status)
        {
            var apiParam = new HarcApiParam(api_name);
            var paramDic = new HarcDictionary();

            paramDic.Add("shopId", this.shop_id);
            paramDic.Add("status", (int)update_status);

            //targetOrdersパラメタ生成
            var arrayDic = new HarcDictionary();

            arrayDic.Add("orderCode", order.orderCode);
            arrayDic.Add("orderDate", order.orderDate.Value.ToString("yyyy/MM/dd"));

            if ((EnumMallOrderStatusAmazon)update_status == EnumMallOrderStatusAmazon.Shipped)
            {
                arrayDic.Add("carrier", order.carrier);
                arrayDic.Add("deliveryDate", TimeZoneInfo.ConvertTimeToUtc(order.deliveryDate.Value).ToString("yyyy-MM-ddTHH:mm:ssZ"));
                arrayDic.Add("deliveryMethod", order.deliveryMethod);
                arrayDic.Add("invoiceNumber", order.invoiceNumber);

                if (order.paymentMethod.HasValue())
                {
                    arrayDic.Add("paymentMethod", order.paymentMethod);
                }

                if (order.listItems != null)
                {
                    foreach (var item in order.listItems)
                    {
                        var arrayDicItem = new HarcDictionary();

                        arrayDicItem.Add("itemId", item.itemId);
                        arrayDicItem.Add("quantity", item.quantity);

                        arrayDic.AddArray("items", arrayDicItem);
                    }
                }
            }

            paramDic.AddArray("targetOrders", arrayDic);

            //ParamをparamDicへ置き換え
            apiParam.Param = paramDic;

            return apiParam;
        }
    }
}
