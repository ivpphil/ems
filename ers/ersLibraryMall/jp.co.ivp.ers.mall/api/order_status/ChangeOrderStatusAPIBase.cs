﻿using System.Collections.Generic;
using com.hunglead.harc;
using Jayrock.Json;
using jp.co.ivp.ers.mall.mall_order;

namespace jp.co.ivp.ers.mall.api.order_status
{
    /// <summary>
    /// モール伝票データ ステータス変更API [Change order status API base]
    /// </summary>
    public class ChangeOrderStatusAPIBase
    {
        /// <summary>
        /// API処理分割数 [API processing division count]
        /// </summary>
        public static int API_PROCESSING_DEVISION_COUNT = 500;

        /// <summary>
        /// APIエラーリトライカウント [API retry count for error]
        /// </summary>
        public static int API_ERROR_RETRY_COUNT = 5;

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listApiErrorMessageBase = new List<string>()
        {
            "Login Failed.",                // ログイン失敗 [Login failed]
            "Analyze Failed.",              // 遷移失敗 [Analyze failed]
            "SQLSTATE[42P07]"               // テーブル重複（※暫定対応） [Duplicate table]
        };

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listApiErrorMessage { get; set; }

        /// <summary>
        /// 正常結果メッセージリスト [List of normal message]
        /// </summary>
        protected IList<string> listApiNormalResultMessageBase = new List<string>()
        {
            "Success.",             // 成功 [Success]
            "Needless To Move."     // 変更の必要なし [No need to change]
        };

        /// <summary>
        /// 正常結果メッセージリスト [List of normal message]
        /// </summary>
        protected IList<string> listApiNormalResultMessage { get; set; }

        /// <summary>
        /// リトライ可能結果メッセージリスト [List of retry able messeage]
        /// </summary>
        protected IList<string> listRetryAbleResultMessageBase = new List<string>();

        /// <summary>
        /// リトライ可能結果メッセージリスト [List of retry able messeage]
        /// </summary>
        protected IList<string> listRetryAbleResultMessage { get; set; }


        /// <summary>
        /// API名 [API name]
        /// </summary>
        public virtual string api_name { get; set; }

        /// <summary>
        /// APIパラメタ [API param]
        /// </summary>
        protected virtual ChangeOrderStatusAPIParamBase param { get; set; }

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">ChangeOrderStatusAPIParamBase</param>
        public ChangeOrderStatusAPIBase(ChangeOrderStatusAPIParamBase param)
        {
            this.param = param;

            this.listApiErrorMessage = listApiErrorMessageBase;
            this.listApiNormalResultMessage = listApiNormalResultMessageBase;
            this.listRetryAbleResultMessage = listRetryAbleResultMessageBase;
        }

        /// <summary>
        /// モール伝票ステータス更新 [Update mall order status]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="listOrder">伝票情報リスト [The list of order information]</param>
        /// <param name="updateStatus">更新ステータス [Update status]</param>
        /// <returns>更新結果 [Result of update]</returns>
        public virtual Dictionary<string, object> ChangeOrderStatus(HarcApiRequest request, IList<UpdateOrderStatusParam> listOrder, EnumMallOrderStatus? updateStatus)
        {
            //パラメタ取得
            var apiParam = this.param.GetHarcApiParamOrders(this.api_name, listOrder, updateStatus);

            //strategy取得
            var convertStgy = ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy();

            var jsonObj = this.Request(request, apiParam);

            if (jsonObj != null)
            {
                return convertStgy.ConvertToDicListFromUpdateResultJson(jsonObj);
            }

            return null;
        }

        /// <summary>
        /// モール伝票ステータス更新 [Update mall order status]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="order">伝票情報 [Order information]</param>
        /// <param name="updateStatus">更新ステータス [Update status]</param>
        public virtual Dictionary<string, object> ChangeOrderStatus(HarcApiRequest request, UpdateOrderStatusParam order, EnumMallOrderStatus? updateStatus)
        {
            //パラメタ取得
            var apiParam = this.param.GetHarcApiParam(this.api_name, order, updateStatus);

            //strategy取得
            var convertStgy = ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy();

            var jsonObj = this.Request(request, apiParam);

            if (jsonObj != null)
            {
                return convertStgy.ConvertToDicListFromUpdateResultJson(jsonObj);
            }

            return null;
        }

        /// <summary>
        /// APIリクエスト [API request]
        /// </summary>
        /// <param name="request">リクエストオブジェクト [Request object]</param>
        /// <param name="apiParam">APIパラメータ [API parameter]</param>
        protected virtual JsonObject Request(HarcApiRequest request, HarcApiParam apiParam)
        {
            int retry = 0;

            // APIリクエスト API request
            while (true)
            {
                try
                {
                    var jsonObj = request.simpleRequest(apiParam);
                    return jsonObj;
                }
                catch (HarcApiException e)
                {
                    // リトライ可能な場合は5回までリトライさせる
                    if (this.IsRetryAbleError(e.Message))
                    {
                        if (retry++ < API_ERROR_RETRY_COUNT)
                        {
                            continue;
                        }
                    }

                    throw new APIFailedException(e, apiParam);
                }
            }
        }

        /// <summary>
        /// リトライ可能なエラーかどうか [Whether error can retry]
        /// </summary>
        /// <param name="errorMessage">エラーメッセージ [Error message]</param>
        /// <returns>true : リトライ可能 [Can retry] / false : リトライ不可 [Can't retry]</returns>
        protected virtual bool IsRetryAbleError(string errorMessage)
        {
            foreach (var message in this.listApiErrorMessage)
            {
                if (errorMessage.Contains(message))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 正常な結果かどうか [Whether the results normal]
        /// </summary>
        /// <param name="resutlMessage">結果メッセージ [Error message]</param>
        /// <returns>true : 正常 [Normal] / false : エラー [Error]</returns>
        public bool IsNormalResult(string resutlMessage)
        {
            foreach (var message in this.listApiNormalResultMessage)
            {
                if (resutlMessage.Contains(message))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// リトライ可能な結果エラーかどうか [Whether result error can retry]
        /// </summary>
        /// <param name="resutlMessage">結果メッセージ [Error message]</param>
        /// <returns>true : リトライ可能 [Can retry] / false : リトライ不可 [Can't retry]</returns>
        public bool IsRetryAbleResultError(string resutlMessage)
        {
            foreach (var message in this.listRetryAbleResultMessage)
            {
                if (resutlMessage.Contains(message))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
