﻿
namespace jp.co.ivp.ers.mall.api.order_status
{
    /// <summary>
    /// Amazonモール伝票データ ステータス変更API [Change order status API Amazon]
    /// </summary>
    public class ChangeOrderStatusAPIAmazon : ChangeOrderStatusAPIBase
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public override string api_name { get { return "changeAmazonStatus"; } }

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">ChangeOrderStatusAPIParamBase</param>
        public ChangeOrderStatusAPIAmazon(ChangeOrderStatusAPIParamBase param)
            : base(param)
        {
        }
    }
}
