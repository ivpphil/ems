﻿using System.Collections.Generic;
using System.Linq;

namespace jp.co.ivp.ers.mall.api.order_status
{
    /// <summary>
    /// Yahooモール伝票データ ステータス変更API [Change order status API Yahoo]
    /// </summary>
    public class ChangeOrderStatusAPIYahoo : ChangeOrderStatusAPIBase
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public override string api_name
        {
            get
            {
                return "changeYahoosStatus2";
            }
        }

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listApiErrorMessageYahoo = new List<string>()
        {
            "Search upload url failed.",    // アップロード失敗 [Upload failed]
            "Get Body failed",              // 解析失敗 [Analyze failed]
            "Invalid Page."                 // 遷移エラー [Transition error]
        };

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">ChangeOrderStatusAPIParamBase</param>
        public ChangeOrderStatusAPIYahoo(ChangeOrderStatusAPIParamBase param)
            : base(param)
        {
            this.listApiErrorMessage = listApiErrorMessageBase.Concat(listApiErrorMessageYahoo).ToList();
        }
    }
}
