﻿using System.Collections.Generic;
using System.Linq;

namespace jp.co.ivp.ers.mall.api.order_status
{
    /// <summary>
    /// 楽天市場モール伝票データ ステータス変更API [Change order status API Rakuten]
    /// </summary>
    public class ChangeOrderStatusAPIRakuten : ChangeOrderStatusAPIBase
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public override string api_name
        {
            get
            {
                return "changeRakutenStatus";
            }
        }

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listApiErrorMessageRakuten = new List<string>()
        {
            "Search order list failed",     // 検索失敗 [Search failed]
            "Authentication Error."         // 認証失敗 [Authentication failed]
        };

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listRetryAbleResultMessageRakuten = new List<string>()
        {
            "Not Found.",                           // 受注データなし [Does not found order]
            "Can't Move.",                          // ステータス取得失敗 [Failed to get status]
            "Move Failed.",                         // ステータス変更失敗 [Failed to change status]
            "Success Verify Failed.",               // 再検証失敗 [Verify failed]
            "Process retry count is over limit."    // リトライ失敗 [Retry failed]
        };

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">ChangeOrderStatusAPIParamBase</param>
        public ChangeOrderStatusAPIRakuten(ChangeOrderStatusAPIParamBase param)
            : base(param)
        {
            this.listApiErrorMessage = listApiErrorMessageBase.Concat(listApiErrorMessageRakuten).ToList();
            this.listRetryAbleResultMessage = listRetryAbleResultMessageBase.Concat(listRetryAbleResultMessageRakuten).ToList();
        }
    }
}
