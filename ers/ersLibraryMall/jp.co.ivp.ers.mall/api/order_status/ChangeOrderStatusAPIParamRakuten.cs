﻿using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.order_status
{
    /// <summary>
    /// モール伝票データ ステータス変更APIパラメタ [Change order status API param base]
    /// </summary> 
    public class ChangeOrderStatusAPIParamRakuten
        : ChangeOrderStatusAPIParamBase
    {
        /// <summary>
        /// 再検証フラグ [Falg of verify]
        /// </summary>
        public virtual bool successVerify { get; set; }


        /// <summary>
        /// HARC API パラメタ取得 [Get HARC API param]
        /// </summary>
        /// <param name="api_name">string</param>
        /// <param name="order">UpdateOrderStatusParam</param>
        /// <param name="update_status">EnumMoleOrderStatus?</param>
        /// <returns>HarcApiParam</returns>
        public override HarcApiParam GetHarcApiParam(string api_name, UpdateOrderStatusParam order, EnumMallOrderStatus? update_status)
        {
            var apiParam = new HarcApiParam(api_name);
            var paramDic = new HarcDictionary();

            paramDic.Add("shopId", this.shop_id);
            paramDic.Add("status", (int)update_status);

            //targetOrdersパラメタ生成
            var arrayDic = new HarcDictionary();

            arrayDic.Add("orderCode", order.orderCode);
            arrayDic.Add("orderDate", order.orderDate.Value.ToString("yyyy/MM/dd"));

            if (update_status == EnumMallOrderStatus.Done)
            {
                var arrayDicItem = new HarcDictionary();

                arrayDicItem.Add("deliveryName", order.deliveryName);
                arrayDicItem.Add("deliveryAddress", order.deliveryAddress);
                arrayDicItem.Add("invoiceNo", order.invoiceNumber);

                arrayDic.AddArray("detail", arrayDicItem);
            }

            paramDic.AddArray("targetOrders", arrayDic);

            //ParamをparamDicへ置き換え
            apiParam.Param = paramDic;

            return apiParam;
        }
    }
}
