﻿using System;
using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.order_status
{
    /// <summary>
    /// モール伝票データ ステータス変更APIパラメタ [Change order status API param base]
    /// </summary> 
    public class ChangeOrderStatusAPIParamBase
    {
        /// <summary>
        /// ショップID [Shop ID]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// 店舗名 [Shop name]
        /// </summary>
        public virtual string shop_name { get; set; }

        /// <summary>
        /// 複数受注 HARC API パラメタ取得 [Get HARC API param orders]
        /// </summary>
        /// <param name="api_name">string</param>
        /// <param name="orderList">IList&lt;UpdateOrderStatusParam&gt;</param>
        /// <param name="update_status">EnumMoleOrderStatus?</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParamOrders(string api_name, IList<UpdateOrderStatusParam> orderList, EnumMallOrderStatus? update_status)
        {
            var apiParam = new HarcApiParam(api_name);
            var paramDic = new HarcDictionary();

            paramDic.Add("shopId", this.shop_id);
            paramDic.Add("status", (int)update_status);

            //targetOrdersパラメタ生成
            foreach (var order in orderList)
            {
                var arrayDic = new HarcDictionary();

                arrayDic.Add("orderCode", order.orderCode);
                arrayDic.Add("orderDate", order.orderDate.Value.ToString("yyyy/MM/dd"));

                paramDic.AddArray("targetOrders", arrayDic);
            }

            //ParamをparamDicへ置き換え
            apiParam.Param = paramDic;

            return apiParam;
        }

        /// <summary>
        /// HARC API パラメタ取得 [Get HARC API param]
        /// </summary>
        /// <param name="api_name">string</param>
        /// <param name="order">UpdateOrderStatusParam</param>
        /// <param name="update_status">EnumMoleOrderStatus?</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParam(string api_name, UpdateOrderStatusParam order, EnumMallOrderStatus? update_status)
        {
            var apiParam = new HarcApiParam(api_name);
            var paramDic = new HarcDictionary();

            paramDic.Add("shopId", this.shop_id);
            paramDic.Add("status", (int)update_status);

            //targetOrdersパラメタ生成
            var arrayDic = new HarcDictionary();

            arrayDic.Add("orderCode", order.orderCode);
            arrayDic.Add("orderDate", order.orderDate.Value.ToString("yyyy/MM/dd"));

            paramDic.AddArray("targetOrders", arrayDic);

            //ParamをparamDicへ置き換え
            apiParam.Param = paramDic;

            return apiParam;
        }
    }

    /// <summary>
    /// 受注ステータス更新API用受注情報 [Order information for Update order status API]
    /// </summary>
    public struct UpdateOrderStatusParam
    {
        /// <summary>
        /// 受注番号 [Order code]
        /// </summary>
        public string orderCode { get; set; }

        /// <summary>
        /// 受注日時 [Order date]
        /// </summary>
        public DateTime? orderDate { get; set; }

        /// <summary>
        /// 配送先氏名 [Delivery name]
        /// </summary>
        public string deliveryName { get; set; }

        /// <summary>
        /// 配送先住所 [Delivery address]
        /// </summary>
        public string deliveryAddress { get; set; }

        /// <summary>
        /// 配送業者 [Delivery company]
        /// </summary>
        public string carrier { get; set; }

        /// <summary>
        /// 出荷日 [Delivery date]
        /// </summary>
        public DateTime? deliveryDate { get; set; }

        /// <summary>
        /// 配送方法 [Delivery method]
        /// </summary>
        public string deliveryMethod { get; set; }

        /// <summary>
        /// 送り状番号 [Tracking number]
        /// </summary>
        public string invoiceNumber { get; set; }

        /// <summary>
        /// 支払い方法 [Payment method]
        /// </summary>
        public string paymentMethod { get; set; }

        /// <summary>
        /// 明細リスト [The list of details]
        /// </summary>
        public IList<UpdateOrderStatusItemParam> listItems { get; set; }
    }

    /// <summary>
    /// 受注ステータス更新API用受注明細情報 [Order detail information for Update order status API]
    /// </summary>
    public struct UpdateOrderStatusItemParam
    {
        /// <summary>
        /// 商品ID [Item id]
        /// </summary>
        public string itemId { get; set; }

        /// <summary>
        /// キャンセル理由 [Cancel reason]
        /// </summary>
        public string cancelReason { get; set; }

        /// <summary>
        /// 数量 [Quantity]
        /// </summary>
        public int? quantity { get; set; }
    }
}
