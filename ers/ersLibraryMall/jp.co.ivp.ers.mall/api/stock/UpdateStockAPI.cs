﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.stock
{
    /// <summary>
    /// 在庫数更新API [API for update stock]
    /// </summary>
    public class UpdateStockAPI
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public virtual string api_name
        {
            get
            {
                return "updateStock";
            }
        }

        /// <summary>
        /// APIパラメータ [API parameter]
        /// </summary>
        public virtual UpdateStockAPIParam param { get; set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        public UpdateStockAPI(UpdateStockAPIParam param)
        {
            this.param = param;
        }

        /// <summary>
        /// 在庫数更新 [Update stock]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Request object of HARC]</param>
        /// <param name="products">商品情報リスト [List of product information]</param>
        /// <returns>処理結果 [Result]</returns>
        public virtual Dictionary<string, List<object>> UpdateStock(HarcApiRequest request, IList<UpdateStockParam> products)
        {
            // APIパラメータ取得 [Get the parameter for API]
            var paramApi = this.param.GetHarcApiParam(this.api_name, products);

            try
            {
                // APIリクエスト [API request]
                var objJson = request.simpleRequest(paramApi);

                return ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy().ConvertUpdateStockResult(objJson);
            }
            catch (HarcApiException e)
            {
                throw new APIFailedException(e, paramApi);
            }
        }
    }
}
