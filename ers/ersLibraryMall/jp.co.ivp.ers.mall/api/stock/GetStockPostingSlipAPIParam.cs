﻿using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.stock
{
    /// <summary>
    /// 在庫更新履歴詳細取得APIパラメタ [Get the detail of history of update stock API param]
    /// </summary>
    public class GetStockPostingSlipAPIParam
    {
        /// <summary>
        /// HARC API パラメータ取得 [Get the parameter for HARC API]
        /// </summary>
        /// <param name="apiName">API名 [API name]</param>
        /// <param name="stockId">在庫リクエストID [Stock request ID]</param>
        /// <returns>HARC API パラメータ [Parameter for HARC API]</returns>
        public virtual HarcApiParam GetHarcApiParam(string apiName, int? stockId)
        {
            var apiParam = new HarcApiParam(apiName);

            apiParam.Param.Add("stockPostingSlipId", stockId);

            return apiParam;
        }
    }
}
