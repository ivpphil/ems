﻿using System.Collections.Generic;
using com.hunglead.harc;
using jp.co.ivp.ers.mall.common;

namespace jp.co.ivp.ers.mall.api.stock
{
    /// <summary>
    /// 在庫数更新APIパラメータ [API parameter for update stock]
    /// </summary>
    public class UpdateStockAPIParam
    {
        /// <summary>
        /// ショップID [Shop ID]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// HARC APIパラメータ取得 [Get the parameter for HARC API]
        /// </summary>
        /// <param name="api_name">API名 [API name]</param>
        /// <param name="products">商品情報リスト [List of product information]</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParam(string api_name, IList<UpdateStockParam> products)
        {
            var apiParam = new HarcApiParam(api_name);
            var paramDic = new HarcDictionary();

            paramDic.Add("shopId", this.shop_id);

            foreach (var product in products)
            {
                var dicParam = new HarcDictionary();

                dicParam.Add("productCode", product.mallProductCode);
                dicParam.Add("quantity", product.quantity);
                dicParam.Add("operation", product.operation.ToString());

                paramDic.AddArray("products", dicParam);
            }

            apiParam.Param = paramDic;

            return apiParam;
        }
    }

    /// <summary>
    /// 在庫更新API用商品情報 [Product information for Update stock API]
    /// </summary>
    public struct UpdateStockParam
    {
        /// <summary>
        /// モール用商品コード [Product code for Mall]
        /// </summary>
        public string mallProductCode
        {
            get
            {
                return ErsMallCommonService.GetMallSkuFromErs(this.productCode);
            }
        }

        /// <summary>
        /// 商品ID [Product ID]
        /// </summary>
        public int? productId { get; set; }

        /// <summary>
        /// 商品コード [Product code]
        /// </summary>
        public string productCode { get; set; }

        /// <summary>
        /// 数量 [Quantity]
        /// </summary>
        public int? quantity { get; set; }

        /// <summary>
        /// 操作 [Operation]
        /// </summary>
        public EnumMallStockOperation? operation { get; set; }
    }
}
