﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.stock
{
    /// <summary>
    /// 在庫更新履歴詳細取得API [API for Get the detail of history of update stock]
    /// </summary>
    public class GetStockPostingSlipAPI
    {
        /// <summary>
        /// APIエラーリトライカウント [API retry count for error]
        /// </summary>
        public static int API_ERROR_RETRY_COUNT = 5;

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listApiErrorMessage = new List<string>()
        {
            "Internal Error",       // 内部エラー [Internal error]
            "request error"         // サーバ接続不可 [Doesn't connect to the server]
        };


        /// <summary>
        /// API名 [API name]
        /// </summary>
        public virtual string apiName
        {
            get
            {
                return "getStockPostingSlip";
            }
        }

        /// <summary>
        /// APIパラメータ [API parameter]
        /// </summary>
        public virtual GetStockPostingSlipAPIParam param { get; set; }

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">GetOrderInfosAPIParamBase</param>
        public GetStockPostingSlipAPI(GetStockPostingSlipAPIParam param)
        {
            this.param = param;
        }

        /// <summary>
        /// 在庫更新履歴検索取得 [Get the detail of history of update stock]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Request object of HARC]</param>
        /// <param name="stockId">在庫リクエストID [Stock request ID]</param>
        /// <returns>処理結果 [Result]</returns>
        public virtual Dictionary<string, object> GetStockPostingSlipDetail(HarcApiRequest request, int? stockId)
        {
            // APIパラメータ取得 [Get the parameter for API]
            var paramApi = this.param.GetHarcApiParam(this.apiName, stockId);

            int retry = 0;

            while (true)
            {
                try
                {
                    // APIリクエスト [API request]
                    var objJson = request.simpleRequest(paramApi);

                    return ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy().ConvertGetStockPostingSlipResult(objJson);
                }
                catch (HarcApiException e)
                {
                    // リトライ可能な場合は5回までリトライさせる
                    if (this.IsRetryAbleError(e.Message))
                    {
                        if (retry++ < API_ERROR_RETRY_COUNT)
                        {
                            continue;
                        }
                    }

                    throw new APIFailedException(e, paramApi);
                }
            }
        }

        /// <summary>
        /// リトライ可能なエラーかどうか [Whether error can retry]
        /// </summary>
        /// <param name="errorMessage">エラーメッセージ [Error message]</param>
        /// <returns>true : リトライ可能 [Can retry] / false : リトライ不可 [Can't retry]</returns>
        protected virtual bool IsRetryAbleError(string errorMessage)
        {
            foreach (var message in this.listApiErrorMessage)
            {
                if (errorMessage.Contains(message))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
