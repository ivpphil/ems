﻿using System;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.stock
{
    /// <summary>
    /// 在庫更新履歴検索APIパラメタ [Get search history of update stock API param]
    /// </summary>
    public class FindStockPostingSlipAPIParam
    {
        /// <summary>
        /// 店舗ID [Mall shop ID]
        /// </summary>
        public virtual int? shopId { get; set; }

        /// <summary>
        /// 更新ステータス [Status of update]
        /// </summary>
        public virtual EnumMallStockPostingSlipStatus? status { get; set; }

        /// <summary>
        /// 検索開始日時 [Datetime of search start]
        /// </summary>
        public virtual DateTime? searchFrom { get; set; }

        /// <summary>
        /// 検索終了日時 [Datetime of search end]
        /// </summary>
        public virtual DateTime? searchTo { get; set; }


        /// <summary>
        /// HARC API パラメータ取得 [Get the parameter for HARC API]
        /// </summary>
        /// <param name="apiName">API名 [API name]</param>
        /// <param name="status">更新ステータス [Status of update]</param>
        /// <returns>HARC API パラメータ [Parameter for HARC API]</returns>
        public virtual HarcApiParam GetHarcApiParam(string apiName, EnumMallStockPostingSlipStatus? status)
        {
            var apiParam = new HarcApiParam(apiName);

            var paramDic = new HarcDictionary();

            {
                var dicParam = new HarcDictionary();

                dicParam.Add("shopId", this.shopId);
                dicParam.Add("status", (int)status);
                dicParam.Add("periodFrom", this.searchFrom.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                dicParam.Add("periodTo", this.searchTo.Value.ToString("yyyy/MM/dd HH:mm:ss"));

                paramDic.Add("condition", dicParam);
            }

            apiParam.Param = paramDic;

            return apiParam;
        }
    }
}
