﻿using jp.co.ivp.ers.mall.api.order;
using jp.co.ivp.ers.mall.api.order_status;
using jp.co.ivp.ers.mall.api.payment;
using jp.co.ivp.ers.mall.api.product;
using jp.co.ivp.ers.mall.api.shop;
using jp.co.ivp.ers.mall.api.stock;

namespace jp.co.ivp.ers.mall.api
{
    /// <summary>
    /// モールAPI関連ファクトリ [Factory for api of mall]
    /// </summary>
    public class ErsMallAPIFactory
    {
        #region 店舗情報関連 [Shop information]
        /// <summary>
        /// 店舗情報追加API取得 [Get API for add shop information]
        /// </summary>
        /// <param name="param">AddShopInfoAPIParam</param>
        /// <returns>AddShopInfoAPI</returns>
        public AddShopInfoAPI GetAddShopInfoAPI(AddShopInfoAPIParam param)
        {
            return new AddShopInfoAPI(param);
        }

        /// <summary>
        /// 店舗情報追加APIパラメータ取得 [Get API parameter for add shop information]
        /// </summary>
        /// <returns>AddShopInfoAPIParam</returns>
        public AddShopInfoAPIParam GetAddShopInfoAPIParam()
        {
            return new AddShopInfoAPIParam();
        }

        /// <summary>
        /// 店舗情報更新API取得 [Get API for update shop information]
        /// </summary>
        /// <param name="param">AddShopInfoAPIParam</param>
        /// <returns>UpdateShopInfoAPI</returns>
        public UpdateShopInfoAPI GetUpdateShopInfoAPI(UpdateShopInfoAPIParam param)
        {
            return new UpdateShopInfoAPI(param);
        }

        /// <summary>
        /// 店舗情報更新APIパラメータ取得 [Get API parameter for update shop information]
        /// </summary>
        /// <returns>UpdateShopInfoAPIParam</returns>
        public UpdateShopInfoAPIParam GetUpdateShopInfoAPIParam()
        {
            return new UpdateShopInfoAPIParam();
        }
        #endregion

        #region 受注データ関連 [Order]
        /// <summary>
        /// モール伝票データ取得API 取得 [Get get order infos API base]
        /// </summary>
        /// <param name="param">GetOrderInfosAPIParamBase</param>
        /// <returns>GetOrderInfosAPIBase</returns>
        public GetOrderInfosAPIBase GetGetOrderInfosAPIBase(GetOrderInfosAPIParamBase param)
        {
            return new GetOrderInfosAPIBase(param);
        }

        /// <summary>
        /// 楽天市場モール伝票データ取得API 取得 [Get get order infos API Rakuten]
        /// </summary>
        /// <param name="param">GetOrderInfosAPIParamBase</param>
        /// <returns>GetOrderInfosAPIRakuten</returns>
        public GetOrderInfosAPIRakuten GetGetOrderInfosAPIRakuten(GetOrderInfosAPIParamBase param)
        {
            return new GetOrderInfosAPIRakuten(param);
        }

        /// <summary>
        /// Yahooモール伝票データ取得API 取得 [Get get order infos API Yahoo]
        /// </summary>
        /// <param name="param">GetOrderInfosAPIParamBase</param>
        /// <returns>GetOrderInfosAPIYahoo</returns>
        public GetOrderInfosAPIYahoo GetGetOrderInfosAPIYahoo(GetOrderInfosAPIParamBase param)
        {
            return new GetOrderInfosAPIYahoo(param);
        }

        /// <summary>
        /// Amazonモール伝票データ取得API 取得 [Get get order infos API Amazon]
        /// </summary>
        /// <param name="param">GetOrderInfosAPIParamBase</param>
        /// <returns>GetOrderInfosAPIAmazon</returns>
        public GetOrderInfosAPIAmazon GetGetOrderInfosAPIAmazon(GetOrderInfosAPIParamBase param)
        {
            return new GetOrderInfosAPIAmazon(param);
        }

        /// <summary>
        /// モール伝票データ取得APIパラメタ 取得 [Get get order infos API param base]
        /// </summary>
        /// <returns>GetOrderInfosAPIParamBase</returns>
        public GetOrderInfosAPIParamBase GetGetOrderInfosAPIParamBase()
        {
            return new GetOrderInfosAPIParamBase();
        }

        /// <summary>
        /// Yahoo!モール受注データ取得APIパラメタ 取得 [Get get order infos API param Yahoo!]
        /// </summary>
        /// <returns>GetOrderInfosAPIParamYahoo</returns>
        public GetOrderInfosAPIParamYahoo GetGetOrderInfosAPIParamYahoo()
        {
            return new GetOrderInfosAPIParamYahoo();
        }

        /// <summary>
        /// Amazonモール伝票データ取得APIパラメタ 取得 [Get get order infos API param Amazon]
        /// </summary>
        /// <returns>GetOrderInfosAPIParamYahoo</returns>
        public GetOrderInfosAPIParamAmazon GetGetOrderInfosAPIParamAmazon()
        {
            return new GetOrderInfosAPIParamAmazon();
        }
        #endregion

        #region 受注ステータス関連 [Order status]
        /// <summary>
        /// モール伝票データ ステータス変更API 取得 [Get change order status API base]
        /// </summary>
        /// <param name="param">ChangeOrderStatusAPIParamBase</param>
        /// <returns>ChangeOrderStatusAPIBase</returns>
        public ChangeOrderStatusAPIBase GetChangeOrderStatusAPIBase(ChangeOrderStatusAPIParamBase param)
        {
            return new ChangeOrderStatusAPIBase(param);
        }

        /// <summary>
        /// 楽天市場モール伝票データ ステータス変更API 取得 [Get change order status API Rakuten]
        /// </summary>
        /// <param name="param">ChangeOrderStatusAPIParamBase</param>
        /// <returns>ChangeOrderStatusAPIRakuten</returns>
        public ChangeOrderStatusAPIRakuten GetChangeOrderStatusAPIRakuten(ChangeOrderStatusAPIParamBase param)
        {
            return new ChangeOrderStatusAPIRakuten(param);
        }

        /// <summary>
        /// Yahooモール伝票データ ステータス変更API 取得 [Get change order status API Yahoo]
        /// </summary>
        /// <param name="param">ChangeOrderStatusAPIParamBase</param>
        /// <returns>ChangeOrderStatusAPIYahoo</returns>
        public ChangeOrderStatusAPIYahoo GetChangeOrderStatusAPIYahoo(ChangeOrderStatusAPIParamBase param)
        {
            return new ChangeOrderStatusAPIYahoo(param);
        }

        /// <summary>
        /// Amazonモール伝票データ ステータス変更API 取得 [Get change order status API Amazon]
        /// </summary>
        /// <param name="param">ChangeOrderStatusAPIParamBase</param>
        /// <returns>ChangeOrderStatusAPIAmazon</returns>
        public ChangeOrderStatusAPIAmazon GetChangeOrderStatusAPIAmazon(ChangeOrderStatusAPIParamBase param)
        {
            return new ChangeOrderStatusAPIAmazon(param);
        }

        /// <summary>
        /// モール伝票データ ステータス変更APIパラメタ 取得 [Get change order status API param base]
        /// </summary>
        /// <returns>ChangeOrderStatusAPIParamBase</returns>
        public ChangeOrderStatusAPIParamBase GetChangeOrderStatusAPIParamBase()
        {
            return new ChangeOrderStatusAPIParamBase();
        }

        /// <summary>
        /// 楽天モール伝票データ ステータス変更APIパラメタ 取得 [Get change order status API param base Rakuten]
        /// </summary>
        /// <returns>ChangeOrderStatusAPIParamRakuten</returns>
        public ChangeOrderStatusAPIParamRakuten GetChangeOrderStatusAPIParamRakuten()
        {
            return new ChangeOrderStatusAPIParamRakuten();
        }

        /// <summary>
        /// Yahoo!モール伝票データ ステータス変更APIパラメタ 取得 [Get change order status API param base Yahoo!]
        /// </summary>
        /// <returns>ChangeOrderStatusAPIParamYahoo</returns>
        public ChangeOrderStatusAPIParamYahoo GetChangeOrderStatusAPIParamYahoo()
        {
            return new ChangeOrderStatusAPIParamYahoo();
        }

        /// <summary>
        /// Amazonモール伝票データ ステータス変更APIパラメタ 取得 [Get change order status API param base Amazon]
        /// </summary>
        /// <returns>ChangeOrderStatusAPIParamRakuten</returns>
        public ChangeOrderStatusAPIParamAmazon GetChangeOrderStatusAPIParamAmazon()
        {
            return new ChangeOrderStatusAPIParamAmazon();
        }
        #endregion

        #region クレジットカード決済関連 [Credit card payment]
        /// <summary>
        /// Yahoo!決済状況取得API取得 [Get the API for get payment condition (Yahoo!)]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        /// <returns>GetYahoosOrderPaymentInfosByOrderCodeAPI</returns>
        public GetYahoosOrderPaymentInfosByOrderCodeAPI GetGetYahoosOrderPaymentInfosByOrderCodeAPI(GetYahoosOrderPaymentInfosByOrderCodeAPIParam param)
        {
            return new GetYahoosOrderPaymentInfosByOrderCodeAPI(param);
        }

        /// <summary>
        /// Yahoo!決済状況取得APIパラメータ取得 [Get the API parameter for get payment condition (Yahoo!)]
        /// </summary>
        /// <returns>GetYahoosOrderPaymentInfosByOrderCodeAPIParam</returns>
        public GetYahoosOrderPaymentInfosByOrderCodeAPIParam GetGetYahoosOrderPaymentInfosByOrderCodeAPIParam()
        {
            return new GetYahoosOrderPaymentInfosByOrderCodeAPIParam();
        }

        /// <summary>
        /// Yahoo!売上確定API取得 [Get the API for execute payment (Yahoo!)]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        /// <returns>ExecuteYahoosOrderPaymentsAPI</returns>
        public ExecuteYahoosOrderPaymentsAPI GetExecuteYahoosOrderPaymentsAPI(ExecuteYahoosOrderPaymentsAPIParam param)
        {
            return new ExecuteYahoosOrderPaymentsAPI(param);
        }

        /// <summary>
        /// Yahoo!売上確定APIパラメータ取得 [Get the API parameter for execute payment (Yahoo!)]
        /// </summary>
        /// <returns>ExecuteYahoosOrderPaymentsAPIParam</returns>
        public ExecuteYahoosOrderPaymentsAPIParam GetExecuteYahoosOrderPaymentsAPIParam()
        {
            return new ExecuteYahoosOrderPaymentsAPIParam();
        }

        /// <summary>
        /// Yahoo!決済取消API取得 [Get the API for cancel payment (Yahoo!)]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        /// <returns>CancelYahoosOrderPaymentsAPI</returns>
        public CancelYahoosOrderPaymentsAPI GetCancelYahoosOrderPaymentsAPI(CancelYahoosOrderPaymentsAPIParam param)
        {
            return new CancelYahoosOrderPaymentsAPI(param);
        }

        /// <summary>
        /// Yahoo!決済取消APIパラメータ取得 [Get the API parameter for cancel payment (Yahoo!)]
        /// </summary>
        /// <returns>CancelYahoosOrderPaymentsAPIParam</returns>
        public CancelYahoosOrderPaymentsAPIParam GetCancelYahoosOrderPaymentsAPIParam()
        {
            return new CancelYahoosOrderPaymentsAPIParam();
        }

        /// <summary>
        /// Yahoo!オーソリAPI取得 [Get the API for authorize (Yahoo!)]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        /// <returns>AuthorizeYahoosOrderPaymentsAPI</returns>
        public AuthorizeYahoosOrderPaymentsAPI GetAuthorizeYahoosOrderPaymentsAPI(AuthorizeYahoosOrderPaymentsAPIParam param)
        {
            return new AuthorizeYahoosOrderPaymentsAPI(param);
        }

        /// <summary>
        /// Yahoo!オーソリAPIパラメータ取得 [Get the API parameter for authorize (Yahoo!)]
        /// </summary>
        /// <returns>AuthorizeYahoosOrderPaymentsAPIParam</returns>
        public AuthorizeYahoosOrderPaymentsAPIParam GetAuthorizeYahoosOrderPaymentsAPIParam()
        {
            return new AuthorizeYahoosOrderPaymentsAPIParam();
        }

        /// <summary>
        /// Yahoo!再オーソリAPI取得 [Get the API for re-authorize (Yahoo!)]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        /// <returns>ReAuthorizeYahoosOrderPaymentsAPI</returns>
        public ReAuthorizeYahoosOrderPaymentsAPI GetReAuthorizeYahoosOrderPaymentsAPI(ReAuthorizeYahoosOrderPaymentsAPIParam param)
        {
            return new ReAuthorizeYahoosOrderPaymentsAPI(param);
        }

        /// <summary>
        /// Yahoo!再オーソリAPIパラメータ取得 [Get the API parameter for re-authorize (Yahoo!)]
        /// </summary>
        /// <returns>ReAuthorizeYahoosOrderPaymentsAPIParam</returns>
        public ReAuthorizeYahoosOrderPaymentsAPIParam GetReAuthorizeYahoosOrderPaymentsAPIParam()
        {
            return new ReAuthorizeYahoosOrderPaymentsAPIParam();
        }

        /// <summary>
        /// 楽天受注データ更新API取得 [Get the API for update order (Rakuten)]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        /// <returns>UpdateRakutenOrderInfosAPI</returns>
        public UpdateRakutenOrderInfosAPI GetUpdateRakutenOrderInfosAPI(UpdateRakutenOrderInfosAPIParam param)
        {
            return new UpdateRakutenOrderInfosAPI(param);
        }

        /// <summary>
        /// 楽天受注データ更新APIパラメータ取得 [Get the API parameter for update order (Rakuten)]
        /// </summary>
        /// <returns>UpdateRakutenOrderInfosAPIParam</returns>
        public UpdateRakutenOrderInfosAPIParam GetUpdateRakutenOrderInfosAPIParam()
        {
            return new UpdateRakutenOrderInfosAPIParam();
        }
        #endregion

        #region 在庫関連 [Stock]
        /// <summary>
        /// 在庫数更新API取得 [Get the API for update stock]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        /// <returns>ChangeOrderStatusAPIBase</returns>
        public UpdateStockAPI GetUpdateStockAPI(UpdateStockAPIParam param)
        {
            return new UpdateStockAPI(param);
        }

        /// <summary>
        /// 在庫数更新APIパラメータ取得 [Get the API parameter for update stock]
        /// </summary>
        /// <returns>ChangeOrderStatusAPIParamBase</returns>
        public UpdateStockAPIParam GetUpdateStockAPIParam()
        {
            return new UpdateStockAPIParam();
        }
        #endregion

        #region 在庫更新履歴関連 [Stock posting slip]
        /// <summary>
        /// 在庫更新履歴検索API取得 [Get the API for search history of update stock]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        /// <returns>ChangeOrderStatusAPIBase</returns>
        public FindStockPostingSlipAPI GetFindStockPostingSlipAPI(FindStockPostingSlipAPIParam param)
        {
            return new FindStockPostingSlipAPI(param);
        }

        /// <summary>
        /// 在庫更新履歴検索APIパラメタ取得 [Get the API parameter for search history of update stock]
        /// </summary>
        /// <returns>FindStockPostingSlipAPIParam</returns>
        public FindStockPostingSlipAPIParam GetFindStockPostingSlipAPIParam()
        {
            return new FindStockPostingSlipAPIParam();
        }

        /// <summary>
        /// 在庫更新履歴検索API取得 [Get the API for search history of update stock]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        /// <returns>GetStockPostingSlipAPI</returns>
        public GetStockPostingSlipAPI GetGetStockPostingSlipAPI(GetStockPostingSlipAPIParam param)
        {
            return new GetStockPostingSlipAPI(param);
        }

        /// <summary>
        /// 在庫更新履歴詳細取得APIパラメタ取得 [Get the API parameter for get the detail of history of update stock API param]
        /// </summary>
        /// <returns>GetStockPostingSlipAPIParam</returns>
        public GetStockPostingSlipAPIParam GetGetStockPostingSlipAPIParam()
        {
            return new GetStockPostingSlipAPIParam();
        }
        #endregion

        #region 商品関連 [Product]
        /// <summary>
        /// 商品CSV登録API取得 [Get the API for import product csv]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        /// <returns>ImportRawProductCsvAPI</returns>
        public ImportRawProductCsvAPI GetImportRawProductCsvAPI(ImportRawProductCsvAPIParam param)
        {
            return new ImportRawProductCsvAPI(param);
        }

        /// <summary>
        /// 商品CSV登録APIパラメータ取得 [Get the API parameter for import product csv]
        /// </summary>
        /// <returns>ImportRawProductCsvAPIParam</returns>
        public ImportRawProductCsvAPIParam GetImportRawProductCsvAPIParam()
        {
            return new ImportRawProductCsvAPIParam();
        }
        #endregion

        #region 商品情報関連 [Product information]
        /// <summary>
        /// 店舗別商品情報取得API取得 [Get the API for get product information of mall shops]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        /// <returns>GetShopProductInfoAPI</returns>
        public GetShopProductInfoAPI GetGetShopProductInfoAPI(GetShopProductInfoAPIParam param)
        {
            return new GetShopProductInfoAPI(param);
        }

        /// <summary>
        /// 店舗別商品情報取得APIパラメータ取得 [Get the API parameter for get product information of mall shops]
        /// </summary>
        /// <returns>GetShopProductInfoAPIParam</returns>
        public GetShopProductInfoAPIParam GetGetShopProductInfoAPIParam()
        {
            return new GetShopProductInfoAPIParam();
        }

        /// <summary>
        /// 店舗別商品情報更新API取得 [Get the API for update product information of mall shops]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        /// <returns>UpdateShopProductInfoAPI</returns>
        public UpdateShopProductInfoAPI GetUpdateShopProductInfoAPI(UpdateShopProductInfoAPIParam param)
        {
            return new UpdateShopProductInfoAPI(param);
        }

        /// <summary>
        /// 店舗別商品情報更新APIパラメータ取得 [Get the API parameter for update product information of mall shops]
        /// </summary>
        /// <returns>UpdateShopProductInfoAPIParam</returns>
        public UpdateShopProductInfoAPIParam GetUpdateShopProductInfoAPIParam()
        {
            return new UpdateShopProductInfoAPIParam();
        }
        #endregion
    }
}
