﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.shop
{
    /// <summary>
    /// 店舗情報追加API [API for add shop information]
    /// </summary>
    public class AddShopInfoAPI
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public virtual string api_name
        {
            get
            {
                return "addShopInfo";
            }
        }

        /// <summary>
        /// APIパラメータ [API parameter]
        /// </summary>
        public virtual AddShopInfoAPIParam param { get; set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        public AddShopInfoAPI(AddShopInfoAPIParam param)
        {
            this.param = param;
        }

        /// <summary>
        /// 店舗情報追加 [Add shop information]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Request object of HARC]</param>
        /// <param name="param">パラメータ [Parameter]</param>
        /// <returns>処理結果 [Result]</returns>
        public virtual Dictionary<string, object> AddShopInfo(HarcApiRequest request, AddShopInfoParam param)
        {
            // APIパラメータ取得 [Get the parameter for API]
            var paramApi = this.param.GetHarcApiParam(this.api_name, param);

            try
            {
                // APIリクエスト [API request]
                var objJson = request.simpleRequest(paramApi);

                return ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy().ConvertAddShopInfoResult(objJson);
            }
            catch (HarcApiException e)
            {
                throw new APIFailedException(e, paramApi);
            }
        }
    }
}
