﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.shop
{
    /// <summary>
    /// 店舗情報更新APIパラメータ [API parameter for update shop information]
    /// </summary>
    public class UpdateShopInfoAPIParam
    {
        /// <summary>
        /// HARC APIパラメータ取得 [Get the parameter for HARC API]
        /// </summary>
        /// <param name="api_name">API名 [API name]</param>
        /// <param name="param">パラメータ [Parameter]</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParam(string api_name, UpdateShopInfoParam param)
        {
            var apiParam = new HarcApiParam(api_name);
            var paramDic = new HarcDictionary();

            paramDic.Add("shopId", param.shopId);
            paramDic.Add("shopName", param.shopName);

            if (param.basicParam != null)
            {
                var dicParam = new HarcDictionary();

                foreach (var key in param.basicParam.Keys)
                {
                    dicParam.Add(key, param.basicParam[key]);
                }

                paramDic.Add("basicParam", dicParam);
            }

            if (param.stockPostingParam != null)
            {
                var dicParam = new HarcDictionary();

                foreach (var key in param.stockPostingParam.Keys)
                {
                    dicParam.Add(key, param.stockPostingParam[key]);
                }

                paramDic.Add("stockPostingParam", dicParam);
            }

            if (param.productPostingParam != null)
            {
                var dicParam = new HarcDictionary();

                foreach (var key in param.productPostingParam.Keys)
                {
                    dicParam.Add(key, param.productPostingParam[key]);
                }

                paramDic.Add("productPostingParam", dicParam);
            }

            if (param.orderScrapingParam != null)
            {
                var dicParam = new HarcDictionary();

                foreach (var key in param.orderScrapingParam.Keys)
                {
                    dicParam.Add(key, param.orderScrapingParam[key]);
                }

                paramDic.Add("orderScrapingParam", dicParam);
            }

            apiParam.Param = paramDic;

            return apiParam;
        }
    }

    /// <summary>
    /// 店舗情報更新API用パラメータ [Parameter for Update shop information]
    /// </summary>
    public struct UpdateShopInfoParam
    {
        /// <summary>
        /// 店舗ID [Shop id]
        /// </summary>
        public int? shopId { get; set; }

        /// <summary>
        /// 店舗名 [Shop name]
        /// </summary>
        public string shopName { get; set; }

        /// <summary>
        /// 基本設定 [Basic setting]
        /// </summary>
        public IDictionary<string, object> basicParam { get; set; }

        /// <summary>
        /// 在庫設定 [Stock setting]
        /// </summary>
        public IDictionary<string, object> stockPostingParam { get; set; }

        /// <summary>
        /// 商品設定 [Product setting]
        /// </summary>
        public IDictionary<string, object> productPostingParam { get; set; }

        /// <summary>
        /// 商品設定 [Product setting]
        /// </summary>
        public IDictionary<string, object> orderScrapingParam { get; set; }
    }
}
