﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.shop
{
    /// <summary>
    /// 店舗情報更新API [API for update shop information]
    /// </summary>
    public class UpdateShopInfoAPI
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public virtual string api_name
        {
            get
            {
                return "updateShopInfo";
            }
        }

        /// <summary>
        /// APIパラメータ [API parameter]
        /// </summary>
        public virtual UpdateShopInfoAPIParam param { get; set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        public UpdateShopInfoAPI(UpdateShopInfoAPIParam param)
        {
            this.param = param;
        }

        /// <summary>
        /// 店舗情報更新 [Update shop information]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Request object of HARC]</param>
        /// <param name="param">パラメータ [Parameter]</param>
        /// <returns>処理結果 [Result]</returns>
        public virtual Dictionary<string, object> UpdateShopInfo(HarcApiRequest request, UpdateShopInfoParam param)
        {
            // APIパラメータ取得 [Get the parameter for API]
            var paramApi = this.param.GetHarcApiParam(this.api_name, param);

            try
            {
                // APIリクエスト [API request]
                var objJson = request.simpleRequest(paramApi);

                return ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy().ConvertUpdateShopInfoResult(objJson);
            }
            catch (HarcApiException e)
            {
                throw new APIFailedException(e, paramApi);
            }
        }
    }
}
