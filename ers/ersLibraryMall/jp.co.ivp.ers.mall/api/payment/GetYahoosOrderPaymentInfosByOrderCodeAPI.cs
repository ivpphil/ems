﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.payment
{
    /// <summary>
    /// Yahoo!決済状況取得API [API for get payment condition (Yahoo!)]
    /// </summary>
    public class GetYahoosOrderPaymentInfosByOrderCodeAPI
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public virtual string api_name
        {
            get
            {
                return "getYahoosOrderPaymentInfosByOrderCode";
            }
        }

        /// <summary>
        /// APIパラメータ [API parameter]
        /// </summary>
        public virtual GetYahoosOrderPaymentInfosByOrderCodeAPIParam param { get; set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        public GetYahoosOrderPaymentInfosByOrderCodeAPI(GetYahoosOrderPaymentInfosByOrderCodeAPIParam param)
        {
            this.param = param;
        }

        /// <summary>
        /// 決済状況取得 [Get payment condition]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Request object of HARC]</param>
        /// <param name="listOrderCode">受注番号リスト [List of order code]</param>
        /// <returns>処理結果 [Result]</returns>
        public virtual Dictionary<string, List<Dictionary<string, object>>> GetOrderPaymentInfos(HarcApiRequest request, List<string> listOrderCode)
        {
            // APIパラメータ取得 [Get the parameter for API]
            var paramApi = this.param.GetHarcApiParam(this.api_name, listOrderCode);

            int retry = 0;

            while (true)
            {
                try
                {
                    // APIリクエスト [API request]
                    var objJson = request.simpleRequest(paramApi);

                    return ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy().ConvertGetYahoosOrderPaymentInfosByOrderCodeResultJson(objJson);
                }
                catch (HarcApiException e)
                {
                    // リトライ可能な場合は5回までリトライさせる
                    if (YahooPaymentAPIRetryableSpec.IsRetryAbleError(e.Message))
                    {
                        if (retry++ < YahooPaymentAPIRetryableSpec.API_ERROR_RETRY_COUNT)
                        {
                            continue;
                        }
                    }

                    throw new APIFailedException(e, paramApi);
                }
            }
        }
    }
}
