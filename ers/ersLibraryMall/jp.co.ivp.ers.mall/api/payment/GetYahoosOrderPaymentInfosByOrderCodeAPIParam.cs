﻿using System.Collections;
using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.payment
{
    /// <summary>
    /// Yahoo!決済状況取得APIパラメータ [API parameter for get payment condition (Yahoo!)]
    /// </summary>
    public class GetYahoosOrderPaymentInfosByOrderCodeAPIParam
    {
        /// <summary>
        /// ショップID [Shop ID]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// HARC APIパラメータ取得 [Get the parameter for HARC API]
        /// </summary>
        /// <param name="api_name">API名 [API name]</param>
        /// <param name="listOrderCode">受注番号リスト [List of order code]</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParam(string api_name, List<string> listOrderCode)
        {
            var apiParam = new HarcApiParam(api_name);

            apiParam.Param.Add("shopId", this.shop_id);
            apiParam.Param.Add("orderCodeList", new ArrayList(listOrderCode));

            return apiParam;
        }
    }
}
