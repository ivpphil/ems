﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.payment
{
    /// <summary>
    /// Yahoo!オーソリAPI [API for authorize (Yahoo!)]
    /// </summary>
    public class AuthorizeYahoosOrderPaymentsAPI
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public virtual string api_name
        {
            get
            {
                return "authorizeYahoosOrderPayments";
            }
        }

        /// <summary>
        /// APIパラメータ [API parameter]
        /// </summary>
        public virtual AuthorizeYahoosOrderPaymentsAPIParam param { get; set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        public AuthorizeYahoosOrderPaymentsAPI(AuthorizeYahoosOrderPaymentsAPIParam param)
        {
            this.param = param;
        }

        /// <summary>
        /// オーソリ [Authorize]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Request object of HARC]</param>
        /// <param name="listPaymentInfo">決済情報リスト [List of payment information]</param>
        /// <returns>処理結果 [Result]</returns>
        public virtual List<Dictionary<string, object>> Authorize(HarcApiRequest request, IList<AuthorizeYahoosOrderPaymentsParam> listPaymentInfo)
        {
            // APIパラメータ取得 [Get the parameter for API]
            var paramApi = this.param.GetHarcApiParam(this.api_name, listPaymentInfo);

            int retry = 0;

            while (true)
            {
                try
                {
                    // APIリクエスト [API request]
                    var objJson = request.simpleRequest(paramApi);

                    return ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy().ConvertAuthorizeYahoosOrderPaymentsResult(objJson);
                }
                catch (HarcApiException e)
                {
                    // リトライ可能な場合は5回までリトライさせる
                    if (YahooPaymentAPIRetryableSpec.IsRetryAbleError(e.Message))
                    {
                        if (retry++ < YahooPaymentAPIRetryableSpec.API_ERROR_RETRY_COUNT)
                        {
                            continue;
                        }
                    }

                    throw new APIFailedException(e, paramApi);
                }
            }
        }
    }
}
