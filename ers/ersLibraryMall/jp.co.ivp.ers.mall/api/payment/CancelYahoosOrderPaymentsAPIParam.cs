﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.payment
{
    /// <summary>
    /// Yahoo!決済取消APIパラメータ [API parameter for cancel payment (Yahoo!)]
    /// </summary>
    public class CancelYahoosOrderPaymentsAPIParam
    {
        /// <summary>
        /// ショップID [Shop ID]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// HARC APIパラメータ取得 [Get the parameter for HARC API]
        /// </summary>
        /// <param name="api_name">API名 [API name]</param>
        /// <param name="listPaymentInfo">決済情報リスト [List of payment information]</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParam(string api_name, IList<CancelYahoosOrderPaymentsParam> listPaymentInfo)
        {
            var apiParam = new HarcApiParam(api_name);
            var paramDic = new HarcDictionary();

            paramDic.Add("shopId", this.shop_id);

            foreach (var payment in listPaymentInfo)
            {
                var dicParam = new HarcDictionary();

                dicParam.Add("paymentId", payment.paymentId);
                dicParam.Add("orderCode", payment.orderCode);
                dicParam.Add("cancelType", (int)payment.cancelType);
                dicParam.Add("cancelReason", (int)payment.cancelReason);

                paramDic.AddArray("orders", dicParam);
            }

            apiParam.Param = paramDic;

            return apiParam;
        }
    }

    /// <summary>
    /// Yahoo!決済取API用決済情報 [Payment information for Cancel payment API (Yahoo!)]
    /// </summary>
    public struct CancelYahoosOrderPaymentsParam
    {
        /// <summary>
        /// 決済ID [Payment ID]
        /// </summary>
        public string paymentId { get; set; }

        /// <summary>
        /// 受注番号 [Order code]
        /// </summary>
        public string orderCode { get; set; }

        /// <summary>
        /// 取消種別 [Type of cancel]
        /// </summary>
        public EnumMallYahooCardPaymentCancelType? cancelType { get; set; }

        /// <summary>
        /// 取消理由 [Reason of cancel]
        /// </summary>
        public EnumMallYahooCardPaymentCancelReason? cancelReason { get; set; }
    }
}
