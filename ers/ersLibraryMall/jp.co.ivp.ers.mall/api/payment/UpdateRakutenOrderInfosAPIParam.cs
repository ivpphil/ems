﻿using System;
using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.payment
{
    /// <summary>
    /// 楽天受注データ更新APIパラメータ [API parameter for update order (Rakuten)]
    /// </summary>
    public class UpdateRakutenOrderInfosAPIParam
    {
        /// <summary>
        /// ショップID [Shop ID]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// HARC APIパラメータ取得 [Get the parameter for HARC API]
        /// </summary>
        /// <param name="api_name">API名 [API name]</param>
        /// <param name="orders">受注情報リスト [List of order information]</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParam(string api_name, IList<UpdateRakutenOrderInfosParam> orders)
        {
            var apiParam = new HarcApiParam(api_name);
            var paramDic = new HarcDictionary();

            paramDic.Add("shopId", this.shop_id);

            foreach (var order in orders)
            {
                var dicParam = new HarcDictionary();

                dicParam.Add("orderCode", order.orderCode);
                dicParam.Add("orderDate", order.orderDate.Value.ToString("yyyy/MM/dd"));
                dicParam.Add("paymentOperation", order.paymentOperation.ToString());

                paramDic.AddArray("targetOrders", dicParam);
            }

            apiParam.Param = paramDic;

            return apiParam;
        }
    }

    /// <summary>
    /// 楽天受注データ更新API用受注情報 [Order information for Update order API(Rakuten)]
    /// </summary>
    public struct UpdateRakutenOrderInfosParam
    {
        /// <summary>
        /// 受注番号 [Order code]
        /// </summary>
        public string orderCode { get; set; }

        /// <summary>
        /// 受注日時 [Order date]
        /// </summary>
        public DateTime? orderDate { get; set; }

        /// <summary>
        /// オーソリ金額 [Authorize price]
        /// </summary>
        public EnumMallRakutenPaymentOperation? paymentOperation { get; set; }
    }
}
