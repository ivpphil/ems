﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.payment
{
    /// <summary>
    /// Yahoo!オーソリAPIパラメータ [API parameter for authorize (Yahoo!)]
    /// </summary>
    public class AuthorizeYahoosOrderPaymentsAPIParam
    {
        /// <summary>
        /// ショップID [Shop ID]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// HARC APIパラメータ取得 [Get the parameter for HARC API]
        /// </summary>
        /// <param name="api_name">API名 [API name]</param>
        /// <param name="listPaymentInfo">決済情報リスト [List of payment information]</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParam(string api_name, IList<AuthorizeYahoosOrderPaymentsParam> listPaymentInfo)
        {
            var apiParam = new HarcApiParam(api_name);
            var paramDic = new HarcDictionary();

            paramDic.Add("shopId", this.shop_id);

            foreach (var payment in listPaymentInfo)
            {
                var dicParam = new HarcDictionary();

                dicParam.Add("paymentId", payment.paymentId);
                dicParam.Add("orderCode", payment.orderCode);
                dicParam.Add("authorizeType", (int)payment.authorizeType);
                dicParam.Add("authorizePrice", payment.authorizePrice);
                dicParam.Add("paymentMethod", payment.paymentMethod);
                dicParam.Add("paymentCount", payment.paymentCount);
                dicParam.Add("remarks", payment.remarks);

                paramDic.AddArray("orders", dicParam);
            }

            apiParam.Param = paramDic;

            return apiParam;
        }
    }

    /// <summary>
    /// Yahoo!オーソリAPI用決済情報 [Payment information for Authorize API (Yahoo!)]
    /// </summary>
    public struct AuthorizeYahoosOrderPaymentsParam
    {
        /// <summary>
        /// 決済ID [Payment ID]
        /// </summary>
        public string paymentId { get; set; }

        /// <summary>
        /// 受注番号 [Order code]
        /// </summary>
        public string orderCode { get; set; }

        /// <summary>
        /// オーソリタイプ [Authorize type]
        /// </summary>
        public EnumMallYahooCardPaymentAuthorizeType? authorizeType { get; set; }

        /// <summary>
        /// オーソリ金額 [Authorize price]
        /// </summary>
        public int? authorizePrice { get; set; }

        /// <summary>
        /// 支払い方法 [Payment method]
        /// </summary>
        public int? paymentMethod { get; set; }

        /// <summary>
        /// 分割回数 [Payment count]
        /// </summary>
        public int? paymentCount { get; set; }

        /// <summary>
        /// 備考 [Remarks]
        /// </summary>
        public string remarks { get; set; }
    }
}
