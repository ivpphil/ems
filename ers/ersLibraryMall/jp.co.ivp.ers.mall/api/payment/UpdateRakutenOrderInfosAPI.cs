﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.payment
{
    /// <summary>
    /// 楽天受注データ更新API [API for update order (Rakuten)]
    /// </summary>
    public class UpdateRakutenOrderInfosAPI
    {
        #region 結果メッセージ [Result message]
        /// <summary>
        /// 成功 [Seccess]
        /// </summary>
        public static string RESULT_MESSAGE_SUCCESS = "Success.";

        /// <summary>
        /// オーソリ済み [Already authorized]
        /// </summary>
        public static string RESULT_MESSAGE_ALREADY_AUTHORIZED = "Already Authorized.";
        #endregion

        /// <summary>
        /// APIエラーリトライカウント [API retry count for error]
        /// </summary>
        public static int API_ERROR_RETRY_COUNT = 5;

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listApiErrorMessage = new List<string>()
        {
            "Analyze Failed.",      // 解析失敗 [Analyze Failed]
            "Internal Error",       // 内部エラー [Internal error]
            "request error",        // サーバ接続不可 [Doesn't connect to the server]
        };

        /// <summary>
        /// API名 [API name]
        /// </summary>
        public virtual string api_name
        {
            get
            {
                return "updateRakutenOrderInfos";
            }
        }

        /// <summary>
        /// APIパラメータ [API parameter]
        /// </summary>
        public virtual UpdateRakutenOrderInfosAPIParam param { get; set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        public UpdateRakutenOrderInfosAPI(UpdateRakutenOrderInfosAPIParam param)
        {
            this.param = param;
        }

        /// <summary>
        /// 決済実行 [Execute payment]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Request object of HARC]</param>
        /// <param name="orders">受注情報リスト [List of order information]</param>
        /// <returns>処理結果 [Result]</returns>
        public virtual List<Dictionary<string, object>> ExecutePayment(HarcApiRequest request, IList<UpdateRakutenOrderInfosParam> orders)
        {
            // APIパラメータ取得 [Get the parameter for API]
            var paramApi = this.param.GetHarcApiParam(this.api_name, orders);

            int retry = 0;

            while (true)
            {
                try
                {
                    // APIリクエスト [API request]
                    var objJson = request.simpleRequest(paramApi);

                    return ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy().ConvertUpdateRakutenOrderInfosResult(objJson);
                }
                catch (HarcApiException e)
                {
                    // リトライ可能な場合は5回までリトライさせる
                    if (this.IsRetryAbleError(e.Message))
                    {
                        if (retry++ < API_ERROR_RETRY_COUNT)
                        {
                            continue;
                        }
                    }

                    throw new APIFailedException(e, paramApi);
                }
            }
        }

        /// <summary>
        /// リトライ可能なエラーかどうか [Whether error can retry]
        /// </summary>
        /// <param name="errorMessage">エラーメッセージ [Error message]</param>
        /// <returns>true : リトライ可能 [Can retry] / false : リトライ不可 [Can't retry]</returns>
        protected virtual bool IsRetryAbleError(string errorMessage)
        {
            foreach (var message in this.listApiErrorMessage)
            {
                if (errorMessage.Contains(message))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
