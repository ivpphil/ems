﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.payment
{
    /// <summary>
    /// Yahoo!決済APIリトライ可能判定 [Judgment retryable for Payment API of Yahoo!]
    /// </summary>
    public static class YahooPaymentAPIRetryableSpec
    {
        /// <summary>
        /// APIエラーリトライカウント [API retry count for error]
        /// </summary>
        public static int API_ERROR_RETRY_COUNT = 5;

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        public static IList<string> listApiErrorMessage = new List<string>()
        {
            "Login Failed.",                // ログイン失敗 [Login failed]
            "Payment Manager not Found"     // 権限エラー（遷移エラー） [Authentication error (Transition error)]
        };


        /// <summary>
        /// リトライ可能なエラーかどうか [Whether error can retry]
        /// </summary>
        /// <param name="errorMessage">エラーメッセージ [Error message]</param>
        /// <returns>true : リトライ可能 [Can retry] / false : リトライ不可 [Can't retry]</returns>
        public static bool IsRetryAbleError(string errorMessage)
        {
            foreach (var message in listApiErrorMessage)
            {
                if (errorMessage.Contains(message))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
