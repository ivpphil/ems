﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.payment
{
    /// <summary>
    /// Yahoo!売上確定APIパラメータ [API parameter for execute payment (Yahoo!)]
    /// </summary>
    public class ExecuteYahoosOrderPaymentsAPIParam
    {
        /// <summary>
        /// ショップID [Shop ID]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// HARC APIパラメータ取得 [Get the parameter for HARC API]
        /// </summary>
        /// <param name="api_name">API名 [API name]</param>
        /// <param name="listPaymentInfo">決済情報リスト [List of payment information]</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParam(string api_name, IList<ExecuteYahoosOrderPaymentsParam> listPaymentInfo)
        {
            var apiParam = new HarcApiParam(api_name);
            var paramDic = new HarcDictionary();

            paramDic.Add("shopId", this.shop_id);

            foreach (var payment in listPaymentInfo)
            {
                var dicParam = new HarcDictionary();

                dicParam.Add("paymentId", payment.paymentId);
                dicParam.Add("orderCode", payment.orderCode);

                paramDic.AddArray("orders", dicParam);
            }

            apiParam.Param = paramDic;

            return apiParam;
        }
    }

    /// <summary>
    /// Yahoo!売上確定API用決済情報 [Payment information for Execute payment API (Yahoo!)]
    /// </summary>
    public struct ExecuteYahoosOrderPaymentsParam
    {
        /// <summary>
        /// 決済ID [Payment ID]
        /// </summary>
        public string paymentId { get; set; }

        /// <summary>
        /// 受注番号 [Order code]
        /// </summary>
        public string orderCode { get; set; }
    }
}
