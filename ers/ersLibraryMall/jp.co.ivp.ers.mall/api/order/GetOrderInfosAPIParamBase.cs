﻿using System;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.order
{
    /// <summary>
    /// モール伝票データ取得APIパラメタ [Get order infos API param base]
    /// </summary>
    public class GetOrderInfosAPIParamBase
    {
        /// <summary>
        /// ショップID [Shop ID]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// 店舗名 [Shop name]
        /// </summary>
        public virtual string shop_name { get; set; }

        /// <summary>
        /// 検索期間開始日 [Search date from]
        /// </summary>
        public virtual DateTime? search_date_from { get; set; }

        /// <summary>
        /// 検索期間終了日 [Search date to]
        /// </summary>
        public virtual DateTime? search_date_to { get; set; }

        /// <summary>
        /// HARC API パラメタ取得 [Get HARC API param]
        /// </summary>
        /// <param name="api_name">string</param>
        /// <param name="get_status">object</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParam(string api_name, object get_status)
        {
            var apiParam = new HarcApiParam(api_name);
            apiParam.Param.Add("shopId", this.shop_id);

            //プロパティに値がある場合は、パラメタに設定
            if (get_status != null)
            {
                apiParam.Param.Add("status", (int)get_status);
            }

            if (this.search_date_from != null)
            {
                apiParam.Param.Add("fromDate", this.search_date_from.Value.ToString("yyyy/MM/dd"));
            }

            if (this.search_date_to != null)
            {
                apiParam.Param.Add("toDate", this.search_date_to.Value.ToString("yyyy/MM/dd"));
            }

            return apiParam;
        }
    }
}
