﻿using System;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.order
{
    /// <summary>
    /// Amazonモール伝票データ取得APIパラメタ [Get order infos API param Amazon]
    /// </summary>
    public class GetOrderInfosAPIParamAmazon : GetOrderInfosAPIParamBase
    {
        /// <summary>
        /// 検索開始日時（作成日時） [From order date]
        /// </summary>
        public virtual DateTime? fromOrderDate { get; set; }

        /// <summary>
        /// 検索終了日時（作成日時） [To order date]
        /// </summary>
        public virtual DateTime? toOrderDate { get; set; }

        /// <summary>
        /// 検索開始日時（更新日時） [From last update date]
        /// </summary>
        public virtual DateTime? fromLastUpdateDate { get; set; }

        /// <summary>
        /// 検索終了日時（更新日時） [To last update date]
        /// </summary>
        public virtual DateTime? toLastUpdateDate { get; set; }

        /// <summary>
        /// HARC API パラメタ取得 [Get HARC API param]
        /// </summary>
        /// <param name="api_name">string</param>
        /// <param name="get_status">object</param>
        /// <returns>HarcApiParam</returns>
        public override HarcApiParam GetHarcApiParam(string api_name, object get_status)
        {
            var apiParam = new HarcApiParam(api_name);
            apiParam.Param.Add("shopId", this.shop_id);

            //プロパティに値がある場合は、パラメタに設定
            if (get_status != null)
            {
                apiParam.Param.Add("status", (int[])get_status);
            }

            if (this.fromOrderDate != null)
            {
                apiParam.Param.Add("fromOrderDate", this.fromOrderDate.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            if (this.toOrderDate != null)
            {
                apiParam.Param.Add("toOrderDate", this.toOrderDate.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            if (this.fromLastUpdateDate != null)
            {
                apiParam.Param.Add("fromLastUpdateDate", this.fromLastUpdateDate.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            if (this.toLastUpdateDate != null)
            {
                apiParam.Param.Add("toLastUpdateDate", this.toLastUpdateDate.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            return apiParam;
        }
    }
}
