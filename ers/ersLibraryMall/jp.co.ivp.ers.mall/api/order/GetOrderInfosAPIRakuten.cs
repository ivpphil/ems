﻿using System.Collections.Generic;
using System.Linq;

namespace jp.co.ivp.ers.mall.api.order
{
    /// <summary>
    /// 楽天市場モール伝票データ取得API [Get order infos API Rakuten]
    /// </summary>
    public class GetOrderInfosAPIRakuten : GetOrderInfosAPIBase
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public override string api_name
        {
            get
            {
                return "getRakutenOrderInfos";
            }
        }

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listApiErrorMessageRakuten = new List<string>()
        {
            "order search error.",      // 取得失敗エラー（遷移エラー） [Obtain failure error (Transition error)]
            "Search order list failed", // 取得失敗エラー（遷移エラー） [Obtain failure error (Transition error)]
            "Authentication Error."     // 認証エラー [Authentication error]
        };

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">GetOrderInfosAPIParamBase</param>
        public GetOrderInfosAPIRakuten(GetOrderInfosAPIParamBase param)
            : base(param)
        {
            this.listApiErrorMessage = listApiErrorMessageBase.Concat(listApiErrorMessageRakuten).ToList();
        }
    }
}
