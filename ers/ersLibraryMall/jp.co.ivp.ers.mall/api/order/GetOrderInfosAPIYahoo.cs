﻿using System.Collections.Generic;
using System.Linq;

namespace jp.co.ivp.ers.mall.api.order
{
    /// <summary>
    /// Yahooモール伝票データ取得API [Get order infos API Yahoo]
    /// </summary>
    public class GetOrderInfosAPIYahoo : GetOrderInfosAPIBase
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public override string api_name
        {
            get
            {
                return "getYahoosOrderInfos2";
            }
        }

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listApiErrorMessageYahoo = new List<string>()
        {
            "does not have permission to download a file.",     // 権限エラー（遷移エラー） [Authentication error (Transition error)]
            "Payment Manager not Found",                        // 権限エラー（遷移エラー） [Authentication error (Transition error)]
            "invalid file was downloaded",                      // 予期しないCSVフォーマット [Unexpected CSV file format]
            "login exception",                                  // ログインエラー [Login error]
            "Invalid Page."                                     // 遷移エラー [Transition error]
        };


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">GetOrderInfosAPIParamBase</param>
        public GetOrderInfosAPIYahoo(GetOrderInfosAPIParamBase param)
            : base(param)
        {
            this.listApiErrorMessage = listApiErrorMessageBase.Concat(listApiErrorMessageYahoo).ToList();
        }
    }
}
