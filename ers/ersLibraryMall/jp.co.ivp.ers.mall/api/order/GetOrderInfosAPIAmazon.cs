﻿using System.Collections.Generic;
using System.Linq;

namespace jp.co.ivp.ers.mall.api.order
{
    /// <summary>
    /// Amazonモール伝票データ取得API [Get order infos API Amazon]
    /// </summary>
    public class GetOrderInfosAPIAmazon : GetOrderInfosAPIBase
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public override string api_name
        {
            get
            {
                return "getAmazonOrderInfos2";
            }
        }

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listApiErrorMessageAmazon = new List<string>()
        {
            "The CreatedBefore date or the LastUpdatedBefore date is later than two minutes before the time that your request was submitted."   // 検索日時エラー [Search datetime error]
        };

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">GetOrderInfosAPIParamBase</param>
        public GetOrderInfosAPIAmazon(GetOrderInfosAPIParamBase param)
            : base(param)
        {
            this.listApiErrorMessage = listApiErrorMessageBase.Concat(listApiErrorMessageAmazon).ToList();
        }
    }
}
