﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.order
{
    /// <summary>
    /// モール伝票データ取得API [Get order infos API base]
    /// </summary>
    public class GetOrderInfosAPIBase
    {
        /// <summary>
        /// APIエラーリトライカウント [API retry count for error]
        /// </summary>
        public static int API_ERROR_RETRY_COUNT = 5;

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listApiErrorMessageBase = new List<string>()
        {
            "Login Failed.",        // ログイン失敗 [Login failed]
            "Analyze Failed.",      // 遷移失敗 [Analyze failed]
            "invalid account",      // アカウント無効 [Invalid account]
            "Internal Error",       // 内部エラー [Internal error]
            "request error",        // サーバ接続不可 [Doesn't connect to the server]
            "SQLSTATE[42P07]",      // テーブル重複（※暫定対応） [Duplicate table]
            "SQLSTATE[XX000]"       // 内部エラー（※暫定対応） [Internal error]
        };

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listApiErrorMessage { get; set; }


        /// <summary>
        /// API名 [API name]
        /// </summary>
        public virtual string api_name { get; set; }

        /// <summary>
        /// APIパラメタ [API param]
        /// </summary>
        public virtual GetOrderInfosAPIParamBase param { get; set; }

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">GetOrderInfosAPIParamBase</param>
        public GetOrderInfosAPIBase(GetOrderInfosAPIParamBase param)
        {
            this.param = param;

            this.listApiErrorMessage = listApiErrorMessageBase;
        }

        /// <summary>
        /// モール伝票データ取得 [Get order infos]
        /// </summary>
        /// <param name="request">HarcApiRequest</param>
        /// <param name="get_status">object</param>
        /// <returns>List<Dictionary<string, object>></returns>
        public virtual List<Dictionary<string, object>> GetOrderInfos(HarcApiRequest request, object get_status)
        {
            //パラメタ取得
            var apiParam = this.param.GetHarcApiParam(this.api_name, get_status);

            //strategy取得
            var convertStgy = ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy();

            int retry = 0;

            //APIリクエスト
            while (true)
            {
                try
                {
                    var jsonObj = request.simpleRequest(apiParam);
                    return convertStgy.ConvertImportOrderResult(jsonObj);
                }
                catch (HarcApiException e)
                {
                    // リトライ可能な場合は5回までリトライさせる
                    if (this.IsRetryAbleError(e.Message))
                    {
                        if (retry++ < API_ERROR_RETRY_COUNT)
                        {
                            continue;
                        }
                    }

                    throw new APIFailedException(e, apiParam);
                }
            }
        }

        /// <summary>
        /// リトライ可能なエラーかどうか [Whether error can retry]
        /// </summary>
        /// <param name="errorMessage">エラーメッセージ [Error message]</param>
        /// <returns>true : リトライ可能 [Can retry] / false : リトライ不可 [Can't retry]</returns>
        protected virtual bool IsRetryAbleError(string errorMessage)
        {
            foreach (var message in this.listApiErrorMessage)
            {
                if (errorMessage.Contains(message))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
