﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.order
{
    /// <summary>
    /// Yahoo!支店受注データ取得APIパラメタ [Get order infos API param Yahoo!]
    /// </summary>
    public class GetOrderInfosAPIParamYahoo
        : GetOrderInfosAPIParamBase
    {
        /// <summary>
        /// Yahoo!管理ツールタイプ [Type of management tool (Yahoo!)]
        /// </summary>
        public virtual EnumMallYahooManagementToolType? managementTool { get; set; }


        /// <summary>
        /// HARC API パラメタ取得 [Get HARC API param]
        /// </summary>
        /// <param name="api_name">string</param>
        /// <param name="get_status">object</param>
        /// <returns>HarcApiParam</returns>
        public override HarcApiParam GetHarcApiParam(string api_name, object get_status)
        {
            var apiParam = base.GetHarcApiParam(api_name, get_status);

            if (this.managementTool != null)
            {
                apiParam.Param.Add("managementTool", this.managementTool.ToString());
            }

            return apiParam;
        }
    }
}
