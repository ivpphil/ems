﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api
{
    /// <summary>
    /// APIパラメータ拡張 [Extension of API parameter]
    /// </summary>
    public static class APIParamExtension
    {
        /// <summary>
        /// 文字列化 [To string]
        /// </summary>
        /// <param name="param">パラメータ [Parameter]</param>
        /// <returns>文字列 [String]</returns>
        public static string ToStringParam(this HarcApiParam param)
        {
            return ToStringDicParam(param.Param, 0);
        }

        /// <summary>
        /// パディング取得 [Get padding]
        /// </summary>
        /// <param name="depth">深さ [Depth]</param>
        /// <returns>パディング [Padding]</returns>
        private static string GetPadding(int depth)
        {
            return depth > 0 ? string.Format("{0," + depth * 4 + "}", " ") : string.Empty;
        }

        /// <summary>
        /// ディクショナリ文字列化 [Dictionary to string]
        /// </summary>
        /// <param name="dicParam">ディクショナリ [Dictionary]</param>
        /// <param name="depth">深さ [Depth]</param>
        /// <returns>文字列 [String]</returns>
        public static string ToStringDicParam(IDictionary dicParam, int depth)
        {
            var ret = string.Empty;
            var pad = GetPadding(depth);

            depth++;

            foreach (var key in dicParam.Keys)
            {
                if (dicParam[key] is HarcDictionary)
                {
                    ret += string.Format("{0}{1} = \r\n{0}[\r\n{2}{0}]\r\n", pad, key, ToStringDicParam((HarcDictionary)(dicParam[key]), depth));
                }
                else if (dicParam[key] is ArrayList)
                {
                    ret += string.Format("{0}{1} = \r\n{0}[\r\n{2}\r\n{0}]\r\n", pad, key, ToStringArrayParam((ArrayList)(dicParam[key]), depth));
                }
                else
                {
                    ret += string.Format("{0}{1} = {2}\r\n", pad, key, Convert.ToString(dicParam[key]));
                }
            }

            return ret;
        }

        /// <summary>
        /// リスト文字列化 [List to string]
        /// </summary>
        /// <param name="listParam">リスト [List]</param>
        /// <param name="depth">深さ [Depth]</param>
        /// <returns>文字列 [String]</returns>
        public static string ToStringArrayParam(IList listParam, int depth)
        {
            var ret = string.Empty;
            var pad = GetPadding(depth);

            depth++;

            var listTmp = new List<string>();

            foreach (var value in listParam)
            {
                if (value is HarcDictionary)
                {
                    listTmp.Add(ToStringDicParam((HarcDictionary)(value), depth));
                }
                else if (value is ArrayList)
                {
                    listTmp.Add(ToStringArrayParam((ArrayList)(value), depth));
                }
                else
                {
                    listTmp.Add(value.ToString());
                }
            }

            return pad + String.Join(",\r\n" + pad, listTmp);
        }
    }
}
