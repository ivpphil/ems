﻿using System;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api
{
    public class APIFailedException 
        : Exception
    {
        /// <summary>
        /// HARC API 例外 [HARC API exception]
        /// </summary>
        public virtual HarcApiException harcApiException { get; protected set; }

        /// <summary>
        /// HARC API パラメータ [HARC API parameter]
        /// </summary>
        public virtual HarcApiParam harcApiParam { get; protected set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="message">string</param>
        public APIFailedException(string message)
            : base(message)
        { 
        }

        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="harcApiException">HARC API 例外 [HARC API exception]</param>
        /// <param name="harcApiParam">HARC API パラメータ [HARC API parameter]</param>
        public APIFailedException(HarcApiException harcApiException, HarcApiParam harcApiParam)
            : base(harcApiException.ToString())
        {
            this.harcApiException = harcApiException;
            this.harcApiParam = harcApiParam;
        }

        /// <summary>
        /// 文字列化 [To string]
        /// </summary>
        /// <returns>文字列 [String]</returns>
        public override string ToString()
        {
            if (this.harcApiParam != null)
            {
                return string.Format("param = \r\n{0}\r\n{1}", this.harcApiParam.ToStringParam(), this.harcApiException.ToString());
            }

            return this.harcApiException.ToString();
        }
    }
}
