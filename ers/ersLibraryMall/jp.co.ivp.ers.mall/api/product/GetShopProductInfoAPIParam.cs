﻿using System.Collections;
using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.product
{
    /// <summary>
    /// 店舗別商品情報取得APIパラメータ [API parameter for get product information of mall shops]
    /// </summary>
    public class GetShopProductInfoAPIParam
    {
        /// <summary>
        /// ショップID [Shop ID]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// HARC APIパラメータ取得 [Get the parameter for HARC API]
        /// </summary>
        /// <param name="api_name">API名 [API name]</param>
        /// <param name="listProductCode">商品コードリスト [List of product code]</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParam(string api_name, List<string> listProductCode)
        {
            var apiParam = new HarcApiParam(api_name);

            apiParam.Param.Add("shopId", this.shop_id);
            apiParam.Param.Add("productCodes", new ArrayList(listProductCode));

            return apiParam;
        }
    }
}
