﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.product
{
    /// <summary>
    /// 店舗別商品情報更新API [API for update product information of mall shops]
    /// </summary>
    public class UpdateShopProductInfoAPI
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public virtual string api_name
        {
            get
            {
                return "updateShopProductInfo";
            }
        }

        /// <summary>
        /// APIパラメータ [API parameter]
        /// </summary>
        public virtual UpdateShopProductInfoAPIParam param { get; set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        public UpdateShopProductInfoAPI(UpdateShopProductInfoAPIParam param)
        {
            this.param = param;
        }

        /// <summary>
        /// 店舗別商品情報更新 [Update product information of mall shops]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Request object of HARC]</param>
        /// <param name="products">商品情報リスト [List of product information]</param>
        /// <returns>処理結果 [Result]</returns>
        public virtual Dictionary<string, List<object>> UpdateShopProductInfo(HarcApiRequest request, IList<UpdateShopProductInfoParam> products)
        {
            // APIパラメータ取得 [Get the parameter for API]
            var paramApi = this.param.GetHarcApiParam(this.api_name, products);

            try
            {
                // APIリクエスト [API request]
                var objJson = request.simpleRequest(paramApi);

                return ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy().ConvertUpdateShopProductInfoResult(objJson);
            }
            catch (HarcApiException e)
            {
                throw new APIFailedException(e, paramApi);
            }
        }
    }
}
