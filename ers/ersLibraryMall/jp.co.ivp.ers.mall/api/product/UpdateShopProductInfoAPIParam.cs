﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.product
{
    /// <summary>
    /// 店舗別商品情報更新APIパラメータ [API parameter for update product information of mall shops]
    /// </summary>
    public class UpdateShopProductInfoAPIParam
    {
        /// <summary>
        /// ショップID [Shop ID]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// HARC APIパラメータ取得 [Get the parameter for HARC API]
        /// </summary>
        /// <param name="api_name">API名 [API name]</param>
        /// <param name="products">商品情報リスト [List of product information]</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParam(string api_name, IList<UpdateShopProductInfoParam> products)
        {
            var apiParam = new HarcApiParam(api_name);
            var paramDic = new HarcDictionary();

            paramDic.Add("shopId", this.shop_id);
            paramDic.Add("posting", true);

            foreach (var product in products)
            {
                var dicParam = new HarcDictionary();

                dicParam.Add("productCode", product.productCode);
                dicParam.Add("salePrice", product.salePrice);

                paramDic.AddArray("products", dicParam);
            }

            apiParam.Param = paramDic;

            return apiParam;
        }
    }

    /// <summary>
    /// 店舗別商品情報更新API用商品情報 [Product information for Update product information of mall shops API]
    /// </summary>
    public struct UpdateShopProductInfoParam
    {
        /// <summary>
        /// 商品コード [Product code]
        /// </summary>
        public string productCode { get; set; }

        /// <summary>
        /// 価格 [Price]
        /// </summary>
        public int? salePrice { get; set; }
    }
}
