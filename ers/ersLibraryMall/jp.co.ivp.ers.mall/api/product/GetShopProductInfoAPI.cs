﻿using System.Collections.Generic;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.product
{
    /// <summary>
    /// 店舗別商品情報取得API [API for update product information of mall shops]
    /// </summary>
    public class GetShopProductInfoAPI
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public virtual string api_name
        {
            get
            {
                return "getShopProductInfo";
            }
        }

        /// <summary>
        /// APIパラメータ [API parameter]
        /// </summary>
        public virtual GetShopProductInfoAPIParam param { get; set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        public GetShopProductInfoAPI(GetShopProductInfoAPIParam param)
        {
            this.param = param;
        }

        /// <summary>
        /// 店舗別商品情報更新 [Update product information of mall shops]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Request object of HARC]</param>
        /// <param name="listProductCode">商品コードリスト [List of product code]</param>
        /// <returns>処理結果 [Result]</returns>
        public virtual List<Dictionary<string, object>> GetShopProductInfo(HarcApiRequest request, List<string> listProductCode)
        {
            // APIパラメータ取得 [Get the parameter for API]
            var paramApi = this.param.GetHarcApiParam(this.api_name, listProductCode);

            try
            {
                // APIリクエスト [API request]
                var objJson = request.simpleRequest(paramApi);

                return ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy().ConvertGetShopProductInfoResult(objJson);
            }
            catch (HarcApiException e)
            {
                throw new APIFailedException(e, paramApi);
            }
        }
    }
}
