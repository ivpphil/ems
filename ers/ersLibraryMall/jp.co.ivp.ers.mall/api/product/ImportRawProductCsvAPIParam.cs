﻿using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.product
{
    /// <summary>
    /// 商品CSV登録APIパラメータ [API parameter for import product csv]
    /// </summary>
    public class ImportRawProductCsvAPIParam
    {
        /// <summary>
        /// ショップID [Shop ID]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// HARC APIパラメータ取得 [Get the parameter for HARC API]
        /// </summary>
        /// <param name="api_name">API名 [API name]</param>
        /// <param name="file_id">ファイルID [File id]</param>
        /// <returns>HarcApiParam</returns>
        public virtual HarcApiParam GetHarcApiParam(string api_name, string file_id)
        {
            var apiParam = new HarcApiParam(api_name);
            var paramDic = new HarcDictionary();

            paramDic.Add("shopId", this.shop_id);
            paramDic.Add("fileId", file_id);

            apiParam.Param = paramDic;

            return apiParam;
        }
    }
}
