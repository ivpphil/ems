﻿using System.Collections.Generic;
using System.IO;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.api.product
{
    /// <summary>
    /// 商品CSV登録API [API for import product csv]
    /// </summary>
    public class ImportRawProductCsvAPI
    {
        /// <summary>
        /// API名 [API name]
        /// </summary>
        public virtual string api_name
        {
            get
            {
                return "importRawProductCsv";
            }
        }

        /// <summary>
        /// APIパラメータ [API parameter]
        /// </summary>
        public virtual ImportRawProductCsvAPIParam param { get; set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="param">APIパラメータ [API parameter]</param>
        public ImportRawProductCsvAPI(ImportRawProductCsvAPIParam param)
        {
            this.param = param;
        }

        /// <summary>
        /// 登録 [Import (Register)]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Request object of HARC]</param>
        /// <param name="string">ファイルパス [File path]</param>
        /// <returns>処理結果 [Result]</returns>
        public virtual Dictionary<string, Dictionary<string, object>> Import(HarcApiRequest request, string path)
        {
            HarcApiParam paramApi = null;

            try
            {
                // ファイルアップロードリクエスト [Request for upload file]
                HarcApiResponse response = request.uploadFile(new FileInfo(path));

                // APIパラメータ取得 [Get the parameter for API]
                paramApi = this.param.GetHarcApiParam(this.api_name, response.Id);

                // APIリクエスト [API request]
                var objJson = request.simpleRequest(paramApi);

                return ErsMallFactory.ersMallCommonFactory.GetConvertJsonResultStgy().ConvertImportRawProductCsvResult(objJson);
            }
            catch (HarcApiException e)
            {
                throw new APIFailedException(e, paramApi);
            }
        }
    }
}
