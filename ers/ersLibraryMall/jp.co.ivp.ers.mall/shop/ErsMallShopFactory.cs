﻿using jp.co.ivp.ers.mall.shop.strategy;

namespace jp.co.ivp.ers.mall.shop
{
    /// <summary>
    /// モール店舗情報関連ファクトリ [Factory for shop information of mall]
    /// </summary>
    public class ErsMallShopFactory
    {
        /// <summary>
        /// 店舗情報追加クラス取得 [Get the class of Add shop information]
        /// </summary>
        /// <returns>Instance of AddShopInfoStgy</returns>
        public virtual AddShopInfoStgy GetAddShopInfoStgy()
        {
            return new AddShopInfoStgy();
        }

        /// <summary>
        /// 店舗情報更新クラス取得 [Get the class of update shop information]
        /// </summary>
        /// <returns>Instance of UpdateShopInfoStgy</returns>
        public virtual UpdateShopInfoStgy GetUpdateShopInfoStgy()
        {
            return new UpdateShopInfoStgy();
        }
    }
}
