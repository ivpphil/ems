﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.api;
using jp.co.ivp.ers.mall.api.shop;

namespace jp.co.ivp.ers.mall.shop.strategy
{
    /// <summary>
    /// 店舗情報更新 [Update shop information]
    /// </summary>
    public class UpdateShopInfoStgy
    {
        /// <summary>
        /// 店舗情報更新 [Update shop information]
        /// </summary>
        /// <param name="param">パラメータ [Parameter]</param>
        /// <returns>店舗ID [Shop id]</returns>
        public virtual int? UpdateShopInfo(UpdateShopInfoParam param)
        {
            var paramApi = ErsMallFactory.ersMallAPIFactory.GetUpdateShopInfoAPIParam();

            // HARCログイン [Log in to HARC]
            var request = ErsMallFactory.ersMallCommonFactory.GetHarcLoginStgy().HarcLogin();

            try
            {
                // 店舗情報更新 [Update shop information]
                var dicResult = ErsMallFactory.ersMallAPIFactory.GetUpdateShopInfoAPI(paramApi).UpdateShopInfo(request, param);

                if (!dicResult.ContainsKey("shopId"))
                {
                    throw new Exception(ErsResources.GetMessage("102103", "Not get shopId."));
                }

                return Convert.ToInt32(dicResult["shopId"]);
            }
            catch (APIFailedException e)
            {
                throw new Exception(ErsResources.GetMessage("102103", e.ToString()));
            }
        }
    }
}
