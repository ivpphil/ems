﻿
namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// クレジットカード決済操作 [Type of operation for credit card payment]
    /// </summary>
    public enum EnumMallCardPaymentOperation
    {
        /// <summary>
        /// 1 : オーソリ [Authorization]
        /// </summary>
        Auth = 1,

        /// <summary>
        /// 2 : オーソリキャンセル [Cancel authorization]
        /// </summary>
        CancelAuth = 2,

        /// <summary>
        /// 3 : 売上確定 [Sales]
        /// </summary>
        Sales = 3,

        /// <summary>
        /// 4 : 売上キャンセル [Cancel sales]
        /// </summary>
        CancelSales = 4,

        /// <summary>
        /// 5 : 再オーソリ [Reauthorization]
        /// </summary>
        Reauth = 5,

        /// <summary>
        /// 100 : オーソリ＋売上確定 [Authorization + Sales]
        /// </summary>
        AuthAndSales = 100,
    }
}
