﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// モール商品操作タイプ [Operation type for mall product]
    /// </summary>
    public enum EnumMallProductOperationType
    {
        /// <summary>
        /// 0 : 登録 [Regist]
        /// </summary>
        REGIST = 0,

        /// <summary>
        /// 1 : 更新 [Update]
        /// </summary>
        UPDATE = 1,

        /// <summary>
        /// 2 : 削除 [Delete]
        /// </summary>
        DELETE = 2,
    }
}
