﻿
namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// Yahoo!クレジットカード決済取消種別 [Type of cancel payment for credit card payment (Yahoo!)]
    /// </summary>
    public enum EnumMallYahooCardPaymentCancelType
    {
        /// <summary>
        /// 1 : 売上取消 [Cancel payment]
        /// </summary>
        CancelPayment = 1,

        /// <summary>
        /// 2 : オーソリキャンセル [Cancel authorization]
        /// </summary>
        CancelAuth = 2,
    }
}
