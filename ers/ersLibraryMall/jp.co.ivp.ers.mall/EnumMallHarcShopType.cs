﻿
namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// Enums for HARC shop type
    /// </summary>
    public enum EnumMallHarcShopType
    {
        /// <summary>
        /// 1 : 楽天 [Rakuten]
        /// </summary>
        RAKUTEN = 1,

        /// <summary>
        /// 117 : Yahoo! [Yahoo!]
        /// </summary>
        YAHOO = 117,

        /// <summary>
        /// 116 : Amazon [Amazon]
        /// </summary>
        AMAZON = 116,
    }
}
