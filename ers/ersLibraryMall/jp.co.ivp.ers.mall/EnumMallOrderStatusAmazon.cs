﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// モール伝票ステータス（Amazon） [Mall order status (Amazon)]
    /// </summary>
    public enum EnumMallOrderStatusAmazon
    {
        /// <summary>
        /// 1 : 出荷前 [Unshipped, PartiallyShipped]
        /// </summary>
        Unshipped = 1,

        /// <summary>
        /// 2 : 出荷済み [Shipped]
        /// </summary>
        Shipped = 2,

        /// <summary>
        /// -1 : キャンセル [Canceled]
        /// </summary>
        Canceled = -1,

        /// <summary>
        /// -2 : 保留中 [Pending]
        /// </summary>
        Pending = -2,

        /// <summary>
        /// -3 : 出荷不可 [Unfulfillable]
        /// </summary>
        Unfulfillable = -3
    }
}
