﻿
namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// Yahoo!クレジットカード決済オーソリタイプ [Type of authorize for credit card payment (Yahoo!)]
    /// </summary>
    public enum EnumMallYahooCardPaymentAuthorizeType
    {
        /// <summary>
        /// 1 : 変更 [Change]
        /// </summary>
        Change = 1,

        /// <summary>
        /// 2 : 追加 [Add]
        /// </summary>
        Add = 2,

        /// <summary>
        /// 3 : 取消追加 [Cancel and Add]
        /// </summary>
        CancelAndAdd = 3,
    }
}
