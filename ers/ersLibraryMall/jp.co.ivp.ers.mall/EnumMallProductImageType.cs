﻿
namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// Enums for type of mall product image
    /// </summary>
    public enum EnumMallProductImageType
    {
        /// <summary>
        /// 0 : なし [None]
        /// </summary>
        None = 0,

        /// <summary>
        /// 1 : SKU [SKU]
        /// </summary>
        Sku = 1,
    }
}
