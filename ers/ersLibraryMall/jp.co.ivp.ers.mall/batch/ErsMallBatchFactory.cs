﻿using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mall.product.amazon;
using jp.co.ivp.ers.mall.product.yahoo;
using jp.co.ivp.ers.mall.product.rakuten;

namespace jp.co.ivp.ers.mall.batch
{
    /// <summary>
    /// Factory
    /// </summary>
    public class ErsMallBatchFactory
    {
        protected static SetupMallBatch _setup
        {
            get
            {
                return (SetupMallBatch)ErsCommonContext.GetPooledObject("_setupBatchMall");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_setupBatchMall", value);
            }
        }

        /// <summary>
        /// 設定情報を格納したクラスを取得する（参照用）
        /// </summary>
        /// <returns></returns>
        public virtual SetupMallBatch getSetup()
        {
            if (_setup == null)
                _setup = new SetupMallBatch();

            return _setup;
        }

        public AmazonTsvMapperBase GetAmazonTsvMapper()
        {
            var setup = this.getSetup();
            return (AmazonTsvMapperBase)ErsReflection.CreateInstanceFromFullName(setup.AmazonTsvMapperClass);
        }

        public YahooTsvMapper GetYahooTsvMapper()
        {
            return new YahooTsvMapper();
        }

        public RakutenTsvMapper GetRakutenTsvMapper()
        {
            return new RakutenTsvMapper();
        }
    }
}
