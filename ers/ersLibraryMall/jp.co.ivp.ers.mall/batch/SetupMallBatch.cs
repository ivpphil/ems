﻿using jp.co.ivp.ers.batch;
using System;

namespace jp.co.ivp.ers.mall.batch
{
    public class SetupMallBatch
        : jp.co.ivp.ers.util.SetupConfigReader
    {
        #region "ImportMallOrder モール伝票取り込み"
        #region モール伝票取り込み実行フラグ [Do execute import mall order flag]
        /// <summary>
        /// 楽天 取込みフラグ(true:取込む/false:取込まない)
        /// </summary>
        public virtual bool doImportRakuten
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doImportRakuten"));
            }
        }

        /// <summary>
        /// Yahoo! 取込みフラグ(true:取込む/false:取込まない)
        /// </summary>
        public virtual bool doImportYahoo
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doImportYahoo"));
            }
        }

        /// <summary>
        /// Amazon 取込みフラグ(true:取込む/false:取込まない)
        /// </summary>
        public virtual bool doImportAmazon
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doImportAmazon"));
            }
        }
        #endregion

        /// <summary>
        /// Amazon在庫取り込み有効フラグ [Flag of import amazon stock]
        /// </summary>
        public virtual bool enableAmazonImportStock
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("enableAmazonImportStock"));
            }
        }
        #endregion

        #region "MergeMallOrder モール伝票マージ"
        /// <summary>
        /// アラートメール from
        /// </summary>
        public virtual string mergeMallOrderAlertMailFrom
        {
            get
            {
                return GetSiteConfigFileValue("mergeMallOrderAlertMailFrom");
            }
        }

        /// <summary>
        /// アラートメール to
        /// </summary>
        public virtual string mergeMallOrderAlertMailTo
        {
            get
            {
                return GetSiteConfigFileValue("mergeMallOrderAlertMailTo");
            }
        }

        /// <summary>
        /// アラートメール cc
        /// </summary>
        public virtual string mergeMallOrderAlertMailCc
        {
            get
            {
                return GetSiteConfigFileValue("mergeMallOrderAlertMailCc");
            }
        }

        /// <summary>
        /// アラートメール bcc
        /// </summary>
        public virtual string mergeMallOrderAlertMailBcc
        {
            get
            {
                return GetSiteConfigFileValue("mergeMallOrderAlertMailBcc");
            }
        }

        /// <summary>
        /// アラートメールタイトル
        /// </summary>
        public virtual string mergeMallOrderAlertMailTitle
        {
            get
            {
                return GetSiteConfigFileValue("mergeMallOrderAlertMailTitle");
            }
        }
        #endregion

        #region "OperateMallProduct モール商品操作"
        /// <summary>
        /// 商品CSVテンポラリ出力パス [Output path for mall product CSV temporary]
        /// </summary>
        public virtual string mallProductCsvOutputPath
        {
            get
            {
                return GetSiteConfigFileValue("mallProductCsvOutputPath");
            }
        }
        /// <summary>
        /// IVP商品カテゴリパス [Yahoo product category path]
        /// </summary>
        public virtual string ivpProductCategoryPath
        {
            get
            {
                return GetSiteConfigFileValue("ivpProductCategoryPath");
            }
        }

        #region モール商品操作実行フラグ [Do execute operate mall product flag]
        /// <summary>
        /// 商品操作フラグ（楽天） [Operate mall product (Rakuten)]
        /// </summary>
        public virtual bool doOperateProductRakuten
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doOperateProductRakuten"));
            }
        }

        /// <summary>
        /// 商品操作フラグ（Yahoo!） [Operate mall product (Yahoo!)]
        /// </summary>
        public virtual bool doOperateProductYahoo
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doOperateProductYahoo"));
            }
        }

        /// <summary>
        /// 商品操作フラグ（Amazon） [Operate mall product (Amazon)]
        /// </summary>
        public virtual bool doOperateProductAmazon
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doOperateProductAmazon"));
            }
        }
        #endregion

        #region 楽天 [Rakuten]
        /// <summary>
        /// 楽天画像URLフォーマット [Rakuten Image url format]
        /// <para>0 : ユーザID [User ID]</para>
        /// </summary>
        public virtual string rakutenImageUrlFormat
        {
            get
            {
                return GetSiteConfigFileValue("rakutenImageUrlFormat");
            }
        }
        #endregion

        #region Amazon [Amazon]
        /// <summary>
        /// AmazonTSVマッパークラス名 [Amazon TSV mapper class name]
        /// </summary>
        public string AmazonTsvMapperClass
        {
            get
            {
                return GetSiteConfigFileValue("AmazonTsvMapperClass");
            }
        }
        #endregion
        #endregion

        #region "OperateMallProductImage モール商品画像操作"
        /// <summary>
        /// 商品画像バックアップパス [Backup path for mall product image]
        /// </summary>
        public virtual string mallProductImageBackupPath
        {
            get
            {
                return GetSiteConfigFileValue("mallProductImageBackupPath");
            }
        }

        /// <summary>
        /// 商品画像テンポラリパス [Temporary path for mall product image]
        /// </summary>
        public virtual string mallProductImageTempPath
        {
            get
            {
                return GetSiteConfigFileValue("mallProductImageTempPath");
            }
        }

        #region モール商品画像操作実行フラグ [Do execute operate mall product image flag]
        /// <summary>
        /// 商品画像操作フラグ（楽天） [Operate mall product image (Rakuten)]
        /// </summary>
        public virtual bool doOperateProductImageRakuten
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doOperateProductImageRakuten"));
            }
        }

        /// <summary>
        /// 商品画像操作フラグ（Yahoo!） [Operate mall product image (Yahoo!)]
        /// </summary>
        public virtual bool doOperateProductImageYahoo
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doOperateProductImageYahoo"));
            }
        }

        /// <summary>
        /// 商品画像操作フラグ（Amazon） [Operate mall product image (Amazon)]
        /// </summary>
        public virtual bool doOperateProductImageAmazon
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doOperateProductImageAmazon"));
            }
        }
        #endregion
        #endregion

        #region "UploadMallProductFile モール商品ファイルアップロード"
        #region モール商品ファイルアップロード実行フラグ [Do execute upload mall product file flag]
        /// <summary>
        /// 商品アップロードフラグ（楽天） [Upload mall product file (Rakuten)]
        /// </summary>
        public virtual bool doUploadProductFileRakuten
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doUploadProductFileRakuten"));
            }
        }

        /// <summary>
        /// 商品アップロードフラグ（Yahoo!） [Upload mall product file (Yahoo!)]
        /// </summary>
        public virtual bool doUploadProductFileYahoo
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doUploadProductFileYahoo"));
            }
        }

        /// <summary>
        /// 商品アップロードフラグ（Amazon） [Upload mall product file (Amazon)]
        /// </summary>
        public virtual bool doUploadProductFileAmazon
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doUploadProductFileAmazon"));
            }
        }
        #endregion

        #region Amazon [Amazon]
        /// <summary>
        /// Amazonアプリケーション名 [Amazon application name]
        /// </summary>
        public virtual string amazonMWSApplicationName
        {
            get
            {
                return GetSiteConfigFileValue("amazonMWSApplicationName");
            }
        }

        /// <summary>
        /// Amazonアプリケーションバージョン [Amazon application version]
        /// </summary>
        public virtual string amazonMWSApplicationVersion
        {
            get
            {
                return GetSiteConfigFileValue("amazonMWSApplicationVersion");
            }
        }

        /// <summary>
        /// AmazonサービスURL [Amazon service URL]
        /// </summary>
        public virtual string amazonMWSServiceURL
        {
            get
            {
                return GetSiteConfigFileValue("amazonMWSServiceURL");
            }
        }
        #endregion
        #endregion

        #region "UploadMallProductImageFile モール商品画像ファイルアップロード"
        #region モール商品画像ファイルアップロード実行フラグ [Do execute upload mall product image file flag]
        /// <summary>
        /// 商品画像アップロードフラグ（楽天） [Upload mall product image file (Rakuten)]
        /// </summary>
        public virtual bool doUploadProductImageFileRakuten
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doUploadProductImageFileRakuten"));
            }
        }

        /// <summary>
        /// 商品画像アップロードフラグ（Yahoo!） [Upload mall product image file (Yahoo!)]
        /// </summary>
        public virtual bool doUploadProductImageFileYahoo
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("doUploadProductImageFileYahoo"));
            }
        }
        #endregion

        #region 楽天 [Rakuten]
        /// <summary>
        /// 楽天FTP画像ディレクトリ [Rakuten FTP Image directory]
        /// </summary>
        public virtual string rakutenFTPImageDir
        {
            get
            {
                return GetSiteConfigFileValue("rakutenFTPImageDir");
            }
        }
        #endregion
        #endregion

        #region "MonitorUpdateMallStockHistory モール在庫更新履歴監視"
        /// <summary>
        /// 処理中監視経過時間（分） [Monitor elapsed processing time (Minutes)]
        /// </summary>
        public virtual int? monitorUpdateMallStockHistoryElapsedProcessingMin
        {
            get
            {
                return Convert.ToInt32(GetSiteConfigFileValue("monitorUpdateMallStockHistoryElapsedProcessingMin"));
            }
        }
        #endregion
    }
}
