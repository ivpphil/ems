﻿using jp.co.ivp.ers.batch;

namespace jp.co.ivp.ers.mall.batch
{
    /// <summary>
    /// 
    /// </summary>
    public class ErsMallBatchEnvironment
        : IErsBatchEnvironment
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationRootPath"></param>
        public void Initialization(string applicationRootPath)
        {
            SetFactory();
        }

        /// <summary>
        /// ファクトリーセット
        /// </summary>
        protected void SetFactory()
        {
            ErsMallFactory.ersSiteFactory = new jp.co.ivp.ers.mall.site.ErsSiteFactory();

            ErsMallFactory.ersMallBatchFactory = new ErsMallBatchFactory();

            ErsMallFactory.ersMallUtilityFactory = new jp.co.ivp.ers.mall.util.ErsMallUtilityFactory();

            ErsMallFactory.ersMallShopFactory = new jp.co.ivp.ers.mall.shop.ErsMallShopFactory();

            ErsMallFactory.ersMallMailFactory = new jp.co.ivp.ers.mall.sendmail.ErsMallMailFactory();

            ErsMallFactory.ersMallViewServiceFactory = new jp.co.ivp.ers.mall.viewService.ErsMallViewServiceFactory();

            ErsMallFactory.ersMallAmazonFactory = new jp.co.ivp.ers.mall.amazon.ErsMallAmazonFactory();

            ErsMallFactory.ersMallAPIFactory = new jp.co.ivp.ers.mall.api.ErsMallAPIFactory();

            ErsMallFactory.ersMallCommonFactory = new jp.co.ivp.ers.mall.common.ErsMallCommonFactory();

            ErsMallFactory.ersMallStopTimeFactory = new jp.co.ivp.ers.mall.stop_time.ErsMallStopTimeFactory();

            ErsMallFactory.ersMallOrderFactory = new jp.co.ivp.ers.mall.mall_order.ErsMallOrderFactory();

            ErsMallFactory.ersMallProductFactory = new jp.co.ivp.ers.mall.product.ErsMallProductFactory();

            ErsMallFactory.ersMallStockErrorFactory = new jp.co.ivp.ers.mall.stock_error.ErsMallStockErrorFactory();

            ErsMallFactory.ersMallStockRecoveryFactory = new jp.co.ivp.ers.mall.stock_recovery.ErsMallStockRecoveryFactory();

            ErsMallFactory.ersMallStockFactory = new jp.co.ivp.ers.mall.stock.ErsMallStockFactory();
        }
    }
}
