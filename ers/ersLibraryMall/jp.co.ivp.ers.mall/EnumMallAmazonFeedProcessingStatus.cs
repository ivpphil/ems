﻿
namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// Enums for amazon feed processing status
    /// </summary>
    public enum EnumMallAmazonFeedProcessingStatus
    {
        /// <summary>
        /// リクエストは処理中ですが、完了前の外部情報を待っています。
        /// </summary>
        _AWAITING_ASYNCHRONOUS_REPLY_,

        /// <summary>
        /// 致命的なエラーのため、リクエストが中断されました。
        /// </summary>
        _CANCELLED_,

        /// <summary>
        /// リクエストの処理が完了しました。フィードで成功したレコード、エラーになったレコードを確認する処理レポートを取得するために、GetFeedSubmissionResult オペレーションを呼び出すことができます。
        /// </summary>
        _DONE_,

        /// <summary>
        /// リクエストは処理中です。
        /// </summary>
        _IN_PROGRESS_,

        /// <summary>
        /// リクエストは処理中ですが、システムがフィードにエラーの可能性(リクエストが出品用アカウントから全在庫を削除してしまうなど)があると判断しました。
        /// </summary>
        _IN_SAFETY_NET_,

        /// <summary>
        /// リクエストを受け付けましたが、処理は開始されていません。
        /// </summary>
        _SUBMITTED_,

        /// <summary>
        /// リクエストは保留中です。
        /// </summary>
        _UNCONFIRMED_
    }
}
