﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// Enums for import type
    /// </summary>
    public enum EnumImported
    {
        /// <summary>
        /// 0 : 取り込み未完 [Not imported]
        /// </summary>
        NotImported = 0,

        /// <summary>
        /// 1 : 取り込み完了 [Imported]
        /// </summary>
        Imported = 1,
    }
}
