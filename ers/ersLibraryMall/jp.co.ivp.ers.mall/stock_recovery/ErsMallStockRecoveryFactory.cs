﻿using System.Collections.Generic;
using jp.co.ivp.ers.mall.stock_error.strategy;

namespace jp.co.ivp.ers.mall.stock_recovery
{
    /// <summary>
    /// モール在庫リカバリ関連ファクトリ [Factory for stock recovery of mall]
    /// </summary>
    public class ErsMallStockRecoveryFactory
    {
        /// <summary>
        /// モール連携在庫リカバリエンティティ取得 [Get mall stock recovery]
        /// </summary>
        /// <returns>ErsMallStockRecovery</returns>
        public virtual ErsMallStockRecovery GetErsMallStockRecovery()
        {
            return new ErsMallStockRecovery();
        }

        /// <summary>
        /// モール連携在庫リカバリリポジトリ取得 [Get mall stock recovery repository]
        /// </summary>
        /// <returns>ErsMallStockRecoveryRepository</returns>
        public virtual ErsMallStockRecoveryRepository GetErsMallStockRecoveryRepository()
        {
            return new ErsMallStockRecoveryRepository();
        }

        /// <summary>
        /// モール連携在庫リカバリクライテリア取得 [Get mall stock recovery criteria]
        /// </summary>
        /// <returns>ErsMallStockRecoveryCriteria</returns>
        public virtual ErsMallStockRecoveryCriteria GetErsMallStockRecoveryCriteria()
        {
            return new ErsMallStockRecoveryCriteria();
        }

        /// <summary>
        /// モール連携在庫リカバリエンティティ取得（パラメータから） [Get mall stock recovery entity (from parameters)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsMallStockRecovery</returns>
        public virtual ErsMallStockRecovery GetErsMallStockRecoveryWithParameters(Dictionary<string, object> dicParams)
        {
            var obj = this.GetErsMallStockRecovery();
            obj.OverwriteWithParameter(dicParams);
            return obj;
        }

        /// <summary>
        /// モール連携在庫リカバリエンティティ取得（IDから） [Get mall stock recovery entity (from ID)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsMallStockRecovery</returns>
        public virtual ErsMallStockRecovery GetErsMallStockRecoveryWithID(int id)
        {
            var repository = this.GetErsMallStockRecoveryRepository();

            var criteria = this.GetErsMallStockRecoveryCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            return list.Count > 0 ? list[0] : null;
        }


        /// <summary>
        /// モール連携在庫リカバリ登録クラス取得 [Get the class of Register mall stock recovery]
        /// </summary>
        /// <returns>モール連携在庫リカバリ登録クラスインスタンス [Instance for RegisterMallStockRecoveryStgy]</returns>
        public virtual RegisterMallStockRecoveryStgy GetRegisterMallStockRecoveryStgy()
        {
            return new RegisterMallStockRecoveryStgy();
        }
    }
}
