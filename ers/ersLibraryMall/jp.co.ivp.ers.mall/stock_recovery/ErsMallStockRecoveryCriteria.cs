﻿using System;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.stock_recovery
{
    /// <summary>
    /// モール連携在庫リカバリクライテリア [Criteria for mall stock recovery table]
    /// </summary>
    public class ErsMallStockRecoveryCriteria
        : Criteria
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_stock_recovery_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// アクティブ [Active]
        /// </summary>
        public virtual EnumActive? active
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_stock_recovery_t.active", Convert.ToInt32(value), Operation.EQUAL));
            }
        }


        /// <summary>
        /// IDソート [Sort by ID]
        /// </summary> 
        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("mall_stock_recovery_t.id", orderBy);
        }
    }
}
