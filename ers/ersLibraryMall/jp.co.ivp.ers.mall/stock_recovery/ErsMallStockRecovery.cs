﻿using System;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.stock_recovery
{
    /// <summary>
    /// モール連携在庫リカバリエンティティ [Entity for mall stock recovery table]
    /// </summary>
    public class ErsMallStockRecovery
        : ErsRepositoryEntity
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 登録日時 [Datetime of register]
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新日時 [Datetime of update]
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// アクティブ [Active]
        /// </summary>
        public virtual EnumActive? active { get; set; }

        /// <summary>
        /// SKUコード [SKU code]
        /// </summary>
        public virtual string scode { get; set; }

        /// <summary>
        /// 在庫数 [Stock num]
        /// </summary>
        public virtual int? stock { get; set; }

        /// <summary>
        /// 在庫操作 [TMall stock operation]
        /// </summary>
        public virtual EnumMallStockOperation? operation { get; set; }
    }
}
