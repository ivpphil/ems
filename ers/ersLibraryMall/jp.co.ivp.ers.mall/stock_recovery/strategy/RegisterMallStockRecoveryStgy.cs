﻿using System.Collections.Generic;
using jp.co.ivp.ers.mall.api.stock;

namespace jp.co.ivp.ers.mall.stock_error.strategy
{
    /// <summary>
    /// モール連携在庫リカバリ登録 [Register mall stock recovery]
    /// </summary>
    public class RegisterMallStockRecoveryStgy
    {
        /// <summary>
        /// 登録 [Register]
        /// </summary>
        /// <param name="listParam">パラメータリスト [The list of parameter]</param>
        public void Register(IList<UpdateStockParam> listParam)
        {
            var repository = ErsMallFactory.ersMallStockRecoveryFactory.GetErsMallStockRecoveryRepository();
            var objMallStockRecovery = ErsMallFactory.ersMallStockRecoveryFactory.GetErsMallStockRecovery();

            foreach (var param in listParam)
            {
                objMallStockRecovery.scode = param.productCode;
                objMallStockRecovery.stock = param.quantity;
                objMallStockRecovery.operation = param.operation;

                repository.Insert(objMallStockRecovery);
            }
        }
    }
}