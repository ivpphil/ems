﻿using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.stock_recovery
{
    /// <summary>
    /// モール連携在庫リカバリリポジトリ [Repository for mall stock recovery table]
    /// </summary>
    public class ErsMallStockRecoveryRepository
        : ErsRepository<ErsMallStockRecovery>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public ErsMallStockRecoveryRepository()
            : base("mall_stock_recovery_t", ErsCommonsSetting.ersDatabaseFactory.GetNewErsDatabase())
        {
        }
    }
}
