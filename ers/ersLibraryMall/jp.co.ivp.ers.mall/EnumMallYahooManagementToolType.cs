﻿
namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// Yahoo!管理ツールタイプ [Type of management tool (Yahoo!)]
    /// </summary>
    public enum EnumMallYahooManagementToolType
    {
        /// <summary>
        /// 1 : ストアマネージャ [Store manager]
        /// </summary>
        StoreManager = 1,

        /// <summary>
        /// 2 : ストアクリエイターPro [Store creator pro]
        /// </summary>
        StoreCreatorPro = 2,
    }
}
