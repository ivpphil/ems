﻿using MarketplaceWebService;

namespace jp.co.ivp.ers.mall.amazon.strategy
{
    /// <summary>
    /// AmazonMWSサービスクライアント取得 [Get the client of Amazon MWS service]
    /// </summary>
    public class ObtainMWSSeriviceClientStgy
    {
        /// <summary>
        /// AmazonMWSサービスクライアント取得 [Get the client of Amazon MWS service]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <returns>AmazonMWSサービスクライアント [The client of Amazon MWS service]</returns>
        public virtual AmazonMwsServiceClient Obtain(int? siteId)
        {
            var ret = default(AmazonMwsServiceClient);
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();

            ret.merchantId = mallSetup.GetAmazonMerchantId(siteId.Value);
            ret.marketplaceId = mallSetup.GetAmazonMarketplaceId(siteId.Value);

            MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();
            MarketplaceWebServiceClient service = new MarketplaceWebServiceClient(
                mallSetup.GetAmazonAccesskeyId(siteId.Value),
                mallSetup.GetAmazonSecretAccesskey(siteId.Value),
                setup.amazonMWSApplicationName,
                setup.amazonMWSApplicationVersion,
                config);

            config.ServiceURL = setup.amazonMWSServiceURL;
            config.SetUserAgentHeader(setup.amazonMWSApplicationName, setup.amazonMWSApplicationVersion, "C#");

            ret.serviceClient = service;

            return ret;
        }
    }

    /// <summary>
    /// AmazonMWSサービスクライアント [The client of Amazon MWS service]
    /// </summary>
    public struct AmazonMwsServiceClient
    {
        /// <summary>
        /// 出品者ID [Merchant id]
        /// </summary>
        public string merchantId { get; set; }

        /// <summary>
        /// マーケットプレイスID [Marketplace id]
        /// </summary>
        public string marketplaceId { get; set; }

        /// <summary>
        /// サービスクライアント [The client of service]
        /// </summary>
        public MarketplaceWebServiceClient serviceClient { get; set; }
    }
}
