﻿using jp.co.ivp.ers.mall.amazon.strategy;

namespace jp.co.ivp.ers.mall.amazon
{
    /// <summary>
    /// Amazon関連ファクトリ [Factory for Amazon]
    /// </summary>
    public class ErsMallAmazonFactory
    {
        /// <summary>
        /// SubmitFeed取得 [Get SubmitFeed]
        /// </summary>
        /// <returns>SubmitFeed</returns>
        public SubmitFeed GetSubmitFeed()
        {
            return new SubmitFeed();
        }

        /// <summary>
        /// GetFeedSubmissionList取得 [Get GetFeedSubmissionList]
        /// </summary>
        /// <returns>GetFeedSubmissionList</returns>
        public GetFeedSubmissionList GetGetFeedSubmissionList()
        {
            return new GetFeedSubmissionList();
        }

        /// <summary>
        /// GetFeedSubmissionListByNextToken取得 [Get GetFeedSubmissionListByNextToken]
        /// </summary>
        /// <returns>GetFeedSubmissionListByNextToken</returns>
        public GetFeedSubmissionListByNextToken GetGetFeedSubmissionListByNextToken()
        {
            return new GetFeedSubmissionListByNextToken();
        }


        /// <summary>
        /// AmazonMWSサービスクライアント取得 [Get the client of Amazon MWS service]
        /// </summary>
        /// <returns>ObtainMWSSeriviceClientStgy</returns>
        public ObtainMWSSeriviceClientStgy GetObtainMWSSeriviceClientStgy()
        {
            return new ObtainMWSSeriviceClientStgy();
        }
    }
}
