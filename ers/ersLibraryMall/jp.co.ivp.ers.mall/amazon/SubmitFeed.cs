﻿using System.Collections.Generic;
using System.IO;
using jp.co.ivp.ers.mall.amazon.strategy;
using MarketplaceWebService;
using MarketplaceWebService.Model;

namespace jp.co.ivp.ers.mall.amazon
{
    /// <summary>
    /// SubmitFeed [SubmitFeed]
    /// </summary>
    public class SubmitFeed
    {
        /// <summary>
        /// リクエスト [Request]
        /// </summary>
        /// <param name="service">AmazonMWSサービスクライアント [The client of Amazon MWS service]</param>
        /// <param name="stream">コンテンツストリーム [Contents stream]</param>
        /// <param name="feedType">フィードタイプ [Feed type]</param>
        /// <returns>サブミッションID [Submission id]</returns>
        public virtual string Request(AmazonMwsServiceClient service, Stream stream, EnumMallAmazonFeedType feedType)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            // リクエスト変数設定
            SubmitFeedRequest request = new SubmitFeedRequest();

            // 引数設定
            request.Merchant = service.merchantId;
            request.MarketplaceIdList = new IdList();
            request.MarketplaceIdList.Id = new List<string>(new string[] { service.marketplaceId });

            // アップロードファイル設定
            request.FeedContent = stream;

            // MD5変換
            request.ContentMD5 = MarketplaceWebServiceClient.CalculateContentMD5(request.FeedContent);
            request.FeedContent.Position = 0;

            // Feed Type 設定
            request.FeedType = feedType.ToString();

            // SubmitFeed 実行
            SubmitFeedResponse response = service.serviceClient.SubmitFeed(request);

            // 戻り値チェック
            if (response.IsSetSubmitFeedResult())
            {
                SubmitFeedResult submitFeedResult = response.SubmitFeedResult;

                if (submitFeedResult.IsSetFeedSubmissionInfo())
                {
                    FeedSubmissionInfo feedSubmissionInfo = submitFeedResult.FeedSubmissionInfo;

                    if (feedSubmissionInfo.IsSetFeedSubmissionId())
                    {
                        // FeedSubmission IDを保存し、処理状況チェックで利用する。
                        return feedSubmissionInfo.FeedSubmissionId;
                    }
                }
            }

            return null;
        }
    }
}
