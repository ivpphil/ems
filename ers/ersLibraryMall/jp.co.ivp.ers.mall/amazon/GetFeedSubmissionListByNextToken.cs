﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.amazon.strategy;
using MarketplaceWebService;
using MarketplaceWebService.Model;

namespace jp.co.ivp.ers.mall.amazon
{
    /// <summary>
    /// GetFeedSubmissionListByNextToken [GetFeedSubmissionListByNextToken]
    /// </summary>
    public class GetFeedSubmissionListByNextToken
    {
        /// <summary>
        /// リクエスト [Request]
        /// </summary>
        /// <param name="service">MWSサービス [MWS service]</param>
        /// <param name="nextToken">トークン [Token]</param>
        /// <returns>処理ステータス [Processing status]</returns>
        public virtual EnumMallAmazonFeedProcessingStatus? Request(AmazonMwsServiceClient service, string nextToken)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            // リクエスト変数設定
            GetFeedSubmissionListByNextTokenRequest request = new GetFeedSubmissionListByNextTokenRequest();

            // 引数設定
            request.Merchant = service.merchantId;

            // トークン設定
            request.NextToken = nextToken;

            // Submission Next 結果取得
            GetFeedSubmissionListByNextTokenResponse response = service.serviceClient.GetFeedSubmissionListByNextToken(request);

            if (response.IsSetGetFeedSubmissionListByNextTokenResult())
            {
                GetFeedSubmissionListByNextTokenResult getFeedSubmissionListByNextTokenResult = response.GetFeedSubmissionListByNextTokenResult;

                List<FeedSubmissionInfo> feedSubmissionInfoList = getFeedSubmissionListByNextTokenResult.FeedSubmissionInfo;

                foreach (FeedSubmissionInfo feedSubmissionInfo in feedSubmissionInfoList)
                {
                    if (feedSubmissionInfo.IsSetFeedProcessingStatus())
                    {
                        EnumMallAmazonFeedProcessingStatus result;

                        if (Enum.TryParse(feedSubmissionInfo.FeedProcessingStatus, out result))
                        {
                            return result;
                        }
                        else
                        {
                            throw new Exception(string.Format("Unexpected processing status. status = {0}", feedSubmissionInfo.FeedProcessingStatus));
                        }
                    }
                }
            }

            return null;
        }
    }
}
