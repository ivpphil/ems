﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.amazon.strategy;
using MarketplaceWebService;
using MarketplaceWebService.Model;

namespace jp.co.ivp.ers.mall.amazon
{
    /// <summary>
    /// GetFeedSubmissionList [GetFeedSubmissionList]
    /// </summary>
    public class GetFeedSubmissionList
    {
        /// <summary>
        /// リクエスト [Request]
        /// </summary>
        /// <param name="service">AmazonMWSサービスクライアント [The client of Amazon MWS service]</param>
        /// <param name="submissionId">サブミッションID [Submission id]</param>
        /// <returns>処理ステータス [Processing status]</returns>
        public virtual EnumMallAmazonFeedProcessingStatus? Request(AmazonMwsServiceClient service, string submissionId)
        {
            var setup = ErsMallFactory.ersMallBatchFactory.getSetup();

            // リクエスト変数設定
            GetFeedSubmissionListRequest request = new GetFeedSubmissionListRequest();

            // 引数設定
            request.Merchant = service.merchantId;

            // SubmissionID 設定
            request.FeedSubmissionIdList = new IdList();
            request.FeedSubmissionIdList.Id = new List<string>(new string[] { submissionId });

            // Submission 結果取得
            GetFeedSubmissionListResponse response = service.serviceClient.GetFeedSubmissionList(request);

            // 戻り値チェック
            if (response.IsSetGetFeedSubmissionListResult())
            {
                GetFeedSubmissionListResult getFeedSubmissionListResult = response.GetFeedSubmissionListResult;

                if (getFeedSubmissionListResult.IsSetHasNext())
                {
                    List<FeedSubmissionInfo> feedSubmissionInfoList = getFeedSubmissionListResult.FeedSubmissionInfo;

                    foreach (FeedSubmissionInfo feedSubmissionInfo in feedSubmissionInfoList)
                    {
                        if (feedSubmissionInfo.IsSetFeedProcessingStatus())
                        {
                            EnumMallAmazonFeedProcessingStatus result;

                            if (Enum.TryParse(feedSubmissionInfo.FeedProcessingStatus, out result))
                            {
                                return result;
                            }
                            else
                            {
                                throw new Exception(string.Format("Unexpected processing status. status = {0}", feedSubmissionInfo.FeedProcessingStatus));
                            }
                        }
                    }
                }
            }

            return null;
        }
    }
}
