﻿
namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// Yahoo!クレジットカード決済取消理由 [Type of cancel reason for credit card payment (Yahoo!)]
    /// </summary>
    public enum EnumMallYahooCardPaymentCancelReason
    {
        /// <summary>
        /// 1 : お客様都合 [Customer]
        /// </summary>
        Customer = 1,

        /// <summary>
        /// 2 : ストア都合 [Store]
        /// </summary>
        Store = 2,

        /// <summary>
        /// 3 : 不正利用懸念 [Fear misappropriation]
        /// </summary>
        FearMisappropriation = 3,

        /// <summary>
        /// 4 : その他 [Other]
        /// </summary>
        Other = 4,
    }
}
