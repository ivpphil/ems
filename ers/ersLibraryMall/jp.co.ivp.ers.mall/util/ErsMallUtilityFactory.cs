﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mall.util
{
    public class ErsMallUtilityFactory
    {
        /// <summary>
        /// Gets and sets pooled setup
        /// </summary>
        protected static SetupMall _setup
        {
            get
            {
                return (SetupMall)jp.co.ivp.ers.util.ErsCommonContext.GetPooledObject("_setupMall");
            }
            set
            {
                jp.co.ivp.ers.util.ErsCommonContext.SetPooledObject("_setupMall", value);
            }
        }

        /// <summary>
        /// 設定情報を格納したクラスを取得する（参照用）
        /// </summary>
        /// <returns>Returns instance of Setup</returns>
        public virtual SetupMall getSetup()
        {
            if (_setup == null)
                _setup = new SetupMall();

            return _setup;
        }


        /// <summary>
        /// モール用WebClient取得 [Get WebClient for Mall]
        /// </summary>
        /// <param name="timeoutMsec">タイムアウト（ミリ秒） [Timeout (Millisecond)]</param>
        /// <returns>Instance of MallWebClient</returns>
        public virtual MallWebClient GetMallWebClient(int timeoutMsec)
        {
            return new MallWebClient(timeoutMsec);
        }
    }
}
