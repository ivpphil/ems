﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mall.util
{
    public class SetupMall
        : jp.co.ivp.ers.util.SetupConfigReader
    {
        internal SetupMall()
        {
        }

        #region モール連携 [Mall cooperation]
        /// <summary>
        /// モール連携の有効／無効 [Mall cooperation enable / disable]
        /// </summary>
        public virtual bool enableMall
        {
            get
            {
                return bool.Parse(GetCommonConfigFileValue("enableMall"));
            }
        }

        /// <summary>
        /// モール商品SKU接頭語 [Mall product sku prefix]
        /// </summary>
        public virtual string mallProductSkuPrefix
        {
            get
            {
                return GetCommonConfigFileValue("mallProductSkuPrefix");
            }
        }


        /// <summary>
        /// HARC login user
        /// </summary>
        public virtual string harcLoginUser
        {
            get
            {
                return GetCommonConfigFileValue("harcLoginUser");
            }
        }

        /// <summary>
        /// HARC login user password
        /// </summary>
        public virtual string harcLoginPassword
        {
            get
            {
                return GetCommonConfigFileValue("harcLoginPassword");
            }
        }

        /// <summary>
        /// HARC auth url
        /// </summary>
        public virtual string harcAuthUrl
        {
            get
            {
                return GetCommonConfigFileValue("harcAuthUrl");
            }
        }

        /// <summary>
        /// HARC api url
        /// </summary>
        public virtual string harcApiUrl
        {
            get
            {
                return string.Format(GetCommonConfigFileValue("harcApiUrl"), GetCommonConfigFileValue("harcLoginUser"));
            }
        }

        /// <summary>
        /// HARC serialize file path
        /// </summary>
        public virtual string harcSerializeFilePath
        {
            get
            {
                return Path.Combine(SiteTypeVariables.GetApplicationRoot(EnumSiteType.ADMIN, 0), GetCommonConfigFileValue("harcSerializeFilePath"));
            }
        }

        /// <summary>
        /// HARC serialize file name
        /// </summary>
        public virtual string harcSerializeFileName
        {
            get
            {
                return GetCommonConfigFileValue("harcSerializeFileName");
            }
        }

        /// <summary>
        /// HARC time out seconds
        /// </summary>
        public virtual int harcTimeOutSeconds
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("harcTimeOutSeconds"));
            }
        }

        /// <summary>
        /// モール連携受注ステータス更新再検証フラグ [Flag of verify update order status for Mall cooperation]
        /// </summary>
        public virtual bool mall_order_update_status_verify
        {
            get
            {
                return Convert.ToBoolean(GetCommonConfigFileValue("mall_order_update_status_verify"));
            }
        }

        /// <summary>
        /// モール連携APIバックアップパス [Path of backup for Mall cooperation]
        /// </summary>
        public virtual string mall_api_backup_path
        {
            get
            {
                return GetCommonConfigFileValue("mall_api_backup_path");
            }
        }

        /// <summary>
        /// HARC CSVテンポラリ出力パス [Output path for HARC product CSV temporary]
        /// </summary>
        public virtual string harcProductCsvOutputPath
        {
            get
            {
                return ErsCommonContext.MapPath(GetCommonConfigFileValue("harcProductCsvOutputPath"));
            }
        }

        /// <summary>
        /// Yahoo!ストアクリエイターPro切替日時 [Cahange to Yahoo! store creator pro datetime]
        /// </summary>
        public virtual DateTime? mallYahooStoreCreatorProDatetime
        {
            get
            {
                var tmp = GetCommonConfigFileValue("mallYahooStoreCreatorProDatetime");
                return !string.IsNullOrEmpty(tmp) ? (DateTime?)Convert.ToDateTime(tmp) : null;
            }
        }

        #region 在庫平準化アラート
        /// <summary>
        /// 在庫平準化アラートメールFROM [Mail FROM of alert for stock leveling]
        /// </summary>
        public virtual string mallStockLevelingAlertMailFrom
        {
            get
            {
                return GetCommonConfigFileValue("mallStockLevelingAlertMailFrom");
            }
        }

        /// <summary>
        /// 在庫平準化アラートメールTo [Mail TO of alert for stock leveling]
        /// </summary>
        public virtual string mallStockLevelingAlertMailTo
        {
            get
            {
                return GetCommonConfigFileValue("mallStockLevelingAlertMailTo");
            }
        }

        /// <summary>
        /// 在庫平準化アラートメールCC [Mail CC of alert for stock leveling]
        /// </summary>
        public virtual string mallStockLevelingAlertMailCc
        {
            get
            {
                return GetCommonConfigFileValue("mallStockLevelingAlertMailCc");
            }
        }

        /// <summary>
        /// 在庫平準化アラートメールBCC [Mail BCC of alert for stock leveling]
        /// </summary>
        public virtual string mallStockLevelingAlertMailBcc
        {
            get
            {
                return GetCommonConfigFileValue("mallStockLevelingAlertMailBcc");
            }
        }

        /// <summary>
        /// 在庫平準化アラートメール件名 [Mail Title of alert for stock leveling]
        /// </summary>
        public virtual string mallStockLevelingAlertMailTitle
        {
            get
            {
                return GetCommonConfigFileValue("mallStockLevelingAlertMailTitle");
            }
        }
        #endregion

        #region メール [Mail]
        /// <summary>
        /// モールメール送信フラグ [Send mail mall falg]
        /// </summary>
        public virtual bool enableSendMailMall
        {
            get
            {
                return Convert.ToBoolean(GetCommonConfigFileValue("enableSendMailMall"));
            }
        }
        #endregion
        #endregion
    }
}
