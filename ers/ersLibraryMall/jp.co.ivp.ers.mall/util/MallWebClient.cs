﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace jp.co.ivp.ers.mall.util
{
    /// <summary>
    /// モール用WebClient [WebClient for Mall]
    /// </summary>
    public class MallWebClient
        : WebClient
    {
        /// <summary>
        /// タイムアウト（ミリ秒） [Timeout (Millisecond)]
        /// </summary>
        protected virtual int timeoutMsec { get; set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="timeoutMsec">タイムアウト（ミリ秒） [Timeout (Millisecond)]</param>
        public MallWebClient(int timeoutMsec)
            : base()
        {
            this.timeoutMsec = timeoutMsec;
        }

        /// <summary>
        /// WebRequest取得 [Get WebRequest]
        /// </summary>
        /// <param name="uri">URI [URI]</param>
        /// <returns>WebRequest [WebRequest]</returns>
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest webReq = base.GetWebRequest(uri);
            webReq.Timeout = this.timeoutMsec;
            return webReq;
        }
    }
}
