﻿using System.Collections.Generic;
using jp.co.ivp.ers.mall.product.strategy;

namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// Enum extension for mall shop type
    /// </summary>
    public static class EnumMallShopKbnExtension
    {
        /// <summary>
        /// ProductLevelingSettingsCsvService のインスタンスを取得
        /// </summary>
        /// <param name="shop_kbn"></param>
        /// <returns></returns>
        public static ProductLevelingSettingsCsvService GetProductLevelingSettingsCsvService(EnumMallShopKbn shop_kbn)
        {
            switch (shop_kbn)
            {
                case EnumMallShopKbn.RAKUTEN:
                    return new ProductLevelingSettingsCsvServiceRakuten();
                case EnumMallShopKbn.YAHOO:
                    return new ProductLevelingSettingsCsvServiceYahoo();
                case EnumMallShopKbn.AMAZON:
                    return new ProductLevelingSettingsCsvServiceAmazon();
                case EnumMallShopKbn.ERS:
                    return new ProductLevelingSettingsCsvServiceEcCube();
                default:
                    return null;
            }
        }
    }
}
