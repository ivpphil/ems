﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// Enums for mall function type
    /// </summary>
    public enum EnumMallFuncType
    {
        /// <summary>
        /// 1 : 受注 [Order]
        /// </summary>
        Order = 1,

        /// <summary>
        /// 2 : 商品 [Product]
        /// </summary>
        Product = 2,

        /// <summary>
        /// 3 : 商品画像 [Product image]
        /// </summary>
        ProductImage = 3,

        /// <summary>
        /// 4 : 在庫 [Stock]
        /// </summary>
        Stock = 4,
    }
}
