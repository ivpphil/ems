﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.viewService
{
    /// <summary>
    /// Amazon商品タイプサービス [View service of Rakuten country]
    /// </summary>
    public class ErsViewMallAmazonProductTypeService
        : ErsViewServiceBase
    {
        public const string cacheKey = "type_name-name_type_code";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsMallFactory.ersMallProductFactory.GetErsMallAmazonProductTypeRepository();
                var criteria = ErsMallFactory.ersMallProductFactory.GetErsMallAmazonProductTypeCriteria();
                criteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
                var list = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(list);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// レコード数取得 [Get the record count]
        /// </summary>
        /// <returns>レコード数 [Record count]</returns>
        public virtual long GetCount()
        {
            var list = this.GetCachedList();

            return list.Count;
        }

        /// <summary>
        /// リスト取得 [Get the list]
        /// </summary>
        /// <returns>リスト [List]</returns>
        public virtual List<Dictionary<string, object>> GetList()
        {
            var list = this.GetCachedList();

            list = this.GetOnlyActiveRecord(list);

            list = this.GetNameValueList(list, "type_name", "type_code");

            return list;
        }

        /// <summary>
        /// 名称取得 [Get the name]
        /// </summary>
        /// <param name="id">ID [ID]</param>
        /// <returns>名称 [Name]</returns>
        public virtual string GetName(string type_code)
        {
            if (!type_code.HasValue())
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "type_name", "type_code", type_code);
        }

        /// <summary>
        /// 存在チェック [Check exist]
        /// </summary>
        /// <param name="id">ID [ID]</param>
        /// <returns>true : 存在している [Exist] / false : 存在していない [Not exist]</returns>
        public virtual bool ExistValue(string type_code)
        {
            var list = this.GetCachedList();

            return this.ExistValue(list, "type_code", type_code);
        }
    }
}
