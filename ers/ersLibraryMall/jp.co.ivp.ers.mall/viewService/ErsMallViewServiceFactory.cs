﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mall.viewService
{
    public class ErsMallViewServiceFactory
    {
        public virtual ErsViewMallRakutenCountryService GetErsViewMallRakutenCountryService()
        {
            return new ErsViewMallRakutenCountryService();
        }

        public virtual ErsViewMallAmazonProductTypeService GetErsViewMallAmazonProductTypeService()
        {
            return new ErsViewMallAmazonProductTypeService();
        }

        public virtual ErsViewSiteService GetErsViewSiteService()
        {
            return new ErsViewSiteService();
        }
    }
}
