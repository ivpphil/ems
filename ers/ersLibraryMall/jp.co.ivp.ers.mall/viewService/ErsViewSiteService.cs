﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.viewService;

namespace jp.co.ivp.ers.mall.viewService
{
    public class ErsViewSiteService
        : ErsViewServiceBase
    {
        public const string cacheKey = "site_name-name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsMallFactory.ersSiteFactory.GetErsSiteRepository();
                var criteria = ErsMallFactory.ersSiteFactory.GetErsSiteCriteria();
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                if(!ErsFactory.ersUtilityFactory.getSetup().Multiple_sites)
                {
                    criteria.site_cate = (int)EnumSiteId.COMMON_SITE_ID;
                }
                var list = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(list);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// Obtain a list of job names using ErsDB_site_t.
        /// </summary>
        /// <returns>Returns list of site names</returns>
        public virtual List<Dictionary<string, object>> SelectAsList(EnumMallShopKbn? mall_shop_kbn = null)
        {
            if (mall_shop_kbn.HasValue)
            {
                return this.SelectAsList((record) => (int)record["mall_shop_kbn"] == (int)mall_shop_kbn.Value);
            }
            else
            {
                return this.SelectAsList((record) => true);
            }
        }

        /// <summary>
        /// Obtain a list of job names using ErsDB_site_t.
        /// </summary>
        /// <returns>Returns list of site names</returns>
        public virtual List<Dictionary<string, object>> SelectAsList(Func<Dictionary<string, object>, bool> funcCompare)
        {
            var list = this.GetCachedList();

            list = this.GetOnlyActiveRecord(list);

            list = list.FindAll((record) => funcCompare(record));

            list = this.GetNameValueList(list, "site_name", "id");

            return list;
        }

        /// <summary>
        /// Obtain a list of job names using ErsDB_site_t.
        /// </summary>
        /// <returns>Returns list of site names</returns>
        public virtual List<Dictionary<string, object>> SelectAsList(int[] id, EnumMallShopKbn? mall_shop_kbn = null)
        {
            return this.SetSelected(this.SelectAsList(mall_shop_kbn), "id", id, "isSelected");
        }

        /// <summary>
        /// Gets job name according to the specified id using ErsDB_site_t.
        /// </summary>
        /// <param name="id">job id use for finding site name</param>
        /// <returns>Returns value of site name</returns>
        public virtual string GetStringFromId(int? id)
        {
            if (id == null)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "site_name", "id", id);
        }

        /// <summary>
        /// Gets job name according to the specified id using ErsDB_site_t.
        /// </summary>
        /// <param name="id">job id use for finding site name</param>
        /// <param name="mall_shop_kbn">job mall_shop_kbn use for finding site name</param>
        /// <returns>Returns value of site name</returns>
        public virtual string GetStringFromIds(int[] id)
        {
            if (id == null || id.Length == 0)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFrommSiteIdAndMallShopKbn(list, id);
        }

        protected virtual string GetStringFrommSiteIdAndMallShopKbn(List<Dictionary<string, object>> list, int[] id)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (id == null || id.Length == 0)
            {
                return null;
            }

            var result = list.Where((dictionary) =>
            {
                return id.Contains(Convert.ToInt32(dictionary["id"]));
            });

            if (result == null)
            {
                return null;
            }
            var resultValues = result.Select(e => Convert.ToString(e["site_name"]));

            return string.Join(", ", resultValues);
        }

        /// <summary>
        /// Get's boolean result of the specified id if it's existing or not using ErsDB_job_t.
        /// </summary>
        /// <param name="id">job id use for finding id</param>
        /// <returns>Returns true if the id is existing, returns false if not existing.</returns>
        public virtual bool ExistValue(int? id)
        {
            if (id == null)
            {
                return false;
            }

            var list = this.GetCachedList();

            return this.ExistValue(list, "id", (int?)id);
        }
    }
}
