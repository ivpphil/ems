﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.viewService
{
    /// <summary>
    /// 楽天国ビューサービス [View service of Rakuten country]
    /// </summary>
    public class ErsViewMallRakutenCountryService
        : ErsViewServiceBase
    {
        public const string cacheKey = "country_name-name_id";

        private List<Dictionary<string, object>> GetCachedList()
        {
            if (!this.CachedValue.ContainsKey(cacheKey))
            {
                var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallRakutenCountryRepository();
                var criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallRakutenCountryCriteria();
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
                var list = repository.Find(criteria);

                this.CachedValue[cacheKey] = ErsCommon.ConvertEntityListToDictionaryList(list);
            }
            return this.CachedValue[cacheKey];
        }

        /// <summary>
        /// レコード数取得 [Get the record count]
        /// </summary>
        /// <returns>レコード数 [Record count]</returns>
        public virtual long GetCount()
        {
            var list = this.GetCachedList();

            return list.Count;
        }

        /// <summary>
        /// リスト取得 [Get the list]
        /// </summary>
        /// <returns>リスト [List]</returns>
        public virtual IList<Dictionary<string, object>> GetList()
        {
            var list = this.GetCachedList();

            list = this.GetOnlyActiveRecord(list);

            list = this.GetNameValueList(list, "country_name", "id");

            return list;
        }

        /// <summary>
        /// 名称取得 [Get the name]
        /// </summary>
        /// <param name="id">ID [ID]</param>
        /// <returns>名称 [Name]</returns>
        public virtual string GetName(int id)
        {
            if (id == 0)
            {
                return null;
            }

            var list = this.GetCachedList();

            return this.GetStringFromId(list, "country_name", "id", id);
        }

        /// <summary>
        /// 存在チェック [Check exist]
        /// </summary>
        /// <param name="id">ID [ID]</param>
        /// <returns>true : 存在している [Exist] / false : 存在していない [Not exist]</returns>
        public virtual bool ExistValue(int id)
        {
            var list = this.GetCachedList();

            return this.ExistValue(list, "id", id);
        }
    }
}
