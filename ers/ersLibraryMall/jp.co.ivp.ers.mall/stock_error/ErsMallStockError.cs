﻿using System;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.stock_error
{
    /// <summary>
    /// モール連携在庫エラーエンティティ [Entity for mall stock error table]
    /// </summary>
    public class ErsMallStockError
        : ErsRepositoryEntity
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 登録日時 [Datetime of register]
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新日時 [Datetime of update]
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// 検索日時（FROM） [Datetime of search (From)]
        /// </summary>
        public virtual DateTime? search_from { get; set; }

        /// <summary>
        /// 検索日時（TO） [Datetime of search (To)]
        /// </summary>
        public virtual DateTime? search_to { get; set; }

        /// <summary>
        /// 在庫リクエストサイトID [Site id]
        /// </summary>
        public virtual int? site_id { get; set; }

        /// <summary>
        /// 在庫リクエスト店舗タイプ [The type of mall shop]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// 在庫リクエスト店舗ID [The ID of mall shop]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// 在庫リクエストID [The ID of update stock request]
        /// </summary>
        public virtual int? stock_id { get; set; }

        /// <summary>
        /// 在庫リクエスト実行ステータス [The status of update stock process]
        /// </summary>
        public virtual EnumMallStockPostingSlipStatus? stock_status { get; set; }

        /// <summary>
        /// 在庫リクエスト開始日時 [The datetime of update stock request start]
        /// </summary>
        public virtual DateTime? stock_start { get; set; }

        /// <summary>
        /// 在庫リクエスト終了日時 [The datetime of update stock request end]
        /// </summary>
        public virtual DateTime? stock_end { get; set; }

        /// <summary>
        /// 在庫リクエスト結果メッセージ [The result message of update stock request]
        /// </summary>
        public virtual string stock_result_message { get; set; }

        /// <summary>
        /// 在庫リクエスト商品情報 [The product information of update stock request]
        /// </summary>
        public virtual string stock_product_info { get; set; }
    }
}
