﻿using System.Collections.Generic;
using jp.co.ivp.ers.mall.stock_error.strategy;

namespace jp.co.ivp.ers.mall.stock_error
{
    /// <summary>
    /// モール在庫エラー関連ファクトリ [Factory for stock error of mall]
    /// </summary>
    public class ErsMallStockErrorFactory
    {
        /// <summary>
        /// モール連携在庫エラーエンティティ取得 [Get mall order import]
        /// </summary>
        /// <returns>ErsMallStockError</returns>
        public virtual ErsMallStockError GetErsMallStockError()
        {
            return new ErsMallStockError();
        }

        /// <summary>
        /// モール連携在庫エラーリポジトリ取得 [Get mall stock error repository]
        /// </summary>
        /// <returns>ErsMallStockErrorRepository</returns>
        public virtual ErsMallStockErrorRepository GetErsMallStockErrorRepository()
        {
            return new ErsMallStockErrorRepository();
        }

        /// <summary>
        /// モール連携在庫エラークライテリア取得 [Get mall stock error criteria]
        /// </summary>
        /// <returns>ErsMallStockErrorCriteria</returns>
        public virtual ErsMallStockErrorCriteria GetErsMallStockErrorCriteria()
        {
            return new ErsMallStockErrorCriteria();
        }

        /// <summary>
        /// モール連携在庫エラーエンティティ取得（パラメータから） [Get mall stock error entity (from parameters)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsMallStockError</returns>
        public virtual ErsMallStockError GetErsMallStockErrorWithParameters(Dictionary<string, object> dicParams)
        {
            var obj = this.GetErsMallStockError();
            obj.OverwriteWithParameter(dicParams);
            return obj;
        }

        /// <summary>
        /// モール連携在庫エラーエンティティ取得（IDから） [Get mall stock error entity (from ID)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsMallStockError</returns>
        public virtual ErsMallStockError GetErsMallStockErrorWithID(int id)
        {
            var repository = this.GetErsMallStockErrorRepository();

            var criteria = this.GetErsMallStockErrorCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            return list.Count > 0 ? list[0] : null;
        }


        /// <summary>
        /// モール連携在庫更新エラー登録・更新クラス取得 [Get the class of RegisterAndUpdateMallStockErrorStgy]
        /// </summary>
        /// <returns>モール連携在庫更新エラー登録・更新クラスインスタンス [Instance for RegisterAndUpdateMallStockErrorStgy]</returns>
        public virtual RegisterAndUpdateMallStockErrorStgy GetRegisterAndUpdateMallStockErrorStgy()
        {
            return new RegisterAndUpdateMallStockErrorStgy();
        }

        /// <summary>
        /// 最新検索日時（TO）取得クラス取得 [Get the class of ObtainLastSearchToStgy]
        /// </summary>
        /// <returns>最新検索日時（TO）取得クラスインスタンス [Instance for ObtainLastSearchToStgy]</returns>
        public virtual ObtainLastSearchToStgy GetObtainLastSearchToStgy()
        {
            return new ObtainLastSearchToStgy();
        }
    }
}
