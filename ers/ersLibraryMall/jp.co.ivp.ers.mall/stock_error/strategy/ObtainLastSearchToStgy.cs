﻿using System;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.stock_error.strategy
{
    /// <summary>
    /// 最新検索日時（TO）取得 [Get the datetime of last search (To)]
    /// </summary>
    public class ObtainLastSearchToStgy
    {
        /// <summary>
        /// 最新検索日時（TO）取得 [Get the datetime of last search (To)]
        /// </summary>
        /// <returns>最新検索日時（TO） [The datetime of last search (To)]</returns>
        public virtual DateTime? Obtain()
        {
            var spec = new LastSearchToSQLSpec();

            var listFind = ErsRepository.SelectSatisfying(spec, null);

            if (listFind[0]["max_search_to"] != null)
            {
                return Convert.ToDateTime(listFind[0]["max_search_to"]);
            }

            return null;
        }


        /// <summary>
        /// 最新検索日時（TO）検索用SQLクラス [SQL class for Get the datetime of last search (To)]
        /// </summary>
        protected class LastSearchToSQLSpec
           : ISpecificationForSQL
        {
            /// <summary>
            /// コンストラクタ [Constructor]
            /// </summary>
            public LastSearchToSQLSpec()
            {
            }

            /// <summary>
            /// SQL文 [SQL statement]
            /// </summary>
            /// <returns>SQL文 [SQL statement]</returns>
            public virtual string asSQL()
            {
                return "SELECT MAX(search_to) AS max_search_to FROM mall_stock_error_t";
            }
        }
    }
}