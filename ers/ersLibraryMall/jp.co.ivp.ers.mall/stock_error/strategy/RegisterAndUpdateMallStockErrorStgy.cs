﻿using System;
using System.Collections;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.common;

namespace jp.co.ivp.ers.mall.stock_error.strategy
{
    /// <summary>
    /// モール連携在庫更新エラー登録・更新 [Register and update mall update stock error]
    /// </summary>
    public class RegisterAndUpdateMallStockErrorStgy
    {
        #region 登録（エラーなし） [Register (No error)]
        /// <summary>
        /// 登録（エラーなし） [Register (No error)]
        /// </summary>
        /// <param name="dateSearchFrom">検索日時（FROM） [Datetime of search (From)]</param>
        /// <param name="dateSearchTo">検索日時（TO） [Datetime of search (To)]</param>
        public void RegisterNoError(DateTime? dateFrom, DateTime? dateTo)
        {
            var repository = ErsMallFactory.ersMallStockErrorFactory.GetErsMallStockErrorRepository();
            var objMallStockError = ErsMallFactory.ersMallStockErrorFactory.GetErsMallStockError();

            objMallStockError.search_from = dateFrom;
            objMallStockError.search_to = dateTo;

            repository.Insert(objMallStockError);
        }
        #endregion

        #region 登録 [Register]
        /// <summary>
        /// 登録 [Register]
        /// </summary>
        /// <param name="dicResult">実行結果ディクショナリ [Dictionary of result]</param>
        /// <param name="dateSearchFrom">検索日時（FROM） [Datetime of search (From)]</param>
        /// <param name="dateSearchTo">検索日時（TO） [Datetime of search (To)]</param>
        /// <returns>モール連携在庫更新エラー [Mall update stock error]</returns>
        public ErsMallStockError Register(IDictionary<string, object> dicResult, DateTime? dateFrom, DateTime? dateTo)
        {
            var repository = ErsMallFactory.ersMallStockErrorFactory.GetErsMallStockErrorRepository();
            var criteria = ErsMallFactory.ersMallStockErrorFactory.GetErsMallStockErrorCriteria();

            criteria.stock_id = Convert.ToInt32(dicResult["stockPostingSlipId"]);

            if (repository.GetRecordCount(criteria) > 0)
            {
                return null;
            }

            var objMallStockError = ErsMallFactory.ersMallStockErrorFactory.GetErsMallStockError();

            objMallStockError = this.SetDataForRegister(dicResult, objMallStockError, dateFrom, dateTo);

            if (objMallStockError == null)
            {
                return null;
            }

            repository.Insert(objMallStockError, true);

            return objMallStockError;
        }

        /// <summary>
        /// 登録用データセット [Set the data for Register]
        /// </summary>
        /// <param name="dicResult">実行結果ディクショナリ [Dictionary of result]</param>
        /// <param name="objMallStockError">モール連携在庫更新エラー [Mall update stock error]</param>
        /// <param name="dateSearchFrom">検索日時（FROM） [Datetime of search (From)]</param>
        /// <param name="dateSearchTo">検索日時（TO） [Datetime of search (To)]</param>
        /// <returns>モール連携在庫更新エラー [Mall update stock error]</returns>
        protected ErsMallStockError SetDataForRegister(IDictionary<string, object> dicResult, ErsMallStockError objMallStockError, DateTime? dateFrom, DateTime? dateTo)
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var siteData = ErsMallFactory.ersSiteFactory.GetSiteData();

            objMallStockError.search_from = dateFrom;
            objMallStockError.search_to = dateTo;

            objMallStockError.shop_id = Convert.ToInt32(dicResult["shopId"]);
            objMallStockError.site_id = mallSetup.GetSiteIdFromShopId(objMallStockError.shop_id);

            if (objMallStockError.site_id == null)
            {
                return null;
            }

            objMallStockError.mall_shop_kbn = siteData.GetMallShopKbnFromSiteId(objMallStockError.site_id.Value);

            objMallStockError.stock_id = Convert.ToInt32(dicResult["stockPostingSlipId"]);
            objMallStockError.stock_status = (EnumMallStockPostingSlipStatus)Enum.ToObject(typeof(EnumMallStockPostingSlipStatus), dicResult["status"]);
            objMallStockError.stock_start = Convert.ToDateTime(dicResult["issueDate"]);
            objMallStockError.stock_end = dicResult["postingFinish"] != null ? (DateTime?)Convert.ToDateTime(dicResult["postingFinish"]) : null;
            objMallStockError.stock_result_message = Convert.ToString(dicResult["resultMessage"]);

            var listProducts = new List<string>();

            var prefix = ErsMallCommonService.mallProductSkuPrefix;

            foreach (Dictionary<string, object> item in (ArrayList)dicResult["stockPostingSlipItems"])
            {
                // 接頭語判定 [Judgement prefix]
                if (item.ContainsKey("productSkuCode") &&
                    (prefix.HasValue() && !Convert.ToString(item["productSkuCode"]).StartsWith(prefix)))
                {
                    continue;
                }

                var listProduct = new List<string>();

                foreach (var data in item)
                {
                    listProduct.Add(string.Format("{0} = {1}", data.Key, data.Value));
                }

                listProducts.Add(String.Join(", ", listProduct));
            }

            if (listProducts.Count == 0)
            {
                return null;
            }

            objMallStockError.stock_product_info = String.Join(Environment.NewLine, listProducts);

            return objMallStockError;
        }
        #endregion

        #region 更新 [Update]
        /// <summary>
        /// 更新 [Update]
        /// </summary>
        /// <param name="dicResult">実行結果ディクショナリ [Dictionary of result]</param>
        /// <param name="objMallStockError">モール連携在庫更新エラー [Mall update stock error]</param>
        /// <returns>モール連携在庫更新エラー [Mall update stock error]</returns>
        public ErsMallStockError Update(IDictionary<string, object> dicResult, ErsMallStockError objMallStockError)
        {
            var repository = ErsMallFactory.ersMallStockErrorFactory.GetErsMallStockErrorRepository();

            var objNew = ErsMallFactory.ersMallStockErrorFactory.GetErsMallStockErrorWithID(objMallStockError.id.Value);

            this.SetDataForUpdate(dicResult, objNew);

            repository.Update(objMallStockError, objNew);

            return objNew;
        }

        /// <summary>
        /// 更新用データセット [Set the data for Update]
        /// </summary>
        /// <param name="dicResult">実行結果ディクショナリ [Dictionary of result]</param>
        /// <param name="objMallStockError">モール連携在庫更新エラー [Mall update stock error]</param>
        protected void SetDataForUpdate(IDictionary<string, object> dicResult, ErsMallStockError objMallStockError)
        {
            objMallStockError.stock_status = (EnumMallStockPostingSlipStatus)Enum.ToObject(typeof(EnumMallStockPostingSlipStatus), dicResult["status"]);
            objMallStockError.stock_end = Convert.ToDateTime(dicResult["postingFinish"]);
            objMallStockError.stock_result_message = Convert.ToString(dicResult["resultMessage"]);
        }
        #endregion
    }
}