﻿using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.stock_error
{
    /// <summary>
    /// モール連携在庫エラーリポジトリ [Repository for mall stock error table]
    /// </summary>
    public class ErsMallStockErrorRepository
        : ErsRepository<ErsMallStockError>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public ErsMallStockErrorRepository()
            : base("mall_stock_error_t")
        {
        }
    }
}
