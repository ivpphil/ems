﻿using System;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.stock_error
{
    /// <summary>
    /// モール連携在庫エラークライテリア [Criteria for mall stock error table]
    /// </summary>
    public class ErsMallStockErrorCriteria
        : Criteria
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_stock_error_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 在庫リクエストID [The id of update stock process]
        /// </summary>
        public virtual int? stock_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_stock_error_t.stock_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 在庫リクエスト実行ステータス [The status of update stock process]
        /// </summary>
        public virtual EnumMallStockPostingSlipStatus? stock_status
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_stock_error_t.stock_status", Convert.ToInt32(value), Operation.EQUAL));
            }
        }


        /// <summary>
        /// IDソート [Sort by ID]
        /// </summary> 
        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("mall_stock_error_t.id", orderBy);
        }
    }
}
