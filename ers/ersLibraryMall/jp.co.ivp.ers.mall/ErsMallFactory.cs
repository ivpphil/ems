﻿
namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// モール連携関連ファクトリ [Factory for mall]
    /// </summary>
    public class ErsMallFactory
    {
        /// <summary>
        /// ユーティリティ・設定関連クラスのFactory
        /// </summary>
        public static jp.co.ivp.ers.mall.util.ErsMallUtilityFactory ersMallUtilityFactory;

        /// <summary>
        /// サイト設定関連ファクトリ [Factory for site settings of mall]
        /// </summary>
        public static jp.co.ivp.ers.mall.site.ErsSiteFactory ersSiteFactory;

        /// <summary>
        /// 店舗情報関連ファクトリ [Factory for shop information of mall]
        /// </summary>
        public static jp.co.ivp.ers.mall.shop.ErsMallShopFactory ersMallShopFactory;

        /// <summary>
        /// メール関連クラスのFactory
        /// </summary>
        public static jp.co.ivp.ers.mall.sendmail.ErsMallMailFactory ersMallMailFactory;

        /// <summary>
        /// バッチ関連クラスのFactory
        /// </summary>
        public static jp.co.ivp.ers.mall.batch.ErsMallBatchFactory ersMallBatchFactory;

        /// <summary>
        /// ViewServiceクラスのFactory
        /// </summary>
        public static jp.co.ivp.ers.mall.viewService.ErsMallViewServiceFactory ersMallViewServiceFactory;

        /// <summary>
        /// Amazon関連ファクトリ [Factory for Amazon]
        /// </summary>
        public static jp.co.ivp.ers.mall.amazon.ErsMallAmazonFactory ersMallAmazonFactory;

        /// <summary>
        /// モールAPI関連ファクトリ [Factory for api of mall]
        /// </summary>
        public static jp.co.ivp.ers.mall.api.ErsMallAPIFactory ersMallAPIFactory;

        /// <summary>
        /// モール共通処理関連ファクトリ [Factory for common processes of mall]
        /// </summary>
        public static jp.co.ivp.ers.mall.common.ErsMallCommonFactory ersMallCommonFactory;

        /// <summary>
        /// モール停止時間関連ファクトリ [Factory for stop time of mall]
        /// </summary>
        public static jp.co.ivp.ers.mall.stop_time.ErsMallStopTimeFactory ersMallStopTimeFactory;

        /// <summary>
        /// モール伝票関連ファクトリ [Factory for mall order]
        /// </summary>
        public static jp.co.ivp.ers.mall.mall_order.ErsMallOrderFactory ersMallOrderFactory;

        /// <summary>
        /// モール商品情報関連ファクトリ [Factory for product information]
        /// </summary>
        public static jp.co.ivp.ers.mall.product.ErsMallProductFactory ersMallProductFactory;

        /// <summary>
        /// モール在庫エラー関連ファクトリ [Factory for stock error of mall]
        /// </summary>
        public static jp.co.ivp.ers.mall.stock_error.ErsMallStockErrorFactory ersMallStockErrorFactory;

        /// <summary>
        /// モール在庫リカバリ関連ファクトリ [Factory for stock recovery of mall]
        /// </summary>
        public static jp.co.ivp.ers.mall.stock_recovery.ErsMallStockRecoveryFactory ersMallStockRecoveryFactory;

        /// <summary>
        /// モール在庫関連ファクトリ [Factory for stock of mall]
        /// </summary>
        public static jp.co.ivp.ers.mall.stock.ErsMallStockFactory ersMallStockFactory;
    }
}
