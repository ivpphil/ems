﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// 在庫操作 [Mall stock operation]
    /// </summary>
    public enum EnumMallStockOperation
    {
        /// <summary>
        /// set : セット [Set]
        /// </summary>
        set = 0,

        /// <summary>
        /// add : 加算 [Add]
        /// </summary>
        add = 1,

        /// <summary>
        /// sub : 減算 [Subtract]
        /// </summary>
        sub = 2,
    }
}
