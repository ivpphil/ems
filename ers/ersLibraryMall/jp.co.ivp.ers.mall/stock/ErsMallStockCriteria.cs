﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.stock
{
    public class ErsMallStockCriteria
        : Criteria
    {
        public string scode
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_stock_t.scode", value, Operation.EQUAL));
            }
        }
    }
}
