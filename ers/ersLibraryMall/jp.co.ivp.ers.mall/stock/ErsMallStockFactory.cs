﻿using jp.co.ivp.ers.mall.stock.strategy;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall.stock.specification;

namespace jp.co.ivp.ers.mall.stock
{
    /// <summary>
    /// モール在庫関連ファクトリ [Factory for stock of mall]
    /// </summary>
    public class ErsMallStockFactory
    {
        /// <summary>
        /// モール在庫更新取得 [Get update stock for mall]
        /// </summary>
        /// <returns>Instance of UpdateMallStockStgy</returns>
        public virtual UpdateMallStockStgy GetUpdateMallStockStgy()
        {
            return new UpdateMallStockStgy();
        }

        /// <summary>
        /// ERS内で保持するHarcの在庫数を取得
        /// </summary>
        /// <param name="objDB"></param>
        /// <returns></returns>
        public virtual ErsMallStockRepository GetErsMallStockRepository(ErsDatabase objDB)
        {
            return new ErsMallStockRepository(objDB);
        }

        /// <summary>
        /// ERS内で保持するHarcの在庫数を取得
        /// </summary>
        /// <returns></returns>
        public virtual ErsMallStockCriteria GetErsMallStockCriteria()
        {
            return new ErsMallStockCriteria();
        }

        /// <summary>
        /// ERS内で保持するHarcの在庫数を取得
        /// </summary>
        /// <returns></returns>
        public virtual ErsMallStock GetErsMallStock()
        {
            return new ErsMallStock();
        }

        /// <summary>
        /// ERS内で保持するHarcの在庫数の加算/減算をおこなう
        /// </summary>
        /// <param name="objDB"></param>
        /// <returns></returns>
        public virtual UpdateErsMallStockStgy GetUpdateErsMallStockStgy()
        {
            return new UpdateErsMallStockStgy();
        }

        public virtual SearchParentMallStockDiffSpec GetSearchParentMallStockDiffSpec()
        {
            return new SearchParentMallStockDiffSpec();
        }
    }
}
