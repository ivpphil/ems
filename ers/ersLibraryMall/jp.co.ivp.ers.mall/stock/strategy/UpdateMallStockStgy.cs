﻿using System;
using System.Collections.Generic;
using System.Linq;
using jp.co.ivp.ers.mall.api;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.stock.strategy
{
    /// <summary>
    /// モール在庫更新 [Update stock for mall]
    /// </summary>
    public class UpdateMallStockStgy
    {
        /// <summary>
        /// モール在庫更新 [Update stock for mall]
        /// </summary>
        /// <param name="listParam">パラメータリスト [The list of parameter]</param>
        /// <returns>エラーメッセージ [Error message]</returns>
        public virtual string UpdateMallStock(IList<UpdateStockParam> listParam)
        {
            string result;
            var objDB = ErsCommonsSetting.ersDatabaseFactory.GetNewErsDatabase();
            using (var otherTx = ErsDB_parent.BeginTransaction(objDB))
            {
                result = this.UpdateMallStock(listParam, objDB);

                otherTx.Commit();
            }

            return result;
        }

        /// <summary>
        /// モール在庫更新 [Update stock for mall]
        /// </summary>
        /// <param name="listParam">パラメータリスト [The list of parameter]</param>
        /// <returns>エラーメッセージ [Error message]</returns>
        public virtual string UpdateMallStock(IList<UpdateStockParam> listParam, ErsDatabase objDB)
        {
            if (listParam.Count == 0)
            {
                return null;
            }

            var setup = ErsMallFactory.ersMallUtilityFactory.getSetup();

            // OFF時は処理しない
            if (!setup.enableMall)
            {
                return null;
            }

            // VEX中は処理しない [If on VEX, Ignore this process.]
            if (setup.onVEX)
            {
                return null;
            }

            if (listParam == null || listParam.Count == 0)
            {
                return null;
            }

            // パラメータリスト再計算 [Re-calc list of parameter]
            var listRecalcParam = this.GetRecalcListParam(listParam);

            var paramApi = ErsMallFactory.ersMallAPIFactory.GetUpdateStockAPIParam();
            paramApi.shop_id = null;

            Dictionary<string, List<object>> dicResult;

            // ERSのHarc在庫数を更新
            this.UpdateErsMallStock(objDB, listRecalcParam);

            // HARCログイン [Log in to HARC]
            var request = ErsMallFactory.ersMallCommonFactory.GetHarcLoginStgy().HarcLogin();

            // モール在庫数更新 [Update mall stock]
            dicResult = ErsMallFactory.ersMallAPIFactory.GetUpdateStockAPI(paramApi).UpdateStock(request, listRecalcParam);

            var listErrorMessage = new List<string>();

            if (dicResult.ContainsKey("productIds") && (listRecalcParam.Count != dicResult["productIds"].Count))
            {
                IList<string> listScode = new List<string>();

                foreach (var param in listRecalcParam)
                {
                    listScode.Add(param.productCode);
                }

                listErrorMessage.Add(ErsResources.GetMessage("102002", String.Join(", ", listScode), String.Join(", ", dicResult["productIds"].Count)));
            }

            if (dicResult.ContainsKey("errors"))
            {
                listErrorMessage.Add(ErsResources.GetMessage("102008", String.Join(Environment.NewLine, dicResult["errors"])));
            }

            return listErrorMessage.Count == 0 ? null : String.Join(Environment.NewLine, listErrorMessage);
        }

        /// <summary>
        /// パラメータリスト再計算 [Re-calc list of parameter]
        /// </summary>
        /// <param name="listParam">パラメータリスト [The list of parameter]</param>
        /// <returns>パラメータリスト [The list of parameter]</returns>
        protected virtual IList<UpdateStockParam> GetRecalcListParam(IList<UpdateStockParam> listParam)
        {
            // リクエストパラメータに同じ商品コードが複数あった場合に合算する必要がある

            var dicSet = new Dictionary<string, int>();
            var dicAdd = new Dictionary<string, int>();

            foreach (var param in listParam)
            {
                // set + set : NG
                // set + (add or sub) : NG

                if (dicSet.ContainsKey(param.productCode))
                {
                    throw new Exception("Not allowed mixed operation.");
                }

                if (param.operation == EnumMallStockOperation.set)
                {
                    if (dicAdd.ContainsKey(param.productCode))
                    {
                        throw new Exception("Not allowed mixed operation.");
                    }

                    dicSet.Add(param.productCode, param.quantity.Value);
                }
                else
                {
                    if (!dicAdd.ContainsKey(param.productCode))
                    {
                        dicAdd[param.productCode] = 0;
                    }

                    switch (param.operation)
                    {
                        case EnumMallStockOperation.add:
                            dicAdd[param.productCode] += param.quantity.Value;
                            break;

                        case EnumMallStockOperation.sub:
                            dicAdd[param.productCode] -= param.quantity.Value;
                            break;
                    }
                }
            }

            var listRet = new List<UpdateStockParam>();

            // set
            foreach (var key in dicSet.Keys)
            {
                var param = default(UpdateStockParam);

                param.productCode = key;
                param.quantity = dicSet[key];
                param.operation = EnumMallStockOperation.set;

                listRet.Add(param);
            }

            // add
            foreach (var key in dicAdd.Keys)
            {
                if (dicAdd[key] == 0)
                {
                    continue;
                }

                var param = default(UpdateStockParam);

                param.productCode = key;
                param.quantity = dicAdd[key];
                param.operation = EnumMallStockOperation.add;

                listRet.Add(param);
            }

            return listRet;
        }

        /// <summary>
        /// ERSでHarcの在庫数を保持する
        /// </summary>
        /// <param name="objDB"></param>
        /// <param name="listParam"></param>
        private void UpdateErsMallStock(ErsDatabase objDB, IList<UpdateStockParam> listParam)
        {
            var updateStrategy = ErsMallFactory.ersMallStockFactory.GetUpdateErsMallStockStgy();

            foreach (var product in listParam)
            {
                updateStrategy.Update(objDB, product.productCode, product.operation, product.quantity.Value);
            }
        }
    }
}
