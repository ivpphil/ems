﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.stock.strategy
{
    public class UpdateErsMallStockStgy
        : ISpecificationForSQL
    {
        public void Update(ErsDatabase objDB, string scode, EnumMallStockOperation? operation, int amount)
        {
            switch (operation)
            {
                case EnumMallStockOperation.add:
                    this.IncreaseStock(objDB, scode, amount);
                    break;

                case EnumMallStockOperation.sub:
                    this.DecreaseStock(objDB, scode, amount);
                    break;

                case EnumMallStockOperation.set:
                    this.SetStock(objDB, scode, amount);
                    break;
            }
        }

        protected virtual string scode { get; set; }
        protected virtual int amount { get; set; }

        private void IncreaseStock(ErsDatabase objDB, string scode, int amount)
        {
            this.scode = scode;
            this.amount = amount;
            var result = ErsRepository.UpdateSatisfying(objDB, this, null);
        }

        private void DecreaseStock(ErsDatabase objDB, string scode, int amount)
        {
            this.scode = scode;
            this.amount = -amount;
            var result = ErsRepository.UpdateSatisfying(objDB, this, null);
        }

        private void SetStock(ErsDatabase objDB, string scode, int amount)
        {
            var repository = ErsMallFactory.ersMallStockFactory.GetErsMallStockRepository(objDB);
            var criteria = ErsMallFactory.ersMallStockFactory.GetErsMallStockCriteria();
            criteria.scode = scode;
            criteria.ForUpdate = true;

            var newMallStock = repository.FindSingle(criteria);
            var oldMallStock = ErsMallFactory.ersMallStockFactory.GetErsMallStock();
            oldMallStock.OverwriteWithParameter(newMallStock.GetPropertiesAsDictionary());

            newMallStock.stock = amount;

            repository.Update(oldMallStock, newMallStock);
        }

        public string asSQL()
        {
            var retString = "UPDATE mall_stock_t SET stock = stock + " + amount + " WHERE scode = '" + scode + "' ";

            return retString;
        }
    }
}
