﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.stock
{
    public class ErsMallStock
        : ErsRepositoryEntity
    {
        public override int? id { get; set; }

        public string scode { get; set; }

        public int? stock { get; set; }
    }
}
