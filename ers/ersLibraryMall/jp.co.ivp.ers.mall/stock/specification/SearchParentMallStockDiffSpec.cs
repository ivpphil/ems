﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.stock.specification
{
    public class SearchParentMallStockDiffSpec
        : ISpecificationForSQL
    {
        public IList<Dictionary<string, object>> GetSearchData(string scode)
        {
            this._scode = scode;

            return ErsRepository.SelectSatisfying(this, null);
        }
        private string _scode { get; set; }



        public string asSQL()
        {
            var strSQL = @"--親在庫の現在モール在庫を算出して差分があるもの
            SELECT parent_scode, TRUNC(MIN(stock_t.stock / set_master_t.amount)) - COALESCE(mall_stock_t.stock) AS diffStock
            FROM set_master_t 
            INNER JOIN stock_t ON stock_t.scode = set_master_t.scode
            INNER JOIN mall_stock_t ON set_master_t.parent_scode = mall_stock_t.scode
            WHERE parent_scode IN (SELECT parent_scode FROM set_master_t WHERE scode = '" + this._scode + @"')
            GROUP BY parent_scode, mall_stock_t.stock
            HAVING COALESCE(mall_stock_t.stock) <> TRUNC(MIN(stock_t.stock / set_master_t.amount ))
            ";

            return strSQL;
        }
    }
}
