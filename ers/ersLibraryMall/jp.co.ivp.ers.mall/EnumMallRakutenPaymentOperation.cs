﻿
namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// 楽天決済種別 [Type of payment for credit card payment (Rakuten)]
    /// </summary>
    public enum EnumMallRakutenPaymentOperation
    {
        /// <summary>
        /// 0 : オーソリ [Auth]
        /// </summary>
        authorizeCredit = 0,

        /// <summary>
        /// 1 : 請求実行 [Sales]
        /// </summary>
        requestCredit = 1,

        /// <summary>
        /// 2 : 取消実行 [Cancel]
        /// </summary>
        cancelRequestCredit = 2,
    }
}
