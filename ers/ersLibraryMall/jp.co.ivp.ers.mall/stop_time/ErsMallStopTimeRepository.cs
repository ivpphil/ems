﻿using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.stop_time
{
    /// <summary>
    /// モール連携停止時間リポジトリ [Repository for mall stop time table]
    /// </summary>
    public class ErsMallStopTimeRepository
        : ErsRepository<ErsMallStopTime>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public ErsMallStopTimeRepository()
            : base("mall_stop_time_t")
        {
        }
    }
}
