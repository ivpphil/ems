﻿using System;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.stop_time
{
    /// <summary>
    /// モール連携停止時間クライテリア [Criteria for mall stop time table]
    /// </summary>
    public class ErsMallStopTimeCriteria
        : Criteria
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_stop_time_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 停止日時（FROM） [Datetime of stop (From)]
        /// </summary>
        public virtual DateTime? stop_from_less_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_stop_time_t.stop_from", value, Operation.LESS_EQUAL));
            }
        }

        /// <summary>
        /// 停止日時（TO） [Datetime of stop (To)]
        /// </summary>
        public virtual DateTime? stop_to_greater_equal
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_stop_time_t.stop_to", value, Operation.GREATER_EQUAL));
            }
        }

        /// <summary>
        /// 店舗タイプ [The type of mall shop]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_stop_time_t.mall_shop_kbn", Convert.ToInt32(value), Operation.EQUAL));
            }
        }

        /// <summary>
        /// 機能タイプ [The type of mall function]
        /// </summary>
        public virtual EnumMallFuncType? mall_func_type
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_stop_time_t.mall_func_type", Convert.ToInt32(value), Operation.EQUAL));
            }
        }


        /// <summary>
        /// IDソート [Sort by ID]
        /// </summary> 
        public void SetOrderById(OrderBy orderBy)
        {
            AddOrderBy("mall_stop_time_t.id", orderBy);
        }
    }
}
