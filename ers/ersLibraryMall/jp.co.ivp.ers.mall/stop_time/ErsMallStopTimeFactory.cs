﻿using System.Collections.Generic;
using jp.co.ivp.ers.mall.stop_time.strategy;

namespace jp.co.ivp.ers.mall.stop_time
{
    /// <summary>
    /// モール停止時間関連ファクトリ [Factory for stop time of mall]
    /// </summary>
    public class ErsMallStopTimeFactory
    {
        /// <summary>
        /// モール連携停止時間エンティティ取得 [Get mall stop time]
        /// </summary>
        /// <returns>ErsMallStopTime</returns>
        public virtual ErsMallStopTime GetErsMallStopTime()
        {
            return new ErsMallStopTime();
        }

        /// <summary>
        /// モール連携停止時間リポジトリ取得 [Get mall stop time repository]
        /// </summary>
        /// <returns>ErsMallStockErrorRepository</returns>
        public virtual ErsMallStopTimeRepository GetErsMallStopTimeRepository()
        {
            return new ErsMallStopTimeRepository();
        }

        /// <summary>
        /// モール連携停止時間クライテリア取得 [Get mall stop time criteria]
        /// </summary>
        /// <returns>ErsMallStopTimeCriteria</returns>
        public virtual ErsMallStopTimeCriteria GetErsMallStopTimeCriteria()
        {
            return new ErsMallStopTimeCriteria();
        }

        /// <summary>
        /// モール連携停止時間エンティティ取得（パラメータから） [Get mall stop time entity (from parameters)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsMallStopTime</returns>
        public virtual ErsMallStopTime GetErsMallStopTimeWithParameters(Dictionary<string, object> dicParams)
        {
            var obj = this.GetErsMallStopTime();
            obj.OverwriteWithParameter(dicParams);
            return obj;
        }

        /// <summary>
        /// モール連携停止時間エンティティ取得（IDから） [Get mall stop time entity (from ID)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsMallStockError</returns>
        public virtual ErsMallStopTime GetErsMallStopTimeWithID(int id)
        {
            var repository = this.GetErsMallStopTimeRepository();

            var criteria = this.GetErsMallStopTimeCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            return list.Count > 0 ? list[0] : null;
        }


        /// <summary>
        /// モール処理除外日時内判定クラス取得 [Get the class of Judgement within exclude dateTime for mall function]
        /// </summary>
        /// <returns>モール処理除外日時内判定クラスインスタンス [Instance for IsWithinExcludeDateTimeStgy]</returns>
        public virtual IsWithinExcludeDateTimeStgy GetIsWithinExcludeDateTimeStgy()
        {
            return new IsWithinExcludeDateTimeStgy();
        }
    }
}
