﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mall.stop_time.strategy
{
    /// <summary>
    /// モール処理除外日時内判定 [Judgement within exclude dateTime for mall function]
    /// </summary>
    public class IsWithinExcludeDateTimeStgy
    {
        /// <summary>
        /// モール処理除外日時内判定 [Judgement within exclude dateTime for mall function]
        /// </summary>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="funcType">機能タイプ [Type of function]</param>
        /// <param name="dateNow">現在日時 [DateTime Now]</param>
        /// <returns>True : 除外 [Exclude] / False : 除外しない [Not exlude]</returns>
        public bool IsWithin(EnumMallShopKbn? shopKbn, EnumMallFuncType funcType, DateTime dateNow)
        {
            var repository = ErsMallFactory.ersMallStopTimeFactory.GetErsMallStopTimeRepository();
            var criteria = ErsMallFactory.ersMallStopTimeFactory.GetErsMallStopTimeCriteria();

            criteria.stop_from_less_equal = dateNow;
            criteria.stop_to_greater_equal = dateNow;
            criteria.mall_shop_kbn = shopKbn;
            criteria.mall_func_type = funcType;

            return repository.GetRecordCount(criteria) > 0;
        }
    }
}