﻿using System;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.stop_time
{
    /// <summary>
    /// モール連携停止時間エンティティ [Entity for mall stop time table]
    /// </summary>
    public class ErsMallStopTime
        : ErsRepositoryEntity
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 登録日時 [Datetime of register]
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新日時 [Datetime of update]
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// 停止日時（FROM） [Datetime of stop (From)]
        /// </summary>
        public virtual DateTime? stop_from { get; set; }

        /// <summary>
        /// 停止日時（TO） [Datetime of stop (To)]
        /// </summary>
        public virtual DateTime? stop_to { get; set; }

        /// <summary>
        /// 店舗タイプ [The type of mall shop]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// 機能タイプ [The type of mall function]
        /// </summary>
        public virtual EnumMallFuncType? mall_func_type { get; set; }
    }
}
