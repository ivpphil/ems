﻿
namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// Enums for amazon feed type
    /// </summary>
    public enum EnumMallAmazonFeedType
    {
        /// <summary>
        /// 商品フィード
        /// </summary>
        _POST_PRODUCT_DATA_,

        /// <summary>
        /// 関係性フィード
        /// </summary>
        _POST_PRODUCT_RELATIONSHIP_DATA_,

        /// <summary>
        /// 単一フォーマット 商品フィード
        /// </summary>
        _POST_ITEM_DATA_,

        /// <summary>
        /// オーバーライドフィード
        /// </summary>
        _POST_PRODUCT_OVERRIDES_DATA_,

        /// <summary>
        /// 画像フィード
        /// </summary>
        _POST_PRODUCT_IMAGE_DATA_,

        /// <summary>
        /// 価格フィード
        /// </summary>
        _POST_PRODUCT_PRICING_DATA_,

        /// <summary>
        /// 在庫フィード
        /// </summary>
        _POST_INVENTORY_AVAILABILITY_DATA_,

        /// <summary>
        /// 注文確認フィード
        /// </summary>
        _POST_ORDER_ACKNOWLEDGEMENT_DATA_,

        /// <summary>
        /// フルフィルメントフィード
        /// </summary>
        _POST_ORDER_FULFILLMENT_DATA_,

        /// <summary>
        /// FBA マルチチャネルサービス フルフィルメントフィード
        /// </summary>
        _POST_FULFILLMENT_ORDER_REQUEST_DATA_,

        /// <summary>
        /// FBA マルチチャネルサービス キャンセルフィード
        /// </summary>
        _POST_FULFILLMENT_ORDER_CANCELLATION_REQUEST_DATA,

        /// <summary>
        /// 注文修正フィード
        /// </summary>
        _POST_PAYMENT_ADJUSTMENT_DATA_,

        /// <summary>
        /// 請求確認フィード
        /// </summary>
        _POST_INVOICE_CONFIRMATION_DATA_,

        /// <summary>
        /// ACES 3.0 データ(Automotive Part Finder)フィード。JPでは使用されていません。
        /// </summary>
        _POST_STD_ACES_DATA_,

        /// <summary>
        /// フラットファイル出品フィード / フラットファイル ビデオの商品登録ファイル
        /// </summary>
        _POST_FLAT_FILE_LISTINGS_DATA_,

        /// <summary>
        /// フラットファイル注文確認フィード
        /// </summary>
        _POST_FLAT_FILE_ORDER_ACKNOWLEDGEMENT_DATA_,

        /// <summary>
        /// フラットファイル フルフィルメントフィード
        /// </summary>
        _POST_FLAT_FILE_FULFILLMENT_DATA_,

        /// <summary>
        /// フラットファイルFBA マルチチャネルサービス フルフィルメントフィード
        /// </summary>
        _POST_FLAT_FILE_FULFILLMENT_ORDER_REQUEST_DATA_,

        /// <summary>
        /// フラットファイルFBA マルチチャネルサービス キャンセルフィード
        /// </summary>
        _POST_FLAT_FILE_FULFILLMENT_ORDER_CANCELLATION_REQUEST_DATA_,

        /// <summary>
        /// フラットファイルFBA納品作成フィード
        /// </summary>
        _POST_FLAT_FILE_FBA_CREATE_INBOUND_SHIPMENT_,

        /// <summary>
        /// フラットファイルFBA納品更新フィード
        /// </summary>
        _POST_FLAT_FILE_FBA_UPDATE_INBOUND_SHIPMENT_,

        /// <summary>
        /// フラットファイルFBA納品通知フィード
        /// </summary>
        _POST_FLAT_FILE_FBA_SHIPMENT_NOTIFICATION_FEED_,

        /// <summary>
        /// フラットファイルFBA返送/所有権の放棄依頼作成フィード
        /// </summary>
        _POST_FLAT_FILE_FBA_CREATE_REMOVAL_,

        /// <summary>
        /// フラットファイル注文修正フィード
        /// </summary>
        _POST_FLAT_FILE_PAYMENT_ADJUSTMENT_DATA_,

        /// <summary>
        /// フラットファイル請求確認フィード
        /// </summary>
        _POST_FLAT_FILE_INVOICE_CONFIRMATION_DATA_,

        /// <summary>
        /// フラットファイル 出品ファイル
        /// </summary>
        _POST_FLAT_FILE_INVLOADER_DATA_,

        /// <summary>
        /// フラットファイル ミュージックの商品
        /// </summary>
        _POST_FLAT_FILE_CONVERGENCE_LISTINGS_DATA_,

        /// <summary>
        /// フラットファイル 本の商品登録ファイル
        /// </summary>
        _POST_FLAT_FILE_BOOKLOADER_DATA_,

        /// <summary>
        /// フラットファイル 価格と数量更新ファイル
        /// </summary>
        _POST_FLAT_FILE_PRICEANDQUANTITYONLY_UPDATE_DATA_,

        /// <summary>
        /// Product Adsフラットファイル ※日本では未使用
        /// </summary>
        _POST_FLAT_FILE_SHOPZILLA_DATA_,

        /// <summary>
        /// UIEE 在庫ファイル
        /// </summary>
        _POST_UIEE_BOOKLOADER_DATA_
    }
}
