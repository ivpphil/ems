﻿
namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// 在庫更新ステータス [Status of update stock]
    /// </summary>
    public enum EnumMallStockPostingSlipStatus
    {
        /// <summary>
        /// -1 : 実行中 [Processing]
        /// </summary>
        Processing = -1,

        /// <summary>
        /// 0 : 正常終了 [Success]
        /// </summary>
        Success = 0,

        /// <summary>
        /// 1 : 異常終了 [Failure]
        /// </summary>
        Failure = 1,
    }
}
