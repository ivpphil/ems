﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mall
{
    /// <summary>
    /// モール商品アップロードファイルタイプ [Mall product upload file type]
    /// </summary>
    public enum EnumMallProductUploadFileType
    {
        /// <summary>
        /// 楽天：商品 [Rakuten : Product]
        /// </summary>
        RAKUTEN_ITEM = 100,

        /// <summary>
        /// 楽天：カテゴリ [Rakuten : Category]
        /// </summary>
        RAKUTEN_CATEGORY = 101,

        /// <summary>
        /// 楽天：画像 [Rakuten : Image]
        /// </summary>
        RAKUTEN_IMAGE = 150,


        /// <summary>
        /// Yahoo!：商品 [Yahoo! : Product]
        /// </summary>
        YAHOO_ITEM = 200,

        /// <summary>
        /// Yahoo!：画像 [Yahoo! : Image]
        /// </summary>
        YAHOO_IMAGE = 250,


        /// <summary>
        /// Amazon：商品 [Amazon : Product]
        /// </summary>
        AMAZON_ITEM = 300,
    }
}
