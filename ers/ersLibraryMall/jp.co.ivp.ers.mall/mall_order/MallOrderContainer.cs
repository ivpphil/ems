﻿using System;
using System.Collections.Generic;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票コンテナ [Mall order container]
    /// </summary>
    public class MallOrderContainer
    {
        /// <summary>
        /// モール伝票ヘッダ [Mall order hearder]
        /// </summary>
        public virtual ErsMallOrder objMallOrder { get; set; }

        /// <summary>
        /// モール伝票明細リスト [List of mall order detail]
        /// </summary>
        public virtual IList<ErsMallOrderDetail> listMallOrderDetail { get; set; }
    }
}
