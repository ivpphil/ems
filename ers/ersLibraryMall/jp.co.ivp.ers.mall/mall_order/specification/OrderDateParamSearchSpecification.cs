﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order.specification
{
    public class OrderDateParamSearchSpecification
        : ISearchSpecification
    {
        /// <summary>
        /// 検索データ取得
        /// </summary>
        /// <param name="crtOrderDateParam">クライテリア</param>
        /// <returns>データテーブル</returns>
        public virtual List<Dictionary<string, object>> GetSearchData(Criteria criteria)
        {
            var specificationForSQL = new OrderDateParamSearchRecordSpecification();

            criteria.AddGroupBy("import_time");
            criteria.AddGroupBy("site_id");
            criteria.AddGroupBy("mall_shop_kbn");

            return ErsRepository.SelectSatisfying(specificationForSQL, criteria);
        }

        /// <summary>
        /// レコード数取得
        /// </summary>
        /// <param name="crtOrderDateParam">クライテリア</param>
        /// <returns>データテーブル</returns>
        public virtual int GetCountData(Criteria criteria)
        {
            var specificationForSQL = new OrderDateParamSearchCountSpecification();

            var record = ErsRepository.SelectSatisfying(specificationForSQL, criteria);

            if (record.Count == 0)
                return 0;

            return Convert.ToInt32(record[0]["count"]);
        }

        /// <summary>
        /// 
        /// </summary>
        internal protected class OrderDateParamSearchRecordSpecification
           : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                return " SELECT MIN(import_time) AS import_time FROM mall_order_import_t ";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        internal protected class OrderDateParamSearchCountSpecification
            : ISpecificationForSQL
        {
            /// <summary>
            /// SQL文
            /// </summary>
            /// <returns>SQL文</returns>
            public virtual string asSQL()
            {
                return " SELECT COUNT(import_time) AS count FROM mall_order_import_t ";
            }
        }
    }
}