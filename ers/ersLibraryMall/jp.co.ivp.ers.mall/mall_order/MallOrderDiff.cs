﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票取込み差分 [Mall order diff]
    /// </summary>
    public class MallOrderDiff
    {
        /// <summary>
        /// 伝票ヘッダ差分カラム名 [Diff columns name of header]
        /// </summary>
        public virtual IList<string> listColsChangedHeader { get; set; }

        /// <summary>
        /// 削除明細アイテムコード名 [Deleted Item code name]
        /// </summary>
        public virtual IList<string> listDelItemCodesDetail { get; set; }

        /// <summary>
        /// 追加明細アイテムコード名 [Added Item code name]
        /// </summary>
        public virtual IList<string> listAddItemCodesDetail { get; set; }

        /// <summary>
        /// 伝票明細差分カラム名ディクショナリ [Dictionary of diff columns name of detail]
        /// </summary>
        public virtual Dictionary<string, IList<string>> dicListColsChangedDetail { get; set; }

        /// <summary>
        /// 伝票明細数量差分 [Dictionary of quantity diff of detail]
        /// </summary>
        public virtual Dictionary<string, int> dicQuantityDiffDetail { get; set; }

        /// <summary>
        /// 更新前ステータス [Old order status]
        /// </summary>
        public virtual string oldOrderStatus { get; set; }


        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public MallOrderDiff()
        {
            this.listColsChangedHeader = null;
            this.listDelItemCodesDetail = new List<string>();
            this.listAddItemCodesDetail = new List<string>();
            this.dicListColsChangedDetail = new Dictionary<string, IList<string>>();
            this.dicQuantityDiffDetail = new Dictionary<string, int>();
        }
    }
}
