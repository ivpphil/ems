﻿using System;
using System.Collections;
using System.Collections.Generic;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// 楽天国マスタエンティティ [Entity for raukten country table]
    /// </summary>
    public class ErsMallRakutenCountry
        : ErsRepositoryEntity
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 国 [Country name]
        /// </summary>
        public virtual string country { get; set; }

        /// <summary>
        /// アクティブ [Active flag]
        /// </summary>
        public virtual EnumActive active { get; set; }
    }
}
