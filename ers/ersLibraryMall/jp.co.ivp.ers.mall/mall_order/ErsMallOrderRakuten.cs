﻿using System;
using System.Collections;
using System.Collections.Generic;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// 楽天市場モール伝票ヘッダエンティティ [Rakuten Entity for mall order table]
    /// </summary>
    public class ErsMallOrderRakuten
        : ErsMallOrder
    {
        /// <summary>
        /// プロパティへの値セット [Set properties]
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="d_no"></param>
        /// <param name="site_id"></param>
        /// <param name="mall_shop_kbn"></param>
        public override void SetProperties(Dictionary<string, object> dictionary, string d_no, int? site_id, EnumMallShopKbn? mall_shop_kbn)
        {
            //strategy取得
            var convertStgy = ErsMallFactory.ersMallOrderFactory.GetConvertArrayStgy();

            //RSA暗号化クラス
            var encObj = ErsFactory.ersUtilityFactory.getErsEncryption();

            var other = (Dictionary<string, object>)dictionary["other"];

            if (other.ContainsKey("card_info2"))
            {
                this.r_card_info2 = Convert.ToString(other["card_info2"]);
            }
            if (other.ContainsKey("card_info3"))
            {
                this.r_card_info3 = encObj.HexEncode(Convert.ToString(other["card_info3"]));
            }
            if (other.ContainsKey("card_info4"))
            {
                this.r_card_info4 = Convert.ToString(other["card_info4"]);
            }
            if (other.ContainsKey("card_info5"))
            {
                this.r_card_info5 = Convert.ToString(other["card_info5"]);
            }
            if (other.ContainsKey("payment_status"))
            {
                this.r_payment_status = Convert.ToString(other["payment_status"]);
            }
            if (other.ContainsKey("coupon"))
            {
                this.r_coupon = convertStgy.ConvertToStringFromArrayListMultilayer((ArrayList)other["coupon"], "{0} = {1}", "; ", ", ");
            }
            if (other.ContainsKey("order_type"))
            {
                this.r_order_type = convertStgy.ConvertToStringFromArrayList((ArrayList)other["order_type"]);
            }
            if (other.ContainsKey("order_pending"))
            {
                this.r_order_pending = Convert.ToString(other["order_pending"]);
            }
            if (other.ContainsKey("tracking_user"))
            {
                this.r_tracking_user = Convert.ToString(other["tracking_user"]);
            }
            if (other.ContainsKey("caution"))
            {
                this.r_caution = Convert.ToString(other["caution"]);
            }

            base.SetProperties(dictionary, d_no, site_id, mall_shop_kbn);
        }
    }
}
