﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// DeNAモール伝票ヘッダエンティティ [DeNA Entity for mall order table]
    /// </summary>
    public class ErsMallOrderDeNA
        : ErsMallOrder
    {
        /// <summary>
        /// プロパティへの値セット [Set properties]
        /// </summary>
        public override void SetProperties(Dictionary<string, object> dictionary, string d_no, int? site_id, EnumMallShopKbn? mall_shop_kbn)
        {
            var other = (Dictionary<string, object>)dictionary["other"];

            if (other.ContainsKey("card_type"))
            {
                this.d_card_type = Convert.ToString(other["card_type"]);
            }
            if (other.ContainsKey("card_number"))
            {
                this.d_card_number = Convert.ToString(other["card_number"]);
            }
            if (other.ContainsKey("card_expire_date"))
            {
                this.d_card_expire_date = Convert.ToString(other["card_expire_date"]);
            }
            if (other.ContainsKey("card_owner"))
            {
                this.d_card_owner = Convert.ToString(other["card_owner"]);
            }
            if (other.ContainsKey("card_owner_birthday"))
            {
                this.d_card_owner_birthday = Convert.ToString(other["card_owner_birthday"]);
            }
            if (other.ContainsKey("payment_status"))
            {
                this.d_payment_status = Convert.ToString(other["payment_status"]);
            }

            base.SetProperties(dictionary, d_no, site_id, mall_shop_kbn);
        }
    }
}
