﻿using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票取り込み管理リポジトリ [Repository for mall order import]
    /// </summary>
    public class ErsMallOrderImportRepository
        : ErsRepository<ErsMallOrderImport>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public ErsMallOrderImportRepository()
            : base("mall_order_import_t")
        {
        }
    }
}
