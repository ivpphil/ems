﻿using System.Collections.Generic;

namespace jp.co.ivp.ers.mall.mall_order.payment.strategy
{
    /// <summary>
    /// モール伝票決済処理 [Execute mall order payment]
    /// </summary>
    public class ExecuteMallOrderPaymentStgy
    {
        /// <summary>
        /// モール伝票決済処理 [Execute mall order payment]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Shop type]</param>
        /// <param name="param">パラメータ [Parameter]</param>
        public void ExecuteMallOrderPayment(int? siteId, EnumMallShopKbn? shopKbn, MallOrderPaymentParam param)
        {
            // HARCログイン [Log in to HARC]
            var request = ErsMallFactory.ersMallCommonFactory.GetHarcLoginStgy().HarcLogin();

            // 更新クラスディクショナリ取得 [Get the dictionary for update class]
            var updateExecute = this.GetExecuteClassDic(siteId, shopKbn);

            if (updateExecute == null)
            {
                return;
            }

            // 決済実行 [Execute payment]
            updateExecute.ExecutePayment(request, param);
        }

        /// <summary>
        /// 処理クラスディクショナリ取得 [Get the dictionary for execute class]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Shop type]</param>
        /// <returns>処理クラスディクショナリ [The dictionary for execute class]</returns>
        protected ExecuteMallOrderPaymentBase GetExecuteClassDic(int? siteId, EnumMallShopKbn? shopKbn)
        {
            switch (shopKbn)
            {
                case EnumMallShopKbn.RAKUTEN:
                    // 楽天 [Rakuten]
                    return new ExecuteMallOrderPaymentRakuten(siteId, EnumMallShopKbn.RAKUTEN);
                case EnumMallShopKbn.YAHOO:
                    // Yahoo! [Yahoo!]
                    return new ExecuteMallOrderPaymentYahoo(siteId, EnumMallShopKbn.YAHOO);
                default:
                    return null;
            }
        }
    }
}
