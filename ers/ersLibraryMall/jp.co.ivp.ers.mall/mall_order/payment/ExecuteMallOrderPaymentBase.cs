﻿using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.mall_order.payment
{
    /// <summary>
    /// モール伝票決済クラス（基底） [Class for execute mall order payment (Base)]
    /// </summary>
    public class ExecuteMallOrderPaymentBase
    {
        #region 基本パラメータ [Basic parameters]
        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public virtual int? siteId { get; protected set; }

        /// <summary>
        /// 店舗タイプ [Shop type]
        /// </summary>
        public virtual EnumMallShopKbn? shopKbn { get; protected set; }
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public ExecuteMallOrderPaymentBase(int? siteId, EnumMallShopKbn? shopKbn)
        {
            this.siteId = siteId;
            this.shopKbn = shopKbn;
        }
        #endregion

        #region 決済実行 [Execute payment]
        /// <summary>
        /// 決済実行 [Execute payment]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        public virtual void ExecutePayment(HarcApiRequest request, MallOrderPaymentParam param)
        {
        }
        #endregion
    }
}
