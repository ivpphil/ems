﻿using System;

namespace jp.co.ivp.ers.mall.mall_order.payment
{
    /// <summary>
    /// モール伝票決済パラメータ [Parameter for mall order payment]
    /// </summary>
    public class MallOrderPaymentParam
    {
        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public virtual int? siteId { get; set; }

        /// <summary>
        /// 店舗タイプ [Shop type]
        /// </summary>
        public virtual EnumMallShopKbn? shopKbn { get; set; }

        /// <summary>
        /// 受注番号 [Order code]
        /// </summary>
        public virtual string order_code { get; set; }

        /// <summary>
        /// 受注日時 [Order date]
        /// </summary>
        public virtual DateTime? order_date { get; set; }

        /// <summary>
        /// クレジットカード決済操作 [Type of operation for credit card payment]
        /// </summary>
        public virtual EnumMallCardPaymentOperation? operation { get; set; }


        /// <summary>
        /// 決済金額 [Payment price]
        /// </summary>
        public virtual int? payment_price { get; set; }

        /// <summary>
        /// 支払い方法 [Payment method]
        /// </summary>
        public virtual int? payment_method { get; set; }

        /// <summary>
        /// 分割回数 [Payment count]
        /// </summary>
        public virtual int? payment_count { get; set; }

        /// <summary>
        /// 備考 [Remarks]
        /// </summary>
        public virtual string remarks { get; set; }


        /// <summary>
        /// キャンセル理由 [Cancel reason]
        /// </summary>
        public EnumMallYahooCardPaymentCancelReason? cancel_reason { get; set; }
    }
}
