﻿using System;
using System.Collections.Generic;
using com.hunglead.harc;
using jp.co.ivp.ers.mall.api;
using jp.co.ivp.ers.mall.api.payment;

namespace jp.co.ivp.ers.mall.mall_order.payment
{
    /// <summary>
    /// モール伝票決済クラス（楽天） [Class for execute mall order payment (Rakuten)]
    /// </summary>
    public class ExecuteMallOrderPaymentRakuten
        : ExecuteMallOrderPaymentBase
    {
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public ExecuteMallOrderPaymentRakuten(int? siteId, EnumMallShopKbn? shopKbn)
            : base(siteId, shopKbn)
        {
        }
        #endregion

        #region 決済実行 [Execute payment]
        /// <summary>
        /// 決済実行 [Execute payment]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        public override void ExecutePayment(HarcApiRequest request, MallOrderPaymentParam param)
        {
            switch (param.operation)
            {
                // 1 : オーソリ [Authorization]
                case EnumMallCardPaymentOperation.Auth:
                    this.ExecuteAuth(request, param);
                    break;

                // 2 : オーソリキャンセル [Cancel authorization]
                case EnumMallCardPaymentOperation.CancelAuth:
                    this.ExecuteCancelAuth(request, param);
                    break;

                // 3 : 売上確定 [Sales]
                case EnumMallCardPaymentOperation.Sales:
                    this.ExecuteSales(request, param);
                    break;

                // 4 : 売上キャンセル [Cancel sales]
                case EnumMallCardPaymentOperation.CancelSales:
                    this.ExecuteCancelSales(request, param);
                    break;

                // 5 : 再オーソリ [Reauthorization]
                case EnumMallCardPaymentOperation.Reauth:
                    throw new Exception(ErsResources.GetMessage("106601", this.siteId, (int)this.shopKbn, param.order_code, "Not supported operation"));

                // 100 : オーソリ＋売上確定 [Authorization + Sales]
                case EnumMallCardPaymentOperation.AuthAndSales:
                    this.ExecuteAuthAndSales(request, param);
                    break;

                default:
                    throw new Exception(ErsResources.GetMessage("106001", this.siteId, (int)this.shopKbn, param.order_code, (int)param.operation));
            }
        }
        #endregion

        #region 決済操作実行 [Execute payment operation]
        /// <summary>
        /// 決済操作実行 [Execute payment operation]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        /// <param name="paymentOperation">楽天決済種別 [Type of payment for credit card payment (Rakuten)]</param>
        /// <returns>結果情報 [Result information]</returns>
        protected virtual Dictionary<string, object> ExecutePaymentOperation(HarcApiRequest request, MallOrderPaymentParam param, EnumMallRakutenPaymentOperation paymentOperation)
        {
            var ordesParam = default(UpdateRakutenOrderInfosParam);

            // 決済情報パラメータセット [Set the parameter of payment information]
            ordesParam.orderCode = param.order_code;
            ordesParam.orderDate = param.order_date;
            ordesParam.paymentOperation = paymentOperation;

            try
            {
                // 楽天受注データ更新API取得 [Get the API for update order (Rakuten)]
                var dicResult = this.GetUpdateRakutenOrderInfosAPI().ExecutePayment(request, new List<UpdateRakutenOrderInfosParam>() { ordesParam });

                foreach (var data in dicResult)
                {
                    if (Convert.ToString(data["orderCode"]) == param.order_code &&
                        Convert.ToString(data["orderDate"]) == param.order_date.Value.ToString("yyyy/MM/dd"))
                    {
                        return data;
                    }
                }
            }
            catch (APIFailedException e)
            {
                throw e;
            }

            return null;
        }

        /// <summary>
        /// 楽天受注データ更新API取得 [Get the API for update order (Rakuten)]
        /// </summary>
        /// <returns>楽天受注データ更新API [The API for update order (Rakuten)]</returns>
        protected virtual UpdateRakutenOrderInfosAPI GetUpdateRakutenOrderInfosAPI()
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetUpdateRakutenOrderInfosAPIParam();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);

            return ErsMallFactory.ersMallAPIFactory.GetUpdateRakutenOrderInfosAPI(apiParam);
        }
        #endregion

        #region オーソリ [Authorization]
        /// <summary>
        /// オーソリ [Authorization]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        /// <returns>結果情報 [Result information]</returns>
        protected virtual Dictionary<string, object> ExecuteAuth(HarcApiRequest request, MallOrderPaymentParam param)
        {
            try
            {
                // 決済操作実行 [Execute payment operation]
                var dicResult = this.ExecutePaymentOperation(request, param, EnumMallRakutenPaymentOperation.authorizeCredit);

                string result = dicResult.ContainsKey("result") ? Convert.ToString(dicResult["result"]) : string.Empty;
                bool isError = true;

                // 結果判定 [Judgment result]
                if (Convert.ToBoolean(dicResult["status"]))
                {
                    if (result == UpdateRakutenOrderInfosAPI.RESULT_MESSAGE_SUCCESS)
                    {
                        isError = false;
                    }
                }

                if (isError)
                {
                    string message = ErsResources.GetMessage("106203", this.siteId, (int)this.shopKbn, param.order_code, dicResult["status"], result);
                    throw new Exception(message);
                }

                return dicResult;
            }
            catch (APIFailedException e)
            {
                string errorMessage = ErsResources.GetMessage("106201", this.siteId, (int)this.shopKbn, param.order_code, e.ToString());
                throw new Exception(errorMessage);
            }
        }
        #endregion

        #region オーソリキャンセル [Cancel authorization]
        /// <summary>
        /// オーソリキャンセル [Cancel authorization]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        /// <returns>結果情報 [Result information]</returns>
        protected virtual Dictionary<string, object> ExecuteCancelAuth(HarcApiRequest request, MallOrderPaymentParam objMallOrderPayment)
        {
            try
            {
                // 決済操作実行 [Execute payment operation]
                var dicResult = this.ExecutePaymentOperation(request, objMallOrderPayment, EnumMallRakutenPaymentOperation.cancelRequestCredit);

                string result = dicResult.ContainsKey("result") ? Convert.ToString(dicResult["result"]) : string.Empty;
                bool isError = true;

                // 結果判定 [Judgment result]
                if (Convert.ToBoolean(dicResult["status"]))
                {
                    if (result == UpdateRakutenOrderInfosAPI.RESULT_MESSAGE_SUCCESS)
                    {
                        isError = false;
                    }
                }

                if (isError)
                {
                    string message = ErsResources.GetMessage("106302", this.siteId, (int)this.shopKbn, objMallOrderPayment.order_code, dicResult["status"], result);
                    throw new Exception(message);
                }

                return dicResult;
            }
            catch (APIFailedException e)
            {
                string errorMessage = ErsResources.GetMessage("106301", this.siteId, (int)this.shopKbn, objMallOrderPayment.order_code, e.ToString());
                throw new Exception(errorMessage);
            }
        }
        #endregion

        #region 売上確定 [Sales]
        /// <summary>
        /// 売上確定 [Sales]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        /// <returns>結果情報 [Result information]</returns>
        protected virtual Dictionary<string, object> ExecuteSales(HarcApiRequest request, MallOrderPaymentParam param)
        {
            try
            {
                // 決済操作実行 [Execute payment operation]
                var dicResult = this.ExecutePaymentOperation(request, param, EnumMallRakutenPaymentOperation.requestCredit);

                string result = dicResult.ContainsKey("result") ? Convert.ToString(dicResult["result"]) : string.Empty;
                bool isError = true;

                // 結果判定 [Judgment result]
                if (Convert.ToBoolean(dicResult["status"]))
                {
                    if (result == UpdateRakutenOrderInfosAPI.RESULT_MESSAGE_SUCCESS)
                    {
                        isError = false;
                    }
                }

                if (isError)
                {
                    string message = ErsResources.GetMessage("106402", this.siteId, (int)this.shopKbn, param.order_code, dicResult["status"], result);
                    throw new Exception(message);
                }

                return dicResult;
            }
            catch (APIFailedException e)
            {
                string errorMessage = ErsResources.GetMessage("106401", this.siteId, (int)this.shopKbn, param.order_code, e.ToString());
                throw new Exception(errorMessage);
            }
        }
        #endregion

        #region 売上キャンセル [Cancel sales]
        /// <summary>
        /// 売上キャンセル [Cancel sales]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        /// <returns>結果情報 [Result information]</returns>
        protected virtual Dictionary<string, object> ExecuteCancelSales(HarcApiRequest request, MallOrderPaymentParam param)
        {
            try
            {
                // 決済操作実行 [Execute payment operation]
                var dicResult = this.ExecutePaymentOperation(request, param, EnumMallRakutenPaymentOperation.cancelRequestCredit);

                string result = dicResult.ContainsKey("result") ? Convert.ToString(dicResult["result"]) : string.Empty;
                bool isError = true;

                // 結果判定 [Judgment result]
                if (Convert.ToBoolean(dicResult["status"]))
                {
                    if (result == UpdateRakutenOrderInfosAPI.RESULT_MESSAGE_SUCCESS)
                    {
                        isError = false;
                    }
                }

                if (isError)
                {
                    string message = ErsResources.GetMessage("106502", this.siteId, (int)this.shopKbn, param.order_code, dicResult["status"], result);
                    throw new Exception(message);
                }

                return dicResult;
            }
            catch (APIFailedException e)
            {
                string errorMessage = ErsResources.GetMessage("106501", this.siteId, (int)this.shopKbn, param.order_code, e.ToString());
                throw new Exception(errorMessage);
            }
        }
        #endregion

        #region オーソリ＋売上確定 [Authorization + Sales]
        /// <summary>
        /// オーソリ＋売上確定 [Authorization + Sales]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        protected virtual void ExecuteAuthAndSales(HarcApiRequest request, MallOrderPaymentParam param)
        {
            try
            {
                // 決済操作実行 [Execute payment operation]
                var dicResult = this.ExecutePaymentOperation(request, param, EnumMallRakutenPaymentOperation.authorizeCredit);

                string result = dicResult.ContainsKey("result") ? Convert.ToString(dicResult["result"]) : string.Empty;
                bool isError = true;

                // 結果判定 [Judgment result]
                if (Convert.ToBoolean(dicResult["status"]))
                {
                    // 成功 or オーソリ済みは正常とする [Success or Already authorized is valid]
                    if (result == UpdateRakutenOrderInfosAPI.RESULT_MESSAGE_SUCCESS || 
                        result == UpdateRakutenOrderInfosAPI.RESULT_MESSAGE_ALREADY_AUTHORIZED)
                    {
                        isError = false;
                    }
                }

                if (isError)
                {
                    string message = ErsResources.GetMessage("106203", this.siteId, (int)this.shopKbn, param.order_code, dicResult["status"], result);
                    throw new Exception(message);
                }
            }
            catch (APIFailedException e)
            {
                string errorMessage = ErsResources.GetMessage("106201", this.siteId, (int)this.shopKbn, param.order_code, e.ToString());
                throw new Exception(errorMessage);
            }

            // 決済実行 [Execute payment]
            this.ExecutePaymentOperation(request, param, EnumMallRakutenPaymentOperation.requestCredit);
        }
        #endregion
    }
}
