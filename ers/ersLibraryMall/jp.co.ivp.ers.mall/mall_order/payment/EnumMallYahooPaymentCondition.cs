﻿
namespace jp.co.ivp.ers.mall.mall_order.payment
{
    /// <summary>
    /// 決済状況（Yahoo!） [Payment condition (yahoo!)]
    /// </summary>
    public enum EnumMallYahooPaymentCondition
    {
        /// <summary>
        /// 1 : オーソリOK [Valid auth]
        /// </summary>
        ValidAuth = 1,

        /// <summary>
        /// 2 : オーソリエラー [Invalid auth]
        /// </summary>
        InvalidAuth = 2,

        /// <summary>
        /// 3 : 売上処理中 [Saling]
        /// </summary>
        Saling = 3,

        /// <summary>
        /// 4 : 売上済 [Saled]
        /// </summary>
        Saled = 4,

        /// <summary>
        /// 5 : オーソリ取消 [Canceld auth]
        /// </summary>
        CanceledAuth = 5,

        /// <summary>
        /// 6 : 売上取消 [Canceld sales]
        /// </summary>
        CanceledSales = 6,

        /// <summary>
        /// -1 : 不明 [Unknown]
        /// </summary>
        Unknown = -1,


        /// <summary>
        /// 100 : 未オーソリ [Not auth]
        /// </summary>
        NotAuth = 100
    }
}
