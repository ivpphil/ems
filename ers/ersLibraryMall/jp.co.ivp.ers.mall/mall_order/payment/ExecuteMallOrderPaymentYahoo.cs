﻿using System;
using System.Collections.Generic;
using com.hunglead.harc;
using jp.co.ivp.ers.mall.api;
using jp.co.ivp.ers.mall.api.payment;

namespace jp.co.ivp.ers.mall.mall_order.payment
{
    /// <summary>
    /// モール伝票決済クラス（Yahoo!） [Class for execute mall order payment (Yahoo!)]
    /// </summary>
    public class ExecuteMallOrderPaymentYahoo
        : ExecuteMallOrderPaymentBase
    {
        #region 構造体 [Struct]
        /// <summary>
        /// 決済情報 [Payment information]
        /// </summary>
        protected struct PaymentInfo
        {
            /// <summary>
            /// 決済状況 [Payment condition]
            /// </summary>
            public EnumMallYahooPaymentCondition paymentCondition { get; set; }

            /// <summary>
            /// 決済情報 [Payment information]
            /// </summary>
            public Dictionary<string, object> dicPaymentInfo { get; set; }
        }
        #endregion

        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public ExecuteMallOrderPaymentYahoo(int? siteId, EnumMallShopKbn? shopKbn)
            : base(siteId, shopKbn)
        {
        }
        #endregion

        #region 決済実行 [Execute payment]
        /// <summary>
        /// 決済実行 [Execute payment]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        public override void ExecutePayment(HarcApiRequest request, MallOrderPaymentParam param)
        {
            // 決済情報取得 [Get payment information]
            var paymentInfo = this.GetPaymetInfo(request, param.order_code);

            switch (param.operation)
            {
                // 1 : オーソリ [Authorization]
                case EnumMallCardPaymentOperation.Auth:
                    this.ExecuteAuth(request, param, paymentInfo);
                    break;

                // 2 : オーソリキャンセル [Cancel authorization]
                case EnumMallCardPaymentOperation.CancelAuth:
                    this.ExecuteCancelAuth(request, param, paymentInfo);
                    break;

                // 3 : 売上確定 [Sales]
                case EnumMallCardPaymentOperation.Sales:
                    this.ExecuteSales(request, param, paymentInfo);
                    break;

                // 4 : 売上キャンセル [Cancel sales]
                case EnumMallCardPaymentOperation.CancelSales:
                    this.ExecuteCancelSales(request, param, paymentInfo);
                    break;

                // 5 : 再オーソリ [Reauthorization]
                case EnumMallCardPaymentOperation.Reauth:
                    this.ExecuteReauth(request, param, paymentInfo);
                    break;

                // 100 : オーソリ＋売上確定 [Authorization + Sales]
                case EnumMallCardPaymentOperation.AuthAndSales:
                    this.ExecuteAuthAndSales(request, param, paymentInfo);
                    break;

                default:
                    throw new Exception(ErsResources.GetMessage("106001", this.siteId, (int)this.shopKbn, param.order_code, (int)param.operation));
            }
        }
        #endregion

        #region 決済情報取得 [Get payment information]
        /// <summary>
        /// 決済情報取得 [Get payment information]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="orderCode">受注番号 [Order code]</param>
        /// <returns>決済情報 [Payment information]</returns>
        protected virtual PaymentInfo GetPaymetInfo(HarcApiRequest request, string orderCode)
        {
            try
            {
                // 決済状況取得 [Get payment condition]
                var dicResult = this.GetGetYahoosOrderPaymentInfosByOrderCodeAPI().GetOrderPaymentInfos(request, new List<string>() { orderCode });

                return this.GetPaymentInfo(dicResult[orderCode]);
            }
            catch (APIFailedException e)
            {
                string errorMessage = ErsResources.GetMessage("106100", this.siteId, (int)this.shopKbn, orderCode, e.ToString());
                throw new Exception(errorMessage);
            }
        }

        /// <summary>
        /// Yahoo!決済状況取得API取得 [Get the API for get payment condition (Yahoo!)]
        /// </summary>
        /// <returns>Yahoo!決済状況取得API [The API for get payment condition (Yahoo!)]</returns>
        protected virtual GetYahoosOrderPaymentInfosByOrderCodeAPI GetGetYahoosOrderPaymentInfosByOrderCodeAPI()
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetGetYahoosOrderPaymentInfosByOrderCodeAPIParam();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);

            return ErsMallFactory.ersMallAPIFactory.GetGetYahoosOrderPaymentInfosByOrderCodeAPI(apiParam);
        }

        /// <summary>
        /// 決済情報取得 [Get payment info]
        /// </summary>
        /// <param name="listPaymentInfo">決済情報リスト [The list of payment information]</param>
        /// <returns>決済情報 [Payment info]</returns>
        protected virtual PaymentInfo GetPaymentInfo(List<Dictionary<string, object>> listPaymentInfo)
        {
            PaymentInfo result = default(PaymentInfo);

            foreach (var dataCondition in listPaymentInfo)
            {
                var status = Convert.ToInt32(dataCondition["status"]);

                // 売上済み [Saled]
                if (status == (int)EnumMallYahooPaymentCondition.Saled)
                {
                    result.paymentCondition = EnumMallYahooPaymentCondition.Saled;
                }
                // 売上処理中 [Saling]
                else if (status == (int)EnumMallYahooPaymentCondition.Saling)
                {
                    result.paymentCondition = EnumMallYahooPaymentCondition.Saling;
                }
                // オーソリOK [Valid auth]
                else if (status == (int)EnumMallYahooPaymentCondition.ValidAuth)
                {
                    result.paymentCondition = EnumMallYahooPaymentCondition.ValidAuth;
                }
                // オーソリエラー [Invalid auth]
                else if (status == (int)EnumMallYahooPaymentCondition.InvalidAuth)
                {
                    result.paymentCondition = EnumMallYahooPaymentCondition.InvalidAuth;
                }
                // 未オーソリ [Not auth]
                else
                {
                    result.paymentCondition = EnumMallYahooPaymentCondition.NotAuth;
                }

                result.dicPaymentInfo = dataCondition;
            }

            return result;
        }
        #endregion

        #region オーソリ [Authorization]
        /// <summary>
        /// オーソリ [Authorization]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        /// <param name="paymentInfo">決済情報 [Payment information]</param>
        /// <returns>結果情報 [Result information]</returns>
        protected virtual Dictionary<string, object> ExecuteAuth(HarcApiRequest request, MallOrderPaymentParam param, PaymentInfo paymentInfo)
        {
            // オーソリエラー・未オーソリ以外 [Except Invalid auth / Not auth]
            if (paymentInfo.paymentCondition != EnumMallYahooPaymentCondition.InvalidAuth && paymentInfo.paymentCondition != EnumMallYahooPaymentCondition.NotAuth)
            {
                throw new Exception(ErsResources.GetMessage("106200", this.siteId, (int)this.shopKbn, param.order_code, paymentInfo.paymentCondition.ToString()));
            }

            var paymentInfoParam = default(AuthorizeYahoosOrderPaymentsParam);

            // 決済情報パラメータセット [Set the parameter of payment information]
            paymentInfoParam.paymentId = Convert.ToString(paymentInfo.dicPaymentInfo["paymentId"]);
            paymentInfoParam.orderCode = param.order_code;
            paymentInfoParam.authorizeType = EnumMallYahooCardPaymentAuthorizeType.Add;
            paymentInfoParam.authorizePrice = param.payment_price;
            paymentInfoParam.paymentMethod = param.payment_method;
            paymentInfoParam.paymentCount = param.payment_count;
            paymentInfoParam.remarks = param.remarks;

            try
            {
                // Yahoo!オーソリAPI取得 [Get the API for authorize (Yahoo!)]
                var dicResult = this.GetAuthorizeYahoosOrderPaymentsAPI().Authorize(request, new List<AuthorizeYahoosOrderPaymentsParam>() { paymentInfoParam });

                foreach (var data in dicResult)
                {
                    if (Convert.ToString(data["orderCode"]) == param.order_code &&
                        Convert.ToString(data["paymentId"]) == paymentInfoParam.paymentId)
                    {
                        // 結果判定 [Judgment result]
                        if ((data.ContainsKey("authorizeResult") && !Convert.ToBoolean(data["authorizeResult"])) || !Convert.ToBoolean(data["status"]))
                        {
                            string message = ErsResources.GetMessage("106202", this.siteId, (int)this.shopKbn, param.order_code, data["newPaymentId"], data["status"], data["message"]);
                            throw new Exception(message);
                        }

                        return data;
                    }
                }
            }
            catch (APIFailedException e)
            {
                string errorMessage = ErsResources.GetMessage("106201", this.siteId, (int)this.shopKbn, param.order_code, e.ToString());
                throw new Exception(errorMessage);
            }

            return null;
        }

        /// <summary>
        /// Yahoo!オーソリAPI取得 [Get the API for authorize (Yahoo!)]
        /// </summary>
        /// <returns>Yahoo!オーソリAPI [The API for authorize (Yahoo!)]</returns>
        protected virtual AuthorizeYahoosOrderPaymentsAPI GetAuthorizeYahoosOrderPaymentsAPI()
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetAuthorizeYahoosOrderPaymentsAPIParam();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);

            return ErsMallFactory.ersMallAPIFactory.GetAuthorizeYahoosOrderPaymentsAPI(apiParam);
        }
        #endregion

        #region オーソリキャンセル [Cancel authorization]
        /// <summary>
        /// オーソリキャンセル [Cancel authorization]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        /// <param name="paymentInfo">決済情報 [Payment information]</param>
        /// <returns>結果情報 [Result information]</returns>
        protected virtual Dictionary<string, object> ExecuteCancelAuth(HarcApiRequest request, MallOrderPaymentParam param, PaymentInfo paymentInfo)
        {
            // オーソリOK以外 [Except Valid auth]
            if (paymentInfo.paymentCondition != EnumMallYahooPaymentCondition.ValidAuth)
            {
                throw new Exception(ErsResources.GetMessage("106300", this.siteId, (int)this.shopKbn, param.order_code, paymentInfo.paymentCondition.ToString()));
            }

            var paymentInfoParam = default(CancelYahoosOrderPaymentsParam);

            // 決済情報パラメータセット [Set the parameter of payment information]
            paymentInfoParam.paymentId = Convert.ToString(paymentInfo.dicPaymentInfo["paymentId"]);
            paymentInfoParam.orderCode = param.order_code;
            paymentInfoParam.cancelType = EnumMallYahooCardPaymentCancelType.CancelAuth;
            paymentInfoParam.cancelReason = param.cancel_reason;

            try
            {
                // Yahoo!決済取消API取得 [Get the API for cancel payment (Yahoo!)]
                var dicResult = this.GetCancelYahoosOrderPaymentsAPI().CancelPayment(request, new List<CancelYahoosOrderPaymentsParam>() { paymentInfoParam });

                foreach (var data in dicResult)
                {
                    if (Convert.ToString(data["orderCode"]) == param.order_code &&
                        Convert.ToString(data["paymentId"]) == paymentInfoParam.paymentId)
                    {
                        // 結果判定 [Judgment result]
                        if (!Convert.ToBoolean(data["status"]))
                        {
                            string message = ErsResources.GetMessage("106302", this.siteId, (int)this.shopKbn, param.order_code, data["status"], data["message"]);
                            throw new Exception(message);
                        }

                        return data;
                    }
                }
            }
            catch (APIFailedException e)
            {
                string errorMessage = ErsResources.GetMessage("106301", this.siteId, (int)this.shopKbn, param.order_code, e.ToString());
                throw new Exception(errorMessage);
            }

            return null;
        }

        /// <summary>
        /// Yahoo!決済取消API取得 [Get the API for cancel payment (Yahoo!)]
        /// </summary>
        /// <returns>Yahoo!決済取消API [The API for cancel payment (Yahoo!)]</returns>
        protected virtual CancelYahoosOrderPaymentsAPI GetCancelYahoosOrderPaymentsAPI()
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetCancelYahoosOrderPaymentsAPIParam();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);

            return ErsMallFactory.ersMallAPIFactory.GetCancelYahoosOrderPaymentsAPI(apiParam);
        }
        #endregion

        #region 売上確定 [Sales]
        /// <summary>
        /// 売上確定 [Sales]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        /// <param name="paymentInfo">決済情報 [Payment information]</param>
        /// <param name="paymentId">決済ID [Payment ID]</param>
        /// <returns>結果情報 [Result information]</returns>
        protected virtual Dictionary<string, object> ExecuteSales(HarcApiRequest request, MallOrderPaymentParam param, PaymentInfo paymentInfo, string paymentId = null)
        {
            // オーソリOK以外 [Except Valid auth]
            if (paymentInfo.paymentCondition != EnumMallYahooPaymentCondition.ValidAuth)
            {
                throw new Exception(ErsResources.GetMessage("106400", this.siteId, (int)this.shopKbn, param.order_code, paymentInfo.paymentCondition.ToString()));
            }

            var paymentInfoParam = default(ExecuteYahoosOrderPaymentsParam);

            // 決済情報パラメータセット [Set the parameter of payment information]
            paymentInfoParam.paymentId = (paymentId == null ? Convert.ToString(paymentInfo.dicPaymentInfo["paymentId"]) : paymentId);
            paymentInfoParam.orderCode = param.order_code;

            try
            {
                // Yahoo!売上確定API取得 [Get the API for execute payment (Yahoo!)]
                var dicResult = this.GetExecuteYahoosOrderPaymentsAPI().ExecutePayment(request, new List<ExecuteYahoosOrderPaymentsParam>() { paymentInfoParam });

                foreach (var data in dicResult)
                {
                    if (Convert.ToString(data["orderCode"]) == param.order_code &&
                        Convert.ToString(data["paymentId"]) == paymentInfoParam.paymentId)
                    {
                        // 結果判定 [Judgment result]
                        if (!Convert.ToBoolean(data["status"]))
                        {
                            string message = ErsResources.GetMessage("106402", this.siteId, (int)this.shopKbn, param.order_code, data["status"], data["message"]);
                            throw new Exception(message);
                        }

                        return data;
                    }
                }
            }
            catch (APIFailedException e)
            {
                string errorMessage = ErsResources.GetMessage("106401", this.siteId, (int)this.shopKbn, param.order_code, e.ToString());
                throw new Exception(errorMessage);
            }

            return null;
        }

        /// <summary>
        /// Yahoo!売上確定API取得 [Get the API for execute payment (Yahoo!)]
        /// </summary>
        /// <returns>Yahoo!売上確定API [The API for execute payment (Yahoo!)]</returns>
        protected virtual ExecuteYahoosOrderPaymentsAPI GetExecuteYahoosOrderPaymentsAPI()
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetExecuteYahoosOrderPaymentsAPIParam();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);

            return ErsMallFactory.ersMallAPIFactory.GetExecuteYahoosOrderPaymentsAPI(apiParam);
        }
        #endregion

        #region 売上キャンセル [Cancel sales]
        /// <summary>
        /// 売上キャンセル [Cancel sales]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        /// <param name="paymentInfo">決済情報 [Payment information]</param>
        /// <returns>結果情報 [Result information]</returns>
        protected virtual Dictionary<string, object> ExecuteCancelSales(HarcApiRequest request, MallOrderPaymentParam param, PaymentInfo paymentInfo)
        {
            // 売上済以外 [Except Saled]
            if (paymentInfo.paymentCondition != EnumMallYahooPaymentCondition.Saled)
            {
                throw new Exception(ErsResources.GetMessage("106500", this.siteId, (int)this.shopKbn, param.order_code, paymentInfo.paymentCondition.ToString()));
            }

            var paymentInfoParam = default(CancelYahoosOrderPaymentsParam);

            // 決済情報パラメータセット [Set the parameter of payment information]
            paymentInfoParam.paymentId = Convert.ToString(paymentInfo.dicPaymentInfo["paymentId"]);
            paymentInfoParam.orderCode = param.order_code;
            paymentInfoParam.cancelType = EnumMallYahooCardPaymentCancelType.CancelPayment;
            paymentInfoParam.cancelReason = param.cancel_reason;

            try
            {
                // Yahoo!決済取消API取得 [Get the API for cancel payment (Yahoo!)]
                var dicResult = this.GetCancelYahoosOrderPaymentsAPI().CancelPayment(request, new List<CancelYahoosOrderPaymentsParam>() { paymentInfoParam });

                foreach (var data in dicResult)
                {
                    if (Convert.ToString(data["orderCode"]) == param.order_code &&
                        Convert.ToString(data["paymentId"]) == paymentInfoParam.paymentId)
                    {
                        // 結果判定 [Judgment result]
                        if (!Convert.ToBoolean(data["status"]))
                        {
                            string message = ErsResources.GetMessage("106502", this.siteId, (int)this.shopKbn, param.order_code, data["status"], data["message"]);
                            throw new Exception(message);
                        }

                        return data;
                    }
                }
            }
            catch (APIFailedException e)
            {
                string errorMessage = ErsResources.GetMessage("106501", this.siteId, (int)this.shopKbn, param.order_code, e.ToString());
                throw new Exception(errorMessage);
            }

            return null;
        }
        #endregion

        #region 再オーソリ [Reauthorization]
        /// <summary>
        /// 再オーソリ [Reauthorization]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        /// <param name="paymentInfo">決済情報 [Payment information]</param>
        /// <returns>結果情報 [Result information]</returns>
        protected virtual void ExecuteReauth(HarcApiRequest request, MallOrderPaymentParam param, PaymentInfo paymentInfo)
        {
            // オーソリOK以外 [Except Valid auth]
            if (paymentInfo.paymentCondition != EnumMallYahooPaymentCondition.ValidAuth)
            {
                throw new Exception(ErsResources.GetMessage("106600", this.siteId, (int)this.shopKbn, param.order_code, paymentInfo.paymentCondition.ToString()));
            }

            var paymentInfoParam = default(ReAuthorizeYahoosOrderPaymentsParam);

            // 決済情報パラメータセット [Set the parameter of payment information]
            paymentInfoParam.paymentId = Convert.ToString(paymentInfo.dicPaymentInfo["paymentId"]);
            paymentInfoParam.orderCode = param.order_code;
            paymentInfoParam.authorizePrice = param.payment_price;
            paymentInfoParam.paymentMethod = param.payment_method;
            paymentInfoParam.paymentCount = param.payment_count;
            paymentInfoParam.remarks = param.remarks;

            try
            {
                // Yahoo!再オーソリAPI取得 [Get the API for re-authorize (Yahoo!)]
                var dicResult = this.GetReAuthorizeYahoosOrderPaymentsAPI().ReAuthorize(request, new List<ReAuthorizeYahoosOrderPaymentsParam>() { paymentInfoParam });

                foreach (var data in dicResult)
                {
                    if (Convert.ToString(data["orderCode"]) == param.order_code &&
                        Convert.ToString(data["paymentId"]) == paymentInfoParam.paymentId)
                    {
                        string message = string.Empty;

                        // 結果判定 [Judgment result]
                        if (!Convert.ToBoolean(data["authorizeCancelResult"]))
                        {
                            message = ErsResources.GetMessage("106302", this.siteId, (int)this.shopKbn, param.order_code, data["status"], data["message"]);
                        }
                        else if (!Convert.ToBoolean(data["authorizeResult"]))
                        {
                            message = ErsResources.GetMessage("106602", this.siteId, (int)this.shopKbn, param.order_code, data["newPaymentId"], data["status"], data["message"]);
                        }

                        if (!string.IsNullOrEmpty(message))
                        {
                            throw new Exception(message);
                        }
                    }
                }
            }
            catch (APIFailedException e)
            {
                string errorMessage = ErsResources.GetMessage("106601", this.siteId, (int)this.shopKbn, param.order_code, e.ToString());
                throw new Exception(errorMessage);
            }
        }

        /// <summary>
        /// Yahoo!再オーソリAPI取得 [Get the API for re-authorize (Yahoo!)]
        /// </summary>
        /// <returns>Yahoo!再オーソリAPI [The API for re-authorize (Yahoo!)]</returns>
        protected virtual ReAuthorizeYahoosOrderPaymentsAPI GetReAuthorizeYahoosOrderPaymentsAPI()
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetReAuthorizeYahoosOrderPaymentsAPIParam();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);

            return ErsMallFactory.ersMallAPIFactory.GetReAuthorizeYahoosOrderPaymentsAPI(apiParam);
        }
        #endregion

        #region オーソリ＋売上確定 [Authorization + Sales]
        /// <summary>
        /// オーソリ＋売上確定 [Authorization + Sales]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="param">モール伝票決済パラメータ [Mall order payment parameter]</param>
        /// <param name="paymentInfo">決済情報 [Payment information]</param>
        protected virtual void ExecuteAuthAndSales(HarcApiRequest request, MallOrderPaymentParam param, PaymentInfo paymentInfo)
        {
            // 売上済・売上処理中 [Saled or Saling]
            if (paymentInfo.paymentCondition == EnumMallYahooPaymentCondition.Saled || paymentInfo.paymentCondition == EnumMallYahooPaymentCondition.Saling)
            {
                throw new Exception(ErsResources.GetMessage("106400", this.siteId, (int)this.shopKbn, param.order_code, paymentInfo.paymentCondition.ToString()));
            }

            string paymentId = Convert.ToString(paymentInfo.dicPaymentInfo["paymentId"]);

            // オーソリエラー・未オーソリ [Invalid auth or Not auth]
            if (paymentInfo.paymentCondition == EnumMallYahooPaymentCondition.InvalidAuth || paymentInfo.paymentCondition == EnumMallYahooPaymentCondition.NotAuth)
            {
                // オーソリ [Authorization]
                var dicResult = this.ExecuteAuth(request, param, paymentInfo);

                // 新決済ID取得 [Get new payment id]
                paymentId = Convert.ToString(dicResult["newPaymentId"]);

                // オーソリOK [Valid auth]
                paymentInfo.paymentCondition = EnumMallYahooPaymentCondition.ValidAuth;
            }
            // オーソリOK以外 [Except Valid auth]
            else if (paymentInfo.paymentCondition != EnumMallYahooPaymentCondition.ValidAuth)
            {
                throw new Exception(ErsResources.GetMessage("106401", this.siteId, (int)this.shopKbn, param.order_code, "Unexpected condition"));
            }

            // 売上確定 [Sales]
            this.ExecuteSales(request, param, paymentInfo, paymentId);
        }
        #endregion
    }
}
