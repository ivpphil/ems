﻿using System;
using System.Collections;
using System.Collections.Generic;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.mall_order;

namespace jp.co.ivp.ers.mall.mall_order.strategy
{
    /// <summary>
    /// モール伝票取り込み結果クラス変換 [Convert to the class of import results]
    /// </summary>
    public class ObtainMallOrderResultStgy
    {
        /// <summary>
        /// 取り込み結果クラス取得 [Get the class of import result]
        /// </summary>
        /// <param name="dicOrder">伝票データ [Data of order]</param>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        /// <param name="typeOrder">伝票ヘッダクラスタイプ [The class type of order header]</param>
        /// <param name="typeOrderDetail">伝票ボディクラスタイプ [The class type of order detail]</param>
        /// <returns>取り込み結果クラス [The class of import result]</returns>
        public MallOrderResult GetMallOrderResult(Dictionary<string, object> dicOrder, int? siteId, EnumMallShopKbn? shopKbn, Type typeOrder, Type typeOrderDetail)
        {
            var result = ErsMallFactory.ersMallOrderFactory.GetMallOrderResult();

            // モール伝票取得 [Get mall order]
            var objMallOrder = this.GetMallOrder(siteId, Convert.ToString(dicOrder["order_code"]));

            // 伝票番号取得 [Get the d_no]
            string d_no = (objMallOrder == null ? ErsMallFactory.ersMallOrderFactory.GetErsMallOrderRepository().GetNextDNo() : objMallOrder.d_no);

            // ヘッダ取得 [Get the header]
            ErsMallOrder mallOrder = (ErsMallOrder)Activator.CreateInstance(typeOrder);
            mallOrder.SetProperties(dicOrder, d_no, siteId, shopKbn);
            mallOrder.merge_status = (objMallOrder == null ? EnumMergeStatus.Ready : objMallOrder.merge_status);
            result.objMallOrder = mallOrder;

            var listDetail = new List<ErsMallOrderDetail>();

            foreach (Dictionary<string, object> detail in (ArrayList)dicOrder["details"])
            {
                // ボディ取得 [Get the body]
                ErsMallOrderDetail detailEntity = (ErsMallOrderDetail)Activator.CreateInstance(typeOrderDetail);
                detailEntity.SetProperties(detail, d_no, Convert.ToString(dicOrder["order_code"]));
                listDetail.Add(detailEntity);
            }
            result.listMallOrderDetail = listDetail;


            // 存在フラグ [Falg of exists]
            result.isExists = (objMallOrder != null);

            // 差分 [Diff]
            result.diff = !result.isExists ? null : this.GetDiff(result, objMallOrder, typeOrder, typeOrderDetail);

            return result;
        }

        #region 差分チェック [Check diff]
        /// <summary>
        /// 差分取得 [Get diff]
        /// </summary>
        /// <param name="result">取り込み結果 [Import result]</param>
        /// <param name="objOld">旧モール伝票ヘッダ [Old mall order header]</param>
        /// <param name="typeOrder">伝票ヘッダクラスタイプ [The class type of order header]</param>
        /// <param name="typeOrderDetail">伝票ボディクラスタイプ [The class type of order detail]</param>
        /// <returns>差分 [Diff]</returns>
        protected virtual MallOrderDiff GetDiff(MallOrderResult result, ErsMallOrder objOld, Type typeOrder, Type typeOrderDetail)
        {
            var ret = false;
            var diff = ErsMallFactory.ersMallOrderFactory.GetMallOrderDiff();

            // 差分チェック（ヘッダ） [Check diff (Header)]
            if (this.CheckDiffHeader(result.objMallOrder, objOld, diff))
            {
                ret = true;
            }

            // モール伝票明細リスト取得 [Get the list of mall order detail]
            var listOld = this.GetMallOrderDetailList(result.objMallOrder.d_no);

            // 差分チェック（明細） [Check diff (Detail)]
            if (this.CheckDiffDetail(result.listMallOrderDetail, listOld, diff, typeOrderDetail))
            {
                ret = true;
            }

            return ret ? diff : null;
        }

        /// <summary>
        /// 差分チェック（ヘッダ） [Check diff (Header)]
        /// </summary>
        /// <param name="objNew">新モール伝票ヘッダ [New mall order header]</param>
        /// <param name="objOld">旧モール伝票ヘッダ [Old mall order header]</param>
        /// <param name="diff">モール伝票取込み差分 [Mall order diff]</param>
        /// <returns>true : 差分あり [Exists diff] / false : 差分なし [Not exists diff]</returns>
        protected virtual bool CheckDiffHeader(ErsMallOrder objNew, ErsMallOrder objOld, MallOrderDiff diff)
        {
            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderRepository();

            // セットされていないカラム [Doesn't set columns]
            objNew.id = objOld.id;
            objNew.intime = objOld.intime;
            objNew.utime = objOld.utime;
            objNew.merge_status = objOld.merge_status;

            var encObj = ErsFactory.ersUtilityFactory.getErsEncryption();

            // 複合化 [Decrypt]
            if (!string.IsNullOrEmpty(objOld.r_card_info3))
            {
                objOld.r_card_info3 = encObj.HexDecode(objOld.r_card_info3);
            }
            if (!string.IsNullOrEmpty(objNew.r_card_info3))
            {
                objNew.r_card_info3 = encObj.HexDecode(objNew.r_card_info3);
            }

            // 差分チェック [Check diff]
            var cols = repository.GetChangedColumns(objOld, objNew);

            // 暗号化 [Encrypt]
            if (!string.IsNullOrEmpty(objOld.r_card_info3))
            {
                objOld.r_card_info3 = encObj.HexEncode(objOld.r_card_info3);
            }
            if (!string.IsNullOrEmpty(objNew.r_card_info3))
            {
                objNew.r_card_info3 = encObj.HexEncode(objNew.r_card_info3);
            }

            diff.listColsChangedHeader = cols;

            // 更新前ステータス
            diff.oldOrderStatus = objOld.order_status;

            return cols.Length > 0;
        }

        /// <summary>
        /// 差分チェック（明細） [Check diff (Detail)]
        /// </summary>
        /// <param name="listNew">新モール伝票取込み結果 [New mall order result]</param>
        /// <param name="listOld">旧モール伝票取込み結果 [Old mall order result]</param>
        /// <param name="diff">モール伝票取込み差分 [Mall order diff]</param>
        /// <param name="typeOrderDetail">伝票ボディクラスタイプ [The class type of order detail]</param>
        /// <returns>true : 差分あり [Exists diff] / false : 差分なし [Not exists diff]</returns>
        protected virtual bool CheckDiffDetail(IList<ErsMallOrderDetail> listNew, IList<ErsMallOrderDetail> listOld, MallOrderDiff diff, Type typeOrderDetail)
        {
            var ret = false;

            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderDetailRepository();

            // 取り込みアイテムコードリスト取得 [Get the list of import item codes]
            var listNewItemCode = new List<string>();

            foreach (var detail in listNew)
            {
                listNewItemCode.Add(detail.item_key);
            }

            var dicDetail = new Dictionary<string, ErsMallOrderDetail>();

            // 削除チェック [Check delete]
            foreach (var objOld in listOld)
            {
                ErsMallOrderDetail objOldTmp = (ErsMallOrderDetail)Activator.CreateInstance(typeOrderDetail);
                objOldTmp.OverwriteWithParameter(objOld.GetPropertiesAsDictionary());

                dicDetail.Add(objOldTmp.item_key, objOldTmp);

                // 削除された [Deleted]
                if (!listNewItemCode.Contains(objOldTmp.item_key))
                {
                    diff.listDelItemCodesDetail.Add(objOldTmp.item_key);
                    ret = true;
                }
            }

            // 追加・差分チェック [Check add or diff]
            foreach (var objNew in listNew)
            {
                // 追加された [Added]
                if (!dicDetail.ContainsKey(objNew.item_key))
                {
                    diff.listAddItemCodesDetail.Add(objNew.item_key);
                    ret = true;
                }
                else
                {
                    var objOld = dicDetail[objNew.item_key];

                    // セットされていないカラム [Doesn't set columns]
                    objNew.id = objOld.id;
                    objNew.intime = objOld.intime;
                    objNew.utime = objOld.utime;

                    // 差分チェック [Check diff]
                    var cols = repository.GetChangedColumns(objOld, objNew);

                    if (cols.Length > 0)
                    {
                        diff.dicListColsChangedDetail.Add(objNew.item_key, cols);

                        // 数量差分 [Quantity diff]
                        if (diff.dicListColsChangedDetail[objNew.item_key].Contains("quantity"))
                        {
                            diff.dicQuantityDiffDetail.Add(objNew.item_key, objOld.quantity.Value - objNew.quantity.Value);
                        }
                        ret = true;
                    }
                }
            }

            return ret;
        }
        #endregion

        #region エンティティ取得 [Get the entity]
        /// <summary>
        /// モール伝票取得 [Get mall order]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="order_code">伝票コード [Order code]</param>
        /// <returns>モール伝票 [Mall order]</returns>
        protected virtual ErsMallOrder GetMallOrder(int? siteId, string order_code)
        {
            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderRepository();
            var criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderCriteria();

            criteria.site_id = siteId;
            criteria.order_code = order_code;

            var listFind = repository.Find(criteria);

            return listFind.Count == 0 ? null : listFind[0];
        }

        /// <summary>
        /// モール伝票明細リスト取得 [Get the list of mall order detail]
        /// </summary>
        /// <param name="d_no">伝票番号 [d_no]</param>
        /// <returns>モール伝票明細リスト [The list of mall order detail]</returns>
        protected virtual IList<ErsMallOrderDetail> GetMallOrderDetailList(string d_no)
        {
            var repository_detail = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderDetailRepository();
            var criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderDetailCriteria();

            criteria.d_no = d_no;

            return repository_detail.Find(criteria);
        }
        #endregion
    }
}
