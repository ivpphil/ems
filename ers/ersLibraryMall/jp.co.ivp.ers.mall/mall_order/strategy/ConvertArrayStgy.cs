﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace jp.co.ivp.ers.mall.mall_order.strategy
{
    /// <summary>
    /// Array型変換 [Convert array]
    /// </summary>
    public class ConvertArrayStgy
    {
        /// <summary>
        /// ArrayListからStringへ変換 [Convert to string from arraylist]
        /// </summary>
        /// <param name="arrayList">ArrayList</param>
        /// <returns>変換後文字列 [Converted string]</returns>
        public virtual string ConvertToStringFromArrayList(ArrayList arrayList)
        {
            return String.Join(", ", arrayList.ToArray());
        }

        /// <summary>
        /// 多層ArrayListからStringへ変換 [Convert to string from arraylist multilayer]
        /// </summary>
        /// <param name="arrayList">ArrayList</param>
        /// <param name="formatField">フィールドフォーマット [Format of field]</param>
        /// <param name="separatorField">フィールド区切り文字 [Separator of field]</param>
        /// <param name="separatorArray">配列区切り文字 [Separator of array]</param>
        /// <returns>変換後文字列 [Converted string]</returns>
        public virtual string ConvertToStringFromArrayListMultilayer(ArrayList arrayList, string formatField, string separatorField, string separatorArray)
        {
            IList<string> listRet = new List<string>();

            foreach (Dictionary<string, object> array in arrayList)
            {
                IList<string> listTmp = new List<string>();

                foreach (var item in array)
                {
                    listTmp.Add(string.Format(formatField, item.Key, item.Value));
                }

                if (listTmp.Count > 0)
                {
                    listRet.Add(String.Join(separatorField, listTmp));
                }
            }

            if (listRet.Count > 0)
            {
                return String.Join(separatorArray, listRet);
            }

            return null;
        }
    }
}
