﻿using System;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.mall_order;

namespace jp.co.ivp.ers.mall.mall_order.strategy
{
    /// <summary>
    /// モール伝票取り込み管理登録・更新 [Update and register mall order import management]
    /// </summary>
    public class UpdateAndRegisterMallOrderImportStgy
    {
        /// <summary>
        /// レコード存在チェック [Check exists record]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="dateToday">実行日時 [Today's datetime]</param>
        /// <returns>true : 存在している [Exist] / false : 存在していない [Not exist]</returns>
        public virtual bool IsExistsRecord(int? siteId, DateTime dateToday)
        {
            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderImportRepository();
            var criteria = this.GetCriteria(siteId, dateToday);

            return repository.GetRecordCount(criteria) != 0;
        }

        /// <summary>
        /// インサート [Insert]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="dateToday">実行日時 [Today's datetime]</param>
        /// <param name="imported">取り込みフラグ [Falg of imported]</param>
        public virtual void Insert(int? siteId, DateTime dateToday, EnumImported imported)
        {
            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderImportRepository();
            var obj = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderImport();

            // パラメータセット [Set the parameters]
            obj.site_id = siteId;
            obj.mall_shop_kbn = ErsMallFactory.ersSiteFactory.GetSiteData().GetMallShopKbnFromSiteId(siteId.Value);
            obj.import_time = dateToday;
            obj.imported = imported;

            // インサート [Insert]
            repository.Insert(obj);
        }

        /// <summary>
        /// アップデート [Update]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="dateGet">取得日時 [Datetime of getting]</param>
        public virtual void Update(int? siteId, DateTime dateGet)
        {
            var repository = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderImportRepository();

            var criteria = this.GetCriteria(siteId, dateGet);

            var objOld = repository.Find(criteria)[0];
            var objNew = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderImportWithID(objOld.id.Value);

            // 取り込み済み [Imported]
            objNew.imported = EnumImported.Imported;

            // アップデート [Update]
            repository.Update(objOld, objNew);
        }

        /// <summary>
        /// クライテリア取得 [Get the criteria]
        /// </summary>
        /// <param name="siteId">サイトID [Site id]</param>
        /// <param name="dateToday">実行日時 [Today's datetime]</param>
        /// <returns>クライテリア [Criteria]</returns>
        protected virtual ErsMallOrderImportCriteria GetCriteria(int? siteId, DateTime dateToday)
        {
            var criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderImportCriteria();

            criteria.import_time = dateToday;
            criteria.site_id = siteId;

            return criteria;
        }
    }
}
