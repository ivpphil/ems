﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.mall_order;
using jp.co.ivp.ers.order;

namespace jp.co.ivp.ers.mall.mall_order.strategy
{
    /// <summary>
    /// モール伝票メールアドレスバリデート [Validate mall order email]
    /// </summary>
    public class ValidateMallOrderEmailStgy
    {
        /// <summary>
        /// モール伝票メールアドレスバリデート [Validate mall order email]
        /// </summary>
        /// <param name="objMallContainer">モール伝票コンテナ [The mall order container]</param>
        /// <returns>エラーメッセージ [Error message]</returns>
        public string Validate(ErsOrderContainer objContainer)
        {
            // 楽天：マスク化されていないアドレス [Rakuten : Not masked e-mail address]
            if (objContainer.OrderHeader.mall_shop_kbn == EnumMallShopKbn.RAKUTEN && objContainer.OrderHeader.email.StartsWith("*@"))
            {
                var siteData = ErsMallFactory.ersSiteFactory.GetSiteData().dicSiteData;
                var siteName = siteData[objContainer.OrderHeader.site_id.Value].site_name;

                return ErsResources.GetMessage("107004", siteName, objContainer.OrderHeader.mall_d_no, objContainer.OrderHeader.intime);
            }

            return string.Empty;
        }
    }
}
