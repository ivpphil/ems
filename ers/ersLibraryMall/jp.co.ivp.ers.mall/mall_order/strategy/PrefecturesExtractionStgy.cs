﻿using System;
using System.Collections;
using System.Collections.Generic;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.mall_order.strategy
{
    /// <summary>
    /// 都道府県抽出 [Prefectures extraction]
    /// </summary>
    public class PrefecturesExtractionStgy
    {
        /// <summary>
        /// 国外 [Foreign]
        /// </summary>
        private string foreign = "国外";

        /// 国外判定 true:国外 / false:国内 [Is country true:country / false:not country]
        /// </summary>
        private bool isCountry { get; set; }

        /// <summary>
        /// 都道府県抽出 [Prefectures extraction]
        /// </summary>
        /// <param name="address">string</param>
        /// <returns>string</returns>
        public virtual string PrefecturesExtraction(string address)
        {
            if (string.IsNullOrEmpty(address))
            {
                return null;
            }

            //国外判定を初期化
            this.isCountry = false;

            //都道府県を抽出
            var pref = this.GetPrefectures(address);

            if (string.IsNullOrEmpty(pref))
            {
                //都道府県を抽出できない場合は国名の抽出を試みる
                pref = this.GetCountryName(address);
            }

            return pref;
        }

        /// <summary>
        /// 住所から都道府県を削除 [Remove prefectures from address]
        /// </summary>
        /// <param name="address">string</param>
        /// <param name="pref">string</param>
        /// <returns>string</returns>
        public virtual string RemovePrefecturesFromAddress(string address, string pref)
        {
            if (string.IsNullOrEmpty(pref))
            {
                return address;
            }

            if (!address.Contains(this.foreign) && this.isCountry)
            {
                //グローバルサイトの場合は末尾からprefの文字数を削除
                return this.ReverseString(this.ReverseString(address).Remove(0, pref.Length));
            }
            else
            {
                address = address.Replace(this.foreign, "");

                //グローバルサイトでない場合は先頭からprefの文字数を削除
                return address.Remove(0, pref.Length); ;
            }
        }

        /// <summary>
        /// 国名取得 [Get country name]
        /// </summary>
        /// <param name="address">string</param>
        /// <returns>string</returns>
        protected virtual string GetCountryName(string address)
        {
            //リスト取得
            var countryList = ErsMallFactory.ersMallViewServiceFactory.GetErsViewMallRakutenCountryService().GetList();

            if (!address.Contains(this.foreign))
            {
                //住所を反転
                var reverse_address = this.ReverseString(address);

                //グローバルサイトでの購入の場合は末尾の国名を抽出
                foreach (Dictionary<string, object> country in countryList)
                {
                    var name = Convert.ToString(country["name"]);

                    if (reverse_address.StartsWith(this.ReverseString(name)))
                    {
                        this.isCountry = true;
                        return name;
                    }
                }
            }
            else
            {
                //国外の文字列を削除
                var replace_address = address.Replace(this.foreign, "");

                //グローバルサイトでの購入でない場合は先頭の国名を抽出
                foreach (Dictionary<string, object> country in countryList)
                {
                    var name = Convert.ToString(country["name"]);

                    if (replace_address.StartsWith(name))
                    {
                        this.isCountry = true;
                        return name;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// 都道府県取得 [Get Prefectures]
        /// </summary>
        /// <param name="address">string</param>
        /// <returns>string</returns>
        protected virtual string GetPrefectures(string address)
        {
            //都道府県リスト取得
            var prefList = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList();

            foreach (Dictionary<string, object> pref in prefList)
            {
                if (address.StartsWith(Convert.ToString(pref["pref_name"])))
                {
                    this.isCountry = false;
                    return Convert.ToString(pref["pref_name"]);
                }
            }

            return null;
        }

        /// <summary>
        /// 文字列反転 [Reverse string]
        /// </summary>
        /// <param name="text">string</param>
        /// <returns>string</returns>
        protected virtual string ReverseString(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return null;
            }

            var array = text.ToCharArray();
            Array.Reverse(array);
            return new String(array);
        }
    }
}
