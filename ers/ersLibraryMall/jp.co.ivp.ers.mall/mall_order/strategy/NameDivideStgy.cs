﻿using System;
using System.Collections;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order.strategy
{
    /// <summary>
    /// 氏名分割 [Name divide]
    /// </summary>
    public class NameDivideStgy
    {
        /// <summary>
        /// 姓取得 [Get lname]
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual string GetLname(string name)
        {
            //氏名分割
            var array = this.NameDivide(name);

            if (array == null)
            {
                return null;
            }

            return array[0];
        }

        /// <summary>
        /// 名取得 [Get fname]
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual string GetFname(string name)
        {
            //氏名分割
            var array = this.NameDivide(name);

            if (array == null)
            {
                return null;
            }

            string retName = null;
            var cnt = 0;

            foreach (var divide in array)
            {
                //姓は取得しない
                if (cnt != 0)
                {
                    retName += " " + divide;
                }

                cnt++;
            }

            //余計な先頭の半角スペースを削除
            if (!string.IsNullOrEmpty(retName))
            {
                retName = retName.Remove(0, 1);
            }

            return retName;
        }

        /// <summary>
        /// 氏名分割 [Nmae divide]
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>string[]</returns>
        protected virtual string[] NameDivide(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            //全角スペースを半角スペースへ置換
            name = name.Replace("　", " ");

            return name.Split(' ');
        }
    }
}
