﻿using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票明細リポジトリ [Repository for mall order detail table]
    /// </summary>
    public class ErsMallOrderDetailRepository
        : ErsRepository<ErsMallOrderDetail>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="type">DBタイプ [Type of DB]</param>
        public ErsMallOrderDetailRepository()
            : base("mall_ds_master_t")
        {
        }

        /// <summary>
        /// 差分取得 [Get changed columns]
        /// </summary>
        /// <param name="old_obj">旧モール伝票明細エンティティ [Old mall order detail entity]</param>
        /// <param name="new_obj">新モール伝票明細エンティティ [New mall order detail entity]</param>
        /// <returns>string[]</returns>
        public virtual string[] GetChangedColumns(ErsMallOrderDetail old_obj, ErsMallOrderDetail new_obj)
        {
            var newDic = new_obj.GetPropertiesAsDictionary(this.ersDB_table);
            var oldDic = old_obj.GetPropertiesAsDictionary(this.ersDB_table);

            return ErsCommon.GetChangedKeys(oldDic, newDic);
        }
    }
}
