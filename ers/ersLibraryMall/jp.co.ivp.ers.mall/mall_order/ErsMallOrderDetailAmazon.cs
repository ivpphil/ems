﻿using System;
using System.Collections;
using System.Collections.Generic;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// Amazonモール伝票明細エンティティ [Amazon Entity for mall order detail table]
    /// </summary>
    public class ErsMallOrderDetailAmazon
        : ErsMallOrderDetail
    {
        /// <summary>
        /// 商品キー [Item key]
        /// </summary>
        public override string item_key
        {
            get
            {
                return this.a_item_id;
            }
        }

        /// <summary>
        /// プロパティへの値セット [Set properties]
        /// </summary>
        public override void SetProperties(Dictionary<string, object> detail, string d_no, string order_code)
        {
            //strategy取得
            var convertStgy = ErsMallFactory.ersMallOrderFactory.GetConvertArrayStgy();

            //Amazon other内のデータを格納
            var detailOther = (Dictionary<string, object>)detail["other"];

            if (detailOther.ContainsKey("asin"))
            {
                this.a_asin = Convert.ToString(detailOther["asin"]);
            }
            if (detailOther.ContainsKey("item_id"))
            {
                this.a_item_id = Convert.ToString(detailOther["item_id"]);
            }
            if (detailOther.ContainsKey("quantity_shipped") && detailOther["quantity_shipped"] != null && Convert.ToString(detailOther["quantity_shipped"]) != "")
            {
                this.a_quantity_shipped = Convert.ToInt32(detailOther["quantity_shipped"]);
            }
            if (detailOther.ContainsKey("shipping_price") && detailOther["shipping_price"] != null && Convert.ToString(detailOther["shipping_price"]) != "")
            {
                this.a_shipping_price = Convert.ToInt32(detailOther["shipping_price"]);
            }
            if (detailOther.ContainsKey("giftwrap_price") && detailOther["giftwrap_price"] != null && Convert.ToString(detailOther["giftwrap_price"]) != "")
            {
                this.a_giftwrap_price = Convert.ToInt32(detailOther["giftwrap_price"]);
            }
            if (detailOther.ContainsKey("item_tax") && detailOther["item_tax"] != null && Convert.ToString(detailOther["item_tax"]) != "")
            {
                this.a_item_tax = Convert.ToInt32(detailOther["item_tax"]);
            }
            if (detailOther.ContainsKey("shipping_tax") && detailOther["shipping_tax"] != null && Convert.ToString(detailOther["shipping_tax"]) != "")
            {
                this.a_shipping_tax = Convert.ToInt32(detailOther["shipping_tax"]);
            }
            if (detailOther.ContainsKey("giftwrap_tax") && detailOther["giftwrap_tax"] != null && Convert.ToString(detailOther["giftwrap_tax"]) != "")
            {
                this.a_giftwrap_tax = Convert.ToInt32(detailOther["giftwrap_tax"]);
            }
            if (detailOther.ContainsKey("shipping_discount") && detailOther["shipping_discount"] != null && Convert.ToString(detailOther["shipping_discount"]) != "")
            {
                this.a_shipping_discount = Convert.ToInt32(detailOther["shipping_discount"]);
            }
            if (detailOther.ContainsKey("promotion_discount") && detailOther["promotion_discount"] != null && Convert.ToString(detailOther["promotion_discount"]) != "")
            {
                this.a_promotion_discount = Convert.ToInt32(detailOther["promotion_discount"]);
            }
            if (detailOther.ContainsKey("promotion_ids"))
            {
                this.a_promotion_ids = convertStgy.ConvertToStringFromArrayList((ArrayList)detailOther["promotion_ids"]);
            }
            if (detailOther.ContainsKey("codfee") && detailOther["codfee"] != null && Convert.ToString(detailOther["codfee"]) != "")
            {
                this.a_codfee = Convert.ToInt32(detailOther["codfee"]);
            }
            if (detailOther.ContainsKey("codfee_discount") && detailOther["codfee_discount"] != null && Convert.ToString(detailOther["codfee_discount"]) != "")
            {
                this.a_codfee_discount = Convert.ToInt32(detailOther["codfee_discount"]);
            }

            base.SetProperties(detail, d_no, order_code);
        }
    }
}
