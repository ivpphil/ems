﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票ヘッダエンティティ [Entity for mall order table]
    /// </summary>
    public class ErsMallOrder
        : ErsRepositoryEntity
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 登録日時 [Insert time]
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新日時 [Update time]
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// 伝票番号 [DeNA no]
        /// </summary>
        public virtual string d_no { get; set; }

        /// <summary>
        /// 店舗ID [Shop ID]
        /// </summary>
        public virtual int? shop_id { get; set; }

        /// <summary>
        /// 店舗名 [Shop name]
        /// </summary>
        public virtual string shop_name { get; set; }

        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public virtual int? site_id { get; set; }

        /// <summary>
        /// 店舗タイプ [Shop type]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// マージステータス [Merge status]
        /// </summary>
        public virtual EnumMergeStatus? merge_status { get; set; }

        /// <summary>
        /// 受注番号 [Order code]
        /// </summary>
        public virtual string order_code { get; set; }

        /// <summary>
        /// 受注日時 [Order date]
        /// </summary>
        public virtual DateTime? order_date { get; set; }

        /// <summary>
        /// ECサイトステータス [Order status]
        /// </summary>
        public virtual string order_status { get; set; }

        /// <summary>
        /// ローカルステータスID [Order status ID]
        /// </summary>
        public virtual int? order_status_id { get; set; }

        /// <summary>
        /// 注文者氏名 [Order name]
        /// </summary>
        public virtual string order_name { get; set; }

        /// <summary>
        /// 注文者氏名カナ [Order kana]
        /// </summary>
        public virtual string order_kana { get; set; }

        /// <summary>
        /// 注文者郵便番号 [Order zipcode]
        /// </summary>
        public virtual string order_zipcode { get; set; }

        /// <summary>
        /// 注文者住所 [Order address]
        /// </summary>
        public virtual string order_address { get; set; }

        /// <summary>
        /// 注文者電話番号 [Order phone]
        /// </summary>
        public virtual string order_phone { get; set; }

        /// <summary>
        /// 注文者メールアドレス [Order email]
        /// </summary>
        public virtual string order_email { get; set; }

        /// <summary>
        /// 支払方法 [Payment method]
        /// </summary>
        public virtual string payment_method { get; set; }

        /// <summary>
        /// 支払日 [Payment date]
        /// </summary>
        public virtual DateTime? payment_date { get; set; }

        /// <summary>
        /// 請求金額 [Order price]
        /// </summary>
        public virtual int order_price { get; set; }

        /// <summary>
        /// 消費税 [Order tax]
        /// </summary>
        public virtual int order_tax { get; set; }

        /// <summary>
        /// 送料 [Delivery cost]
        /// </summary>
        public virtual int delivery_cost { get; set; }

        /// <summary>
        /// 手数料 [Commission]
        /// </summary>
        public virtual int commission { get; set; }

        /// <summary>
        /// 利用ポイント [Use point]
        /// </summary>
        public virtual int use_point { get; set; }

        /// <summary>
        /// 包装料 [Giftwrap cost]
        /// </summary>
        public virtual int giftwrap_cost { get; set; }

        /// <summary>
        /// 割引金額 [Discount]
        /// </summary>
        public virtual int discount { get; set; }

        /// <summary>
        /// 備考 [Remarks]
        /// </summary>
        public virtual string remarks { get; set; }

        /// <summary>
        /// メモ１ [Memo1]
        /// </summary>
        public virtual string memo1 { get; set; }

        /// <summary>
        /// メモ２ [Memo2]
        /// </summary>
        public virtual string memo2 { get; set; }

        #region Rakuten

        /// <summary>
        /// クレジットカード情報1 [Rakuten Card info1]
        /// </summary>
        public virtual string r_card_info1 { get; set; }

        /// <summary>
        /// クレジットカード情報2 [Rakuten Card info2]
        /// </summary>
        public virtual string r_card_info2 { get; set; }

        /// <summary>
        /// クレジットカード情報3 [Rakuten Card info3]
        /// </summary>
        public virtual string r_card_info3 { get; set; }

        /// <summary>
        /// クレジットカード情報4 [Rakuten Card info4]
        /// </summary>
        public virtual string r_card_info4 { get; set; }

        /// <summary>
        /// クレジットカード情報5 [Rakuten Card info5]
        /// </summary>
        public virtual string r_card_info5 { get; set; }

        /// <summary>
        /// 決済ステータス [Rakuten Payment status]
        /// </summary>
        public virtual string r_payment_status { get; set; }

        /// <summary>
        /// クーポン情報 [Rakuten Coupon]
        /// </summary>
        public virtual string r_coupon { get; set; }

        /// <summary>
        /// 受注アイコン [Rakuten Order type]
        /// </summary>
        public virtual string r_order_type { get; set; }

        /// <summary>
        /// 請求金額確定状態 [Rakuten Order pending]
        /// </summary>
        public virtual string r_order_pending { get; set; }

        /// <summary>
        /// トラッキングID [Rakuten Tracking user]
        /// </summary>
        public virtual string r_tracking_user { get; set; }

        /// <summary>
        /// 要注意 [Rakuten Caution]
        /// </summary>
        public virtual string r_caution { get; set; }

        #endregion

        #region Yahoo

        /// <summary>
        /// 購入元注文番号 [Yahoo From order code]
        /// </summary>
        public virtual string y_from_order_code { get; set; }

        /// <summary>
        /// 枝番 [Yahoo Branch code]
        /// </summary>
        public virtual string y_branch_code { get; set; }

        /// <summary>
        /// モバイル注文 [Yahoo Is_mobile order]
        /// </summary>
        public virtual string y_is_mobile_order { get; set; }

        /// <summary>
        /// 発売日 [Yahoo Release date]
        /// </summary>
        public virtual DateTime? y_release_date { get; set; }

        /// <summary>
        /// ロイヤリティ確定日 [Yahoo Royalty fix date]
        /// </summary>
        public virtual DateTime? y_royalty_fix_date { get; set; }

        /// <summary>
        /// 仮ポイント [Yahoo Temp point]
        /// </summary>
        public virtual int? y_temp_point { get; set; }

        /// <summary>
        /// 自動完了予定日 [Yahoo Point auto complete date]
        /// </summary>
        public virtual DateTime? y_point_auto_complete_date { get; set; }

        /// <summary>
        /// ロイヤリティ処理日 [Yahoo Royalty processing date]
        /// </summary>
        public virtual DateTime? y_royalty_processing_date { get; set; }

        /// <summary>
        /// 確定ポイント [Yahoo Fix point]
        /// </summary>
        public virtual int? y_fix_point { get; set; }

        /// <summary>
        /// 緊急連絡先 [Yahoo Contact]
        /// </summary>
        public virtual string y_contact { get; set; }

        /// <summary>
        /// カード支払種別 [Yahoo Card_payment type]
        /// </summary>
        public virtual string y_card_payment_type { get; set; }

        /// <summary>
        /// カード承認番号 [Yahoo Card_approval number]
        /// </summary>
        public virtual string y_card_approval_number { get; set; }

        /// <summary>
        /// 決済ステータス [Yahoo Payment status]
        /// </summary>
        public virtual string y_payment_status { get; set; }

        /// <summary>
        /// 決済備考 [Yahoo Payment remarks]
        /// </summary>
        public virtual string y_payment_remarks { get; set; }

        /// <summary>
        /// ギフト包装 [Yahoo Gift wrap]
        /// </summary>
        public virtual string y_gift_wrap { get; set; }

        /// <summary>
        /// 名入れ [Yahoo Naire]
        /// </summary>
        public virtual string y_naire { get; set; }

        /// <summary>
        /// いたずら注文フラグ [Yahoo mischief flag]
        /// </summary>
        public virtual string y_caution { get; set; }

        /// <summary>
        /// いたずら理由 [Yahoo mischief reason]
        /// </summary>
        public virtual string y_caution_reason { get; set; }

        /// <summary>
        /// その他入力値 [Yahoo Options]
        /// </summary>
        public virtual string y_options { get; set; }

        #endregion

        #region Amazon

        /// <summary>
        /// SellerOrderId [Amazon Seller order ID]
        /// </summary>
        public virtual string a_seller_order_id { get; set; }

        /// <summary>
        /// LastUpdateDate [Amazon Last update date]
        /// </summary>
        public virtual DateTime? a_last_update_date { get; set; }

        /// <summary>
        /// FulfillmentChannel [Amazon Fulfillment channel]
        /// </summary>
        public virtual string a_fulfillment_channel { get; set; }

        /// <summary>
        /// SalesChannel [Amazon Sales channel]
        /// </summary>
        public virtual string a_sales_channel { get; set; }

        /// <summary>
        /// OrderChannel [Amazon Order channel]
        /// </summary>
        public virtual string a_order_channel { get; set; }

        /// <summary>
        /// ShipServiceLevel [Amazon Ship service level]
        /// </summary>
        public virtual string a_ship_service_level { get; set; }

        /// <summary>
        /// CurrencyCode [Amazon Currency code]
        /// </summary>
        public virtual string a_currency_code { get; set; }

        /// <summary>
        /// NumberOfItemsShipped [Amazon Number of items shipped]
        /// </summary>
        public virtual int? a_number_of_items_shipped { get; set; }

        /// <summary>
        /// NumberOfItemsUnshipped [Amazon Number of item unshipped]
        /// </summary>
        public virtual int? a_number_of_items_unshipped { get; set; }

        /// <summary>
        /// PaymentExecutionDetailItems [Amazon Payment execution details]
        /// </summary>
        public virtual string a_payment_execution_details { get; set; }

        /// <summary>
        /// Order type [Amazon Order type]
        /// </summary>
        public virtual string a_order_type { get; set; }

        /// <summary>
        /// Earliest ship date [Amazon Earliest ship date]
        /// </summary>
        public virtual DateTime? a_earliest_ship_date { get; set; }

        /// <summary>
        /// Latest ship date [Amazon Latest ship date]
        /// </summary>
        public virtual DateTime? a_latest_ship_date { get; set; }

        #endregion

        #region DeNA

        /// <summary>
        /// カード種類 [DeNA Card type]
        /// </summary>
        public virtual string d_card_type { get; set; }

        /// <summary>
        /// カード番号 [DeNA Card number]
        /// </summary>
        public virtual string d_card_number { get; set; }

        /// <summary>
        /// カード有効期限 [DeNA Card expire date]
        /// </summary>
        public virtual string d_card_expire_date { get; set; }

        /// <summary>
        /// カード名義人 [DeNA Card owner]
        /// </summary>
        public virtual string d_card_owner { get; set; }

        /// <summary>
        /// 名義人生年月日 [DeNA Card owner birthday]
        /// </summary>
        public virtual string d_card_owner_birthday { get; set; }

        /// <summary>
        /// 決済ステータス [DeNA Payment status]
        /// </summary>
        public virtual string d_payment_status { get; set; }

        #endregion

        /// <summary>
        /// プロパティへの値セット [Set properties]
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="d_no"></param>
        /// <param name="siteId"></param>
        /// <param name="mall_shop_kbn"></param>
        public virtual void SetProperties(Dictionary<string, object> dictionary, string d_no, int? site_id, EnumMallShopKbn? mall_shop_kbn)
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();

            this.d_no = d_no;
            this.shop_id = mallSetup.GetShopId(site_id.Value);
            this.shop_name = mallSetup.GetShopName(site_id.Value);
            this.site_id = site_id;
            this.mall_shop_kbn = mall_shop_kbn;

            this.order_code = Convert.ToString(dictionary["order_code"]);

            if (dictionary["order_date"] != null && Convert.ToString(dictionary["order_date"]) != "")
            {
                this.order_date = Convert.ToDateTime(dictionary["order_date"]);
            }

            this.order_status = Convert.ToString(dictionary["order_status"]);

            if (dictionary["order_status_id"] != null && Convert.ToString(dictionary["order_status_id"]) != "")
            {
                this.order_status_id = Convert.ToInt32(dictionary["order_status_id"]);
            }

            this.order_name = Convert.ToString(dictionary["order_name"]);
            this.order_kana = Convert.ToString(dictionary["order_kana"]);
            this.order_zipcode = Convert.ToString(dictionary["order_zipcode"]);
            this.order_address = Convert.ToString(dictionary["order_address"]);
            this.order_phone = Convert.ToString(dictionary["order_phone"]);
            this.order_email = Convert.ToString(dictionary["order_email"]);
            this.payment_method = Convert.ToString(dictionary["payment_method"]);

            if (dictionary["payment_date"] != null && Convert.ToString(dictionary["payment_date"]) != "")
            {
                this.payment_date = Convert.ToDateTime(dictionary["payment_date"]);
            }
            if (dictionary["order_price"] != null && Convert.ToString(dictionary["order_price"]) != "")
            {
                this.order_price = Convert.ToInt32(dictionary["order_price"]);
            }
            if (dictionary["order_tax"] != null && Convert.ToString(dictionary["order_tax"]) != "")
            {
                this.order_tax = Convert.ToInt32(dictionary["order_tax"]);
            }
            if (dictionary["delivery_cost"] != null && Convert.ToString(dictionary["delivery_cost"]) != "")
            {
                this.delivery_cost = Convert.ToInt32(dictionary["delivery_cost"]);
            }
            if (dictionary["commission"] != null && Convert.ToString(dictionary["commission"]) != "")
            {
                this.commission = Convert.ToInt32(dictionary["commission"]);
            }
            if (dictionary["use_point"] != null && Convert.ToString(dictionary["use_point"]) != "")
            {
                this.use_point = Convert.ToInt32(dictionary["use_point"]);
            }
            if (dictionary["giftwrap_cost"] != null && Convert.ToString(dictionary["giftwrap_cost"]) != "")
            {
                this.giftwrap_cost = Convert.ToInt32(dictionary["giftwrap_cost"]);
            }
            if (dictionary["discount"] != null && Convert.ToString(dictionary["discount"]) != "")
            {
                this.discount = Convert.ToInt32(dictionary["discount"]);
            }
            this.remarks = Convert.ToString(dictionary["remarks"]);
            this.memo1 = Convert.ToString(dictionary["memo1"]);
            this.memo2 = Convert.ToString(dictionary["memo2"]);
        }
    }
}
