﻿using System;
using System.Collections.Generic;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票取込み結果 [Mall order result]
    /// </summary>
    public class MallOrderResult
    {
        /// <summary>
        /// モール伝票ヘッダ [Mall order hearder]
        /// </summary>
        public virtual ErsMallOrder objMallOrder { get; set; }

        /// <summary>
        /// モール伝票明細リスト [List of mall order detail]
        /// </summary>
        public virtual IList<ErsMallOrderDetail> listMallOrderDetail { get; set; }

        /// <summary>
        /// 存在フラグ
        /// true : 既に存在している [Already exists] / false : 存在していない [Doesn't exists]
        /// </summary>
        public virtual bool isExists { get; set; }

        /// <summary>
        /// エラーメッセージ [Error message]
        /// </summary>
        public virtual string errorMessage { get; set; }

        /// <summary>
        /// モール伝票取込み差分 [Mall order diff]
        /// </summary>
        public virtual MallOrderDiff diff { get; set; }


        /// <summary>
        /// 登録日時設定 [Set intime]
        /// </summary>
        /// <param name="dateNow">現在日時 [Datetime of now]</param>
        public void SetIntime(DateTime dateNow)
        {
            this.objMallOrder.intime = dateNow;

            foreach (var detail in this.listMallOrderDetail)
            {
                detail.intime = dateNow;
            }
        }

        /// <summary>
        /// 更新日時設定 [Set utime]
        /// </summary>
        /// <param name="dateNow">現在日時 [Datetime of now]</param>
        public void SetUtime(DateTime dateNow)
        {
            this.objMallOrder.utime = dateNow;

            foreach (var detail in this.listMallOrderDetail)
            {
                detail.utime = dateNow;
            }
        }
    }
}
