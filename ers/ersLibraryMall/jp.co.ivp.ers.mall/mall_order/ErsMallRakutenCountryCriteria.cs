﻿using System;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// 楽天国マスタクライテリア [Criteria for raukten country table]
    /// </summary>
    public class ErsMallRakutenCountryCriteria
        : Criteria
    {
        public void SetOrderById(Criteria.OrderBy orderBy)
        {
            this.AddOrderBy("mall_rakuten_country_t.id", orderBy);
        }

        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_rakuten_country_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// アクティブ [Active flag]
        /// </summary>
        public virtual EnumActive active
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_rakuten_country_t.active", (int)value, Operation.EQUAL));
            }
        }
    }
}
