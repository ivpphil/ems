﻿using System.Collections.Generic;
using jp.co.ivp.ers.mall.mall_order.payment;
using jp.co.ivp.ers.mall.mall_order.payment.strategy;
using jp.co.ivp.ers.mall.mall_order.specification;
using jp.co.ivp.ers.mall.mall_order.status;
using jp.co.ivp.ers.mall.mall_order.status.strategy;
using jp.co.ivp.ers.mall.mall_order.strategy;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票関連ファクトリ [Factory for mall order]
    /// </summary>
    public class ErsMallOrderFactory
    {
        #region モール伝票コンテナ [Mall order container]
        /// <summary>
        /// モール伝票コンテナ取得 [Get mall order container]
        /// </summary>
        /// <returns>MallOrderContainer</returns>
        public virtual MallOrderContainer GetMallOrderContainer()
        {
            return new MallOrderContainer();
        }
        #endregion

        #region モール伝票ヘッダ [Mall order]
        /// <summary>
        /// モール伝票ヘッダエンティティ取得 [Get mall order entity]
        /// </summary>
        /// <returns>ErsMallOrder</returns>
        public virtual ErsMallOrder GetErsMallOrder()
        {
            return new ErsMallOrder();
        }

        /// <summary>
        /// 楽天市場モール伝票ヘッダエンティティ取得 [Get Rakuten mall order entity]
        /// </summary>
        /// <returns>ErsMallOrderRakuten</returns>
        public virtual ErsMallOrderRakuten GetErsMallOrderRakuten()
        {
            return new ErsMallOrderRakuten();
        }

        /// <summary>
        /// Yahooモール伝票ヘッダエンティティ取得 [Get Yahoo mall order entity]
        /// </summary>
        /// <returns>ErsMallOrderYahoo</returns>
        public virtual ErsMallOrderYahoo GetErsMallOrderYahoo()
        {
            return new ErsMallOrderYahoo();
        }

        /// <summary>
        /// Amazonモール伝票ヘッダエンティティ取得 [Get Amazon mall order entity]
        /// </summary>
        /// <returns>ErsMallOrderAmazon</returns>
        public virtual ErsMallOrderAmazon GetErsMallOrderAmazon()
        {
            return new ErsMallOrderAmazon();
        }

        /// <summary>
        /// DeNAモール伝票ヘッダエンティティ取得 [Get DeNA mall order entity]
        /// </summary>
        /// <returns>ErsMallOrderDeNA</returns>
        public virtual ErsMallOrderDeNA GetErsMallOrderDeNA()
        {
            return new ErsMallOrderDeNA();
        }

        /// <summary>
        /// モール伝票取込み結果クラス取得 [Get mall order result]
        /// </summary>
        /// <returns>MallOrderResult</returns>
        public virtual MallOrderResult GetMallOrderResult()
        {
            return new MallOrderResult();
        }

        /// <summary>
        /// モール伝票取込み差分クラス取得 [Get mall order diff]
        /// </summary>
        /// <returns>MallOrderDiff</returns>
        public virtual MallOrderDiff GetMallOrderDiff()
        {
            return new MallOrderDiff();
        }

        /// <summary>
        /// モール伝票ヘッダリポジトリ取得 [Get mall order repository]
        /// </summary>
        /// <param name="type">DBタイプ [Type of DB]</param>
        /// <returns></returns>
        public virtual ErsMallOrderRepository GetErsMallOrderRepository()
        {
            return new ErsMallOrderRepository();
        }

        /// <summary>
        /// モール伝票ヘッダクライテリア取得 [Get mall order criteria]
        /// </summary>
        /// <returns>ErsDirectionCriteria</returns>
        public virtual ErsMallOrderCriteria GetErsMallOrderCriteria()
        {
            return new ErsMallOrderCriteria();
        }

        /// <summary>
        /// モール伝票取込結果エンティティ変換取得 [Get obtain mall order result list from json]
        /// </summary>
        /// <returns>ObtainMallOrderResultFromJsonStgy</returns>
        public virtual ObtainMallOrderResultStgy GetObtainMallOrderResultStgy()
        {
            return new ObtainMallOrderResultStgy();
        }

        /// <summary>
        /// モール伝票ヘッダエンティティ取得（パラメータから） [Get mall order entity (from parameters)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsMallOrder</returns>
        public virtual ErsMallOrder GetErsMallOrderWithParameters(Dictionary<string, object> dicParams)
        {
            var obj = this.GetErsMallOrder();
            obj.OverwriteWithParameter(dicParams);
            return obj;
        }

        /// <summary>
        /// モール伝票ヘッダエンティティ取得（IDから） [Get mall order entity (from ID)]
        /// </summary>
        /// <param name="dbType">EnumDBType</param>
        /// <param name="id">id or bridge_id</param>
        /// <returns>ErsMallOrder</returns>
        public virtual ErsMallOrder GetErsMallOrderWithID(int id)
        {
            var repository = this.GetErsMallOrderRepository();
            var criteria = this.GetErsMallOrderCriteria();

            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count != 1)
            {
                return null;
            }

            return list[0];
        }

        /// <summary>
        /// モール伝票ヘッダエンティティ取得（伝票番号から） [Get mall order entity (from D No)]
        /// </summary>
        /// <param name="dbType">EnumDBType</param>
        /// <param name="d_no">d_no</param>
        /// <returns>ErsMallOrder</returns>
        public virtual ErsMallOrder GetErsMallOrderWithDNo(string d_no)
        {
            var repository = this.GetErsMallOrderRepository();
            var criteria = this.GetErsMallOrderCriteria();

            criteria.d_no = d_no;

            var list = repository.Find(criteria);

            if (list.Count != 1)
            {
                return null;
            }

            return list[0];
        }

        #endregion

        #region モール伝票明細 [Mall order detail]
        /// <summary>
        /// モール伝票明細エンティティ取得 [Get mall order detail entity]
        /// </summary>
        /// <returns>ErsMallOrderDetail</returns>
        public virtual ErsMallOrderDetail GetErsMallOrderDetail()
        {
            return new ErsMallOrderDetail();
        }

        /// <summary>
        /// 楽天モール伝票明細エンティティ取得 [Get Rakuten mall order detail entity]
        /// </summary>
        /// <returns>ErsMallOrderDetailRakuten</returns>
        public virtual ErsMallOrderDetailRakuten GetErsMallOrderDetailRakuten()
        {
            return new ErsMallOrderDetailRakuten();
        }

        /// <summary>
        /// Amazonモール伝票明細エンティティ取得 [Get Amazon mall order detail entity]
        /// </summary>
        /// <returns>ErsMallOrderDetailAmazon</returns>
        public virtual ErsMallOrderDetailAmazon GetErsMallOrderDetailAmazon()
        {
            return new ErsMallOrderDetailAmazon();
        }
        
        /// <summary>
        /// モール伝票明細リポジトリ取得 [Get mall order detail repository]
        /// </summary>
        /// <param name="type">DBタイプ [Type of DB]</param>
        /// <returns></returns>
        public virtual ErsMallOrderDetailRepository GetErsMallOrderDetailRepository()
        {
            return new ErsMallOrderDetailRepository();
        }

        /// <summary>
        /// モール伝票明細クライテリア取得 [Get mall order detail criteria]
        /// </summary>
        /// <returns>ErsMallOrderDetailCriteria</returns>
        public virtual ErsMallOrderDetailCriteria GetErsMallOrderDetailCriteria()
        {
            return new ErsMallOrderDetailCriteria();
        }

        /// <summary>
        /// モール伝票明細エンティティ取得（パラメータから） [Get mall order detail entity (from parameters)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsMallOrderDetail</returns>
        public virtual ErsMallOrderDetail GetErsMallOrderDetailWithParameters(Dictionary<string, object> dicParams)
        {
            var obj = this.GetErsMallOrderDetail();
            obj.OverwriteWithParameter(dicParams);
            return obj;
        }

        /// <summary>
        /// モール伝票明細エンティティ取得（IDから） [Get mall order detail entity (from ID)]
        /// </summary>
        /// <param name="dbType">EnumDBType</param>
        /// <param name="id">id or bridge_id</param>
        /// <returns>ErsMallOrderDetail</returns>
        public virtual ErsMallOrderDetail GetErsMallOrderDetailWithID(int id)
        {
            var repository = this.GetErsMallOrderDetailRepository();
            var criteria = this.GetErsMallOrderDetailCriteria();

            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count != 1)
            {
                return null;
            }

            return list[0];
        }

        #endregion

        #region モール伝票取り込み管理 [Mall order import]
        /// <summary>
        /// モール伝票取り込み管理エンティティ取得 [Get mall order import]
        /// </summary>
        /// <returns>ErsMallOrderImport</returns>
        public virtual ErsMallOrderImport GetErsMallOrderImport()
        {
            return new ErsMallOrderImport();
        }

        /// <summary>
        /// モール伝票取り込み管理リポジトリ取得 [Get mall order import repository]
        /// </summary>
        /// <returns></returns>
        public virtual ErsMallOrderImportRepository GetErsMallOrderImportRepository()
        {
            return new ErsMallOrderImportRepository();
        }

        /// <summary>
        /// モール伝票取り込み管理クライテリア取得 [Get mall order import criteria]
        /// </summary>
        /// <returns>ErsMallOrderImportCriteria</returns>
        public virtual ErsMallOrderImportCriteria GetErsMallOrderImportCriteria()
        {
            return new ErsMallOrderImportCriteria();
        }

        /// <summary>
        /// モール伝票取り込み管理エンティティ取得（パラメータから） [Get mall order import entity (from parameters)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsMallOrderImport</returns>
        public virtual ErsMallOrderImport GetErsMallOrderImportWithParameters(Dictionary<string, object> dicParams)
        {
            var obj = this.GetErsMallOrderImport();
            obj.OverwriteWithParameter(dicParams);
            return obj;
        }

        /// <summary>
        /// モール伝票取り込み管理エンティティ取得（IDから） [Get mall order import entity (from ID)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsMallOrderImport</returns>
        public virtual ErsMallOrderImport GetErsMallOrderImportWithID(int id)
        {
            //repository取得
            var repository = this.GetErsMallOrderImportRepository();

            //criteria取得
            var criteria = this.GetErsMallOrderImportCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count != 1)
            {
                return null;
            }

            return list[0];
        }

        /// <summary>
        /// モール伝票取り込み管理登録・更新取得 [Get update and register mall order import]
        /// </summary>
        /// <returns>ObtainHarcGetOrderDateParamStgy</returns>
        public virtual UpdateAndRegisterMallOrderImportStgy GetUpdateAndRegisterMallOrderImportStgy()
        {
            return new UpdateAndRegisterMallOrderImportStgy();
        }

        /// <summary>
        /// Array型変換取得 [Get convert array]
        /// </summary>
        /// <returns>ConvertArrayStgy</returns>
        public virtual ConvertArrayStgy GetConvertArrayStgy()
        {
            return new ConvertArrayStgy();
        }

        /// <summary>
        /// 氏名分割取得 [Get name divide]
        /// </summary>
        /// <returns>NameDivideStgy</returns>
        public virtual NameDivideStgy GetNameDivideStgy()
        {
            return new NameDivideStgy();
        }

        /// <summary>
        /// 都道府県抽出取得 [Get prefectures extraction]
        /// </summary>
        /// <returns>PrefecturesExtractionStgy</returns>
        public virtual PrefecturesExtractionStgy GetPrefecturesExtractionStgy()
        {
            return new PrefecturesExtractionStgy();
        }

        /// <summary>
        /// モール伝票取検索期間検索用SPEC取得 [Get order date param search specification]
        /// </summary>
        /// <returns>OrderDateParamSearchSpecification</returns>
        public virtual OrderDateParamSearchSpecification GetOrderDateParamSearchSpecification()
        {
            return new OrderDateParamSearchSpecification();
        }

        #endregion

        #region 楽天国マスタ [raukten country]
        /// <summary>
        /// 楽天国マスタエンティティ取得 [Get raukten country]
        /// </summary>
        /// <returns>ErsRakutenCountry</returns>
        public virtual ErsMallRakutenCountry GetErsMallRakutenCountry()
        {
            return new ErsMallRakutenCountry();
        }

        /// <summary>
        /// 楽天国マスタリポジトリ取得 [Get raukten country repository]
        /// </summary>
        /// <returns></returns>
        public virtual ErsMallRakutenCountryRepository GetErsMallRakutenCountryRepository()
        {
            return new ErsMallRakutenCountryRepository();
        }

        /// <summary>
        /// 楽天国マスタクライテリア取得 [Get raukten country criteria]
        /// </summary>
        /// <returns>ErsRakutenCountryCriteria</returns>
        public virtual ErsMallRakutenCountryCriteria GetErsMallRakutenCountryCriteria()
        {
            return new ErsMallRakutenCountryCriteria();
        }

        /// <summary>
        /// 楽天国マスタエンティティ取得（パラメータから） [Get raukten country entity (from parameters)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsRakutenCountry</returns>
        public virtual ErsMallRakutenCountry GetErsMallRakutenCountryWithParameters(Dictionary<string, object> dicParams)
        {
            var obj = this.GetErsMallRakutenCountry();
            obj.OverwriteWithParameter(dicParams);
            return obj;
        }

        /// <summary>
        /// 楽天国マスタエンティティ取得（IDから） [Get raukten country entity (from ID)]
        /// </summary>
        /// <param name="parameters">パラメータディクショナリ [Dictionary of parameters]</param>
        /// <returns>ErsRakutenCountry</returns>
        public virtual ErsMallRakutenCountry GetErsMallRakutenCountryWithID(int id)
        {
            //repository取得
            var repository = this.GetErsMallRakutenCountryRepository();

            //criteria取得
            var criteria = this.GetErsMallRakutenCountryCriteria();
            criteria.id = id;

            var list = repository.Find(criteria);

            if (list.Count != 1)
            {
                return null;
            }

            return list[0];
        }

        #endregion

        #region モール伝票ステータス [Mall order status]
        /// <summary>
        /// モール伝票ステータス更新クラス（基底）取得 [Get the class for update mall order status (Base)]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopType">店舗タイプ [Type of shop]</param>
        /// <returns>Instance of UpdateMallOrderStatusBase</returns>
        public virtual UpdateMallOrderStatusBase GetUpdateMallOrderStatusBase(int? siteId, EnumMallShopKbn? shopKbn)
        {
            return new UpdateMallOrderStatusBase(siteId, shopKbn);
        }

        /// <summary>
        /// モール伝票ステータス更新クラス（楽天）取得 [Get the class for update mall order status (Rakuten)]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopType">店舗タイプ [Type of shop]</param>
        /// <returns>Instance of UpdateMallOrderStatusRakuten</returns>
        public virtual UpdateMallOrderStatusRakuten GetUpdateMallOrderStatusRakuten(int? siteId, EnumMallShopKbn? shopKbn)
        {
            return new UpdateMallOrderStatusRakuten(siteId, shopKbn);
        }

        /// <summary>
        /// モール伝票ステータス更新クラス（Yahoo!）取得 [Get the class for update mall order status (Yahoo!)]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopType">店舗タイプ [Type of shop]</param>
        /// <returns>Instance of UpdateMallOrderStatusYahoo</returns>
        public virtual UpdateMallOrderStatusYahoo GetUpdateMallOrderStatusYahoo(int? siteId, EnumMallShopKbn? shopKbn)
        {
            return new UpdateMallOrderStatusYahoo(siteId, shopKbn);
        }

        /// <summary>
        /// モール伝票ステータス更新クラス（Amazon）取得 [Get the class for update mall order status (Amazon)]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopType">店舗タイプ [Type of shop]</param>
        /// <returns>Instance of UpdateMallOrderStatusAmazon</returns>
        public virtual UpdateMallOrderStatusAmazon GetUpdateMallOrderStatusAmazon(int? siteId, EnumMallShopKbn? shopKbn)
        {
            return new UpdateMallOrderStatusAmazon(siteId, shopKbn);
        }

        /// <summary>
        /// 更新用モール伝票ステータス取得取得 [Get the calss for get mall order status for update]
        /// </summary>
        /// <returns>Instance of ObtainMallOrderStatusForUpdateStgy</returns>
        public virtual ObtainMallOrderStatusForUpdateStgy GetObtainMallOrderStatusForUpdateStgy()
        {
            return new ObtainMallOrderStatusForUpdateStgy();
        }

        /// <summary>
        /// モール伝票ステータス更新取得 [Get the class for update mall order status]
        /// </summary>
        /// <returns>Instance of UpdateMallOrderStatusStgy</returns>
        public virtual UpdateMallOrderStatusStgy GetUpdateMallOrderStatusStgy()
        {
            return new UpdateMallOrderStatusStgy();
        }
        #endregion

        #region モール伝票決済 [Mall order payment]
        /// <summary>
        /// モール伝票決済クラス（基底）取得 [Get the class for execute mall order payment (Base)]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopType">店舗タイプ [Type of shop]</param>
        /// <returns>Instance of ExecuteMallOrderPaymentBase</returns>
        public virtual ExecuteMallOrderPaymentBase GetExecuteMallOrderPaymentBase(int? siteId, EnumMallShopKbn? shopKbn)
        {
            return new ExecuteMallOrderPaymentBase(siteId, shopKbn);
        }

        /// <summary>
        /// モール伝票決済クラス（楽天）取得 [Get the class for execute mall order payment (Rakuten)]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopType">店舗タイプ [Type of shop]</param>
        /// <returns>Instance of ExecuteMallOrderPaymentRakuten</returns>
        public virtual ExecuteMallOrderPaymentRakuten GetExecuteMallOrderPaymentRakuten(int? siteId, EnumMallShopKbn? shopKbn)
        {
            return new ExecuteMallOrderPaymentRakuten(siteId, shopKbn);
        }

        /// <summary>
        /// モール伝票決済クラス（Yahoo!）取得 [Get the class for execute mall order payment (Yahoo!)]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopType">店舗タイプ [Type of shop]</param>
        /// <returns>Instance of ExecuteMallOrderPaymentYahoo</returns>
        public virtual ExecuteMallOrderPaymentYahoo GetExecuteMallOrderPaymentYahoo(int? siteId, EnumMallShopKbn? shopKbn)
        {
            return new ExecuteMallOrderPaymentYahoo(siteId, shopKbn);
        }

        /// <summary>
        /// モール伝票決済パラメータ取得 [Get the parameter for mall order payment]
        /// </summary>
        /// <returns>Instance of MallOrderPaymentParam</returns>
        public virtual MallOrderPaymentParam GetMallOrderPaymentParam()
        {
            return new MallOrderPaymentParam();
        }

        /// <summary>
        /// モール伝票決済処理 [Get the class for execute mall order payment]
        /// </summary>
        /// <returns>Instance of ExecuteMallOrderPaymentStgy</returns>
        public virtual ExecuteMallOrderPaymentStgy GetExecuteMallOrderPaymentStgy()
        {
            return new ExecuteMallOrderPaymentStgy();
        }
        #endregion

        #region その他処理クラス [Other strategy]
        /// <summary>
        /// モール伝票メールアドレスバリデート取得 [Get the class for Validate mall order email]
        /// </summary>
        /// <returns>Instance of ValidateMallOrderEmailStgy</returns>
        public virtual ValidateMallOrderEmailStgy GetValidateMallOrderEmailStgy()
        {
            return new ValidateMallOrderEmailStgy();
        }

        #endregion
    }
}
