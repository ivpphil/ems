﻿using System;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票取り込み管理クライテリア [Criteria for mall order import table]
    /// </summary>
    public class ErsMallOrderImportCriteria
        : Criteria
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_order_import_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID [Site id]
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_order_import_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 店舗タイプ [Type of shop]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_order_import_t.mall_shop_kbn", Convert.ToInt32(value), Operation.EQUAL));
            }
        }

        /// <summary>
        /// 取り込み日 [Date of import]
        /// </summary>
        public virtual DateTime import_time
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_order_import_t.import_time", value.ToString("yyyy/MM/dd"), Operation.EQUAL));
            }
        }

        /// <summary>
        /// 取り込み日 field &lt; value [Date of import field &lt; value]
        /// </summary>
        public virtual DateTime import_time_less_than
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_order_import_t.import_time", value.ToString("yyyy/MM/dd"), Operation.LESS_THAN));
            }
        }

        /// <summary>
        /// 取り込み済みフラグ [Flags for imported]
        /// </summary>
        public virtual EnumImported? imported
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_order_import_t.imported", Convert.ToInt32(value), Operation.EQUAL));
            }
        }

        /// <summary>
        /// 取り込み日ソート [for sorting of records using import_time]
        /// </summary> 
        public void SetOrderByImportTime(OrderBy orderBy)
        {
            AddOrderBy("mall_order_import_t.import_time", orderBy);
        }
    }
}
