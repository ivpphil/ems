﻿using System;
using System.Collections;
using System.Collections.Generic;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// Yahooモール伝票ヘッダエンティティ [Yahoo Entity for mall order table]
    /// </summary>
    public class ErsMallOrderYahoo
        : ErsMallOrder
    {
        /// <summary>
        /// プロパティへの値セット [Set properties]
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="d_no"></param>
        /// <param name="site_id"></param>
        /// <param name="mall_shop_kbn"></param>
        public override void SetProperties(Dictionary<string, object> dictionary, string d_no, int? site_id, EnumMallShopKbn? mall_shop_kbn)
        {
            //strategy取得
            var convertStgy = ErsMallFactory.ersMallOrderFactory.GetConvertArrayStgy();

            var other = (Dictionary<string, object>)dictionary["other"];

            if (other.ContainsKey("from_order_code"))
            {
                this.y_from_order_code = Convert.ToString(other["from_order_code"]);
            }
            if (other.ContainsKey("branch_code"))
            {
                this.y_branch_code = Convert.ToString(other["branch_code"]);
            }
            if (other.ContainsKey("is_mobile_order"))
            {
                this.y_is_mobile_order = Convert.ToString(other["is_mobile_order"]);
            }
            if (other.ContainsKey("release_date") && other["release_date"] != null && Convert.ToString(other["release_date"]) != "")
            {
                this.y_release_date = Convert.ToDateTime(other["release_date"]);
            }
            if (other.ContainsKey("royalty_fix_date") && other["royalty_fix_date"] != null && Convert.ToString(other["royalty_fix_date"]) != "")
            {
                this.y_royalty_fix_date = Convert.ToDateTime(other["royalty_fix_date"]);
            }
            if (other.ContainsKey("temp_point") && other["temp_point"] != null && Convert.ToString(other["temp_point"]) != "")
            {
                this.y_temp_point = Convert.ToInt32(other["temp_point"]);
            }
            if (other.ContainsKey("point_auto_complete_date") && other["point_auto_complete_date"] != null && Convert.ToString(other["point_auto_complete_date"]) != "")
            {
                this.y_point_auto_complete_date = Convert.ToDateTime(other["point_auto_complete_date"]);
            }
            if (other.ContainsKey("royalty_processing_date") && other["royalty_processing_date"] != null && Convert.ToString(other["royalty_processing_date"]) != "")
            {
                this.y_royalty_processing_date = Convert.ToDateTime(other["royalty_processing_date"]);
            }
            if (other.ContainsKey("fix_point") && other["fix_point"] != null && Convert.ToString(other["fix_point"]) != "")
            {
                this.y_fix_point = Convert.ToInt32(other["fix_point"]);
            }
            if (other.ContainsKey("contact"))
            {
                this.y_contact = Convert.ToString(other["contact"]);
            }
            if (other.ContainsKey("card_payment_type"))
            {
                this.y_card_payment_type = Convert.ToString(other["card_payment_type"]);
            }
            if (other.ContainsKey("card_approval_number"))
            {
                this.y_card_approval_number = Convert.ToString(other["card_approval_number"]);
            }
            if (other.ContainsKey("card_payment_status"))
            {
                this.y_payment_status = Convert.ToString(other["card_payment_status"]);
            }
            if (other.ContainsKey("payment_remarks"))
            {
                this.y_payment_remarks = Convert.ToString(other["payment_remarks"]);
            }
            if (other.ContainsKey("gift_wrap"))
            {
                this.y_gift_wrap = Convert.ToString(other["gift_wrap"]);
            }
            if (other.ContainsKey("naire"))
            {
                this.y_naire = Convert.ToString(other["naire"]);
            }
            if (other.ContainsKey("caution"))
            {
                this.y_caution = Convert.ToString(other["caution"]);
            }
            if (other.ContainsKey("caution_reason"))
            {
                this.y_caution_reason = Convert.ToString(other["caution_reason"]);
            }
            if (other.ContainsKey("options"))
            {
                this.y_options = convertStgy.ConvertToStringFromArrayListMultilayer((ArrayList)other["options"], "{1}", ", ", ", ");
            }

            base.SetProperties(dictionary, d_no, site_id, mall_shop_kbn);
        }
    }
}
