﻿using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// 楽天国マスタリポジトリ [Repository for raukten country]
    /// </summary>
    public class ErsMallRakutenCountryRepository
        : ErsRepository<ErsMallRakutenCountry>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        public ErsMallRakutenCountryRepository()
            : base("mall_rakuten_country_t")
        {
        }

        /// <summary>
        /// インサート [Insert]
        /// </summary>
        /// <param name="obj">楽天国マスタエンティティ [raukten country entity]</param>
        public void Insert(ErsMallRakutenCountry obj)
        {
            this.ersDB_table.gInsert(obj.GetPropertiesAsDictionary(this.ersDB_table));
        }

        /// <summary>
        /// アップデート [Update]
        /// </summary>
        /// <param name="old_obj">旧楽天国マスタエンティティ [Old raukten country entity]</param>
        /// <param name="new_obj">新楽天国マスタエンティティ [New raukten country entity]</param>
        public override void Update(ErsMallRakutenCountry old_obj, ErsMallRakutenCountry new_obj)
        {
            var newDic = new_obj.GetPropertiesAsDictionary(this.ersDB_table);
            var oldDic = old_obj.GetPropertiesAsDictionary(this.ersDB_table);

            var changedColumns = ErsCommon.GetChangedKeys(oldDic, newDic);

            if (changedColumns.Length > 0)
            {
                this.ersDB_table.gUpdateColumn(changedColumns, newDic, "WHERE id = " + old_obj.id);
            }
        }

        /// <summary>
        /// デリート [Delete]
        /// </summary>
        /// <param name="obj">楽天国マスタエンティティ [raukten country entity]</param>
        public override void Delete(ErsMallRakutenCountry obj)
        {
            ErsMallRakutenCountryCriteria criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallRakutenCountryCriteria();
            criteria.id = obj.id;

            this.ersDB_table.gDelete(criteria);
        }

        /// <summary>
        /// データリスト取得 [Get list of data]
        /// </summary>
        /// <param name="criteria">楽天国マスタクライテリア [Criteria for raukten country]</param>
        /// <returns>データリスト [List of data]</returns>
        public override IList<ErsMallRakutenCountry> Find(Criteria criteria)
        {
            var listRet = new List<ErsMallRakutenCountry>();

            var list = this.ersDB_table.gSelect(criteria);

            foreach (var dr in list)
            {
                var obj = ErsMallFactory.ersMallOrderFactory.GetErsMallRakutenCountryWithParameters(dr);
                listRet.Add(obj);
            }

            return listRet;
        }

        /// <summary>
        /// データ数取得 [Get count of data]
        /// </summary>
        /// <param name="criteria">楽天国マスタクライテリア [Criteria for raukten country]</param>
        /// <returns>データ数 [Count of data]</returns>
        public override long GetRecordCount(Criteria criteria)
        {
            return this.ersDB_table.gSelectCount("id", criteria);
        }
    }
}
