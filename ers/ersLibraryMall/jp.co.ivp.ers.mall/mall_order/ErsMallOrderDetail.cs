﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mall.common;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票明細エンティティ [Entity for mall order detail table]
    /// </summary>
    public class ErsMallOrderDetail
        : ErsRepositoryEntity
    {
        /// <summary>
        /// 商品キー [Item key]
        /// </summary>
        public virtual string item_key
        {
            get
            {
                return this.item_code;
            }
        }

        /// <summary>
        /// ERS用商品コード [Product code for ERS]
        /// </summary>
        public virtual string ers_item_code
        {
            get
            {
                return this.trimed_item_code.HasValue() ? ErsMallCommonService.GetErsSkuFromMall(this.trimed_item_code) : this.trimed_item_code;
            }
        }

        /// <summary>
        /// 商品番号（トリム） [Item code (Trimed)]
        /// </summary>
        public virtual string trimed_item_code
        {
            get
            {
                return this.item_code.HasValue() ? this.item_code.Trim() : this.item_code;
            }
        }

        /// <summary>
        /// ID [ID]
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// 登録日時 [Insert time]
        /// </summary>
        public virtual DateTime? intime { get; set; }

        /// <summary>
        /// 更新日時 [Update time]
        /// </summary>
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// 伝票番号 [D no]
        /// </summary>
        public virtual string d_no { get; set; }

        /// <summary>
        /// 受注番号 [Order code]
        /// </summary>
        public virtual string order_code { get; set; }

        /// <summary>
        /// 商品番号 [Item code]
        /// </summary>
        public virtual string item_code { get; set; }

        /// <summary>
        /// 商品名 [Item name]
        /// </summary>
        public virtual string item_name { get; set; }

        /// <summary>
        /// 規格名 [Item select}
        /// </summary>
        public virtual string item_select { get; set; }

        /// <summary>
        /// 単価 [Unit price]
        /// </summary>
        public virtual int unit_price { get; set; }

        /// <summary>
        /// 数量 [Quantity]
        /// </summary>
        public virtual int? quantity { get; set; }

        /// <summary>
        /// 送付先氏名 [Ship name]
        /// </summary>
        public virtual string ship_name { get; set; }

        /// <summary>
        /// 送付先氏名カナ [Ship kana]
        /// </summary>
        public virtual string ship_kana { get; set; }

        /// <summary>
        /// 送付先郵便番号 [Ship zipcode]
        /// </summary>
        public virtual string ship_zipcode { get; set; }

        /// <summary>
        /// 送付先住所 [Ship address]
        /// </summary>
        public virtual string ship_address { get; set; }

        /// <summary>
        /// 送付先電話番号 [Ship phone]
        /// </summary>
        public virtual string ship_phone { get; set; }

        /// <summary>
        /// 配送方法 [Delivery method]
        /// </summary>
        public virtual string delivery_method { get; set; }

        /// <summary>
        /// 配送希望日 [Delivery hope date]
        /// </summary>
        public virtual DateTime? delivery_hope_date { get; set; }

        /// <summary>
        /// 配送希望時間帯 [Delivery hope time]
        /// </summary>
        public virtual string delivery_hope_time { get; set; }

        /// <summary>
        /// 配送希望メモ [Delivery memo]
        /// </summary>
        public virtual string delivery_memo { get; set; }

        /// <summary>
        /// 出荷日 [Delivery date]
        /// </summary>
        public virtual DateTime? delivery_date { get; set; }

        /// <summary>
        /// 到着予定日 [Arrival schedule date]
        /// </summary>
        public virtual DateTime? arrival_schedule_date { get; set; }

        /// <summary>
        /// 到着予定時間帯 [Arrival schedule time]
        /// </summary>
        public virtual string arrival_schedule_time { get; set; }

        /// <summary>
        /// 配送会社 [Distributing company]
        /// </summary>
        public virtual string distributing_company { get; set; }

        /// <summary>
        /// 送り状番号 [Invoice number]
        /// </summary>
        public virtual string invoice_number { get; set; }

        /// <summary>
        /// お届け完了日 [Arrival date]
        /// </summary>
        public virtual DateTime? arrival_date { get; set; }

        /// <summary>
        /// 包装種別 [Gift wrap kind]
        /// </summary>
        public virtual string gift_wrap_kind { get; set; }

        /// <summary>
        /// ギフトメッセージ [Gift message]
        /// </summary>
        public virtual string gift_message { get; set; }

        /// <summary>
        /// のし [noshi]
        /// </summary>
        public virtual string noshi { get; set; }

        #region 楽天
        /// <summary>
        /// ラッピング情報 [Wrapping information]
        /// </summary>
        public virtual string r_wrapping { get; set; }

        /// <summary>
        /// 商品URL [Product url]
        /// </summary>
        public virtual string r_product_url { get; set; }

        /// <summary>
        /// ポイント倍率 [Point rate]
        /// </summary>
        public virtual string r_point_rate { get; set; }

        /// <summary>
        /// お届けの目安 [Shipping information]
        /// </summary>
        public virtual string r_ship_info { get; set; }

        /// <summary>
        /// 税種別 [Tax type]
        /// </summary>
        public virtual string r_tax_type { get; set; }

        /// <summary>
        /// 送料種別 [Delivery cost type]
        /// </summary>
        public virtual string r_delivery_cost_type { get; set; }

        /// <summary>
        /// 代引種別 [Receive cost type]
        /// </summary>
        public virtual string r_receive_cost_type { get; set; }
        #endregion

        #region Amazon

        /// <summary>
        /// ASIN [Amazon Asin]
        /// </summary>
        public virtual string a_asin { get; set; }

        /// <summary>
        /// OrderItemId [Amazon Item id]
        /// </summary>
        public virtual string a_item_id { get; set; }

        /// <summary>
        /// QuantityShipped [Amazon Quantity shipped]
        /// </summary>
        public virtual int? a_quantity_shipped { get; set; }

        /// <summary>
        /// ShippingPrice [Amazon Shipping price]
        /// </summary>
        public virtual int? a_shipping_price { get; set; }

        /// <summary>
        /// GiftWrapPrice [Amazon Giftwrap price]
        /// </summary>
        public virtual int? a_giftwrap_price { get; set; }

        /// <summary>
        /// ItemTax [Amazon Item tax]
        /// </summary>
        public virtual int? a_item_tax { get; set; }

        /// <summary>
        /// ShippingTax [Amazon Shipping tax]
        /// </summary>
        public virtual int? a_shipping_tax { get; set; }

        /// <summary>
        /// GiftWrapTax [Amazon Giftwrap tax]
        /// </summary>
        public virtual int? a_giftwrap_tax { get; set; }

        /// <summary>
        /// ShippingDiscount [Amazon Shipping discount]
        /// </summary>
        public virtual int? a_shipping_discount { get; set; }

        /// <summary>
        /// PromotionDiscount [Amazon Promotion discount]
        /// </summary>
        public virtual int? a_promotion_discount { get; set; }

        /// <summary>
        /// PromotionIds [Amazon Promotion IDs]
        /// </summary>
        public virtual string a_promotion_ids { get; set; }

        /// <summary>
        /// CODFee [Amazon Codfee]
        /// </summary>
        public virtual int? a_codfee { get; set; }

        /// <summary>
        /// CODFeeDiscount [Amazon Codfee discount]
        /// </summary>
        public virtual int? a_codfee_discount { get; set; }

        #endregion

        /// <summary>
        /// プロパティへの値セット [Set properties]
        /// </summary>
        public virtual void SetProperties(Dictionary<string, object> detail, string d_no, string order_code)
        {
            this.d_no = d_no;
            this.order_code = order_code;

            this.item_code = Convert.ToString(detail["item_code"]);
            this.item_name = Convert.ToString(detail["item_name"]);       
            this.item_select = Convert.ToString(detail["item_select"]);

            if (detail["unit_price"] != null && Convert.ToString(detail["unit_price"]) != "")
            {
                this.unit_price = Convert.ToInt32(detail["unit_price"]);
            }
            if (detail["quantity"] != null && Convert.ToString(detail["quantity"]) != "")
            {
                this.quantity = Convert.ToInt32(detail["quantity"]);
            }
            this.ship_name = Convert.ToString(detail["ship_name"]);
            this.ship_kana = Convert.ToString(detail["ship_kana"]);
            this.ship_zipcode = Convert.ToString(detail["ship_zipcode"]);
            this.ship_address = Convert.ToString(detail["ship_address"]);
            this.ship_phone = Convert.ToString(detail["ship_phone"]);
            this.delivery_method = Convert.ToString(detail["delivery_method"]);

            if (detail["delivery_hope_date"] != null && Convert.ToString(detail["delivery_hope_date"]) != "")
            {
                this.delivery_hope_date = Convert.ToDateTime(detail["delivery_hope_date"]);
            }

            this.delivery_hope_time = Convert.ToString(detail["delivery_hope_time"]);
            this.delivery_memo = Convert.ToString(detail["delivery_memo"]);

            if (detail["delivery_date"] != null && Convert.ToString(detail["delivery_date"]) != "")
            {
                this.delivery_date = Convert.ToDateTime(detail["delivery_date"]);
            }
            if (detail["arrival_schedule_date"] != null && Convert.ToString(detail["arrival_schedule_date"]) != "")
            {
                this.arrival_schedule_date = Convert.ToDateTime(detail["arrival_schedule_date"]);
            }

            this.arrival_schedule_time = Convert.ToString(detail["arrival_schedule_time"]);
            this.distributing_company = Convert.ToString(detail["distributing_company"]);
            this.invoice_number = Convert.ToString(detail["invoice_number"]);

            if (detail["arrival_date"] != null && Convert.ToString(detail["arrival_date"]) != "")
            {
                this.arrival_date = Convert.ToDateTime(detail["arrival_date"]);
            }

            this.gift_wrap_kind = Convert.ToString(detail["gift_wrap_kind"]);
            this.gift_message = Convert.ToString(detail["gift_message"]);
            this.noshi = Convert.ToString(detail["noshi"]);
        }
    }
}
