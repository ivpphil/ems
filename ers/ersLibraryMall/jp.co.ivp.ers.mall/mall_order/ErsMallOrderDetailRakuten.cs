﻿using System;
using System.Collections;
using System.Collections.Generic;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// 楽天モール伝票明細エンティティ [Rakuten Entity for mall order detail table]
    /// </summary>
    public class ErsMallOrderDetailRakuten
        : ErsMallOrderDetail
    {
        /// <summary>
        /// プロパティへの値セット [Set properties]
        /// </summary>
        public override void SetProperties(Dictionary<string, object> detail, string d_no, string order_code)
        {
            //strategy取得
            var convertStgy = ErsMallFactory.ersMallOrderFactory.GetConvertArrayStgy();

            //楽天 other内のデータを格納
            var detailOther = (Dictionary<string, object>)detail["other"];

            if (detailOther.ContainsKey("wrapping"))
            {
                this.r_wrapping = Convert.ToString(detailOther["wrapping"]);
            }
            if (detailOther.ContainsKey("product_url"))
            {
                this.r_product_url = Convert.ToString(detailOther["product_url"]);
            }
            if (detailOther.ContainsKey("point_rate"))
            {
                this.r_point_rate = Convert.ToString(detailOther["point_rate"]);
            }
            if (detailOther.ContainsKey("ship_info"))
            {
                this.r_ship_info = Convert.ToString(detailOther["ship_info"]);
            }
            if (detailOther.ContainsKey("tax_type"))
            {
                this.r_tax_type = Convert.ToString(detailOther["tax_type"]);
            }
            if (detailOther.ContainsKey("delivery_cost_type"))
            {
                this.r_delivery_cost_type = Convert.ToString(detailOther["delivery_cost_type"]);
            }
            if (detailOther.ContainsKey("receive_cost_type"))
            {
                this.r_receive_cost_type = Convert.ToString(detailOther["receive_cost_type"]);
            }

            base.SetProperties(detail, d_no, order_code);
        }
    }
}
