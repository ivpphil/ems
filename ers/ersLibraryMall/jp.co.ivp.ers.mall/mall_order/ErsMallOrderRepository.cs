﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票ヘッダリポジトリ [Repository for mall order table]
    /// </summary>
    public class ErsMallOrderRepository
        : ErsRepository<ErsMallOrder>
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="type">DBタイプ [Type of DB]</param>
        public ErsMallOrderRepository()
            : base("mall_d_master_t")
        {
        }

        /// <summary>
        /// インサート [Insert]
        /// </summary>
        /// <param name="obj">モール伝票ヘッダエンティティ [Mall order entity]</param>
        public void Insert(ErsMallOrder obj)
        {
            //idをシーケンスから取得する
            obj.id = this.ersDB_table.GetNextSequence();
            this.ersDB_table.gInsert(obj.GetPropertiesAsDictionary(this.ersDB_table));
        }

        /// <summary>
        /// アップデート [Update]
        /// </summary>
        /// <param name="old_obj">旧モール伝票ヘッダエンティティ [Old mall order entity]</param>
        /// <param name="new_obj">新モール伝票ヘッダエンティティ [New mall order entity]</param>
        public override void Update(ErsMallOrder old_obj, ErsMallOrder new_obj)
        {
            var newDic = new_obj.GetPropertiesAsDictionary(this.ersDB_table);
            var changedColumns = this.GetChangedColumns(old_obj, new_obj);

            if (changedColumns.Length > 0)
            {
                this.ersDB_table.gUpdateColumn(changedColumns, newDic, "WHERE id = " + old_obj.id);
            }
        }

        /// <summary>
        /// 差分取得 [Get changed columns]
        /// </summary>
        /// <param name="old_obj">旧モール伝票ヘッダエンティティ [Old mall order entity]</param>
        /// <param name="new_obj">新モール伝票ヘッダエンティティ [New mall order entity]</param>
        /// <returns>string[]</returns>
        public virtual string[] GetChangedColumns(ErsMallOrder old_obj, ErsMallOrder new_obj)
        {
            var newDic = new_obj.GetPropertiesAsDictionary(this.ersDB_table);
            var oldDic = old_obj.GetPropertiesAsDictionary(this.ersDB_table);

            return ErsCommon.GetChangedKeys(oldDic, newDic);
        }

        /// <summary>
        /// デリート [Delete]
        /// </summary>
        /// <param name="obj">モール伝票ヘッダエンティティ [Mall order entity]</param>
        public override void Delete(ErsMallOrder obj)
        {
            ErsMallOrderCriteria criteria = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderCriteria();
            criteria.id = obj.id;

            this.ersDB_table.gDelete(criteria);
        }

        /// <summary>
        /// データリスト取得 [Get list of data]
        /// </summary>
        /// <param name="criteria">モール伝票ヘッダクライテリア [Criteria for mall order]</param>
        /// <returns>データリスト [List of data]</returns>
        public override IList<ErsMallOrder> Find(Criteria criteria)
        {
            var listRet = new List<ErsMallOrder>();

            var list = this.ersDB_table.gSelect(criteria);

            foreach (var dr in list)
            {
                var obj = ErsMallFactory.ersMallOrderFactory.GetErsMallOrderWithParameters(dr);
                listRet.Add(obj);
            }

            return listRet;
        }

        /// <summary>
        /// データ数取得 [Get count of data]
        /// </summary>
        /// <param name="criteria">モール伝票ヘッダクライテリア [Criteria for mall order]</param>
        /// <returns>データ数 [Count of data]</returns>
        public override long GetRecordCount(Criteria criteria)
        {
            return this.ersDB_table.gSelectCount("id", criteria);
        }

        /// <summary>
        /// 次の伝票番号取得 [Get nexr d no]
        /// </summary>
        /// <returns>string</returns>
        public virtual string GetNextDNo()
        {
            return Convert.ToString(this.ersDB_table.GetNextSequence("mall_d_master_t_d_no_seq"));
        }
    }
}
