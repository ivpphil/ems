﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票ヘッダクライテリア [Criteria for mall order table]
    /// </summary>
    public class ErsMallOrderCriteria
        : Criteria
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_d_master_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public virtual int? site_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_d_master_t.site_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 店舗タイプ [Shop type]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_d_master_t.mall_shop_kbn", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// マージステータス [Merge status]
        /// </summary>
        public virtual EnumMergeStatus? merge_status
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_d_master_t.merge_status", (int)value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 伝票番号 [D No]
        /// </summary>
        public virtual string d_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_d_master_t.d_no", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 受注番号 [Order code]
        /// </summary>
        public virtual string order_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_d_master_t.order_code", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 受注ステータスID [Order statud id]
        /// </summary>
        public virtual int? order_status_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_d_master_t.order_status_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 受注ステータスID（IN） [Order statud id (IN)]
        /// </summary>
        public virtual IEnumerable<int> order_status_id_in
        {
            set
            {
                this.Add(Criteria.GetInClauseCriterion("mall_d_master_t.order_status_id", value));
            }
        }

        /// <summary>
        /// 受注日時（開始） [Order date (from)]
        /// </summary>
        public virtual DateTime order_date_from
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_d_master_t.order_date", value, Operation.GREATER_EQUAL));
            }
        }

        /// <summary>
        /// 受注日時（終了） [Order date (to)]
        /// </summary>
        public virtual DateTime order_date_to
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_d_master_t.order_date", value, Operation.LESS_EQUAL));
            }
        }


        /// <summary>
        ///  ソート：id [Sorting : id]
        /// </summary>
        /// <param name="orderBy">ソート [Sorting]</param>
        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("mall_d_master_t.id", orderBy);
        }
    }
}
