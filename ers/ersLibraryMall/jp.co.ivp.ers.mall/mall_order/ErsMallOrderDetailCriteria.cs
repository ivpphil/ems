﻿using System;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票明細クライテリア [Criteria for mall order detail table]
    /// </summary>
    public class ErsMallOrderDetailCriteria
        : Criteria
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public virtual int? id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_ds_master_t.id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 中間ID [Bridge ID]
        /// </summary>
        public virtual int? bridge_id
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_ds_master_t.bridge_id", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 伝票番号 [d no]
        /// </summary>
        public virtual string d_no
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_ds_master_t.d_no", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 受注番号 [Order code]
        /// </summary>
        public virtual string order_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_ds_master_t.order_code", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 商品番号 [Item code]
        /// </summary>
        public virtual string item_code
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_ds_master_t.item_code", value, Operation.EQUAL));
            }
        }

        /// <summary>
        /// 数量（&gt;） [Quantity (&gt;)]
        /// </summary>
        public virtual int? quantity_greater
        {
            set
            {
                this.Add(Criteria.GetCriterion("mall_ds_master_t.quantity", value, Operation.GREATER_THAN));
            }
        }


        /// <summary>
        ///  ソート：id [Sorting : id]
        /// </summary>
        /// <param name="orderBy">ソート [Sorting]</param>
        public virtual void SetOrderById(OrderBy orderBy)
        {
            this.AddOrderBy("mall_ds_master_t.id", orderBy);
        }
    }
}
