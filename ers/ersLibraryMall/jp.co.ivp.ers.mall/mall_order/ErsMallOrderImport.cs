﻿using System;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// モール伝票取り込み管理エンティティ [Entity for mall order import table]
    /// </summary>
    public class ErsMallOrderImport
        : ErsRepositoryEntity
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        public override int? id { get; set; }

        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public virtual int? site_id { get; set; }

        /// <summary>
        /// 店舗タイプ [Type of shop]
        /// </summary>
        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// 取り込み日 [Date of import]
        /// </summary>
        public virtual DateTime? import_time { get; set; }

        /// <summary>
        /// 取り込み済みフラグ [Flags for imported]
        /// </summary>
        public virtual EnumImported? imported { get; set; }
    }
}
