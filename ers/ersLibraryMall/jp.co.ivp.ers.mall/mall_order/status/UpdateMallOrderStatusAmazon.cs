﻿using System;
using jp.co.ivp.ers.mall.api.order_status;

namespace jp.co.ivp.ers.mall.mall_order.status
{
    /// <summary>
    /// モール伝票ステータス更新クラス（Amazon） [Class for update mall order status (Amazon)]
    /// </summary>
    public class UpdateMallOrderStatusAmazon
        : UpdateMallOrderStatusBase
    {
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public UpdateMallOrderStatusAmazon(int? siteId, EnumMallShopKbn? shopKbn)
            : base(siteId, shopKbn)
        {
        }
        #endregion

        #region ステータス更新 [Update status]
        /// <summary>
        /// ステータス変更API取得 [Get the API for change status]
        /// </summary>
        /// <param name="dateOrder">注文日時 [Order datetime]</param>
        /// <returns>ステータス変更API [The API for change status]</returns>
        protected override ChangeOrderStatusAPIBase GetChangeOrderStatusAPI(DateTime dateOrder)
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIParamAmazon();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);
            apiParam.shop_name = mallSetup.GetShopName(this.siteId.Value);

            return ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIAmazon(apiParam);
        }
        #endregion
    }
}
