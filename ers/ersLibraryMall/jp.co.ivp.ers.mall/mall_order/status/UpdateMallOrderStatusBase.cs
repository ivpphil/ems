﻿using System;
using System.Collections.Generic;
using com.hunglead.harc;
using jp.co.ivp.ers.mall.api;
using jp.co.ivp.ers.mall.api.order_status;

namespace jp.co.ivp.ers.mall.mall_order.status
{
    /// <summary>
    /// モール伝票ステータス更新クラス（基底） [Class for update mall order status (Base)]
    /// </summary>
    public class UpdateMallOrderStatusBase
    {
        #region 基本パラメータ [Basic parameters]
        /// <summary>
        /// サイトID [Site ID]
        /// </summary>
        public virtual int? siteId { get; protected set; }

        /// <summary>
        /// 店舗タイプ [Shop type]
        /// </summary>
        public virtual EnumMallShopKbn? shopKbn { get; protected set; }
        #endregion


        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public UpdateMallOrderStatusBase(int? siteId, EnumMallShopKbn? shopKbn)
        {
            this.siteId = siteId;
            this.shopKbn = shopKbn;
        }
        #endregion

        #region ステータス更新 [Update status]
        /// <summary>
        /// ステータス更新 [Update status]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="order">伝票情報 [Order information]</param>
        /// <param name="updateStatus">更新ステータス [Update status]</param>
        /// <param name="delegateCheckErrorResult">エラー結果判定デリゲート [Delegate for check error result]</param>
        public virtual void UpdateStatus(HarcApiRequest request, UpdateOrderStatusParam order, EnumMallOrderStatus? updateStatus, IsHandleAbleUpdateErrorResultDelegate delegateCheckErrorResult)
        {
            // リトライカウント
            int retry = 0;

            // エラーメッセージ
            string errorMessage = string.Empty;

            // ハンドルメッセージ
            string handleMessage = string.Empty;

            // ステータス変更API取得 [Get the API for change status]
            var apiChangeOrderStatus = this.GetChangeOrderStatusAPI(order.orderDate.Value);

            while (true)
            {
                string errorResult = string.Empty;

                // ステータス変更 [Change status]
                var dicUpdateResult = this.ChangeStatus(request, order, apiChangeOrderStatus, updateStatus);

                if (dicUpdateResult.ContainsKey(order.orderCode))
                {
                    // 更新結果取得 [Get the result of update]
                    string updateResult = Convert.ToString(dicUpdateResult[order.orderCode]);

                    // 正常な結果かどうか [Is valid result]
                    if (!apiChangeOrderStatus.IsNormalResult(updateResult))
                    {
                        // リトライ可能なエラーかどうか [Is retryable error]
                        if (apiChangeOrderStatus.IsRetryAbleResultError(updateResult))
                        {
                            // 指定回数リトライ [Retry specified times]
                            if (retry++ < ChangeOrderStatusAPIBase.API_ERROR_RETRY_COUNT)
                            {
                                continue;
                            }
                        }

                        errorResult = updateResult;
                    }
                }
                // 更新結果なし [No update result]
                else
                {
                    errorResult = "No update result.";
                }

                // 失敗していた場合はエラーメッセージを代入する [Set the error message]
                if (!string.IsNullOrEmpty(errorResult))
                {
                    var tempMessage = ErsResources.GetMessage("101002", this.siteId, (int)this.shopKbn, (int)updateStatus, order.orderCode, errorResult);

                    // ハンドルエラー判定 [Judgement handleable error]
                    if (delegateCheckErrorResult(errorResult))
                    {
                        handleMessage = tempMessage;
                    }
                    else
                    {
                        errorMessage = tempMessage;
                    }
                }

                break;
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                throw new Exception(ErsResources.GetMessage("101000", errorMessage));
            }
            else if (!string.IsNullOrEmpty(handleMessage))
            {
                throw new UpdateMallOrderHandleException(ErsResources.GetMessage("101000", handleMessage));
            }
        }

        /// <summary>
        /// ハンドルエラー判定 [Judgement handleable error]
        /// </summary>
        /// <param name="errorResult">エラー結果 [Error result]</param>
        /// <returns>true : ハンドル可能 [Handleable] ／ false : ハンドル不可 [Not handleable]</returns>
        public delegate bool IsHandleAbleUpdateErrorResultDelegate(string errorResult);

        /// <summary>
        /// ステータス変更 [Change status]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        /// <param name="objMallOrder">モール伝票 [Mall order]</param>
        /// <param name="ChangeOrderStatusAPIBase">モール伝票ステータス変更API [API for update mall order status]</param>
        /// <param name="updateStatus">変更ステータス [Update status]</param>
        /// <returns>更新結果 [Update result]</returns>
        protected virtual Dictionary<string, object> ChangeStatus(HarcApiRequest request, UpdateOrderStatusParam order, ChangeOrderStatusAPIBase apiChangeOrderStatus, EnumMallOrderStatus? updateStatus)
        {
            try
            {
                // ステータス変更 [Change status]
                return apiChangeOrderStatus.ChangeOrderStatus(request, order, updateStatus);
            }
            catch (APIFailedException e)
            {
                string errorMessage = ErsResources.GetMessage("101001", this.siteId, (int)this.shopKbn, (int)updateStatus, order.orderCode) + e.ToString();
                throw new Exception(errorMessage);
            }
        }

        /// <summary>
        /// ステータス変更API取得 [Get the API for change status]
        /// </summary>
        /// <param name="dateOrder">注文日時 [Order datetime]</param>
        /// <returns>ステータス変更API [The API for change status]</returns>
        protected virtual ChangeOrderStatusAPIBase GetChangeOrderStatusAPI(DateTime dateOrder)
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIParamBase();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);
            apiParam.shop_name = mallSetup.GetShopName(this.siteId.Value);

            return ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIBase(apiParam);
        }
        #endregion
    }
}
