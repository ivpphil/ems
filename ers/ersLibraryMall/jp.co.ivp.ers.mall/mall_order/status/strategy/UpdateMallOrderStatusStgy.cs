﻿using System.Collections.Generic;
using jp.co.ivp.ers.mall.api.order_status;
using System;

namespace jp.co.ivp.ers.mall.mall_order.status.strategy
{
    /// <summary>
    /// モール伝票ステータス更新 [Update mall order status]
    /// </summary>
    public class UpdateMallOrderStatusStgy
    {
        /// <summary>
        /// モール伝票ステータス更新 [Update mall order status]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Shop type]</param>
        /// <param name="order">伝票情報 [Order information]</param>
        /// <param name="status">更新ステータス [Update status]</param>
        public void UpdateMallOrderStatus(int? siteId, EnumMallShopKbn? shopKbn, UpdateOrderStatusParam order, EnumMallOrderStatus? status)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            if (setup.debug && shopKbn == EnumMallShopKbn.AMAZON && status == EnumMallOrderStatus.Done)
            {
                throw new Exception("[ERS] Can't change the status of the amazon order to Shipped when setup.debug is true;(To avoid unnecessary billing.)");
            }

            // HARCログイン [Log in to HARC]
            var request = ErsMallFactory.ersMallCommonFactory.GetHarcLoginStgy().HarcLogin();

            // 更新クラスディクショナリ取得 [Get the dictionary for update class]
            var updateMallOrderStatus = this.GetUpdateClassDic(siteId, shopKbn);
            if (updateMallOrderStatus == null)
            {
                return;
            }

            // デリゲートディクショナリ取得 [Get the dictionary for delegate]
            var errorResultDelegate = this.GetDelegateDic(shopKbn);
            if (errorResultDelegate == null)
            {
                return;
            }

            // ステータス更新 [Update status]
            updateMallOrderStatus.UpdateStatus(request, order, status, errorResultDelegate);
        }

        #region 処理用ディクショナリ取得 [Get the dictionary for process]
        /// <summary>
        /// 更新クラスディクショナリ取得 [Get the dictionary for update class]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Shop type]</param>
        /// <returns>更新クラスディクショナリ [The dictionary for update class]</returns>
        protected UpdateMallOrderStatusBase GetUpdateClassDic(int? siteId, EnumMallShopKbn? mall_shop_kbn)
        {
            switch (mall_shop_kbn)
            {
                case EnumMallShopKbn.RAKUTEN:
                    // 楽天 [Rakuten]
                    return ErsMallFactory.ersMallOrderFactory.GetUpdateMallOrderStatusRakuten(siteId, EnumMallShopKbn.RAKUTEN);

                case EnumMallShopKbn.YAHOO:
                    // Yahoo! [Yahoo!]
                    return ErsMallFactory.ersMallOrderFactory.GetUpdateMallOrderStatusYahoo(siteId, EnumMallShopKbn.YAHOO);

                case EnumMallShopKbn.AMAZON:
                    // Amazon [amazon]
                    return ErsMallFactory.ersMallOrderFactory.GetUpdateMallOrderStatusAmazon(siteId, EnumMallShopKbn.AMAZON);
            }

            return null;
        }

        /// <summary>
        /// デリゲートディクショナリ取得 [Get the dictionary for delegate]
        /// </summary>
        /// <returns>デリゲートディクショナリ [The dictionary for delegate]</returns>
        protected UpdateMallOrderStatusBase.IsHandleAbleUpdateErrorResultDelegate GetDelegateDic(EnumMallShopKbn? mall_shop_kbn)
        {
            switch (mall_shop_kbn)
            {
                case EnumMallShopKbn.RAKUTEN:
                    // 楽天 [Rakuten]
                    return new UpdateMallOrderStatusBase.IsHandleAbleUpdateErrorResultDelegate(IsHandleAbleUpdateErrorResultRakuten);

                case EnumMallShopKbn.YAHOO:
                    // Yahoo! [Yahoo!]
                    return new UpdateMallOrderStatusBase.IsHandleAbleUpdateErrorResultDelegate(IsHandleAbleUpdateErrorResultYahoo);

                case EnumMallShopKbn.AMAZON:
                    // Amazon [amazon]
                    return new UpdateMallOrderStatusBase.IsHandleAbleUpdateErrorResultDelegate(IsHandleAbleUpdateErrorResultAmazon);
            }

            return null;
        }
        #endregion

        #region デリゲート用 [For delegate]
        /// <summary>
        /// ハンドルエラー判定（楽天） [Judgement handleable error (Rakuten)]
        /// </summary>
        /// <param name="errorResult">エラー結果 [Error result]</param>
        /// <returns>true : ハンドル可能 [Handleable] ／ false : ハンドル不可 [Not handleable]</returns>
        public bool IsHandleAbleUpdateErrorResultRakuten(string errorResult)
        {
            // 既にキャンセル [Already canceled]
            if (errorResult.Contains("Can't Move. (current status id : '-1')"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// ハンドルエラー判定（Yahoo!） [Judgement handleable error (Yahoo!)]
        /// </summary>
        /// <param name="errorResult">エラー結果 [Error result]</param>
        /// <returns>true : ハンドル可能 [Handleable] ／ false : ハンドル不可 [Not handleable]</returns>
        public bool IsHandleAbleUpdateErrorResultYahoo(string errorResult)
        {
            return false;
        }

        /// <summary>
        /// ハンドルエラー判定（Amazon） [Judgement handleable error (Amazon)]
        /// </summary>
        /// <param name="errorResult">エラー結果 [Error result]</param>
        /// <returns>true : ハンドル可能 [Handleable] ／ false : ハンドル不可 [Not handleable]</returns>
        public bool IsHandleAbleUpdateErrorResultAmazon(string errorResult)
        {
            // 許可されない [Can't be acknowledged]
            if (errorResult.Contains("cannot be acknowledged, because it is either still pending or has been canceled.)"))
            {
                return true;
            }

            return false;
        }
        #endregion
    }
}
