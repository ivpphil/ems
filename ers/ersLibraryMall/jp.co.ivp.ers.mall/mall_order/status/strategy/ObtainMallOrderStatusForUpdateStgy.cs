﻿
namespace jp.co.ivp.ers.mall.mall_order.status.strategy
{
    /// <summary>
    /// 更新用モール伝票ステータス取得 [Get mall order status for update]
    /// </summary>
    public class ObtainMallOrderStatusForUpdateStgy
    {
        /// <summary>
        /// 更新用モール伝票ステータス取得 [Get mall order status for update]
        /// </summary>
        /// <param name="shopKbn">店舗タイプ [Shop type]</param>
        /// <param name="oldStatus">旧受注ステータス [Old order status]</param>
        /// <param name="newStatus">新受注ステータス [New order status]</param>
        /// <returns>更新ステータス [Status for update]</returns>
        public EnumMallOrderStatus? Obtain(EnumMallShopKbn? shopKbn, EnumOrderStatusType? newStatus)
        {
            EnumMallOrderStatus? ret = null;

            switch (newStatus)
            {
                // 配送済み
                case EnumOrderStatusType.DELIVERED:
                    // → 配送済み
                    switch (shopKbn)
                    {
                        // 楽天
                        case EnumMallShopKbn.RAKUTEN:
                            // 処理済み [Done]
                            ret = EnumMallOrderStatus.Done;
                            break;

                        // Yahoo!
                        case EnumMallShopKbn.YAHOO:
                            // 完了 [Done]
                            ret = EnumMallOrderStatus.Done;
                            break;

                        // Amazon
                        case EnumMallShopKbn.AMAZON:
                            // 出荷済み [Shipped]
                            ret = EnumMallOrderStatus.Done;
                            break;
                    }
                    break;

                // キャンセル
                case EnumOrderStatusType.CANCELED:
                    // キャンセル [Canceled]
                    ret = EnumMallOrderStatus.Canceled;
                    break;
            }

            return ret;
        }
    }
}
