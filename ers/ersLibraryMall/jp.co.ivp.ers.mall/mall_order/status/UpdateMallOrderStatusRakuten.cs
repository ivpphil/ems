﻿using System;
using jp.co.ivp.ers.mall.api.order_status;

namespace jp.co.ivp.ers.mall.mall_order.status
{
    /// <summary>
    /// モール伝票ステータス更新クラス（楽天） [Class for update mall order status (Rakuten)]
    /// </summary>
    public class UpdateMallOrderStatusRakuten
        : UpdateMallOrderStatusBase
    {
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public UpdateMallOrderStatusRakuten(int? siteId, EnumMallShopKbn? shopKbn)
            : base(siteId, shopKbn)
        {
        }
        #endregion

        #region ステータス更新 [Update status]
        /// <summary>
        /// ステータス変更API取得 [Get the API for change status]
        /// </summary>
        /// <param name="dateOrder">注文日時 [Order datetime]</param>
        /// <returns>ステータス変更API [The API for change status]</returns>
        protected override ChangeOrderStatusAPIBase GetChangeOrderStatusAPI(DateTime dateOrder)
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIParamRakuten();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);
            apiParam.shop_name = mallSetup.GetShopName(this.siteId.Value);
            apiParam.successVerify = ErsMallFactory.ersMallUtilityFactory.getSetup().mall_order_update_status_verify;

            return ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIRakuten(apiParam);
        }
        #endregion
    }
}
