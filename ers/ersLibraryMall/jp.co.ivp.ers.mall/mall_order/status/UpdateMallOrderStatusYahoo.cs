﻿using System;
using jp.co.ivp.ers.mall.api.order_status;

namespace jp.co.ivp.ers.mall.mall_order.status
{
    /// <summary>
    /// モール伝票ステータス更新クラス（Yahoo!） [Class for update mall order status (Yahoo!)]
    /// </summary>
    public class UpdateMallOrderStatusYahoo
        : UpdateMallOrderStatusBase
    {
        #region コンストラクタ [Constructor]
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="siteId">サイトID [Site ID]</param>
        /// <param name="shopKbn">店舗タイプ [Type of shop]</param>
        public UpdateMallOrderStatusYahoo(int? siteId, EnumMallShopKbn? shopKbn)
            : base(siteId, shopKbn)
        {
        }
        #endregion

        #region ステータス更新 [Update status]
        /// <summary>
        /// ステータス変更API取得 [Get the API for change status]
        /// </summary>
        /// <param name="dateOrder">注文日時 [Order datetime]</param>
        /// <returns>ステータス変更API [The API for change status]</returns>
        protected override ChangeOrderStatusAPIBase GetChangeOrderStatusAPI(DateTime dateOrder)
        {
            var mallSetup = ErsMallFactory.ersSiteFactory.GetMallSetup();
            var apiParam = ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIParamYahoo();

            // APIパラメータ設定 [Set the parameters for API]
            apiParam.shop_id = mallSetup.GetShopId(this.siteId.Value);
            apiParam.shop_name = mallSetup.GetShopName(this.siteId.Value);

            // ストアクリエイターProへの対応 [Responding to Store creator pro]
            var dateChange = ErsMallFactory.ersMallUtilityFactory.getSetup().mallYahooStoreCreatorProDatetime;
            if (dateOrder >= dateChange)
            {
                apiParam.managementTool = EnumMallYahooManagementToolType.StoreCreatorPro;
            }
            else
            {
                apiParam.managementTool = EnumMallYahooManagementToolType.StoreManager;
            }

            return ErsMallFactory.ersMallAPIFactory.GetChangeOrderStatusAPIYahoo(apiParam);
        }
        #endregion
    }
}
