﻿using System;
using com.hunglead.harc;

namespace jp.co.ivp.ers.mall.mall_order.status
{
    /// <summary>
    /// モール伝票ステータス更新例外 [Exception for update mall order status]
    /// </summary>
    public class UpdateMallOrderHandleException 
        : Exception
    {
        /// <summary>
        /// コンストラクタ [Constructor]
        /// </summary>
        /// <param name="message">エラーメッセージ [Error message]</param>
        public UpdateMallOrderHandleException(string message)
            : base(message)
        {
        }
    }
}
