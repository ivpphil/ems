﻿using System;
using System.Collections;
using System.Collections.Generic;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mall.mall_order
{
    /// <summary>
    /// Amazonモール伝票ヘッダエンティティ [Amazon Entity for mall order table]
    /// </summary>
    public class ErsMallOrderAmazon
        : ErsMallOrder
    {
        /// <summary>
        /// プロパティへの値セット [Set properties]
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="d_no"></param>
        /// <param name="site_id"></param>
        /// <param name="mall_shop_kbn"></param>
        public override void SetProperties(Dictionary<string, object> dictionary, string d_no, int? site_id, EnumMallShopKbn? mall_shop_kbn)
        {
            //strategy取得
            var convertStgy = ErsMallFactory.ersMallOrderFactory.GetConvertArrayStgy();

            var other = (Dictionary<string, object>)dictionary["other"];

            if (other.ContainsKey("seller_order_id"))
            {
                this.a_seller_order_id = Convert.ToString(other["seller_order_id"]);
            }
            if (other.ContainsKey("last_update_date") && other["last_update_date"] != null && Convert.ToString(other["last_update_date"]) != "")
            {
                this.a_last_update_date = Convert.ToDateTime(other["last_update_date"]);
            }
            if (other.ContainsKey("fulfillment_channel"))
            {
                this.a_fulfillment_channel = Convert.ToString(other["fulfillment_channel"]);
            }
            if (other.ContainsKey("sales_channel"))
            {
                this.a_sales_channel = Convert.ToString(other["sales_channel"]);
            }
            if (other.ContainsKey("order_channel"))
            {
                this.a_order_channel = Convert.ToString(other["order_channel"]);
            }
            if (other.ContainsKey("ship_service_level"))
            {
                this.a_ship_service_level = Convert.ToString(other["ship_service_level"]);
            }
            if (other.ContainsKey("currency_code"))
            {
                this.a_currency_code = Convert.ToString(other["currency_code"]);
            }
            if (other.ContainsKey("number_of_items_shipped") && other["number_of_items_shipped"] != null && Convert.ToString(other["number_of_items_shipped"]) != "")
            {
                this.a_number_of_items_shipped = Convert.ToInt32(other["number_of_items_shipped"]);
            }
            if (other.ContainsKey("number_of_items_unshipped") && other["number_of_items_unshipped"] != null && Convert.ToString(other["number_of_items_unshipped"]) != "")
            {
                this.a_number_of_items_unshipped = Convert.ToInt32(other["number_of_items_unshipped"]);
            }
            if (other.ContainsKey("payment_execution_details"))
            {
                this.a_payment_execution_details = convertStgy.ConvertToStringFromArrayListMultilayer((ArrayList)other["payment_execution_details"], "{0} = {1}", "; ", ", ");
            }
            if (other.ContainsKey("order_type"))
            {
                this.a_order_type = Convert.ToString(other["order_type"]);
            }
            if (other.ContainsKey("earliest_ship_date") && other["earliest_ship_date"] != null && Convert.ToString(other["earliest_ship_date"]) != "")
            {
                this.a_earliest_ship_date = Convert.ToDateTime(other["earliest_ship_date"]);
            }
            if (other.ContainsKey("latest_ship_date") && other["latest_ship_date"] != null && Convert.ToString(other["latest_ship_date"]) != "")
            {
                this.a_latest_ship_date = Convert.ToDateTime(other["latest_ship_date"]);
            }

            base.SetProperties(dictionary, d_no, site_id, mall_shop_kbn);
        }
    }
}
