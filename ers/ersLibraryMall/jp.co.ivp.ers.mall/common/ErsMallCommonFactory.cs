﻿using jp.co.ivp.ers.mall.common.strategy;

namespace jp.co.ivp.ers.mall.common
{
    /// <summary>
    /// モール共通処理関連ファクトリ [Factory for common processes of mall]
    /// </summary>
    public class ErsMallCommonFactory
    {
        /// <summary>
        /// HARCログイン [Harc login]
        /// </summary>
        /// <returns>HarcLoginStgy</returns>
        public HarcLoginStgy GetHarcLoginStgy()
        {
            return new HarcLoginStgy();
        }

        /// <summary>
        /// JsonObjectをDictonaryListへ変換 [Convert to dictionary list from Json]
        /// </summary>
        /// <returns>ConvertJsonResultStgy</returns>
        public ConvertJsonResultStgy GetConvertJsonResultStgy()
        {
            return new ConvertJsonResultStgy();
        }

        /// <summary>
        /// 都道府県変換 [Convert prefecture]
        /// </summary>
        /// <returns>ConvertPrefectureStgy</returns>
        public ConvertPrefectureStgy GetConvertPrefectureStgy()
        {
            return new ConvertPrefectureStgy();
        }
    }
}
