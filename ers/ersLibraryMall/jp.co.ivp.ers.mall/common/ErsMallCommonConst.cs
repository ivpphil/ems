﻿using System;

namespace jp.co.ivp.ers.mall.common
{
    /// <summary>
    /// モール共通定数 [Mall common constant]
    /// </summary>
    public static class ErsMallCommonConst
    {
        #region モール商品画像ディレクトリ [Mall product image directory]
        /// <summary>
        /// モール商品画像枚数（ディレクトリ毎） [Mall product image count (By directory)]
        /// <para>【楽天用】</para>
        /// <para>最大10枚/SKU（バッファあり）</para>
        /// <para>500枚/ディレクトリ（最大500ディレクトリ）のプランで、25000 SKUまで対応可能</para>
        /// </summary>
        public const int MALL_PRODUCT_IMAGE_COUNT_BY_DIRECTORY = 50;

        /// <summary>
        /// モール商品画像ディレクトリ名フォーマット [Mall product image directory name format]
        /// </summary>
        public const string MALL_PRODUCT_IMAGE_DIRECTORY_NAME_FORMAT = "image{0:D5}";
        #endregion

        #region モール在庫補充 [Replenish mall stock]
        /// <summary>
        /// モール在庫補充閾値 [Threshold for replenish mall stock]
        /// </summary>
        public const int REPLENISH_MALL_STOCK_THRESHOLD = 10000000;

        /// <summary>
        /// モール在庫補充値 [Quantity for replenish mall stock]
        /// </summary>
        public const int REPLENISH_MALL_STOCK_QUANTITY = 99999999;
        #endregion
    }
}
