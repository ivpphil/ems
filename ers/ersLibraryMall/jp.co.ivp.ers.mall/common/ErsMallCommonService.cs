﻿
namespace jp.co.ivp.ers.mall.common
{
    /// <summary>
    /// モール共通サービス [Mall common service]
    /// </summary>
    public static class ErsMallCommonService
    {
        /// <summary>
        /// モール商品SKU接頭語 [Mall product sku prefix]
        /// </summary>
        public static string mallProductSkuPrefix
        {
            get
            {
                return ErsMallFactory.ersMallUtilityFactory.getSetup().mallProductSkuPrefix;
            }
        }

        /// <summary>
        /// モールSKUコード取得（ERSから） [Get mall sku code (from ERS)]
        /// </summary>
        /// <param name="scode">SKUコード [SKU code]</param>
        /// <returns>モールSKUコード [Mall sku code]</returns>
        public static string GetMallSkuFromErs(string scode)
        {
            if (!mallProductSkuPrefix.HasValue())
            {
                return scode;
            }

            return mallProductSkuPrefix + scode;
        }

        /// <summary>
        /// ERSSKUコード取得（モールから） [Get ERS sku code (from Mall)]
        /// </summary>
        /// <param name="scode">SKUコード [SKU code]</param>
        /// <returns>ERSSKUコード [ERS sku code]</returns>
        public static string GetErsSkuFromMall(string scode)
        {
            if (!mallProductSkuPrefix.HasValue())
            {
                return scode;
            }

            return scode.Replace(mallProductSkuPrefix, string.Empty);
        }
    }
}
