﻿using System;

namespace jp.co.ivp.ers.mall.common
{
    /// <summary>
    /// 共通構造体 [Common struct]
    /// </summary>
    public static class ErsCommonStruct
    {
        /// <summary>
        /// 開始・終了日時 [DateTime of Start and End]
        /// </summary>
        public struct DateTimeStartEnd
        {
            /// <summary>
            /// 開始日時 [Datetime of Start]
            /// </summary>
            public DateTime? dateFrom { get; set; }

            /// <summary>
            /// 終了日時 [DateTime of End]
            /// </summary>
            public DateTime? dateTo { get; set; }
        }
    }
}
