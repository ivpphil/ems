﻿using System;

namespace jp.co.ivp.ers.mall.common.strategy
{
    /// <summary>
    /// 都道府県変換 [Convert prefecture]
    /// </summary>
    public class ConvertPrefectureStgy
    {
        /// <summary>
        /// 都道府県取得 [Get the prefecture]
        /// </summary>
        /// <param name="address">住所 [Address]</param>
        /// <param name="removed">都道府県除去住所 [Remove prefecture addrss]</param>
        /// <returns>都道府県 [Prefecture]</returns>
        public virtual int? GetPrefecture(string address, out string removed)
        {
            removed = address;

            var service = ErsFactory.ersViewServiceFactory.GetErsViewPrefService();
            var listPref = service.SelectAsList();

            foreach (var data in listPref)
            {
                var pref_name = Convert.ToString(data["pref_name"]);

                if (address.Contains(pref_name))
                {
                    removed = address.Replace(pref_name, string.Empty).Trim();
                    return service.GetIdFromString(pref_name);
                }
            }

            return null;
        }
    }
}
