﻿using System.Collections;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Jayrock.Json;

namespace jp.co.ivp.ers.mall.common.strategy
{
    /// <summary>
    /// JsonObjectをDictonaryListへ変換 [Convert to dictionary list from Json]
    /// </summary>
    public class ConvertJsonResultStgy
    {
        /// <summary>
        /// シリアライザー [Serializer]
        /// </summary>
        private static JavaScriptSerializer serializer = new JavaScriptSerializer();


        #region 変換 [Convertion]
        /// <summary>
        /// 結果変換 [Convert from result]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <param name="key">取得キー [Key name for get]</param>
        /// <returns>結果リスト [Result list]</returns>
        protected List<Dictionary<string, object>> ConvertToDicListResult(JsonObject obj, string key)
        {
            var objJson = !string.IsNullOrEmpty(key) ? obj[key] : obj;

            var listRet = new List<Dictionary<string, object>>();

            if (objJson is CollectionBase)
            {
                var objCollection = (CollectionBase)objJson;

                foreach (var item in objCollection)
                {
                    // Json形式の文字列に変換
                    var jsonString = item.ToString();

                    // Json形式の文字列をディクショナリに変換
                    var dictionary = serializer.Deserialize<Dictionary<string, object>>(jsonString);
                    listRet.Add(dictionary);
                }
            }

            return listRet;
        }

        /// <summary>
        /// 結果変換 [Convert from result]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <param name="key">取得キー [Key name for get]</param>
        /// <returns>結果ディクショナリ [Result dictionary]</returns>
        protected List<object> ConvertToListResult(JsonObject obj, string key)
        {
            var objJson = !string.IsNullOrEmpty(key) ? obj[key] : obj;

            var listRet = new List<object>();

            if (objJson is CollectionBase)
            {
                // 文字列に変換
                var strJson = objJson.ToString();

                // 列挙型に変換
                var data = serializer.Deserialize<IEnumerable>(strJson);

                return new List<object>(((ArrayList)data).ToArray());
            }

            return listRet;
        }

        /// <summary>
        /// 結果変換 [Convert from result]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <param name="key">取得キー [Key name for get]</param>
        /// <returns>結果ディクショナリ [Result dictionary]</returns>
        protected Dictionary<string, object> ConvertToDicResult(JsonObject obj, string key)
        {
            var objJson = !string.IsNullOrEmpty(key) ? obj[key] : obj;

            if (objJson is DictionaryBase)
            {
                // 文字列に変換
                var strJson = objJson.ToString();

                // ディクショナリに変換
                return serializer.Deserialize<Dictionary<string, object>>(strJson);
            }

            return null;
        }
        #endregion

        #region 店舗情報 [Shop information]
        /// <summary>
        /// 店舗情報追加API結果変換 [Convert json to dictionary for result of add shop information]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>結果リスト [Result list]</returns>
        public virtual Dictionary<string, object> ConvertAddShopInfoResult(JsonObject obj)
        {
            return this.ConvertToDicResult(obj, null);
        }

        /// <summary>
        /// 店舗情報更新API結果変換 [Convert json to dictionary for result of update shop information]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>結果リスト [Result list]</returns>
        public virtual Dictionary<string, object> ConvertUpdateShopInfoResult(JsonObject obj)
        {
            return this.ConvertToDicResult(obj, null);
        }
        #endregion

        #region 受注データ取込 [Import order]
        /// <summary>
        /// 取込モール伝票JsonObjectをDictonaryListへ変換 [Convert to dictionary list from import result Json]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>結果リスト [Result list]</returns>
        public virtual List<Dictionary<string, object>> ConvertImportOrderResult(JsonObject obj)
        {
            return this.ConvertToDicListResult(obj, "orders");
        }
        #endregion

        #region 受注ステータス変更 [Change order status]
        /// <summary>
        /// モール伝票更新JsonObjectをDictonaryへ変換 [Convert to dictionary from update result Json]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>結果ディクショナリ [Result dictionary]</returns>
        public virtual Dictionary<string, object> ConvertToDicListFromUpdateResultJson(JsonObject obj)
        {
            return this.ConvertToDicResult(obj, "results");
        }
        #endregion

        #region クレジットカード決済 [Credit card payment]
        /// <summary>
        /// Yahoo!決済状況取得API結果変換 [Convert json to dictionary for result of get payment information (Yahoo!)]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>結果ディクショナリ [Result dictionary]</returns>
        public virtual Dictionary<string, List<Dictionary<string, object>>> ConvertGetYahoosOrderPaymentInfosByOrderCodeResultJson(JsonObject obj)
        {
            var dicRet = new Dictionary<string, List<Dictionary<string, object>>>();
            var dicTmp = this.ConvertToDicResult(obj, null);

            if (dicTmp != null && dicTmp.Count > 0)
            {
                foreach (var dic in dicTmp)
                {
                    var listTmp = new List<Dictionary<string, object>>();

                    var listData = (ArrayList)dic.Value;

                    foreach (var data in listData)
                    {
                        listTmp.Add((Dictionary<string, object>)data);
                    }

                    dicRet[dic.Key] = listTmp;
                }
            }

            return dicRet;
        }

        /// <summary>
        /// Yahoo!売上確定API結果変換 [Convert json to dictionary for result of execute payment (Yahoo!)]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>変換後リスト [Converted list]</returns>
        public virtual List<Dictionary<string, object>> ConvertExecuteYahoosOrderPaymentsResult(JsonObject obj)
        {
            return this.ConvertToDicListResult(obj, "results");
        }

        /// <summary>
        /// Yahoo!決済取消API結果変換 [Convert json to dictionary for result of cancel payment (Yahoo!)]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>変換後リスト [Converted list]</returns>
        public virtual List<Dictionary<string, object>> ConvertCancelYahoosOrderPaymentsResult(JsonObject obj)
        {
            return this.ConvertToDicListResult(obj, "results");
        }

        /// <summary>
        /// Yahoo!オーソリAPI結果変換 [Convert json to dictionary for result of authorize (Yahoo!)]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>変換後リスト [Converted list]</returns>
        public virtual List<Dictionary<string, object>> ConvertAuthorizeYahoosOrderPaymentsResult(JsonObject obj)
        {
            return this.ConvertToDicListResult(obj, "results");
        }

        /// <summary>
        /// Yahoo!再オーソリAPI結果変換 [Convert json to dictionary for result of re-authorize (Yahoo!)]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>変換後リスト [Converted list]</returns>
        public virtual List<Dictionary<string, object>> ConvertReAuthorizeYahoosOrderPaymentsResult(JsonObject obj)
        {
            return this.ConvertToDicListResult(obj, "results");
        }

        /// <summary>
        /// 楽天受注データ更新API結果変換 [Convert json to dictionary for result of update order (Rakuten)]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>変換後リスト [Converted list]</returns>
        public virtual List<Dictionary<string, object>> ConvertUpdateRakutenOrderInfosResult(JsonObject obj)
        {
            return this.ConvertToDicListResult(obj, "results");
        }
        #endregion

        #region 在庫数更新 [Update stock]
        /// <summary>
        /// 在庫更新API結果変換 [Convert json to dictionary for result of update stock]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>変換後リスト [Converted list]</returns>
        public virtual Dictionary<string, List<object>> ConvertUpdateStockResult(JsonObject obj)
        {
            var dicRet = new Dictionary<string, List<object>>();
            List<object> listTmp = null;

            listTmp = this.ConvertToListResult(obj, "productIds");
            if (listTmp.Count > 0)
            {
                dicRet["productIds"] = listTmp;
            }

            listTmp = this.ConvertToListResult(obj, "productSkuIds");
            if (listTmp.Count > 0)
            {
                dicRet["productSkuIds"] = listTmp;
            }

            listTmp = this.ConvertToListResult(obj, "errors");
            if (listTmp.Count > 0)
            {
                dicRet["errors"] = listTmp;
            }

            return dicRet;
        }
        #endregion

        #region 在庫更新履歴取得 [Get the history of update stock]
        /// <summary>
        /// 在庫更新履歴取得 [Get the history of update stock]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>変換後リスト [Converted list]</returns>
        public virtual List<Dictionary<string, object>> ConvertFindStockPostingSlipResult(JsonObject obj)
        {
            return this.ConvertToDicListResult(obj, "results");
        }

        /// <summary>
        /// 在庫更新履歴詳細取得 [Get the detail of history of update stock]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>変換後リスト [Converted list]</returns>
        public virtual Dictionary<string, object> ConvertGetStockPostingSlipResult(JsonObject obj)
        {
            return this.ConvertToDicResult(obj, null);
        }
        #endregion

        #region 商品CSV登録 [Import product csv]
        /// <summary>
        /// 商品CSV登録API結果変換 [Convert json to dictionary for result of import product csv]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>変換後リスト [Converted list]</returns>
        public virtual Dictionary<string, Dictionary<string, object>> ConvertImportRawProductCsvResult(JsonObject obj)
        {
            var dicRet = new Dictionary<string, Dictionary<string, object>>();
            Dictionary<string, object> dicTmp = null;

            dicTmp = this.ConvertToDicResult(obj, "products");
            if (dicTmp != null && dicTmp.Count > 0)
            {
                dicRet["products"] = dicTmp;
            }

            dicTmp = this.ConvertToDicResult(obj, "productSkus");
            if (dicTmp != null && dicTmp.Count > 0)
            {
                dicRet["productSkus"] = dicTmp;
            }

            dicTmp = this.ConvertToDicResult(obj, "shopProducts");
            if (dicTmp != null && dicTmp.Count > 0)
            {
                dicRet["shopProducts"] = dicTmp;
            }

            dicTmp = this.ConvertToDicResult(obj, "shopProductSkus");
            if (dicTmp != null && dicTmp.Count > 0)
            {
                dicRet["shopProductSkus"] = dicTmp;
            }

            return dicRet;
        }
        #endregion

        #region 店舗別商品情報取得 [Get product information of mall shops]
        /// <summary>
        /// 店舗別商品情報取得API結果変換 [Convert json to dictionary for result of get product information of mall shops]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>変換後リスト [Converted list]</returns>
        public virtual List<Dictionary<string, object>> ConvertGetShopProductInfoResult(JsonObject obj)
        {
            return this.ConvertToDicListResult(obj, "products");
        }
        #endregion

        #region 店舗別商品情報更新 [Update product information of mall shops]
        /// <summary>
        /// 店舗別商品情報更新API結果変換 [Convert json to dictionary for result of update product information of mall shops]
        /// </summary>
        /// <param name="obj">Jsonオブジェクト [Json object]</param>
        /// <returns>変換後リスト [Converted list]</returns>
        public virtual Dictionary<string, List<object>> ConvertUpdateShopProductInfoResult(JsonObject obj)
        {
            var dicRet = new Dictionary<string, List<object>>();
            List<object> listTmp = null;

            listTmp = this.ConvertToListResult(obj, "productIds");
            if (listTmp.Count > 0)
            {
                dicRet["productIds"] = listTmp;
            }

            return dicRet;
        }
        #endregion
    }
}
