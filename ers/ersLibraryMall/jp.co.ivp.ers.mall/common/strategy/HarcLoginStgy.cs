﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using com.hunglead.harc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mall.common.strategy
{
    /// <summary>
    /// HARCログイン [Harc login]
    /// </summary>
    public class HarcLoginStgy
    {
        /// <summary>
        /// HARCリクエストオブジェクト [HARC request object]
        /// </summary>
        protected static HarcApiRequest _HarcApiRequest
        {
            get
            {
                return (HarcApiRequest)ErsCommonContext.GetPooledObject("_HarcApiRequest");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_HarcApiRequest", value);
            }
        }

        /// <summary>
        /// APIエラーリトライカウント [API retry count for error]
        /// </summary>
        public static int API_ERROR_RETRY_COUNT = 5;

        /// <summary>
        /// エラーメッセージリスト [List of error message]
        /// </summary>
        protected IList<string> listApiErrorMessage = new List<string>()
        {
            "request error"     // サーバ接続不可 [Doesn't connect to the server]
        };


        /// <summary>
        /// HARCログイン [Harc login]
        /// </summary>
        public virtual HarcApiRequest HarcLogin()
        {
            if (_HarcApiRequest != null)
            {
                return _HarcApiRequest;
            }

            var setup = ErsMallFactory.ersMallUtilityFactory.getSetup();

            var filePath = Path.Combine(setup.harcSerializeFilePath, string.Format("timeout_{0}_{1}", setup.harcTimeOutSeconds, setup.harcSerializeFileName));

            // ファイル存在確認 [Check exists the file]
            if (File.Exists(filePath))
            {
                // デシリアライズ [Deserialize]
                using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    var request = (HarcApiRequest)(new BinaryFormatter()).Deserialize(stream);

                    // ログイン [Login]
                    this.Login(request);

                    _HarcApiRequest = request;
                    return request;
                }
            }
            else
            {
                // リクエストオブジェクト生成 [Create the object for request]
                var request = new HarcApiRequest(setup.harcLoginUser, setup.harcLoginPassword, setup.harcAuthUrl, setup.harcApiUrl, setup.harcTimeOutSeconds);

                // ログイン [Login]
                this.Login(request);

                // シリアライズ [Serialize]
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    (new BinaryFormatter()).Serialize(stream, request);
                }

                _HarcApiRequest = request;
                return request;
            }
        }

        /// <summary>
        /// ログイン [Login]
        /// </summary>
        /// <param name="request">HARCリクエストオブジェクト [Object for HARC request]</param>
        protected virtual void Login(HarcApiRequest request)
        {
            int retry = 0;

            while (true)
            {
                try
                {
                    // ログイン [Login]
                    request.login();
                    break;
                }
                catch (HarcApiException e)
                {
                    // リトライ可能な場合は指定回数リトライ [When it is retryable, retry specified times]
                    if (this.IsRetryAbleError(e.Message))
                    {
                        if (retry++ < API_ERROR_RETRY_COUNT)
                        {
                            continue;
                        }
                    }

                    throw e;
                }
            }
        }

        /// <summary>
        /// リトライ可能なエラーかどうか [Whether error can retry]
        /// </summary>
        /// <param name="errorMessage">エラーメッセージ [Error message]</param>
        /// <returns>true : リトライ可能 [Can retry] / false : リトライ不可 [Can't retry]</returns>
        protected virtual bool IsRetryAbleError(string errorMessage)
        {
            foreach (var message in this.listApiErrorMessage)
            {
                if (errorMessage.Contains(message))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
