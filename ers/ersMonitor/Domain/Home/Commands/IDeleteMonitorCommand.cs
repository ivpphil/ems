﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersMonitor.Domain.Home.Commands
{
    public interface IDeleteMonitorCommand
        : ICommand
    {
        EnumMonitorPurchase? monitor_purchase { get; set; }

        EnumPmFlg? pm_flg { get; set; }
    }
}