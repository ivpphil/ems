﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersMonitor.Domain.Home.Mappables;
using ersMonitor.Models.Home.settings;
using jp.co.ivp.ers.mvc.model;
using jp.co.ivp.ers;
using System.Threading.Tasks;
using jp.co.ivp.ers.db;
using System.Text.RegularExpressions;
using ersMonitor.jp.co.ivp.ers.long_life_sql;
using System.Collections.Concurrent;

namespace ersMonitor.Domain.Home.Mappers
{
    public class LongLifeSqlMapper
        : IMapper<ILongLifeSqlMappable>
    {
        public void Map(ILongLifeSqlMappable objMappable)
        {
            this.Execute(objMappable);
        }

        /// <summary>
        /// Execute
        /// </summary>
        public void Execute(ILongLifeSqlMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var modelSettings = new long_life_sqlSettingsModel();

            // Get settings
            ErsXmlModelBinder.Bind(string.Format("{0}\\ersMonitor\\settings\\long_life_sql.config", setup.root_path), modelSettings);

            var listErrorLog = new ConcurrentQueue<string>();

            // Execute vacuum
            Parallel.ForEach(modelSettings.connection_settings, setting =>
            {
                try
                {
                    this.ExecuteObserve(modelSettings, setting);
                }
                catch (ErsDBLongLifeSQLObserveException e)
                {
                    listErrorLog.Enqueue(e.Message);
                }
                catch (Exception e)
                {
                    listErrorLog.Enqueue(e.ToString());
                }
            });

            if (listErrorLog.Count > 0)
            {
                objMappable.error_message = String.Join(this.ReplaceNewLine(modelSettings.separator), listErrorLog);
            }
        }

        /// <summary>
        /// ExecuteObserve
        /// </summary>
        /// <param name="modelSettings"></param>
        /// <param name="connectionSetting"></param>
        protected void ExecuteObserve(long_life_sqlSettingsModel modelSettings, ConnectionSettings connectionSetting)
        {
            List<string> listMessage = new List<string>();

            try
            {
                ErsDB_universal objDB = ErsDB_universal.GetInstance(ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase(connectionSetting.connection_string));

                var observer = ExecuteObserveForPostgresBase.GetPostgresVersion(objDB);

                observer.ExecuteForPg(objDB, modelSettings, listMessage, connectionSetting);
            }
            catch (Exception e)
            {
                listMessage.Add(string.Format("[{0}]\r\n{1}\r\n", connectionSetting.connection_name, e.ToString()));
            }

            if (listMessage.Count() > 0)
            {
                throw new ErsDBLongLifeSQLObserveException(String.Join("\r\n", listMessage));
            }
        }

        public class ErsDBLongLifeSQLObserveException
            : Exception
        {
            public ErsDBLongLifeSQLObserveException(string errorMessage)
                : base(errorMessage)
            {
                this.errorMessage = errorMessage;
            }

            public string errorMessage { get; set; }
        }

        /// <summary>
        /// 改行コード置換
        /// </summary>
        /// <param name="strSrc">置換対象</param>
        /// <returns>置換後文字列</returns>
        protected virtual string ReplaceNewLine(string strSrc)
        {
            return strSrc.Replace("\\r\\n", Environment.NewLine);
        }
    }
}