﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersMonitor.Domain.Home.Mappables;
using System.Threading.Tasks;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using ersMonitor.Models.Home.settings;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.model;
using System.Collections.Concurrent;

namespace ersMonitor.Domain.Home.Mappers
{
    public class CheckwwwMapper
        : IMapper<ICheckwwwMappable>
    {
        public void Map(ICheckwwwMappable objMappable)
        {
            this.LoadSettings(objMappable);

            this.Monitor(objMappable);
        }

        private void LoadSettings(ICheckwwwMappable objMappable)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            objMappable.modelSettings = new checkwwwSettingsModel();

            // Get settings
            ErsXmlModelBinder.Bind(string.Format("{0}\\ersMonitor\\settings\\checkwww.config", setup.root_path), objMappable.modelSettings);
        }

        /// <summary>
        /// 監視
        /// </summary>
        public void Monitor(ICheckwwwMappable objMappable)
        {
            var listErrorMsg = new ConcurrentQueue<string>();

            Parallel.ForEach(objMappable.modelSettings.urls, url =>
            {
                // ページ監視
                try
                {
                    this.MonitorPage(url);
                }
                catch (Exception e)
                {
                    listErrorMsg.Enqueue(string.Format(this.ReplaceNewLine(objMappable.modelSettings.message_format_url), url, e.ToString()));
                }
            });

            // DB監視
            try
            {
                this.MonitorDB();
            }
            catch (Exception e)
            {
                listErrorMsg.Enqueue(string.Format(this.ReplaceNewLine(objMappable.modelSettings.message_format_db), e.ToString()) + Environment.NewLine);
            }

            if (listErrorMsg.Count > 0)
            {
                objMappable.error_message = String.Join(this.ReplaceNewLine(objMappable.modelSettings.separator), listErrorMsg);
            }
        }

        /// <summary>
        /// ページ監視
        /// </summary>
        /// <param name="url">URL</param>
        protected void MonitorPage(string url)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            if (setup.debug)
            {
                //debugがonのときはSSLエラーは無視する
                ErsHttpClient.SetIgnoreInvalidSSL();
            }

            using (var result = ErsHttpClient.HttpGetStream(url, setup.BasicAuthUserName, setup.BasicAuthPassword))
            {
            }
        }

        /// <summary>
        /// DB監視
        /// </summary>
        protected void MonitorDB()
        {
            var check_t = new ErsDB_parent("check_t");

            Criteria criteria = new Criteria();
            criteria.Add(Criteria.GetCriterion("id", 0, Criteria.Operation.EQUAL));

            // DELETE
            check_t.gDelete(criteria);

            // INSERT
            var dicInsert = new Dictionary<string, object>();

            dicInsert.Add("id", 0);
            dicInsert.Add("serial", 0);
            dicInsert.Add("dt", DateTime.Now);

            check_t.gInsert(dicInsert);

            // UPDATE
            check_t.gUpdateColumn(new[] { "serial" }, new Dictionary<string, object>() { { "serial", -1 } }, criteria);
        }

        /// <summary>
        /// 改行コード置換
        /// </summary>
        /// <param name="strSrc">置換対象</param>
        /// <returns>置換後文字列</returns>
        protected virtual string ReplaceNewLine(string strSrc)
        {
            return strSrc.Replace("\\r\\n", Environment.NewLine);
        }
    }
}