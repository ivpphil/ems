﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersMonitor.Domain.Home.Mappables;
using jp.co.ivp.ers;
using ersMonitor.Models.Home.settings;
using jp.co.ivp.ers.mvc.model;
using System.Threading.Tasks;
using jp.co.ivp.ers.util;
using System.IO;
using NCrontab;
using System.Collections.Concurrent;

namespace ersMonitor.Domain.Home.Mappers
{
    public class CheckNormalLogMapper
        : IMapper<ICheckNormalLogMappable>
    {
        public void Map(ICheckNormalLogMappable objMappable)
        {
            this.LoadSettings(objMappable);

            this.Monitor(objMappable);
        }

        private void LoadSettings(ICheckNormalLogMappable objMappable)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            objMappable.modelSettings = new check_normal_logSettingsModel();

            // Get settings
            ErsXmlModelBinder.Bind(string.Format("{0}\\ersMonitor\\settings\\check_normal_log.config", setup.root_path), objMappable.modelSettings);
        }

        private void Monitor(ICheckNormalLogMappable objMappable)
        {
            var listLowResult = new ConcurrentQueue<string>();
            var listResult = new ConcurrentQueue<Dictionary<string, object>>();

            Parallel.ForEach(objMappable.modelSettings.logs, log =>
            {
                // ログ監視
                var result = this.MonitorLog(objMappable, log);

                if (result.ContainsKey("error_message"))
                {
                    listResult.Enqueue(result);
                    listLowResult.Enqueue(string.Format("{3}{0}\r\n[{1}]\r\n{2}", log.log_type_name, log.log_path, result["error_message"].ToString(), ErsResources.GetMessage("urgency_low")));
                }

                if (result.ContainsKey("low_error_message"))
                {
                    listLowResult.Enqueue(string.Format("{3}{0}\r\n[{1}]\r\n{2}", log.log_type_name, log.log_path, result["low_error_message"].ToString(), ErsResources.GetMessage("urgency_low")));
                }
            });

            objMappable.listLowResult = listLowResult.ToList();
            objMappable.listResult = listResult.ToList();

            if (objMappable.listResult.Count > 0)
            {
                objMappable.message = this.ReplaceNewLine(objMappable.modelSettings.message);
            }

            // メール通知
            if (objMappable.listLowResult.Count > 0)
            {
                this.MailSend(objMappable.modelSettings.mail_to, objMappable.modelSettings.mail_from, objMappable.modelSettings.mail_cc, null, objMappable.modelSettings.mail_title, String.Join(this.ReplaceNewLine(objMappable.modelSettings.separator), objMappable.listLowResult));
            }
        }

        /// <summary>
        /// ログ監視
        /// </summary>
        /// <param name="log">ログ</param>
        /// <returns>エラー内容</returns>
        protected Dictionary<string, object> MonitorLog(ICheckNormalLogMappable objMappable, NormalLogSettings log)
        {
            List<string> listResult = new List<string>();
            List<string> listLowResult = new List<string>();

            // ログオンユーザ変更の必要があるかどうか
            bool doLogon = !string.IsNullOrEmpty(objMappable.modelSettings.target_server_ip) && !string.IsNullOrEmpty(objMappable.modelSettings.target_server_user) && !string.IsNullOrEmpty(objMappable.modelSettings.target_server_pass);

            using (var logon = ChangeLogonUserHelper.BeginChange(objMappable.modelSettings.target_server_ip, objMappable.modelSettings.target_server_user, objMappable.modelSettings.target_server_pass, doLogon))
            {
                // 監視対象のディレクトリが存在しない
                if (!Directory.Exists(log.log_path))
                {
                    this.AddTmpResult("Monitor directory was not found.\r\n", log, listResult, listLowResult);
                }
                else
                {
                    var files = Directory.GetFiles(log.log_path);

                    // 監視対象のファイルが存在しない
                    if (files == null || (files != null && files.Length == 0))
                    {
                        this.AddTmpResult("Monitor files was not found.\r\n", log, listResult, listLowResult);
                    }
                    else
                    {
                        foreach (var path in files)
                        {
                            string result = this.GetErrorMessage(path, log);

                            this.AddTmpResult(result, log, listResult, listLowResult);
                        }
                    }
                }
            }

            Dictionary<string, object> dicResult = new Dictionary<string, object>();

            if (listResult.Count > 0)
            {
                dicResult["type_name"] = ErsResources.GetMessage("urgency_high") + log.log_type_name;
                dicResult["log_path"] = log.log_path;
                dicResult["error_message"] = String.Join(this.ReplaceNewLine(objMappable.modelSettings.separator), listResult);
            }

            if (listLowResult.Count > 0)
            {
                dicResult["low_error_message"] = String.Join(this.ReplaceNewLine(objMappable.modelSettings.separator), listLowResult);
            }

            return dicResult;
        }

        /// <summary>
        /// エラーメッセージ取得（ログのタイムスタンプが正常かどうか）
        /// </summary>
        /// <param name="path">ファイルパス</param>
        /// <param name="log">ログ</param>
        /// <returns>エラーメッセージ</returns>
        protected string GetErrorMessage(string path, NormalLogSettings log)
        {
            if (!log.monitor_elapsed.HasValue())
            {
                return string.Empty;
            }

            DateTime dateNow = DateTime.Now;

            DateTime lastDate = File.GetLastWriteTime(path);

            var contab = CrontabSchedule.Parse(log.monitor_elapsed);
            var nextDate = contab.GetNextOccurrence(lastDate);
            nextDate = nextDate.AddSeconds(-nextDate.Second);

            var delay_minutes = log.delay_minutes ?? 0;

            if (nextDate.AddMinutes(delay_minutes) < dateNow)
            {
                var dicValue = log.GetPropertiesAsDictionary();
                dicValue["next_date"] = nextDate.ToString("yyyy/MM/dd HH:mm");
                dicValue["last_date"] = lastDate.ToString("yyyy/MM/dd HH:mm");

                TimeSpan span = dateNow - nextDate;
                dicValue["elapsed_time"] = Convert.ToInt32(Math.Floor(span.TotalMinutes));

                return StringExtension.Format(this.ReplaceNewLine(log.message_format), dicValue);
            }

            return string.Empty;
        }

        /// <summary>
        /// 結果（一時）追加
        /// </summary>
        /// <param name="result">エラー内容</param>
        /// <param name="log">ログ</param>
        /// <param name="listResult">結果リスト</param>
        /// <param name="listLowResult">結果（緊急度低）リスト</param>
        protected void AddTmpResult(string result, NormalLogSettings log, List<string> listResult, List<string> listLowResult)
        {
            if (string.IsNullOrEmpty(result))
            {
                return;
            }

            // 緊急度別
            switch (log.urgency)
            {
                case EnumUrgency.Low:
                    listLowResult.Add(result);
                    break;

                case EnumUrgency.High:
                    listResult.Add(result);
                    break;
            }
        }

        /// <summary>
        /// メール送信
        /// </summary>
        /// <param name="mailTo">送信先</param>
        /// <param name="mailFrom">送信元</param>
        /// <param name="mailCc">CC（カンマ区切りで複数）</param>
        /// <param name="mailBcc">BCC（カンマ区切りで複数）</param>
        /// <param name="mailTitle">メールタイトル</param>
        /// <param name="mailBody">メール本文</param>
        protected virtual void MailSend(string mailTo, string mailFrom, IEnumerable<string> mailCc, string mailBcc, string mailTitle, string mailBody)
        {
            if (string.IsNullOrEmpty(mailTo) || string.IsNullOrEmpty(mailFrom))
            {
                return;
            }

            string strCc = null;
            if (mailCc != null)
            {
                strCc = string.Join(",", mailCc);
            }

            var mail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();
            mail.MailSend(
                mailTitle, mailBody,
                System.IO.Path.GetFileName(System.Reflection.Assembly.GetCallingAssembly().Location),
                mailTo, mailFrom,
                strCc, mailBcc);
        }

        /// <summary>
        /// 改行コード置換
        /// </summary>
        /// <param name="strSrc">置換対象</param>
        /// <returns>置換後文字列</returns>
        protected virtual string ReplaceNewLine(string strSrc)
        {
            return strSrc.Replace("\\r\\n", Environment.NewLine);
        }
    }
}