﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersMonitor.Domain.Home.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.model;
using ersMonitor.Models.Home.settings;
using System.Threading.Tasks;
using System.IO;
using jp.co.ivp.ers.util;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections.Concurrent;

namespace ersMonitor.Domain.Home.Mappers
{
    public class CheckSystemErrorMapper
        : IMapper<ICheckSystemErrorMappable>
    {
        public void Map(ICheckSystemErrorMappable objMappable)
        {
            this.LoadSettings(objMappable);

            this.Monitor(objMappable);
        }

        private void LoadSettings(ICheckSystemErrorMappable objMappable)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            objMappable.modelSettings = new check_system_errorSettingsModel();

            // Get settings
            ErsXmlModelBinder.Bind(string.Format("{0}\\ersMonitor\\settings\\check_system_error.config", setup.root_path), objMappable.modelSettings);
        }

        private void Monitor(ICheckSystemErrorMappable objMappable)
        {
            var listIgnoreResult = new ConcurrentQueue<string>();
            var listResult = new ConcurrentQueue<Dictionary<string, object>>();

            Parallel.ForEach(objMappable.modelSettings.logs, log =>
            {
                // ログ監視
                var result = this.MonitorLog(objMappable, log);

                if (result.ContainsKey("error_message"))
                {
                    listResult.Enqueue(result);
                    listIgnoreResult.Enqueue(string.Format("{0}\r\n[{1}]\r\n{2}", log.log_type_name, log.log_path, result["error_message"].ToString()));
                }

                if (result.ContainsKey("ignore_error_message"))
                {
                    listIgnoreResult.Enqueue(string.Format("{0}\r\n[{1}]\r\n{2}", log.log_type_name, log.log_path, result["ignore_error_message"].ToString()));
                }
            });

            objMappable.listResult = listResult.ToList();

            // メール通知
            if (objMappable.listResult.Count > 0)
            {
                this.MailSend(objMappable.modelSettings.mail_to, objMappable.modelSettings.mail_from, objMappable.modelSettings.mail_cc, null, objMappable.modelSettings.mail_title, String.Join(this.ReplaceNewLine(objMappable.modelSettings.separator), objMappable.listResult.Select(d => d["error_message"]).ToList()));
            }
        }

        /// <summary>
        /// ログ監視
        /// </summary>
        /// <param name="log">ログ</param>
        /// <returns>エラー内容</returns>
        protected Dictionary<string, object> MonitorLog(ICheckSystemErrorMappable objMappable, SystemErrorLogSettings log)
        {
            List<string> listResult = new List<string>();

            // ログオンユーザ変更の必要があるかどうか
            bool doLogon = !string.IsNullOrEmpty(log.target_server_ip) && !string.IsNullOrEmpty(log.target_server_user) && !string.IsNullOrEmpty(log.target_server_pass);

            using (var logon = ChangeLogonUserHelper.BeginChange(log.target_server_ip, log.target_server_user, log.target_server_pass, doLogon))
            {
                // 監視対象のディレクトリが存在しない
                if (!Directory.Exists(log.log_path))
                {
                    listResult.Add("Monitor directory was not found.\r\n");
                }
                else
                {
                    string logBkDir = log.log_path + "\\bk\\";

                    // バックアップディレクトリ作成
                    ErsDirectory.CreateDirectories(logBkDir);

                    foreach (var path in Directory.GetFiles(log.log_path))
                    {
                        if (this.IsExistsMonitorErrorLog(path))
                        {
                            var dicResultError = this.GetErrorMessage(objMappable, path, log);
                            var strErrorMessage = dicResultError["error_message"].ToString();

                            if (!string.IsNullOrEmpty(strErrorMessage))
                            {
                                listResult.Add(strErrorMessage);

                                // ログ退避
                                File.Move(path, Path.Combine(logBkDir, string.Format("{0}_{1}", DateTime.Now.ToString("yyyyMMddHHmmssfffffff"), Path.GetFileName(path))));
                            }
                        }
                    }
                }
            }

            Dictionary<string, object> dicResult = new Dictionary<string, object>();

            if (listResult.Count > 0)
            {
                dicResult["type_name"] = log.log_type_name;
                dicResult["log_path"] = log.log_path;
                dicResult["error_message"] = String.Join(this.ReplaceNewLine(objMappable.modelSettings.separator), listResult);
            }

            return dicResult;
        }

        /// <summary>
        /// 監視すべきエラーログが存在しているかどうか
        /// </summary>
        /// <param name="path">ファイルパス</param>
        /// <returns>true : 存在している ／ false : 存在していない</returns>
        protected bool IsExistsMonitorErrorLog(string path)
        {
            // "exception" 始まりかどうか
            if (!Path.GetFileName(path).StartsWith("exception"))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// エラーメッセージ取得
        /// </summary>
        /// <param name="path">ファイルパス</param>
        /// <param name="log">ログ</param>
        /// <returns>エラーメッセージディクショナリ</returns>
        protected Dictionary<string, object> GetErrorMessage(ICheckSystemErrorMappable objMappable, string path, SystemErrorLogSettings log)
        {
            Dictionary<string, object> dicResult = new Dictionary<string, object>();

            dicResult["error_message"] = string.Empty;

            string strContents = this.GetFileContents(path);

            if (string.IsNullOrEmpty(strContents))
            {
                return dicResult;
            }

            List<string> listResult = new List<string>();

            // エラー発生日時行から次のエラー発生日時行の前までのパターン
            string ptnEachLog = @"((\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}) ERROR.*?)(?=\d{4}-\d{2}|\z)";
            MatchCollection mc = new Regex(ptnEachLog, RegexOptions.Singleline).Matches(strContents);

            foreach (Match match in mc)
            {
                // エラー発生日時
                DateTime dateLog = DateTime.ParseExact(match.Groups[2].Value, "yyyy-MM-dd HH:mm:ss,fff", null);

                string strErrorLog = match.Groups[1].Value;

                listResult.Add(strErrorLog);
            }

            if (listResult.Count > 0)
            {
                dicResult["error_message"] = String.Join(this.ReplaceNewLine(objMappable.modelSettings.separator), listResult);
            }

            return dicResult;
        }

        /// <summary>
        /// ファイル内容取得
        /// </summary>
        /// <param name="path">ファイルパス</param>
        /// <returns>ファイル内容</returns>
        protected string GetFileContents(string path)
        {
            string result = string.Empty;

            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var sr = new StreamReader(fs, ErsEncoding.ShiftJIS))
            {
                result = sr.ReadToEnd();
            }

            return result;
        }

        /// <summary>
        /// メール送信
        /// </summary>
        /// <param name="mailTo">送信先</param>
        /// <param name="mailFrom">送信元</param>
        /// <param name="mailCc">CC（カンマ区切りで複数）</param>
        /// <param name="mailBcc">BCC（カンマ区切りで複数）</param>
        /// <param name="mailTitle">メールタイトル</param>
        /// <param name="mailBody">メール本文</param>
        protected virtual void MailSend(string mailTo, string mailFrom, IEnumerable<string> mailCc, string mailBcc, string mailTitle, string mailBody)
        {
            if (string.IsNullOrEmpty(mailTo) || string.IsNullOrEmpty(mailFrom))
            {
                return;
            }

            string strCc = null;
            if (mailCc != null)
            {
                strCc = string.Join(",", mailCc);
            }

            var mail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();
            mail.MailSend(
                mailTitle, mailBody,
                System.IO.Path.GetFileName(System.Reflection.Assembly.GetCallingAssembly().Location),
                mailTo, mailFrom,
                strCc, mailBcc);
        }

        /// <summary>
        /// 改行コード置換
        /// </summary>
        /// <param name="strSrc">置換対象</param>
        /// <returns>置換後文字列</returns>
        protected virtual string ReplaceNewLine(string strSrc)
        {
            return strSrc.Replace("\\r\\n", Environment.NewLine);
        }
    }
}