﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersMonitor.Domain.Home.Mappables;
using jp.co.ivp.ers;
using ersMonitor.Models.Home.settings;
using jp.co.ivp.ers.mvc.model;
using System.Threading.Tasks;
using jp.co.ivp.ers.util;
using System.IO;
using System.Text;
using System.Collections.Concurrent;

namespace ersMonitor.Domain.Home.Mappers
{
    public class CheckErrorLogMapper
        : IMapper<ICheckErrorLogMappable>
    {
        public void Map(ICheckErrorLogMappable objMappable)
        {
            this.LoadSettings(objMappable);

            this.Monitor(objMappable);
        }

        private void LoadSettings(ICheckErrorLogMappable objMappable)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            objMappable.modelSettings = new check_error_logSettingsModel();

            // Get settings
            ErsXmlModelBinder.Bind(string.Format("{0}\\ersMonitor\\settings\\check_error_log.config", setup.root_path), objMappable.modelSettings);
        }

        private void Monitor(ICheckErrorLogMappable objMappable)
        {
            var listLowResult = new ConcurrentQueue<string>();
            var listResult = new ConcurrentQueue<Dictionary<string, object>>();
            List<string> listErrorMsg = new List<string>();

            Parallel.ForEach(objMappable.modelSettings.logs, log =>
            {
                // ログ監視
                var result = this.MonitorLog(objMappable, log);

                if (result.ContainsKey("error_message"))
                {
                    listResult.Enqueue(result);
                    listLowResult.Enqueue(string.Format("{0}\r\n[{1}]\r\n{2}", log.log_type_name, log.log_path, result["error_message"].ToString(), ErsResources.GetMessage("urgency_low")));
                }

                if (result.ContainsKey("low_error_message"))
                {
                    listLowResult.Enqueue(string.Format("{0}\r\n[{1}]\r\n{2}", log.log_type_name, log.log_path, result["low_error_message"].ToString(), ErsResources.GetMessage("urgency_low")));
                }
            });

            objMappable.listLowResult = listLowResult.ToList();
            objMappable.listResult = listResult.ToList();

            if (objMappable.listResult.Count > 0)
            {
                objMappable.message = this.ReplaceNewLine(objMappable.modelSettings.message);
            }

            // メール通知
            if (objMappable.listLowResult.Count > 0)
            {
                this.MailSend(objMappable.modelSettings.mail_to, objMappable.modelSettings.mail_from, objMappable.modelSettings.mail_cc, null, objMappable.modelSettings.mail_title, String.Join(this.ReplaceNewLine(objMappable.modelSettings.separator), objMappable.listLowResult));
            }
        }

        /// <summary>
        /// ログ監視
        /// </summary>
        /// <param name="log">ログ</param>
        /// <returns>エラー内容</returns>
        protected Dictionary<string, object> MonitorLog(ICheckErrorLogMappable objMappable, ErrorLogSettings log)
        {
            List<string> listResult = new List<string>();
            List<string> listLowResult = new List<string>();

            // ログオンユーザ変更の必要があるかどうか
            bool doLogon = !string.IsNullOrEmpty(objMappable.modelSettings.target_server_ip) && !string.IsNullOrEmpty(objMappable.modelSettings.target_server_user) && !string.IsNullOrEmpty(objMappable.modelSettings.target_server_pass);

            using (var logon = ChangeLogonUserHelper.BeginChange(objMappable.modelSettings.target_server_ip, objMappable.modelSettings.target_server_user, objMappable.modelSettings.target_server_pass, doLogon))
            {
                // 監視対象のディレクトリが存在する
                if (Directory.Exists(log.log_path))
                {
                    foreach (var path in Directory.GetFiles(log.log_path))
                    {
                        if (log.delay_minutes.HasValue)
                        {
                            //検知までの猶予時間設定（猶予時間経過後にエラー発報）
                            if (File.GetCreationTime(path).AddMinutes(log.delay_minutes.Value) > DateTime.Now)
                            {
                                continue;
                            }
                        }

                        string result = this.GetErrorMessage(path, log);

                        this.AddTmpResult(result, log, listResult, listLowResult);
                    }
                }
            }

            Dictionary<string, object> dicResult = new Dictionary<string, object>();

            if (listResult.Count > 0)
            {
                dicResult["type_name"] = ErsResources.GetMessage("urgency_high") + log.log_type_name;
                dicResult["log_path"] = log.log_path;
                dicResult["error_message"] = String.Join(this.ReplaceNewLine(objMappable.modelSettings.separator), listResult);
            }

            if (listLowResult.Count > 0)
            {
                dicResult["low_error_message"] = String.Join(this.ReplaceNewLine(objMappable.modelSettings.separator), listLowResult);
            }

            return dicResult;
        }

        /// <summary>
        /// エラーメッセージ取得
        /// </summary>
        /// <param name="path">ファイルパス</param>
        /// <param name="log">ログ</param>
        /// <returns>エラーメッセージ</returns>
        protected string GetErrorMessage(string path, ErrorLogSettings log)
        {
            var dicValues = log.GetPropertiesAsDictionary();

            dicValues["result"] = !log.hide_contents ? this.GetFileContents(path) : Path.GetFileName(path);

            return StringExtension.Format(this.ReplaceNewLine(log.message_format), dicValues);
        }

        /// <summary>
        /// ファイル内容取得
        /// </summary>
        /// <param name="path">ファイルパス</param>
        /// <returns>ファイル内容</returns>
        protected string GetFileContents(string path)
        {
            string result = string.Empty;

            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var sr = new StreamReader(fs, ErsEncoding.ShiftJIS))
            {
                result = sr.ReadToEnd();
            }

            return result;
        }

        /// <summary>
        /// 結果（一時）追加
        /// </summary>
        /// <param name="result">エラー内容</param>
        /// <param name="log">ログ</param>
        /// <param name="listResult">結果リスト</param>
        /// <param name="listLowResult">結果（緊急度低）リスト</param>
        protected void AddTmpResult(string result, ErrorLogSettings log, List<string> listResult, List<string> listLowResult)
        {
            if (string.IsNullOrEmpty(result))
            {
                return;
            }

            // 緊急度別
            switch (log.urgency)
            {
                case EnumUrgency.Low:
                    listLowResult.Add(result);
                    break;

                case EnumUrgency.High:
                    listResult.Add(result);
                    break;
            }
        }

        /// <summary>
        /// メール送信
        /// </summary>
        /// <param name="mailTo">送信先</param>
        /// <param name="mailFrom">送信元</param>
        /// <param name="mailCc">CC（カンマ区切りで複数）</param>
        /// <param name="mailBcc">BCC（カンマ区切りで複数）</param>
        /// <param name="mailTitle">メールタイトル</param>
        /// <param name="mailBody">メール本文</param>
        protected virtual void MailSend(string mailTo, string mailFrom, IEnumerable<string> mailCc, string mailBcc, string mailTitle, string mailBody)
        {
            if (string.IsNullOrEmpty(mailTo) || string.IsNullOrEmpty(mailFrom))
            {
                return;
            }

            string strCc = null;
            if (mailCc != null)
            {
                strCc = string.Join(",", mailCc);
            }

            var mail = ErsFactory.ersBatchFactory.GetErsSendMailBatch();
            mail.MailSend(
                mailTitle, mailBody,
                System.IO.Path.GetFileName(System.Reflection.Assembly.GetCallingAssembly().Location),
                mailTo, mailFrom,
                strCc, mailBcc);
        }

        /// <summary>
        /// 改行コード置換
        /// </summary>
        /// <param name="strSrc">置換対象</param>
        /// <returns>置換後文字列</returns>
        protected virtual string ReplaceNewLine(string strSrc)
        {
            return strSrc.Replace("\\r\\n", Environment.NewLine);
        }
    }
}