﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersMonitor.Domain.Home.Commands;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;

namespace ersMonitor.Domain.Home.Handlers
{
    public class DeleteMonitorHandler
        : ICommandHandler<IDeleteMonitorCommand>
    {
        public ICommandResult Submit(IDeleteMonitorCommand command)
        {
            this.DeleteMember(command);

            this.DeleteOrder(command);

            return new CommandResult(true);
        }

        /// <summary>
        /// Delete orders for purchase monitoring.
        /// </summary>
        private void DeleteOrder(IDeleteMonitorCommand command)
        {
            var orderSearchCriteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            if (command.monitor_purchase == EnumMonitorPurchase.First)
            {
                orderSearchCriteria.SetMonitorFirstSearch();
            }
            else
            {
                orderSearchCriteria.SetMonitorSecondSearch();
            }
            orderSearchCriteria.pm_flg = command.pm_flg;

            var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var listOrder = orderRepository.Find(orderSearchCriteria);

            foreach (var order in listOrder)
            {
                var orderCriteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
                orderCriteria.d_no = order.d_no;

                orderRepository.Delete(orderCriteria);

                var orderRecordCriteria = ErsFactory.ersOrderFactory.GetErsOrderRecordCriteria();
                orderRecordCriteria.d_no = order.d_no;

                orderRecordRepository.Delete(orderRecordCriteria);
            }
        }

        /// <summary>
        /// Delete an member for first purchase monitoring.
        /// </summary>
        private void DeleteMember(IDeleteMonitorCommand command)
        {
            if (command.monitor_purchase != EnumMonitorPurchase.First)
            {
                return;
            }

            var memberCriteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            memberCriteria.SetMonitorSearch();

            var memberRepository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var listMember = memberRepository.Find(memberCriteria);

            foreach (var member in listMember)
            {
                var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
                criteria.mcode = member.mcode;
                memberRepository.Delete(criteria);
            }
        }
    }
}