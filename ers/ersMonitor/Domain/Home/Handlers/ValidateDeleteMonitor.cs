﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersMonitor.Domain.Home.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersMonitor.Domain.Home.Handlers
{
    public class ValidateDeleteMonitor
        : IValidationHandler<IDeleteMonitorCommand>
    {
        public IEnumerable<ValidationResult> Validate(IDeleteMonitorCommand command)
        {
            yield return command.CheckRequired("monitor_purchase");
            yield return command.CheckRequired("pm_flg");
        }
    }
}