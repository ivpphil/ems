﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersMonitor.Models.Home.settings;

namespace ersMonitor.Domain.Home.Mappables
{
    public interface ICheckErrorLogMappable
        : IMappable
    {
        check_error_logSettingsModel modelSettings { get; set; }

        List<string> listLowResult { get; set; }

        List<Dictionary<string, object>> listResult { get; set; }

        string message { get; set; }
    }
}