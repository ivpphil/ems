﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersMonitor.Domain.Home.Mappables
{
    public interface ICheckNormalLogMappable
        : IMappable
    {
        Models.Home.settings.check_normal_logSettingsModel modelSettings { get; set; }

        List<Dictionary<string, object>> listResult { get; set; }

        List<string> listLowResult { get; set; }

        string message { get; set; }
    }
}