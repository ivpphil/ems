﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersMonitor.Models.Home.settings;

namespace ersMonitor.Domain.Home.Mappables
{
    public interface ICheckwwwMappable
        : IMappable
    {
        string error_message { get; set; }

        checkwwwSettingsModel modelSettings { get; set; }
    }
}