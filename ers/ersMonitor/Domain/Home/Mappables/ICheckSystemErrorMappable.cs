﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersMonitor.Domain.Home.Mappables
{
    public interface ICheckSystemErrorMappable
        : IMappable
    {
        Models.Home.settings.check_system_errorSettingsModel modelSettings { get; set; }

        string message { get; set; }

        List<Dictionary<string, object>> listResult { get; set; }
    }
}