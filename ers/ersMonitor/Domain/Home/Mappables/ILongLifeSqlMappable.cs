﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersMonitor.Domain.Home.Mappables
{
    public interface ILongLifeSqlMappable
        : IMappable
    {
        string error_message { get; set; }
    }
}