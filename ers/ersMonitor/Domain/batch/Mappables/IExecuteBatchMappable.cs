﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersMonitor.Domain.batch.Mappables
{
    public interface IExecuteBatchMappable
        : IMappable
    {

        List<Dictionary<string, object>> batchLogsList { get; set; }
        List<Dictionary<string, object>> batchList { get; set; }
    }
}