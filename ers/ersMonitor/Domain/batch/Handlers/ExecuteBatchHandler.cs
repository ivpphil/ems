﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersMonitor.Domain.batch.Commands;
using jp.co.ivp.ers;

namespace ersMonitor.Domain.batch.Handlers
{
    public class ExecuteBatchHandler
        : ICommandHandler<IExecuteBatchCommand>
    {
        public ICommandResult Submit(IExecuteBatchCommand command)
        {
            this.InsertImmediateInstruction(command);
            return new CommandResult(true);
        }

        private void InsertImmediateInstruction(IExecuteBatchCommand command)
        {
            var repository = ErsFactory.ersBatchFactory.GetErsBatchImmediateRepository();
            var objImmediate = ErsFactory.ersBatchFactory.GetErsBatchImmediate();
            objImmediate.batch_id = command.batch_id;
            objImmediate.execute_date = DateTime.Now;
            objImmediate.active = EnumActive.Active;
            repository.Insert(objImmediate);
        }
    }
}