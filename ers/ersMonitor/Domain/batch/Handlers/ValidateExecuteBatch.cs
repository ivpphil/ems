﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersMonitor.Domain.batch.Commands;
using System.ComponentModel.DataAnnotations;

namespace ersMonitor.Domain.batch.Handlers
{
    public class ValidateExecuteBatch
        : IValidationHandler<IExecuteBatchCommand>
    {
        public IEnumerable<ValidationResult> Validate(IExecuteBatchCommand command)
        {
            yield return command.CheckRequired("batch_id");
        }
    }
}