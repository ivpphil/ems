﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersMonitor.Domain.batch.Commands
{
    public interface IExecuteBatchCommand
        : ICommand
    {
        string batch_id { get; set; }
    }
}