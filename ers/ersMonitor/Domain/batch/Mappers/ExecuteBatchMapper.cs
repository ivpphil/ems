﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersMonitor.Domain.batch.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using NCrontab;

namespace ersMonitor.Domain.batch.Mappers
{
    public class ExecuteBatchMapper
        : IMapper<IExecuteBatchMappable>
    {
        public void Map(IExecuteBatchMappable objMappable)
        {
            this.MapBatches(objMappable);
            MapBatchScheduleLogs(objMappable);
        }

        private void MapBatches(IExecuteBatchMappable objMappable)
        {
            var repository = ErsFactory.ersBatchFactory.GetErsBatchScheduleRepository();
            var criteria = ErsFactory.ersBatchFactory.GetErsBatchScheduleCriteria();
            criteria.active = EnumActive.Active;
            criteria.SetOrderByBatch_id(Criteria.OrderBy.ORDER_BY_ASC);

            var listBatchRecords = repository.Find(criteria);

            var batchList = new List<Dictionary<string, object>>();

            foreach (var record in listBatchRecords)
            {
                var dictionary = record.GetPropertiesAsDictionary();
                dictionary["w_active"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Active, EnumCommonNameColumnName.namename, (int?)record.active);

                var last_date = record.last_date ?? record.intime;

                dictionary["isRecently"] = last_date > DateTime.Now.AddMinutes(-15);

                var contab = CrontabSchedule.Parse(record.schedule);
                var nextDate = contab.GetNextOccurrence(last_date.Value);
                dictionary["schedule_date"] = nextDate.AddSeconds(-nextDate.Second);

                var immediateRepository = ErsFactory.ersBatchFactory.GetErsBatchImmediateRepository();
                var immediateCriteria = ErsFactory.ersBatchFactory.GetErsBatchImmediateCriteria();
                immediateCriteria.batch_id = record.batch_id;
                immediateCriteria.active = EnumActive.Active;
                immediateCriteria.SetOrderByExecute_date(Criteria.OrderBy.ORDER_BY_ASC);
                immediateCriteria.LIMIT = 1;
                var immediateList = immediateRepository.Find(immediateCriteria);
                if (immediateList.Count > 0)
                {
                    dictionary["immediate_date"] = immediateList.First().execute_date;
                }

                batchList.Add(dictionary);
            }

            objMappable.batchList = batchList;
        }

        private void MapBatchScheduleLogs(IExecuteBatchMappable objMappable)
        {

            var repository = ErsFactory.ersRequestFactory.GetErsScheduleBatchLogRepository();
            var criteria = ErsFactory.ersRequestFactory.GetErsScheduleBatchLogCriteria();

            criteria.execution_date = DateTime.Today;

            var batchLogRecords = repository.Find(criteria);
            List<Dictionary<string, object>> listLogs = new List<Dictionary<string, object>>();

            foreach (var record in batchLogRecords)
            {
                var dictionary = record.GetPropertiesAsDictionary();

                dictionary["w_status"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.BatchStatus, EnumCommonNameColumnName.namename, (int)record.status);

                listLogs.Add(dictionary);

            }

            objMappable.batchLogsList = listLogs;


          
        }

    }
}