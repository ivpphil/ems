﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.stop_time;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersMonitor.Domain.mall.Mappables
{
    /// <summary>
    /// Mappable
    /// </summary>
    public interface IStopTimeModifyMappable
        : IMappable
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        int? id { get; set; }

        /// <summary>
        /// 停止開始日時 [Stop start datetime]
        /// </summary>
        DateTime? stop_from { get; set; }

        /// <summary>
        /// 停止終了日時 [Stop finish datetime]
        /// </summary>
        DateTime? stop_to { get; set; }

        /// <summary>
        /// モール店舗区分 [Mall shop type]
        /// </summary>
        EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// モール機能タイプ [Mall function type]
        /// </summary>
        EnumMallFuncType? mall_func_type { get; set; }


        /// <summary>
        /// 登録ボタン [Register button]
        /// </summary>
        bool register_btn { get; set; }

        /// <summary>
        /// 修正ボタン [Modify button]
        /// </summary>
        bool modify_btn { get; set; }

        /// <summary>
        /// 削除ボタン [Delete button]
        /// </summary>
        bool delete_btn { get; set; }


        /// <summary>
        /// データリスト [List of data]
        /// </summary>
        IList<Dictionary<string, object>> listStopTime { get; set; }

        /// <summary>
        /// モール店舗区分リスト [List of mall shop type]
        /// </summary>
        IList<Dictionary<string, object>> listMallShopKbn { get; set; }

        /// <summary>
        /// モール機能タイプリスト [List of mall function type]
        /// </summary>
        IList<Dictionary<string, object>> listMallFuncType { get; set; }
    }
}