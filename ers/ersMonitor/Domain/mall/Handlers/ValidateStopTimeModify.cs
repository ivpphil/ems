﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ersMonitor.Domain.mall.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersMonitor.Domain.mall.Handlers
{
    /// <summary>
    /// Validate
    /// </summary>
    public class ValidateStopTimeModify
        : IValidationHandler<IStopTimeModifyCommand>
    {
        /// <summary>
        /// Validate
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>結果 [Results]</returns>
        public IEnumerable<ValidationResult> Validate(IStopTimeModifyCommand command)
        {
            if (command.modify_btn || command.delete_btn)
            {
                yield return command.CheckRequired("id");
            }

            if (command.register_btn || command.modify_btn)
            {
                yield return command.CheckRequired("stop_from");
                yield return command.CheckRequired("stop_to");
                yield return command.CheckRequired("mall_shop_kbn");
                yield return command.CheckRequired("mall_func_type");

                if (command.stop_from.HasValue && command.stop_to.HasValue)
                {
                    foreach (var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDateTime("stop_from", command.stop_from, "stop_to", command.stop_to))
                    {
                        yield return result;
                    }
                }
            }
        }
    }
}