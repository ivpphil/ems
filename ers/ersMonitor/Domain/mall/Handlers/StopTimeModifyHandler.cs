﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersMonitor.Domain.mall.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.stop_time;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersMonitor.Domain.mall.Handlers
{
    /// <summary>
    /// Handler
    /// </summary>
    public class StopTimeModifyHandler
        : ICommandHandler<IStopTimeModifyCommand>
    {
        /// <summary>
        /// Submit
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>結果 [Results]</returns>
        public ICommandResult Submit(IStopTimeModifyCommand command)
        {
            if (command.register_btn)
            {
                // 登録 [Register]
                this.Register(command);
            }
            else if (command.modify_btn)
            {
                // 修正 [Modify]
                this.Modify(command);
            }
            else if (command.delete_btn)
            {
                // 削除 [Delete]
                this.Delete(command);
            }

            return new CommandResult(true);
        }

        #region 登録 [Register]
        /// <summary>
        /// 登録 [Register]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        protected virtual void Register(IStopTimeModifyCommand command)
        {
            var repository = ErsMallFactory.ersMallStopTimeFactory.GetErsMallStopTimeRepository();

            // 登録用データ取得 [Get the data for register]
            var objRegister = this.GetMallStopTimeForRegister(command);

            repository.Insert(objRegister);
        }

        /// <summary>
        /// 登録用データ取得 [Get the data for register]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <returns>登録用データ [The data for register]</returns>
        protected virtual ErsMallStopTime GetMallStopTimeForRegister(IStopTimeModifyCommand command)
        {
            var objRet = ErsMallFactory.ersMallStopTimeFactory.GetErsMallStopTime();

            objRet.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            return objRet;
        }
        #endregion

        #region 修正 [Modify]
        /// <summary>
        /// 修正 [Modify]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        protected virtual void Modify(IStopTimeModifyCommand command)
        {
            var repository = ErsMallFactory.ersMallStopTimeFactory.GetErsMallStopTimeRepository();

            var objOld = ErsMallFactory.ersMallStopTimeFactory.GetErsMallStopTimeWithID(command.id.Value);

            // 修正用データ取得 [Get the data for modify]
            var objNew = this.GetMallStopTimeForModify(command, objOld);

            repository.Update(objOld, objNew);
        }

        /// <summary>
        /// 修正用データ取得 [Get the data for modify]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        /// <param name="objOld">旧データ [Old data]</param>
        /// <returns>修正用データ [The data for modify]</returns>
        protected virtual ErsMallStopTime GetMallStopTimeForModify(IStopTimeModifyCommand command, ErsMallStopTime objOld)
        {
            var objRet = ErsMallFactory.ersMallStopTimeFactory.GetErsMallStopTimeWithParameters(objOld.GetPropertiesAsDictionary());

            objRet.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            return objRet;
        }
        #endregion

        #region 削除 [Delete]
        /// <summary>
        /// 削除 [Delete]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        protected virtual void Delete(IStopTimeModifyCommand command)
        {
            var repository = ErsMallFactory.ersMallStopTimeFactory.GetErsMallStopTimeRepository();

            var objDelete = ErsMallFactory.ersMallStopTimeFactory.GetErsMallStopTimeWithID(command.id.Value);

            repository.Delete(objDelete);
        }
        #endregion
    }
}