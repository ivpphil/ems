﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersMonitor.Domain.mall.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.stop_time;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersMonitor.Domain.mall.Mappers
{
    /// <summary>
    /// Mapper
    /// </summary>
    public class StopTimeModifyMapper
        : IMapper<IStopTimeModifyMappable>
    {
        /// <summary>
        /// リスト最大数 [Max count for list]
        /// </summary>
        protected const int STOP_TIME_LIST_MAX = 50;


        /// <summary>
        /// Map
        /// </summary>
        /// <param name="objMappable">Mappable</param>
        public void Map(IStopTimeModifyMappable objMappable)
        {
            // データロード [Load data]
            this.LoadData(objMappable);
        }

        /// <summary>
        /// データロード [Load data]
        /// </summary>
        /// <param name="objMappable">Mappable</param>
        protected virtual void LoadData(IStopTimeModifyMappable objMappable)
        {
            // データリスト取得 [Get the data list]
            var listFind = this.GetMallStopTimeList();

            objMappable.listStopTime = new List<Dictionary<string, object>>();

            foreach (var data in listFind)
            {
                var dicTmp = data.GetPropertiesAsDictionary();

                if (objMappable.modify_btn &&
                    objMappable.id.HasValue && objMappable.id == data.id)
                {
                    // 表示用データセット [Set data for display]
                    this.SetDispData(objMappable.GetPropertiesAsDictionary(), dicTmp);
                }
                else
                {
                    // 表示用データセット [Set data for display]
                    this.SetDispData(dicTmp, dicTmp);
                }

                objMappable.listStopTime.Add(dicTmp);
            }

            // 登録用データ挿入 [Insert the data for register]
            this.InsertDataForRegister(objMappable);
        }

        /// <summary>
        /// データリスト取得 [Get the data list]
        /// </summary>
        /// <returns>データリスト [The data list]</returns>
        protected virtual IList<ErsMallStopTime> GetMallStopTimeList()
        {
            var repository = ErsMallFactory.ersMallStopTimeFactory.GetErsMallStopTimeRepository();
            var criteria = ErsMallFactory.ersMallStopTimeFactory.GetErsMallStopTimeCriteria();

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.LIMIT = STOP_TIME_LIST_MAX;

            return repository.Find(criteria);
        }

        /// <summary>
        /// 表示用データセット [Set data for display]
        /// </summary>
        /// <param name="dicSrc">元データディクショナリ [Data dictionary (source)]</param>
        /// <param name="dicDst">先データディクショナリ [Data dictionary (destination)]</param>
        protected virtual void SetDispData(Dictionary<string, object> dicSrc, Dictionary<string, object> dicDst)
        {
            dicDst["disp_stop_from"] = dicSrc["stop_from"];
            dicDst["disp_stop_to"] = dicSrc["stop_to"];
            dicDst["disp_mall_shop_kbn"] = dicSrc["mall_shop_kbn"];
            dicDst["disp_mall_func_type"] = dicSrc["mall_func_type"];

            if (dicDst["disp_stop_from"] != null && dicDst["disp_stop_to"] != null)
            {
                var dateNow = DateTime.Now;
                dicDst["isActive"] = Convert.ToDateTime(dicDst["disp_stop_from"]) <= dateNow && dateNow <= Convert.ToDateTime(dicDst["disp_stop_to"]);
            }
        }

        /// <summary>
        /// 登録用データ挿入 [Insert the data for register]
        /// </summary>
        /// <param name="objMappable">Mappable</param>
        protected virtual void InsertDataForRegister(IStopTimeModifyMappable objMappable)
        {
            var dicTmp = new Dictionary<string, object>();

            if (objMappable.register_btn)
            {
                // 表示用データセット [Set data for display]
                this.SetDispData(objMappable.GetPropertiesAsDictionary(), dicTmp);
            }

            // 先頭に登録用データ挿入 [Insert the data for register to first]
            objMappable.listStopTime.Insert(0, dicTmp);
        }
    }
}