﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersMonitor.Models.batch;
using ersMonitor.Domain.batch.Mappables;
using ersMonitor.Domain.batch.Commands;

namespace ersMonitor.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    public class batchController
        : ErsControllerBase
    {
        /// <summary>
        /// バッチエラーログ監視
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult execute_batch(execute_batch execute_batch, EnumEck? eck = null)
        {
            if (!ModelState.IsValid && !this.IsErrorBack(eck))
            {
                return GetErrorView();
            }
            this.mapperBus.Map<IExecuteBatchMappable>(execute_batch);

            return this.View("execute_batch", execute_batch);
        }

        /// <summary>
        /// バッチエラーログ監視
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult execute_batch_complete(execute_batch execute_batch)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IExecuteBatchCommand>(execute_batch), execute_batch);
            if (!ModelState.IsValid)
            {
                return this.execute_batch(execute_batch, EnumEck.Error);
            }

            this.commandBus.Submit<IExecuteBatchCommand>(execute_batch, EnumCommandTransaction.BeginTransaction);

            this.mapperBus.Map<IExecuteBatchMappable>(execute_batch);

            return this.RedirectToAction("execute_batch");
        }

        #region Override or New implementations
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override ErsState GetErsSessionState()
        {
            return ErsFactory.ersSessionStateFactory.getErsSessionState();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void SetSessionToView()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override ErsState GetErsState()
        {
            return ErsContext.sessionState;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestContext"></param>
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            ErsContext.sessionState = this.GetErsSessionState();
            ErsContext.sessionState.Restore(Request.Cookies, Request.Form, Request.QueryString);
        }

        #endregion
    }
}