﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ersMonitor.Models.Home;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using ersMonitor.Domain.Home.Mappables;
using ersMonitor.Domain.Home.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;


namespace ersMonitor.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    public class HomeController
        : ErsControllerBase
    {
        /// <summary>
        /// 死活監視
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult checkwww(checkwww model)
        {
            this.mapperBus.Map<ICheckwwwMappable>(model);
            return View("checkwww", model);
        }

        /// <summary>
        /// 500エラー監視
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult check_system_error(check_system_error model)
        {
            this.mapperBus.Map<ICheckSystemErrorMappable>(model);
            return View("check_system_error", model);
        }

        /// <summary>
        /// バッチ正常ログ監視（実行監視）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult check_normal_log(check_normal_log model)
        {
            this.mapperBus.Map<ICheckNormalLogMappable>(model);
            return View("check_normal_log", model);
        }

        /// <summary>
        /// バッチエラーログ監視
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult check_error_log(check_error_log model)
        {
            this.mapperBus.Map<ICheckErrorLogMappable>(model);
            return View("check_error_log", model);
        }

        /// <summary>
        /// 滞留SQL監視
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult long_life_sql(long_life_sql model)
        {
            this.mapperBus.Map<ILongLifeSqlMappable>(model);
            return View("long_life_sql", model);
        }

        /// <summary>
        /// 監視用伝票・会員削除
        /// </summary>
        /// <param name="del_monitor"></param>
        /// <returns></returns>
        public ActionResult delete_monitor(delete_monitor del_monitor)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IDeleteMonitorCommand>(del_monitor), del_monitor);

            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.commandBus.Submit<IDeleteMonitorCommand>(del_monitor, EnumCommandTransaction.BeginTransaction);

            return View("task_complete");
        }

        #region Override or New implementations
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override ErsState GetErsSessionState()
        {
            return ErsFactory.ersSessionStateFactory.getErsSessionState();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void SetSessionToView()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override ErsState GetErsState()
        {
            return ErsContext.sessionState;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestContext"></param>
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            ErsContext.sessionState = this.GetErsSessionState();
            ErsContext.sessionState.Restore(Request.Cookies, Request.Form, Request.QueryString);
        }

        #endregion
    }
}
