﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ersMonitor.Domain.mall.Commands;
using ersMonitor.Domain.mall.Mappables;
using ersMonitor.Models.mall;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersMonitor.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    public class mallController
        : ErsControllerBase
    {
        #region Override or New implementations
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override ErsState GetErsSessionState()
        {
            return ErsFactory.ersSessionStateFactory.getErsSessionState();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void SetSessionToView()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override ErsState GetErsState()
        {
            return ErsContext.sessionState;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestContext"></param>
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            ErsContext.sessionState = this.GetErsSessionState();
            ErsContext.sessionState.Restore(Request.Cookies, Request.Form, Request.QueryString);
        }

        #endregion

        #region 停止時間修正 [Modify mall stop time]
        /// <summary>
        /// 停止時間修正 [Modify mall stop time]
        /// </summary>
        /// <param name="model">モデル [Model]</param>
        /// <param name="eck">戻りフラグ [Back flag]</param>
        /// <returns>結果 [Result]</returns>
        public ActionResult stop_time_modify(stop_time_modify model, EnumEck? eck = null)
        {
            if (!ModelState.IsValid && !this.IsErrorBack(eck))
            {
                return GetErrorView();
            }

            this.mapperBus.Map<IStopTimeModifyMappable>(model);

            return this.View("stop_time_modify", model);
        }

        /// <summary>
        /// 停止時間修正完了 [Modify complete mall stop time]
        /// </summary>
        /// <param name="model">モデル [Model]</param>
        /// <returns>結果 [Result]</returns>
        public ActionResult stop_time_modify_complete(stop_time_modify model)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IStopTimeModifyCommand>(model), model);

            if (!ModelState.IsValid)
            {
                return this.stop_time_modify(model, EnumEck.Error);
            }

            this.commandBus.Submit<IStopTimeModifyCommand>(model, EnumCommandTransaction.BeginTransaction);

            this.mapperBus.Map<IStopTimeModifyMappable>(model);

            return this.RedirectToAction("stop_time_modify");
        }
        #endregion
    }
}