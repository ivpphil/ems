using System.Web.Mvc;
using System.Reflection;
using jp.co.ivp.ers.mvc.CommandProcessor.Dispatcher;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.member;

namespace ersMonitor
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            //command-handler定義 同名対応の為、フルパスで記載し、usingは使わない

            //Home
            container.RegisterType<IMapper<ersMonitor.Domain.Home.Mappables.ICheckwwwMappable>, ersMonitor.Domain.Home.Mappers.CheckwwwMapper>();
            container.RegisterType<IMapper<ersMonitor.Domain.Home.Mappables.ICheckSystemErrorMappable>, ersMonitor.Domain.Home.Mappers.CheckSystemErrorMapper>();
            container.RegisterType<IMapper<ersMonitor.Domain.Home.Mappables.ICheckNormalLogMappable>, ersMonitor.Domain.Home.Mappers.CheckNormalLogMapper>();
            container.RegisterType<IMapper<ersMonitor.Domain.Home.Mappables.ICheckErrorLogMappable>, ersMonitor.Domain.Home.Mappers.CheckErrorLogMapper>();
            container.RegisterType<IMapper<ersMonitor.Domain.Home.Mappables.ILongLifeSqlMappable>, ersMonitor.Domain.Home.Mappers.LongLifeSqlMapper>();

            container.RegisterType<ICommandHandler<ersMonitor.Domain.Home.Commands.IDeleteMonitorCommand>, ersMonitor.Domain.Home.Handlers.DeleteMonitorHandler>();
            container.RegisterType<IValidationHandler<ersMonitor.Domain.Home.Commands.IDeleteMonitorCommand>, ersMonitor.Domain.Home.Handlers.ValidateDeleteMonitor>();

            //Batch
            container.RegisterType<IMapper<ersMonitor.Domain.batch.Mappables.IExecuteBatchMappable>, ersMonitor.Domain.batch.Mappers.ExecuteBatchMapper>();
            container.RegisterType<IValidationHandler<ersMonitor.Domain.batch.Commands.IExecuteBatchCommand>, ersMonitor.Domain.batch.Handlers.ValidateExecuteBatch>();
            container.RegisterType<ICommandHandler<ersMonitor.Domain.batch.Commands.IExecuteBatchCommand>, ersMonitor.Domain.batch.Handlers.ExecuteBatchHandler>();

            //Mall
            container.RegisterType<IMapper<ersMonitor.Domain.mall.Mappables.IStopTimeModifyMappable>, ersMonitor.Domain.mall.Mappers.StopTimeModifyMapper>();
            container.RegisterType<IValidationHandler<ersMonitor.Domain.mall.Commands.IStopTimeModifyCommand>, ersMonitor.Domain.mall.Handlers.ValidateStopTimeModify>();
            container.RegisterType<ICommandHandler<ersMonitor.Domain.mall.Commands.IStopTimeModifyCommand>, ersMonitor.Domain.mall.Handlers.StopTimeModifyHandler>();


            return container;
        }
    }
}