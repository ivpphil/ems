﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using jp.co.ivp.ers;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.atmail;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.common;
using jp.co.ivp.ers.contents;
using jp.co.ivp.ers.coupon;
using jp.co.ivp.ers.doc_bundle;
using jp.co.ivp.ers.lp;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.sendmail;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.stepmail;
using jp.co.ivp.ers.target;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.language;
using jp.co.ivp.ers.request;

namespace ersMonitor
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : ErsMvcApplication
    {
        protected override string[] GetNamespace()
        {
            return new[] { "ersMonitor.Controllers" };
        }

        protected override void SetFactory()
        {
            this.SetMallFactory();

            ErsFactory.ersBasketFactory = new ErsBasketFactory();

            ErsFactory.ersMerchandiseFactory = new ErsMerchandiseFactory();

            ErsFactory.ersMemberFactory = new ErsMemberFactory();

            ErsFactory.ersMailFactory = new ErsMailFactory();

            ErsFactory.ersSessionStateFactory = new ErsSessionStateFactory();

            ErsFactory.ersViewServiceFactory = new ErsViewServiceFactory();

            ErsFactory.ersUtilityFactory = new ErsUtilityFactory();

            ErsFactory.ersOrderFactory = new ErsOrderFactory();

            ErsFactory.ersAddressInfoFactory = new ErsAddressInfoFactory();

            ErsFactory.ersPointHistoryFactory = new ErsPointHistoryFactory();
			
			ErsFactory.ersLanguageFactory = new ErsLanguageFactory();

            ErsFactory.ersAdministratorFactory = new ErsAdministratorFactory();

            ErsFactory.ersTargetFactory = new ErsTargetFactory();

            ErsFactory.ersDocBundleFactory = new ErsDocBundleFactory();

            ErsFactory.ersCouponFactory = new ErsCouponFactory();

            ErsFactory.ersContentsFactory = new ErsContentsFactory();

            ErsFactory.ErsAtMailFactory = new ErsAtMailFactory();

            ErsFactory.ersStepMailFactory = new ErsStepMailFactory();

            ErsFactory.ersCommonFactory = new ErsCommonFactory();

            ErsFactory.ersBatchFactory = new ErsBatchFactory();

            ErsFactory.ersLpFactory = new ErsLpFactory();

            ErsFactory.ersRequestFactory = new ErsRequestFactory();
        }

        protected virtual void SetMallFactory()
        {
            ErsMallFactory.ersSiteFactory = new global::jp.co.ivp.ers.mall.site.ErsSiteFactory();

            ErsMallFactory.ersMallBatchFactory = new global::jp.co.ivp.ers.mall.batch.ErsMallBatchFactory();

            ErsMallFactory.ersMallUtilityFactory = new global::jp.co.ivp.ers.mall.util.ErsMallUtilityFactory();

            ErsMallFactory.ersMallShopFactory = new global::jp.co.ivp.ers.mall.shop.ErsMallShopFactory();

            ErsMallFactory.ersMallMailFactory = new global::jp.co.ivp.ers.mall.sendmail.ErsMallMailFactory();

            ErsMallFactory.ersMallViewServiceFactory = new global::jp.co.ivp.ers.mall.viewService.ErsMallViewServiceFactory();

            ErsMallFactory.ersMallAmazonFactory = new global::jp.co.ivp.ers.mall.amazon.ErsMallAmazonFactory();

            ErsMallFactory.ersMallAPIFactory = new global::jp.co.ivp.ers.mall.api.ErsMallAPIFactory();

            ErsMallFactory.ersMallCommonFactory = new global::jp.co.ivp.ers.mall.common.ErsMallCommonFactory();

            ErsMallFactory.ersMallStopTimeFactory = new global::jp.co.ivp.ers.mall.stop_time.ErsMallStopTimeFactory();

            ErsMallFactory.ersMallOrderFactory = new global::jp.co.ivp.ers.mall.mall_order.ErsMallOrderFactory();

            ErsMallFactory.ersMallProductFactory = new global::jp.co.ivp.ers.mall.product.ErsMallProductFactory();

            ErsMallFactory.ersMallStockErrorFactory = new global::jp.co.ivp.ers.mall.stock_error.ErsMallStockErrorFactory();

            ErsMallFactory.ersMallStockRecoveryFactory = new global::jp.co.ivp.ers.mall.stock_recovery.ErsMallStockRecoveryFactory();

            ErsMallFactory.ersMallStockFactory = new global::jp.co.ivp.ers.mall.stock.ErsMallStockFactory();
        }

        protected override void RegisterRoutes(RouteCollection routes)
        {
            base.RegisterRoutes(routes);

            routes.Remove(routes["Default"]);

            routes.IgnoreRoute("js/{*pathInfo}");
            routes.IgnoreRoute("css/{*pathInfo}");
            routes.IgnoreRoute("images/{*pathInfo}");

            routes.MapRoute(
                "Explain", // Route name
                "explain/{*staticPath}", // URL with parameters
                new { controller = "Home", action = "rooting" }, // Parameter defaults
                    GetNamespace()
            );

            routes.MapRoute(
               "DefaultAsp", // Route name
               "{action}.asp", // URL with parameters
               new { controller = "Home", action = "index" }, // Parameter defaults
                  GetNamespace()
            );

            routes.MapRoute(
               "Default", // Route name
               "{controller}/{action}", // URL with parameters
               new { controller = "Home", action = "index" }, // Parameter defaults
                  GetNamespace()
            );
        }

        protected override void SetResolver()
        {
            Bootstrapper.Initialise();
        }
    }
}