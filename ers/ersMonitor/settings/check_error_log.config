<?xml version="1.0"?>
<settings>
    <!--監視対象サーバIP-->
    <target_server_ip></target_server_ip>
    <!--監視対象サーバユーザ-->
    <target_server_user></target_server_user>
    <!--監視対象サーバパス-->
    <target_server_pass></target_server_pass>

    <!--メール送信元-->
    <mail_from>v7-appli-kanshi@ivp.co.jp</mail_from>
    <!--メール送信先-->
    <mail_to>appli-kanshi@ivp.co.jp</mail_to>
    <!--メールCC-->
    <mail_cc>
        <address>nagaike@ivp.co.jp</address>
    </mail_cc>
    
    <!--メールタイトル-->
    <mail_title>【ers-v7】エラーログ監視</mail_title>

    <!--メッセージ-->
    <message>エラーログ監視でエラーが発生しました。\r\nそれぞれ対処方法に従って対処された後、エラーログを"bk"フォルダ（無ければ作成）へ退避してください。\r\n</message>

    <!--セパレータ-->
    <separator>\r\n---------------------------------------------------------\r\n</separator>

    <!--ログ-->
    <logs>
        <!--旧sendmail-->
        <log>
            <!--ログパス-->
            <log_path>C:\sendmailivp\mqueue</log_path>
            <!--ログ種別名-->
            <log_type_name>旧SMTPメール送信</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--検知までの猶予時間（猶予時間経過後にエラー発報）-->
            <delay_minutes>5</delay_minutes>
            <!--メッセージ-->
            <message_format>
                指定時間送信されていないメールファイルを検知しました。({delay_minutes}分)\r\n
                メールサーバーとの疎通を確認してください。\r\n
                復旧に時間がかかる場合は、お客様へ連絡してください。\r\n
            </message_format>
        </log>

        <!--sendmail-->
        <log>
            <!--ログパス-->
            <log_path>C:\sendmailivp\log\error</log_path>
            <!--ログ種別名-->
            <log_type_name>SMTPメール送信</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>
                新SMTPメール監視でエラーを検知しました。
                メールサーバーとの疎通を確認してください。(ログ：C:\sendmailivp\log\sendmailivplog.log)※拡張子にご注意（旧smtpのログは.txt）

                {result}

                【参考】以下のエラーはネットワーク疎通不具合にて想定されるエラーです。
                ○System.Net.Sockets.SocketException: 対象のコンピューターによって拒否されたため、接続できませんでした。 127.0.0.2:25
                ○System.TimeoutException: Tcp Connect Timeout（SMTPサーバーとの接続タイムアウト。）

                疎通確認後、下記の手順でメールを再送信してください。
                1.) C:\sendmailivp\mqueue_retry以下のテキストをC:\sendmailivp\mqueueへコピーする。
                2.) smtp_taskを実行する。
                3.) smtp_taskの実行ログ(C:\sendmailivp\log\sendmailivplog.txt)を確認し、正常であればC:\sendmailivp\mqueue_retry以下のテキストを削除する。
            </message_format>
        </log>

        <!--メール配信リトライフォルダ-->
        <log>
            <!--ログパス-->
            <log_path>C:\sendmailivp\mqueue_retry</log_path>
            <!--ログ種別名-->
            <log_type_name>メール配信リトライフォルダ</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>メール配信リトライフォルダにメールが残っています。\r\n{result}\r\n</message_format>
            <!--ファイル内容非表示-->
            <hide_contents>true</hide_contents>
        </log>

        <!--＠メール監視-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\MonitorMassSend\err</log_path>
            <!--ログ種別名-->
            <log_type_name>＠メール監視（MonitorMassSend）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>
                ＠メール監視でエラーを検知しました。\r\n
                下記エラーが「＠メール監視用メールが送信されていませんでした。」というエラーメッセージの場合は、\r\n
                MonitorMassSendをリトライし、解消しなければコンテンツサーバーとメールサーバーの疎通を確認してください。\r\n
                リトライの際は、MonitorMassSendPrepareを実行した後、1分待った後にMonitorMassSendを実行してください。\r\n
                {result}\r\n
            </message_format>
        </log>

        <!--＠メール監視（準備）-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\MonitorMassSendPrepare\err</log_path>
            <!--ログ種別名-->
            <log_type_name>＠メール監視（準備）（MonitorMassSendPrepare）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければコンテンツサーバーとメールサーバーの疎通を確認してください。\r\n{result}\r\n</message_format>
        </log>

        <!--DeleteFiles-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\DeleteFiles\err</log_path>
            <!--ログ種別名-->
            <log_type_name>ファイル削除（DeleteFiles）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--DeleteTables-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\DeleteTables\err</log_path>
            <!--ログ種別名-->
            <log_type_name>テーブル削除（DeleteTables）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--MassSendManager-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\MassSendManager\err\</log_path>
            <!--ログ種別名-->
            <log_type_name>@メール送信処理（MassSendManager）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--CreateListForStepMail-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\CreateListForStepMail\err</log_path>
            <!--ログ種別名-->
            <log_type_name>ステップメールリスト作成処理（CreateListForStepMail）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--ContactMailReception-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\ContactMailReception\err</log_path>
            <!--ログ種別名-->
            <log_type_name>＠コンタクトメール受信処理（ContactMailReception）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--ContactMailSend-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\ContactMailSend\err</log_path>
            <!--ログ種別名-->
            <log_type_name>＠コンタクトメール送信処理（ContactMailSend）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--CreditAccountLaunderingDownload-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\CreditAccountLaunderingDownload\err</log_path>
            <!--ログ種別名-->
            <log_type_name>カード洗い換え結果取得処理（CreditAccountLaunderingDownload）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--CreditAccountLaunderingUpload-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\CreditAccountLaunderingUpload\err</log_path>
            <!--ログ種別名-->
            <log_type_name>カード洗い換えデータ送信処理（CreditAccountLaunderingUpload）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--CreditCapturePayment-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\CreditCapturePayment\err</log_path>
            <!--ログ種別名-->
            <log_type_name>クレジット決済売り上げ処理（CreditCapturePayment）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--MemberPointGiveDelete-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\MemberPointGiveDelete\err</log_path>
            <!--ログ種別名-->
            <log_type_name>ポイント付与/失効処理（MemberPointGiveDelete）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--UpdatePriceForSearch-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\UpdatePriceForSearch\err</log_path>
            <!--ログ種別名-->
            <log_type_name>検索用価格更新処理（UpdatePriceForSearch）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--RegularOrderBilling-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\RegularOrderBilling\err</log_path>
            <!--ログ種別名-->
            <log_type_name>定期伝票発行処理（RegularOrderBilling）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--MemberRank-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\MemberRank\err</log_path>
            <!--ログ種別名-->
            <log_type_name>会員ランク付与バッチ（MemberRank）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--ProductRanking-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\ProductRanking\err</log_path>
            <!--ログ種別名-->
            <log_type_name>商品ランキング生成処理（ProductRanking）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--ProductStockAlertMail-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\ProductStockAlertMail\err</log_path>
            <!--ログ種別名-->
            <log_type_name>在庫アラートメール（ProductStockAlertMail）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--BundleDocument-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\BundleDocument\err</log_path>
            <!--ログ種別名-->
            <log_type_name>同梱処理（BundleDocument）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--CtsStockRelease-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\CtsStockRelease\err</log_path>
            <!--ログ種別名-->
            <log_type_name>仮受注在庫リリース（CtsStockRelease）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--SendRegularOrderMail-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\SendRegularOrderMail\err</log_path>
            <!--ログ種別名-->
            <log_type_name>定期伝票発行通知メール（SendRegularOrderMail）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--SendShippedMail-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\SendShippedMail\err</log_path>
            <!--ログ種別名-->
            <log_type_name>配送完了メール（SendShippedMail）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--SendMallShippedMail-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\SendMallShippedMail\err</log_path>
            <!--ログ種別名-->
            <log_type_name>モール配送完了メール（SendMallShippedMail）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>
                1) IVPのメールサーバ（SMTP）との疎通を確認して下さい。
                2) 楽天のメールサーバ（SMTP-AUTH）との疎通を確認して下さい。
                　ホスト：sub.fw.rakuten.ne.jp
                　ポート：587
                3) 1), 2) の確認後、問題なければリトライして下さい。それでも解消しなければ担当者へ連絡して下さい。
                {result}
            </message_format>
        </log>

        <!--SendMallThankyouMail-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\SendMallThankyouMail\err</log_path>
            <!--ログ種別名-->
            <log_type_name>モール購入完了メール（SendMallThankyouMail）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>
                1) IVPのメールサーバ（SMTP）との疎通を確認して下さい。
                2) 楽天のメールサーバ（SMTP-AUTH）との疎通を確認して下さい。
                　ホスト：sub.fw.rakuten.ne.jp
                　ポート：587
                3) 1), 2) の確認後、問題なければリトライして下さい。それでも解消しなければ担当者へ連絡して下さい。
                {result}
            </message_format>
        </log>

        <!--ImportMallOrder-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\ImportMallOrder\err</log_path>
            <!--ログ種別名-->
            <log_type_name>モール伝票取り込み（ImportMallOrder）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ不要です。担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--MergeMallOrder-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\MergeMallOrder\err</log_path>
            <!--ログ種別名-->
            <log_type_name>モール伝票マージ（MergeMallOrder）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ不要です。担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--OperateMallOrder-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\OperateMallOrder\err</log_path>
            <!--ログ種別名-->
            <log_type_name>モール伝票ステータス更新（OperateMallOrder）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ不要です。担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--OperateMallProduct-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\OperateMallProduct\err</log_path>
            <!--ログ種別名-->
            <log_type_name>モール商品更新（OperateMallProduct）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ不要です。担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--OperateMallProductImage-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\OperateMallProductImage\err</log_path>
            <!--ログ種別名-->
            <log_type_name>モール商品画像更新（OperateMallProductImage）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--UpdateRakutenCampaignProducts--><!--
        <log>
            --><!--ログパス--><!--
            <log_path>C:\logs\ers-v7.2.0\batch\UpdateRakutenCampaignProducts\err</log_path>
            --><!--ログ種別名--><!--
            <log_type_name>モール楽天キャンペーン商品更新（UpdateRakutenCampaignProducts）</log_type_name>
            --><!--緊急度--><!--
            <urgency>1</urgency>
            --><!--メッセージ--><!--
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>-->

        <!--RecoveryMallStock-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\RecoveryMallStock\err</log_path>
            <!--ログ種別名-->
            <log_type_name>モール在庫リカバリ（RecoveryMallStock）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--MonitorUpdateMallStockHistory-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\MonitorUpdateMallStockHistory\err</log_path>
            <!--ログ種別名-->
            <log_type_name>モール在庫更新履歴監視（MonitorUpdateMallStockHistory）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--CreditCaptureMallPayment-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\CreditCaptureMallPayment\err</log_path>
            <!--ログ種別名-->
            <log_type_name>モールクレジット決済売り上げ処理（CreditCaptureMallPayment）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--ReplicationObserver-->
        <log>
          <!--ログパス-->
          <log_path>C:\logs\ers-v7.2.0\batch\ReplicationObserver\err</log_path>
          <!--ログ種別名-->
          <log_type_name>レプリケーション監視（ReplicationObserver）</log_type_name>
          <!--緊急度-->
          <urgency>1</urgency>
          <!--メッセージ-->
          <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--UpdateProductKeyword-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\UpdateProductKeyword\err</log_path>
            <!--ログ種別名-->
            <log_type_name>サジェスチョンキーワード作成（UpdateProductKeyword）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--UpdateSpecifiedColumn-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\UpdateSpecifiedColumn\err</log_path>
            <!--ログ種別名-->
            <log_type_name>指定テーブル更新（UpdateSpecifiedColumn）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--DownloadSpecifiedColumnCommand-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\DownloadSpecifiedColumnCommand\err</log_path>
            <!--ログ種別名-->
            <log_type_name>指定テーブルダウンロード（DownloadSpecifiedColumnCommand）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--InsertSpecifiedColumn-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\InsertSpecifiedColumn\err</log_path>
            <!--ログ種別名-->
            <log_type_name>指定テーブルインサート（InsertSpecifiedColumn）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--CreditContinualBillingDownload-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\CreditContinualBillingDownload\err</log_path>
            <!--ログ種別名-->
            <log_type_name>継続課金結果取得処理（CreditContinualBillingDownload）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--CreditContinualBillingUpload-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\CreditContinualBillingUpload\err</log_path>
            <!--ログ種別名-->
            <log_type_name>継続課金データ送信処理（CreditContinualBillingUpload）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>

        <!--RegularOrderDeactivate-->
        <log>
            <!--ログパス-->
            <log_path>C:\logs\ers-v7.2.0\batch\RegularOrderDeactivate\err</log_path>
            <!--ログ種別名-->
            <log_type_name>定期伝票停止処理（RegularOrderDeactivate）</log_type_name>
            <!--緊急度-->
            <urgency>1</urgency>
            <!--メッセージ-->
            <message_format>リトライ後、解消しなければ担当者へ連絡して下さい。\r\n{result}\r\n</message_format>
        </log>
    </logs>
</settings>
