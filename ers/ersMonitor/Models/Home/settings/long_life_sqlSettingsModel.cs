﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace ersMonitor.Models.Home.settings
{
    public class long_life_sqlSettingsModel
        : ErsBindableModel
    {
        /// <summary>
        /// コネクション設定
        /// </summary>
        [XmlField]
        [BindTable("connection_settings")]
        public List<ConnectionSettings> connection_settings { get; set; }

        /// <summary>
        /// 経過時間（分）
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        [XmlField]
        public int elapsed_minutes { get; set; }

        /// <summary>
        /// 無視アプリケーション
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, isArray = true)]
        [XmlField]
        public string[] ignore_applications { get; set; }

        /// <summary>
        /// 無視クエリ
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, isArray = true)]
        [XmlField]
        public string[] ignore_queries { get; set; }

        /// <summary>
        /// セパレータ
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string separator { get; set; }
    }

    /// <summary>
    /// コネクション設定
    /// </summary>
    public class ConnectionSettings
        : ErsBindableModel
    {
        /// <summary>
        /// コネクション名
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string connection_name { get; set; }

        /// <summary>
        /// コネクションストリング
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.ConnectionString)]
        public virtual string connection_string { get; set; }

        /// <summary>
        /// 対象DB名
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        [XmlField]
        public string target_db_name { get; set; }
    }
}
