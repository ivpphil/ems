﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace ersMonitor.Models.Home.settings
{
    public class checkwwwSettingsModel
        : ErsBindableModel
    {
        /// <summary>
        /// URL
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All, isArray = true)]
        [XmlField]
        public string[] urls { get; set; }

        /// <summary>
        /// メッセージフォーマット（URL）
        /// </summary>
        /// <remarks>0 : URL ／ 1 : エラーメッセージ</remarks>
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        [XmlField]
        public string message_format_url { get; set; }

        /// <summary>
        /// メッセージフォーマット（DB）
        /// </summary>
        /// <remarks>0 : エラーメッセージ</remarks>
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        [XmlField]
        public string message_format_db { get; set; }

        /// <summary>
        /// セパレータ
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string separator { get; set; }
    }
}
