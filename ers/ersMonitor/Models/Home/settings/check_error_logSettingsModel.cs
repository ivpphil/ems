﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers;
using jp.co.ivp.ers.batch;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace ersMonitor.Models.Home.settings
{
    public class check_error_logSettingsModel
        : ErsBindableModel
    {
        /// <summary>
        /// 監視対象サーバIP
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string target_server_ip { get; set; }

        /// <summary>
        /// 監視対象サーバユーザ
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string target_server_user { get; set; }

        /// <summary>
        /// 監視対象サーバパス
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string target_server_pass { get; set; }

        /// <summary>
        /// メール送信元
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string mail_from { get; set; }

        /// <summary>
        /// メール送信先
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string mail_to { get; set; }

        /// <summary>
        /// メールCC
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string[] mail_cc { get; set; }

        /// <summary>
        /// メールタイトル
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string mail_title { get; set; }

        /// <summary>
        /// メッセージ
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string message { get; set; }

        /// <summary>
        /// セパレータ
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string separator { get; set; }

        /// <summary>
        /// ログ
        /// </summary>
        [XmlField]
        [BindTable("logs")]
        public List<ErrorLogSettings> logs { get; set; }
    }

    public class ErrorLogSettings
        : ErsBindableModel
    {
        /// <summary>
        /// ログパス
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string log_path { get; set; }

        /// <summary>
        /// ログ種別名
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string log_type_name { get; set; }

        /// <summary>
        /// 緊急度
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual EnumUrgency urgency { get; set; }

        /// <summary>
        /// 検知までの猶予時間（猶予時間経過後にエラー発報）
        /// </summary>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? delay_minutes { get; set; }

        /// <summary>
        /// メッセージフォーマット
        /// </summary>
        /// <remarks>0 : ファイル名 ／ 1 : エラー内容</remarks>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string message_format { get; set; }

        /// <summary>
        /// ファイル内容非表示
        /// </summary>
        /// <remarks>true : 非表示 ／ false : 表示</remarks>
        [XmlField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual bool hide_contents { get; set; }
    }
}
