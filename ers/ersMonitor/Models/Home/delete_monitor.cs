﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using ersMonitor.Domain.Home.Commands;
using jp.co.ivp.ers.mvc.validation;

namespace ersMonitor.Models.Home
{
    public class delete_monitor
        : ErsModelBase, IDeleteMonitorCommand
    {
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public EnumMonitorPurchase? monitor_purchase { get; set; }

        [ErsSchemaValidation("d_master_t.pm_flg")]
        public EnumPmFlg? pm_flg { get; set; }
    }
}