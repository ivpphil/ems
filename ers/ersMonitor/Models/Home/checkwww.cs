﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using ersMonitor.Models.Home.settings;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.model;
using jp.co.ivp.ers.util;
using ersMonitor.Domain.Home.Mappables;

namespace ersMonitor.Models.Home
{
    public class checkwww
        : ErsModelBase, ICheckwwwMappable
    {
        public checkwwwSettingsModel modelSettings { get; set; }

        public string error_message { get; set; }
    }
}