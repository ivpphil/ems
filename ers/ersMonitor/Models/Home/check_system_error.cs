﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using ersMonitor.Models.Home.settings;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.model;
using jp.co.ivp.ers.util;
using ersMonitor.Domain.Home.Mappables;

namespace ersMonitor.Models.Home
{
    public class check_system_error
        : ErsModelBase, ICheckSystemErrorMappable
    {
        public string message { get; set; }
        public List<Dictionary<string, object>> listResult { get; set; }

        public check_system_errorSettingsModel modelSettings { get; set; }
    }
}