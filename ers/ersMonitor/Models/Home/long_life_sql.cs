﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersMonitor.Domain.Home.Mappables;

namespace ersMonitor.Models.Home
{
    public class long_life_sql
        : ErsModelBase, ILongLifeSqlMappable
    {
        public string error_message { get; set; }
    }
}