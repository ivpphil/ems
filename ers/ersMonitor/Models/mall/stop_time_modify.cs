﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using ersMonitor.Domain.mall.Commands;
using ersMonitor.Domain.mall.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersMonitor.Models.mall
{
    /// <summary>
    /// Model
    /// </summary>
    public class stop_time_modify
        : ErsModelBase, IStopTimeModifyMappable, IStopTimeModifyCommand
    {
        /// <summary>
        /// ID [ID]
        /// </summary>
        [ErsSchemaValidation("mall_stop_time_t.id")]
        public virtual int? id { get; set; }

        /// <summary>
        /// 停止開始日時 [Stop start datetime]
        /// </summary>
        [ErsSchemaValidation("mall_stop_time_t.stop_from")]
        public virtual DateTime? stop_from { get; set; }

        /// <summary>
        /// 停止終了日時 [Stop finish datetime]
        /// </summary>
        [ErsSchemaValidation("mall_stop_time_t.stop_to")]
        public virtual DateTime? stop_to { get; set; }

        /// <summary>
        /// モール店舗区分 [Mall shop type]
        /// </summary>
        [ErsSchemaValidation("mall_stop_time_t.mall_shop_kbn")]
        public virtual EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// モール機能タイプ [Mall function type]
        /// </summary>
        [ErsSchemaValidation("mall_stop_time_t.mall_func_type")]
        public virtual EnumMallFuncType? mall_func_type { get; set; }


        /// <summary>
        /// 登録ボタン [Register button]
        /// </summary>
        [HtmlSubmitButton]
        public virtual bool register_btn { get; set; }

        /// <summary>
        /// 修正ボタン [Modify button]
        /// </summary>
        [HtmlSubmitButton]
        public virtual bool modify_btn { get; set; }

        /// <summary>
        /// 削除ボタン [Delete button]
        /// </summary>
        [HtmlSubmitButton]
        public virtual bool delete_btn { get; set; }


        /// <summary>
        /// データリスト [List of data]
        /// </summary>
        public virtual IList<Dictionary<string, object>> listStopTime { get; set; }

        /// <summary>
        /// モール店舗区分リスト [List of mall shop type]
        /// </summary>
        public virtual IList<Dictionary<string, object>> listMallShopKbn
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MallShopKbn, EnumCommonNameColumnName.namename, null, null, false);
            }
            set
            {
            }
        }

        /// <summary>
        /// モール機能タイプリスト [List of mall function type]
        /// </summary>
        public virtual IList<Dictionary<string, object>> listMallFuncType
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.MallFuncType, EnumCommonNameColumnName.namename);
            }
            set
            {
            }
        }
    }
}