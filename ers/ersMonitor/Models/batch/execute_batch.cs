﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersMonitor.Domain.batch.Mappables;
using ersMonitor.Domain.batch.Commands;
using System.ComponentModel;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;

namespace ersMonitor.Models.batch
{
    public class execute_batch
        : ErsModelBase, IExecuteBatchMappable, IExecuteBatchCommand
    {
        [ErsUniversalValidation(type = CHK_TYPE.OneByteCharacter)]
        public string batch_id { get; set; }
      
        public List<Dictionary<string, object>> batchLogsList { get; set; }
        public List<Dictionary<string, object>> batchList { get; set; }



    }
}