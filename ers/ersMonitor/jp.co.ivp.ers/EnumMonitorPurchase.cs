﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jp.co.ivp.ers
{
    public enum EnumMonitorPurchase
    {
        /// <summary>
        /// 初回購入
        /// </summary>
        First = 0,

        /// <summary>
        /// 会員購入
        /// </summary>
        Second
    }
}