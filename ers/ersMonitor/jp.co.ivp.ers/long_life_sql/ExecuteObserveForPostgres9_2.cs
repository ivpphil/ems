﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersMonitor.Models.Home.settings;
using jp.co.ivp.ers.db;

namespace ersMonitor.jp.co.ivp.ers.long_life_sql
{
    public class ExecuteObserveForPostgres9_2
        : ExecuteObserveForPostgresBase
    {
        /// <summary>
        /// PostgreSQL 9.2以降用の監視
        /// </summary>
        /// <param name="objDB"></param>
        /// <param name="modelSettings"></param>
        /// <param name="listMessage"></param>
        /// <param name="connectionSetting"></param>
        public override void ExecuteForPg(ErsDB_universal objDB, long_life_sqlSettingsModel modelSettings, List<string> listMessage, ConnectionSettings connectionSetting)
        {
            string strSQL = string.Format("SELECT * FROM pg_stat_activity WHERE datname = '{0}' AND state <> 'idle'", objDB.DatabaseName);

            List<Dictionary<string, object>> listDic = objDB.ExecuteQuery(strSQL);

            foreach (var data in listDic)
            {
                // Check ignore application
                if (modelSettings.ignore_applications != null && data["application_name"] != null)
                {
                    if (this.IsIgnoreApplication(modelSettings.ignore_applications, data["application_name"].ToString()))
                    {
                        continue;
                    }
                }
                // Check ignore query
                if (modelSettings.ignore_queries != null && data["query"] != null)
                {
                    if (this.IsIgnoreQuery(modelSettings.ignore_queries, data["query"].ToString()))
                    {
                        continue;
                    }
                }

                if (data["query_start"] == null)
                {
                    continue;
                }

                DateTime startDate;

                if (DateTime.TryParse(data["query_start"].ToString(), out startDate))
                {
                    // Check time
                    if (this.CheckDiffTime(modelSettings.elapsed_minutes, startDate))
                    {
                        string strMessage = string.Format("[{0}]\r\n", connectionSetting.connection_name)
                                            + string.Format("{0}分以上実行中のSQLが検出されました。\r\n", modelSettings.elapsed_minutes)
                                            + string.Format("SQL = {0}\r\n", data["query"])
                                            + string.Format("application_name = {0}\r\n", data["application_name"])
                                            + string.Format("client_addr = {0}\r\n", data["client_addr"])
                                            + string.Format("pid = {0}\r\n", data["pid"])
                                            + string.Format("datid = {0}", data["datid"]);

                        listMessage.Add(strMessage);
                    }
                }
            }
        }
    }
}