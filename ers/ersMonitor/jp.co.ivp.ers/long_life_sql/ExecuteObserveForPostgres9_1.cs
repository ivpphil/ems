﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using ersMonitor.Models.Home.settings;

namespace ersMonitor.jp.co.ivp.ers.long_life_sql
{
    public class ExecuteObserveForPostgres9_1
         : ExecuteObserveForPostgresBase
    {
        /// <summary>
        /// PostgreSQL 9.1以前用の監視
        /// </summary>
        /// <param name="objDB"></param>
        /// <param name="modelSettings"></param>
        /// <param name="listMessage"></param>
        /// <param name="connectionSetting"></param>
        public override void ExecuteForPg(ErsDB_universal objDB, long_life_sqlSettingsModel modelSettings, List<string> listMessage, ConnectionSettings connectionSetting)
        {
            string strSQL;
            //version 9.1以下
            strSQL = string.Format(
                    "SELECT main.*, activity_t.application_name, activity_t.client_addr FROM "
                    + "("
                    + "SELECT "
                    + "backendid, "
                    + "pg_stat_get_backend_pid(backendid_t.backendid) AS procpid, "
                    + "pg_stat_get_backend_activity_start(backendid_t.backendid) AS start, "
                    + "pg_stat_get_backend_activity(backendid_t.backendid) AS current_query "
                    + "FROM (SELECT pg_stat_get_backend_idset() AS backendid) AS backendid_t"
                    + ") AS main "
                    + "INNER JOIN pg_stat_activity AS activity_t ON activity_t.procpid = main.procpid "
                    + "WHERE main.current_query <> '<IDLE>' "
                    + "AND activity_t.datname = '{0}'",
                    connectionSetting.target_db_name);

            List<Dictionary<string, object>> listDic = objDB.ExecuteQuery(strSQL);

            foreach (var data in listDic)
            {
                // Check ignore application
                if (modelSettings.ignore_applications != null && data["application_name"] != null)
                {
                    if (this.IsIgnoreApplication(modelSettings.ignore_applications, data["application_name"].ToString()))
                    {
                        continue;
                    }
                }
                // Check ignore query
                if (modelSettings.ignore_queries != null && data["current_query"] != null)
                {
                    if (this.IsIgnoreQuery(modelSettings.ignore_queries, data["current_query"].ToString()))
                    {
                        continue;
                    }
                }

                if (data["start"] == null)
                {
                    continue;
                }

                DateTime startDate;

                if (DateTime.TryParse(data["start"].ToString(), out startDate))
                {
                    // Check time
                    if (this.CheckDiffTime(modelSettings.elapsed_minutes, startDate))
                    {
                        string strMessage = string.Format("[{0}]\r\n", connectionSetting.connection_name)
                                            + string.Format("{0}分以上実行中のSQLが検出されました。\r\n", modelSettings.elapsed_minutes)
                                            + string.Format("SQL = {0}\r\n", data["current_query"])
                                            + string.Format("application_name = {0}\r\n", data["application_name"])
                                            + string.Format("client_addr = {0}\r\n", data["client_addr"])
                                            + string.Format("procpid = {0}\r\n", data["procpid"])
                                            + string.Format("backendid = {0}\r\n", data["backendid"]);

                        listMessage.Add(strMessage);
                    }
                }
            }
        }
    }
}