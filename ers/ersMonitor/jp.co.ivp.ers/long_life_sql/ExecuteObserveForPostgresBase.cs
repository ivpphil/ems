﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.db;
using ersMonitor.Models.Home.settings;
using System.Text.RegularExpressions;

namespace ersMonitor.jp.co.ivp.ers.long_life_sql
{
    public abstract class ExecuteObserveForPostgresBase
    {
        public abstract void ExecuteForPg(ErsDB_universal objDB, long_life_sqlSettingsModel modelSettings, List<string> listMessage, ConnectionSettings connectionSetting);

        /// <summary>
        /// Postgresのバージョンを取得する。
        /// </summary>
        /// <param name="objDB"></param>
        /// <returns></returns>
        public static ExecuteObserveForPostgresBase GetPostgresVersion(ErsDB_universal objDB)
        {
            string strVersionSQL = "SELECT version()";

            string strVersion = Convert.ToString(objDB.ExecuteQuery(strVersionSQL).First()["version"]);
            var match = Regex.Match(strVersion, "PostgreSQL (\\d+.\\d+).\\d+");
            var version = Double.Parse(match.Result("$1"));
            if (version < 9.2)
            {
                return new ExecuteObserveForPostgres9_1();
            }
            else
            {
                return new ExecuteObserveForPostgres9_2();
            }
        }

        /// <summary>
        /// IsIgnoreApplication
        /// </summary>
        /// <param name="ignore_applications"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        protected bool IsIgnoreApplication(string[] ignore_applications, string application)
        {
            string applicationUpper = application.ToUpper();

            foreach (var ignore_application in ignore_applications)
            {
                if (applicationUpper.StartsWith(ignore_application.ToUpper()))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// IsIgnoreQuery
        /// </summary>
        /// <param name="ignore_queries"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        protected bool IsIgnoreQuery(string[] ignore_queries, string query)
        {
            string queryUpper = query.ToUpper();

            foreach (var ignore_query in ignore_queries)
            {
                if (queryUpper.StartsWith(ignore_query.ToUpper()))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// CheckDiffTime
        /// </summary>
        /// <param name="elapsed_minutes"></param>
        /// <param name="start"></param>
        /// <returns></returns>
        protected bool CheckDiffTime(int elapsed_minutes, DateTime start)
        {
            if (DateTime.Now.AddMinutes(-elapsed_minutes) >= start)
            {
                return true;
            }

            return false;
        }
    }
}