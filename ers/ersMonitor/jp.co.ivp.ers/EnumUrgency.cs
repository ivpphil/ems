﻿
namespace jp.co.ivp.ers
{
    /// <summary>
    /// 緊急度
    /// </summary>
    public enum EnumUrgency
    {
        /// <summary>
        /// 低
        /// </summary>
        Low = 0,

        /// <summary>
        /// 高
        /// </summary>
        High = 1,
    }
}
