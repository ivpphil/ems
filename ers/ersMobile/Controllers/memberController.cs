﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using System.ComponentModel.DataAnnotations;
using ersMobile.Models.member;
using ersMobile.Models;
using ersMobile.Domain.Common.Commands;
using ersMobile.Domain.Common.Mappables;
using jp.co.ivp.ers;

namespace ersMobile.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsAuthorization]
    [ErsSideMenu]
    public class memberController
        : ers.Controllers.memberController
    {
        #region member2
        [NonAction]
        public override ActionResult member2(ers.Models.Member member) { throw new NotImplementedException(); }

        [HttpPost]
        public virtual ActionResult member2(Member member)
        {
            if (!member.zip_flg1)
            {
                return base.member2(member);
            }

            //郵便番号検索
            ModelState.AddModelErrors(commandBus.Validate<IZipSearchCommand>(member), member);

            this.mapperBus.Map<IZipSearchMappable>(member);

            return this.member1(member, EnumEck.Error);
        }
        #endregion

        #region address2
        [NonAction]
        public override ActionResult address2(ers.Models.AddressInfo address) { throw new NotImplementedException(); }

        [HttpPost]
        public virtual ActionResult address2(AddressInfo address)
        {
            if (!address.zip_flg2)
            {
                return base.address2(address);
            }

            //郵便番号検索
            ModelState.AddModelErrors(commandBus.Validate<IAnotherZipSearchCommand>(address), address);

            this.mapperBus.Map<IAnotherZipSearchMappable>(address);

            return this.address1(address, EnumEck.Error);
        }
        #endregion
    }
}
