﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ers.Models.ResizeImage;
using jp.co.ivp.ers.mvc;

namespace ersMobile.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [ErsSideMenu]
    public class ResizeImageController : ers.Controllers.ResizeImageController
    {
    }
}
