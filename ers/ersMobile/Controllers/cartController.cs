﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using ersMobile.Models.cart;

namespace ersMobile.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsSideMenu]
    public class cartController
        : ers.Controllers.cartController
    {
        [NonAction]
        public override ActionResult cart(ers.Models.Cart cart, ers.Models.List list) { throw new NotImplementedException(); }

        public virtual ActionResult cart(Cart cart, ers.Models.List list)
        {
            return base.cart(cart, list);
        }
    }
}
