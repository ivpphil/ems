﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using ersMobile.Models;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersMobile.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class DetailController
        : ers.Controllers.DetailController
    {
        #region detail

        [NonAction]
        public override ActionResult detail(ers.Models.Detail detail, ers.Models.List list, EnumEck? eck = null)
        {
            return this.detail((Detail)detail, list, eck);
        }

        public virtual ActionResult detail(Detail detail, ers.Models.List list, EnumEck? eck = null)
        {
            return base.detail(detail, list, eck);
        }
        #endregion

        #region cart
        [NonAction]
        public override ActionResult cart(ers.Models.Cart cart, ers.Models.List list)
        {
            return base.cart(cart, list);
        }

        public virtual ActionResult cart(ersMobile.Models.cart.Cart cart, ers.Models.List list)
        {
            return base.cart(cart, list);
        }
        #endregion

        protected override ers.Models.Detail GetDetailModel()
        {
            return this.GetBindedModel<Detail>();
        }
    }
}
