using System.Web.Mvc;
using System.Reflection;
using jp.co.ivp.ers.mvc.CommandProcessor.Dispatcher;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.member;

namespace ersMobile
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            //command-handler定義 同名対応の為、フルパスで記載し、usingは使わない

            //Common
            container.RegisterType<IValidationHandler<ersMobile.Domain.Common.Commands.IZipSearchCommand>, ersMobile.Domain.Common.Handlers.ValidateZipSearch>();
            container.RegisterType<IMapper<ersMobile.Domain.Common.Mappables.IZipSearchMappable>, ersMobile.Domain.Common.Mappers.ZipSearchMapper>();
            container.RegisterType<IValidationHandler<ersMobile.Domain.Common.Commands.IAnotherZipSearchCommand>, ersMobile.Domain.Common.Handlers.ValidateAnotherZipSearch>();
            container.RegisterType<IMapper<ersMobile.Domain.Common.Mappables.IAnotherZipSearchMappable>, ersMobile.Domain.Common.Mappers.AnotherZipSearchMapper>();

            //Api
            container.RegisterType<IValidationHandler<ers.Domain.Api.Commands.IGetZipCommand>, ers.Domain.Api.Handlers.ValidateGetZip>();
            container.RegisterType<IMapper<ers.Domain.Api.Mappables.IGetZipMappable>, ers.Domain.Api.Mappers.GetZipMapper>();

            //Cart
            container.RegisterType<IValidationHandler<ers.Domain.Cart.Commands.ICartCommand>, ers.Domain.Cart.Handlers.ValidateCart>();
            container.RegisterType<ICommandHandler<ers.Domain.Cart.Commands.ICartCommand>, ers.Domain.Cart.Handlers.CartHandler>();
            container.RegisterType<IMapper<ers.Domain.Cart.Mappables.ICartMappable>, ersMobile.Domain.Cart.Mappers.CartMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Cart.Commands.ICartBasketRecordCommand>, ers.Domain.Cart.Handlers.ValidateCartBasketRecord>();
            container.RegisterType<IValidationHandler<ers.Domain.Cart.Commands.ICartRegularBasketRecordCommand>, ers.Domain.Cart.Handlers.ValidateCartRegularBasketRecord>();
            container.RegisterType<IMapper<ers.Domain.Cart.Mappables.ICartBasketRecordMappable>, ers.Domain.Cart.Mappers.CartBasketRecordMapper>();
            container.RegisterType<IMapper<ers.Domain.Cart.Mappables.ICartRegularBasketRecordMappable>, ers.Domain.Cart.Mappers.CartRegularBasketRecordMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Cart.Commands.IWishlistCommand>, ers.Domain.Cart.Handlers.ValidateWishlist>();
            container.RegisterType<ICommandHandler<ers.Domain.Cart.Commands.IWishlistCommand>, ers.Domain.Cart.Handlers.WishlistHandler>();
            container.RegisterType<IMapper<ers.Domain.Cart.Mappables.IWishlistMappable>, ers.Domain.Cart.Mappers.WishlistMapper>();

            container.RegisterType<ICommandHandler<ers.Domain.Cart.Commands.ICartForcedRefleshCommand>, ers.Domain.Cart.Handlers.CartForcedRefleshHandler>();

            //Cms
            container.RegisterType<IMapper<ers.Domain.Cms.Mappables.INewsListMappable>, ers.Domain.Cms.Mappers.NewsListMapper>();
            container.RegisterType<IValidationHandler<ers.Domain.Cms.Commands.INewsDetailCommand>, ers.Domain.Cms.Handlers.ValidateNewsDetail>();
            container.RegisterType<IMapper<ers.Domain.Cms.Mappables.INewsDetailMappable>, ers.Domain.Cms.Mappers.NewsDetailMapper>();
            container.RegisterType<IValidationHandler<ers.Domain.Cms.Commands.INewsDetailPreviewCommand>, ers.Domain.Cms.Handlers.ValidateNewsPreviewDetail>();
            container.RegisterType<IMapper<ers.Domain.Cms.Mappables.INewsDetailPreviewMappable>, ers.Domain.Cms.Mappers.NewsDetailPreviewMapper>();

            //Detail
            container.RegisterType<IValidationHandler<ers.Domain.Detail.Commands.IMerchandiseDetailCommand>, ers.Domain.Detail.Handlers.ValidateMerchandiseDetail>();
            container.RegisterType<IMapper<ers.Domain.Detail.Mappables.IMerchandiseDetailMappable>, ers.Domain.Detail.Mappers.MerchandiseDetailMapper>();
            container.RegisterType<IMapper<ers.Domain.Detail.Mappables.IMerchandiseRecommendMappable>, ers.Domain.Detail.Mappers.MerchandiseRecommendMapper>();
            container.RegisterType<IValidationHandler<ers.Domain.Detail.Commands.IDetailViewHistoryCommand>, ers.Domain.Detail.Handlers.ValidateDetailViewHistory>();
            container.RegisterType<IMapper<ers.Domain.Detail.Mappables.IDetailViewHistoryMappable>, ers.Domain.Detail.Mappers.DetailViewHistoryMapper>();

            //Home
            container.RegisterType<IMapper<ers.Domain.Home.Mappables.INewsMappable>, ers.Domain.Home.Mappers.NewsListMapper>();
            container.RegisterType<IMapper<ers.Domain.Home.Mappables.IStaticPathMappable>, ers.Domain.Home.Mappers.StaticPathMapper>();

            //Images
            container.RegisterType<IValidationHandler<ers.Domain.Images.Commands.IStepSpacerCommand>, ers.Domain.Images.Handlers.ValidateStepSpacer>();
            container.RegisterType<ICommandHandler<ers.Domain.Images.Commands.IStepSpacerCommand>, ers.Domain.Images.Handlers.StepSpacerHandler>();

            //Login
            container.RegisterType<IValidationHandler<ers.Domain.Login.Commands.ILoginCommand>, ers.Domain.Login.Handlers.ValidateLogin>();
            container.RegisterType<ICommandHandler<ers.Domain.Login.Commands.ILoginCommand>, ers.Domain.Login.Handlers.LoginHandler>();

            //Member
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IMemberInfoMappable>, ers.Domain.Member.Mappers.MemberInfoMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IMemberUpdateCommand>, ers.Domain.Member.Handlers.ValidateMemberUpdate>();
            container.RegisterType<ICommandHandler<ers.Domain.Member.Commands.IMemberUpdateCommand>, ers.Domain.Member.Handlers.MemberUpdateHandler>();
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IMemberUpdateMappable>, ers.Domain.Member.Mappers.MemberUpdateMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IBillListCommand>, ers.Domain.Member.Handlers.ValidateBillList>();
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IBillListMappable>, ers.Domain.Member.Mappers.BillListMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IBillDetailCommand>, ers.Domain.Member.Handlers.ValidateBillDetail>();
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IBillDetailMappable>, ers.Domain.Member.Mappers.BillDetailMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IMailUpdateCommand>, ers.Domain.Member.Handlers.ValidateMailUpdate>();
            container.RegisterType<ICommandHandler<ers.Domain.Member.Commands.IMailUpdateCommand>, ers.Domain.Member.Handlers.MailUpdateHandler>();
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IMailUpdateMappable>, ers.Domain.Member.Mappers.MailUpdateMapper>();

            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IAddressListMappable>, ers.Domain.Member.Mappers.AddressListMapper>();
            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IAddressUpdateCommand>, ers.Domain.Member.Handlers.ValidateAddressUpdate>();
            container.RegisterType<ICommandHandler<ers.Domain.Member.Commands.IAddressUpdateCommand>, ers.Domain.Member.Handlers.AddressUpdateHandler>();
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IAddressUpdateMappable>, ers.Domain.Member.Mappers.AddressUpdateMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IAddAddressInsertCommand>, ers.Domain.Member.Handlers.ValidateAddAddressInsert>();
            container.RegisterType<ICommandHandler<ers.Domain.Member.Commands.IAddAddressInsertCommand>, ers.Domain.Member.Handlers.AddAddressInsertHandler>();
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IAddAddressInsertMappable>, ers.Domain.Member.Mappers.AddAddressInsertMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IAddressDeleteCommand>, ers.Domain.Member.Handlers.ValidateAddressDelete>();
            container.RegisterType<ICommandHandler<ers.Domain.Member.Commands.IAddressDeleteCommand>, ers.Domain.Member.Handlers.AddressDeleteHandler>();
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IAddressDeleteMappable>, ers.Domain.Member.Mappers.AddressDeleteMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IPointHistoryCommand>, ers.Domain.Member.Handlers.ValidatePointHistory>();
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IPointHistoryMappable>, ers.Domain.Member.Mappers.PointHistoryMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IAccountCloseCommand>, ers.Domain.Member.Handlers.ValidateAccountClose>();
            container.RegisterType<ICommandHandler<ers.Domain.Member.Commands.IAccountCloseCommand>, ers.Domain.Member.Handlers.AccountCloseHandler>();
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IAccountCloseMappable>, ers.Domain.Member.Mappers.AccountCloseMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IPassrimCommand>, ers.Domain.Member.Handlers.ValidatePassrim>();
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IPassrimMappable>, ers.Domain.Member.Mappers.PassrimMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IPasschangeCommand>, ers.Domain.Member.Handlers.ValidatePasschange>();
            container.RegisterType<ICommandHandler<ers.Domain.Member.Commands.IPasschangeCommand>, ers.Domain.Member.Handlers.PasschangeHandler>();
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IPasschangeMappable>, ers.Domain.Member.Mappers.PasschangeMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IMypageCardCommand>, ers.Domain.Member.Handlers.ValidateMypageCard>();
            container.RegisterType<IValidationHandler<ers.Domain.Member.Commands.IMypageCardRecordCommand>, ers.Domain.Member.Handlers.ValidateMypageCardRecord>();
            container.RegisterType<ICommandHandler<ers.Domain.Member.Commands.IMypageCardCommand>, ers.Domain.Member.Handlers.MypageCardHandler>();
            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IMypageCardMappable>, ers.Domain.Member.Mappers.MypageCardMapper>();

            container.RegisterType<IMapper<ers.Domain.Member.Mappables.IRegularBillMappable>, ers.Domain.Member.Mappers.RegularBillMapper>();

            //Quest
            container.RegisterType<IValidationHandler<ers.Domain.Quest.Commands.IQuestCommand>, ers.Domain.Quest.Handlers.ValidateQuest>();

            //Redirect
            container.RegisterType<IValidationHandler<ers.Domain.Redirect.Commands.IRedirectCommand>, ers.Domain.Redirect.Handlers.ValidateRedirect>();
            container.RegisterType<ICommandHandler<ers.Domain.Redirect.Commands.IRedirectCommand>, ers.Domain.Redirect.Handlers.RedirectHandler>();

            //Register
            container.RegisterType<IValidationHandler<ers.Domain.Register.Commands.IMemberEntryCommand>, ers.Domain.Register.Handlers.ValidateMemberEntry>();
            container.RegisterType<ICommandHandler<ers.Domain.Register.Commands.IMemberEntryCommand>, ersMobile.Domain.Register.Handlers.MemberRegistHandler>();

            container.RegisterType<IValidationHandler<ers.Domain.Register.Commands.IOrderRegistCommand>, ersMobile.Domain.Register.Handlers.ValidateOrderRegist>();
            container.RegisterType<ICommandHandler<ers.Domain.Register.Commands.IOrderRegistCommand>, ers.Domain.Register.Handlers.OrderRegistHandler>();
            container.RegisterType<IValidationHandler<ers.Domain.Register.Commands.IOrderMemberRegistCommand>, ers.Domain.Register.Handlers.ValidateOrderMemberRegist>();
            container.RegisterType<ICommandHandler<ers.Domain.Register.Commands.IOrderMemberRegistCommand>, ers.Domain.Register.Handlers.OrderMemberRegistHandler>();
            container.RegisterType<ICommandHandler<ers.Domain.Register.Commands.IOrderMemberCardDeleteCommand>, ers.Domain.Register.Handlers.OrderMemberCardDeleteHandler>();

            container.RegisterType<ICommandHandler<ers.Domain.Register.Commands.IRegisterCartCommand>, ers.Domain.Register.Handlers.RegisterCartHandler>();

            container.RegisterType<IMapper<ers.Domain.Register.Mappables.IOrderMappable>, ersMobile.Domain.Register.Mappers.OrderMapper>();
            container.RegisterType<IMapper<ers.Domain.Register.Mappables.IMemberOrderMappable>, ers.Domain.Register.Mappers.MemberOrderMapper>();

            container.RegisterType<IValidationHandler<ers.Domain.Register.Commands.IPaypalReturnCommand>, ers.Domain.Register.Handlers.ValidatePaypalReturn>();
            container.RegisterType<IMapper<ers.Domain.Register.Mappables.IPaypalReturnMappable>, ers.Domain.Register.Mappers.PaypalReturnMapper>();

            //ResizeImage
            container.RegisterType<IMapper<ers.Domain.ResizeImage.Mappables.IResizeImageMappable>, ers.Domain.ResizeImage.Mappers.ResizeImageMapper>();

            //Search
            container.RegisterType<IMapper<ers.Domain.Search.Mappables.IMerchandiseListMappable>, ers.Domain.Search.Mappers.MerchandiseListMapper>();
            container.RegisterType<IValidationHandler<ers.Domain.Search.Commands.IMerchandiseListCommand>, ers.Domain.Search.Handlers.ValidateMerchandiseList>();

            return container;
        }
    }
}