﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ers.Domain.Cart.Mappables;
using jp.co.ivp.ers.basket;
using ersMobile.Models.cart;

namespace ersMobile.Domain.Cart.Mappers
{
    public class CartMapper
        : ers.Domain.Cart.Mappers.CartMapper
    {
        protected override void SetBasketDataToCookie(ICartMappable objMappable)
        {
            //base.SetBasketDataToCookie();
            //don't write this data to cookie in model.
        }

        protected override void RefreshView(ICartMappable objMappable, ErsBasket basket)
        {
            objMappable.basketItems = this.LoadDefaultValue(objMappable, basket.objBasketRecord.Values);
            objMappable.regularBasketItems = this.LoadRegularDefaultValue(objMappable, basket.objRegularBasketRecord.Values);
        }
    }
}