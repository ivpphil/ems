﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc;

namespace ersMobile.Domain.Common.Mappables
{
    public interface IZipSearchMappable
         : IMappable, IErsModelBase
   {
       string zip { get; }

       string zip_search_error { set; }

       int? pref { set; }

       string address { set; }
   }
}