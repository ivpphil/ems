﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersMobile.Domain.Common.Commands;

namespace ersMobile.Domain.Common.Handlers
{
    public class ValidateAnotherZipSearch
        : IValidationHandler<IAnotherZipSearchCommand>
    {
        public virtual IEnumerable<ValidationResult> Validate(IAnotherZipSearchCommand command)
        {
            yield return command.CheckRequired("add_zip");
        }
    }
}