﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersMobile.Domain.Common.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersMobile.Domain.Common.Mappers
{
    public class AnotherZipSearchMapper
         : IMapper<IAnotherZipSearchMappable>
    {
        public void Map(IAnotherZipSearchMappable objMappable)
        {
            if (!objMappable.IsValid)
            {
                objMappable.add_zip_search_error = String.Join(Environment.NewLine, objMappable.GetAllErrorMessageList());
                objMappable.controller.ClearModelState(objMappable);
            }
            else
            {
                var zipService = ErsFactory.ersViewServiceFactory.GetErsViewZipService();
                zipService.Search(objMappable.add_zip);
                objMappable.add_zip_search_error = zipService.error_message;
                if (string.IsNullOrEmpty(zipService.error_message))
                {
                    objMappable.add_pref = zipService.pref;
                    objMappable.add_address = zipService.address;
                }
            }
        }
    }
}