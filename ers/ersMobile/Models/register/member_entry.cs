﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersMobile.Domain.Common.Mappables;
using jp.co.ivp.ers.mvc;
using ersMobile.Domain.Common.Commands;

namespace ersMobile.Models
{
    public class MemberEntry
        : ers.Models.MemberEntry, IZipSearchMappable, IZipSearchCommand
    {
        [HtmlSubmitButton]
        public bool zip_flg1 { get; set; }

        public string zip_search_error { get; set; }

    }
}