﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersMobile.Domain.Common.Commands;
using ersMobile.Domain.Common.Mappables;
using jp.co.ivp.ers.mvc;

namespace ersMobile.Models.member
{
    public class AddressInfo
        : ers.Models.AddressInfo, IAnotherZipSearchCommand, IAnotherZipSearchMappable
    {
        [HtmlSubmitButton]
        public bool zip_flg2 { get; set; }

        public string add_zip_search_error { get; set; }
    }
}