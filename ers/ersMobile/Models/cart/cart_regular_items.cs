﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersMobile.Models.cart
{
    public class Cart_regular_items
        : ers.Models.cart.Cart_regular_items
    {
        public virtual string disp_sname
        {
            get
            {
                if (string.IsNullOrEmpty(m_sname))
                {
                    return sname;
                }
                return m_sname;
            }
        }
    }
}