﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.validation;

namespace ersMobile.Models.cart
{
    public class Cart
        : ers.Models.Cart
    {
        /// <summary>
        /// 戻り先ＵＲＬ
        /// </summary>
        public virtual string current_url { get { return "top/cart/asp/cart.asp"; } }

        [ErsUniversalValidation]
        public override string del_key
        {
            get { return Convert.ToString(this.deleteButton); }
        }

        [ErsUniversalValidation]
        public override string del_regular_key
        {
            get { return Convert.ToString(this.deleteRegularButton); }
        }
    }
}