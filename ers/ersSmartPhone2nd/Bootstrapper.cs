using System.Web.Mvc;
using System.Reflection;
using jp.co.ivp.ers.mvc.CommandProcessor.Dispatcher;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.member;

namespace ersSmartPhone2nd
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            ers.Bootstrapper.Initialise();
        }
    }
}
