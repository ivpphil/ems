﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersSmartPhone2nd.Models
{
    /// <summary>
    /// スマホ用
    /// </summary>
    public class Register
        : ers.Models.Register
    {
        /// <summary>
        /// 環境依存のデータセット
        /// </summary>
        /// <returns></returns>
        public override EnumPmFlg pm_flg
        {
            get
            {
                return EnumPmFlg.SmartPhone;  //7:Smartphone
            }
        }
    }
}
