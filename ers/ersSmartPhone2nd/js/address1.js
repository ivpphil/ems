/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function(){
	var ersObj = Ersaddress1();
	ersObj.zipAutoedit();
	ersObj.zipPrevention();
});

/* ErsMember1オブジェクト生成コンストラクタ */
var Ersaddress1 = function () {

    var that = {};

    //formオブジェクト
    var objForm = $("form[name='entry_form']");

    /* 郵便番号から住所自動入力
    ---------------------------------------------------------------- */
    that.zipAutoedit = function () {
        var objAddress = {}; 	//郵便番号検索の引数設定オブジェクト

        //clickイベントをバインド
        $("#zip_flg2").click(function () {
            var thisId = $(this).attr("id");

            //引数の設定
            if (thisId === "zip_flg2") {
                //別お届け先用
                objAddress = {
                    "domZip": $("#add_zip"),
                    "domPref": $("#add_pref"),
                    "domAddress": $("#add_address"),
                    "domAddress2": $("#add_taddress"),
                    "domAddress3": $("#add_maddress"),
                    "zip_search_error": $("#add_zip_search_error")
                }
            }

            //郵便番号検索
            ErsLib.zipSearch(objAddress);

            return false;
        });

        $("#add_zip").change(function () {
            $("#add_zip_search_error").hide();
        });
    }

    /* Enterキー押下時の郵便番号検索画面への遷移防止
    ---------------------------------------------------------------- */
    that.zipPrevention = function () {
        objForm.prepend("<input type='submit' value='dummy' style='display:none;'>");
    }

    return that;
}
