﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise.category;
using System.Data.Common;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;


namespace ersSmartPhone2nd.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class SearchController
        : ers.Controllers.SearchController
    {

    }
}
