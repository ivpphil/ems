﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersSmartPhone2nd.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsAuthorization]
    [ErsSideMenu]
    public class memberController
        : ers.Controllers.memberController
    {
    }
}
