﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersSmartPhone2nd.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [RedirectSite]
    [ErsSideMenu]
    public class DetailController
        : ers.Controllers.DetailController
    {
        [NonAction]
        public override ActionResult daily_ranking(ers.Models.daily_ranking daily_ranking)
        {
            throw new NotImplementedException();
        }

        [NonAction]
        public override ActionResult viewHistory(ers.Models.view_history view_history)
        {
            throw new NotImplementedException();
        }
    }
}
