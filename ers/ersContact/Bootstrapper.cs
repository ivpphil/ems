using System.Web.Mvc;
using System.Reflection;
using jp.co.ivp.ers.mvc.CommandProcessor.Dispatcher;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.member;

namespace ersContact
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            //command-handler定義 同名対応の為、フルパスで記載し、usingは使わない
            //Api
            container.RegisterType<IValidationHandler<ersContact.Domain.Api.Commands.IGetZipCommand>, ersContact.Domain.Api.Handlers.ValidateGetZip>();
            container.RegisterType<IMapper<ersContact.Domain.Api.Mappables.IGetZipMappable>, ersContact.Domain.Api.Mappers.GetZipMapper>();

            container.RegisterType<IValidationHandler<ersContact.Domain.Api.Commands.IWithdrawMemberCommand>, ersContact.Domain.Api.Handlers.ValidateWithdrawMember>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Api.Commands.IWithdrawMemberCommand>, ersContact.Domain.Api.Handlers.WithdrawMemberHandler>();
       
            //Home
            container.RegisterType<ICommandHandler<ersContact.Domain.Information.Commands.IInfoReadInsertCommand>, ersContact.Domain.Information.Handlers.InfoReadInsertHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Information.Mappables.IInformationListMappable>, ersContact.Domain.Information.Mappers.InformationListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Information.Mappables.IUnReadInformationMappable>, ersContact.Domain.Information.Mappers.UnReadInformationMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Information.Mappables.ICheckInstructionListMappable>, ersContact.Domain.Information.Mappers.CheckInstructionListMapper>();

            //Search
            container.RegisterType<IValidationHandler<ersContact.Domain.Search.Commands.ISearchCommand>, ersContact.Domain.Search.Handlers.ValidateSearch>();
            container.RegisterType<IValidationHandler<ersContact.Domain.Search.Commands.ICtsOrderDeleteCommand>, ersContact.Domain.Search.Handlers.ValidateCtsOrderDelete>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Search.Commands.ICtsOrderDeleteCommand>, ersContact.Domain.Search.Handlers.CtsOrderDeleteHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Search.Mappables.ICampaignListMappable>, ersContact.Domain.Search.Mappers.CampaignListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Search.Mappables.IClientInfoListMappable>, ersContact.Domain.Search.Mappers.ClientInfoListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Search.Mappables.ICtsOrderListMappable>, ersContact.Domain.Search.Mappers.CtsOrderListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Search.Mappables.IInquiryListMappable>, ersContact.Domain.Search.Mappers.InquiryListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Search.Mappables.IOrderListMappable>, ersContact.Domain.Search.Mappers.OrderListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Search.Mappables.IRegularOrderListMappable>, ersContact.Domain.Search.Mappers.RegularOrderListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Search.Mappables.ISearchListMappable>, ersContact.Domain.Search.Mappers.SearchListMapper>();

            //Direction
            container.RegisterType<ICommandHandler<ersContact.Domain.Direction.Commands.IInstructionUpdateCommand>, ersContact.Domain.Direction.Handlers.InstructionUpdateHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Direction.Commands.IDirectionRegisterCommand>, ersContact.Domain.Direction.Handlers.DirectionRegisterHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Direction.Commands.ICardErrListUpdateCommand>, ersContact.Domain.Direction.Handlers.CardErrListUpdateHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Direction.Mappables.IDirectionListMappable>, ersContact.Domain.Direction.Mappers.DirectionListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Direction.Mappables.IInstructionListMappable>, ersContact.Domain.Direction.Mappers.InstructionListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Direction.Mappables.IInboundAllListMappable>, ersContact.Domain.Direction.Mappers.InboundAllListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Direction.Mappables.IInboundInstructionListMappable>, ersContact.Domain.Direction.Mappers.InboundInstructionListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Direction.Mappables.IInboundListMappable>, ersContact.Domain.Direction.Mappers.InboundListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Direction.Mappables.IInboundNowListMappable>, ersContact.Domain.Direction.Mappers.InboundNowListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Direction.Mappables.ICardErrListMappable>, ersContact.Domain.Direction.Mappers.CardErrListMapper>();

            //Cart
            container.RegisterType<IValidationHandler<ersContact.Domain.Cart.Commands.ICartCommand>, ersContact.Domain.Cart.Handlers.ValidateCart>();
            container.RegisterType<IValidationHandler<ersContact.Domain.Cart.Commands.ICartRecordCommand>, ersContact.Domain.Cart.Handlers.ValidateCartRecord>();
            container.RegisterType<IValidationHandler<ersContact.Domain.Cart.Commands.ICartRegularRecordCommand>, ersContact.Domain.Cart.Handlers.ValidateCartRegularRecord>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Cart.Commands.ICartCommand>, ersContact.Domain.Cart.Handlers.CartHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Cart.Mappables.ICartMappable>, ersContact.Domain.Cart.Mappers.CartMapper>();

            //Faq
            container.RegisterType<IValidationHandler<ersContact.Domain.Faq.Commands.IFaqRegisterCommand>, ersContact.Domain.Faq.Handlers.ValidateFaq>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Faq.Commands.IFaqRegisterCommand>, ersContact.Domain.Faq.Handlers.FaqRegisterHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Faq.Commands.IFaqUpdateCommand>, ersContact.Domain.Faq.Handlers.FaqUpdateHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Faq.Mappables.ICategoryListMappable>, ersContact.Domain.Faq.Mappers.CategoryListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Faq.Mappables.IFaqListMappable>, ersContact.Domain.Faq.Mappers.FaqListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Faq.Mappables.IRegisterListMappable>, ersContact.Domain.Faq.Mappers.RegisterListMapper>();

            //Information
            container.RegisterType<IValidationHandler<ersContact.Domain.Information.Commands.IInformationCommand>, ersContact.Domain.Information.Handlers.ValidateInformation>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Information.Commands.IInformationCommand>, ersContact.Domain.Information.Handlers.InformationHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Information.Mappables.IModifyInfoListMappable>, ersContact.Domain.Information.Mappers.ModifyInfoListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Information.Mappables.IUnReadInfoListMappable>, ersContact.Domain.Information.Mappers.UnReadInfoListMapper>();

            //Inqmember
            container.RegisterType<IValidationHandler<ersContact.Domain.Inqmember.Commands.IInqMemberCommand>, ersContact.Domain.Inqmember.Handlers.ValidateInqmember>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Inqmember.Commands.IInqMemberCommand>, ersContact.Domain.Inqmember.Handlers.InqMemberHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Inqmember.Mappables.IClientInfoMappable>, ersContact.Domain.Inqmember.Mappers.ClientInfoMapper>();

            //Inquiry
            container.RegisterType<IValidationHandler<ersContact.Domain.Inquiry.Commands.IInquiryCommand>, ersContact.Domain.Inquiry.Handlers.ValidateInquiry>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Inquiry.Commands.IEnquiryMailCommand>, ersContact.Domain.Inquiry.Handlers.EnquiryMailHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Inquiry.Commands.IInquiryCommand>, ersContact.Domain.Inquiry.Handlers.InquiryHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Inquiry.Mappables.ICaseInfoMappable>, ersContact.Domain.Inquiry.Mappers.CaseInfoMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Inquiry.Mappables.ICategoryLabelMappable>, ersContact.Domain.Inquiry.Mappers.CategoryLabelMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Inquiry.Mappables.ICategoryListMappable>, ersContact.Domain.Inquiry.Mappers.CategoryListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Inquiry.Mappables.IEscalateListMappable>, ersContact.Domain.Inquiry.Mappers.EscalateListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Inquiry.Mappables.IFromAddressMappable>, ersContact.Domain.Inquiry.Mappers.FromAddressMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Inquiry.Mappables.IHistoryListMappable>, ersContact.Domain.Inquiry.Mappers.HistoryListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Inquiry.Mappables.IIncMailListMappable>, ersContact.Domain.Inquiry.Mappers.IncMailListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Inquiry.Mappables.IInquiryListMappable>, ersContact.Domain.Inquiry.Mappers.InquiryListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Inquiry.Mappables.ILockByOtherMappable>, ersContact.Domain.Inquiry.Mappers.LockByOtherMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Inquiry.Mappables.IMailApproveMappable>, ersContact.Domain.Inquiry.Mappers.MailApproveMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Inquiry.Mappables.ISearchCaseListMappable>, ersContact.Domain.Inquiry.Mappers.SearchCaseListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Inquiry.Mappables.ISupportCountMappable>, ersContact.Domain.Inquiry.Mappers.SupportCountMapper>();

            //Login
            container.RegisterType<IValidationHandler<ersContact.Domain.Login.Commands.ILoginCommand>, ersContact.Domain.Login.Handlers.ValidateLogin>();
            container.RegisterType<IValidationHandler<ersContact.Domain.Login.Commands.ILoginAdminCommand>, ersContact.Domain.Login.Handlers.ValidateLoginAdmin>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Login.Commands.ILoginCommand>, ersContact.Domain.Login.Handlers.LoginHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Login.Mappables.IAgentMappable>, ersContact.Domain.Login.Mappers.AgentMapper>();

            //Merge
            container.RegisterType<IValidationHandler<ersContact.Domain.Merge.Commands.IMergeCommand>, ersContact.Domain.Merge.Handlers.ValidateMerge>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Merge.Commands.IMergeCommand>, ersContact.Domain.Merge.Handlers.MergeHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Merge.Mappables.IMergeMappable>, ersContact.Domain.Merge.Mappers.MergeMapper>();

            //Operator
            container.RegisterType<IValidationHandler<ersContact.Domain.Operator.Commands.IOperatorCommand>, ersContact.Domain.Operator.Handlers.ValidateOperator>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Operator.Commands.IOperatorCommand>, ersContact.Domain.Operator.Handlers.OperatorHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Operator.Mappables.IOperatorMappable>, ersContact.Domain.Operator.Mappers.OperatorMapper>();

            //Order
            container.RegisterType<IValidationHandler<ersContact.Domain.Order.Commands.IOrderCommand>, ersContact.Domain.Order.Handlers.ValidateOrder>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Order.Commands.ICtsOrderCommand>, ersContact.Domain.Order.Handlers.CtsOrderHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Order.Commands.IDeliveryCommand>, ersContact.Domain.Order.Handlers.DeliveryHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Order.Commands.IMemberCommand>, ersContact.Domain.Order.Handlers.MemberHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Order.Mappables.IMemberMappable>, ersContact.Domain.Order.Mappers.MemberMapper>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Order.Commands.IOrderCommand>, ersContact.Domain.Order.Handlers.OrderHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Order.Commands.IPaymentCommand>, ersContact.Domain.Order.Handlers.PaymentHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Order.Commands.IShippingCommand>, ersContact.Domain.Order.Handlers.ShippingHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Order.Mappables.IDeliveryMappable>, ersContact.Domain.Order.Mappers.DeliveryMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Order.Mappables.IOrderMappable>, ersContact.Domain.Order.Mappers.OrderMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Order.Mappables.IPaymentMappable>, ersContact.Domain.Order.Mappers.PaymentMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Order.Mappables.IPointMappable>, ersContact.Domain.Order.Mappers.PointMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Order.Mappables.IReCalcMappable>, ersContact.Domain.Order.Mappers.ReCalcMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Order.Mappables.IRegularMappable>, ersContact.Domain.Order.Mappers.RegularMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Order.Mappables.IShippingMappable>, ersContact.Domain.Order.Mappers.ShippingMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Order.Mappables.IOrderRecomputeMappable>, ersContact.Domain.Order.Mappers.OrderRecomputeMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Order.Mappables.IDelivMethodDisplayListMappable>, ersContact.Domain.Order.Mappers.DelivMethodDisplayListMapper>();

            //Product
            container.RegisterType<IValidationHandler<ersContact.Domain.Product.Commands.IWishListRegistCommand>, ersContact.Domain.Product.Handlers.ValidateWishListRegist>();
            container.RegisterType<IMapper<ersContact.Domain.Product.Mappables.IProductMappable>, ersContact.Domain.Product.Mappers.ProductMapper>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Product.Commands.IWishListRegistCommand>, ersContact.Domain.Product.Handlers.WishListRegistHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Product.Mappables.IWishListMappable>, ersContact.Domain.Product.Mappers.WishListMapper>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Product.Commands.IWishListDeleteCommand>, ersContact.Domain.Product.Handlers.WishListDeleteHandler>();

            //Regular
            container.RegisterType<IValidationHandler<ersContact.Domain.Regular.Commands.IOrderCommand>, ersContact.Domain.Regular.Handlers.ValidateRegular>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Regular.Commands.IMemberCommand>, ersContact.Domain.Regular.Handlers.MemberHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Regular.Commands.IOrderCommand>, ersContact.Domain.Regular.Handlers.OrderHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Regular.Commands.IPaymentCommand>, ersContact.Domain.Regular.Handlers.PaymentHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Regular.Commands.IShippingCommand>, ersContact.Domain.Regular.Handlers.ShippingHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Regular.Commands.ISkipOrderCommand>, ersContact.Domain.Regular.Handlers.SkipOrderHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Regular.Mappables.IMemberMappable>, ersContact.Domain.Regular.Mappers.MemberMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Regular.Mappables.IOrderDetailMappable>, ersContact.Domain.Regular.Mappers.OrderDetailMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Regular.Mappables.IOrderMappable>, ersContact.Domain.Regular.Mappers.OrderMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Regular.Mappables.IPaymentMappable>, ersContact.Domain.Regular.Mappers.PaymentMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Regular.Mappables.IShippingMappable>, ersContact.Domain.Regular.Mappers.ShippingMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Regular.Mappables.IRegularCalcMappable>, ersContact.Domain.Regular.Mappers.RegularCalcMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Regular.Mappables.IProductMappable>, ersContact.Domain.Regular.Mappers.ProductMapper>();
            //Reports
            container.RegisterType<IValidationHandler<ersContact.Domain.Reports.Commands.IReportsCommand>, ersContact.Domain.Reports.Handlers.ValidateReports>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.ICallListMappable>, ersContact.Domain.Reports.Mappers.CallListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.ICampaignMappable>, ersContact.Domain.Reports.Mappers.CampaignMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.ICategoryLabelMappable>, ersContact.Domain.Reports.Mappers.CategoryLabelMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IProductAgeMappable>, ersContact.Domain.Reports.Mappers.ProductAgeMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IProductSalesMappable>, ersContact.Domain.Reports.Mappers.ProductSalesMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IRepAgeMappable>, ersContact.Domain.Reports.Mappers.RepAgeMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IRepCategoryDetailsMappable>, ersContact.Domain.Reports.Mappers.RepCategoryDetailsMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IRepCategoryListMappable>, ersContact.Domain.Reports.Mappers.RepCategoryListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IRepCategoryMappable>, ersContact.Domain.Reports.Mappers.RepCategoryMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IRepContactLogDetailMappable>, ersContact.Domain.Reports.Mappers.RepContactLogDetailMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IRepContactLogMappable>, ersContact.Domain.Reports.Mappers.RepContactLogMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IRepOrderMappable>, ersContact.Domain.Reports.Mappers.RepOrderMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IRepProdMappable>, ersContact.Domain.Reports.Mappers.RepProdMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IRepSalesMappable>, ersContact.Domain.Reports.Mappers.RepSalesMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.ICallCsvMappable>, ersContact.Domain.Reports.Mappers.CallCsvMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.ICategoryCsvMappable>, ersContact.Domain.Reports.Mappers.CategoryCsvMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IContactLogsCsvMappable>, ersContact.Domain.Reports.Mappers.ContactLogsCsvMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IOrderCsvMappable>, ersContact.Domain.Reports.Mappers.OrderCsvMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.IProductCsvMappable>, ersContact.Domain.Reports.Mappers.ProductCsvMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Reports.Mappables.ISalesCsvMappable>, ersContact.Domain.Reports.Mappers.SalesCsvMapper>();

            //Configuration
            container.RegisterType<IValidationHandler<ersContact.Domain.Configuration.Commands.ICtsFaqUpdateCommand>, ersContact.Domain.Configuration.Handlers.ValidateCtsFaqCSV>();
            container.RegisterType<IValidationHandler<ersContact.Domain.Configuration.Commands.ICtsFaqCSVRecordCommand>, ersContact.Domain.Configuration.Handlers.ValidateCtsFaqCSVRecord>();
            container.RegisterType<IValidationHandler<ersContact.Domain.Configuration.Commands.ICtsEmailListUpdateCommand>, ersContact.Domain.Configuration.Handlers.ValidateCtsEmailFrom>();
            container.RegisterType<IValidationHandler<ersContact.Domain.Configuration.Commands.ICtsEscalationUpdateCommand>, ersContact.Domain.Configuration.Handlers.ValidateCtsEscalation>();
            container.RegisterType<IValidationHandler<ersContact.Domain.Configuration.Commands.ICtsCategoryUpdateCommand>, ersContact.Domain.Configuration.Handlers.ValidateCategory>();
            container.RegisterType<IValidationHandler<ersContact.Domain.Configuration.Commands.ICtsCategoryUpdateRecordCommand>, ersContact.Domain.Configuration.Handlers.ValidateCategoryRecord>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Configuration.Commands.ICtsFaqUpdateCommand>, ersContact.Domain.Configuration.Handlers.CtsFaqUpdateHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Configuration.Commands.ICtsEmailListUpdateCommand>, ersContact.Domain.Configuration.Handlers.CtsEmailListUpdateHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Configuration.Commands.ICtsEscalationUpdateCommand>, ersContact.Domain.Configuration.Handlers.CtsEscalationUpdateHandler>();
            container.RegisterType<ICommandHandler<ersContact.Domain.Configuration.Commands.ICtsCategoryUpdateCommand>, ersContact.Domain.Configuration.Handlers.CtsCategoryUpdateHandler>();
            container.RegisterType<IMapper<ersContact.Domain.Configuration.Mappables.ICtsFaqCSVDownMappable>, ersContact.Domain.Configuration.Mappers.CtsFaqCSVDownMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Configuration.Mappables.ICtsEmailFromListMappable>, ersContact.Domain.Configuration.Mappers.CtsEmailFromListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Configuration.Mappables.ICtsEmailFromDetailMappable>, ersContact.Domain.Configuration.Mappers.CtsEmailFromDetailMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Configuration.Mappables.ICtsEscalationListMappable>, ersContact.Domain.Configuration.Mappers.CtsEscalationListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Configuration.Mappables.ICtsEscalationDetailMappable>, ersContact.Domain.Configuration.Mappers.CtsEscalationDetailMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Configuration.Mappables.ICtsCategoryListMappable>, ersContact.Domain.Configuration.Mappers.CtsCategoryListMapper>();
            container.RegisterType<IMapper<ersContact.Domain.Configuration.Mappables.ICtsCategoryMappable>, ersContact.Domain.Configuration.Mappers.CtsCategoryMapper>();

            return container;
        }
    }
}