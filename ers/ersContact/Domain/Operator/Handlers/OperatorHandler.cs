﻿using ersContact.Domain.Operator.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.cts_operators;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Operator.Handlers
{
    public class OperatorHandler : ICommandHandler<IOperatorCommand>
    {
        public ICommandResult Submit(IOperatorCommand command)
        {
            if (command.regist && command.done)
            {
                this.Insert(command);
            }
            else if (command.modify && command.done)
            {
                this.Update(command);
            }
            else if (command.delete)
            {
                this.Delete(command);
                command.done = true;
            }

            return new CommandResult(true);
        }

        internal void Insert(IOperatorCommand command) //(string authority)
        {
            var repository = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorRepository();
            var Operator = new ErsCtsOperator();
            Operator.OverwriteWithModel(command);
            if (command.authority == CtsAuthorityType.SUPERVISOR.ToString())
            {
                //スーパバイザーは常に管理者
                Operator.ag_type = EnumAgType.Admin;
            }
            Operator.active = (int)EnumActive.Active;
            Operator.site_id = (int)EnumSiteId.COMMON_SITE_ID;
            repository.Insert(Operator);
        }

        internal void Update(IOperatorCommand command)
        {
            var repository = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorRepository();
            var Operator = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(command.id.Value);
            if (Operator == null)
            {
                throw new ErsException("29002");
            }
            Operator.OverwriteWithModel(command);
            if (command.authority == CtsAuthorityType.SUPERVISOR.ToString())
            {
                //スーパバイザーは常に管理者
                Operator.ag_type = EnumAgType.Admin;
            }

            var old_Operator = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(command.id.Value);

            repository.Update(old_Operator, Operator);
        }

        internal void Delete(IOperatorCommand command)
        {
            var repository = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorRepository();
            var newOpt = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(command.id.Value);
            if (newOpt == null)
            {
                throw new ErsException("29002");
            }
            if (command.authority == CtsAuthorityType.SUPERVISOR.ToString())
            {
                //スーパバイザーは常に管理者
                newOpt.ag_type = EnumAgType.Admin;
            }
            newOpt.active = (int)EnumActive.NonActive;

            var oldOpt = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(command.id.Value);

            repository.Update(oldOpt, newOpt);
        }
    }
}
