﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Operator.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using jp.co.ivp.ers.cts_operators;

namespace ersContact.Domain.Operator.Handlers
{
    public class ValidateOperator
           : IValidationHandler<IOperatorCommand>
    {
        public IEnumerable<ValidationResult> Validate(IOperatorCommand command)
        {
            if ((command.modify && command.done) || command.delete)
            {
                yield return command.CheckRequired("id");
            }
            if ((command.regist || command.modify) && command.done)
            {
                yield return command.CheckRequired("ag_name");
                yield return command.CheckRequired("user_id");
                yield return command.CheckRequired("passwd");
                yield return ErsFactory.ersCtsOperatorFactory.GetCheckDuplicateOperatorStgy().CheckDuplicate(command.id, command.user_id);
                if (command.authority != CtsAuthorityType.SUPERVISOR.ToString())
                {
                    yield return command.CheckRequired("ag_type");
                    yield return ErsFactory.ersCtsOperatorFactory.GetCheckExistAgentTypeStgy().ValidAgentType(command.ag_type);
                }
            }
        }
    }
}