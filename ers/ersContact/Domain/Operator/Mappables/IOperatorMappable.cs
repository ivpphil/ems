﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersContact.Domain.Operator.Mappables
{
    public interface IOperatorMappable
        : IMappable, IErsModelBase
    {
        string authority { get; set; }
        string PageLabel { set; }
        string ag_name { set; }
        string user_id { set; }
        string passwd { set; }
        EnumAgType? ag_type { set; }
        int? id { get; set; }
        long search_result_cnt { get; set; }
        List<Dictionary<string, object>> operatorList { set; }
    }
}
