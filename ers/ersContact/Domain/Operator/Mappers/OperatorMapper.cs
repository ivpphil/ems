﻿using ersContact.Domain.Operator.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.cts_operators;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Operator.Mappers
{
    public class OperatorMapper
        : IMapper<IOperatorMappable>
    {
        public void Map(IOperatorMappable objMappable)
        {
            this.SetPageLabel(objMappable);

            this.LoadList(objMappable);

            this.LoadDetail(objMappable);
        }

        private void SetPageLabel(IOperatorMappable objMappable)
        {
            if (objMappable.authority == CtsAuthorityType.SUPERVISOR)
            {
                objMappable.PageLabel = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.AuthorityType, EnumCommonNameColumnName.namename, (int?)EnumAuthorityType.SUPERVISOR);
            }
            else
            {
                objMappable.PageLabel = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.AuthorityType, EnumCommonNameColumnName.namename, (int?)EnumAuthorityType.OPERATOR);
            }
        }

        private void LoadList(IOperatorMappable objMappable)
        {
            var repository = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorRepository();
            var criteria = GetOperatorsCriteria(objMappable);

            objMappable.search_result_cnt = repository.GetRecordCount(criteria);
            if (objMappable.search_result_cnt != 0)
            {
                criteria.SetOrderByUserID(Criteria.OrderBy.ORDER_BY_ASC);

                var list = repository.FindOperator(criteria);

                objMappable.operatorList = ErsCommon.ConvertEntityListToDictionaryList(list);
            }
        }

        private void LoadDetail(IOperatorMappable objMappable)
        {
            if (objMappable.id > 0)
            {
                ErsCtsOperator operator1 = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(objMappable.id.Value);
                if (operator1 == null)
                {
                    throw new ErsException("29002");
                }
                objMappable.OverwriteWithParameter(operator1.GetPropertiesAsDictionary());
            }
            else
            {
                objMappable.id = null;
                objMappable.ag_name = "";
                objMappable.user_id = "";
                objMappable.passwd = "";
                objMappable.ag_type = null;
            }
        }

        private ErsCtsOperatorCriteria GetOperatorsCriteria(IOperatorMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorCriteria();
            criteria.authority = objMappable.authority;
            criteria.exclude_id = objMappable.id;
            criteria.active = EnumActive.Active;
            criteria.site_id = (int)EnumSiteId.COMMON_SITE_ID;
            return criteria;
        }
    }
}