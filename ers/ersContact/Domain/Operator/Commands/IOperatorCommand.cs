﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Operator.Commands
{
    public interface IOperatorCommand : ICommand
    {
        bool regist { get; }
        bool modify { get; }
        bool delete { get; }
        bool done { get; set; }
        EnumAgType? ag_type { get; set; }
        int? id { get; }
        string user_id { get; }
        string ag_name { get; }
        string passwd { get; }

        string authority { get; set; }
    }
}