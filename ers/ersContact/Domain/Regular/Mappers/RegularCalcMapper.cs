﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Regular.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersContact.Domain.Regular.Mappers
{
    public class RegularCalcMapper
        : IMapper<IRegularCalcMappable>
    {
        public void Map(IRegularCalcMappable objMappable)
        {
            objMappable.resultDate = this.CalculateDate(objMappable).ToString("yyyy/MM/dd");
        }

        public DateTime CalculateDate(IRegularCalcMappable objMappable)
        {
            if (objMappable.next_date == null)
            {
                throw new ErsException("10025", ErsResources.GetFieldName("next_date"));
            }

            var regularPatternService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(objMappable.send_ptn.Value);
            if (objMappable.weekend_operation == null)
            {
                return regularPatternService.CalculateBase(objMappable, objMappable.next_date.Value);
            }
            else
            {
                return regularPatternService.CalculateActual(objMappable, objMappable.next_date.Value, objMappable.weekend_operation.Value);
            }
        }
    }
}