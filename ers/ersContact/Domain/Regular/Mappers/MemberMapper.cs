﻿using ersContact.Domain.Regular.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersContact.Domain.Regular.Mappers
{
    public class MemberMapper
        : IMapper<IMemberMappable>
    {
        public void Map(IMemberMappable objMappable)
        {
            if (objMappable.mcode == "")
            {
                objMappable.mcode = ErsMember.DEFAUTL_MCODE;
            }
            objMappable.member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objMappable.mcode, true);

            if (objMappable.member == null)
            {
                objMappable.member_register = false;
                objMappable.member_modify = true;
                objMappable.member_cancel = false;
            }
            else
            {
                SetMemberInfo(objMappable);
                if (objMappable.member_modify)
                {
                    objMappable.member_cancel = false;
                    objMappable.member_modify = true;
                }
                else
                {
                    objMappable.member_cancel = true;
                    objMappable.member_modify = false;
                }
            }
        }

        private void SetMemberInfo(IMemberMappable objMappable)
        {
            objMappable.member_id = objMappable.member.id;
            objMappable.lname = objMappable.member.lname;
            objMappable.fname = objMappable.member.fname;
            objMappable.lnamek = objMappable.member.lnamek;
            objMappable.fnamek = objMappable.member.fnamek;
            objMappable.tel = objMappable.member.tel;
            objMappable.zip = objMappable.member.zip;
            objMappable.pref = objMappable.member.pref;
            objMappable.pref_name = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(objMappable.member.pref);
            objMappable.address = objMappable.member.address;
            objMappable.taddress = objMappable.member.taddress;
            objMappable.maddress = objMappable.member.maddress;
            objMappable.sex = objMappable.member.sex;
            objMappable.email = objMappable.member.email;
            objMappable.memo = objMappable.member.memo;
            var age = ErsFactory.ersMemberFactory.GetGetAgeStgy().GetAge(objMappable.member.birth);
            objMappable.age_code = ErsFactory.ersMemberFactory.GetGetAgeCodeStgy().GetAgeCode(age);
            if (objMappable.member.birth != null)
            {
                objMappable.birthday_y = objMappable.member.birth.Value.Year;
                objMappable.birthday_m = objMappable.member.birth.Value.Month;
                objMappable.birthday_d = objMappable.member.birth.Value.Day;
            }
            objMappable.dm_flg = objMappable.member.dm_flg ?? EnumDmFlg.NoNeed;
            objMappable.out_bound_flg = objMappable.member.out_bound_flg ?? EnumOutBoundFlg.NoNeed;
            objMappable.w_dm_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.NEED, EnumCommonNameColumnName.namename, (int?)objMappable.dm_flg);
            objMappable.w_out_bound_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.NEED, EnumCommonNameColumnName.namename, (int?)objMappable.out_bound_flg);
        }
    }
}