﻿using ersContact.Domain.Regular.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.order;

namespace ersContact.Domain.Regular.Mappers
{
    public class OrderDetailMapper
        : IMapper<IOrderDetailMappable>
    {
        public void Map(IOrderDetailMappable objMappable)
        {
            if (objMappable.d_id > 0)
            {
                var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
                criteria.id = objMappable.d_id;

                var detail = repository.Find(objMappable.orderList[0], criteria);

                if (detail.Count > 0)
                {
                    var det = detail[0];
                    objMappable.OverwriteWithParameter(det.GetPropertiesAsDictionary());
                    objMappable.status = this.GetStatus(det, objMappable);

                    if (objMappable.edit_running)
                    {
                        objMappable.ptn_interval_month_m = detail[0].ptn_interval_month;
                        objMappable.ptn_interval_month_w = detail[0].ptn_interval_month;

                        if (objMappable.card_id == null)
                        {
                            objMappable.card_id = objMappable.member_card_id;
                        }

                        if (objMappable.paymethod == null)
                            objMappable.paymethod = objMappable.pay;
                    }
                }
            }
        }

        private EnumStatus GetStatus(ErsRegularOrderRecord objDetail, IOrderDetailMappable objMappable)
        {
            if (objDetail.delete_date == null)
            {
                return EnumStatus.Running;
            }
            else if (objDetail.delete_date > objDetail.next_date)
            {
                return EnumStatus.Running;
            }
            else if (objDetail.delete_date <= objDetail.next_date)
            {
                return EnumStatus.Stopped;
            }
            else
            {
                return EnumStatus.Running;
            }
        }
    }
}