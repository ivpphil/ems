﻿using System.Collections.Generic;
using ersContact.Domain.Regular.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Regular.Mappers
{
    public class PaymentMapper
        : IMapper<IPaymentMappable>
    {
        public void Map(IPaymentMappable objMappable)
        {
            if (objMappable.d_id > 0)
            {
                var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
                criteria.id = objMappable.d_id;

                var detail = repository.Find(objMappable.orderList[0], criteria);

                if (detail.Count > 0 && objMappable.edit_running)
                {
                    this.CreatePaymentMethodView(objMappable);
                }
            }
        }

        public void CreatePaymentMethodView(IPaymentMappable objMappable)
        {
            if (objMappable.mcode == ErsMember.DEFAUTL_MCODE)
                return;

            if (objMappable.card_new && objMappable.card_modify)
            {
                LoadCardInfo(objMappable);
            }
            else if (objMappable.card_register)
            {
                objMappable.card_cancel = true;
                objMappable.paymethod = EnumPaymentType.CREDIT_CARD;
                this.LoadCards(objMappable);
            }
            else if (objMappable.card_delete)
            {
                objMappable.card_cancel = true;
                objMappable.paymethod = EnumPaymentType.CREDIT_CARD;
                this.LoadCards(objMappable);
            }
            else
            {
                objMappable.card_id = objMappable.card_id;
                if (!objMappable.card_cancel)
                {
                    if (!objMappable.paymethod.HasValue)
                        objMappable.paymethod = EnumPaymentType.CREDIT_CARD;
                }
                this.LoadCards(objMappable);
                objMappable.validity_m = 1;
                objMappable.card_cancel = true;
            }
        }

        private void ClearCard(IPaymentMappable objMappable)
        {
            objMappable.card_holder_name = "";
            objMappable.card_type = 0;
            objMappable.cardno = "";
            objMappable.validity_y = 0;
            objMappable.validity_m = 1;
            objMappable.securityno = "";
        }

        private void LoadCards(IPaymentMappable objMappable)
        {
            objMappable.member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objMappable.mcode, true);

            //Use this to get Card Information from the internet [setup.cs of Gmo]
            var g = new ErsGmoCard();
            var dic = g.ObtainMemberCardInfo(objMappable.member);
            objMappable.membercardList = ErsCommon.ConvertEntityListToDictionaryList(dic);

            if (objMappable.membercardList.Count > 0)
            {
                objMappable.membercardListExtend = new List<Dictionary<string, object>>();
                foreach (var clist in dic)
                {
                    var cinfo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD)).ObtainMemberCardInfo(objMappable.member, clist.card_id);
                    objMappable.membercardListExtend.Add(cinfo.GetPropertiesAsDictionary());

                }
            }
        }

        private void LoadCardInfo(IPaymentMappable objMappable)
        {
            objMappable.member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objMappable.mcode, true);
            //Use this to get Card Information from the internet [setup.cs of Gmo]
            var g = new ErsGmoCard();
            var dic = g.ObtainMemberCardInfo(objMappable.member);

            var updateCardInfo = dic.Find((objCardInfo) => objCardInfo.card_id == objMappable.card_id);
            if (updateCardInfo != null)
            {
                objMappable.card_type = updateCardInfo.card_type;
                objMappable.validity_m = updateCardInfo.validity_m;
                objMappable.validity_y = updateCardInfo.validity_y;
            }
        }
    }
}