﻿using System.Collections.Generic;
using ersContact.Domain.Regular.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.Payment;
using System;

namespace ersContact.Domain.Regular.Mappers
{
    public class ProductMapper
        : IMapper<IProductMappable>
    {
        public void Map(IProductMappable objMappable)
        {
            LoadProductDetails(objMappable);
        }

        private void LoadProductDetails(IProductMappable objMappable)
        {
            string disp_send_ptn = string.Empty;
            var groupRepository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var groupCriteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            groupCriteria.gcode = objMappable.gcode;
            var listGroup = groupRepository.Find(groupCriteria);
            if (listGroup.Count > 0)
            {
                disp_send_ptn = listGroup[0].disp_send_ptn.PadLeft(3, '0');

            }
            objMappable.disp_send_ptn_month_intervals = Convert.ToInt32(disp_send_ptn.Substring(0, 1));
            objMappable.disp_send_ptn_week_intervals = Convert.ToInt32(disp_send_ptn.Substring(1, 1));
            objMappable.disp_send_ptn_month_day_intervals = Convert.ToInt32(disp_send_ptn.Substring(2, 1));
        }
    }
}