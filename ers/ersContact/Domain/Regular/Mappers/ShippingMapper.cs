﻿using System;
using ersContact.Domain.Regular.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;

namespace ersContact.Domain.Regular.Mappers
{
    public class ShippingMapper
        : IMapper<IShippingMappable>
    {
        public void Map(IShippingMappable objMappable)
        {
            if (objMappable.shipping_new)
            {
                this.ClearShipping(objMappable);
                objMappable.shipping_cancel = false;
            }
            else if (objMappable.shipping_modify)
            {
                objMappable.shipping_id = objMappable.modify_shipping_id;
                this.LoadShippingDetail(objMappable);
                objMappable.shipping_cancel = false;
            }
            else if (objMappable.shipping_delete)
            {
                this.LoadShippingAddress(objMappable);
                objMappable.shipping_cancel = true;
                objMappable.shipping_modify = false;
            }
            else if (objMappable.shipping_register)
            {
                this.LoadShippingAddress(objMappable);
                objMappable.shipping_cancel = true;
                objMappable.shipping_modify = false;
            }
            else
            {
                this.LoadShippingAddress(objMappable);
                objMappable.shipping_cancel = true;
                objMappable.shipping_modify = false;
            }
        }

        private void ClearShipping(IShippingMappable objMappable)
        {
            objMappable.shipping_id = 0;
            objMappable.address_name = "";
            objMappable.add_lname = "";
            objMappable.add_fname = "";
            objMappable.add_lnamek = "";
            objMappable.add_fnamek = "";
            objMappable.add_zip = "";
            objMappable.add_pref = 0;
            objMappable.add_address = "";
            objMappable.add_taddress = "";
            objMappable.add_maddress = "";
            objMappable.add_tel = "";
        }

        private void LoadShippingDetail(IShippingMappable objMappable)
        {
            int id = 0;
            if (objMappable.shipping_id != null && objMappable.shipping_id > 0)
            {
                id = Convert.ToInt32(objMappable.shipping_id.ToString());
            }

            ErsAddressInfo shipping = null;

            if (id > 0) 
                shipping = this.GetShippingData(id, objMappable.mcode);

            if (shipping == null)
            {
                this.SetShippingInfo(objMappable);
            }
            else
            {
                this.SetShippingInfo(shipping, objMappable);
            }
        }

        internal ErsAddressInfo GetShippingData(int id,string mcode)
        {
            ErsAddressInfoRepository repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            ErsAddressInfoCriteria addCri = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

            //検索条件をクライテリアに保存
            addCri.id = id;
            addCri.mcode = mcode;

            var list = repository.Find(addCri);

            return list[0];
        }

        private void SetShippingInfo(IShippingMappable objMappable)
        {
            objMappable.shipping_id = 0;
            objMappable.address_name = objMappable.member.lname + " " + objMappable.member.fname;
            objMappable.add_lname = objMappable.member.lname;
            objMappable.add_fname = objMappable.member.fname;
            objMappable.add_lnamek = objMappable.member.lnamek;
            objMappable.add_fnamek = objMappable.member.fnamek;
            objMappable.add_zip = objMappable.member.zip;
            objMappable.add_pref = objMappable.member.pref;
            objMappable.add_address = objMappable.member.address;
            objMappable.add_taddress = objMappable.member.taddress;
            objMappable.add_maddress = objMappable.member.maddress;
            objMappable.add_tel = objMappable.tel;
        }

        private void SetShippingInfo(ErsAddressInfo shipping, IShippingMappable objMappable)
        {
            objMappable.address_name = shipping.address_name;
            objMappable.add_lname = shipping.add_lname;
            objMappable.add_fname = shipping.add_fname;
            objMappable.add_lnamek = shipping.add_lnamek;
            objMappable.add_fnamek = shipping.add_fnamek;
            objMappable.add_zip = shipping.add_zip;
            objMappable.add_pref = shipping.add_pref;
            objMappable.add_address = shipping.add_address;
            objMappable.add_taddress = shipping.add_taddress;
            objMappable.add_maddress = shipping.add_maddress;
            objMappable.add_tel = shipping.add_tel;
        }

        private void LoadShippingAddress(IShippingMappable objMappable)
        {
            ErsAddressInfoRepository repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            ErsAddressInfoCriteria addCri = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

            addCri.mcode = objMappable.mcode;
            addCri.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);
            var list = repository.Find(addCri);

            var DicList = ErsCommon.ConvertEntityListToDictionaryList(list);
            foreach (var lst in DicList)
            {
                lst.Add("member_add_status", AddressStatus((int)lst["id"], objMappable.mcode));
            }
            objMappable.shippingList = DicList;
        }


        private int AddressStatus(int? member_add_id, string mcode)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderCriteria();
            criteria.mcode = mcode;
            criteria.member_add_id = member_add_id;
            criteria.SetExcludeCanceledRegularOrder();
            criteria.SetIntimeOrderBy(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.site_id = setup.site_id;
            var resultList = repository.Find(criteria);
            foreach (var regulerOrder in resultList)
            {
                foreach (ErsRegularOrderRecord regularOrderRecord in regulerOrder.regularOrderRecords)
                {
                    return 1;
                }
            }
            return 0;
        }
    }
}