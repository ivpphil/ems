﻿using System.Collections.Generic;
using ersContact.Domain.Regular.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.Payment;

namespace ersContact.Domain.Regular.Mappers
{
    public class OrderMapper
        : IMapper<IOrderMappable>
    {
        public void Map(IOrderMappable objMappable)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderCriteria();
            criteria.mcode = objMappable.mcode;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.SetIntimeOrderBy(Criteria.OrderBy.ORDER_BY_DESC);

            objMappable.orderList = repository.Find(criteria);


            this.LoadOrderDetails(objMappable);
        }

        private void LoadOrderDetails(IOrderMappable objMappable)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
            criteria.mcode = objMappable.mcode;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            switch (objMappable.all_status)
            {
                case 1:
                    criteria.SetActiveDetailsOnly();
                    break;
                case 2:
                    criteria.SetInactiveDetailsOnly();
                    break;
            }

            objMappable.recordCount = repository.GetRecordCount(criteria);

            criteria.SetSCodeOrderBy(Criteria.OrderBy.ORDER_BY_DESC);
            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            if (objMappable.recordCount > 0)
            {
                var listDetail = repository.Find(objMappable.orderList[0], criteria);
                var orderDetailsList = new List<Dictionary<string, object>>();
                foreach (var objDetail in listDetail)
                {
                    var dictionary = objDetail.GetPropertiesAsDictionary();
                    dictionary.Add("delivery_term", ErsFactory.ersViewServiceFactory.GetErsRegularOrderViewService().GetDeliveryTerm(objDetail));
                    
                    var status = this.GetStatus(objDetail, objMappable);
                    dictionary.Add("status", status);

                    dictionary.Add("status_name", ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Status, EnumCommonNameColumnName.namename, (int)status));
                    orderDetailsList.Add(dictionary);

                    //1-4-4)クレジットカード決済で、使用カードの「カード洗い替えステータス」が「エラー」の場合、メッセージ表示
                    if (status != EnumStatus.Stopped && objDetail.pay == EnumPaymentType.CREDIT_CARD)
                    {
                        var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                        var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
                        memberCardCriteria.active = EnumActive.Active;
                        memberCardCriteria.mcode = objDetail.mcode;
                        memberCardCriteria.id = objDetail.member_card_id;
                        var ersMemberCard = memberCardRepository.FindSingle(memberCardCriteria);
                        if (ersMemberCard == null || ersMemberCard.update_status == EnumCardUpdateStatus.Error)
                        {
                            dictionary.Add("card_status_error", true);
                        }
                    }
                }
                objMappable.orderDetailsList = orderDetailsList;
            }
        }

        private EnumStatus GetStatus(ErsRegularOrderRecord objDetail, IOrderMappable objMappable)
        {
            if (objDetail.delete_date == null)
            {
                return EnumStatus.Running;
            }
            else if (objDetail.delete_date > objDetail.next_date)
            {
                return EnumStatus.Running;
            }
            else if (objDetail.delete_date <= objDetail.next_date)
            {
                return EnumStatus.Stopped;
            }
            else
            {
                return EnumStatus.Running;
            }
        }
    }
}