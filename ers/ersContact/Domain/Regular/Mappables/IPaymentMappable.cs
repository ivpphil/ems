﻿using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.order;

namespace ersContact.Domain.Regular.Mappables
{
    public interface IPaymentMappable
        : IMappable
    {
        bool edit_running { get; }
        bool card_register { get; }
        bool card_delete { get; }
        bool card_new { get; }
        bool card_modify { get; }
        bool card_cancel { get; set; }
        int d_id { get; }
        int? validity_y { set; }
        int? validity_m { set; }
        int? card_type { set; }
        string mcode { get; set; }
        int? card_id { get; set; }
        string card_holder_name { set; }
        string cardno { set; }
        string securityno { set; }
        EnumPaymentType? paymethod { get; set; }
        ErsMember member { get; set; }
        List<Dictionary<string, object>> membercardList { get; set; }
        List<Dictionary<string, object>> membercardListExtend { get; set; }
        IList<ErsRegularOrder> orderList { get; }
    }
}
