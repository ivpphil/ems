﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.order;

namespace ersContact.Domain.Regular.Mappables
{
    public interface IOrderDetailMappable
        : IMappable, IErsModelBase
    {
        bool edit_running { get; }
        int d_id { get; }
        int? member_card_id { get; }
        short? ptn_interval_month_m { set; }
        short? ptn_interval_month_w { set; }
        int? card_id { get; set; }
        DateTime? delete_date { get; }
        EnumStatus? status { set; }
        EnumPaymentType? paymethod { get; set; }
        EnumPaymentType pay { get; }
        IList<ErsRegularOrder> orderList { get; }
    }
}
