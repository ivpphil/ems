﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.order;

namespace ersContact.Domain.Regular.Mappables
{
    public interface IOrderMappable
        : IMappable
    {
        int? all_status { get; }
        long recordCount { get; set; }
        string mcode { get; }
        int? card_id { set; }
        DateTime? delete_date { get; }
        ErsMember member { get; set; }
        ErsPagerModel pager { get; }
        IList<ErsRegularOrder> orderList { get; set; }
        List<Dictionary<string, object>> orderDetailsList { set; }
    }
}
