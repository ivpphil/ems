﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.order;

namespace ersContact.Domain.Regular.Mappables
{
    public interface IProductMappable
        : IMappable
    {
        string gcode { get; }

        int disp_send_ptn_month_intervals { get; set; }
        int disp_send_ptn_week_intervals { get; set; }
        int disp_send_ptn_month_day_intervals { get; set; }
    }
}
