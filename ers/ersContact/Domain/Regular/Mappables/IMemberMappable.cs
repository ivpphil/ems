﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Regular.Mappables
{
    public interface IMemberMappable
        : IMappable
    {
        bool member_register { set; }
        bool member_modify { get; set; }
        bool member_cancel { set; }
        int? member_id { set; }
        int? pref { set; }
        int? age_code { set; }
        int? birthday_y { set; }
        int? birthday_m { set; }
        int? birthday_d { set; }
        string mcode { get; set; }
        string lname { set; }
        string fname { set; }
        string lnamek { set; }
        string fnamek { set; }
        string tel { set; }
        string zip { set; }
        string pref_name { set; }
        string address { set; }
        string taddress { set; }
        string maddress { set; }
        string email { set; }
        string w_dm_flg { set; }
        string w_out_bound_flg { set; }
        string memo { set; }
        EnumSex? sex { set; }
        EnumDmFlg dm_flg { get; set; }
        EnumOutBoundFlg out_bound_flg { get; set; }
        ErsMember member { get; set; }
    }
}
