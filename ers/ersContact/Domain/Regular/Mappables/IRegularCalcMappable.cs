﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;

namespace ersContact.Domain.Regular.Mappables
{
    public interface IRegularCalcMappable
        : IMappable, IManageRegularPatternDatasource
    {
        EnumWeekendOperation? weekend_operation { get; set; }

        string resultDate { get; set; }
    }
}