﻿using System.Collections.Generic;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Regular.Mappables
{
    public interface IShippingMappable
        : IMappable
    {
        bool shipping_new { get; }
        bool shipping_modify { get; set; }
        bool shipping_delete { get; }
        bool shipping_register { get; }
        bool shipping_cancel { set; }
        int? shipping_id { get; set; }
        int? modify_shipping_id { get; }
        int? add_pref { set; }
        string address_name { set; }
        string add_lname { set; }
        string add_fname { set; }
        string add_lnamek { set; }
        string add_fnamek { set; }
        string add_zip { set; }
        string add_address { set; }
        string add_taddress { set; }
        string add_maddress { set; }
        string add_tel { set; }
        string tel { get; }
        string mcode { get; set; }
        ErsMember member { get; }
        List<Dictionary<string, object>> shippingList { set; }
    }
}
