﻿using System;
using ersContact.Domain.Regular.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;

namespace ersContact.Domain.Regular.Handlers
{
    public class OrderHandler : ICommandHandler<IOrderCommand>
    {
        public ICommandResult Submit(IOrderCommand command)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
            criteria.id = command.d_id;

            var det_old = repository.Find(command.orderList[0], criteria);
            var det = repository.Find(command.orderList[0], criteria);
            ErsRegularOrderRecord detail = null;
            ErsRegularOrderRecord old_detail = null;

            if (det.Count > 0)
            {
                old_detail = det_old[0];
                detail = det[0];


                // Update Rules, Destination and Update Payment Method
                if (command.to_modify)
                {
                    if (command.edit_status == 1)
                    {
                        detail.delete_date = null;
                    }
                    else
                    {
                        detail.delete_date = DateTime.Now;
                    }

                    if (command.next_date_base != null)
                    {
                        // Schedule
                        detail.next_date_base = command.next_date_base;
                        detail.next_sendtime_base = command.next_sendtime_base;
                        detail.next_date = command.next_date;
                        detail.next_sendtime = command.next_sendtime;
                        detail.next2_date_base = command.next2_date_base;
                        detail.next2_sendtime_base = command.next2_sendtime_base;
                        detail.next2_date = command.next2_date;
                        detail.next2_sendtime = command.next2_sendtime;
                        detail.next3_date_base = command.next3_date_base;
                        detail.next3_sendtime_base = command.next3_sendtime_base;
                        detail.next3_date = command.next3_date;
                        detail.next3_sendtime = command.next3_sendtime;
                        detail.next4_date_base = command.next4_date_base;
                        detail.next4_sendtime_base = command.next4_sendtime_base;
                        detail.next4_date = command.next4_date;
                        detail.next4_sendtime = command.next4_sendtime;
                        detail.next5_date_base = command.next5_date_base;
                        detail.next5_sendtime_base = command.next5_sendtime_base;
                        detail.next5_date = command.next5_date;
                        detail.next5_sendtime = command.next5_sendtime;

                        // Term Delivery
                        detail.send_ptn = command.send_ptn;
                        detail.ptn_interval_month = null;
                        detail.ptn_day = null;
                        detail.ptn_interval_week = null;
                        detail.ptn_weekday = null;
                        detail.ptn_interval_day = null;
                        switch (command.send_ptn)
                        {
                            case EnumSendPtn.MONTH_INTERVALS:
                                detail.ptn_interval_month = command.ptn_interval_month_m;
                                detail.ptn_day = command.ptn_day;
                                break;
                            case EnumSendPtn.WEEK_INTERVALS:
                                detail.ptn_interval_month = command.ptn_interval_month_w;
                                detail.ptn_interval_week = command.ptn_interval_week;
                                detail.ptn_weekday = command.ptn_weekday;
                                break;
                            case EnumSendPtn.DAY_INTERVALS:
                                detail.ptn_interval_day = command.ptn_interval_day;
                                break;
                        }
                        detail.weekend_operation = command.weekend_operation;

                        // Memo
                        detail.skip_date = command.skip_date;
                        detail.delete_date = command.delete_date;
                        detail.shipping_memo = command.shipping_memo;
                    }

                    detail.member_add_id = command.member_add_id;
                    detail.pay = command.pay;
                    if (command.pay == EnumPaymentType.CREDIT_CARD)
                    {
                        detail.member_card_id = command.card_id;
                    }
                    else
                    {
                        detail.member_card_id = null;
                    }
                    detail.conv_code = command.conv_code;
                }

                repository.Update(old_detail, detail);
            }
            return new CommandResult(true);
        }

        private void InsertMember(IMemberCommand command)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            ErsMember member = ErsFactory.ersMemberFactory.GetErsMember();

            member.OverwriteWithModel(command);
            member.email = "";
            member.passwd = "";
            member.ques = 0;

            repository.Insert(member, true);

            command.mcode = member.mcode;
        }

        private void UpdateMember(IMemberCommand command)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode, true);
            var mid = member.id;
            var time = member.intime;
            var email = member.email;

            member.OverwriteWithModel(command);
            member.intime = time;
            member.email = email;
            member.id = mid;

            var old_member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode, true);

            repository.Update(old_member, member);
        }
    }
}
