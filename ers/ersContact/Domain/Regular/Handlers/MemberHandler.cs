﻿using ersContact.Domain.Regular.Commands;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Regular.Handlers
{
    public class MemberHandler : ICommandHandler<IMemberCommand>
    {
        public ICommandResult Submit(IMemberCommand command)
        {
            if (command.mcode == "")
                command.mcode = ErsMember.DEFAUTL_MCODE;

            if (command.mcode == ErsMember.DEFAUTL_MCODE)
            {
                this.InsertMember(command);
            }
            else
            {
                this.UpdateMember(command);
            }

            return new CommandResult(true);
        }

        private void InsertMember(IMemberCommand command)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            ErsMember member = ErsFactory.ersMemberFactory.GetErsMember();

            member.OverwriteWithModel(command);
            //member.email = "";
            member.passwd = "";
            member.ques = 0;

            repository.Insert(member, true);

            command.mcode = member.mcode;
        }

        private void UpdateMember(IMemberCommand command)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode, true);
            var mid = member.id;
            var time = member.intime;
            //var email = member.email;

            member.OverwriteWithModel(command);
            member.dm_flg = command.dm_flg;
            member.out_bound_flg = command.out_bound_flg;

            member.intime = time;
            //member.email = email;
            member.id = mid;

            var old_member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode, true);
            member.site_id = old_member.site_id;
            repository.Update(old_member, member);
        }
    }
}
