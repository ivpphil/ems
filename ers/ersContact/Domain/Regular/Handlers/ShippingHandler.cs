﻿using System;
using ersContact.Domain.Regular.Commands;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Regular.Handlers
{
    public class ShippingHandler : ICommandHandler<IShippingCommand>
    {
        public ICommandResult Submit(IShippingCommand command)
        {
            if (command.shipping_delete)
            {
                command.shipping_id = command.modify_shipping_id;
                this.DeleteShipping(command);
            }
            else if (command.shipping_register)
            {
                this.SaveShipping(command);
            }

            return new CommandResult(true);
        }

        private void DeleteShipping(IShippingCommand command)
        {
            if (command.shipping_id > 0)
            {
                var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
                ErsAddressInfo address = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(Convert.ToInt32(command.shipping_id), command.mcode);

                repository.Delete(address);
            }
        }

        public void SaveShipping(IShippingCommand command)
        {
            if (command.shipping_id == null)
                command.shipping_id = 0;

            if (command.shipping_id == 0)
            {
                this.InsertShipping(command);
            }
            else
            {
                this.UpdateShipping(command);
            }
        }

        private void InsertShipping(IShippingCommand command)
        {
            var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            ErsAddressInfo address = ErsFactory.ersAddressInfoFactory.GetErsAddressInfo();

            address.OverwriteWithModel(command);
            address.id = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository().GetNextSequence();
            address.address_name = Convert.ToString(address.id);

            repository.Insert(address, false);

            command.shipping_id = address.id;
        }

        private void UpdateShipping(IShippingCommand command)
        {
            var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            ErsAddressInfo address = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(command.shipping_id, command.mcode);

            address.OverwriteWithModel(command);
            address.id = command.shipping_id;

            var old_address = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(command.shipping_id, command.mcode);

            repository.Update(old_address, address);
        }
    }
}
