﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Regular.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.util;
using System;

namespace ersContact.Domain.Regular.Handlers
{
    public class ValidateRegular
           : IValidationHandler<IOrderCommand>
    {
        public IEnumerable<ValidationResult> Validate(IOrderCommand command)
        {
            if (command.member_register)
            {
                yield return command.CheckRequired("lname");
                yield return command.CheckRequired("fname");
                yield return command.CheckRequired("lnamek");
                yield return command.CheckRequired("fnamek");
                yield return command.CheckRequired("tel");
                yield return ErsFactory.ersMemberFactory.GetCheckEmailRequiredStgy().Check(command.mcode, command.email);
                yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailStgy().CheckDuplicate(command.mcode, command.email);
                yield return command.CheckRequired("zip");
                yield return command.CheckRequired("pref");
                foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("pref", command.pref, false))
                {
                    yield return result;
                }
                if (command.IsValidField("zip", "pref"))
                {
                    yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("zip", command.zip, "pref", command.pref, false);
                }

                yield return command.CheckRequired("address");
                yield return command.CheckRequired("taddress");
                yield return command.CheckRequired("sex");
                yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("sex", EnumCommonNameType.Sex, (int?)command.sex);
            }

            if (command.shipping_register)
            {
                yield return command.CheckRequired("add_lname");
                yield return command.CheckRequired("add_fname");
                // yield return command.CheckRequired("add_lnamek"); //海外配送対応
                // yield return command.CheckRequired("add_fnamek"); //海外配送対応
                yield return command.CheckRequired("add_tel");
                yield return command.CheckRequired("add_zip");
                yield return command.CheckRequired("add_pref");
                foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("add_pref", command.add_pref, false))
                {
                    yield return result;
                }
                if (command.IsValidField("add_zip", "add_pref"))
                {
                    yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("add_zip", command.add_zip, "add_pref", command.add_pref, false);
                }

                yield return command.CheckRequired("add_address");
                yield return command.CheckRequired("add_taddress");
            }

            if (command.card_register || command.card_update)
            {
                yield return command.CheckRequired("card_type");
                yield return command.CheckRequired("cardno");
                yield return command.CheckRequired("validity_y");
                yield return command.CheckRequired("validity_m");
                yield return ErsFactory.ersOrderFactory.GetCheckValidityStgy().Check(command.validity_y, command.validity_m);
            }


            if (command.to_modify & !command.card_modify & !command.card_update)
            {
                if (command.next_date_base != null)
                {
                    yield return command.CheckRequired("next_date_base");
                    yield return command.CheckRequired("next2_date_base");
                    yield return command.CheckRequired("next3_date_base");
                    yield return command.CheckRequired("next4_date_base");
                    yield return command.CheckRequired("next5_date_base");
                    yield return command.CheckRequired("next_sendtime_base");
                    yield return command.CheckRequired("next2_sendtime_base");
                    yield return command.CheckRequired("next3_sendtime_base");
                    yield return command.CheckRequired("next4_sendtime_base");
                    yield return command.CheckRequired("next5_sendtime_base");
                    yield return command.CheckRequired("next_date");
                    yield return command.CheckRequired("next2_date");
                    yield return command.CheckRequired("next3_date");
                    yield return command.CheckRequired("next4_date");
                    yield return command.CheckRequired("next5_date");
                    yield return command.CheckRequired("next_sendtime");
                    yield return command.CheckRequired("next2_sendtime");
                    yield return command.CheckRequired("next3_sendtime");
                    yield return command.CheckRequired("next4_sendtime");
                    yield return command.CheckRequired("next5_sendtime");

                    var checkDateStgy = ErsFactory.ersCommonFactory.GetCheckDateFromToStgy();
                    foreach (var result in checkDateStgy.CheckDate("next_date_base", command.next_date_base, "next2_date_base", command.next2_date_base, false))
                    {
                        yield return result;
                    }
                    foreach (var result in checkDateStgy.CheckDate("next2_date_base", command.next2_date_base, "next3_date_base", command.next3_date_base, false))
                    {
                        yield return result;
                    }
                    foreach (var result in checkDateStgy.CheckDate("next3_date_base", command.next3_date_base, "next4_date_base", command.next4_date_base, false))
                    {
                        yield return result;
                    }
                    foreach (var result in checkDateStgy.CheckDate("next4_date_base", command.next4_date_base, "next5_date_base", command.next5_date_base, false))
                    {
                        yield return result;
                    }



                    foreach (var result in checkDateStgy.CheckDate(null, DateTime.Now.AddDays(ErsFactory.ersBatchFactory.getSetup().create_regular_order_days), "next_date", command.next_date))
                    {
                        yield return result;
                    }

                    foreach (var result in checkDateStgy.CheckDate("next_date", command.next_date, "next2_date", command.next2_date, false))
                    {
                        yield return result;
                    }
                    foreach (var result in checkDateStgy.CheckDate("next2_date", command.next2_date, "next3_date", command.next3_date, false))
                    {
                        yield return result;
                    }
                    foreach (var result in checkDateStgy.CheckDate("next3_date", command.next3_date, "next4_date", command.next4_date, false))
                    {
                        yield return result;
                    }
                    foreach (var result in checkDateStgy.CheckDate("next4_date", command.next4_date, "next5_date", command.next5_date, false))
                    {
                        yield return result;
                    }


                    switch (command.send_ptn)
                    {
                        case EnumSendPtn.MONTH_INTERVALS:
                            yield return command.CheckRequired("ptn_interval_month_m");
                            yield return command.CheckRequired("ptn_day");
                            break;
                        case EnumSendPtn.WEEK_INTERVALS:
                            yield return command.CheckRequired("ptn_interval_month_w");
                            yield return command.CheckRequired("ptn_interval_week");
                            yield return command.CheckRequired("ptn_weekday");
                            break;
                        case EnumSendPtn.DAY_INTERVALS:
                            yield return command.CheckRequired("ptn_interval_day");
                            break;
                    }
                    yield return command.CheckRequired("weekend_operation");
                }
                //

                var send = (command.member_add_id != 0 ? EnumSendTo.ANOTHER_ADDRESS : EnumSendTo.MEMBER_ADDRESS);
                var address_add = (command.member_add_id != 0 ? EnumAddressAdd.Add : EnumAddressAdd.NotAdd);

                //別住所でカード以外は不可
                yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().ValidateRecipientName(
                    "pay", command.pay, send,
                    command.lname, command.add_lname,
                    command.fname, command.add_fname,
                    command.lnamek, command.add_lnamek,
                    command.fnamek, command.add_fnamek
                 );

                yield return ErsFactory.ersOrderFactory.GetValidateSendStgy().ValidateAnotherAddressSave("address_add", send, address_add, command.member_add_id, ErsContext.sessionState.Get("ransu"));


                yield return command.CheckRequired("pay");
                yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().Validate("pay", command.pay, true);
                var shipping_pref = this.GetShippingPref(command);
                yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().ValidateOverseas("pay", command.pay, shipping_pref);
                if (command.pay == EnumPaymentType.NON_NEEDED_PAYMENT)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName("pay")), new[] { "pay" });
                }
                if (command.pay == EnumPaymentType.CREDIT_CARD)
                {
                    yield return command.CheckRequired("card_id");
                }

                //コンビニ用Validation
                if (command.pay == EnumPaymentType.CONVENIENCE_STORE)
                {
                    yield return command.CheckRequired("conv_code");
                    if (command.IsValidField("conv_code"))
                    {
                        yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("conv_code", EnumCommonNameType.ConvCode, (int?)command.conv_code);
                        var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode);
                        string email= "";
                        if (member != null)
                            email = member.email;
                        yield return ErsFactory.ersCtsOrderFactory.GetCheckMemberEmailExist().ValidateExistEmail(email, EnumPaymentType.CONVENIENCE_STORE);
                    }
                }
                yield return command.CheckRequired("member_add_id");
            }

            if (command.card_delete)
            {
                yield return ErsFactory.ersMemberFactory.GetCheckInOperationCardRegularOrder().CheckInOperationCard(command.mcode, command.card_id);
            }
        }

        /// <summary>
        /// 配送先都道府県を取得する
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private int? GetShippingPref(IOrderCommand command)
        {
            if (command.member_add_id == null)
            {
                return null;
            }

            if (command.member_add_id == 0)
            {
                var ersMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode);
                if (ersMember == null)
                {
                    return null;
                }
                return ersMember.pref;
            }

            var objAddress = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(command.member_add_id, command.mcode);
            if (objAddress == null)
            {
                return null;
            }

            return objAddress.add_pref;
        }
    }
}