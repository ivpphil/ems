﻿using ersContact.Domain.Regular.Commands;
using ersContact.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using System;
using jp.co.ivp.ers.order.strategy;

namespace ersContact.Domain.Regular.Handlers
{
    public class SkipOrderHandler : ICommandHandler<ISkipOrderCommand>
    {
        PutRegularDateForwardStgy putRegularDateForwardStgy = ErsFactory.ersOrderFactory.GetPutRegularDateForwardStgy();
        
        public ICommandResult Submit(ISkipOrderCommand command)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
            criteria.id = command.d_id;

            var det_old = repository.Find(command.orderList[0], criteria);
            var det = repository.Find(command.orderList[0], criteria);
            ErsRegularOrderRecord detail = null;
            ErsRegularOrderRecord old_detail = null;

            if (det.Count > 0)
            {
                old_detail = det_old[0];
                detail = det[0];

                putRegularDateForwardStgy.Execute(detail, false);

                repository.Update(old_detail, detail);
            }
            return new CommandResult(true);
        }

        private DateTime CalculateDate(IManageRegularPatternDatasource command, DateTime? next_date, EnumWeekendOperation? weekend_operation)
        {
            var regularPatternService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(command.send_ptn.Value);
            if (weekend_operation == null)
            {
                return regularPatternService.CalculateBase(command, next_date.Value);
            }
            else
            {
                return regularPatternService.CalculateActual(command, next_date.Value, weekend_operation.Value);
            }
        }
    }
}
