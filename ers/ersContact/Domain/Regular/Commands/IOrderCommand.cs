﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;

namespace ersContact.Domain.Regular.Commands
{
    public interface IOrderCommand : ICommand
    {
        bool to_modify { get; }
        bool member_register { get; }
        bool shipping_register { get; }
        bool card_register { get; }
        bool card_delete { get; set; }
        int d_id { get; }
        int edit_status { get; }
        int? next_sendtime_base { get; }
        int? next_sendtime { get; }
        int? next2_sendtime_base { get; }
        int? next2_sendtime { get; }
        int? next3_sendtime_base { get; }
        int? next3_sendtime { get; }
        int? next4_sendtime_base { get; }
        int? next4_sendtime { get; }
        int? next5_sendtime_base { get; }
        int? next5_sendtime { get; }
        int? member_add_id { get; }
        int? pref { get; }
        int? add_pref { get; }
        int? card_type { get; }
        int? validity_y { get; }
        int? validity_m { get; }
        short? ptn_day { get; }
        short? ptn_interval_month_m { get; }
        short? ptn_interval_month_w { get; }
        short? ptn_interval_week { get; }
        short? ptn_interval_day { get; }
        string shipping_memo { get; }
        int? card_id { get; }
        string lname { get; }
        string fname { get; }
        string lnamek { get; }
        string fnamek { get; }
        string tel { get; }
        string email { get; }
        string zip { get; }
        string address { get; }
        string taddress { get; }
        string add_lname { get; }
        string add_fname { get; }
        string add_lnamek { get; }
        string add_fnamek { get; }
        string add_tel { get; }
        string add_zip { get; }
        string add_address { get; }
        string add_taddress { get; }
        string cardno { get; }
        DateTime? next_date_base { get; }
        DateTime? next_date { get; }
        DateTime? next2_date_base { get; }
        DateTime? next2_date { get; }
        DateTime? next3_date_base { get; }
        DateTime? next3_date { get; }
        DateTime? next4_date_base { get; }
        DateTime? next4_date { get; }
        DateTime? next5_date_base { get; }
        DateTime? next5_date { get; }
        DateTime? skip_date { get; }
        DateTime? delete_date { get; }
        DayOfWeek? ptn_weekday { get; }
        EnumSendPtn? send_ptn { get; }
        EnumWeekendOperation? weekend_operation { get; }
        EnumPaymentType pay { get; }
        EnumSex? sex { get; }
        IList<ErsRegularOrder> orderList { get; }

        string mcode { get; set; }

        EnumConvCode? conv_code { get; set; }

        bool card_modify { get; set; }

        bool card_update { get; set; }
    }
}