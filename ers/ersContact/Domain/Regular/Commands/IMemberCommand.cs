﻿using System;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Regular.Commands
{
    public interface IMemberCommand : ICommand
    {
        int? pref { get; }
        int? age_code { get; }
        string mcode { get; set; }
        string lname { get; }
        string fname { get; }
        string lnamek { get; }
        string fnamek { get; }
        string tel { get; }
        string zip { get; }
        string address { get; }
        string taddress { get; }
        string maddress { get; }
        string email { get; }
        DateTime? birth { get; }
        EnumSex? sex { get; }
        EnumDmFlg dm_flg { get; }
        EnumOutBoundFlg out_bound_flg { get; }
    }
}