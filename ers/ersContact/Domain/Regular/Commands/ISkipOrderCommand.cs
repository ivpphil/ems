﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;
using System;

namespace ersContact.Domain.Regular.Commands
{
    public interface ISkipOrderCommand : ICommand
    {
        int d_id { get; }
        IList<ErsRegularOrder> orderList { get; }

        DateTime? last_date_base { get; set; }
        int? last_sendtime_base { get; set; }
    }
}