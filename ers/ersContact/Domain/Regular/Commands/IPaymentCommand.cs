﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;

namespace ersContact.Domain.Regular.Commands
{
    public interface IPaymentCommand : ICommand
    {
        bool edit_running { get; }
        bool card_update { get; }
        bool card_register { get; }
        bool card_delete { get; }
        bool card_will_add { get; }
        int d_id { get; }
        int? card_type { get; }
        int? validity_y { get; }
        int? validity_m { get; }
        string mcode { get; set; }
        int? card_id { get; set; }
        string card_holder_name { get; }
        string cardno { get; }
        string securityno { get; }
        IList<ErsRegularOrder> orderList { get; }
    }
}