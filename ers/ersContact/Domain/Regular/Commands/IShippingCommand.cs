﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Regular.Commands
{
    public interface IShippingCommand : ICommand
    {
        bool shipping_delete { get; }
        bool shipping_register { get; }
        int? shipping_id { get; set; }
        int? modify_shipping_id { get; }
        string address_name { set; }
        string add_lname { get; }
        string add_fname { get; }
        string mcode { get; }
    }
}