﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using ersContact.Domain.Search.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Search.Handlers
{
    public class ValidateSearch
           : IValidationHandler<ISearchCommand>
    {
        public IEnumerable<ValidationResult> Validate(ISearchCommand command)
        {
            if (!string.IsNullOrEmpty(command.zip1) && !string.IsNullOrEmpty(command.zip2))
            {

                if (command.zip1.Trim().Length < 3)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10212"));
                }

            }

            //for html tag validations**************/
            Regex tagRegex = new Regex(@"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>");
            if (!string.IsNullOrEmpty(command.address) && tagRegex.IsMatch(command.address))
            {
                yield return new ValidationResult(ErsResources.GetMessage("10038"));
            }

            if (!string.IsNullOrEmpty(command.fname) && tagRegex.IsMatch(command.fname))
            {
                yield return new ValidationResult(ErsResources.GetMessage("10038"));
            }

            if (!string.IsNullOrEmpty(command.lname) && tagRegex.IsMatch(command.lname))
            {
                yield return new ValidationResult(ErsResources.GetMessage("10038"));
            }
        }
    }
}