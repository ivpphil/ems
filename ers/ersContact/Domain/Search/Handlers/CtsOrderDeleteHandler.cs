﻿using System;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersContact.Domain.Search.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.ctsorder;
using jp.co.ivp.ers;

namespace ersContact.Domain.Search.Handlers
{
    public class CtsOrderDeleteHandler
        : ICommandHandler<ICtsOrderDeleteCommand>
    {
        public ICommandResult Submit(ICtsOrderDeleteCommand command)
        {
            var repository = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderRepository();
            ErsCtsOrder order = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderWithTempDNo(Convert.ToInt32(command.deleteOrder));
            order.status = 0;
            order.active = EnumActive.NonActive;
            order.charge3 = command.user_id;

            ErsCtsOrder oldorder = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderWithTempDNo(Convert.ToInt32(command.deleteOrder));

            repository.Update(oldorder, order);

            return new CommandResult(true);
        }
    }
}