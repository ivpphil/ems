﻿using ersContact.Domain.Search.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ersContact.Domain.Search.Handlers
{
    public class ValidateCtsOrderDelete : IValidationHandler<ICtsOrderDeleteCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICtsOrderDeleteCommand command)
        {
            yield return command.CheckRequired("deleteOrder");

            if (!string.IsNullOrEmpty(command.deleteOrder))
            {
                yield return CtsOrderExists(command.deleteOrder);
            }
        }

        private ValidationResult CtsOrderExists(string temp_d_no)
        {
            var criteria = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderCriteria();

            criteria.temp_d_no = temp_d_no;
            criteria.active = EnumActive.Active;

            if (ErsFactory.ersCtsOrderFactory.GetErsCtsOrderRepository().GetRecordCount(criteria) != 1)
            {
                return new ValidationResult(ErsResources.GetMessage("10200")); ;
            }

            return null;
        }
    }
}