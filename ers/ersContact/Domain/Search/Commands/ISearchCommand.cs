﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Search.Commands
{
    public interface ISearchCommand
        : ICommand
    {
        string zip1 { get; }
        string zip2 { get; }
        string tel { get; }
        string fax { get; }
        string address { get; }
        string fname { get; }
        string lname { get; }
    }
}