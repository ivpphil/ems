﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Search.Commands
{
    public interface ICtsOrderDeleteCommand
        : ICommand
    {
        string deleteOrder { get; }
        string user_id { get; }
    }
}