﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.member;

namespace ersContact.Domain.Search.Mappables
{
    public interface IClientInfoListMappable
        : IMappable
    {
        ErsMember objMember { get; set; }
        string mcode { get; set; }
        string w_age { get; set; }
        List<Dictionary<string, object>> shippingList { get; set; }
        long recordCount { get; set; }
        string d_no { get; set; }
        bool orderstatus { get; set; }
        int? add_pref { get; set; }
        string add_address { get; set; }
        string add_taddress { get; set; }
        string add_tel { get; set; }
        string add_zip { get; set; }
        string add_lname { get; set; }
        string add_fname { get; set; }
        string sendtime { get; set; }
        string w_etc { get; set; }
        int total { get; set; }
        int subtotal { get; set; }
        int carriage { get; set; }
        int tax { get; set; }
        int etc { get; set; }
        int p_service { get; set; }
        int coupon_discount { get; set; }
        int order_total_amount { get; set; }
        EnumPaymentType? pay { get; set; }
        int amounttotal { get; set; }
        CreditCardInfo savedCardInfo { get; set; }
        int prodCount { get; set; }
        List<Dictionary<string, object>> prodList { get; set; }
        int? age_code { get; set; }
        string w_sex { get; set; }

        string lname { get; set; }

        string fname { get; set; }

        string lnamek { get; set; }

        string fnamek { get; set; }

        string tel { get; set; }

        string pref_name { get; set; }

        string address { get; set; }

        string taddress { get; set; }

        string maddress { get; set; }

        string add_pref_name { get; set; }

        System.DateTime? senddate { get; set; }

        string zip { get; set; }

        EnumMallShopKbn? mall_shop_kbn { get; set; }

        string w_pay { get; set; }

        string site_name { get; set; }

        string mall_d_no { get; set; }

        int? site_id { get; }
    }
}
