﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersContact.Domain.Search.Mappables
{
    public interface IOrderListMappable
        : IMappable
    {
        List<Dictionary<string, object>> searchOrderList { get; set; }
        string mcode { get; set; }
        long recordCount { get; set; }
        long maxItemCount { get; }
        long pagerPageCount { get; set; }
        ErsPagerModel pager { get; }
        int? site_id { get; }
    }
}
