﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.order;

namespace ersContact.Domain.Search.Mappables
{
    public interface IRegularOrderListMappable
        : IMappable
    {
        IList<ErsRegularOrder> orderList { get; set; }
        List<Dictionary<string, object>> searchRegOrderList { get; set; }
        string mcode { get; set; }
        long recordCount { get; set; }
        long maxItemCount { get; }
        long pagerPageCount { get; set; }
        ErsPagerModel pager { get; }
    }
}
