﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using System;

namespace ersContact.Domain.Search.Mappables
{
    public interface ISearchListMappable
        : IMappable
    {
        bool TargetClient { get; set; }
        string d_no { get; set; }
        bool regordertype { get; set; }
        bool ctsordertype { get; set; }
        bool ordertype { get; set; }
        string mcode { get; set; }
        string lname { get; set; }
        string fname { get; set; }
        string lnamek { get; set; }
        string fnamek { get; set; }
        int? pref { get; set; }
        string address { get; set; }
        string email { get; set; }
        string tel { get; set; }
        string fax { get; set; }
        string zip { get; set; }
        string zip1 { get; set; }
        string zip2 { get; set; }
        string ccode { get; set; }
        int? campaign_id { get; set; }
        List<Dictionary<string, object>> searchClientList { get; set; }
        List<Dictionary<string, object>> searchOrderList { get; set; }
        List<Dictionary<string, object>> searchList { get; set; }
        long recordCount { get; set; }
        long maxItemCount { get; }
        ErsPagerModel pager { get; }
        long pagerPageCount { get; set; }

        EnumDeleted? src_deleted { get; set; }

        string mall_d_no { get; set; }

        DateTime? s_af_cancel_date_from { get; set; }

        DateTime? s_af_cancel_date_to { get; set; }

        DateTime? s_cancel_date_from { get; set; }

        DateTime? s_cancel_date_to { get; set; }
    }
}
