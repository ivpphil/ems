﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Search.Mappables
{
    public interface ICampaignListMappable
        : IMappable
    {
        List<Dictionary<string, object>> campaignList { get; set; }
    }
}
