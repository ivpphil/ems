﻿using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Search.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Search.Mappers
{
    public class InquiryListMapper
        : IMapper<IInquiryListMappable>
    {
        public void Map(IInquiryListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryCriteria();
            criteria.mcode = objMappable.mcode;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            objMappable.recordCount = repository.GetRecordCount(criteria);

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;	// 余りは切り上げ
            }
            objMappable.pagerPageCount = pagerPageCount;

            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            var list = repository.FindHeader(criteria);
            objMappable.inquiryList = ErsCommon.ConvertEntityListToDictionaryList(list);
        }
    }
}