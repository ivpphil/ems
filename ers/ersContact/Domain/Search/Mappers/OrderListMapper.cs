﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Search.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;

namespace ersContact.Domain.Search.Mappers
{
    public class OrderListMapper
        : IMapper<IOrderListMappable>
    {
        public void Map(IOrderListMappable objMappable)
        {
            objMappable.searchOrderList = this.LoadSearchOrderList(objMappable);
        }

        public List<Dictionary<string, object>> LoadSearchOrderList(IOrderListMappable objMappable)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.mcode = objMappable.mcode;
            criteria.site_id = objMappable.site_id;
            objMappable.recordCount = repository.GetRecordCount(criteria);

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;	// 余りは切り上げ
            }
            objMappable.pagerPageCount = pagerPageCount;

            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            var list = repository.Find(criteria);

            var listItem = new List<Dictionary<string, object>>();
            var obtainErsOrderStatusStgy = ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy();
            var viewOrderStatusService = ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService();

            foreach (ErsOrder order in list)
            {
                var orderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order);

                var dictionary = order.GetPropertiesAsDictionary();

                var tmpstat = obtainErsOrderStatusStgy.ObtainFrom(orderRecords.Values);
                var summary_order_status = viewOrderStatusService.GetStringFromId(tmpstat);
                dictionary["summary_order_status"] = summary_order_status;
                dictionary["pay_name"] = ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(order.pay);
                dictionary["orderRecords"] = orderRecords.Values;
                listItem.Add(dictionary);
            }

            return listItem;
        }
    }
}