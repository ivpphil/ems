﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Search.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;

namespace ersContact.Domain.Search.Mappers
{
    public class RegularOrderListMapper
        : IMapper<IRegularOrderListMappable>
    {
        public void Map(IRegularOrderListMappable objMappable)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderCriteria();
            criteria.mcode = objMappable.mcode;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.SetIntimeOrderBy(Criteria.OrderBy.ORDER_BY_DESC);

            objMappable.orderList = repository.Find(criteria);
            objMappable.searchRegOrderList = this.LoadSearchRegularOrderList(objMappable);
        }

        public List<Dictionary<string, object>> LoadSearchRegularOrderList(IRegularOrderListMappable objMappable)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
            criteria.mcode = objMappable.mcode;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            objMappable.recordCount = repository.GetRecordCount(criteria);
            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            { 
                pagerPageCount += 1;
            }

            objMappable.pagerPageCount = pagerPageCount;

            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            if (objMappable.recordCount > 0)
            {
                criteria.SetSCodeOrderBy(Criteria.OrderBy.ORDER_BY_DESC);

                var RegularOrderList = repository.Find(objMappable.orderList[0], criteria);
                var listItem = new List<Dictionary<string, object>>();
                foreach (var dr in RegularOrderList)
                {
                    var dictionary = dr.GetPropertiesAsDictionary();
                    dictionary["d_no"] = GetOrderDNo(dr.id, dr.regular_id);
                    dictionary["status"] = this.GetStatus(dr, objMappable);
                    dictionary["w_pay"] = ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(dr.pay);
                    dictionary["intervaldesc"] = ErsFactory.ersViewServiceFactory.GetErsRegularOrderViewService().GetDeliveryTerm(dr);
                    listItem.Add(dictionary);
                }

                return listItem;
            }
            else
            {
                return null;
            }
        }

        private EnumStatus GetStatus(ErsRegularOrderRecord objDetail, IRegularOrderListMappable objMappable)
        {
            if (objDetail.delete_date == null)
            {
                return EnumStatus.Running;
            }
            else if (objDetail.delete_date > objDetail.next_date)
            {
                return EnumStatus.Running;
            }
            else if (objDetail.delete_date <= objDetail.next_date)
            {
                return EnumStatus.Stopped;
            }
            else
            {
                return EnumStatus.Running;
            }
        }

        /// <summary>
        /// Gets ds_master_t's d_no from regular_detail_t's id.
        /// </summary>
        /// <param name="regular_detail_id"></param>
        /// <returns></returns>
        private string GetOrderDNo(int? id, int? regular_id)
        {
            if (id.HasValue && regular_id.HasValue)
            {
                var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();

                criteria.regular_detail_id = new int[] { id.Value };
                criteria.d_no_like = RegularOrderD_No(regular_id);

                var order = ErsFactory.ersOrderFactory.GetOrderSerachSpecification().GetSearchData(criteria);

                if (order.Count == 1)
                {
                    return order[0]["d_no"].ToString();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets regular_detail_t's d_no from regular_t.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private string RegularOrderD_No(int? id)
        {
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderCriteria();

            criteria.id = id;

            var regular = ErsFactory.ersOrderFactory.GetErsRegularOrderRepository().FindSingle(criteria);

            if (regular != null)
            {
                return regular.d_no;
            }
            else
            {
                return null;
            }
        }
    }
}