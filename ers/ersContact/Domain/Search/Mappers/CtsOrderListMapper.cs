﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Search.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Search.Mappers
{
    public class CtsOrderListMapper
        : IMapper<ICtsOrderListMappable>
    {
        public void Map(ICtsOrderListMappable objMappable)
        {
            objMappable.searchCtsOrderList = this.LoadSearchCtsOrderList(objMappable);
        }

        public List<Dictionary<string, object>> LoadSearchCtsOrderList(ICtsOrderListMappable objMappable)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var repository = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderRepository();
            var criteria = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderCriteria();
            criteria.mcode = objMappable.mcode;
            criteria.site_id = setup.site_id;
            objMappable.recordCount = repository.GetRecordCount(criteria);

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;	// 余りは切り上げ
            }
            objMappable.pagerPageCount = pagerPageCount;

            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            var list = repository.Find(criteria);
            var listItem = new List<Dictionary<string, object>>();
            foreach (var ctsOrder in list)
            {
                var dictionary = ctsOrder.GetPropertiesAsDictionary();
                
                var baskRepository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
                var baskCriteria = ErsFactory.ersBasketFactory.GetErsBaskRecordCriteria();
                baskCriteria.ransu = ctsOrder.ransu;
                baskCriteria.site_id = setup.site_id;
                var listBaskRecords = baskRepository.Find(baskCriteria);

                dictionary["hasBaskRecords"] = listBaskRecords.Count > 0;

                dictionary["baskRecords"] = listBaskRecords;

                listItem.Add(dictionary);
            }

            return listItem;
        }
    }
}