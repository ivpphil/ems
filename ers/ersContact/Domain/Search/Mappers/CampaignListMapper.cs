﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Search.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersContact.Domain.Search.Mappers
{
    public class CampaignListMapper
        : IMapper<ICampaignListMappable>
    {
        public void Map(ICampaignListMappable objMappable)
        {
            objMappable.campaignList = this.initCampaign();
        }

        public List<Dictionary<string, object>> initCampaign()
        {
            var repository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();
            var criteria = ErsFactory.ersDocBundleFactory.GetErsCampaignCriteria();
            criteria.active = EnumActive.Active;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            var list = repository.Find(criteria);

            var retList = new List<Dictionary<string, object>>();
            foreach (var dr in list)
            {
                var dictionary = new Dictionary<string, object>();
                dictionary["value"] = dr.id;
                dictionary["name"] = dr.campaign_name;
                retList.Add(dictionary);
            }
            return retList;
        }
    }
}