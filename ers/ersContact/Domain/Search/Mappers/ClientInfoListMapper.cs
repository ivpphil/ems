﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Search.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.search;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.mall;

namespace ersContact.Domain.Search.Mappers
{
    public class ClientInfoListMapper
        : IMapper<IClientInfoListMappable>
    {
        public void Map(IClientInfoListMappable objMappable)
        {
            if (objMappable.mcode.HasValue())
            {
                ErsMember member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objMappable.mcode, true);
                objMappable.objMember = member;
                objMappable.OverwriteWithParameter(member.GetPropertiesAsDictionary());
                objMappable.w_sex = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename, (int?)member.sex);
                objMappable.w_age = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ORDAGE, EnumCommonNameColumnName.namename, objMappable.age_code);

                objMappable.shippingList = this.LoadShippingList(objMappable);
            }
            
            if (objMappable.d_no.HasValue())
            {
                this.LoadOrderStatus(objMappable);
                objMappable.orderstatus = true;
            }
        }

        public List<Dictionary<string, object>> LoadShippingList(IClientInfoListMappable objMappable)
        {
            ErsAddressInfoRepository repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();

            var criteria = GetShippingListCriteria(objMappable.mcode);

            objMappable.recordCount = repository.GetRecordCount(criteria);
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);

            var list = repository.Find(criteria);

            return ErsCommon.ConvertEntityListToDictionaryList(list);
        }

        public void LoadOrderStatus(IClientInfoListMappable objMappable)
        {
            ErsOrder order = ErsFactory.ersOrderFactory.GetOrderWithD_no(objMappable.d_no, objMappable.site_id);
            if (order == null)
            {
                throw new ErsException("30104", objMappable.d_no);
            }

            objMappable.lname = order.lname;
            objMappable.fname = order.fname;
            objMappable.lnamek = order.lnamek;
            objMappable.fnamek = order.fnamek;
            objMappable.tel = order.tel;
            objMappable.pref_name = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(order.pref);
            objMappable.zip = order.zip;
            objMappable.address = order.address;
            objMappable.taddress = order.taddress;
            objMappable.maddress = order.maddress;

            objMappable.senddate = order.senddate;

            var shippingAddress = ErsFactory.ersOrderFactory.GetShippingAddressInfo();
            shippingAddress.LoadShippingAddress(order);

            objMappable.add_pref_name = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(shippingAddress.shipping_pref);
            objMappable.add_address = shippingAddress.shipping_address;
            objMappable.add_taddress = shippingAddress.shipping_taddress;
            objMappable.add_tel = shippingAddress.shipping_tel;
            objMappable.add_zip = shippingAddress.shipping_zip;
            objMappable.add_lname = shippingAddress.shipping_lname;
            objMappable.add_fname = shippingAddress.shipping_fname;
            objMappable.sendtime = ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId(order.sendtime);
            objMappable.w_etc = ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetEtcNameFromId(order.pay);
            objMappable.total = order.total;
            objMappable.subtotal = order.subtotal;
            objMappable.carriage = order.carriage;
            objMappable.tax = order.tax;
            objMappable.etc = order.etc;
            objMappable.p_service = order.p_service;
            objMappable.coupon_discount = order.coupon_discount;
            objMappable.order_total_amount = (objMappable.subtotal + objMappable.carriage + objMappable.tax + objMappable.etc - objMappable.p_service - objMappable.coupon_discount);
            objMappable.pay = order.pay;
            objMappable.w_pay = ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(order.pay);
            objMappable.mall_shop_kbn = order.mall_shop_kbn;
            var objSite = ErsMallFactory.ersSiteFactory.GetErsSiteByID(order.site_id);
            if (objSite != null)
            {
                objMappable.site_name = objSite.site_name;
            }
            objMappable.mall_d_no = order.mall_d_no;

            var orderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order);
            objMappable.amounttotal = ErsFactory.ersOrderFactory.GetObtainAmountTotalStgy().Obtain(orderRecords.Values);

            if (objMappable.pay == EnumPaymentType.CREDIT_CARD)
            {
                ErsMember member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objMappable.mcode, true);
                objMappable.savedCardInfo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD)).ObtainMemberCardInfo(member, order.member_card_id);
            }

            objMappable.prodList = this.LoadProductInfo(objMappable);
        }

        private ErsAddressInfoCriteria GetShippingListCriteria(string mcode)
        {
            ErsAddressInfoCriteria criteria = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();
            criteria.mcode = mcode;
            criteria.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();
            return criteria;
        }

        private List<Dictionary<string, object>> LoadProductInfo(IClientInfoListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsSearchFactory.GetErsCtsSearchStgy();
            var criteria = ErsFactory.ersCtsSearchFactory.GetErsCtsSearchCriteria();
            criteria.ordno = objMappable.d_no;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            var list = repository.FindProduct(criteria);

            var listResult = new List<Dictionary<string, object>>();
            // キャンセルレコードを分割する # 2013-01-07 NAKAUCHI
            foreach(var record in list)
            {
                // キャンセル数 + 返品数
                var cancel_amount = record.cancel_amount + record.after_cancel_amount;

                if (record.amount.HasValue)
                {
                    objMappable.prodCount += (int)record.amount - (int)cancel_amount;
                }

                var dictionary = record.GetPropertiesAsDictionary();
                dictionary["order_status"] = ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().GetStringFromId(record.order_status);
                listResult.Add(dictionary);

                if (record.order_status == EnumOrderStatusType.CANCELED || record.order_status == EnumOrderStatusType.CANCELED_AFTER_DELIVER)
                {
                    //表示用にキャンセルを設定
                    dictionary["cancel-row"] = true;
                }
                else if (cancel_amount > 0)
                {
                    ErsProductRecord newRecord = new ErsProductRecord();
                    newRecord.scode = record.scode;
                    newRecord.gcode = record.gcode;
                    newRecord.sname = record.sname;
                    newRecord.price = record.price;
                    newRecord.deliv_method = record.deliv_method;
                    newRecord.sendno = record.sendno;

                    newRecord.total = 0;
                    newRecord.amount = -1 * cancel_amount;
                    newRecord.cancel_amount = newRecord.amount;

                    var newDictionary = newRecord.GetPropertiesAsDictionary();
                    newDictionary["order_status"] = ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().GetStringFromId(EnumOrderStatusType.CANCELED);
                    newDictionary["cancel-row"] = true;
                    listResult.Add(newDictionary);
                }
            }

            return listResult;
        }
    }
}