﻿using System;
using System.Collections.Generic;
using System.Linq;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Search.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.search;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using System.Text.RegularExpressions;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;

namespace ersContact.Domain.Search.Mappers
{
    public class SearchListMapper
        : IMapper<ISearchListMappable>
    {
        public void Map(ISearchListMappable objMappable)
        {
            // 顧客検索の場合
            if (objMappable.TargetClient == true)
            {
                objMappable.searchClientList = this.LoadClientList(objMappable);
                if (objMappable.searchClientList.Count() > 0)
                {
                    objMappable.searchList = objMappable.searchClientList;
                }
            }
            //　受注検索の場合
            else
            {
                objMappable.searchOrderList = this.LoadSearchOrderList(objMappable);
                if (objMappable.searchOrderList.Count() > 0)
                {
                    objMappable.searchList = objMappable.searchOrderList;
                }
            }
        }

        public List<Dictionary<string, object>> LoadClientList(ISearchListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsSearchFactory.GetErsCtsSearchStgy();
            var criteria = this.GetCriteriaClient(objMappable);

            objMappable.recordCount = repository.GetRecordCountClient(criteria);
            criteria.SetOrderByName();

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;

            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            { 
                pagerPageCount += 1;
            }

            objMappable.pagerPageCount = pagerPageCount;

            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            var list = repository.FindClient(criteria);

            return ErsCommon.ConvertEntityListToDictionaryList(list);
        }

        public List<Dictionary<string, object>> LoadSearchOrderList(ISearchListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsSearchFactory.GetErsCtsSearchStgy();
            var criteria = this.GetCriteriaOrder(objMappable);

            objMappable.recordCount = repository.GetRecordCountOrder(criteria);

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            { pagerPageCount += 1; }

            objMappable.pagerPageCount = pagerPageCount;

            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            criteria.AddOrderBy("d_no", Criteria.OrderBy.ORDER_BY_DESC);

            var list = repository.FindOrder(criteria);
            var listResult = new List<Dictionary<string, object>>();

            foreach (var record in list)
            {
                var dictionary = record.GetPropertiesAsDictionary();
                dictionary["is_receipt_exist"] = ErsFactory.ersOrderFactory.GetOrderWithD_no(record.d_no, record.site_id) == null ? false : true;
                listResult.Add(dictionary);
            }

            return listResult;

            //return ErsCommon.ConvertEntityListToDictionaryList(list);
        }

        private ErsMemberCriteria GetCriteriaClient(ISearchListMappable objMappable)
        {
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            //受注番号

            if (objMappable.ordertype || objMappable.ctsordertype || objMappable.regordertype)
            {
                criteria.SetOrderType(objMappable.ordertype, objMappable.ctsordertype, objMappable.regordertype, objMappable.d_no, setup.site_id);
            }
            else if (!string.IsNullOrEmpty(objMappable.d_no))
            {
                criteria.SetOrderType(true, true, true, objMappable.d_no, setup.site_id);
            }

            // 返品日
            if (objMappable.s_af_cancel_date_from.HasValue)
            {
                criteria.af_cancel_date_from = objMappable.s_af_cancel_date_from.Value.Date;
            }
            if (objMappable.s_af_cancel_date_to.HasValue)
            {
                criteria.af_cancel_date_to = objMappable.s_af_cancel_date_to.Value.Date.AddDays(1).AddMilliseconds(-1);
            }

            // キャンセル日
            if (objMappable.s_cancel_date_from.HasValue)
            {
                criteria.cancel_date_from = objMappable.s_cancel_date_from.Value.Date;
            }
            if (objMappable.s_cancel_date_to.HasValue)
            {
                criteria.cancel_date_to = objMappable.s_cancel_date_to.Value.AddDays(1).AddMilliseconds(-1);
            }

            if (!string.IsNullOrEmpty(objMappable.mcode))
                criteria.mcodeLikePrefix = objMappable.mcode;

            if (!string.IsNullOrEmpty(objMappable.lname))
                criteria.lnameLikePrefix = objMappable.lname;

            if (!string.IsNullOrEmpty(objMappable.fname))
                criteria.fnameLikePrefix = objMappable.fname;

            if (!string.IsNullOrEmpty(objMappable.lnamek))
                criteria.lnamekLikePrefix = objMappable.lnamek;

            if (!string.IsNullOrEmpty(objMappable.fnamek))
                criteria.fnamekLikePrefix = objMappable.fnamek;

            if (objMappable.pref != null && objMappable.pref > 0)
                criteria.pref = objMappable.pref;

            if (objMappable.src_deleted != EnumDeleted.Deleted)
            {
                criteria.deleted = EnumDeleted.NotDeleted;
            }

            criteria.ignoreMonitor();

            if (!string.IsNullOrEmpty(objMappable.address))
                criteria.SetLikeAddress(objMappable.address);

            if (!string.IsNullOrEmpty(objMappable.email))
                criteria.emailLikePrefix = objMappable.email;

            if (!string.IsNullOrEmpty(objMappable.tel))
                criteria.telLikePrefix = objMappable.tel;

            if (!string.IsNullOrEmpty(objMappable.fax))
                criteria.faxLikePrefix = objMappable.fax;

            if (!string.IsNullOrEmpty(objMappable.zip1))
            {
                objMappable.zip = objMappable.zip1;
                if (objMappable.zip.Length == 3)
                {
                    objMappable.zip = objMappable.zip1 + "-" + objMappable.zip2;
                }
                criteria.zipLikePrefix = objMappable.zip;
            }

            if (!string.IsNullOrEmpty(objMappable.ccode))
                criteria.ccodeLikePrefix = objMappable.ccode;

            if (objMappable.campaign_id != null)
                criteria.campaign_id = objMappable.campaign_id;

            criteria.active = EnumActive.Active;
            criteria.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();

            return criteria;
        }

        private ErsCtsSearchCriteria GetCriteriaOrder(ISearchListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsSearchFactory.GetErsCtsSearchCriteria();

            criteria.d_master_t = objMappable.ordertype;
            criteria.regular_t = objMappable.regordertype;
            criteria.cts_order_t = objMappable.ctsordertype;
            criteria.d_no = objMappable.d_no;

            if (!string.IsNullOrEmpty(objMappable.mall_d_no))
                criteria.mall_d_no = objMappable.mall_d_no;

            if (!string.IsNullOrEmpty(objMappable.mcode))
                criteria.mcodeLikePrefix = objMappable.mcode;

            if (!string.IsNullOrEmpty(objMappable.lname))
                criteria.lnameLikePrefix = objMappable.lname;

            if (!string.IsNullOrEmpty(objMappable.fname))
                criteria.fnameLikePrefix = objMappable.fname;

            if (!string.IsNullOrEmpty(objMappable.lnamek))
                criteria.lnamekLikePrefix = objMappable.lnamek;

            if (!string.IsNullOrEmpty(objMappable.fnamek))
                criteria.fnamekLikePrefix = objMappable.fnamek;

            if (objMappable.pref != null && objMappable.pref > 0)
                criteria.pref = objMappable.pref;

            if (objMappable.src_deleted != EnumDeleted.Deleted)
            {
                criteria.deleted = EnumDeleted.NotDeleted;
            }

            if (!string.IsNullOrEmpty(objMappable.address))
                criteria.SetLikeAddress(objMappable.address);

            if (!string.IsNullOrEmpty(objMappable.email))
                criteria.emailLikePrefix = objMappable.email;

            if (!string.IsNullOrEmpty(objMappable.tel))
                criteria.telLikePrefix = objMappable.tel;

            if (!string.IsNullOrEmpty(objMappable.fax))
                criteria.faxLikePrefix = objMappable.fax;

            if (!string.IsNullOrEmpty(objMappable.zip1))
            {
                objMappable.zip = objMappable.zip1;
                if (objMappable.zip.Length == 3)
                {
                    objMappable.zip = objMappable.zip1 + "-" + objMappable.zip2;
                }
                criteria.zipLikePrefix = objMappable.zip;
            }

            // 返品日
            if (objMappable.s_af_cancel_date_from.HasValue)
            {
                criteria.af_cancel_date_from = objMappable.s_af_cancel_date_from.Value.Date;
            }
            if (objMappable.s_af_cancel_date_to.HasValue)
            {
                criteria.af_cancel_date_to = objMappable.s_af_cancel_date_to.Value.Date.AddDays(1).AddMilliseconds(-1);
            }

            // キャンセル日
            if (objMappable.s_cancel_date_from.HasValue)
            {
                criteria.cancel_date_from = objMappable.s_cancel_date_from.Value.Date;
            }
            if (objMappable.s_cancel_date_to.HasValue)
            {
                criteria.cancel_date_to = objMappable.s_cancel_date_to.Value.AddDays(1).AddMilliseconds(-1);
            }

            if (!string.IsNullOrEmpty(objMappable.ccode))
                criteria.ccodeLikePrefix = objMappable.ccode;

            if (objMappable.campaign_id != null)
                criteria.campaign_id = objMappable.campaign_id;

            criteria.searchData_site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            criteria.Add(Criteria.GetCriterion("searchData.active", 1, Criteria.Operation.EQUAL));
            
            criteria.ignoreMonitor();

            return criteria;
        }
    }
}