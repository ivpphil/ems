﻿using System;
using ersContact.Domain.Login.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;

namespace ersContact.Domain.Login.Handlers
{
    public class LoginHandler
        : ICommandHandler<ILoginCommand>
    {
        public ICommandResult Submit(ILoginCommand command)
        {
            var tpCtsUserId = ((ISession)ErsContext.sessionState).GetUserCode(command.user_id, command.passwd);
            command.cts_user_id = Convert.ToInt32(tpCtsUserId);
            command.authority = ((ErsSessionStateContact)ErsContext.sessionState).GetUserAuthority(tpCtsUserId);

            ((ISession)ErsContext.sessionState).getNewSSLransu(tpCtsUserId);

            return new CommandResult(true);
        }
    }
}