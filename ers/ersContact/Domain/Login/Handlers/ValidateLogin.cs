﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Login.Commands;
using jp.co.ivp.ers.cts_operators;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using jp.co.ivp.ers.state;

namespace ersContact.Domain.Login.Handlers
{
    public class ValidateLogin
           : IValidationHandler<ILoginCommand>
    {
        public IEnumerable<ValidationResult> Validate(ILoginCommand command)
        {
            yield return command.CheckRequired("user_id");
            yield return command.CheckRequired("passwd");

            if (command.IsValidField("user_id", "passwd"))
            {
                var tpCtsUserId = ((ISession)ErsContext.sessionState).GetUserCode(command.user_id, command.passwd);
                if (tpCtsUserId == ErsCtsOperator.DEFAULT_USER_ID)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10204"), new[] { "user_id", "passwd" });
                }
            }
        }
    }
}