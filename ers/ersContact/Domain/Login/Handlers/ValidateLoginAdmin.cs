﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Login.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Login.Handlers
{
    public class ValidateLoginAdmin
           : IValidationHandler<ILoginAdminCommand>
    {
        public IEnumerable<ValidationResult> Validate(ILoginAdminCommand command)
        {
            yield return command.CheckRequired("admin_ransu");
            yield return command.CheckRequired("admin_ssl_ransu");
            yield return command.CheckRequired("user_cd");

            if (command.IsValidField("admin_ransu", "admin_ssl_ransu", "user_cd"))
            {
                var userState = ErsFactory.ersSessionStateFactory.GetUserStateAdminSpecification().getUserState(command.admin_ransu, command.admin_ssl_ransu, command.user_cd);
                var administrator = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithUserCode(command.user_cd);
                var agent = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(administrator.agent_id);

                if (userState != EnumUserState.LOGIN || agent == null)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("10209"));
                }
            }
        }
    }
}