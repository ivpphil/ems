﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Login.Mappables
{
    public interface IAgentMappable
        : IMappable
    {
        string user_cd { get; }
        string user_id { set; }
        string passwd { set; }
    }
}
