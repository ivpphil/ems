﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Login.Commands
{
    public interface ILoginCommand : ICommand
    {
        int? cts_user_id { set; }
        string user_id { get; }
        string passwd { get; }
        string authority { set; }
    }
}