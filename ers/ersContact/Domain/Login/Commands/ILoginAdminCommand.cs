﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Login.Commands
{
    public interface ILoginAdminCommand : ICommand
    {
        string user_cd { get; }
        string admin_ransu { get; }
        string admin_ssl_ransu { get; }
    }
}