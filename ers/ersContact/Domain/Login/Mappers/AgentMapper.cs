﻿using ersContact.Domain.Login.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersContact.Domain.Login.Mappers
{
    public class AgentMapper
        : IMapper<IAgentMappable>
    {
        public void Map(IAgentMappable objMappable)
        {
            var administrator = ErsFactory.ersAdministratorFactory.GetErsAdministratorWithUserCode(objMappable.user_cd);
            var agent = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(administrator.agent_id.Value);
            if (agent == null)
            {
                throw new ErsException("29002");
            }

            objMappable.user_id = agent.user_id;
            objMappable.passwd = agent.passwd;
        }
    }
}