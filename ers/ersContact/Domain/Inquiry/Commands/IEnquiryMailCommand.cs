﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Inquiry.Commands
{
    public interface IEnquiryMailCommand : ICommand
    {
        Dictionary<string, int> esc_incmail_list { get; set; }
        Dictionary<string, int> del_incmail_list { get; set; }
        string user_id { get; }
        int agent_id { get; }
        int? case_no { get; set; }
        bool incmail { set; }
    }
}