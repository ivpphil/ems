﻿using System;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Inquiry.Commands
{
    public interface IInquiryCommand : ICommand
    {
        bool LockedByOther { get; }
        bool IsRegistAction { get; }
        bool btnLock { get; }
        bool btnRelease { get; }
        bool doEscalate { set; }
        bool showincmail { set; }
        bool mailapp { get; }
        bool mailesctray { get; }
        bool mailescreset { get; }
        bool mailescsend { get; }
        bool mailsent { get; }
        bool mailtray { get; }
        bool mailreg { get; set; }
        bool execdone {get; set; }
        bool temppres { get; }
        bool phonereg { get; }
        bool memoreg { get; }
        bool reset { get; }
        bool setstarttime { get; }
        bool setfinishtime { get; }
        int? case_no { get; set; }
        string user_id { get; }
        int? lockid { get; set; }
        string esc_id { get; }
        EnumCorresponding? corresponding { get; set; }
        int? sub_no { get;  set; }
        EnumEnqProgress? enq_progress { set; get; }
        EnumEnqSituation? enq_situation { set; }
        EnumEnqPriority? enq_priorty { set; }
        EnumEnqStatus? enq_status { set; }
        EnumEnqType? enq_type { set; }
        int cate1 { set; }
        int cate2 { set; }
        int cate3 { set; }
        int cate4 { set; }
        int cate5 { set; }
        EnumEnqCorresponding? enq_corresponding { get; }
        string email_title_esc { get; set; }
        string email_from_esc { get; set; }
        string email_header_esc { get; set; }
        string email_body_esc { get; set; }
        string email_fotter_esc { get; set; }
        string enq_casename { set; }
        string enq_detail { get; set; }
        string ans_detail { get; set; }
        string email_from { get; set; }
        string email_title { get; set; }
        string[] email_to { get; set; }
        string[] email_cc { get; set; }
        string[] email_bcc { get; set; }
        string email_header { get; set; }
        string email_body { get; set; }
        string email_fotter { get; set; }
        string email_from_name { set; }
        string memo { get; set; }
        string fname { set; }
        string lname { set; }
        DateTime? starttime { get; set; }
        DateTime? finishtime { get; set; }
        DateTime? intime { set; }
        DateTime? utime { set; }
        int? site_id { get; set; }
        bool submit_update { get; }
        bool mail_update { get; }
    }
}