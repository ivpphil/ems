﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Inquiry.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Inquiry.Handlers
{
    public class ValidateInquiry
           : IValidationHandler<IInquiryCommand>
    {
        public IEnumerable<ValidationResult> Validate(IInquiryCommand command)
        {
            if (command.temppres ||
               command.phonereg ||
               command.mailtray ||
               command.mailreg ||
               command.memoreg ||
               command.mailsent)
            {
                yield return command.CheckRequired("mcode");
                yield return command.CheckRequired("intime");
                yield return command.CheckRequired("esc_id");

                if (command.enq_corresponding == EnumEnqCorresponding.Phone)
                {
                    yield return command.CheckRequired("enq_detail");
                    yield return command.CheckRequired("ans_detail");
                }

                if (command.enq_corresponding == EnumEnqCorresponding.Email)
                {
                    yield return command.CheckRequired("email_title");
                    yield return command.CheckRequired("email_to");
                    yield return command.CheckRequired("email_from");
                    yield return command.CheckRequired("email_body");
                }

                if (command.enq_corresponding == EnumEnqCorresponding.Memo)
                {
                    yield return command.CheckRequired("memo");
                }

            }
            else if (command.mailescsend || command.mailesctray)
            {
                yield return command.CheckRequired("email_title_esc");
                yield return command.CheckRequired("email_to");
                yield return command.CheckRequired("email_from_esc");
            }
        }
    }
}