﻿using System;
using System.Text.RegularExpressions;
using ersContact.Domain.Inquiry.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.incomingmail;
using jp.co.ivp.ers.Inquiry;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.viewService;

namespace ersContact.Domain.Inquiry.Handlers
{
    public class EnquiryMailHandler : ICommandHandler<IEnquiryMailCommand>
    {
        public ICommandResult Submit(IEnquiryMailCommand command)
        {
            Regex objReg = new Regex("\\(([0-9]*?)\\)");

            //メールをエスカレート
            foreach (string esckey in command.esc_incmail_list.Keys)
            {
                if (esckey != null)
                {
                    ErsCtsIncomingMail _incmail = ErsFactory.ersCtsInquiryFactory.GetErsCtsIncomingMailWithID(Convert.ToInt32(esckey));
                    if (_incmail != null)
                    {
                        var objInquiry = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiry();
                        objInquiry.mcode = (!string.IsNullOrEmpty(_incmail.mcode)) ? _incmail.mcode : "1";
                        objInquiry.intime = _incmail.intime;
                        objInquiry.utime = DateTime.Now;
                        objInquiry.recepttime = DateTime.Now;
                        objInquiry.enq_type = EnumEnqType.InboundEmail;
                        objInquiry.enq_status = EnumEnqStatus.Other;
                        objInquiry.enq_progress = EnumEnqProgress.Open;
                        objInquiry.enq_situation = EnumEnqSituation.Correspondence;
                        objInquiry.enq_priorty = 0;
                        objInquiry.enq_casename = _incmail.mail_title;
                        objInquiry.user_id = command.user_id;
                        objInquiry.save_mode = EnumEnqSaveMode.Submit;
                        objInquiry.esc_id = command.agent_id.ToString();
                        objInquiry.site_id = _incmail.site_id;

                        if (_incmail.mail_title != null)
                        {
                            if (!objReg.IsMatch(_incmail.mail_title))
                            {
                                var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
                                repository.Insert(objInquiry, true);

                                _incmail.mail_title = string.Format("{0} ({1})", _incmail.mail_title.Trim(), objInquiry.case_no); //adding case no to email title
                            }
                            else
                            {
                                var CaseNo = objReg.Match(_incmail.mail_title).Groups[1].Value;
                                command.case_no = Convert.ToInt32(CaseNo);
                                objInquiry.case_no = command.case_no;
                            }
                        }

                        var objInquiryDetail = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetail();
                        objInquiryDetail.case_no = objInquiry.case_no;
                        objInquiryDetail.active = EnumActive.NonActive;
                        objInquiryDetail.enq_corresponding = EnumEnqCorresponding.Email;
                        objInquiryDetail.email_title = _incmail.mail_title;
                        objInquiryDetail.email_from = _incmail.mail_from;
                        objInquiryDetail.email_to = _incmail.mail_to;
                        if (_incmail.mail_cc != null)
                        {
                            objInquiryDetail.email_cc = _incmail.mail_cc;
                        }
                        if (_incmail.mail_bcc != null)
                        {
                            objInquiryDetail.email_bcc = _incmail.mail_bcc;
                        }
                        objInquiryDetail.email_body = _incmail.mail_body;
                        objInquiryDetail.email_status = EnumEnqEmailStatus.EmailRegist;
                        objInquiryDetail.email_type = EnumEnqEmailType.Client;
                        objInquiryDetail.active = EnumActive.Active;
                        objInquiryDetail.intime = DateTime.Now;
                        objInquiryDetail.utime = DateTime.Now;
                        objInquiryDetail.email_id = _incmail.id;

                        var ctsEnquiryDetailRepository = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetailRepository();
                        ctsEnquiryDetailRepository.Insert(objInquiryDetail, true);

                        this.UpdateIncMailStatus(Convert.ToInt32(esckey), EnumActive.Active);
                    }
                }
            }

            //電子メールを削除
            foreach (string delkey in command.del_incmail_list.Keys)
            {
                if (delkey != null) this.UpdateIncMailStatus(Convert.ToInt32(delkey), EnumActive.NonActive);
            }

            command.incmail = true;

            return new CommandResult(true);
        }

        internal void UpdateIncMailStatus(int ID, EnumActive status)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsIncomingMailRepository();
            var new_incmail = ErsFactory.ersCtsInquiryFactory.GetErsCtsIncomingMailWithID(ID);
            new_incmail.active = status;

            var old_incmail = ErsFactory.ersCtsInquiryFactory.GetErsCtsIncomingMailWithID(ID);

            repository.Update(old_incmail, new_incmail);
        }
    }
}
