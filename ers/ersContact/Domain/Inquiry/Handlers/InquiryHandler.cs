﻿using System;
using ersContact.Domain.Inquiry.Commands;
using jp.co.ivp.ers.Inquiry;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.cts_Inquiry;

namespace ersContact.Domain.Inquiry.Handlers
{
    public class InquiryHandler : ICommandHandler<IInquiryCommand>
    {
        public ICommandResult Submit(IInquiryCommand command)
        {
            if (!command.LockedByOther)
            {
                if (command.IsRegistAction)
                {
                    if (command.case_no == null)
                    {
                        this.Insert(command);
                    }
                    else
                    {
                        this.Update(command);
                    }
                }
                else if (command.btnLock || command.btnRelease)
                {
                    this.LockRelease(command);
                }
            }
            else
            {
                command.doEscalate = false;
                command.showincmail = false;
            }

            this.ResetField(command);
            this.SetTime(command);

            if (!command.submit_update && command.execdone)
            {
                command.showincmail = false;
            }

            return new CommandResult(true);
        }

        internal void Insert(IInquiryCommand command)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
            var inquiry = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiry();
            inquiry.OverwriteWithParameter(command.GetPropertiesAsDictionary());

            this.OverwriteInquiry(inquiry, command);

            var case_no_is_null = inquiry.case_no == null;

            repository.Insert(inquiry, true);

            command.case_no = inquiry.case_no;

            var  ctsEnquiryDetail = this.InsertEnquiryDetail(command, case_no_is_null);

            var insertSendMail = false;
            if (command.mailapp || command.mailesctray || command.mailsent)
            {
                insertSendMail = true;
            }

            if (insertSendMail)
            {
                var objErsCtsSendEmailRepository = ErsFactory.ersCtsInquiryFactory.GetErsCtsSendEmailRepository();
                var objErsCtsSendEmail = ErsFactory.ersCtsInquiryFactory.GetErsCtsSendEmail();

                this.OverwriteSendEmail(objErsCtsSendEmail, command);

                if (command.mailescsend || command.mailsent)
                {
                    objErsCtsSendEmail.email_status = EnumEnqEmailStatus.EmailRegist;
                }
                objErsCtsSendEmail.active = EnumActive.NonActive;

                objErsCtsSendEmailRepository.Insert(objErsCtsSendEmail, true);
            }

            command.sub_no = ctsEnquiryDetail.sub_no;

            this.clearDetailFields(command);

            command.execdone = true;
        }

        private ErsCtsEnquiryDetail InsertEnquiryDetail(IInquiryCommand command, bool case_no_is_null)
        {
            var ctsEnquiryDetailRepository = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetailRepository();
            var ctsEnquiryDetail = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetail();

            this.OverwriteInquiryDetail(ctsEnquiryDetail, command);

            if (case_no_is_null && command.enq_corresponding == EnumEnqCorresponding.Email)
            {
                Regex objReg = new Regex("\\(([0-9]*?)\\)");
                if (!objReg.IsMatch(command.email_title + ""))
                {
                    //adding case no to email title
                    ctsEnquiryDetail.email_title = string.Format("{0} ({1})", command.email_title.Trim(), command.case_no);
                }
            }

            ctsEnquiryDetail.active = EnumActive.Active;
            ctsEnquiryDetail.intime = DateTime.Now;

            ctsEnquiryDetailRepository.Insert(ctsEnquiryDetail, true);

            return ctsEnquiryDetail;
        }

        internal void Update(IInquiryCommand command)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();

            var new_inq = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryWithCaseNo(command.case_no.Value);

            if (command.mailescsend)
            {
                new_inq.utime = DateTime.Now;
                new_inq.recepttime = DateTime.Now;
            }
            else
            {

                new_inq.OverwriteWithParameter(command.GetPropertiesAsDictionary());
            }

            this.OverwriteInquiry(new_inq, command);

            if (!command.mailesctray)
            {
                var old_inq = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryWithCaseNo(command.case_no.Value);
                repository.Update(old_inq, new_inq);
            }

            var ctsEnquiryDetailRepository = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetailRepository();
            var ctsEnquiryDetail = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetail();
            this.OverwriteInquiryDetail(ctsEnquiryDetail, command);

            if (command.enq_corresponding == EnumEnqCorresponding.Email)
            {
                Regex objReg = new Regex("\\(([0-9]*?)\\)");
                if (!objReg.IsMatch(command.email_title + ""))
                {
                    //adding case no to email title
                    ctsEnquiryDetail.email_title = string.Format("{0} ({1})", command.email_title.Trim(), command.case_no);
                }
            }

            var insertSendMail = false;
            if ((command.mailapp || command.mailesctray || command.mailsent) && !command.submit_update)
            {
                insertSendMail = true;
            }

            if ((command.mailapp && command.mailsent) || command.submit_update)
            {
                var cri = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetailCriteria();
                cri.case_no = command.case_no;
                cri.sub_no = command.sub_no;
                var ctsOldEnquiryDetail = ctsEnquiryDetailRepository.FindSingle(cri);
                var ctsNewEnquiryDetail = this.GetCtsNewEnquiryDetail(command);

                if (ctsOldEnquiryDetail != null)
                {
                    this.OverwriteInquiryDetail(ctsNewEnquiryDetail, command);
                    ctsNewEnquiryDetail.id = ctsOldEnquiryDetail.id;
                    ctsNewEnquiryDetail.email_status = EnumEnqEmailStatus.SendFinish;
                    ctsNewEnquiryDetail.active = EnumActive.Active;
                    ctsNewEnquiryDetail.save_mode = EnumEnqSaveMode.Submit;

                    if (command.enq_corresponding == EnumEnqCorresponding.Memo)
                    {
                        ctsNewEnquiryDetail.corresponding = ctsOldEnquiryDetail.corresponding;
                    }

                    ctsEnquiryDetailRepository.Update(ctsOldEnquiryDetail, ctsNewEnquiryDetail);

                    if (command.enq_progress == EnumEnqProgress.Open && !command.submit_update)
                    {
                        new_inq.esc_id = "1";
                        new_inq.enq_progress = EnumEnqProgress.Open;
                    }
                    var old_inq = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryWithCaseNo(command.case_no.Value);
                    repository.Update(old_inq, new_inq);
                }
            }
            else
            {
                ctsEnquiryDetail.active = EnumActive.Active;
                ctsEnquiryDetail.intime = DateTime.Now;

                ctsEnquiryDetailRepository.Insert(ctsEnquiryDetail, true);
            }

            if (insertSendMail)
            {
                var objErsCtsSendEmail = ErsFactory.ersCtsInquiryFactory.GetErsCtsSendEmail();

                this.OverwriteSendEmail(objErsCtsSendEmail, command);

                if (command.mailescsend || command.mailsent)
                {
                    objErsCtsSendEmail.email_status = EnumEnqEmailStatus.EmailRegist;
                }
                objErsCtsSendEmail.active = EnumActive.NonActive;

                var objErsCtsSendEmailRepository = ErsFactory.ersCtsInquiryFactory.GetErsCtsSendEmailRepository();
                objErsCtsSendEmailRepository.Insert(objErsCtsSendEmail, true);
            }

            this.clearDetailFields(command);

            command.execdone = true;
        }

        private ErsCtsEnquiryDetail GetCtsNewEnquiryDetail(IInquiryCommand command)
        {
            if (command.enq_corresponding == EnumEnqCorresponding.Memo)
            {
                return ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetailWithParameter(command.GetPropertiesAsDictionary("update_memo"));
            }

            return ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetailWithParameter(command.GetPropertiesAsDictionary());
        }

        private void UpdateEnquiryDetail(IInquiryCommand command, ErsCtsEnquiryDetail ctsNewEnquiryDetail)
        {
            var ctsEnquiryDetailRepository = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetailRepository();
            var ctsOldEnquiryDetail = ErsFactory.ersCtsInquiryFactory.GetErsCtsEnquiryDetailWithParameter(ctsNewEnquiryDetail.GetPropertiesAsDictionary());

            this.OverwriteInquiryDetail(ctsNewEnquiryDetail, command);

            ctsNewEnquiryDetail.active = EnumActive.Active;
            ctsNewEnquiryDetail.intime = DateTime.Now;

            ctsEnquiryDetailRepository.Update(ctsOldEnquiryDetail, ctsNewEnquiryDetail);
        }

        internal void LockRelease(IInquiryCommand command)
        {
            var lockObj = ErsFactory.ersCtsInquiryFactory.GetObtainLockControl();

            if (command.btnLock)
            {
                lockObj.Lock(command.case_no.Value, Convert.ToInt32(command.user_id));
            }
            else if (command.btnRelease)
            {
                lockObj.Release(command.case_no.Value, Convert.ToInt32(command.user_id));
            }
        }

        /// <summary>
        /// ErsCtsInquiry に情報セット
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private void OverwriteInquiry(ErsCtsInquiry data, IInquiryCommand command)
        {
            if (data == null)
            {
                return;
            }

            data.lockid = (Convert.ToInt32(command.user_id) == command.lockid) ? null : command.lockid;

            if ((command.temppres && command.enq_corresponding == EnumEnqCorresponding.Phone) || command.phonereg) //phone 
            {
                if (command.temppres)
                {
                    data.save_mode = EnumEnqSaveMode.Temporary;
                }
                else if (command.phonereg)
                {
                    data.save_mode = EnumEnqSaveMode.Submit;
                    data.esc_id = command.esc_id;
                }
            }
            else if ((command.temppres && command.enq_corresponding == EnumEnqCorresponding.Email) || command.mailtray || command.mailreg || command.mailsent) //email
            {
                if (command.temppres)
                {
                    data.save_mode = EnumEnqSaveMode.Temporary;
                }
                else if (command.mailreg)
                {
                    data.save_mode = EnumEnqSaveMode.Submit;
                    data.esc_id = command.esc_id;
                }
            }
            else if ((command.temppres && command.enq_corresponding == EnumEnqCorresponding.Memo) || command.memoreg) //memo
            {
                if (command.temppres)
                {
                    data.save_mode = EnumEnqSaveMode.Temporary;
                }
                else if (command.memoreg)
                {
                    data.save_mode = EnumEnqSaveMode.Submit;
                    data.esc_id = command.esc_id;
                }
            }
        }

        /// <summary>
        /// ErsCtsInquiry に情報セット
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private void OverwriteInquiryDetail(ErsCtsEnquiryDetail data, IInquiryCommand command)
        {
            if (data == null)
            {
                return;
            }

            data.case_no = command.case_no;
            data.enq_corresponding = command.enq_corresponding;

            if ((command.temppres && command.enq_corresponding == EnumEnqCorresponding.Phone) || command.phonereg) //phone 
            {
                data.corresponding = command.corresponding;
                data.starttime = command.starttime;
                data.finishtime = command.finishtime;
                data.enq_detail = command.enq_detail;
                data.ans_detail = command.ans_detail;
                if (command.temppres)
                {
                    data.save_mode = EnumEnqSaveMode.Temporary;
                }
                else if (command.phonereg)
                {
                    data.save_mode = EnumEnqSaveMode.Submit;
                }
            }
            else if ((command.temppres && command.enq_corresponding == EnumEnqCorresponding.Email) || command.mailtray || command.mailreg || command.mailsent || (command.submit_update && command.mail_update)) //email
            {
                data.corresponding = command.corresponding;
                data.email_from_name = command.email_from;
                data.email_title = command.email_title;
                data.email_to = command.email_to;
                data.email_cc = command.email_cc;
                data.email_bcc = command.email_bcc;

                data.email_from = command.email_from;
                data.email_header = command.email_header;
                data.email_body = command.email_body;
                data.email_fotter = command.email_fotter;

                if (command.temppres)
                {
                    data.save_mode = EnumEnqSaveMode.Temporary;
                }
                else if (command.mailtray)
                {
                    data.email_status = EnumEnqEmailStatus.EmailTray;
                    data.email_type = EnumEnqEmailType.EndUser;
                }
                else if (command.mailreg)
                {
                    data.save_mode = EnumEnqSaveMode.Submit;
                    data.email_status = EnumEnqEmailStatus.EmailRegist;
                    data.email_type = EnumEnqEmailType.EndUser;
                }
                else if (command.mailsent)
                {
                    data.email_status = EnumEnqEmailStatus.SendFinish;
                    data.email_type = EnumEnqEmailType.EndUser;
                }
            }
            else if (command.mailesctray || command.mailescsend)
            {
                data.enq_corresponding = EnumEnqCorresponding.Email;
                data.email_title = command.email_title_esc;
                data.email_from = command.email_from_esc;

                data.email_to = command.email_to;
                data.email_cc = command.email_cc;
                data.email_bcc = command.email_bcc;
                data.email_header = command.email_header_esc;
                data.email_body = command.email_body_esc;
                data.email_fotter = command.email_fotter_esc;
                data.email_type = EnumEnqEmailType.Client;

                if (command.mailesctray)
                {
                    data.email_status = EnumEnqEmailStatus.EmailTray;
                }
                if (command.mailescsend)
                {
                    data.email_status = EnumEnqEmailStatus.SendFinish;
                }
            }
            else if ((command.temppres && command.enq_corresponding == EnumEnqCorresponding.Memo) || command.memoreg) //memo
            {
                data.memo = command.memo;
                if (command.temppres)
                {
                    data.save_mode = EnumEnqSaveMode.Temporary;
                }
                else if (command.memoreg)
                {
                    data.save_mode = EnumEnqSaveMode.Submit;
                }
            }
        }

        /// <summary>
        /// ErsCtsInquiry に情報セット
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private void OverwriteSendEmail(ErsCtsSendEmail data, IInquiryCommand command)
        {
            if (data == null)
            {
                return;
            }

            data.case_no = command.case_no;

            if ((command.temppres && command.enq_corresponding == EnumEnqCorresponding.Email) || command.mailtray || command.mailreg || command.mailsent) //email
            {
                data.email_from_name = command.email_from;
                data.email_title = command.email_title;
                data.email_to = command.email_to;
                data.email_cc = command.email_cc;
                data.email_bcc = command.email_bcc;

                data.email_from = command.email_from;
                data.email_header = command.email_header;
                data.email_body = command.email_body;
                data.email_fotter = command.email_fotter;

                if (command.mailtray)
                {
                    data.email_status = EnumEnqEmailStatus.EmailTray;
                }
                else if (command.mailreg)
                {
                    data.email_status = EnumEnqEmailStatus.EmailRegist;
                }
                else if (command.mailsent)
                {
                    data.email_status = EnumEnqEmailStatus.SendFinish;
                }
            }
            else if (command.mailesctray || command.mailescsend)
            {
                data.email_title = command.email_title_esc;
                data.email_from = command.email_from_esc;

                data.email_to = command.email_to;
                data.email_cc = command.email_cc;
                data.email_bcc = command.email_bcc;
                data.email_header = command.email_header_esc;
                data.email_body = command.email_body_esc;
                data.email_fotter = command.email_fotter_esc;

                if (command.mailesctray)
                {
                    data.email_status = EnumEnqEmailStatus.EmailTray;
                }
                if (command.mailescsend)
                {
                    data.email_status = EnumEnqEmailStatus.SendFinish;
                }
            }
        }

        /// <summary>
        ///画面表示詳細情報の初期化 
        /// </summary>
        private void clearDetailFields(IInquiryCommand command)
        {
            command.starttime = null;
            command.finishtime = null;
            command.enq_detail = "";
            command.ans_detail = "";
            command.corresponding = EnumCorresponding.None;
            command.email_from = "";
            command.email_title = "";
            command.email_to = null;
            command.email_cc = null;
            command.email_bcc = null;
            command.email_header = "";
            command.email_body = "";
            command.email_fotter = "";
            command.email_from_name = "";
            command.memo = "";
            command.enq_progress = null;
            command.enq_situation = null;
            command.fname = "";
            command.lname = "";
            command.lockid = null;
        }

        /// <summary>
        /// 画面表示情報の初期化
        /// </summary>
        internal void ResetField(IInquiryCommand command)
        {
            if (command.reset)
            {
                this.clearMainFields(command);
            }
            else if (command.mailsent)
            {
                this.clearMailSentFields(command);
            }
            else if (command.mailescreset)
            {
                this.clearMailEscFields(command);
            }
            else if (command.phonereg)
            {
                command.enq_detail = "";
                command.ans_detail = "";
                command.email_body = "";
            }
        }

        /// <summary>
        /// 画面表示情報の初期化
        /// </summary>
        private void clearMainFields(IInquiryCommand command)
        {
            command.case_no = null;
            command.intime = null;
            command.utime = null;
            command.enq_priorty = EnumEnqPriority.None;
            command.enq_status = EnumEnqStatus.None;
            command.enq_type = EnumEnqType.None;
            command.enq_casename = "";
            command.cate1 = 0;
            command.cate2 = 0;
            command.cate3 = 0;
            command.cate4 = 0;
            command.cate5 = 0;
            command.lockid = null;

            this.clearDetailFields(command);
        }

        /// <summary>
        /// 画面メール表示情報の初期化
        /// </summary>
        private void clearMailSentFields(IInquiryCommand command)
        {
            command.email_from = "";
            command.email_title = "";
            command.email_to = null;
            command.email_cc = null;
            command.email_bcc = null;
            command.email_header = "";
            command.email_body = "";
            command.email_fotter = "";
            command.email_from_name = "";
            command.fname = "";
            command.lname = "";
        }

        /// <summary>
        /// 画面エスカレーションメール表示情報の初期化
        /// </summary>
        private void clearMailEscFields(IInquiryCommand command)
        {
            command.email_from_esc = "";
            command.email_title_esc = "";
            command.email_header_esc = "";
            command.email_body_esc = "";
            command.email_fotter_esc = "";
            command.doEscalate = true;
            command.mailreg = true;
        }

        internal void SetTime(IInquiryCommand command)
        {
            if (command.setstarttime)
            {
                command.starttime = DateTime.Now;
            }
            if (command.setfinishtime)
            {
                command.finishtime = DateTime.Now;
            }
        }
    }
}
