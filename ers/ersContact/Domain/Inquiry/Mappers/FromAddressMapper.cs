﻿using System;
using ersContact.Domain.Inquiry.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.fromaddress;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Inquiry.Mappers
{
    public class FromAddressMapper
        : IMapper<IFromAddressMappable>
    {
        public void Map(IFromAddressMappable objMappable)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsFromAddressRepository();
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsFromAddressCriteria();
            criteria.active = EnumActive.Active;
            criteria.site_id = objMappable.site_id;

            var list = repository.Find(criteria);

            objMappable.frmList = ErsCommon.ConvertEntityListToDictionaryList(list);

            var email_from_name_id = objMappable.email_from_name;
            if (objMappable.mailtemplate && !string.IsNullOrEmpty(objMappable.email_from_name))
            {
                ErsCtsFromAddress fromaddress = ErsFactory.ersCtsInquiryFactory.GetErsCtsFromAddressWithID(Int32.Parse(objMappable.email_from_name));
                objMappable.OverwriteWithParameter(fromaddress.GetPropertiesAsDictionary());

                objMappable.email_from_name = email_from_name_id;
                objMappable.email_cc = fromaddress.email_cc;
                objMappable.email_bcc = fromaddress.email_bcc;
            }
        }
    }
}