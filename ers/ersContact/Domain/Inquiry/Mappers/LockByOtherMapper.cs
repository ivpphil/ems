﻿using ersContact.Domain.Inquiry.Mappables;
using jp.co.ivp.ers.Inquiry;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using System;

namespace ersContact.Domain.Inquiry.Mappers
{
    public class LockByOtherMapper
        : IMapper<ILockByOtherMappable>
    {
        public void Map(ILockByOtherMappable objMappable)
        {
            ErsCtsInquiry register = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryWithCaseNo(objMappable.case_no);

            if (register.lockid != null && register.lockid != Convert.ToInt32(objMappable.user_id))
            {
                objMappable.LockedByOther = true;
            }
            else
            {
                objMappable.LockedByOther = false;
            }
        }
    }
}