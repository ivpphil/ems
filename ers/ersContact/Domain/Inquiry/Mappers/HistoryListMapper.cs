﻿using System.Collections.Generic;
using ersContact.Domain.Inquiry.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.Inquiry;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Inquiry.Mappers
{
    public class HistoryListMapper
        : IMapper<IHistoryListMappable>
    {
        public void Map(IHistoryListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryCriteria();
            criteria.case_no = objMappable.case_no;
            criteria.site_id = objMappable.site_id;
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            objMappable.historyList = this.FindDetail(repository, criteria);
        }

        /// <summary>
        /// 問い合わせ詳細取得
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        private List<Dictionary<string, object>> FindDetail(ErsCtsInquiryRepository repository, ErsCtsInquiryCriteria criteria)
        {
            var listDtl = repository.FindDetail(criteria);


            return ErsCommon.ConvertEntityListToDictionaryList(listDtl);
        }
    }
}