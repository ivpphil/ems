﻿using System;
using ersContact.Domain.Inquiry.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersContact.Domain.Inquiry.Mappers
{
    public class CategoryListMapper
        : IMapper<ICategoryListMappable>
    {
        public void Map(ICategoryListMappable objMappable)
        {
            var list = ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT_NAME, EnumCtsEnquiryCategoryColumnName.namename);

            if (list != null)
            {
                foreach (var itm in list)
                {
                    switch (Convert.ToInt32(itm["value"]))
                    {
                        case 1:
                            objMappable.showCat1 = true;
                            break;
                        case 2:
                            objMappable.showCat2 = true;
                            break;
                        case 3:
                            objMappable.showCat3 = true;
                            break;
                        case 4:
                            objMappable.showCat4 = true;
                            break;
                        case 5:
                            objMappable.showCat5 = true;
                            break;
                    }
                }
                objMappable.showCatLabel = true;
            }
            else
            {
                objMappable.showCatLabel = false;
            }
        }
    }
}