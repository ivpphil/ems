﻿using ersContact.Domain.Inquiry.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.Inquiry;
using System.Collections.Generic;

namespace ersContact.Domain.Inquiry.Mappers
{
    public class SearchCaseListMapper
        : IMapper<ISearchCaseListMappable>
    {
        public void Map(ISearchCaseListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryCriteria();

            if (objMappable.search)
            {
                if (objMappable.caseno_srch != null)
                {
                    criteria.case_no = objMappable.caseno_srch;
                }
                if (objMappable.enq_status_srch > 0)
                {
                    criteria.enq_status = objMappable.enq_status_srch;
                }
                if (objMappable.enq_progress_srch > 0)
                {
                    criteria.enq_progress = objMappable.enq_progress_srch;
                }
                if (objMappable.cate1_srch > 0)
                {
                    criteria.cate1 = objMappable.cate1_srch;
                }
                if (objMappable.cate2_srch > 0)
                {
                    criteria.cate2 = objMappable.cate2_srch;
                }
                if (objMappable.cate3_srch > 0)
                {
                    criteria.cate3 = objMappable.cate3_srch;
                }
                if (objMappable.cate4_srch > 0)
                {
                    criteria.cate4 = objMappable.cate4_srch;
                }
                if (objMappable.cate5_srch > 0)
                {
                    criteria.cate5 = objMappable.cate5_srch;
                }
                if (!string.IsNullOrEmpty(objMappable.keywords_srch))
                {
                    criteria.keywords_search = objMappable.keywords_srch;
                }
                if (objMappable.mcode.HasValue())
                {
                    criteria.mcode = objMappable.mcode;
                }

                objMappable.searchcase = objMappable.search;
                objMappable.caselist = objMappable.search;
            }
            criteria.site_id = objMappable.site_id;

            objMappable.recordCount = repository.GetRecordCountInqCase(criteria);

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;
            }
            objMappable.pagerPageCount = pagerPageCount;

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            var list = repository.FindInqCase(criteria);

            this.GetStatusName(list);

            objMappable.inquiryList = ErsCommon.ConvertEntityListToDictionaryList(list);
        }

        protected void GetStatusName(IList<ErsCtsInquiryScreen> list)
        {
            foreach(var ret in list)
            {
                var StatusNo = ret.enq_status.Value;
                ret.enq_status_name = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ENQSTS, EnumCommonNameColumnName.namename, (int?)StatusNo);
            }
        }
    }
}