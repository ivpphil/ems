﻿using System.Collections.Generic;
using ersContact.Domain.Inquiry.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using jp.co.ivp.ers.state;

namespace ersContact.Domain.Inquiry.Mappers
{
    public class EscalateListMapper
        : IMapper<IEscalateListMappable>
    {
        public void Map(IEscalateListMappable objMappable)
        {
            this.loadAgentId(objMappable);

            //エスカレーションメール登録時
            if (objMappable.doEscalate)
            {
                objMappable.escalatetoList = GetClientEscList();

            }
            else
            {
                //ステータス
                switch (objMappable.enq_situation)
                {
                    case EnumEnqSituation.AGEscalation:
                    case EnumEnqSituation.SVEscalation:
                        objMappable.escalatetoList = GetAgentEscList(objMappable);
                        break;
                    case EnumEnqSituation.ClientEscalation:
                        objMappable.escalatetoList = GetClientEscList();
                        break;
                }
            }
        }

        private List<Dictionary<string, object>> GetClientEscList()
        {
            return ErsFactory.ersViewServiceFactory.GetErsViewCtsClientEscalationService().GetCtsClientEscalationList();
        }

        private List<Dictionary<string, object>> GetAgentEscList(IEscalateListMappable objMappable)
        {
            return ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentService().SelectAsList((EnumEnqSituation)objMappable.enq_situation);
        }

        private void loadAgentId(IEscalateListMappable objMappable)
        {
            var controller = objMappable.controller as ErsControllerSecureContact;

            if (controller != null)
            {
                objMappable.agent_id = ((ErsControllerSecureContact)objMappable.controller).cts_User_ID;
            }
        }
    }
}