﻿using System.Collections.Generic;
using ersContact.Domain.Inquiry.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using System;

namespace ersContact.Domain.Inquiry.Mappers
{
    public class InquiryListMapper
        : IMapper<IInquiryListMappable>
    {
        public void Map(IInquiryListMappable objMappable)
        {
            objMappable.inquiryList = this.GetInquiryList(objMappable);
        }

        private List<Dictionary<string, object>> GetInquiryList(IInquiryListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryCriteria();

            if (objMappable.needsup)
            {
                var user_id = new[] { "0" };
                if (objMappable.user_id.HasValue())
                {
                    user_id = new[] { "0", objMappable.user_id };
                }

                criteria.esc_id_in = user_id;
                criteria.enq_progress = EnumEnqProgress.Open;
            }

            if (!string.IsNullOrEmpty(objMappable.mcode))
            {
                criteria.mcode = objMappable.mcode;
            }

            criteria.site_id = objMappable.site_id;            
                                   
            if (objMappable.needsup)
            {
                criteria.AddOrderBy("utime", Criteria.OrderBy.ORDER_BY_DESC);
            }
            else
            {
                objMappable.recordCount = repository.GetRecordCountHeader(criteria);
                long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;

                if (objMappable.recordCount % objMappable.maxItemCount > 0)
                {
                    pagerPageCount += 1;
                }

                objMappable.pagerPageCount = pagerPageCount;
                objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);
            }

            var list = repository.FindHeader(criteria);

            return ErsCommon.ConvertEntityListToDictionaryList(list);
        }
    }
}