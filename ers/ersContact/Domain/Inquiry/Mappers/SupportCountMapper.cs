﻿using ersContact.Domain.Inquiry.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using System;

namespace ersContact.Domain.Inquiry.Mappers
{
    public class SupportCountMapper
        : IMapper<ISupportCountMappable>
    {
        public void Map(ISupportCountMappable objMappable)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryCriteria();
            criteria.esc_id_in = new[] { "0", objMappable.user_id };
            criteria.enq_progress = EnumEnqProgress.Open;
            criteria.site_id = objMappable.site_id;

            if (objMappable.mcode.HasValue())
            {
                criteria.mcode = objMappable.mcode;
            }

            objMappable.supportCount = repository.GetRecordCountHeader(criteria);
        }
    }
}