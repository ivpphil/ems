﻿using System;
using System.Collections.Generic;
using System.Linq;
using ersContact.Domain.Inquiry.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ersContact.Domain.Inquiry.Mappers
{
    public class MailApproveMapper
        : IMapper<IMailApproveMappable>
    {
        public void Map(IMailApproveMappable objMappable)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryCriteria();
            criteria.email_status = EnumEnqEmailStatus.EmailTray;
            criteria.enq_corresponding = EnumEnqCorresponding.Email;
            criteria.site_id = objMappable.site_id;

            criteria.SetOrderByCaseNo(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderBySubNo(Criteria.OrderBy.ORDER_BY_ASC);

            var list = repository.FindMailApprove(criteria);



            foreach (var record in list)
            {
                var inquiry = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(Convert.ToInt32(record.user_id));

                if (inquiry != null)
                {
                    record.ag_name = inquiry.ag_name;
                }
            }

            objMappable.emailappList = ErsCommon.ConvertEntityListToDictionaryList(list);
        }
    }
}