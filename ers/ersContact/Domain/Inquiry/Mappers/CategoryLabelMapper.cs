﻿using System;
using ersContact.Domain.Inquiry.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersContact.Domain.Inquiry.Mappers
{
    public class CategoryLabelMapper
        : IMapper<ICategoryLabelMappable>
    {
        public void Map(ICategoryLabelMappable objMappable)
        {
            var list = ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT_NAME, EnumCtsEnquiryCategoryColumnName.namename);

            foreach (var itm in list)
            {
                switch (Convert.ToInt32(itm["value"]))
                {
                    case 1:
                        objMappable.catlabel1 = Convert.ToString(itm["name"]);
                        break;
                    case 2:
                        objMappable.catlabel2 = Convert.ToString(itm["name"]);
                        break;
                    case 3:
                        objMappable.catlabel3 = Convert.ToString(itm["name"]);
                        break;
                    case 4:
                        objMappable.catlabel4 = Convert.ToString(itm["name"]);
                        break;
                    case 5:
                        objMappable.catlabel5 = Convert.ToString(itm["name"]);
                        break;
                }
            }
        }
    }
}