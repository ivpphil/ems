﻿using System.Collections.Generic;
using ersContact.Domain.Inquiry.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Inquiry.Mappers
{
    public class IncMailListMapper
        : IMapper<IIncMailListMappable>
    {
        public void Map(IIncMailListMappable objMappable)
        {
            objMappable.inquiryList = this.GetInquiryList(objMappable);
        }

        private List<Dictionary<string, object>> GetInquiryList(IIncMailListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsIncomingMailRepository();
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsIncomingMailCriteria();
            criteria.cts_enquiry_detail_t_id = null; 
            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.active = EnumActive.Active;

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            if (setup.Multiple_sites)
            {
                criteria.site_id = objMappable.site_id;
            }

            var list = repository.Find(criteria);

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].mail_to != null) list[i].mail_to_string = string.Join(",", list[i].mail_to);
                if (list[i].mail_cc != null) list[i].mail_cc_string = string.Join(",", list[i].mail_cc);
                if (list[i].mail_bcc != null) list[i].mail_bcc_string = string.Join(",", list[i].mail_bcc);
                if (!string.IsNullOrEmpty(list[i].mcode)) list[i].mail_from = this.MemberName(list[i].mcode);
            }

            return ErsCommon.ConvertEntityListToDictionaryList(list);
        }

        /// <summary>
        /// 会員名の取得
        /// </summary>
        /// <param name="mcode"></param>
        /// <returns></returns>
        string MemberName(string mcode)
        {
            string name = "";

            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode, true);
            if (member != null) name = member.fname.Trim() + " " + member.lname.Trim();
            
            return name;
        }
    }
}