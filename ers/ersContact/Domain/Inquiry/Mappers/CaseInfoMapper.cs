﻿using System;
using System.Text.RegularExpressions;
using ersContact.Domain.Inquiry.Mappables;
using jp.co.ivp.ers.clientescalation;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.incomingmail;
using jp.co.ivp.ers.Inquiry;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;

namespace ersContact.Domain.Inquiry.Mappers
{
    public class CaseInfoMapper
        : IMapper<ICaseInfoMappable>
    {
        public void Map(ICaseInfoMappable objMappable)
        {
            if ((objMappable.doEscalate &&
                (objMappable.enq_situation == EnumEnqSituation.ClientEscalation || objMappable.esc_mail_id > 0)))
            {
                this.LoadClientEscInfo(objMappable);
            }
            else if (objMappable.showincmail && objMappable.mail_id > 0) //email escalation from incoming mail
            {
                this.LoadIncMailEscInfo(objMappable);
            }
            else
            {
                this.reset(objMappable);
                if (!objMappable.execdone && !objMappable.mailtemplate || (objMappable.execdone && objMappable.submit_update))
                    this.LoadRegisterInfo(objMappable);
            }
        }

        private void LoadClientEscInfo(ICaseInfoMappable objMappable)
        {
            ErsCtsClientEscalation escLst = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationWithID(Int32.Parse(objMappable.esc_mail_id.ToString()));
            if (escLst != null)
            {
                objMappable.email_from_esc = escLst.email_from;
                objMappable.email_to = escLst.email_to;
                objMappable.email_cc = escLst.email_cc;
                objMappable.email_bcc = escLst.email_bcc;
                objMappable.email_header_esc = escLst.email_header;
                objMappable.email_body_esc = escLst.email_body;
                objMappable.email_fotter_esc = escLst.email_fotter;
            }
        }

        private void LoadIncMailEscInfo(ICaseInfoMappable objMappable)
        {
            ErsCtsIncomingMail incmailLst = ErsFactory.ersCtsInquiryFactory.GetErsCtsIncomingMailWithID(Int32.Parse(objMappable.mail_id.ToString()));
            if (incmailLst != null)
            {

                objMappable.email_from_esc = (!string.IsNullOrEmpty(incmailLst.mcode)) ? this.MemberName(incmailLst.mcode) : incmailLst.mail_from;
                objMappable.email_to = incmailLst.mail_to;
                objMappable.email_cc = incmailLst.mail_cc;
                objMappable.email_bcc = incmailLst.mail_bcc;
                objMappable.email_title_esc = incmailLst.mail_title;
                objMappable.email_body_esc = incmailLst.mail_body;
            }
        }

        private void LoadRegisterInfo(ICaseInfoMappable objMappable)
        {
            objMappable.showRegBtn = true;

            if ((objMappable.enq_corresponding == EnumEnqCorresponding.Email && objMappable.showmailapp) || (objMappable.enq_type == EnumEnqType.InboundEmail)
                || objMappable.edit_dtl)
            {
                var rep = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
                var cri = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryCriteria();

                if (objMappable.case_no != null && objMappable.case_no > 0)
                    cri.case_no = objMappable.case_no;
                if (objMappable.sub_no > 0)
                    cri.sub_no = objMappable.sub_no;
                if (objMappable.mcode != null)
                    cri.mcode = objMappable.mcode;
                cri.SetOrderBySubNo(Criteria.OrderBy.ORDER_BY_DESC);

                var list = rep.FindDetail(cri);

                if (list.Count > 0)
                {
                    if (list[0] != null)
                    {
                        SetRegister(list[0], objMappable);
                    }
                }
            }
            else if (objMappable.case_no != null && objMappable.case_no > 0 && !(objMappable.mailescreset || objMappable.mailescsend || objMappable.mailsent))
            {
                ErsCtsInquiryScreen register = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryScreenWithCaseNo(objMappable.case_no);
                if (register != null)
                {
                    SetRegister(register, objMappable);
                }
            }

            if (!objMappable.mailsent && !objMappable.reset)
            {
                var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objMappable.mcode, true);
                if (member != null)
                {
                    objMappable.fname = member.fname;
                    objMappable.lname = member.lname;
                }
                if (objMappable.intime == null)
                { objMappable.intime = DateTime.Now; }
                if (objMappable.utime == null)
                { objMappable.utime = DateTime.Now; }

                if (objMappable.starttime == null) objMappable.starttime = DateTime.Now;
                if (objMappable.finishtime == null) objMappable.finishtime = DateTime.Now;

            }
        }

        private void reset(ICaseInfoMappable objMappable)
        {
            if (objMappable.execdone)
            {
                objMappable.temppres = false;
                objMappable.phonereg = false;
                objMappable.mailtray = false;
                objMappable.mailreg = false;
                objMappable.memoreg = false;
                objMappable.setstarttime = false;
                objMappable.setfinishtime = false;
                objMappable.showLock = (objMappable.lockid == null);
                objMappable.showRelease = (Convert.ToInt32(objMappable.user_id) == objMappable.lockid);
            }
        }

        /// <summary>
        /// 会員名の取得
        /// </summary>
        /// <param name="mcode"></param>
        /// <returns></returns>
        string MemberName(string mcode)
        {
            string name = "";

            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode, true);
            if (member != null) name = member.fname.Trim() + " " + member.lname.Trim();

            return name;
        }

        /// <summary>
        /// modelに情報をセット
        /// </summary>
        /// <param name="register"></param>
        private void SetRegister(ErsCtsInquiryScreen register, ICaseInfoMappable objMappable)
        {
            objMappable.intime = register.intime;
            objMappable.utime = register.utime;
            objMappable.mcode = register.mcode;
            objMappable.enq_type = register.enq_type;
            objMappable.enq_status = register.enq_status;
            objMappable.enq_progress = register.enq_progress;
            objMappable.enq_situation = register.enq_situation;
            objMappable.enq_priorty = register.enq_priorty;
            objMappable.enq_casename = register.enq_casename;
            objMappable.cate1 = register.cate1;
            objMappable.cate2 = register.cate2;
            objMappable.cate3 = register.cate3;
            objMappable.cate4 = register.cate4;
            objMappable.cate5 = register.cate5;
            objMappable.user_id = register.user_id;
            objMappable.lockid = register.lockid;
            objMappable.showLock = string.IsNullOrEmpty(register.lockid.ToString());
            objMappable.showRelease = !string.IsNullOrEmpty(register.lockid.ToString());
            objMappable.ag_name = register.ag_name;
            objMappable.showRegBtn = !(objMappable.lockid != null && objMappable.lockid != Convert.ToInt32(objMappable.user_id));
            objMappable.esc_id = register.esc_id;


            if (((objMappable.enq_corresponding == EnumEnqCorresponding.Email && objMappable.showmailapp) 
                || (objMappable.enq_type == EnumEnqType.InboundEmail) && !objMappable.mailsent)
                || objMappable.edit_dtl)
                {
                objMappable.email_from = register.email_from;
                objMappable.email_title = register.email_title;
                objMappable.email_to = register.email_to;
                objMappable.email_cc = register.email_cc;
                objMappable.email_bcc = register.email_bcc;

                //#19858 emailの場合に入るロジックなので、インバウンドタイプを考慮する必要はなし
                //if (objMappable.enq_type != EnumEnqType.InboundEmail)
                if ((objMappable.enq_corresponding == EnumEnqCorresponding.Email && objMappable.showmailapp)
                    || (objMappable.enq_corresponding == EnumEnqCorresponding.Email && objMappable.edit_dtl))
                {
                    objMappable.email_header = register.email_header;
                    objMappable.email_body = register.email_body;
                    objMappable.email_fotter = register.email_fotter;
                    objMappable.email_from_name = register.email_from_name;
                    objMappable.enq_progress = register.enq_progress;
                    objMappable.enq_situation = register.enq_situation;
                    //}
                    //else
                    //{
                    if (!string.IsNullOrEmpty(register.email_title))
                    {
                        Regex objReg = new Regex("\\(([0-9]*?)\\)");
                        var exist = objReg.IsMatch(register.email_title + "");
                        if (!exist)
                        {
                            objMappable.email_title = string.Format("{0} ({1})", register.email_title.Trim(), objMappable.case_no);
                        }
                    }
                    objMappable.sub_no = register.sub_no;

                    if (objMappable.enq_corresponding == EnumEnqCorresponding.Phone && !objMappable.setstarttime && !objMappable.setfinishtime)
                        objMappable.enq_corresponding = EnumEnqCorresponding.Email;
                }

                if (objMappable.edit_dtl && objMappable.enq_corresponding == EnumEnqCorresponding.Memo)
                {
                    objMappable.memo = register.memo;
                }

                if (objMappable.edit_dtl)
                {
                    objMappable.corresponding = register.corresponding;
                    objMappable.starttime = register.starttime;
                    objMappable.finishtime = register.finishtime;
                    objMappable.enq_detail = register.enq_detail;
                    objMappable.ans_detail = register.ans_detail;
                }
            }
        }
    }
}