﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Inquiry.Mappables
{
    public interface IFromAddressMappable
        : IMappable, IErsModelBase
    {
        List<Dictionary<string, object>> frmList { set; }
        long supportCount { set; }
        string email_from_name { get; set; }
        string[] email_cc { set; }
        string[] email_bcc { set; }
        bool mailtemplate { get; }
        int? site_id { get; }
    }
}
