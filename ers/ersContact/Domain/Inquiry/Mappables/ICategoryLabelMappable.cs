﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Inquiry.Mappables
{
    public interface ICategoryLabelMappable
        : IMappable
    {
        string catlabel1 { get; set; }
        string catlabel2 { get; set; }
        string catlabel3 { get; set; }
        string catlabel4 { get; set; }
        string catlabel5 { get; set; }
    }
}
