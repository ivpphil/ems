﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersContact.Domain.Inquiry.Mappables
{
    public interface IInquiryListMappable
        : IMappable
    {
        ErsPagerModel pager { get; }
        bool needsup { get; }
        string user_id { get; }
        string mcode { get; }
        long recordCount { get; set; }
        long maxItemCount { get; }
        List<Dictionary<string, object>> inquiryList { set; }

        long pagerPageCount { get; set; }
        int? site_id { get; }
    }
}
