﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Inquiry.Mappables
{
    public interface IIncMailListMappable
        : IMappable
    {
        List<Dictionary<string, object>> inquiryList { set; }
        int? site_id { get; }
    }
}
