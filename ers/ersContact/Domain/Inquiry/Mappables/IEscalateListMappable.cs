﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersContact.Domain.Inquiry.Mappables
{
    public interface IEscalateListMappable
        : IMappable
    {
        bool doEscalate { get; }
        EnumEnqSituation? enq_situation { get; }
        List<Dictionary<string, object>> escalatetoList { set; }

        bool newcase { get; set; }
        int agent_id {  set; }
    }
}
