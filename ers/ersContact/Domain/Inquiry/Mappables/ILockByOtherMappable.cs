﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Inquiry.Mappables
{
    public interface ILockByOtherMappable
        : IMappable
    {
        int? case_no { get; }
        string user_id { get; }
        bool LockedByOther { set; }
    }
}
