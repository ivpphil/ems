﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersContact.Domain.Inquiry.Mappables
{
    public interface ICaseInfoMappable
        : IMappable
    {
        bool doEscalate { get; }
        bool showincmail { get; }
        bool execdone { get; }
        bool mailtemplate { get; }
        bool showmailapp { get; }
        bool temppres { set; }
        bool phonereg { set; }
        bool mailtray { set; }
        bool mailreg { set; }
        bool memoreg { set; }
        bool setstarttime { get; set; }
        bool setfinishtime { get; set; }
        bool showLock { set; }
        bool showRelease { set; }
        bool showRegBtn { set; }
        bool mailsent { get; set; }
        bool mailescreset { get; }
        bool mailescsend { get; }
        bool reset { get; }
        int mail_id { get; }
        EnumEnqType? enq_type { get; set; }
        EnumEnqStatus? enq_status { set; }
        EnumEnqProgress? enq_progress { set; }
        EnumEnqSituation? enq_situation { get; set; }
        EnumEnqPriority? enq_priorty { set; }
        int? case_no { get; }
        int? sub_no { get; set; }
        int cate1 { set; }
        int cate2 { set; }
        int cate3 { set; }
        int cate4 { set; }
        int cate5 { set; }
        int? esc_mail_id { get; }
        string user_id { get; set; }
        int? lockid { get; set; }
        string enq_detail { set; }
        string ans_detail { set; }
        EnumEnqCorresponding? enq_corresponding { get; set; }
        EnumCorresponding? corresponding { get; set; }
        string email_from_esc { set; }
        string email_header_esc { set; }
        string email_title_esc { set; }
        string email_body_esc { set; }
        string email_fotter_esc { set; }
        string mcode { get; set; }
        string enq_casename { set; }
        string ag_name { set; }
        string email_from { set; }
        string email_title { set; }
        string[] email_to { get; set; }
        string[] email_cc { get; set; }
        string[] email_bcc { get; set; }
        string email_header { set; }
        string email_body { set; }
        string email_fotter { set; }
        string email_from_name { set; }
        string fname { set; }
        string lname { set; }
        DateTime? intime { set; get; }
        DateTime? utime { set; get; }
        DateTime? starttime { set; get; }
        DateTime? finishtime { set; get; }
        List<Dictionary<string, object>> inquiryList { set; }
        string memo { get; set; }

        string esc_id { get; set; }
        bool edit_dtl { get; }
        bool submit_update { get; }
    }
}
