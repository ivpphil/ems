﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Inquiry.Mappables
{
    public interface ICategoryListMappable
        : IMappable
    {
        bool showCat1 { get; set; }
        bool showCat2 { get; set; }
        bool showCat3 { get; set; }
        bool showCat4 { get; set; }
        bool showCat5 { get; set; }
        bool showCatLabel { get; set; }
    }
}
