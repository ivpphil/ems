﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersContact.Domain.Inquiry.Mappables
{
    public interface ISearchCaseListMappable
        : IMappable
    {
        bool search { get; }
        int? caseno_srch { get; }
        EnumEnqStatus? enq_status_srch { get; }
        EnumEnqProgress? enq_progress_srch { get; }
        int cate1_srch { get; }
        int cate2_srch { get; }
        int cate3_srch { get; }
        int cate4_srch { get; }
        int cate5_srch { get; }
        string keywords_srch { get; }
        bool searchcase { set; }
        bool caselist { set; }
        List<Dictionary<string, object>> inquiryList { set; }

        long recordCount { get; set; }

        long maxItemCount { get; }

        long pagerPageCount { get; set; }

        string mcode { get; set; }
        int? site_id { get;}
    }
}
