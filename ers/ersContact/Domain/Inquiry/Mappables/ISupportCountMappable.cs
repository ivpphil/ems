﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Inquiry.Mappables
{
    public interface ISupportCountMappable
        : IMappable
    {
        string user_id { get; }
        long supportCount { set; }
        int? site_id { get; }
        string mcode { get; }
    }
}
