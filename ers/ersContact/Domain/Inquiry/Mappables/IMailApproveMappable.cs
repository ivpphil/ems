﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Inquiry.Mappables
{
    public interface IMailApproveMappable
        : IMappable
    {
        List<Dictionary<string, object>> emailappList { set; }
        int? site_id { get;}
    }
}
