﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Configuration.Commands
{
    public interface ICtsEscalationUpdateCommand
        : ICommand, IErsModelBase
    {
        bool save { get; }
        bool delete { get; }
        int? id { get; }
        string email_from_name { get; set; }
        string email_from { get; set; }
        bool active_string { get; set; }
        string esc_name { get; set; }
        string[] email_to { get; set; }
        string[] email_cc { get; set; }
        string[] email_bcc { get; set; }
        EnumActive? active { get; set; }
    }
}