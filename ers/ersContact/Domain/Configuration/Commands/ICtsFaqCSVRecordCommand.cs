﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Configuration.Commands
{
    public interface ICtsFaqCSVRecordCommand
        : ICommand
    {
        string faq_casename { get; set; }
        int? cate1 { get; set; }
        int? cate2 { get; set; }
        int? cate3 { get; set; }
        int? cate4 { get; set; }
        int? cate5 { get; set; }
        string faq_text { get; set; }
        string faq_text2 { get; set; }
    }
}