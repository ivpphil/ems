﻿using System.Collections.Generic;
using ersContact.Models.configuration;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Configuration.Commands
{
    public interface ICtsCategoryUpdateRecordCommand
        : ICommand, IErsModelBase
    {
        int? disp_order { get; set; }
        string namename { get; set; }
        List<CtsConfigCategoryListData> categoryList { get; set; }
    }
}