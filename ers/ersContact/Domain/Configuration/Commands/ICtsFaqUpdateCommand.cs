﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Configuration.Commands
{
    public interface ICtsFaqUpdateCommand
        : ICommand
    {
        int replace { get; }
        int? ctsUserID { get; }
        ErsCsvContainer<ersContact.Models.csv.ctsFaq_csv> csv_file { get; set; }
        bool chk_find { get; set; }
        bool regist { get; set; }
    }
}