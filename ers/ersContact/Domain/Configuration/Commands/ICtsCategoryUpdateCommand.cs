﻿using System.Collections.Generic;
using ersContact.Models.configuration;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Configuration.Commands
{
    public interface ICtsCategoryUpdateCommand
        : ICommand, IErsModelBase
    {
        bool batch { get; }
        bool modify { get; }
        int? id_main { get; set; }
        int disp_order { get; set; }
        string namename_main { get; set; }
        string namename { get; set; }
        EnumActive active { get; set; }
        EnumCtsEnquiryCategoryType type_code { get; set; }
        List<CtsConfigCategoryListData> categoryList { get; set; }
    }
}