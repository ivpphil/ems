﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersContact.Domain.Configuration.Mappables
{
    public interface ICtsEmailFromListMappable
        : IMappable
    {
        ErsPagerModel pager { get; }

        long recordCount { get; set; }

        long pagerPageCount { get; set; }

        long maxItemCount { get; }

        List<Dictionary<string, object>> fromAddressList { get; set; }
    }
}