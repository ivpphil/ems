﻿using System.Collections.Generic;
using ersContact.Models.configuration;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Configuration.Mappables
{
    public interface ICtsCategoryMappable
        : IMappable
    {
        int? id_main { get; set; }
        string namename_main { get; set; }
        EnumActive active { get; set; }
        EnumCtsEnquiryCategoryType type_code { get; set; }
        List<CtsConfigCategoryListData> categoryList { get; set; }
    }
}