﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Configuration.Mappables
{
    public interface ICtsEscalationDetailMappable
        : IMappable, IErsModelBase
    {
        bool regist { get; set; }
        bool modify { get; set; }
        bool reset { get; set; }
        int? id { get; set; }
        EnumActive? active { get; set; }
        string[] email_to { get; set; }
        string[] email_cc { get; set; }
        string[] email_bcc { get; set; }
    }
}