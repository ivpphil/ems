﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Configuration.Mappables
{
    public interface ICtsCategoryListMappable
        : IMappable
    {
        List<Dictionary<string, object>> categoryList { get; set; }
    }
}