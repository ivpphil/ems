﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Configuration.Mappables
{
    public interface ICtsFaqCSVDownMappable
        : IMappable
    {
        ErsCsvCreater csvCreater { get; set; }
    }
}