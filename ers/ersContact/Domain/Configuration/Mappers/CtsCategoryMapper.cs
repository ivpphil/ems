﻿using System.Collections.Generic;
using ersContact.Domain.Configuration.Mappables;
using ersContact.Models.configuration;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersContact.Domain.Configuration.Mappers
{
    public class CtsCategoryMapper
        : IMapper<ICtsCategoryMappable>
    {
        public void Map(ICtsCategoryMappable objMappable)
        {
            //ヘッダー
            if (objMappable.id_main != null) { 
            var objCategory = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryWithId(objMappable.id_main.Value);
                if (objCategory != null) { 
                    objMappable.namename_main = objCategory.namename;
                    objMappable.active = objCategory.active;
                }
            }

            //カテゴリ
            var repository = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryRepository();
            var criteria = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryCriteria();
            criteria.type_code = objMappable.type_code;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByCode(Criteria.OrderBy.ORDER_BY_ASC);

            objMappable.categoryList = new List<CtsConfigCategoryListData>();

            var listCategory = repository.Find(criteria);

            foreach (var category in listCategory)
            {
                var listData = new CtsConfigCategoryListData();
                listData.OverwriteWithParameter(category.GetPropertiesAsDictionary());

                objMappable.categoryList.Add(listData);
            }
        }
    }
}