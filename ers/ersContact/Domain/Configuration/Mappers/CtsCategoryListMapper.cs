﻿using ersContact.Domain.Configuration.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Configuration.Mappers
{
    public class CtsCategoryListMapper
        : IMapper<ICtsCategoryListMappable>
    {
        public void Map(ICtsCategoryListMappable objMappable)
        {
            var repository = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryRepository();
            var criteria = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryCriteria();
            criteria.type_code = EnumCtsEnquiryCategoryType.ENQCT_NAME;
            criteria.SetOrderByDispOrder(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetOrderByCode(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            var list = repository.Find(criteria);

            objMappable.categoryList = ErsCommon.ConvertEntityListToDictionaryList(list);
        }
    }
}