﻿using System;
using ersContact.Domain.Configuration.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.fromaddress;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersContact.Domain.Configuration.Mappers
{
    public class CtsEmailFromDetailMapper
        : IMapper<ICtsEmailFromDetailMappable>
    {
        public void Map(ICtsEmailFromDetailMappable objMappable)
        {
            if (objMappable.regist)
            {
                objMappable.id = 0;
                objMappable.active = EnumActive.Active;
                objMappable.email_from_name = "";
                objMappable.email_from = "";
                objMappable.email_header = "";
                objMappable.email_body = "";
                objMappable.email_fotter = "";

            }

            if (objMappable.modify || objMappable.reset)
            {
                if (objMappable.id > 0)
                {
                    ErsCtsFromAddress fromaddress = ErsFactory.ersCtsInquiryFactory.GetErsCtsFromAddressWithID(Int32.Parse(objMappable.id.ToString()));
                    objMappable.OverwriteWithParameter(fromaddress.GetPropertiesAsDictionary());
                }
                else
                {
                    objMappable.id = 0;
                    objMappable.active = EnumActive.Active;
                    objMappable.email_from_name = "";
                    objMappable.email_from = "";
                    objMappable.email_header = "";
                    objMappable.email_body = "";
                    objMappable.email_fotter = "";
                }
            }
        }
    }
}