﻿using System;
using ersContact.Domain.Configuration.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.clientescalation;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersContact.Domain.Configuration.Mappers
{
    public class CtsEscalationDetailMapper
        : IMapper<ICtsEscalationDetailMappable>
    {
        public void Map(ICtsEscalationDetailMappable objMappable)
        {
            if (objMappable.regist)
            {
                objMappable.id = 0;
                objMappable.active = EnumActive.Active;
            }

            if (objMappable.modify || objMappable.reset)
            {
                ErsCtsClientEscalation clientescalation = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationWithID(Int32.Parse(objMappable.id.ToString()));
                objMappable.OverwriteWithParameter(clientescalation.GetPropertiesAsDictionary());
            }
        }
    }
}