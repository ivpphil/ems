﻿using System;
using ersContact.Domain.Configuration.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersContact.Domain.Configuration.Mappers
{
    public class CtsFaqCSVDownMapper
        : IMapper<ICtsFaqCSVDownMappable>
    {
        public void Map(ICtsFaqCSVDownMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
        }

        public void CreateCsvFile(ICtsFaqCSVDownMappable objMappable)
        {
            var repositry = ErsFactory.ersCtsFAQFactory.GetErsCtsFAQRepository();
            var criteria = ErsFactory.ersCtsFAQFactory.GetErsCtsFAQCriteria();
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            var faqList = repositry.Find(criteria);

            var filename = DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";

            ersContact.Models.csv.ctsFaq_csv faq_csv;
            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            objMappable.csvCreater = csvCreater;

            using (var writer = csvCreater.GetWriter(filename))
            {
                csvCreater.WriteCsvHeader<ersContact.Models.csv.ctsFaq_csv>(writer);

                foreach (var item in faqList)
                {
                    faq_csv = new ersContact.Models.csv.ctsFaq_csv();
                    faq_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());

                    csvCreater.WriteBody(faq_csv, writer);
                }
            }
        }
    }
}