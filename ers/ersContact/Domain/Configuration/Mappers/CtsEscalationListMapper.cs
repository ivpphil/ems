﻿using ersContact.Domain.Configuration.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Configuration.Mappers
{
    public class CtsEscalationListMapper
        : IMapper<ICtsEscalationListMappable>
    {
        public void Map(ICtsEscalationListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationRepository();
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationCriteria();
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            objMappable.recordCount = repository.GetRecordCount(criteria);

            if(objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            objMappable.pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;

            if(objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                objMappable.pagerPageCount += 1;
            }


            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);

            objMappable.fromAddressList = ErsCommon.ConvertEntityListToDictionaryList(repository.Find(criteria));
        }
    }
}