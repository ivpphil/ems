﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ersContact.Domain.Configuration.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersContact.Domain.Configuration.Handlers
{
    public class ValidateCtsFaqCSV
        : IValidationHandler<ICtsFaqUpdateCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICtsFaqUpdateCommand command)
        {
            var controller = command.controller;

            if (command.csv_file.csv_file == null)
            {
                //アップロードファイルを指定してください。
                //Specify the uploaded file
                throw new ErsException("10202");
            }

            foreach (var model in command.csv_file.GetValidatedModels(command.chk_find))
            {
                model.AddInvalidField(controller.commandBus.Validate<ICtsFaqCSVRecordCommand>(model));

                if (!model.IsValid)
                {
                    foreach (var errorMessage in model.GetAllErrorMessageList())
                    {
                        yield return new ValidationResult(errorMessage, new[] { "csv_file" });
                    }
                    command.csv_file.MarkRecordAsInvalid(model);
                }
            }

            if (command.regist)
            {
                //保持された登録情報が無い場合、エラーメッセージを表示。
                if (command.csv_file.validIndexes.Count() == 0 && command.IsValid)
                {
                    //対象データが存在しません。確認してください。
                    yield return new ValidationResult(ErsResources.GetMessage("10200"), new[] { "csv_file" });
                }
            }
        }
    }
}