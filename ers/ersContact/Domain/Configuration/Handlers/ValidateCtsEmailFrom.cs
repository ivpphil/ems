﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Configuration.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Configuration.Handlers
{
    public class ValidateCtsEmailFrom
        : IValidationHandler<ICtsEmailListUpdateCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICtsEmailListUpdateCommand command)
        {
            if (command.save)
            {
                yield return command.CheckRequired("email_from_name");
                yield return command.CheckRequired("email_from");
                yield return ErsFactory.ersCtsInquiryFactory.GetValidCtsFromAddressStgy().CheckDuplicate(command.id, command.email_from_name);
            }
        }
    }
}