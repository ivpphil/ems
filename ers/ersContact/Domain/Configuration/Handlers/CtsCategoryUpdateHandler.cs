﻿using ersContact.Domain.Configuration.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Configuration.Handlers
{
    public class CtsCategoryUpdateHandler
        : ICommandHandler<ICtsCategoryUpdateCommand>
    {
        public ICommandResult Submit(ICtsCategoryUpdateCommand command)
        {
            if (command.batch)
            {
                this.BatchUpdate(command);
            }

            if (command.modify)
            {
                this.Update(command);
            }

            this.Delete(command);

            return new CommandResult(true);
        }

        internal void BatchUpdate(ICtsCategoryUpdateCommand command)
        {
            if (command.id_main != null) { 
                var repository = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryRepository();

                var CommonType = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryWithId(command.id_main.Value);
                CommonType.namename = command.namename_main;
                CommonType.active = command.active;

                var old_CommonType = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryWithId(command.id_main.Value);

                repository.Update(old_CommonType, CommonType);
            }
        }

        internal void Update(ICtsCategoryUpdateCommand command)
        {
            var repository = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryRepository();

            if (!string.IsNullOrEmpty(command.namename))
            {
                //新規カテゴリ登録
                var CommonType = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategory();
                CommonType.namename = command.namename;
                CommonType.disp_order = command.disp_order;
                CommonType.active = EnumActive.Active;

                CommonType.code = this.GetNewCode(command.type_code);
                CommonType.type_code = command.type_code.ToString();
                CommonType.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
                repository.Insert(CommonType);

                command.disp_order = 0;
                command.namename = "";
            }

            foreach (var record in command.categoryList)
            {
                var CommonType = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryWithId(record.id.Value);
                CommonType.disp_order = record.disp_order;
                CommonType.active = record.active;
                CommonType.namename = record.namename;
                var old_CommonType = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryWithId(record.id.Value);
                repository.Update(old_CommonType, CommonType);
            }
        }

        internal void Delete(ICtsCategoryUpdateCommand command)
        {
            if (command.categoryList == null)
            {
                return;
            }

            foreach (var record in command.categoryList)
            {
                if (record.delete)
                {
                    var repository = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryRepository();
                    var CommonType = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryWithId(record.id.Value);

                    repository.Delete(CommonType);
                }
            }
        }

        internal int GetNewCode(EnumCtsEnquiryCategoryType type_code)
        {
            var repository = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryRepository();
            var criteria = ErsFactory.ersCommonFactory.GetErsCtsEnquiryCategoryCriteria();
            criteria.type_code = type_code;
            criteria.SetOrderByCode(Criteria.OrderBy.ORDER_BY_DESC);

            var list = repository.Find(criteria);

            if (list.Count > 0)
            {
                return list[0].code.Value + 1;
            }

            return 1;
        }
    }
}