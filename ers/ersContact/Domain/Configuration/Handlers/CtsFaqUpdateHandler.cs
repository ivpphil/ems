﻿using System;
using ersContact.Domain.Configuration.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.faq;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Configuration.Handlers
{
    public class CtsFaqUpdateHandler
        : ICommandHandler<ICtsFaqUpdateCommand>
    {
        public ICommandResult Submit(ICtsFaqUpdateCommand command)
        {
            this.Update(command);

            return new CommandResult(true);
        }

        internal void Update(ICtsFaqUpdateCommand command)
        {
            int count = 0;
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var agent = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(Convert.ToInt32(command.ctsUserID));
            if (agent == null)
            {
                throw new ErsException("29002");
            }
            var repository = ErsFactory.ersCtsFAQFactory.GetErsCtsFAQRepository();

            // Truncate Table
            if (command.replace == 1)
            {
                var FAQcrit = ErsFactory.ersCtsFAQFactory.GetErsCtsFAQCriteria();
                FAQcrit.site_id = setup.site_id;
                repository.Delete(FAQcrit);
            }

            foreach (var item in command.csv_file.GetValidModels())
            {
                // Insert
                var FAQ = new ErsCtsFAQ();
                FAQ.OverwriteWithModel(command);

                FAQ.faq_casename = item.faq_casename;
                FAQ.cate1 = item.cate1;
                FAQ.cate2 = item.cate2;
                FAQ.cate3 = item.cate3;
                FAQ.cate4 = item.cate4;
                FAQ.cate5 = item.cate5;
                FAQ.faq_text = item.faq_text;
                FAQ.faq_text2 = item.faq_text2;
                FAQ.intime = DateTime.Now;
                FAQ.utime = DateTime.Now;
                FAQ.user_id = agent.id;
                FAQ.active = EnumActive.Active;
                FAQ.site_id = setup.site_id;
                repository.Insert(FAQ, true);
                count++;
            }

            //保持された登録情報が無い場合、エラーメッセージを表示。
            if (count == 0)
            {
                //対象データが存在しません。確認してください。
                throw new ErsException("10200");
            }
        }
    }
}