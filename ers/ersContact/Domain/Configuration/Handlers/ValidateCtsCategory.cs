﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Configuration.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Configuration.Handlers
{
    public class ValidateCategory
        : IValidationHandler<ICtsCategoryUpdateCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICtsCategoryUpdateCommand command)
        {
            yield return command.CheckRequired("id_main");

            if (command.modify)
            {
                //新規登録
                if (command.namename != "")
                {
                    yield return command.CheckRequired("disp_order");
                }

                var controller = command.controller;
                foreach (var model in command.categoryList)
                {
                    model.AddInvalidField(controller.commandBus.Validate<ICtsCategoryUpdateRecordCommand>(model));

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "categoryList" });
                        }
                    }
                }
            }
        }
    }
}