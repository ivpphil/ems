﻿using System;
using ersContact.Domain.Configuration.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.fromaddress;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Configuration.Handlers
{
    public class CtsEmailListUpdateHandler
        : ICommandHandler<ICtsEmailListUpdateCommand>
    {
        public ICommandResult Submit(ICtsEmailListUpdateCommand command)
        {
            if (command.save)
            {
                if (command.id == 0)
                {
                    this.Insert(command);
                }
                else
                {
                    this.Update(command);
                }
            }
            else if (command.delete)
            {
                this.Delete(command);
            }

            return new CommandResult(true);
        }

        internal void Insert(ICtsEmailListUpdateCommand command)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsFromAddressRepository();

            var FromAddress = new ErsCtsFromAddress();
            FromAddress.OverwriteWithModel(command);
            FromAddress.active = (command.active_string == true) ? EnumActive.Active : EnumActive.NonActive;
            FromAddress.utime = DateTime.Now;
            FromAddress.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            repository.Insert(FromAddress, true);
        }

        internal void Update(ICtsEmailListUpdateCommand command)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsFromAddressRepository();

            command.active = (command.active_string == true) ? EnumActive.Active : EnumActive.NonActive;

            var FromAddress = ErsFactory.ersCtsInquiryFactory.GetErsCtsFromAddressWithID(command.id.Value);
            FromAddress.OverwriteWithModel(command);

            var old_FromAddress = ErsFactory.ersCtsInquiryFactory.GetErsCtsFromAddressWithID(command.id.Value);

            repository.Update(old_FromAddress, FromAddress);
        }

        internal void Delete(ICtsEmailListUpdateCommand command)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsFromAddressRepository();
            var CommonType = ErsFactory.ersCtsInquiryFactory.GetErsCtsFromAddressWithID(command.id.Value);

            repository.Delete(CommonType);
        }
    }
}