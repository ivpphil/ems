﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Configuration.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Configuration.Handlers
{
    public class ValidateCtsEscalation
        : IValidationHandler<ICtsEscalationUpdateCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICtsEscalationUpdateCommand command)
        {
            if (command.save)
            {
                yield return command.CheckRequired("esc_name");
                yield return command.CheckRequired("email_from_name");
                yield return command.CheckRequired("email_from");
                yield return ErsFactory.ersCtsInquiryFactory.GetValidCtsClientEscalationStgy().CheckDuplicate(command.id, command.esc_name);
            }
        }
    }
}