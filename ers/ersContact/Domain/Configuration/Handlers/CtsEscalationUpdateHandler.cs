﻿using System;
using ersContact.Domain.Configuration.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.clientescalation;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Configuration.Handlers
{
    public class CtsEscalationUpdateHandler
        : ICommandHandler<ICtsEscalationUpdateCommand>
    {
        public ICommandResult Submit(ICtsEscalationUpdateCommand command)
        {
            if (command.save)
            {
                if (command.id == 0)
                {
                    this.Insert(command);
                }
                else
                {
                    this.Update(command);
                }
            }
            else if (command.delete)
            {
                this.Delete(command);
            }

            return new CommandResult(true);
        }

        internal void Insert(ICtsEscalationUpdateCommand command)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationRepository();

            var ClientEscalation = new ErsCtsClientEscalation();
            ClientEscalation.OverwriteWithModel(command);
            ClientEscalation.active = (command.active_string == true) ? EnumActive.Active : EnumActive.NonActive;
            ClientEscalation.utime = DateTime.Now;
            ClientEscalation.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            repository.Insert(ClientEscalation, true);
        }

        internal void Update(ICtsEscalationUpdateCommand command)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationRepository();

            command.active = (command.active_string == true) ? EnumActive.Active : EnumActive.NonActive;

            var ClientEscalation = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationWithID(command.id.Value);
            ClientEscalation.OverwriteWithModel(command);

            var old_ClientEscalation = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationWithID(command.id.Value);
            repository.Update(old_ClientEscalation, ClientEscalation);
        }

        internal void Delete(ICtsEscalationUpdateCommand command)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationRepository();
            var CommonType = ErsFactory.ersCtsInquiryFactory.GetErsCtsClientEscalationWithID(command.id.Value);

            repository.Delete(CommonType);
        }
    }
}