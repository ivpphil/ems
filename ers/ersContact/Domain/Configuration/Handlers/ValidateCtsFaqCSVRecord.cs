﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Configuration.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Configuration.Handlers
{
    public class ValidateCtsFaqCSVRecord
        : IValidationHandler<ICtsFaqCSVRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICtsFaqCSVRecordCommand command)
        {
            yield return command.CheckRequired("faq_casename");
            yield return command.CheckRequired("cate1");
            yield return command.CheckRequired("cate2");
            yield return command.CheckRequired("cate3");
            yield return command.CheckRequired("cate4");
            yield return command.CheckRequired("cate5");


            if ((command.cate1 != null) && !Exist_Cate(1, command))
                yield return new ValidationResult(ErsResources.GetMessage("10220", command.faq_casename, 1));

            if ((command.cate2 != null) && !Exist_Cate(2, command))
                yield return new ValidationResult(ErsResources.GetMessage("10220", command.faq_casename, 2));

            if ((command.cate3 != null) && !Exist_Cate(3, command))
                yield return new ValidationResult(ErsResources.GetMessage("10220", command.faq_casename, 3));

            if ((command.cate4 != null) && !Exist_Cate(4, command))
                yield return new ValidationResult(ErsResources.GetMessage("10220", command.faq_casename, 4));

            if ((command.cate5 != null) && !Exist_Cate(5, command))
                yield return new ValidationResult(ErsResources.GetMessage("10220", command.faq_casename, 5));
        }

        [Obsolete("validatorを作成する。")]
        private bool Exist_Cate(int cate_type, ICtsFaqCSVRecordCommand command)
        {
            int? code;
            EnumCtsEnquiryCategoryType type_code;

            switch (cate_type)
            {
                case 1:
                    code = command.cate1;
                    type_code = EnumCtsEnquiryCategoryType.ENQCT1;
                    break;
                case 2:
                    code = command.cate2;
                    type_code = EnumCtsEnquiryCategoryType.ENQCT2;
                    break;
                case 3:
                    code = command.cate3;
                    type_code = EnumCtsEnquiryCategoryType.ENQCT3;
                    break;
                case 4:
                    code = command.cate4;
                    type_code = EnumCtsEnquiryCategoryType.ENQCT4;
                    break;
                case 5:
                    code = command.cate5;
                    type_code = EnumCtsEnquiryCategoryType.ENQCT5;
                    break;
                default:
                    throw new Exception("invalid cate_type");
            }

            if (code == 0)
                return true;
            else
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().ExistValue(type_code, code); ;
        }
    }
}