﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Configuration.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Configuration.Handlers
{
    public class ValidateCategoryRecord
        : IValidationHandler<ICtsCategoryUpdateRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICtsCategoryUpdateRecordCommand command)
        {
            foreach (var result in this.ValidateValue(command))
            {
                yield return result;
            }
        }

        public IEnumerable<ValidationResult> ValidateValue(ICtsCategoryUpdateRecordCommand command)
        {
            yield return command.CheckRequired("namename");
            yield return command.CheckRequired("disp_order");
        }
    }
}