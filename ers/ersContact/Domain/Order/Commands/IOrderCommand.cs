﻿using System;
using System.Collections.Generic;
using ersContact.Models.cart;
using jp.co.ivp.ers;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.order.strategy;

namespace ersContact.Domain.Order.Commands
{
    public interface IOrderCommand
        : ICommand, IOrderIntegratedDateContainer, IPaymentInfoGmoInputContainer, IPaymentInfoGmoConvenienceContainer, IRegistRegularOrderDataSource
    {
        bool order_success { set; }
        bool card_will_add { get; }
        bool IsOrderUpdate { get; }
        bool carriage_recomp_flg { get; }
        bool disable_issue { get; }
        bool isKanshi { get; }
        bool member_register { get; }
        bool shipping_register { get; }
        bool goto_page1 { get; }
        bool goto_page2 { get; }
        bool goto_page3 { get; }
        bool goto_page4 { get; }
        bool cts_order_register { get; }
        bool reaload_cart { set; }
        bool page2 { get; }
        bool coupon_flg { get; }
        bool recompute { get; }
        int Edit_Temp { get; }
        int? ent_point { get; set; }
        int cts_agent_id { get; }
        EnumAgType? cts_order_charge1_ag_type { set; }
        int points_available { get; }
        int regularBasketItemCount { get; }
        int basketItemCount { get; }
        int? card_type { get; }
        int? member_add_id { get; set; }
        int? shipping_id { get; set; }
        int? add_pref { get; set; }
        int? wk_add_pref { get; set; }
        EnumFirstTimeOrdinary? firstTimeOrdinary { get; set; }
        string coupon_code { get; set; }
        int? coupon_discount { get; set; }
        int? carriage { get; set; }
        int? etc { get; set; }
        int? tax { get; set; }
        int? point_ck { set; }
        int? paid_price { set; }
        int? old_coupon_discount { get; set; }
        int? pref { get; }
        int? cts_order_id { get; }
        int? last_used_card_info { get; }
        int? send_chk { get; }
        int? cts_order_temp_d_no { get; }
        string securityno { get; }
        string mcode { get; }
        string address_name { set; }
        string add_lname { get; set; }
        string add_fname { get; set; }
        string add_lnamek { get; set; }
        string add_fnamek { get; set; }
        string add_tel { get; set; }
        string add_zip { get; set; }
        string add_address { get; set; }
        string add_taddress { get; set; }
        string add_maddress { get; set; }
        string wk_add_lname { get; set; }
        string wk_add_fname { get; set; }
        string wk_add_lnamek { get; set; }
        string wk_add_fnamek { get; set; }
        string wk_add_tel { get; set; }
        string wk_add_zip { get; set; }
        string wk_add_address { get; set; }
        string wk_add_taddress { get; set; }
        string wk_add_maddress { get; set; }
        int? member_card_id { get; set; }
        string ordered_user_id { get; }
        string ordered_ag_name { set; }
        string memo2 { set; }
        string order_d_no { get; set; }
        string cts_order_user_id { get; set; }
        string cts_order_charge1_ag_name { set; }
        string lname { get; }
        string fname { get; }
        string lnamek { get; }
        string fnamek { get; }
        string tel { get; }
        string email { get; }
        string zip { get; }
        string address { get; }
        string taddress { get; }
        string cts_order_ransu { get; }
        string ccode { get; }
        string cts_order_memo { get; }
        DateTime? paid_date { set; }
        DateTime? intime { set; }
        EnumSendTo? send { get; set; }
        EnumWrap? wrap { set; }
        EnumOrderPaymentStatusType? order_payment_status { set; }
        EnumSex? sex { get; }
        EnumDmFlg? dm_flg { get; set; }
        EnumOutBoundFlg? out_bound_flg { get; set; }
        EnumAddressAdd address_add { get; }
        ErsMember member { get; }
        ErsBasket basket { get; set; }
        ErsOrderContainer orderContainer { get; set; }
        List<Cart_items> basketItems { get; set; }
        List<Cart_regular_items> regularBasketItems { get; }
        Dictionary<string, int?> up_amount_list { get; }
        IEnumerable<ErsBaskRecord> orderRecords { get; }
        bool IsNonNeededPaymentSpec { get; }

        string ransu { get; set; }

        string memo3 { get; set; }

        string user_id { get; set; }

        string d_no { get; set; }

        EnumDeleted? deleted { get; set; }

        bool card_delete { get; set; }

        bool card_update { get; set; }

        bool ValidateMappedOrder { get; set; }

        string old_coupon_code { get; set; }

        bool page3 { get; set; }

        bool page4 { get; set; }

        int? disp_order_total_amount { get; }

        EnumPmFlg? pm_flg { get; set; }

        bool GetOrderForUpdate { get; set; }

        EnumMallShopKbn? mall_shop_kbn { get; set; }

        EnumDelvMethod? deliv_method { get; set; }

        ErsOrderIntegrated orderIntegrated { get; set; }

        bool reauthed { get; set; }

        bool non_member { get; }

        int? point_magnification { get; set; }

        int? site_id { get; }
        bool regular_recalc { get; } 
    }
}