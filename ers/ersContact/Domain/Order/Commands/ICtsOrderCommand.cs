﻿using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Order.Commands
{
    public interface ICtsOrderCommand : ICommand
    {
        bool goto_page4 { get; }
        bool cts_order_success { set; }
        bool load_new_order { set; }
        int Edit_Temp { get; }
        EnumAgType? cts_order_charge1_ag_type { get; }
        int cts_order_status { get; }
        int? cts_order_id { get; }
        int? cts_order_temp_d_no { get; set; }
        string mcode { get; }
        string cts_order_ransu { get; }
        string lname { get; }
        string fname { get; }
        string lnamek { get; }
        string fnamek { get; }
        string cts_order_memo { get; }
        string cts_order_user_id { get; }
        string tel { get; }
        string cts_order_charge1_ag_name { get; }
        ErsBasket basket { get; set; }
        EnumPmFlg? pm_flg { get; }
        IEnumerable<ErsBaskRecord> orderRecords { get; }
        int? shipping_id { get; set; }
        int? site_id { get; }
        bool IsOrderUpdate { get; }
    }
}