﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Order.Commands
{
    public interface IShippingCommand : ICommand
    {
        bool shipping_delete { get; }
        bool shipping_register { get; set; }
        bool shipping_edit { get; set; }
        int? shipping_id { get; set; }
        string mcode { get; }
        string add_lname { get; }
        string add_fname { get; }
        string address_name { set; }
        EnumAddressAdd address_add { get; }

        string order_d_no { get; set; }

        bool IsOrderUpdate { get; }

        string add_lnamek { get; set; }

        string add_fnamek { get; set; }

        string add_tel { get; set; }

        string add_zip { get; set; }

        int? add_pref { get; set; }

        string add_address { get; set; }

        string add_taddress { get; set; }

        string add_maddress { get; set; }

        EnumDeleted? deleted { get; set; }

        bool non_member { get; set; }

        int? site_id { get; }

        EnumSendTo? send { get; set; }
    }
}