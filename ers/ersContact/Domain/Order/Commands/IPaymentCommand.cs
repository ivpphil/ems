﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Order.Commands
{
    public interface IPaymentCommand : ICommand
    {
        bool card_delete { get; }
        bool card_update { get; }
        string mcode { get; }
        int? card_id { get; }

        string card_holder_name { get; }
        string cardno { get; }
        int? card_type { get; }
        int? validity_y { get; }
        int? validity_m { get; }
    }
}