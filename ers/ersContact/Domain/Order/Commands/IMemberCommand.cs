﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Order.Commands
{
    public interface IMemberCommand : ICommand
    {
        bool member_register { get; set; }
        bool member_edit { get; set; }
        string mcode { get; set; }
        bool non_member { get; }
        string order_d_no { get; }

        string lname { get; set; }
        string fname { get; set; }
        string lnamek { get; set; }
        string fnamek { get; set; }
        string tel { get; set; }
        string zip { get; set; }
        int? pref { get; set; }
        string address { get; set; }
        string taddress { get; set; }
        string maddress { get; set; }
        string email { get; set; }

        string wk_lname { get; set; }
        string wk_fname { get; set; }
        string wk_lnamek { get; set; }
        string wk_fnamek { get; set; }
        string wk_tel { get; set; }
        string wk_zip { get; set; }
        int? wk_pref { get; set; }
        string wk_address { get; set; }
        string wk_taddress { get; set; }
        string wk_maddress { get; set; }
        string wk_email { get; set; }
    }
}