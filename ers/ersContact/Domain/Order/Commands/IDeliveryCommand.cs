﻿using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Order.Commands
{
    public interface IDeliveryCommand : ICommand
    {
        bool page2 { get; }
        bool page3 { get; }
        bool coupon_flg { get; }
        bool carriage_recomp_flg { get; }
        bool IsOrderUpdate { get; }
        bool reaload_cart { set; }
        string mcode { get; }
        string cts_order_ransu { get; }
        ErsBasket basket { get; set; }
        Dictionary<string, int?> up_amount_list { get; }
        Dictionary<string, EnumOrderStatusType> product_status_list { get; }

        bool recalc { get; set; }
    }
}