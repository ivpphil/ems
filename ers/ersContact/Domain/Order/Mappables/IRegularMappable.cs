﻿using System.Collections.Generic;
using ersContact.Models.cart;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Order.Mappables
{
    public interface IRegularMappable
        : IMappable
    {
        bool page2 { get; }
        ErsBasket basket { get; }
        List<Cart_regular_items> regularBasketItems { get; set; }

        bool IsErrorBack { get; set; }
    }
}
