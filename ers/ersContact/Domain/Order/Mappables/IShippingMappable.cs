﻿using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Order.Mappables
{
    public interface IShippingMappable
        : IMappable
    {
        bool shipping_register { get; set; }
        bool shipping_edit { get; set; }
        bool shipping_modify { get; set; }
        bool shipping_new { get; }
        bool shipping_cancel { get; set; }
        bool shipping_delete { get; }
        bool shipping_err { get; }
        int? shipping_id { get; set; }
        int? modify_shipping_id { get; }
        int? add_pref { get; set; }
        int? wk_add_pref { get; set; }
        int? old_shipping_id { get; }
        string mcode { get; }
        string add_lname { get; set; }
        string add_fname { get; set; }
        string add_lnamek { get; set; }
        string add_fnamek { get; set; }
        string add_tel { get; set; }
        string add_zip { get; set; }
        string add_address { get; set; }
        string add_taddress { get; set; }
        string add_maddress { get; set; }
        string wk_add_lname { get; set; }
        string wk_add_fname { get; set; }
        string wk_add_lnamek { get; set; }
        string wk_add_fnamek { get; set; }
        string wk_add_tel { get; set; }
        string wk_add_zip { get; set; }
        string wk_add_address { get; set; }
        string wk_add_taddress { get; set; }
        string wk_add_maddress { get; set; }
        string address_name { set; }
        string tel { get; }
        EnumAddressAdd address_add { get; }
        ErsMember member { get; }
        EnumSendTo? send { get; set; }
        EnumSendTo? old_send { get; }
        List<Dictionary<string, object>> shippingList { set; }

        bool IsOrderUpdate { get; }

        bool goto_page1 { get; set; }

        EnumDeleted? deleted { get; set; }
        bool non_member { get; set; }
        string order_d_no { get; }
        int? send_chk { get; }
    }
}