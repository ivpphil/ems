﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Order.Mappables
{
    public interface IPointMappable
        : IMappable
    {
        bool IsOrderUpdate { get; }
        bool from_page1 { get; }
        int points_available { get; set; }
        int? ent_point { set; }
        int ent_point_orig { get; set; }
        string order_d_no { get; }
        string mcode { get; }
        int? p_service { set; }
        int? site_id { get; }
    }
}
