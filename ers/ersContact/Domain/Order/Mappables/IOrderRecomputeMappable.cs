﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.member;
using ersContact.Models.cart;

namespace ersContact.Domain.Order.Mappables
{
    public interface IOrderRecomputeMappable
        : IMappable, IOrderIntegratedDateContainer, IPaymentInfoGmoInputContainer
    {
        ErsOrderContainer orderContainer { get; set; }
        ErsMember member { get; }
        ErsBasket basket { get; set; }
        string mcode { get; }
        string cts_order_ransu { get; }
        string order_d_no { get; set; }

        bool IsOrderUpdate { get; }
        bool page2 { get; }
        bool carriage_recomp_flg { get; }

        EnumFirstTimeOrdinary? firstTimeOrdinary { get; set; }
        int? ent_point { get; set; }
        string ordered_user_id { get; }
        string ordered_ag_name { get; set; }
        int? shipping_id { get; set; }
        EnumSendTo? send { get; set; }
        int? coupon_discount { get; set; }
        int? carriage { get; set; }
        int? etc { get; set; }
        int? tax { get; set; }

        int? member_add_id { get; set; }
        int? member_card_id { set; }
        string tel { get; }
        string address_name { set; }
        string add_lname { get; set; }
        string add_fname { get; set; }
        string add_lnamek { get; set; }
        string add_fnamek { get; set; }
        string add_tel { get; set; }
        string add_zip { get; set; }
        int? add_pref { get; set; }
        string add_address { get; set; }
        string add_taddress { get; set; }
        string add_maddress { get; set; }
        string wk_add_lname { get; set; }
        string wk_add_fname { get; set; }
        string wk_add_lnamek { get; set; }
        string wk_add_fnamek { get; set; }
        string wk_add_tel { get; set; }
        string wk_add_zip { get; set; }
        int? wk_add_pref { get; set; }
        string wk_add_address { get; set; }
        string wk_add_taddress { get; set; }
        string wk_add_maddress { get; set; }

        bool GetOrderForUpdate { get; set; }

        EnumDelvMethod? deliv_method { get; set; }
        int? site_id { get; }

        string wk_lname { get; set; }
        string wk_fname { get; set; }
        string wk_lnamek { get; set; }
        string wk_fnamek { get; set; }
        string wk_tel { get; set; }
        string wk_zip { get; set; }
        int? wk_pref { get; set; }
        string wk_address { get; set; }
        string wk_taddress { get; set; }
        string wk_maddress { get; set; }
        string wk_email { get; set; }
    }
}