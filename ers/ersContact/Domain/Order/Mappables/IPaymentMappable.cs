﻿using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.order.related;

namespace ersContact.Domain.Order.Mappables
{
    public interface IPaymentMappable
        : IMappable
    {
        bool card_new { get; }
        bool card_cancel { get; set; }
        bool card_delete { get; }
        bool card_will_add { set; }
        int? card_type { get; set; }
        int? validity_y { get; set; }
        int? validity_m { get; set; }
        int? disp_card_type { set; }
        int? disp_validity_y { set; }
        int? disp_validity_m { set; }
        string card_holder_name { get; set; }
        string cardno { get; set; }
        string securityno { get; set; }
        int? card_id { get; set; }
        string disp_card_holder_name { set; }
        string disp_cardno { set; }
        string disp_securityno { set; }
        CreditCardInfo savedCardInfo { get; set; }
        EnumPaymentType? pay { get; set; }
        ErsMember member { get; }
        List<Dictionary<string, object>> membercardList { get; set; }
        List<Dictionary<string, object>> membercardListExtend { get; set; }
    }
}
