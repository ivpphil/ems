﻿using System;
using System.Collections.Generic;
using ersContact.Models.cart;
using jp.co.ivp.ers;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.order.related;

namespace ersContact.Domain.Order.Mappables
{
    public interface IDeliveryMappable
        : IMappable
    {
        bool page2 { get; }
        bool page3 { get; }
        bool IsOrderUpdate { get; }
        bool senddate_add { set; }
        int? send_chk { get; }
        int? shipping_id { get; set; }
        int? disp_card_type { set; }
        int? disp_validity_y { set; }
        int? disp_validity_m { set; }
        int? add_pref { get; set; }
        int? sendtime { set; }
        int? wk_add_pref { get; set; }
        int? card_id { get; set; }
        string card_holder_name { get; set; }
        string cardno { get; set; }
        string address_name { set; }
        string add_lname { get; set; }
        string add_fname { get; set; }
        string add_lnamek { get; set; }
        string add_fnamek { get; set; }
        string add_tel { get; set; }
        string add_zip { get; set; }
        string add_address { get; set; }
        string add_taddress { get; set; }
        string add_maddress { get; set; }
        string tel { get; }
        string senddate_v { get; set; }
        string senddate_t { set; }
        string mcode { get; }
        string disp_card_holder_name { set; }
        string disp_cardno { set; }
        string wk_add_lname { get; set; }
        string wk_add_fname { get; set; }
        string wk_add_lnamek { get; set; }
        string wk_add_fnamek { get; set; }
        string wk_add_tel { get; set; }
        string wk_add_zip { get; set; }
        string wk_add_address { get; set; }
        string wk_add_taddress { get; set; }
        string wk_add_maddress { get; set; }
        CreditCardInfo savedCardInfo { get; set; }
        DateTime? senddate { get; }
        EnumSendTo? send { get; set; }
        ErsMember member { get; }
        ErsBasket basket { get; }
        List<Dictionary<string, object>> senddateList { get; }
        List<Cart_items> basketItems { get; set; }
        string order_d_no { get; set; }

        string lname { get; }
        string fname { get; }
        string lnamek { get; }
        string fnamek { get; }
        string zip { get; }
        int? pref { get; }
        string address { get; }
        string taddress { get; }
        string maddress { get; }

        string wk_lname { get; set; }
        string wk_fname { get; set; }
        string wk_lnamek { get; set; }
        string wk_fnamek { get; set; }
        string wk_zip { get; set; }
        int? wk_pref { get; set; }
        string wk_address { get; set; }
        string wk_taddress { get; set; }
        string wk_maddress { get; set; }
        string wk_tel { get; set; }
    }
}
