﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers;

namespace ersContact.Domain.Order.Mappables
{
    public interface IMemberMappable
        : IMappable
    {
        bool member_register { get; set; }
        bool member_edit { get; set; }
        bool member_cancel { get; set; }
        bool member_modify { get; set; }
        int? member_id { set; }
        int? birthday_y { set; }
        int? birthday_m { set; }
        int? birthday_d { set; }
        string mcode { get; set; }
        string lname { set; get; }
        string fname { set; get; }
        string lnamek { set; get; }
        string fnamek { set; get; }
        string tel { set; get; }
        string zip { set; get; }
        int? pref { set; get; }
        string address { set; get; }
        string taddress { set; get; }
        string maddress { set; get; }
        string email { set; get; }
        string w_dm_flg { set; }
        string w_out_bound_flg { set; }
        ErsMember member { get; set; }
        EnumSex? sex { set; }
        EnumDmFlg? dm_flg { get; set; }
        EnumOutBoundFlg? out_bound_flg { get; set; }

        bool IsOrderUpdate { get; }

        string order_d_no { get; set; }

        EnumDeleted? deleted { get; set; }

        string memo { get; set; }

        bool non_member { get; set; }
        bool page1 { get; set; }
        string add_lname { get; set; }
        string add_fname { get; set; }
        string add_lnamek { get; set; }
        string add_fnamek { get; set; }
        string add_tel { get; set; }
        string add_zip { get; set; }
        int? add_pref { get; set; }
        string add_address { get; set; }
        string add_taddress { get; set; }
        string add_maddress { get; set; }
        bool shipping_register { get; }

        string wk_lname { get; set; }
        string wk_fname { get; set; }
        string wk_lnamek { get; set; }
        string wk_fnamek { get; set; }
        string wk_tel { get; set; }
        string wk_zip { get; set; }
        int? wk_pref { get; set; }
        string wk_address { get; set; }
        string wk_taddress { get; set; }
        string wk_maddress { get; set; }
        string wk_email { get; set; }

        int? site_id { get; }
        bool IsErrorBack { get; set; }
        bool goto_page1 { get; set; }
    }
}