﻿using System;
using jp.co.ivp.ers;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.order;

namespace ersContact.Domain.Order.Mappables
{
    public interface IOrderMappable
        : IMappable
    {
        bool page1 { get; set; }
        bool page2 { get; set; }
        bool page3 { get; set; }
        bool page4 { get; set; }
        bool goto_page1 { get; }
        bool goto_page2 { get; }
        bool goto_page3 { get; }
        bool goto_page4 { get; }
        bool IsOrderUpdate { get; }
        bool reaload_cart { set; }
        int cts_agent_id { get; }
        EnumAgType? cts_order_charge1_ag_type { set; }
        int cts_order_status { set; }
        int? regular_sendtime { get; }
        int cts_order_subtotal { set; }
        int Edit_Temp { get; set; }
        int? cts_order_temp_d_no { get; set; }
        int? cts_order_id { set; }
        int? shipping_id { get; set; }
        int? old_shipping_id { set; }
        int? add_pref { set; }
        int? sendtime { get; set; }
        int? subtotal { set; }
        int? carriage { set; }
        int? etc { set; }
        int? tax { set; }
        int? point_ck { set; }
        int? coupon_discount { set; }
        EnumFirstTimeOrdinary? firstTimeOrdinary { set; }
        string order_d_no { get; }
        string Cts_Ransu { get; }
        string cts_order_ransu { get; set; }
        string cts_order_user_id { set; }
        string cts_order_charge1_ag_name { set; }
        string mcode { get; set; }
        string add_lname { set; }
        string add_fname { set; }
        string add_lnamek { set; }
        string add_fnamek { set; }
        string add_zip { set; }
        string add_address { set; }
        string add_taddress { set; }
        string add_maddress { set; }
        string add_tel { set; }
        int? card_id { get; set; }
        string cts_order_memo { set; }
        string ent_coupon_code { set; }
        string old_coupon_code { set; }
        string coupon_code { set; }
        string ordered_user_id { get; set; }
        string ordered_ag_name { set; }
        string ccode { set; }
        EnumPmFlg? pm_flg { set; }
        EnumSendTo? send { get; set; }
        EnumSendTo? old_send { set; }
        EnumPaymentType? pay { get; set; }
        EnumPaymentType? old_pay { set; }
        DateTime? senddate { set; }
        ErsBasket basket { get; set; }

        EnumDeleted? deleted { get; set; }
        bool non_member { get; set; }

        EnumMallShopKbn? mall_shop_kbn { get; set; }

        EnumConvCode? conv_code { get; set; }

        EnumDelvMethod? deliv_method { get; set; }

        int? p_service { get; set; }

        string lname { get; set; }
        string fname { get; set; }
        string tel { get; set; }
        string zip { get; set; }
        int? pref { get; set; }
        string address { get; set; }
        string taddress { get; set; }
        string maddress { get; set; }

        int? point_magnification { get; set; }
        int? site_id { get; }
        bool last_used_card_flg { get; }
        int? last_used_card_info { get; set; }
        ErsOrderContainer orderContainer { get; set; }
    }
}
