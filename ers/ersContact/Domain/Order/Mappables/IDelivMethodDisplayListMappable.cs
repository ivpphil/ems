﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;
using ersContact.Models.cart;
using jp.co.ivp.ers;

namespace ersContact.Domain.Order.Mappables
{
    public interface IDelivMethodDisplayListMappable
        : IMappable
    {
        List<Cart_items> basketItems { get; set; }

        List<Cart_regular_items> regularBasketItems { get; set; }

        short DisplayedDelvMethod { get; set; }

        int amounttotal_all_item { get; }

        EnumDelvMethod? deliv_method { get; set; }

        bool page3 { get; set; }
    }
}