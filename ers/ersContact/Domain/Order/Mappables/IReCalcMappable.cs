﻿using System.Collections.Generic;
using ersContact.Models.cart;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersContact.Domain.Order.Mappables
{
    public interface IReCalcMappable
        : IMappable
    {
        bool page2 { get; }
        string mcode { get; }
        string cts_order_ransu { get; }
        ErsBasket basket { get; set; }
        List<Cart_regular_items> regularBasketItems { get; set; }

        bool IsOrderUpdate { get; }

        Dictionary<string, EnumOrderStatusType> product_status_list { get; }
    }
}
