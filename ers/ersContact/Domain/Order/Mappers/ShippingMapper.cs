﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Order.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Order.Mappers
{
    public class ShippingMapper
        : IMapper<IShippingMappable>
    {
        public void Map(IShippingMappable objMappable)
        {
            //未登録別住所では登録無しで遷移
            if (objMappable.shipping_id == -1)
            {
                if (objMappable.shipping_register && objMappable.address_add == EnumAddressAdd.Add)
                {
                    //追加登録
                    objMappable.shipping_id = null;
                }
                else if (objMappable.shipping_register)
                {
                    //別お届け先登録しない場合
                    this.AddToWorkAddress(objMappable);
                    if (!objMappable.IsOrderUpdate || objMappable.deleted != EnumDeleted.Deleted)
                    {
                        objMappable.shipping_register = false;
                    }
                    objMappable.shipping_edit = false;
                }
                else if (objMappable.shipping_modify)
                {
                    //編集
                    this.WorkToAddAddress(objMappable);
                }
                else if (objMappable.non_member && !string.IsNullOrEmpty(objMappable.order_d_no) && objMappable.send_chk==1  )
                {
                    //Restore shipping address from hidden
                    this.WorkToAddAddress(objMappable);
                }
            }

            if (objMappable.shipping_new)
            {
                this.ClearShipping(objMappable);
                objMappable.shipping_cancel = false;
                objMappable.shipping_edit = true;
            }
            else if (objMappable.shipping_modify)
            {
                objMappable.shipping_id = objMappable.modify_shipping_id;
                this.LoadShippingDetail(objMappable);
                objMappable.shipping_cancel = false;
                objMappable.shipping_edit = true;
            }
            else if (objMappable.shipping_delete)
            {
                this.LoadShippingAddress(objMappable);
                objMappable.shipping_cancel = true;
                objMappable.shipping_modify = false;
                objMappable.shipping_edit = false;
            }
            else if (objMappable.shipping_register)
            {
                if (this.IsUpdateNoMemberOrder(objMappable))
                {
                    //非会員購入時の伝票修正では、別お届け先登録時はそれを初期選択
                    objMappable.shipping_id = -1;
                }
                this.LoadShippingAddress(objMappable);
                objMappable.shipping_cancel = true;
                objMappable.shipping_modify = false;
                objMappable.shipping_edit = false;
            }
            else if (objMappable.shipping_err)
            {
                //エラー戻り時
                objMappable.shipping_cancel = false;
                objMappable.shipping_modify = true;
                objMappable.shipping_edit = true;
            }
            else if (objMappable.shipping_cancel)
            {
                if (this.IsUpdateNoMemberOrder(objMappable))
                {
                    //非会員購入時の伝票修正では、別お届け先登録時はそれを初期選択
                    objMappable.send = EnumSendTo.ANOTHER_ADDRESS;
                }
                this.LoadShippingAddress(objMappable);
                objMappable.shipping_cancel = true;
                objMappable.shipping_modify = false;
                objMappable.shipping_edit = false;
            }
            else if (objMappable.shipping_edit == true)
            {
                //エラー戻り時に画面編集だった場合
                objMappable.shipping_modify = true;
            }
            else
            {
                this.LoadShippingAddress(objMappable);
                objMappable.shipping_cancel = true;
                objMappable.shipping_modify = false;
                objMappable.shipping_edit = false;
            }
        }

        //未登録別住所をワークに退避
        private void AddToWorkAddress(IShippingMappable objMappable)
        {
            objMappable.wk_add_lname = objMappable.add_lname;
            objMappable.wk_add_fname = objMappable.add_fname;
            objMappable.wk_add_lnamek = objMappable.add_lnamek;
            objMappable.wk_add_fnamek = objMappable.add_fnamek;
            objMappable.wk_add_tel = objMappable.add_tel;
            objMappable.wk_add_zip = objMappable.add_zip;
            objMappable.wk_add_address = objMappable.add_address;
            objMappable.wk_add_taddress = objMappable.add_taddress;
            objMappable.wk_add_maddress = objMappable.add_maddress;
            objMappable.wk_add_pref = objMappable.add_pref;
        }

        //未登録別住所ワークを実エリアに移動
        private void WorkToAddAddress(IShippingMappable objMappable)
        {
            objMappable.add_lname = objMappable.wk_add_lname;
            objMappable.add_fname = objMappable.wk_add_fname;
            objMappable.add_lnamek = objMappable.wk_add_lnamek;
            objMappable.add_fnamek = objMappable.wk_add_fnamek;
            objMappable.add_tel = objMappable.wk_add_tel;
            objMappable.add_zip = objMappable.wk_add_zip;
            objMappable.add_address = objMappable.wk_add_address;
            objMappable.add_taddress = objMappable.wk_add_taddress;
            objMappable.add_maddress = objMappable.wk_add_maddress;
            objMappable.add_pref = objMappable.wk_add_pref;
        }

        private void ClearShipping(IShippingMappable objMappable)
        {
            objMappable.shipping_id = 0;
            objMappable.address_name = "";
            objMappable.add_lname = "";
            objMappable.add_fname = "";
            objMappable.add_lnamek = "";
            objMappable.add_fnamek = "";
            objMappable.add_zip = "";
            objMappable.add_pref = 0;
            objMappable.add_address = "";
            objMappable.add_taddress = "";
            objMappable.add_maddress = "";
            objMappable.add_tel = "";
        }

        private void LoadShippingDetail(IShippingMappable objMappable)
        {
            int id = 0;
            if (objMappable.shipping_id != null && objMappable.shipping_id > 0)
            {
                id = Convert.ToInt32(objMappable.shipping_id.ToString());
            }

            ErsAddressInfo shipping = null;

            if (id > 0) shipping = this.GetShippingData(id, objMappable);

            if (shipping == null)
            {
                if (objMappable.shipping_id == 0)
                {
                    SetShippingInfo(objMappable);
                }
                else if (objMappable.shipping_id == -1)
                {
                    //未登録別住所を設定
                    this.WorkToAddAddress(objMappable);
                }
            }
            else
            {
                SetShippingInfo(shipping, objMappable);
            }
        }

        internal ErsAddressInfo GetShippingData(int id, IShippingMappable objMappable)
        {
            ErsAddressInfoRepository repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            ErsAddressInfoCriteria criteria = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

            //検索条件をクライテリアに保存
            criteria.id = id;
            criteria.mcode = objMappable.mcode;
            var list = repository.Find(criteria);

            return list[0];
        }

        private void SetShippingInfo(IShippingMappable objMappable)
        {
            if (objMappable.member == null)
            {
                return;
            }
            objMappable.shipping_id = 0;
            objMappable.address_name = objMappable.member.lname + " " + objMappable.member.fname;
            objMappable.add_lname = objMappable.member.lname;
            objMappable.add_fname = objMappable.member.fname;
            objMappable.add_lnamek = objMappable.member.lnamek;
            objMappable.add_fnamek = objMappable.member.fnamek;
            objMappable.add_zip = objMappable.member.zip;
            objMappable.add_pref = objMappable.member.pref;
            objMappable.add_address = objMappable.member.address;
            objMappable.add_taddress = objMappable.member.taddress;
            objMappable.add_maddress = objMappable.member.maddress;
            objMappable.add_tel = objMappable.tel;
        }

        private void SetShippingInfo(ErsAddressInfo shipping, IShippingMappable objMappable)
        {
            objMappable.address_name = shipping.address_name;
            objMappable.add_lname = shipping.add_lname;
            objMappable.add_fname = shipping.add_fname;
            objMappable.add_lnamek = shipping.add_lnamek;
            objMappable.add_fnamek = shipping.add_fnamek;
            objMappable.add_zip = shipping.add_zip;
            objMappable.add_pref = shipping.add_pref;
            objMappable.add_address = shipping.add_address;
            objMappable.add_taddress = shipping.add_taddress;
            objMappable.add_maddress = shipping.add_maddress;
            objMappable.add_tel = shipping.add_tel;
        }

        private void LoadShippingAddress(IShippingMappable objMappable)
        {
            IList<ErsAddressInfo> list;

            if (objMappable.deleted == EnumDeleted.Deleted || objMappable.non_member)
            {
                list = new List<ErsAddressInfo>();
            }
            else
            {
                ErsAddressInfoRepository repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
                ErsAddressInfoCriteria criteria = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

                criteria.mcode = objMappable.mcode;
                //criteria.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();
                criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);
                list = repository.Find(criteria);
            }

            //未登録別住所
            if ((objMappable.old_send == EnumSendTo.ANOTHER_ADDRESS && objMappable.old_shipping_id == 0) ||
                 (this.IsUpdateNoMemberOrder(objMappable) && objMappable.shipping_id != 0))
            {
                if (!objMappable.shipping_cancel)
                {
                    if ((objMappable.send == EnumSendTo.ANOTHER_ADDRESS && objMappable.shipping_id == 0) ||
                           (this.IsUpdateNoMemberOrder(objMappable) && !objMappable.goto_page1))
                    {
                        this.AddToWorkAddress(objMappable);
                    }
                }

                //別お届け先を初期選択
                objMappable.shipping_id = -1;

                var addAddress = ErsFactory.ersAddressInfoFactory.GetErsAddressInfo();

                addAddress.id = -1;
                addAddress.add_lname = objMappable.wk_add_lname;
                addAddress.add_fname = objMappable.wk_add_fname;
                addAddress.add_lnamek = objMappable.wk_add_lnamek;
                addAddress.add_fnamek = objMappable.wk_add_fnamek;
                addAddress.add_tel = objMappable.wk_add_tel;
                addAddress.add_zip = objMappable.wk_add_zip;
                addAddress.add_address = objMappable.wk_add_address;
                addAddress.add_taddress = objMappable.wk_add_taddress;
                addAddress.add_maddress = objMappable.wk_add_maddress;
                addAddress.add_pref = objMappable.wk_add_pref;

                list.Add(addAddress);
            }

            objMappable.shippingList = ErsCommon.ConvertEntityListToDictionaryList(list);

            if (objMappable.shipping_id == 0)
            {
                objMappable.send = EnumSendTo.MEMBER_ADDRESS;
            }
            else
            {
                objMappable.send = EnumSendTo.ANOTHER_ADDRESS;
            }
        }

        private bool IsUpdateNoMemberOrder(IShippingMappable objMappable)
        {
            return (objMappable.IsOrderUpdate && (objMappable.non_member || objMappable.deleted == EnumDeleted.Deleted));
        }
    }
}