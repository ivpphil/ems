﻿using System.Linq;
using System.Collections.Generic;
using ersContact.Domain.Order.Mappables;
using ersContact.Models.cart;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.order.strategy;

namespace ersContact.Domain.Order.Mappers
{
    public class ReCalcMapper
        : IMapper<IReCalcMappable>
    {
        public void Map(IReCalcMappable objMappable)
        {
            if (objMappable.regularBasketItems == null)
                return;


            var confirmationList = new Dictionary<string, ErsBaskRecord>();

            var createBaskRecordKeyStgy = ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy();
            foreach (var cartItem in objMappable.regularBasketItems)
            {
                var merchandise = objMappable.basket.objRegularBasketRecord.FirstOrDefault((record) => record.Key == cartItem.regular_key);
                if (merchandise.Key.HasValue())
                {
                    cartItem.LoadDisplayValue(merchandise.Value);

                    //Create and Add in confirmation list
                    var confirmationMerchandise = ErsFactory.ersBasketFactory.GetErsBaskRecordWithParameter(merchandise.Value.GetPropertiesAsDictionary());
                    confirmationList.Add(merchandise.Key, confirmationMerchandise);

                    if (objMappable.product_status_list != null && objMappable.product_status_list.Count > 0)
                    {
                        cartItem.order_status = objMappable.product_status_list[cartItem.regular_key];
                    }
                }
            }
            
            var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().Retrieve(objMappable.mcode);
            
            //recompute regular order
            objMappable.basket.ReComputeRegular(objMappable.regularBasketItems, member_rank);

            //Confirm regular keys
            this.ReConfirmRegularKeys(objMappable, confirmationList);
        }

        protected virtual void ReConfirmRegularKeys(IReCalcMappable objMappable, Dictionary<string, ErsBaskRecord> objRegularBasketRecord)
        {
            foreach (var cartItem in objMappable.regularBasketItems)
            {
                var merchandise = objRegularBasketRecord.FirstOrDefault((record) => record.Key == cartItem.regular_key);
                if (merchandise.Key.HasValue())
                {
                    this.ReConfirmRegularKey(objMappable, merchandise.Value, cartItem);
                }
            }
        }

        protected virtual void ReConfirmRegularKey(IReCalcMappable objMappable, ErsBaskRecord merchandise, Cart_regular_items cartItem)
        {
            var confirmationMerchandise = ErsFactory.ersBasketFactory.GetErsBaskRecord();
            confirmationMerchandise.OverwriteWithModel(merchandise);

            //Get the fields value that required in to get regular_key
            var confirmation_regular_key_group = cartItem.GetPropertiesAsDictionary("confirmation_regular_key");
            confirmationMerchandise.OverwriteWithParameter(confirmation_regular_key_group);

            //Recalculate Existing Next Date
            this.ReCalculateNextDate(confirmationMerchandise);

            //Get the new confirmation key
            string confirmation_regular_key = ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(confirmationMerchandise);

            var updatedMerchandise = objMappable.basket.objRegularBasketRecord.FirstOrDefault((record) => record.Key == confirmation_regular_key);
            if (cartItem.record_key != confirmation_regular_key && updatedMerchandise.Key.HasValue())
            {
                cartItem.regular_key = confirmation_regular_key;
            }
        }

        protected virtual void ReCalculateNextDate(ErsBaskRecord merchandise)
        {
            if (merchandise.send_ptn.HasValue)
            {
                var regularPatternService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(merchandise.send_ptn.Value);
                var next_date = regularPatternService.CalculateFirstTime(merchandise, System.DateTime.Now);
                if (next_date.HasValue)
                {
                    merchandise.next_date = CalcNextSendStgy.CalculateWeekendOperation(next_date.Value, EnumWeekendOperation.NONE);
                }
            }
        }
    }
}