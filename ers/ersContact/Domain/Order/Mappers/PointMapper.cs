﻿using ersContact.Domain.Order.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;

namespace ersContact.Domain.Order.Mappers
{
    public class PointMapper
        : IMapper<IPointMappable>
    {
        public void Map(IPointMappable objMappable)
        {
            objMappable.points_available = ErsFactory.ersMemberFactory.GetObtainMemberPointStgy().GetPoint(objMappable.mcode, objMappable.site_id);
            if (!objMappable.IsOrderUpdate) // still in basket
            {
                if (objMappable.from_page1) objMappable.ent_point = 0;
                objMappable.ent_point_orig = 0;
            }
            else
            {
                objMappable.ent_point_orig = this.GetPointsUsed(objMappable);
                objMappable.points_available += objMappable.ent_point_orig;

                if (objMappable.from_page1)
                {
                    objMappable.ent_point = objMappable.ent_point_orig;
                    objMappable.p_service = objMappable.ent_point_orig;
                } 
            }
        }

        private int GetPointsUsed(IPointMappable objMappable)
        {
            ErsOrder order = ErsFactory.ersOrderFactory.GetOrderWithD_no(objMappable.order_d_no, objMappable.site_id);
            if (order == null)
            {
                throw new ErsException("30104", objMappable.order_d_no);
            }
            return order.p_service;
        }
    }
}