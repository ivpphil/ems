﻿using System.Collections.Generic;
using ersContact.Domain.Order.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.order.related;

namespace ersContact.Domain.Order.Mappers
{
    public class PaymentMapper
        : IMapper<IPaymentMappable>
    {
        public void Map(IPaymentMappable objMappable)
        {
            if (objMappable.card_new)
            {
                this.ClearCard(objMappable);
                objMappable.card_cancel = false;
            }
            else
            {
                this.LoadCards(objMappable);
                objMappable.card_cancel = true;
            }
            this.setDispData(objMappable);
        }

        private void ClearCard(IPaymentMappable objMappable)
        {
            objMappable.card_holder_name = "";
            objMappable.card_type = 0;
            objMappable.cardno = "";
            objMappable.validity_y = 0;
            objMappable.validity_m = 1;
            objMappable.securityno = "";
        }

        private void LoadCards(IPaymentMappable objMappable)
        {
            //Use this to get Card Information from the internet [setup.cs of Gmo]
            var g = new ErsGmoCard();
            if (objMappable.member == null)
            {
                //該当会員無し
                throw new ErsException("20302");
            }
            var dic = g.ObtainMemberCardInfo(objMappable.member);
            objMappable.membercardList = ErsCommon.ConvertEntityListToDictionaryList(dic);

            if (objMappable.membercardList.Count > 0)
            {
                if (objMappable.card_id.HasValue)
                {
                    this.LoadCard(objMappable);
                }

                objMappable.membercardListExtend = new List<Dictionary<string, object>>();
                foreach (var clist in dic)
                {
                    var cinfo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD)).ObtainMemberCardInfo(objMappable.member, clist.card_id);
                    if (cinfo != null)
                    {
                        objMappable.membercardListExtend.Add(cinfo.GetPropertiesAsDictionary());
                    }
                }
            }
        }

        private void LoadCard(IPaymentMappable objMappable)
        {
            objMappable.savedCardInfo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD)).ObtainMemberCardInfo(objMappable.member, objMappable.card_id);
            if (objMappable.savedCardInfo != null)
            {
                objMappable.disp_card_holder_name = objMappable.savedCardInfo.card_holder_name;
                objMappable.card_id = objMappable.savedCardInfo.card_id;
                objMappable.disp_card_type = objMappable.savedCardInfo.card_type;
                objMappable.disp_cardno = objMappable.savedCardInfo.card_no;
                objMappable.disp_validity_y = objMappable.savedCardInfo.validity_y;
                objMappable.disp_validity_m = objMappable.savedCardInfo.validity_m;
            }
        }

        /// <summary>
        /// 新規データを表示用にセット
        /// </summary>
        private void setDispData(IPaymentMappable objMappable)
        {
            if (objMappable.card_id == CreditCardInfo.IgnoreCardSequenceValue)
            {
                objMappable.disp_card_holder_name = objMappable.card_holder_name;
                objMappable.disp_card_type = objMappable.card_type;
                objMappable.disp_cardno = objMappable.cardno;
                objMappable.disp_validity_y = objMappable.validity_y;
                objMappable.disp_validity_m = objMappable.validity_m;
                objMappable.disp_securityno = objMappable.securityno;
            }
        }
    }
}