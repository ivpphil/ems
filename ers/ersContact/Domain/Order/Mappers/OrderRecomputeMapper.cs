﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Order.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;

namespace ersContact.Domain.Order.Mappers
{
    public class OrderRecomputeMapper
        : IMapper<IOrderRecomputeMappable>
    {
        public void Map(IOrderRecomputeMappable objMappable)
        {
            if (objMappable.GetOrderForUpdate)
            {
                objMappable.orderContainer = this.GetOrderForUpdate(objMappable);
            }
            else
            {
                this.RefreshBasket(objMappable);
                this.Recompute(objMappable);
            }
        }

        private void RefreshBasket(IOrderRecomputeMappable objMappable)
        {
            objMappable.basket = ErsFactory.ersBasketFactory.GetErsBasket();
            objMappable.basket.IsOrderUpdate = objMappable.IsOrderUpdate;
            objMappable.basket.PrepareSession();
            objMappable.basket.Init(objMappable.mcode, objMappable.cts_order_ransu);
        }

        private void Recompute(IOrderRecomputeMappable objMappable)
        {
            this.LoadDefaultParameters(objMappable);
            //定期のみの場合、配送時間を定期のものにする
            if (objMappable.sendtime == null && objMappable.regular_sendtime != null)
            {
                objMappable.sendtime = objMappable.regular_sendtime;
            }

            //Combines regular basket items and ordinary basket items.
            objMappable.basket.CalcRegularFirstDate(EnumWeekendOperation.NONE);

            //If there is not ordinary order then sets the value which is the shortest date of regular order into the senddate.
            if (objMappable.basket.objBasketRecord.Count == 0)
            {
                objMappable.firstTimeOrdinary = EnumFirstTimeOrdinary.WithRegular;
            }

            switch (objMappable.firstTimeOrdinary)
            {
                case EnumFirstTimeOrdinary.Shortest:
                    objMappable.senddate = null;
                    break;
                case EnumFirstTimeOrdinary.WithRegular:
                    objMappable.senddate = objMappable.basket.GetRegularShortestDate();
                    break;
            }

            if (objMappable.IsOrderUpdate)
            {
                objMappable.orderContainer = this.GetOrderForUpdate(objMappable);
            }
            else
            {
                //Insert
                var order = ErsFactory.ersOrderFactory.GetErsOrderIntegrated();
                objMappable.orderContainer.OrderHeader = order;
                order.OverwriteWithParameter(objMappable.GetPropertiesAsDictionary());
                order.GenerateOrder(objMappable, objMappable.basket, objMappable.deliv_method);

                var pay = (objMappable.page2) ? EnumPaymentType.CREDIT_CARD : objMappable.orderContainer.OrderHeader.pay;
                var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(pay);
                objPayment.SetPaymentMethod(objMappable.orderContainer.OrderHeader, objMappable);

                //normalize point value
                if (objMappable.ent_point == null)
                {
                    objMappable.ent_point = objMappable.orderContainer.OrderHeader.p_service;
                }
                objMappable.p_service = objMappable.ent_point;
            }

            int this_user_id = 0;
            if (Int32.TryParse(objMappable.ordered_user_id, out this_user_id))
            {
                var agent = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(this_user_id);
                if (agent == null)
                {
                    throw new ErsException("29002");
                }
                objMappable.ordered_ag_name = agent.ag_name;
            }
        }

        private void LoadDefaultParameters(IOrderRecomputeMappable objMappable)
        {
            objMappable.send = (objMappable.shipping_id == 0) ? EnumSendTo.MEMBER_ADDRESS : EnumSendTo.ANOTHER_ADDRESS;

            if (objMappable.send != EnumSendTo.ANOTHER_ADDRESS)
            {
                objMappable.member_add_id = 0;
            }
            else
            {
                if (objMappable.shipping_id > 0)
                {
                    objMappable.member_add_id = objMappable.shipping_id;
                }
                else
                {
                    objMappable.member_add_id = null;
                }
                this.LoadShippingDetail(objMappable);
            }
            
            objMappable.member_card_id = ((IPaymentInfoGmoInputContainer)objMappable).card_id;

            this.RefreshBasket(objMappable);
        }

        private ErsOrderContainer GetOrderForUpdate(IOrderRecomputeMappable objMappable)
        {
            var order = ErsFactory.ersOrderFactory.GetOrderWithD_no(objMappable.order_d_no, objMappable.site_id);
            var overwriteDictionary = objMappable.GetPropertiesAsDictionary();
            if (objMappable.GetOrderForUpdate)
            {
                overwriteDictionary["lname"] = objMappable.wk_lname;
                overwriteDictionary["fname"] = objMappable.wk_fname;
                overwriteDictionary["lnamek"] = objMappable.wk_lnamek;
                overwriteDictionary["fnamek"] = objMappable.wk_fnamek;
                overwriteDictionary["tel"] = objMappable.wk_tel;
                overwriteDictionary["zip"] = objMappable.wk_zip;
                overwriteDictionary["pref"] = objMappable.wk_pref;
                overwriteDictionary["address"] = objMappable.wk_address;
                overwriteDictionary["taddress"] = objMappable.wk_taddress;
                overwriteDictionary["maddress"] = objMappable.wk_maddress;
                overwriteDictionary["email"] = objMappable.wk_email;
            }
            else
            { 
                overwriteDictionary.Remove("lname");
                overwriteDictionary.Remove("fname");
                overwriteDictionary.Remove("lnamek");
                overwriteDictionary.Remove("fnamek");
                overwriteDictionary.Remove("tel");
                overwriteDictionary.Remove("zip");
                overwriteDictionary.Remove("pref");
                overwriteDictionary.Remove("address");
                overwriteDictionary.Remove("taddress");
                overwriteDictionary.Remove("maddress");
                overwriteDictionary.Remove("email");
            }
            order.OverwriteWithParameter(overwriteDictionary);
            order.d_no = objMappable.order_d_no;
            var orderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order.d_no, objMappable.basket, objMappable.deliv_method, null);

            var orderContainer = ErsFactory.ersOrderFactory.GetErsOrderContainer();
            orderContainer.OrderHeader = order;
            orderContainer.OrderRecords = orderRecords;

            //payがnullの場合（一度0円決済にしてから、すぐに0円以上決済に戻した場合を想定。）
            if (!objMappable.pay.HasValue)
            {
                var oldOrder = ErsFactory.ersOrderFactory.GetOrderWithD_no(order.d_no, objMappable.site_id);
                objMappable.pay = oldOrder.pay;
            }

            EnumCarriageFreeStatus? status = null;

            if (ErsFactory.ersBasketFactory.GetRegularShippingFreeSpec().IsSatisfiedBy(orderRecords.Values)
                || ErsFactory.ersBasketFactory.GetCarriageCostTypeFreeSpec().IsSatisfiedBy(orderRecords.Values))
                status = EnumCarriageFreeStatus.CARRIAGE_FREE;
            else
                status = ErsFactory.ersBasketFactory.GetCarriageFreeSpecification().GetCarriageFreeStatus(objMappable.basket.subtotal + objMappable.basket.regular_subtotal);

            ErsFactory.ersOrderFactory.GetErsCalcService().calcOrder(order, orderRecords.Values, order.p_service, status, true, objMappable.carriage_recomp_flg);

            //Update
            //再計算で取得した送料を復活させる
            objMappable.carriage = order.carriage;
            objMappable.tax = order.tax;
            objMappable.etc = order.etc;
            objMappable.coupon_discount = order.coupon_discount;
            objMappable.ent_point = order.p_service;

            var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(order.pay);
            objPayment.SetPaymentMethod(order, objMappable);

            ErsFactory.ersOrderFactory.GetErsCalcService().calcOrderTotal(order);

            return orderContainer;
        }

        private void LoadShippingDetail(IOrderRecomputeMappable objMappable)
        {
            int id = 0;
            if (objMappable.shipping_id != null && objMappable.shipping_id > 0)
            {
                id = Convert.ToInt32(objMappable.shipping_id.ToString());
            }

            ErsAddressInfo shipping = null;

            if (id > 0) shipping = this.GetShippingData(id, objMappable.mcode);

            if (shipping == null)
            {
                if (objMappable.shipping_id == 0)
                {
                    this.SetShippingInfo(objMappable);
                }
                else if (objMappable.shipping_id == -1)
                {
                    //未登録別住所を設定
                    this.WorkToAddAddress(objMappable);
                }
            }
            else
            {
                this.SetShippingInfo(shipping, objMappable);
            }
        }

        internal ErsAddressInfo GetShippingData(int id, string mcode)
        {
            ErsAddressInfoRepository repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            ErsAddressInfoCriteria criteria = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

            //検索条件をクライテリアに保存
            criteria.id = id;
            criteria.mcode = mcode;

            var list = repository.Find(criteria);

            return list[0];
        }

        private void SetShippingInfo(IOrderRecomputeMappable objMappable)
        {
            if (objMappable.member == null)
            {
                return;
            }
            objMappable.shipping_id = 0;
            objMappable.address_name = objMappable.member.lname + " " + objMappable.member.fname;
            objMappable.add_lname = objMappable.member.lname;
            objMappable.add_fname = objMappable.member.fname;
            objMappable.add_lnamek = objMappable.member.lnamek;
            objMappable.add_fnamek = objMappable.member.fnamek;
            objMappable.add_zip = objMappable.member.zip;
            objMappable.add_pref = objMappable.member.pref;
            objMappable.add_address = objMappable.member.address;
            objMappable.add_taddress = objMappable.member.taddress;
            objMappable.add_maddress = objMappable.member.maddress;
            objMappable.add_tel = objMappable.tel;
        }

        //未登録別住所ワークを実エリアに移動
        private void WorkToAddAddress(IOrderRecomputeMappable objMappable)
        {
            objMappable.add_lname = objMappable.wk_add_lname;
            objMappable.add_fname = objMappable.wk_add_fname;
            objMappable.add_lnamek = objMappable.wk_add_lnamek;
            objMappable.add_fnamek = objMappable.wk_add_fnamek;
            objMappable.add_tel = objMappable.wk_add_tel;
            objMappable.add_zip = objMappable.wk_add_zip;
            objMappable.add_address = objMappable.wk_add_address;
            objMappable.add_taddress = objMappable.wk_add_taddress;
            objMappable.add_maddress = objMappable.wk_add_maddress;
            objMappable.add_pref = objMappable.wk_add_pref;
        }

        private void SetShippingInfo(ErsAddressInfo shipping, IOrderRecomputeMappable objMappable)
        {
            objMappable.address_name = shipping.address_name;
            objMappable.add_lname = shipping.add_lname;
            objMappable.add_fname = shipping.add_fname;
            objMappable.add_lnamek = shipping.add_lnamek;
            objMappable.add_fnamek = shipping.add_fnamek;
            objMappable.add_zip = shipping.add_zip;
            objMappable.add_pref = shipping.add_pref;
            objMappable.add_address = shipping.add_address;
            objMappable.add_taddress = shipping.add_taddress;
            objMappable.add_maddress = shipping.add_maddress;
            objMappable.add_tel = shipping.add_tel;
        }
    }
}