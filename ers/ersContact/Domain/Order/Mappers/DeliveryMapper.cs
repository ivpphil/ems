﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Order.Mappables;
using ersContact.Models.cart;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.Payment;

namespace ersContact.Domain.Order.Mappers
{
    public class DeliveryMapper
        : IMapper<IDeliveryMappable>
    {
        public void Map(IDeliveryMappable objMappable)
        {
            if (objMappable.send_chk == 1)
            {
                objMappable.send = EnumSendTo.ANOTHER_ADDRESS;
            }
            else
            {
                objMappable.send = EnumSendTo.MEMBER_ADDRESS;
            }
            if (objMappable.send == EnumSendTo.ANOTHER_ADDRESS)
            {
                this.LoadShippingDetail(objMappable);
            }
            else
            {
                this.SetShippingInfo(objMappable);
            }

            if (objMappable.senddate != null)
            {
                if (objMappable.page2 || objMappable.page3)
                {
                    objMappable.senddate_v = objMappable.senddate.Value.ToString("yyyy/MM/dd");
                    objMappable.senddate_t = objMappable.senddate.Value.ToString("yyyy年MM月dd日");
                }

                if (objMappable.IsOrderUpdate && objMappable.page2)
                {
                    objMappable.senddate_add = this.shouldAddDate(objMappable);
                }
            }
            objMappable.basketItems = this.LoadDefaultValue<Cart_items>(objMappable);

            //通常商品がない場合は、配送希望日初期化（戻るボタンで戻ったときにエラーとなるため）
            if (objMappable.basketItems.Count == 0)
            {
                objMappable.sendtime = null;
            }
        }

        private void LoadShippingDetail(IDeliveryMappable objMappable)
        {
            int id = 0;
            if (objMappable.shipping_id.HasValue && objMappable.shipping_id > 0)
            {
                id = objMappable.shipping_id.Value;
            }

            ErsAddressInfo shipping = null;

            if (id > 0) shipping = this.GetShippingData(id, objMappable);

            if (shipping == null)
            {
                if (objMappable.shipping_id == 0)
                {
                    this.SetShippingInfo(objMappable);
                }
                else if (objMappable.shipping_id == -1)
                {
                    //未登録別住所を設定
                    this.WorkToAddAddress(objMappable);
                }
            }
            else
            {
                this.SetShippingInfo(shipping, objMappable);
            }
        }

        internal ErsAddressInfo GetShippingData(int id, IDeliveryMappable objMappable)
        {
            ErsAddressInfoRepository repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            ErsAddressInfoCriteria criteria = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

            //検索条件をクライテリアに保存
            criteria.id = id;
            criteria.mcode = objMappable.mcode;

            var list = repository.Find(criteria);

            return list[0];
        }

        private void LoadCard(IDeliveryMappable objMappable)
        {
            objMappable.savedCardInfo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD)).ObtainMemberCardInfo(objMappable.member, objMappable.card_id);
            if (objMappable.savedCardInfo != null)
            {
                objMappable.disp_card_holder_name = objMappable.savedCardInfo.card_holder_name;
                objMappable.card_id = objMappable.savedCardInfo.card_id;
                objMappable.disp_card_type = objMappable.savedCardInfo.card_type;
                objMappable.disp_cardno = objMappable.savedCardInfo.card_no;
                objMappable.disp_validity_y = objMappable.savedCardInfo.validity_y;
                objMappable.disp_validity_m = objMappable.savedCardInfo.validity_m;
            }
        }

        private void SetShippingInfo(IDeliveryMappable objMappable)
        {
            if(objMappable.IsOrderUpdate)
            {
                objMappable.address_name = objMappable.wk_lname + " " + objMappable.wk_fname;
                objMappable.add_lname = objMappable.wk_lname;
                objMappable.add_fname = objMappable.wk_fname;
                objMappable.add_lnamek = objMappable.wk_lnamek;
                objMappable.add_fnamek = objMappable.wk_fnamek;
                objMappable.add_zip = objMappable.wk_zip;
                objMappable.add_pref = objMappable.wk_pref;
                objMappable.add_address = objMappable.wk_address;
                objMappable.add_taddress = objMappable.wk_taddress;
                objMappable.add_maddress = objMappable.wk_maddress;
                objMappable.add_tel = objMappable.wk_tel;
            }
            else if (objMappable.member == null)
            {
                objMappable.address_name = objMappable.lname + " " + objMappable.fname;
                objMappable.add_lname = objMappable.lname;
                objMappable.add_fname = objMappable.fname;
                objMappable.add_lnamek = objMappable.lnamek;
                objMappable.add_fnamek = objMappable.fnamek;
                objMappable.add_zip = objMappable.zip;
                objMappable.add_pref = objMappable.pref;
                objMappable.add_address = objMappable.address;
                objMappable.add_taddress = objMappable.taddress;
                objMappable.add_maddress = objMappable.maddress;
                objMappable.add_tel = objMappable.tel;
            }
            else
            {
                objMappable.shipping_id = 0;
                objMappable.address_name = objMappable.member.lname + " " + objMappable.member.fname;
                objMappable.add_lname = objMappable.member.lname;
                objMappable.add_fname = objMappable.member.fname;
                objMappable.add_lnamek = objMappable.member.lnamek;
                objMappable.add_fnamek = objMappable.member.fnamek;
                objMappable.add_zip = objMappable.member.zip;
                objMappable.add_pref = objMappable.member.pref;
                objMappable.add_address = objMappable.member.address;
                objMappable.add_taddress = objMappable.member.taddress;
                objMappable.add_maddress = objMappable.member.maddress;
                objMappable.add_tel = objMappable.tel;
            }
        }

        private void SetShippingInfo(ErsAddressInfo shipping, IDeliveryMappable objMappable)
        {
            objMappable.address_name = shipping.address_name;
            objMappable.add_lname = shipping.add_lname;
            objMappable.add_fname = shipping.add_fname;
            objMappable.add_lnamek = shipping.add_lnamek;
            objMappable.add_fnamek = shipping.add_fnamek;
            objMappable.add_zip = shipping.add_zip;
            objMappable.add_pref = shipping.add_pref;
            objMappable.add_address = shipping.add_address;
            objMappable.add_taddress = shipping.add_taddress;
            objMappable.add_maddress = shipping.add_maddress;
            objMappable.add_tel = shipping.add_tel;
        }

        //未登録別住所ワークを実エリアに移動
        private void WorkToAddAddress(IDeliveryMappable objMappable)
        {
            objMappable.add_lname = objMappable.wk_add_lname;
            objMappable.add_fname = objMappable.wk_add_fname;
            objMappable.add_lnamek = objMappable.wk_add_lnamek;
            objMappable.add_fnamek = objMappable.wk_add_fnamek;
            objMappable.add_tel = objMappable.wk_add_tel;
            objMappable.add_zip = objMappable.wk_add_zip;
            objMappable.add_address = objMappable.wk_add_address;
            objMappable.add_taddress = objMappable.wk_add_taddress;
            objMappable.add_maddress = objMappable.wk_add_maddress;
            objMappable.add_pref = objMappable.wk_add_pref;
        }

        private bool shouldAddDate(IDeliveryMappable objMappable)
        {
            bool ret = false;
            foreach (var item in objMappable.senddateList)
            {
                var d = ErsViewHelper.GetVariableValue(item, "value");
                if (d.ToString() == objMappable.senddate_v)
                {
                    ret = true;
                    break;
                }
            }

            return !ret;
        }

        /// <summary>
        /// load default value of basket itmes
        /// </summary>
        /// <returns></returns>
        private List<T> LoadDefaultValue<T>(IDeliveryMappable objMappable)
            where T : Cart_items, new()
        {
            var basketItems = new List<T>();
            foreach (var itemData in objMappable.basket.objBasketRecord.Values)
            {
                var cartItem = new T();
                cartItem.LoadDefaultValue(itemData);

                if (objMappable.page2)
                {
                    //同梱物の場合、編集不可フラグを立てる
                    cartItem.orderUpdateDocBundingDisabledFlg(itemData.doc_bundling_flg);

                    //商品マスタに存在しない場合、編集不可フラグを立てる
                    cartItem.disabledFlg = ErsFactory.ersBasketFactory.GetOrderEditSpecification().IsSatisfiedBy(itemData.scode);
                }

                basketItems.Add(cartItem);
            }

            return basketItems;
        }
    }
}