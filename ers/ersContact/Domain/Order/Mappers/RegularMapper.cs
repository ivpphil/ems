﻿using System.Linq;
using System.Collections.Generic;
using ersContact.Domain.Order.Mappables;
using ersContact.Models.cart;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.order.strategy;

namespace ersContact.Domain.Order.Mappers
{
    public class RegularMapper
        : IMapper<IRegularMappable>
    {
        public void Map(IRegularMappable objMappable)
        {
            if (!objMappable.IsErrorBack)
            {
                objMappable.regularBasketItems = this.LoadDefaultValue<Cart_regular_items>(objMappable);
            }
            else
            {
                var createBaskRecordKeyStgy = ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy();
                foreach (var cartItem in objMappable.regularBasketItems)
                {
                    var merchandise = objMappable.basket.objRegularBasketRecord.FirstOrDefault((record) => record.Key == cartItem.regular_key);
                    if(merchandise.Key.HasValue())
                    {
                        cartItem.LoadDisplayValue(merchandise.Value);
                    }
                }
            }
        }

        /// <summary>
        /// load default value of basket itmes
        /// </summary>
        /// <returns></returns>
        private List<T> LoadDefaultValue<T>(IRegularMappable objMappable)
            where T : Cart_items, new()
        {
            var basketItems = new List<T>();
            foreach (var baskKey in objMappable.basket.objRegularBasketRecord.Keys)
            {
                var itemData = objMappable.basket.objRegularBasketRecord[baskKey];

                var cartItem = new T();

                this.SetNonBaskProperty(objMappable, cartItem, baskKey);

                cartItem.LoadDefaultValue(itemData);

                if (objMappable.page2)
                {
                    //同梱物の場合、編集不可フラグを立てる
                    cartItem.orderUpdateDocBundingDisabledFlg(itemData.doc_bundling_flg);

                    //商品マスタに存在しない場合、編集不可フラグを立てる
                    cartItem.disabledFlg = ErsFactory.ersBasketFactory.GetOrderEditSpecification().IsSatisfiedBy(itemData.scode);
                }

                basketItems.Add(cartItem);
            }

            return basketItems;
        }

        protected void SetNonBaskProperty(IRegularMappable objMappable, Cart_items cartItem, string baskKey)
        {
            if (cartItem is Cart_regular_items && objMappable.regularBasketItems != null)
            {
                var newRegularCart = cartItem as Cart_regular_items;

                var regularList = objMappable.regularBasketItems.FindAll((record) => record.regular_key == baskKey);

                if (regularList.Count > 0)
                {
                    var regularCartItem = regularList.FirstOrDefault();
                    newRegularCart.firstTime = regularCartItem.firstTime;
                }
            }
        }
    }
}