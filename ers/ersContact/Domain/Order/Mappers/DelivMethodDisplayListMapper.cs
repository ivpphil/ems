﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Order.Mappables;
using jp.co.ivp.ers;

namespace ersContact.Domain.Order.Mappers
{
    public class DelivMethodDisplayListMapper
        : IMapper<IDelivMethodDisplayListMappable>
    {
        public void Map(IDelivMethodDisplayListMappable objMappable)
        {
            this.SetDisplayedDelvMethod(objMappable);
        }

        /// <summary>
        /// Display options Deliv Mehotd by above conditions
        /// </summary>
        /// <param name="objMappable"></param>
        protected virtual void SetDisplayedDelvMethod(IDelivMethodDisplayListMappable objMappable)
        {
            if (this.FindItemDelivMethod(objMappable, EnumDelvMethod.Mail))
                objMappable.DisplayedDelvMethod = (short)EnumDelvMethod.Mail;
            else if (this.FindItemDelivMethod(objMappable, EnumDelvMethod.ExpressAndMail))
            {
                objMappable.DisplayedDelvMethod = (short)EnumDelvMethod.Express;

                if (objMappable.amounttotal_all_item == 1)
                    objMappable.DisplayedDelvMethod = (short)EnumDelvMethod.ExpressAndMail;
            }
            else
            {
                objMappable.DisplayedDelvMethod = (short)EnumDelvMethod.Express;
            }
        }

        protected virtual bool FindItemDelivMethod(IDelivMethodDisplayListMappable objMappable, EnumDelvMethod deliv_method)
        {
            var result = false;

            if (objMappable.basketItems != null && objMappable.basketItems.Count > 0)
            {
                result = objMappable.basketItems.Where((record) => record.deliv_method == deliv_method).Count() > 0;
            }

            if (result == false)
            {
                if (objMappable.regularBasketItems != null && objMappable.regularBasketItems.Count > 0)
                {
                    result = objMappable.regularBasketItems.Where((record) => record.deliv_method == deliv_method).Count() > 0;
                }
            }

            return result;
        }
    }
}