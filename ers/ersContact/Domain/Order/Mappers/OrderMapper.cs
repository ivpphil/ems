﻿using System;
using ersContact.Domain.Order.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.ctsorder;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.order;
using System.Linq;

namespace ersContact.Domain.Order.Mappers
{
    public class OrderMapper
        : IMapper<IOrderMappable>
    {
        public void Map(IOrderMappable objMappable)
        {
            if (!(objMappable.page1 || objMappable.page2 || objMappable.page3 || objMappable.page4))
            {
                if (objMappable.IsOrderUpdate)
                {
                    this.LoadOrder(objMappable);
                }
                else if (objMappable.cts_order_temp_d_no != null && objMappable.cts_order_temp_d_no > 0)
                {
                    this.LoadCtsOrder(objMappable);
                }
                else
                {
                    this.ResetOrder(objMappable);
                }
            }

            if (objMappable.goto_page1) this.Page(1, objMappable);
            if (objMappable.goto_page2) this.Page(2, objMappable);
            if (objMappable.goto_page3) this.Page(3, objMappable);
            if (objMappable.goto_page4) this.Page(4, objMappable);


            if (objMappable.page3 && objMappable.Edit_Temp == 1)
            {
                if (objMappable.shipping_id == 0 || objMappable.shipping_id == null)
                {
                    objMappable.add_zip = objMappable.zip;
                    objMappable.add_tel = objMappable.tel;
                    objMappable.add_lname = objMappable.lname;
                    objMappable.add_fname = objMappable.fname;
                    objMappable.add_address = objMappable.address;
                    objMappable.add_taddress = objMappable.taddress;
                    objMappable.add_maddress = objMappable.maddress;
                }
                else
                {
                    var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
                    var criteria = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

                    criteria.id = objMappable.shipping_id;
                    criteria.site_id = objMappable.site_id;
                    var list = repository.Find(criteria);
                    var list01 = list.First();
                    objMappable.add_zip = list01.add_zip;
                    objMappable.add_tel = list01.add_tel;
                    objMappable.add_lname = list01.add_lname;
                    objMappable.add_fname = list01.add_fname;
                    objMappable.add_address = list01.add_address;
                    objMappable.add_taddress = list01.add_taddress;
                    objMappable.add_maddress = list01.add_maddress;
                }
            }

            ErsOrder order = ErsFactory.ersOrderFactory.GetOrderWithD_no(objMappable.order_d_no, objMappable.site_id);
            objMappable.point_ck = order != null ? order.point_ck : null;

            if (order != null)
            {
                var orderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order);
                var orderStatus = ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy().ObtainFrom(orderRecords.Values);

                if (!(orderStatus == EnumOrderStatusType.NEW_ORDER || orderStatus == EnumOrderStatusType.DELIVER_WAITING || orderStatus == EnumOrderStatusType.CANCELED))
                {
                    objMappable.controller.AddInformation(ErsResources.GetMessage("71001"));
                }
            }
        }

        private void LoadOrder(IOrderMappable objMappable)
        {
            if (!objMappable.IsOrderUpdate)
            {
                //new order
                this.ResetOrder(objMappable);
            }
            else
            {
                //order update
                ErsOrder order = ErsFactory.ersOrderFactory.GetOrderWithD_no(objMappable.order_d_no, objMappable.site_id);
                if (order == null)
                {
                    throw new ErsException("30104", objMappable.order_d_no);
                }

                this.SetOrderInfo(order, objMappable);
                this.RefreshOrderBasket(order, objMappable);

                objMappable.cts_order_ransu = order.ransu;
                objMappable.reaload_cart = true;

                this.Page(1, objMappable);

                if(objMappable.last_used_card_flg)
                {
                    objMappable.last_used_card_info = 1;
                    objMappable.card_id = null;
                }

                objMappable.orderContainer.OrderHeader = order;
            }
        }

        private void LoadCtsOrder(IOrderMappable objMappable)
        {
            ErsCtsOrder order = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderWithTempDNo(Convert.ToInt32(objMappable.cts_order_temp_d_no),objMappable.site_id);
            if (order == null)
            {
                this.ResetOrder(objMappable);
            }
            else
            {
                this.SetCtsOrderInfo(order, objMappable);

                objMappable.reaload_cart = true;
                objMappable.pm_flg = order.pm_flg;
                objMappable.Edit_Temp = 1;
                this.Page(1, objMappable);
            }
        }

        private void ResetOrder(IOrderMappable objMappable)
        {
            if (objMappable.cts_order_temp_d_no == null)
            {
                //初回だけバスケットの中身も空にする（cts_order_temp_d_noを使用しない場合は注意！！）
                ErsFactory.ersBasketFactory.GetEmptyBasketStrategy().EmptyBasket(objMappable.Cts_Ransu);
            }

            this.Page(1, objMappable);

            objMappable.cts_order_id = 0;
            objMappable.cts_order_temp_d_no = 0;
            objMappable.cts_order_ransu = objMappable.Cts_Ransu;

            this.SetAgentInfo(objMappable);

            objMappable.cts_order_status = 1;
            objMappable.pm_flg = EnumPmFlg.TEL;
            
            if (objMappable.shipping_id == null)
            {
                objMappable.shipping_id = 0;
            }
        }

        public void Page(int page, IOrderMappable objMappable)
        {
            objMappable.page1 = page == 1;
            objMappable.page2 = page == 2;
            objMappable.page3 = page == 3;
            objMappable.page4 = page == 4;
        }

        private void SetAgentInfo(IOrderMappable objMappable)
        {
            var agent = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(objMappable.cts_agent_id);
            if (agent == null)
            {
                throw new ErsException("29002");
            }

            objMappable.cts_order_user_id = agent.id.Value.ToString();
            objMappable.cts_order_charge1_ag_type = agent.ag_type;
            objMappable.cts_order_charge1_ag_name = agent.ag_name;
        }

        private void SetOrderInfo(ErsOrder order, IOrderMappable objMappable)
        {
            objMappable.mcode = order.mcode;
            objMappable.send = order.send.Value;
            if (objMappable.send == EnumSendTo.MEMBER_ADDRESS)
            {
                objMappable.shipping_id = 0;
            }
            else
            {
                objMappable.shipping_id = Convert.ToInt32(order.member_add_id);
                if (objMappable.shipping_id == 0
                    || (objMappable.IsOrderUpdate && objMappable.shipping_id > 0)
                    || order.mall_shop_kbn != EnumMallShopKbn.ERS)//退会会員の別お届け先登録時も伝票から取得しておく
                {
                    //未登録別住所読み込み
                    objMappable.add_lname = order.add_lname;
                    objMappable.add_fname = order.add_fname;
                    objMappable.add_lnamek = order.add_lnamek;
                    objMappable.add_fnamek = order.add_fnamek;
                    objMappable.add_zip = order.add_zip;
                    objMappable.add_pref = order.add_pref;
                    objMappable.add_address = order.add_address;
                    objMappable.add_taddress = order.add_taddress;
                    objMappable.add_maddress = order.add_maddress;
                    objMappable.add_tel = order.add_tel;
                }
            }

            objMappable.old_send = objMappable.send;
            objMappable.old_shipping_id = objMappable.shipping_id;
            objMappable.pay = order.pay;
            objMappable.conv_code = order.conv_code;
            objMappable.old_pay = objMappable.pay;
            objMappable.card_id = order.member_card_id;
            objMappable.senddate = order.senddate;
            objMappable.sendtime = order.sendtime;
            if (objMappable.sendtime == 0)
            {
                objMappable.sendtime = objMappable.regular_sendtime;
            }

            objMappable.cts_order_memo = order.memo3;
            objMappable.ent_coupon_code = order.coupon_code;
            if (!string.IsNullOrEmpty(objMappable.order_d_no))
            {
                objMappable.old_coupon_code = order.coupon_code;
                objMappable.coupon_code = order.coupon_code;
                objMappable.ordered_user_id = order.user_id;

                int this_user_id = 0;
                if (Int32.TryParse(objMappable.ordered_user_id, out this_user_id))
                {
                    var agent = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(this_user_id);
                    if (agent == null)
                    {
                        throw new ErsException("29002");
                    }
                    objMappable.ordered_ag_name = agent.ag_name;
                }
            }

            objMappable.ccode = order.ccode;
            objMappable.pm_flg = order.pm_flg;
            objMappable.subtotal = order.subtotal;
            objMappable.carriage = order.carriage;
            objMappable.etc = order.etc;
            objMappable.tax = order.tax;
            objMappable.coupon_discount = order.coupon_discount;
            // 非定期の最短お届け可能日とお届け希望日が一緒なら、｢最短｣を選択とする
            objMappable.firstTimeOrdinary = (order.senddate == null) ? EnumFirstTimeOrdinary.Shortest : EnumFirstTimeOrdinary.Specify;
            objMappable.mall_shop_kbn = order.mall_shop_kbn;

            var records = ErsFactory.ersOrderFactory.GetErsOrderRecordList(order);

            objMappable.deliv_method = records.First().Value.deliv_method;
        }

        private void SetCtsOrderInfo(ErsCtsOrder order, IOrderMappable objMappable)
        {
            objMappable.cts_order_id = order.id;
            objMappable.cts_order_temp_d_no = order.temp_d_no;
            objMappable.mcode = order.mcode;
            objMappable.cts_order_ransu = order.ransu;
            objMappable.cts_order_status = order.status;
            objMappable.cts_order_memo = order.memo;
            objMappable.cts_order_user_id = order.user_id;
            if (order.addressbook_id != 0)
            {
                objMappable.shipping_id = order.addressbook_id;
            }
            this.RefreshBasket(objMappable);
            objMappable.cts_order_subtotal = objMappable.basket.subtotal; // GetBasketSubTotal();
        }

        private void RefreshOrderBasket(ErsOrder order, IOrderMappable objMappable)
        {
            objMappable.basket = ErsFactory.ersBasketFactory.GetErsBasket();
            objMappable.basket.IsOrderUpdate = objMappable.IsOrderUpdate;
            objMappable.basket.InitOrderBakset(order);
        }

        private void RefreshBasket(IOrderMappable objMappable)
        {
            objMappable.basket = ErsFactory.ersBasketFactory.GetErsBasket();
            objMappable.basket.IsOrderUpdate = objMappable.IsOrderUpdate;
            objMappable.basket.PrepareSession();
            objMappable.basket.Init(objMappable.mcode, objMappable.cts_order_ransu);
        }
    }
}