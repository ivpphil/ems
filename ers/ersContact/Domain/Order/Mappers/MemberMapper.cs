﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Order.Mappables;
using jp.co.ivp.ers;

namespace ersContact.Domain.Order.Mappers
{
    public class MemberMapper
        : IMapper<IMemberMappable>
    {
        public void Map(IMemberMappable objMappable)
        {
            if (objMappable.member_cancel == false && objMappable.member_edit == true)
            {
                // Back from Zip Search - cancelled
                objMappable.member_register = false;
                objMappable.member_modify = true;
                objMappable.member_cancel = false;
                objMappable.member_edit = true;
            }
            else
            {
                this.LoadMember(objMappable);
            }
        }

        private void LoadMember(IMemberMappable objMappable)
        {
            objMappable.member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objMappable.mcode, true);
            if (!objMappable.IsOrderUpdate && objMappable.member == null)
            {
                objMappable.member_register = false;
                objMappable.member_modify = true;
                objMappable.member_cancel = false;
                objMappable.member_edit = true;
            }
            else
            {
                if(objMappable.IsOrderUpdate && objMappable.page1 && !objMappable.IsErrorBack && !objMappable.goto_page1
                    && !objMappable.member_register && !objMappable.member_modify)
                {
                    SetMemberInfoForShippingFromOrder(objMappable);
                }

                if (objMappable.member != null && objMappable.member.deleted != EnumDeleted.Deleted)
                {
                    this.SetMemberInfo(objMappable);
                }
                else if (objMappable.member == null && !objMappable.non_member && !string.IsNullOrEmpty(objMappable.order_d_no))
                {
                    //During initial load  (Non-member)
                    this.SetMemberInfoFromOrder(objMappable);
                }
                else if (objMappable.member == null && objMappable.non_member && objMappable.member_cancel && !string.IsNullOrEmpty(objMappable.order_d_no))
                {
                    //when cancel button is clicked (Non-member)
                    this.GetMemberInfoFromHidden(objMappable);
                }

                if (objMappable.member_modify)
                {
                    objMappable.member_cancel = false;
                    objMappable.member_modify = true;
                    objMappable.member_edit = true;
                }
                else
                {
                    objMappable.member_cancel = true;
                    objMappable.member_modify = false;
                    objMappable.member_edit = false;
                }
            }
        }

        private void SetMemberInfo(IMemberMappable objMappable)
        {
            var member = objMappable.member;

            objMappable.member_id = member.id;
            objMappable.lname = member.lname;
            objMappable.fname = member.fname;
            objMappable.lnamek = member.lnamek;
            objMappable.fnamek = member.fnamek;
            objMappable.tel = member.tel;
            objMappable.zip = member.zip;
            objMappable.pref = member.pref;
            objMappable.address = member.address;
            objMappable.taddress = member.taddress;
            objMappable.maddress = member.maddress;
            objMappable.sex = member.sex;
            objMappable.email = member.email;
            if (member.birth != null)
            {
                objMappable.birthday_y = member.birth.Value.Year;
                objMappable.birthday_m = member.birth.Value.Month;
                objMappable.birthday_d = member.birth.Value.Day;
            }
            objMappable.dm_flg = member.dm_flg;
            objMappable.out_bound_flg = member.out_bound_flg;
            objMappable.w_dm_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.NEED, EnumCommonNameColumnName.namename, (int?)objMappable.dm_flg);
            objMappable.w_out_bound_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.NEED, EnumCommonNameColumnName.namename, (int?)objMappable.out_bound_flg);
            objMappable.deleted = member.deleted;
            objMappable.memo = member.memo;
        }

        private void SetMemberInfoFromOrder(IMemberMappable objMappable)
        {
            var order = ErsFactory.ersOrderFactory.GetOrderWithD_no(objMappable.order_d_no,objMappable.site_id);

            objMappable.lname = order.lname;
            objMappable.fname = order.fname;
            objMappable.lnamek = order.lnamek;
            objMappable.fnamek = order.fnamek;
            objMappable.tel = order.tel;
            objMappable.zip = order.zip;
            objMappable.pref = order.pref;
            objMappable.address = order.address;
            objMappable.taddress = order.taddress;
            objMappable.maddress = order.maddress;
            objMappable.email = order.email;
            objMappable.w_dm_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.NEED, EnumCommonNameColumnName.namename, (int?)objMappable.dm_flg);
            objMappable.w_out_bound_flg = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.NEED, EnumCommonNameColumnName.namename, (int?)objMappable.out_bound_flg);
            objMappable.non_member = true;
            objMappable.deleted = EnumDeleted.NotDeleted;

            objMappable.add_lname = order.add_lname;
            objMappable.add_fname = order.add_fname;
            objMappable.add_lnamek = order.add_lnamek;
            objMappable.add_fnamek = order.add_fnamek;
            objMappable.add_tel = order.add_tel;
            objMappable.add_zip = order.add_zip;
            objMappable.add_address = order.add_address;
            objMappable.add_taddress = order.add_taddress;
            objMappable.add_maddress = order.add_maddress;
            objMappable.add_pref = order.add_pref;
        }

        /// <summary>
        ///  Set unregistered member's information to hidden 
        /// </summary>
        /// <param name="objMappable"></param>
        private void SetMemberInfoForShippingFromOrder(IMemberMappable objMappable)
        {
            var order = ErsFactory.ersOrderFactory.GetOrderWithD_no(objMappable.order_d_no, objMappable.site_id);

            objMappable.wk_lname = order.lname;
            objMappable.wk_fname = order.fname;
            objMappable.wk_lnamek = order.lnamek;
            objMappable.wk_fnamek = order.fnamek;
            objMappable.wk_tel = order.tel;
            objMappable.wk_zip = order.zip;
            objMappable.wk_pref = order.pref;
            objMappable.wk_address = order.address;
            objMappable.wk_taddress = order.taddress;
            objMappable.wk_maddress = order.maddress;
            objMappable.wk_email = order.email;
        }


        /// <summary>
        /// Get unregistered member's information from hidden 
        /// </summary>
        /// <param name="objMappable"></param>
        private void GetMemberInfoFromHidden(IMemberMappable objMappable)
        {
            objMappable.lname = objMappable.wk_lname;
            objMappable.lnamek = objMappable.wk_lnamek;
            objMappable.fname = objMappable.wk_fname;
            objMappable.fnamek = objMappable.wk_fnamek;
            objMappable.tel = objMappable.wk_tel;
            objMappable.zip = objMappable.wk_zip;
            objMappable.address = objMappable.wk_address;
            objMappable.taddress = objMappable.wk_taddress;
            objMappable.maddress = objMappable.wk_maddress;
            objMappable.email = objMappable.wk_email;
            objMappable.pref = objMappable.wk_pref;
        }

    }
}