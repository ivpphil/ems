﻿using ersContact.Domain.Order.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Order.Handlers
{
    public class MemberHandler : ICommandHandler<IMemberCommand>
    {
        public ICommandResult Submit(IMemberCommand command)
        {
            if (command.member_register)
            {
                this.SaveMember(command);
                command.member_edit = false;
            }

            return new CommandResult(true);
        }

        public void SaveMember(IMemberCommand command)
        {
            if (!command.mcode.HasValue())
            {
                if (!command.non_member)
                {
                    this.InsertMember(command);
                }
                else
                {
                    command.wk_lname = command.lname;
                    command.wk_lnamek = command.lnamek;
                    command.wk_fname = command.fname;
                    command.wk_fnamek = command.fnamek;
                    command.wk_tel = command.tel;
                    command.wk_zip = command.zip;
                    command.wk_address = command.address;
                    command.wk_taddress = command.taddress;
                    command.wk_maddress = command.maddress;
                    command.wk_email = command.email;
                    command.wk_pref = command.pref;
                }
            }
            else
            {
                this.UpdateMember(command);
            }
        }

        private void InsertMember(IMemberCommand command)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            ErsMember member = ErsFactory.ersMemberFactory.GetErsMember();

            member.OverwriteWithModel(command);
            //member.email = "";
            member.passwd = "";
            member.pm_flg = EnumPmFlg.CTS;
            member.ques = 0;
            member.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();

            repository.Insert(member, true);

            command.mcode = member.mcode;
        }

        private void UpdateMember(IMemberCommand command)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode, true);

            var time = member.intime;
            //var email = member.email;
            member.OverwriteWithModel(command);
            member.intime = time;

            var old_member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode, true);
            member.pm_flg = old_member.pm_flg;
            member.deleted = old_member.deleted;
            member.site_id = old_member.site_id;
            repository.Update(old_member, member);
        }
    }
}
