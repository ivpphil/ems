﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Order.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.util;
using ersContact.Domain.Cart.Commands;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.Payment;
using ersContact.Domain.Order.Mappables;

namespace ersContact.Domain.Order.Handlers
{
    public class ValidateOrder
           : IValidationHandler<IOrderCommand>
    {
        public IEnumerable<ValidationResult> Validate(IOrderCommand command)
        {
            var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().Retrieve(command.mcode);

            if (command.ValidateMappedOrder)
            {
                return this.ValidateGeneratedOrder(command);
            }
            else
            {
                return this.ValidateInputValues(command, member_rank);
            }
        }

        public virtual IEnumerable<ValidationResult> ValidateGeneratedOrder(IOrderCommand command)
        {
            var order = command.orderContainer.OrderHeader;

            //編集時は変更時のみクーポンチェック
            if (!command.IsOrderUpdate
                || string.IsNullOrEmpty(command.order_d_no)
                || command.old_coupon_code != command.ent_coupon_code
                || command.coupon_code != command.ent_coupon_code)
            {
       
                yield return ErsFactory.ersCouponFactory.GetErsValidateCouponStgy().Validate(command.mcode, command.ent_coupon_code, order.subtotal, command.order_d_no, command.IsOrderUpdate, command.site_id);
            }

            if (command.IsOrderUpdate)
            {
                foreach (var orderRecord in command.orderContainer.OrderRecords.Values)
                {
                    if (command.page3 || command.page4)
                    {
                        if (orderRecord.id != null)
                        {
                            var oldOrderRecord = ErsFactory.ersOrderFactory.GetErsOrderRecordWithId(orderRecord.id);
                            yield return ErsFactory.ersOrderFactory.GetValidateOrderRecordStatusTransition().Validate(oldOrderRecord, orderRecord.order_status, new[] { "order_d_no" });

                        }
                    }
                }

                //全商品キャンセル時
                var isAllCanceled = ErsFactory.ersOrderFactory.GetIsAllRecordCanceledSpec().IsAllCanceled(command.orderContainer.OrderRecords.Values);
                if (isAllCanceled)
                {
                    foreach (var orderRecord in command.orderContainer.OrderRecords.Values)
                    {
                        if (!orderRecord.IsCanceled.Value)
                        {
                            //エラー
                            yield return new ValidationResult(ErsResources.GetMessage("29004"), new[] { "order_d_no" });
                        }
                    }
                }
                else
                {
                    if (command.page3 || command.page4)
                    {
                        //キャンセルゼロ縛り判定
                        foreach (var orderRecord in command.orderContainer.OrderRecords.Values)
                        {
                            if (orderRecord.amount == 0)
                            {
                                //数量０でキャンセルステータス以外はＮＧ
                                if (orderRecord.order_status != EnumOrderStatusType.CANCELED)
                                {
                                    //エラー
                                    yield return new ValidationResult(ErsResources.GetMessage("29004"), new[] { "order_d_no" });
                                }
                            }
                            else
                            {
                                //キャンセルステータスで、数量０以外もＮＧ
                                if (orderRecord.order_status == EnumOrderStatusType.CANCELED)
                                {
                                    //同梱でなければ
                                    if (orderRecord.doc_bundling_flg == EnumDocBundlingFlg.OFF)
                                    {
                                        //エラー
                                        yield return new ValidationResult(ErsResources.GetMessage("29004"), new[] { "order_d_no" });
                                    }
                                }
                            }
                        }
                    }
                }

                //前回カード情報をそのまま使用する場合のチェック
                if (command.last_used_card_info == 1 && command.coupon_flg == false && (command.page3 || command.page4))
                {
                    yield return ErsFactory.ersOrderFactory.GetLastUsedCardInfoStrategy().LastUsedCardInfoValidate(order, command.order_d_no);
                }

            }

            //新規、編集共に判定必要
            //フロアリミット金額チェック
            if (command.pay != null && (command.page3 || command.page4))
            {
                if (!command.IsOrderUpdate || command.pay != EnumPaymentType.NON_NEEDED_PAYMENT)
                {
                    yield return ErsFactory.ersOrderFactory.GetCheckFloorLimitStgy().Validate(command.pay, command.disp_order_total_amount.Value, this.EnvPM_flg(command));
                }
            }
        }

        //処理中必要となる暫定PM_FLGを設定
        public virtual EnumPmFlg EnvPM_flg(IOrderCommand command)
        {
            if (command.pm_flg != null)
            {
                return (EnumPmFlg)command.pm_flg;
            }
            return EnumPmFlg.CTS;  //2:CTS
        }

        public virtual IEnumerable<ValidationResult> ValidateInputValues(IOrderCommand command, int? member_rank)
        {
            var isMall = false;

            if (command.IsOrderUpdate && !string.IsNullOrEmpty(command.order_d_no))
            {
                var order = ErsFactory.ersOrderFactory.GetOrderWithD_no(command.order_d_no,command.site_id);

                if (order != null && !string.IsNullOrEmpty(order.mall_d_no))
                {
                    isMall = true;
                }
            }

            if (command.member_register)
            {
                if (!isMall || (string.IsNullOrEmpty(command.lname) && string.IsNullOrEmpty(command.fname)))
                {
                    yield return command.CheckRequired("lname");
                    yield return command.CheckRequired("fname");
                }
                if (!isMall || (string.IsNullOrEmpty(command.lnamek) && string.IsNullOrEmpty(command.fnamek)))
                {
                    yield return command.CheckRequired("lnamek");
                    yield return command.CheckRequired("fnamek");
                }
                yield return command.CheckRequired("tel");
                yield return ErsFactory.ersMemberFactory.GetCheckEmailRequiredStgy().Check(command.mcode, command.email);

                yield return command.CheckRequired("zip");
                yield return command.CheckRequired("pref");
                foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("pref", command.pref, false))
                {
                    yield return result;
                }
                if (command.IsValidField("zip", "pref"))
                {
                    yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("zip", command.zip, "pref", command.pref, false);
                }

                yield return command.CheckRequired("address");

                if (!isMall)
                {
                    yield return command.CheckRequired("taddress");
                }

                if (!command.non_member)
                {
                    yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailStgy().CheckDuplicate(command.mcode, command.email);
                    yield return command.CheckRequired("sex");
                    yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("sex", EnumCommonNameType.Sex, (int?)command.sex);

                    if (command.dm_flg == null)
                    {
                        command.dm_flg = EnumDmFlg.NoNeed;
                    }
                    if (command.out_bound_flg == null)
                    {
                        command.out_bound_flg = EnumOutBoundFlg.NoNeed;
                    }
                }
            }

            if (command.shipping_register)
            {
                if (!isMall || (string.IsNullOrEmpty(command.add_lname) && string.IsNullOrEmpty(command.add_fname)))
                {
                    yield return command.CheckRequired("add_lname");
                    yield return command.CheckRequired("add_fname");
                }
                //if (!isMall || (string.IsNullOrEmpty(command.add_lnamek) && string.IsNullOrEmpty(command.add_fnamek)))
                //{
                //    yield return command.CheckRequired("add_lnamek"); //海外配送対応
                //    yield return command.CheckRequired("add_fnamek"); //海外配送対応
                //}
                yield return command.CheckRequired("add_tel");
                yield return command.CheckRequired("add_zip");
                yield return command.CheckRequired("add_pref");
                foreach (var result in ErsFactory.ersMemberFactory.GetValidatePrefStgy().Validate("add_pref", command.add_pref, false))
                {
                    yield return result;
                }
                if (command.IsValidField("add_zip", "add_pref"))
                {
                    yield return ErsFactory.ersCommonFactory.GetValidateZipPrefStgy().Validate("add_zip", command.add_zip, "add_pref", command.add_pref, false);
                }

                yield return command.CheckRequired("add_address");

                if (!isMall)
                {
                    yield return command.CheckRequired("add_taddress");
                }
            }

            //入力クーポンコードを大文字にする
            if (!string.IsNullOrEmpty(command.ent_coupon_code))
            {
                command.ent_coupon_code = command.ent_coupon_code.ToUpper();
            }

            if (command.goto_page1)
            {
                //for back ent_coupon_code clear
                command.ent_coupon_code = null;
            }
            if ((command.goto_page3 && command.Edit_Temp == 1) || command.cts_order_register)
            {
                yield return command.CheckRequired("lname");
                yield return command.CheckRequired("fname");
                yield return command.CheckRequired("lnamek");
                yield return command.CheckRequired("fnamek");
                yield return command.CheckRequired("tel");
                yield return command.CheckRequired("cts_order_ransu");
                if (command.cts_order_id == 0)
                {
                    yield return ErsFactory.ersCtsOrderFactory.GetCheckDuplicateCtsOrderStgy().CheckDuplicate(command.mcode, command.cts_order_ransu);
                }
            }

            if (command.goto_page1)
            {
                command.reaload_cart = false;
            }

            if (command.goto_page2 || command.goto_page3 || command.goto_page4)
            {
                yield return command.CheckRequired("pm_flg");

                //check if the ccode value is registered to campaign_t.
                if (command.ccode != null)
                {
                    foreach (var result in ErsFactory.ersDocBundleFactory.GetCampaignCheckExistStrategy().Check(command.ccode))
                    {
                        yield return result;
                    }
                }
            }

            if (command.goto_page2)
            {
                if (!command.IsOrderUpdate)
                {
                    if (!command.mcode.HasValue())
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("10215"));
                    }
                }

                if (command.send_chk == 1 && !command.non_member)
                {
                    yield return command.CheckRequired("shipping", "shipping_id");
                }
                else
                {
                    yield return command.CheckRequired("lname");
                    yield return command.CheckRequired("fname");
                    yield return command.CheckRequired("lnamek");
                    yield return command.CheckRequired("fnamek");
                    yield return command.CheckRequired("tel");
                    yield return command.CheckRequired("address");
                    yield return command.CheckRequired("taddress");
                }
            }

            if (command.goto_page2 || command.Edit_Temp == 1)
            {
                this.RefreshBasket(command);
            }

            if ((command.goto_page3 || command.regular_recalc) && command.Edit_Temp == 0 )
            {
                if (command.basket != null)
                {
                    var controller = command.controller;
                    foreach (var model in command.basketItems)
                    {
                        model.AddInvalidField(controller.commandBus.Validate<ICartRecordCommand>(model));

                        if (!model.IsValid)
                        {
                            foreach (var errorMessage in model.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "basketItems" });
                            }
                        }
                    }
                }

                if (command.regularBasketItems != null)
                {
                    var controller = command.controller;
                    foreach (var model in command.regularBasketItems)
                    {
                        model.AddInvalidField(controller.commandBus.Validate<ICartRegularRecordCommand>(model));

                        if (!model.IsValid)
                        {
                            foreach (var errorMessage in model.GetAllErrorMessageList())
                            {
                                yield return new ValidationResult(errorMessage, new[] { "regularBasketItems" });
                            }
                        }
                    }
                }
            }

            //this.CreatePointsView();

            if ((command.page2 && !command.goto_page1) || command.goto_page3 || command.goto_page4)
            {
                if (!command.coupon_flg)
                {
                    var ifRecomputeClicked = command.recompute;
                    if (command.IsOrderUpdate)
                    {
                        //always true when order update in order to skip recalculate check.
                        ifRecomputeClicked = true;
                    }

                    var points_available = ErsFactory.ersMemberFactory.GetObtainMemberPointStgy().GetPoint(command.mcode, command.site_id);
                    var old_order = ErsFactory.ersOrderFactory.GetOrderWithD_no(command.order_d_no, command.site_id);

                    if (command.IsOrderUpdate)
                    {
                        points_available += old_order.p_service;
                    }

                    var pointCheckResult = ErsFactory.ersOrderFactory.GetCheckPointsStgy().CheckPoints(points_available, command.ent_point, command.p_service, ifRecomputeClicked);
                    if (pointCheckResult != null && (old_order == null || string.IsNullOrEmpty(old_order.mall_d_no)))
                    {
                        command.p_service = 0;
                        yield return pointCheckResult;
                    }
                    else
                    {
                        //入力されたポイントはp_serviceへセット
                        command.p_service = command.ent_point;
                    }

                    if (!command.recompute)
                    {
                        ////再計算ボタン未押下チェック
                        if (command.coupon_code != command.ent_coupon_code)
                        {
                            if(!command.IsOrderUpdate)
                            {
                                // recompute for coupon code is not need in order updating
                                yield return new ValidationResult(ErsResources.GetMessage("20212"), new[] { "ent_coupon_code" });
                            }
                        }
                    }
                }
            }

            var objMappable = (IOrderRecomputeMappable)command;
            if (command.recompute || command.coupon_flg)
            {
                command.controller.mapperBus.Map<IOrderRecomputeMappable>(objMappable);
            }

            if ((command.goto_page3 || command.goto_page4) && command.Edit_Temp == 0)
            {
                yield return command.CheckRequired("deliv_method");

                if (!command.IsNonNeededPaymentSpec)
                {
                    yield return command.CheckRequired("pay");
                    yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().Validate("pay", command.pay, !command.IsOrderUpdate && command.regularBasketItemCount > 0, false);

                    var shipping_pref = this.GetShippingPref(command);
                    yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().ValidateOverseas("pay", command.pay, shipping_pref);

                    if (!command.IsOrderUpdate && command.pay == EnumPaymentType.NON_NEEDED_PAYMENT)
                    {
                        yield return new ValidationResult(ErsResources.GetMessage("10207", ErsResources.GetFieldName("pay")), new[] { "pay" });
                    }

                    //コンビニ用Validation
                    if (command.pay == EnumPaymentType.CONVENIENCE_STORE)
                    {
                        yield return command.CheckRequired("conv_code");
                        if (command.IsValidField("conv_code"))
                        {
                            yield return ErsFactory.ersCommonFactory.GetValidateCommonNameCodeStgy().Validate("conv_code", EnumCommonNameType.ConvCode, (int?)command.conv_code);
                        }

                        yield return ErsFactory.ersCtsOrderFactory.GetCheckMemberEmailExist().ValidateExistEmail(command.email, EnumPaymentType.CONVENIENCE_STORE);
                    }
                    //銀行振り込み用Validation
                    if (command.pay == EnumPaymentType.BANK)
                    {
                        yield return ErsFactory.ersCtsOrderFactory.GetCheckMemberEmailExist().ValidateExistEmail(command.email, EnumPaymentType.BANK);
                    }
                }
                else
                {
                    yield return command.CheckRequired("pay");
                }

                //初回購入時
                if (string.IsNullOrEmpty(command.order_d_no))
                {
                    if (command.up_amount_list != null)
                    {
                        foreach (var keys in command.up_amount_list.Keys)
                        {
                            if (command.up_amount_list[keys] == 0)
                            {
                                yield return new ValidationResult(ErsResources.GetMessage("29005"));
                                break;
                            }
                        }
                    }
                }

                if (command.firstTimeOrdinary == EnumFirstTimeOrdinary.Specify)
                {
                    yield return command.CheckRequired("senddate");

                    if (!command.IsOrderUpdate)
                    {
                        foreach (var result in ErsFactory.ersOrderFactory.GetValidateSenddateStgy().Validate(DateTime.Now, command.senddate))
                        {
                            yield return result;
                        }
                    }
                }

                if (command.regularBasketItemCount != 0)
                {
                    yield return command.CheckRequired("disable_issue");
                }

                if (command.basketItemCount != 0)
                {
                    yield return command.CheckRequired("sendtime");
                    foreach (var result in ErsFactory.ersOrderFactory.GetValidateSendtimeStgy().Validate(command.sendtime))
                    {
                        yield return result;
                    }
                }

                command.controller.mapperBus.Map<IOrderRecomputeMappable>(objMappable);

                //別住所でカード以外は不可
                //if (objMappable.IsOrderUpdate && objMappable.orderContainer.OrderHeader.mall_shop_kbn == EnumMallShopKbn.ERS && !isMall)
                //{
                //    yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().ValidateRecipientName(
                //        "pay", command.pay, command.send,
                //        command.lname, command.add_lname,
                //        command.fname, command.add_fname,
                //        command.lnamek, command.add_lnamek,
                //        command.fnamek, command.add_fnamek
                //     );
                //}

                yield return ErsFactory.ersOrderFactory.GetValidateSendStgy().ValidateAnotherAddressSave("address_add", command.send, command.address_add, command.member_add_id, ErsContext.sessionState.Get("contact_ransu"));

                if (command.goto_page3 && command.pay != null && command.regularBasketItems != null && command.regularBasketItems.Count > 0)
                {
                    //入力チェックの為、一時的にバスケットに値を入れる。
                    using (var tx = ErsDB_parent.BeginTransaction())
                    {
                        command.basket.ReComputeRegular(command.regularBasketItems, member_rank);

                        //一時的にバスケットに入れるので、コミットはしない！！

                        //20121115:定期商品を含みクレジット支払い、カード未保存はエラー
                        EnumCardSave card_save = EnumCardSave.NotSave;
                        if (command.card_will_add)
                        {
                            card_save = EnumCardSave.Save;
                        }
                        //20121115:定期商品を含みクレジット支払い以外はエラー
                        yield return ErsFactory.ersOrderFactory.GetValidatePayStgy().ValidateCardSave(command.pay, card_save, ((IPaymentInfoGmoInputContainer)command).card_id, ErsContext.sessionState.Get("contact_ransu"));
                    }

                    //バスケットを再ロード
                    command.controller.mapperBus.Map<IOrderRecomputeMappable>(objMappable);
                }

                //メール便が選択された場合代引き選択不可
                yield return ErsFactory.ersOrderFactory.GetCheckMailDeliveryStgy().Validate(command.deliv_method, command.pay);

                //メール便が選択された場合は配送日・配送時刻指定不可
                yield return ErsFactory.ersOrderFactory.GetCheckMailDeliveryStgy().CkSendDateTimeBasketMailDelivery(command.deliv_method, command.senddate, command.sendtime);
            }

            if (command.goto_page2 || command.goto_page3 || command.goto_page4)
            {
                command.basket.CheckBasketItems(command.IsOrderUpdate);
            }
            
            if (command.goto_page3 || command.goto_page4)
            {

                if (command.regularBasketItems != null && command.up_amount_list != null && command.up_amount_list.Where((value) => value.Value > 0).Count() > 0)
                {
                    foreach (var model in command.regularBasketItems)
                    {
                        if (model.send_ptn.HasValue)
                        {
                            var regularPatternService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(model.send_ptn.Value);
                            var next_date = regularPatternService.CalculateFirstTime(model, DateTime.Now);
                            if ((command.firstTimeOrdinary == EnumFirstTimeOrdinary.WithRegular || command.senddate == next_date) && command.sendtime != command.regular_sendtime)
                            {
                                yield return new ValidationResult(ErsResources.GetMessage("20222"), new[] { "sendtime" });
                            }
                        }
                    }
                }

            }

            if (command.card_delete)
            {
                yield return ErsFactory.ersMemberFactory.GetCheckInOperationCardRegularOrder().CheckInOperationCard(command.mcode, ((IPaymentInfoGmoInputContainer)command).card_id);
            }
            if (command.card_update)
            {
                yield return command.CheckRequired("card_type");
                yield return command.CheckRequired("cardno");
                yield return command.CheckRequired("validity_y");
                yield return command.CheckRequired("validity_m");
                yield return ErsFactory.ersOrderFactory.GetCheckValidityStgy().Check(command.validity_y, command.validity_m);
            }

            if (command.goto_page3 && command.pay == EnumPaymentType.CREDIT_CARD && !command.IsNonNeededPaymentSpec && command.mall_shop_kbn == EnumMallShopKbn.ERS)
            {
                //前回カード情報をそのまま使用しない場合のみチェック
                if (command.last_used_card_info != 1)
                {
                    if (((IPaymentInfoGmoInputContainer)command).card_id == CreditCardInfo.IgnoreCardSequenceValue)
                    {
                        yield return command.CheckRequired("card_type");
                        yield return command.CheckRequired("cardno");
                        yield return command.CheckRequired("validity_y");
                        yield return command.CheckRequired("validity_m");
                        yield return ErsFactory.ersOrderFactory.GetCheckValidityStgy().Check(command.validity_y, command.validity_m);
                    }
                    else
                    {
                        yield return command.CheckRequired("カード選択", "card_id", ((IPaymentInfoGmoInputContainer)command).card_id);
                    }
                }
            }
        }

        /// <summary>
        /// 配送先都道府県を取得する
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private int? GetShippingPref(IOrderCommand command)
        {
            if(command.shipping_id == null)
            {
                return null;
            }

            if (command.shipping_id == 0)
            {
                return command.pref;
            }

            if (command.shipping_id == -1)
            {
                return command.wk_add_pref;
            }

            var objAddress = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(command.shipping_id, command.mcode);
            if (objAddress == null)
            {
                return null;
            }

            return objAddress.add_pref;
        }

        private void RefreshBasket(IOrderCommand command)
        {
            command.basket = ErsFactory.ersBasketFactory.GetErsBasket();
            command.basket.IsOrderUpdate = command.IsOrderUpdate;
            command.basket.PrepareSession();
            command.basket.Init(command.mcode, command.cts_order_ransu);
        }
    }
}