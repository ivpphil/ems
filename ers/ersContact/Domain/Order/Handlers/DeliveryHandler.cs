﻿using ersContact.Domain.Order.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Order.Handlers
{
    public class DeliveryHandler : ICommandHandler<IDeliveryCommand>
    {
        public ICommandResult Submit(IDeliveryCommand command)
        {
            this.RefreshBasket(command);

            if (command.page2)
            {
                //2013/01/11 Y.Hamano クーポン適用、送料再計算でもバスケット情報更新
                if (command.coupon_flg || command.carriage_recomp_flg || command.recalc)
                {
                    command.basket.IsOrderUpdate = command.IsOrderUpdate;
                    command.basket.ReCompute(command.mcode, command.up_amount_list, command.product_status_list);
                    command.reaload_cart = true;
                }
            }

            if (command.page3)
            {
                command.basket.IsOrderUpdate = command.IsOrderUpdate;
                command.basket.ReCompute(command.mcode, command.up_amount_list, command.product_status_list);
                command.reaload_cart = true;
            }

            return new CommandResult(true);
        }

        private void RefreshBasket(IDeliveryCommand command)
        {
            command.basket = ErsFactory.ersBasketFactory.GetErsBasket();
            command.basket.IsOrderUpdate = command.IsOrderUpdate;
            command.basket.PrepareSession();
            command.basket.Init(command.mcode, command.cts_order_ransu);
        }
    }
}
