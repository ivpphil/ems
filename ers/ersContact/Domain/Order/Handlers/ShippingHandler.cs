﻿using System;
using ersContact.Domain.Order.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Order.Handlers
{
    public class ShippingHandler : ICommandHandler<IShippingCommand>
    {
        public ICommandResult Submit(IShippingCommand command)
        {
            //未登録別住所では登録無しで遷移
            if (command.shipping_id == -1)
            {
                if (command.shipping_register && command.address_add == EnumAddressAdd.Add)
                {
                    //追加登録
                    command.shipping_id = null;
                }
                else if (command.shipping_register)
                {
                    //追加無し登録
                    if (!command.non_member)
                    {
                        if (!command.IsOrderUpdate || command.deleted != EnumDeleted.Deleted)
                        {
                            command.shipping_register = false;
                            UpdateOrderAddAddress(command);
                        }
                    }
                    command.shipping_edit = false;
                }
            }

            if (command.shipping_delete)
            {
                this.DeleteShipping(command);
            }
            else if (command.shipping_register)
            {
                this.SaveShipping(command);
            }

            return new CommandResult(true);
        }

        private void DeleteShipping(IShippingCommand command)
        {
            if (command.shipping_id > 0)
            {
                var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
                ErsAddressInfo address = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(Convert.ToInt32(command.shipping_id), command.mcode);

                repository.Delete(address);
            }
        }

        public void SaveShipping(IShippingCommand command)
        {
            if (command.IsOrderUpdate && (command.deleted == EnumDeleted.Deleted || command.non_member))
            {
                //伝票UPDATE時に非会員購入の場合は保存しない。
                return;
            }

            if (command.shipping_id == null)
            {
                command.shipping_id = 0;
            }

            if (command.shipping_id == 0)
            {
                this.InsertShipping(command);
            }
            else
            {
                this.UpdateShipping(command);
            }
        }

        private void InsertShipping(IShippingCommand command)
        {
            var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            ErsAddressInfo address = ErsFactory.ersAddressInfoFactory.GetErsAddressInfo();

            address.OverwriteWithModel(command);
            address.id = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository().GetNextSequence();
            address.address_name = Convert.ToString(address.id);
            address.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();
            repository.Insert(address, false);

            command.shipping_id = address.id;
        }

        private void UpdateShipping(IShippingCommand command)
        {
            var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            ErsAddressInfo address = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(command.shipping_id, command.mcode);

            address.OverwriteWithModel(command);

            var old_address = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoWithId(command.shipping_id, command.mcode);
            address.address_name = old_address.address_name;
            address.intime = old_address.intime;
            address.site_id = old_address.site_id;
            repository.Update(old_address, address);
        }

        private void UpdateOrderAddAddress(IShippingCommand command)
        {
            var oldOrder = ErsFactory.ersOrderFactory.GetOrderWithD_no(command.order_d_no, command.site_id);
            var newOrder = ErsFactory.ersOrderFactory.GetErsOrder();

            newOrder.OverwriteWithModel(oldOrder);
            newOrder.add_lnamek = command.add_lnamek;
            newOrder.add_lname = command.add_lname;
            newOrder.add_fnamek = command.add_fnamek;
            newOrder.add_fname = command.add_fname;
            newOrder.add_zip = command.add_zip;
            newOrder.add_pref = command.add_pref;
            newOrder.add_address = command.add_address;
            newOrder.add_taddress = command.add_taddress;
            newOrder.add_maddress = command.add_maddress;
            newOrder.add_tel = command.add_tel;

            command.send = EnumSendTo.ANOTHER_ADDRESS;
            command.shipping_id = 0;

            ErsFactory.ersOrderFactory.GetErsOrderRepository().Update(oldOrder, newOrder);
        }
    }
}
