﻿using System;
using System.Linq;
using System.Collections.Generic;
using ersContact.Domain.Order.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.ctsorder;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.order.specification;
using jp.co.ivp.ers.order.strategy;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using ersContact.Domain.Order.Mappables;
using jp.co.ivp.ers.mall;
using jp.co.ivp.ers.mall.api.order_status;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.basket;

namespace ersContact.Domain.Order.Handlers
{
    public class OrderHandler : ICommandHandler<IOrderCommand>
    {
        public ICommandResult Submit(IOrderCommand command)
        {
            if (command.pay == EnumPaymentType.CREDIT_CARD)
            {
                this.SaveCard(command);
            }

            this.SaveOrder(command);

            command.order_success = true;

            return new CommandResult(true);
        }

        private void SaveCard(IOrderCommand command)
        {
            var memberCard = new ErsMemberCard();
            if (command.card_will_add)
            {
                var cardInfo = new CreditCardInfo(command.card_holder_name, command.card_type, command.cardno, command.validity_y, command.validity_m, command.securityno);
                var objMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode, true);

                //Save credit card info to gmo.
                var ersPaymentCredit = (IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD);
                memberCard = ersPaymentCredit.SaveMemberCardInfo(cardInfo, objMember);

                //Insert credit card info to database.
                var oldMemberCard = ErsFactory.ersMemberFactory.GetRetrieveAlreadyStoredMemberCardStgy().Retrieve(memberCard.card_mcode, memberCard.card_sequence);
                if (oldMemberCard == null)
                {
                    var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                    memberCardRepository.Insert(memberCard, true);
                }
                else
                {
                    memberCard = oldMemberCard;
                }
                ((IPaymentInfoGmoInputContainer)command).card_id = memberCard.id;
            }
        }

        private void SaveOrder(IOrderCommand command)
        {
            //TODO:  Transfer function to new Strategy Class
            if (!command.IsOrderUpdate)
            {
                using (var tx  = ErsDB_parent.BeginTransaction())
                {
                    this.InsertOrder(command);
                    tx.Commit();
                }
            }
            else
            {
                //UpdateOrder内でトランザクションを制御します
                this.UpdateOrder(command);
            }
        }

        private void InsertOrder(IOrderCommand command)
        {
            this.LoadDefaultParameters(command);

            var Result = ErsFactory.ersMemberFactory.GetErsMemberRankSetupSearchSpecification().SelectAsList(command.mcode);
            if (Result.Count > 0)
            {
                command.point_magnification = (int)Result[0]["point_magnification"];
            }
            else
            {
                command.point_magnification = 100;
            }


            command.wrap = EnumWrap.NotWish;
            command.memo2 = null;
            command.member_add_id = command.shipping_id;

            command.ransu = command.cts_order_ransu;
            command.memo3 = command.cts_order_memo;
            command.user_id = command.cts_order_user_id;

            var tmp_pay = command.pay;

            if (command.disp_order_total_amount == 0)
            {
                command.pay = EnumPaymentType.NON_NEEDED_PAYMENT;
            }

            //->regular order include
            var orderIntegrated = ErsFactory.ersOrderFactory.GetErsOrderIntegrated();
            orderIntegrated.OverwriteWithParameter(command.GetPropertiesAsDictionary());
            orderIntegrated.GenerateOrder(command, command.basket, command.deliv_method);

            command.orderIntegrated = orderIntegrated;

            var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(orderIntegrated.pay);
            objPayment.SetPaymentMethod(orderIntegrated, command);

            ErsFactory.ersOrderFactory.GetSetD_noStgy().SetNext(orderIntegrated, null);
            command.d_no = orderIntegrated.d_no;

            orderIntegrated.order_payment_status = EnumOrderPaymentStatusType.NOT_PAID;

            if (command.basket.objRegularBasketRecord.Count > 0)
            {
                command.pay = tmp_pay;

                //insert regular bill here
                ErsFactory.ersOrderFactory.GetRegistRegularOrderStgy().Regist(command, command.basket, orderIntegrated, command.regular_sendtime);
            }

            if (command.disp_order_total_amount == 0)
            {
                command.pay = EnumPaymentType.NON_NEEDED_PAYMENT;
            }

            //if disable_issue is checked, the billing will be skipped.
            if (!command.disable_issue)
            {
                foreach (var innerOrder in orderIntegrated.GetListOrder())
                {
                    this.PrepareBills(innerOrder.OrderHeader, innerOrder.OrderRecords, command);
                }

                //Update Member data
                this.UpdateMember(command.member, orderIntegrated.subtotal);

                if (command.cts_order_temp_d_no == 0)
                {
                    this.UpdateStock(command);
                }
            }

            this.EmptyBasket(command);

            this.SoftDeleteCtsOrder(command);

            command.order_d_no = orderIntegrated.d_no;

            //if disable_issue is checked, the billing will be skipped.
            if (!command.disable_issue)
            {
                // モール在庫更新 [Update mall stock]
                var listParam = this.UpdateMallStock(command.orderRecords);

                try
                {
                    //Executes payment with order instance
                    foreach (var innerOrder in orderIntegrated.GetListOrder())
                    {
                        this.ExecutePayment(innerOrder.OrderHeader, command);
                    }
                }
                catch
                {
                    if (listParam.Count > 0)
                    {
                        try
                        {
                            // モール連携在庫リカバリ登録 [Register mall stock recovery]
                            ErsMallFactory.ersMallStockRecoveryFactory.GetRegisterMallStockRecoveryStgy().Register(listParam);
                        }
                        catch (Exception eTmp)
                        {
                            ErsCommon.loggingException(string.Format("Failed to register mall stock recovery.\r\n{0}", eTmp.ToString()));
                        }
                    }

                    throw;
                }
            }
        }

        /// <summary>
        /// モール在庫更新 [Update mall stock]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        protected IList<UpdateStockParam> UpdateMallStock(IEnumerable<ErsBaskRecord> orderRecords)
        {
            var listParam = new List<UpdateStockParam>();

            foreach (var record in orderRecords)
            {
                if (record.h_mall_flg != EnumOnOff.On)
                {
                    continue;
                }

                if (record.set_flg != EnumSetFlg.IsNotSet)
                {
                    //子商品リスト取得
                    var DecreaseSetItemList = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(record.scode);

                    foreach (var setProduct in DecreaseSetItemList)
                    {
                        var chiled_merchandise = ErsFactory.ersMerchandiseFactory.GetErsMerchandiseWithScode(setProduct.scode, null);

                        if (chiled_merchandise.h_mall_flg == EnumOnOff.Off)
                        {
                            continue;
                        }

                        UpdateStockParam param = default(UpdateStockParam);

                        param.productCode = setProduct.scode;
                        param.quantity = record.amount * setProduct.amount;
                        param.operation = EnumMallStockOperation.sub;

                        listParam.Add(param);
                    }
                }
                else
                {
                    UpdateStockParam param = default(UpdateStockParam);

                    param.productCode = record.scode;
                    param.quantity = record.amount;
                    param.operation = EnumMallStockOperation.sub;

                    listParam.Add(param);
                }
            }

            // モール在庫更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }

            return listParam;
        }

        private void UpdateOrder(IOrderCommand command)
        {
            this.SetValueToCommand(command);

            this.UpdateOrderData(command);

            this.EmptyBasket(command);
        }

        private void SetValueToCommand(IOrderCommand command)
        {
            this.LoadDefaultParameters(command);

            if (ErsFactory.ersOrderFactory.GetIsNonNeededPaymentSpec().IsSpecified(command.orderContainer.OrderHeader.total))
            {
                command.pay = EnumPaymentType.NON_NEEDED_PAYMENT;
            }

            this.LoadOldParameters(command);

            foreach (var item in command.basket.objRegularBasketRecord.Values)
            {
                ErsFactory.ersBasketFactory.GetOrdinaryAddToCartStgy().AddMerchandise(command.basket, item, item.amount, EnumOrderType.Subscription);
            }

            if (command.shipping_id > 0)
            {
                command.member_add_id = command.shipping_id;
            }
            this.SetAgentInfo(command);
        }

        private void LoadDefaultParameters(IOrderCommand command)
        {
            command.send = (command.shipping_id == 0) ? EnumSendTo.MEMBER_ADDRESS : EnumSendTo.ANOTHER_ADDRESS;

            if (command.send != EnumSendTo.ANOTHER_ADDRESS)
            {
                command.member_add_id = 0;
            }
            else
            {
                if (command.shipping_id > 0)
                {
                    command.member_add_id = command.shipping_id;
                }
            }
            command.member_card_id = ((IPaymentInfoGmoInputContainer)command).card_id;

            var objMappable = (IOrderRecomputeMappable)command;
            command.controller.mapperBus.Map<IOrderRecomputeMappable>(objMappable);
        }

        /// <summary>
        /// Load into Model columns that are not part of the edit (that might have gotten lost along the process)
        /// </summary>
        private void LoadOldParameters(IOrderCommand command)
        {
            //TODO: Add more necessary columns
            var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var old_order = ErsFactory.ersOrderFactory.GetOrderWithD_no(command.order_d_no, command.site_id);
            if (old_order == null)
            {
                throw new ErsException("30104", command.order_d_no);
            }

            command.point_ck = old_order.point_ck;
            command.order_payment_status = old_order.order_payment_status;
            command.paid_date = old_order.paid_date;
            command.paid_price = old_order.paid_price;
            command.wrap = old_order.wrap;
            command.memo2 = old_order.memo2;
            command.intime = old_order.intime;
            command.coupon_code = command.orderContainer.OrderHeader.coupon_code;
            command.old_coupon_discount = command.orderContainer.OrderHeader.coupon_discount;
        }

        internal void ExecutePayment(ErsOrder order, IOrderCommand command)
        {
            var oldOrder = ErsFactory.ersOrderFactory.GetErsOrderWithParameters(order.GetPropertiesAsDictionary());

            var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(order.pay);
            objPayment.SetPaymentMethod(order, command);

            objPayment.SendAuth(order, command.member, command.isKanshi);

            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            repository.Update(oldOrder, order);
        }

        private void PrepareBills(ErsOrder order, IDictionary<string, ErsOrderRecord> orderRecords, IOrderCommand command)
        {
            order.mcode = command.mcode;
            order.ransu = command.cts_order_ransu;
            order.memo3 = command.cts_order_memo;
            if (!order.intime.HasValue)
            {
                order.intime = DateTime.Now;
            }
            command.intime = order.intime;

            //Update member data.
            var oldmember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode, true);

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            repository.Update(oldmember, command.member);

            if (command.p_service > 0)
            {
                //Use points
                var reason = ErsResources.GetFieldName("point_reason_purchase");
                ErsFactory.ersOrderFactory.GetUpdateMemberPointStgy().Decrease(command.member.mcode, order.p_service, reason, order.d_no, order.intime, (int)order.site_id);

                // set payment to paid.
                if (order.total == 0)
                {
                    order.order_payment_status = EnumOrderPaymentStatusType.PAID;
                }
            }

            var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            orderRepository.Insert(order, true);

            var orderRecordRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var orderMailRepository = ErsFactory.ersOrderFactory.GetErsOrderMailRepository();

            foreach (var orderRecord in orderRecords.Values)
            {
                ///insert ds_master_t here
                orderRecord.deliv_method = command.deliv_method;
                orderRecordRepository.Insert(orderRecord, true);

                var newOrderMail = this.GetOrderMail(orderRecord);
                orderMailRepository.Insert(newOrderMail);

                //Increase or Decrease the Warehouse Stock
                this.WareHouseStockMonitor(orderRecord);
            }

            ErsFactory.ersOrderFactory.GetRegistSetItemsStgy().Regist(orderRecords.Values);
        }

        /// <summary>
        /// メール送信判定レコード取得
        /// </summary>
        /// <param name="objOrder"></param>
        /// <param name="orderRecord"></param>
        /// <returns></returns>
        private ErsOrderMail GetOrderMail(ErsOrderRecord orderRecord)
        {
            var newOrderMail = ErsFactory.ersOrderFactory.GetErsOrderMail();
            newOrderMail.d_no = orderRecord.d_no;
            newOrderMail.ds_id = orderRecord.id;
            newOrderMail.purchase_mail = EnumSentFlg.Sent;
            newOrderMail.site_id = orderRecord.site_id;
            return newOrderMail;
        }

        private void UpdateStock(IOrderCommand command)
        {
            var decreaseStockStgy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetDecreaseStockStgy();

            foreach (var baskRecord in command.orderRecords)
            {
                decreaseStockStgy.Decrease(baskRecord.scode, baskRecord.amount);
            }
        }

        private void EmptyBasket(IOrderCommand command)
        {
            ErsFactory.ersBasketFactory.GetEmptyBasketStrategy().EmptyBasket(command.cts_order_ransu);
        }

        /// <summary>
        /// DELETE cts_order_t
        /// </summary>
        /// <param name="command"></param>
        private void SoftDeleteCtsOrder(IOrderCommand command)
        {
            if (command.cts_order_temp_d_no > 0)
            {
                var repository = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderRepository();
                var cri = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderCriteria();
                cri.temp_d_no = command.cts_order_temp_d_no.ToString();
                repository.Delete(cri);
            }
        }

        private void SetAgentInfo(IOrderCommand command)
        {
            var agent = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(command.cts_agent_id);
            if (agent == null)
            {
                throw new ErsException("29002");
            }

            command.cts_order_user_id = agent.id.Value.ToString();
            command.cts_order_charge1_ag_type = agent.ag_type;
            command.cts_order_charge1_ag_name = agent.ag_name;
        }

        internal void UpdateOrderData(IOrderCommand command)
        {
            //更新用の伝票データを取得
            command.GetOrderForUpdate = true;
            command.controller.mapperBus.Map<IOrderRecomputeMappable>((IOrderRecomputeMappable)command);
            command.GetOrderForUpdate = false;

            ErsFactory.ersOrderFactory.GetSetD_noStgy().Set(command.orderContainer.OrderHeader, command.orderContainer.OrderRecords.Values, command.order_d_no);

            string[] d_no = command.order_d_no.Split('-');
            command.orderContainer.OrderHeader.base_d_no = d_no[0];

            var new_order = command.orderContainer.OrderHeader;

            var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var old_order = ErsFactory.ersOrderFactory.GetOrderWithD_no(command.order_d_no, command.site_id);
            if (old_order == null)
            {
                throw new ErsException("30104", command.order_d_no);
            }

            bool doVoidAuth;
            using (var tx = ErsDB_parent.BeginTransaction())
            {
                var newOrderRecords = command.orderContainer.OrderRecords;
                var oldOrderRecords = ErsFactory.ersOrderFactory.GetErsOrderProductRecordList(old_order);

                var isAllCanceled = ErsFactory.ersOrderFactory.GetIsAllRecordCanceledSpec().IsAllCanceled(newOrderRecords.Values);

                new_order.id = old_order.id;
                new_order.ransu = command.cts_order_ransu;
                new_order.memo3 = command.cts_order_memo;
                new_order.memo = old_order.memo;
                new_order.site_id = old_order.site_id;
                new_order.point_magnification = old_order.point_magnification;

                if (command.disp_order_total_amount == 0)
                {
                    new_order.pay = EnumPaymentType.NON_NEEDED_PAYMENT;
                }

                if (command.member_add_id <= 0)
                {
                    new_order.member_add_id = old_order.member_add_id;
                }

                /**
                *   Below code is used to address https://192.168.100.219/issues/24548
                *   I comment out because it is affecting:
                *       https://192.168.100.219/issues/37739
                */
                //if (command.member_card_id == null || command.member_card_id == 9999)
                //{
                //    new_order.member_card_id = old_order.member_card_id;
                //}

                //更新時はuser_id変更しない
                new_order.user_id = old_order.user_id;

                if (command.member != null)
                {
                    var reason = ErsResources.GetFieldName("point_reason_purchase");
                    ErsFactory.ersOrderFactory.GetUpdateMemberPointStgy().Increase(command.member.mcode, old_order.p_service, reason, old_order.d_no, old_order.intime, (int)old_order.site_id);
                    if(!isAllCanceled)
                    {
                        ErsFactory.ersOrderFactory.GetUpdateMemberPointStgy().Decrease(command.member.mcode, new_order.p_service, reason, new_order.d_no, new_order.intime, (int)new_order.site_id);
                    }
                }

                if (command.deleted != EnumDeleted.Deleted && !command.non_member)
                {
                    //会員情報の更新
                    var old_member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(old_order.mcode, true);
                    var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
                    repository.Update(old_member, command.member);
                }

                //クーポン金額に変更があった場合
                if (command.old_coupon_discount != command.coupon_discount)
                {
                    var old_coupon_discount = new_order.coupon_discount;

                    ////伝票に反映
                    new_order.coupon_discount = command.coupon_discount.Value;
                    //再計算
                    new_order.total = new_order.total + old_coupon_discount - command.coupon_discount.Value;
                }
                new_order.coupon_code = command.ent_coupon_code;

                var listParam = new List<UpdateStockParam>();

                var orderCancelStrategy = ErsFactory.ersOrderFactory.GetOrderCancelStrategy();
                var addOrderStrategy = ErsFactory.ersOrderFactory.GetAddOrdinaryOrderStrategy();
                var isBeforeDeliverSpec = ErsFactory.ersOrderFactory.GetIsBeforeDeliverSpec();
                var calcService = ErsFactory.ersOrderFactory.GetErsCalcService();

                //数量・金額計算
                foreach (var old_record in oldOrderRecords.Values)
                {
                    var new_record = this.SelectOrderRecordWithId(old_record.id, newOrderRecords);
                    if (new_record == null)
                    {
                        throw new ErsException("30101", old_record.scode);
                    }

                    //定期IDを旧から再設定
                    new_record.regular_detail_id = old_record.regular_detail_id;

                    var old_effective_amount = old_record.GetAmount(); //過去伝票の有効個数
                    var old_amount = old_record.amount; //過去伝票のキャンセル分を考慮しない個数

                    var input_amount = new_record.amount.Value;//入力された個数

                    new_record.cancel_amount = old_record.cancel_amount; //キャンセル個数は過去分を利用
                    new_record.after_cancel_amount = old_record.after_cancel_amount; //返品個数は過去分を利用

                    new_record.amount = old_amount;
                    
                    // 在庫を減算するか否か
                    var doReflectStock = false;

                    // 加算された数
                    var increasedAmount = input_amount - old_effective_amount;

                    // 返品ステータスか否か
                    var isReturned = (new_record.order_status == EnumOrderStatusType.CANCELED_AFTER_DELIVER);

                    //refs #23910 return product must not update stock when canceled after delivery
                    var canceledAfterDeliver = ((isReturned || new_record.order_status == EnumOrderStatusType.CANCELED) && (old_record.order_status == EnumOrderStatusType.DELIVER_REQUEST || old_record.order_status == EnumOrderStatusType.DELIVER_WAITING));

                    if (isReturned)
                    {
                        //キャンセル時は、返品数 + 減算数を減らす
                        increasedAmount -= (int)new_record.after_cancel_amount;
                        new_record.after_cancel_amount = 0;
                    }

                    //数量加算
                    if (increasedAmount > 0)
                    {
                        // 加算時は常に在庫を更新
                        doReflectStock = true;

                        //明細の再計算
                        addOrderStrategy.AddOrderContact(new_record, increasedAmount);

                        calcService.calcOrder(new_order, newOrderRecords.Values, new_order.p_service, null, true);

                        var decreaseStockStgy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetDecreaseStockStgy();
                        decreaseStockStgy.Decrease(new_record.scode, increasedAmount);
                    }
                    //数量減算
                    else if (increasedAmount < 0)
                    {
                        
                        if (canceledAfterDeliver)
                        {
                            doReflectStock = false;
                        }
                        else
                        {
                            // 出荷前のステータスか否か
                            var wasBeforeDeliver = isBeforeDeliverSpec.Determine(old_record.order_status.Value, old_record.shipdate);
                            doReflectStock = wasBeforeDeliver || isReturned; // 減算時はステータスによっては在庫を更新
                        }
                        var decreasedAmount = -increasedAmount;

                        orderCancelStrategy.CancelRecord(new_record, decreasedAmount, doReflectStock);

                        calcService.calcOrder(new_order, newOrderRecords.Values, new_order.p_service, null, true);

                        // 出荷前のキャンセル/減算または返品時は、在庫を戻す。
                        if (doReflectStock)
                        {
                           var increaseStockStgy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetIncreaseStockStgy();
                           increaseStockStgy.Increase(new_record.scode, decreasedAmount);                            
                        }
                    }
                    if (!canceledAfterDeliver)
                    {
                        //注文数が変動しない場合
                        if (increasedAmount == 0)
                        {
                            if (new_record.order_status == EnumOrderStatusType.CANCELED && old_record.order_status != EnumOrderStatusType.CANCELED)
                            {

                                //在庫返却処理
                                var setmerchandise_List = ErsFactory.ersMerchandiseFactory.GetSetMerchandiseList(new_record.scode);
                                var increaseStockStgy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetIncreaseStockStgy();

                                if (setmerchandise_List.Count > 0)
                                {
                                    foreach (var list in setmerchandise_List)
                                    {   //セット商品在庫返却
                                        increaseStockStgy.Increase(list.scode, list.amount.Value * new_record.amount.Value);
                                    }
                                }
                                else
                                {
                                    //在庫返却
                                    increaseStockStgy.Increase(new_record.scode, new_record.amount.Value);
                                }
                            }

                        }
                    }

                    //在庫変動がある場合
                    if ((increasedAmount != 0) && doReflectStock)
                    {
                        var objSku = ErsFactory.ersMerchandiseFactory.GetErsSkuWithScode(old_record.scode);

                        if (objSku != null && objSku.h_mall_flg == EnumOnOff.On)
                        {
                            UpdateStockParam param = default(UpdateStockParam);

                            param.productCode = objSku.scode;
                            param.quantity = increasedAmount;
                            param.operation = EnumMallStockOperation.sub;

                            listParam.Add(param);
                        }
                    }
                }

                if (command.deleted != EnumDeleted.Deleted && command.member != null)
                {
                    //Update Member Data
                    this.UpdateMemberForBillUpdate(command.member, old_order.subtotal, new_order.subtotal, isAllCanceled);
                }

                //与信取得前に伝票データを一度UPDATE
                orderRepository.Update(old_order, new_order);
                this.UpdateOrderRecords(command, new_order.d_no, new_order.senddate, oldOrderRecords, newOrderRecords, old_order, new_order);


                // モール在庫更新 [Update stock for mall]
                var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
                if (ret.HasValue())
                {
                    throw new Exception(ErsResources.GetMessage("102100", ret));
                }

                try
                {
                    doVoidAuth = this.UpdateAuth(command, old_order, new_order, isAllCanceled, oldOrderRecords, newOrderRecords);
                }
                catch
                {
                    if (listParam.Count > 0)
                    {
                        try
                        {
                            // モール連携在庫リカバリ登録 [Register mall stock recovery]
                            ErsMallFactory.ersMallStockRecoveryFactory.GetRegisterMallStockRecoveryStgy().Register(listParam);
                        }
                        catch (Exception eTmp)
                        {
                            ErsCommon.loggingException(string.Format("Failed to register mall stock recovery.\r\n{0}", eTmp.ToString()));
                        }
                    }

                    throw;
                }

                tx.Commit();
            }

            //伝票コミット後に、与信破棄を行う(入金済みの場合は処理しない)
            if (doVoidAuth && new_order.order_payment_status == EnumOrderPaymentStatusType.NOT_PAID)
            {
                // モールは与信破棄はモールでやってもらう
                if (new_order.mall_shop_kbn == EnumMallShopKbn.ERS)
                {
                    try
                    {
                        var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(old_order.pay);
                        objPayment.SetPaymentMethod(old_order, old_order);
                        objPayment.SendVoidAuth(old_order);
                    }
                    catch (Exception eTmp)
                    {
                        ErsCommon.loggingException(string.Format("Failed to void auth of payment.\r\n{0}", eTmp.ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// 与信の更新を行う
        /// </summary>
        /// <param name="command"></param>
        /// <param name="old_order"></param>
        /// <param name="new_order"></param>
        /// <param name="isAllCanceled"></param>
        /// <returns></returns>
        private bool UpdateAuth(IOrderCommand command, ErsOrder old_order, ErsOrder new_order, bool isAllCanceled, IDictionary<string, ErsOrderRecord> oldOrderRecords, IDictionary<string, ErsOrderRecord> newOrderRecords)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var orderRepository = ErsFactory.ersOrderFactory.GetErsOrderRepository();

            bool doVoidAuth = false;
            if (new_order.mall_shop_kbn == EnumMallShopKbn.ERS)
            {
                //ALLキャンセルまたは決済方法が変更された場合は、与信破棄
                if (isAllCanceled)
                {
                    //与信破棄はコミット後に行う
                    doVoidAuth = true;
                }
                else if (new_order.pay != old_order.pay)
                {
                    //カード以外からカード払いになった場合は与信取得
                    this.ExecutePayment(new_order, command);

                    //与信破棄はコミット後に行う
                    doVoidAuth = true;
                }
                else
                {
                    //入金ステータスが与信未取得の伝票（継続課金伝票）の修正が行われたときに与信を取得
                    if (ErsFactory.ersOrderFactory.GetIsContinualBillingOrderSpec().Satisfy(old_order))
                    {
                        // ただし、配送済みの場合は、与信の取得は行わない（継続課金を依頼しているため）
                        var newOrderStatus = ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy().ObtainFrom(newOrderRecords.Values);
                        if (old_order.sent_continual_billing != EnumSentContinualBillingFlg.Sent)
                        {
                            // キャンセルの場合は与信を取得しない
                            // 30000万円以内の場合は与信を取得しない
                            if (!ErsOrderCriteria.CANCEL_STATUS_ARRAY.Contains(newOrderStatus.Value)
                                && setup.gmo_compensable_total < new_order.total)
                            {
                                this.ExecutePayment(new_order, command);
                            }
                        }
                        else
                        {
                            //配送済み伝票の金額が変更になる場合は、完了画面に以下のメッセージを表示する
                            if (old_order.total != new_order.total)
                            {
                                command.controller.AddInformation(ErsResources.GetMessage("53305"));
                            }
                        }
                    }
                    //カードシーケンスの変更時に与信破棄＋再与信取得
                    else if (new_order.pay == EnumPaymentType.CREDIT_CARD && new_order.member_card_id != old_order.member_card_id
                        && new_order.member_card_id.HasValue)
                    {
                        this.ExecutePayment(new_order, command);

                        //与信破棄はコミット後に行う
                        doVoidAuth = true;
                    }
                    else if (old_order.total != new_order.total)
                    {
                        if (new_order.pay == EnumPaymentType.CONVENIENCE_STORE)
                        {
                            this.ExecutePayment(new_order, command);
                        }
                        else
                        {
                            //請求額増加は再与信
                            var objPayment = ErsFactory.ersOrderFactory.GetErsPayment(new_order.pay);

                            objPayment.SetPaymentMethod(new_order, old_order);
                            /// 請求額変更
                            objPayment.SendAlterAuth(new_order, new_order.total);
                        }

                        command.reauthed = true;
                    }
                }

                orderRepository.Update(old_order, new_order);
            }

            return doVoidAuth;
        }

        /// <summary>
        /// 会員の購入情報を更新する。
        /// </summary>
        /// <param name="mcode"></param>
        /// <param name="total"></param>
        /// <param name="intime"></param>
        private void UpdateMember(ErsMember member, int subtotal)
        {
            var oldMember = ErsFactory.ersMemberFactory.getErsMemberWithParameter(member.GetPropertiesAsDictionary());

            //sale値を加算の為追加
            ErsFactory.ersOrderFactory.GetUpdateMemberDataStgy().Update(member, subtotal, 1, DateTime.Now);

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            repository.Update(oldMember, member);
        }

        /// <summary>
        /// 会員の購入情報を更新する。(伝票更新時)
        /// </summary>
        /// <param name="mcode"></param>
        /// <param name="total"></param>
        /// <param name="intime"></param>
        private void UpdateMemberForBillUpdate(ErsMember member, int old_subtotal, int new_subtotal, bool isAllCanceled)
        {
            //会員購入情報の修正
            if (isAllCanceled)
            {
                ErsFactory.ersOrderFactory.GetUpdateMemberDataStgy().Update(member, -old_subtotal, -1);
            }
            else
            {
                ErsFactory.ersOrderFactory.GetUpdateMemberDataStgy().Update(member, -old_subtotal, 0);
                ErsFactory.ersOrderFactory.GetUpdateMemberDataStgy().Update(member, new_subtotal, 0);
            }

            var oldMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(member.mcode, true);

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            repository.Update(oldMember, member);
        }

        /// <summary>
        /// 新しい明細リストから、指定されたIDの明細を取得します。
        /// </summary>
        /// <param name="old_id"></param>
        /// <param name="newOrderRecords"></param>
        /// <returns></returns>
        private ErsOrderRecord SelectOrderRecordWithId(int? id, IDictionary<string, ErsOrderRecord> orderRecords)
        {
            var listResult = orderRecords.Values.Where((orderrecord) => orderrecord.id == id);
            if (listResult.Count() != 1)
            {
                return null;
            }
            return listResult.First();
        }

        /// <summary>
        /// アップデート（旧伝票マスタ使用版）
        /// </summary>
        /// <param name="old_order">変更前伝票</param>
        /// <param name="new_order">変更後伝票</param>
        public void UpdateOrderRecords(IOrderCommand command, string d_no, DateTime? senddate, IDictionary<string, ErsOrderRecord> oldOrderRecords,
            IDictionary<string, ErsOrderRecord> newOrderRecords, ErsOrder old_order, ErsOrder new_order)
        {
            var old_records = oldOrderRecords.Select(e => e.Value);
            var new_records = newOrderRecords.Select(e => e.Value);

            string subd_no = null;
            //明細を更新
            var dsRepository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();
            var orderMailRepository = ErsFactory.ersOrderFactory.GetErsOrderMailRepository();
            foreach (var new_record in newOrderRecords.Values)
            {
                var old_record = SelectOrderRecordWithId(new_record.id, oldOrderRecords);

                if (old_record != null)
                {
                    //配送番号のセット
                    var arrNotShipped = new[] { EnumOrderStatusType.NEW_ORDER, EnumOrderStatusType.CANCELED, EnumOrderStatusType.DELIVER_WAITING };
                    if (arrNotShipped.Contains(old_record.order_status.Value)
                        && !arrNotShipped.Contains(new_record.order_status.Value))
                    {
                        if (!subd_no.HasValue())
                        {
                            subd_no = ErsFactory.ersOrderFactory.GetObtainNewSubd_noStgy().Obtain(newOrderRecords.Values);
                        }

                        new_record.subd_no = subd_no;
                    }

                    new_record.deliv_method = command.deliv_method;
                    new_record.intime = old_record.intime;
                    new_record.order_type = old_record.order_type;//keep order_type value.
                    new_record.plural_order_type = old_record.plural_order_type;
                    
                    dsRepository.Update(old_record, new_record);

                    //Increase or Decrease the Warehouse Stock
                    this.WareHouseStockMonitor(new_record, old_record.order_status);

                    if (old_record.order_status != new_record.order_status)
                    {
                        // 受注ステータス更新履歴登録 [Regist order status history]
                        ErsFactory.ersOrderFactory.GetRegistOrderRecordStatusHistoryStgy().
                            Regist(old_record, new_record, old_order, old_records, new_order, new_records);
                    }

                    if (old_record.amount != new_record.amount || old_record.cancel_amount + old_record.after_cancel_amount != new_record.cancel_amount + new_record.after_cancel_amount)
                    {
                        //ds_set_tの更新
                        int ds_set_amount = new_record.GetAmount();

                        ErsFactory.ersOrderFactory.GetRegistSetItemsStgy().UpdateAmount(old_record, ds_set_amount);
                    }
                }
                else
                {
                    new_record.d_no = d_no;
                    dsRepository.Insert(new_record, true);

                    //Increase or Decrease the Warehouse Stock
                    this.WareHouseStockMonitor(new_record);

                    var newOrderMail = this.GetOrderMail(new_record);
                    orderMailRepository.Insert(newOrderMail);

                    //セット商品の追加
                    ErsFactory.ersOrderFactory.GetRegistSetItemsStgy().UpRegist(new_record);
                }
            }
        }

        /// <summary>
        /// Update Warehouse Stock Monitor
        /// </summary>
        /// <param name="orderRecord"></param>
        /// <param name="oldOrderStatus"></param>
        protected void WareHouseStockMonitor(ErsOrderRecord orderRecord, EnumOrderStatusType? oldOrderStatus = null)
        {
            var decreaseWhStockStgy = ErsFactory.ersWarehouseFactory.GetDecreaseWhStockStgy();
            var increaseWhStockStgy = ErsFactory.ersWarehouseFactory.GetIncreaseWhStockStgy();

            if (!oldOrderStatus.HasValue)
            {
                if (orderRecord.order_status == EnumOrderStatusType.DELIVERED)
                {
                    decreaseWhStockStgy.Decrease(orderRecord.scode, orderRecord.GetAmount(), null);
                    decreaseWhStockStgy.Decrease(orderRecord.scode, orderRecord.GetAmount(), EnumShelfNumber.SHELF001);
                }
            }
            else
            {
                if (orderRecord.order_status == EnumOrderStatusType.DELIVERED && oldOrderStatus != EnumOrderStatusType.DELIVERED)
                {
                    decreaseWhStockStgy.Decrease(orderRecord.scode, orderRecord.GetAmount(), null);
                    decreaseWhStockStgy.Decrease(orderRecord.scode, orderRecord.GetAmount(), EnumShelfNumber.SHELF001);
                }

                if (orderRecord.order_status != EnumOrderStatusType.DELIVERED && oldOrderStatus == EnumOrderStatusType.DELIVERED)
                {
                    increaseWhStockStgy.Increase(orderRecord.scode, orderRecord.GetAmount(), null);
                    increaseWhStockStgy.Increase(orderRecord.scode, orderRecord.GetAmount(), EnumShelfNumber.SHELF001);
                }

            }
        }
    }
}
