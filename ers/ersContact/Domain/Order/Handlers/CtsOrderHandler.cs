﻿using System;
using ersContact.Domain.Order.Commands;
using jp.co.ivp.ers.ctsorder;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using System.Collections.Generic;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mall.api.stock;
using jp.co.ivp.ers.mall;

namespace ersContact.Domain.Order.Handlers
{
    public class CtsOrderHandler : ICommandHandler<ICtsOrderCommand>
    {
        public ICommandResult Submit(ICtsOrderCommand command)
        {
            SaveCtsOrder(command);

            return new CommandResult(true);
        }

        private void SaveCtsOrder(ICtsOrderCommand command)
        {
            this.RefreshBasket(command);

            if (command.cts_order_id == 0 || command.cts_order_id == null)
            {
                this.InsertCtsOrder(command);
                command.cts_order_success = true;
                command.load_new_order = true;
            }
            else
            {
                this.UpdateCtsOrder(command);
                command.cts_order_success = true;
                command.load_new_order = false;
            }
        }

        private void RefreshBasket(ICtsOrderCommand command)
        {
            command.basket = ErsFactory.ersBasketFactory.GetErsBasket();
            command.basket.IsOrderUpdate = command.IsOrderUpdate;
            command.basket.PrepareSession();
            command.basket.Init(command.mcode, command.cts_order_ransu);
        }

        private void InsertCtsOrder(ICtsOrderCommand command)
        {
            var repository = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderRepository();
            ErsCtsOrder order = ErsFactory.ersCtsOrderFactory.GetErsCtsOrder();

            order.OverwriteWithModel(command);

            order.ransu = command.cts_order_ransu;
            order.mcode = command.mcode;
            order.lname = command.lname;
            order.fname = command.fname;
            order.lnamek = command.lnamek;
            order.fnamek = command.fnamek;
            order.memo = command.cts_order_memo;
            order.charge1 = command.cts_order_user_id;
            //TODO: マジックナンバーを解消
            order.status = 2;
            order.user_id = command.cts_order_user_id;
            order.tel = command.tel;
            order.subtotal = command.basket.subtotal; // GetBasketSubTotal();
            order.charge1_ag_type = command.cts_order_charge1_ag_type;
            order.charge1_ag_name = command.cts_order_charge1_ag_name;
            order.pm_flg = command.pm_flg.Value;
            order.active = EnumActive.Active;
            order.addressbook_id = command.shipping_id;
            order.site_id = command.site_id;
            repository.Insert(order, true);

            var basketRepository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            foreach (var item in command.basket.objBasketRecord)
            {
                var oldBaskRecord = ErsFactory.ersBasketFactory.GetErsBaskRecordWithId(item.Value.id);
                var newBaskRecord = ErsFactory.ersBasketFactory.GetErsBaskRecordWithId(item.Value.id);

                newBaskRecord.old_amount = item.Value.amount;
                newBaskRecord.stock_reservation = EnumStockReservation.Reserve;

                basketRepository.Update(oldBaskRecord, newBaskRecord);
            }

            foreach (var item in command.basket.objRegularBasketRecord)
            {
                var oldBaskRecord = ErsFactory.ersBasketFactory.GetErsBaskRecordWithId(item.Value.id);
                var newBaskRecord = ErsFactory.ersBasketFactory.GetErsBaskRecordWithId(item.Value.id);

                newBaskRecord.old_amount = item.Value.amount;
                newBaskRecord.stock_reservation = EnumStockReservation.Reserve;

                basketRepository.Update(oldBaskRecord, newBaskRecord);
            }

            this.UpdateStock(command);

            command.cts_order_temp_d_no = order.temp_d_no;
        }

        private void UpdateCtsOrder(ICtsOrderCommand command)
        {
            var repository = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderRepository();

            ErsCtsOrder order = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderWithTempDNo(Convert.ToInt32(command.cts_order_temp_d_no),command.site_id);
            var tme = order.intime;

            order.OverwriteWithModel(command);
            order.ransu = command.basket.ransu;
            order.intime = tme;
            order.memo = command.cts_order_memo;
            order.charge2 = command.cts_order_user_id;
            order.status = command.cts_order_status;
            order.tel = command.tel;
            order.subtotal = command.basket.subtotal; // GetBasketSubTotal();
            order.addressbook_id = command.shipping_id;
            
            var old_order = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderWithTempDNo(Convert.ToInt32(command.cts_order_temp_d_no));
            order.utime = DateTime.Now;
            repository.Update(old_order, order);

            // Update Old Amount
            var basketRepository = ErsFactory.ersBasketFactory.GetErsBaskRecordRepository();
            var listParam = new List<UpdateStockParam>();
            foreach (var item in command.basket.objBasketRecord)
            {
                var oldBaskRecord = ErsFactory.ersBasketFactory.GetErsBaskRecordWithId(item.Value.id);
                var newBaskRecord = ErsFactory.ersBasketFactory.GetErsBaskRecordWithId(item.Value.id);

                newBaskRecord.old_amount = item.Value.amount;
                newBaskRecord.stock_reservation = EnumStockReservation.Reserve;

                var param = this.UpdateStockByReservedStock(oldBaskRecord.stock_reservation, newBaskRecord.scode, oldBaskRecord.old_amount.Value, item.Value.amount);

                basketRepository.Update(oldBaskRecord, newBaskRecord);

                if (param.HasValue &&
                    (newBaskRecord.h_mall_flg == EnumOnOff.On))
                {
                    listParam.Add(param.Value);
                }
            }

            //Update Old Amount Regular
            foreach (var item in command.basket.objRegularBasketRecord)
            {
                var oldBaskRecord = ErsFactory.ersBasketFactory.GetErsBaskRecordWithId(item.Value.id);
                var newBaskRecord = ErsFactory.ersBasketFactory.GetErsBaskRecordWithId(item.Value.id);

                newBaskRecord.old_amount = item.Value.amount;
                newBaskRecord.stock_reservation = EnumStockReservation.Reserve;

                var param = this.UpdateStockByReservedStock(oldBaskRecord.stock_reservation, newBaskRecord.scode, oldBaskRecord.old_amount.Value, item.Value.amount);

                basketRepository.Update(oldBaskRecord, newBaskRecord);

                if (param.HasValue &&
                    (newBaskRecord.h_mall_flg == EnumOnOff.On))
                {
                    listParam.Add(param.Value);
                }
            }

            // モール在庫更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }
        }

        private void UpdateStock(ICtsOrderCommand command)
        {
            var decreaseStockStgy = ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetDecreaseStockStgy();

            foreach (var baskRecord in command.orderRecords)
            {
                decreaseStockStgy.Decrease(baskRecord.scode, baskRecord.amount);
            }

            this.UpdateMallStock(command.orderRecords);
        }

        private UpdateStockParam? UpdateStockByReservedStock(EnumStockReservation? stock_reservation, string scode, int old_amount, int amount)
        {
            UpdateStockParam? retParam = null;

            if (stock_reservation == EnumStockReservation.NonReserve)
            {
                ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetDecreaseStockStgy().Decrease(scode, amount);

                var param = default(UpdateStockParam);
                param.productCode = scode;
                param.quantity = amount;
                param.operation = EnumMallStockOperation.sub;
                retParam = param;
            }
            else if ((old_amount - amount) != 0)
            {
                if (amount > old_amount)
                {
                    ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetDecreaseStockStgy().Decrease(scode, amount - old_amount);
                }
                else if (amount < old_amount)
                {
                    ErsFactory.ersMerchandiseFactory.GetErsStockFactory().GetIncreaseStockStgy().Increase(scode, old_amount - amount);
                }

                var param = default(UpdateStockParam);
                param.productCode = scode;
                param.quantity = amount - old_amount;
                param.operation = EnumMallStockOperation.sub;
                retParam = param;
            }

            return retParam;
        }

        /// <summary>
        /// モール在庫更新 [Update mall stock]
        /// </summary>
        /// <param name="command">コマンド [Command]</param>
        protected void UpdateMallStock(IEnumerable<ErsBaskRecord> orderRecords)
        {
            var listParam = new List<UpdateStockParam>();

            foreach (var record in orderRecords)
            {
                if (record.h_mall_flg == EnumOnOff.On)
                {
                    UpdateStockParam param = default(UpdateStockParam);

                    param.productCode = record.scode;
                    param.quantity = record.amount;
                    param.operation = EnumMallStockOperation.sub;

                    listParam.Add(param);
                }
            }

            // モール在庫更新 [Update stock for mall]
            var ret = ErsMallFactory.ersMallStockFactory.GetUpdateMallStockStgy().UpdateMallStock(listParam);
            if (ret.HasValue())
            {
                throw new Exception(ErsResources.GetMessage("102100", ret));
            }
        }
    }
}
