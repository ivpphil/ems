﻿using ersContact.Domain.Order.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.Payment;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.db;
using System;
using jp.co.ivp.ers.order.strategy;

namespace ersContact.Domain.Order.Handlers
{
    public class PaymentHandler : ICommandHandler<IPaymentCommand>
    {
        IErsPaymentCreditBase ersGmo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD));
        PutRegularDateForwardStgy putRegularDateForwardStgy = ErsFactory.ersOrderFactory.GetPutRegularDateForwardStgy();

        public ICommandResult Submit(IPaymentCommand command)
        {
            if (command.card_delete)
            {
                this.DeleteCard(command);
            }
            if (command.card_update)
            {
                this.ModifyCard(command);
            }
            return new CommandResult(true);
        }

        /// <summary>
        /// カード削除処理
        /// </summary>
        private void DeleteCard(IPaymentCommand command)
        {
            var member = ErsFactory.ersMemberFactory.GetErsMember();
            var ersGmo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD));

            var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
            var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();

            member.mcode = command.mcode;
            member.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();

            //GMO側保持カードの削除　今回DBに登録する情報取得
            var newErsMemberCard = ersGmo.deleteCardInfo(this.GetFindData(command.mcode, command.card_id), member);

            //削除対象
            memberCardCriteria.id = command.card_id;
            memberCardCriteria.mcode = command.mcode;
            memberCardCriteria.active = EnumActive.Active;
            
            var oldErsMemberCard = memberCardRepository.Find(memberCardCriteria)[0];
            newErsMemberCard.id = oldErsMemberCard.id;

            //IVP側カード処理
            memberCardRepository.Update(oldErsMemberCard, newErsMemberCard);
        }

        /// <summary>
        /// 確認画面用個別データ取得
        /// </summary>
        public CreditCardInfo GetFindData(string mcode, int? card_id)
        {
            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);

            var ersGmo = ((IErsPaymentCreditBase)ErsFactory.ersOrderFactory.GetErsPayment(EnumPaymentType.CREDIT_CARD));

            //member.mcode = mcode;

            var dic = ersGmo.ObtainMemberCardInfo(member, card_id);
            if (dic == null)
            {
                //対象データ無し
                throw new ErsException("10200");
            }
            return dic;
        }

        private void ModifyCard(IPaymentCommand command)
        {
            var ersMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode);
            ErsMemberCard newErsMemberCard;
            using (var tx = ErsDB_parent.BeginTransaction())
            {
                //新規カード預けを行う

                CreditCardInfo updateCardInfo = new CreditCardInfo(
                                ersMember.mcode, null, null, null, command.card_holder_name, command.card_type, command.cardno, command.validity_y, command.validity_m, null);

                var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
                var memberCardCriteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
                memberCardCriteria.id = command.card_id;
                var oldMemberCard = memberCardRepository.FindSingle(memberCardCriteria);

                CreditCardInfo creditCardInfo = new CreditCardInfo(
                    ersMember.mcode, oldMemberCard.card_mcode, oldMemberCard.id, null, updateCardInfo.card_holder_name, updateCardInfo.card_type, updateCardInfo.card_no, updateCardInfo.validity_y, updateCardInfo.validity_m, null);

                newErsMemberCard = this.InsertCardData(ersMember, creditCardInfo);

                //定期伝票更新
                this.UpdateRegularOrderCardSequence(command.card_id, newErsMemberCard);

                tx.Commit();
            }

            //預けたカードのIDが削除対象のIDと同一の場合は、削除はしない
            if (command.card_id == newErsMemberCard.id)
            {
                return;
            }

            try
            {
                this.DeleteCard(command);
            }
            catch (Exception e)
            {
                //カード削除はエラー表示させない。
                //ログに落とす。
                ErsCommon.loggingException(e.ToString());

                if (ErsFactory.ersUtilityFactory.getSetup().debug)
                    throw;
            }
        }


        /// <summary>
        /// 定期明細で使用しているカードを削除した場合に、カードシーケンスを新しく登録したカード番号で更新する。
        /// </summary>
        /// <param name="command"></param>
        /// <param name="memberCard"></param>
        private void UpdateRegularOrderCardSequence(int? card_id, ErsMemberCard memberCard)
        {
            var setup = ErsFactory.ersBatchFactory.getSetup();

            var regularOrderRepository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            var regularOrderCriteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
            regularOrderCriteria.member_card_id = card_id;

            var listOrderRecord = regularOrderRepository.Find(regularOrderCriteria);
            foreach (var record in listOrderRecord)
            {
                var oldRecord = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordWithId(record.id);
                record.member_card_id = memberCard.id;

                // put next day forward if today past the :next_date + create_regular_order_days.
                while (true)
                {
                    if ((record.next_date.Value.AddDays(-setup.create_regular_order_days)) > DateTime.Now.Date)
                    {
                        break;
                    }

                    putRegularDateForwardStgy.Execute(record, false);
                }
                
                regularOrderRepository.Update(oldRecord, record);
            }
        }

        /// <summary>
        /// カード預けとDBInsertをおこなう
        /// </summary>
        /// <param name="ersMember"></param>
        /// <param name="creditCardInfo"></param>
        /// <returns></returns>
        private ErsMemberCard InsertCardData(ErsMember ersMember, CreditCardInfo creditCardInfo)
        {
            var memberCardRepository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();

            //カード預け
            var newErsMemberCard = ersGmo.SaveMemberCardInfo(creditCardInfo, ersMember);

            var oldErsMemberCard = ErsFactory.ersMemberFactory.GetRetrieveAlreadyStoredMemberCardStgy().Retrieve(newErsMemberCard.card_mcode, newErsMemberCard.card_sequence);
            if (oldErsMemberCard != null)
            {
                newErsMemberCard = ErsFactory.ersMemberFactory.GetErsMemberCard();
                newErsMemberCard.OverwriteWithParameter(oldErsMemberCard.GetPropertiesAsDictionary());
                newErsMemberCard.update_status = EnumCardUpdateStatus.Success;

                memberCardRepository.Update(oldErsMemberCard, newErsMemberCard);

                return oldErsMemberCard;
            }

            //IVP側カード処理
            memberCardRepository.Insert(newErsMemberCard, true);

            return newErsMemberCard;
        }
    }
}
