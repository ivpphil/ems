﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Information.Commands
{
    public interface IInfoReadInsertCommand : ICommand
    {
        int id { get; }
        int? agent_id { get; }
    }

}