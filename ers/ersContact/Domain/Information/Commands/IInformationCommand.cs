﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Information.Commands
{
    public interface IInformationCommand : ICommand
    {
        int? id { get; set; }
        string title { get; set; }
        string contents { get; set; }
        bool regist { get; set; }
        bool modify { get; set; }
        bool delete { get; set; }
        bool done { get; set; }
    }
}