﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Information.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Information.Handlers
{
    public class ValidateInformation
           : IValidationHandler<IInformationCommand>
    {
        public IEnumerable<ValidationResult> Validate(IInformationCommand command)
        {
            if ((command.modify && command.done) || command.delete)
            {
                yield return command.CheckRequired("id");
            }
            if ((command.regist || command.modify) && command.done)
            {
                yield return command.CheckRequired("title");
                yield return command.CheckRequired("contents");
            }
        }
    }
}