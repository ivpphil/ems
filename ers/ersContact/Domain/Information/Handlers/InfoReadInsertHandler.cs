﻿
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.information;
using System;
using ersContact.Domain.Information.Commands;
using jp.co.ivp.ers;

namespace ersContact.Domain.Information.Handlers
{

    //インフォメーション既読処理
    public class InfoReadInsertHandler : ICommandHandler<IInfoReadInsertCommand>
    {
        public ICommandResult Submit(IInfoReadInsertCommand command)
        {
            if (!this.confirmExist(command))
            {

                var rep = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationReadRepository();

                var objRead = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationRead();
                objRead.information_id = command.id;
                objRead.user_id = command.agent_id;
                objRead.intime  = DateTime.Now;
                objRead.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
                rep.Insert(objRead, true);

            }

            return new CommandResult(true);
        }

        private bool confirmExist(IInfoReadInsertCommand command)
        {
            if (command.id > 0)
            {

                var criteria = GetconfirmExistCriteria(command);
                var infoRead = ErsFactory.ersCtsInformationFactory.GetCtsInformationSearchSpecification();
                var list = infoRead.GetCountReadInfoData(criteria);
                if (list.Count == 0)
                {
                    return false;
                }
                else
                {
                    return Convert.ToInt32(list[0]["count"]) > 0;
                }
            }
            return false;
        }

        private ErsCtsInformationCriteria GetconfirmExistCriteria(IInfoReadInsertCommand command)
        {

            var criteria = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationCriteria();
            criteria.read_Info_id = command.id;
            criteria.read_user_id = command.agent_id;
            criteria.site_id_of_read = ErsFactory.ersUtilityFactory.getSetup().site_id;
            return criteria;

        }
    }

}
