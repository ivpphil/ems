﻿using ersContact.Domain.Information.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersContact.Domain.Information.Handlers
{
    public class InformationHandler : ICommandHandler<IInformationCommand>
    {
        public ICommandResult Submit(IInformationCommand command)
        {
            var repository = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationRepository();

            if (command.regist && command.done)
            {
                var information = ErsFactory.ersCtsInformationFactory.GetErsCtsInformation();
                information.OverwriteWithModel(command);

                information.active = EnumActive.Active;
                information.utime = DateTime.Now;
                information.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
                repository.Insert(information);

                return new CommandResult(true);
            }
            else if (command.modify && command.done)
            {
                var information = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationWithId(command.id.Value);
                information.OverwriteWithModel(command);

                var old_information = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationWithId(command.id.Value);

                repository.Update(old_information, information);

                var readRepository = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationReadRepository();
                var readCriteria = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationReadCriteria();
                readCriteria.information_id = information.id;
                readRepository.Delete(readCriteria);

                return new CommandResult(true);
            }
            else
            {
                var information = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationWithId(command.id.Value);
                command.OverwriteWithParameter(information.GetPropertiesAsDictionary());
                information.active = (int)EnumActive.NonActive;
                information.OverwriteWithModel(command);

                var old_Information = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationWithId(command.id.Value);

                repository.Update(old_Information, information);

                var readRepository = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationReadRepository();
                var readCriteria = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationReadCriteria();
                readCriteria.information_id = information.id;
                readRepository.Delete(readCriteria);

                command.done = true;
                
                return new CommandResult(true);
            }
        }
    }
}
