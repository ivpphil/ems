﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Information.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.information;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.direction;

namespace ersContact.Domain.Information.Mappers
{
    //トップページ：Informationリスト取得
    public class UnReadInformationMapper
        : IMapper<IUnReadInformationMappable>
    {

        public void Map(IUnReadInformationMappable objMappable)
        {
            objMappable.calendarMonth = objMappable.calendarMonth.AddMonths(-1);
            objMappable.unreadinfo = loadUnreadInfo(objMappable);
            objMappable.calendarMonth = objMappable.calendarMonth.AddMonths(+ 1);
        }

        public int loadUnreadInfo(IUnReadInformationMappable objMappable)
        {
            var informationSerchSpec = ErsFactory.ersCtsInformationFactory.GetCtsInformationSearchSpecification();

            var criteria = GetloadUnreadInfoCriteria(objMappable);
            return informationSerchSpec.GetUnreadCountData(criteria, objMappable.agent_id.Value);
        }

        private ErsCtsInformationCriteria GetloadUnreadInfoCriteria(IUnReadInformationMappable objMappable)
        {
            DateTime curDate = new DateTime(objMappable.calendarMonth.Year, objMappable.calendarMonth.Month, 1);
            DateTime nxtMon = new DateTime(curDate.AddMonths(1).Year, curDate.AddMonths(1).Month, System.DateTime.DaysInMonth(curDate.AddMonths(1).Year, curDate.AddMonths(1).Month));
            var criteria = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationCriteria();

            criteria.active = EnumActive.Active;
            criteria.read_ag_type = ErsFactory.ersCtsOperatorFactory.GetObtainAgentTypeStgy().getAgeType(objMappable.agent_id.Value);
            criteria.readid = 0;
            criteria.intimeFrom = curDate;
            criteria.intimeTo = nxtMon;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            return criteria;
        }
    }
}