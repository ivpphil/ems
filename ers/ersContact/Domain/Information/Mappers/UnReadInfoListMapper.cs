﻿using System.Collections.Generic;
using System.Linq;
using ersContact.Domain.Information.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Information.Mappers
{
    public class UnReadInfoListMapper
        : IMapper<IUnReadInfoListMappable>
    {
        public void Map(IUnReadInfoListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationRepository();
            var criteria = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationCriteria();
            criteria.site_id_tb = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.AddOrderBy("tb.user_id", Criteria.OrderBy.ORDER_BY_ASC);

            //var list = repository.FindUnread(criteria, objMappable.id.Value, ErsFactory.ersCtsOperatorFactory.GetObtainAgentTypeStgy().getAgeType(objMappable.agent_id.Value));
            var list = repository.FindUnread(criteria, objMappable.id.Value);

            objMappable.readList = ErsCommon.ConvertEntityListToDictionaryList(list.Where(data => data.intime.HasValue));
            objMappable.unreadList = ErsCommon.ConvertEntityListToDictionaryList(list.Where(data => !data.intime.HasValue));
        }
    }
}