﻿using System.Collections.Generic;
using ersContact.Domain.Information.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.information;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Information.Mappers
{
    public class ModifyInfoListMapper
        : IMapper<IModifyInfoListMappable>
    {
        public void Map(IModifyInfoListMappable objMappable)
        {
            objMappable.informationList = this.LoadModifyInfoList(objMappable);
        }

        public List<Dictionary<string, object>> LoadModifyInfoList(IModifyInfoListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationRepository();
            var criteria = GetCriteria(objMappable);

            objMappable.recordCount = repository.GetRecordCount(criteria);
            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;
            }

            objMappable.pagerPageCount = pagerPageCount;

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_DESC);
            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            var list = repository.Find(criteria);

            return ErsCommon.ConvertEntityListToDictionaryList(list);
        }

        private ErsCtsInformationCriteria GetCriteria(IModifyInfoListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationCriteria();

            if (objMappable.id == null)
            {
                criteria.exclude_id = 0;
            }
            else
            {
                criteria.exclude_id = objMappable.id;
            }
            criteria.active = EnumActive.Active;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            return criteria;
        }
    }
}