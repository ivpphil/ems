﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Information.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.information;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Information.Mappers
{
    //トップページ：Informationリスト取得
    public class InformationListMapper
        : IMapper<IInformationListMappable>
    {

        public void Map(IInformationListMappable objMappable)
        {

            objMappable.ErsCtsInformationList = this.GetInformationList(objMappable,1);
            objMappable.informationList = ErsCommon.ConvertEntityListToDictionaryList(this.GetInformationList(objMappable));

        }

        private IList<ErsCtsInformation> GetInformationList(IInformationListMappable objMappable,int? all=null)
        {
            var criteria = GetInformationCriteria(objMappable);


            objMappable.recordCount = this.GetRecordCount(criteria);

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;
            }
            objMappable.pagerPageCount = pagerPageCount;

            criteria.SetOrderByUTime(Criteria.OrderBy.ORDER_BY_DESC);
          
           
            return this.GetRecordList(criteria, objMappable);
        }
        private IList<ErsCtsInformation> GetInformationList(IInformationListMappable objMappable)
        {
            var criteria = GetInformationCriteria(objMappable);

  
            if (!String.IsNullOrEmpty(objMappable.utime))
            {
                DateTime dtUtime = Convert.ToDateTime(objMappable.utime);
                criteria.utimeFrom = dtUtime;
                criteria.utimeTo = dtUtime.AddDays(1).AddMilliseconds(-1);
            }
 

            objMappable.recordCount = this.GetRecordCount(criteria);

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;
            }
            objMappable.pagerPageCount = pagerPageCount;

            criteria.SetOrderByUTime(Criteria.OrderBy.ORDER_BY_DESC);
            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            return this.GetRecordList(criteria, objMappable);
        }


        private int GetRecordCount(Criteria criteria)
        {
            var informationSerchSpec = ErsFactory.ersCtsInformationFactory.GetCtsInformationSearchSpecification();
            return informationSerchSpec.GetCountData(criteria);
        }

        private IList<ErsCtsInformation> GetRecordList(Criteria criteria, IInformationListMappable objMappable)
        {
            var informationSerchSpec = ErsFactory.ersCtsInformationFactory.GetCtsInformationSearchSpecification();
            var retList = new List<ErsCtsInformation>();

            var list = informationSerchSpec.GetInfoData(criteria, objMappable.agent_id.Value);

            foreach (var dr in list)
            {
                var Information = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationWithParameters(dr);
                retList.Add(Information);
            }

            return retList;
        }

        private ErsCtsInformationCriteria GetInformationCriteria(IInformationListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationCriteria();

            criteria.ag_type = ErsFactory.ersCtsOperatorFactory.GetObtainAgentTypeStgy().getAgeType((int)objMappable.agent_id);
            criteria.active = EnumActive.Active;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            return criteria;
        }

    }
}