﻿using ersContact.Domain.Information.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.direction;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersContact.Domain.Information.Mappers
{
    public class CheckInstructionListMapper
        : IMapper<ICheckInstructionListMappable>
    {
        public void Map(ICheckInstructionListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionsManagementRepository();
            var criteria = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionsManagementCriteria();
            criteria.to_user_id = objMappable.user_id;
            criteria.check_s = 0;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.enq_progress = EnumEnqProgress.Open;
            criteria.AddOrderByIntime(Criteria.OrderBy.ORDER_BY_DESC);


            objMappable.listInstruction = repository.FindInstruction(criteria);
        }
    }
}