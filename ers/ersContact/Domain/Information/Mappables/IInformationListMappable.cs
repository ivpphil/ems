﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.information;

namespace ersContact.Domain.Information.Mappables
{
    public interface IInformationListMappable
        : IMappable
    {
        List<Dictionary<string, object>> informationList { get; set; }
        IList<ErsCtsInformation> ErsCtsInformationList { get; set; }
        ErsPagerModel pager { get; }
        int? agent_id { get; set; }
        string utime { get; set; }
        long recordCount { get; set; }


        long maxItemCount { get; }

        long pagerPageCount { get; set; }
    }
}
