﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersContact.Domain.Information.Mappables
{
    public interface IModifyInfoListMappable
        : IMappable
    {
        int? id { get; set; }
        long recordCount { get; set; }
        ErsPagerModel pager { get; }
        List<Dictionary<string, object>> informationList { get; set; }

        long maxItemCount { get; }

        long pagerPageCount { get; set; }
    }
}
