﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.information;

namespace ersContact.Domain.Information.Mappables
{
    public interface IUnReadInformationMappable
        : IMappable
    {
        int unreadinfo { get; set; }
        int? agent_id { get; set; }
        DateTime calendarMonth { get; set; }

    }
}
