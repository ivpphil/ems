﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Information.Mappables
{
    public interface IUnReadInfoListMappable
        : IMappable
    {
        int? id { get; set; }
        int? agent_id { get; set; }
        List<Dictionary<string, object>> readList { get; set; }
        List<Dictionary<string, object>> unreadList { get; set; }
    }
}
