﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using System;
using jp.co.ivp.ers;
using jp.co.ivp.ers.direction;
using System.Collections.Generic;

namespace ersContact.Domain.Information.Mappables
{
    public interface ICheckInstructionListMappable
        : IMappable
    {
        IList<ErsCtsDirection> listInstruction { get; set; }
        int? user_id { get; set; }
    }
}
