﻿using ersContact.Domain.Merge.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersContact.Domain.Merge.Mappers
{
    public class MergeMapper
        : IMapper<IMergeMappable>
    {
        public void Map(IMergeMappable objMappable)
        {
            if (objMappable.next_member)
            {
                this.LoadMember(objMappable);
            }
            if (objMappable.next_case)
            {
                this.LoadCase(objMappable);
            }
            if (objMappable.return_member || objMappable.return_case)
            {
                objMappable.page = 1;
            }
        }

        private void LoadMember(IMergeMappable objMappable)
        {
            objMappable.page = 2;
            var oldmember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objMappable.old_mcode, true);
            if (oldmember != null)
            {
                objMappable.old_fname = oldmember.fname;
                objMappable.old_lname = oldmember.lname;
                objMappable.old_address = oldmember.address;
                objMappable.old_email = oldmember.email;
            }

            var newmember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objMappable.new_mcode, true);
            if (newmember != null)
            {
                objMappable.new_fname = newmember.fname;
                objMappable.new_lname = newmember.lname;
                objMappable.new_address = newmember.address;
                objMappable.new_email = newmember.email;
            }
        }

        private void LoadCase(IMergeMappable objMappable)
        {
            objMappable.page = 2;
            if (objMappable.case_no != null)
            {
                var caseInfo = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryWithCaseNo(objMappable.case_no);
                if (caseInfo != null)
                {
                    objMappable.enq_casename = caseInfo.enq_casename;
                    objMappable.enq_type = caseInfo.enq_type;
                    objMappable.intime = caseInfo.intime;
                    objMappable.utime = caseInfo.utime;
                }
            }

            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objMappable.case_mcode, true);
            if (member != null)
            {
                objMappable.old_fname = member.fname;
                objMappable.old_lname = member.lname;
                objMappable.old_address = member.address;
                objMappable.old_email = member.email;
            }
        }
    }
}