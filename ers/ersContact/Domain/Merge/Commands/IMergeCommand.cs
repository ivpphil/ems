﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Merge.Commands
{
    public interface IMergeCommand : ICommand
    {
        bool next_member { get; }
        bool next_case { get; }
        bool final_member { get; }
        bool final_case { get; }

        int? case_no { get; }
        EnumMergeProcessType? process_type { set; }
        string old_mcode { get; }
        string new_mcode { get; }
        string case_mcode { get; }

        string user_id { get; set; }
    }
}