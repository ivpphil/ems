﻿using System;
using System.Collections.Generic;
using System.Linq;
using ersContact.Domain.Merge.Commands;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.merge;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Merge.Handlers
{
    public class MergeHandler : ICommandHandler<IMergeCommand>
    {
        public ICommandResult Submit(IMergeCommand command)
        {
            if (command.final_member)
            {
                this.FinalMember(command);
            }
            if (command.final_case)
            {
                this.FinalCase(command);
            }

            return new CommandResult(true);
        }

        private void FinalMember(IMergeCommand command)
        {
            List<ErsCtsMergeHistory> mergeHist = new List<ErsCtsMergeHistory>();

            this.Update_cts_enquiry_t(mergeHist, command);
            this.Update_d_master_t(mergeHist, command);
            this.Update_regular_t(mergeHist, command);
            this.Update_regular_detail_t(mergeHist, command);
            this.Update_cts_order_t(mergeHist, command);
            this.Update_addressbook_t(mergeHist, command);
            this.Update_point_t(mergeHist, command);
            this.Update_member_card_t(mergeHist, command);
            this.Update_member_t(mergeHist, command);

            command.process_type = EnumMergeProcessType.CUSTOMER;

            ErsCtsMergeProcess merge = new ErsCtsMergeProcess();
            merge.process_type = EnumMergeProcessType.CUSTOMER;
            merge.old_mcode = command.old_mcode;
            merge.new_mcode = command.new_mcode;
            merge.intime = DateTime.Today;
            merge.user_id = command.user_id;

            this.insertMerge(merge, mergeHist);
        }

        private void FinalCase(IMergeCommand command)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
            var new_inq = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryWithCaseNo(command.case_no.Value);
            new_inq.mcode = command.case_mcode;
            var old_inq = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryWithCaseNo(command.case_no.Value);

            repository.Update(old_inq, new_inq);

            ErsCtsMergeProcess merge = new ErsCtsMergeProcess();
            merge.process_type = EnumMergeProcessType.ENQUIRY;
            merge.new_mcode = command.case_mcode;
            merge.old_mcode = old_inq.mcode;
            merge.intime = DateTime.Today;
            merge.user_id = command.user_id;

            ErsCtsMergeHistory mergeHist = new ErsCtsMergeHistory();
            mergeHist.process_type = EnumHistoryProcessType.ENQ.ToString();
            mergeHist.record_id = old_inq.id;

            List<ErsCtsMergeHistory> mergeHistList = new List<ErsCtsMergeHistory>();
            mergeHistList.Add(mergeHist);

            this.insertMerge(merge, mergeHistList);
        }

        private void Update_cts_enquiry_t(List<ErsCtsMergeHistory> mergeHist, IMergeCommand command)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryCriteria();
            criteria.mcode = command.old_mcode;

            var enqList = repository.Find(criteria);

            foreach (var oldEnq in enqList)
            {
                var enqCri = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryCriteria();
                enqCri.id = oldEnq.id;

                var newEnq = repository.Find(enqCri).First();

                newEnq.mcode = command.new_mcode;

                repository.Update(oldEnq, newEnq);

                ErsCtsMergeHistory mrgHst = ErsFactory.ersCtsMergeFactory.GetErsCtsMergeHistory();
                mrgHst.process_type = EnumHistoryProcessType.ENQ.ToString();
                mrgHst.record_id = oldEnq.id;
                mergeHist.Add(mrgHst);
            }
        }

        private void Update_d_master_t(List<ErsCtsMergeHistory> mergeHist, IMergeCommand command)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
            criteria.mcode = command.old_mcode;
            criteria.ignoreMonitor();

            var mastList = repository.Find(criteria);

            foreach (var oldMast in mastList)
            {
                var mstCri = ErsFactory.ersOrderFactory.GetErsOrderCriteria();
                mstCri.id = oldMast.id;
                mstCri.ignoreMonitor();

                var newMast = repository.Find(mstCri).First();

                newMast.mcode = command.new_mcode;

                repository.Update(oldMast, newMast);

                ErsCtsMergeHistory mrgHst = ErsFactory.ersCtsMergeFactory.GetErsCtsMergeHistory();
                mrgHst.process_type = EnumHistoryProcessType.DMT.ToString();
                mrgHst.record_id = oldMast.id;
                mergeHist.Add(mrgHst);
            }
        }

        private void Update_regular_t(List<ErsCtsMergeHistory> mergeHist, IMergeCommand command)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderCriteria();
            criteria.mcode = command.old_mcode;

            var regList = repository.Find(criteria);

            foreach (var oldReg in regList)
            {
                var regCri = ErsFactory.ersOrderFactory.GetErsRegularOrderCriteria();
                regCri.id = oldReg.id;

                var newReg = repository.Find(regCri).First();

                newReg.mcode = command.new_mcode;

                repository.Update(oldReg, newReg);

                ErsCtsMergeHistory mrgHst = ErsFactory.ersCtsMergeFactory.GetErsCtsMergeHistory();
                mrgHst.process_type = EnumHistoryProcessType.RET.ToString();
                mrgHst.record_id = oldReg.id;
                mergeHist.Add(mrgHst);
            }
        }

        private void Update_regular_detail_t(List<ErsCtsMergeHistory> mergeHist, IMergeCommand command)
        {
            var repository = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordRepository();
            var criteria = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
            criteria.mcode = command.old_mcode;

            var detList = repository.Find(criteria);

            foreach (var oldDet in detList)
            {
                var detCri = ErsFactory.ersOrderFactory.GetErsRegularOrderRecordCriteria();
                detCri.id = oldDet.id;

                var newDet = repository.Find(detCri).First();

                newDet.mcode = command.new_mcode;

                repository.Update(oldDet, newDet);

                ErsCtsMergeHistory mrgHst = ErsFactory.ersCtsMergeFactory.GetErsCtsMergeHistory();
                mrgHst.process_type = EnumHistoryProcessType.RDT.ToString();
                mrgHst.record_id = oldDet.id;
                mergeHist.Add(mrgHst);
            }
        }

        private void Update_cts_order_t(List<ErsCtsMergeHistory> mergeHist, IMergeCommand command)
        {
            var repository = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderRepository();
            var criteria = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderCriteria();
            criteria.mcode = command.old_mcode;

            var ordList = repository.Find(criteria);

            foreach (var obj in ordList)
            {
                var ordCri = ErsFactory.ersCtsOrderFactory.GetErsCtsOrderCriteria();
                ordCri.id = obj.id;

                var new_ord = repository.Find(ordCri);

                var neword = new_ord[0];
                neword.mcode = command.new_mcode;

                var old_ord = repository.Find(ordCri);
                var oldord = old_ord[0];

                repository.Update(oldord, neword);

                ErsCtsMergeHistory mrgHst = new ErsCtsMergeHistory();
                mrgHst.process_type = EnumHistoryProcessType.CTS.ToString();
                mrgHst.record_id = obj.id;
                mergeHist.Add(mrgHst);
            }
        }

        private void Update_addressbook_t(List<ErsCtsMergeHistory> mergeHist, IMergeCommand command)
        {
            var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
            var criteria = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();

            criteria.mcode = command.old_mcode;

            var addList = repository.Find(criteria);

            foreach (ErsAddressInfo obj in addList)
            {
                var addCri = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoCriteria();
                addCri.id = obj.id;

                var nAdd = repository.Find(addCri);

                var newAdd = nAdd[0];
                newAdd.mcode = command.new_mcode;

                var oAdd = repository.Find(addCri);
                var oldAdd = oAdd[0];

                repository.Update(oldAdd, newAdd);

                ErsCtsMergeHistory mrgHst = new ErsCtsMergeHistory();
                mrgHst.process_type = EnumHistoryProcessType.ADD.ToString();
                mrgHst.record_id = obj.id;
                mergeHist.Add(mrgHst);
            }
        }

        private void Update_point_t(List<ErsCtsMergeHistory> mergeHist, IMergeCommand command)
        {
            var repository = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryRepository();
            var oldCri = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryCriteria();
            oldCri.mcode = command.old_mcode;
            oldCri.LIMIT = 1;
            oldCri.AddOrderBy("point_t.dt", Criteria.OrderBy.ORDER_BY_DESC);

            var ListOldPoint = repository.Find(oldCri);

            if (ListOldPoint.Count == 0)
            {
                return;
            }

            var oldPoint = ListOldPoint[0];

            var oldPt = this.OverwritePoint(oldPoint);
            if (oldPt.reason == null)
            {
                oldPt.reason = "merge to member code " + command.new_mcode;
            }

            repository.Insert(oldPt, true);

            var newCri = ErsFactory.ersPointHistoryFactory.GetErsPointHistoryCriteria();
            newCri.mcode = command.new_mcode;
            newCri.LIMIT = 1;
            newCri.AddOrderBy("point_t.dt", Criteria.OrderBy.ORDER_BY_DESC);

            var newPoint = repository.Find(newCri);

            if (oldPoint.total_p != 0)
            {
                if (newPoint.Count > 0)
                {
                    var newPt = this.OverwritePoint(newPoint[0]);
                    newPt.now_p = oldPoint.total_p;
                    newPt.total_p = newPt.last_p + newPt.now_p;
                    if (newPt.reason == null)
                    {
                        newPt.reason = "merge from member code " + command.old_mcode;
                    }

                    repository.Insert(newPt, true);
                }
                else
                {
                    var newPt = this.OverwritePoint(oldPoint);
                    newPt.mcode = command.new_mcode;
                    newPt.last_p = 0;
                    newPt.now_p = oldPoint.total_p;
                    newPt.total_p = newPt.now_p;
                    if (newPt.reason == null)
                    {
                        newPt.reason = "merge from member code " + command.old_mcode;
                    }

                    repository.Insert(newPt, true);
                }
            }

            ErsCtsMergeHistory mrgHst = ErsFactory.ersCtsMergeFactory.GetErsCtsMergeHistory();
            mrgHst.process_type = EnumHistoryProcessType.ENQ.ToString();
            mrgHst.record_id = oldPoint.id;
            mergeHist.Add(mrgHst);
        }

        private void Update_member_t(List<ErsCtsMergeHistory> mergeHist, IMergeCommand command)
        {
            var delMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.old_mcode, true);

            var newMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.new_mcode, true);
            ErsFactory.ersOrderFactory.GetUpdateMemberDataStgy().Update(newMember, delMember.sale.Value, delMember.sale_times.Value, (delMember.last_sale_date > newMember.last_sale_date) ? delMember.last_sale_date : newMember.last_sale_date);

            var oldMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.new_mcode, true);

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            repository.Update(oldMember, newMember);

            //旧会員を削除
            var memberRep = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var memberCri = ErsFactory.ersMemberFactory.GetErsMemberCriteria();
            memberCri.mcode = command.old_mcode;

            memberRep.Delete(memberCri);

            ErsCtsMergeHistory mrgHst = ErsFactory.ersCtsMergeFactory.GetErsCtsMergeHistory();
            mrgHst.process_type = EnumHistoryProcessType.MEM.ToString();
            mrgHst.record_id = delMember.id;
            mergeHist.Add(mrgHst);
        }

        private void Update_member_card_t(List<ErsCtsMergeHistory> mergeHist, IMergeCommand command)
        {
            var repository = ErsFactory.ersMemberFactory.GetErsMemberCardRepository();
            var criteria = ErsFactory.ersMemberFactory.GetErsMemberCardCriteria();
            criteria.mcode = command.old_mcode;
            criteria.active = EnumActive.Active;

            var listMemberCard = repository.Find(criteria);

            foreach (var memberCard in listMemberCard)
            {
                var newMemberCard = ErsFactory.ersMemberFactory.GetErsMemberCard();
                newMemberCard.OverwriteWithParameter(memberCard.GetPropertiesAsDictionary());
                newMemberCard.mcode = command.new_mcode;
                repository.Update(memberCard, newMemberCard);

                ErsCtsMergeHistory mrgHst = ErsFactory.ersCtsMergeFactory.GetErsCtsMergeHistory();
                mrgHst.process_type = EnumHistoryProcessType.MCD.ToString();
                mrgHst.record_id = memberCard.id;
                mergeHist.Add(mrgHst);
            }
        }

        private ErsPointHistory OverwritePoint(ErsPointHistory pt)
        {
            var _pt = new ErsPointHistory();

            _pt.OverwriteWithParameter(pt.GetPropertiesAsDictionary());
            _pt.dt = DateTime.Today;
            _pt.last_p = pt.total_p;
            _pt.now_p = -pt.total_p;
            _pt.total_p = 0;

            return _pt;
        }

        private void insertMerge(ErsCtsMergeProcess ersCtsMerge, List<ErsCtsMergeHistory> ersCtsMergeHist)
        {
            var ctsMergeRepository = ErsFactory.ersCtsMergeFactory.GetErsCtsMergeProcessRepository();
            var ctsMergeHistoryRepository = ErsFactory.ersCtsMergeFactory.GetErsCtsMergeHistoryRepository();
            ctsMergeRepository.Insert(ersCtsMerge, true);

            foreach (ErsCtsMergeHistory mrgHst in ersCtsMergeHist)
            {
                mrgHst.process_id = Convert.ToInt32(ersCtsMerge.id);
                ctsMergeHistoryRepository.Insert(mrgHst);
            }
        }
    }
}
