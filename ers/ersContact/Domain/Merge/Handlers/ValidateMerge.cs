﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Merge.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Merge.Handlers
{
    public class ValidateMerge
           : IValidationHandler<IMergeCommand>
    {
        public IEnumerable<ValidationResult> Validate(IMergeCommand command)
        {
            if (command.next_member)
            {
                yield return command.CheckRequired("old_mcode");
                yield return command.CheckRequired("new_mcode");
                if (command.old_mcode.HasValue())
                {
                    yield return ErsFactory.ersCtsMergeFactory.GetMergeCheckMemberStgy().CheckMemberExist(command.old_mcode, "old_mcode");
                }
                if (command.new_mcode.HasValue())
                {
                    yield return ErsFactory.ersCtsMergeFactory.GetMergeCheckMemberStgy().CheckMemberExist(command.new_mcode, "new_mcode");

                    //退会済み会員にはマージできない
                    yield return ErsFactory.ersCtsMergeFactory.GetMergeCheckMemberStgy().CheckNotResigned(command.new_mcode);
                }

                if (command.old_mcode == command.new_mcode)
                {
                    yield return new ValidationResult(ErsResources.GetMessage("70201"));
                }
            }

            if (command.next_case)
            {
                yield return command.CheckRequired("case_mcode");
                yield return command.CheckRequired("case_no");
                if (command.case_mcode.HasValue())
                {
                    yield return ErsFactory.ersCtsMergeFactory.GetMergeCheckMemberStgy().CheckMemberExist(command.case_mcode, "case_mcode");
                }
                if (command.case_no.HasValue)
                {
                    yield return ErsFactory.ersCtsMergeFactory.GetMergeCheckInquiryStgy().CheckCaseNoExist(command.case_no);
                }
            }
        }
    }
}