﻿using System;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersContact.Domain.Merge.Mappables
{
    public interface IMergeMappable
        : IMappable
    {
        bool next_member { get; }
        bool next_case { get; }
        bool return_member { get; }
        bool return_case { get; }
        EnumEnqType? enq_type { set; }
        int? page { set; }
        int? case_no { get; set; }
        string old_mcode { get; set; }
        string new_mcode { get; set; }
        string case_mcode { get; set; }
        string old_fname { set; }
        string new_fname { set; }
        string old_lname { set; }
        string new_lname { set; }
        string old_address { set; }
        string new_address { set; }
        string old_email { set; }
        string new_email { set; }
        string enq_casename { set; }
        DateTime? intime { set; }
        DateTime? utime { set; }
    }
}
