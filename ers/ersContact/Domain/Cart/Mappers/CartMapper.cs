﻿using System.Collections.Generic;
using ersContact.Domain.Cart.Mappables;
using ersContact.Models.cart;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using System.Linq;


namespace ersContact.Domain.Cart.Mappers
{
    public class CartMapper
        : IMapper<ICartMappable>
    {
        public void Map(ICartMappable objMappable)
        {
            var basket =ErsFactory.ersBasketFactory.GetErsBasket();
            basket.IsOrderUpdate = objMappable.IsOrderUpdate;
            objMappable.basket = basket;

            basket.PrepareSession();
            basket.Init("", objMappable.ransu);

            this.RefreshView(objMappable, basket);
        }

        /// <summary>
        /// refresh the values of basket items
        /// </summary>
        protected virtual void RefreshView(ICartMappable objMappable, ErsBasket basket)
        {
            objMappable.basketItems = LoadDefaultValue<Cart_items>(objMappable, basket.objBasketRecord.Values);
            objMappable.regularBasketItems = LoadDefaultValue<Cart_regular_items>(objMappable, basket.objRegularBasketRecord.Values);
        }

        /// <summary>
        /// load default value of basket itmes
        /// </summary>
        /// <returns></returns>
        protected List<T> LoadDefaultValue<T>(ICartMappable objMappable, IEnumerable<ErsBaskRecord> records)
            where T : Cart_items, new()
        {
            var basketItems = new List<T>();
            foreach (var itemData in records)
            {
                if (itemData.merchandise != null)
                {
                    if (!ErsFactory.ersMerchandiseFactory.GetChecksActivenessOfMerchandiseSpec().Check(itemData.merchandise))
                    {
                        var setup = ErsFactory.ersUtilityFactory.getSetup();
                        objMappable.controller.AddInformation(ErsResources.GetMessage("cts_ProductNotOnSale", itemData.merchandise.scode));
                    }
                }

                var cartItem = new T();
                cartItem.LoadDefaultValue(itemData);

                //伝票編集の場合
                if (!string.IsNullOrEmpty(itemData.d_no))
                {
                    //同梱物の場合、編集不可フラグを立てる
                    cartItem.orderUpdateDocBundingDisabledFlg(itemData.doc_bundling_flg);

                    //商品マスタに存在しない場合、編集不可フラグを立てる
                    cartItem.disabledFlg = ErsFactory.ersBasketFactory.GetOrderEditSpecification().IsSatisfiedBy(itemData.scode);
                }

                basketItems.Add(cartItem);
            }

            return basketItems;
        }
    }
}