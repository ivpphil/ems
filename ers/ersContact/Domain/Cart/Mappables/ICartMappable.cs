﻿using System.Collections.Generic;
using ersContact.Models.cart;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Cart.Mappables
{
    public interface ICartMappable
         : IMappable
    {
        string ransu { get; }

        List<Cart_items> basketItems { set; }

        List<Cart_regular_items> regularBasketItems { set; }

        ErsBasket basket { set; }

        bool IsOrderUpdate { get; }
    }
}