﻿using System.Collections.Generic;
using ersContact.Models.cart;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using System;

namespace ersContact.Domain.Cart.Commands
{
    public interface ICartRegularRecordCommand
         : ICommand
    {
        int? firstTime { get; set; }
        DateTime? next_date { get; }
        EnumSendPtn? send_ptn { get; set; }
        List<Cart_items> basketItems { get; }
    }
}