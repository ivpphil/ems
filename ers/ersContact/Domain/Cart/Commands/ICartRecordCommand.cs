﻿using System.Collections.Generic;
using ersContact.Models.cart;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Cart.Commands
{
    public interface ICartRecordCommand
         : ICommand
    {
        List<Cart_items> basketItems { get; }
    }
}