﻿using System.Collections.Generic;
using ersContact.Models.cart;
using jp.co.ivp.ers;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;

namespace ersContact.Domain.Cart.Commands
{
    public interface ICartCommand
         : ICommand, IManageRegularPatternDatasource
    {
        string ransu { get; }

        ErsBasket basket { get; }

        int? firstTime { get; }

        bool recompute { get; }

        bool regular_recompute { get; }

        bool regular_basket_in { get; }

        bool IsOrderUpdate { get; }

        string del_key { get; }

        string del_regular_key { get; }

        List<Cart_items> basketItems { get; }

        List<Cart_regular_items> regularBasketItems { get; }

        Dictionary<string, int?> up_amount_list { get; }

        Dictionary<string, EnumOrderStatusType> product_status_list { get; }

        string mcode { get; set; }
    }
}