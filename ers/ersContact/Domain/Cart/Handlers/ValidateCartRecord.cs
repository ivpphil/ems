﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Cart.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Cart.Handlers
{
    public class ValidateCartRecord
           : IValidationHandler<ICartRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICartRecordCommand command)
        {
            foreach (var result in this.ValidateValue(command))
            {
                yield return result;
            }
        }

        public IEnumerable<ValidationResult> ValidateValue(ICartRecordCommand command)
        {
            yield return command.CheckRequired("key");
            yield return command.CheckRequired("amount");
        }
    }
}