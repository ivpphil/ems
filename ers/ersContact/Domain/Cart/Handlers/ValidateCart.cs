﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Cart.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Cart.Handlers
{
    public class ValidateCart
           : IValidationHandler<ICartCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICartCommand command)
        {
            yield return this.ValidateDelivMethodCart(command);

            if (command.regular_basket_in)
            {
                yield return command.CheckRequired("firstTime");

                yield return command.CheckRequired("send_ptn");

                if (command.next_date == null & command.firstTime == 1)
                {
                    yield return new ValidationResult(string.Format(ErsResources.GetMessage("10000"), ErsResources.GetFieldName("senddate")), new[] { "senddate" });
                }

                if (command.send_ptn.HasValue && command.send_ptn != EnumSendPtn.NORMAL)
                {
                    var validateRegularService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(command.send_ptn.Value);

                    foreach (var result in validateRegularService.Validate(command))
                    {
                        yield return result;
                    }
                }
            }

            if (command.basketItems != null)
            {
                var controller = command.controller;
                foreach (var model in command.basketItems)
                {
                    model.AddInvalidField(controller.commandBus.Validate<ICartRecordCommand>(model));

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "basketItems" });
                        }
                    }
                }
            }

            if (command.regularBasketItems != null)
            {
                var controller = command.controller;
                foreach (var model in command.regularBasketItems)
                {
                    model.AddInvalidField(controller.commandBus.Validate<ICartRegularRecordCommand>(model));

                    if (!model.IsValid)
                    {
                        foreach (var errorMessage in model.GetAllErrorMessageList())
                        {
                            yield return new ValidationResult(errorMessage, new[] { "regularBasketItems" });
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Validate Deliv Method of adding product
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public ValidationResult ValidateDelivMethodCart(ICartCommand command)
        {
            bool result = true;

            bool IsDeleteItem = !command.regular_basket_in && command.del_key.HasValue();


            if (!IsDeleteItem && command.scode.HasValue())
            {
                if (ErsFactory.ersBasketFactory.GetBasketDelivMethodMailMaxPurchaseSpec().IsSpecifiedByExistedMail(command.ransu, command.scode, command.regular_basket_in))
                    result = false;
                else
                {
                    var merchandise = ErsFactory.ersMerchandiseFactory.GetActiveErsMerchandiseWithScode(command.scode, null);
                    if (ErsFactory.ersBasketFactory.GetBasketDelivMethodMailMaxPurchaseSpec().IsSpecifiedByDelvMethodType(command.ransu, merchandise))
                        result = false;
                }

                if (result == false)
                    return new ValidationResult(ErsResources.GetMessage("BT00100"));
            }

            return null;
        }
    }
}