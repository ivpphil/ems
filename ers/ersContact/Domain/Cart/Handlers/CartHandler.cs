﻿using System.Linq;
using ersContact.Domain.Cart.Commands;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Cart.Handlers
{
    public class CartHandler
         : ICommandHandler<ICartCommand>
    {
        public ICommandResult Submit(ICartCommand command)
        {
            var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().Retrieve(command.mcode);

            var basket = ErsFactory.ersBasketFactory.GetErsBasket();
            basket.IsOrderUpdate = command.IsOrderUpdate;

            basket.PrepareSession();
            basket.Init(command.mcode, command.ransu);

            this.Refresh(command, basket, member_rank);

            return new CommandResult(true);
        }

        protected virtual void Refresh(ICartCommand command, ErsBasket basket, int? member_rank)
        {
            if (!string.IsNullOrEmpty(command.scode))
            {
                var merchandise = this.GetAddMerchandise(command, basket.ransu, member_rank);
                if (merchandise != null)
                {
                    basket.Add(merchandise, 1);
                }
            }
            else if (!string.IsNullOrEmpty(command.del_key))
            {
                basket.Remove(command.del_key);
            }
            else if (command.recompute)
            {
                this.ReCompute(command, basket);
            }
            else if (!string.IsNullOrEmpty(command.del_regular_key))
            {
                basket.RemoveRegular(command.del_regular_key);
            }
            else if (command.regular_recompute)
            {
                this.ReComputeRegular(command, basket, member_rank);
            }
        }

        private ErsBaskRecord GetAddMerchandise(ICartCommand command, string ransu, int? member_rank)
        {
            ErsBaskRecord merchandise = null;
            if (command.regular_basket_in)
            {
                var service = ErsFactory.ersOrderFactory.GetManageRegularPatternService(command.send_ptn.Value);
                merchandise = service.GetMerchandise(ransu, command.scode, command, member_rank);
                if (command.IsOrderUpdate)
                {
                    // 伝票UPDATEの場合
                    //価格は定期のものを採用し、その他はそのまま
                    if (merchandise != null)
                    {
                        merchandise.price = merchandise.regular_price;
                        merchandise.order_type = EnumOrderType.Subscription;
                    }
                }
            }
            else
            {
                merchandise = ErsFactory.ersBasketFactory.GetErsBaskRecordWithScode(ransu, command.scode, member_rank);
                if (merchandise != null)
                {
                    merchandise.send_ptn = EnumSendPtn.NORMAL;
                    merchandise.order_type = EnumOrderType.Usually;
                }
            }
            return merchandise;
        }

        /// <summary>
        /// recompute basket items
        /// </summary>
        protected void ReCompute(ICartCommand command, ErsBasket basket)
        {
            basket.IsOrderUpdate = command.IsOrderUpdate;
            basket.ReCompute(command.mcode, command.up_amount_list, command.product_status_list);
        }

        /// <summary>
        /// recompute basket items
        /// </summary>
        protected void ReComputeRegular(ICartCommand command, ErsBasket basket, int? member_rank)
        {
            if (command.up_amount_list == null || command.up_amount_list.Count == 0)
                return;

            // Select target record.
            foreach (var item in command.regularBasketItems)
            {
                if (item.regular_key == command.up_amount_list.First().Key)
                {
                    var regular_key = item.regular_key;
                    var up_amount = command.up_amount_list[regular_key];
                    var order_status = command.product_status_list[regular_key];

                    //recompute regular order
                    basket.ReComputeRegularRecord(item, regular_key, up_amount??0, order_status, member_rank);
                }
            }
        }
    }
}