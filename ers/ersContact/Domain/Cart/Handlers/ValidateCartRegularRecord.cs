﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Cart.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using System;

namespace ersContact.Domain.Cart.Handlers
{
    public class ValidateCartRegularRecord
           : IValidationHandler<ICartRegularRecordCommand>
    {
        public IEnumerable<ValidationResult> Validate(ICartRegularRecordCommand command)
        {
            foreach (var result in this.ValidateValue(command))
            {
                yield return result;
            }
        }

        public IEnumerable<ValidationResult> ValidateValue(ICartRegularRecordCommand command)
        {
            if (command.send_ptn == EnumSendPtn.DAY_INTERVALS)
            {
                yield return command.CheckRequired("firstTime");
            }
            yield return command.CheckRequired("regular_key");
            yield return command.CheckRequired("amount");

            yield return command.CheckRequired("send_ptn");

            if (command.send_ptn.HasValue && command.send_ptn != EnumSendPtn.NORMAL)
            {
                var regularPatternService = ErsFactory.ersOrderFactory.GetManageRegularPatternService(command.send_ptn.Value);

                foreach (var result in regularPatternService.Validate(command))
                {
                    yield return result;
                }
            }

            if (command.firstTime != null && command.firstTime == 0)
            {
                var shortest_day = ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetRegularFromDate(DateTime.Now);
            }
        }
    }
}