﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Direction.Mappables;
using jp.co.ivp.ers.direction;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;

namespace ersContact.Domain.Direction.Mappers
{
    public class DirectionListMapper
        : IMapper<IDirectionListMappable>
    {
        public void Map(IDirectionListMappable objMappable)
        {
            objMappable.AgentInstructionList = this.LoadClientList(objMappable);
        }

        public List<Dictionary<string, object>> LoadClientList(IDirectionListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionsManagementRepository();
            var criteria = this.GetCriteria(objMappable);

            if (Convert.ToDateTime(objMappable.dateto) < Convert.ToDateTime(objMappable.datefrom))
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10045", ErsResources.GetFieldName("dateto"), ErsResources.GetFieldName("datefrom")));
                return null;
            }

            objMappable.recordCount = repository.GetRecordCountInstruction(criteria);

            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return null;
            }

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;
            }
            objMappable.pagerPageCount = pagerPageCount;
            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            criteria.AddOrderByIntime(Criteria.OrderBy.ORDER_BY_ASC);

            var list = repository.FindInstruction(criteria);
            objMappable.searchresult = (list.Count > 0);
            if (list.Count == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
            }
            return ErsCommon.ConvertEntityListToDictionaryList(list);
        }

        private ErsCtsDirectionsManagementCriteria GetCriteria(IDirectionListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionsManagementCriteria();

            if (objMappable.datefrom != null)
            {
                if (!string.IsNullOrEmpty(objMappable.timefrom))
                {
                    objMappable.datefrom = ValidateDateTime(objMappable.datefrom.Value.ToShortDateString() + " " + objMappable.timefrom);
                    criteria.datefrom = objMappable.datefrom;
                }
                else
                {
                    criteria.datefrom = objMappable.datefrom;
                }

                if (objMappable.dateto == null)
                    objMappable.dateto = objMappable.datefrom;
            }

            if (objMappable.dateto != null)
            {
                if (objMappable.datefrom == null)
                {
                    objMappable.datefrom = objMappable.dateto;
                    criteria.datefrom = objMappable.datefrom;
                }

                if (!string.IsNullOrEmpty(objMappable.timeto))
                {
                    objMappable.dateto = ValidateDateTime(objMappable.dateto.Value.ToShortDateString() + " " + objMappable.timeto);

                    if (objMappable.dateto.Value.Second == 0)
                    {
                        objMappable.dateto = ValidateDateTime(objMappable.dateto.Value.ToShortDateString() + " " + (DateTime.Parse(objMappable.timeto).AddSeconds(59.00)).ToLongTimeString());
                    }

                    criteria.dateto = objMappable.dateto;
                }
                else
                {
                    objMappable.dateto = ValidateDateTime(objMappable.dateto.Value.ToShortDateString() + " " + (DateTime.Parse("23:59:59")).ToLongTimeString());
                    criteria.dateto = objMappable.dateto;
                }
            }

            if (objMappable.ag_type.HasValue) criteria.ag_type = objMappable.ag_type;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            return criteria;
        }

        private DateTime ValidateDateTime(string dateTime)
        {
            DateTime dateOut;

            if (!DateTime.TryParse(dateTime, out dateOut))
            {
                throw new ErsException("10050", ErsResources.GetMessage("direction.ctsAgentSearch.DateofInstruction"));
            }

            return dateOut;
        }
    }
}