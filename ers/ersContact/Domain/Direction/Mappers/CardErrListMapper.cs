﻿using ersContact.Domain.Direction.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using System.Collections.Generic;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using System;
using jp.co.ivp.ers;
using jp.co.ivp.ers.Inquiry;
using ersContact.Models;

namespace ersContact.Domain.Direction.Mappers
{
    public class CardErrListMapper
        : IMapper<ICardErrListMappable>
    {
        public void Map(ICardErrListMappable objMappable)
        {
            objMappable.enq_progress = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ENQPGR, EnumCommonNameColumnName.namename, Convert.ToInt32(objMappable.progress));
            if (!String.IsNullOrEmpty(objMappable.user_id))
            {
                objMappable.ag_name = ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentService().GetStringFromId(objMappable.situation, Convert.ToInt32(objMappable.user_id));
            }
            objMappable.cardErrList = this.GetCardErrList(objMappable);
        }

        private List<CtsDirectionErrListData> GetCardErrList(ICardErrListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();
            var criteria = this.GetCriteria(objMappable);

            objMappable.recordCount = repository.GetRecordCountHeader(criteria);

            if (objMappable.recordCount > 0)
            {
                objMappable.carderrHasRecord = true;
            }
            else
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
            }

            string pagerRecordCount = objMappable.recordCount.ToString();
            long pagerPageCount = objMappable.recordCount / objMappable.cardErr_maxItemCount;

            if (objMappable.recordCount % objMappable.cardErr_maxItemCount > 0)
            {
                pagerPageCount += 1;
            }
            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            objMappable.pagerPageCount = pagerPageCount;

            criteria.SetOrderById(Criteria.OrderBy.ORDER_BY_ASC);

            var cardErrList = new List<CtsDirectionErrListData>();

            var list = repository.FindHeader(criteria);

            foreach (var carderror in list)
            {
                var listData = new CtsDirectionErrListData();
                listData.OverwriteWithParameter(carderror.GetPropertiesAsDictionary());

                cardErrList.Add(listData);
            }

            return cardErrList;
        }

        private ErsCtsInquiryCriteria GetCriteria(ICardErrListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryCriteria();

            if (objMappable.progress != null)
            {
                criteria.enq_progress = objMappable.progress;
            }
            if (!String.IsNullOrEmpty(objMappable.user_id))
            {
                criteria.esc_id = Convert.ToInt32(objMappable.user_id);
            }
            criteria.card_error_flg = EnumCardErrFlg.Err;

            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            return criteria;
        }
    }
}