﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Direction.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Direction.Mappers
{
    public class InboundListMapper
        : IMapper<IInboundListMappable>
    {
        public void Map(IInboundListMappable objMappable)
        {
            objMappable.InboundStatusList = this.LoadInboundList(objMappable);
        }

        public List<Dictionary<string, object>> LoadInboundList(IInboundListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundStgy();
            var criteria = this.GetCriteria(objMappable);

            objMappable.recordCount = repository.GetRecordCount(criteria);
            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            { 
                pagerPageCount += 1;
            }
            objMappable.pagerPageCount = pagerPageCount;

            if (objMappable.recordCount == 0)
            {
                return null;
            }
            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);
            criteria.AddOrderBy("cts_enquiry_t.intime", ErsCtsRepInboundCriteria.OrderBy.ORDER_BY_ASC);

            var list = repository.FindInboundList(criteria);

            return ErsCommon.ConvertEntityListToDictionaryList(list);
        }

        private ErsCtsRepInboundCriteria GetCriteria(IInboundListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundCriteria();

            criteria.enq_type = objMappable.type;
            criteria.enq_progress = objMappable.progress;
            criteria.ag_type = ErsFactory.ersCtsOperatorFactory.GetObtainAgentTypeStgy().getAgeType(objMappable.ctsUserID.Value);

            if (objMappable.progress == EnumEnqProgress.Open)
            {
                criteria.enq_situation = objMappable.situation;
            }

            if (objMappable.ShowListNow)
            {
                criteria.datefrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                criteria.dateto = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            }

            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            return criteria;
        }
    }
}