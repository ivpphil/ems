﻿using System;
using System.Linq;
using ersContact.Domain.Direction.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Direction.Mappers
{
    public class InboundAllListMapper
        : IMapper<IInboundAllListMappable>
    {
        public void Map(IInboundAllListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundStgy();

            var allCriType = this.GetTypeCriteria(null, null, objMappable);
            var allCriProg = this.GetProgressCriteria(null, null, objMappable);
            var allCriSit = this.GetSituationCriteria(null, null, objMappable);

            var alllistType = repository.Find(allCriType);
            var alllistProgress = repository.FindProgress(allCriProg);
            var alllistSituation = repository.FindSituation(allCriSit);

            foreach (var typ in alllistType)
            {
                var iPrg = alllistProgress.Where(x => x.enq_type == typ.enq_type).Distinct().Count();
                var iSit = alllistSituation.Where(x => x.enq_type == typ.enq_type).Distinct().Count();
                typ.rawspan_prg = iSit + 1;
                typ.rawspan_typ = (iPrg > 1) ? iSit + 3 : iSit + 2;
            }

            var lstProg = alllistProgress.OrderBy(x => x.enq_type).ThenByDescending(x => x.namename).ToList();
            var lstSit = alllistSituation.OrderBy(x => x.enq_type).ThenByDescending(x => x.enq_situation).ToList();

            objMappable.AllInboundType = ErsCommon.ConvertEntityListToDictionaryList(alllistType);
            objMappable.AllInboundProgress = ErsCommon.ConvertEntityListToDictionaryList(lstProg);
            objMappable.AllInboundSituation = ErsCommon.ConvertEntityListToDictionaryList(lstSit);
        }

        private ErsCtsRepInboundCriteria GetTypeCriteria(DateTime? dateFrom, DateTime? dateTo, IInboundAllListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundCriteria();

            criteria.type_code = EnumCommonNameType.ENQTYP.ToString();
            criteria.ag_type = ErsFactory.ersCtsOperatorFactory.GetObtainAgentTypeStgy().getAgeType(objMappable.ctsUserID.Value);
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.AddGroupBy("common_namecode_t.namename");
            criteria.AddGroupBy("cts_enquiry_t.enq_type");
            criteria.AddOrderBy("cts_enquiry_t.enq_type", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepInboundCriteria GetProgressCriteria(DateTime? dateFrom, DateTime? dateTo, IInboundAllListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundCriteria();

            criteria.type_code = EnumCommonNameType.ENQPGR.ToString();
            criteria.ag_type = ErsFactory.ersCtsOperatorFactory.GetObtainAgentTypeStgy().getAgeType(objMappable.ctsUserID.Value);
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.AddGroupBy("common_namecode_t.namename");
            criteria.AddGroupBy("common_namecode_t.code");
            criteria.AddGroupBy("cts_enquiry_t.enq_type");
            criteria.AddOrderBy("common_namecode_t.namename", Criteria.OrderBy.ORDER_BY_DESC);
            
            return criteria;
        }

        private ErsCtsRepInboundCriteria GetSituationCriteria(DateTime? dateFrom, DateTime? dateTo, IInboundAllListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundCriteria();

            criteria.type_code = EnumCommonNameType.ENQSIT.ToString();
            criteria.ag_type = ErsFactory.ersCtsOperatorFactory.GetObtainAgentTypeStgy().getAgeType(objMappable.ctsUserID.Value);
            criteria.enq_progress = EnumEnqProgress.Open;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            criteria.AddGroupBy("common_namecode_t.namename");
            criteria.AddGroupBy("cts_enquiry_t.enq_type");
            criteria.AddGroupBy("cts_enquiry_t.enq_situation");
            
            return criteria;
        }
    }
}