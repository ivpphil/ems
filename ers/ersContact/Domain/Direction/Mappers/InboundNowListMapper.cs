﻿using System;
using System.Linq;
using ersContact.Domain.Direction.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Direction.Mappers
{
    public class InboundNowListMapper
        : IMapper<IInboundNowListMappable>
    {
        public void Map(IInboundNowListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundStgy();

            var dtFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var dtTo = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            var site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            objMappable.LabelNow = dtFrom;

            var nowCriType = this.GetTypeCriteria(dtFrom, dtTo, objMappable, site_id);
            var nowCriProg = this.GetProgressCriteria(dtFrom, dtTo, objMappable, site_id);
            var nowCriSit = this.GetSituationCriteria(dtFrom, dtTo, objMappable, site_id);

            var nowlistType = repository.Find(nowCriType);
            var nowlistProgress = repository.FindProgress(nowCriProg);
            var nowlistSituation = repository.FindSituation(nowCriSit);

            foreach (var typ in nowlistType)
            {
                var iPrg = nowlistProgress.Where(x => x.enq_type == typ.enq_type).Distinct().Count();
                var iSit = nowlistSituation.Where(x => x.enq_type == typ.enq_type).Distinct().Count();
                typ.rawspan_prg = iSit + 1;
                typ.rawspan_typ = (iPrg > 1) ? iSit + 3 : iSit + 2;
            }

            var lstProg = nowlistProgress.OrderBy(x => x.enq_type).ThenByDescending(x => x.namename).ToList();
            var lstSit = nowlistSituation.OrderBy(x => x.enq_type).ThenByDescending(x => x.enq_situation).ToList();

            objMappable.NowInboundType = ErsCommon.ConvertEntityListToDictionaryList(nowlistType);
            objMappable.NowInboundProgress = ErsCommon.ConvertEntityListToDictionaryList(lstProg);
            objMappable.NowInboundSituation = ErsCommon.ConvertEntityListToDictionaryList(lstSit);
        }

        private ErsCtsRepInboundCriteria GetTypeCriteria(DateTime? dateFrom, DateTime? dateTo, IInboundNowListMappable objMappable, int? site_id)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundCriteria();

            criteria.datefrom = dateFrom;
            criteria.dateto = dateTo;
            criteria.type_code = EnumCommonNameType.ENQTYP.ToString();
            criteria.ag_type = ErsFactory.ersCtsOperatorFactory.GetObtainAgentTypeStgy().getAgeType(objMappable.ctsUserID.Value);
            criteria.site_id = site_id;
            criteria.AddGroupBy("common_namecode_t.namename");
            criteria.AddGroupBy("cts_enquiry_t.enq_type");
            criteria.AddOrderBy("cts_enquiry_t.enq_type", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepInboundCriteria GetProgressCriteria(DateTime? dateFrom, DateTime? dateTo, IInboundNowListMappable objMappable, int? site_id)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundCriteria();

            criteria.datefrom = dateFrom;
            criteria.dateto = dateTo;
            criteria.type_code = EnumCommonNameType.ENQPGR.ToString();
            criteria.ag_type = ErsFactory.ersCtsOperatorFactory.GetObtainAgentTypeStgy().getAgeType(objMappable.ctsUserID.Value);
            criteria.site_id = site_id;
            criteria.AddGroupBy("common_namecode_t.namename");
            criteria.AddGroupBy("common_namecode_t.code");
            criteria.AddGroupBy("cts_enquiry_t.enq_type");
            criteria.AddOrderBy("common_namecode_t.namename", Criteria.OrderBy.ORDER_BY_DESC);
            
            return criteria;
        }

        private ErsCtsRepInboundCriteria GetSituationCriteria(DateTime? dateFrom, DateTime? dateTo, IInboundNowListMappable objMappable, int? site_id)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundCriteria();

            criteria.datefrom = dateFrom;
            criteria.dateto = dateTo;
            criteria.type_code = EnumCommonNameType.ENQSIT.ToString();
            criteria.ag_type = ErsFactory.ersCtsOperatorFactory.GetObtainAgentTypeStgy().getAgeType(objMappable.ctsUserID.Value);
            criteria.enq_progress = EnumEnqProgress.Open;
            criteria.site_id = site_id;
            criteria.AddGroupBy("common_namecode_t.namename");
            criteria.AddGroupBy("cts_enquiry_t.enq_type");
            criteria.AddGroupBy("cts_enquiry_t.enq_situation");
            
            return criteria;
        }
    }
}