﻿using ersContact.Domain.Direction.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers;

namespace ersContact.Domain.Direction.Mappers
{
    public class InboundInstructionListMapper
        : IMapper<IInboundInstructionListMappable>
    {
        public void Map(IInboundInstructionListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundStgy();
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundCriteria();
            criteria.case_no = objMappable.case_no;

            var list = repository.FindInboundList(criteria);
            if (list.Count > 0)
            {
                ErsCtsRepInbound inst = list[0];
                objMappable.type = inst.enq_type;
                objMappable.enq_casename = inst.enq_casename;
                objMappable.priority = inst.w_priority;
                objMappable.enq_progress = inst.w_progress;
                objMappable.enq_situation = inst.w_situation;
                if (inst.cate1 != null) objMappable.cate_name = inst.cate1;
                if (inst.cate2 != null) objMappable.cate_name += ((!string.IsNullOrEmpty(objMappable.cate_name)) ? " / " : "") + inst.cate2;
                if (inst.cate3 != null) objMappable.cate_name += ((!string.IsNullOrEmpty(objMappable.cate_name)) ? " / " : "") + inst.cate3;
                if (inst.cate4 != null) objMappable.cate_name += ((!string.IsNullOrEmpty(objMappable.cate_name)) ? " / " : "") + inst.cate4;
                if (inst.cate5 != null) objMappable.cate_name += ((!string.IsNullOrEmpty(objMappable.cate_name)) ? " / " : "") + inst.cate5;
                objMappable.ag_name = inst.ag_name;
                objMappable.dmemo = "";
            }
        }
    }
}