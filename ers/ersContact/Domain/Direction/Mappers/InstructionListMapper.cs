﻿using ersContact.Domain.Direction.Mappables;
using jp.co.ivp.ers.direction;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersContact.Domain.Direction.Mappers
{
    public class InstructionListMapper
        : IMapper<IInstructionListMappable>
    {
        public void Map(IInstructionListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionsManagementRepository();
            var criteria = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionsManagementCriteria();
            criteria.id = objMappable.id;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            var list = repository.FindInstruction(criteria);
            if (list.Count > 0)
            {
                this.OverwriteThisModel(list[0], objMappable);
            }
        }

        protected void OverwriteThisModel(ErsCtsDirection inst, IInstructionListMappable objMappable)
        {
            objMappable.id = inst.id;
            objMappable.intime = (inst.intime < inst.utime) ? inst.utime : inst.intime;
            objMappable.to_user_id = inst.to_user_id;
            objMappable.from_user_id = inst.from_user_id;
            objMappable.check_desc = inst.check_desc;
            objMappable.enq_type = inst.w_type;
            objMappable.case_no = inst.case_no;
            objMappable.enq_casename = inst.enq_casename;
            objMappable.priority = inst.w_priority;
            objMappable.enq_progress = inst.w_progress;
            objMappable.enq_situation = inst.w_situation;
            objMappable.dmemo = inst.dmemo;

            if (inst.cate1 != null) objMappable.cate_name = inst.cate1;
            if (inst.cate2 != null) objMappable.cate_name += ((!string.IsNullOrEmpty(objMappable.cate_name)) ? " / " : "") + inst.cate2;
            if (inst.cate3 != null) objMappable.cate_name += ((!string.IsNullOrEmpty(objMappable.cate_name)) ? " / " : "") + inst.cate3;
            if (inst.cate4 != null) objMappable.cate_name += ((!string.IsNullOrEmpty(objMappable.cate_name)) ? " / " : "") + inst.cate4;
            if (inst.cate5 != null) objMappable.cate_name += ((!string.IsNullOrEmpty(objMappable.cate_name)) ? " / " : "") + inst.cate5;

            objMappable.ag_name = inst.ag_name;
            objMappable.to_ag_name = inst.to_ag_name;
            objMappable.from_ag_name = inst.from_ag_name;
        }
    }
}