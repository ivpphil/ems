﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Direction.Commands
{
    public interface IInstructionUpdateCommand
        : ICommand
    {
        int? id { get; }
    }
}