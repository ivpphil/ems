﻿using System.Collections.Generic;
using ersContact.Models.configuration;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersContact.Models;

namespace ersContact.Domain.Direction.Commands
{
    public interface ICardErrListUpdateCommand
        : ICommand, IErsModelBase
    {
        bool register { get; }
        List<CtsDirectionErrListData> cardErrList { get; set; }
    }
}