﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Direction.Commands
{
    public interface IDirectionRegisterCommand
        : ICommand
    {
        int? case_no { get; }
        int? ctsUserID { get; }
        short check_s { get; }
        string dmemo { get; }
        bool RegistDone { get; set; }
        bool inboundInput { get; set; }
    }
}