﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using System;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersContact.Domain.Direction.Mappables
{
    public interface IDirectionListMappable
        : IMappable, IErsModelBase
    {
        DateTime? datefrom { get; set; }
        DateTime? dateto { get; set; }
        string timefrom { get; }
        string timeto { get; }
        EnumAgType? ag_type { get; set; }
        long recordCount { get; set; }
        long maxItemCount { get; }
        ErsPagerModel pager { get; }
        List<Dictionary<string, object>> AgentInstructionList { get; set; }
        bool searchresult { get; set; }

        long pagerPageCount { get; set; }
    }
}
