﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers;
using ersContact.Models;

namespace ersContact.Domain.Direction.Mappables
{
    public interface ICardErrListMappable
        : IMappable
    {
        ErsPagerModel pager { get; }
        bool carderrHasRecord { set; }
        long recordCount { get; set; }
        long cardErr_maxItemCount { get; }
        string enq_progress { set; }
        string ag_name { set; }
        string user_id { get; }
        EnumEnqProgress? progress { get; }
        EnumEnqSituation? situation { get; }
        List<CtsDirectionErrListData> cardErrList { get; set; }

        long pagerPageCount { get; set; }
    }
}
