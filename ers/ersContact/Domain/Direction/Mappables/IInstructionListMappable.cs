﻿using System;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersContact.Domain.Direction.Mappables
{
    public interface IInstructionListMappable
        : IMappable
    {
        int? id { get; set; }
        DateTime? intime { get; set; }
        int? to_user_id { get; set; }
        int? from_user_id { get; set; }
        string check_desc { get; set; }
        string enq_type { get; set; }
        int? case_no { get; set; }
        string enq_casename { get; set; }
        string priority { get; set; }
        string enq_progress { get; set; }
        string enq_situation { get; set; }
        string dmemo { get; set; }
        string cate_name { get; set; }
        string ag_name { get; set; }
        string to_ag_name { get; set; }
        string from_ag_name { get; set; }
    }
}
