﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;

namespace ersContact.Domain.Direction.Mappables
{
    public interface IInboundListMappable
        : IMappable, IErsModelBase
    {
        EnumEnqType? type { get; set; }
        EnumEnqProgress? progress { get; set; }
        EnumEnqSituation? situation { get; set; }
        bool ShowListNow { get; set; }
        long recordCount { get; set; }
        long maxItemCount { get; }
        ErsPagerModel pager { get; }
        List<Dictionary<string, object>> InboundStatusList { get; set; }
        int? ctsUserID { get; set; }
        long pagerPageCount { get; set; }
    }
}
