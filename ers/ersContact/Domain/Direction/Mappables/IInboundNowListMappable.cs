﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using System;

namespace ersContact.Domain.Direction.Mappables
{
    public interface IInboundNowListMappable
        : IMappable, IErsModelBase
    {
        DateTime? LabelNow { get; set; }
        int? ctsUserID { get; set; }
        List<Dictionary<string, object>> NowInboundType { get; set; }
        List<Dictionary<string, object>> NowInboundProgress { get; set; }
        List<Dictionary<string, object>> NowInboundSituation { get; set; }
    }
}
