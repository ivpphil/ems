﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc;
using System;

namespace ersContact.Domain.Direction.Mappables
{
    public interface IInboundAllListMappable
        : IMappable, IErsModelBase
    {
        DateTime? LabelNow { get; set; }
        int? ctsUserID { get; set; }
        List<Dictionary<string, object>> AllInboundType { get; set; }
        List<Dictionary<string, object>> AllInboundProgress { get; set; }
        List<Dictionary<string, object>> AllInboundSituation { get; set; }
    }
}
