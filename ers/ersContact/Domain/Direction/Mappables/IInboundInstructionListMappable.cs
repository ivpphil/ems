﻿using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersContact.Domain.Direction.Mappables
{
    public interface IInboundInstructionListMappable
        : IMappable
    {
        int? case_no { get; set; }
        EnumEnqType? type { get; set; }
        string enq_casename { get; set; }
        string priority { get; set; }
        string enq_progress { get; set; }
        string enq_situation { get; set; }
        string cate_name { get; set; }
        string ag_name { get; set; }
        string dmemo { get; set; }
    }
}
