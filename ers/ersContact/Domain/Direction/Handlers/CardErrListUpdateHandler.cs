﻿using ersContact.Domain.Direction.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System;

namespace ersContact.Domain.Direction.Handlers
{
    public class CardErrListUpdateHandler
        : ICommandHandler<ICardErrListUpdateCommand>
    {
        public ICommandResult Submit(ICardErrListUpdateCommand command)
        {
            if (command.register)
            {
                this.Update(command);
            }

            return new CommandResult(true);
        }

        internal void Update(ICardErrListUpdateCommand command)
        {
            var repository = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryRepository();

            foreach (var record in command.cardErrList)
            {
                var CardErrList = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryScreenWithID(record.id.Value);
                var old_CardErrList = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryScreenWithParameters(CardErrList.GetPropertiesAsDictionary());

                switch (record.enq_progress)
                {
                    case EnumEnqProgress.Close:
                        // クローズの場合
                        break;
                    case EnumEnqProgress.None:
                        //初回設定の場合
                        if (record.esc_id.HasValue())
                        {
                            CardErrList.esc_id = record.esc_id;
                            CardErrList.enq_progress = EnumEnqProgress.Open;
                        }
                        break;
                    case EnumEnqProgress.Open:
                        if (record.modify_esc_id.HasValue())
                        {
                            CardErrList.esc_id = record.modify_esc_id;
                        }
                        //担当者解除の場合
                        else
                        {
                            CardErrList.esc_id = record.modify_esc_id;
                            CardErrList.enq_progress = EnumEnqProgress.None;
                        }
                        break;
                }

                repository.UpdateCardErr(CardErrList, old_CardErrList);
            }
        }
    }
}