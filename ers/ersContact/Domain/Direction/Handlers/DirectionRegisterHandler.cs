﻿using System;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersContact.Domain.Direction.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.direction;
using jp.co.ivp.ers;

namespace ersContact.Domain.Direction.Handlers
{
    public class DirectionRegisterHandler
        : ICommandHandler<IDirectionRegisterCommand>
    {
        public ICommandResult Submit(IDirectionRegisterCommand command)
        {
            var repository = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionsManagementRepository();
            var inquiry = ErsFactory.ersCtsInquiryFactory.GetErsCtsInquiryScreenWithCaseNo(command.case_no);

            var objData = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionsManagement();
            objData.id = 0;
            objData.case_no = command.case_no;
            
            var agent = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(Convert.ToInt32(inquiry.user_id));
            if (agent == null)
            {
                throw new ErsException("29002");
            }

            objData.from_user_id = command.ctsUserID;
            objData.to_user_id = (int)agent.id;
            objData.check_s = command.check_s;
            objData.dmemo = command.dmemo;
            objData.intime = DateTime.Now;
            objData.utime = DateTime.Now;
            objData.sub_no = inquiry.sub_no;
            objData.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            repository.Insert(objData, true);

            command.RegistDone = true;
            command.inboundInput = true;

            return new CommandResult(true);
        }
    }
}