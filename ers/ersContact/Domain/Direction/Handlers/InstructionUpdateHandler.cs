﻿using System;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersContact.Domain.Direction.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.direction;
using jp.co.ivp.ers;

namespace ersContact.Domain.Direction.Handlers
{
    public class InstructionUpdateHandler
        : ICommandHandler<IInstructionUpdateCommand>
    {
        public ICommandResult Submit(IInstructionUpdateCommand command)
        {
            var repository = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionsManagementRepository();
            var criteria = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionsManagementCriteria();
            criteria.id = command.id;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            var newlist = repository.Find(criteria);
            if (newlist.Count > 0)
            {
                var newInst = newlist[0];
                newInst.check_s = 1;
                newInst.utime = DateTime.Now;

                var oldList = repository.Find(criteria);
                var oldInst = oldList[0];

                repository.Update(oldInst, newInst);
            }

            return new CommandResult(true);
        }
    }
}