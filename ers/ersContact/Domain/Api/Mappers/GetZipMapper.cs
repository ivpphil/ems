﻿using ersContact.Domain.Api.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersContact.Domain.Api.Mappers
{
    public class GetZipMapper
        : IMapper<IGetZipMappable>
    {
        public void Map(IGetZipMappable objMappable)
        {
            var zipService = ErsFactory.ersViewServiceFactory.GetErsViewZipService();

            zipService.Search(objMappable.zip);
            objMappable.inner_error_message = zipService.error_message;
            objMappable.pref = zipService.pref;
            objMappable.address = zipService.address;
            objMappable.address2 = zipService.address2;
        }
    }
}