﻿using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Api.Commands
{
    public interface IGetZipCommand
        : ICommand
    {
    }
}