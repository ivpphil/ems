﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Api.Commands
{
    public interface IWithdrawMemberCommand
        : ICommand
    {
        string mcode { get; set; }
    }
}