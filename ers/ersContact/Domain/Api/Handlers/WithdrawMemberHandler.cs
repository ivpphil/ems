﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersContact.Domain.Api.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Api.Handlers
{
    public class WithdrawMemberHandler
        : ICommandHandler<IWithdrawMemberCommand>
    {
        public ICommandResult Submit(IWithdrawMemberCommand command)
        {
            var mcode = command.mcode;

            //トランザクション開始
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();

            //会員削除
            var oldMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);

            if (oldMember != null)
            {
                // 存在する場合のみ更新
                var objMember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);
                objMember.deleted = EnumDeleted.Deleted;
                repository.Update(oldMember, objMember);
            }

            return new CommandResult(true);
        }
    }
}