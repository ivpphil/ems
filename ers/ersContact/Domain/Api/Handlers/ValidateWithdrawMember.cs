﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ersContact.Domain.Api.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;

namespace ersContact.Domain.Api.Handlers
{
    public class ValidateWithdrawMember
        : IValidationHandler<IWithdrawMemberCommand>
    {
        public IEnumerable<ValidationResult> Validate(IWithdrawMemberCommand command)
        {
            yield return command.CheckRequired("mcode");

            if (!command.IsValidField("mcode"))
            {
                yield break;
            }

            var mcode = command.mcode;

            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(mcode);

            //退会済み
            if (member == null)
            {
                yield break;
            }

            var specification = ErsFactory.ersMemberFactory.GetCancelMembershipSpecification();
            specification.mcode = member.mcode;
            specification.site_id = member.site_id;

            //新着注文および未配送注文が存在するため退会できません
            if (specification.isSatisfiedBy() == false
                || ErsFactory.ersMemberFactory.GetHasNotYetSaledIssuedRegularOrderSpec().Has(mcode, member.site_id))
            {
                yield return new ValidationResult(ErsResources.GetMessage("20301"), new[] { "mcode" });
            }

            yield break;
        }
    }
}