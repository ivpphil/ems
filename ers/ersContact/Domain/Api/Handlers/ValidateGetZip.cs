﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Api.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Api.Handlers
{
    public class ValidateGetZip
        : IValidationHandler<IGetZipCommand>
    {
        public IEnumerable<ValidationResult> Validate(IGetZipCommand command)
        {
            yield return command.CheckRequired("zip");
        }
    }
}