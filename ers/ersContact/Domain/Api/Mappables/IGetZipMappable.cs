﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Api.Mappables
{
    public interface IGetZipMappable
        : IMappable
    {
        string zip { get; }

        string inner_error_message { set; }

        int? pref { set; }

        string address { set; }

        string address2 { set; }
    }
}