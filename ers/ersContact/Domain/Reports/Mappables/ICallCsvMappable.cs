﻿using System;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappables
{
    public interface ICallCsvMappable
        : IMappable
    {
        bool TargetRegOrder { get; set; }
        bool TargetByDay { get; set; }
        bool TargetByMonth { get; set; }
        bool TargetByTime { get; set; }
        bool repcall { set; }
        EnumAgType? ag_type { get; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; }

        ErsCsvCreater csvCreater { get; set; }
    }
}
