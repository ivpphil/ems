﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.reports;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IRepAgeMappable
        : IMappable
    {
        int? pref1 { get; }
        int? pref2 { get; }
        int? pref3 { get; }
        int? pref4 { get; }
        int? pref5 { get; }
        int billCount { set; }
        string scode1 { get; }
        string scode2 { get; }
        string scode3 { get; }
        string scode4 { get; }
        string scode5 { get; }
        DateTime? datefrom { get; set; }
        DateTime? dateto { get; set; }
        IList<ErsCtsRepAge> repageList { set; }
    }
}
