﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IProductSalesMappable
        : IMappable, IErsModelBase
    {
        bool searchscode1 { get; }
        bool searchscode2 { get; }
        bool searchscode3 { get; }
        bool searchscode4 { get; }
        bool searchscode5 { get; }
        bool repproductsearch { set; }
        long recordCount { get; set; }
        long maxItemCount { get; }
        string scode1 { get; }
        string scode2 { get; }
        string scode3 { get; }
        string scode4 { get; }
        string scode5 { get; }
        ErsPagerModel pager { get; }
        List<Dictionary<string, object>> repproductList { get; set; }

        long pagerPageCount { get; set; }
    }
}
