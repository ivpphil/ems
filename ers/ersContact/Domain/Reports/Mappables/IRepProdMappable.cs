﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IRepProdMappable
        : IMappable, IErsModelBase
    {
        EnumAgType? ag_type { get; }
        long total_number { get; set; }
        long total_amount { get; set; }
        string prodcode { get; }
        string prodname { get; }
        string agentid { get; }
        string TargetOrder { get; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; }
        List<Dictionary<string, object>> repprodList { get; set; }
    }
}
