﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IRepContactLogDetailMappable
        : IMappable, IErsModelBase
    {
        bool todetail { set; }
        bool repcontact { set; }
        int? case_no { get; }
        List<Dictionary<string, object>> repcontactlogList { set; }
        List<Dictionary<string, object>> repcontactlogDetailList { set; }
    }
}
