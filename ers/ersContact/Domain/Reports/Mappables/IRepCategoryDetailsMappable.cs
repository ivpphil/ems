﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IRepCategoryDetailsMappable
        : IMappable
    {
        int case_no { get; }
        List<Dictionary<string, object>> repcategoryDetail { set; }
        List<Dictionary<string, object>> repcategoryBillDetail { set; }
        bool repcategory { set; }
        bool repcategoryList { set; }
        bool repcategoryDetails { set; }
    }
}
