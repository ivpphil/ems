﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Reports.Mappables
{
    public interface ICategoryLabelMappable
        : IMappable
    {
        string catlabel1 { set; }
        string catlabel2 { set; }
        string catlabel3 { set; }
        string catlabel4 { set; }
        string catlabel5 { set; }
    }
}
