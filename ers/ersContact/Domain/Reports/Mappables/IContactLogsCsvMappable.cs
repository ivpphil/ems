﻿using System;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IContactLogsCsvMappable
        : IMappable
    {
        bool repcontact { set; }
        int? typcode { get; }
        int? prycode { get; }
        int? stscode { get; }
        int? pgrcode { get; }
        int? sitcode { get; }
        int? ct1code { get; }
        int? ct2code { get; }
        int? ct3code { get; }
        int? ct4code { get; }
        int? ct5code { get; }
        string enq_casename { get; }
        ErsCsvCreater csvCreater { get; set; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; }
    }
}
