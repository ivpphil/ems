﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IRepContactLogMappable
        : IMappable, IErsModelBase
    {
        bool repcontactHasRecord { set; }
        int? typcode { get; }
        int? prycode { get; }
        int? stscode { get; }
        int? pgrcode { get; }
        int? sitcode { get; }
        int? ct1code { get; }
        int? ct2code { get; }
        int? ct3code { get; }
        int? ct4code { get; }
        int? ct5code { get; }
        long recordCount { get; set; }
        long maxItemCount { get; }
        string enq_casename { get; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; }
        ErsPagerModel pager { get; }
        List<Dictionary<string, object>> repcontactlogList { get; set; }

        long pagerPageCount { get; set; }
    }
}
