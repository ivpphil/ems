﻿using System;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Reports.Mappables
{
    public interface ICategoryCsvMappable
        : IMappable
    {
        bool repcategory { set; }
        bool repcategoryList { set; }
        int p_code { get; }
        int c_code { get; }
        int? typcode { get; }
        int? prycode { get; }
        int? sitcode { get; }
        int? stscode { get; }
        int? pgrcode { get; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; }

        ErsCsvCreater csvCreater { get; set; }
    }
}
