﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IRepCategoryListMappable
        : IMappable
    {
        bool repcategory { set; }
        bool repcategoryList { set; }
        int p_code { get; }
        int c_code { get; }
        long recordCount { get; set; }
        long maxItemCount { get; }
        int? typcode { get; }
        int? prycode { get; }
        int? stscode { get; }
        int? pgrcode { get; }
        int? sitcode { get; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; set; }
        ErsPagerModel pager { get; }
        List<Dictionary<string, object>> repcategoryListDetail { set; }

        long pagerPageCount { get; set; }
    }
}
