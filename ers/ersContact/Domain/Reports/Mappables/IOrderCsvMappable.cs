﻿using System;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IOrderCsvMappable
        : IMappable
    {
        bool reporder { set; }
        EnumAgType? ag_type { get; }
        string agentid { get; }
        ErsCsvCreater csvCreater { get; set; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; }
    }
}
