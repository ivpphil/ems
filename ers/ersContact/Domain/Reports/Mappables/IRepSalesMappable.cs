﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IRepSalesMappable
        : IMappable, IErsModelBase
    {
        bool TargetRegOrder { get; set; }
        bool TargetTempOrder { get; set; }
        bool repsalesHasRecord { set; }
        int? colcount { set; }
        int? order_total { set; get; }
        string TargetOrder { get; set; }
        string scode1 { get; }
        string scode2 { get; }
        string scode3 { get; }
        string scode4 { get; }
        string scode5 { get; }
        string[] scodes { get; set; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; }
        List<Dictionary<string, object>> timeinterval { set; }
        List<Dictionary<string, object>> repsalesList { get; set; }
        List<Dictionary<string, object>> repsalesListByInterval { set; }
        List<Dictionary<string, object>> repsalesListByDNo { get; set; }
    }
}
