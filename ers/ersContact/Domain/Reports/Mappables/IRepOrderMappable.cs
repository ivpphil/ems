﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IRepOrderMappable
        : IMappable, IErsModelBase
    {
        bool reporder { set; }
        bool reporderHasRecord { set; }
        EnumAgType? ag_type { get; }
        string agentid { get; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; }
        List<Dictionary<string, object>> reporderList { get; set; }
    }
}
