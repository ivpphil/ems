﻿using System;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IProductCsvMappable
        : IMappable
    {
        ErsCsvCreater csvCreater { get; set; }

        bool repprod { set; }
        EnumAgType? ag_type { get; }
        string prodcode { get; }
        string prodname { get; }
        string agentid { get; }
        string TargetOrder { get; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; }
    }
}
