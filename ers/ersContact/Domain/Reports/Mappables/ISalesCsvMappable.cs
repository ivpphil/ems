﻿using System;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Reports.Mappables
{
    public interface ISalesCsvMappable
        : IMappable
    {
        bool repsales { set; }
        bool TargetRegOrder { get; }
        string scode1 { get; }
        string scode2 { get; }
        string scode3 { get; }
        string scode4 { get; }
        string scode5 { get; }
        string[] scodes { get; set; }
        ErsCsvCreater csvCreater { get; set; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; }
    }
}
