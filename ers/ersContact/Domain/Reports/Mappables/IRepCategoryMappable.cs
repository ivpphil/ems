﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Reports.Mappables
{
    public interface IRepCategoryMappable
        : IMappable
    {
        bool repcategoryHasRecord { set; }
        int? typcode { get; }
        int? prycode { get; }
        int? stscode { get; }
        int? pgrcode { get; }
        int? sitcode { get; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; set; }
        List<Dictionary<string, object>> repcategoryListParent { set; }
        List<Dictionary<string, object>> repcategoryListChild { set; }
        List<Dictionary<string, object>> repcategoryRowSpan { set; }
        List<Dictionary<string, object>> repcategoryChildCount { set; }
    }
}
