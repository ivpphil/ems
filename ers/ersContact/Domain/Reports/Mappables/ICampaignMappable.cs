﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersContact.Domain.Reports.Mappables
{
    public interface ICampaignMappable
        : IMappable, IErsModelBase
    {
        bool reload { get; }
        bool medselect { get; }
        bool medsearch { get; }
        int pageCnt { set; }
        long recordCount { get; set; }
        long maxItemCount { get; }
        string ccode { get; }
        string campaign_name { set; }
        ErsPagerModel pager { get; }
        List<Dictionary<string, object>> campaignList { set; }

        long pagerPageCount { get; set; }
    }
}
