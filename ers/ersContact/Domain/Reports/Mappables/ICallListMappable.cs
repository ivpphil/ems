﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappables
{
    public interface ICallListMappable
        : IMappable, IErsModelBase
    {
        bool TargetRegOrder { get; set; }
        bool TargetTempOrder { get; set; }
        bool TargetByDay { get; set; }
        bool TargetByMonth { get; set; }
        bool TargetByTime { get; set; }
        bool repcallHasRecord { set; }
        int? colcount { set; }
        int? rowcount { set; }
        EnumAgType? ag_type { get; }
        string TargetOrder { get; set; }
        string TargetScale { get; set; }
        DateTime? datefrom { get; }
        DateTime? dateto { get; }
        IList<ErsCtsRepCall> list { get; set; }
        IList<ErsCtsRepCall> listTime { get; set; }
        IList<ErsCtsRepCall> listUserTime { get; set; }
        IList<ErsCtsRepCall> listInterval { get; set; }
        List<Dictionary<string, object>> repcallTotalTime { set; }
        List<Dictionary<string, object>> repcallTotalUserTime { get; set; }
        List<Dictionary<string, object>> repcallList { get; set; }
        List<Dictionary<string, object>> repcallInterval { get; set; }

        int repcallTotal { get; set; }
    }
}
