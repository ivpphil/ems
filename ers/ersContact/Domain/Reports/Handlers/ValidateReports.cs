﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Reports.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Handlers
{
    public class ValidateReports
           : IValidationHandler<IReportsCommand>
    {
        public IEnumerable<ValidationResult> Validate(IReportsCommand command)
        {
            yield return command.CheckRequired("datefrom");
            yield return command.CheckRequired("dateto");

            foreach (var result in ErsFactory.ersCommonFactory.GetCheckDateFromToStgy().CheckDateTime("datefrom", command.datefrom, "dateto", command.dateto))
            {
                yield return result;
            }
        }
    }
}