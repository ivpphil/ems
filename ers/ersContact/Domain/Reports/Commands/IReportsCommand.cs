﻿using System;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Reports.Commands
{
    public interface IReportsCommand : ICommand
    {
        DateTime? datefrom { get; }
        DateTime? dateto { get; }
    }
}