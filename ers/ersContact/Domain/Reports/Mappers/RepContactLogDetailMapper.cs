﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappers
{
    public class RepContactLogDetailMapper
        : IMapper<IRepContactLogDetailMappable>
    {
        public void Map(IRepContactLogDetailMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogStgy();
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogCriteria();
            criteria.case_no = objMappable.case_no;

            var list = repository.FindDetail(criteria);
            objMappable.repcontactlogList = ErsCommon.ConvertEntityListToDictionaryList(list);
            objMappable.repcontactlogDetailList = this.loadRepContactLogBillDetail(objMappable);
            objMappable.todetail = true;
            objMappable.repcontact = true;
        }

        private List<Dictionary<string, object>> loadRepContactLogBillDetail(IRepContactLogDetailMappable objMappable)
        {
            List<ErsCtsRepContactLog> first = new List<ErsCtsRepContactLog>();
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogStgy();
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogCriteria();
            criteria.case_num = Convert.ToInt16(objMappable.case_no);
            criteria.AddOrderBy("cts_enquiry_detail_t.sub_no", Criteria.OrderBy.ORDER_BY_DESC);

            var list = repository.FindBillDetail(criteria);

            return ErsCommon.ConvertEntityListToDictionaryList(list);
        }
    }
}