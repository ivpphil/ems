﻿using System.Web;
using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Reports.Mappers
{
    public class RepSalesMapper
        : IMapper<IRepSalesMappable>
    {
        public void Map(IRepSalesMappable objMappable)
        {
            string target = objMappable.TargetOrder;
            if (target == null)
            {
                if (!objMappable.TargetRegOrder && !objMappable.TargetTempOrder)
                {
                    target = HttpContext.Current.Request.Form.GetValues("TargetOrder").GetValue(0).ToString();
                }
                else
                {
                    if (objMappable.TargetRegOrder)
                    {
                        target = EnumTargetOrder.RegularOrder.ToString();
                    }
                    else
                    {
                        target = EnumTargetOrder.TemporaryOrder.ToString();
                    }
                }
            }

            if (target == EnumTargetOrder.RegularOrder.ToString())
            {
                objMappable.TargetRegOrder = true;
                objMappable.TargetTempOrder = false;
            }
            else
            {
                objMappable.TargetRegOrder = false;
                objMappable.TargetTempOrder = true;
            }

            objMappable.TargetOrder = target;

            if (objMappable.TargetRegOrder)
            {
                this.loadRepSales(objMappable);
            }
            else
            {
                this.loadRepSalesTemp(objMappable);
            }
        }

        private void loadRepSales(IRepSalesMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesStgy();

            var criteria = this.repsalesCriteria(objMappable);
            var criteriaTotalByInterval = this.repsalesCriteria(objMappable);
            var criteriaTotalByDNo = this.repsalesCriteria(objMappable);
            var criteriaTime = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesCriteria();

            this.GetCriteria(ref criteria, ref criteriaTotalByInterval, ref criteriaTotalByDNo, ref criteriaTime);

            objMappable.timeinterval = ErsCommon.ConvertEntityListToDictionaryList(repository.FindInterval(criteriaTime));
            objMappable.repsalesList = ErsCommon.ConvertEntityListToDictionaryList(repository.Find(criteria));
            objMappable.repsalesListByInterval = ErsCommon.ConvertEntityListToDictionaryList(repository.FindTotalByInterval(criteriaTotalByInterval));

            var listTotalByDNo = repository.FindTotalByDNo(criteriaTotalByDNo);
            objMappable.repsalesListByDNo = ErsCommon.ConvertEntityListToDictionaryList(listTotalByDNo);

            objMappable.colcount = objMappable.repsalesListByDNo.Count + 1;
            objMappable.order_total = 0;
            foreach (var d_no_total in listTotalByDNo)
            {
                objMappable.order_total += d_no_total.d_no_count;
            }

            if (objMappable.repsalesList.Count > 0)
            {
                objMappable.repsalesHasRecord = true;
            }
            else
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
            }
        }

        public void loadRepSalesTemp(IRepSalesMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesStgy();

            var criteria = this.repsalesCriteria(objMappable);
            var criteriaTotalByInterval = this.repsalesCriteria(objMappable);
            var criteriaTotalByDNo = this.repsalesCriteria(objMappable);
            var criteriaTime = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesCriteria();

            GetCriteria(ref criteria, ref criteriaTotalByInterval, ref criteriaTotalByDNo, ref criteriaTime);

            objMappable.timeinterval = ErsCommon.ConvertEntityListToDictionaryList(repository.FindInterval(criteriaTime));
            objMappable.repsalesList = ErsCommon.ConvertEntityListToDictionaryList(repository.FindTemp(criteria));
            objMappable.repsalesListByInterval = ErsCommon.ConvertEntityListToDictionaryList(repository.FindTempTotalByInterval(criteriaTotalByInterval));
            objMappable.repsalesListByDNo = ErsCommon.ConvertEntityListToDictionaryList(repository.FindTempTotalByDNo(criteriaTotalByDNo));

            objMappable.colcount = objMappable.repsalesList.Count;

            if (objMappable.repsalesList.Count > 0)
            {
                objMappable.repsalesHasRecord = true;
            }
            else
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
            }
        }

        private ErsCtsRepSalesCriteria repsalesCriteria(IRepSalesMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesCriteria();
            criteria.scale_code = EnumScaleCode.byhalfhour.ToString();

            criteria.datefrom = objMappable.datefrom;
            criteria.dateto = objMappable.dateto;

            objMappable.scodes = new string[5];
            if (!string.IsNullOrEmpty(objMappable.scode1))
                objMappable.scodes[0] = objMappable.scode1;
            if (!string.IsNullOrEmpty(objMappable.scode2))
                objMappable.scodes[1] = objMappable.scode2;
            if (!string.IsNullOrEmpty(objMappable.scode3))
                objMappable.scodes[2] = objMappable.scode3;
            if (!string.IsNullOrEmpty(objMappable.scode4))
                objMappable.scodes[3] = objMappable.scode4;
            if (!string.IsNullOrEmpty(objMappable.scode5))
                objMappable.scodes[4] = objMappable.scode5;

            if (!string.IsNullOrEmpty(objMappable.scode1) || !string.IsNullOrEmpty(objMappable.scode2) || !string.IsNullOrEmpty(objMappable.scode3)
                   || !string.IsNullOrEmpty(objMappable.scode4) || !string.IsNullOrEmpty(objMappable.scode5))
                criteria.scodes = objMappable.scodes;

            return criteria;
        }

        private void GetCriteria(ref ErsCtsRepSalesCriteria criteria, ref ErsCtsRepSalesCriteria criteriaTotalByInterval,
            ref ErsCtsRepSalesCriteria criteriaTotalByDNo, ref ErsCtsRepSalesCriteria criteriaTime)
        {
            criteria.AddGroupBy("byhalfhour");
            criteria.AddGroupBy("scode");
            criteria.AddOrderBy("scode", ErsCtsRepSalesCriteria.OrderBy.ORDER_BY_ASC);
            criteria.AddOrderBy("byhalfhour", ErsCtsRepSalesCriteria.OrderBy.ORDER_BY_ASC);

            criteriaTotalByInterval.AddGroupBy("byhalfhour");
            criteriaTotalByInterval.AddOrderBy("byhalfhour", ErsCtsRepSalesCriteria.OrderBy.ORDER_BY_ASC);

            criteriaTotalByDNo.AddGroupBy("scode");
            criteriaTotalByDNo.AddOrderBy("scode", ErsCtsRepSalesCriteria.OrderBy.ORDER_BY_ASC);

            criteriaTime.AddOrderBy("byhalfhour", ErsCtsRepSalesCriteria.OrderBy.ORDER_BY_ASC);
        }
    }
}