﻿using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using Models.reports.csv;
using System;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappers
{
    public class CategoryCsvMapper
        : IMapper<ICategoryCsvMappable>
    {
        public void Map(ICategoryCsvMappable objMappable)
        {
            this.CreateCsvFile(objMappable);

            objMappable.repcategory = true;
            objMappable.repcategoryList = true;
        }

        public virtual void CreateCsvFile(ICategoryCsvMappable objMappable)
        {
            var criteria = this.GetListCriteria(objMappable);
            criteria.AddOrderBy("cts_enquiry_t.intime", ErsCtsRepCategoryCriteria.OrderBy.ORDER_BY_ASC);

            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryStgy();
            var list = repository.FindList(criteria);
            var filename = "TabulationbyCategory" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";

            contactlogs_csv category_csv;
            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            objMappable.csvCreater = csvCreater;
            using (var writer = csvCreater.GetWriter(filename))
            {
                csvCreater.WriteCsvHeader<contactlogs_csv>(writer);
                foreach (var item in list)
                {
                    category_csv = new contactlogs_csv();
                    
                    category_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                    category_csv.inquiry = !String.IsNullOrEmpty(item.inquiry) ? item.inquiry.Replace("\\r\\n", Environment.NewLine).Replace("\\n", Environment.NewLine) : null;
                    category_csv.csv_CaseNo = item.case_no.ToString().Trim();
                    category_csv.csv_ReceptionDate = item.intime;
                    category_csv.ClientsName = item.lname + " " + item.fname;
                    category_csv.Status = item.status;
                    //category_csv.enq_progress = GetStatusCode((int?)item.enq_progress);
                    category_csv.w_status = item.w_status;
                    category_csv.Subject = item.enq_casename;
                    category_csv.PersonInCharge = item.ag_name;
                    category_csv.csv_pref = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId((int)item.pref, ErsFactory.ersUtilityFactory.getSetup().site_id);
                    if(item.w_situation.HasValue())
                    {
                        category_csv.w_situation = ErsResources.GetMessage(item.w_situation, null);
                    }
                    if (item.w_status.HasValue())
                    {
                        category_csv.w_status = ErsResources.GetMessage(item.w_status, null);
                    }

                    csvCreater.WriteBody(category_csv, writer);
                }
            }
        }

        /// <summary>
        /// Get Enquiry Status Code
        /// </summary>
        /// <param name="stscode"></param>
        /// <returns></returns>
        public string GetStatusCode(int? stscode)
        {
            string stsVal = "";
            switch (stscode)
            {
                case null:
                    stsVal = "";
                    break;
                case 0:
                    stsVal = "";
                    break;
                case 1:
                    stsVal = ErsResources.GetMessage("namecode.ENQPGR1");
                    break;
                case 2:
                    stsVal = ErsResources.GetMessage("namecode.ENQPGR2");
                    break;
            }

            return stsVal;
        }

        private ErsCtsRepCategoryCriteria GetListCriteria(ICategoryCsvMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryCriteria();
            switch (objMappable.p_code)
            {
                case 1:
                    criteria.ct1code = objMappable.c_code;
                    break;
                case 2:
                    criteria.ct2code = objMappable.c_code;
                    break;
                case 3:
                    criteria.ct3code = objMappable.c_code;
                    break;
                case 4:
                    criteria.ct4code = objMappable.c_code;
                    break;
                case 5:
                    criteria.ct5code = objMappable.c_code;
                    break;
            }

            if (objMappable.typcode != null)
                criteria.typcode = objMappable.typcode;
            if (objMappable.prycode != null)
                criteria.prycode = objMappable.prycode;
            if (objMappable.sitcode != null)
                criteria.sitcode = objMappable.sitcode;
            if (objMappable.stscode != null)
                criteria.stscode = objMappable.stscode;
            if (objMappable.pgrcode != null)
                criteria.pgrcode = objMappable.pgrcode;
            if (objMappable.datefrom != null)
                criteria.datefrom = objMappable.datefrom;
            if (objMappable.dateto != null)
                criteria.dateto = objMappable.dateto;

            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            return criteria;
        }
    }
}