﻿using System.Collections.Generic;
using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappers
{
    public class RepOrderMapper
        : IMapper<IRepOrderMappable>
    {
        public void Map(IRepOrderMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderStgy();
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var criteria = this.reporderCriteria(objMappable);
            criteria.site_id = setup.site_id;
            criteria.AddGroupBy("agent");
            criteria.AddGroupBy("ag_type");
            criteria.AddOrderBy("ag_type", ErsCtsRepOrderCriteria.OrderBy.ORDER_BY_ASC);

            var criteriaTotal = this.reporderCriteria(objMappable);
            criteriaTotal.site_id = setup.site_id;

            var criteriaTmp = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderCriteria();
            if (!string.IsNullOrEmpty(objMappable.agentid))
                criteriaTmp.user_id = objMappable.agentid;
            criteriaTmp.AddOrderBy("cts_agetype_t.id", ErsCtsRepOrderCriteria.OrderBy.ORDER_BY_ASC);

            var tmp = repository.FindList(criteriaTmp);
            var list = repository.Find(criteria);
            var tmplist = repository.FindTemp(criteria);
            var totlist = repository.FindTotal(criteriaTotal);
            var tottmplist = repository.FindTempTotal(criteriaTotal);

            List<ErsCtsRepOrder> total = new List<ErsCtsRepOrder>();

            if (list.Count != 0 || tmplist.Count != 0)
            {
                for (int i = 0; i < tmp.Count; i++)
                {
                    ErsCtsRepOrder item = new ErsCtsRepOrder();
                    item.agent = tmp[i].agent;
                    item.ag_type = tmp[i].ag_type;
                    item.amount = 0;
                    item.temp_amount = 0;
                    item.total_amount = 0;
                    item.total_price = 0;
                    if (objMappable.ag_type.HasValue)
                    {
                        if (objMappable.ag_type == tmp[i].ag_type)
                            total.Add(item);
                    }
                    else
                    {
                        total.Add(item);
                    }
                }

                foreach (var i in total)
                {
                    foreach (var l in list)
                        if (l.ag_type == i.ag_type)
                        {
                            i.amount = l.amount;
                            i.total_amount += l.amount;
                            i.total_price += l.price;
                        }

                    foreach (var m in tmplist)
                        if (m.ag_type == i.ag_type)
                        {
                            i.temp_amount = m.temp_amount;
                            i.total_amount += m.temp_amount;
                            i.total_price += m.temp_price;
                        }
                }

                if (total.Count != 0)
                {
                    ErsCtsRepOrder last = new ErsCtsRepOrder();
                    last.agent = ErsResources.GetMessage("cts_repOrder_agent");
                    last.ag_type = null;
                    last.amount = totlist[0].amount;
                    last.temp_amount = tottmplist[0].temp_amount;
                    last.total_amount = totlist[0].amount + tottmplist[0].temp_amount;
                    last.total_price = totlist[0].price + tottmplist[0].temp_price;
                    total.Add(last);
                }
            }

            objMappable.reporderList = ErsCommon.ConvertEntityListToDictionaryList(total);

            objMappable.reporder = true;

            if (objMappable.reporderList.Count > 0)
            {
                objMappable.reporderHasRecord = true;
            }
            else
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
            }
        }

        private ErsCtsRepOrderCriteria reporderCriteria(IRepOrderMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderCriteria();
            this.repOrderLoad(criteria, objMappable);
            if (objMappable.ag_type.HasValue)
                criteria.ag_type = objMappable.ag_type;

            if (!string.IsNullOrEmpty(objMappable.agentid))
                criteria.agentid = objMappable.agentid;

            return criteria;
        }

        private ErsCtsRepOrderCriteria repOrderLoad(ErsCtsRepOrderCriteria criteria, IRepOrderMappable objMappable)
        {
            criteria.datefrom = objMappable.datefrom;
            criteria.dateto = objMappable.dateto;

            return criteria;
        }
    }
}