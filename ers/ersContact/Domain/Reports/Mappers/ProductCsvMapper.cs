﻿using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using System.Collections.Generic;
using System;
using Models.reports.csv;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappers
{
    public class ProductCsvMapper
        : IMapper<IProductCsvMappable>
    {
        public void Map(IProductCsvMappable objMappable)
        {
            this.CreateCsvFile(objMappable);

            objMappable.repprod = true;
        }

        private ErsCtsRepProdCriteria repprodCriteria(IProductCsvMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepProdCriteria();

            if (objMappable.datefrom != null)
                criteria.datefrom = objMappable.datefrom;

            if (objMappable.dateto != null)
                criteria.dateto = objMappable.dateto;

            if (!string.IsNullOrEmpty(objMappable.prodcode))
                criteria.prodcodeLikePrefix = objMappable.prodcode;

            if (!string.IsNullOrEmpty(objMappable.prodname))
                criteria.prodnameLikePrefix = objMappable.prodname;

            if (!string.IsNullOrEmpty(objMappable.agentid))
                criteria.agentidLikePrefix = objMappable.agentid;

            if (objMappable.ag_type.HasValue)
                criteria.ag_type = objMappable.ag_type;

            return criteria;
        }

        public virtual void CreateCsvFile(IProductCsvMappable objMappable)
        {
            var criteria = this.repprodCriteria(objMappable);

            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepProdStgy();

            criteria.param_datefrom = objMappable.datefrom;
            criteria.param_dateto = objMappable.dateto;
            criteria.param_prodcode = objMappable.prodcode;
            criteria.param_prodname = objMappable.prodname;
            criteria.param_agentid = objMappable.agentid;
            criteria.param_ag_type = objMappable.ag_type;
            criteria.param_TargetOrder = objMappable.TargetOrder;
            criteria.param_site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
            var list = repository.FindList(criteria);

            var filename = "PromptReportOfProductTabulation" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";

            prod_csv prod_csv;
            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            objMappable.csvCreater = csvCreater;
            using (var writer = csvCreater.GetWriter(filename))
            {
                csvCreater.WriteCsvHeader<prod_csv>(writer);
                foreach (var item in list)
                {
                    prod_csv = new prod_csv();
                    prod_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                    prod_csv.ProductName = item.sname;
                    prod_csv.ProductGroup = item.gcode;
                    prod_csv.ProductCode = item.scode;
                    prod_csv.UnitPrice = item.price;
                    prod_csv.TemporaryOrder = item.tempord;
                    prod_csv.CTS = item.cts;
                    prod_csv.PC = item.pc;
                    prod_csv.MB = item.mb;
                    prod_csv.TotalNumber = item.totnum;
                    prod_csv.TotalAmount = item.totamt;
                    prod_csv.NumberAvailableForSale = item.stock;
                    prod_csv.Tel = item.tel;
                    prod_csv.Letter = item.let;
                    prod_csv.Fax = item.fax;

                    csvCreater.WriteBody(prod_csv, writer);
                }
            }

        }
    }
}