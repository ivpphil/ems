﻿using System;
using System.Collections.Generic;
using System.Linq;
using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Reports.Mappers
{
    public class RepAgeMapper
        : IMapper<IRepAgeMappable>
    {
        public void Map(IRepAgeMappable objMappable)
        {
            var setPrefs = this.GetPrefWhere(objMappable);
            var scodesCondition = this.GetScodeCondition(objMappable);

            // Ratio
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepAgeStgy();

            var dateCondition = this.GetDateCondition(objMappable);
            var siteCondition = this.GetSiteIdCondition(objMappable);

            var where = dateCondition.Item1 + " " + scodesCondition.Item1 + " " + siteCondition.Item1 + " " + setPrefs;
            var tmpWhere = dateCondition.Item2 + " " + scodesCondition.Item2 + " " + siteCondition.Item2 + " " + setPrefs;

            var ratioList = repository.Find(null, where, tmpWhere);

            var entire_total = ratioList.Select((ctsAge) => ctsAge.total).Sum();
            ratioList.AsParallel().ForAll((ctsAge) => ctsAge.entire_total = entire_total);

            objMappable.repageList = ratioList;

            objMappable.billCount = repository.FindDList(null, where, tmpWhere);
        }

        private string GetPrefWhere(IRepAgeMappable objMappable)
        {
            string strRetVal = string.Empty;
            if (objMappable.pref1 != null || objMappable.pref2 != null || objMappable.pref3 != null
                || objMappable.pref4 != null || objMappable.pref5 != null)
            {
                strRetVal = "AND member_t.pref IN (";

                int[] prefs = this.GetPrefArray(objMappable);
                foreach (int s in prefs)
                {
                    if (s != 0)
                        strRetVal += s.ToString() + ",";
                }

                strRetVal = strRetVal.TrimEnd(',');
                strRetVal += ") ";
            }

            return strRetVal;
        }

        private int[] GetPrefArray(IRepAgeMappable objMappable)
        {
            int[] prefs = new int[5];

            if (objMappable.pref1 != null)
                prefs[0] = Convert.ToInt16(objMappable.pref1);
            if (objMappable.pref2 != null)
                prefs[1] = Convert.ToInt16(objMappable.pref2);
            if (objMappable.pref3 != null)
                prefs[2] = Convert.ToInt16(objMappable.pref3);
            if (objMappable.pref4 != null)
                prefs[3] = Convert.ToInt16(objMappable.pref4);
            if (objMappable.pref5 != null)
                prefs[4] = Convert.ToInt16(objMappable.pref5);
            return prefs;
        }

        private Tuple<string, string> GetScodeCondition(IRepAgeMappable objMappable)
        {
            string strRetVal = string.Empty;

            if (!string.IsNullOrEmpty(objMappable.scode1) || !string.IsNullOrEmpty(objMappable.scode2) || !string.IsNullOrEmpty(objMappable.scode3)
                   || !string.IsNullOrEmpty(objMappable.scode4) || !string.IsNullOrEmpty(objMappable.scode5))
            {
                strRetVal = " (";

                string[] scodes = this.GetScodeArray(objMappable);
                foreach (string s in scodes)
                {
                    if (!string.IsNullOrEmpty(s))
                        strRetVal += "'" + s + "',";
                }

                strRetVal = strRetVal.TrimEnd(',');
                strRetVal += ") ";
            }

            if (string.IsNullOrEmpty(strRetVal))
            {
                return Tuple.Create(string.Empty, string.Empty);
            }

            return Tuple.Create("AND ds_master_t.scode IN " + strRetVal, "AND bask_t.scode IN" + strRetVal);
        }

        private string[] GetScodeArray(IRepAgeMappable objMappable)
        {
            string[] scodes = new string[5];
            if (!string.IsNullOrEmpty(objMappable.scode1))
                scodes[0] = objMappable.scode1;
            if (!string.IsNullOrEmpty(objMappable.scode2))
                scodes[1] = objMappable.scode2;
            if (!string.IsNullOrEmpty(objMappable.scode3))
                scodes[2] = objMappable.scode3;
            if (!string.IsNullOrEmpty(objMappable.scode4))
                scodes[3] = objMappable.scode4;
            if (!string.IsNullOrEmpty(objMappable.scode5))
                scodes[4] = objMappable.scode5;
            return scodes;
        }

        private Tuple<string, string> GetDateCondition(IRepAgeMappable objMappable)
        {
            var setDate = string.Empty;
            var setTempDate = string.Empty;

            setDate += " AND d_master_t.intime >= '" + objMappable.datefrom.Value.ToString() + "' ";
            setTempDate += " AND cts_order_t.intime >= '" + objMappable.datefrom.Value.ToString() + "' ";

            setDate += " AND d_master_t.intime <= '" + objMappable.dateto.Value.ToString() + "' ";
            setTempDate += " AND cts_order_t.intime <= '" + objMappable.dateto.Value.ToString() + "' ";

            return Tuple.Create(setDate, setTempDate);
        }

        private Tuple<string, string> GetSiteIdCondition(IRepAgeMappable objMappable)
        {
            var site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            var setSiteId = " AND (d_master_t.site_id = " + site_id + " OR d_master_t.site_id = " + (int)EnumSiteId.COMMON_SITE_ID + ") ";
            var setTempSiteID = " AND (cts_order_t.site_id = " + site_id + " OR cts_order_t.site_id = " + (int)EnumSiteId.COMMON_SITE_ID + ") ";

            return Tuple.Create(setSiteId, setTempSiteID);
        }
    }
}