﻿using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappers
{
    public class RepContactLogMapper
        : IMapper<IRepContactLogMappable>
    {
        public void Map(IRepContactLogMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogStgy();
            var criteria = this.repcontactlogCriteria(objMappable);

            objMappable.recordCount = repository.GetRecordCount(criteria);
            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;
            }
            objMappable.pagerPageCount = pagerPageCount;
            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            criteria.AddOrderBy("cts_enquiry_t.intime", ErsCtsRepContactLogCriteria.OrderBy.ORDER_BY_ASC);
            criteria.AddOrderBy("cts_enquiry_t.case_no", ErsCtsRepContactLogCriteria.OrderBy.ORDER_BY_ASC);
            var list = repository.Find(criteria);
            objMappable.repcontactlogList = ErsCommon.ConvertEntityListToDictionaryList(list);

            if (objMappable.repcontactlogList.Count > 0)
            {
                objMappable.repcontactHasRecord = true;
            }
            else
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
            }
        }

        private ErsCtsRepContactLogCriteria repcontactlogCriteria(IRepContactLogMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogCriteria();

            if (objMappable.typcode != null)
                criteria.typcode = objMappable.typcode;
            if (objMappable.prycode != null)
                criteria.prycode = objMappable.prycode;
            if (objMappable.stscode != null)
                criteria.stscode = objMappable.stscode;
            if (objMappable.pgrcode != null)
                criteria.pgrcode = objMappable.pgrcode;
            if (objMappable.sitcode != null)
                criteria.sitcode = objMappable.sitcode;
            if (objMappable.ct1code != null)
                criteria.ct1code = objMappable.ct1code;
            if (objMappable.ct2code != null)
                criteria.ct2code = objMappable.ct2code;
            if (objMappable.ct3code != null)
                criteria.ct3code = objMappable.ct3code;
            if (objMappable.ct4code != null)
                criteria.ct4code = objMappable.ct4code;
            if (objMappable.ct5code != null)
                criteria.ct5code = objMappable.ct5code;

            if (!string.IsNullOrEmpty(objMappable.enq_casename))
                criteria.enq_casename = objMappable.enq_casename;

            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;
 
            this.repContactLoad(criteria, objMappable);

            return criteria;
        }

        private ErsCtsRepContactLogCriteria repContactLoad(ErsCtsRepContactLogCriteria criteria, IRepContactLogMappable objMappable)
        {
            criteria.datefrom = objMappable.datefrom;
            criteria.dateto = objMappable.dateto.Value.AddDays(1).AddMilliseconds(-1);

            return criteria;
        }
    }
}