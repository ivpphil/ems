﻿using System.Collections.Generic;
using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappers
{
    public class RepCategoryDetailsMapper
        : IMapper<IRepCategoryDetailsMappable>
    {
        public void Map(IRepCategoryDetailsMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryStgy();
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryCriteria();

            criteria.case_no = objMappable.case_no;

            var list = repository.FindDetail(criteria);
            var listDetail = new List<Dictionary<string, object>>();
            foreach(var record in list)
            {
                var detail = record.GetPropertiesAsDictionary();
                detail["w_progress"] = !string.IsNullOrEmpty(record.w_progress) ? ErsResources.GetMessage(record.w_progress) : null;
                detail["w_status"] = !string.IsNullOrEmpty(record.w_status) ? ErsResources.GetMessage(record.w_status) : null;
                detail["w_situation"] = !string.IsNullOrEmpty(record.w_situation) ? ErsResources.GetMessage(record.w_situation) : null;
                detail["w_type"] = !string.IsNullOrEmpty(record.w_type) ? ErsResources.GetMessage(record.w_type) : null;
                detail["w_priority"] = !string.IsNullOrEmpty(record.w_priority) ? ErsResources.GetMessage(record.w_priority) : null;

                listDetail.Add(detail);
            }
            objMappable.repcategoryDetail = listDetail;

            List <ErsCtsRepCategory> first = new List<ErsCtsRepCategory>();
            var cri = ErsFactory.ersCtsRepFactory.GetErsCtsRepInboundCriteria();
            cri.case_num = objMappable.case_no;
            cri.AddOrderBy("cts_enquiry_detail_t.sub_no", Criteria.OrderBy.ORDER_BY_DESC);
            var bill = repository.FindBillDetail(cri);

            if (bill.Count != 0)
            {
                ErsCtsRepCategory item = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategory();
                item.memo = bill[0].memo;
                item.enq_detail = bill[0].enq_detail;
                item.ans_detail = bill[0].ans_detail;
                item.email_body = bill[0].email_body;
                item.email_fotter = bill[0].email_fotter;
                item.email_header = bill[0].email_header;

                first.Add(item);
            }

            objMappable.repcategoryBillDetail = ErsCommon.ConvertEntityListToDictionaryList(first);

            objMappable.repcategory = true;
            objMappable.repcategoryList = true;
            objMappable.repcategoryDetails = true;
        }
    }
}