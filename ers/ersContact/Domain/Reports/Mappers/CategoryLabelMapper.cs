﻿using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersContact.Domain.Reports.Mappers
{
    public class CategoryLabelMapper
        : IMapper<ICategoryLabelMappable>
    {
        public void Map(ICategoryLabelMappable objMappable)
        {
            var list = ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT_NAME, EnumCtsEnquiryCategoryColumnName.namename,false);

            foreach (var dr in list)
            {
                switch ((int)dr["value"])
                {
                    case 1:
                        objMappable.catlabel1 = (string)dr["name"];
                        break;
                    case 2:
                        objMappable.catlabel2 = (string)dr["name"];
                        break;
                    case 3:
                        objMappable.catlabel3 = (string)dr["name"];
                        break;
                    case 4:
                        objMappable.catlabel4 = (string)dr["name"];
                        break;
                    case 5:
                        objMappable.catlabel5 = (string)dr["name"];
                        break;
                }
            }
        }
    }
}