﻿using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using System;
using Models.reports.csv;
using jp.co.ivp.ers;
using System.IO;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Reports.Mappers
{
    public class ContactLogsCsvMapper
        : IMapper<IContactLogsCsvMappable>
    {
        public void Map(IContactLogsCsvMappable objMappable)
        {
            this.CreateCsvFile(objMappable);

            objMappable.repcontact = true;
        }

        public virtual void CreateCsvFile(IContactLogsCsvMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogCriteria();
            criteria = this.repcontactlogCriteria(objMappable);
            criteria.AddOrderBy("cts_enquiry_t.intime", ErsCtsRepContactLogCriteria.OrderBy.ORDER_BY_ASC);
            criteria.AddOrderBy("cts_enquiry_t.case_no", ErsCtsRepContactLogCriteria.OrderBy.ORDER_BY_ASC);

            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogStgy();
            var list = repository.GetCSVContactLog(criteria);
            var filename = "ContactLogsTabulation" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";

            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            objMappable.csvCreater = csvCreater;
            using (var writer = csvCreater.GetWriter(filename))
            {
                csvCreater.WriteCsvHeader<contactlogcsv>(writer);
                foreach (var item in list)
                {
                    this.writeCSV(item, csvCreater, writer);
                }
            }
        }

        private void writeCSV(ErsCtsRepContactLog item, ErsCsvCreater csvCreater, StreamWriter writer)
        {
            var log = new contactlogcsv();

            log.csv_ReceptionDate = (!string.IsNullOrEmpty(item.intime.ToString())) ? item.intime.ToString() : "";

            log.csv_CaseNo = item.case_no.ToString().Trim();
            log.csv_PersonName = (!string.IsNullOrEmpty(item.personname)) ? item.personname : "";
            log.csv_CustomerName = (!string.IsNullOrEmpty(item.lnamek) && !string.IsNullOrEmpty(item.fnamek)) ? item.lnamek + " " + item.fnamek : "";
            log.csv_cate2 = (!string.IsNullOrEmpty(item.cat2)) ? item.cat2 : "";
            log.csv_cate3 = (!string.IsNullOrEmpty(item.cat3)) ? item.cat3 : "";
            log.csv_sex = (!string.IsNullOrEmpty(item.gender)) ? item.gender : "";
            log.enq_progress = GetSituationCode(item.enq_situation.Value);
            log.Status = GetStatusCode(item.enq_progress.Value);
            log.enq_casename = (!string.IsNullOrEmpty(item.prodidcode)) ? item.prodidcode : "";
            log.csv_content = (!string.IsNullOrEmpty(item.content)) ? item.content : "";
            log.csv_birth = (!string.IsNullOrEmpty(item.birth)) ? item.birth : "";
            if (!string.IsNullOrEmpty(log.csv_birth))
            {
                log.csv_age = Convert.ToString(this.Getage(Convert.ToDateTime(log.csv_birth))) + "歳";
            }

            string initial = this.MakeInitial(item.lnamek, item.fnamek);
            //log.csv_initial = (!string.IsNullOrEmpty(initial)) ? initial : "";
            log.csv_pref = (!string.IsNullOrEmpty(item.pref_name)) ? item.pref_name : "X.X.";
            log.csv_cate1 = (!string.IsNullOrEmpty(item.cat1)) ? item.cat1 : "";
            
            log.csv_escalation = (!string.IsNullOrEmpty(item.escalation)) ? item.escalation : "";
            log.csv_content = (!string.IsNullOrEmpty(item.content)) ? item.content : "";
            //log.csv_informationdate = (!string.IsNullOrEmpty(item.utime.ToString())) ? item.utime.ToString() : "";
            //log.csv_reportingdate = (!string.IsNullOrEmpty(item.intime.ToString())) ? item.intime.ToString() : "";
            csvCreater.WriteBody(log, writer);
        }

        /// <summary>
        /// Get Enquiry Situation Code
        /// </summary>
        /// <param name="sitcode"></param>
        /// <returns></returns>
        public string GetSituationCode(int sitcode)
        {
            string sitVal = "";
            switch(sitcode)
            {
                case 0:
                    sitVal = "";
                    break;
                case 1:
                    sitVal = ErsResources.GetMessage("namecode.ENQSIT1");
                    break;
                case 2:
                    sitVal = ErsResources.GetMessage("namecode.ENQSIT2");
                    break;
                case 3:
                    sitVal = ErsResources.GetMessage("namecode.ENQSIT3");
                    break;
                case 4:
                    sitVal = ErsResources.GetMessage("namecode.ENQSIT4");
                    break;
            }

            return sitVal;
        }

        /// <summary>
        /// Get Enquiry Status Code
        /// </summary>
        /// <param name="stscode"></param>
        /// <returns></returns>
        public string GetStatusCode(int stscode)
        {
            string stsVal = "";
            switch(stscode)
            {
                case 0:
                    stsVal = "";
                    break;
                case 1:
                    stsVal = ErsResources.GetMessage("namecode.ENQPGR1");
                    break;
                case 2:
                    stsVal = ErsResources.GetMessage("namecode.ENQPGR2");
                    break;
            }

            return stsVal;
        }


        /// <summary>
        /// イニシャル作成
        /// </summary>
        /// <param name="lnamek"></param>
        /// <param name="fnamek"></param>
        private string MakeInitial(string lnamek, string fnamek)
        {
            string initial = "";
            if (lnamek != null && fnamek != null)
            {

                //匿名希望対応
                if (lnamek == ErsResources.GetFieldName("anonymity") && fnamek == ErsResources.GetFieldName("esperance"))
                {
                    initial = ErsResources.GetFieldName("a_initial");
                }
                else
                {
                    initial = ErsCommon.MakeInitial(lnamek, fnamek);
                }
            }

            return initial;
        }

        /// <summary>
        /// 年齢取得
        /// </summary>
        public virtual int? Getage(DateTime birth)
        {
            int? tmp_age = null;

            if (birth != null)
            {
                DateTime dt1 = DateTime.Today; //基準日
                DateTime dt2 = Convert.ToDateTime(birth); //誕生日
                long d1 = Convert.ToInt64(dt1.ToString("yyyyMMdd")); //基準日を半角数値に変換
                long d2 = Convert.ToInt64(dt2.ToString("yyyyMMdd")); //誕生日を半角数値に変換
                tmp_age = (int)Math.Floor((double)((d1 - d2) / 10000));

            }
            return tmp_age;
        }

        private ErsCtsRepContactLogCriteria repcontactlogCriteria(IContactLogsCsvMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepContactLogCriteria();

            if (objMappable.typcode != null)
                criteria.typcode = objMappable.typcode;
            if (objMappable.prycode != null)
                criteria.prycode = objMappable.prycode;
            if (objMappable.stscode != null)
                criteria.stscode = objMappable.stscode;
            if (objMappable.pgrcode != null)
                criteria.pgrcode = objMappable.pgrcode;
            if (objMappable.sitcode != null)
                criteria.sitcode = objMappable.sitcode;
            if (objMappable.ct1code != null)
                criteria.ct1code = objMappable.ct1code;
            if (objMappable.ct2code != null)
                criteria.ct2code = objMappable.ct2code;
            if (objMappable.ct3code != null)
                criteria.ct3code = objMappable.ct3code;
            if (objMappable.ct4code != null)
                criteria.ct4code = objMappable.ct4code;
            if (objMappable.ct5code != null)
                criteria.ct5code = objMappable.ct5code;
            if (!string.IsNullOrEmpty(objMappable.enq_casename))
                criteria.enq_casename = objMappable.enq_casename;

            this.repContactLoad(criteria, objMappable);

            return criteria;
        }

        private ErsCtsRepContactLogCriteria repContactLoad(ErsCtsRepContactLogCriteria criteria, IContactLogsCsvMappable objMappable)
        {
            criteria.datefrom = objMappable.datefrom;
            criteria.dateto = objMappable.dateto.Value.AddDays(1).AddMilliseconds(-1);

            return criteria;
        }
    }
}