﻿using System.Collections.Generic;
using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappers
{
    public class RepProdMapper
        : IMapper<IRepProdMappable>
    {
        public void Map(IRepProdMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepProdStgy();
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepProdCriteria();
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            criteria.param_datefrom = objMappable.datefrom;
            criteria.param_dateto = objMappable.dateto;
            criteria.param_prodcode = objMappable.prodcode;
            criteria.param_prodname = objMappable.prodname;
            criteria.param_agentid = objMappable.agentid;
            criteria.param_ag_type = objMappable.ag_type;
            criteria.param_TargetOrder = objMappable.TargetOrder;
            criteria.param_site_id = setup.site_id;
            var list = repository.FindList(criteria);

            objMappable.repprodList = ErsCommon.ConvertEntityListToDictionaryList(list);

            if (objMappable.repprodList.Count == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
            }

            foreach (var list_line in list)
            {
                objMappable.total_number += list_line.totnum;
                objMappable.total_amount += list_line.totamt;
            }
        }
    }
}