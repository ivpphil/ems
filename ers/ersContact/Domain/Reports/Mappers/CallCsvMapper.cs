﻿using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using System.Collections.Generic;
using Models.reports.csv;
using System;

namespace ersContact.Domain.Reports.Mappers
{
    public class CallCsvMapper
        : IMapper<ICallCsvMappable>
    {
        public void Map(ICallCsvMappable objMappable)
        {
            this.CreateCsvFile(objMappable);
            objMappable.repcall = true;
        }

        private ErsCtsRepCallCriteria repcallCriteriaTotalUserTime(ICallCsvMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallCriteria();

            if (objMappable.TargetByDay)
                criteria.scale_code = EnumScaleCode.byday.ToString();
            if (objMappable.TargetByMonth)
                criteria.scale_code = EnumScaleCode.bymonth.ToString();
            if (objMappable.TargetByTime)
                criteria.scale_code = EnumScaleCode.byhalfhour.ToString();

            criteria = this.repCallLoad(criteria, objMappable);
            criteria.AddGroupBy("user_id");
            criteria.AddGroupBy("byhalfhour");
            criteria.AddOrderBy("user_id", Criteria.OrderBy.ORDER_BY_ASC);
            criteria.AddOrderBy("byhalfhour", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepCallCriteria repcallCriteria(ICallCsvMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallCriteria();

            if (objMappable.TargetByDay)
                criteria.scale_code = EnumScaleCode.byday.ToString();
            if (objMappable.TargetByMonth)
                criteria.scale_code = EnumScaleCode.bymonth.ToString();
            if (objMappable.TargetByTime)
                criteria.scale_code = EnumScaleCode.byhalfhour.ToString();

            criteria = this.repCallLoad(criteria, objMappable);
            criteria.AddGroupBy("user_id");
            criteria.AddGroupBy("ag_name");
            criteria.AddOrderBy("user_id", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepCallCriteria repcallCriteriaTotalTime(ICallCsvMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallCriteria();

            if (objMappable.TargetByDay)
                criteria.scale_code = EnumScaleCode.byday.ToString();
            if (objMappable.TargetByMonth)
                criteria.scale_code = EnumScaleCode.bymonth.ToString();
            if (objMappable.TargetByTime)
                criteria.scale_code = EnumScaleCode.byhalfhour.ToString();

            criteria = this.repCallLoad(criteria, objMappable);
            criteria.AddGroupBy("byhalfhour");
            criteria.AddOrderBy("byhalfhour", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepCallCriteria repcallCriteriaInterval(ICallCsvMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallCriteria();
            if (objMappable.TargetByMonth)
                criteria.scale_code = EnumScaleCode.bymonth.ToString();
            if (objMappable.TargetByTime)
                criteria.scale_code = EnumScaleCode.byhalfhour.ToString();
            if (objMappable.TargetByDay)
            {
                criteria.scale_code = EnumScaleCode.byday.ToString();

                if (objMappable.datefrom != null && objMappable.dateto != null)
                {
                    if (objMappable.datefrom.Value.Month == objMappable.dateto.Value.Month && objMappable.datefrom.Value.Year == objMappable.dateto.Value.Year)
                    {
                        if (objMappable.datefrom.Value.Month == 2 && objMappable.datefrom.Value.Year % 4 == 0)
                        {
                            criteria.days = "30日";
                            criteria.days = "31日";
                        }
                        else if (objMappable.datefrom.Value.Month == 2 && objMappable.datefrom.Value.Year % 4 != 0)
                        {
                            criteria.days = "29日";
                            criteria.days = "30日";
                            criteria.days = "31日";
                        }
                        else if (objMappable.datefrom.Value.Month == 4 || objMappable.datefrom.Value.Month == 6 ||
                            objMappable.datefrom.Value.Month == 9 || objMappable.datefrom.Value.Month == 11)
                        {
                            criteria.days = "31日";
                        }

                    }
                }
            }
            criteria.AddOrderBy("byhalfhour", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepCallCriteria repCallLoad(ErsCtsRepCallCriteria criteria, ICallCsvMappable objMappable)
        {
            criteria.datefrom = objMappable.datefrom;
            criteria.dateto = objMappable.dateto;

            //AG TYPE
            if (objMappable.ag_type.HasValue)
            {
                criteria.ag_type = objMappable.ag_type;
            }

            return criteria;
        }


        public void CreateCsvFile(ICallCsvMappable objMappable)
        {
            var criteria = this.repcallCriteriaTotalUserTime(objMappable);
            var criteriaTotalByUser = this.repcallCriteria(objMappable);
            var criteriaTotalByTime = this.repcallCriteriaTotalTime(objMappable);
            var criteriaInterval = this.repcallCriteriaInterval(objMappable);

            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallStgy();

            var listTimeInterval = repository.FindInterval(criteriaInterval);

            IList<ErsCtsRepCall> listMain;
            IList<ErsCtsRepCall> listByInterval;
            IList<ErsCtsRepCall> listByUser;

            if (objMappable.TargetRegOrder)
            {
                listMain = repository.FindTotalUserTime(criteria);
                listByInterval = repository.FindTotalTime(criteriaTotalByTime);
                listByUser = repository.Find(criteriaTotalByUser);

                if (objMappable.TargetByDay)
                {
                    listMain = repository.FindTotalUserTimeDay(criteria);
                    listByInterval = repository.FindTotalTimeDay(criteriaTotalByTime);
                    listByUser = repository.FindDay(criteriaTotalByUser);
                }

                if (objMappable.TargetByMonth)
                {
                    listMain = repository.FindTotalUserTimeMonth(criteria);
                    listByInterval = repository.FindTotalTimeMonth(criteriaTotalByTime);
                    listByUser = repository.FindMonth(criteriaTotalByUser);
                }
            }
            else
            {
                listMain = repository.FindTempTotalUserTime(criteria);
                listByInterval = repository.FindTempTotalTime(criteriaTotalByTime);
                listByUser = repository.FindTempTotalUser(criteriaTotalByUser);

                if (objMappable.TargetByDay)
                {
                    listMain = repository.FindTempTotalUserTimeDay(criteria);
                    listByInterval = repository.FindTempTotalTimeDay(criteriaTotalByTime);
                    listByUser = repository.FindTempTotalUserDay(criteriaTotalByUser);
                }

                if (objMappable.TargetByMonth)
                {
                    listMain = repository.FindTempTotalUserTimeMonth(criteria);
                    listByInterval = repository.FindTempTotalTimeMonth(criteriaTotalByTime);
                    listByUser = repository.FindTempTotalUserMonth(criteriaTotalByUser);
                }
            }

            var filename = "CallResultsTabulation" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";

            //call_csv call_csv;
            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            objMappable.csvCreater = csvCreater;
            using (var writer = csvCreater.GetWriter(filename))
            {
                //csvCreater.WriteCsvHeader<call_csv>(writer);
                var call_dic = new Dictionary<string, object>();
                if (objMappable.TargetByDay)
                {
                    call_dic["time_interval"] = ErsResources.GetMessage("sumry_by_day");
                }
                else if (objMappable.TargetByMonth)
                {
                    call_dic["time_interval"] = ErsResources.GetMessage("sumry_by_month");
                }
                else
                {
                    call_dic["time_interval"] = ErsResources.GetMessage("sumry_by_hour");
                }

                string[] user_id = new string[listByUser.Count];

                // Header
                for (int i = 0; i < listByUser.Count; i++)
                {
                    call_dic[listByUser[i].ag_name] = listByUser[i].ag_name;
                }

                call_dic["total"] = ErsResources.GetFieldName("total");

                csvCreater.WriteBody(call_dic, writer);

                int total_byInterval = 0;

                // Middle
                foreach (var item in listTimeInterval)
                {
                    call_dic = new Dictionary<string, object>();
                    call_dic["time_interval"] = item.byhalfhour;

                    for (int i = 0; i < listByUser.Count; i++)
                        foreach (var main in listMain)
                            if (main.user_id == listByUser[i].user_id)
                                if (main.byhalfhour == item.byhalfhour)
                                    user_id[i] = main.d_no_count.ToString();

                    for (int i = 0; i < listByUser.Count; i++)
                    {
                        call_dic[listByUser[i].user_id] = user_id[i];
                    }

                    foreach (var total in listByInterval)
                        if (item.byhalfhour == total.byhalfhour)
                        {
                            total_byInterval += total.d_no_count;
                            call_dic["total"]= total.d_no_count.ToString();
                        }

                    csvCreater.WriteBody(call_dic, writer);
                }

                //Footer
                call_dic = new Dictionary<string, object>();
                call_dic["time_interval"] = ErsResources.GetFieldName("total");

                for (int i = 0; i < listByUser.Count; i++)
                {
                    call_dic[listByUser[i].user_id] = listByUser[i].d_no_count.ToString();
                }

                call_dic["total"] = total_byInterval.ToString();
                csvCreater.WriteBody(call_dic, writer);
            }
        }
    }
}