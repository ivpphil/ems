﻿using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappers
{
    public class RepCategoryListMapper
        : IMapper<IRepCategoryListMappable>
    {
        private Setup setup { get; set; }
        public void Map(IRepCategoryListMappable objMappable)
        {
            this.setup = ErsFactory.ersUtilityFactory.getSetup();
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryStgy();
            var criteria = this.GetListCriteria(objMappable);

            objMappable.recordCount = repository.GetRecordCount(criteria);
            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            criteria.AddOrderBy("cts_enquiry_t.intime", ErsCtsRepCategoryCriteria.OrderBy.ORDER_BY_ASC);

            var list = repository.FindList(criteria,(setup.Multiple_sites == true) ? (int?)setup.site_id : null);

            objMappable.repcategoryListDetail = ErsCommon.ConvertEntityListToDictionaryList(list);
            objMappable.repcategory = true;
            objMappable.repcategoryList = true;
        }

        private ErsCtsRepCategoryCriteria GetListCriteria(IRepCategoryListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryCriteria();
            switch (objMappable.p_code)
            {
                case 1:
                    criteria.ct1code = objMappable.c_code;
                    break;
                case 2:
                    criteria.ct2code = objMappable.c_code;
                    break;
                case 3:
                    criteria.ct3code = objMappable.c_code;
                    break;
                case 4:
                    criteria.ct4code = objMappable.c_code;
                    break;
                case 5:
                    criteria.ct5code = objMappable.c_code;
                    break;
            }

            if (objMappable.typcode != null)
                criteria.typcode = objMappable.typcode;
            if (objMappable.prycode != null)
                criteria.prycode = objMappable.prycode;
            if (objMappable.sitcode != null)
                criteria.sitcode = objMappable.sitcode;
            if (objMappable.stscode != null)
                criteria.stscode = objMappable.stscode;
            if (objMappable.pgrcode != null)
                criteria.pgrcode = objMappable.pgrcode;
            if (objMappable.datefrom != null)
                criteria.datefrom = objMappable.datefrom;
            if (objMappable.dateto != null)
                criteria.dateto = objMappable.dateto;

            criteria.site_id = setup.site_id;

            return criteria;
        }
    }
}