﻿using System;
using System.Linq;
using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Reports.Mappers
{
    public class RepCategoryMapper
        : IMapper<IRepCategoryMappable>
    {
        private Setup setup { get; set; }
        public void Map(IRepCategoryMappable objMappable)
        {
            this.setup = ErsFactory.ersUtilityFactory.getSetup();

            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryStgy();
            var criteriaParent = this.GetParentCriteria();
            var criteriaChild = this.GetChildCriteria();
            var criteriaRowSpan = this.GetRowSpanCriteria();
            var criteriaChildCount = this.GetChildCountCriteria();
            
            var listParent = repository.Find(criteriaParent);
            var listChild = repository.FindChild(criteriaChild, (setup.Multiple_sites == true) ? (int?)setup.site_id : null);
            var listRowSpan = repository.FindRowSpan(criteriaRowSpan, (setup.Multiple_sites == true) ? (int?)setup.site_id : null);
            var listChildCount = repository.FindChildCount(criteriaChildCount, this.GetChildCountWhere(objMappable));
          
            foreach (var p in listParent)
            {
                ErsCtsRepCategory nullrep = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategory();
                nullrep.code = "0";
                nullrep.namename = "未設定";
                nullrep.child_code = p.code;
                nullrep.childcount = 0;
                nullrep.parent_code = p.code;
                nullrep.active = EnumActive.Active;   

                listChild.Insert(0, nullrep);

                p.parentcount = 0;
                foreach (var c in listChildCount)
                {
                    if (c.parent_code == p.code)
                        p.parentcount += c.childcount; 
                }
                p.hasChild = listChild.Where((record) => record.parent_code == p.code).Count() > 0;
            }

            foreach (var r in listRowSpan)
            {
                r.rowcount++;
            }

            objMappable.repcategoryListParent = ErsCommon.ConvertEntityListToDictionaryList(listParent);
            objMappable.repcategoryListChild = ErsCommon.ConvertEntityListToDictionaryList(listChild);
            objMappable.repcategoryRowSpan = ErsCommon.ConvertEntityListToDictionaryList(listRowSpan);            
            objMappable.repcategoryChildCount = ErsCommon.ConvertEntityListToDictionaryList(listChildCount);

            if (listParent.Count > 0)
                objMappable.repcategoryHasRecord = true;
        }

        private ErsCtsRepCategoryCriteria GetParentCriteria()
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryCriteria();
            criteria.type_code = EnumCtsEnquiryCategoryType.ENQCT_NAME.ToString();
            criteria.parent_site_id = setup.site_id;
            criteria.AddOrderBy("p.code", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepCategoryCriteria GetChildCriteria()
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryCriteria();
            criteria.parent_type_code = EnumCtsEnquiryCategoryType.ENQCT_NAME.ToString();
            criteria.child_site_id = setup.site_id;

            //criteria.AddOrderBy("c.parent_type_code", Criteria.OrderBy.ORDER_BY_ASC);
            //criteria.AddOrderBy("c.active", Criteria.OrderBy.ORDER_BY_DESC);
            criteria.AddOrderBy("c.disp_order", Criteria.OrderBy.ORDER_BY_ASC);
            criteria.AddOrderBy("c.code", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepCategoryCriteria GetRowSpanCriteria()
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryCriteria();
            criteria.parent_type_code = EnumCtsEnquiryCategoryType.ENQCT_NAME.ToString();
            criteria.child_site_id = setup.site_id;
            criteria.AddGroupBy("c.child_code");
            criteria.AddOrderBy("c.child_code", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepCategoryCriteria GetChildCountCriteria()
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCategoryCriteria();
            criteria.parent_site_id = setup.site_id;
            criteria.AddOrderBy("p.code", Criteria.OrderBy.ORDER_BY_ASC);
            criteria.AddOrderBy("code", Criteria.OrderBy.ORDER_BY_ASC);
            return criteria;
        }

        private string GetChildCountWhere(IRepCategoryMappable objMappable)
        {
            string strQuery = "";

            if (objMappable.typcode != null)
            {
                if (!String.IsNullOrEmpty(strQuery))
                {
                    strQuery += " AND";
                }
                strQuery += " enq_type = " + objMappable.typcode.ToString();
            }
            if (objMappable.prycode != null)
            {
                if (!String.IsNullOrEmpty(strQuery))
                {
                    strQuery += " AND";
                }
                strQuery += " enq_priorty = " + objMappable.prycode.ToString();
            }
            if (objMappable.stscode != null)
            {
                if (!String.IsNullOrEmpty(strQuery))
                {
                    strQuery += " AND";
                }
                strQuery += " enq_status = " + objMappable.stscode.ToString();
            }
            if (objMappable.pgrcode != null)
            {
                if (!String.IsNullOrEmpty(strQuery))
                {
                    strQuery += " AND";
                }
                strQuery += " enq_progress = " + objMappable.pgrcode.ToString();
            }
            if (objMappable.sitcode != null)
            {
                if (!String.IsNullOrEmpty(strQuery))
                {
                    strQuery += " AND";
                }
                strQuery += " enq_situation = " + objMappable.sitcode.ToString();
            }

            if (!String.IsNullOrEmpty(strQuery))
            {
                strQuery += " AND";
            }

            strQuery += " intime >= '" + objMappable.datefrom.Value + "' AND ";
            strQuery += " intime <= '" + objMappable.dateto.Value + "'";

            strQuery += " AND";
            strQuery += " (site_id = " + setup.site_id + " OR site_id = " + (int)EnumSiteId.COMMON_SITE_ID + ")";
       

            if (!String.IsNullOrEmpty(strQuery))
            {
                strQuery = " WHERE" + strQuery;
            }

            return strQuery;
        }
    }
}