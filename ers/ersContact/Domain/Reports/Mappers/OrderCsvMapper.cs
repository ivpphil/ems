﻿using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using System.Collections.Generic;
using System;
using Models.reports.csv;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappers
{
    public class OrderCsvMapper
        : IMapper<IOrderCsvMappable>
    {
        public void Map(IOrderCsvMappable objMappable)
        {
            this.CreateCsvFile(objMappable);

            objMappable.reporder = true;
        }

        public virtual void CreateCsvFile(IOrderCsvMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderCriteria();
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            criteria = this.reporderCriteria(objMappable);
            criteria.site_id = setup.site_id;

            var criteriaTotal = this.reporderCriteria(objMappable);
            criteriaTotal.site_id = setup.site_id;

            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderStgy();

            criteria.AddGroupBy("agent");
            criteria.AddGroupBy("ag_type");
            criteria.AddOrderBy("ag_type", ErsCtsRepOrderCriteria.OrderBy.ORDER_BY_ASC);

            var cri = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderCriteria();
            if (!string.IsNullOrEmpty(objMappable.agentid))
                cri.user_id = objMappable.agentid;
            cri.AddOrderBy("cts_agetype_t.id", ErsCtsRepOrderCriteria.OrderBy.ORDER_BY_ASC);

            var tmp = repository.FindList(cri);

            var list = repository.Find(criteria);
            var tmplist = repository.FindTemp(criteria);
            var totlist = repository.FindTotal(criteriaTotal);
            var tottmplist = repository.FindTempTotal(criteriaTotal);

            List<ErsCtsRepOrder> total = new List<ErsCtsRepOrder>();

            if (list.Count != 0 || tmplist.Count != 0)
            {
                for (int i = 0; i < tmp.Count; i++)
                {
                    ErsCtsRepOrder item = new ErsCtsRepOrder();
                    item.agent = tmp[i].agent;
                    item.ag_type = tmp[i].ag_type;
                    item.amount = 0;
                    item.temp_amount = 0;
                    item.total_amount = 0;
                    item.total_price = 0;
                    if (objMappable.ag_type.HasValue)
                    {
                        if (objMappable.ag_type == tmp[i].ag_type)
                            total.Add(item);
                    }
                    else
                        total.Add(item);
                }

                foreach (var i in total)
                {
                    foreach (var l in list)
                        if (l.ag_type == i.ag_type)
                        {
                            i.amount = l.amount;
                            i.total_amount += l.amount;
                            i.total_price += l.price;
                        }

                    foreach (var m in tmplist)
                        if (m.ag_type == i.ag_type)
                        {
                            i.temp_amount = m.temp_amount;
                            i.total_amount += m.temp_amount;
                            i.total_price += m.temp_price;
                        }
                }

                if (total.Count != 0)
                {
                    ErsCtsRepOrder last = new ErsCtsRepOrder();
                    last.agent = "Total";
                    last.ag_type = null;
                    last.amount = totlist[0].amount;
                    last.temp_amount = tottmplist[0].temp_amount;
                    last.total_amount = totlist[0].amount + tottmplist[0].temp_amount;
                    last.total_price = totlist[0].price + tottmplist[0].temp_price;
                    total.Add(last);
                }
            }


            var filename = "PromptReportOfOrdersTabulation" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";

            order_csv order_csv;
            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            objMappable.csvCreater = csvCreater;
            using (var writer = csvCreater.GetWriter(filename))
            {
                csvCreater.WriteCsvHeader<order_csv>(writer);
                foreach (var item in total)
                {
                    order_csv = new order_csv();
                    order_csv.OverwriteWithParameter(item.GetPropertiesAsDictionary());
                    order_csv.agent = item.agent;
                    order_csv.temp_order = item.temp_amount;
                    order_csv.reg_order = item.amount;
                    order_csv.total = item.total_amount;
                    order_csv.price = item.total_price;

                    csvCreater.WriteBody(order_csv, writer);
                }
            }

        }

        private ErsCtsRepOrderCriteria reporderCriteria(IOrderCsvMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepOrderCriteria();
            this.repOrderLoad(criteria, objMappable);
            if (objMappable.ag_type.HasValue)
                criteria.ag_type = objMappable.ag_type;

            if (!string.IsNullOrEmpty(objMappable.agentid))
                criteria.agentid = objMappable.agentid;

            return criteria;
        }

        private ErsCtsRepOrderCriteria repOrderLoad(ErsCtsRepOrderCriteria criteria, IOrderCsvMappable objMappable)
        {
            criteria.datefrom = objMappable.datefrom;
            criteria.dateto = objMappable.dateto;

            return criteria;
        }
    }
}