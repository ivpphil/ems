﻿using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappers
{
    public class CampaignMapper
        : IMapper<ICampaignMappable>
    {
        public void Map(ICampaignMappable objMappable)
        {
            if (objMappable.reload || objMappable.medselect)
                this.reloadMedia(objMappable);

            if (objMappable.medsearch)
                this.searchMedia(objMappable);
        }

        private void reloadMedia(ICampaignMappable objMappable)
        {
            var repository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();
            var criteria = ErsFactory.ersDocBundleFactory.GetErsCampaignCriteria();
            criteria.ccode = objMappable.ccode;
            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            var list = repository.Find(criteria);
            if (list.Count > 0)
            {
                objMappable.campaign_name = list[0].campaign_name;
            }
            else
            {
                objMappable.campaign_name = "No Data";
            }
        }

        private void searchMedia(ICampaignMappable objMappable)
        {
            objMappable.pageCnt = objMappable.pager.pageCnt;

            var repository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();
            var criteria = ErsFactory.ersDocBundleFactory.GetErsCampaignCriteria();

            if (!string.IsNullOrEmpty(objMappable.ccode))
                criteria.ccode = objMappable.ccode;

            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            objMappable.recordCount = repository.GetRecordCount(criteria);
            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;
            }
            objMappable.pagerPageCount = pagerPageCount;
            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            criteria.AddOrderBy("campaign_t.id", Criteria.OrderBy.ORDER_BY_ASC);

            var list = repository.Find(criteria);
            objMappable.campaignList = ErsCommon.ConvertEntityListToDictionaryList(list);
        }
    }
}