﻿using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappers
{
    public class ProductSalesMapper
        : IMapper<IProductSalesMappable>
    {
        public void Map(IProductSalesMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesStgy();
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesCriteria();

            if (objMappable.searchscode1)
                criteria.scode = objMappable.scode1;
            if (objMappable.searchscode2)
                criteria.scode = objMappable.scode2;
            if (objMappable.searchscode3)
                criteria.scode = objMappable.scode3;
            if (objMappable.searchscode4)
                criteria.scode = objMappable.scode4;
            if (objMappable.searchscode5)
                criteria.scode = objMappable.scode5;

            objMappable.recordCount = repository.GetRecordCount(criteria);
            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("20002"));
                return;
            }

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;
            }
            objMappable.pagerPageCount = pagerPageCount;
            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            criteria.AddOrderBy("scode", Criteria.OrderBy.ORDER_BY_ASC);
            objMappable.repproductList = ErsCommon.ConvertEntityListToDictionaryList(repository.FindProducts(criteria));

            if (objMappable.repproductList.Count != 0)
            {
                objMappable.repproductsearch = true;
            }
            else
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
            }
        }
    }
}