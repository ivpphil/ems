﻿using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers;
using System.Collections.Generic;
using System;
using Models.reports.csv;

namespace ersContact.Domain.Reports.Mappers
{
    public class SalesCsvMapper
        : IMapper<ISalesCsvMappable>
    {
        public void Map(ISalesCsvMappable objMappable)
        {
            this.CreateCsvFile(objMappable);

            objMappable.repsales = true;
        }

        public virtual void CreateCsvFile(ISalesCsvMappable objMappable)
        {
            var criteria = this.repsalesCriteria(objMappable);
            var criteriaTotalByInterval = this.repsalesCriteria(objMappable);
            var criteriaTotalByDNo = this.repsalesCriteria(objMappable);
            var criteriaTime = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesCriteria();

            this.GetCriteria(criteria, criteriaTotalByInterval, criteriaTotalByDNo, criteriaTime);

            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesStgy();

            var listTimeInterval = repository.FindInterval(criteriaTime);

            IList<ErsCtsRepSales> listMain;
            IList<ErsCtsRepSales> listByInterval;
            IList<ErsCtsRepSales> listByDNo;

            if (objMappable.TargetRegOrder)
            {
                listMain = repository.Find(criteria);
                listByInterval = repository.FindTotalByInterval(criteriaTotalByInterval);
                listByDNo = repository.FindTotalByDNo(criteriaTotalByDNo);
            }
            else
            {
                listMain = repository.FindTemp(criteria);
                listByInterval = repository.FindTempTotalByInterval(criteriaTotalByInterval);
                listByDNo = repository.FindTempTotalByDNo(criteriaTotalByDNo);
            }

            var filename = "SalesProductTabulation" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";

            //sales_csv sales_csv;
            var csvCreater = ErsFactory.ersUtilityFactory.GetErsCsvCreater();
            objMappable.csvCreater = csvCreater;
            using (var writer = csvCreater.GetWriter(filename))
            {
                var sales_dic = new Dictionary<string, object>();
                //csvCreater.WriteCsvHeader<sales_csv>(writer);

                string[] scode = new string[listByDNo.Count];

                // Header
                for (int i = 0; i < listByDNo.Count; i++)
                    scode[i] = listByDNo[i].scode;

                sales_dic = new Dictionary<string,object>();
                sales_dic["time_interval"] = ErsResources.GetFieldName("time_interval");
               for (int i = 0; i < listByDNo.Count; i++)
               {
                    sales_dic[listByDNo[i].scode] = listByDNo[i].scode;
                }
               sales_dic["total"] = ErsResources.GetFieldName("total");
                csvCreater.WriteBody(sales_dic, writer);

                // Middle
                foreach (var item in listTimeInterval)
                {
                    sales_dic = new Dictionary<string,object>();
                    sales_dic["time_interval"]= item.byhalfhour;

                    for (int i = 0; i < listByDNo.Count; i++)
                        foreach (var main in listMain)
                            if (main.scode == listByDNo[i].scode)
                                if (main.byhalfhour == item.byhalfhour)
                                    scode[i] = main.amount.ToString();

                    for (int i = 0; i < listByDNo.Count; i++)
                   {
                        sales_dic[listByDNo[i].scode] = scode[i];
                    }

                    foreach (var total in listByInterval)
                        if (item.byhalfhour == total.byhalfhour)
                            sales_dic["total"]= total.amount.ToString();

                    csvCreater.WriteBody(sales_dic,writer);
                }

                //Footer
                int order_total = 0;
                sales_dic = new Dictionary<string, object>();
                sales_dic["time_interval"] = ErsResources.GetFieldName("total");
                for (int i = 0; i < listByDNo.Count; i++)
                {
                    sales_dic[listByDNo[i].scode] = listByDNo[i].d_no_count.ToString();
                    order_total += listByDNo[i].d_no_count;
                }

                sales_dic["total"] = order_total.ToString();
                csvCreater.WriteBody(sales_dic, writer);
            }

        }

        private ErsCtsRepSalesCriteria repsalesCriteria(ISalesCsvMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepSalesCriteria();
            criteria.scale_code = EnumScaleCode.byhalfhour.ToString();

            criteria.datefrom = objMappable.datefrom;
            criteria.dateto = objMappable.dateto;

            objMappable.scodes = new string[5];
            if (!string.IsNullOrEmpty(objMappable.scode1))
                objMappable.scodes[0] = objMappable.scode1;
            if (!string.IsNullOrEmpty(objMappable.scode2))
                objMappable.scodes[1] = objMappable.scode2;
            if (!string.IsNullOrEmpty(objMappable.scode3))
                objMappable.scodes[2] = objMappable.scode3;
            if (!string.IsNullOrEmpty(objMappable.scode4))
                objMappable.scodes[3] = objMappable.scode4;
            if (!string.IsNullOrEmpty(objMappable.scode5))
                objMappable.scodes[4] = objMappable.scode5;
            if (!string.IsNullOrEmpty(objMappable.scode1) || !string.IsNullOrEmpty(objMappable.scode2) || !string.IsNullOrEmpty(objMappable.scode3)
                   || !string.IsNullOrEmpty(objMappable.scode4) || !string.IsNullOrEmpty(objMappable.scode5))
                criteria.scodes = objMappable.scodes;

            return criteria;
        }

        private void GetCriteria(ErsCtsRepSalesCriteria criteria, ErsCtsRepSalesCriteria criteriaTotalByInterval,
            ErsCtsRepSalesCriteria criteriaTotalByDNo, ErsCtsRepSalesCriteria criteriaTime)
        {
            criteria.AddGroupBy("byhalfhour");
            criteria.AddGroupBy("scode");
            criteria.AddOrderBy("scode", ErsCtsRepSalesCriteria.OrderBy.ORDER_BY_ASC);
            criteria.AddOrderBy("byhalfhour", ErsCtsRepSalesCriteria.OrderBy.ORDER_BY_ASC);

            criteriaTotalByInterval.AddGroupBy("byhalfhour");
            criteriaTotalByInterval.AddOrderBy("byhalfhour", ErsCtsRepSalesCriteria.OrderBy.ORDER_BY_ASC);

            criteriaTotalByDNo.AddGroupBy("scode");
            criteriaTotalByDNo.AddOrderBy("scode", ErsCtsRepSalesCriteria.OrderBy.ORDER_BY_ASC);

            criteriaTime.AddOrderBy("byhalfhour", ErsCtsRepSalesCriteria.OrderBy.ORDER_BY_ASC);
        }
    }
}