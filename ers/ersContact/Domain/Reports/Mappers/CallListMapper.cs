﻿using System.Web;
using System.Linq;
using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Reports.Mappers
{
    public class CallListMapper
        : IMapper<ICallListMappable>
    {
        public void Map(ICallListMappable objMappable)
        {
            string target = objMappable.TargetOrder;

            if (target == null)
            {
                if (!objMappable.TargetRegOrder && !objMappable.TargetTempOrder)
                {
                    target = HttpContext.Current.Request.Form.GetValues("TargetOrder").GetValue(0).ToString();
                }
                else
                {
                    if (objMappable.TargetRegOrder)
                    {
                        target = EnumTargetOrder.RegularOrder.ToString();
                    }
                    else
                    {
                        target = EnumTargetOrder.TemporaryOrder.ToString();
                    }
                }
            }

            string target2 = objMappable.TargetScale;
            if (target2 == null)
            {
                if (!objMappable.TargetByDay && !objMappable.TargetByMonth && !objMappable.TargetByTime)
                {
                    target2 = HttpContext.Current.Request.Form.GetValues("TargetScale").GetValue(0).ToString();
                }
                else
                {
                    if (objMappable.TargetByDay)
                    {
                        target = EnumScaleCode.byday.ToString();
                    }
                    else if (objMappable.TargetByMonth)
                    {
                        target = EnumScaleCode.bymonth.ToString();
                    }
                    else if (objMappable.TargetByTime)
                    {
                        target = EnumScaleCode.byhalfhour.ToString();
                    }
                }
            }

            if (target == EnumTargetOrder.RegularOrder.ToString())
            {
                objMappable.TargetRegOrder = true;
                objMappable.TargetTempOrder = false;
            }
            else
            {
                objMappable.TargetRegOrder = false;
                objMappable.TargetTempOrder = true;
            }

            objMappable.TargetOrder = target;

            if (target2 == EnumScaleCode.byday.ToString())
            {
                objMappable.TargetByDay = true;
                objMappable.TargetByMonth = false;
                objMappable.TargetByTime = false;
            }
            else if (target2 == EnumScaleCode.bymonth.ToString())
            {
                objMappable.TargetByDay = false;
                objMappable.TargetByMonth = true;
                objMappable.TargetByTime = false;
            }
            else if (target2 == EnumScaleCode.byhalfhour.ToString())
            {
                objMappable.TargetByDay = false;
                objMappable.TargetByMonth = false;
                objMappable.TargetByTime = true;
            }

            objMappable.TargetScale = target2;

            if (objMappable.TargetByDay)
            {
                this.loadRepCallDay(objMappable);
            }
            else if (objMappable.TargetByMonth)
            {
                this.loadRepCallMonth(objMappable);
            }
            else if (objMappable.TargetByTime)
            {
                this.loadRepCall(objMappable);
            }
        }

        public void loadRepCallDay(ICallListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallStgy();
            var criteria = this.repcallCriteria(objMappable);
            var criteriaTotalTime = this.repcallCriteriaTotalTime(objMappable);
            var criteriaTotalUserTime = this.repcallCriteriaTotalUserTime(objMappable);
            var criteriaInterval = this.repcallCriteriaInterval(objMappable);

            if (objMappable.TargetTempOrder == true)
            {
                objMappable.list = repository.FindTempTotalUserDay(criteria);
                objMappable.listTime = repository.FindTempTotalTimeDay(criteriaTotalTime);
                objMappable.listUserTime = repository.FindTempTotalUserTimeDay(criteriaTotalUserTime);
                objMappable.repcallTotalTime = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listTime);
                objMappable.repcallTotalUserTime = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listUserTime);
                objMappable.repcallList = ErsCommon.ConvertEntityListToDictionaryList(objMappable.list);
            }
            else
            {
                objMappable.list = repository.FindDay(criteria);
                objMappable.listTime = repository.FindTotalTimeDay(criteriaTotalTime);
                objMappable.listUserTime = repository.FindTotalUserTimeDay(criteriaTotalUserTime);
                objMappable.repcallTotalTime = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listTime);
                objMappable.repcallTotalUserTime = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listUserTime);
                objMappable.repcallList = ErsCommon.ConvertEntityListToDictionaryList(objMappable.list);
            }
            objMappable.repcallTotal = objMappable.list.Sum((repcall) => repcall.d_no_count);

            objMappable.listInterval = repository.FindInterval(criteriaInterval);
            objMappable.repcallInterval = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listInterval);

            // For No Data Found Label
            if (objMappable.repcallTotalUserTime.Count > 0)
            {
                objMappable.repcallHasRecord = true;
            }
            else
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
            }
            objMappable.colcount = objMappable.repcallList.Count + 1;
            objMappable.rowcount = objMappable.repcallInterval.Count + 2;
        }

        public void loadRepCallMonth(ICallListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallStgy();
            var criteria = this.repcallCriteria(objMappable);
            var criteriaTotalTime = this.repcallCriteriaTotalTime(objMappable);
            var criteriaTotalUserTime = this.repcallCriteriaTotalUserTime(objMappable);
            var criteriaInterval = this.repcallCriteriaInterval(objMappable);

            if (objMappable.TargetTempOrder == true)
            {
                objMappable.list = repository.FindTempTotalUserMonth(criteria);
                objMappable.listTime = repository.FindTempTotalTimeMonth(criteriaTotalTime);
                objMappable.listUserTime = repository.FindTempTotalUserTimeMonth(criteriaTotalUserTime);
                objMappable.repcallTotalTime = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listTime);
                objMappable.repcallTotalUserTime = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listUserTime);
                objMappable.repcallList = ErsCommon.ConvertEntityListToDictionaryList(objMappable.list);
            }
            else
            {
                objMappable.list = repository.FindMonth(criteria);
                objMappable.listTime = repository.FindTotalTimeMonth(criteriaTotalTime);
                objMappable.listUserTime = repository.FindTotalUserTimeMonth(criteriaTotalUserTime);
                objMappable.repcallTotalTime = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listTime);
                objMappable.repcallTotalUserTime = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listUserTime);
                objMappable.repcallList = ErsCommon.ConvertEntityListToDictionaryList(objMappable.list);
            }
            objMappable.repcallTotal = objMappable.list.Sum((repcall) => repcall.d_no_count);

            objMappable.listInterval = repository.FindInterval(criteriaInterval);
            objMappable.repcallInterval = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listInterval);

            // For No Data Found Label
            if (objMappable.repcallTotalUserTime.Count > 0)
            {
                objMappable.repcallHasRecord = true;
            }
            else
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
            }
            objMappable.colcount = objMappable.repcallList.Count + 1;
            objMappable.rowcount = objMappable.repcallInterval.Count + 2;
        }

        public void loadRepCall(ICallListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallStgy();
            var criteria = this.repcallCriteria(objMappable);
            var criteriaTotalTime = this.repcallCriteriaTotalTime(objMappable);
            var criteriaTotalUserTime = this.repcallCriteriaTotalUserTime(objMappable);
            var criteriaInterval = this.repcallCriteriaInterval(objMappable);

            if (objMappable.TargetTempOrder == true)
            {
                objMappable.list = repository.FindTempTotalUser(criteria);
                objMappable.listTime = repository.FindTempTotalTime(criteriaTotalTime);
                objMappable.listUserTime = repository.FindTempTotalUserTime(criteriaTotalUserTime);
                objMappable.repcallTotalTime = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listTime);
                objMappable.repcallTotalUserTime = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listUserTime);
                objMappable.repcallList = ErsCommon.ConvertEntityListToDictionaryList(objMappable.list);
            }
            else
            {
                objMappable.list = repository.Find(criteria);
                objMappable.listTime = repository.FindTotalTime(criteriaTotalTime);
                objMappable.listUserTime = repository.FindTotalUserTime(criteriaTotalUserTime);
                objMappable.repcallTotalTime = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listTime);
                objMappable.repcallTotalUserTime = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listUserTime);
                objMappable.repcallList = ErsCommon.ConvertEntityListToDictionaryList(objMappable.list);
            }
            objMappable.repcallTotal = objMappable.list.Sum((repcall) => repcall.d_no_count);

            objMappable.listInterval = repository.FindInterval(criteriaInterval);
            objMappable.repcallInterval = ErsCommon.ConvertEntityListToDictionaryList(objMappable.listInterval);

            // For No Data Found Label
            if (objMappable.repcallTotalUserTime.Count > 0)
            {
                objMappable.repcallHasRecord = true;
            }
            else
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
            }
            objMappable.colcount = objMappable.repcallList.Count + 1;
            objMappable.rowcount = objMappable.repcallInterval.Count + 2;
        }

        private ErsCtsRepCallCriteria repcallCriteria(ICallListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallCriteria();

            if (objMappable.TargetByDay)
                criteria.scale_code = EnumScaleCode.byday.ToString();
            if (objMappable.TargetByMonth)
                criteria.scale_code = EnumScaleCode.bymonth.ToString();
            if (objMappable.TargetByTime)
                criteria.scale_code = EnumScaleCode.byhalfhour.ToString();

            criteria = this.repCallLoad(criteria, objMappable);
            criteria.AddGroupBy("user_id");
            criteria.AddGroupBy("ag_name");
            criteria.AddOrderBy("user_id", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepCallCriteria repcallCriteriaTotalTime(ICallListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallCriteria();

            if (objMappable.TargetByDay)
                criteria.scale_code = EnumScaleCode.byday.ToString();
            if (objMappable.TargetByMonth)
                criteria.scale_code = EnumScaleCode.bymonth.ToString();
            if (objMappable.TargetByTime)
                criteria.scale_code = EnumScaleCode.byhalfhour.ToString();

            criteria = this.repCallLoad(criteria, objMappable);
            criteria.AddGroupBy("byhalfhour");
            criteria.AddOrderBy("byhalfhour", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepCallCriteria repcallCriteriaTotalUserTime(ICallListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallCriteria();

            if (objMappable.TargetByDay)
                criteria.scale_code = EnumScaleCode.byday.ToString();
            if (objMappable.TargetByMonth)
                criteria.scale_code = EnumScaleCode.bymonth.ToString();
            if (objMappable.TargetByTime)
                criteria.scale_code = EnumScaleCode.byhalfhour.ToString();

            criteria = this.repCallLoad(criteria, objMappable);
            criteria.AddGroupBy("user_id");
            criteria.AddGroupBy("byhalfhour");
            criteria.AddOrderBy("user_id", Criteria.OrderBy.ORDER_BY_ASC);
            criteria.AddOrderBy("byhalfhour", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepCallCriteria repcallCriteriaInterval(ICallListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsRepFactory.GetErsCtsRepCallCriteria();

            if (objMappable.TargetByMonth)
                criteria.scale_code = EnumScaleCode.bymonth.ToString();
            if (objMappable.TargetByTime)
                criteria.scale_code = EnumScaleCode.byhalfhour.ToString();
            if (objMappable.TargetByDay)
            {
                criteria.scale_code = EnumScaleCode.byday.ToString();

                if (objMappable.datefrom != null && objMappable.dateto != null)
                {
                    if (objMappable.datefrom.Value.Month == objMappable.dateto.Value.Month && objMappable.datefrom.Value.Year == objMappable.dateto.Value.Year)
                    {
                        if (objMappable.datefrom.Value.Month == 2 && objMappable.datefrom.Value.Year % 4 == 0)
                        {
                            criteria.days = "30日";
                            criteria.days = "31日";
                        }
                        else if (objMappable.datefrom.Value.Month == 2 && objMappable.datefrom.Value.Year % 4 != 0)
                        {
                            criteria.days = "29日";
                            criteria.days = "30日";
                            criteria.days = "31日";
                        }
                        else if (objMappable.datefrom.Value.Month == 4 || objMappable.datefrom.Value.Month == 6 ||
                            objMappable.datefrom.Value.Month == 9 || objMappable.datefrom.Value.Month == 11)
                        {
                            criteria.days = "31日";
                        }

                    }
                }
            }
            criteria.AddOrderBy("byhalfhour", Criteria.OrderBy.ORDER_BY_ASC);

            return criteria;
        }

        private ErsCtsRepCallCriteria repCallLoad(ErsCtsRepCallCriteria criteria, ICallListMappable objMappable)
        {
            criteria.datefrom = objMappable.datefrom;
            criteria.dateto = objMappable.dateto;

            //AG TYPE
            if (objMappable.ag_type.HasValue)
            {
                criteria.ag_type = objMappable.ag_type;
            }
            return criteria;
        }
    }
}