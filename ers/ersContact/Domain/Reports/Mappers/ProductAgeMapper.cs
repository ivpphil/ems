﻿using ersContact.Domain.Reports.Mappables;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;

namespace ersContact.Domain.Reports.Mappers
{
    public class ProductAgeMapper
        : IMapper<IProductAgeMappable>
    {
        public void Map(IProductAgeMappable objMappable)
        {
            var repository = ErsFactory.ersMerchandiseFactory.GetErsSkuRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();

            if (objMappable.searchscode1)
                criteria.scode_like = objMappable.scode1;
            if (objMappable.searchscode2)
                criteria.scode_like = objMappable.scode2;
            if (objMappable.searchscode3)
                criteria.scode_like = objMappable.scode3;
            if (objMappable.searchscode4)
                criteria.scode_like = objMappable.scode4;
            if (objMappable.searchscode5)
                criteria.scode_like = objMappable.scode5;

            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            objMappable.recordCount = repository.GetRecordCount(criteria);
            if (objMappable.recordCount == 0)
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;
            }
            objMappable.pagerPageCount = pagerPageCount;
            objMappable.pager.SetLimitAndOffsetToCriteria(criteria);

            criteria.AddOrderBy("scode", Criteria.OrderBy.ORDER_BY_ASC);
            objMappable.repproductList = repository.Find(criteria);

            if (objMappable.repproductList.Count != 0)
            {
                objMappable.repproductsearch = true;
            }
            else
            {
                objMappable.controller.AddInformation(ErsResources.GetMessage("10200"));
                return;
            }
        }
    }
}