﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers.mvc.pager;

namespace ersContact.Domain.Product.Mappables
{
    public interface IProductMappable
        : IMappable
    {
        List<Dictionary<string, object>> merchandiseList { set; }
        int? cate1 { get; }
        long recordCount { get; set; }
        long maxItemCount { get; }
        string gcode { get; }
        string gname { get; }
        ErsPagerModel pager { get; }

        long pagerPageCount { get; set; }

        string mcode { get; set; }
    }
}
