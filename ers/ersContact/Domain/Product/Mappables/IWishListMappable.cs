﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Product.Mappables
{
    public interface IWishListMappable
        : IMappable
    {
        bool IsSuperUser { get; }

        List<Dictionary<string, object>> merchandiseShareList { get; set; }

        List<Dictionary<string, object>> merchandiseFavoriteList { get; set; }
    }
}