﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ersContact.Domain.Product.Commands
{
    public interface IWishListDeleteCommand
        : IWishListRegistCommand
    {
    }
}