﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Product.Commands
{
    public interface IWishListRegistCommand
        : ICommand
    {
        string scode { get; }

        bool IsSuperUser { get; }

        bool btn_favorite { get; }

        bool btn_delete_f { get; }
    }
}