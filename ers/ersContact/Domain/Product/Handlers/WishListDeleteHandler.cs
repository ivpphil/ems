﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersContact.Domain.Product.Commands;

namespace ersContact.Domain.Product.Handlers
{
    public class WishListDeleteHandler
        : WishListRegistHandler, ICommandHandler<IWishListDeleteCommand>
    {
        public ICommandResult Submit(IWishListDeleteCommand command)
        {
            this.Delete(command);
            return new CommandResult(true);
        }

        internal void Delete(IWishListDeleteCommand command)
        {
            var criteria = this.GetCtsWishListCritera(command, true);
            cts_wishlistRepository.Delete(criteria);
        }
    }
}