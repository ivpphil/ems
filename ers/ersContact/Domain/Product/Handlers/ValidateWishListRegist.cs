﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersContact.Domain.Product.Commands;

namespace ersContact.Domain.Product.Handlers
{
    public class ValidateWishListRegist
        : IValidationHandler<IWishListRegistCommand>
    {
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(IWishListRegistCommand command)
        {
            yield return command.CheckRequired("scode");
        }
    }
}