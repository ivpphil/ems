﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersContact.Domain.Product.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.cts_wishlist;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.util;

namespace ersContact.Domain.Product.Handlers
{
    public class WishListRegistHandler
        : ICommandHandler<IWishListRegistCommand>
    {
        protected ErsCtsWishListRepository cts_wishlistRepository = ErsFactory.ersCtsWishListFactory.GetErsCtsWishListRepository();
        
        public ICommandResult Submit(IWishListRegistCommand command)
        {
            this.ExecuteOperation(command);
            return new CommandResult(true);
        }

        internal void ExecuteOperation(IWishListRegistCommand command)
        {
            var criteria = this.GetCtsWishListCritera(command);
            var recordCount = cts_wishlistRepository.GetRecordCount(criteria);

            if (recordCount == 1)
            {
                this.Update(command, criteria);
                return;
            }

            if (recordCount == 0)
            {
                var maxFiveCriteria = GetCtsWishListCritera(command, false);
                if (cts_wishlistRepository.GetRecordCount(maxFiveCriteria) == 5)
                    this.DeleteMaxFive(command, maxFiveCriteria);

                this.Insert(command);
            }
        }

        internal void Insert(IWishListRegistCommand command)
        {
            var cts_wishlist = ErsFactory.ersCtsWishListFactory.GetErsCtsWishList();
            cts_wishlist.wishlist_type = this.GetCtsWishlistType(command);
            cts_wishlist.scode = command.scode;
            cts_wishlist.user_id = ((ErsSessionStateContact)ErsContext.sessionState).Get("cts_user_id");
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            if (setup.Multiple_sites)
            {
                cts_wishlist.site_id = (EnumSiteId)setup.site_id;
            }
            cts_wishlistRepository.Insert(cts_wishlist);
        }

        internal void Update(IWishListRegistCommand command, ErsCtsWishListCritera criteria)
        {
            var cts_wishlist = ErsFactory.ersCtsWishListFactory.GetErsCtsWishList();
            var old_record = cts_wishlistRepository.Find(criteria).First();
            var new_record = ErsFactory.ersCtsWishListFactory.GetErsCtsWishListWithParameters(old_record.GetPropertiesAsDictionary());
            new_record.utime = DateTime.Now;
            cts_wishlistRepository.Update(old_record, new_record);
        }

        internal void DeleteMaxFive(IWishListRegistCommand command, ErsCtsWishListCritera criteria)
        {
            criteria.SetCtsWishListByUtime(Criteria.OrderBy.ORDER_BY_ASC);
            criteria.SetCtsWishListByIntime(Criteria.OrderBy.ORDER_BY_ASC);
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            if (setup.Multiple_sites)
            {
                criteria.site_id = setup.site_id;
            }
            criteria.LIMIT = 1;

            var cts_wishlist = cts_wishlistRepository.Find(criteria).First();
            cts_wishlistRepository.Delete(cts_wishlist);

        }

        internal ErsCtsWishListCritera GetCtsWishListCritera(IWishListRegistCommand command, bool WithScode = true)
        {
            var criteria = ErsFactory.ersCtsWishListFactory.GetErsCtsWishListCritera();
            criteria.wishlist_type = this.GetCtsWishlistType(command);
            Setup setup = ErsFactory.ersUtilityFactory.getSetup();
            if (setup.Multiple_sites)
            {
                criteria.site_id = setup.site_id;
            }
            if (WithScode)
                criteria.scode = command.scode;

            if (this.GetCtsWishlistType(command) == EnumCtsWishlistType.AG)
                criteria.user_id = ((ErsSessionStateContact)ErsContext.sessionState).Get("cts_user_id");

            return criteria;
        }

        private EnumCtsWishlistType GetCtsWishlistType(IWishListRegistCommand command)
        {
            if (command.btn_favorite || command.btn_delete_f)
                return EnumCtsWishlistType.AG;

            return EnumCtsWishlistType.SV;
        }
    }
}