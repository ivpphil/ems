﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Product.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersContact.Domain.Product.Mappers
{
    public class ProductMapper
        : IMapper<IProductMappable>
    {
        public void Map(IProductMappable objMappable)
        {
            var member_rank = ErsFactory.ersMemberFactory.GetRetrieveMemberRankStgy().Retrieve(objMappable.mcode);
            objMappable.merchandiseList = this.GetFindList(objMappable, member_rank);
        }

        private List<Dictionary<string, object>> GetFindList(IProductMappable objMappable, int? member_rank)
        {

            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var criteria = this.GetCriteria(objMappable);

            //検索結果の総数を取得
            objMappable.recordCount = repository.GetRecordCountSkuBase(criteria);

            long pagerPageCount = objMappable.recordCount / objMappable.maxItemCount;
            if (objMappable.recordCount % objMappable.maxItemCount > 0)
            {
                pagerPageCount += 1;	// 余りは切り上げ
            }

            objMappable.pagerPageCount = pagerPageCount;

            this.SetSortToCriteria(criteria, objMappable);

            var objlist = repository.FindSkuBaseItemList(criteria, member_rank);

            var viewList = new List<Dictionary<string, object>>();
            foreach (var merchandise in objlist)
            {
                var viewDictionary = merchandise.GetPropertiesAsDictionary();

                viewDictionary["price"] = merchandise.price;
                viewDictionary["price2"] = merchandise.price2;

                if (ErsFactory.ersMerchandiseFactory.GetOnCampaignSpecification().IsSatisfiedBy(merchandise.p_date_from, merchandise.p_date_to))
                {
                    viewDictionary["price"] = merchandise.sale_price;
                    viewDictionary["default_price"] = merchandise.price;
                    viewDictionary["onCampaign"] = true;
                }
                
                viewDictionary["DatePeriod"] = ErsFactory.ersMerchandiseFactory.GetOnSaleSpecification().GetDatePeriod(merchandise.date_from, merchandise.date_to, DateTime.Now);
                viewDictionary["StockStatus"] = ErsFactory.ersMerchandiseFactory.GetStockStatusSpecification().GetStockStatusOfMerchandise(merchandise);
                viewDictionary["sale_regular_flg"] = this.get_sale_regular_flg(merchandise.s_sale_ptn);
                viewDictionary["sale_normal_flg"] = this.get_sale_normal_flg(merchandise.s_sale_ptn);

                viewList.Add(viewDictionary);
            }

            return viewList;
        }

        private ErsSkuCriteria GetCriteria(IProductMappable objMappable)
        {
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            criteria.ignore_gcode = setup.IgnoreGcode;

            if (objMappable.cate1.HasValue)
                criteria.cate1 = objMappable.cate1;

            if (!string.IsNullOrEmpty(objMappable.gcode))
                criteria.gcodeLikePrefix = objMappable.gcode;

            if (!string.IsNullOrEmpty(objMappable.gname))
                criteria.sname_and_gname = objMappable.gname;

            //criteria.disp_list_flg = EnumDisp_list_flg.Visible;
            criteria.SetActiveOnly(DateTime.Now);

            criteria.g_site_id = setup.site_id;

            return criteria;
        }

        private void SetSortToCriteria(ErsSkuCriteria emCri, IProductMappable objMappable)
        {
            objMappable.pager.SetLimitAndOffsetToCriteria(emCri);

            //商品出力結果
            emCri.SetOrderBySort(Criteria.OrderBy.ORDER_BY_ASC);
            emCri.SetOrderByDateFrom(Criteria.OrderBy.ORDER_BY_DESC);
            emCri.SetOrderByGcode(Criteria.OrderBy.ORDER_BY_ASC);
            emCri.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);
        }

        //通常販売フラグ
        public bool get_sale_normal_flg(EnumSalePatternType? s_sale_ptn)
        {
            switch (s_sale_ptn)
            {
                case EnumSalePatternType.NORMAL:
                    return true;
                case EnumSalePatternType.REGULAR:
                    return false;
                case EnumSalePatternType.ALL:
                    return true;
            }

            return false;
        }

        //定期フラグ
        public bool get_sale_regular_flg(EnumSalePatternType? s_sale_ptn)
        {
            switch (s_sale_ptn)
            {
                case EnumSalePatternType.NORMAL:
                    return false;
                case EnumSalePatternType.REGULAR:
                    return true;
                case EnumSalePatternType.ALL:
                    return true;
            }

            return false;
        }
    }
}