﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using ersContact.Domain.Product.Mappables;
using jp.co.ivp.ers.cts_wishlist;
using jp.co.ivp.ers;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.db;

namespace ersContact.Domain.Product.Mappers
{
    public class WishListMapper
        : IMapper<IWishListMappable>
    {
        public void Map(IWishListMappable objMappable)
        {
            this.LoadDefaulData(objMappable);
        }


        internal void LoadDefaulData(IWishListMappable objMappable)
        {
            var spec = ErsFactory.ersCtsWishListFactory.GetSearchCtsWishlistWithMerchandiseSpec();
            var criteria = this.GetCtsWishListCritera(objMappable, EnumCtsWishlistType.AG);

            criteria.SetCtsWishListByUtime(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.SetCtsWishListByScode(Criteria.OrderBy.ORDER_BY_ASC);
            objMappable.merchandiseFavoriteList = spec.GetSearchData(criteria);

            objMappable.merchandiseFavoriteList = this.GetUnionDictionaryToDictionary(objMappable.merchandiseFavoriteList, 
            this.GetFindList(objMappable, objMappable.merchandiseFavoriteList.Select((scode) => (string)scode["scode"])));

            criteria = this.GetCtsWishListCritera(objMappable, EnumCtsWishlistType.SV);
            criteria.SetCtsWishListByUtime(Criteria.OrderBy.ORDER_BY_DESC);
            criteria.SetCtsWishListByScode(Criteria.OrderBy.ORDER_BY_ASC);
            objMappable.merchandiseShareList = spec.GetSearchData(criteria);

            objMappable.merchandiseShareList = this.GetUnionDictionaryToDictionary(objMappable.merchandiseShareList,
            this.GetFindList(objMappable, objMappable.merchandiseShareList.Select((scode) => (string)scode["scode"])));
        }

        internal ErsCtsWishListCritera GetCtsWishListCritera(IWishListMappable objMappable, EnumCtsWishlistType wishlist_type)
        {
            var criteria = ErsFactory.ersCtsWishListFactory.GetErsCtsWishListCritera();
            criteria.wishlist_type = wishlist_type;

            if (wishlist_type == EnumCtsWishlistType.AG)
            {
                criteria.user_id = ((ErsSessionStateContact)ErsContext.sessionState).Get("cts_user_id");
            }

            var setup = ErsFactory.ersUtilityFactory.getSetup();

            criteria.site_id = setup.site_id;
            criteria.g_site_id = setup.site_id;

            return criteria;
        }

        private List<Dictionary<string, object>> GetFindList(IWishListMappable objMappable, IEnumerable<string> scode)
        {
            var OnSaleSpecification = ErsFactory.ersMerchandiseFactory.GetOnSaleSpecification();
            var StockStatusSpecification = ErsFactory.ersMerchandiseFactory.GetStockStatusSpecification();
            var GetOnCampaignSpecification = ErsFactory.ersMerchandiseFactory.GetOnCampaignSpecification();

            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();
            var criteria = ErsFactory.ersMerchandiseFactory.GetErsSkuCriteria();
            criteria.scode_in = scode;
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            criteria.site_id = setup.site_id;

            criteria.SetOrderByScode(Criteria.OrderBy.ORDER_BY_ASC);

            var viewList = new List<Dictionary<string, object>>();

            if (scode.Count() > 0)
            {
                var objlist = repository.FindSkuBaseItemList(criteria, null);

                foreach (var merchandise in objlist)
                {
                    var record = merchandise.GetPropertiesAsDictionary();

                    if (GetOnCampaignSpecification.IsSatisfiedBy(merchandise.p_date_from, merchandise.p_date_to))
                    {
                        record["price"] = merchandise.sale_price;
                        record["default_price"] = merchandise.price;
                        record["onCampaign"] = true;
                    }

                    record["DatePeriod"] = OnSaleSpecification.GetDatePeriod(merchandise.date_from, merchandise.date_to, DateTime.Now);
                    record["StockStatus"] = StockStatusSpecification.GetStockStatusOfMerchandise(merchandise);
                    record["sale_regular_flg"] = this.get_sale_regular_flg(merchandise.s_sale_ptn);
                    record["sale_normal_flg"] = this.get_sale_normal_flg(merchandise.s_sale_ptn);

                    viewList.Add(record);
                }
            }

            return viewList;
        }

        //通常販売フラグ
        public bool get_sale_normal_flg(EnumSalePatternType? s_sale_ptn)
        {
            switch (s_sale_ptn)
            {
                case EnumSalePatternType.NORMAL:
                    return true;
                case EnumSalePatternType.REGULAR:
                    return false;
                case EnumSalePatternType.ALL:
                    return true;
            }

            return false;
        }

        //定期フラグ
        public bool get_sale_regular_flg(EnumSalePatternType? s_sale_ptn)
        {
            switch (s_sale_ptn)
            {
                case EnumSalePatternType.NORMAL:
                    return false;
                case EnumSalePatternType.REGULAR:
                    return true;
                case EnumSalePatternType.ALL:
                    return true;
            }

            return false;
        }

        internal List<Dictionary<string, object>> GetUnionDictionaryToDictionary(List<Dictionary<string, object>> mainDictionaryList, List<Dictionary<string, object>> pairableDictionaryList)
        {
            var retListDic = new List<Dictionary<string, object>>();
            foreach (var record in mainDictionaryList)
            {
                var dicUnion = pairableDictionaryList.Find((scode) => scode.ContainsKey("scode") && Convert.ToString(scode["scode"]) == Convert.ToString(record["scode"]));

                if (dicUnion != null)
                {
                    retListDic.Add(record.Union(dicUnion)
                        .GroupBy(g => g.Key)
                        .ToDictionary(keyValue => keyValue.Key, objValue => objValue.FirstOrDefault().Value));

                    continue;
                }

                retListDic.Add(record);
            }

            return retListDic;
        }
    }
}