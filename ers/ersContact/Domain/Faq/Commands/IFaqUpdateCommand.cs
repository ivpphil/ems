﻿using System;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Faq.Commands
{
    public interface IFaqUpdateCommand
        : ICommand
    {
        DateTime? intime { get; set; }
        DateTime? utime { get; set; }
        int? cate1 { get; set; }
        int? cate2 { get; set; }
        int? cate3 { get; set; }
        int? cate4 { get; set; }
        int? cate5 { get; set; }
        int? id { get; set; }
        EnumActive? active { get; set; }
    }
}