﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Faq.Mappables
{
    public interface IFaqListMappable
        : IMappable
    {
        int? cate1 { get; set; }
        int? cate2 { get; set; }
        int? cate3 { get; set; }
        int? cate4 { get; set; }
        int? cate5 { get; set; }
        string keywords { get; set; }
        long recordCount { get; set; }
        List<Dictionary<string, object>> searchList { get; set; }
    }
}
