﻿using jp.co.ivp.ers.mvc.MapperProcessor;

namespace ersContact.Domain.Faq.Mappables
{
    public interface ICategoryListMappable
        : IMappable
    {
        string catlabel1 { get; set; }
        string catlabel2 { get; set; }
        string catlabel3 { get; set; }
        string catlabel4 { get; set; }
        string catlabel5 { get; set; }
        bool showCat1 { get; set; }
        bool showCat2 { get; set; }
        bool showCat3 { get; set; }
        bool showCat4 { get; set; }
        bool showCat5 { get; set; }
    }
}
