﻿using ersContact.Domain.Faq.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersContact.Domain.Faq.Mappers
{
    public class CategoryListMapper
        : IMapper<ICategoryListMappable>
    {
        public void Map(ICategoryListMappable objMappable)
        {
            var list = ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT_NAME, EnumCtsEnquiryCategoryColumnName.namename, false);

            foreach (var dr in list)
            {
                switch ((int)dr["value"])
                {
                    case 1:
                        objMappable.catlabel1 = (string)dr["name"];
                        objMappable.showCat1 = (EnumActive.Active == (EnumActive)dr["active"]);
                        break;
                    case 2:
                        objMappable.catlabel2 = (string)dr["name"];
                        objMappable.showCat2 = (EnumActive.Active == (EnumActive)dr["active"]);
                        break;
                    case 3:
                        objMappable.catlabel3 = (string)dr["name"];
                        objMappable.showCat3 = (EnumActive.Active == (EnumActive)dr["active"]);
                        break;
                    case 4:
                        objMappable.catlabel4 = (string)dr["name"];
                        objMappable.showCat4 = (EnumActive.Active == (EnumActive)dr["active"]);
                        break;
                    case 5:
                        objMappable.catlabel5 = (string)dr["name"];
                        objMappable.showCat5 = (EnumActive.Active == (EnumActive)dr["active"]);
                        break;
                }
            }
        }
    }
}