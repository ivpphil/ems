﻿using ersContact.Domain.Faq.Mappables;
using jp.co.ivp.ers.faq;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers;

namespace ersContact.Domain.Faq.Mappers
{
    public class RegisterListMapper
        : IMapper<IRegisterListMappable>
    {
        public void Map(IRegisterListMappable objMappable)
        {
            ErsCtsFAQ regist = ErsFactory.ersCtsFAQFactory.GetErsCtsFAQWithID(objMappable.id);

            if (regist != null)
            {
                objMappable.intime = regist.intime;
                objMappable.utime = regist.utime;
                objMappable.faq_casename = regist.faq_casename;
                objMappable.cate1 = regist.cate1;
                objMappable.cate2 = regist.cate2;
                objMappable.cate3 = regist.cate3;
                objMappable.cate4 = regist.cate4;
                objMappable.cate5 = regist.cate5;

                if (regist.faq_text.HasValue())
                {
                    objMappable.faq_text = regist.faq_text.Replace("\r", "\n");
                }

                if (regist.faq_text2.HasValue())
                {
                    objMappable.faq_text2 = regist.faq_text2.Replace("\r", "\n");
                }
                
                objMappable.user_id = regist.user_id;
                objMappable.active = regist.active;
                objMappable.openRegist = true;
            }
        }
    }
}