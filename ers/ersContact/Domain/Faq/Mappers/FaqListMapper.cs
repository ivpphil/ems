﻿using System.Collections.Generic;
using ersContact.Domain.Faq.Mappables;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.faq;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace ersContact.Domain.Faq.Mappers
{
    public class FaqListMapper
        : IMapper<IFaqListMappable>
    {
        public void Map(IFaqListMappable objMappable)
        {
            objMappable.searchList = this.LoadFAQList(objMappable);
        }

        public List<Dictionary<string, object>> LoadFAQList(IFaqListMappable objMappable)
        {
            var repository = ErsFactory.ersCtsFAQFactory.GetErsCtsFAQRepository();
            var criteria = this.GetCriteria(objMappable);

            objMappable.recordCount = repository.GetRecordCount(criteria);

            criteria.AddOrderBy("cts_faq_template_t.id", Criteria.OrderBy.ORDER_BY_ASC);
            
            var list = repository.Find(criteria);

            return ErsCommon.ConvertEntityListToDictionaryList(list);
        }

        private ErsCtsFAQCriteria GetCriteria(IFaqListMappable objMappable)
        {
            var criteria = ErsFactory.ersCtsFAQFactory.GetErsCtsFAQCriteria();

            if (objMappable.cate1 != null && objMappable.cate1 > 0)
                criteria.cate1 = objMappable.cate1;

            if (objMappable.cate2 != null && objMappable.cate2 > 0)
                criteria.cate2 = objMappable.cate2;

            if (objMappable.cate3 != null && objMappable.cate3 > 0)
                criteria.cate3 = objMappable.cate3;

            if (objMappable.cate4 != null && objMappable.cate4 > 0)
                criteria.cate4 = objMappable.cate4;

            if (objMappable.cate5 != null && objMappable.cate5 > 0)
                criteria.cate5 = objMappable.cate5;

            if (!string.IsNullOrEmpty(objMappable.keywords))
                criteria.keywords = objMappable.keywords;

            criteria.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            return criteria;
        }
    }
}