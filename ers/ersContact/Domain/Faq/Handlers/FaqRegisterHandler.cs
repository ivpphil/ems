﻿using System;
using ersContact.Domain.Faq.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Faq.Handlers
{
    public class FaqRegisterHandler
        : ICommandHandler<IFaqRegisterCommand>
    {
        public ICommandResult Submit(IFaqRegisterCommand command)
        {
            updateDefaults(command);

            var repository = ErsFactory.ersCtsFAQFactory.GetErsCtsFAQRepository();
            var regist = ErsFactory.ersCtsFAQFactory.GetErsCtsFAQ();
            regist.OverwriteWithModel(command);
            regist.user_id = command.user_id;
            regist.active = EnumActive.Active;
            repository.Insert(regist, true);

            return new CommandResult(true);
        }

        internal void updateDefaults(IFaqRegisterCommand command)
        {
            if (command.intime == null)
                command.intime = DateTime.Now;

            if (command.utime == null)
                command.utime = DateTime.Now;

            if (command.cate1 == null)
                command.cate1 = 0;

            if (command.cate2 == null)
                command.cate2 = 0;

            if (command.cate3 == null)
                command.cate3 = 0;

            if (command.cate4 == null)
                command.cate4 = 0;

            if (command.cate5 == null)
                command.cate5 = 0;

            if (command.active == null)
                command.active = EnumActive.NonActive;
        }
    }
}