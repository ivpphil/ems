﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Faq.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Faq.Handlers
{
    public class ValidateFaq
           : IValidationHandler<IFaqRegisterCommand>
    {
        public IEnumerable<ValidationResult> Validate(IFaqRegisterCommand command)
        {
            if (command.regist)
            {
                yield return command.CheckRequired("faq_casename");
                yield return command.CheckRequired("faq_text");
                yield return command.CheckRequired("faq_text2");
            }
        }
    }
}