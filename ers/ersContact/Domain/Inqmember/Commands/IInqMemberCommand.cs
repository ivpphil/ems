﻿using System;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Inqmember.Commands
{
    public interface IInqMemberCommand : ICommand
    {
        string mcode { get; set; }
        string lname { get; }
        string fname { get; }
        string lnamek { get; }
        string fnamek { get; }
        string ccode { get; }
        string zip { get; }
        int? pref { get; set; }
        string address { get; }
        string taddress { get; }
        string maddress { get; }
        string email { get; }
        string tel { get; }
        string fax { get; }
        DateTime? birth { get; }
        EnumSex? sex { get; }
        int? job { get; }
        int? ques { get; }
        string ans { get; }
        EnumDmFlg dm_flg { get; }
        EnumOutBoundFlg out_bound_flg { get; }
        EnumPmFlg? pm_flg { get; }
        bool renew { get; }

        string memo { get; set; }
    }
}