﻿using ersContact.Domain.Inqmember.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Inqmember.Handlers
{
    public class InqMemberHandler : ICommandHandler<IInqMemberCommand>
    {
        public ICommandResult Submit(IInqMemberCommand command)
        {
            if (string.IsNullOrEmpty(command.mcode))
            {
                var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
                var member = new ErsMember();
                this.OverwriteMemberWithModel(command, member);
                member.passwd = null;
                member.pm_flg = this.EnvPM_flg();
                member.site_id = ErsFactory.ersMemberFactory.GetObtainMemberSiteIdStgy().GetSiteId();

                repository.Insert(member, true);

                command.mcode = member.mcode;

                return new CommandResult(true);
            }
            else
            {
                var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
                var members = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode, true);

                if (command.pref == null)
                {
                    command.pref = 0;
                }

                this.OverwriteMemberWithModel(command, members);
                if (string.IsNullOrEmpty(command.email))
                {
                    members.email = string.Empty;
                }

                members.ques = members.ques ?? 0;

                var oldmember = ErsFactory.ersMemberFactory.getErsMemberWithMcode(command.mcode, true);

                repository.Update(oldmember, members);

                return new CommandResult(true);
            }
            
        }

        /// <summary>
        /// コマンドの値でErsMemberオブジェクトを更新
        /// </summary>
        /// <param name="command"></param>
        /// <param name="member"></param>
        private void OverwriteMemberWithModel(IInqMemberCommand command, ErsMember member)
        {
            member.lnamek = command.lnamek;
            member.fnamek = command.fnamek;
            member.lname = command.lname;
            member.fname = command.fname;
            member.zip = command.zip;
            member.pref = command.pref;
            member.address = command.address;
            member.taddress = command.taddress;
            member.maddress = command.maddress;
            member.email = command.email;
            member.tel = command.tel;
            member.fax = command.fax;
            member.birth = command.birth;
            member.sex = command.sex;
            member.job = command.job;
            member.ques = command.ques;
            member.ans = command.ans;
            member.dm_flg = command.dm_flg;
            member.out_bound_flg = command.out_bound_flg;
            member.memo = command.memo;

            var age = ErsFactory.ersMemberFactory.GetGetAgeStgy().GetAge(member.birth);
            member.age_code = ErsFactory.ersMemberFactory.GetGetAgeCodeStgy().GetAgeCode(age);
        }

        /// <summary>
        /// 環境依存のデータセット
        /// </summary>
        public virtual EnumPmFlg EnvPM_flg()
        {
            return EnumPmFlg.CTS;  //2:CTS
        }
    }
}
