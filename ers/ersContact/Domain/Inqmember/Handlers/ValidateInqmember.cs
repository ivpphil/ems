﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Inqmember.Commands;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Domain.Inqmember.Handlers
{
    public class ValidateInqmember
           : IValidationHandler<IInqMemberCommand>
    {
        public IEnumerable<ValidationResult> Validate(IInqMemberCommand command)
        {
            if (command.renew)
            {
                yield return command.CheckRequired("lname");
                yield return command.CheckRequired("fname");
                yield return command.CheckRequired("lnamek");
                yield return command.CheckRequired("fnamek");

                foreach (var result in ErsFactory.ersMemberFactory.GetValidateBirthdayStgy().ValidateRange(command.birth))
                {
                    yield return result;
                }

                yield return ErsFactory.ersMemberFactory.GetCheckEmailRequiredStgy().Check(command.mcode, command.email);
                yield return ErsFactory.ersMemberFactory.GetCheckDuplicateEmailStgy().CheckDuplicate(command.mcode, command.email);

                //if (command.pm_flg != EnumPmFlg.CTS)
                //{
                //    if (!string.IsNullOrEmpty(command.mcode))
                //    {
                //        yield return command.CheckRequired("ques");
                //        yield return command.CheckRequired("ans");
                //    }
                //}
            }
        }
    }
}