﻿using ersContact.Domain.Inqmember.Mappables;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor.Mapper;

namespace ersContact.Domain.Inqmember.Mappers
{
    public class ClientInfoMapper
        : IMapper<IClientInfoMappable>
    {
        public void Map(IClientInfoMappable objMappable)
        {
            var member = ErsFactory.ersMemberFactory.getErsMemberWithMcode(objMappable.mcode, true);
            if (member != null)
            {
                objMappable.OverwriteWithParameter(member.GetPropertiesAsDictionary());
                objMappable.w_pref = ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(member.pref);
                objMappable.w_sex = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename, (int?)member.sex);
                objMappable.w_job = ErsFactory.ersViewServiceFactory.GetErsViewJobService().GetStringFromId(member.job);
                objMappable.w_ques = ErsFactory.ersViewServiceFactory.GetErsViewQuesService().GetStringFromId(member.ques);
                objMappable.dm_flg_desc = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.NEED, EnumCommonNameColumnName.namename, (int?)objMappable.dm_flg);
                objMappable.out_bound_flg_desc = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.NEED, EnumCommonNameColumnName.namename, (int?)objMappable.out_bound_flg);

                switch (objMappable.blacklist)
                {
                    case null:
                        objMappable.blacklistdesc = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.BlackList, EnumCommonNameColumnName.namename, (int?)EnumBlackList.Usual);
                        break;
                    case 0:
                        objMappable.blacklistdesc = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.BlackList, EnumCommonNameColumnName.namename, (int?)EnumBlackList.Grey);
                        break;
                    case 1:
                        objMappable.blacklistdesc = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.BlackList, EnumCommonNameColumnName.namename, (int?)EnumBlackList.Black);
                        break;
                }

                if (member.birth != null)
                {
                    objMappable.birthday_y = member.birth.Value.Year;
                    objMappable.birthday_m = member.birth.Value.Month;
                    objMappable.birthday_d = member.birth.Value.Day;
                }
            }
        }
    }
}