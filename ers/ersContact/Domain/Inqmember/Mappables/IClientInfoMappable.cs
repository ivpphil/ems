﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.MapperProcessor;
using jp.co.ivp.ers;

namespace ersContact.Domain.Inqmember.Mappables
{
    public interface IClientInfoMappable
        : IMappable, IErsModelBase
    {
        string mcode { get; set; }
        string w_pref { get; set; }
        string w_sex { get; set; }
        string w_job { get; set; }
        string w_ques { get; set; }
        EnumDmFlg dm_flg { get; set; }
        EnumOutBoundFlg out_bound_flg { get; set; }
        string dm_flg_desc { get; set; }
        string out_bound_flg_desc { get; set; }
        short? blacklist { get; set; }
        string blacklistdesc { get; set; }
        int? birthday_y { set; }
        int? birthday_m { set; }
        int? birthday_d { set; }
    }
}
