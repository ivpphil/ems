/**
 * Calendar Script
 * Creates a calendar widget which can be used to select the date more easily than using just a text box
 * http://www.openjs.com/scripts/ui/calendar/
 *
 * Example: 
 * <input type="text" name="date" id="date" />
 * <script type="text/javascript">
 * 		calendar.set("date");
 * </script>
 */
calendar = {
    month_names: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
    weekdays: ["日", "月", "火", "水", "木", "金", "土"],
    month_days: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
    //Get today's date - year, month, day and date
    today: new Date(),
    opt: {},
    data: [],
    radioId: "",
    add_date: 5,

    //Functions
    /// Used to create HTML in a optimized way.
    wrt: function (txt) {
        this.data.push(txt);
    },

    /* Inspired by http://www.quirksmode.org/dom/getstyles.html */
    getStyle: function (ele, property) {
        if (ele.currentStyle) {
            var alt_property_name = property.replace(/\-(\w)/g, function (m, c) { return c.toUpperCase(); }); //background-color becomes backgroundColor
            var value = ele.currentStyle[property] || ele.currentStyle[alt_property_name];

        } else if (window.getComputedStyle) {
            property = property.replace(/([A-Z])/g, "-$1").toLowerCase(); //backgroundColor becomes background-color

            var value = document.defaultView.getComputedStyle(ele, null).getPropertyValue(property);
        }

        //Some properties are special cases
        if (property == "opacity" && ele.filter) value = (parseFloat(ele.filter.match(/opacity\=([^)]*)/)[1]) / 100);
        else if (property == "width" && isNaN(value)) value = ele.clientWidth || ele.offsetWidth;
        else if (property == "height" && isNaN(value)) value = ele.clientHeight || ele.offsetHeight;
        return value;
    },
    getPosition: function (ele) {
        var x = 0;
        var y = 0;
        while (ele) {
            x += ele.offsetLeft;
            y += ele.offsetTop;
            ele = ele.offsetParent;
        }
        if (navigator.userAgent.indexOf("Mac") != -1 && typeof document.body.leftMargin != "undefined") {
            x += document.body.leftMargin;
            offsetTop += document.body.topMargin;
        }

        var xy = new Array(x, y);
        return xy;
    },
    /// Called when the user clicks on a date in the calendar.
    selectDate: function (year, month, day) {
        var ths = _calendar_active_instance;
        //document.getElementById(ths.opt["input"]).value = year + "-" + month + "-" + day; // Date format is :HARDCODE:
        //document.getElementById(ths.opt["input"]).value = month + "/" + day + "/" + year; // Date format is :HARDCODE:
        document.getElementById(ths.opt["input"]).value = year + "/" + month + "/" + day; // Date format is :HARDCODE:
		//20120719 modified
        $("#firstTime1", $("#" + this.radioId).parent()).attr("checked", true);
        $("#fixed", $("#" + this.radioId).parent().parent()).attr("checked", true);
        ths.hideCalendar();
        //document.getElementById(ths.opt["input"]).onchange();
    },
    /// Creates a calendar with the date given in the argument as the selected date.
    makeCalendar: function (year, month, day) {
        if (!day) { day = 1; }
        year = parseInt(year);
        month = parseInt(month);
        day = parseInt(day);

        //Display the table
        var next_month = month + 1;
        var next_month_year = year;
        if (next_month > 12) {
            next_month = 1;
            next_month_year++;
        }

        var previous_month = month - 1;
        var previous_month_year = year;
        if (previous_month < 1) {
            previous_month = 12;
            previous_month_year--;
        }

        this.wrt("<table>");
        this.wrt("<tr><th><a href='javascript:calendar.makeCalendar(" + (previous_month_year) + "," + (previous_month) + ");' title='" + this.month_names[previous_month] + " " + (previous_month_year) + "'>&lt;</a></th>");
        this.wrt("<th colspan='5' class='calendar-title'><select name='calendar-month' class='calendar-month' onChange='calendar.makeCalendar(" + year + ",this.value);'>");
        for (var i in this.month_names) {
            this.wrt("<option value='" + eval(eval(i) + 1) + "'");
            if ((eval(i) + 1) == month) this.wrt(" selected='selected'");
            this.wrt(">" + this.month_names[i] + "</option>");
        }
        this.wrt("</select>");
        this.wrt("<select name='calendar-year' class='calendar-year' onChange='calendar.makeCalendar(this.value, " + month + ");'>");
        var current_year = this.today.getYear();
        if (current_year < 1900) current_year += 1900;

        for (var i = current_year - 20; i < current_year + 20; i++) {
            this.wrt("<option value='" + i + "'")
            if (i == year) this.wrt(" selected='selected'");
            this.wrt(">" + i + "</option>");
        }
        this.wrt("</select></th>");
        this.wrt("<th><a href='javascript:calendar.makeCalendar(" + (next_month_year) + "," + (next_month) + ");' title='" + this.month_names[next_month] + " " + (next_month_year) + "'>&gt;</a></th></tr>");
        this.wrt("<tr class='header'>");
        for (var weekday = 0; weekday < 7; weekday++) this.wrt("<td>" + this.weekdays[weekday] + "</td>");
        this.wrt("</tr>");

        //Get the first day of this month
        var first_day = new Date(year, month - 1, 1);
        var start_day = first_day.getDay();

        var d = 1;
        var flag = 0;

        //Leap year support
        if (year % 4 == 0) this.month_days[1] = 29;
        else this.month_days[1] = 28;

        var days_in_this_month = this.month_days[month - 1];

        //Create the calender
        for (var i = 0; i <= 5; i++) {
            if (w >= days_in_this_month) break;
            this.wrt("<tr>");
            for (var j = 0; j < 7; j++) {
                if (d > days_in_this_month) flag = 0; //If the days has overshooted the number of days in this month, stop writing
                else if (j >= start_day && !flag) flag = 1; //If the first day of this month has come, start the date writing

                if (flag) {
                    var w = d, mon = month;
                    if (w < 10) w = "0" + w;
                    if (mon < 10) mon = "0" + mon;

                    //Is it today?
                    var class_name = '';
                    var yea = this.today.getYear();
                    if (yea < 1900) yea += 1900;

                    //                    if (yea == year && this.today.getMonth() == month && this.today.getDate() == d) class_name = " today";
                    if (day == d) class_name += " selected";

                    class_name += " " + this.weekdays[j].toLowerCase();

                    this.wrt("<td class='days" + class_name + "'><a href='javascript:calendar.selectDate(\"" + year + "\",\"" + mon + "\",\"" + w + "\")'>" + w + "</a></td>");
                    d++;
                } else {
                    this.wrt("<td class='days'>&nbsp;</td>");
                }
            }
            this.wrt("</tr>");
        }
        this.wrt("</table>");
        this.wrt("<center><input type='button' value='Cancel' class='calendar-cancel' onclick='calendar.hideCalendar();' /></center>");

        document.getElementById(this.opt['calendar']).innerHTML = this.data.join("");
        this.data = [];
    },

    /// Display the calendar - if a date exists in the input box, that will be selected in the calendar.
    showCalendar: function (add_date) {
        var input = document.getElementById(this.opt['input']);

        //Position the div in the correct location...
        var div = document.getElementById(this.opt['calendar']);
        var xy = this.getPosition(input);
        //var width = parseInt(this.getStyle(input, 'width'));
        var height = parseInt(this.getStyle(input, 'height'));
        div.style.left = (xy[0]) + "px";
        div.style.top = (xy[1] + height + 5) + "px";

        // Show the calendar with the date in the input as the selected date
        var date_in_input = input.value;

        if (date_in_input) {
            var selected_date = false;
            var date_parts = date_in_input.split("/");
            if (date_parts.length == 3) {
                selected_date = new Date(date_in_input);
            }
            if (selected_date && !isNaN(selected_date.getYear())) { //Valid date.
                existing_date = selected_date;
            }
            var the_month = existing_date.getMonth() + 1;
        } else {
            var existing_date = new Date();
            //5日後
            //existing_date = new Date(existing_date.getYear() + 1900, existing_date.getMonth() + 1, existing_date.getDate() + 5);
            existing_date = new Date(existing_date.getYear() + 1900, existing_date.getMonth(), existing_date.getDate() + add_date);
            var the_month = existing_date.getMonth() + 1;
        }

        var the_year = existing_date.getYear();
        if (the_year < 1900) the_year += 1900;
        var the_date = existing_date.getDate();
        this.makeCalendar(the_year, the_month, the_date);
        document.getElementById(this.opt['calendar']).style.display = "block";
        _calendar_active_instance = this;
    },

    /// Hides the currently show calendar.
    hideCalendar: function (instance) {
        var active_calendar_id = "";
        if (instance) active_calendar_id = instance.opt['calendar'];
        else active_calendar_id = _calendar_active_instance.opt['calendar'];

        if (active_calendar_id) document.getElementById(active_calendar_id).style.display = "none";
        _calendar_active_instance = {};
    },

    /// Setup a text input box to be a calendar box.
    set: function (input_id) {
        var input = document.getElementById(input_id);
        if (!input) return; //If the input field is not there, exit.

        if (!this.opt['calendar']) this.init();

        var ths = this;
        input.onclick = function () {
            ths.opt['input'] = this.id;
            ths.showCalendar(5);
        };

		//20120719追加
		ths.radioId = input_id;
    },

    set_for_search: function (input_id, days) {
        var input = document.getElementById(input_id);
        if (!input) return; //If the input field is not there, exit.

        if (!this.opt['calendar']) this.init();

        var ths = this;
        input.onclick = function () {
            ths.opt['input'] = this.id;
            ths.showCalendar(days);
        };

        //20120719追加
        ths.radioId = input_id;
    },

    /// Will be called once when the first input is set.
    init: function () {
        if (!this.opt['calendar'] || !document.getElementById(this.opt['calendar'])) {
            var div = document.createElement('div');
            if (!this.opt['calendar']) this.opt['calendar'] = 'calender_div_' + Math.round(Math.random() * 100);

            div.setAttribute('id', this.opt['calendar']);
            div.className = "calendar-box";

            document.getElementsByTagName("body")[0].insertBefore(div, document.getElementsByTagName("body")[0].firstChild);
        }
    }
}
