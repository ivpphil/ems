/**
 * Calendar Script
 * Creates a calendar widget which can be used to select the date more easily than using just a text box
 * http://www.openjs.com/scripts/ui/calendar/
 *
 * Example: 
 * <input type="text" name="date" id="date" />
 * <script type="text/javascript">
 * 		calendar.set("date");
 * </script>
 */
calendar_delvy = {
    month_names: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
    weekdays: ["日", "月", "火", "水", "木", "金", "土"],
    month_days: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
    //Get today's date - year, month, day and date
    today: new Date(),
    opt: {},
    data: [],
    radioId: "",
    add_date: 5,
    holiday: {},

    //Functions
    /// Used to create HTML in a optimized way.
    wrt: function (txt) {
        this.data.push(txt);
    },

    /* Inspired by http://www.quirksmode.org/dom/getstyles.html */
    getStyle: function (ele, property) {
        if (ele.currentStyle) {
            var alt_property_name = property.replace(/\-(\w)/g, function (m, c) { return c.toUpperCase(); }); //background-color becomes backgroundColor
            var value = ele.currentStyle[property] || ele.currentStyle[alt_property_name];

        } else if (window.getComputedStyle) {
            property = property.replace(/([A-Z])/g, "-$1").toLowerCase(); //backgroundColor becomes background-color

            var value = document.defaultView.getComputedStyle(ele, null).getPropertyValue(property);
        }

        //Some properties are special cases
        if (property == "opacity" && ele.filter) value = (parseFloat(ele.filter.match(/opacity\=([^)]*)/)[1]) / 100);
        else if (property == "width" && isNaN(value)) value = ele.clientWidth || ele.offsetWidth;
        else if (property == "height" && isNaN(value)) value = ele.clientHeight || ele.offsetHeight;
        return value;
    },
    getPosition: function (ele) {
        var x = 0;
        var y = 0;
        while (ele) {
            x += ele.offsetLeft;
            y += ele.offsetTop;
            ele = ele.offsetParent;
        }
        if (navigator.userAgent.indexOf("Mac") != -1 && typeof document.body.leftMargin != "undefined") {
            x += document.body.leftMargin;
            offsetTop += document.body.topMargin;
        }

        var xy = new Array(x, y);
        return xy;
    },
    /// Called when the user clicks on a date in the calendar.
    selectDate: function (year, month, day) {
        var dateFormat = new DateFormat("yyyy/MM/dd");

        var ths = _calendar_active_instance;
        var result = false
        var chkdate = new Date(year + "/" + month + "/" + day);

        var limitdate = new Date("2000/01/01");
        while (!result) {
            var str = dateFormat.format(chkdate);
            if (holiday.indexOf(str) >= 0) {
                result = false;
                chkdate.setDate(chkdate.getDate() - 1);
                if (chkdate < limitdate) {
                    result = true;
                    //リミットまで配送可能日がない日は、設定された値をイキとする。
                    chkdate = new Date(year + "/" + month + "/" + day);
                }
            } else {
                result = true;
            }
        }
        year = chkdate.getFullYear();
        month = chkdate.getMonth() + 1;
        day = chkdate.getDate();

        //document.getElementById(ths.opt["input"]).value = year + "-" + month + "-" + day; // Date format is :HARDCODE:
        //document.getElementById(ths.opt["input"]).value = month + "/" + day + "/" + year; // Date format is :HARDCODE:
        document.getElementById(ths.opt["input"]).value = year + "/" + month + "/" + day; // Date format is :HARDCODE:
        //20120719 modified
        $("#firstTime1", $("#" + this.radioId).parent()).attr("checked", true);
        $("#fixed", $("#" + this.radioId).parent().parent()).attr("checked", true);
        ths.hideCalendar();
        //document.getElementById(ths.opt["input"]).onchange();
    },
    /// Creates a calendar with the date given in the argument as the selected date.
    makeCalendar: function (year, month, day) {
        if (!day) { day = 1; }
        year = parseInt(year);
        month = parseInt(month);
        day = parseInt(day);

        //Display the table
        var next_month = month + 1;
        var next_month_year = year;
        if (next_month > 12) {
            next_month = 1;
            next_month_year++;
        }

        var previous_month = month - 1;
        var previous_month_year = year;
        if (previous_month < 1) {
            previous_month = 12;
            previous_month_year--;
        }

        this.wrt("<table>");
        this.wrt("<tr><th><a href='javascript:calendar_delvy.makeCalendar(" + (previous_month_year) + "," + (previous_month) + ");' title='" + this.month_names[previous_month] + " " + (previous_month_year) + "'>&lt;</a></th>");
        this.wrt("<th colspan='5' class='calendar-title'><select name='calendar-month' class='calendar-month' onChange='calendar_delvy.makeCalendar(" + year + ",this.value);'>");
        for (var i in this.month_names) {
            this.wrt("<option value='" + eval(eval(i) + 1) + "'");
            if ((eval(i) + 1) == month) this.wrt(" selected='selected'");
            this.wrt(">" + this.month_names[i] + "</option>");
        }
        this.wrt("</select>");
        this.wrt("<select name='calendar-year' class='calendar-year' onChange='calendar_delvy.makeCalendar(this.value, " + month + ");'>");
        var current_year = this.today.getYear();
        if (current_year < 1900) current_year += 1900;

        for (var i = current_year - 20; i < current_year + 20; i++) {
            this.wrt("<option value='" + i + "'")
            if (i == year) this.wrt(" selected='selected'");
            this.wrt(">" + i + "</option>");
        }
        this.wrt("</select></th>");
        this.wrt("<th><a href='javascript:calendar_delvy.makeCalendar(" + (next_month_year) + "," + (next_month) + ");' title='" + this.month_names[next_month] + " " + (next_month_year) + "'>&gt;</a></th></tr>");
        this.wrt("<tr class='header'>");
        for (var weekday = 0; weekday < 7; weekday++) this.wrt("<td>" + this.weekdays[weekday] + "</td>");
        this.wrt("</tr>");

        //Get the first day of this month
        var first_day = new Date(year, month - 1, 1);
        var start_day = first_day.getDay();

        var d = 1;
        var flag = 0;

        //Leap year support
        if (year % 4 == 0) this.month_days[1] = 29;
        else this.month_days[1] = 28;

        var days_in_this_month = this.month_days[month - 1];

        //Create the calender
        for (var i = 0; i <= 5; i++) {
            if (w >= days_in_this_month) break;
            this.wrt("<tr>");
            for (var j = 0; j < 7; j++) {
                if (d > days_in_this_month) flag = 0; //If the days has overshooted the number of days in this month, stop writing
                else if (j >= start_day && !flag) flag = 1; //If the first day of this month has come, start the date writing

                if (flag) {
                    var w = d, mon = month;
                    if (w < 10) w = "0" + w;
                    if (mon < 10) mon = "0" + mon;

                    //Is it today?
                    var class_name = '';
                    var yea = this.today.getYear();
                    if (yea < 1900) yea += 1900;

                    //                    if (yea == year && this.today.getMonth() == month && this.today.getDate() == d) class_name = " today";
                    if (day == d) class_name += " selected";

                    class_name += " " + this.weekdays[j].toLowerCase();

                    this.wrt("<td class='days" + class_name + "'><a href='javascript:calendar_delvy.selectDate(\"" + year + "\",\"" + mon + "\",\"" + w + "\")'>" + w + "</a></td>");
                    d++;
                } else {
                    this.wrt("<td class='days'>&nbsp;</td>");
                }
            }
            this.wrt("</tr>");
        }
        this.wrt("</table>");
        this.wrt("<center><input type='button' value='Cancel' class='calendar-cancel' onclick='calendar_delvy.hideCalendar();' /></center>");

        document.getElementById(this.opt['calendar']).innerHTML = this.data.join("");
        this.data = [];
    },

    /// Display the calendar - if a date exists in the input box, that will be selected in the calendar.
    showCalendar: function (add_date) {
        var input = document.getElementById(this.opt['input']);

        //Position the div in the correct location...
        var div = document.getElementById(this.opt['calendar']);
        var xy = this.getPosition(input);
        //var width = parseInt(this.getStyle(input, 'width'));
        var height = parseInt(this.getStyle(input, 'height'));
        div.style.left = (xy[0]) + "px";
        div.style.top = (xy[1] + height + 5) + "px";

        // Show the calendar with the date in the input as the selected date
        var date_in_input = input.value;

        if (date_in_input) {
            var selected_date = false;
            var date_parts = date_in_input.split("/");
            if (date_parts.length == 3) {
                selected_date = new Date(date_in_input);
            }
            if (selected_date && !isNaN(selected_date.getYear())) { //Valid date.
                existing_date = selected_date;
            }
            var the_month = existing_date.getMonth() + 1;
        } else {
            var existing_date = new Date();
            //5日後
            //existing_date = new Date(existing_date.getYear() + 1900, existing_date.getMonth() + 1, existing_date.getDate() + 5);
            existing_date = new Date(existing_date.getYear() + 1900, existing_date.getMonth(), existing_date.getDate() + add_date);
            var the_month = existing_date.getMonth() + 1;
        }

        var the_year = existing_date.getYear();
        if (the_year < 1900) the_year += 1900;
        var the_date = existing_date.getDate();
        this.makeCalendar(the_year, the_month, the_date);
        document.getElementById(this.opt['calendar']).style.display = "block";
        _calendar_active_instance = this;
    },

    /// Hides the currently show calendar.
    hideCalendar: function (instance) {
        var active_calendar_id = "";
        if (instance) active_calendar_id = instance.opt['calendar'];
        else active_calendar_id = _calendar_active_instance.opt['calendar'];

        if (active_calendar_id) document.getElementById(active_calendar_id).style.display = "none";
        _calendar_active_instance = {};
    },

    /// Setup a text input box to be a calendar box.
    set: function (input_id, holidays) {
        holiday = holidays;

        var input = document.getElementById(input_id);
        if (!input) return; //If the input field is not there, exit.

        if (!this.opt['calendar']) this.init();

        var ths = this;
        input.onclick = function () {
            ths.opt['input'] = this.id;
            ths.showCalendar(5);
        };

        //20120719追加
        ths.radioId = input_id;
    },

    set_for_search: function (input_id, days) {
        var input = document.getElementById(input_id);
        if (!input) return; //If the input field is not there, exit.

        if (!this.opt['calendar']) this.init();

        var ths = this;
        input.onclick = function () {
            ths.opt['input'] = this.id;
            ths.showCalendar(days);
        };

        //20120719追加
        ths.radioId = input_id;
    },

    /// Will be called once when the first input is set.
    init: function () {
        if (!this.opt['calendar'] || !document.getElementById(this.opt['calendar'])) {
            var div = document.createElement('div');
            if (!this.opt['calendar']) this.opt['calendar'] = 'calender_div_' + Math.round(Math.random() * 100);

            div.setAttribute('id', this.opt['calendar']);
            div.className = "calendar-box";

            document.getElementsByTagName("body")[0].insertBefore(div, document.getElementsByTagName("body")[0].firstChild);
        }
    }
}

var DateFormat = function (pattern) {
    this._init(pattern);
};

DateFormat.prototype = {
    _init: function (pattern) {

        this.pattern = pattern;
        this._patterns = [];

        for (var i = 0; i < pattern.length; i++) {
            var ch = pattern.charAt(i);
            if (this._patterns.length == 0) {
                this._patterns[0] = ch;
            } else {
                var index = this._patterns.length - 1;
                if (this._patterns[index].charAt(0) == "'") {
                    if (this._patterns[index].length == 1
             || this._patterns[index].charAt(this._patterns[index].length - 1) != "'") {
                        this._patterns[index] += ch;
                    } else {
                        this._patterns[index + 1] = ch;
                    }
                } else if (this._patterns[index].charAt(0) == ch) {
                    this._patterns[index] += ch;
                } else {
                    this._patterns[index + 1] = ch;
                }
            }
        }
    },

    format: function (date) {

        var result = [];
        for (var i = 0; i < this._patterns.length; i++) {
            result[i] = this._formatWord(date, this._patterns[i]);
        }
        return result.join('');
    },
    _formatWord: function (date, pattern) {

        var formatter = this._formatter[pattern.charAt(0)];
        if (formatter) {
            return formatter.apply(this, [date, pattern]);
        } else {
            return pattern;
        }
    },
    _formatter: {
        "y": function (date, pattern) {
            // Year
            var year = String(date.getFullYear());
            if (pattern.length <= 2) {
                year = year.substring(2, 4);
            } else {
                year = this._zeroPadding(year, pattern.length);
            }
            return year;
        },
        "M": function (date, pattern) {
            // Month in year
            return this._zeroPadding(String(date.getMonth() + 1), pattern.length);
        },
        "d": function (date, pattern) {
            // Day in month
            return this._zeroPadding(String(date.getDate()), pattern.length);
        },
        "H": function (date, pattern) {
            // Hour in day (0-23)
            return this._zeroPadding(String(date.getHours()), pattern.length);
        },
        "m": function (date, pattern) {
            // Minute in hour
            return this._zeroPadding(String(date.getMinutes()), pattern.length);
        },
        "s": function (date, pattern) {
            // Second in minute
            return this._zeroPadding(String(date.getSeconds()), pattern.length);
        },
        "S": function (date, pattern) {
            // Millisecond
            return this._zeroPadding(String(date.getMilliseconds()), pattern.length);
        },
        "'": function (date, pattern) {
            // escape
            if (pattern == "''") {
                return "'";
            } else {
                return pattern.replace(/'/g, '');
            }
        }
    },

    _zeroPadding: function (str, length) {
        if (str.length >= length) {
            return str;
        }

        return new Array(length - str.length + 1).join("0") + str;
    },


    /// Parser ///
    parse: function (text) {

        if (typeof text != 'string' || text == '') return null;

        var result = { year: 1970, month: 1, day: 1, hour: 0, min: 0, sec: 0, msec: 0 };

        for (var i = 0; i < this._patterns.length; i++) {
            if (text == '') return null; // parse error!!
            text = this._parseWord(text, this._patterns[i], result);
            if (text === null) return null; // parse error!!
        }
        if (text != '') return null; // parse error!!

        return new Date(
                result.year,
                result.month - 1,
                result.day,
                result.hour,
                result.min,
                result.sec,
                result.msec);
    },
    _parseWord: function (text, pattern, result) {

        var parser = this._parser[pattern.charAt(0)];
        if (parser) {
            return parser.apply(this, [text, pattern, result]);
        } else {
            if (text.indexOf(pattern) != 0) {
                return null;
            } else {
                return text.substring(pattern.length);
            }
        }
    },
    _parser: {
        "y": function (text, pattern, result) {
            // Year
            var year;
            if (pattern.length <= 2) {
                year = text.substring(0, 2);
                year = year < 70 ? '20' + year : '19' + year;
                text = text.substring(2);
            } else {
                var length = (pattern.length == 3) ? 4 : pattern.length;
                year = text.substring(0, length);
                text = text.substring(length);
            }
            if (!this._isNumber(year)) return null; // error
            result.year = parseInt(year, 10);
            return text;
        },
        "M": function (text, pattern, result) {
            // Month in year
            var month;
            if (pattern.length == 1 && text.length > 1
          && text.substring(0, 2).match(/1[0-2]/) != null) {
                month = text.substring(0, 2);
                text = text.substring(2);
            } else {
                month = text.substring(0, pattern.length);
                text = text.substring(pattern.length);
            }
            if (!this._isNumber(month)) return null; // error
            result.month = parseInt(month, 10);
            return text;
        },
        "d": function (text, pattern, result) {
            // Day in month
            var day;
            if (pattern.length == 1 && text.length > 1
          && text.substring(0, 2).match(/1[0-9]|2[0-9]|3[0-1]/) != null) {
                day = text.substring(0, 2);
                text = text.substring(2);
            } else {
                day = text.substring(0, pattern.length);
                text = text.substring(pattern.length);
            }
            if (!this._isNumber(day)) return null; // error
            result.day = parseInt(day, 10);
            return text;
        },
        "H": function (text, pattern, result) {
            // Hour in day (0-23)
            var hour;
            if (pattern.length == 1 && text.length > 1
          && text.substring(0, 2).match(/1[0-9]|2[0-3]/) != null) {
                hour = text.substring(0, 2);
                text = text.substring(2);
            } else {
                hour = text.substring(0, pattern.length);
                text = text.substring(pattern.length);
            }
            if (!this._isNumber(hour)) return null; // error
            result.hour = parseInt(hour, 10);
            return text;
        },
        "m": function (text, pattern, result) {
            // Minute in hour
            var min;
            if (pattern.length == 1 && text.length > 1
          && text.substring(0, 2).match(/[1-5][0-9]/) != null) {
                min = text.substring(0, 2);
                text = text.substring(2);
            } else {
                min = text.substring(0, pattern.length);
                text = text.substring(pattern.length);
            }
            if (!this._isNumber(min)) return null; // error
            result.min = parseInt(min, 10);
            return text;
        },
        "s": function (text, pattern, result) {
            // Second in minute
            var sec;
            if (pattern.length == 1 && text.length > 1
          && text.substring(0, 2).match(/[1-5][0-9]/) != null) {
                sec = text.substring(0, 2);
                text = text.substring(2);
            } else {
                sec = text.substring(0, pattern.length);
                text = text.substring(pattern.length);
            }
            if (!this._isNumber(sec)) return null; // error
            result.sec = parseInt(sec, 10);
            return text;
        },
        "S": function (text, pattern, result) {
            // Millimsecond
            var msec;
            if (pattern.length == 1 || pattern.length == 2) {
                if (text.length > 2 && text.substring(0, 3).match(/[1-9][0-9][0-9]/) != null) {
                    msec = text.substring(0, 3);
                    text = text.substring(3);
                } else if (text.length > 1 && text.substring(0, 2).match(/[1-9][0-9]/) != null) {
                    msec = text.substring(0, 2);
                    text = text.substring(2);
                } else {
                    msec = text.substring(0, pattern.length);
                    text = text.substring(pattern.length);
                }
            } else {
                msec = text.substring(0, pattern.length);
                text = text.substring(pattern.length);
            }
            if (!this._isNumber(msec)) return null; // error
            result.msec = parseInt(msec, 10);
            return text;
        },
        "'": function (text, pattern, result) {
            // escape
            if (pattern == "''") {
                pattern = "'";
            } else {
                pattern = pattern.replace(/'/g, '');
            }
            if (text.indexOf(pattern) != 0) {
                return null; // error
            } else {
                return text.substring(pattern.length);
            }
        }
    },

    _isNumber: function (str) {
        return /^[0-9]*$/.test(str);
    }
}
