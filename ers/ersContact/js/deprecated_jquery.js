var ErsDeprecatedJquery = function () {

    var that = {};

    that.Init = function () {
        (function ($) {         

            $.browser = {
                msie: ErsLib.isIE() || ErsLib.getBrowser() === "Other",
                opera: ErsLib.getBrowser() === "Opera",
                mozilla: ErsLib.getBrowser() === "FX",
                safari: ErsLib.getBrowser() === "Safari" || ErsLib.getBrowser() === "Chrome"
            }

            if($.browser.msie){
                $.browser.version = checkV(window.navigator.userAgent.toLowerCase()).version;
            }

        })(jQuery);
    }

    return that;


}

var checkV = function (ver) {r = /(webkit)[ \/]([\w.]+)/; s = /(opera)(?:.*version)?[ \/]([\w.]+)/; t = /(msie) ([\w.]+)/; u = /(mozilla)(?:.*? rv:([\w.]+))?/; ver = ver.toLowerCase(); var b = r.exec(ver) || s.exec(ver) || t.exec(ver) || ver.indexOf("compatible") < 0 && u.exec(ver) || []; return { browser: b[1] || "", version: b[2] || "0"} }

ErsDeprecatedJquery().Init();