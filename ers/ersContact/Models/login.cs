﻿using ersContact.Domain.Login.Commands;
using ersContact.Domain.Login.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;

namespace ersContact.Models
{
    public class Login
        : ErsContactModelBase, ILoginCommand, IAgentMappable
    {
        [HtmlSubmitButton]
        public bool sessionError { get; set; }

        [ErsSchemaValidation("cts_login_t.user_id")]
        public string user_id { get; set; }

        [ErsSchemaValidation("cts_login_t.passwd")]
        public string passwd { get; set; }

        [ErsSchemaValidation("cts_login_t.id")]
        public int? cts_user_id  { get; set; }

        [ErsSchemaValidation("cts_login_t.authority")]
        public string authority { get; set; }

        [ErsSchemaValidation("ransu_admin_t.user_cd")]
        public string user_cd { get; set; }
    }
}