﻿using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using ersContact.Domain.Configuration.Commands;

namespace ersContact.Models.configuration
{
    public class CtsConfigCategoryListData
        : ErsBindableModel, ICtsCategoryUpdateRecordCommand
    {
        [ErsSchemaValidation("common_namecode_t.id")]
        public int? id { get; set; }

        [ErsSchemaValidation("common_namecode_t.code")]
        public int? code { get; set; }

        [ErsSchemaValidation("common_namecode_t.active")]
        public EnumActive active { get; set; }

        [ErsSchemaValidation("common_namecode_t.disp_order")]
        public int? disp_order { get; set; }

        [ErsSchemaValidation("common_namecode_t.namename")]
        public string namename { get; set; }

        [HtmlSubmitButton]
        public bool delete { get; set; }

        [BindTable("categoryList")]
        public List<CtsConfigCategoryListData> categoryList { get; set; }
    }
}