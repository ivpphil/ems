﻿using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersContact.Models
{
    public class CtsConfig
        : ErsContactModelBase
    {
        [ErsSchemaValidation("cts_faq_template_t.active")]
        public virtual int replace { get; set; }
    }
}
