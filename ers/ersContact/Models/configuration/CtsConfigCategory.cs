﻿using System.Collections.Generic;
using ersContact.Domain.Configuration.Mappables;
using ersContact.jp.co.ivp.ers.mvc;

namespace ersContact.Models
{
    public class CtsConfigCategory
        : ErsContactModelBase, ICtsCategoryListMappable
    {
        public List<Dictionary<string, object>> categoryList { get; set; }
    }
}
