﻿using System.Collections.Generic;
using ersContact.Domain.Configuration.Commands;
using ersContact.Domain.Configuration.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using ersContact.Models.configuration;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersContact.Models
{
    public class CtsConfigCategoryManage
        : ErsContactModelBase, ICtsCategoryUpdateCommand, ICtsCategoryMappable
    {
        [ErsSchemaValidation("common_namecode_t.id")]
        public virtual int? id_main { get; set; }

        [ErsSchemaValidation("common_namecode_t.namename")]
        public virtual string namename_main { get; set; }

        [ErsSchemaValidation("common_namecode_t.active")]
        public virtual EnumActive active { get; set; }

        [ErsSchemaValidation("common_namecode_t.type_code")]
        public EnumCtsEnquiryCategoryType type_code { get; set; }

        [ErsSchemaValidation("common_namecode_t.disp_order")]
        public virtual int disp_order { get; set; }

        [ErsSchemaValidation("common_namecode_t.namename")]
        public virtual string namename { get; set; }

        [HtmlSubmitButton]
        public bool batch { get; set; }

        [HtmlSubmitButton]
        public bool modify { get; set; }

        [BindTable("categoryList")]
        public List<CtsConfigCategoryListData> categoryList { get; set; }
    }
}
