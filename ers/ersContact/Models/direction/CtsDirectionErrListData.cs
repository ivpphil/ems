﻿using System.Collections.Generic;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System;
using ersContact.Domain.Direction.Mappables;

namespace ersContact.Models
{
    public class CtsDirectionErrListData
        : ErsBindableModel
    {
        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.id")]
        public int? id { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.case_no")]
        public int? case_no { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.mcode")]
        public string mcode { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.intime")]
        public DateTime? intime { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.utime")]
        public DateTime? utime { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.enq_progress")]
        public EnumEnqProgress? enq_progress { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_progress")]
        public string enq_progress_name { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_casename")]
        public string enq_casename { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.user_id")]
        public string user_id { get; set; }

        [ErsSchemaValidation("cts_login_t.ag_name")]
        public string user_ag_name { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.esc_id")]
        public string esc_id { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.esc_id")]
        public string modify_esc_id { get; set; }

        [ErsSchemaValidation("cts_login_t.ag_name")]
        public string esc_ag_name { get; set; }

        [BindTable("cardErrList")]
        public List<CtsDirectionErrListData> cardErrList { get; set; }

        public bool changable { get { return this.esc_ag_name.HasValue(); } }
    }
}