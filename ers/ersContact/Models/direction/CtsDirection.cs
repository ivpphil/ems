﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Direction.Commands;
using ersContact.Domain.Direction.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers;

namespace ersContact.Models
{
    public class CtsDirection
        : ErsContactModelBase
        , IDirectionListMappable, IInstructionListMappable
        , IInboundListMappable, IInboundInstructionListMappable, IInboundNowListMappable
        , IInboundAllListMappable, IDirectionRegisterCommand, IInstructionUpdateCommand
        , ICardErrListMappable, ICardErrListUpdateCommand
    {
        #region Field Declarations
        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsDirectionsmanagementListItemNumberOnPage; } }

        public long cardErr_maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsDirectionscarderrListItemNumberOnPage; } }

        public long recordCount { get; set; }

        public List<Dictionary<string, object>> ctsAgetypeList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentTypeService().SelectAsList(); }
        }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long pagerPageCount { get; set; }

        public int? ctsUserID { get; set; }

        [ErsSchemaValidation("cts_directions_management_t.id")]
        public virtual int? id { get; set; }

        [ErsSchemaValidation("cts_directions_management_t.case_no")]
        public virtual int? case_no { get; set; }

        [ErsSchemaValidation("cts_directions_management_t.sub_no")]
        public virtual int sub_no { get; set; }

        [ErsSchemaValidation("cts_directions_management_t.to_user_id")]
        public virtual int? to_user_id { get; set; }

        [ErsSchemaValidation("cts_directions_management_t.from_user_id")]
        public virtual int? from_user_id { get; set; }

        [ErsSchemaValidation("cts_directions_management_t.intime")]
        public virtual DateTime? intime { get; set; }

        [ErsSchemaValidation("cts_directions_management_t.utime")]
        public virtual DateTime? utime { get; set; }

        [ErsSchemaValidation("cts_directions_management_t.check_s")]
        public virtual short check_s { get; set; }

        [ErsSchemaValidation("cts_directions_management_t.check_s")]
        public virtual string check_desc { get; set; }

        [ErsSchemaValidation("cts_directions_management_t.dmemo")]
        public virtual string dmemo { get; set; }
        
        [ErsSchemaValidation("cts_enquiry_t.enq_progress")]
        public EnumEnqProgress? progress { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_progress")]
        public string enq_progress { get; set; }

        [ErsOutputHidden("pager")]
        [ErsUniversalValidation]
        public string progress_namename { get; set; }

        [ErsOutputHidden("pager")]
        [ErsUniversalValidation]
        public long enq_situation_count { get; set; }

        [ErsOutputHidden("pager")]
        [ErsUniversalValidation]
        public long enq_progress_count { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_type")]
        public EnumEnqType? type { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_type")]
        public string enq_type { get; set; }
        
        [ErsSchemaValidation("cts_enquiry_t.enq_situation")]
        public EnumEnqSituation? situation { get; set; }
        
        [ErsSchemaValidation("cts_enquiry_t.enq_situation")]
        public string enq_situation { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_priorty")]
        public virtual string priority { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_casename")]
        public virtual string enq_casename { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate1")]
        public virtual string cate_name { get; set; }

        [ErsSchemaValidation("cts_login_t.ag_name")]
        public virtual string ag_name { get; set; }

        [ErsSchemaValidation("cts_login_t.ag_name")]
        public virtual string to_ag_name { get; set; }

        [ErsSchemaValidation("cts_login_t.ag_name")]
        public virtual string from_ag_name { get; set; }

        [ErsSchemaValidation("cts_login_t.ag_type")]
        public EnumAgType? ag_type { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.user_id")]
        public virtual string user_id { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.esc_id")]
        public virtual string esc_id { get; set; }

        [ErsUniversalValidation]
        public DateTime? LabelNow { get; set; }

        [ErsUniversalValidation]
        public string LabelProgress { get; set; }

        [ErsUniversalValidation]
        public bool inboundList { get; set; }

        [ErsUniversalValidation]
        public bool inboundInput { get; set; }

        [ErsUniversalValidation]
        public bool agentsearch { get; set; }

        [ErsUniversalValidation]
        public bool searchresult { get; set; }

        [ErsUniversalValidation]
        public bool showinst { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? datefrom { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string timefrom {
            get
            {
                return datefrom.GetValueOrDefault().ToString("hh:mm:ss");
            }
        }

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? dateto { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public string timeto {
            get
            {
                return dateto.GetValueOrDefault().ToString("hh:mm:ss");
            }
        }

        [ErsUniversalValidation]
        public int? source { get; set; }

        [HtmlSubmitButton]
        public bool search { get; set; }

        [HtmlSubmitButton]
        public bool register { get; set; }

        [HtmlSubmitButton]
        public bool ShowInboundStatus { get; set; }

        [HtmlSubmitButton]
        public bool ShowListNow { get; set; }

        [HtmlSubmitButton]
        public bool ShowListAll { get; set; }

        [HtmlSubmitButton]
        public bool InstRegist { get; set; }

        [HtmlSubmitButton]
        public bool Instcancel { get; set; }

        [ErsUniversalValidation]
        public bool RegistDone { get; set; }

        [ErsUniversalValidation]
        public bool ConfirmIns { get; set; }

        [ErsUniversalValidation]
        public bool carderrHasRecord { get; set; }

        [ErsUniversalValidation]
        public virtual int rawspan_typ { get; set; }

        [ErsUniversalValidation]
        public virtual int rawspan_prg { get; set; }
        #endregion

        #region List Objects
        public List<Dictionary<string, object>> NowInboundType { get; set; }

        public List<Dictionary<string, object>> NowInboundProgress { get; set; }

        public List<Dictionary<string, object>> NowInboundSituation { get; set; }

        public List<Dictionary<string, object>> AllInboundType { get; set; }

        public List<Dictionary<string, object>> AllInboundProgress { get; set; }

        public List<Dictionary<string, object>> AllInboundSituation { get; set; }

        public List<Dictionary<string, object>> InboundStatusList { get; set; }

        public List<Dictionary<string, object>> AgentInstructionList { get; set; }

        [BindTable("cardErrList")]
        public List<CtsDirectionErrListData> cardErrList { get; set; }
        #endregion

        #region Load List
        private IList<ErsCtsRepInbound> CheckProgressList(IList<ErsCtsRepInbound> listProgress)
        {
            bool[] HasClose = new bool[3];
            bool[] HasOpen = new bool[3];
            ErsCtsRepInbound r;
            foreach (var dr in listProgress)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (dr.namename == "Close" && dr.enq_type == (EnumEnqType)i + 1)
                        HasClose[i] = true;
                    if (dr.namename == "Open" && dr.enq_type == (EnumEnqType)i + 1)
                        HasOpen[i] = true;
                }
            }

            for (int i = 0; i < 3; i++)
            {
                if (!HasOpen[i])
                { r = new ErsCtsRepInbound(); r.enq_type = (EnumEnqType)i + 1; r.namename = "Open"; r.enq_progress_count = 0; listProgress.Add(r); }
                if (!HasClose[i])
                { r = new ErsCtsRepInbound(); r.enq_type = (EnumEnqType)i + 1; r.namename = "Close"; r.enq_progress_count = 0; listProgress.Add(r); }

            }

            return listProgress;
        }

        private IList<ErsCtsRepInbound> CheckSituationList(IList<ErsCtsRepInbound> listSituation)
        {
            bool[] HasUnder = new bool[5]; bool[] HasSelf = new bool[5];
            bool[] HasAG = new bool[5]; bool[] HasSV = new bool[5];
            bool[] HasClient = new bool[5];
            ErsCtsRepInbound r;
            foreach (var dr in listSituation)
            {
                for (int i = 0; i < 5; i++)
                {
                    if (dr.enq_situation == EnumEnqSituation.Correspondence && dr.enq_type == (EnumEnqType)i + 1)
                        HasUnder[i] = true;
                    if (dr.enq_situation == EnumEnqSituation.SelfSolution && dr.enq_type == (EnumEnqType)i + 1)
                        HasSelf[i] = true;
                    if (dr.enq_situation == EnumEnqSituation.AGEscalation && dr.enq_type == (EnumEnqType)i + 1)
                        HasAG[i] = true;
                    if (dr.enq_situation == EnumEnqSituation.SVEscalation && dr.enq_type == (EnumEnqType)i + 1)
                        HasSV[i] = true;
                    if (dr.enq_situation == EnumEnqSituation.ClientEscalation && dr.enq_type == (EnumEnqType)i + 1)
                        HasClient[i] = true;
                }
            }
            for (int i = 0; i < 5; i++)
            {
                if (!HasUnder[i])
                { r = new ErsCtsRepInbound(); r.enq_situation = EnumEnqSituation.Correspondence; r.enq_type = (EnumEnqType)i + 1; r.namename = "対応中"; r.enq_situation_count = 0; listSituation.Add(r); }
                if (!HasSelf[i])
                { r = new ErsCtsRepInbound(); r.enq_situation = EnumEnqSituation.SelfSolution; r.enq_type = (EnumEnqType)i + 1; r.namename = "自己解決"; r.enq_situation_count = 0; listSituation.Add(r); }
                if (!HasAG[i])
                { r = new ErsCtsRepInbound(); r.enq_situation = EnumEnqSituation.AGEscalation; r.enq_type = (EnumEnqType)i + 1; r.namename = "AGエスカレーション"; r.enq_situation_count = 0; listSituation.Add(r); }
                if (!HasSV[i])
                { r = new ErsCtsRepInbound(); r.enq_situation = EnumEnqSituation.SVEscalation; r.enq_type = (EnumEnqType)i + 1; r.namename = "SVエスカレーション"; r.enq_situation_count = 0; listSituation.Add(r); }
                if (!HasClient[i])
                { r = new ErsCtsRepInbound(); r.enq_situation = EnumEnqSituation.ClientEscalation; r.enq_type = (EnumEnqType)i + 1; r.namename = "クライアントエスカレーション"; r.enq_situation_count = 0; listSituation.Add(r); }
            }
            return listSituation;
        }
        public List<Dictionary<string, object>> pgrList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQPGR, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> agentList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentService().SelectAsList(this.situation);
            }
        }
        #endregion
    }
}
