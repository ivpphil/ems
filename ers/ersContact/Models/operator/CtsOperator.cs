﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Operator.Commands;
using ersContact.Domain.Operator.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;

namespace ersContact.Models
{
    public class CtsOperator
        : ErsContactModelBase
        , IOperatorCommand, IOperatorMappable
    {
        public long search_result_cnt { get; set; }

        [ErsSchemaValidation("cts_login_t.id")]
        public virtual int? id { get; set; }

        [ErsSchemaValidation("cts_login_t.authority")]
        public string authority { get; set; }

        [ErsSchemaValidation("cts_login_t.user_id")]
        public string user_id { get; set; }

        [ErsSchemaValidation("cts_login_t.passwd")]
        public string passwd { get; set; }

        [ErsSchemaValidation("cts_login_t.ag_name")]
        public string ag_name { get; set; }

        [ErsSchemaValidation("cts_login_t.ag_type")]
        public EnumAgType? ag_type { get; set; }

        [ErsSchemaValidation("cts_login_t.active")]
        public int? active { get; set; }

        [HtmlSubmitButton]
        public bool regist { get; set; }

        [HtmlSubmitButton]
        public bool modify { get; set; }

        [HtmlSubmitButton]
        public bool delete { get; set; }

        [HtmlSubmitButton]
        public bool done { get; set; }

        [HtmlSubmitButton]
        public bool cancel { get; set; }

        public List<Dictionary<string, object>> ctsAgetypeList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentTypeService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> operatorList { get; set; }

        public string PageLabel { get; set; }
    }
}
