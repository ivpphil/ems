﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.state.specification;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.member;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
//using jp.co.ivp.ers.strategy.contact;

namespace ersContact.Models
{
    public class index : ErsContactModelBase
        {

            //ディクショナリの設定
            public Dictionary<string, Dictionary<string, string>> dict = new Dictionary<string, Dictionary<string, string>>();

            //コンストラクタ
            public index()
            {

                DateTime dt = DateTime.Now;
                this.CurDate = dt.ToString("yyyy年MM月dd日 HH時mm分ss秒");

            }



            //現在日時(速報値)
            public string CurDate
            {
                get;
                set;
            }

            // ---------------------------------------------------
            //売上(本日)
            public object TodaySales
            {
                get;
                set;
            }

            //売上(今週)
            public object ThisWeekSales
            {
                get;
                set;
            }

            //売上(先週)
            public object LastWeekSales
            {
                get;
                set;
            }

            //売上(今月)
            public object ThisMonthSales
            {
                get;
                set;
            }

            //売上(先月)
            public object LastMonthSales
            {
                get;
                set;
            }

            //売上(今年)
            public object ThisYearSales
            {
                get;
                set;
            }

            //売上(昨年)
            public object LastYearSales
            {
                get;
                set;
            }
            // ---------------------------------------------------
            //受注(本日)
            public object TodayOrders
            {
                get;
                set;
            }

            //受注(今週)
            public object ThisWeekOrders
            {
                get;
                set;
            }

            //受注(先週)
            public object LastWeekOrders
            {
                get;
                set;
            }

            //受注(今月)
            public object ThisMonthOrders
            {
                get;
                set;
            }

            //受注(先月)
            public object LastMonthOrders
            {
                get;
                set;
            }

            //受注(今年)
            public object ThisYearOrders
            {
                get;
                set;
            }

            //受注(昨年)
            public object LastYearOrders
            {
                get;
                set;
            }
            // ---------------------------------------------------
            //キャンセル(本日)
            public int TodayCancelNum
            {
                get;
                set;
            }

            //キャンセル(今週)
            public int ThisWeekCancelNum
            {
                get;
                set;
            }

            //キャンセル(先週)
            public int LastWeekCancelNum
            {
                get;
                set;
            }

            //キャンセル(今月)
            public int ThisMonthCancelNum
            {
                get;
                set;
            }

            //キャンセル(先月)
            public int LastMonthCancelNum
            {
                get;
                set;
            }

            //キャンセル(今年)
            public int ThisYearCancelNum
            {
                get;
                set;
            }

            //キャンセル(昨年)
            public int LastYearCancelNum
            {
                get;
                set;
            }
            // ---------------------------------------------------

            //累計会員数
            public long MemberAllCount
            {
                get;
                set;
            }

            //累計ポイント発行数
            public long AllPoint
            {
                get;
                set;
            }

            //使用ポイント数
            public long UsedPoint
            {
                get;
                set;
            }

            //売上トップ5
            public object GetTop
            {
                get;
                set;
            }

            private object loadSaleData(string s_date_from, string s_date_to)
            {

                NewSaleSpecification salespecification = ErsFactory.ersOrderFactory.GetNewSaleSpecification();
                salespecification.date_from = s_date_from;
                salespecification.date_to = s_date_to;
                salespecification.isSatisfiedBy();
                return salespecification.dict;

            }

            private object loadOrderData(string s_date_from, string s_date_to)
            {

                NewOrderSpecification orderspecification = ErsFactory.ersOrderFactory.GetNewOrderSpecification();
                orderspecification.date_from = s_date_from;
                orderspecification.date_to = s_date_to;
                orderspecification.isSatisfiedBy();
                return orderspecification.dict;

            }

            private int loadCancelData(string s_date_from, string s_date_to)
            {
                int result;
                NewCancelNumSpecification canselspecification = ErsFactory.ersOrderFactory.GetNewCancelNumSpecification();
                canselspecification.date_from = s_date_from;
                canselspecification.date_to = s_date_to;
                result = canselspecification.isSatisfiedBy();
                return result;

            }

            internal void getSaleData()
            {
                //書式フォーマット
                const string format = "yyyy/MM/dd";

                //今月設定
                DateTime today = DateTime.Today;
                DateTime firstDay_s = today.AddDays(-today.Day + 1);
                DateTime endDay_s = firstDay_s.AddMonths(1).AddDays(-1);
                string firstDayText_s = firstDay_s.ToString(format);
                string endDayText_s = endDay_s.ToString(format);

                //先月設定
                DateTime today_e = DateTime.Today.AddMonths(-1);
                DateTime firstDay_e = today_e.AddDays(-today.Day + 1);
                DateTime endDay_e = firstDay_e.AddMonths(1).AddDays(-1);
                string firstDayText_e = firstDay_e.ToString(format);
                string endDayText_e = endDay_e.ToString(format);

                DateTime dtNow = DateTime.Now;
                String uWeekday = dtNow.DayOfWeek.ToString("d");

                /// 売上金額、売上件数

                /// 本日(売上)
                this.TodaySales = loadSaleData(dtNow.ToShortDateString() + " 00:00:00", dtNow.ToShortDateString() + " 23:59:59");

                /// 今週(売上)
                this.ThisWeekSales = loadSaleData(dtNow.AddDays(0 - int.Parse(uWeekday)).ToShortDateString() + " 00:00:00", dtNow.AddDays((0 - int.Parse(uWeekday)) + 6).ToShortDateString() + " 23:59:59");

                /// 先週(売上)
                this.LastWeekSales = loadSaleData(dtNow.AddDays((0 - int.Parse(uWeekday)) - 7).ToShortDateString() + " 00:00:00", dtNow.AddDays(((0 - int.Parse(uWeekday)) + 6) - 7).ToShortDateString() + " 23:59:59");

                /// 今月(売上)    
                this.ThisMonthSales = loadSaleData(firstDayText_s + " 00:00:00", endDayText_s + " 23:59:59");

                /// 先月(売上)         
                this.LastMonthSales = loadSaleData(firstDayText_e + " 00:00:00", endDayText_e + " 23:59:59");

                /// 今年(売上)         
                this.ThisYearSales = loadSaleData(dtNow.Year + "/01/01 00:00:00", dtNow.Year + "/12/31 23:59:59");

                /// 昨年(売上)         
                this.LastYearSales = loadSaleData(dtNow.AddYears(-1).Year + "/01/01 00:00:00", dtNow.AddYears(-1).Year + "/12/31 23:59:59");

            }

            internal void getOrderData()
            {
                //書式フォーマット
                const string format = "yyyy/MM/dd";

                //今月設定
                DateTime today = DateTime.Today;
                DateTime firstDay_s = today.AddDays(-today.Day + 1);
                DateTime endDay_s = firstDay_s.AddMonths(1).AddDays(-1);
                string firstDayText_s = firstDay_s.ToString(format);
                string endDayText_s = endDay_s.ToString(format);

                //先月設定
                DateTime today_e = DateTime.Today.AddMonths(-1);
                DateTime firstDay_e = today_e.AddDays(-today.Day + 1);
                DateTime endDay_e = firstDay_e.AddMonths(1).AddDays(-1);
                string firstDayText_e = firstDay_e.ToString(format);
                string endDayText_e = endDay_e.ToString(format);

                DateTime dtNow = DateTime.Now;
                String uWeekday = dtNow.DayOfWeek.ToString("d");

                /// 受注金額、受注件数

                /// 本日(受注)
                this.TodayOrders = loadOrderData(dtNow.ToShortDateString() + " 00:00:00", dtNow.ToShortDateString() + " 23:59:59");

                /// 今週(受注)
                this.ThisWeekOrders = loadOrderData(dtNow.AddDays(0 - int.Parse(uWeekday)).ToShortDateString() + " 00:00:00", dtNow.AddDays((0 - int.Parse(uWeekday)) + 6).ToShortDateString() + " 23:59:59");

                /// 先週(受注)
                this.LastWeekOrders = loadOrderData(dtNow.AddDays((0 - int.Parse(uWeekday)) - 7).ToShortDateString() + " 00:00:00", dtNow.AddDays(((0 - int.Parse(uWeekday)) + 6) - 7).ToShortDateString() + " 23:59:59");

                /// 今月(受注)    
                this.ThisMonthOrders = loadOrderData(firstDayText_s + " 00:00:00", endDayText_s + " 23:59:59");

                /// 先月(受注)         
                this.LastMonthOrders = loadOrderData(firstDayText_e + " 00:00:00", endDayText_e + " 23:59:59");

                /// 今年(受注)         
                this.ThisYearOrders = loadOrderData(dtNow.Year + "/01/01 00:00:00", dtNow.Year + "/12/31 23:59:59");

                /// 昨年(受注)         
                this.LastYearOrders = loadOrderData(dtNow.AddYears(-1).Year + "/01/01 00:00:00", dtNow.AddYears(-1).Year + "/12/31 23:59:59");

            }

            internal void getCancelData()
            {

                //書式フォーマット
                const string format = "yyyy/MM/dd";

                //今月設定
                DateTime today = DateTime.Today;
                DateTime firstDay_s = today.AddDays(-today.Day + 1);
                DateTime endDay_s = firstDay_s.AddMonths(1).AddDays(-1);
                string firstDayText_s = firstDay_s.ToString(format);
                string endDayText_s = endDay_s.ToString(format);

                //先月設定
                DateTime today_e = DateTime.Today.AddMonths(-1);
                DateTime firstDay_e = today_e.AddDays(-today.Day + 1);
                DateTime endDay_e = firstDay_e.AddMonths(1).AddDays(-1);
                string firstDayText_e = firstDay_e.ToString(format);
                string endDayText_e = endDay_e.ToString(format);

                DateTime dtNow = DateTime.Now;
                String uWeekday = dtNow.DayOfWeek.ToString("d");

                /// キャンセル数

                /// 本日(キャンセル数)
                this.TodayCancelNum = loadCancelData(dtNow.ToShortDateString() + " 00:00:00", dtNow.ToShortDateString() + " 23:59:59");

                /// 今週(キャンセル数)
                this.ThisWeekCancelNum = loadCancelData(dtNow.AddDays(0 - int.Parse(uWeekday)).ToShortDateString() + " 00:00:00", dtNow.AddDays((0 - int.Parse(uWeekday)) + 6).ToShortDateString() + " 23:59:59");

                /// 先週(キャンセル数)
                this.LastWeekCancelNum = loadCancelData(dtNow.AddDays((0 - int.Parse(uWeekday)) - 7).ToShortDateString() + " 00:00:00", dtNow.AddDays(((0 - int.Parse(uWeekday)) + 6) - 7).ToShortDateString() + " 23:59:59");

                /// 今月(キャンセル数)    
                this.ThisMonthCancelNum = loadCancelData(firstDayText_s + " 00:00:00", endDayText_s + " 23:59:59");

                /// 先月(キャンセル数)         
                this.LastMonthCancelNum = loadCancelData(firstDayText_e + " 00:00:00", endDayText_e + " 23:59:59");

                /// 今年(キャンセル数)         
                this.ThisYearCancelNum = loadCancelData(dtNow.Year + "/01/01 00:00:00", dtNow.Year + "/12/31 23:59:59");

                /// 昨年(キャンセル数)         
                this.LastYearCancelNum = loadCancelData(dtNow.AddYears(-1).Year + "/01/01 00:00:00", dtNow.AddYears(-1).Year + "/12/31 23:59:59");

            }

            internal void getAllMember()
            {
                /// 累計会員数
                NewMemberAllCountSpecification allmempspecification = ErsFactory.ersMemberFactory.GetNewMemberAllCountSpecification();
                this.MemberAllCount = allmempspecification.isSatisfiedBy();

            }

            internal void getIssuePoint()
            {
                /// 累計ﾎﾟｲﾝﾄ発行数
                NewPointSpecification allpspecification = ErsFactory.ersPointHistoryFactory.GetNewPointSpecification();
                allpspecification.Conditions = "all";
                this.AllPoint = allpspecification.isSatisfiedBy();

            }

            internal void getUsePoint()
            {
                /// 使用ポイント数
                NewPointSpecification usepspecification = ErsFactory.ersPointHistoryFactory.GetNewPointSpecification();
                usepspecification.Conditions = "use";
                this.UsedPoint = usepspecification.isSatisfiedBy();

            }

            internal void getSaleTop()
            {
                /// 今月の売上商品トップ5
                const string format = "yyyy/MM/dd";
                DateTime today = DateTime.Today;
                DateTime firstDay = today.AddDays(-today.Day + 1);
                DateTime endDay = firstDay.AddMonths(1).AddDays(-1);
                string firstDayText = firstDay.ToString(format);
                string endDayText = endDay.ToString(format);

                NewTop5Specification topspecification = ErsFactory.ersOrderFactory.GetNewTop5Specification();
                topspecification.date_from = firstDayText + " 00:00:00";
                topspecification.date_to = endDayText + " 23:59:59";
                this.GetTop = topspecification.isSatisfiedBy();

            }

        }
    

}