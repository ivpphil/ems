﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.information;
using jp.co.ivp.ers.direction;
using ersContact.Models;
using jp.co.ivp.ers.cts_operators;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersContact.Domain.Information.Commands;
using ersContact.Domain.Information.Mappables;

namespace ersContact.Models
{
    public class CtsIndex
        : ErsContactModelBase
        , IInfoReadInsertCommand, IInformationListMappable, IUnReadInformationMappable, ICheckInstructionListMappable
    {
        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsInformationListItemNumberOnPage; } }
        
        public long recordCount { get;  set; }

        public int unreadinfo { get;  set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long pagerPageCount { get; set; }

        [ErsSchemaValidation("cts_information_t.id")]
        public int id { get; set; }

        [ErsSchemaValidation("cts_information_t.agent_id")]
        public int? agent_id { get; set; }


        [ErsSchemaValidation("cts_information_read_t.id")]
        public int readid { get; set; }

        [ErsSchemaValidation("cts_information_read_t.user_id")]
        public int? user_id { get; set; }

        [ErsSchemaValidation("cts_information_read_t.information_id")]
        public int information_id { get; set; }

		[ErsSchemaValidation("cts_information_read_t.intime")]
		public string intime { get; set; }

		[ErsOutputHidden]
		[ErsSchemaValidation("cts_information_t.utime")]
		public string utime { get; set; }


        public bool loadUnreadInstructionFlg { get; set; }

        public List<Dictionary<string, object>> informationList { get;  set; }

        public IList<ErsCtsInformation> ErsCtsInformationList { get; set; }

        public List<Dictionary<string, object>> calendarinforList { get;  set; }

        [ErsOutputHidden]
  		[ErsUniversalValidation(type = CHK_TYPE.Date)]
		public DateTime calendarMonth { get; set; }

		[HtmlSubmitButton]
		public bool backCalendar { get; set; }

		[HtmlSubmitButton]
		public bool nextCalendar { get; set; }

        [HtmlSubmitButton]
        public bool moveCalendar { get; set; }

        public string CalendarHtml { get { return ErsFactory.ersViewServiceFactory.GetErsViewCtsCalenderService().GetCalenderHtml(this.agent_id, this.calendarMonth, this.ErsCtsInformationList); } }

        [HtmlSubmitButton]
        public bool confirm { get; set; }

        public IList<ErsCtsDirection> listInstruction { get; set; }
    }
}