﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.merchandise;
using ersContact.Models.cart;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;
using ersContact.Domain.Cart.Mappables;
using ersContact.Domain.Cart.Commands;

namespace ersContact.Models
{
    public class CtsCart
        : ErsContactModelBase, IManageRegularPatternDatasource
        , ICartMappable, ICartCommand
    {
        public CtsCart()
        {
            basket = ErsFactory.ersBasketFactory.GetErsBasket();
        }

        public ErsBasket basket { get; set; }

        [HtmlSubmitButton]
        public bool search { get; set; }

        [ErsSchemaValidation("bask_t.scode")]
        public string scode { get; set; }

        [ErsUniversalValidation]
        public string del_key { get; set; }

        [ErsUniversalValidation]
        public string del_regular_key { get; set; }

        [BindTable("basketItems")]
        public List<Cart_items> basketItems { get; set; }

        [HtmlDictionary("up_amount")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public Dictionary<string, int?> up_amount_list { get; set; }

        [HtmlDictionary("product_status")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public Dictionary<string, EnumOrderStatusType> product_status_list { get; set; }

        [HtmlSubmitButton]
        public bool recompute { get; set; }

        [HtmlSubmitButton]
        public bool regular_recompute { get; set; }

        public int basketItemCount
        {
            get
            {
                return basket.objBasketRecord.Count;
            }
        }

        /// <summary>
        /// regular order items
        /// </summary>
        [BindTable("regularBasketItems")]
        public List<Cart_regular_items> regularBasketItems { get; set; }

        public int amounttotal { get { return basket.amounttotal + basket.regular_amounttotal; } }

        public int subtotal { get { return basket.subtotal + basket.regular_subtotal; } }

        public int tax { get { return basket.tax + basket.regular_tax; } }

        public int total { get { return basket.total + basket.regular_total; } }

        [ErsSchemaValidation("member_t.mcode")]
        public string mcode { get; set; }

        [ErsSchemaValidation("bask_t.ransu")]
        public string ransu { get; set; }

        [HtmlSubmitButton]
        public bool regular_basket_in { get; set; }

        [HtmlSubmitButton]
        public bool normal_basket_in { get; set; }

        [ErsSchemaValidation("regular_detail_t.send_ptn")]
        public virtual EnumSendPtn? send_ptn { get; set; }

        //fields for regular order
        [ErsSchemaValidation("regular_detail_t.next_date")]
        public DateTime? next_date_day { get; set; }

        //fields for regular order
        [ErsSchemaValidation("regular_detail_t.next_date")]
        public DateTime? next_date_month { get; set; }

        public DateTime? next_date
        {
            get
            {
                if (send_ptn == EnumSendPtn.DAY_INTERVALS && (this.firstTime != null && firstTime == 1))
                    return next_date_day;
                else if (send_ptn == EnumSendPtn.MONTH_INTERVALS)
                    return next_date_month;
                else
                    return null;
            }
        }

        [ErsSchemaValidation("regular_detail_t.ptn_interval_day")]
        public short? ptn_interval_day { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public short? ptn_interval_month
        {
            get
            {
                if (send_ptn == EnumSendPtn.MONTH_INTERVALS)
                    return ptn_monthly_interval_month;
                else if (send_ptn == EnumSendPtn.WEEK_INTERVALS)
                    return ptn_weekly_interval_month;
                else
                    return null;
            }
        }

        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public short? ptn_monthly_interval_month { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public short? ptn_weekly_interval_month { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_day")]
        public short? ptn_day { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_interval_week")]
        public short? ptn_interval_week { get; set; }

        [ErsSchemaValidation("regular_detail_t.ptn_weekday")]
        public DayOfWeek? ptn_weekday { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public int? firstTime { get; set; }

        [HtmlSubmitButton]
        public bool IsOrderUpdate { get; set; }
    }
}