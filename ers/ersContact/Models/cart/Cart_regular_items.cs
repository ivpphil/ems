﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.basket;
using System.ComponentModel;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.order;
using ersContact.Domain.Cart.Commands;

namespace ersContact.Models.cart
{
    public class Cart_regular_items
        : Cart_items, IManageRegularPatternDatasource, ICartRegularRecordCommand
    {
        [ErsOutputHidden]
        [ErsUniversalValidation]
        public virtual string regular_key { get; set; }

        public virtual int id { get; private set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public int? firstTime { get; set; }

        //fields for regular order
        [ErsOutputHidden]
        [ErsSchemaValidation("regular_detail_t.send_ptn")]
        [BindTarget("confirmation_regular_key")]
        public EnumSendPtn? send_ptn { get; set; }

        [BindTarget("confirmation_regular_key")]
        public DateTime? next_date
        {
            get
            {
                if (send_ptn == EnumSendPtn.DAY_INTERVALS && (this.firstTime != null && firstTime == 1))
                    return next_date_day;
                else if (send_ptn == EnumSendPtn.MONTH_INTERVALS)
                    return next_date_month;
                else
                    return null;
            }
        }

        //fields for regular order
        [ErsOutputHidden]
        [ErsSchemaValidation("regular_detail_t.next_date")]
        public DateTime? next_date_day { get; set; }

        //fields for regular order
        [ErsOutputHidden]
        [ErsSchemaValidation("regular_detail_t.next_date")]
        public DateTime? next_date_month { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_day")]
        [BindTarget("confirmation_regular_key")]
        public short? ptn_interval_day { get; set; }

        [BindTarget("confirmation_regular_key")]
        public short? ptn_interval_month
        {
            get
            {
                if (send_ptn == EnumSendPtn.MONTH_INTERVALS)
                    return ptn_monthly_interval_month;
                else if (send_ptn == EnumSendPtn.WEEK_INTERVALS)
                    return ptn_weekly_interval_month;
                else
                    return null;
            }
        }

        [ErsOutputHidden]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public short? ptn_monthly_interval_month { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public short? ptn_weekly_interval_month { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("regular_detail_t.ptn_day")]
        [BindTarget("confirmation_regular_key")]
        public short? ptn_day { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_week")]
        [BindTarget("confirmation_regular_key")]
        public short? ptn_interval_week { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("regular_detail_t.ptn_weekday")]
        [BindTarget("confirmation_regular_key")]
        public DayOfWeek? ptn_weekday { get; set; }

        [ErsOutputHidden]
        [BindTable("regularBasketItems")]
        public List<Cart_regular_items> regularBasketItems { get; set; }

        public string ptn_day_name
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.PtnDay, EnumCommonNameColumnName.namename, this.ptn_day);
            }
        }

        public string ptn_weekday_name
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename, (int?)this.ptn_weekday);
            }
        }

        public override void LoadDefaultValue(ErsBaskRecord merchandise)
        {
            this.OverwriteWithParameter(merchandise.GetPropertiesAsDictionary());

            this.next_date_day = merchandise.next_date;
            this.next_date_month = merchandise.next_date;
            this.ptn_monthly_interval_month = merchandise.ptn_interval_month;
            this.ptn_weekly_interval_month = merchandise.ptn_interval_month;

            this.regular_key = ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(merchandise);

            var shortest_day = ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().GetRegularFromDate(DateTime.Now);

            if (this.next_date == null)
                this.firstTime = 0;
            else
                this.firstTime = 1;

            this.LoadRegularTermData(merchandise);
        }

        public override void LoadDisplayValue(ErsBaskRecord merchandise)
        {
            base.LoadDisplayValue(merchandise);

            this.LoadRegularTermData(merchandise);
        }

        private void LoadRegularTermData(ErsBaskRecord merchandise)
        {
            var product = ErsFactory.ersMerchandiseFactory.GetActiveErsMerchandiseWithScode(merchandise.scode, null);
            if (product == null)
            {
                return;
            }

            if (!product.disp_send_ptn.HasValue())
            {
                product.disp_send_ptn = "0";
            }
            var disp_send_ptn = product.disp_send_ptn.PadLeft(3, '0');

            this.disp_send_ptn_month_intervals = (Convert.ToInt32(disp_send_ptn.Substring(0, 1)) == 1);
            this.disp_send_ptn_week_intervals = (Convert.ToInt32(disp_send_ptn.Substring(1, 1)) == 1);
            this.disp_send_ptn_month_day_intervals = (Convert.ToInt32(disp_send_ptn.Substring(2, 1)) == 1);
        }

        public bool disp_send_ptn_month_intervals { get; set; }

        public bool disp_send_ptn_week_intervals { get; set; }

        public bool disp_send_ptn_month_day_intervals { get; set; }
    }
}