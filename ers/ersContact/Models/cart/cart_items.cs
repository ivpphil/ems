﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using ersContact.Domain.Cart.Commands;

namespace ersContact.Models.cart
{
    public class Cart_items
        : ErsBindableModel, ICartRecordCommand
    {
        [ErsOutputHidden]
        [ErsUniversalValidation]
        public virtual string key { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("bask_t.amount")]
        public virtual int? amount { get; set; }

        public virtual string scode { get; internal set; }
        public virtual string sname { get; internal set; }
        public virtual string colorw { get; internal set; }
        public virtual string sizew { get; internal set; }
        public virtual int? price { get; internal set; }
        public virtual int? total { get; internal set; }
        public virtual string cts_sname { get; internal set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("bask_t.order_status")]
        public virtual EnumOrderStatusType? order_status { get; set; }

        public virtual string d_no { get; internal set; }

        public Boolean disabledFlg { get; set; }

        public virtual Boolean disp_regular_flg { get; private set; }

        [BindTable("basketItems")]
        public List<Cart_items> basketItems { get; set; }

        public EnumOrderType? order_type { get; set; }

        public virtual int? max_purchase_count { get; set; }

        public virtual EnumDelvMethod? deliv_method { get; set; }

        public virtual void LoadDefaultValue(ErsBaskRecord merchandise)
        {
            this.LoadDisplayValue(merchandise);

            this.scode = merchandise.scode;
            this.amount = merchandise.amount;
            this.order_status = merchandise.order_status;
            this.order_type = merchandise.order_type;
            this.deliv_method = merchandise.deliv_method;
            this.max_purchase_count = merchandise.max_purchase_count;

            this.key = ErsFactory.ersBasketFactory.GetCreateBaskRecordKeyStgy().GetKey(merchandise);
        }

        public virtual void LoadDisplayValue(ErsBaskRecord merchandise)
        {
            this.sname = merchandise.sname;
            this.cts_sname = merchandise.cts_sname;
            //this.colorw = merchandise.colorw;
            //this.sizew = merchandise.sizew;

            this.price = merchandise.price;
            this.total = merchandise.total;
            this.order_status = merchandise.order_status;
            this.order_type = merchandise.order_type;
            this.d_no = merchandise.d_no;
            this.deliv_method = merchandise.deliv_method;

            this.disp_regular_flg = false;
            if (merchandise.price != merchandise.price2)
            {
                this.disp_regular_flg = true;
            }
        }

        public virtual void orderUpdateDocBundingDisabledFlg(EnumDocBundlingFlg doc_bundling_flg)
        {
            if (doc_bundling_flg == EnumDocBundlingFlg.ON)
            {
                this.disabledFlg = true;
            }
        }


    }
}