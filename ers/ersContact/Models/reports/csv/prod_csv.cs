﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace Models.reports.csv
{
    public class prod_csv
        : ErsModelBase
    {
        //[CsvField]
        //public virtual string SupplierName { get; set; }

        [CsvField]
        public virtual string ProductName { get; set; }

        [CsvField]
        public virtual string ProductGroup { get; set; }

        [CsvField]
        public virtual string ProductCode { get; set; }

        [CsvField]
        public virtual int UnitPrice { get; set; }

        [CsvField]
        public virtual int TemporaryOrder { get; set; }

        [CsvField]
        public virtual int CTS { get; set; }

        [CsvField]
        public virtual int Tel { get; set; }

        [CsvField]
        public virtual int Letter { get; set; }

        [CsvField]
        public virtual int Fax { get; set; }

        [CsvField]
        public virtual int PC { get; set; }

        [CsvField]
        public virtual int MB { get; set; }

        [CsvField]
        public virtual int TotalNumber { get; set; }

        [CsvField]
        public virtual int TotalAmount { get; set; }

        [CsvField]
        public virtual int NumberAvailableForSale { get; set; }
    }
}