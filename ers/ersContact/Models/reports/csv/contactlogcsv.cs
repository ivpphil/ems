﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace Models.reports.csv
{
    public class contactlogcsv
        : ErsModelBase
    {
        [CsvField]
        public virtual string csv_CaseNo { get; set; }

        [CsvField]
        public virtual string csv_ReceptionDate { get; set; }

        [CsvField]
        public virtual string csv_CustomerName { get; set; }       

        [CsvField]
        public virtual string csv_pref { get; set; }

        [CsvField]
        public virtual string csv_sex { get; set; }

        //[CsvField]
        public virtual string csv_cate3 { get; set; }

        [CsvField]
        public virtual string enq_progress { get; set; }

        [CsvField]
        public virtual string Status { get; set; }

        [CsvField]
        public virtual string csv_PersonName { get; set; }

        [CsvField]
        public virtual string enq_casename { get; set; }

        [CsvField]
        public virtual string csv_content { get; set; }

        //[CsvField]
        public virtual string csv_birth { get; set; }       

        //[CsvField]
        public virtual string csv_age { get; set; }                   

        //[CsvField]
        public virtual string csv_cate1 { get; set; }

        //[CsvField]
        public virtual string csv_escalation { get; set; }

        //[CsvField]
        public virtual string csv_cate2 { get; set; }
    }
}















