﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace Models.reports.csv
{
    public class call_csv
        : ErsModelBase
    {
        [CsvField]
        public virtual string time_interval { get; set; }

        [CsvField]
        public virtual string[] ag_name { get; set; }

        [CsvField]
        public virtual string total { get; set; }
    }
}