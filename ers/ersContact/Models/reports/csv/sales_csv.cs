﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace Models.reports.csv
{
    public class sales_csv
        : ErsModelBase
    {
        [CsvField]
        public virtual string time_interval { get; set; }

        [CsvField]
        public virtual string[] scode { get; set; }

        [CsvField]
        public virtual string total { get; set; }
    }
}