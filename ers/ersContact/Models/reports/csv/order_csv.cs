﻿using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace Models.reports.csv
{
    public class order_csv
        : ErsModelBase
    {
        [CsvField]
        public virtual string agent { get; set; }

        [CsvField]
        public virtual int temp_order { get; set; }

        [CsvField]
        public virtual int reg_order { get; set; }

        [CsvField]
        public virtual int total { get; set; }

        [CsvField]
        public virtual int price { get; set; }
    }
}