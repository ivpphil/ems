﻿using System;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.ComponentModel;

namespace Models.reports.csv
{
    public class contactlogs_csv
        : ErsModelBase
    {
        [CsvField]
        public virtual string csv_CaseNo { get; set; }

        [CsvField]
        public virtual DateTime csv_ReceptionDate { get; set; }

        [CsvField]
        public virtual string ClientsName { get; set; }

        [CsvField]
        [DisplayName("pref")]
        public virtual string csv_pref { get; set; }

        //[CsvField]
        public virtual EnumEnqStatus? Status { get; set; }

        [CsvField]
        [DisplayName("Progress")]
        public virtual string w_status { get; set; }

        //[CsvField]
        //public virtual string enq_progress { get; set; }

        [CsvField]
        [DisplayName("Status")]
        public virtual string w_situation { get; set; }

        [CsvField]
        public virtual string PersonInCharge { get; set; }

        [CsvField]
        public virtual string cate1 { get; set; }

        [CsvField]
        public virtual string cate2 { get; set; }

        [CsvField]
        public virtual string cate3 { get; set; }

        [CsvField]
        public virtual string cate4 { get; set; }

        [CsvField]
        [DisplayName("enq_casename")]
        public virtual string Subject { get; set; }

        [CsvField]
        [DisplayName("csv_content")]
        public virtual string inquiry { get; set; }
    }
}