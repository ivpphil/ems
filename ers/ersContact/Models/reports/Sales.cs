﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Reports.Commands;
using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace Models.reports
{
    public class Sales
        : ErsContactModelBase
        , IProductSalesMappable, IRepSalesMappable, IReportsCommand, ISalesCsvMappable
    {
        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? datefrom { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? dateto { get; set; }

        [HtmlSubmitButton]
        public bool repsales { get; set; }

        [HtmlSubmitButton]
        public bool repsalescsv { get; set; }

        [HtmlSubmitButton]
        public bool searchscode1 { get; set; }

        [HtmlSubmitButton]
        public bool searchscode2 { get; set; }

        [HtmlSubmitButton]
        public bool searchscode3 { get; set; }

        [HtmlSubmitButton]
        public bool searchscode4 { get; set; }

        [HtmlSubmitButton]
        public bool searchscode5 { get; set; }

        [HtmlSubmitButton]
        public bool searchscode { get; set; }

        [HtmlSubmitButton]
        public bool repproductsearch { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? colcount { get; set; }

        public int? order_total { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode")]
        public string scode1 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode")]
        public string scode2 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode")]
        public string scode3 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode")]
        public string scode4 { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode")]
        public string scode5 { get; set; }

        [ErsSchemaValidation("s_master_t.scode")]
        public string scode { get; set; }

        public string[] scodes { get; set; }

        [ErsUniversalValidation]
        public string TargetOrder { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool TargetTempOrder { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool TargetRegOrder { get; set; }

        public List<Dictionary<string, object>> timeinterval { get; set; }

        public List<Dictionary<string, object>> repsalesList { get; set; }

        public List<Dictionary<string, object>> repsalesListByInterval { get; set; }

        public List<Dictionary<string, object>> repsalesListByDNo { get; set; }

        public List<Dictionary<string, object>> repproductList { get; set; }

        [HtmlSubmitButton]
        public bool repsalesHasRecord { get; set; }

        protected ErsViewPrefService prefService;

        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsRepContactLogsItemNumberOnPage; } }

        public long recordCount { get; set; }

        public ErsCsvCreater csvCreater { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long pagerPageCount { get; set; }
    }
}