﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Reports.Commands;
using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace Models.reports
{
    public class Category
        : ErsContactModelBase
        , IRepCategoryMappable, IRepCategoryListMappable, IRepCategoryDetailsMappable, IReportsCommand
        , ICategoryCsvMappable
    {
        public List<Dictionary<string, object>> ctsEnqtypList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQTYP, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqpryList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQPRY, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqstsList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQSTS, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqpgrList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQPGR, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqsitList 
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQSIT, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ctsEnqct1List
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT1, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqct2List
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT2, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqct3List
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT3, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqct4List
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT4, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqct5List
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT5, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }

        public string w_enqct_name
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetStringFromId(EnumCtsEnquiryCategoryType.ENQCT_NAME, EnumCtsEnquiryCategoryColumnName.namename, cate_code);
            }
        }

        [HtmlSubmitButton]
        public bool repcategory { get; set; }

        [HtmlSubmitButton]
        public bool todetail { get; set; }

        [HtmlSubmitButton]
        public bool repcategoryHasRecord { get; set; }

        [HtmlSubmitButton]
        public bool repcategorycsv { get; set; }

        [HtmlSubmitButton]
        public bool repcategoryList { get; set; }

        [HtmlSubmitButton]
        public bool repcategoryDetails { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("common_namecode_t.code")]
        public int p_code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.cate1")]
        public int c_code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.enq_type")]
        public int? typcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.enq_priorty")]
        public int? prycode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.enq_status")]
        public int? stscode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.enq_progress")]
        public int? pgrcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.enq_situation")]
        public int? sitcode { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate1")]
        public int? ct1code { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate2")]
        public int? ct2code { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate3")]
        public int? ct3code { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate4")]
        public int? ct4code { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate5")]
        public int? ct5code { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_casename")]
        public string enq_casename { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.case_no")]
        public int case_no { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int cate_code { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? datefrom { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? dateto { get; set; }

        public ErsCsvCreater csvCreater { get; set; }

        public List<Dictionary<string, object>> repcategoryListParent { get; set; }

        public List<Dictionary<string, object>> repcategoryListChild { get; set; }

        public List<Dictionary<string, object>> repcategoryRowSpan { get; set; }

        public List<Dictionary<string, object>> repcategoryChildCount { get; set; }

        public List<Dictionary<string, object>> repcategoryParentCount { get; private set; }

        public List<Dictionary<string, object>> repcategoryListDetail { get; set; }

        public List<Dictionary<string, object>> repcategoryDetail { get; set; }

        public List<Dictionary<string, object>> repcategoryBillDetail { get; set; }

        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsRepContactLogsItemNumberOnPage; } }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long pagerPageCount { get; set; }
    }
}