﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Reports.Commands;
using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace Models.reports
{
    public class Product
        : ErsContactModelBase
        , IRepProdMappable, IReportsCommand, IProductCsvMappable
    {
        public ErsCsvCreater csvCreater { get; set; }

        public List<Dictionary<string, object>> ctsAgetypeList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentTypeService().SelectAsList(); } }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? datefrom { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? dateto { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 50)]
        public virtual string agentid { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? brdcastdate { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? picdate { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? purchclerk { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.scode")]
        public string prodcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("s_master_t.sname")]
        public string prodname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_login_t.ag_type")]
        public EnumAgType? ag_type { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation]
        public string TargetOrder { get; set; }

        [HtmlSubmitButton]
        public bool TargetTempOrder { get; set; }

        [HtmlSubmitButton]
        public bool TargetRegOrder { get; set; }

        [HtmlSubmitButton]
        public bool repprod { get; set; }

        [HtmlSubmitButton]
        public bool repprodcsv { get; set; }

        [HtmlSubmitButton]
        public bool repprodHasRecord { get { return this.repprodList != null && this.repprodList.Count > 0; } }

        public List<Dictionary<string, object>> repprodList { get; set; }

        public List<Dictionary<string, object>> repprodTotalPrice { get; private set; }

        public List<Dictionary<string, object>> repprodTotalNumber { get; private set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public long total_number { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public long total_amount { get; set; }
    }
}