﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Reports.Commands;
using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace Models.reports
{
    public class Call 
        : ErsContactModelBase
        , ICallListMappable, IReportsCommand, ICallCsvMappable
    {
        #region Variables
        public List<Dictionary<string, object>> ctsAgetypeList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentTypeService().SelectAsList(); } }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? datefrom { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? dateto { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_login_t.ag_type")]
        public EnumAgType? ag_type { get; set; }

        // Other Variables
        [ErsSchemaValidation("s_scale.byhalfhour")]
        public string byhalfhour { get; set; }

        [ErsSchemaValidation("d_master_t.d_no")]
        public int d_no_count { get; set; }

        [ErsSchemaValidation("d_master_t.intime")]
        public DateTime intime { get; set; }

        [ErsSchemaValidation("d_master_t.user_id")]
        public string user_id { get; set; }

        [ErsSchemaValidation("common_namecode_t.namename")]
        public string namename { get; set; }

        public string TargetOrder { get; set; }

        public string TargetScale { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? colcount { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? rowcount { get; set; }

        public ErsCsvCreater csvCreater { get; set; }
        #endregion

        #region Boolean Variables
        [HtmlSubmitButton]
        public bool repcallHasRecord { get; set; }

        [HtmlSubmitButton]
        public bool repcall { get; set; }

        [HtmlSubmitButton]
        public bool repcallcsv { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool TargetTempOrder { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool TargetRegOrder { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool TargetByDay { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool TargetByMonth { get; set; }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool TargetByTime { get; set; }

        [HtmlSubmitButton]
        public bool OutputAsCSV { get; set; }
        #endregion

        public IList<ErsCtsRepCall> list { get; set; }

        public IList<ErsCtsRepCall> listTime { get; set; }

        public IList<ErsCtsRepCall> listUserTime { get; set; }

        public IList<ErsCtsRepCall> listInterval { get; set; }

        #region List Objects
        public List<Dictionary<string, object>> repcallList { get; set; }

        public List<Dictionary<string, object>> repcallTotalTime { get; set; }

        public List<Dictionary<string, object>> repcallTotalUserTime { get; set; }

        public List<Dictionary<string, object>> repcallInterval { get; set; }
        #endregion

        public int repcallTotal { get; set; }

        public int? site_id { get; set; }
    }
}