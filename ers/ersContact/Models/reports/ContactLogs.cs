﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Reports.Commands;
using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers.util;

namespace Models.reports
{
    public class ContactLogs
        : ErsContactModelBase
        , ICategoryLabelMappable, IRepContactLogMappable, IRepContactLogDetailMappable, IReportsCommand
        , IContactLogsCsvMappable
    {
        public List<Dictionary<string, object>> ctsEnqtypList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQTYP, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqpryList 
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQPRY, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqstsList 
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQSTS, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqpgrList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQPGR, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqsitList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQSIT, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ctsEnqct1List
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT1, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqct2List
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT2, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqct3List
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT3, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqct4List
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT4, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ctsEnqct5List
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT5, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }

        [ErsUniversalValidation]
        public string catlabel1 { get; set; }

        [ErsUniversalValidation]
        public string catlabel2 { get; set; }

        [ErsUniversalValidation]
        public string catlabel3 { get; set; }

        [ErsUniversalValidation]
        public string catlabel4 { get; set; }

        [ErsUniversalValidation]
        public string catlabel5 { get; set; }

        [HtmlSubmitButton]
        public bool repcontact { get; set; }

        [HtmlSubmitButton]
        public bool todetail { get; set; }

        [HtmlSubmitButton]
        public bool repcontactHasRecord { get; set; }

        [HtmlSubmitButton]
        public bool repcontactcsv { get; set; }

        public List<Dictionary<string, object>> repcontactlogList { get; set; }

        public List<Dictionary<string, object>> repcontactlogDetailList { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.enq_type")]
        public int? typcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.enq_priorty")]
        public int? prycode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.enq_status")]
        public int? stscode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.enq_progress")]
        public int? pgrcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.enq_situation")]
        public int? sitcode { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.cate1")]
        public int? ct1code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.cate2")]
        public int? ct2code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.cate3")]
        public int? ct3code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.cate4")]
        public int? ct4code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.cate5")]
        public int? ct5code { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_enquiry_t.enq_casename")]
        public string enq_casename { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.case_no")]
        public int? case_no { get; set; }

        [ErsUniversalValidation]
        public EnumEnqProgress? enq_progress { get; set; }

        [ErsUniversalValidation]
        public EnumEnqStatus? enq_status { get; set; }

        [ErsUniversalValidation]
        public EnumEnqSituation? enq_situation { get; set; }

        [ErsUniversalValidation]
        public EnumEnqType? enq_type { get; set; }

        [ErsUniversalValidation]
        public EnumEnqPriority? enq_priorty { get; set; }

        [ErsUniversalValidation]
        public string status { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? datefrom { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? dateto { get; set; }

        protected ErsViewPrefService prefService;

        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsRepContactLogsItemNumberOnPage; } }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long pagerPageCount { get; set; }

        public ErsCsvCreater csvCreater { get; set; }
    }
}