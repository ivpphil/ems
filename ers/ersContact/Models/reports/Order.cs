﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Reports.Commands;
using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;

namespace Models.reports
{
    public class Order
        : ErsContactModelBase
        , IRepOrderMappable, IReportsCommand, IOrderCsvMappable
    {
        public List<Dictionary<string, object>> ctsAgetypeList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentTypeService().SelectAsList(); } }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? datefrom { get; set; }

        [ErsOutputHidden]
        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? dateto { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_login_t.user_id")]
        public virtual string agentid { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("cts_login_t.ag_type")]
        public EnumAgType? ag_type { get; set; }

        /// <summary>
        /// ag_type name in Katakana
        /// </summary>
        ///
        [ErsUniversalValidation(type=CHK_TYPE.FullString)]
        public string ag_typek
        {
            get
            {
                if (ag_type.HasValue)
                {
                    if(ag_type == EnumAgType.Admin)
                    {
                        return "管理";
                    }
                    else
                    {
                        return "コールセンター";
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        public List<Dictionary<string, object>> reporderList { get; set; }

        public List<Dictionary<string, object>> reporderTotal { get; private set; }

        [HtmlSubmitButton]
        public bool reporder { get; set; }

        [HtmlSubmitButton]
        public bool repordercsv { get; set; }

        [HtmlSubmitButton]
        public bool reporderHasRecord { get; set; }

        public ErsCsvCreater csvCreater { get; set; }
    }
}