﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Reports.Commands;
using ersContact.Domain.Reports.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.reports;
using jp.co.ivp.ers.viewService;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;

namespace Models.reports
{
    public class Age
        : ErsContactModelBase
        , ICampaignMappable, IProductAgeMappable, IRepAgeMappable, IReportsCommand
    {
        public List<Dictionary<string, object>> ctsPrefectList { get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(false, this.site_id); } }

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? datefrom { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        public DateTime? dateto { get; set; }

        [HtmlSubmitButton]
        public bool repage { get; set; }

        [HtmlSubmitButton]
        public bool repagecsv { get; set; }

        public int totalCount { get; set; }

        public int billCount { get; set; }

        public int invalidbillCount { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int maleTotalCount { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int femaleTotalCount { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int allTotalCount { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public Double maleTotalRate { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public Double femaleTotalRate { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public Double allTotalRate { get; set; }

        public IList<ErsCtsRepAge> repageList { get; set; }

        #region Product Search
        [HtmlSubmitButton]
        public bool searchscode1 { get; set; }

        [HtmlSubmitButton]
        public bool searchscode2 { get; set; }

        [HtmlSubmitButton]
        public bool searchscode3 { get; set; }

        [HtmlSubmitButton]
        public bool searchscode4 { get; set; }

        [HtmlSubmitButton]
        public bool searchscode5 { get; set; }

        [HtmlSubmitButton]
        public bool searchscode { get; set; }

        [HtmlSubmitButton]
        public bool repproductsearch { get; set; }

        [HtmlSubmitButton]
        public bool reload { get; set; }

        [HtmlSubmitButton]
        public bool medsearch { get; set; }
        
        [HtmlSubmitButton]
        public bool medselect { get; set; }

        [ErsSchemaValidation("s_master_t.scode")]
        public string scode { get; set; }

        [ErsSchemaValidation("s_master_t.scode")]
        public string scode1 { get; set; }

        [ErsSchemaValidation("s_master_t.scode")]
        public string scode2 { get; set; }

        [ErsSchemaValidation("s_master_t.scode")]
        public string scode3 { get; set; }

        [ErsSchemaValidation("s_master_t.scode")]
        public string scode4 { get; set; }

        [ErsSchemaValidation("s_master_t.scode")]
        public string scode5 { get; set; }

        [ErsSchemaValidation("pref_t.id")]
        public int? pref1 { get; set; }

        [ErsSchemaValidation("pref_t.id")]
        public int? pref2 { get; set; }

        [ErsSchemaValidation("pref_t.id")]
        public int? pref3 { get; set; }

        [ErsSchemaValidation("pref_t.id")]
        public int? pref4 { get; set; }

        [ErsSchemaValidation("pref_t.id")]
        public int? pref5 { get; set; }

        [ErsSchemaValidation("campaign_t.ccode")]
        public virtual string ccode { get; set; }
        
        [ErsSchemaValidation("campaign_t.campaign_name")]
        public virtual string campaign_name { get; set; }

        public string w_pref1
        { 
            get
            {
                if(this.pref1 != null)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId((int)this.pref1);
                }
                return null; 
            } 
        }
        public string w_pref2
        {
            get
            {
                if (this.pref2 != null)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId((int)this.pref2);
                }
                return null;
            }
        }
        public string w_pref3
        {
            get
            {
                if (this.pref3 != null)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId((int)this.pref3);
                }
                return null;
            }
        }
        public string w_pref4
        {
            get
            {
                if (this.pref4 != null)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId((int)this.pref4);
                }
                return null;
            }
        }
        public string w_pref5
        {
            get
            {
                if (this.pref5 != null)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId((int)this.pref5);
                }
                return null;
            }
        }

        public IList<ErsSku> repproductList { get; set; }

        public List<Dictionary<string, object>> campaignList { get; set; }

        public string setDate { get; set; }

        public string setTempDate { get; set; }

        public virtual int? site_id { get { return ErsFactory.ersUtilityFactory.getSetup().site_id; } } 

        #endregion

        #region paging
        protected ErsViewPrefService prefService;

        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsSearchListItemNumberOnPage; } }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long pagerPageCount { get; set; }
        #endregion
    }
}