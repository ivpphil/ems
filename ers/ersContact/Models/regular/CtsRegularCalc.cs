﻿using System;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.util;
using ersContact.Domain.Regular.Mappables;

namespace ersContact.Models
{
    public class CtsRegularCalc
        : ErsContactModelBase, IManageRegularPatternDatasource, IRegularCalcMappable
    {
        public string resultDate { get; set; }

        public string scode
        {
            get { throw new NotImplementedException(); }
        }

        [ErsSchemaValidation("regular_detail_t.next_date")]
        public DateTime? next_date { get; set; }

        //C-49
        [ErsSchemaValidation("regular_detail_t.send_ptn")]
        public EnumSendPtn? send_ptn { get; set; }

        //C-50
        //public short? ptn_interval_month
        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public virtual short? ptn_interval_month { get; set; }

        //C-51
        //short? IManageRegularPatternDatasource.ptn_day
        [ErsSchemaValidation("regular_detail_t.ptn_day")]
        public virtual short? ptn_day { get; set; }

        //C-53
        //short? IManageRegularPatternDatasource.ptn_interval_week
        [ErsSchemaValidation("regular_detail_t.ptn_interval_week")]
        public virtual short? ptn_interval_week { get; set; }

        //C-54
        //short? IManageRegularPatternDatasource.ptn_weekday
        [ErsSchemaValidation("regular_detail_t.ptn_weekday")]
        public virtual DayOfWeek? ptn_weekday { get; set; }

        //C-55
        //short? IManageRegularPatternDatasource.ptn_interval_day
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_day")]
        public virtual short? ptn_interval_day { get; set; }

        //C-56
        [ErsSchemaValidation("regular_detail_t.weekend_operation")]
        public virtual EnumWeekendOperation? weekend_operation { get; set; }
    }
}
