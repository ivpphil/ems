﻿using System;
using System.Collections.Generic;
using System.Linq;
using ersContact.Domain.Regular.Commands;
using ersContact.Domain.Regular.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.util;
using System.ComponentModel;
using jp.co.ivp.ers.viewService;

namespace ersContact.Models
{
    public class CtsRegular
        : ErsContactModelBase
        , IOrderMappable, IProductMappable,IMemberCommand, IMemberMappable, IShippingCommand
        , IShippingMappable, IOrderCommand, ISkipOrderCommand, IOrderDetailMappable
        , IPaymentCommand, IPaymentMappable
    {
        public CtsRegular()
        {
            this.Debug = ErsFactory.ersUtilityFactory.getSetup().debug;
        }

        //public string Cts_Ransu { get; private set; }

        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsInformationListItemNumberOnPage; } }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [ErsOutputHidden("input_cts_order")]
        [HtmlSubmitButton]
        public bool page1 { get; set; }

        public bool Debug { get; private set; }

        public string HighlightColor { get { return ErsFactory.ersUtilityFactory.getSetup().cts_highlightcolor; } }

        public int RegularOrderDayCount { get { return ErsFactory.ersBatchFactory.getSetup().create_regular_order_days; } }

        #region " client information "
        public ErsMember member { get; set; }

        [HtmlSubmitButton]
        public bool member_modify { get; set; }

        [HtmlSubmitButton]
        public bool member_register { get; set; }

        [HtmlSubmitButton]
        public bool member_cancel { get; set; }

        [ErsSchemaValidation("member_t.id")]
        public virtual int? member_id { get; set; }

        [ErsOutputHidden("input_form")]
        [ErsSchemaValidation("member_t.mcode")]
        public virtual string mcode { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.lname")]
        public virtual string lname { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.fname")]
        public virtual string fname { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.lnamek")]
        public virtual string lnamek { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.fnamek")]
        public virtual string fnamek { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.tel")]
        public virtual string tel { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.zip")]
        public virtual string zip { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.pref")]
        public virtual int? pref { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("pref_t.pref_name")]
        public virtual string pref_name { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.address")]
        public virtual string address { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.taddress")]
        public virtual string taddress { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.maddress")]
        public virtual string maddress { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.email")]
        public virtual string email { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.sex")]
        public EnumSex? sex { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsSchemaValidation("member_t.age_code")]
        public int? age_code { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = ErsViewBirthdayService.minimumOfSelectyear, rangeTo = ErsViewBirthdayService.maximumOfSelectyear)]
        public int? birthday_y { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 12)]
        public int? birthday_m { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 31)]
        public int? birthday_d { get; set; }

        [ErsSchemaValidation("member_t.dm_flg")]
        [ErsOutputHidden("input_payment")]
        public virtual EnumDmFlg dm_flg { get; set; }

        public virtual string w_dm_flg { get; set; }

        [ErsSchemaValidation("member_t.memo")]
        [ErsOutputHidden("input_payment")]
        public virtual string memo { get; set; }

        [ErsSchemaValidation("member_t.out_bound_flg")]
        [ErsOutputHidden("input_payment")]
        public virtual EnumOutBoundFlg out_bound_flg { get; set; }

        public virtual string w_out_bound_flg { get; set; }

        public DateTime? birth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetBirthDay(this.birthday_y, this.birthday_m, this.birthday_d);
            }
        }
        public List<Dictionary<string, object>> prefList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(false, this.site_id); }
        }

        public List<Dictionary<string, object>> sexList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename); }
        }

        public string w_sex
        {
            get
            {
                if (!this.sex.HasValue)
                {
                    return null;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename, (int)this.sex.Value);
            }
        }
        public List<Dictionary<string, object>> ageTypeList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ORDAGE, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> shippingList { get; set; }

        public List<Dictionary<string, object>> birthday_yList
        {
            get
            {
                var min_user_age = ErsFactory.ersUtilityFactory.getSetup().min_user_age;
                var max_user_age = ErsFactory.ersUtilityFactory.getSetup().max_user_age;
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListYear(max_user_age, min_user_age);
            }
        }

        public List<Dictionary<string, object>> birthday_mList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListMonth(); }
        }

        public List<Dictionary<string, object>> birthday_dList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListDay(); }
        }
        #endregion

        #region " shipping address "
        [HtmlSubmitButton]
        public bool shipping_new { get; set; }

        [HtmlSubmitButton]
        public bool shipping_modify { get; set; }

        [HtmlSubmitButton]
        public bool shipping_delete { get; set; }

        [HtmlSubmitButton]
        public bool shipping_register { get; set; }

        [HtmlSubmitButton]
        public bool shipping_cancel { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.id")]
        public int? modify_shipping_id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.id")]
        public int? shipping_id { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.address_name")]
        public string address_name { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_lname")]
        public string add_lname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_fname")]
        public string add_fname { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_lnamek")]
        public string add_lnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_fnamek")]
        public string add_fnamek { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_zip")]
        public string add_zip { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_pref")]
        public int? add_pref { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_address")]
        public string add_address { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_taddress")]
        public string add_taddress { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_maddress")]
        public string add_maddress { get; set; }

        [ErsOutputHidden]
        [ErsSchemaValidation("addressbook_t.add_tel")]
        public string add_tel { get; set; }
        #endregion

        #region " order "
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_t.id")]
        public int? id { get; set; }

        [ErsOutputHidden("input_form")]
        [ErsSchemaValidation("regular_t.d_no")]
        public string d_no
        {
            get
            {
                return _d_no;
            }
            set
            {
                //Formatting the d_no value since a hyphen is removed at validation of DataAnnotation.
                if (!string.IsNullOrEmpty(value) && value.Length > 8 && !value.Contains('-'))
                {
                    _d_no = value.Substring(0, 8) + "-" + value.Substring(8);
                }
                else
                {
                    _d_no = value;
                }
            }
        }
        private string _d_no;

        // show Edit Options
        public bool edit_mode { get; private set; }

        // show All Options - (if status == stopped, show only status option)
        public bool edit_running { get; private set; }

        public DateTime Today { get { return DateTime.Now; } }       
        #endregion

        #region " order details listing "
        public IList<ErsRegularOrder> orderList { get; set; }

        public List<Dictionary<string, object>> orderDetailsList { get; set; }

        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_t.id")]
        public int? all_status { get; set; }

        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_t.memo")]
        public int refetch { get; set; }

        [HtmlDictionary("r_active")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public Dictionary<string, string> r_active_list { get; set; }

        [HtmlDictionary("r_status")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public Dictionary<string, EnumStatus?> r_status_list { get; set; }

        [HtmlDictionary("r_code")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public Dictionary<string, int> r_code_list { get; set; }

        [HtmlDictionary("r_skip")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public Dictionary<string, string> r_skip_list { get; set; }

        [HtmlDictionary("r_edit")]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public Dictionary<string, string> r_edit_list { get; set; }
        #endregion

        #region " order details "
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.id")]
        public virtual int d_id { get; set; }

        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_t.id")]
        public EnumStatus? status { get; set; }

        [HtmlSubmitButton]
        public bool done { get; set; }
        #endregion

        #region " order details - sked "
        //C-25
        [ErsSchemaValidation("regular_detail_t.last_date_base")]
        public virtual DateTime? last_date_base { get; set; }

        //C-26
        [ErsSchemaValidation("regular_detail_t.last_sendtime_base")]
        public virtual int? last_sendtime_base { get; set; }

        //C-26
        public virtual string w_last_sendtime_base
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId(this.last_sendtime_base);
            }
        }

        //C-27
        [ErsSchemaValidation("regular_detail_t.last_date")]
        public virtual DateTime? last_date { get; set; }

        //C-28
        [ErsSchemaValidation("regular_detail_t.last_sendtime")]
        public virtual int last_sendtime { get; set; }

        //C-28
        public virtual string w_last_sendtime
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId(this.last_sendtime);
            }
        }

        //C-29
        [ErsSchemaValidation("regular_detail_t.next_date_base")]
        public virtual DateTime? next_date_base { get; set; }

        //C-33
        [ErsSchemaValidation("regular_detail_t.next2_date_base")]
        public virtual DateTime? next2_date_base { get; set; }

        //C-37
        [ErsSchemaValidation("regular_detail_t.next3_date_base")]
        public virtual DateTime? next3_date_base { get; set; }

        //C-41
        [ErsSchemaValidation("regular_detail_t.next4_date_base")]
        public virtual DateTime? next4_date_base { get; set; }

        //C-45
        [ErsSchemaValidation("regular_detail_t.next5_date_base")]
        public virtual DateTime? next5_date_base { get; set; }

        //C-30
        [ErsSchemaValidation("regular_detail_t.next_sendtime_base")]
        public virtual int? next_sendtime_base { get; set; }

        //C-34
        [ErsSchemaValidation("regular_detail_t.next2_sendtime_base")]
        public virtual int? next2_sendtime_base { get; set; }

        //C-38
        [ErsSchemaValidation("regular_detail_t.next3_sendtime_base")]
        public virtual int? next3_sendtime_base { get; set; }

        //C-42
        [ErsSchemaValidation("regular_detail_t.next4_sendtime_base")]
        public virtual int? next4_sendtime_base { get; set; }

        //C-46
        [ErsSchemaValidation("regular_detail_t.next5_sendtime_base")]
        public virtual int? next5_sendtime_base { get; set; }

        //C-31
        [ErsSchemaValidation("regular_detail_t.next_date")]
        public virtual DateTime? next_date { get; set; }

        //C-35
        [ErsSchemaValidation("regular_detail_t.next2_date")]
        public virtual DateTime? next2_date { get; set; }

        //C-39
        [ErsSchemaValidation("regular_detail_t.next3_date")]
        public virtual DateTime? next3_date { get; set; }

        //C-43
        [ErsSchemaValidation("regular_detail_t.next4_date")]
        public virtual DateTime? next4_date { get; set; }

        //C-47
        [ErsSchemaValidation("regular_detail_t.next5_date")]
        public virtual DateTime? next5_date { get; set; }

        //C-32
        [ErsSchemaValidation("regular_detail_t.next_sendtime")]
        public virtual int? next_sendtime { get; set; }

        //C-36
        [ErsSchemaValidation("regular_detail_t.next2_sendtime")]
        public virtual int? next2_sendtime { get; set; }

        //C-40
        [ErsSchemaValidation("regular_detail_t.next3_sendtime")]
        public virtual int? next3_sendtime { get; set; }

        //C-44
        [ErsSchemaValidation("regular_detail_t.next4_sendtime")]
        public virtual int? next4_sendtime { get; set; }

        //C-48
        [ErsSchemaValidation("regular_detail_t.next5_sendtime")]
        public virtual int? next5_sendtime { get; set; }
        #endregion

        #region " order details - term delivery "
        //C-49
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.send_ptn")]
        public EnumSendPtn? send_ptn { get; set; }

        //C-50
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public virtual short? ptn_interval_month_m { get; set; }

        //C-51
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.ptn_day")]
        public virtual short? ptn_day { get; set; }

        //C-52
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_month")]
        public virtual short? ptn_interval_month_w { get; set; }

        //C-53
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_week")]
        public virtual short? ptn_interval_week { get; set; }

        //C-54
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.ptn_weekday")]
        public virtual DayOfWeek? ptn_weekday { get; set; }

        //C-55
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.ptn_interval_day")]
        public virtual short? ptn_interval_day { get; set; }

        //C-56
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.weekend_operation")]
        public virtual EnumWeekendOperation? weekend_operation { get; set; }
        #endregion

        #region " order details - date / memo "
        //C-57
        [ErsSchemaValidation("regular_detail_t.skip_date")]
        public virtual DateTime? skip_date { get; set; }

        //C-58
        [ErsSchemaValidation("regular_detail_t.delete_date")]
        public virtual DateTime? delete_date { get; set; }

        //C-59
        [ErsSchemaValidation("regular_detail_t.shipping_memo")]
        public virtual string shipping_memo { get; set; }
        #endregion

        [HtmlSubmitButton]
        public bool to_modify { get; set; }

        #region " order details - destination "
        //C-72
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.member_add_id")]
        public virtual int? member_add_id { get; set; }
        #endregion

        #region " order details - payment method "
 
        //C-82
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.member_card_id")]
        public virtual EnumPaymentType? paymethod { get; set; }

        //C-82
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.pay")]
        public EnumPaymentType pay { get; set; }

        //C-83
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.member_card_id")]
        public virtual int? member_card_id { get; set; }

        [HtmlSubmitButton]
        public bool is_edit_payment_method { get; set; }

        [HtmlSubmitButton]
        public bool card_new { get; set; }

        [HtmlSubmitButton]
        public bool card_register { get; set; }

        [HtmlSubmitButton]
        public bool card_update { get; set; }

        [HtmlSubmitButton]
        public bool card_modify { get; set; }

        [HtmlSubmitButton]
        public bool card_cancel { get; set; }

        [HtmlSubmitButton]
        public bool card_delete { get; set; }

        [HtmlSubmitButton]
        public bool card_will_add { get; set; }

        //カード入力フォーム表示フラグ
        private int entryCardInfoCount { get { return ErsFactory.ersUtilityFactory.getSetup().entryCardInfoCount; } }

        //カード情報入力フォームの表示フラグ
        public Boolean newEntryFlg
        {
            get
            {
                if (this.membercardList == null || this.membercardList.Count() < entryCardInfoCount)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<Dictionary<string, object>> membercardList { get; set; }

        public List<Dictionary<string, object>> membercardListExtend { get; set; }

        public CreditCardInfo savedCardInfo { get; private set; }

        public List<Dictionary<string, object>> cardList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCardService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> card_yList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCardService().GetListYear(); }
        }

        public List<Dictionary<string, object>> card_mList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCardService().GetListMonth(); }
        }

        public List<Dictionary<string, object>> payList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPayService().SelectAsList(true); }
        }

        public List<Dictionary<string, object>> convenienceList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ConvCode, EnumCommonNameColumnName.namename); }
        }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_card_t.id")]
        [DisplayName("cts_card_id")]
        public virtual int? card_id { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 100)]
        public virtual string card_holder_name { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? card_type { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(rangeFrom = 13, rangeTo = 16, type = CHK_TYPE.NumericString)]
        public virtual string cardno { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? validity_y { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 12, type = CHK_TYPE.Numeric)]
        public virtual int? validity_m { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(rangeFrom = 3, rangeTo = 3, type = CHK_TYPE.NumericString)]
        public virtual string securityno { get; set; }

        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_detail_t.conv_code")]
        public virtual EnumConvCode? conv_code { get; set; }

        public virtual string w_card_type
        {
            get
            {
                if (this.card_type == null)
                {
                    return null;
                }
                else
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewCardService().GetStringFromId(Convert.ToInt32(this.card_type));
                }
            }
        }
        
        public int card_count
        {
            get
            {
                if (membercardList == null)
                {
                    return 0;
                }
                else
                {
                    return membercardList.Count() - 1;
                }
            }
        }

        public bool existCreditData
        {
            get
            {
                if (this.membercardList == null || this.membercardList.Count() == 0)
                    return false;

                else
                    return true;
            }
        }
        #endregion

        #region " update options "

        //C-22
        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_t.id")]
        public int edit_status { get; set; }

        //C-30, C-32, C-34, C-36, C-38, C-40, C-42, C-44, C-46, C-48
        public List<Dictionary<string, object>> sendtimeList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().SelectAsList(); }
        }

        //C-50, C-52
        public List<Dictionary<string, object>> ListPtnIntervalMonth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalMonth, EnumCommonNameColumnName.namename);
            }
        }

        //C-51
        public List<Dictionary<string, object>> ListPtnDay
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnDay, EnumCommonNameColumnName.namename);
            }
        }

        //C-53
        public List<Dictionary<string, object>> ListPtnIntervalWeek
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalWeek, EnumCommonNameColumnName.namename);
            }
        }

        //C-54
        public List<Dictionary<string, object>> ListPtnWeekday
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename);
            }
        }

        //C-55
        public List<Dictionary<string, object>> ListPtnIntervalDay
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalDay, EnumCommonNameColumnName.namename);
            }
        }


        //C-55
        public List<Dictionary<string, object>> ListWeekendOperation
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.WeekendOperation, EnumCommonNameColumnName.opt_chr1);
            }
        }

        #endregion

        #region " regular orders "
        public void EditMode(EnumStatus? mode)
        {
            // 0 - No detail chosen
            // 1 - running detail chosen
            // 2 - stoppted detail chosen
            switch (mode)
            {
                case EnumStatus.Running:
                    this.edit_mode = true;
                    this.edit_running = true;
                    break;
                case EnumStatus.Stopped:
                    this.edit_mode = true;
                    this.edit_running = false;
                    break;
            }
        }
        #endregion

        [ErsSchemaValidation("g_master_t.gcode")]
        public virtual string gcode { get; set; }

        /// <summary>
        /// お届け周期パターン(月毎)
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public int disp_send_ptn_month_intervals { get; set; }

        /// <summary>
        /// お届け周期パターン(週毎)
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public int disp_send_ptn_week_intervals { get; set; }

        /// <summary>
        /// お届け周期パターン(日毎)
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 1)]
        public int disp_send_ptn_month_day_intervals { get; set; }

        public List<DateTime?> listHolidays
        {
            get
            {
                var repository = ErsFactory.ersOrderFactory.GetErsCalendarRepository();
                var criteria = ErsFactory.ersOrderFactory.GetErsCalendarCriteria();
                var listCalendar = repository.Find(criteria);
                var listHolidays = new List<DateTime>();
                return listCalendar.Select((calendar) => calendar.close_date).ToList();
            }
        }

        public virtual int? site_id { get { return ErsFactory.ersUtilityFactory.getSetup().site_id; } } 
    }
}
