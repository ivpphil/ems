﻿using System;
using System.Collections.Generic;
using System.Linq;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.order.related;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersContact.Domain.Search.Mappables;
using jp.co.ivp.ers.member;

namespace ersContact.Models 
{
    public class CtsClientInfo
        : ErsContactModelBase
        , IClientInfoListMappable
    {
        public virtual ErsMember objMember { get; set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsSearchListItemNumberOnPage; } }

        public long recordCount { get; set; }

        public int prodCount { get; set; }
        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public List<Dictionary<string, object>> shippingList { get; set; }

        public List<Dictionary<string, object>> prodList { get; set; }

        public CreditCardInfo savedCardInfo { get; set; }
        
        #region " client information "
        [ErsSchemaValidation("member_t.id")]
        public virtual int? member_id { get; set; }

        [ErsSchemaValidation("member_t.mcode")]
        public virtual string mcode { get; set; }

        [ErsSchemaValidation("member_t.lname")]
        public virtual string lname { get; set; }

        [ErsSchemaValidation("member_t.fname")]
        public virtual string fname { get; set; }

        [ErsSchemaValidation("member_t.lnamek")]
        public virtual string lnamek { get; set; }

        [ErsSchemaValidation("member_t.fnamek")]
        public virtual string fnamek { get; set; }

        [ErsSchemaValidation("member_t.tel")]
        public virtual string tel { get; set; }

        [ErsSchemaValidation("member_t.zip")]
        public virtual string zip { get; set; }

        public string pref_name { get; set; }

        [ErsSchemaValidation("member_t.address")]
        public virtual string address { get; set; }

        [ErsSchemaValidation("member_t.taddress")]
        public virtual string taddress { get; set; }

        public string maddress { get; set; }

        [ErsSchemaValidation("member_t.sex")]
        public string w_sex { get; set; }

        [ErsSchemaValidation("member_t.birth")]
        public virtual DateTime? birth { get; set; }

        [ErsSchemaValidation("member_t.age_code")]
        public virtual int? age_code { get; set; }

        public string memo { get; set; }

        [ErsSchemaValidation("d_master_t.d_no")]
        public string d_no
        {
            get
            {
                return _d_no;
            }
            set
            {
                //Formatting the d_no value since a hyphen is removed at validation of DataAnnotation.
                if (!string.IsNullOrEmpty(value) && value.Length > 8 && !value.Contains('-'))
                {
                    _d_no = value.Substring(0, 8) + "-" + value.Substring(8);
                }
                else
                {
                    _d_no = value;
                }
            }
        }
        private string _d_no;

        [ErsSchemaValidation("member_card_t.id")]
        public virtual string card_id { get; set; }

        [ErsSchemaValidation("d_master_t.pay")]
        public EnumPaymentType? pay { get; set; }

        [ErsSchemaValidation("ds_master_t.scode")]
        public string scode { get; set; }

        [ErsSchemaValidation("ds_master_t.sname")]
        public string sname { get; set; }

        [ErsSchemaValidation("ds_master_t.price")]
        public int price { get; set; }

        [ErsSchemaValidation("ds_master_t.amount")]
        public int amount { get; set; }

        [ErsSchemaValidation("ds_master_t.total")]
        public int total { get; set; }

        [ErsSchemaValidation("addressbook_t.add_zip")]
        public string add_zip { get; set; }

        [ErsSchemaValidation("addressbook_t.add_pref")]
        public int? add_pref { get; set; }

        public string add_pref_name { get; set; }

        [ErsSchemaValidation("addressbook_t.add_address")]
        public string add_address { get; set; }

        [ErsSchemaValidation("addressbook_t.add_taddress")]
        public string add_taddress { get; set; }

        [ErsSchemaValidation("addressbook_t.add_tel")]
        public string add_tel { get; set; }

        [ErsSchemaValidation("addressbook_t.add_lname")]
        public string add_lname { get; set; }

        [ErsSchemaValidation("addressbook_t.add_fname")]
        public string add_fname { get; set; }

        [ErsSchemaValidation("addressbook_t.id")]
        public int? shipping_id { get; set; }

        [ErsSchemaValidation("d_master_t.subtotal")]
        public int subtotal { get; set; }

        [ErsSchemaValidation("d_master_t.tax")]
        public int tax { get; set; }

        [ErsSchemaValidation("d_master_t.p_service")]
        public int p_service { get; set; }

        [ErsSchemaValidation("d_master_t.carriage")]
        public int carriage { get; set; }

        [ErsSchemaValidation("d_master_t.coupon_discount")]
        public int coupon_discount { get; set; }

        [ErsSchemaValidation("d_master_t.etc")]
        public int etc { get; set; }

        [ErsUniversalValidation]
        public virtual string sendtime { get; set; }

        public virtual EnumDeleted? deleted { get; set; }

        [HtmlSubmitButton]
        public bool orderstatus { get; set; }

        public DateTime? senddate { get; set; }

        public int? age { get; private set; }

        public virtual string w_age { get; set; }

        public int order_total_amount { get; set; }

        public int amounttotal { get; set; }

        public string w_etc { get; set; }

        public virtual string w_deliv_method
        {
            get
            {
                if (this.prodList == null || this.prodList.Count == 0)
                {
                    return null;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().
                    GetStringFromId(EnumCommonNameType.DelvMethod, EnumCommonNameColumnName.namename, prodList.Min(e => Convert.ToInt32(e["deliv_method"])));
            }
        }

        public virtual string deliv_name
        {
            get
            {
                if (this.prodList == null || this.prodList.Count == 0)
                {
                    return null;
                }

                var method = new EnumDelvMethod();

                foreach(var prod in this.prodList)
                {
                    if (prod["deliv_method"] != null)
                    {
                        method = (EnumDelvMethod)Convert.ToInt32(prod["deliv_method"]);
                        break;
                    }
                }

                var setup = ErsFactory.ersUtilityFactory.getSetup();

                if (method == EnumDelvMethod.Mail)
                {
                    return setup.mail_delivery;
                }
                return setup.delivery;
            }
        }

        public virtual List<string> sendno_list
        {
            get
            {
                if (this.prodList == null || this.prodList.Count == 0)
                {
                    return null;
                }

                var ret_list = new List<string>();

                foreach (var prod in this.prodList)
                {
                    if (prod["sendno"] != null &&
                        !ret_list.Contains(Convert.ToString(prod["sendno"])))
                    {
                        ret_list.Add(Convert.ToString(prod["sendno"]));
                    }
                }

                return ret_list;
            }
        }

        #endregion

        public EnumMallShopKbn? mall_shop_kbn { get; set; }

        public string w_pay { get; set; }

        public string site_name { get; set; }

        public string mall_d_no { get; set; }

        public int point
        {
            get
            {
                if (this.objMember == null)
                    return 0;

                return ErsFactory.ersMemberFactory.GetObtainMemberPointStgy().GetPoint(objMember.mcode,this.site_id);
            }
        }

        public virtual int? site_id { get { return ErsFactory.ersUtilityFactory.getSetup().site_id; } } 
    }
}