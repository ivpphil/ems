﻿using System.Collections.Generic;
using System.Linq;
using ersContact.Domain.Search.Commands;
using ersContact.Domain.Search.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using jp.co.ivp.ers.doc_bundle;
using System.ComponentModel;
using System;

namespace ersContact.Models 
{
    public class CtsSearch
        : ErsContactModelBase
        , ICampaignListMappable, ISearchListMappable, IInquiryListMappable, ISearchCommand
    {
        public ErsPagerModel pager { get; internal set; }
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsSearchListItemNumberOnPage; } }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [ErsUniversalValidation]
        public string TargetSearch { get; set; }

        public long pagerPageCount { get; set; }

        public bool FirstLoad { get; set; }
        
        [HtmlSubmitButton]
        public bool TargetClient { get; set; }
        [HtmlSubmitButton]
        public bool TargetOrder { get; set; }
        [HtmlSubmitButton]
        public bool detail { get; set; }

        [ErsUniversalValidation(rangeFrom = 0, rangeTo = 3, type = CHK_TYPE.Numeric)]
        public int tabno { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public string zip1 { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public string zip2 { get; set; }

        public List<Dictionary<string, object>> prefList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(false, this.site_id); }
        }

        public List<Dictionary<string, object>> campaignList { get; set; }

        public List<ErsCampaign> ErsCampaignList { get; set; }

        public virtual int? site_id { get { return ErsFactory.ersUtilityFactory.getSetup().site_id; } } 

        #region Order Info
        [ErsSchemaValidation("d_master_t.id")]
        public int? id { get; set; }
        
        [ErsSchemaValidation("d_master_t.mcode")]
        public virtual string mcode { get; set; }

        [ErsSchemaValidation("d_master_t.lname")]
        public virtual string lname { get; set; }

        [ErsSchemaValidation("d_master_t.fname")]
        public virtual string fname { get; set; }

        [ErsSchemaValidation("d_master_t.lnamek")]
        public virtual string lnamek { get; set; }

        [ErsSchemaValidation("d_master_t.fnamek")]
        public virtual string fnamek { get; set; }

        [ErsSchemaValidation("d_master_t.zip")]
        public virtual string zip { get; set; }

        [ErsSchemaValidation("d_master_t.pref")]
        public virtual int? pref { get; set; }
        
        [ErsSchemaValidation("d_master_t.address")]
        public virtual string address { get; set; }

        [ErsSchemaValidation("d_master_t.email")]
        public virtual string email { get; set; }

        [ErsSchemaValidation("member_t.tel")]
        public virtual string tel { get; set; }

        [ErsSchemaValidation("d_master_t.fax")]
        public virtual string fax { get; set; }

        [ErsSchemaValidation("member_t.deleted")]
        public virtual EnumDeleted? src_deleted { get; set; }

        [ErsSchemaValidation("cts_order_t.temp_d_no")]
        public virtual string temp_d_no { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.OneByteCharacter)]
        public virtual string d_no
        {
            get
            {
                return _d_no;
            }
            set
            {
                //Formatting the d_no value since a hyphen is removed at validation of DataAnnotation.
                if (!string.IsNullOrEmpty(value) && value.Length > 8 && !value.Contains('-'))
                {
                    _d_no = value.Substring(0, 8) + "-" + value.Substring(8);
                }
                else
                {
                    _d_no = value;
                }
            }
        }
        private string _d_no;

        [ErsSchemaValidation("d_master_t.mall_d_no")]
        public string mall_d_no { get; set; }

        [ErsSchemaValidation("d_master_t.ccode")]
        public virtual string ccode { get; set; }

        [ErsSchemaValidation("campaign_t.id")]
        public virtual int? campaign_id { get; set; }

        #endregion

        #region Order Type
        [HtmlSubmitButton]
        public bool ordertype { get; set; }
        [HtmlSubmitButton]
        public bool ctsordertype { get; set; }
        [HtmlSubmitButton]
        public bool regordertype { get; set; }
        #endregion

        #region cancel date

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("af_cancel_date")]
        public DateTime? s_af_cancel_date_from { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("af_cancel_date")]
        public DateTime? s_af_cancel_date_to { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("cancel_date")]
        public DateTime? s_cancel_date_from { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Date)]
        [DisplayName("cancel_date")]
        public DateTime? s_cancel_date_to { get; set; }

        #endregion

        #region List Objects
        public List<Dictionary<string, object>> searchList { get; set; }

        public List<Dictionary<string, object>> searchClientList { get; set; }

        public List<Dictionary<string, object>> searchOrderList { get; set; }

        public List<Dictionary<string, object>> detailinfoList { get; private set; }

        public List<Dictionary<string, object>> inquiryList { get; set; }
        #endregion
    }
}