﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using ersContact.jp.co.ivp.ers.mvc;
using ersContact.Domain.Search.Mappables;
using ersContact.Domain.Search.Commands;
using jp.co.ivp.ers;

namespace ersContact.Models 
{
    public class  CtsSearchCtsOrder
        : ErsContactModelBase
        , ICtsOrderListMappable, ICtsOrderDeleteCommand
    {
        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsSearchListItemNumberOnPage; } }

        public long recordCount { get; set; }
        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long pagerPageCount { get; set; }

        public List<Dictionary<string, object>> searchCtsOrderList { get; set; }

        [ErsSchemaValidation("d_master_t.mcode")]
        public virtual string mcode { get; set; }

        [ErsSchemaValidation("cts_order_t.temp_d_no")]
        public int? temp_d_no { get; set; }

        [ErsSchemaValidation("d_master_t.d_no")]
        public int? d_no { get; set; }

        [ErsSchemaValidation("cts_order_t.user_id")]
        public virtual string user_id { get; set; }

        [ErsUniversalValidation]
        public string deleteOrder { get; set; }
    }
}