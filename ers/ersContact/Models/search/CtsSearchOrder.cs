﻿using System.Collections.Generic;
using System.Linq;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using ersContact.jp.co.ivp.ers.mvc;
using ersContact.Domain.Search.Mappables;
using ersContact.Domain.Search.Commands;
using jp.co.ivp.ers;

namespace ersContact.Models 
{
    public class  CtsSearchOrder
        : ErsContactModelBase
        , IOrderListMappable
    {
        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsSearchListItemNumberOnPage; } }

        public long recordCount { get; set; }
        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long pagerPageCount { get; set; }

        public List<Dictionary<string, object>> searchOrderList { get; set; }

        [ErsSchemaValidation("d_master_t.d_no")]
        public virtual string d_no
        {
            get
            {
                return _d_no;
            }
            set
            {
                //Formatting the d_no value since a hyphen is removed at validation of DataAnnotation.
                if (!string.IsNullOrEmpty(value) && value.Length > 8 && !value.Contains('-'))
                {
                    _d_no = value.Substring(0, 8) + "-" + value.Substring(8);
                }
                else
                {
                    _d_no = value;
                }
            }
        }
        private string _d_no;

        [ErsSchemaValidation("d_master_t.mcode")]
        public virtual string mcode { get; set; }

        public virtual int? site_id { get { return ErsFactory.ersUtilityFactory.getSetup().site_id; } } 
    }
}