﻿using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.order;
using ersContact.jp.co.ivp.ers.mvc;
using ersContact.Domain.Search.Mappables;
using jp.co.ivp.ers;

namespace ersContact.Models 
{
    public class  CtsSearchRegOrder
        : ErsContactModelBase
        , IRegularOrderListMappable
    {
        public ErsPagerModel pager { get; internal set; }
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsSearchListItemNumberOnPage; } }

        public long recordCount { get; set; }
        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long pagerPageCount { get; set; }

        public IList<ErsRegularOrder> orderList { get; set; }

        public List<Dictionary<string, object>> searchRegOrderList { get; set; }
        
        [ErsSchemaValidation("regular_t.mcode")]
        public virtual string mcode { get; set; }

        [ErsSchemaValidation("regular_t.id")]
        public int? id { get; set; }

        public void init(ErsPagerModel pager)
        {
            this.pager = pager;
        }

        [ErsOutputHidden("input_order")]
        [ErsSchemaValidation("regular_t.id")]
        public int? all_status { get; set; }

        public string interval { get; private set; }
    }
}