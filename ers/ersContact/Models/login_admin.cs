﻿using ersContact.Domain.Login.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersContact.Models
{
    public class Login_admin
        : ErsModelBase, ILoginAdminCommand
    {
        [ErsSchemaValidation("ransu_admin_t.ransu")]
        public string admin_ransu { get; set; }

        [ErsSchemaValidation("ransu_admin_t.ssl_ransu")]
        public string admin_ssl_ransu { get; set; }

        [ErsSchemaValidation("ransu_admin_t.user_cd")]
        public string user_cd { get; set; }

        [ErsSchemaValidation("d_master_t.d_no")]
        public string d_no { get; set; }
    }
}