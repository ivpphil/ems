﻿using System.Collections.Generic;
using ersContact.Domain.Information.Commands;
using ersContact.Domain.Information.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;

namespace ersContact.Models
{
    public class CtsInformation
        : ErsContactModelBase
        , IUnReadInfoListMappable, IModifyInfoListMappable, IInformationCommand
    {
        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsInformationListItemNumberOnPage; } }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long pagerPageCount { get; set; }

        [ErsSchemaValidation("cts_information_t.id")]
        public virtual int? id { get; set; }

        [ErsSchemaValidation("cts_information_t.ag_name")]
        public string ag_name { get; set; }

        [ErsSchemaValidation("cts_information_t.agent_id")]
        public int? agent_id { get; set; }

        [ErsSchemaValidation("cts_information_t.title")]
        public string title { get; set; }

        [ErsSchemaValidation("cts_information_t.contents")]
        public string contents { get; set; }

        [HtmlSubmitButton]
        public bool regist { get; set; }

        [HtmlSubmitButton]
        public bool modify { get; set; }

        [HtmlSubmitButton]
        public bool delete { get; set; }

        [HtmlSubmitButton]
        public bool done { get; set; }

        [HtmlSubmitButton]
        public bool cancel { get; set; }

        public List<Dictionary<string, object>> informationList { get; set; }

        public List<Dictionary<string, object>> unreadList { get; set; }

        public List<Dictionary<string, object>> readList { get; set; }
    }
}
