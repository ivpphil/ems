﻿using System.Collections.Generic;
using ersContact.Domain.Product.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;
using System.Linq;
using ersContact.Domain.Product.Commands;
using jp.co.ivp.ers.state;
using System;
using jp.co.ivp.ers.cts_operators;

namespace ersContact.Models
{
    public class CtsProduct
        : ErsContactModelBase
        , IProductMappable
        , IWishListRegistCommand, IWishListMappable, IWishListDeleteCommand
    {
        [HtmlSubmitButton]
        public bool search { get; set; }

        [HtmlSubmitButton]
        public bool IsOrderUpdate { get; set; }
        
        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().MerchandiseListItemNumberOnPage; } }

        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long pagerPageCount { get; set; }

        [ErsSchemaValidation("g_master_t.gcode")]
        public string gcode { get; set; }

        [ErsSchemaValidation("g_master_t.gname")]
        public string gname { get; set; }

        [ErsSchemaValidation("g_master_t.cate1")]
        public int? cate1 { get; set; }

        public string cate1_name { get { return ErsFactory.ersUtilityFactory.getSetup().cate1; } }

        public List<Dictionary<string, object>> cateList
        {
            get
            {
                var setup = ErsFactory.ersUtilityFactory.getSetup();
                if (setup.Multiple_sites)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().GetCategory(1, null,setup.site_id);
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewCategoryService().GetCategory(1, null);
            }
        }

        public List<Dictionary<string, object>> merchandiseList { get; set; }

        [ErsUniversalValidation]
        public string TargetController { get; set; }

        [ErsSchemaValidation("ransu_contact_t.ransu")]
        public string ransu { get; set; }

        [ErsSchemaValidation("member_t.mcode")]
        public string mcode { get; set; }

        public List<Dictionary<string, object>> ListPtnIntervalDay
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalDay, EnumCommonNameColumnName.namename);
            }
        }

        #region CTS WISHLIST PROPERTIES

        [HtmlSubmitButton]
        public bool btn_favorite { get; set; }

        [HtmlSubmitButton]
        public bool btn_share { get; set; }

        [HtmlSubmitButton]
        public bool btn_delete_f { get; set; }

        [ErsSchemaValidation("cts_wishlist_t.scode")]
        public string scode { get; set; }

        public List<Dictionary<string, object>> merchandiseFavoriteList { get; set; }

        public List<Dictionary<string, object>> merchandiseShareList { get; set; }

        /// <summary>
        /// 
        /// 
        /// 
        /// </summary>
        public bool IsSuperUser
        {
            get
            {
                return ErsCtsOperator.DEFAULT_USER_AUTHORITY != ((ErsSessionStateContact)ErsContext.sessionState).Get("cts_authority");
            }
        }

        #endregion
    }
}
