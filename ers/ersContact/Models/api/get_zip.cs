﻿using ersContact.Domain.Api.Commands;
using ersContact.Domain.Api.Mappables;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System;

namespace ersContact.Models
{
    /// <summary>
    /// 郵便番号検索処理
    /// </summary>
    public class getZip
        : ErsModelBase, IGetZipCommand, IGetZipMappable
    {
        //郵便番号 (入力パラメータ)
        //Postal code input parameters
        [ErsUniversalValidation(type = CHK_TYPE.HyphenNumber)]
        public string zip { get; set; }

        public int? pref { get; set; }

        public string address { get; set; }

        public string address2 { get; set; }

        public string inner_error_message { protected get; set; }

        public string error_message
        {
            get
            {
                if (!string.IsNullOrEmpty(this.inner_error_message))
                {
                    return this.inner_error_message;
                }

                return String.Join(ErsViewHelper.TAG_NEW_LINE, this.GetAllErrorMessageList());
            }
        }
    }
}
