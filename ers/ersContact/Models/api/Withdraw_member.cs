﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using ersContact.Domain.Api.Commands;
using jp.co.ivp.ers.mvc.validation;

namespace ersContact.Models.api
{
    public class Withdraw_member
        : ErsModelBase, IWithdrawMemberCommand
    {
        [ErsSchemaValidation("member_t.mcode")]
        public string mcode { get; set; }
    }
}