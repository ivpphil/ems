﻿using ersContact.Domain.Search.Commands;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersContact.Models.api
{
    public class DeleteReserved : ErsModelBase, ICtsOrderDeleteCommand
    {
        [ErsSchemaValidation("cts_order_t.user_id")]
        public virtual string user_id { get; set; }

        [ErsUniversalValidation]
        public string deleteOrder { get; set; }
    }
}