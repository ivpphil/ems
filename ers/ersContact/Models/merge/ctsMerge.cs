﻿using System;
using ersContact.Domain.Merge.Commands;
using ersContact.Domain.Merge.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers;
using System.ComponentModel;

namespace ersContact.Models 
{ 
    public class CtsMerge
        : ErsContactModelBase
        , IMergeMappable, IMergeCommand
    {
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int? page { get; set; }

        [HtmlSubmitButton]
        public bool next_member { get; set; }

        [HtmlSubmitButton]
        public bool final_member { get; set; }

        [HtmlSubmitButton]
        public bool return_member { get; set; }

        [HtmlSubmitButton]
        public bool next_case { get; set; }

        [HtmlSubmitButton]
        public bool final_case { get; set; }

        [HtmlSubmitButton]
        public bool return_case { get; set; }

        #region Member/Inquiry
        [ErsSchemaValidation("cts_merge_process_t.old_mcode")]
        public virtual string old_mcode { get; set; }

        public virtual string old_lname { get; set; }

        public virtual string old_fname { get; set; }

        public virtual string old_address { get; set; }

        public virtual string old_email { get; set; }

        [ErsSchemaValidation("cts_merge_process_t.new_mcode")]
        public virtual string new_mcode { get; set; }

        public virtual string new_lname { get; set; }

        public virtual string new_fname { get; set; }

        public virtual string new_address { get; set; }

        public virtual string new_email { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.case_no")]
        public virtual int? case_no { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.mcode")]
        [DisplayName("case_mcode")]
        public virtual string case_mcode { get; set; }

        public virtual string enq_casename { get; set; }

        public virtual DateTime? intime { get; set; }

        public virtual DateTime? utime { get; set; }

        public virtual EnumEnqType? enq_type { get; set; }
        #endregion

        public int? sale { get; set; }

        public EnumMergeProcessType? process_type { get; set; }

        public string user_id { get; set; }
    }
}