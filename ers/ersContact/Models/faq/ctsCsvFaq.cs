﻿using System.Linq;
using ersContact.Domain.Configuration.Commands;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersContact.Models 
{ 
    public class ctsCsvFaq
        : ErsContactModelBase, ICtsFaqUpdateCommand
    {
        /// <summary>
        /// Gets display message on finish page.
        /// </summary>
        public virtual string resultMsg
        {
            get
            {
                if (this.csv_file == null)
                {
                    return string.Empty;
                }
                return ErsResources.GetMessage("30000", this.csv_file.validIndexes.Count());
            }
        }

        /// <summary>
        /// 1行目をスキップする場合はtrue
        /// <para>If you want to skip the first line, value must be true</para>
        /// </summary>
        [ErsOutputHidden]
        [HtmlSubmitButton]
        public virtual bool chk_find { get; set; }

        /// <summary>
        /// アップロードデータ
        /// <para>Upload data</para>
        /// </summary>
        [ErsOutputHidden]
        [BindCsvFile]
        public ErsCsvContainer<csv.ctsFaq_csv> csv_file { get; set; }

        /// <summary>
        /// 登録ボタンの押下
        /// </summary>
        [HtmlSubmitButton]
        public virtual bool regist { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.active")]
        public virtual int replace { get; set; }

        public int? ctsUserID { get; set; }
    }
}