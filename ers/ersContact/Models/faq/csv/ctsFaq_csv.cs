﻿using ersContact.Domain.Configuration.Commands;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace ersContact.Models.csv
{
    public class ctsFaq_csv
        : ErsBindableModel, ICtsFaqCSVRecordCommand
    {

        [CsvField]
        [ErsSchemaValidation("cts_faq_template_t.faq_casename")]
        public virtual string faq_casename { get; set; }

        [CsvField]
        [ErsSchemaValidation("cts_faq_template_t.cate1")]
        public virtual int? cate1 { get; set; }

        [CsvField]
        [ErsSchemaValidation("cts_faq_template_t.cate2")]
        public virtual int? cate2 { get; set; }

        [CsvField]
        [ErsSchemaValidation("cts_faq_template_t.cate3")]
        public virtual int? cate3 { get; set; }

        [CsvField]
        [ErsSchemaValidation("cts_faq_template_t.cate4")]
        public virtual int? cate4 { get; set; }

        [CsvField]
        [ErsSchemaValidation("cts_faq_template_t.cate5")]
        public virtual int? cate5 { get; set; }

        [CsvField]
        [ErsSchemaValidation("cts_faq_template_t.faq_text")]
        public virtual string faq_text { get; set; }

        [CsvField]
        [ErsSchemaValidation("cts_faq_template_t.faq_text2")]
        public virtual string faq_text2 { get; set; }
    }
}
