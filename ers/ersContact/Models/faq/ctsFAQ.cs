﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Faq.Commands;
using ersContact.Domain.Faq.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace ersContact.Models 
{ 
    public class ctsFAQ
        : ErsContactModelBase
        , ICategoryListMappable, IFaqListMappable, IFaqRegisterCommand, IFaqUpdateCommand, IRegisterListMappable
    {
        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsSearchListItemNumberOnPage; } }
        
        public long recordCount { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public List<Dictionary<string, object>> searchList { get; set; }
        
        public bool openRegist { get; set; }

        [HtmlSubmitButton]
        public bool newregist { get; set; }
      
        [HtmlSubmitButton]
        public bool regist { get; set; }

        [HtmlSubmitButton]
        public bool reset { get; set; }

        [HtmlSubmitButton] 
        public bool back { get; set; }

        [HtmlSubmitButton]
        public bool search { get; set; }

        [HtmlSubmitButton]
        public bool showCat1 { get; set; }

        [HtmlSubmitButton]
        public bool showCat2 { get; set; }

        [HtmlSubmitButton]
        public bool showCat3 { get; set; }

        [HtmlSubmitButton]
        public bool showCat4 { get; set; }

        [HtmlSubmitButton]
        public bool showCat5 { get; set; }

        [ErsUniversalValidation]
        public string catlabel1 { get; set; }

        [ErsUniversalValidation]
        public string catlabel2 { get; set; }

        [ErsUniversalValidation]
        public string catlabel3 { get; set; }

        [ErsUniversalValidation]
        public string catlabel4 { get; set; }

        [ErsUniversalValidation]
        public string catlabel5 { get; set; }

        #region FAQ Template
        [ErsSchemaValidation("cts_faq_template_t.id")]
        public int? id { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.intime")]
        public DateTime? intime { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.utime")]
        public DateTime? utime { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.faq_casename")]
        public virtual string faq_casename { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.cate1")]
        public virtual int? cate1 { get; set; }
        
        [ErsSchemaValidation("cts_faq_template_t.cate2")]
        public virtual int? cate2 { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.cate3")]
        public virtual int? cate3 { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.cate4")]
        public virtual int? cate4 { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.cate5")]
        public virtual int? cate5 { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.faq_text")]
        public virtual string faq_text { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.faq_text2")]
        public virtual string faq_text2 { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.user_id")]
        public virtual int? user_id { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.active")]
        public virtual EnumActive? active { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.faq_text")]
        public virtual string keywords { get; set; }
        #endregion

        #region Category List
        public List<Dictionary<string, object>> CateList1
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT1, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> CateList2
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT2, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> CateList3
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT3, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> CateList4
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT4, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> CateList5
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT5, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        #endregion
    }
}