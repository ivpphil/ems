﻿using ersContact.Domain.Configuration.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace ersContact.Models 
{ 
    public class ctsFaq_csv
        : ErsContactModelBase, ICtsFaqCSVDownMappable
    {
        public ErsCsvCreater csvCreater { get; set; }
    }
}