﻿using System;
using System.Collections.Generic;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.doc_bundle;


namespace ersContact.Models
{
    public class Campaign
        : ErsContactModelBase
    {
        [ErsSchemaValidation("d_master_t.ccode")]
        public string ccode { get; set; }

        public string campaign_name
        {
            get
            {
                return GetCampaignName();
            }
        }

        private string GetCampaignName()
        {
            ErsCampaignRepository repository = ErsFactory.ersDocBundleFactory.GetErsCampaignRepository();
            ErsCampaignCriteria crtCampaign = ErsFactory.ersDocBundleFactory.GetErsCampaignCriteria();

            crtCampaign.ccode = this.ccode;
            crtCampaign.active = EnumActive.Active;

            var list = repository.Find(crtCampaign);

            if (list.Count > 0)
            {
                string campaign_name = list[0].campaign_name;

                return campaign_name;
            }
            else
                return String.Empty;
        }
    }
}
