﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using ersContact.Domain.Order.Commands;
using ersContact.Domain.Order.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using ersContact.Models.cart;
using jp.co.ivp.ers;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.order.related;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.viewService;

namespace ersContact.Models
{
    public class CtsOrder
        : ErsContactModelBase
        , IOrderMappable, IMemberCommand, IMemberMappable, IShippingMappable, IShippingCommand
        , IPaymentCommand, IPaymentMappable, IDeliveryCommand, IDeliveryMappable
        , IPointMappable, IRegularMappable
        , IReCalcMappable, ICtsOrderCommand, IOrderCommand, IOrderRecomputeMappable, IDelivMethodDisplayListMappable
    {
        #region . Basic .
        public CtsOrder()
        {
            this.orderContainer = ErsFactory.ersOrderFactory.GetErsOrderContainer();

            this.Debug = ErsFactory.ersUtilityFactory.getSetup().debug;
            this.firstTimeOrdinary = 0;
            this.disable_issue = false;
            this.mall_shop_kbn = EnumMallShopKbn.ERS;

            //set default date for to be able to compare.
            if (string.IsNullOrEmpty(this.order_d_no))
            {
                this.coupon_code = null;

                //this.deliv_method = EnumDelvMethod.Express;
            }
            this.ent_coupon_code = null;

        }

        public ErsOrderContainer orderContainer { get; set; }

        public string Cts_Ransu { get; set; }

        public int cts_agent_id { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("cts_order_t.id")]
        public int Edit_Temp { get; set; }

        [HtmlSubmitButton]
        public bool goto_page1 { get; set; }

        [HtmlSubmitButton]
        public bool goto_page2 { get; set; }

        [HtmlSubmitButton]
        public bool goto_page3 { get; set; }

        [HtmlSubmitButton]
        public bool from_page1 { get; set; }

        [HtmlSubmitButton]
        public bool from_page2 { get; set; }

        [HtmlSubmitButton]
        public bool from_page3 { get; set; }

        [HtmlSubmitButton]
        public bool goto_page4 { get; set; }

        [HtmlSubmitButton]
        public bool recompute { get; set; }

        [ErsOutputHidden("input_payment")]
        [HtmlSubmitButton]
        public bool page1 { get; set; }

        [ErsOutputHidden("input_payment")]
        [HtmlSubmitButton]
        public bool page2 { get; set; }

        [ErsOutputHidden("input_payment")]
        [HtmlSubmitButton]
        public bool page3 { get; set; }

        [ErsOutputHidden("input_payment")]
        [HtmlSubmitButton]
        public bool page4 { get; set; }

        [HtmlSubmitButton]
        public bool from_page4 { get; set; }

        [HtmlSubmitButton]
        public bool reaload_cart { get; set; }

        [HtmlSubmitButton]
        public bool load_new_order { get; set; }

        [HtmlSubmitButton]
        public bool load_same_order { get; set; }

        [HtmlSubmitButton]
        public bool card_delete { get; set; }

        [HtmlSubmitButton]
        public bool card_update { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber)]
        public bool member_edit { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber)]
        public bool shipping_edit { get; set; }

        /// <summary>
        /// regular order items
        /// </summary>
        [BindTable("regularBasketItems")]
        public List<Cart_regular_items> regularBasketItems { get; set; }

        public bool Debug { get; private set; }

        public string HighlightColor { get { return ErsFactory.ersUtilityFactory.getSetup().cts_highlightcolor; } }

        //戻り先設定
        public string getReturnUrl()
        {
            Dictionary<string, object> setDic = new Dictionary<string, object>();
            add_hidden_list = new List<Dictionary<string, object>>();
            if (this.shipping_register)
            {
                setDic["name"] = "shipping_err";
                setDic["value"] = "true";
                add_hidden_list.Add(setDic);
            }
            string path = ErsFactory.ersUtilityFactory.getSetup().sec_url + "top/order/asp/";
            return path + "order.asp";
        }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public List<Dictionary<string, object>> add_hidden_list { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.All)]
        [DisplayName("TargetController")]
        public bool shipping_err { get; set; }

        /// <summary>
        /// 前回カード情報 使用フラグ
        /// </summary>
        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeTo = 1)]
        public int? last_used_card_info { get; set; }

        /// <summary>
        /// 前回未預けカード使用判定
        /// </summary>
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public bool last_used_card_flg
        {
            get
            {
                if (string.IsNullOrEmpty(this.order_d_no) == false)
                {
                    var d_no = this.order_d_no;
                    if (!d_no.Contains("-"))
                    {
                        d_no += "-01";
                    }
                    return ErsFactory.ersOrderFactory.GetLastUsedCardInfoStrategy().CheckLastOrderCardStatus(d_no);
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsNonNeededPaymentSpec
        {
            get
            {
                return ErsFactory.ersOrderFactory.GetIsNonNeededPaymentSpec().IsSpecified(this.order_total);
            }
        }

        public bool IsAllBasketItemMailDelivery
        {
            get
            {
                return ErsFactory.ersOrderFactory.GetIsAllBasketItemMailDeliverySpec().Satisfy(this.cts_order_ransu);
            }
        }
        #endregion

        #region . order .
        /// <summary>
        /// d_no of the target of the Oder update
        /// </summary>
        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.d_no")]
        public string order_d_no
        {
            get
            {
                return _order_d_no;
            }
            set
            {
                //Formatting the d_no value since a hyphen is removed at validation of DataAnnotation.
                if (!string.IsNullOrEmpty(value) && value.Length > 8 && !value.Contains('-'))
                {
                    _order_d_no = value.Substring(0, 8) + "-" + value.Substring(8);
                }
                else
                {
                    _order_d_no = value;
                }
            }
        }

        private string _order_d_no;

        /// <summary>
        /// whether update order is being oparated or not.
        /// </summary>
        public bool IsOrderUpdate { get { return !string.IsNullOrEmpty(order_d_no); } }

        [ErsSchemaValidation("d_master_t.order_payment_status")]
        public int order_status { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 1)]
        public int? send_chk { get; set; }

        [ErsSchemaValidation("d_master_t.send")]
        public EnumSendTo? send { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.send")]
        [DisplayName("send")]
        public EnumSendTo? old_send { get; set; }

        [ErsSchemaValidation("d_master_t.wrap")]
        public virtual EnumWrap? wrap { get; set; }

        [ErsSchemaValidation("d_master_t.memo2")]
        public virtual string memo2 { get; set; }

        [ErsSchemaValidation("d_master_t.senddate")]
        public virtual DateTime? senddate { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 0, rangeTo = 2)]
        public virtual EnumFirstTimeOrdinary? firstTimeOrdinary { get; set; }

        [ErsSchemaValidation("d_master_t.sendtime")]
        public virtual int? sendtime { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.point_magnification")]
        public virtual int? point_magnification { get; set; }

        /// <summary>
        /// Returns same value as sendtime
        /// </summary>
        [ErsSchemaValidation("d_master_t.sendtime")]
        public virtual int? regular_sendtime { get; set; }

        [ErsOutputHidden("input_email")]
        [ErsSchemaValidation("d_master_t.email")]
        public virtual string email { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.pm_flg")]
        public virtual EnumPmFlg? pm_flg { get; set; }

        public bool showPmflg
        {
            get
            {
                return (this.pm_flg != EnumPmFlg.CTS && this.pm_flg != EnumPmFlg.REGULAR);
            }
        }

        public virtual int? point_ck { get; set; }

        public virtual EnumOrderPaymentStatusType? order_payment_status { get; set; }

        public virtual DateTime? paid_date { get; set; }

        public virtual int? paid_price { get; set; }

        public virtual DateTime? intime { get; set; }

        public List<Dictionary<string, object>> pm_flgList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PmFlg, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> convenienceList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ConvCode, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> OrderStatusList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewOrderStatusService().SelectAsList(); }
        }

        public int disp_subtotal
        {
            get
            {
                if (this.orderContainer.OrderHeader == null)
                {
                    if (this.orderIntegrated != null)
                    {
                        return this.orderIntegrated.subtotal;
                    }
                    else
                    {
                        return 0;
                    }
                }

                return this.orderContainer.OrderHeader.subtotal;
            }
        }

        public int? disp_tax
        {
            get
            {
                if (this.orderContainer.OrderHeader == null)
                {
                    if (this.orderIntegrated != null)
                    {
                        return this.orderIntegrated.tax;
                    }
                    else
                    {
                        return 0;
                    }
                }

                return this.orderContainer.OrderHeader.tax;
            }
        }

        public int? disp_carriage
        {
            get
            {
                if (this.orderContainer.OrderHeader == null)
                {
                    if (this.orderIntegrated != null)
                    {
                        return this.orderIntegrated.carriage;
                    }
                    else
                    {
                        return 0;
                    }
                }

                return this.orderContainer.OrderHeader.carriage;
            }
        }

        public int? disp_etc
        {
            get
            {
                if (this.orderContainer.OrderHeader == null)
                {
                    if (this.orderIntegrated != null)
                    {
                        return this.orderIntegrated.etc;
                    }
                    else
                    {
                        return 0;
                    }
                }

                return this.orderContainer.OrderHeader.etc;
            }
        }

        public int disp_total
        {
            get
            {
                if (this.basket == null)
                    return 0;

                return this.basket.amounttotal + this.basket.regular_amounttotal;
            }
        }

        //クーポン割引額
        public int? disp_coupon_discount
        {
            get
            {
                if (this.orderContainer.OrderHeader == null)
                {
                    if (this.orderIntegrated != null)
                    {
                        return this.orderIntegrated.coupon_discount;
                    }
                    else
                    {
                        return 0;
                    }
                }

                return orderContainer.OrderHeader.coupon_discount;
            }
        }

        public int? disp_p_service
        {
            get
            {
                if (this.orderContainer.OrderHeader == null)
                {
                    if (this.orderIntegrated != null)
                    {
                        return this.orderIntegrated.p_service;
                    }
                    else
                    {
                        return 0;
                    }
                }

                return this.orderContainer.OrderHeader.p_service;
            }
        }

        public int? disp_order_total_amount
        {
            get
            {
                if (this.orderContainer.OrderHeader == null)
                {
                    if (this.orderIntegrated != null)
                    {
                        return this.orderIntegrated.total;
                    }
                    else
                    {
                        return 0;
                    }
                }

                return this.orderContainer.OrderHeader.total;
            }
        }

        public string w_etc
        {
            get
            {
                EnumPaymentType? _pay;
                if (this.orderContainer.OrderHeader != null && this.orderContainer.OrderHeader.pay.HasValue)
                {
                    _pay = orderContainer.OrderHeader.pay;
                }
                else if (this.orderIntegrated != null)
                {
                    _pay = this.orderIntegrated.pay;
                }
                else
                {
                    return null;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetEtcNameFromId(orderContainer.OrderHeader.pay);
            }
        }

        [ErsSchemaValidation("d_master_t.subtotal")]
        public int? subtotal { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.tax")]
        public int? tax { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.carriage")]
        public int? carriage { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.etc")]
        public int? etc { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.coupon_discount")]
        public int? coupon_discount { get; set; }

        [ErsSchemaValidation("d_master_t.coupon_discount")]
        public int? old_coupon_discount { get; set; }

        [ErsSchemaValidation("d_master_t.p_service")]
        public int? p_service { get; set; }

        /// クーポン再計算がおされた場合
        [HtmlSubmitButton]
        public bool coupon_flg { get; set; }

        [ErsSchemaValidation("d_master_t.mall_shop_kbn")]
        public EnumMallShopKbn? mall_shop_kbn { get; set; }

        /// <summary>
        /// 送料再計算ボタン
        /// </summary>
        [HtmlSubmitButton]
        public bool carriage_recomp_flg { get; set; }

        /// <summary>
        /// 数量再計算
        /// </summary>
        [HtmlSubmitButton]
        public bool recalc { get; set; }

        /// <summary>
        /// 定期の数量再計算
        /// </summary>
        [HtmlSubmitButton]
        public bool regular_recalc { get; set; }

        [ErsSchemaValidation("d_master_t.coupon_code")]
        public string coupon_code { get; set; }

        [ErsSchemaValidation("d_master_t.coupon_code")]
        public string ent_coupon_code { get; set; }

        [DisplayName("coupon_code")]
        [ErsSchemaValidation("d_master_t.coupon_code")]
        public string old_coupon_code { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.ccode")]
        public string ccode { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("ds_master_t.deliv_method")]
        public virtual EnumDelvMethod? deliv_method { get; set; }

        [ErsSchemaValidation("d_master_t.pay")]
        public EnumPaymentType? pay { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.pay")]
        [DisplayName("pay")]
        public EnumPaymentType? old_pay { get; set; }

        [ErsSchemaValidation("d_master_t.member_add_id")]
        public int? member_add_id { get; set; }

        [ErsSchemaValidation("d_master_t.member_card_id")]
        public int? member_card_id { get; set; }

        public bool isKanshi { get; set; }

        public bool order_success { get; set; }

        //カード入力フォーム表示フラグ
        private int entryCardInfoCount { get { return ErsFactory.ersUtilityFactory.getSetup().entryCardInfoCount; } }

        //カード情報入力フォームの表示フラグ
        public Boolean newEntryFlg
        {
            get
            {
                if (this.membercardList == null || this.membercardList.Count() < entryCardInfoCount)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region . temporary order .
        [HtmlSubmitButton]
        public bool cts_order_register { get; set; }

        [HtmlSubmitButton]
        public bool cts_order_success { get; set; }

        [ErsSchemaValidation("cts_order_t.id")]
        public int? cts_order_id { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("cts_order_t.temp_d_no")]
        public int? cts_order_temp_d_no { get; set; }

        [ErsSchemaValidation("cts_order_t.temp_odate")]
        public DateTime? cts_order_temp_odate { get; set; }

        [ErsSchemaValidation("cts_order_t.id")]
        public DateTime? cts_order_work_time { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("cts_order_t.ransu")]
        public string cts_order_ransu { get; set; }

        [ErsSchemaValidation("cts_order_t.lname")]
        public string cts_order_lname { get; set; }

        [ErsSchemaValidation("cts_order_t.fname")]
        public string cts_order_fname { get; set; }

        [ErsSchemaValidation("cts_order_t.lnamek")]
        public string cts_order_lnamek { get; set; }

        [ErsSchemaValidation("cts_order_t.fnamek")]
        public string cts_order_lfnamek { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("cts_order_t.memo")]
        public string cts_order_memo { get; set; }

        [ErsSchemaValidation("cts_order_t.charge1")]
        public string cts_order_charge1 { get; set; }

        [ErsSchemaValidation("cts_order_t.charge2")]
        public string cts_order_charge2 { get; set; }

        [ErsSchemaValidation("cts_order_t.charge3")]
        public string cts_order_charge3 { get; set; }

        [ErsSchemaValidation("cts_order_t.status")]
        public int cts_order_status { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("cts_order_t.user_id")]
        public string cts_order_user_id { get; set; }

        /// <summary>
        /// 受注時のオペレータID
        /// </summary>
        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("cts_order_t.user_id")]
        public string ordered_user_id { get; set; }

        [ErsUniversalValidation]
        public string ordered_ag_name { get; set; }

        [ErsSchemaValidation("cts_order_t.tel")]
        public string cts_order_tel { get; set; }

        [ErsSchemaValidation("cts_order_t.subtotal")]
        public int cts_order_subtotal { get; set; }

        [ErsSchemaValidation("cts_order_t.charge1_ag_type")]
        public EnumAgType? cts_order_charge1_ag_type { get; set; }

        [ErsSchemaValidation("cts_order_t.charge1_ag_name")]
        public string cts_order_charge1_ag_name { get; set; }
        #endregion

        #region . client information .
        public ErsMember member { get; set; }

        [HtmlSubmitButton]
        public bool member_modify { get; set; }

        [HtmlSubmitButton]
        public bool member_register { get; set; }

        [HtmlSubmitButton]
        public bool member_cancel { get; set; }

        [ErsSchemaValidation("member_t.id")]
        public virtual int? member_id { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.mcode")]
        public virtual string mcode { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.lname")]
        public virtual string lname { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.fname")]
        public virtual string fname { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.lnamek")]
        public virtual string lnamek { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.fnamek")]
        public virtual string fnamek { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.tel")]
        public virtual string tel { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.zip")]
        public virtual string zip { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.pref")]
        public virtual int? pref { get; set; }

        public virtual string pref_name { get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(pref); } }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.address")]
        public virtual string address { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.taddress")]
        public virtual string taddress { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.maddress")]
        public virtual string maddress { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.sex")]
        public EnumSex? sex { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = ErsViewBirthdayService.minimumOfSelectyear, rangeTo = ErsViewBirthdayService.maximumOfSelectyear)]
        public int? birthday_y { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 12)]
        public int? birthday_m { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 31)]
        public int? birthday_d { get; set; }

        [ErsSchemaValidation("member_t.dm_flg")]
        [ErsOutputHidden("input_payment")]
        public virtual EnumDmFlg? dm_flg { get; set; }

        [ErsSchemaValidation("member_t.memo")]
        [ErsOutputHidden("input_payment")]
        public virtual string memo { get; set; }

        public virtual string w_dm_flg { get; set; }

        [ErsSchemaValidation("member_t.out_bound_flg")]
        [ErsOutputHidden("input_payment")]
        public virtual EnumOutBoundFlg? out_bound_flg { get; set; }

        public virtual string w_out_bound_flg { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.lname")]
        public virtual string wk_lname { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.fname")]
        public virtual string wk_fname { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.lnamek")]
        public virtual string wk_lnamek { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.fnamek")]
        public virtual string wk_fnamek { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.tel")]
        public virtual string wk_tel { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.zip")]
        public virtual string wk_zip { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.pref")]
        public virtual int? wk_pref { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("pref_t.pref_name")]
        public virtual string wk_pref_name { get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(wk_pref); } }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.address")]
        public virtual string wk_address { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.taddress")]
        public virtual string wk_taddress { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("member_t.maddress")]
        public virtual string wk_maddress { get; set; }

        [ErsOutputHidden("input_email")]
        [ErsSchemaValidation("d_master_t.email")]
        public virtual string wk_email { get; set; }

        public DateTime? birth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetBirthDay(this.birthday_y, this.birthday_m, this.birthday_d);
            }
        }

        public List<Dictionary<string, object>> birthday_yList
        {
            get
            {
                var min_user_age = ErsFactory.ersUtilityFactory.getSetup().min_user_age;
                var max_user_age = ErsFactory.ersUtilityFactory.getSetup().max_user_age;
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListYear(max_user_age, min_user_age);
            }
        }

        public List<Dictionary<string, object>> birthday_mList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListMonth(); }
        }

        public List<Dictionary<string, object>> birthday_dList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListDay(); }
        }

        public List<Dictionary<string, object>> prefList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(false, this.site_id); }
        }

        public List<Dictionary<string, object>> sexList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> ageTypeList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ORDAGE, EnumCommonNameColumnName.namename);
            }
        }

        public string w_sex
        {
            get
            {
                if (!this.sex.HasValue)
                {
                    return null;
                }

                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename, (int)this.sex.Value);
            }
        }

        public List<Dictionary<string, object>> shippingList { get; set; }

        #endregion

        #region . shipping address .
        [HtmlSubmitButton]
        public bool shipping_new { get; set; }

        [HtmlSubmitButton]
        public bool shipping_modify { get; set; }

        [HtmlSubmitButton]
        public bool shipping_delete { get; set; }

        [HtmlSubmitButton]
        public bool shipping_register { get; set; }

        [HtmlSubmitButton]
        public bool shipping_cancel { get; set; }

        [ErsSchemaValidation("addressbook_t.id")]
        public int? modify_shipping_id { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.id")]
        public int? shipping_id { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.id")]
        [DisplayName("shipping_id")]
        public int? old_shipping_id { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.address_name")]
        public string address_name { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_lname")]
        public string add_lname { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_fname")]
        public string add_fname { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_lnamek")]
        public string add_lnamek { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_fnamek")]
        public string add_fnamek { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_zip")]
        public string add_zip { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_pref")]
        public int? add_pref { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_pref")]
        public string add_pref_name { get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(add_pref); } }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_address")]
        public string add_address { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_taddress")]
        public string add_taddress { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_maddress")]
        public string add_maddress { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_tel")]
        public string add_tel { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeTo = 1)]
        public EnumAddressAdd address_add { get; set; }

        //編集時未保存別住所
        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_lname")]
        [DisplayName("add_lname")]
        public string wk_add_lname { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_fname")]
        [DisplayName("add_fname")]
        public string wk_add_fname { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_lnamek")]
        [DisplayName("add_lnamek")]
        public string wk_add_lnamek { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_fnamek")]
        [DisplayName("add_fnamek")]
        public string wk_add_fnamek { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_zip")]
        [DisplayName("add_zip")]
        public string wk_add_zip { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_pref")]
        [DisplayName("add_pref")]
        public int? wk_add_pref { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_pref")]
        [DisplayName("add_pref_name")]
        public string wk_add_pref_name { get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().GetStringFromId(wk_add_pref); } }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_address")]
        [DisplayName("add_address")]
        public string wk_add_address { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_taddress")]
        [DisplayName("add_taddress")]
        public string wk_add_taddress { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_maddress")]
        [DisplayName("add_maddress")]
        public string wk_add_maddress { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("addressbook_t.add_tel")]
        [DisplayName("add_tel")]
        public string wk_add_tel { get; set; }
        #endregion

        #region . payment method .
        [HtmlSubmitButton]
        public bool card_new { get; set; }

        [HtmlSubmitButton]
        public bool card_register { get; set; }

        [HtmlSubmitButton]
        public bool card_cancel { get; set; }

        [HtmlSubmitButton]
        public bool card_will_add { get; set; }

        public List<Dictionary<string, object>> membercardList { get; set; }

        public List<Dictionary<string, object>> membercardListExtend { get; set; }

        public CreditCardInfo savedCardInfo { get; set; }

        public List<Dictionary<string, object>> cardList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCardService().SelectAsList(this.site_id); }
        }

        public List<Dictionary<string, object>> card_yList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCardService().GetListYear(); }
        }

        public List<Dictionary<string, object>> card_mList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCardService().GetListMonth(); }
        }

        public virtual string w_deliv_method
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.DelvMethod, EnumCommonNameColumnName.namename, (int?)this.deliv_method);
            }
        }

        public virtual bool canSelectMailDelv
        {
            get
            {
                return this.basketItems != null
                    && this.basketItems.Count() == 1
                    && this.basketItems.First().deliv_method == EnumDelvMethod.Mail
                    && this.basketItems.First().amount == 1
                    && (this.regularBasketItems == null || this.regularBasketItems.Count() == 0);
            }
        }

        public List<Dictionary<string, object>> deliv_methodList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.DelvMethod, EnumCommonNameColumnName.namename); }
        }

        public List<Dictionary<string, object>> payList
        {
            get
            {
                var activeOnly = !this.IsOrderUpdate;
                var setup = ErsFactory.ersUtilityFactory.getSetup();

                return ErsFactory.ersViewServiceFactory.GetErsViewPayService().SelectAsList(!this.IsOrderUpdate && this.regularBasketItemCount > 0, activeOnly); ;
            }

        }

        [ErsSchemaValidation("member_card_t.id")]
        public virtual int? card_id { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 100)]
        public virtual string card_holder_name { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? card_type { get; set; }

        [ErsUniversalValidation(rangeFrom = 13, rangeTo = 16, type = CHK_TYPE.NumericString)]
        public virtual string cardno { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? validity_y { get; set; }

        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 12, type = CHK_TYPE.Numeric)]
        public virtual int? validity_m { get; set; }

        [ErsUniversalValidation(rangeFrom = 3, rangeTo = 3, type = CHK_TYPE.NumericString)]
        public virtual string securityno { get; set; }

        [ErsOutputHidden("input_payment")]
        [DisplayName("card_holder_name")]
        [ErsUniversalValidation(type = CHK_TYPE.HalfAlphabetOrNumber, rangeTo = 100)]
        public virtual string disp_card_holder_name { get; set; }

        [ErsOutputHidden("input_payment")]
        [DisplayName("card_type")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? disp_card_type { get; set; }

        [ErsOutputHidden("input_payment")]
        [DisplayName("cardno")]
        [ErsUniversalValidation(rangeFrom = 13, rangeTo = 16, type = CHK_TYPE.NumericString)]
        public virtual string disp_cardno { get; set; }

        [ErsOutputHidden("input_payment")]
        [DisplayName("card_type_id")]
        [ErsUniversalValidation()]
        public virtual int? disp_card_type_id { get; set; }

        [ErsOutputHidden("input_payment")]
        [DisplayName("validity_y")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? disp_validity_y { get; set; }

        [ErsOutputHidden("input_payment")]
        [DisplayName("validity_m")]
        [ErsUniversalValidation(rangeFrom = 1, rangeTo = 12, type = CHK_TYPE.Numeric)]
        public virtual int? disp_validity_m { get; set; }

        [ErsOutputHidden("input_payment")]
        [DisplayName("securityno")]
        [ErsUniversalValidation(rangeFrom = 3, rangeTo = 3, type = CHK_TYPE.NumericString)]
        public virtual string disp_securityno { get; set; }

        [ErsOutputHidden("input_payment")]
        [ErsSchemaValidation("d_master_t.conv_code")]
        public virtual EnumConvCode? conv_code { get; set; }

        public int card_count
        {
            get
            {
                if (membercardList == null)
                {
                    return 0;
                }
                else
                {
                    return membercardList.Count() - 1;
                }
            }
        }

        public bool existCreditData
        {
            get
            {
                if (this.membercardList == null || this.membercardList.Count() == 0)
                    return false;

                else
                    return true;
            }
        }

        /// <summary>
        /// for Display Card Type Name
        /// </summary>
        public virtual string w_card_type
        {
            get
            {
                if (this.disp_card_type != null)
                {
                    var ctype = ErsFactory.ersOrderFactory.GetErsCardWithId((int)this.disp_card_type);
                    if (ctype != null)
                    {
                        return ctype.card_name;
                    }
                }
                return null;
            }
        }
        #endregion

        #region . deliveries .
        public ErsBasket basket { get; set; }

        public List<Dictionary<string, object>> sendtimeList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().SelectAsList(); }
        }

        public List<Dictionary<string, object>> senddateList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewCalendarService().SelectAsList(null, DateTime.Now.AddDays(ErsFactory.ersUtilityFactory.getSetup().sendday_count)); }
        }

        //fields for regular order
        public List<Dictionary<string, object>> ListPtnIntervalDay
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalDay, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ListPtnIntervalMonth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalMonth, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ListPtnDay
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnDay, EnumCommonNameColumnName.namename);
            }
        }

        public List<DateTime> ListRegularSenddateMonth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsRegularOrderViewService().GetListRegularSenddateMonth();
            }
        }

        public List<Dictionary<string, object>> ListPtnIntervalWeek
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnIntervalWeek, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> ListPtnWeekday
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.PtnWeekday, EnumCommonNameColumnName.namename);
            }
        }

        public string senddate_v { get; set; }

        public string senddate_t { get; set; }

        public bool senddate_add { get; set; }

        [BindTable("basketItems")]
        public List<Cart_items> basketItems { get; set; }

        [HtmlDictionary("up_amount")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public Dictionary<string, int?> up_amount_list { get; set; }

        [HtmlDictionary("product_status")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public Dictionary<string, EnumOrderStatusType> product_status_list { get; set; }

        public virtual int setup_tax { get { return ErsFactory.ersUtilityFactory.getSetup().tax; } }

        /// <summary>
        /// ordinary order items count
        /// </summary>
        public int basketItemCount
        {
            get
            {
                if (basketItems == null)
                    return 0;
                else
                    return basketItems.Count;
            }
        }

        /// <summary>
        /// regular order items count
        /// </summary>
        public int regularBasketItemCount
        {
            get
            {
                if (regularBasketItems == null)
                    return 0;
                else
                    return regularBasketItems.Count;
            }
        }

        [ErsOutputHidden]
        [HtmlSubmitButton]
        public bool disable_issue { get; set; }
        #endregion

        #region . points .
        /// <summary>
        /// the number of the point which is now updating order.
        /// </summary>
        [ErsOutputHidden("input_point")]
        [ErsSchemaValidation("point_t.now_p")]
        public int ent_point_orig { get; set; }

        [ErsOutputHidden("input_point")]
        [ErsSchemaValidation("point_t.now_p")]
        public int? ent_point { get; set; }

        [ErsSchemaValidation("point_t.now_p")]
        public int points_available { get; set; }
        #endregion

        #region . order .
        public IEnumerable<ErsBaskRecord> orderRecords
        {
            get
            {
                if (orderContainer == null)
                {
                    yield break;
                }

                foreach (var record in basket.objBasketRecord.Values)
                {
                    yield return record;
                }

                foreach (var record in basket.objRegularBasketRecord.Values)
                {
                    yield return record;
                }
            }
        }
        #endregion


        public int? card { get; set; }

        public string security_no { get; set; }


        public string ransu { get; set; }

        public string memo3 { get; set; }

        public string user_id { get; set; }

        public string d_no { get; set; }

        public EnumDeleted? deleted { get; set; }

        [ErsOutputHidden("input_payment")]
        [HtmlSubmitButton]
        public bool non_member { get; set; }

        public bool ValidateMappedOrder { get; set; }


        public bool GetOrderForUpdate { get; set; }

        public bool IsResetOrder
        {
            get
            {
                return !this.IsOrderUpdate &&
                    (this.cts_order_temp_d_no != null || this.cts_order_temp_d_no > 0);
            }
        }

        #region for purchase mail.
        //for purchase mail.
        public ErsOrderIntegrated orderIntegrated { get; set; }

        public string d_noList
        {
            get
            {
                if (this.orderIntegrated == null)
                {
                    if (this.orderContainer != null && this.orderContainer.OrderHeader != null)
                    {
                        return this.orderContainer.OrderHeader.d_no;
                    }
                    else
                    {
                        return null;
                    }
                }

                var listResult = new List<string>();
                foreach (var innerOrder in this.orderIntegrated.GetListOrder())
                {
                    listResult.Add(innerOrder.OrderHeader.d_no);
                }

                return string.Join(",", listResult);
            }
        }

        public string w_sendtime
        {
            get
            {
                if (orderIntegrated != null && orderIntegrated.sendtime != null)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId(orderIntegrated.sendtime);
                }
                else
                {
                    if (this.orderContainer != null && this.orderContainer.OrderHeader != null)
                    {
                        return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId(this.orderContainer.OrderHeader.sendtime);
                    }
                    else
                    {
                        return string.Empty;
                    }
                }

            }
        }

        public string w_regular_sendtime
        {
            get
            {
                if (this.regular_sendtime != null)
                {
                    return ErsFactory.ersViewServiceFactory.GetErsViewSendtimeService().GetStringFromId((int)this.regular_sendtime);
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public virtual int amounttotal_all_item
        {
            get
            {
                if (this.basket == null)
                {
                    return 0;
                }

                return this.basket.amounttotal + this.basket.regular_amounttotal;
            }
        }

        public int? order_subtotal
        {
            get
            {
                if (orderIntegrated == null)
                {
                    if (this.orderContainer != null && this.orderContainer.OrderHeader != null)
                    {
                        return this.orderContainer.OrderHeader.subtotal;
                    }
                    else
                    {
                        return 0;
                    }
                }
                return orderIntegrated.subtotal;
            }
        }

        public int? order_tax
        {
            get
            {
                if (orderIntegrated == null)
                {
                    if (this.orderContainer != null && this.orderContainer.OrderHeader != null)
                    {
                        return this.orderContainer.OrderHeader.tax;
                    }
                    else
                    {
                        return 0;
                    }
                }
                return orderIntegrated.tax;
            }
        }

        public int? order_total
        {
            get
            {
                if (orderIntegrated == null)
                {
                    if (this.orderContainer != null && this.orderContainer.OrderHeader != null)
                    {
                        return this.orderContainer.OrderHeader.total;
                    }
                    else
                    {
                        return 0;
                    }
                }

                return orderIntegrated.total;
            }
        }


        public virtual string w_pay
        {
            get
            {
                EnumPaymentType? _pay;
                if (orderIntegrated != null)
                {
                    _pay = orderIntegrated.pay;
                }
                else if (this.orderContainer != null && this.orderContainer.OrderHeader != null)
                {
                    _pay = this.orderContainer.OrderHeader.pay;
                }
                else
                {
                    return string.Empty;
                }

                var setup = ErsFactory.ersUtilityFactory.getSetup();
                return ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetStringFromId(_pay);
            }
        }

        public IEnumerable<Dictionary<string, object>> orderHeaderList
        {
            get
            {
                if (this.orderIntegrated == null)
                {
                    if (this.orderContainer != null && this.orderContainer.OrderHeader != null)
                    {
                        var dictionary = orderContainer.OrderHeader.GetPropertiesAsDictionary();
                        dictionary["w_ccode"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ConvCode, EnumCommonNameColumnName.namename, (int?)this.orderContainer.OrderHeader.conv_code);
                        yield return dictionary;
                    }

                    yield break;
                }

                var listResult = new List<string>();
                foreach (var innerOrder in this.orderIntegrated.GetListOrder())
                {
                    var dictionary = innerOrder.OrderHeader.GetPropertiesAsDictionary();
                    dictionary["w_ccode"] = ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetStringFromId(EnumCommonNameType.ConvCode, EnumCommonNameColumnName.namename, (int?)innerOrder.OrderHeader.conv_code);
                    yield return dictionary;
                }
            }
        }

        public string purchasedmail_fullname
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return string.Format("{0} {1}", this.lname, this.fname);

                return string.Format("{0} {1}", this.add_lname, this.add_fname);
            }
        }

        public string purchasedmail_zip
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return this.zip;

                return this.add_zip;
            }
        }

        public string purchasedmail_pref_name
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return this.pref_name;

                return this.add_pref_name;
            }
        }

        public string purchasedmail_address
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return this.address;

                return this.add_address;
            }
        }

        public string purchasedmail_taddress
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return this.taddress;

                return this.add_taddress;
            }
        }

        public string purchasedmail_maddress
        {
            get
            {
                if (this.send == EnumSendTo.MEMBER_ADDRESS)
                    return this.maddress;

                return this.add_maddress;
            }
        }

        #endregion

        public bool reauthed { get; set; }

        public bool IsErrorBack { get; set; }

        public short DisplayedDelvMethod { get; set; }

        public string pay_memo
        {
            get
            {
                if (this.pay == null)
                    return string.Empty;

                return ErsFactory.ersViewServiceFactory.GetErsViewPayService().GetPaymentMessageFromId(this.pay);
            }
        }

        public virtual int? site_id { get { return ErsFactory.ersUtilityFactory.getSetup().site_id; } }
    }

}