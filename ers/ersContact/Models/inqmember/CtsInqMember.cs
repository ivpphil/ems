﻿using System;
using System.Collections.Generic;
using ersContact.Domain.Inqmember.Commands;
using ersContact.Domain.Inqmember.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.viewService;

namespace ersContact.Models
{ 
    public class CtsInqMember
        : ErsContactModelBase
        , IInqMemberCommand, IClientInfoMappable
    {
        //public long recordCount { get; private set; }

        public int? User_ID { get; set; }
        
        //public List<Dictionary<string, object>> ClientList { get; private set; }
        
        #region Other List
        public List<Dictionary<string, object>> prefList { get { return ErsFactory.ersViewServiceFactory.GetErsViewPrefService().SelectAsList(false); } }
        public List<Dictionary<string, object>> jobList { get { return ErsFactory.ersViewServiceFactory.GetErsViewJobService().SelectAsList(); } }
        public List<Dictionary<string, object>> quesList { get { return ErsFactory.ersViewServiceFactory.GetErsViewQuesService().SelectAsList(); } }
        public List<Dictionary<string, object>> sexList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.Sex, EnumCommonNameColumnName.namename); } }
        #endregion

        #region Client Info Fields
        [ErsSchemaValidation("member_t.id")]
        public virtual int? id { get; set; }

        [ErsSchemaValidation("member_t.mcode")]
        public virtual string mcode { get; set; }

        [ErsSchemaValidation("member_t.lname")]
        public virtual string lname { get; set; }

        [ErsSchemaValidation("member_t.fname")]
        public virtual string fname { get; set; }

        [ErsSchemaValidation("member_t.lnamek")]
        public virtual string lnamek { get; set; }

        [ErsSchemaValidation("member_t.fnamek")]
        public virtual string fnamek { get; set; }

        [ErsSchemaValidation("member_t.ccode")]
        public virtual string ccode { get; set; }
         
        [ErsSchemaValidation("member_t.zip")]
        public virtual string zip { get; set; }

        [ErsSchemaValidation("member_t.pref")]
        public virtual int? pref { get; set; }

        [ErsSchemaValidation("member_t.pref")]
        public virtual string w_pref { get; set; }

        [ErsSchemaValidation("member_t.address")]
        public virtual string address { get; set; }

        [ErsSchemaValidation("member_t.taddress")]
        public virtual string taddress { get; set; }

        [ErsSchemaValidation("member_t.maddress")]
        public virtual string maddress { get; set; }

        [ErsSchemaValidation("member_t.email")]
        public virtual string email { get; set; }

        [ErsSchemaValidation("member_t.email3")]
        public virtual string email3 { get; set; }

        [ErsSchemaValidation("member_t.tel")]
        public virtual string tel { get; set; }

        [ErsSchemaValidation("member_t.fax")]
        public virtual string fax { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = ErsViewBirthdayService.minimumOfSelectyear, rangeTo = ErsViewBirthdayService.maximumOfSelectyear)]
        public int? birthday_y { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 12)]
        public int? birthday_m { get; set; }

        [ErsOutputHidden("input_member")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric, rangeFrom = 1, rangeTo = 31)]
        public int? birthday_d { get; set; }

        [ErsSchemaValidation("member_t.birth")]
        public DateTime? birth
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetBirthDay(this.birthday_y, this.birthday_m, this.birthday_d);
            }
        }

        [ErsSchemaValidation("member_t.birth")]
        public virtual EnumDeleted? deleted { get; set; }

        [ErsSchemaValidation("member_t.memo")]
        public virtual string memo { get; set; }

        public List<Dictionary<string, object>> birthday_yList
        {
            get
            {
                var min_user_age = ErsFactory.ersUtilityFactory.getSetup().min_user_age;
                var max_user_age = ErsFactory.ersUtilityFactory.getSetup().max_user_age;
                return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListYear(max_user_age, min_user_age);
            }
        }

        public List<Dictionary<string, object>> birthday_mList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListMonth(); }
        }

        public List<Dictionary<string, object>> birthday_dList
        {
            get { return ErsFactory.ersViewServiceFactory.GetErsViewBirthdayService().GetListDay(); }
        }

        public int max_user_age
        {
            get { return ErsFactory.ersUtilityFactory.getSetup().max_user_age; }
        }

        [ErsSchemaValidation("member_t.sex")]
        public virtual EnumSex? sex { get; set; }

        [ErsSchemaValidation("member_t.sex")]
        public virtual string w_sex { get; set; }

        [ErsSchemaValidation("member_t.job")]
        public virtual int? job { get; set; }

        [ErsSchemaValidation("member_t.job")]
        public virtual string w_job { get; set; }

        [ErsSchemaValidation("member_t.ques")]
        public virtual int? ques { get; set; }

        [ErsSchemaValidation("member_t.ques")]
        public virtual string w_ques { get; set; }

        [ErsSchemaValidation("member_t.ans")]
        public virtual string ans { get; set; }

        [ErsSchemaValidation("member_t.pm_flg")]
        public virtual EnumPmFlg? pm_flg { get; set; }

        [ErsSchemaValidation("member_t.blacklist")]
        public virtual short? blacklist { get; set; }

        [ErsSchemaValidation("member_t.dm_flg")]
        public virtual EnumDmFlg dm_flg { get; set; }

        [ErsSchemaValidation("member_t.out_bound_flg")]
        public virtual EnumOutBoundFlg out_bound_flg { get; set; }

        public string blacklistdesc { get; set; }

        public string dm_flg_desc { get; set; }

        public string out_bound_flg_desc { get; set; }

        [HtmlSubmitButton]
        public bool edit { get; set; }

        [HtmlSubmitButton]
        public bool merge { get; set; }

        [HtmlSubmitButton]
        public bool renew { get; set; }

        [HtmlSubmitButton]
        public bool cancel { get; set; }
        #endregion
    }
}
