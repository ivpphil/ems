﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ersContact.Domain.Inquiry.Commands;
using ersContact.Domain.Inquiry.Mappables;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace ersContact.Models
{
    public class CtsInquiry
        : ErsContactModelBase
        , ICategoryListMappable, ICategoryLabelMappable, ISupportCountMappable, IEnquiryMailCommand
        , IIncMailListMappable, ISearchCaseListMappable, IMailApproveMappable, IInquiryListMappable
        , IHistoryListMappable, IEscalateListMappable, IFromAddressMappable, IInquiryCommand
        , ILockByOtherMappable, ICaseInfoMappable
    {
        public CtsInquiry()
        {
            this.enq_corresponding = EnumEnqCorresponding.Phone;
        }

        public bool displayInputFrame
        {
            get
            {
                return this.showmailapp
                    || (this.showincmail && mcode.HasValue())
                    || this.phonereg
                    || this.mailtray
                    || this.mailsent
                    || this.memoreg
                    || this.doEscalate
                    || this.mailesctray
                    || this.mailescsend
                    || this.mailescreset;
            }
        }

        public ErsPagerModel pager { get; internal set; }

        public long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsInquiryCaseListItemNumberOnPage; } }

        public long recordCount { get; set; }

        public List<Dictionary<string, object>> inquiryList { get; set; }

        public List<Dictionary<string, object>> historyList { get; set; }

        public List<Dictionary<string, object>> ctsAgetypeList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentTypeService().SelectAsList(); } }

        public List<Dictionary<string, object>> ctsAgentList { get { return ErsFactory.ersViewServiceFactory.GetErsViewCtsAgentService().SelectAsList(null); } }

        public List<Dictionary<string, object>> emailappList { get; set; }

        public List<Dictionary<string, object>> escalatetoList { get; set; }

        public List<Dictionary<string, object>> categoryList { get; set; }

        public long supportCount { get; set; }

        [ErsOutputHidden("ctsCase")]
        public int? ctsUserID { get; set; }

        //ページ送り　現在ページ
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        public long pagerPageCount { get; set; }

        [HtmlDictionary("esc_incmail")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public Dictionary<string, int> esc_incmail_list { get; set; }

        [HtmlDictionary("del_incmail")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public Dictionary<string, int> del_incmail_list { get; set; }

        [BindTarget("update_memo")]
        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual string esc_id { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public virtual int? esc_mail_id { get; set; }

        [ErsUniversalValidation]
        public string tabsel1 { get; set; }

        [ErsUniversalValidation]
        public string tabsel2 { get; set; }

        [ErsUniversalValidation]
        public string tabsel3 { get; set; }

        [ErsUniversalValidation]
        public string catlabel1 { get; set; }

        [ErsUniversalValidation]
        public string catlabel2 { get; set; }

        [ErsUniversalValidation]
        public string catlabel3 { get; set; }

        [ErsUniversalValidation]
        public string catlabel4 { get; set; }

        [ErsUniversalValidation]
        public string catlabel5 { get; set; }

        [BindTarget("update_memo")]
        public int? site_id { get; set; }

        #region Common Type List
        public List<Dictionary<string, object>> pryList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQPRY, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> typeList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQTYP, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> stsList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQSTS, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> pgrList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQPGR, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> sitList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQSIT, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> tcrList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQTCR, EnumCommonNameColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> ecrList
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCommonNameCodeService().GetList(EnumCommonNameType.ENQECR, EnumCommonNameColumnName.namename);
            }
        }

        public List<Dictionary<string, object>> frmList { get; set; }

        public List<Dictionary<string, object>> CateList1
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT1, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> CateList2
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT2, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> CateList3
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT3, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> CateList4
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT4, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        public List<Dictionary<string, object>> CateList5
        {
            get
            {
                return ErsFactory.ersViewServiceFactory.GetErsViewCtsEnquiryCategoryService().GetList(EnumCtsEnquiryCategoryType.ENQCT5, EnumCtsEnquiryCategoryColumnName.namename);
            }
        }
        #endregion

        #region Client Info Fields
        [ErsSchemaValidation("member_t.lname")]
        public virtual string lname { get; set; }

        [ErsSchemaValidation("member_t.fname")]
        public virtual string fname { get; set; }
        #endregion

        #region Register Fields

        [BindTarget("update_memo")]
        [ErsOutputHidden("inquiry_list")]
        [ErsSchemaValidation("cts_enquiry_t.case_no")]
        public virtual int? case_no { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.case_no")]
        public virtual int? caseno_srch { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.mcode")]
        [ErsOutputHidden("inquiry_list", "history_list")]
        public virtual string mcode { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_t.intime")]
        public virtual DateTime? intime { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_t.utime")]
        public virtual DateTime? utime { get; set; }

        /// <summary>
        /// 問い合わせ種別
        /// </summary>
        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_t.enq_type")]
        public virtual EnumEnqType? enq_type { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_t.enq_status")]
        public virtual EnumEnqStatus? enq_status { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_status")]
        public virtual EnumEnqStatus? enq_status_srch { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_status")]
        public virtual string enq_status_name { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_t.enq_progress")]
        public virtual EnumEnqProgress? enq_progress { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_progress")]
        public virtual EnumEnqProgress? enq_progress_srch { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_progress")]
        public virtual string enq_progress_name { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_t.enq_situation")]
        public virtual EnumEnqSituation? enq_situation { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_situation")]
        public virtual string enq_situation_name { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_t.enq_priorty")]
        public virtual EnumEnqPriority? enq_priorty { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_priorty")]
        public virtual string enq_priorty_name { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_t.enq_casename")]
        public virtual string enq_casename { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_t.cate1")]
        public virtual int cate1 { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate1")]
        public virtual int cate1_srch { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate1")]
        public virtual string cate1_name { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_t.cate2")]
        public virtual int cate2 { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate2")]
        public virtual int cate2_srch { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate2")]
        public virtual string cate2_name { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_t.cate3")]
        public virtual int cate3 { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate3")]
        public virtual int cate3_srch { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate3")]
        public virtual string cate3_name { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_t.cate4")]
        public virtual int cate4 { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate4")]
        public virtual int cate4_srch { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate4")]
        public virtual string cate4_name { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate5")]
        public virtual int cate5 { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate5")]
        public virtual int cate5_srch { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.cate5")]
        public virtual string cate5_name { get; set; }

        [HtmlSubmitButton]
        public virtual bool skin_problem { get; set; }

        [HtmlSubmitButton]
        public virtual bool quality_complaint { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.user_id")]
        public virtual string user_id { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_detail_t.sub_no")]
        public virtual int? sub_no { get; set; }

        /// <summary>
        /// 操作種別 0:電話対応登録 1:E-メールトレイ　2:メモ登録
        /// </summary>
        [ErsSchemaValidation("cts_enquiry_detail_t.enq_corresponding")]
        public virtual EnumEnqCorresponding? enq_corresponding { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.corresponding")]
        public virtual EnumCorresponding? corresponding { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.starttime")]
        public virtual DateTime? starttime { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.finishtime")]
        public virtual DateTime? finishtime { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_title")]
        public virtual string email_title { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_title")]
        public virtual string email_title_esc { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_from_name")]
        public virtual string email_from_name { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_from")]
        public virtual string email_from { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_from")]
        public virtual string email_from_esc { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_to")]
        public virtual string[] email_to { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_cc")]
        public virtual string[] email_cc { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_bcc")]
        public virtual string[] email_bcc { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.enq_detail")]
        public virtual string enq_detail { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.enq_detail")]
        public virtual string keywords_srch { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.ans_detail")]
        public virtual string ans_detail { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_header")]
        public virtual string email_header { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_header")]
        public virtual string email_header_esc { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_body")]
        public virtual string email_body { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_body")]
        public virtual string email_body_esc { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_fotter")]
        public virtual string email_fotter { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_fotter")]
        public virtual string email_fotter_esc { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.enq_status")]
        public virtual string email_toSV_esc { get; set; }

        [BindTarget("update_memo")]
        [ErsSchemaValidation("cts_enquiry_detail_t.memo")]
        public virtual string memo { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.intime")]
        public virtual DateTime? intime_dtl { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.utime")]
        public virtual DateTime? utime_dtl { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.save_mode")]
        public virtual EnumEnqSaveMode? save_mode { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.active")]
        public virtual EnumActive? active { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_status")]
        public virtual EnumEnqEmailStatus? email_status { get; set; }

        [ErsSchemaValidation("cts_enquiry_detail_t.email_type")]
        public virtual EnumEnqEmailType? email_type { get; set; }

        [ErsSchemaValidation("cts_login_t.id")]
        public int agent_id { get; set; }

        [ErsSchemaValidation("cts_login_t.ag_name")]
        [ErsOutputHidden("ctsCase")]
        public virtual string ag_name { get; set; }

        [ErsSchemaValidation("cts_login_t.ag_name")]
        public virtual string user_ag_name { get; set; }

        [ErsSchemaValidation("cts_enquiry_t.lockid")]
        [ErsOutputHidden("ctsCase")]
        public virtual int? lockid { get; set; }
        #endregion

        #region FAQ Template
        [ErsSchemaValidation("cts_faq_template_t.faq_text")]
        public virtual string faq_text { get; set; }

        [ErsSchemaValidation("cts_faq_template_t.faq_text2")]
        public virtual string faq_text2 { get; set; }
        #endregion

        #region Incoming Mail

        [ErsSchemaValidation("cts_incoming_mail_t.id")]
        public virtual int mail_id { get; set; }

        #endregion

        #region Boolean Variables
        [HtmlSubmitButton]
        public bool newcase { get; set; }

        [HtmlSubmitButton]
        public bool caselist { get; set; }

        [HtmlSubmitButton]
        public bool histlist { get; set; }

        [HtmlSubmitButton]
        public bool needsup { get; set; }

        [HtmlSubmitButton]
        public bool incmail { get; set; }

        [HtmlSubmitButton]
        public bool showincmail { get; set; }

        [HtmlSubmitButton]
        public bool mailapp { get; set; }

        [HtmlSubmitButton]
        public bool mailesc { get; set; }

        [HtmlSubmitButton]
        public bool searchcase { get; set; }

        [HtmlSubmitButton]
        public bool search { get; set; }

        [HtmlSubmitButton]
        public bool escdel { get; set; }

        [HtmlSubmitButton]
        public bool temppres { get; set; }

        [HtmlSubmitButton]
        public bool phonereg { get; set; }

        [HtmlSubmitButton]
        public bool mailtray { get; set; }

        [HtmlSubmitButton]
        public bool mailreg { get; set; }

        [HtmlSubmitButton]
        public bool mailsent { get; set; }

        [HtmlSubmitButton]
        public bool memoreg { get; set; }

        [HtmlSubmitButton]
        public bool reset { get; set; }

        [HtmlSubmitButton]
        public bool execdone { get; set; }

        [HtmlSubmitButton]
        public bool setstarttime { get; set; }

        [HtmlSubmitButton]
        public bool setfinishtime { get; set; }

        [HtmlSubmitButton]
        public bool showhistdtl { get; set; }

        [HtmlSubmitButton]
        public bool mailesctray { get; set; }

        [HtmlSubmitButton]
        public bool mailescsend { get; set; }

        [HtmlSubmitButton]
        public bool mailescreset { get; set; }

        [HtmlSubmitButton]
        public bool mailtemplate { get; set; }

        [HtmlSubmitButton]
        public bool doEscalate { get; set; }

        [HtmlSubmitButton]
        public bool doRegist { get; set; }

        [HtmlSubmitButton]
        public bool showValidate { get; set; }

        /// <summary>
        /// メール承認フラグ
        /// </summary>
        [HtmlSubmitButton]
        public bool showmailapp { get; set; }

        [HtmlSubmitButton]
        public bool insertSendMail { get; set; }

        [HtmlSubmitButton]
        public bool showCat1 { get; set; }

        [HtmlSubmitButton]
        public bool showCat2 { get; set; }

        [HtmlSubmitButton]
        public bool showCat3 { get; set; }

        [HtmlSubmitButton]
        public bool showCat4 { get; set; }

        [HtmlSubmitButton]
        public bool showCat5 { get; set; }

        [HtmlSubmitButton]
        public bool showCatLabel { get; set; }

        [HtmlSubmitButton]
        [ErsOutputHidden("ctsCase")]
        public bool btnLock { get; set; }

        [HtmlSubmitButton]
        public bool showLock { get; set; }

        [HtmlSubmitButton]
        [ErsOutputHidden("ctsCase")]
        public bool btnRelease { get; set; }

        [HtmlSubmitButton]
        public bool showRelease { get; set; }

        [HtmlSubmitButton]
        public bool showRegBtn { get; set; }

        [HtmlSubmitButton]
        public bool edit_dtl { get; set; }

        [HtmlSubmitButton]
        public bool edit_dtl_tabs { get; set; }

        [HtmlSubmitButton]
        public bool submit_update { get; set; }

        [HtmlSubmitButton]
        public bool mail_update { get; set; }

        /// <summary>他者によるロック判定</summary>
        public bool LockedByOther { get; set; }

        /// <summary>登録アクション</summary>
        public bool IsRegistAction
        {
            get
            {
                return (this.temppres || this.phonereg || this.mailtray || this.mailreg ||
                    this.memoreg || this.mailesctray || this.mailescsend || this.mailsent || this.submit_update);
            }
        }

        /// <summary>他者ロック時の登録アクション</summary>
        public bool IsRegistActionWithLockedByOther { get { return ((this.IsRegistAction || this.btnLock || this.btnRelease) && this.LockedByOther); } }

        #endregion
    }
}
