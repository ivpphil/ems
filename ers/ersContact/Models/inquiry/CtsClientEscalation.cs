﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.pager;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.db.table;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.clientescalation;
using ersContact.jp.co.ivp.ers.mvc;
using jp.co.ivp.ers;
using ersContact.Domain.Configuration.Mappables;
using ersContact.Domain.Configuration.Commands;

namespace ersContact.Models
{
    public class CtsClientEscalation
        : ErsContactModelBase, ICtsEscalationListMappable, ICtsEscalationDetailMappable
        , ICtsEscalationUpdateCommand
    {
        public virtual long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().CtsInquiryCaseListItemNumberOnPage; } }

        public ErsPagerModel pager { get; set; }

        public long recordCount { get; set; }

        public long pagerPageCount { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.Numeric)]
        public int pageCnt { get; set; }

        [ErsSchemaValidation("cts_client_escalation_t.id")]
        public virtual int? id { get; set; }

        [ErsSchemaValidation("cts_client_escalation_t.esc_name")]
        public virtual string esc_name { get; set; }

        [ErsSchemaValidation("cts_client_escalation_t.email_from_name")]
        public virtual string email_from_name { get; set; }

        [ErsSchemaValidation("cts_client_escalation_t.email_from")]
        public virtual string email_from { get; set; }

        [ErsSchemaValidation("cts_client_escalation_t.email_to")]
        public virtual string[] email_to { get; set; }

        [ErsSchemaValidation("cts_client_escalation_t.email_cc")]
        public virtual string[] email_cc { get; set; }

        [ErsSchemaValidation("cts_client_escalation_t.email_bcc")]
        public virtual string[] email_bcc { get; set; }

        [ErsSchemaValidation("cts_client_escalation_t.email_header")]
        public virtual string email_header { get; set; }

        [ErsSchemaValidation("cts_client_escalation_t.email_body")]
        public virtual string email_body { get; set; }

        [ErsSchemaValidation("cts_client_escalation_t.email_fotter")]
        public virtual string email_fotter { get; set; }

        [ErsSchemaValidation("cts_client_escalation_t.active")]
        public virtual EnumActive? active { get; set; }

        [HtmlSubmitButton]
        public virtual bool active_string { get; set; }

        [HtmlSubmitButton]
        public bool regist { get; set; }

        [HtmlSubmitButton]
        public bool modify { get; set; }

        [HtmlSubmitButton]
        public bool delete { get; set; }

        [HtmlSubmitButton]
        public bool save { get; set; }

        [HtmlSubmitButton]
        public bool reset { get; set; }

        [HtmlSubmitButton]
        public bool back { get; set; }

        public List<Dictionary<string, object>> fromAddressList { get; set; }
    }
}
