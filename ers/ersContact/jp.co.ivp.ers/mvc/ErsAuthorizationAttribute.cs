﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.state;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class ErsAuthorizationAttribute
        : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var controler = (ErsControllerSecureContact)filterContext.Controller;

            EnumUserState state = ((ErsSessionStateContact)ErsContext.sessionState).getUserState();

            controler.cts_User_ID = Convert.ToInt32(ErsContext.sessionState.Get("cts_user_id"));
            controler.cts_Ransu = ErsContext.sessionState.Get("contact_ransu");

            ErsContext.sessionState.Add("user_name", ((ErsSessionStateContact)ErsContext.sessionState).GetUserName(controler.cts_User_ID));

            object[] obj = filterContext.ActionDescriptor.GetCustomAttributes(typeof(NoNeedSessionAttribute), false);
            if (obj.Length == 0)
            {
                if ((state != EnumUserState.LOGIN))
                {
                    //ログイン状態をチェックし、ログイン状態でなかったらエラー
                    filterContext.Result = controler.ExecuteError();
                }
            }
        }
    }
}