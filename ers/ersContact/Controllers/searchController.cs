﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using ersContact.Domain.Search.Commands;
using ersContact.Domain.Search.Mappables;
using ersContact.Models;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class SearchController
        : ErsControllerSecureContact
    {
        public ActionResult index(CtsSearch ctsSearch)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<ISearchCommand>(ctsSearch), ctsSearch);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsSearch.FirstLoad = true;

            this.mapperBus.Map<ICampaignListMappable>(ctsSearch);

            this.ReadSearchCookies(ctsSearch);

            return View("ctsSearch", ctsSearch);
        }
        
        public ActionResult search(CtsSearch ctsSearch)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<ISearchCommand>(ctsSearch), ctsSearch);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsSearch.FirstLoad = false;

            this.mapperBus.Map<ICampaignListMappable>(ctsSearch);
            ctsSearch.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsSearch.pageCnt, ctsSearch.maxItemCount);

            // 検索対象判定処理
            string target = ctsSearch.TargetSearch;

            if (target == null)
            {
                if (ctsSearch.TargetClient == false && ctsSearch.TargetOrder == false && !string.IsNullOrEmpty(ctsSearch.TargetSearch))
                {
                    target = ctsSearch.TargetSearch;
                }
                else
                {
                    // 顧客検索の場合
                    if (ctsSearch.TargetClient)
                    {
                        target = EnumTargetSerach.ClientSearch.ToString();
                    }
                    // 受注検索の場合
                    else
                    {
                        target = EnumTargetSerach.OrderSearch.ToString();
                    }
                }
            }

            ctsSearch.pageCnt = ctsSearch.pager.pageCnt;
            if (target == EnumTargetSerach.ClientSearch.ToString())
            {
                ctsSearch.TargetClient = true;
                ctsSearch.TargetOrder = false;
            }
            else
            {
                ctsSearch.TargetClient = false;
                ctsSearch.TargetOrder = true;
            }
            ctsSearch.TargetSearch = target;

            this.mapperBus.Map<ISearchListMappable>(ctsSearch);
            ctsSearch.pager.LoadPageList(ctsSearch.recordCount);
            
            this.WriteSearchCookies(ctsSearch);

            return View("ctsSearch", ctsSearch);
        }

        public ActionResult LoadClientInfo(CtsClientInfo ctsClientInfo)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<IClientInfoListMappable>(ctsClientInfo);

            return View("ctsClientInfo", ctsClientInfo);
        }

        public ActionResult LoadOrderDetail(CtsSearchOrder ctsSearchOrder, CtsSearch ctsSearch)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<ISearchCommand>(ctsSearch), ctsSearch);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsSearch.FirstLoad = false;
            this.mapperBus.Map<ICampaignListMappable>(ctsSearch);

            if (ctsSearchOrder.mcode != null)
            {
                ctsSearchOrder.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsSearchOrder.pageCnt, ctsSearchOrder.maxItemCount);
                ctsSearchOrder.pageCnt = ctsSearchOrder.pager.pageCnt;

                this.mapperBus.Map<IOrderListMappable>(ctsSearchOrder);

                ctsSearchOrder.pager.LoadPageList(ctsSearchOrder.recordCount);

                return View("CtsSearchOrder", ctsSearchOrder);
            }
            else
            {
                ctsSearch.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsSearch.pageCnt, ctsSearch.maxItemCount);
                ctsSearch.pageCnt = ctsSearch.pager.pageCnt;
                ctsSearch.TargetClient = false;
                ctsSearch.TargetOrder = true;
                ctsSearch.TargetSearch = EnumTargetSerach.OrderSearch.ToString();

                // SearchList取得Mapper
                this.mapperBus.Map<ISearchListMappable>(ctsSearch);

                ctsSearch.pager.LoadPageList(ctsSearch.recordCount);

                this.WriteSearchCookies(ctsSearch);

                return View("ctsSearch", ctsSearch);
            }
        }

        public ActionResult LoadCTSOrderDetail(CtsSearchCtsOrder ctsSearchCtsOrder, CtsSearch ctsSearch)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<ISearchCommand>(ctsSearch), ctsSearch);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            //if (ctsSearchCtsOrder.deleteOrder != null)
            //{
            //    commandBus.Submit((ICtsOrderDeleteCommand)ctsSearchCtsOrder, EnumCommandTransaction.BeginTransaction);
            //}

            ctsSearch.FirstLoad = false;
            this.mapperBus.Map<ICampaignListMappable>(ctsSearch);

            if (ctsSearchCtsOrder.mcode != null)
            {
                ctsSearchCtsOrder.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsSearchCtsOrder.pageCnt, ctsSearchCtsOrder.maxItemCount);
                ctsSearchCtsOrder.pageCnt = ctsSearchCtsOrder.pager.pageCnt;
                var agent = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(Convert.ToInt32(this.cts_User_ID));
                if (agent == null)
                {
                    throw new ErsException("29002");
                }
                ctsSearchCtsOrder.user_id = agent.user_id;

                if (ctsSearchCtsOrder.d_no != null || ctsSearchCtsOrder.temp_d_no != null)
                {
                    commandBus.Submit((ICtsOrderDeleteCommand)ctsSearchCtsOrder, EnumCommandTransaction.BeginTransaction);
                }

                this.mapperBus.Map<ICtsOrderListMappable>(ctsSearchCtsOrder);

                ctsSearchCtsOrder.pager.LoadPageList(ctsSearchCtsOrder.recordCount);

                return View("CtsSearchCtsOrder", ctsSearchCtsOrder);
            }
            else
            {
                ctsSearch.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsSearch.pageCnt, ctsSearch.maxItemCount);
                ctsSearch.pageCnt = ctsSearch.pager.pageCnt;
                ctsSearch.TargetClient = false;
                ctsSearch.TargetOrder = true;
                ctsSearch.TargetSearch = EnumTargetSerach.OrderSearch.ToString();

                // SearchList取得Mapper
                this.mapperBus.Map<ISearchListMappable>(ctsSearch);

                ctsSearch.pager.LoadPageList(ctsSearch.recordCount);

                this.WriteSearchCookies(ctsSearch);

                return View("ctsSearch", ctsSearch);
            }
        }

        public ActionResult LoadRegOrderDetail(CtsSearchRegOrder ctsSearchRegOrder)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsSearchRegOrder.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsSearchRegOrder.pageCnt, ctsSearchRegOrder.maxItemCount);

            this.mapperBus.Map<IRegularOrderListMappable>(ctsSearchRegOrder);

            ctsSearchRegOrder.pager.LoadPageList(ctsSearchRegOrder.recordCount);

            return View("ctsSearchRegular", ctsSearchRegOrder); 
        }

        public ActionResult LoadInqList(CtsSearch ctsSearch)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<ISearchCommand>(ctsSearch), ctsSearch);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsSearch.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsSearch.pageCnt, ctsSearch.maxItemCount);

            this.mapperBus.Map<IInquiryListMappable>(ctsSearch);

            ctsSearch.pager.LoadPageList(ctsSearch.recordCount);

            return View("ctsInqList", ctsSearch);
        }

        private void ReadSearchCookies(CtsSearch ctsSearch)
        {
            if (Request.Cookies["SearchFilter"] != null)
            {
                ctsSearch.TargetSearch = Server.UrlDecode(Request.Cookies["SearchFilter"]["TargetSearch"]);
                ctsSearch.mcode = Server.UrlDecode(Request.Cookies["SearchFilter"]["mcode"]);
                ctsSearch.lname = Server.UrlDecode(Request.Cookies["SearchFilter"]["lname"]);
                ctsSearch.fname = Server.UrlDecode(Request.Cookies["SearchFilter"]["fname"]);
                ctsSearch.lnamek = Server.UrlDecode(Request.Cookies["SearchFilter"]["lnamek"]);
                ctsSearch.fnamek = Server.UrlDecode(Request.Cookies["SearchFilter"]["fnamek"]);

                ctsSearch.zip1 = Server.UrlDecode(Request.Cookies["SearchFilter"]["zip1"]);
                ctsSearch.zip2 = Server.UrlDecode(Request.Cookies["SearchFilter"]["zip2"]);
                ctsSearch.address = Server.UrlDecode(Request.Cookies["SearchFilter"]["address"]);
                if (Server.UrlDecode(Request.Cookies["SearchFilter"]["taddress"]) != null && Server.UrlDecode(Request.Cookies["SearchFilter"]["taddress"]) != String.Empty)
                    ctsSearch.address = Server.UrlDecode(Request.Cookies["SearchFilter"]["address"]) + " " + Server.UrlDecode(Request.Cookies["SearchFilter"]["taddress"]);

                ctsSearch.email = Server.UrlDecode(Request.Cookies["SearchFilter"]["email"]);
                ctsSearch.tel = Server.UrlDecode(Request.Cookies["SearchFilter"]["tel"]);
                ctsSearch.fax = Server.UrlDecode(Request.Cookies["SearchFilter"]["fax"]);
                ctsSearch.ccode = Server.UrlDecode(Request.Cookies["SearchFilter"]["ccode"]);
                if (!string.IsNullOrEmpty(Request.Cookies["SearchFilter"]["campaign_id"])) ctsSearch.campaign_id = Convert.ToInt32(Server.UrlDecode(Request.Cookies["SearchFilter"]["campaign_id"]));

                if (!string.IsNullOrEmpty(Request.Cookies["SearchFilter"]["ordertype"])) ctsSearch.ordertype = Convert.ToBoolean(Server.UrlDecode(Request.Cookies["SearchFilter"]["ordertype"]));
                if (!string.IsNullOrEmpty(Request.Cookies["SearchFilter"]["ctsordertype"])) ctsSearch.ctsordertype = Convert.ToBoolean(Server.UrlDecode(Request.Cookies["SearchFilter"]["ctsordertype"]));
                if (!string.IsNullOrEmpty(Request.Cookies["SearchFilter"]["regordertype"])) ctsSearch.regordertype = Convert.ToBoolean(Server.UrlDecode(Request.Cookies["SearchFilter"]["regordertype"]));
                if (!string.IsNullOrEmpty(Request.Cookies["SearchFilter"]["src_deleted"])) ctsSearch.src_deleted = (EnumDeleted)Convert.ToInt32(Server.UrlDecode(Request.Cookies["SearchFilter"]["src_deleted"]));
                ctsSearch.d_no = Server.UrlDecode(Request.Cookies["SearchFilter"]["d_no"]);
                ctsSearch.mall_d_no = Server.UrlDecode(Request.Cookies["SearchFilter"]["mall_d_no"]);
            }
        }

        private void WriteSearchCookies(CtsSearch ctsSearch)
        {
            HttpCookie srchCookie = new HttpCookie("SearchFilter");
            srchCookie.Values["TargetSearch"] = Server.UrlEncode(ctsSearch.TargetSearch);
            srchCookie.Values["mcode"] = Server.UrlEncode(ctsSearch.mcode);
            srchCookie.Values["lname"] = Server.UrlEncode(ctsSearch.lname);
            srchCookie.Values["fname"] = Server.UrlEncode(ctsSearch.fname);
            srchCookie.Values["lnamek"] = Server.UrlEncode(ctsSearch.lnamek);
            srchCookie.Values["fnamek"] = Server.UrlEncode(ctsSearch.fnamek);
            srchCookie.Values["zip1"] = Server.UrlEncode(ctsSearch.zip1);
            srchCookie.Values["zip2"] = Server.UrlEncode(ctsSearch.zip2);
            srchCookie.Values["pref"] = Server.UrlEncode(ctsSearch.pref.ToString());

            string alphabet = String.Empty;
            string digit = String.Empty;

            if (ctsSearch.address != null && ctsSearch.address != String.Empty)
            {
                Match regexMatch = Regex.Match(ctsSearch.address, "\\d");
                if (regexMatch.Success) //Found numeric part in the coverage string
                {
                    int digitStartIndex = regexMatch.Index; //Get the index where the numeric digit found
                    if (digitStartIndex > 2)
                        alphabet = ctsSearch.address.Substring(0, digitStartIndex - 2);
                    else alphabet = "";
                    digit = ctsSearch.address.Substring(digitStartIndex);
                    srchCookie.Values["address"] = Server.UrlEncode(alphabet);
                    srchCookie.Values["taddress"] = Server.UrlEncode(digit);
                }
                else
                    srchCookie.Values["address"] = Server.UrlEncode(ctsSearch.address);
            }

            srchCookie.Values["email"] = Server.UrlEncode(ctsSearch.email);
            srchCookie.Values["tel"] = Server.UrlEncode(ctsSearch.tel);
            srchCookie.Values["fax"] = Server.UrlEncode(ctsSearch.fax);
            srchCookie.Values["ccode"] = Server.UrlEncode(ctsSearch.ccode);
            srchCookie.Values["campaign_id"] = Server.UrlEncode(ctsSearch.campaign_id.ToString());
            srchCookie.Values["ordertype"] = Server.UrlEncode(ctsSearch.ordertype.ToString());
            srchCookie.Values["ctsordertype"] = Server.UrlEncode(ctsSearch.ctsordertype.ToString());
            srchCookie.Values["regordertype"] = Server.UrlEncode(ctsSearch.regordertype.ToString());
            srchCookie.Values["src_deleted"] = Server.UrlEncode(Convert.ToString((int?)ctsSearch.src_deleted));
            srchCookie.Values["d_no"] = Server.UrlEncode(ctsSearch.d_no);
            srchCookie.Values["mall_d_no"] = Server.UrlEncode(ctsSearch.mall_d_no);

            Response.Cookies.Add(srchCookie);

            HttpCookie fromSearchCookie = new HttpCookie("FromSearch");
            fromSearchCookie.Values["FromSearch"] = "yes";
            Response.Cookies.Add(fromSearchCookie);
        }
    }
}
