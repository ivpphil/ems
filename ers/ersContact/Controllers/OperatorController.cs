﻿using System.Web.Mvc;
using ersContact.Domain.Operator.Commands;
using ersContact.Domain.Operator.Mappables;
using ersContact.Models;
using jp.co.ivp.ers.cts_operators;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class OperatorController
        : ErsControllerSecureContact
    {
        /// <summary>
        /// トップ
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult index(CtsOperator operators)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            return View("index", operators);
        }

        public ActionResult manage_operators(CtsOperator operators)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            operators.authority = CtsAuthorityType.OPERATOR;
            this.ModelState.AddModelErrors(commandBus.Validate<IOperatorCommand>(operators), operators);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return manage(operators);
        }

        public ActionResult manage_supervisors(CtsOperator operators)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            operators.authority = CtsAuthorityType.SUPERVISOR;
            this.ModelState.AddModelErrors(commandBus.Validate<IOperatorCommand>(operators), operators);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return manage(operators);
        }

        private ActionResult manage(CtsOperator operators)
        {
            commandBus.Submit((IOperatorCommand)operators, EnumCommandTransaction.BeginTransaction);

            if (operators.done)
            {
                operators.regist = false;
                operators.modify = false;
                operators.delete = false;
                operators.cancel = true;
            }

            if (operators.cancel == true)
            {
                operators.id = null;
            }

            this.mapperBus.Map<IOperatorMappable>(operators);

            return View("operators", operators);
        }
    }
}
