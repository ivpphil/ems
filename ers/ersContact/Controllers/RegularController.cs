﻿using System;
using System.Web.Mvc;
using ersContact.Domain.Regular.Commands;
using ersContact.Domain.Regular.Mappables;
using ersContact.Models;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class RegularController
        : ErsControllerSecureContact
    {
        /// <summary>
        /// トップ
        /// </summary>
        /// <returns></returns>
        ///
        public ActionResult Index(CtsRegular order)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderCommand>(order), order);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("index", order);
        }

        public ActionResult Header(CtsOrder order)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("header", order);
        }

        public ActionResult Filler()
        {
            return View("filler");
        }

        public ActionResult Footer()
        {
            return View("footer");
        }

        public ActionResult Regular(CtsRegular order, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderCommand>(order), order);
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    if(order.card_cancel)
                    {
                        GetErrorView();
                    }
                    else
                    {
                        return GetErrorView();
                    }
                }

            }

            order.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", order.pageCnt, order.maxItemCount);
            order.page1 = true;

            if (order.all_status == null)
            { 
                order.all_status = 0;
            }

            if (!string.IsNullOrEmpty(order.mcode))
            {
                this.mapperBus.Map<IOrderMappable>(order);
            }

            if (order.member_register)
            {
                commandBus.Submit((IMemberCommand)order, EnumCommandTransaction.BeginTransaction);
            }

            this.mapperBus.Map<IMemberMappable>(order);

            if (order.mcode != ErsMember.DEFAUTL_MCODE)
            {
                commandBus.Submit((IShippingCommand)order, EnumCommandTransaction.BeginTransaction);
                this.mapperBus.Map<IShippingMappable>(order);
            }

            if (order.r_skip_list.Count > 0 || order.r_edit_list.Count > 0)
            {
                if (order.r_skip_list.Count > 0)
                {
                    // Skip Order Detail
                    foreach (var item in order.r_skip_list)
                    {
                        order.d_id = Int32.Parse(item.Key);
                        commandBus.Submit((ISkipOrderCommand)order, EnumCommandTransaction.BeginTransaction);
                    }

                    order.EditMode(EnumStatus.None);
                    order.d_id = 0;

                    this.mapperBus.Map<IOrderMappable>(order);
                }
                if (order.r_edit_list.Count > 0)
                {
                    // Load Order Detail
                    foreach (var item in order.r_edit_list)
                    {
                        order.d_id = Int32.Parse(item.Key);
                        //order.EditMode(order.r_status_list[item.Key]);
                        order.EditMode(EnumStatus.Running);

                        this.mapperBus.Map<IOrderDetailMappable>(order);
                        this.mapperBus.Map<IProductMappable>(order);

                        commandBus.Submit((IPaymentCommand)order, EnumCommandTransaction.WithoutBeginTransaction);
                        this.mapperBus.Map<IPaymentMappable>(order);
                    }
                }
            }
            else if (order.d_id > 0 && order.is_edit_payment_method)
            {
                // Load Order Detail
                // This is from Payment Method functions
                order.EditMode(order.status);

                this.mapperBus.Map<IOrderDetailMappable>(order);
                commandBus.Submit((IPaymentCommand)order, EnumCommandTransaction.WithoutBeginTransaction);
                this.mapperBus.Map<IPaymentMappable>(order);
            }
            else if (order.to_modify)
            {
                commandBus.Submit((IOrderCommand)order, EnumCommandTransaction.BeginTransaction);
                this.mapperBus.Map<IShippingMappable>(order);
                // Back to List
                order.EditMode(EnumStatus.None);
                order.d_id = 0;

                this.mapperBus.Map<IOrderMappable>(order);
            }
            else
            {
                // Back to List
                order.EditMode(EnumStatus.None);
                order.d_id = 0;

                this.mapperBus.Map<IOrderMappable>(order);
            }

            order.pager.LoadPageList(order.recordCount);
            AddModelToView(order);
            return View("order", order);
        }

        public ActionResult Calculate_Date(CtsRegularCalc detail)
        {
            if (!ModelState.IsValid)
            {
                return this.GetErrorView();
            }

            this.mapperBus.Map<IRegularCalcMappable>(detail);

            return Content(detail.resultDate);
        }
    }
}
