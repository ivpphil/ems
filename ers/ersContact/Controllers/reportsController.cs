﻿using System;
using System.Web.Mvc;
using ersContact.Domain.Reports.Commands;
using ersContact.Domain.Reports.Mappables;
using ersContact.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using Models.reports;
using jp.co.ivp.ers;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class ReportsController
        : ErsControllerSecureContact
    {
        public ActionResult index(CtsReports ctsReports)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("index", ctsReports);
        }

        public ActionResult product(Product ctsRepProd)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepProd), ctsRepProd);
            if (!ModelState.IsValid)
            {
                this.ClearModelState(ctsRepProd);
            }

            var Yr = DateTime.Now.Year;
            var Mo = DateTime.Now.Month;

            if (ctsRepProd.datefrom == null)
                ctsRepProd.datefrom = new DateTime(Yr, Mo, 1, 0, 0, 0);
            if (ctsRepProd.dateto == null)
                ctsRepProd.dateto = new DateTime(Yr, Mo, DateTime.DaysInMonth(Yr, Mo), 23, 59, 59);

            return View("product", ctsRepProd);
        }

        public ActionResult product_tabulation(Product ctsRepProd)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepProd), ctsRepProd);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<IRepProdMappable>(ctsRepProd);

            ctsRepProd.SetOutputHidden(true);

            return View("product_tabulation", ctsRepProd);
        }

        public ActionResult product_download_csv(Product ctsRepProd)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepProd), ctsRepProd);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<IProductCsvMappable>(ctsRepProd);

            return this.CsvFile(ctsRepProd.csvCreater.filePath);
        }

        public ActionResult call(Call ctsRepCall)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepCall), ctsRepCall);
            if (!ModelState.IsValid)
            {
                this.ClearModelState(ctsRepCall);
            }

            var Yr = DateTime.Now.Year;
            var Mo = DateTime.Now.Month;

            if (ctsRepCall.datefrom == null)
                ctsRepCall.datefrom = new DateTime(Yr, Mo, 1, 0, 0, 0);
            if (ctsRepCall.dateto == null)
                ctsRepCall.dateto = new DateTime(Yr, Mo, DateTime.DaysInMonth(Yr, Mo), 23, 59, 59);

            return View("call", ctsRepCall);
        }

        public ActionResult call_tabulation(Call ctsRepCall)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepCall), ctsRepCall);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<ICallListMappable>(ctsRepCall);

            ctsRepCall.SetOutputHidden(true);

            return View("call_tabulation", ctsRepCall);
        }

        public ActionResult call_download_csv(Call ctsRepCall)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepCall), ctsRepCall);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<ICallCsvMappable>(ctsRepCall);

            return this.CsvFile(ctsRepCall.csvCreater.filePath);
        }

        public ActionResult order(Order ctsRepOrder)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepOrder), ctsRepOrder);
            if (!ModelState.IsValid)
            {
                this.ClearModelState(ctsRepOrder);
            }

            var Yr = DateTime.Now.Year;
            var Mo = DateTime.Now.Month;
            if (ctsRepOrder.datefrom == null)
                ctsRepOrder.datefrom = new DateTime(Yr, Mo, 1, 0, 0, 0);
            if (ctsRepOrder.dateto == null)
                ctsRepOrder.dateto = new DateTime(Yr, Mo, DateTime.DaysInMonth(Yr, Mo), 23, 59, 59);

            return View("order", ctsRepOrder);
        }

        public ActionResult order_tabulation(Order ctsRepOrder)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepOrder), ctsRepOrder);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<IRepOrderMappable>(ctsRepOrder);

            ctsRepOrder.SetOutputHidden(true);

            return View("order_tabulation", ctsRepOrder);
        }

        public ActionResult order_download_csv(Order ctsRepOrder)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepOrder), ctsRepOrder);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<IOrderCsvMappable>(ctsRepOrder);

            return this.CsvFile(ctsRepOrder.csvCreater.filePath);
        }

        public ActionResult sales(Sales ctsRepSales)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepSales), ctsRepSales);
            if (!ModelState.IsValid)
            {
                this.ClearModelState(ctsRepSales);
            }

            ctsRepSales.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsRepSales.pageCnt, ctsRepSales.maxItemCount);

            var Yr = DateTime.Now.Year;
            var Mo = DateTime.Now.Month;

            if (ctsRepSales.datefrom == null) ctsRepSales.datefrom = new DateTime(Yr, Mo, 1, 0, 0, 0);
            if (ctsRepSales.dateto == null) ctsRepSales.dateto = new DateTime(Yr, Mo, DateTime.DaysInMonth(Yr, Mo), 23, 59, 59);

            if (ctsRepSales.searchscode)
            {
                if (ctsRepSales.searchscode1)
                    ctsRepSales.scode1 = ctsRepSales.scode;
                if (ctsRepSales.searchscode2)
                    ctsRepSales.scode2 = ctsRepSales.scode;
                if (ctsRepSales.searchscode3)
                    ctsRepSales.scode3 = ctsRepSales.scode;
                if (ctsRepSales.searchscode4)
                    ctsRepSales.scode4 = ctsRepSales.scode;
                if (ctsRepSales.searchscode5)
                    ctsRepSales.scode5 = ctsRepSales.scode;

                ctsRepSales.searchscode = false;
                ctsRepSales.searchscode1 = false;
                ctsRepSales.searchscode2 = false;
                ctsRepSales.searchscode3 = false;
                ctsRepSales.searchscode4 = false;
                ctsRepSales.searchscode5 = false;
            }

            if (ctsRepSales.searchscode1 || ctsRepSales.searchscode2 || ctsRepSales.searchscode3
                || ctsRepSales.searchscode4 || ctsRepSales.searchscode5)
            {
                ctsRepSales.searchscode = true;
                ctsRepSales.pageCnt = ctsRepSales.pager.pageCnt;

                this.mapperBus.Map<IProductSalesMappable>(ctsRepSales);
            }

            ctsRepSales.pager.LoadPageList(ctsRepSales.recordCount);

            return View("sales", ctsRepSales);
        }

        public ActionResult sales_tabulation(Sales ctsRepSales)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepSales), ctsRepSales);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            if (ctsRepSales.searchscode || ctsRepSales.searchscode1 || ctsRepSales.searchscode2
                || ctsRepSales.searchscode3 || ctsRepSales.searchscode4 || ctsRepSales.searchscode5)
            {
                return this.sales(ctsRepSales);
            }

            this.mapperBus.Map<IRepSalesMappable>(ctsRepSales);

            ctsRepSales.SetOutputHidden(true);

            return View("sales_tabulation", ctsRepSales);
        }

        public ActionResult sales_download_csv(Sales ctsRepSales)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepSales), ctsRepSales);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<ISalesCsvMappable>(ctsRepSales);

            return this.CsvFile(ctsRepSales.csvCreater.filePath);
        }

        public ActionResult contactlogs(ContactLogs ctsRepContactLogs)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepContactLogs), ctsRepContactLogs);
            if (!ModelState.IsValid)
            {
                this.ClearModelState(ctsRepContactLogs);
            }

            if (ctsRepContactLogs.datefrom == null)
                ctsRepContactLogs.datefrom = Convert.ToDateTime((DateTime.Today.Month.ToString() + "/01/" + DateTime.Today.Year.ToString()));
            if (ctsRepContactLogs.dateto == null)
                ctsRepContactLogs.dateto = Convert.ToDateTime(DateTime.Today.Month.ToString() + "/" + DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month).ToString() + "/" + DateTime.Today.Year.ToString());

            this.mapperBus.Map<ICategoryLabelMappable>(ctsRepContactLogs);

            return View("contactlogs", ctsRepContactLogs);
        }

        public ActionResult contactlogs_tabulation(ContactLogs ctsRepContactLogs)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepContactLogs), ctsRepContactLogs);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsRepContactLogs.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsRepContactLogs.pageCnt, ctsRepContactLogs.maxItemCount);
            ctsRepContactLogs.pageCnt = ctsRepContactLogs.pager.pageCnt;

            this.mapperBus.Map<ICategoryLabelMappable>(ctsRepContactLogs);
            this.mapperBus.Map<IRepContactLogMappable>(ctsRepContactLogs);

            ctsRepContactLogs.pager.LoadPageList(ctsRepContactLogs.recordCount);

            ctsRepContactLogs.SetOutputHidden(true);

            return View("contactlogs_tabulation", ctsRepContactLogs);
        }

        public ActionResult contactlogs_detail(ContactLogs ctsRepContactLogs)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepContactLogs), ctsRepContactLogs);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<ICategoryLabelMappable>(ctsRepContactLogs);
            this.mapperBus.Map<IRepContactLogDetailMappable>(ctsRepContactLogs);

            return View("contactlogs_detail", ctsRepContactLogs);
        }

        public ActionResult contactlogs_download_csv(ContactLogs ctsRepContactLogs)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepContactLogs), ctsRepContactLogs);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<IContactLogsCsvMappable>(ctsRepContactLogs);

            return this.CsvFile(ctsRepContactLogs.csvCreater.filePath);
        }

        public ActionResult category(Category ctsRepCategory)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepCategory), ctsRepCategory);
            if (!ModelState.IsValid)
            {
                this.ClearModelState(ctsRepCategory);
            }

            var Yr = DateTime.Now.Year;
            var Mo = DateTime.Now.Month;

            if (ctsRepCategory.datefrom == null) ctsRepCategory.datefrom = new DateTime(Yr, Mo, 1);
            if (ctsRepCategory.dateto == null) ctsRepCategory.dateto = new DateTime(Yr, Mo, DateTime.DaysInMonth(Yr, Mo), 23, 59, 59);
            
            return View("category", ctsRepCategory);
        }

        public ActionResult category_tabulation(Category ctsRepCategory)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepCategory), ctsRepCategory);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<IRepCategoryMappable>(ctsRepCategory);

            ctsRepCategory.SetOutputHidden(true);

            return View("category_tabulation", ctsRepCategory);
        }

        public ActionResult category_list(Category ctsRepCategory)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepCategory), ctsRepCategory);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsRepCategory.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsRepCategory.pageCnt, ctsRepCategory.maxItemCount);
            ctsRepCategory.pageCnt = ctsRepCategory.pager.pageCnt;

            this.mapperBus.Map<IRepCategoryListMappable>(ctsRepCategory);

            ctsRepCategory.pager.LoadPageList(ctsRepCategory.recordCount);
            ctsRepCategory.pagerPageCount = ctsRepCategory.pager.LastPage;

            ctsRepCategory.SetOutputHidden(true);

            return View("category_list", ctsRepCategory);
        }

        public ActionResult category_detail(Category ctsRepCategory)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepCategory), ctsRepCategory);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<IRepCategoryDetailsMappable>(ctsRepCategory);

            ctsRepCategory.SetOutputHidden(true);

            return View("category_detail", ctsRepCategory);
        }

        public ActionResult category_download_csv(Category ctsRepCategory)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepCategory), ctsRepCategory);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<ICategoryCsvMappable>(ctsRepCategory);

            return this.CsvFile(ctsRepCategory.csvCreater.filePath);
        }

        public ActionResult age(Age ctsRepAge)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepAge), ctsRepAge);
            if (!ModelState.IsValid)
            {
                this.ClearModelState(ctsRepAge);
            }

            ctsRepAge.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsRepAge.pageCnt, ctsRepAge.maxItemCount);

            var Yr = DateTime.Now.Year;
            var Mo = DateTime.Now.Month;

            if (ctsRepAge.datefrom == null) ctsRepAge.datefrom = new DateTime(Yr, Mo, 1, 0, 0, 0);
            if (ctsRepAge.dateto == null) ctsRepAge.dateto = new DateTime(Yr, Mo, DateTime.DaysInMonth(Yr, Mo), 23, 59, 59);

            if (ctsRepAge.searchscode)
            {
                if (ctsRepAge.searchscode1)
                    ctsRepAge.scode1 = ctsRepAge.scode;
                if (ctsRepAge.searchscode2)
                    ctsRepAge.scode2 = ctsRepAge.scode;
                if (ctsRepAge.searchscode3)
                    ctsRepAge.scode3 = ctsRepAge.scode;
                if (ctsRepAge.searchscode4)
                    ctsRepAge.scode4 = ctsRepAge.scode;
                if (ctsRepAge.searchscode5)
                    ctsRepAge.scode5 = ctsRepAge.scode;

                ctsRepAge.searchscode = false;
                ctsRepAge.searchscode1 = false;
                ctsRepAge.searchscode2 = false;
                ctsRepAge.searchscode3 = false;
                ctsRepAge.searchscode4 = false;
                ctsRepAge.searchscode5 = false;
            }

            if (ctsRepAge.medsearch || ctsRepAge.reload || ctsRepAge.medselect)
            {
                this.mapperBus.Map<ICampaignMappable>(ctsRepAge);
            }

            if (ctsRepAge.searchscode1 || ctsRepAge.searchscode2 || ctsRepAge.searchscode3
                || ctsRepAge.searchscode4 || ctsRepAge.searchscode5)
            {
                ctsRepAge.searchscode = true;
                ctsRepAge.pageCnt = ctsRepAge.pager.pageCnt;

                this.mapperBus.Map<IProductAgeMappable>(ctsRepAge);
            }

            ctsRepAge.pager.LoadPageList(ctsRepAge.recordCount);
            return View("age", ctsRepAge);
        }

        public ActionResult age_tabulation(Age ctsRepAge)
        {
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IReportsCommand>(ctsRepAge), ctsRepAge);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            if (ctsRepAge.medsearch || ctsRepAge.reload || ctsRepAge.medselect)
            {
                return this.age(ctsRepAge);
            }

            if (ctsRepAge.searchscode || ctsRepAge.searchscode1 || ctsRepAge.searchscode2
                || ctsRepAge.searchscode3 || ctsRepAge.searchscode4 || ctsRepAge.searchscode5)
            {
                return this.age(ctsRepAge);
            }

            this.mapperBus.Map<IRepAgeMappable>(ctsRepAge);

            ctsRepAge.SetOutputHidden(true);

            return View("age_tabulation", ctsRepAge);
        }
    }
}
