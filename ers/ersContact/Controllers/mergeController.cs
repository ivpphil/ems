﻿using System.Web.Mvc;
using ersContact.Domain.Merge.Commands;
using ersContact.Domain.Merge.Mappables;
using ersContact.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class mergeController
        : ErsControllerSecureContact
    {
        public ActionResult index(CtsMerge ctsMerge)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IMergeCommand>(ctsMerge), ctsMerge);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<IMergeMappable>(ctsMerge);

            return View("index", ctsMerge);
        }

        public ActionResult mergeconfirm(CtsMerge ctsMerge)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IMergeCommand>(ctsMerge), ctsMerge);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            if (ctsMerge.page == null)
            {
                ctsMerge.page = 1;
            }
            if (ctsMerge.final_member || ctsMerge.final_case)
            {
                ctsMerge.page = 3;
            }

            this.mapperBus.Map<IMergeMappable>(ctsMerge);

            return View("mergeconfirm", ctsMerge);
        }

        public ActionResult mergecomplete(CtsMerge ctsMerge)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IMergeCommand>(ctsMerge), ctsMerge);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsMerge.page = 3;
            var agent = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(this.cts_User_ID);
            if (agent == null)
            {
                throw new ErsException("29002");
            }
            ctsMerge.user_id = agent.user_id;

            commandBus.Submit((IMergeCommand)ctsMerge, EnumCommandTransaction.BeginTransaction);

            return View("mergecomplete", ctsMerge);
        }
    }
}
