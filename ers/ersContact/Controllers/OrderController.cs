﻿using System;
using System.Web;
using System.Web.Mvc;
using ersContact.Domain.Order.Commands;
using ersContact.Domain.Order.Mappables;
using ersContact.Models;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers.state;
using ersContact.jp.co.ivp.ers.mvc;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
    [ErsAuthorization]
    public class OrderController
        : ErsControllerSecureContact
    {
        /// <summary>
        /// トップ
        /// </summary>
        /// <returns></returns>
        ///
        public ActionResult Index(CtsOrder order)
        {
            return View("index", order);
        }

        public ActionResult Header(CtsOrder order)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("header", order);
        }

        public ActionResult Order(CtsOrder order, EnumEck? eck = null)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IOrderCommand>(order), order);
            if (!this.IsErrorBack(eck))
            {
                if (!ModelState.IsValid)
                {
                    //Cromeでのエラー戻り時データが消える為
                    if ((order.page1))
                    {
                        return GetErrorView(order.getReturnUrl(), order, "");
                    }
                    else
                    {
                        return GetErrorView();
                    }
                }
            }
            else
            {
                ClearModelState(order);//Modelのエラーを無視
            }
            order.IsErrorBack = (eck != null && eck != EnumEck.Normal);

            order.Cts_Ransu = this.cts_Ransu;
            order.cts_agent_id = this.cts_User_ID;

            var agent = ErsFactory.ersCtsOperatorFactory.GetErsCtsOperatorWithId(this.cts_User_ID);
            if (agent == null)
            {
                throw new ErsException("29002");
            }
            order.cts_order_user_id = agent.id.Value.ToString();
            order.cts_order_charge1_ag_type = agent.ag_type;
            order.cts_order_charge1_ag_name = agent.ag_name;

            this.mapperBus.Map<IOrderMappable>(order);

            commandBus.Submit((IMemberCommand)order, EnumCommandTransaction.BeginTransaction);
            this.mapperBus.Map<IMemberMappable>(order);

            if (Request.Cookies["FromSearch"] != null)
            {
                if (!string.IsNullOrEmpty(Request.Cookies["FromSearch"]["FromSearch"]))
                {
                    if (Server.UrlDecode(Request.Cookies["FromSearch"]["FromSearch"]) == "yes" && !order.mcode.HasValue() && !order.IsOrderUpdate)
                    {
                        order.lname = Server.UrlDecode(Request.Cookies["SearchFilter"]["lname"]);
                        order.fname = Server.UrlDecode(Request.Cookies["SearchFilter"]["fname"]);
                        order.lnamek = Server.UrlDecode(Request.Cookies["SearchFilter"]["lnamek"]);
                        order.fnamek = Server.UrlDecode(Request.Cookies["SearchFilter"]["fnamek"]);
                        order.tel = Server.UrlDecode(Request.Cookies["SearchFilter"]["tel"]);
                        if (!string.IsNullOrEmpty(Server.UrlDecode(Request.Cookies["SearchFilter"]["zip1"]))
                            || !string.IsNullOrEmpty(Server.UrlDecode(Request.Cookies["SearchFilter"]["zip2"])))
                        {
                            order.zip = Server.UrlDecode(Request.Cookies["SearchFilter"]["zip1"]) + "-" + Server.UrlDecode(Request.Cookies["SearchFilter"]["zip2"]);
                        }
                        order.address = Server.UrlDecode(Request.Cookies["SearchFilter"]["address"]);

                        if (!string.IsNullOrEmpty(Server.UrlDecode(Request.Cookies["SearchFilter"]["pref"])))
                        {
                            int pref_id;
                            if (Int32.TryParse(Server.UrlDecode(Request.Cookies["SearchFilter"]["pref"]), out pref_id))
                            {
                                order.pref = pref_id;
                            }
                        }

                        HttpCookie fromSearchCookie = new HttpCookie("FromSearch");
                        fromSearchCookie.Values["FromSearch"] = "no";
                        Response.Cookies.Add(fromSearchCookie);
                    }
                }
            }

            if (order.page1)
            {
                order.ent_point = null;
                commandBus.Submit((IShippingCommand)order, EnumCommandTransaction.BeginTransaction);
                this.mapperBus.Map<IShippingMappable>(order);
            }

            if (order.page2)
            {
                if (!order.non_member && order.deleted != EnumDeleted.Deleted && order.mall_shop_kbn == EnumMallShopKbn.ERS)
                {
                    commandBus.Submit((IPaymentCommand)order, EnumCommandTransaction.WithoutBeginTransaction);
                    this.mapperBus.Map<IPaymentMappable>(order);
                }

                commandBus.Submit((IDeliveryCommand)order, EnumCommandTransaction.BeginTransaction);
                this.mapperBus.Map<IDeliveryMappable>(order);

                // 定期商品の数量再計算
                if (order.regular_recalc)
                {
                    this.mapperBus.Map<IReCalcMappable>(order);
                }

                this.mapperBus.Map<IPointMappable>(order);

                //お届け希望ラジオデフォ値
                if (!string.IsNullOrEmpty(order.order_d_no) && order.goto_page2 && !order.from_page3)
                {
                    ErsOrder pOrder = ErsFactory.ersOrderFactory.GetOrderWithD_no(order.order_d_no, order.site_id);
                    if (pOrder == null)
                    {
                        throw new ErsException("30104", order.order_d_no);
                    }

                    // 非定期の最短お届け可能日とお届け希望日が一緒なら、｢最短｣を選択とする
                    order.firstTimeOrdinary = (pOrder.senddate == null) ? EnumFirstTimeOrdinary.Shortest : EnumFirstTimeOrdinary.Specify;
                }

                this.mapperBus.Map<IOrderRecomputeMappable>(order);

                //Map後チェック
                order.ValidateMappedOrder = true;
                this.ModelState.AddModelErrors(commandBus.Validate<IOrderCommand>(order), order);
                if (!ModelState.IsValid)
                {
                    return this.GetErrorView();
                }
                order.coupon_code = order.ent_coupon_code;

                //recalculate to reflect the coupon_price
                this.mapperBus.Map<IOrderRecomputeMappable>(order);
                this.mapperBus.Map<IRegularMappable>(order);

                //Set displayed Deliv Method
                this.mapperBus.Map<IDelivMethodDisplayListMappable>(order);
            }

            if (order.page3)
            {
                if (order.Edit_Temp == 0)
                {
                    if (!order.non_member && order.deleted != EnumDeleted.Deleted && order.mall_shop_kbn == EnumMallShopKbn.ERS)
                    {
                        commandBus.Submit((IPaymentCommand)order, EnumCommandTransaction.WithoutBeginTransaction);
                        this.mapperBus.Map<IPaymentMappable>(order);
                    }

                    commandBus.Submit((IDeliveryCommand)order, EnumCommandTransaction.BeginTransaction);
                    this.mapperBus.Map<IDeliveryMappable>(order);

                    this.mapperBus.Map<IPointMappable>(order);
                    this.mapperBus.Map<IOrderRecomputeMappable>(order);

                    //Map後チェック
                    order.ValidateMappedOrder = true;
                    this.ModelState.AddModelErrors(commandBus.Validate<IOrderCommand>(order), order);
                    if (!ModelState.IsValid)
                    {
                        return this.GetErrorView();
                    }

                    //recalculate to reflect the coupon_price
                    this.mapperBus.Map<IReCalcMappable>(order);
                    this.mapperBus.Map<IOrderRecomputeMappable>(order);

                    //Set displayed Deliv Method
                    this.mapperBus.Map<IDelivMethodDisplayListMappable>(order);
                }
            }

            if (order.goto_page4 && order.Edit_Temp == 1)
            {
                commandBus.Submit((ICtsOrderCommand)order, EnumCommandTransaction.BeginTransaction);

                if (order.cts_order_success)
                {
                    if (order.load_new_order)
                    {
                        var session = (ISession)ErsContext.sessionState;
                        session.getNewRansu();
                        var ransu = ErsContext.sessionState.Get("contact_ransu");
                        order.Cts_Ransu = ransu;
                        order.cts_order_ransu = ransu;
                    }
                }
            }

            if (order.goto_page4 && order.Edit_Temp == 0)
            {
                //OrderCommandHandler内でトランザクションを制御します
                commandBus.Submit((IOrderCommand)order, EnumCommandTransaction.WithoutBeginTransaction);

                try
                {
                    if (!order.disable_issue && (order.pay == EnumPaymentType.CONVENIENCE_STORE || order.pay == EnumPaymentType.BANK))
                    {
                        if (order.orderIntegrated != null && order.orderIntegrated.mall_shop_kbn == EnumMallShopKbn.ERS)
                        {
                            this.mapperBus.Map<IDeliveryMappable>(order);

                            order.etc = order.orderIntegrated.etc;
                            order.coupon_discount = order.orderIntegrated.coupon_discount;

                            //購入完了メール送信
                            var sendmailThankyou = ErsFactory.ersMailFactory.getErsSendMailThankyou();

                            //Separated non member send mail
                            if (order.non_member)
                                sendmailThankyou.Send(order.d_no, "", order.email, EnumMformat.PC, order, order, false);
                            else
                                sendmailThankyou.Send(order.d_no, order.member.mcode, order.member.email, order.member.mformat.Value, order, order, false);
                        }
                        else if (order.orderContainer != null && order.orderContainer.OrderHeader != null && (order.old_pay != order.pay || order.reauthed))
                        {
                            this.mapperBus.Map<IDeliveryMappable>(order);

                            //購入完了メール送信
                            var sendmailThankyou = ErsFactory.ersMailFactory.getErsSendMailThankyou();

                            //Separated non member send mail
                            if (order.non_member)
                                sendmailThankyou.Send(order.d_no, "", order.email, EnumMformat.PC, order, order, false);
                            else
                                sendmailThankyou.Send(order.d_no, order.member.mcode, order.member.email, order.member.mformat.Value, order, order, false);
                        }
                    }
                }
                catch (Exception e)
                {
                    //メール送信はエラー表示させない。
                    //ログに落とす。
                    ErsCommon.loggingException(e.ToString());

                    if (ErsFactory.ersUtilityFactory.getSetup().debug)
                        throw;
                }

                if (order.order_success)
                {
                    var session = (ISession)ErsContext.sessionState;
                    session.getNewRansu();
                    var ransu = ErsContext.sessionState.Get("contact_ransu");
                    order.Cts_Ransu = ransu;
                    order.cts_order_ransu = ransu;
                }
            }

            AddModelToView(order);
            return View("order", order);
        }

        [HttpPost]
        public ActionResult CampaignName(Campaign campaign)
        {
            return View("campaign_name", campaign);
        }
    }
}
