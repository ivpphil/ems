﻿using System.Web.Mvc;
using ersContact.Domain.Api.Commands;
using ersContact.Domain.Api.Mappables;
using ersContact.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using ersContact.Models.api;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using System.Web;
using jp.co.ivp.ers;
using System.Collections.Generic;
using ersContact.Domain.Search.Commands;

namespace ersContact.Controllers
{
    [ValidateInput(false)]
    public class apiController
        : ErsControllerSecureContact
    {

        /// <summary>
        /// 郵便番号検索１
        /// 非同期検索処理
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult get_zip(getZip get_zip)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IGetZipCommand>(get_zip), get_zip);

            if (ModelState.IsValid)
            {
                //Validatin結果はMapの中で判定する。
                this.mapperBus.Map<IGetZipMappable>(get_zip);
            }

            return View("get_zip", get_zip);
        }

        /// <summary>
        /// JavaScriptへ渡す定数を定義
        /// </summary>
        /// <param name="get_zip"></param>
        /// <returns></returns>
        public virtual ActionResult get_const()
        {
            return View("get_const");
        }

        [ErsRequireHttps]
        [ErsAuthorization]
        public virtual string withdraw_member(Withdraw_member withdraw_member)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IWithdrawMemberCommand>(withdraw_member), withdraw_member);

            if (!ModelState.IsValid)
            {
                return string.Join("\r\n", withdraw_member.GetFieldErrorMessageList("mcode"));
            }

            //Validatin結果はMapの中で判定する。
            this.commandBus.Submit<IWithdrawMemberCommand>(withdraw_member, EnumCommandTransaction.BeginTransaction);
            return "0";
        }

        [ErsRequireHttps]
        [ErsAuthorization]
        public virtual string delete_reserved(DeleteReserved model)
        {
            ModelState.AddModelErrors(commandBus.Validate<ICtsOrderDeleteCommand>(model), model);

            if (!ModelState.IsValid)
            {
                return string.Join("\r\n", model.GetAllErrorMessageList());
            }

            commandBus.Submit((ICtsOrderDeleteCommand)model, EnumCommandTransaction.BeginTransaction);
            
            return "0";
        }

        /// <summary>
        /// クッキーセッション接頭辞取得 [Get cookie session prefix]
        /// </summary>
        /// <returns>Json</returns>
        [ErsRequireHttps]
        public virtual ActionResult get_cookie_session_prefix()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            return Json(new { prefix = setup.cookieSessionPrefix }, JsonRequestBehavior.AllowGet);
        }
    }
}
