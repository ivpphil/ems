﻿using System.Web.Mvc;
using ersContact.Domain.Configuration.Commands;
using ersContact.Domain.Configuration.Mappables;
using ersContact.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
    [ErsAuthorization]
    public class ConfigurationController
        : ErsControllerSecureContact
    {
        /// <summary>
        /// トップ
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult index(CtsConfig config)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            return View("configuration", config);
        }

        #region " FAQ CSV "

        /// <summary>
        /// FAQ CSV登録
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public ActionResult ConfigFaqCsvMenu(CtsConfig config)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            return View("ConfigFaqCsvMenu", config);
        }

        /// <summary>
        /// 新規一括登録（入れ替え）画面
        /// </summary>
        /// <param name="faq"></param>
        /// <returns></returns>
        public ActionResult ConfigFaqCsvReplace(CtsConfig config)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            config.replace = 1;
            return View("ConfigFaqCsvUploadForm", config);
        }

        /// <summary>
        /// 追加登録画面
        /// </summary>
        /// <param name="faq"></param>
        /// <returns></returns>
        public ActionResult ConfigFaqCsvAppend(CtsConfig config)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            config.replace = 0;
            return View("ConfigFaqCsvUploadForm", config);
        }

        /// <summary>
        /// 新規一括登録、追加登録 完了画面
        /// </summary>
        /// <param name="faq"></param>
        /// <returns></returns>
        public ActionResult ConfigFaqCsvComplete(ctsCsvFaq faq)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<ICtsFaqUpdateCommand>(faq), faq);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            faq.ctsUserID = this.cts_User_ID;

            this.commandBus.Submit<ICtsFaqUpdateCommand>(faq, EnumCommandTransaction.BeginTransaction);

            return View("ConfigFaqCsvUploadComplete", faq);
        }

        /// <summary>
        /// 登録FAQ CSVダウンロード画面
        /// </summary>
        /// <param name="faq"></param>
        /// <returns></returns>
        public ActionResult ConfigFaqCsvDownload(ctsFaq_csv faq)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            this.mapperBus.Map<ICtsFaqCSVDownMappable>(faq);

            return this.CsvFile(faq.csvCreater.filePath);
        }

        #endregion

        #region " Email From Management "

        public ActionResult ConfigContactEmailFrom(CtsFromAddress fromaddress, EnumEck? eck = null)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            if (!ModelState.IsValid)
            {
                if (fromaddress.back)
                {
                    ClearModelState(fromaddress);
                }
            }

            if (fromaddress.regist || fromaddress.modify || fromaddress.delete || fromaddress.reset || fromaddress.save)
            {
                return ManageContactEmailFrom(fromaddress, eck);
            }

            return ContactEmailFromList(fromaddress);
        }

        public ActionResult ContactEmailFromList(CtsFromAddress fromaddress)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", fromaddress.pageCnt, fromaddress.maxItemCount);

            fromaddress.pager = pager;

            this.mapperBus.Map<ICtsEmailFromListMappable>(fromaddress);

            fromaddress.pager.LoadPageList(fromaddress.recordCount);

            //FormタグにHiddenを自動入力
            fromaddress.SetOutputHidden(true);
            fromaddress.SetOutputHidden("id", false);

            return View("ContactEmailFrom", fromaddress);
        }

        public ActionResult ManageContactEmailFrom(CtsFromAddress fromaddress, EnumEck? eck = null)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<ICtsEmailListUpdateCommand>(fromaddress), fromaddress);
            if (!ModelState.IsValid)
            {
                if (!IsErrorBack(eck))
                {
                    fromaddress.SetOutputHidden(true);
                    return GetErrorView();
                }
            }

            if (fromaddress.regist || fromaddress.modify || fromaddress.reset)
            {

                this.mapperBus.Map<ICtsEmailFromDetailMappable>(fromaddress);

                return View("ContactEmailFromManage", fromaddress);
            }

            else if (fromaddress.save || fromaddress.delete)
            {
                this.commandBus.Submit<ICtsEmailListUpdateCommand>(fromaddress, EnumCommandTransaction.BeginTransaction);
                return ContactEmailFromList(fromaddress);
            }
            else
            {
                return ContactEmailFromList(fromaddress);
            }
        }

        #endregion

        #region " Client Escalation "

        public ActionResult ConfigEscalationDetail(CtsClientEscalation clientescalation)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            if (clientescalation.regist || clientescalation.modify || clientescalation.delete || clientescalation.reset || clientescalation.save)
            {
                return ManageContactClientEscalation(clientescalation);
            }
            else
            {
                return ContactClientEscalationList(clientescalation);
            }
        }

        public ActionResult ContactClientEscalationList(CtsClientEscalation clientescalation)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", clientescalation.pageCnt, clientescalation.maxItemCount);

            clientescalation.pager = pager;

            this.mapperBus.Map<ICtsEscalationListMappable>(clientescalation);

            clientescalation.pager.LoadPageList(clientescalation.recordCount);

            //FormタグにHiddenを自動入力
            clientescalation.SetOutputHidden(true);

            return View("ContactClientEscalation", clientescalation);
        }

        public ActionResult ManageContactClientEscalation(CtsClientEscalation clientescalation)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<ICtsEscalationUpdateCommand>(clientescalation), clientescalation);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            if (clientescalation.regist || clientescalation.modify || clientescalation.reset)
            {
                this.mapperBus.Map<ICtsEscalationDetailMappable>(clientescalation);

                return View("ContactClientEscalationManage", clientescalation);
            }
            else if (clientescalation.save || clientescalation.delete)
            {
                this.commandBus.Submit<ICtsEscalationUpdateCommand>(clientescalation, EnumCommandTransaction.BeginTransaction);

                return ContactClientEscalationList(clientescalation);
            }
            else
            {
                return ContactClientEscalationList(clientescalation);
            }
        }

        #endregion

        #region " Category Management "

        public ActionResult ConfigCategory(CtsConfigCategory config)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<ICtsCategoryListMappable>(config);

            return View("ConfigCategory", config);
        }

        public ActionResult ConfigCategoryManage(CtsConfigCategoryManage configManage)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<ICtsCategoryUpdateCommand>(configManage), configManage);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.commandBus.Submit<ICtsCategoryUpdateCommand>(configManage, EnumCommandTransaction.BeginTransaction);

            this.mapperBus.Map<ICtsCategoryMappable>(configManage);

            return View("ConfigCategoryManage", configManage);
        }

        #endregion
    }

}
