﻿using System;
using System.Web;
using System.Web.Mvc;
using ersContact.Domain.Faq.Commands;
using ersContact.Domain.Faq.Mappables;
using ersContact.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class faqController
        : ErsControllerSecureContact
    {
        public ActionResult index(ctsFAQ ctsFaq) 
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IFaqRegisterCommand>(ctsFaq), ctsFaq);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<ICategoryListMappable>(ctsFaq);

            return View("FAQTemplate", ctsFaq);
        }

        public ActionResult template(ctsFAQ  ctsFaq) 
        {
            ctsFaq.user_id = this.cts_User_ID;

            this.ModelState.AddModelErrors(commandBus.Validate<IFaqRegisterCommand>(ctsFaq), ctsFaq);
            if (!ModelState.IsValid)
            {
                return this.index(ctsFaq);
            }

            this.mapperBus.Map<ICategoryListMappable>(ctsFaq);

            if (ctsFaq.reset == true)
            {
                this.resetFAQ(ctsFaq);
                return this.index(ctsFaq);
            }

            if (ctsFaq.search == true)
            { 
                this.mapperBus.Map<IFaqListMappable>(ctsFaq);
                this.writeSearchCookies(ctsFaq);
            }

            if (ctsFaq.newregist == true)
            {
                ctsFaq.id = 0;
                ctsFaq.intime = DateTime.Now;
                ctsFaq.utime = DateTime.Now;
                ctsFaq.faq_casename = null;
                ctsFaq.cate1 = null;
                ctsFaq.cate2 = null;
                ctsFaq.cate3 = null;
                ctsFaq.cate4 = null;
                ctsFaq.cate5 = null;
                ctsFaq.faq_text = null;
                ctsFaq.faq_text2 = null;
                ctsFaq.openRegist = true;
            }

            return View("FAQTemplate", ctsFaq);
        }

        public ActionResult regist(ctsFAQ ctsFaq)
        {
            ctsFaq.user_id = this.cts_User_ID;

            this.ModelState.AddModelErrors(commandBus.Validate<IFaqRegisterCommand>(ctsFaq), ctsFaq);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.mapperBus.Map<ICategoryListMappable>(ctsFaq);

            if (ctsFaq.back)
            {
                this.readSearchCookies(ctsFaq);
                return this.index(ctsFaq);
            }

            if (ctsFaq.reset)
            {
                this.resetFAQ(ctsFaq);
                ctsFaq.openRegist = true;
                return this.index(ctsFaq);
            }

            if (ctsFaq.regist)
            {
                if (ctsFaq.id == 0 || ctsFaq.id == null)
                {
                    commandBus.Submit((IFaqRegisterCommand)ctsFaq, EnumCommandTransaction.BeginTransaction);
                }
                else
                {
                    commandBus.Submit((IFaqUpdateCommand)ctsFaq, EnumCommandTransaction.BeginTransaction);
                }

                this.mapperBus.Map<IFaqListMappable>(ctsFaq);
            }
            else
            {
                this.mapperBus.Map<IRegisterListMappable>(ctsFaq);
            }

            return View("FAQTemplate", ctsFaq);
        }

        internal void resetFAQ(ctsFAQ ctsFaq)
        {
            ctsFaq.faq_casename = null;
            ctsFaq.cate1 = null;
            ctsFaq.cate2 = null;
            ctsFaq.cate3 = null;
            ctsFaq.cate4 = null;
            ctsFaq.cate5 = null;
            ctsFaq.faq_text = null;
            ctsFaq.faq_text2 = null;
        }

        private void writeSearchCookies(ctsFAQ ctsFaq)
        {
            HttpCookie srchFAQ = new HttpCookie("SearchFAQ");
            srchFAQ.Values["cate1"] = Server.UrlEncode(ctsFaq.cate1.ToString());
            srchFAQ.Values["cate2"] = Server.UrlEncode(ctsFaq.cate2.ToString());
            srchFAQ.Values["cate3"] = Server.UrlEncode(ctsFaq.cate3.ToString());
            srchFAQ.Values["cate4"] = Server.UrlEncode(ctsFaq.cate4.ToString());
            srchFAQ.Values["cate5"] = Server.UrlEncode(ctsFaq.cate5.ToString());
            srchFAQ.Values["keywords"] = Server.UrlEncode(ctsFaq.keywords);
            Response.Cookies.Add(srchFAQ);
        }

        private void readSearchCookies(ctsFAQ ctsFaq)
        {
            if (Server.UrlDecode(Request.Cookies["SearchFAQ"]["cate1"]).ToString() == "")
            { ctsFaq.cate1 = null; }
            else
            { ctsFaq.cate1 = Convert.ToInt32(Server.UrlDecode(Request.Cookies["SearchFAQ"]["cate1"])); }

            if (Server.UrlDecode(Request.Cookies["SearchFAQ"]["cate2"]).ToString() == "")
            { ctsFaq.cate2 = null; }
            else
            { ctsFaq.cate2 = Convert.ToInt32(Server.UrlDecode(Request.Cookies["SearchFAQ"]["cate2"])); }

            if (Server.UrlDecode(Request.Cookies["SearchFAQ"]["cate3"]).ToString() == "")
            { ctsFaq.cate3 = null; }
            else
            { ctsFaq.cate3 = Convert.ToInt32(Server.UrlDecode(Request.Cookies["SearchFAQ"]["cate3"])); }

            if (Server.UrlDecode(Request.Cookies["SearchFAQ"]["cate4"]).ToString() == "")
            { ctsFaq.cate4 = null; }
            else
            { ctsFaq.cate4 = Convert.ToInt32(Server.UrlDecode(Request.Cookies["SearchFAQ"]["cate4"])); }

            if (Server.UrlDecode(Request.Cookies["SearchFAQ"]["cate5"]).ToString() == "")
            { ctsFaq.cate5 = null; }
            else
            { ctsFaq.cate5 = Convert.ToInt32(Server.UrlDecode(Request.Cookies["SearchFAQ"]["cate5"])); }

            ctsFaq.keywords = Server.UrlDecode(Request.Cookies["SearchFAQ"]["keywords"]);
        }
    }
}
