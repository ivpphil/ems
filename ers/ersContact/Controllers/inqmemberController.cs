﻿using System.Web.Mvc;
using ersContact.Domain.Inqmember.Commands;
using ersContact.Domain.Inqmember.Mappables;
using ersContact.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class inqmemberController
        : ErsControllerSecureContact
    {
        public ActionResult index(CtsInqMember ctsInqMember)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IInqMemberCommand>(ctsInqMember), ctsInqMember);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("index", ctsInqMember);
        }
        
        public ActionResult clientinfo(CtsInqMember ctsInqMember)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IInqMemberCommand>(ctsInqMember), ctsInqMember);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsInqMember.User_ID = this.cts_User_ID;

            if (ctsInqMember.renew == true || ctsInqMember.cancel == true)
            {
                ctsInqMember.edit = false;
            }
            else if (ctsInqMember.mcode == "")
            {
                ctsInqMember.edit = true;
            }

            if (ctsInqMember.renew == true)
            {
                commandBus.Submit((IInqMemberCommand)ctsInqMember, EnumCommandTransaction.BeginTransaction);
            }

            this.mapperBus.Map<IClientInfoMappable>(ctsInqMember);

            return View("ctsClientInfoIndex", ctsInqMember);
        }

        public ActionResult merge(CtsInqMember ctsInqMember)
        {
            return View("index", ctsInqMember);
        }
    }
}
