﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ersContact.Domain.Login.Commands;
using ersContact.Domain.Login.Mappables;
using ersContact.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    public class loginController
        : ErsControllerSecureContact
    {
        public ActionResult login(Login login, EnumEck? eck = null) 
        {
            if (!ModelState.IsValid && !login.sessionError)
            {
                this.ClearModelState(login); // エラーをクリア
                this.ModelState.AddModelError("common", ErsResources.GetMessage("10204"));　//共通エラー

                if (!this.IsErrorBack(eck))
                {
                    return GetErrorView();
                }
            }

            if (login.sessionError)
            {
                this.ModelState.AddModelError("error", ErsResources.GetMessage("10203"));
            }

            return View("login", login);
        }

        /// <summary>
        /// CTSログイン画面からのログイン
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult loginaction(Login login)
        {
            ((ISession)ErsContext.sessionState).LogOut();

            this.CommonClear();

            this.ModelState.AddModelErrors(commandBus.Validate<ILoginCommand>(login), login);
            if (!ModelState.IsValid)
            {
                return this.login(login, EnumEck.Error);
            }

            commandBus.Submit((ILoginCommand)login, EnumCommandTransaction.WithoutBeginTransaction);

            return View("loginaction");
        }

        /// <summary>
        /// 管理画面からのログイン（伝票修正時）
        /// </summary>
        /// <param name="loginadmin"></param>
        /// <returns></returns>
        public ActionResult loginaction_admin(Login_admin loginadmin)
        {
            ((ISession)ErsContext.sessionState).LogOut();

            this.CommonClear();

            this.ModelState.AddModelErrors(commandBus.Validate<ILoginAdminCommand>(loginadmin), loginadmin);
            if (!ModelState.IsValid)
            {
                this.ClearModelState(loginadmin); // エラーをクリア
                this.ModelState.AddModelError("common", ErsResources.GetMessage("10204"));　//共通エラー

                return GetErrorView();
            }

            //user_cdよりctsログイン情報を取得
            var login = new Login();
            login.user_cd = loginadmin.user_cd;

            this.mapperBus.Map<IAgentMappable>(login);

            commandBus.Submit((ILoginCommand)login, EnumCommandTransaction.WithoutBeginTransaction);

            var dictionary = new RouteValueDictionary();
            dictionary.Add("order_d_no", loginadmin.d_no);

            return this.RedirectToAction("index", "order", dictionary);
        }

        [ErsAuthorization]
        public ActionResult logout()
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ((ISession)ErsContext.sessionState).LogOut();

            this.CommonClear();

            return RedirectToAction("index", "Home");
        }

        /// <summary>
        /// 追加共通クリア処理
        /// </summary>
        private void CommonClear()
        {
            //バスケットクリア
            ErsFactory.ersBasketFactory.GetEmptyBasketStrategy().EmptyBasket(ErsContext.sessionState.Get("ransu"));

            //検索条件クッキー削除
            this.clearSearchCookie();
        }

        /// <summary>
        /// 検索条件のクッキー削除
        /// </summary>
        private void clearSearchCookie()
        {
            HttpCookie srchCookie = new HttpCookie("SearchFilter");
            srchCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(srchCookie);

            HttpCookie fromSearchCookie = new HttpCookie("FromSearch");
            fromSearchCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(fromSearchCookie);
        }
    }
}
