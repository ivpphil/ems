﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.administrator;
using ersContact.Models;
using ersContact.Domain.Information.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using ersContact.Domain.Information.Mappables;
using jp.co.ivp.ers;
using ersContact.Domain.Direction.Commands;
using ersContact.Domain.Direction.Mappables;

namespace ersContact.Controllers
{

    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class HomeController
        : ErsControllerSecureContact
    {
        public ActionResult Index(CtsIndex index)
        {
            var seturl = ErsFactory.ersUtilityFactory.getSetup();
            
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }
            
            EnumUserState state = ((ISession)ErsContext.sessionState).getUserState();

            if (state != EnumUserState.LOGIN)
            {
                return Redirect(seturl.sec_url.ToString() + "top/login/asp/login.asp");
            }

            //model initialize モデル初期化
            index.agent_id = cts_User_ID;
            if (index.calendarMonth == new DateTime())
                index.calendarMonth = DateTime.Today;

            if (index.backCalendar)
                index.calendarMonth = index.calendarMonth.AddMonths(-1);

            if (index.nextCalendar)
                index.calendarMonth = index.calendarMonth.AddMonths(1);

            index.moveCalendar = index.nextCalendar || index.backCalendar;


            //既読処理
            if (index.confirm)
            {
                //既読処理command
                commandBus.Submit((IInfoReadInsertCommand)index, EnumCommandTransaction.BeginTransaction);
            }

            //Information取得Mapper
            index.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", index.pageCnt, index.maxItemCount);
            this.mapperBus.Map<IInformationListMappable>(index);
            index.pager.LoadPageList(index.recordCount);


            //未読Information取得Mapper
            this.mapperBus.Map<IUnReadInformationMappable>(index);

            index.SetOutputHidden(true);

            return View("index", index);
        }

        public ActionResult CheckInst(CtsIndex index)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", index.pageCnt, index.maxItemCount);
            //index.Init(pager, this.cts_User_ID);

            index.user_id = this.cts_User_ID;

            this.mapperBus.Map<ICheckInstructionListMappable>(index);

            return View("CheckInst", index);
        }

        public ActionResult ConfirmInst(CtsIndex index)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", index.pageCnt, index.maxItemCount);
            //index.Init(pager, this.cts_User_ID);

            CtsDirection ctsDir = new CtsDirection();
            ctsDir.ctsUserID = this.cts_User_ID;
            ctsDir.id = index.id;

            this.commandBus.Submit((IInstructionUpdateCommand)ctsDir, EnumCommandTransaction.WithoutBeginTransaction);

            return View("CheckInst");
        }
    }
}
