﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using ersContact.Models;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db.table;
using System.Data;
using jp.co.ivp.ers.db;
using System.Data.Common;
using jp.co.ivp.ers.state;
using ersContact.jp.co.ivp.ers.mvc;


namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class HeaderController
        : ErsControllerSecureContact
    {

        public ActionResult header(ErsContactModelBase list)
        {

            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("header", list);
        }

    }
}
