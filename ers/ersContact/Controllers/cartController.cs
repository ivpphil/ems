﻿using System.Web.Mvc;
using ersContact.Domain.Cart.Commands;
using ersContact.Domain.Cart.Mappables;
using ersContact.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class CartController
        : ErsControllerSecureContact
    {
        /// <summary>
        /// トップ
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index(CtsCart cart)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<ICartCommand>(cart), cart);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var receipt_ransu = cart.ransu;
            string t = this.cts_Ransu;
            if (!string.IsNullOrEmpty(receipt_ransu)) t = receipt_ransu;
            cart.ransu = t;

            this.mapperBus.Map<ICartMappable>(cart);

            this.commandBus.Submit<ICartCommand>(cart, EnumCommandTransaction.BeginTransaction);

            this.mapperBus.Map<ICartMappable>(cart);

            return View("index", cart);
        }

        public ActionResult Disable(CtsCart cart)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<ICartCommand>(cart), cart);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            var receipt_ransu = cart.ransu;
            string t = this.cts_Ransu;
            if (!string.IsNullOrEmpty(receipt_ransu)) t = receipt_ransu;
            cart.ransu = t;

            this.commandBus.Submit<ICartCommand>(cart, EnumCommandTransaction.BeginTransaction);

            this.mapperBus.Map<ICartMappable>(cart);

            return View("d_index", cart);
        }

    }
}
