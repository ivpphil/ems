﻿using System;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using ersContact.Models;
using ersContact.Domain.Direction.Mappables;
using ersContact.Domain.Direction.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class directionController
        : ErsControllerSecureContact
    {
        public ActionResult index(CtsDirection ctsDirection)
        {
            ctsDirection.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsDirection.pageCnt, ctsDirection.maxItemCount);
            ctsDirection.ctsUserID = this.cts_User_ID;
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }
            return View("index", ctsDirection);
        }

        public ActionResult Agent(CtsDirection ctsDirection)
        {
            ctsDirection.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsDirection.pageCnt, ctsDirection.maxItemCount);
            ctsDirection.ctsUserID = this.cts_User_ID;
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsDirection.pageCnt = ctsDirection.pager.pageCnt;

            if (ctsDirection.search)
            {
                this.mapperBus.Map<IDirectionListMappable>(ctsDirection);
            }
            else if (ctsDirection.showinst)
            {
                this.mapperBus.Map<IInstructionListMappable>(ctsDirection);
            }
            else if (ctsDirection.ConfirmIns)
            {
                commandBus.Submit((IInstructionUpdateCommand)ctsDirection, EnumCommandTransaction.WithoutBeginTransaction);
            }

            if (ctsDirection.agentsearch.Equals(false) && ctsDirection.search.Equals(true))
                ctsDirection.agentsearch = ctsDirection.search;

            ctsDirection.pager.LoadPageList(ctsDirection.recordCount);

            var Yr = DateTime.Now.Year;
            var Mo = DateTime.Now.Month;

            if (ctsDirection.datefrom == null) ctsDirection.datefrom = new DateTime(Yr, Mo, 1);
            if (ctsDirection.dateto == null) ctsDirection.dateto = new DateTime(Yr, Mo, DateTime.DaysInMonth(Yr, Mo), 23, 59, 59);

            return View("ctsAgent", ctsDirection);
        }

        public ActionResult Inbound(CtsDirection ctsDirection)
        {

            ctsDirection.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsDirection.pageCnt, ctsDirection.maxItemCount);
            ctsDirection.ctsUserID = this.cts_User_ID;
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            if (ctsDirection.InstRegist)
            {
                commandBus.Submit((IDirectionRegisterCommand)ctsDirection, EnumCommandTransaction.BeginTransaction);
            }
            else
            {
                ctsDirection.pageCnt = ctsDirection.pager.pageCnt;

                if (!ctsDirection.inboundInput && ctsDirection.source > 0 && !ctsDirection.ShowListAll && !ctsDirection.ShowListNow)
                {
                    //TODO: マジックナンバーを解消
                    ctsDirection.ShowListAll = (ctsDirection.source == 2);
                    ctsDirection.ShowListNow = (ctsDirection.source == 1);
                }

                if (ctsDirection.ShowListAll || ctsDirection.ShowListNow)
                {
                    this.mapperBus.Map<IInboundListMappable>(ctsDirection);
                    ctsDirection.inboundList = (!ctsDirection.inboundInput);
                    //TODO: マジックナンバーを解消
                    ctsDirection.source = (ctsDirection.ShowListNow) ? 1 : (ctsDirection.ShowListAll) ? 2 : 0;
                    ctsDirection.LabelProgress = (ctsDirection.progress == EnumEnqProgress.Open) ? "Open" : "Close";
                    ctsDirection.SetOutputHidden("pager", true);
                }
                else if (ctsDirection.inboundInput)
                {
                    this.mapperBus.Map<IInboundInstructionListMappable>(ctsDirection);
                }
                else
                {
                    this.mapperBus.Map<IInboundNowListMappable>(ctsDirection);
                    this.mapperBus.Map<IInboundAllListMappable>(ctsDirection);
                    ctsDirection.ShowInboundStatus = ((ctsDirection.NowInboundType.Count > 0 || ctsDirection.AllInboundType.Count > 0) && !ctsDirection.inboundList && !ctsDirection.inboundInput);
                }
            }

            ctsDirection.pager.LoadPageList(ctsDirection.recordCount);

            return View("ctsInbound", ctsDirection);
        }

        public ActionResult search(CtsDirection ctsDirection)
        {
            ctsDirection.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsDirection.pageCnt, ctsDirection.cardErr_maxItemCount);
            ctsDirection.ctsUserID = this.cts_User_ID;
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }
            return View("search", ctsDirection);
        }

        public ActionResult list(CtsDirection ctsDirection)
        {
            ctsDirection.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsDirection.pageCnt, ctsDirection.cardErr_maxItemCount);
            ctsDirection.ctsUserID = this.cts_User_ID;
            if (!this.IsSuperUser())
            {
                throw new ErsException("10209");
            }

            this.commandBus.Submit<ICardErrListUpdateCommand>(ctsDirection, EnumCommandTransaction.BeginTransaction);

            ctsDirection.pageCnt = ctsDirection.pager.pageCnt;
            this.mapperBus.Map<ICardErrListMappable>(ctsDirection);

            ctsDirection.pager.LoadPageList(ctsDirection.recordCount);

            ctsDirection.SetOutputHidden(true);

            return View("list", ctsDirection);
        }
    }
}
