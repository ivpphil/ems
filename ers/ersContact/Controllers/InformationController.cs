﻿using System.Web.Mvc;
using ersContact.Domain.Information.Commands;
using ersContact.Domain.Information.Mappables;
using ersContact.Models;
using jp.co.ivp.ers.information;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class InformationController
        : ErsControllerSecureContact
    {
        /// <summary>
        /// トップ
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult index(CtsInformation information)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IInformationCommand>(information), information);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return manage(information);
        }

        public ActionResult unreadlist(CtsInformation information)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IInformationCommand>(information), information);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            information.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", information.pageCnt, information.maxItemCount);
            information.agent_id = this.cts_User_ID;
            information.ag_name = information.Logged_In_User;

            this.mapperBus.Map<IUnReadInfoListMappable>(information);

            return View("NonReadAgent", information);
        }

        private ActionResult manage(CtsInformation information)
        {
            if (!this.IsAuthorized())
            {
                throw new ErsException("10209");
            }

            this.ModelState.AddModelErrors(commandBus.Validate<IInformationCommand>(information), information);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            information.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", information.pageCnt, information.maxItemCount);
            information.agent_id = this.cts_User_ID;
            information.ag_name = information.Logged_In_User;

            information.SetOutputHidden(true);

            if ((information.regist && information.done) || (information.modify && information.done) || information.delete)
            {
                commandBus.Submit((IInformationCommand)information, EnumCommandTransaction.BeginTransaction);
            }

            if (information.done)
            {
                information.regist = false;
                information.modify = false;
                information.delete = false;
                information.cancel = true;
            }

            if (information.cancel == true)
            {
                information.id = null;
                information.title = "";
                information.contents = "";
            }

            this.mapperBus.Map<IModifyInfoListMappable>(information);

            if (information.id != null)
            {
                ErsCtsInformation info = ErsFactory.ersCtsInformationFactory.GetErsCtsInformationWithId(information.id.Value);
                information.OverwriteWithParameter(info.GetPropertiesAsDictionary());
            }

            information.pager.LoadPageList(information.recordCount);

            return View("information", information);
        }
    }
}
