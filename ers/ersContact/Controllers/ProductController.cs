﻿using System.Web.Mvc;
using ersContact.Domain.Product.Mappables;
using ersContact.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;
using ersContact.Domain.Product.Commands;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class ProductController
        : ErsControllerSecureContact
    {
        /// <summary>
        /// トップ
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult search(CtsProduct product)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            product.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", product.pageCnt, product.maxItemCount);

            if (string.IsNullOrEmpty(product.ransu))
            {
                product.ransu = this.cts_Ransu;
            }

            product.SetOutputHidden(true);

            this.mapperBus.Map<IProductMappable>(product);
            this.mapperBus.Map<IWishListMappable>(product);

            product.pager.LoadPageList(product.recordCount);

            return View("index", product);
        }

        public ActionResult Disable(CtsProduct product)
        {
            return View("d_index", product);
        }

        public ActionResult regist_wishlist(CtsProduct product)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IWishListRegistCommand>(product), product);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.commandBus.Submit<IWishListRegistCommand>(product, EnumCommandTransaction.BeginTransaction);

            return this.search(product);
        }

        public ActionResult delete_wishlist(CtsProduct product)
        {
            this.ModelState.AddModelErrors(this.commandBus.Validate<IWishListRegistCommand>(product), product);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            this.commandBus.Submit<IWishListDeleteCommand>(product, EnumCommandTransaction.BeginTransaction);

            return this.search(product);
        }
    }
}
