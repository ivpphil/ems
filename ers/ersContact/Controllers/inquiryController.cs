﻿using System.Web.Mvc;
using ersContact.Domain.Inquiry.Commands;
using ersContact.Domain.Inquiry.Mappables;
using ersContact.Models;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;

namespace ersContact.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    [ErsLanguageMenu]
	[ErsAuthorization]
    public class inquiryController
        : ErsControllerSecureContact
    {
        /// <summary>
        /// index
        /// </summary>
        /// <param name="ctsInquiry"></param>
        /// <returns></returns>
        public ActionResult index(CtsInquiry ctsInquiry)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IInquiryCommand>(ctsInquiry), ctsInquiry);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsInquiry.ctsUserID = this.cts_User_ID;
            ctsInquiry.user_id = this.cts_User_ID.ToString();
            ctsInquiry.showValidate = true;

            this.mapperBus.Map<ICategoryListMappable>(ctsInquiry);

            return View("index", ctsInquiry);
        }
        
        /// <summary>
        /// Header
        /// </summary>
        /// <param name="ctsInquiry"></param>
        /// <returns></returns>
        public ActionResult Header(CtsInquiry ctsInquiry)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("header", ctsInquiry);
        }

        /// <summary>
        /// footer
        /// </summary>
        /// <param name="ctsInquiry"></param>
        /// <returns></returns>
        public ActionResult Footer(CtsInquiry ctsInquiry)
        {
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            return View("footer", ctsInquiry);
        }

        /// <summary>
        /// 問い合わせケースリスト
        /// </summary>
        /// <param name="ctsInquiry"></param>
        /// <returns></returns>
        public ActionResult caselist(CtsInquiry ctsInquiry)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IInquiryCommand>(ctsInquiry), ctsInquiry);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsInquiry.pager = ErsFactory.ersViewServiceFactory.GetErsViewPagerService().GetPagerModel(this, "Pager", ctsInquiry.pageCnt, ctsInquiry.maxItemCount);
            ctsInquiry.ctsUserID = this.cts_User_ID;
            ctsInquiry.user_id = this.cts_User_ID.ToString();
            ctsInquiry.showValidate = true;
            ctsInquiry.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            this.mapperBus.Map<ICategoryListMappable>(ctsInquiry);
            this.mapperBus.Map<ICategoryLabelMappable>(ctsInquiry);
            this.mapperBus.Map<ISupportCountMappable>(ctsInquiry);

            if (ctsInquiry.incmail || ctsInquiry.escdel)
            {
                if (ctsInquiry.escdel)
                {
                    commandBus.Submit((IEnquiryMailCommand)ctsInquiry, EnumCommandTransaction.BeginTransaction);
                }
                this.mapperBus.Map<IIncMailListMappable>(ctsInquiry);
            }
            else if (ctsInquiry.search)
            {
                this.mapperBus.Map<ISearchCaseListMappable>(ctsInquiry);
            }
            else if (ctsInquiry.mailapp)
            {
                this.mapperBus.Map<IMailApproveMappable>(ctsInquiry);
            }
            else
            {
                ctsInquiry.pageCnt = ctsInquiry.pager.pageCnt;
                this.mapperBus.Map<IInquiryListMappable>(ctsInquiry);
            }

            if (ctsInquiry.caselist == false && ctsInquiry.needsup == false && ctsInquiry.incmail == false && ctsInquiry.searchcase == false && ctsInquiry.mailapp == false)
            {
                ctsInquiry.caselist = true;
            }

            //ページ送り
            ctsInquiry.pager.LoadPageList(ctsInquiry.recordCount);

            ctsInquiry.SetOutputHidden("inquiry_list", true);

            return View("ctsInquiryList", ctsInquiry);
        }

        public ActionResult historylist(CtsInquiry ctsInquiry)
        {
            this.ModelState.AddModelErrors(commandBus.Validate<IInquiryCommand>(ctsInquiry), ctsInquiry);
            if (!ModelState.IsValid)
            {
                return GetErrorView();
            }

            ctsInquiry.ctsUserID = this.cts_User_ID;
            ctsInquiry.user_id = this.cts_User_ID.ToString();
            ctsInquiry.showValidate = true;
            ctsInquiry.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            this.mapperBus.Map<ICategoryListMappable>(ctsInquiry);
            this.mapperBus.Map<ICategoryLabelMappable>(ctsInquiry);
            this.mapperBus.Map<ISupportCountMappable>(ctsInquiry);
            this.mapperBus.Map<IHistoryListMappable>(ctsInquiry);

            ctsInquiry.SetOutputHidden("history_list", true);

            return View("ctsInquiryList", ctsInquiry);
        }

        public ActionResult caseinfo(CtsInquiry ctsInquiry)
        {
            ctsInquiry.ctsUserID = this.cts_User_ID;
            ctsInquiry.user_id = this.cts_User_ID.ToString();
            ctsInquiry.showValidate = true;
            ctsInquiry.site_id = ErsFactory.ersUtilityFactory.getSetup().site_id;

            this.mapperBus.Map<ICategoryListMappable>(ctsInquiry);
            this.mapperBus.Map<ICategoryLabelMappable>(ctsInquiry);

            if (!ctsInquiry.doRegist && !ctsInquiry.doEscalate && !ctsInquiry.mailescsend && !ctsInquiry.mailesctray) //confirmation = cancel
            {
                ctsInquiry.phonereg = ctsInquiry.doRegist;
                ctsInquiry.mailreg = ctsInquiry.doRegist;
                ctsInquiry.memoreg = ctsInquiry.doRegist;
                ctsInquiry.mailsent = ctsInquiry.doRegist;
                ctsInquiry.showValidate = ctsInquiry.doRegist;
            }
            
            this.mapperBus.Map<IEscalateListMappable>(ctsInquiry);
            this.mapperBus.Map<IFromAddressMappable>(ctsInquiry);

            if (!TabClick(ctsInquiry))
            {
                this.ModelState.AddModelErrors(commandBus.Validate<IInquiryCommand>(ctsInquiry), ctsInquiry);
                if (!ModelState.IsValid)
                {
                    return GetErrorView();
                }
                else
                {
                    if (ctsInquiry.case_no != null && ctsInquiry.case_no > 0)
                    {
                        this.mapperBus.Map<ILockByOtherMappable>(ctsInquiry);
                    }

                    commandBus.Submit((IInquiryCommand)ctsInquiry, EnumCommandTransaction.BeginTransaction);

                    this.mapperBus.Map<ICaseInfoMappable>(ctsInquiry);

                    if ((ctsInquiry.enq_corresponding == EnumEnqCorresponding.Email && ctsInquiry.showmailapp) || ctsInquiry.enq_type == EnumEnqType.InboundEmail)
                    {
                        this.mapperBus.Map<IEscalateListMappable>(ctsInquiry);
                    }
                }
            }

            ctsInquiry.SetOutputHidden("ctsCase", true);

            return View("ctsCase", ctsInquiry);
        }

        public ActionResult escalateto(CtsInquiry ctsInquiry)
        {
            ctsInquiry.ctsUserID = this.cts_User_ID;
            ctsInquiry.user_id = this.cts_User_ID.ToString();
            ctsInquiry.showValidate = true;
            
            this.mapperBus.Map<ICategoryListMappable>(ctsInquiry);
            this.mapperBus.Map<ICategoryLabelMappable>(ctsInquiry);
            this.mapperBus.Map<IEscalateListMappable>(ctsInquiry);

            return View("ctsCaseFooter", ctsInquiry);
        }

        public ActionResult faqDefault(CtsInquiry ctsInquiry)
        {
            ctsInquiry.ctsUserID = this.cts_User_ID;
            ctsInquiry.user_id = this.cts_User_ID.ToString();
            ctsInquiry.showValidate = true;

            this.mapperBus.Map<ICategoryListMappable>(ctsInquiry);

            ctsInquiry.email_body = ctsInquiry.enq_detail;

            return View("ctsCase", ctsInquiry);
        }

        private bool TabClick(CtsInquiry ctsInquiry)
        {
            bool IsClick = false;
            if (!string.IsNullOrEmpty(ctsInquiry.tabsel1))
            {
                IsClick = true;
                ctsInquiry.enq_corresponding = EnumEnqCorresponding.Phone;
            }

            if (!string.IsNullOrEmpty(ctsInquiry.tabsel2))
            {
                IsClick = true;
                ctsInquiry.enq_corresponding = EnumEnqCorresponding.Email;
            }
            if (!string.IsNullOrEmpty(ctsInquiry.tabsel3))
            {
                IsClick = true;
                ctsInquiry.enq_corresponding = EnumEnqCorresponding.Memo;
            }

            if (IsClick && !ctsInquiry.edit_dtl_tabs)
            {
                ctsInquiry.edit_dtl = false;
                ctsInquiry.mailtemplate = true;
            }

            if (ctsInquiry.edit_dtl_tabs)
            {
                IsClick = false;
            }

            return IsClick;
        }
    }
}
