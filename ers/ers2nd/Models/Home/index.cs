﻿using jp.co.ivp.ers;
using jp.co.ivp.ers.util.mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ers2nd.Models.Home
{
    public class Index
        : ers.Models.Home.Index
    {
        /// <summary>
        /// get content code for smartphone
        /// </summary>
        public override string contents_code
        {
            get
            {
                var site_type = new ErsMobileCommon().GetSiteType();

                if (site_type == EnumSiteType.SMARTPHONE)
                {
                    return "20000004";
                }
                
                return "20000001";
            }
        }
    }
}
