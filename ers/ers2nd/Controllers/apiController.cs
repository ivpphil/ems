﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;

namespace ers2nd.Controllers
{
    [ErsRequireHttps(false)]
    [ValidateInput(false)]
    [ErsSideMenu]
    public class apiController
        : ers.Controllers.apiController
    {
    }
}
