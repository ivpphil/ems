/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function(){
	var ersObj = ErsDetail();
	ersObj.simgChange();
	ersObj.matrixChange();
	ersObj.wishList();
	ersObj.socialBookmark();
});

/* ErsDetailオブジェクト生成コンストラクタ */
var ErsDetail = function () {

    var that = {};

    /* 商品画像切り替え処理
    ---------------------------------------------------------------- */
    that.simgChange = function () {
        var domBimg = $("#l_col h3 img"); //大画像DOM
        var path = ""; 	//クリックされた画像パス

        $("#l_col li img").click(function () {
            path = $(this).attr("src");
            path = path.replace("simg", "bimg");
            path = path.split("?")[0] + "?width=264";
            domBimg.fadeOut("slow", function () {
                domBimg.attr("src", path)
                $(this).fadeIn("slow");
            });
        });
    }

    /* グループ商品選択時の処理
    ---------------------------------------------------------------- */
    that.matrixChange = function () {
        var domSelect = $("#matrix_form select"); //グループ商品表示selectタグオブジェクト

        //jqueryでonchangeの挙動がIEだけ異なるため、通常のeventバインド処理
        if (domSelect.size() !== 0) {
            domSelect.get()[0].onchange = function () {
                $("#matrix_form").submit();
            }
        }
    }

    /* ウィッシュリストボタン表示
    ---------------------------------------------------------------- */
    that.wishList = function () {
        //ログイン状態であれば表示
        if (ErsLib.isLogin()) {
            $("#wishlist").css("display", "block");
        }
    }

    /* ソーシャルブックマーク表示
    ---------------------------------------------------------------- */
    that.socialBookmark = function () {
        var objBookmarkDiv; 	//ソーシャルブックマークが入ったdiv
        var objWebserviceDiv; //ブックマークiconが入ったdiv

        objWebserviceDiv = $(".webservice");
        objBookmarkDiv = $(".socialbookmark", objWebserviceDiv);

        $(".bookmark_btn", objWebserviceDiv).mouseover(function () {
            objBookmarkDiv.fadeIn("fast");
        });

        objBookmarkDiv.mouseleave(function () {
            objBookmarkDiv.fadeOut("fast");
        });

        objWebserviceDiv.mouseleave(function () {
            objBookmarkDiv.fadeOut("fast");
        });
    }

    return that;
}

