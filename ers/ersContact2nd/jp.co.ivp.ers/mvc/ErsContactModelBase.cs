﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.cts_operators;
using jp.co.ivp.ers;

namespace ersContact2nd.jp.co.ivp.ers.mvc
{
    public class ersContact2ndModelBase
        : ErsModelBase
    {
        public bool Authorized
        {
            get
            {
                return (ErsContext.sessionState.Get<EnumAgType>("ag_type") == EnumAgType.Admin);
            }
        }

        public bool SuperUser
        {
            get
            {
                return (ErsContext.sessionState.Get("cts_authority") == CtsAuthorityType.SUPERVISOR);
            }
        }

        public string Logged_In_User
        {
            get
            {
                return ErsContext.sessionState.Get("user_name");
            }
        }

        public bool HasUnreadIns
        {
            get
            {
                var rep = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionsManagementRepository();

                var criteria = ErsFactory.ersCtsDirectionFactory.GetErsCtsDirectionsManagementCriteria();

                criteria.to_user_id = Convert.ToInt32(ErsContext.sessionState.Get("cts_user_id"));
                criteria.check_s = 0;
                criteria.enq_progress = EnumEnqProgress.Open;

                var UnreadCount = rep.GetRecordCountInstruction(criteria);
                return UnreadCount > 0;
            }
        }
    }
}
