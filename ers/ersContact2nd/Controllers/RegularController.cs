﻿using System;
using System.Web.Mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ersContact2nd.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    public class RegularController
        : ersContact.Controllers.RegularController
    {
        
    }
}
