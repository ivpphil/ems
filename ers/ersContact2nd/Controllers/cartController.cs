﻿using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ersContact2nd.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    public class CartController
        : ersContact.Controllers.CartController
    {
        
    }
}
