﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;

namespace ersContact2nd.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    public class SearchController
        : ersContact.Controllers.SearchController
    {
      
    }
}
