﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers;

namespace ersContact2nd.Controllers
{

    [ErsRequireHttps]
    [ValidateInput(false)]
    public class HomeController
        : ersContact.Controllers.HomeController
    {
    }
}
