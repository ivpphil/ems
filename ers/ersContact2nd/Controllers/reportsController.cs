﻿using System;
using System.Web.Mvc;

using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.state;
using Models.reports;
using jp.co.ivp.ers;

namespace ersContact2nd.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    public class ReportsController
        : ersContact.Controllers.ReportsController
    {
        
    }
}
