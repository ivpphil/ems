﻿using System.Web.Mvc;
using jp.co.ivp.ers.information;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.state;
using jp.co.ivp.ers;

namespace ersContact2nd.Controllers
{
    [ErsRequireHttps]
    [ValidateInput(false)]
    public class InformationController
        : ersContact.Controllers.InformationController
    {
       
    }
}
