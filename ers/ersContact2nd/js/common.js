﻿/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function () {
    var ersObj = ErsCommon();
    ersObj.welcome();
    ersObj.formSubmit();
    ersObj.imgChange();
    ersObj.inputFocus();
    ersObj.setJqueryUiDatePicker();
    ersObj.smoothScroll();
    ersObj.DblClickCK();
    //ersObj.googleTranslate();
});


/* ErsCommonオブジェクト生成コンストラクタ */
var ErsCommon = function () {

    var that = {};

    /* ヘッダー部分・ようこそ表示
    ---------------------------------------------------------------- */
    that.welcome = function () {
        var objNicknameDiv; 			//nicknameが入るdivタグオブジェクト
        var strNickname = ErsLib.getPageInfo()["strNickname"]; 	//Nickname文字列

        if (ErsLib.isLogin()) {
            objNicknameDiv = $("#ERS_header h2 span");
            if (ErsLib.getPageInfo()["strFilename"] != "entry3.asp" && objNicknameDiv.size() != 0) {
                objNicknameDiv.html("ようこそ！　" + strNickname + "&nbsp;さん！");
            }
        }
    }

    /* フォームsubmit処理
    ---------------------------------------------------------------- */
    that.formSubmit = function () {
        //flexigridの影響を避けるため、setTimeoutでキューの最後にもってくる
        window.setTimeout(function () {
            $(".form_btn").click(function () {
                var attrRel = $(this).attr("rel");
                if (typeof attrRel != "undefined") {
                    var formInfo = attrRel.split('$');
                    //対象form名
                    var formName = formInfo[0];
                    //submit先
                    var formPath = formInfo[1];
                    //submit処理
                    $("#" + formName).attr("action", formPath).submit();
                }
            })
        }, 0)
    }

    /* ロールオーバーイメージ表示
    ---------------------------------------------------------------- */
    that.imgChange = function () {
        var img_out; //mouseout時のsrc属性を格納
        var img_in; //mouseover時のsrc属性を格納
        var objChangeImg = $(".change");
        var hoverDom = {	//マウスオーバーされているDOM要素
            "tarDom": "",
            "img_out": ""
        };

        objChangeImg.hover(function () {
            //mouseover時の処理
            img_out = $(this).attr("src");
            img_in = img_out.replace("_off.gif", "_on.gif");
            $(this).attr("src", img_in);
            hoverDom = {
                "tarDom": $(this),
                "img_out": img_out
            };
        },
		function () {
		    //mouseout時の処理
		    $(this).attr("src", img_out);
		});

        //ページ移動時にロールオーバーを戻す
        $(window).unload(function () {
            //ブラウザバックの時にエラーになるため、try～catch
            try {
                hoverDom["tarDom"].attr("src", hoverDom["img_out"]);
            } catch (e) {
                //何もしない
            }
        });
    }

    /* inputフォーカス時に背景色変更
    ---------------------------------------------------------------- */
    that.inputFocus = function () {
        var bgColor = "#ffF4e1"
        var tarDom = $("input[type='text'],input[type='password'],textarea");

        tarDom.focus(function () {
            $(this).css("backgroundColor", bgColor);
        });

        tarDom.blur(function () {
            $(this).css("backgroundColor", "");
        });
    }

    /* jquery ui DatePickerの設定
    ---------------------------------------------------------------- */
    that.setJqueryUiDatePicker = function () {

        //datepicker共通設定
        var commonSetup = {
            closeText: '閉じる',
            prevText: '&#x3c;前',
            nextText: '次&#x3e;',
            currentText: '今日',
            monthNames: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
            monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
            dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
            dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
            dayNamesMin: ['日', '月', '火', '水', '木', '金', '土'],
            weekHeader: '週',
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: true,
            yearSuffix: '年'
        };

        $(".datepicker_start").datepicker($.extend({}, commonSetup, {
            dateFormat: "yy/mm/dd 00:00:00"
        }));

        $(".datepicker_end").datepicker($.extend({}, commonSetup, {
            dateFormat: "yy/mm/dd 23:59:59"
        }));

        $(".datepicker_date").datepicker($.extend({}, commonSetup, {
            dateFormat: "yy/mm/dd"
        }));


    }

    /* スムーススクロール
    ---------------------------------------------------------------- */
    that.smoothScroll = function () {
        var a_list = $("a[href^='#']").click(function (e) {
            var end = $(this).attr("href");
            ahead = end.substr(1);
            start_scroll(ahead);
            return false;
        });

        function start_scroll(end) {
            //出発地、到着地の要素取得
            var end_posi;
            try {
                end_posi = $("#" + end).get(0);
            } catch (e) {
                try {
                    end_posi = $("@name=" + end).get(0);
                } catch (e) {
                    return false;
                }
            }

            //出発地、到着地の座標取得処理
            var start_co;
            var screen_size;
            var doc_height = document.body.offsetHeight;
            var end_co = end_posi.offsetTop;

            if (ErsLib.isIE()) { // IE判別
                start_co = document.documentElement.scrollTop;
                screen_size = document.documentElement.clientHeight;
            }
            else {
                start_co = window.pageYOffset;
                screen_size = window.innerHeight;
            }

            if (doc_height - end_co < screen_size) {
                end_co = doc_height - screen_size;
            }

            var sabun = end_co - start_co;
            if (sabun == 0) return false;

            //スクロールスピードとかの設定
            var speed = Math.round(sabun / 10); //スクロールスピードの設定
            var riding = Math.round(sabun * 1 / 2); //着地の設定
            var scroll = window.scrollBy;
            var page_ck; //ページ下まで行った時のチェック用
            var doc_ele = document.documentElement;
            // マウス操作でスクロールストップ
            if (window.addEventListener) window.addEventListener("DOMMouseScroll", scroll_stop, false);
            window.onmousewheel = document.onmousewheel = scroll_stop;
            function scroll_stop() {
                window.clearInterval(scroll_trigger);
            }

            // スクロールスタートのトリガー
            if (sabun < 0) { // 下から上へ移動する場合
                if (ErsLib.isIE()) { // IE用処理
                    var scroll_trigger = setInterval(up_timer_ie, 2);
                }
                else {
                    var scroll_trigger = setInterval(up_timer, 2);
                }
            }
            else { // 上から下へ移動する場合
                if (ErsLib.isIE()) { // IE用処理
                    var scroll_trigger = setInterval(bottom_timer_ie, 2);
                }
                else {
                    window.clearInterval(scroll_trigger);
                    var scroll_trigger = setInterval(bottom_timer, 2);
                }
            }

            // スクロールさせる関数
            // 下から上へスクロールさせる場合（InternetEXplorer用）
            function up_timer_ie() { // 
                if (end_co - doc_ele.scrollTop < riding) {
                    scroll(0, speed);
                }
                else if ((end_co - doc_ele.scrollTop) / 10 < -1) {
                    scroll(0, (end_co - doc_ele.scrollTop) / 10);
                }
                else {
                    scroll(0, -1);
                    if (end_co - doc_ele.scrollTop == 0) window.clearInterval(scroll_trigger);
                }
            }

            // 下から上へスクロールさせる場合（IE以外のモダンブラウザ用）
            function up_timer() {
                if (end_co - window.pageYOffset < riding) {
                    scroll(0, speed);
                }
                else if ((end_co - window.pageYOffset) / 10 < -1) {
                    scroll(0, (end_co - window.pageYOffset) / 10);
                }
                else {
                    scroll(0, -1);
                    if (end_co - window.pageYOffset == 0) window.clearInterval(scroll_trigger);
                }
            }

            // 上から下へスクロールさせる場合（InternetEXplorer用）
            function bottom_timer_ie() {
                if (end_co - doc_ele.scrollTop > riding) {
                    page_ck = end_co - doc_ele.scrollTop;
                    scroll(0, speed);
                    if (page_ck == end_co - doc_ele.scrollTop) window.clearInterval(scroll_trigger);
                }
                else if ((end_co - doc_ele.scrollTop) / 10 > 1) {
                    page_ck = end_co - doc_ele.scrollTop;
                    scroll(0, page_ck / 10);
                    if (page_ck == end_co - doc_ele.scrollTop) window.clearInterval(scroll_trigger);
                }
                else {
                    scroll(0, 1);
                    if (end_co - doc_ele.scrollTop == 0) window.clearInterval(scroll_trigger);
                }
            }

            // 上から下へスクロールさせる場合（IE以外のモダンブラウザ用）
            function bottom_timer() {
                if (end_co - window.pageYOffset > riding) {
                    page_ck = end_co - window.pageYOffset;
                    scroll(0, speed);
                    if (page_ck == end_co - window.pageYOffset) window.clearInterval(scroll_trigger);
                }
                else if ((end_co - window.pageYOffset) / 10 > 1) {
                    page_ck = end_co - window.pageYOffset;
                    scroll(0, page_ck / 10);
                    if (page_ck == end_co - window.pageYOffset) window.clearInterval(scroll_trigger);
                }
                else {
                    scroll(0, 1);
                    if (end_co - window.pageYOffset == 0) window.clearInterval(scroll_trigger);
                }
            }
        }
    }
    /* 二度押しチェック
    ---------------------------------------------------------------- */
    that.DblClickCK = function () {
        var SbmBtn = $("input[type='image'],input[type='submit']");
        var dblflg = 0;
        SbmBtn.click(function () {
            //二度押し制限*****************************************
            if (dblflg == 0) {
                var btn_id = $(this).attr("id");
                //郵便番号検索は何度でもOKにしないとAjax処理が一度しか押せなくなる。
                //FAQテンプレートについても何度でもOKにしないとボタンが一度しか押せなくなる。
                if ((btn_id != "zip_flg1") && (btn_id != "zip_flg2") && (btn_id != "zip_flg3") && (btn_id != "faqtemplate")) {
                    //一度目はフラグを立てる
                    dblflg = 1;
                }
            } else {
                //exclude buttons with multi-clickable class
                if (!$(this).hasClass('multi-clickable')) {
                    //二度目はボタンを非活性(イメージは変更出来ない)
                    $(this).attr("disabled", "true");
                    //submit処理を実行飛ばさない
                    return false;
                }
            }
        });
    }


    /* Google翻訳表示
    ---------------------------------------------------------------- */
    that.googleTranslate = function () {
        if (location.host == "dev-ersv7.ivp.co.jp") {
            var str = '<div id="google_translate_element"></div><script type="text/javascript">';
            str += 'function googleTranslateElementInit() {';
            str += 'new google.translate.TranslateElement({pageLanguage: "ja", layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, "google_translate_element");';
            str += '}';
            str += '</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>';
            $("body").prepend(str);
        }
    }

    return that;

}

