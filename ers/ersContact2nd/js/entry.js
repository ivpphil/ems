/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function () {
    var ersObj = ErsEntry();
    ersObj.payCk();
    ersObj.sendCk();
    ersObj.addressCk();
    ersObj.zipAutoedit();
    ersObj.cardCk();
});

/* ErsEntryオブジェクト生成コンストラクタ */
var ErsEntry = function () {

    var that = {};

    //formオブジェクト
    var objForm = $("form[name='entry_form']");

    /* 支払い方法別の表示
    ---------------------------------------------------------------- */
    that.payCk = function () {
        //引数の設定
        var objPay = {
            "strSelector": "input[name='pay']:checked",
            "ckVal": ["1", "8"], 		//サイバーソース or ネットプロテクションカード決済
            "domDisp": $("#cardform")
        }

        //onchangeイベントをバインド
        $('input[name="pay"]:radio').change(function () {
            ErsLib.dispChange(objPay);
        }).change();
    }


    /* 「別住所で配送」選択時の表示
    ---------------------------------------------------------------- */
    that.sendCk = function () {
        //引数の設定
        var objSend = {
            "strSelector": "#send",
            "ckVal": ["2"], 		//別住所にお届け
            "domDisp": $('#aaddress')
        }

        //onchangeイベントをバインド
        $('#send').change(function () {
            ErsLib.dispChange(objSend);
        }).change();
    }

    /* お届け先選択時の項目自動入力
    ---------------------------------------------------------------- */
    that.addressCk = function () {
        //ロード時にチェック
        try {
            selectChange($('#address_type').attr('value'));
        } catch (e) {
            //何もしない
        }

        //onchangeイベントをバインド
        $('#address_type').change(function () {
            selectChange($('#address_type').attr('value'));
        })
    }

    /* カードリスト選択時の項目自動入力
    ---------------------------------------------------------------- */
    that.cardCk = function () {
        //ロード時にチェック
        try {
            selectCardChange($('#card_id').val());
        } catch (e) {
            //何もしない
        }

        //onchangeイベントをバインド
        $('#card_id').change(function () {
            selectCardChange($('#card_id').val());
        })
    }

    /* 郵便番号から住所自動入力
    ---------------------------------------------------------------- */
    that.zipAutoedit = function () {
        var objAddress = {}; 	//郵便番号検索の引数設定オブジェクト
        //clickイベントをバインド
        $("#zip_flg1,#zip_flg2").click(function () {
            var thisId = $(this).attr("id");

            if (thisId === "zip_flg1") {
                //address1用引数の設定
                objAddress = {
                    "domZip": $("#zip"),
                    "domPref": $("#pref"),
                    "domAddress": $("#address"),
                    "zip_search_error": $("#zip_search_error")
                }
            } else if (thisId === "zip_flg2") {
                //address2用引数の設定
                objAddress = {
                    "domZip": $("#add_zip"),
                    "domPref": $("#add_pref"),
                    "domAddress": $("#add_address"),
                    "zip_search_error": $("#add_zip_search_error")
                }
            }

            //郵便番号検索
            ErsLib.zipSearch(objAddress);

            return false;
        });
    }

    return that;
}
