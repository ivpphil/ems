/*============================================================================
    Author and Copyright
        製作者: IVP CO,LTD（http://www.ivp.co.jp/）
        作成日: 2009-03-06
        修正日: 2009-03-06

各関数の最初に下記のようなコメントをつけています。
コメントではその関数が行っている処理と、使用されているページを記載しています。

例）
 処理の内容（使用ページ：そのページのファイル名）
 ---------------------------------------------------------------- 

その他、処理内にも適宜コメントをつけていますので参考ください。
============================================================================*/

//onloadイベントの設定
$(document).ready(function () {
    var ersObj = ErsMember1();
    ersObj.zipAutoedit();
});

/* ErsMember1オブジェクト生成コンストラクタ */
var ErsMember1 = function () {

    var that = {};

    //formオブジェクト
    var objForm = $("form[name='view']");

    /* 郵便番号から住所自動入力
    ---------------------------------------------------------------- */
    that.zipAutoedit = function () {
        var thisId = ""; 	//クリックされた要素のID
        var objAddress = {}; 	//郵便番号検索の引数設定オブジェクト

        //clickイベントをバインド
        $("#zip_flg1").click(function () {
            var thisId = $(this).attr("id");
            
            //引数の設定
            objAddress = {
                "domZip": $("#zip"),
                "domPref": $("#pref"),
                "domAddress": $("#address"),
                "domAddress2": $("#taddress"),
                "domAddress3": $("#maddress"),
                "zip_search_error": $("#zip_search_error")
            }

            //郵便番号検索
            ErsLib.zipSearch(objAddress);

            return false;
        });
    }

    return that;
}
