﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace ersTestLibrary.ers.member
{
    public class user
    {
        /// <summary>
        /// LINK 別お届け先を編集する
        /// </summary>
        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/ul[4]/li[2]/a")]
        public IWebElement address_list_link { get; set; }

        /// <summary>
        /// LINK ご購入履歴の検索
        /// </summary>
        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/ul[1]/li[1]/a")]
        public IWebElement bill1_link { get; set; }
    }
}
