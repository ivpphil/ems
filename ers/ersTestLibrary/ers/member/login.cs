﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace ersLibraryTest.Site.ers.member
{
    public class login
    {
        [FindsBy(How = How.Name, Using = "email")]
        public IWebElement email { get; set; }

        [FindsBy(How = How.Name, Using = "passwd")]
        public IWebElement passwd { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/form/div/div/p[2]/input")]
        public IWebElement submit { get; set; }
    }
}
