﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace ersTestLibrary.ers.member
{
    public class address1
    {
        [FindsBy(How = How.Name, Using = "address_name")]
        public IWebElement address_name { get; set; }

        [FindsBy(How = How.Name, Using = "add_lname")]
        public IWebElement add_lname { get; set; }

        [FindsBy(How = How.Name, Using = "add_fname")]
        public IWebElement add_fname { get; set; }

        [FindsBy(How = How.Name, Using = "add_lnamek")]
        public IWebElement add_lnamek { get; set; }

        [FindsBy(How = How.Name, Using = "add_fnamek")]
        public IWebElement add_fnamek { get; set; }

        [FindsBy(How = How.Name, Using = "add_zip")]
        public IWebElement add_zip { get; set; }

        [FindsBy(How = How.Name, Using = "add_pref")]
        public IWebElement add_pref { get; set; }

        [FindsBy(How = How.Name, Using = "add_address")]
        public IWebElement add_address { get; set; }

        [FindsBy(How = How.Name, Using = "add_taddress")]
        public IWebElement add_taddress { get; set; }

        [FindsBy(How = How.Name, Using = "add_maddress")]
        public IWebElement add_maddress { get; set; }

        [FindsBy(How = How.Name, Using = "add_tel")]
        public IWebElement add_tel { get; set; }

        [FindsBy(How = How.Name, Using = "add_fax")]
        public IWebElement add_fax { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/form/div/p[2]/input")]
        public IWebElement submit { get; set; }

        [FindsBy(How = How.Id, Using = "zip_flg2")]
        public IWebElement search_zip { get; set; }
    }
}
