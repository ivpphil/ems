﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace ersTestLibrary.ers.member
{
    public class address2
    {
        [FindsBy(How = How.XPath, Using = "html/body/div/div/section/article/div/dl/dd")]
        public IWebElement address_name { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/div/dl[2]/dd")]
        public IWebElement add_name { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/div/dl[3]/dd")]
        public IWebElement add_namek { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/div/dl[4]/dd")]
        public IWebElement add_tel { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/div/dl[5]/dd")]
        public IWebElement add_fax { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/div/dl[6]/dd")]
        public IWebElement add_zip { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/div/dl[7]/dd")]
        public IWebElement add_pref { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/div/dl[8]/dd")]
        public IWebElement add_address { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/div[3]/div/form[2]/p/input")]
        public IWebElement submit { get; set; }

        [FindsBy(How = How.Name, Using = "regist_submit")]
        public IWebElement delete_submit { get; set; }
    }
}
