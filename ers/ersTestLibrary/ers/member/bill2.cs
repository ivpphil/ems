﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace ersTestLibrary.ers.member
{
    public class bill2
    {
        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/table")]
        public IWebElement table { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/table/tbody/tr[2]/td[4]/form[2]/input[2]")]
        public IWebElement first_row_submit { get; set; }
    }
}
