﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;
using ersTestLibrary.common;

namespace ersTestLibrary.ers.member
{
    public class address_list
        : PagerCommon
    {
        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/table")]
        public IWebElement table { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/table/tbody/tr[2]/td[4]/form[2]/input[2]")]
        public IWebElement first_row_submit { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/div[3]/form/p/input")]
        public IWebElement regist_submit { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/section/article/table/tbody/tr[2]/td[4]/form[1]/input[2]")]
        public IWebElement delete_submit { get; set; }

        public By tableBy { get { return By.XPath("/html/body/div/div/section/article/table"); } }
    }
}
