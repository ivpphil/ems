﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace ersTestLibrary.ers
{
    public class ValidationMessage
    {
        [FindsBy(How = How.ClassName, Using = "ERS_input_error")]
        public IWebElement DefaultFrontErrorMessages { get; set; }


        public IEnumerable<string> error_messages
        {
            get
            {
                var messageArray = this.DefaultFrontErrorMessages.Text.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                if (this.DefaultFrontErrorMessages != null)
                {
                    foreach (var message in messageArray)
                        yield return message;
                }

                yield break;
            }
        }
    }
}
