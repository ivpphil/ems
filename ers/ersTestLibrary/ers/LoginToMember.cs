﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Remote;
using jp.co.ivp.ers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ersLibraryTest.Site.ers.member;
using OpenQA.Selenium.Support.PageObjects;
using ersTestLibrary.common;

namespace ersTestLibrary.Site.ers
{
    public class LoginToMember
    {
        /// <summary>
        /// Login to member page.
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="email"></param>
        /// <param name="passwd"></param>
        public static void Login(IWebDriver driver, string email, string passwd)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var wait = CommonVariables.GetDefaultWait(driver);
            var pageEnd = CommonVariables.PageEndFront;

            var url = setup.pc_sec_url + "top/member/asp/";
            driver.Url = url;
            wait.Until(ExpectedConditions.ElementExists(pageEnd));

            var login = new login();
            PageFactory.InitElements(driver, login);

            login.email.SendKeys(email);
            login.passwd.SendKeys(passwd);
            login.submit.Click();

            wait.Until(ExpectedConditions.ElementExists(pageEnd));
        }
    }
}
