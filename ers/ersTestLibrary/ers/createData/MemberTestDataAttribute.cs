﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using jp.co.ivp.ers;
using System.Threading;
using jp.co.ivp.ers.member;

namespace ersTestLibrary.Site.ers.createData
{
    /// <summary>
    /// Create test data that is used in each test case.
    /// When you want to write "if" sentence in this class is when you should create another ActionAttribute
    /// </summary>
    public class MemberTestDataAttribute
        : Attribute, ITestAction
    {
        /// <summary>
        /// ThreadStatic attribute is needed to avoid affection against another test cases.
        /// </summary>
        [ThreadStatic]
        public static ErsMember objMember;

        public ActionTargets Targets
        {
            //Suite = 
            get { return ActionTargets.Suite; }
        }

        public void BeforeTest(TestDetails testDetails)
        {
            // Please create test data that is used at all test in this class if you need.
            // You can use ErsRepository and ErsRepositoryEntity.

            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            var objMember = ErsFactory.ersMemberFactory.GetErsMember();
            objMember.lname = "てすと";
            objMember.fname = "太郎";
            objMember.lnamek = "テスト";
            objMember.fnamek = "タロウ";
            objMember.email = "test-member" + DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + Thread.CurrentThread.ManagedThreadId +"@ivp.co.jp";
            objMember.passwd = "aaaaaaaa";
            repository.Insert(objMember, true);
            MemberTestDataAttribute.objMember = objMember;
        }

        public void AfterTest(TestDetails testDetails)
        {
            // Please delete test data is created at BeforeTest().
            var repository = ErsFactory.ersMemberFactory.GetErsMemberRepository();
            repository.Delete(MemberTestDataAttribute.objMember);
            MemberTestDataAttribute.objMember = null;
        }
    }
}
