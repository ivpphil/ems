﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using jp.co.ivp.ers.order;
using jp.co.ivp.ers;
using jp.co.ivp.ers.merchandise;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.basket;
using jp.co.ivp.ers.mvc;
using ersTestLibrary.Site.ers.createData;

namespace ersLibraryTest.Site.ers.createData
{
    /// <summary>
    /// Create test data that is used in each test case.
    /// When you want to write "if" sentence in this class is when you should create another ActionAttribute
    /// </summary>
    public class OrderTestDataAttribute
        : Attribute, ITestAction
    {
        [ThreadStatic]
        public static ErsOrder objOrder;

        [ThreadStatic]
        public static IDictionary<string, ErsOrderRecord> objOrderRecords;

        public ActionTargets Targets
        {
            get { return ActionTargets.Test; }
        }

        public void BeforeTest(TestDetails testDetails)
        {
            // Please create test data that is used at all test in this class if you need.
            // You can use ErsRepository and ErsRepositoryEntity.
            var objOrderContainer = ErsFactory.ersOrderFactory.GetErsOrderContainer();
            var objBasket = ErsFactory.ersBasketFactory.GetErsBasket();

            //New Instance Object for Order Container Properties
            objOrderContainer.OrderHeader = ErsFactory.ersOrderFactory.GetErsOrder();
            objOrderContainer.OrderRecords = new Dictionary<string, ErsOrderRecord>();

            //Get Basket Merchandise
            var merchandise = this.GetErsMerchandiseTest(objBasket);

            if (merchandise != null)
            {
                //Overwrite the Member record and Merchandise record
                objOrderContainer.OrderHeader.OverwriteWithModel(MemberTestDataAttribute.objMember);
                objOrderContainer.OrderHeader.OverwriteWithModel(merchandise);

                //Get Order Records
                objOrderContainer.OrderRecords = ErsFactory.ersOrderFactory.GetErsOrderRecordList(objOrderContainer.OrderHeader.d_no, objBasket, null, null);

                //Calculate the MemberOrder
                ErsFactory.ersOrderFactory.GetErsCalcService().calcOrder(objOrderContainer.OrderHeader, objOrderContainer.OrderRecords.Values, objOrderContainer.OrderHeader.p_service, objBasket.carriageFree);

                //Insert Order Records
                using (var transaction = ErsDB_parent.BeginTransaction())
                {
                    this.Insert(objOrderContainer.OrderHeader, objOrderContainer.OrderRecords);
                    transaction.Commit();
                }
            }

            OrderTestDataAttribute.objOrder = objOrderContainer.OrderHeader;
            OrderTestDataAttribute.objOrderRecords = objOrderContainer.OrderRecords;
        }

        private void Insert(ErsOrder order, IDictionary<string, ErsOrderRecord> orderRecords)
        {
            // Please create test data that is used at all test in this class if you need.
            // You can use ErsRepository and ErsRepositoryEntity.
            var d_repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
            var ds_repository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();

            ErsFactory.ersOrderFactory.GetSetD_noStgy().SetNext(order, null);

            order.order_payment_status = EnumOrderPaymentStatusType.NOT_PAID;

            //Set Some Not Null Constraint at database
            this.SetTemporayNotNullDetail(order, orderRecords);

            d_repository.Insert(order, true);
            ds_repository.Insert(orderRecords.Values.First(), true);

        }

        /// <summary>
        /// Set fields that has not null constraint at database
        /// </summary>
        /// <param name="order"></param>
        /// <param name="orderRecords"></param>
        private void SetTemporayNotNullDetail(ErsOrder order, IDictionary<string, ErsOrderRecord> orderRecords)
        {
            int sequence_subd_no = 0;
            order.address = "temporary_address";
            order.tel = "temporary_tel";
            order.intime = DateTime.Now;

            order.base_d_no = order.d_no;
            var d_no = order.d_no + "-" + (sequence_subd_no.ToString("D2"));

            ErsFactory.ersOrderFactory.GetSetD_noStgy().Set(order, orderRecords.Values, d_no);
        }

        protected ErsBaskRecord GetErsMerchandiseTest(ErsBasket objBasket)
        {
            var merchandiseDic = this.GetErsMerchandiseTestAsDictionary();

            if (merchandiseDic == null)
                return null;

            if (merchandiseDic.ContainsKey("id"))
                merchandiseDic.Remove("id");

            var objBasketRecord = ErsFactory.ersBasketFactory.GetErsBaskRecordWithParameter(merchandiseDic);
            objBasketRecord.ransu = ErsFactory.ersSessionStateFactory.GetObtainRansuStgy().CreateNewRansu();
            objBasketRecord.intime = null;

            objBasketRecord.send_ptn = EnumSendPtn.NORMAL;
            objBasket.AddToBasket(objBasketRecord, 1);

            return objBasketRecord;
        }

        protected Dictionary<string, object> GetErsMerchandiseTestAsDictionary()
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();

            var criteria = ErsFactory.ersMerchandiseFactory.GetErsGroupCriteria();
            var repository = ErsFactory.ersMerchandiseFactory.GetErsGroupRepository();

            //▼Default Criteria that Needed on Merchandise
            //除外するgcode
            criteria.ignore_gcode = setup.IgnoreGcode;
            criteria.disp_list_flg = EnumDisp_list_flg.Visible;
            criteria.SetActiveOnly(DateTime.Now);
            criteria.doc_bundling_flg_not_equal = EnumDocBundlingFlg.ON;
            
            criteria.SetActiveOnly(DateTime.Now);
            //▲Default Criteria that Needed on Merchandise

            if (repository.GetRecordCountGroupBase(criteria) > 1)
            {
                //Get Random Merchandise
                criteria.LIMIT = 1;
                criteria.AddOrderBy("RANDOM()", Criteria.OrderBy.ORDER_BY_ASC);

                var merchandise = repository.FindGroupBaseItemList(criteria).First();

                if (ErsFactory.ersMerchandiseFactory.GetOnCampaignSpecification().IsSatisfiedBy(merchandise.p_date_from, merchandise.p_date_to))
                {
                    merchandise.price = merchandise.sale_price;
                }

                return merchandise.GetPropertiesAsDictionary();
            }

            return null;
        }

        public static EnumFrontOrderStatus? GetFrontOrderStatus()
        {
            EnumFrontOrderStatus? front_status = null;
            if (OrderTestDataAttribute.objOrderRecords != null)
            {
                var order_status = ErsFactory.ersOrderFactory.GetObtainErsOrderStatusStgy().ObtainFrom(OrderTestDataAttribute.objOrderRecords.Values);
                switch (order_status)
                {
                    case EnumOrderStatusType.NEW_ORDER:
                    case EnumOrderStatusType.DELIVER_WAITING:
                    case EnumOrderStatusType.DELIVER_REQUEST:
                        front_status = EnumFrontOrderStatus.NEW_ORDER;
                        break;
                    case EnumOrderStatusType.DELIVERED:
                        front_status = EnumFrontOrderStatus.DELIVERED;
                        break;
                    case EnumOrderStatusType.CANCELED_AFTER_DELIVER:
                    case EnumOrderStatusType.CANCELED:
                        front_status = EnumFrontOrderStatus.CANCELED;
                        break;
                    default:
                        throw new Exception("undefined order status");
                }
            }

            return front_status;
        }

        public void AfterTest(TestDetails testDetails)
        {
            using (var transaction = ErsDB_parent.BeginTransaction())
            {
                // Please delete test data is created at BeforeTest().
                var d_repository = ErsFactory.ersOrderFactory.GetErsOrderRepository();
                var ds_repository = ErsFactory.ersOrderFactory.GetErsOrderRecordRepository();

                d_repository.Delete(OrderTestDataAttribute.objOrder);
                ds_repository.Delete(OrderTestDataAttribute.objOrderRecords.Values.First());

                transaction.Commit();
            }
            
        }
    }
}
