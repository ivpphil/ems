﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using jp.co.ivp.ers;
using jp.co.ivp.ers.member;
using ersLibraryTest.Site.ers.createData;

namespace ersTestLibrary.Site.ers.createData
{
    /// <summary>
    /// Create test data that is used in each test case.
    /// When you want to write "if" sentence in this class is when you should create another ActionAttribute
    /// </summary>
    public class AddressTestDataAttribute
        : Attribute, ITestAction
    {
        public AddressTestDataAttribute() 
        { 
            this.countData = 1; 
        }

        //Set Number of Insert Test Data
        public int countData { get; set; }
        public string[] non_test_data { get; set; }
        public string[] single_list { get; set; }

        [ThreadStatic]
        public static ErsAddressInfo objAddress;

        [ThreadStatic]
        public static List<ErsAddressInfo> objAddressList;

        public ActionTargets Targets
        {
            get { return ActionTargets.Test; }
        }

        public void BeforeTest(TestDetails testDetails)
        {
            var test_count_data = this.GetCountTestData(testDetails);

            if (!this.IsNonTestData(testDetails))
            {
                this.InsertTestData(test_count_data);

                AddressTestDataAttribute.objAddress = AddressTestDataAttribute.objAddressList.First();
            }
        }

        public virtual void InsertTestData(int countData)
        {
            var list = new List<ErsAddressInfo>();

            foreach (var count in Enumerable.Range(0, countData))
            {
                // Please create test data that is used at all test in this class if you need.
                // You can use ErsRepository and ErsRepositoryEntity.
                var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();
                var objAddress = ErsFactory.ersAddressInfoFactory.GetErsAddressInfo();
                objAddress.mcode = MemberTestDataAttribute.objMember.mcode;
                objAddress.address_name = "別お届け先テスト" + "＿" + count;
                objAddress.add_lname = "別お届け先てすと";
                objAddress.add_fname = "別お届け先太郎";
                objAddress.add_lnamek = "ベツオトドケサキテスト";
                objAddress.add_fnamek = "ベツオトドケサキタロウ";
                objAddress.add_tel = "11111111111";
                objAddress.add_fax = "22222222222";
                objAddress.add_zip = "150-0002";
                objAddress.add_pref = 13;
                objAddress.add_address = "渋谷区";
                objAddress.add_taddress = "渋谷2-12-19";
                objAddress.add_maddress = "東建インターナショナルビル別館8F";
                repository.Insert(objAddress, true);
                list.Add(objAddress);
            }

            AddressTestDataAttribute.objAddressList = list;
        }

        public void AfterTest(TestDetails testDetails)
        {
            if (!this.IsNonTestData(testDetails))
            {
                // Please delete test data is created at BeforeTest().
                var repository = ErsFactory.ersAddressInfoFactory.GetErsAddressInfoRepository();

                foreach (var objAddress in AddressTestDataAttribute.objAddressList)
                {
                    repository.Delete(objAddress);
                }
            }
        }

        protected bool IsNonTestData(TestDetails testDetails)
        {
            if (this.non_test_data != null)
            {
                if (this.non_test_data.Contains(testDetails.Method.Name))
                {
                    return true;
                }
            }

            return false;
        }

        protected int GetCountTestData(TestDetails testDetails)
        {
            if (this.single_list != null)
            {
                if (this.single_list.Contains(testDetails.Method.Name))
                    return 1;
            }

            return this.countData;
        }
    }
}
