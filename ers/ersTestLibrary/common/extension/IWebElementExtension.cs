﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ersTestLibrary.common.extension
{
    public static class IWebElementExtension
    {
        /// <summary>
        /// Get value of attribute of this element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static string GetValue(this IWebElement element)
        {
            if (element == null)
            {
                throw new Exception("element is null");
            }

            return element.GetAttribute("value");
        }

        /// <summary>
        /// Get value of selected value of select
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static string GetSelectedValue(this IWebElement element)
        {
            if (element == null)
            {
                throw new Exception("element is null");
            }

            return new SelectElement( element).SelectedOption.GetValue();
        }
    }
}
