﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using System.Collections.ObjectModel;
using jp.co.ivp.ers;

namespace ersTestLibrary.common.extension
{
    public static class IWebDriverExtension
    {
        public static string GetPopupedWindowHandle(this IWebDriver driver, string existingWindowHandle)
        {
            int stopCounter = 0;
            while (true)
            {
                stopCounter++;

                //wait for new window to open 
                driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5L));

                ReadOnlyCollection<string> windowHandles = driver.WindowHandles;

                foreach (string handle in windowHandles)
                {
                    if (handle != existingWindowHandle)
                    {
                        return handle;
                    }
                }

                if (stopCounter == 2)
                {
                    return null;
                }
            }
        }
    }
}
