﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers;
using jp.co.ivp.ers.db;
using OpenQA.Selenium;

namespace ersTestLibrary.common
{
    public class PagerCommon
    {
        public virtual long maxItemCount { get { return ErsFactory.ersUtilityFactory.getSetup().DefaultItemNumberOnPage; } }

        public virtual string pager_button_xpath { get { return "//input[@type='button' and @class='place' and @value='{0}']"; } }

        public virtual long CountAllPage(long recordCount)
        {
            return this.CountAllPage(recordCount, this.maxItemCount);
        }

        public virtual long CountAllPage(long recordCount, long maxItemCount)
        {
            long pageall = recordCount / maxItemCount;
            if (recordCount % maxItemCount > 0)
            {
                pageall += 1;	// 余りは切り上げ, for remainder add another page
            }
            return pageall;
        }

        public virtual void SetLimitAndOffsetToCriteria(int pageCnt, ICriteria criteria)
        {
            this.SetLimitAndOffsetToCriteria(pageCnt, this.maxItemCount, criteria);
        }

        public virtual void SetLimitAndOffsetToCriteria(int pageCnt, long maxItemCount, ICriteria criteria)
        {
            criteria.LIMIT = maxItemCount;
            criteria.OFFSET = (pageCnt - 1) * maxItemCount;
        }

        public By ByPagerValue(int value)
        {
            return By.XPath(String.Format(pager_button_xpath, value));
        }
    }
}
