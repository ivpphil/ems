﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;
using jp.co.ivp.ers.util;
using System.Configuration;

namespace ersTestLibrary.common
{
    /// <summary>
    /// Values that is used in all test.
    /// </summary>
    public class CommonVariables
    {
        public static string DownloadFilePath { get { return ErsCommonContext.MapPath(ConfigurationManager.AppSettings["downloadFilePath"]); } }
        public static string UploadFilePath { get { return ErsCommonContext.MapPath(ConfigurationManager.AppSettings["uploadFilePath"]); } }

        /// <summary>
        /// Drivers used on selenium
        /// </summary>
        public static object[] Drivers = {
            new object[] { EnumBrowser.InternetExplorer, new Func<IWebDriver>(GetInternetExplorerDriver) },
            new object[] { EnumBrowser.Firefox, new Func<IWebDriver>(GetFirefoxDriver) },
            new object[] { EnumBrowser.Chrome, new Func<IWebDriver>(GetChromeDriver) }
        };

        /// <summary>
        /// Drivers used on selenium for download file
        /// </summary>
        public static object[] DownloadDrivers = {
            new object[] { EnumBrowser.Firefox, new Func<IWebDriver>(GetFirefoxDriver) }
        };

        /// <summary>
        /// Tags determin if the page is roaded completely.
        /// </summary>
        public static OpenQA.Selenium.By PageEndFront = By.TagName("footer");
        public static OpenQA.Selenium.By PageEndAdmin = By.Id("page_top");

        /// <summary>
        /// Get wait for browser.
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static WebDriverWait GetDefaultWait(IWebDriver driver)
        {
            return new WebDriverWait(driver, new TimeSpan(0, 0, 10));//10 seconds
        }

        public static InternetExplorerDriver GetInternetExplorerDriver()
        {
            return new InternetExplorerDriver(ErsCommonContext.MapPath(ConfigurationManager.AppSettings["DriverPath"]));
        }

        public static FirefoxDriver GetFirefoxDriver()
        {
            var profile = new FirefoxProfile { EnableNativeEvents = true };
            profile.SetPreference("browser.download.dir", DownloadFilePath);
            profile.SetPreference("browser.download.folderList", 2); 
            profile.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv");
            return new FirefoxDriver(profile);
        }

        public static ChromeDriver GetChromeDriver()
        {
            return new ChromeDriver(ErsCommonContext.MapPath(ConfigurationManager.AppSettings["DriverPath"]));
        }
    }
}
