﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using jp.co.ivp.ers.batch;
using ersLibraryTest.jp.co.jp.libraryTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ersTestLibrary.common
{
    public class ErsTestCommon
        : TestCommon
    {
        public object[] Drivers = CommonVariables.Drivers;
        public object[] DownloadDrivers = CommonVariables.DownloadDrivers;

        protected virtual void WaitUntilIntoNextPage(IWebDriver driver, WebDriverWait wait, string nextDestination, By pageEnd = null)
        {
            var result = wait.Until<IWebDriver>((d) =>
            {
                if (driver.Url == nextDestination)
                {
                    return driver;
                }

                return null;
            });

            if (pageEnd != null)
                wait.Until(ExpectedConditions.ElementExists(pageEnd));
        }

        protected virtual void WaitUntilIntoContainsNextPage(IWebDriver driver, WebDriverWait wait, string nextDestination, By pageEnd = null)
        {
            var result = wait.Until<IWebDriver>((d) =>
            {
                if (driver.Url.Contains(nextDestination))
                {
                    return driver;
                }

                return null;
            });

            if (pageEnd != null)
                wait.Until(ExpectedConditions.ElementExists(pageEnd));
        }
    }
}
