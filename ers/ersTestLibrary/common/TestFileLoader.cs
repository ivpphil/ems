﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using jp.co.ivp.ers;

namespace ersTestLibrary.common
{
    public class TestFileLoader
        : IDisposable
    {
        private TestFileLoader() { }

        /// <summary>
        /// Begin observation of downloaded file. please call this function before you click download button.
        /// </summary>
        /// <param name="browserName"></param>
        /// <returns></returns>
        public static TestFileLoader Begin(EnumBrowser browserName)
        {
            if (CommonVariables.DownloadDrivers.Count((obj) => ((EnumBrowser)((object[])obj)[0]) == browserName) == 0)
            {
                throw new Exception("[Invalid testcase configuration] " + browserName + " doesn't support downloading test.");
            }

            return new TestFileLoader();
        }

        private string fileName;

        /// <summary>
        /// Get downloaded file name.
        /// </summary>
        /// <returns></returns>
        public string GetDownloadedFile()
        {
            var downloadPath = CommonVariables.DownloadFilePath;
            for (var i = 0; i < 10; i++)
            {
                var fileNames = Directory.GetFiles(downloadPath);

                if (fileNames.Count() == 0)
                {
                    Thread.Sleep(500);
                    continue;
                }

                this.fileName = fileNames.First();

                return this.fileName;
            }

            throw new Exception("There is no downloaded file in " + downloadPath + ".");
        }

        public void Dispose()
        {
            if (!this.fileName.HasValue())
            {
                return;
            }

            File.Delete(this.fileName);
        }
    }
}
