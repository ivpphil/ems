﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace ersTestLibrary.ersAdmin.customer
{
    public class cus_search
    {
        [FindsBy(How = How.XPath, Using = "/html/body/div/div[3]/div[2]/form/div[2]/p/input")]
        public IWebElement submit { get; set; }
    }
}
