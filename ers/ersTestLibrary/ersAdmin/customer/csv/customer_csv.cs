﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers;


namespace ersTestLibrary.ersAdmin.customer.csv
{
    public class customer_csv
        : ErsBindableModel
    {
        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual int? id { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string mcode { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string email { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string lname { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string fname { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string lnamek { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string fnamek { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string compname { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string zip { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string disp_pref { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string address { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string taddress { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string maddress { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string birth { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string disp_sex { get; set; }

        public virtual EnumSex? sex { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string disp_job { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string tel { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string fax { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual int? sale { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string disp_m_flg { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string memo { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual DateTime? intime { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual DateTime? utime { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string disp_country { get; set; }

        [CsvField]
        [ErsUniversalValidation(type = CHK_TYPE.All)]
        public virtual string disp_age_code { get; set; }
    }
}
