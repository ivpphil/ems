﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace ersTestLibrary.ersAdmin.login
{
    public class login
    {
        [FindsBy(How = How.Id, Using = "user_login_id")]
        public IWebElement user_login_id { get; set; }

        [FindsBy(How = How.Id, Using = "passwd")]
        public IWebElement passwd { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div[2]/form/div[3]/p/span/input")]
        public IWebElement submit { get; set; }
    }
}
