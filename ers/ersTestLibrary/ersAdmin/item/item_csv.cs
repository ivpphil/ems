﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace ersTestLibrary.ersAdmin.item
{
    public class item_csv
    {
        [FindsBy(How = How.Id, Using = "csv_file")]
        public IWebElement csv_file { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div[3]/div[2]/form/div[2]/p/a/input")]
        public IWebElement submit_csv_file { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div[2]/div[3]/div/button")]
        public IWebElement submit_csv_file_dialog { get; set; }
    }
}
