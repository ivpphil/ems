﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using ersTestLibrary.common;
using jp.co.ivp.ers;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace ersTestLibrary.ersAdmin
{
    public class LoginToAdmin
    {
        /// <summary>
        /// Login to member page.
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="email"></param>
        /// <param name="passwd"></param>
        public static void Login(IWebDriver driver, string email, string passwd)
        {
            var setup = ErsFactory.ersUtilityFactory.getSetup();
            var wait = CommonVariables.GetDefaultWait(driver);
            var pageEnd = CommonVariables.PageEndAdmin;

            var url = setup.admin_sec_url + "top/login/asp/login.asp";
            driver.Url = url;
            wait.Until(ExpectedConditions.ElementExists(pageEnd));

            var index = new ersTestLibrary.ersAdmin.login.login();
            PageFactory.InitElements(driver, index);

            index.user_login_id.SendKeys(email);
            index.passwd.SendKeys(passwd);
            index.submit.Click();

            wait.Until(ExpectedConditions.ElementExists(pageEnd));
        }
    }
}
