﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace ersTestLibrary.ersAdmin.home
{
    public class index
    {
        [FindsBy(How = How.XPath, Using = "/html/body/div/div[3]/div/div/ul/li[2]/a")]
        public IWebElement customer_link { get; set; }
    }
}
