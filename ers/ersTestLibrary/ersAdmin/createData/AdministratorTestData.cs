﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using jp.co.ivp.ers.administrator;
using jp.co.ivp.ers;
using System.Threading;

namespace ersTestLibrary.ersAdmin.createData
{
    public class AdministratorTestData
        : Attribute, ITestAction
    {
        /// <summary>
        /// ThreadStatic attribute is needed to avoid affection against another test cases.
        /// </summary>
        [ThreadStatic]
        public static ErsAdministrator objAdministrator;

        public ActionTargets Targets
        {
            //Suite = 
            get { return ActionTargets.Suite; }
        }


        public void BeforeTest(TestDetails testDetails)
        {
            // Please create test data that is used at all test in this class if you need.
            // You can use ErsRepository and ErsRepositoryEntity.

            var repository = ErsFactory.ersAdministratorFactory.GetErsAdministratorRepository();
            var objAdministrator = ErsFactory.ersAdministratorFactory.GetErsAdministrator();
            objAdministrator.user_cd = DateTime.Now.ToString("yyyyMMddHHmmss") + Thread.CurrentThread.ManagedThreadId;
            objAdministrator.user_login_id = objAdministrator.user_cd;
            objAdministrator.user_name = "テスト管理者";
            objAdministrator.passwd = "ivpers";
            objAdministrator.pass_date = DateTime.Now;
            objAdministrator.role_gcode = "ADMIN";
            objAdministrator.agent_id = 1;
            repository.Insert(objAdministrator, true);
            AdministratorTestData.objAdministrator = objAdministrator;
        }

        public void AfterTest(TestDetails testDetails)
        {
            // Please delete test data is created at BeforeTest().
            var repository = ErsFactory.ersAdministratorFactory.GetErsAdministratorRepository();
            repository.Delete(AdministratorTestData.objAdministrator);
            AdministratorTestData.objAdministrator = null;
        }
    }
}
