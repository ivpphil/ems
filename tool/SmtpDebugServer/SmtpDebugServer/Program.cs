﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ErsDebugServer
{
    class Program
    {
        static void Main(string[] args)
        {

            Smtp smtp = new Smtp();

            string command = null;
            int? status = null;

            if (args.Length > 1)
            {
                if (args.Length != 2)
                {
                    throw new Exception("please execute like \"***.exe helo -1\"");
                }

                command = args[0].ToLower().Trim();

                int tempStatus;
                if (!Int32.TryParse(args[1], out tempStatus))
                {
                    throw new Exception("please execute like \"***.exe helo -1\"");
                }
                status = tempStatus;
            }

            smtp.StartListen(command, status);

            string input = Console.ReadLine();
        }
    }
}
