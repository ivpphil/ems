﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace ErsDebugServer
{
    public class SendMsg
    {
        /// <summary>
        /// send msg to the server
        /// </summary>
        /// <param name="msg"></param>
        public string Send(string msg, NetworkStream nw)
        {
            WriteToStream(ref nw, msg.ToString() + "\r\n");
            string result = ReadFromStream(ref nw, msg);

            return result;
        }

        /// <summary>
        /// write the msg into stream
        /// </summary>
        /// <param name="nw"></param>
        /// <param name="line"></param>
        internal void WriteToStream(ref NetworkStream nw, string line)
        {
            try
            {
                //Encoding _encode = System.Text.Encoding.GetEncoding("iso-2022-jp");
                byte[] arrToSend = Encoding.ASCII.GetBytes(line); //_encode.GetBytes(line); 
                nw.Write(arrToSend, 0, arrToSend.Length);
            }
            catch (System.Exception e)
            {
                //LogMessage(Convert.ToString(Thread.CurrentThread.ManagedThreadId), e.ToString(), this.errlogpath, 1);
                throw new Exception("Write to Stream threw an System.Exception" + e.ToString());
            }
        }

        /// <summary>
        /// read the stream response from server
        /// </summary>
        /// <param name="nw"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal string ReadFromStream(ref NetworkStream nw, string msg)
        {
            try
            {
                byte[] readBuffer = new byte[4096];

                int length = nw.Read(readBuffer, 0, readBuffer.Length);
                string returnMsg = Encoding.ASCII.GetString(readBuffer, 0, length);

                return returnMsg;
            }
            catch (System.Exception e)
            {
                throw new Exception("Read from Stream threw an System.Exception: " + e.ToString());
            }
        }
    }
}
