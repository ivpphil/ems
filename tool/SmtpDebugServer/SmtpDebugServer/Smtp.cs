﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Collections;
using System.Net;
using System.Threading;
using jp.co.ivp.ers.util;

namespace ErsDebugServer
{
    class Smtp
    {
        private ErsTcpServer tcpServer;
        private Encoding encoding;

        public string command;
        public int? status;

        internal void StartListen(string command, int? status)
        {
            this.command = command;
            this.status = status;

            this.tcpServer = new ErsTcpServer();

            this.encoding = Encoding.GetEncoding("iso-2022-jp");

            this.tcpServer.AcceptConnection(3333, 10, encoding, (state => this.Connected(state)));
        }

        private void Connected(ErsTcpConnection ersState)
        {
            //// Send a welcome message to client
            //string msg = "250 Welcome client \n";
            //ersState.Send(msg, null);

            // Let the worker Socket do the further processing for the 
            // just connected client
            ersState.Receive((state) => this.OnDataReceived(state));
        }

        // This the call back function which will be invoked when the socket
        // detects any client writing of data on the stream
        private void OnDataReceived(ErsTcpConnection state)
        {
            try
            {
                state.CheckResult();

                var message = state.GetReceivedMessage();
                Console.WriteLine("message:" + message);

                if (!string.IsNullOrEmpty(message) && !string.IsNullOrEmpty(command) && message.ToLower().Trim().StartsWith(command))
                {
                    if (status != -1)
                    {
                        state.Send(string.Format("{0} custom error\r\n", status), null);
                    }
                    return;
                }

                state.Send("250 ok\r\n", null);

                // Let the worker Socket do the further processing for the 
                // just connected client
                state.Receive((ersState) => this.OnDataReceived(ersState));
            }
            catch (SocketException e)
            {
                if (e.SocketErrorCode != SocketError.ConnectionAborted
                    && e.SocketErrorCode != SocketError.ConnectionReset)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
