﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace ers
{
    public partial class PerformanceTest : System.Web.UI.Page
    {
        /// <summary>
        /// 各環境ごとに設定を書き換えてください
        /// </summary>
        private void Setup()
        {
            ///ers/ers/setup.configの以下の値を調整する
            //onVEX = true > メール送信しないため

            //セキュアページURL
            base_secure_url = "https://ersv65.local.host:4443/";

            //商品コード
            scode = "1";
        }

        /// <summary>
        /// entry4に渡す値を設定してください
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="ransu"></param>
        /// <returns></returns>
        private static Dictionary<string, string> GetPurchaseData(string ransu)
        {
            var dic = new Dictionary<string, string>();
            dic["erssession_ransu"] = ransu;
            dic["k_flg"] = "3";
            dic["lname"] = "IVP";
            dic["fname"] = "てすと";
            dic["lnamek"] = "アイブイピー";
            dic["fnamek"] = "ナガイケ";
            dic["zip"] = "111-1111";
            dic["pref"] = "1";
            dic["address"] = "aaaa";
            dic["taddress"] = "神戸市中央区磯辺通2";
            dic["tel"] = "1111111111";
            dic["email"] = "nagaike@ivp.co.jp";
            dic["email_confirm"] = "nagaike@ivp.co.jp";
            dic["mformat"] = "1";
            dic["m_flg"] = "1";
            dic["pri_chk"] = "1";
            dic["pay"] = "4";
            dic["send"] = "1";
            return dic;
        }


        string base_secure_url;
        string scode;

        protected void Page_Load(object sender, EventArgs e)
        {
            Setup();

            var cart_URL = base_secure_url + "top/detail/asp/cart.asp";
            var entry4_URL = base_secure_url + "top/register/asp/entry4.asp";

            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(OnRemoteCertificateValidationCallback);

            
            string result;
            var ransu = AddToCart(cart_URL);

            result = DoPurchase(entry4_URL, ransu);

            this.result.Text = result;
        }

        /// <summary>
        /// 購入実行
        /// </summary>
        /// <param name="entry4_URL"></param>
        /// <param name="ransu"></param>
        /// <returns></returns>
        private string DoPurchase(string entry4_URL, string ransu)
        {
            string result;
            var dic = GetPurchaseData(ransu);

            var postData = GetPostValue(dic);


            var objRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(entry4_URL);
            objRequest.Method = "POST";
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.ContentLength = Encoding.UTF8.GetByteCount(postData);

            //CookieContainerプロパティを設定する
            objRequest.CookieContainer = new System.Net.CookieContainer();
            var cookies = new System.Net.CookieContainer().GetCookies(objRequest.RequestUri);
            cookies.Add(new System.Net.Cookie("erssession_ransu", ransu));
            objRequest.CookieContainer.Add(objRequest.RequestUri, cookies);

            //送信
            using (System.IO.StreamWriter myWriter = new System.IO.StreamWriter(objRequest.GetRequestStream()))
            {
                myWriter.Write(postData);
            }
            using (System.Net.WebResponse response = objRequest.GetResponse())
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    result = sr.ReadToEnd();
                }
            }
            return result;
        }

        /// <summary>
        /// カートに商品を入れる
        /// </summary>
        /// <param name="cart_URL"></param>
        /// <returns></returns>
        private string AddToCart(string cart_URL)
        {
            var dic = new Dictionary<string, string>();
            dic["scode"] = scode;

            var postData = GetPostValue(dic);

            HttpWebRequest objRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(cart_URL);
            objRequest.Method = "POST";
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.ContentLength = Encoding.UTF8.GetByteCount(postData);

            //CookieContainerプロパティを設定する
            objRequest.CookieContainer = new System.Net.CookieContainer();
            //要求元のURIに関連したCookieを追加し、要求に使用する
            objRequest.CookieContainer.Add(objRequest.CookieContainer.GetCookies(objRequest.RequestUri));
            
            //送信
            var ransu = string.Empty;
            var result = string.Empty;
            using (System.IO.StreamWriter myWriter = new System.IO.StreamWriter(objRequest.GetRequestStream()))
            {
                myWriter.Write(postData);
            }

            using (System.Net.WebResponse response = objRequest.GetResponse())
            {
                var cookie = objRequest.CookieContainer.GetCookies(objRequest.RequestUri);
                var ransuCookie = cookie["erssession_ransu"];
                if (ransuCookie != null)
                {
                    ransu = ransuCookie.Value;
                }
                using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    result = sr.ReadToEnd();
                }
            }

            return ransu;
        }

        /// <summary>
        /// POSTする値
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private static string GetPostValue(Dictionary<string, string> dic)
        {
            var retVal = string.Empty;
            foreach (var key in dic.Keys)
            {
                var val = (dic[key] == null ? string.Empty : Convert.ToString(dic[key]));
                retVal += "&" + key + "=" + HttpUtility.UrlEncode(val, Encoding.UTF8);
            }
            return retVal.Substring(1);
        }

        // 信頼できないSSL証明書を「問題なし」にするメソッド
        private bool OnRemoteCertificateValidationCallback(
          Object sender,
          X509Certificate certificate,
          X509Chain chain,
          SslPolicyErrors sslPolicyErrors)
        {
            return true;  // 「SSL証明書の使用は問題なし」と示す
        }
    }
}