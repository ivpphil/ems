﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using System.Data;
using System.Data.Common;
using System.Linq.Expressions;

namespace jp.co.ivp.ers.mvc
{
    public abstract class ErsRepository
    {
        /// <summary>
        /// Execute select query with specified criteria
        /// </summary>
        public static List<Dictionary<string, object>> SelectSatisfying(ErsDatabase ersDatabase,ISpecificationForSQL spec, Criteria criteria = null)
        {

            string strSQL = spec.asSQL();

            var uni = ErsDB_universal.GetInstance(ersDatabase);

            return uni.ExecuteQuery(strSQL, criteria);

        }
		/// <summary>
		/// Execute select query with specified criteria
		/// </summary>
        public static List<Dictionary<string, object>> SelectSatisfying(ISpecificationForSQL spec, Criteria criteria = null)
        {

            string strSQL = spec.asSQL();

            var uni = ErsDB_universal.GetInstance();

            return uni.ExecuteQuery(strSQL, criteria);

        }

        /// <summary>
        /// Execute update query with specified criteria
        /// </summary>
        public static int UpdateSatisfying(ErsDatabase ersDatabase, ISpecificationForSQL spec, Criteria criteria = null)
        {

            string strSQL = spec.asSQL();

            var uni = ErsDB_universal.GetInstance(ersDatabase);

            return uni.ExecuteUpdate(strSQL, criteria);

        }

        /// <summary>
        /// Execute update query with specified criteria
        /// </summary>
        public static int UpdateSatisfying(ISpecificationForSQL spec, Criteria criteria = null)
        {

            string strSQL = spec.asSQL();

            var uni = ErsDB_universal.GetInstance();

            return uni.ExecuteUpdate(strSQL, criteria);

        }
    }

    public abstract class ErsRepository<T>
        : ErsRepository
        where T : IErsRepositoryEntity
    {
        protected ErsDB_parent ersDB_table;

        public ErsRepository()
        {
        }

        public ErsRepository(string tableName)
        {
            this.ersDB_table= new ErsDB_parent(tableName);
        }

        public ErsRepository(string tableName, ErsDatabase connection)
        {
            this.ersDB_table= new ErsDB_parent(tableName, connection);
        }

        public ErsRepository(ErsDB_parent ersDB_table)
        {
            this.ersDB_table= ersDB_table;
        }

        public virtual void Insert(T obj, bool storeNewIdToObject = false)
        {
            if (storeNewIdToObject == true)
            {
                obj.id = this.GetNextSequence();
            }
            ersDB_table.gInsert(obj.GetPropertiesAsDictionary(ersDB_table));
        }

        public virtual void Update(T old_obj, T new_obj)
        {
            var oldDic = old_obj.GetPropertiesAsDictionary(ersDB_table);
            var newDic = new_obj.GetPropertiesAsDictionary(ersDB_table);

            var changedColumns = ErsCommon.GetChangedKeys(oldDic, newDic);
            if (changedColumns.Length > 0)
                ersDB_table.gUpdateColumn(changedColumns, newDic, "WHERE id = '" + old_obj.id + "'");
        }

        public virtual void Delete(T obj)
        {
            var criteria = new Criteria();
            criteria.Add(Criteria.GetCriterion("id", obj.id, Criteria.Operation.EQUAL));
            this.Delete(criteria);
        }

        public virtual void Delete(Criteria criteria = null)
        {
            ersDB_table.gDelete(criteria);
        }

        public virtual IList<T> Find(Criteria criteria)
        {
            return this.Find(criteria, new[] { "*" });
        }

        public virtual IList<T> Find(Criteria criteria, IEnumerable<string> columns)
        {
            var objList = this.ersDB_table.gSelect(columns, criteria);

            return CreateList(objList);

        }

        /// <summary>
        /// 単一行を取得。複数行の場合はNULL
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public virtual T FindSingle(Criteria criteria)
        {
            return this.FindSingle(criteria, new[] { "*" });
        }

        /// <summary>
        /// 単一行を取得。複数行の場合はNULL
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public virtual T FindSingle(Criteria criteria, IEnumerable<string> columns)
        {
            var retList = this.Find(criteria, columns);

            if (retList.Count != 1)
            {
                return default(T);
            }

            return retList.First();

        }

        /// <summary>
        /// Dictionaryからデータを作成します
        /// </summary>
        /// <param name="funcInstantiation"></param>
        /// <param name="objList"></param>
        /// <returns></returns>
        protected IList<T> CreateList(List<Dictionary<string, object>> objList)
        {
            var retList = new List<T>();

            foreach (var recordData in objList)
            {
                var obj = ErsExpressionInstantiator<T>.GetInstantiation();
                obj.OverwriteWithParameter(recordData);
                retList.Add(obj);
            }

            return retList;
        }

        public virtual long GetRecordCount(db.Criteria criteria)
        {
            return this.GetRecordCount(criteria, "id");
        }

        public virtual long GetRecordCount(db.Criteria criteria, string column)
        {
            return ersDB_table.gSelectCount(column, criteria);
        }

        public virtual int GetCurrentSequence()
        {
            return this.ersDB_table.GetCurrentSequence();
        }

        public virtual int GetNextSequence()
        {
            return this.ersDB_table.GetNextSequence();
        }
    }
}
