﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.IO;
using System.Web.Mvc;
using System.Collections;

namespace jp.co.ivp.ers.util
{
    public class ErsTcpServer
        : IDisposable
    {
        private ErsTcpServerConnection serverState { get; set; }

        public ErsTcpServer()
        {
        }

        /// <summary>
        /// Accept connection from client
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="listenCount"></param>
        public virtual void AcceptConnection(int port, int listenCount, Encoding encoding, Action<ErsTcpConnection> connectedCallback)
        {
            this.serverState = new ErsTcpServerConnection(encoding);

            this.serverState.connectedCallback = connectedCallback;

            // Bind to local IP Address...
            this.serverState.Socket.Bind(new IPEndPoint(IPAddress.Any, port));
            // Start listening...
            this.serverState.Socket.Listen(listenCount);
            // Create the call back for any client connections...
            this.serverState.Socket.BeginAccept(new AsyncCallback(OnClientConnect), this.serverState);
        }

        /// <summary>
        /// This is the call back function, which will be invoked when a client is connected
        /// </summary>
        /// <param name="asyn"></param>
        private void OnClientConnect(IAsyncResult asyn)
        {
            var serverState = (ErsTcpServerConnection)asyn.AsyncState;

            // Here we complete/end the BeginAccept() asynchronous call
            // by calling EndAccept() - which returns the reference to
            // a new Socket object
            Socket workerSocket = serverState.Socket.EndAccept(asyn);

            var workerState = new ErsTcpConnection(serverState.encoding, workerSocket);
            serverState.workerStateList.Add(workerState);

            serverState.connectedCallback(workerState);

            // Since the main Socket is now free, it can go back and wait for
            // other clients who are attempting to connect
            serverState.Socket.BeginAccept(new AsyncCallback(OnClientConnect), serverState);
        }

        /// <summary>
        /// Implements for IDisposable
        /// </summary>
        public void Dispose()
        {
            if (this.serverState != null)
            {
                this.serverState.CloseSocket();
                this.serverState = null;
            }
        }
    }
}
