﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace jp.co.ivp.ers.util.HttpModule
{
    /// <summary>
    /// The http module that customize response header
    /// </summary>
    public class CustomHeaderModule
        : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.PreSendRequestHeaders += (sender, e) =>
                {
                    var response = ((HttpApplication)sender).Response;
                    response.Headers.Remove("Server");
                };
        }


        public void Dispose()
        {

        }
    }
}
