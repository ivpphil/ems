﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using StackExchange.Profiling;

namespace jp.co.ivp.ers.util.HttpModule
{
    /// <summary>
    /// HttpModule for MiniProfiler
    /// </summary>
    public class MiniProfilerStartupModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.BeginRequest += (sender, e) =>
            {
                var setup = new SetupConfigReader();

                if (!setup.EnableMiniProfiler)
                {
                    return;
                }

                var request = ((HttpApplication)sender).Request;

                //If proxy servers are used, HTTP_X_FORWARDED_FOR is set. 
                var forwardedFor = request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                var sourceIP = string.IsNullOrWhiteSpace(forwardedFor)
                                ? request.UserHostAddress
                                : forwardedFor.Split(',').Select(ip => ip.Trim()).First();

                if (request.IsLocal || setup.MiniProfilerAllowedIPs.Any(ip => ip == sourceIP))
                {
                    ErsCommonContext.SetPooledObject("_MiniProfilerStart", true);

                    MiniProfiler.Start();
                }

            };


            // TODO: You can control who sees the profiling information
            /*
            context.AuthenticateRequest += (sender, e) =>
            {
                if (!CurrentUserIsAllowedToSeeProfiler())
                {
                    StackExchange.Profiling.MiniProfiler.Stop(discardResults: true);
                }
            };
            */

            context.EndRequest += (sender, e) =>
            {
                MiniProfiler.Stop();
            };
        }

        public void Dispose() { }
    }
    
}
