﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.util
{
    public static class ErsProhibitedCharactersConverter
    {
        public static string Convert(string value)
        {
            if (!value.HasValue())
            {
                return value;
            }

            var setup = new SetupConfigReader();

            var prohibitionChars = setup.ProhibitionChars;
            var prohibitionCharsReplace = setup.ProhibitionCharsReplace;

            if(setup.debug && prohibitionChars.Count() != prohibitionCharsReplace.Count())
            {
                // デバッグ時のみチェックする
                throw new Exception("There is inconsistency of the count between setup.ProhibitionChars and setup.prohibitionCharsReplace");
            }

            var prohibitionCharsCount = prohibitionChars.Count();

            for (var index = 0; index < prohibitionCharsCount; index++)
            {
                value = value.Replace(prohibitionChars[index], prohibitionCharsReplace[index]);
            }

            return value;
        }
    }
}
