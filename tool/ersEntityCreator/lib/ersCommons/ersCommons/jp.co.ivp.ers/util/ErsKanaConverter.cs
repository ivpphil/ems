﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers.util;
using System.Data;
using System.Data.Common;
using System.Linq.Expressions;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.util
{
    public class ErsKanaConverter
    {
        /// <summary>
        /// キーワード変換処理
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public static string String_conversion(string keyword)
        {
            string keyword00 = string.Empty;
            keyword00 = ErsKanaConverter.Kana_em_conversion(keyword);
            keyword00 = ErsKanaConverter.Hiragana_conversion(keyword00);
            keyword00 = ErsKanaConverter.Alphanumeric_conversion(keyword00);

            //大文字→小文字
            keyword00 = keyword00.ToLower();
            return keyword00;
        }

        /// <summary>
        /// 半角カナを全角カナに変換
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public static string Kana_em_conversion(string keyword)
        {
            Regex re = new Regex("[ｦ-ﾟ]+");
            return re.Replace(keyword, Kana_em_conversion_sub);
        }
        
        /// <summary>
        /// 半角カナ→全角カナに変換用
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        static string Kana_em_conversion_sub(Match m)
        {
            return Strings.StrConv(m.Value, VbStrConv.Wide, 0);
        }

        /// <summary>
        /// カタカナ→ひらがな変換
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public static string Hiragana_conversion(string keyword)
        {
            return (Strings.StrConv(keyword, VbStrConv.Hiragana, 0x411));
        }

        /// <summary>
        /// 全角英数字→半角英数字に変換
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public static string Alphanumeric_conversion(string keyword)
        {
            Regex re = new Regex("[０-９Ａ-Ｚａ-ｚ]+");
            return re.Replace(keyword, Alphanumeric_conversion_sub);
        }

        /// <summary>
        /// 全角英数字→半角英数字に変換用
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        static string Alphanumeric_conversion_sub(Match m)
        {
            return Strings.StrConv(m.Value, VbStrConv.Narrow, 0);
        }


    }
}
