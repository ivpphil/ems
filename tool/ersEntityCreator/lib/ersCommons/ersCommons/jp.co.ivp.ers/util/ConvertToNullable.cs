﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.util
{
    public class ConvertToNullable
    {
        /// <summary>
        /// 文字列をint?に変換
        /// </summary>
        /// <param name="argRange"></param>
        /// <returns></returns>
        public static int? ParseToInt32(string argRange)
        {
            if (string.IsNullOrEmpty(argRange))
            {
                return null;
            }
            int returnValue;
            if (!Int32.TryParse(argRange, out returnValue))
            {
                return null;
            }

            return returnValue;
        }

        /// <summary>
        /// 文字列をint?に変換
        /// </summary>
        /// <param name="argRange"></param>
        /// <returns></returns>
        public static DateTime? ParseToDateTime(string argRange)
        {
            if (string.IsNullOrEmpty(argRange))
            {
                return null;
            }
            DateTime returnValue;
            if (!DateTime.TryParse(argRange, out returnValue))
            {
                return null;
            }

            return returnValue;
        }
    }
}
