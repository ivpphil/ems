﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Runtime.InteropServices;
using jp.co.ivp.ers.mvc;
using CryptV1;
using System.IO;

namespace jp.co.ivp.ers.util
{
    public class ErsEncryption
    {
        private EncodeCls encObj = new EncodeCls();
        private DecodeCls decObj = new DecodeCls();
        private bool enableEncryption;

        /// <summary>
        /// エラー回避用の文字列(５桁未満だと実行時エラーが出るため = を付加)
		/// <para>Append "=", because there's an error occur if it is less than five characters </para>
        /// </summary>
        protected const string Pad = "====";

        public ErsEncryption()
        {
            var configReader = new SetupConfigReader();
            this.enableEncryption = configReader.enableEncryption;
        }

        /// <summary>
        /// 暗号化
		/// <para>Encryption</para>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string HexEncode(string value)
        {
            if (this.enableEncryption)
            {
                return encObj.encode(ErsEncryption.Pad + value);
            }
            else
            {
                return value;
            }
        }


        /// <summary>
        /// 複合化
		/// <para>Decryption</para>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string HexDecode(string value)
        {
            //return value if the value is null or empty
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            //encription
            if (this.enableEncryption)
            {
                try
                {
                    var padLength = ErsEncryption.Pad.Length;
                    var resultValue = decObj.decode(value);
                    if (!string.IsNullOrEmpty(resultValue) && resultValue.Length >= padLength)
                    {
                        resultValue = resultValue.Substring(padLength);
                    }
                    return resultValue;
                }
                catch (Exception)
                {
                    throw new ErsException("10211");
                }
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// 伝票・会員バックアップ
		/// <para>Backup documents and membership</para>
        /// </summary>
        /// <param name="textBody"></param>
        /// <param name="FilePathOut"></param>
        /// <returns></returns>
        public bool EncodeBuckup(string textBody, string FilePathOut)
        {
            if (this.enableEncryption)
            {
                var setup = new SetupConfigReader();
                var username = setup.logFileUserName;
                var password = setup.logFileUserPassword;
                var doChangeAuth = !string.IsNullOrEmpty(username);

                //権限の偽装
                using (var changelogin = ChangeLogonUserHelper.BeginChange(username, password, doChangeAuth))
                {
                    var srcEnc = Convert.ToBase64String(ErsEncoding.ShiftJIS.GetBytes(textBody));
                    var encValue = this.HexEncode(srcEnc);

                    ErsFile.WriteAll(encValue + "\r\n", FilePathOut, ErsEncoding.ShiftJIS);
                }
                return true;
            }
            else
            {
                return true;
            }
        }
    }
}
