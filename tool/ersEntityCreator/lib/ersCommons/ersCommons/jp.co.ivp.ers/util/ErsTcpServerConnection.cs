﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Collections;

namespace jp.co.ivp.ers.util
{
    public class ErsTcpServerConnection
    {
        public Socket Socket { get; protected set; }

        public Encoding encoding { get; protected set; }

        public Action<ErsTcpConnection> connectedCallback { get; set; }

        // An ArrayList is used to keep track of worker sockets that are designed
        // to communicate with each connected client. Make it a synchronized ArrayList
        // For thread safety
        public ArrayList workerStateList { get; private set; }

        public ErsTcpServerConnection(Encoding encoding)
        {
            this.encoding = encoding;
            this.Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.workerStateList = ArrayList.Synchronized(new ArrayList());
        }

        internal void CloseSocket()
        {
            if (this.Socket != null)
            {
                // Release the socket.
                this.Socket.Shutdown(SocketShutdown.Both);
                this.Socket.Close();
                this.Socket = null;
            }

            foreach (var client in this.workerStateList)
            {
                ((ErsTcpConnection)client).CloseSocket();
            }
        }
    }
}
