﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;


namespace jp.co.ivp.ers.util
{
    public class ErsFile
    {

        static private Encoding _encode = System.Text.Encoding.UTF8;

        /// <summary>
        /// ファイルに書き込む
        /// <para>Write to file</para>
        /// </summary>
        /// <param name="strContents">内容</param>
        /// <param name="strPath">ファイルパス</param>
        /// <param name="blnAppend">追加するか上書きするか</param>
        /// <param name="encode">文字エンコード</param>
        /// <returns></returns>
        static public bool WriteAll(string strContents, string strPath, Encoding encode = null)
        {
            if (encode == null)
            {
                encode = _encode;
            }

            var autoResetEvent = new AutoResetEvent(false);

            var count = 0;//現在リトライ数
            var stopCount = 20;//最大リトライ数
            var retryInterval = 500;//リトライまでの待機時間(ミリ秒)

            while (true)
            {
                try
                {
                    try
                    {
                        using (var sw = new System.IO.StreamWriter(strPath, true, encode))
                        {
                            sw.Write(strContents);
                            //閉じる
                            sw.Close();
                        }
                        break;
                    }
                    catch (DirectoryNotFoundException)
                    {
                        ErsDirectory.CreateDirectories(Path.GetDirectoryName(strPath));
                        throw;
                    }
                }
                catch (IOException e)
                {
                    //ロック解除を検知
                    var fileSystemWatcher = new FileSystemWatcher(Path.GetDirectoryName(strPath))
                    {
                        EnableRaisingEvents = true
                    };

                    fileSystemWatcher.Changed += (o, ev) =>
                    {
                        if (Path.GetFullPath(ev.FullPath) == Path.GetFullPath(strPath))
                        {
                            autoResetEvent.Set();
                        }
                    };

                    //ファイルが開放されるか、xミリ秒経過するまで待機
                    if (!autoResetEvent.WaitOne(retryInterval) && ++count == stopCount)
                    {
                        //リトライ回数を超えたらエラー
                        throw new IOException(string.Format("Error when writing file \r\npath:{0}\r\ncontents{1}\r\n", strPath, strContents), e);
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// ファイルに書き込む
        /// <para>Write to file</para>
        /// </summary>
        /// <param name="retryTextPath"></param>
        /// <param name="textBody"></param>
        /// <param name="enc"></param>
        /// <param name="r"></param>
        /// <param name="currentTime"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string WriteAllWithRandomName(string strPath, string extension, string strContents, Encoding encode = null)
        {
            if (encode == null)
            {
                encode = _encode;
            }

            var count = 0;//現在リトライ数
            var stopCount = 20;//最大リトライ数
            var retryInterval = 100;//リトライまでの待機時間(ミリ秒)

            Random r = new Random();
            var path = string.Empty;
            while (true)
            {
                try
                {
                    try
                    {
                        var strRandom = r.Next(1000000, 9999999).ToString();
                        path = strPath + DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + "-" + strRandom + "." + extension;
                        using (var fs = new FileStream(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.None))
                        {
                            using (var sw = new StreamWriter(fs, encode))
                            {
                                sw.Write(strContents);
                                sw.Close();
                            }
                        }

                        break;
                    }
                    catch (DirectoryNotFoundException)
                    {
                        ErsDirectory.CreateDirectories(Path.GetDirectoryName(strPath));
                        throw;
                    }
                }
                catch (IOException e)
                {
                    //ファイルが開放されるか、xミリ秒経過するまで待機
                    if (++count == stopCount)
                    {
                        //リトライ回数を超えたらエラー
                        throw new IOException(string.Format("Error when writing file \r\npath:{0}\r\ncontents{1}\r\n", strPath, strContents), e);
                    }

                    Thread.Sleep(retryInterval);
                }
            }
            return path;
        }

        /// <summary>
        /// ファイルに書き込む
        /// <para>Write to file</para>
        /// </summary>
        /// <param name="retryTextPath"></param>
        /// <param name="textBody"></param>
        /// <param name="enc"></param>
        /// <param name="r"></param>
        /// <param name="currentTime"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string WriteAllFromPathWithRandomName(string strToPath, string extension, string strFromPath, Encoding encode = null)
        {
            using (var fileStream = File.Open(strFromPath, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                return WriteAllWithRandomName(strToPath, extension, fileStream, encode);
            }
        }

        /// <summary>
        /// ファイルに書き込む
        /// <para>Write to file</para>
        /// </summary>
        /// <param name="retryTextPath"></param>
        /// <param name="textBody"></param>
        /// <param name="enc"></param>
        /// <param name="r"></param>
        /// <param name="currentTime"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string WriteAllWithRandomName(string strPath, string extension, Stream streamContents, Encoding encode = null)
        {
            if (encode == null)
            {
                encode = _encode;
            }

            var count = 0;//現在リトライ数
            var stopCount = 20;//最大リトライ数
            var retryInterval = 100;//リトライまでの待機時間(ミリ秒)

            Random r = new Random();
            var path = string.Empty;
            while (true)
            {
                try
                {
                    try
                    {
                        var strRandom = r.Next(1000000, 9999999).ToString();
                        path = strPath + DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + "-" + strRandom + "." + extension;
                        using (var toFs = new FileStream(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.None))
                        {
                            using (var sw = new StreamWriter(toFs, encode))
                            {
                                streamContents.CopyTo(sw.BaseStream);
                                sw.Close();
                            }
                        }

                        break;
                    }
                    catch (DirectoryNotFoundException)
                    {
                        ErsDirectory.CreateDirectories(Path.GetDirectoryName(strPath));
                        throw;
                    }
                }
                catch (IOException e)
                {
                    //ファイルが開放されるか、xミリ秒経過するまで待機
                    if (++count == stopCount)
                    {
                        //リトライ回数を超えたらエラー
                        throw new IOException(string.Format("Error when writing file \r\npath:{0}\r\nstream contents}\r\n", strPath), e);
                    }

                    Thread.Sleep(retryInterval);
                }
            }
            return path;
        }

        /// <summary>
        /// Identify if file is locked using System Error Code
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static bool IsFileLocked(Exception ex)
        {
            int ERROR_SHARING_VIOLATION = 32;
            int ERROR_LOCK_VIOLATION = 33;

            int errorCode = System.Runtime.InteropServices.Marshal.GetHRForException(ex) & ((1 << 16) - 1);

            return errorCode == ERROR_LOCK_VIOLATION || errorCode == ERROR_SHARING_VIOLATION;
        }
    }
}
