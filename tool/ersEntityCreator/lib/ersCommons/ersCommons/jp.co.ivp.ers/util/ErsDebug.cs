﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Text.RegularExpressions;
using System.Collections;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using System.Data;

namespace jp.co.ivp.ers.util
{
    public class ErsDebug
    {
        /// <summary>
        /// Writes a message to Systems.Diagnostics.Debug.Listeners collection
        /// </summary>
        /// <param name="message"></param>
        public static void WriteLine(string message)
        {
            var setup = new SetupConfigReader();
            if (!setup.debug)
                return;

            System.Diagnostics.Debug.WriteLine(message);
        }

        /// <summary>
		/// Writes an executing name and time to Systems.Diagnostics.Debug.Listeners collection
        /// </summary>
        /// <param name="executing_name"></param>
        public static void WriteExecutingLog(string executing_name)
        {
            var setup = new SetupConfigReader();
            if (!setup.debug)
                return;

            System.Diagnostics.Debug.WriteLine("----------[" + DateTime.Now.ToString("HH:mm:ss.fffffff") + "]" + executing_name + "----------");
        }

        /// <summary>
        /// WriteUpdatedValue
        /// </summary>
        /// <param name="key"></param>
        /// <param name="old_value"></param>
        /// <param name="new_value"></param>
        public static void WriteUpdatedValues(string key, object old_value, object new_value)
        {
            var setup = new SetupConfigReader();
            if (!setup.debug)
                return;

            old_value = string.IsNullOrEmpty(Convert.ToString(old_value)) ? "null" : old_value;
            new_value = string.IsNullOrEmpty(Convert.ToString(new_value)) ? "null" : new_value;

            if (Convert.ToString(old_value) == Convert.ToString(new_value))
            {
                return;
            }

            System.Diagnostics.Debug.WriteLine("[{0}] {1} => {2}", key, old_value, new_value);
        }

        /// <summary>
        /// Dictionaryからオブジェクトのプロパティへ、値を全てセット
        /// <para>Sets the values from the dictionary to the property of an object</para>
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="dic"></param>
        public static void WriteUpdatedValuesInOverwriteWithParameter(object obj, IDictionary<string, object> dic, string bindTargetName = null)
        {
            var setup = new SetupConfigReader();
            if (!setup.debug)
                return;

            if (!(dic is ErsReflection.ErsModelDictionary<string, object>))
            {
                //DBデータのバインドは出力しない。多すぎて重いので。
                return;
            }

            ErsDebug.WriteLine("Updating values in ErsRepositoryEntity.OverwriteWithParameter");

            var properties = obj.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!ErsReflection.isBindTarget(property, bindTargetName))
                {
                    continue;
                }

                string dictionaryKey = property.Name;
                if (property.CanWrite && dic.ContainsKey(dictionaryKey))
                {
                    //debug用に、上書きした値を出力
                    var oldValue = (property.CanRead ? property.GetValue(obj, null) : "(unknown)");
                    ErsDebug.WriteUpdatedValues(dictionaryKey, oldValue, dic[dictionaryKey]);
                }
            }
        }

        /// <summary>
		/// Writes the SQL command, parameters and compilation result to Systems.Diagnostics.Debug.Listeners collection
        /// </summary>
        /// <param name="command"></param>
        public static void WriteSQL(IDbCommand command)
        {

            var setup = new SetupConfigReader();
            if (!setup.debug)
                return;

            System.Diagnostics.Debug.WriteLine("SQL:" + Regex.Replace(command.CommandText, "\\s+", " "));

            System.Diagnostics.Debug.WriteLine(GetDbCommandParameterString(command.Parameters));

            try
            {
                System.Diagnostics.Debug.WriteLine("compiled:" + TryCompileSQL(command.CommandText, command.Parameters));
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("compiled:failure");
            }

            System.Diagnostics.Debug.WriteLine(string.Empty);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="strSQL"></param>
		/// <param name="dbParameterCollection"></param>
		/// <returns></returns>
        public static string TryCompileSQL(string strSQL, IDataParameterCollection dbParameterCollection)
        {
            foreach (DbParameter parameter in dbParameterCollection)
            {
                string valueString = null;

                if (!(parameter.Value is string) && parameter.Value is IEnumerable)
                {
                    var listElements = new List<string>();

                    foreach (var elment in (IEnumerable)parameter.Value)
                    {
                        listElements.Add(GetParameterValueString(elment));
                    }

                    valueString = string.Format("(ARRAY[{0}])", String.Join(",", listElements));
                }
                else
                {
                    valueString = GetParameterValueString(parameter.Value);
                }

                Regex regex = new Regex(":" + parameter.ParameterName + @"([^\d]|$)");
                strSQL = regex.Replace(strSQL, valueString + "$1");
            }
            return strSQL;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string GetParameterValueString(object value)
        {
            if (value is int || value is short || value is long || value is bool)
            {
                return Convert.ToString(value);
            }

            return string.Format("'{0}'", Convert.ToString(value));
        }

        /// <summary>
        /// Returns the list of parameters and it's values
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public static string GetDbCommandParameterString(IDataParameterCollection parameters)
        {
            var parameterString = string.Empty;
            if (parameters.Count != 0)
            {
                parameterString += "Parameter:";
                foreach (var item in parameters)
                {
                    var parameter = (DbParameter)item;
                    object outputValue = null;
                    if (!(parameter.Value is string) && parameter.Value is IEnumerable)
                    {
                        var strJoin = string.Empty;
                        foreach (var val in (IEnumerable)parameter.Value)
                        {
                            if (val != null)
                            {
                                strJoin += "," + val.ToString();
                            }
                        }
                        if (!string.IsNullOrEmpty(strJoin))
                            outputValue = "{" + strJoin.Substring(1) + "}";
                    }
                    else
                    {
                        outputValue = parameter.Value;
                    }
                    parameterString += "[" + parameter.ParameterName + "]" + outputValue + "/";
                }
            }
            return parameterString;
        }

        /// <summary>
        /// リソースに定義があるかチェック
		/// <para>Check if definition exists in the resource</para>
        /// </summary>
        /// <param name="retVal"></param>
        internal static void CheckResource(string retVal)
        {
            var setup = new SetupConfigReader();
            if (!setup.debug)
                return;

            if (NotExistResourceMessages == null)
                NotExistResourceMessages = new List<string>();

            if (!NotExistResourceMessages.Contains(retVal))
                NotExistResourceMessages.Add(retVal);

            NotExistResourceDetailMessage += string.Format("**********Detail for [{0}]**********\r\n{1}\r\n\r\n", retVal, Environment.StackTrace);
        }

		/// <summary>
		/// Raise a Resource Error
		/// </summary>
        internal static void RaiseResourceError()
        {
            var setup = new SetupConfigReader();
            if (!setup.debug)
                return;

            if (NotExistResourceMessages != null && NotExistResourceMessages.Count > 0)
            {
                var exceptionMessage = string.Join(Environment.NewLine, NotExistResourceMessages);
                exceptionMessage += "\r\n" + NotExistResourceDetailMessage;
                throw new Exception(exceptionMessage);
            }
        }

		/// <summary>
		/// Returns a collection of NotExistResource error messages
		/// </summary>
        private static List<string> NotExistResourceMessages
        {
            get
            {
                if (ErsCommonContext.GetPooledObject("NotExistResourceMessages") == null)
                    ErsCommonContext.SetPooledObject("NotExistResourceMessages", new List<string>());

                return (List<string>)ErsCommonContext.GetPooledObject("NotExistResourceMessages");
            }
            set
            {
                ErsCommonContext.SetPooledObject("NotExistResourceMessages", value);
            }
        }

        /// <summary>
        /// Returns a collection of NotExistResource error messages
        /// </summary>
        private static string NotExistResourceDetailMessage
        {
            get
            {
                if (ErsCommonContext.GetPooledObject("NotExistResourceDetailMessage") == null)
                    ErsCommonContext.SetPooledObject("NotExistResourceDetailMessage", string.Empty);

                return (string)ErsCommonContext.GetPooledObject("NotExistResourceDetailMessage");
            }
            set
            {
                ErsCommonContext.SetPooledObject("NotExistResourceDetailMessage", value);
            }
        }

        /// <summary>
        /// モデルがErsModelBaseを継承しているかチェック
		/// <para>Check model if it inherits ErsModelBase</para>
        /// </summary>
        /// <param name="p"></param>
        public static object CheckModel(Type modelType, string modelName, string controllerName)
        {
            var setup = new SetupConfigReader();

            //ErsModelBase以外はBindしないように修正（eckは除く）
            if (setup.debug && !typeof(ErsModelBase).IsAssignableFrom(modelType) && modelName != "eck")
            {
                throw new Exception("The argument '" + modelName + "' of the action method '" + controllerName + "' is not sub class of ErsModelBase.\nThe ERS Framework can't bind the value to model if it is not sub class of ErsModelBase.");
            }

            return null;
        }

        /// <summary>
        /// コントローラーがErsControllerBaseを継承しているかチェック
		/// <para>Check controller if inherits ErsControllerBase</para>
        /// </summary>
        /// <param name="globalSystemWebMvcControllerBase"></param>
        public static void CheckController(ControllerBase controller)
        {
            var setup = new SetupConfigReader();
            if (!setup.debug)
                return;

            if (!(controller is ErsControllerBase))
            {
                throw new Exception(controller.GetType().Name + " has to inherit ErsControllerBase.");
            }
        }

        /// <summary>
        /// Add specified error message to errors collection for the model-state dictionary of the ViewResult
        /// </summary>
        /// <param name="exceptionMessage"></param>
        /// <param name="result"></param>
        public static void SetDebugErrorToView(string exceptionMessage, ViewResult result)
        {
            var setup = new SetupConfigReader();
            if (!setup.debug)
                return;

            result.ViewData.ModelState.AddModelError("exception_detail", exceptionMessage);

        }
    }
}
