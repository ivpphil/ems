﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.Collections;
using jp.co.ivp.ers.mvc;
using System.Net;
using System.IO;
using Renci.SshNet;

namespace jp.co.ivp.ers.util
{
    public class SFTPClient
        : IDisposable
    {

        SftpClient sftpClient;

        private SFTPClient(string host, string user, string pass, int? port, string sshKeyPath = "", string sshPassPhrase = "") 
        {
            var methods = new List<AuthenticationMethod>();
            methods.Add(new PasswordAuthenticationMethod(user, pass));
            if (!string.IsNullOrEmpty(sshKeyPath))
            {
                if (!string.IsNullOrEmpty(sshPassPhrase))
                {
                    methods.Add(new PrivateKeyAuthenticationMethod(user, new PrivateKeyFile(sshKeyPath, sshPassPhrase)));
                }
                else
                {
                    methods.Add(new PrivateKeyAuthenticationMethod(user, new PrivateKeyFile(sshKeyPath)));
                }
            }

            ConnectionInfo con;
            if (port.HasValue)
            {
                con = new ConnectionInfo(host, port.Value, user, methods.ToArray());
            }
            else
            {
                con = new ConnectionInfo(host, user, methods.ToArray());
            }

            this.sftpClient = new SftpClient(con);

            sftpClient.Connect();

            this.closed = false;
        }

        /// <summary>
        /// SFTPで接続します。
        /// </summary>
        /// <param name="host"></param>
        /// <param name="user"></param>
        /// <param name="pass"></param>
        /// <param name="port"></param>
        /// <param name="sshKeyPath"></param>
        /// <param name="sshPathPhrase"></param>
        /// <returns></returns>
        public static SFTPClient Connect(string host, string user, string pass, int? port, string sshKeyPath = "", string sshPassPhrase = "") 
        {
            return new SFTPClient(host, user, pass, port, sshKeyPath, sshPassPhrase);
        }

        /// <summary>
        /// サーバーにファイルアップ
        /// </summary>
        /// <param name="sourceFilePath">アップ対象ファイルパス</param>
        /// <param name="ftpPath">FTPアップパス</param>
        public void PutFile(string sourceFilePath, string ftpUpPath)
        {
            using (var fileStream = new FileStream(sourceFilePath, FileMode.Open))
            {
                sftpClient.UploadFile(fileStream, ftpUpPath);
            }
        }

        /// <summary>
        /// ファイルの存在チェック
        /// </summary>
        /// <param name="ftpFilePath"></param>
        /// <returns></returns>
        public bool Exists(string ftpFilePath)
        {
            return sftpClient.Exists(ftpFilePath);
        }

        /// <summary>
        /// サーバーからファイル取得
        /// </summary>
        /// <param name="ftpFilePath">FTPパス</param>
        /// <param name="localFilePath">保存先ファイルパス</param>
        public void GetFile(string ftpFilePath, string localFilePath)
        {
            using (var fileStream = new FileStream(localFilePath, FileMode.CreateNew))
            {
                sftpClient.DownloadFile(ftpFilePath, fileStream);
            }
        }

        /// <summary>
        /// サーバーファイル削除
        /// </summary>
        /// <param name="ftpPath"></param>
        public void DeleteFile(string ftpFilePath)
        {
            sftpClient.DeleteFile(ftpFilePath);
        }

        /// <summary>
        /// サーバーファイル一覧を文字列で取得
        /// </summary>
        /// <param name="ftpPath"></param>
        public List<string> GetFileList(string ftpPath)
        {
            var listReturn = new List<string>();

            var files = sftpClient.ListDirectory(ftpPath);

            foreach (var file in files)
            {
                listReturn.Add(file.FullName);
            }

            return listReturn;
        }

        private bool closed = true;
        public void Close()
        {
            if (!closed)
            {
                sftpClient.Disconnect();
                closed = true;
            }
        }

        public void Dispose()
        {
            this.Close();
        }
    }
}
