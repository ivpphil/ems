﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public static class ErsEncoding
    {
        /// <summary>
        /// Get for search
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static Encoding ShiftJIS
        {
            get
            {
                return Encoding.GetEncoding("Shift_JIS");
            }
        }
        /// <summary>
        /// Get for search
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static Encoding ISO2022JP
        {
            get
            {
                return Encoding.GetEncoding("iso-2022-jp");
            }
        }
    }
}
