﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

namespace jp.co.ivp.ers.util
{
    public class ErsHashCalculater
    {
        /// <summary>
        /// 引数のバイトからハッシュを算出する(SHA-256)
		/// <para>Calculate the hash</para>
        /// </summary>
        /// <param name="byteValues"></param>
        /// <returns></returns>
        public virtual string CalculateSHA256(params byte[][] byteValues)
        {
            //SHA256CryptoServiceProviderオブジェクトを作成
            System.Security.Cryptography.SHA256 sha256 =
                System.Security.Cryptography.SHA256.Create();

            var listByte = new List<Byte>();
            foreach (var arrByte in byteValues)
            {
                listByte.AddRange(arrByte);
            }

            //ハッシュ値を計算する
            byte[] bs = sha256.ComputeHash(listByte.ToArray());

            return BitConverter.ToString(bs).ToLower().Replace("-", "");
        }

        /// <summary>
        /// 引数のバイトからハッシュを算出する(MD5)
        /// </summary>
        /// <param name="byteValues"></param>
        /// <returns></returns>
        public virtual string CalculateMD5(params byte[][] byteValues)
        {
            //SHA256CryptoServiceProviderオブジェクトを作成
            System.Security.Cryptography.MD5 md5 =
                System.Security.Cryptography.MD5.Create();

            var listByte = new List<Byte>();
            foreach (var arrByte in byteValues)
            {
                listByte.AddRange(arrByte);
            }

            //ハッシュ値を計算する
            byte[] bs = md5.ComputeHash(listByte.ToArray());

            return BitConverter.ToString(bs).ToLower().Replace("-", "");
        }

        /// <summary>
        /// 文字列からバイト配列を取得する
		/// <para>Get's the byte array from string</para>
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public byte[] GetByte(string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                return new byte[0];
            }
            return System.Text.Encoding.UTF8.GetBytes(source);
        }

        /// <summary>
        /// アップロードされたファイルからバイト配列を取得する
		/// <para>Gets an array of bytes from a file that is uploaded</para>
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public byte[] GetByte(Stream stream)
        {
            stream.Position = 0;
            Byte[] destination = new Byte[stream.Length];
            stream.Read(destination, 0, destination.Length);
            return destination;
        }
    }
}
