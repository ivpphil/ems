﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace jp.co.ivp.ers.util
{
    public class FTPClient
    {
        /// <summary>
        ///ログインユーザー名
		///<para>Login/Username</para>
        /// </summary>
        public virtual string userName { get; set; }

        /// <summary>
        ///ログインパスワード
		///<para>Password</para>
        /// </summary>
        public virtual string userPassword { get; set; }

        /// <summary>
        /// MethodにWebRequestMethods.Ftp.UploadFile("STOR")を設定
        /// </summary>
        public virtual string Method { get; set; }

        /// <summary>
        /// 要求の完了後に接続を閉じる
		/// <para>Closes the connection after the request completes</para>
        /// </summary>
        public virtual bool KeepAlive { get; set; }

        /// <summary>
        /// ASCIIモードで転送する 
		/// <para>Transferred in ASCII mode</para>
        /// </summary>
        public virtual bool UseBinary { get; set; }

        /// <summary>
        /// PASVモードを無効にする
		/// <para>To use or not to use PASC mode</para>
        /// </summary>
        public virtual bool UsePassive { get; set; }

        /// <summary>
        /// 初期化する。
		/// <para>Initialize</para>
        /// </summary>
        public virtual void Initialize()
        {
            this.Method = System.Net.WebRequestMethods.Ftp.UploadFile;

            this.KeepAlive = false;

            this.UseBinary = true;

            this.UsePassive = false;
        }

        public virtual void Initialize(string userName, string userPassword)
        {
            this.Initialize();

            this.userName = userName;

            this.userPassword = userPassword;
        }

        public virtual void Send(string targetUrl, string fileName, string dataSourceFilePath)
        {
            //アップロードするファイルを開く
			//Open the file you want to upload
            using (System.IO.FileStream fs = new System.IO.FileStream(dataSourceFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                this.Send(targetUrl, fileName, fs);
            }
        }

        public virtual void Send(string targetUrl, string fileName, System.IO.FileStream fs)
        {
            //アップロード先のURI
			//URI of the upload destnation
            var uri = new Uri(targetUrl + fileName);

            //FtpWebRequestの作成
            var ftpReq = (System.Net.FtpWebRequest)System.Net.WebRequest.Create(uri);

            this.LoadSettings(ftpReq);
			
            //ファイルをアップロードするためのStreamを取得
			//Get the stream to upload the file
            using (System.IO.Stream reqStrm = ftpReq.GetRequestStream())
            {
                //アップロードStreamに書き込む
                byte[] buffer = new byte[1024];
                while (true)
                {
                    int readSize = fs.Read(buffer, 0, buffer.Length);
                    if (readSize == 0)
                        break;
                    reqStrm.Write(buffer, 0, readSize);
                }
            }

            //FtpWebResponseを取得
            using (System.Net.FtpWebResponse ftpRes = (System.Net.FtpWebResponse)ftpReq.GetResponse())
            {
                if (ftpRes.StatusCode != FtpStatusCode.ClosingData)
                {
                    throw new Exception(string.Format("[{0}]{1}", ftpRes.StatusCode, ftpRes.StatusDescription));
                }
            }
        }

        private void LoadSettings(System.Net.FtpWebRequest ftpReq)
        {
            //ログインユーザー名とパスワードを設定
			//Set the login username and password
            ftpReq.Credentials = new System.Net.NetworkCredential(this.userName, this.userPassword);

            ftpReq.Method = this.Method;
            ftpReq.KeepAlive = this.KeepAlive;
            ftpReq.UseBinary = this.UseBinary;
            ftpReq.UsePassive = this.UsePassive;
        }
    }
}
