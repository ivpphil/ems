﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Reflection;
using System.ComponentModel;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.template;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers
{
    public class ErsResources
    {
        private ErsResources() { }

        /// <summary>
        /// フィールド名や単語のリソースを取得する
		/// <para>Get the resource field names and words</para>
        /// </summary>
        public static string GetFieldName(string key)
        {
            string retVal = null;
            if (FieldNameResourceDictionary.dicFieldNameResource.ContainsKey(key))
            {
                retVal = FieldNameResourceDictionary.dicFieldNameResource[key];
            }
            else if (key.Contains('.'))
            {
                //カンマを取り除いてフィールドだけを取得
                string newkey = key.Split('.')[1];
                if (FieldNameResourceDictionary.dicFieldNameResource.ContainsKey(newkey))
                {
                    retVal = FieldNameResourceDictionary.dicFieldNameResource[newkey];
                }
            }

            if (!string.IsNullOrEmpty(retVal))
            {
                return retVal;
            }

            retVal = String.Format(FieldNameResourceDictionary.dicFieldNameResource["default"], key);
            ErsDebug.CheckResource(retVal);
            return retVal;

        }

        /// <summary>
        /// メッセージのリソースを取得する
		/// <para>Gets the resource of the message</para>
        /// </summary>
        public static string GetMessage(string key, params object[] args)
        {
            if (!MessageResourceDictionary.dicMessageResource.ContainsKey(key))
            {
                string retVal = String.Format(MessageResourceDictionary.dicMessageResource["default"], key);
                ErsDebug.CheckResource(retVal);
                return retVal;
            }
            string rtn = MessageResourceDictionary.dicMessageResource[key];
            if(args.Length != 0)
                rtn = string.Format(rtn, args);

            return rtn.Replace("\\r\\n", Environment.NewLine);
        }

        public static string GetDisplayName(IEnumerable<Attribute> attributes)
        {
            foreach (var attr in attributes)
            {
                //Validator検索
                var attrDisplayName = attr as DisplayNameAttribute;
                if (attrDisplayName != null)
                    return attrDisplayName.DisplayName;
            }

            return string.Empty;
        }
    }
}
