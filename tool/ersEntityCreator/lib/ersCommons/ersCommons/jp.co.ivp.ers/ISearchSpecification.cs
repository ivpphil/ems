﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mvc
{
    public interface ISearchSpecification
    {
        /// <summary>
        /// 検索データ取得
		/// <para>Search data acquisition</para>
        /// </summary>
        /// <param name="crtOrder">クライテリア
		///		<para>Criteria</para>
		/// </param>
        /// <returns>データテーブル
		///		<para>Data Table</para>
		/// </returns>
        List<Dictionary<string, object>> GetSearchData(Criteria criteria);

        /// <summary>
        /// 検索データ取得
		/// <para>Search data acquisition</para>
        /// </summary>
        /// <param name="crtOrder">クライテリア
		///		<para>Criteria</para>
		/// </param>
        /// <returns>データテーブル
		///		<para>Data Table</para>
		/// </returns>
        int GetCountData(Criteria criteria);
    }
}
