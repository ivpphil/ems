﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    [Flags]
    public enum EnumPictureType
    {
        BMP,
        JPEG,
        GIF,
        EMF,
        WMF,
        TIFF,
        PNG,
        ICO,
    }
}
