﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.db
{
    public interface ISpecificationForSQL
    {
        string asSQL();
    }
}
