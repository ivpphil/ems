﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.db
{
    public class ErsDatabaseFactory : AbstractErsDatabaseFactory
    {

        protected static Dictionary<string, ErsDatabase> dicConnections
        {
            get
            {
                if (ErsCommonContext.GetPooledObject("dicConnections") == null)
                    ErsCommonContext.SetPooledObject("dicConnections", new Dictionary<string, ErsDatabase>());
                return (Dictionary<string, ErsDatabase>)ErsCommonContext.GetPooledObject("dicConnections");
            }
            set
            {
                ErsCommonContext.SetPooledObject("dicConnections", value);
            }
        }
		/// <summary>
		/// Get ErsDatabase Connectionstring
		/// </summary>
        public override ErsDatabase GetErsDatabase()
        {
            return GetErsDatabase(ErsCommonsSetting.strConnectionString);
        }

		/// <summary>
		/// Get ErsDatabase Connectionstring from specified connectionstring
		/// </summary>
        public override ErsDatabase GetErsDatabase(string connectionString)
        {
            if (dicConnections.ContainsKey(connectionString)) return dicConnections[connectionString];

            dicConnections[connectionString] = GetNewErsDatabase(connectionString);

            return dicConnections[connectionString];
        }

		/// <summary>
		/// Get New Ers Database Connectionstring
		/// </summary>
        public override ErsDatabase GetNewErsDatabase()
        {
            return GetNewErsDatabase(ErsCommonsSetting.strConnectionString);
        }
		/// <summary>
		/// Get New Ers Database Connection 
		/// </summary>
        public override ErsDatabase GetNewErsDatabase(string connectionString)
        {
            ErsDatabase objDB = new NpgsqlErsDatabase();
            objDB.connection = objDB.CreateConnection(connectionString);
            return objDB;
        }
    }
}
