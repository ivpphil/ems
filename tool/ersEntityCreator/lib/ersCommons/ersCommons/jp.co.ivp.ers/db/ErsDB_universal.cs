﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace jp.co.ivp.ers.db
{
    public sealed class ErsDB_universal : ErsDB_parent
    {
        private ErsDB_universal()
            : base(string.Empty)
        {

        }

        private ErsDB_universal(ErsDatabase objDB)
            : base(string.Empty, objDB)
        {

        }

        public static ErsDB_universal GetInstance()
        {
            return new ErsDB_universal();
        }

        public static ErsDB_universal GetInstance(ErsDatabase objDB)
        {
            return new ErsDB_universal(objDB);
        }

        /// <summary>
        /// 任意のクエリを実行する 
		/// <para>execute an arbitrary query</para>
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strSQL"></param>
        public List<Dictionary<string, object>> ExecuteQuery(string strSQL, Criteria criteria)
        {

            var strWhere = this.GetOptionalStatement(criteria, strSQL);

            var parameters = this.GetParameter(criteria);

            return ExecuteQuery(strSQL, strWhere, parameters);

        }

        /// <summary>
        /// 任意のクエリを実行する
		/// <para>Execute a specified query</para>
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strSQL"></param>
        public List<Dictionary<string, object>> ExecuteQuery(string strSQL, string strWhere = "", IEnumerable<DbParameter> parameters = null)
        {
            DbCommand command = this.objDB.CreateCommand();
            if (parameters != null)
            {
                foreach (DbParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            if (!string.IsNullOrEmpty(strWhere))
            {
                strSQL += " " + strWhere;
            }

            command.CommandText = strSQL;
            return this.FillDictionary(command);

        }

        /// <summary>
        /// 任意のクエリを実行する
		/// <para>execute an arbitrary query</para>
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strSQL"></param>
        public int ExecuteUpdate(string strSQL, Criteria criteria)
        {

            var strWhere = this.GetOptionalStatement(criteria, strSQL);

            var parameters = this.GetParameter(criteria);

            return ExecuteUpdate(strSQL, strWhere, parameters);

        }

        /// <summary>
        /// 任意のクエリを実行する
		/// <para>execute an arbitrary query</para>
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strSQL"></param>
        public int ExecuteUpdate(string strSQL, string strWhere = "", IEnumerable<DbParameter> parameters = null)
        {
            DbCommand command = this.objDB.CreateCommand();
            if (parameters != null)
            {
                foreach (DbParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            if (!string.IsNullOrEmpty(strWhere))
            {
                strSQL += " " + strWhere;
            }

            command.CommandText = strSQL;
            return this.ExecuteNonQuery(command);

        }
    }
}
