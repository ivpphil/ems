﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db
{
    public class ColumnName
    {
        protected internal string value { get; protected set; }

        public ColumnName(string value)
        {
            this.value = value;
        }
    }
}
