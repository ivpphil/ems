﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// field LIKE value を生成
    /// </summary>
    public abstract class LIKE_COMMON
        : CriterionBase
    {
        Criteria.LIKE_SEARCH_TYPE type;
        //int nthCount { get; set; }
       private string nthString { get; set; }

        public LIKE_COMMON(string field, object value, Criteria.LIKE_SEARCH_TYPE type)
            : base(field, value)
        {
            this.type = type;
        }
        /// <summary>
        /// for wildcard "_"
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <param name="nthCount"></param>
        public LIKE_COMMON(string field, object value, Criteria.LIKE_SEARCH_TYPE type, string nthString)
            : base(field, value)
        {
            this.type = type;
            this.nthString = nthString;
        }

        public abstract override string GetWhere();

        /// <summary>
        /// 後方一致用文字列を取得する。
		/// <para>Get prefix for searching similar word endings</para>
        /// </summary>
        /// <returns></returns>
        protected string GetPrefix()
        {
            switch (type)
            {
                case Criteria.LIKE_SEARCH_TYPE.SUFFIX_SEARCH:
                case Criteria.LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH:
                    return "'%' || ";

                default:
                    //string nthString = string.Empty;
                    //if (nthCount > 0)
                    //{
                    //    for (int i = 0; i < nthCount; i++) nthString += "_";
                        
                    //}
                   
                        return "'" + nthString + "'" + " || ";
            }
        }

        /// <summary>
        /// 前方一致用文字列を取得する。
		/// <para>Get prefix for searching similar word begginings</para>
        /// </summary>
        /// <returns></returns>
        protected string GetSuffix()
        {
            switch (type)
            {
                case Criteria.LIKE_SEARCH_TYPE.PREFIX_SEARCH:
                case Criteria.LIKE_SEARCH_TYPE.AMBIGUOUS_SEARCH:
                    return " || '%'";
                //for wildcard "_"
                default:
                    //string nthString = string.Empty;
                    //if (nthCount > 0)
                    //{
                    //    for (int i = 0; i < nthCount; i++) nthString += "_";

                    //}

                    return " || " + "'" + nthString + "'" ;
            }
        }

        /// <summary>
        /// エスケープ文字などを置換する
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected override object ReplaceEscapes(object value)
        {
            if (value is string && !string.IsNullOrEmpty((string)value))
            {
                return ((string)value).Replace(@"\", @"\\").Replace("%", @"\%").Replace("_", @"\_");
            }

            return value;
        }

    }

    /// <summary>
    /// field LIKE value を生成
    /// </summary>
    public class LIKE
        : LIKE_COMMON
    {

        public LIKE(string field, object value, Criteria.LIKE_SEARCH_TYPE type) : base(field, value, type) { }
        /// <summary>
        /// for wildcard "_"
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <param name="nthCount"></param>
        public LIKE(string field, object value, Criteria.LIKE_SEARCH_TYPE type, string nthString) : base(field, value, type, nthString) { }

        public override string GetWhere()
        {
            return " " + field + " LIKE " + this.GetPrefix() + ":" + field + this.DbParameterCount + this.GetSuffix();
        }
    }

    /// <summary>
    /// field NOT LIKE value を生成
    /// </summary>
    public class NOT_LIKE
        : LIKE_COMMON
    {

        public NOT_LIKE(string field, object value, Criteria.LIKE_SEARCH_TYPE type) : base(field, value, type) { }

        public override string GetWhere()
        {
            return " " + field + " NOT LIKE " + this.GetPrefix() + ":" + field + this.DbParameterCount + this.GetSuffix();
        }
    }

}
