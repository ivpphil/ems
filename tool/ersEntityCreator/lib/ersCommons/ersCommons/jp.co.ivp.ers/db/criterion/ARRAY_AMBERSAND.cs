﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// value && ARRAY (field) を生成
    /// </summary>
    public class ARRAY_AMBERSAND
        : CriterionBase
    {
        public ARRAY_AMBERSAND(string field, object value) : base(field, value) { }

		/// <summary>
		/// Returns the where condition
		/// </summary>
        public override string GetWhere()
        {
            if (value is ColumnName)
            {
                return " " + ((ColumnName)value).value + " && ARRAY[" + field + "]";
            }
            else
            {
                return " :" + field + this.DbParameterCount + " && ARRAY[" + field + "]";
            }
        }
    }
}
