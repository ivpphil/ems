﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// field = value または field IS NULL を生成
    /// </summary>
    public class EQUAL
        : CriterionBase
    {

        public EQUAL(string field, object value) : base(field, value) { }

        public override string GetWhere()
        {
            //nullの場合はSQL分岐
            if (this.value == null)
            {
                return " " + field + " IS NULL";
            }
            else if(value is ColumnName)
            {
                return " " + field + " = " + ((ColumnName)value).value;
            }
            else
            {
                return " " + field + " = :" + field + this.DbParameterCount;
            }
        }
    }

    /// <summary>
    /// field <> value または field IS NOT NULL を生成
    /// </summary>
    public class NOT_EQUAL
        : CriterionBase
    {

        public NOT_EQUAL(string field, object value) : base(field, value) { }

        public override string GetWhere()
        {
            //nullの場合はSQL分岐
            if (this.value == null)
            {
                return " " + field + " IS NOT NULL";
            }
            else if (value is ColumnName)
            {
                return " " + field + " <> " + ((ColumnName)value).value;
            }
            else
            {
                return " " + field + " <> :" + field + this.DbParameterCount;
            }
        }
    }
}
