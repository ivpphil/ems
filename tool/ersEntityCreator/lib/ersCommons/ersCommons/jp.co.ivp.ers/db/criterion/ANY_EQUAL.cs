﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// value = ANY (field) を生成
    /// </summary>
    public class ANY_EQUAL
        : CriterionBase
    {
        public ANY_EQUAL(string field, object value) : base(field, value) { }

        public override string GetWhere()
        {
            if (value is ColumnName)
            {
                return " " + ((ColumnName)value).value + " = ANY(" + field + ")";
            }
            else
            {
                return " :" + field + this.DbParameterCount + " = ANY(" + field + ")";
            }
        }
    }

    /// <summary>
    /// value = ANY (field) を生成
    /// </summary>
    public class ANY_NOT_EQUAL
        : CriterionBase
    {
        public ANY_NOT_EQUAL(string field, object value) : base(field, value) { }

        public override string GetWhere()
        {
            if (value is ColumnName)
            {
                return " " + ((ColumnName)value).value + " <> ANY(" + field + ")";
            }
            else
            {
                return " :" + field + this.DbParameterCount + " <> ANY(" + field + ")";
            }
        }
    }
}
