﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.db
{

    public class ErsDB_parent
    {
        public static bool InTransaction { get; private set; }

        protected virtual string myTableName { get; set; }

        protected ErsDatabase objDB;

        private SetupConfigReader setup = new SetupConfigReader();

		/// <summary>
		/// Returns a DataTabe of myTableName
		/// </summary>
        protected DataTable baseTable
        {
            get
            {
                if (dbDataTablePool.ContainsKey(myTableName))
                {
                    return dbDataTablePool[myTableName];
                }

                var dt = new DataTable();

                string SQLString = "SELECT * FROM " + myTableName + " WHERE false ";

                var command = objDB.CreateCommand();
                command.CommandText = SQLString;

                var adapter = objDB.CreateDataAdapter();
                adapter.SelectCommand = command;

                adapter.FillSchema(dt, SchemaType.Source);

                dbDataTablePool[myTableName] = dt;

                return dt;
            }
        }

        /// <summary>
        /// スキーマ情報を保持する
		/// <para>Holds the schema information</para>
        /// </summary>
        public DataColumnCollection Schema
        {
            get
            {
                return baseTable.Columns;
            }
        }

        /// <summary>
        /// スキーマ付きDataTableを返す。
		/// <para>Returns a DataTable with the schema.</para>
        /// </summary>
        /// <returns></returns>
        public virtual DataTable GetTable()
        {
            return baseTable.Clone();
        }

        /// <summary>
        /// データベース名
        /// </summary>
        public string DatabaseName
        {
            get
            {
                return this.objDB != null ? this.objDB.connection.Database : null;
            }
        }

        public ErsDB_parent(string tableName)
            : this(tableName, ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase())
        {
        }

        /// <summary>
        /// for unit test
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="connection"></param>
        public ErsDB_parent(string tableName, ErsDatabase connection)
        {

            this.objDB = connection;

            this.myTableName = tableName;

        }

        /// <summary>
        /// Begin transaction if the transaction doesn't started yet.
        /// </summary>
        /// <returns></returns>
        public static ErsConnection BeginTransaction()
        {
            return BeginTransaction(ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase());
        }

        /// <summary>
        /// Begin transaction if the transaction doesn't started yet.
        /// </summary>
        /// <returns></returns>
        public static ErsConnection BeginTransaction(DbConnection objDb)
        {
            var connection = OpenConnection(objDb);
            connection.BeginTransaction();
            InTransaction = true;
            return connection;
        }

        public static ErsConnection OpenConnection()
        {
            return OpenConnection(ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase());
        }

        public static ErsConnection OpenConnection(DbConnection objDb)
        {
            var retVal = new ErsConnection(objDb);
            retVal.OpenConnection();
            return retVal;
        }

        private static Dictionary<string, DataTable> _dbSchemaPool
        {
            get
            {
                return (Dictionary<string, DataTable>)ErsCommonContext.GetPooledObject("_dbSchemaPool");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_dbSchemaPool", value);
            }
        }

        /// <summary>
        /// get a pooled schema of table.
        /// </summary>
        protected Dictionary<string, DataTable> dbDataTablePool
        {
            get
            {
                if (_dbSchemaPool == null)
                    _dbSchemaPool = new Dictionary<string, DataTable>();

                return _dbSchemaPool;
            }
        }

        /// <summary>
        /// コメントを取得して、Captureにセット（入力チェック用）
		/// <para>To get a comment, set to Capture (test input)</para>
        /// </summary>
        public virtual void SetCommentToCaption()
        {
            //コメントを取得してCaptureに設定
            var dt = new DataTable();
            var SQLString = "SELECT                                                                                                 "
                                + "    attr.attname AS column                                                                          "
                                + "    ,des.description AS comment                                                                     "
                                + "FROM                                                                                                "
                                + "    pg_catalog.pg_class AS a                                                                        "
                                + "    INNER JOIN pg_catalog.pg_namespace AS ns ON a.relnamespace = ns.oid                             "
                                + "    INNER JOIN pg_catalog.pg_attribute AS attr ON a.oid = attr.attrelid and attr.attnum > 0         "
                                + "    LEFT JOIN pg_catalog.pg_description AS des ON a.oid = des.objoid and attr.attnum = des.objsubid "
                                + "WHERE                                                                                               "
                                + "    a.relkind IN ('r')                                                                              "
                                + "    AND attr.attisdropped = false                                                                   "
                                + "    AND ns.nspname IN ('public')                                                                    "
                                + "    AND a.relname = '" + myTableName + "'                                                           "
                                + "ORDER BY                                                                                            "
                                + "    a.oid                                                                                           "
                                + "    ,attr.attnum                                                                                    ";

            var command = objDB.CreateCommand();
            command.CommandText = SQLString;

            var adapter = objDB.CreateDataAdapter();
            adapter.SelectCommand = command;
            adapter.Fill(dt);

            foreach (DataRow dr in dt.Rows)
            {
                Schema[Convert.ToString(dr["column"])].Caption = (dr["comment"] != DBNull.Value ? Convert.ToString(dr["comment"]) : string.Empty);
            }

        }


        /// <summary>
        /// Criteriaからwhere句を取得する
		/// <para>Gets the where clause from the Criteria</para>
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public virtual string GetOptionalStatement(Criteria criteria, string strSQL = "")
        {
            if (criteria == null)
                return string.Empty;

            var strWhere = criteria.GetWhere();
            if (!string.IsNullOrEmpty(strWhere))
            {
                if (strSQL.Contains("WHERE"))
                    strWhere = " AND " + strWhere;
                else
                    strWhere = " WHERE " + strWhere;
            }

            var strGroupBy = criteria.GetGroupBy();
            if (!string.IsNullOrEmpty(strGroupBy))
                strGroupBy = " GROUP BY " + strGroupBy;

            var strOrderBy = criteria.GetOrderBy();
            if (!string.IsNullOrEmpty(strOrderBy))
                strOrderBy = " ORDER BY " + strOrderBy;

            var strOption = criteria.GetOption();

            return strWhere += strGroupBy + strOrderBy + strOption;

        }

        public IEnumerable<DbParameter> GetParameter(Criteria criteria)
        {
            if (criteria == null)
            {
                yield break;
            }

            foreach (var parameter in criteria.GetParameter(this.objDB))
            {
                yield return parameter;
            }
        }

        //----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// カウントを取得する。
		/// <para>Gets the count</para>
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public virtual long gSelectCount(string column, string strWhere = "", IEnumerable<DbParameter> parameters = null)
        {

            if (strWhere.Contains(" ORDER BY "))
                throw new ArgumentException("In ErsDB_parent.gSelectCount method, you can not use the Order By clause.");

            var list = gSelect(new[] { "COUNT(" + column + ") AS count" }, strWhere, parameters);

            if (list.Count == 1)
            {
                return (long)list[0]["count"];
            }

            return 0;
        }

        /// <summary>
        /// カウントを取得する。(Criteria用)
		/// <para>Gets the count. (For Criteria)</para>
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public virtual long gSelectCount(string column, Criteria criteria)
        {

            var strWhere = this.GetOptionalStatement(criteria);

            var parameters = this.GetParameter(criteria);

            return gSelectCount(column, strWhere, parameters);
        }

        /// <summary>
        /// Select
        /// </summary>
        /// <param name="objDB"></param>
        /// <param name="dt"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public virtual List<Dictionary<string, object>> gSelect(string strWhere = "", IEnumerable<DbParameter> parameters = null)
        {
            return this.gSelect(new string[] { "*" }, strWhere, parameters);
        }

        /// <summary>
        /// Select
        /// </summary>
        /// <param name="objDB"></param>
        /// <param name="dt"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public virtual List<Dictionary<string, object>> gSelect(Criteria criteria)
        {
            var strWhere = this.GetOptionalStatement(criteria);

            var parameters = this.GetParameter(criteria);

            return gSelect(strWhere, parameters);

        }

        /// <summary>
        /// Select
        /// </summary>
        /// <param name="objDB"></param>
        /// <param name="dt"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected virtual List<Dictionary<string, object>> gSelect(IEnumerable<string> columns, string strWhere = "", IEnumerable<DbParameter> parameters = null)
        {
            DbCommand command = objDB.CreateCommand();
            if (parameters != null)
            {
                foreach (DbParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            string selectStatement = string.Empty;
            foreach (string column in columns)
            {
                selectStatement += "," + column;
            }
            string SQLString = "SELECT " + selectStatement.Substring(1) + " FROM " + myTableName;

            if (!string.IsNullOrEmpty(strWhere))
            {
                SQLString += " " + strWhere;
            }

            command.CommandText = SQLString;

            return this.FillDictionary(command);

        }

        /// <summary>
        /// Select
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="dt"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public virtual List<Dictionary<string, object>> gSelect(IEnumerable<string> columns, Criteria criteria)
        {
            var strWhere = this.GetOptionalStatement(criteria);

            var parameters = this.GetParameter(criteria);

            return gSelect(columns, strWhere, parameters);

        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="objDB"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public virtual int gDelete(string strWhere = "")
        {

            return gDelete(strWhere, null);
        }


        public virtual int gDelete(Criteria criteria)
        {

            var strWhere = this.GetOptionalStatement(criteria);

            var parameters = this.GetParameter(criteria);

            return gDelete(strWhere, parameters);
        }

        public virtual int gDelete(string strWhere, IEnumerable<DbParameter> parameters)
        {

            DbCommand command = objDB.CreateCommand();

            if (parameters != null)
            {
                foreach (DbParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            string SQLString = "DELETE FROM " + myTableName;
            if (!string.IsNullOrEmpty(strWhere))
            {
                SQLString += " " + strWhere;
            }
            command.CommandText = SQLString;
            return ExecuteNonQuery(command);

        }

        /// <summary>
        /// Insert(本体)
        /// </summary>
        /// <param name="objDB"></param>
        /// <param name="dr"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public virtual int gInsert(Dictionary<string, object> dic, bool skipColumn = true)
        {
            var dicForInsert = this.GetDictionaryForInsert(dic, skipColumn);

            if (this.Schema.Contains("intime") && (!dicForInsert.ContainsKey("intime") || dicForInsert["intime"] == null))
            {
                dicForInsert["intime"] = DateTime.Now;
            }

            DbCommand command = objDB.CreateCommand();

            string columnStatement = string.Empty;
            string valueStatement = string.Empty;
            foreach (var key in dicForInsert.Keys)
            {
                if (!this.Schema.Contains(key))
                {
                    throw new Exception(string.Format("Column {0}.{1} is not defined in db.", this.myTableName, key));
                }

                //連番フィールドはNullの場合Insertしない
                if (this.Schema[key].AutoIncrement && (!dicForInsert.ContainsKey(key) || dicForInsert[key] == null))
                    continue;

                columnStatement += "," + key;
                valueStatement += ",:" + key;
                command.Parameters.Add(objDB.CreateParameter(key, dicForInsert[key]));
            }

            string SQLString = string.Empty;
            SQLString = "INSERT INTO " + myTableName + "(" + columnStatement.Substring(1) + ") VALUES(" + valueStatement.Substring(1) + ")";

            command.CommandText = SQLString;

            return ExecuteNonQuery(command);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="objDB"></param>
        /// <param name="dr"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public virtual int gUpdateColumn(string[] columns, Dictionary<string, object> dic, string strWhere, IEnumerable<DbParameter> parameters = null)
        {

            var listTarget_columns = new List<string>(columns);

            var dicForInsert = this.GetDictionaryForUpdate(dic);

            if (this.Schema.Contains("utime"))
            {
                if (!listTarget_columns.Contains("utime"))
                    listTarget_columns.Add("utime");

                dicForInsert["utime"] = DateTime.Now;
            }

            DbCommand command = objDB.CreateCommand();
            if (parameters != null)
            {
                foreach (DbParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }


            string statement = string.Empty;
            foreach (var key in listTarget_columns)
            {

                //指定されたカラムのみアップデート
                statement += "," + string.Format("{0} = :{0}", key);
                command.Parameters.Add(objDB.CreateParameter(key, dicForInsert[key]));
            }


            string SQLString = "UPDATE " + myTableName + " SET " + statement.Substring(1) + "";

            if (!string.IsNullOrEmpty(strWhere))
            {
                SQLString += " " + strWhere;
            }

            command.CommandText = SQLString;

            return ExecuteNonQuery(command);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="objDB"></param>
        /// <param name="dr"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public virtual int gUpdateColumn(string[] columns, Dictionary<string, object> dic, Criteria criteria)
        {

            var strWhere = this.GetOptionalStatement(criteria);

            var parameters = this.GetParameter(criteria);

            return gUpdateColumn(columns, GetDictionaryForUpdate(dic), strWhere, parameters);
        }

        /// <summary>
        /// コマンドを発行し、Dictionaryを埋める
		/// <para>Issue the command, fill the Dictionary</para>
        /// </summary>
        /// <param name="command"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public virtual List<Dictionary<string, object>> FillDictionary(DbCommand command)
        {
            var adapter = objDB.CreateDataAdapter();
            adapter.SelectCommand = command;

            var dt = new DataTable();
            using (var conection = ErsDB_parent.OpenConnection(command.Connection))
            {
                adapter.Fill(dt);
            }

            var retVal = new List<Dictionary<string, object>>();
            foreach (DataRow dr in dt.Rows)
            {
                retVal.Add(this.GetDictionaryWithDataRow(dr));
            }

            return retVal;

        }

        /// <summary>
        /// コマンドを発行する
		/// <para>Issue a command</para>
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        protected int ExecuteNonQuery(DbCommand command)
        {
            using (var conection = ErsDB_parent.OpenConnection(command.Connection))
            {
                int result = -1;

                result = command.ExecuteNonQuery();

                return result;
            }
        }

        public virtual int GetNextSequence()
        {
            return this.GetNextSequence(this.myTableName + "_id_seq");
        }

        public virtual int GetNextSequence(string name)
        {
            string SQLString = "SELECT nextval('" + name + "') AS value;";
            var list = ErsDB_universal.GetInstance(this.objDB).ExecuteQuery(SQLString);
            return Convert.ToInt32(list[0]["value"]);
        }

        public virtual int GetCurrentSequence()
        {
            return this.GetCurrentSequence(this.myTableName + "_id_seq");
        }

        public virtual int GetCurrentSequence(string name)
        {
            string SQLString = "SELECT currval('" + name + "') AS value;";
            var list = ErsDB_universal.GetInstance(this.objDB).ExecuteQuery(SQLString);
            return Convert.ToInt32(list[0]["value"]);
        }

        /// <summary>
        /// スキーマに沿った値をセットしたDictionaryを返す。
		/// <para>Returns the Dictionary that set the value along the schema.</para>
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="dr"></param>
        /// <returns></returns>
        protected virtual Dictionary<string, object> GetDictionaryForUpdate(Dictionary<string, object> dic)
        {
            var ret = new Dictionary<string, object>();
            foreach (DataColumn dc in Schema)
            {
                if (!dic.ContainsKey(dc.ColumnName))
                {
                    continue;
                }

                if (dic[dc.ColumnName] == null)
                {
                    ret[dc.ColumnName] = DBNull.Value;
                }
                else
                {
                    ret[dc.ColumnName] = dic[dc.ColumnName];
                }
            }

            return ret;
        }

        /// <summary>
        /// スキーマに沿った値をセットしたDictionaryを返す。
		/// Returns the Dictionary that set the value along the schema.
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="dr"></param>
        /// <returns></returns>
        protected virtual Dictionary<string, object> GetDictionaryForInsert(Dictionary<string, object> dic, bool skipColumn)
        {
            IEnumerable<string> listColumnName;
            if (skipColumn)
            {
                // カラムに含まれるデータのみInsertする
                var tpListColumnName = new List<string>();
                foreach (DataColumn dc in Schema)
                {
                    tpListColumnName.Add(dc.ColumnName);
                }
                listColumnName = tpListColumnName;
            }
            else
            {
                // ディクショナリに存在するデータすべてをINSERTする。
                listColumnName = dic.Keys;
            }

            var ret = new Dictionary<string, object>();
            foreach (string columnName in listColumnName)
            {
                if (!dic.ContainsKey(columnName))
                {
                    continue;
                }

                if (dic[columnName] == null)
                {
                    //The dictionary of return value does not include the value of NULL.
                    //ret[dc.ColumnName] = DBNull.Value;
                }
                else
                {
                    ret[columnName] = dic[columnName];
                }
            }

            return ret;
        }

        /// <summary>
        /// Dictionaryに、DataRowの値をセットする。
		/// <para>The Dictionary, to set the value of the DataRow</para>
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="dr"></param>
        /// <returns></returns>
        protected virtual Dictionary<string, object> GetDictionaryWithDataRow(DataRow dr)
        {

            Dictionary<string, object> dic = new Dictionary<string, object>();
            foreach (DataColumn dc in dr.Table.Columns)
            {
                if (dr[dc.ColumnName] == DBNull.Value)
                {
                    dic[dc.ColumnName] = null;
                }
                else if (setup.debug && dr[dc.ColumnName] is DateTime)
                {
                    dic[dc.ColumnName] = ErsDateTime.ToJstDateTime((DateTime)dr[dc.ColumnName]);
                }
                else
                {
                    dic[dc.ColumnName] = dr[dc.ColumnName];
                }
            }

            return dic;
        }
    }
}
