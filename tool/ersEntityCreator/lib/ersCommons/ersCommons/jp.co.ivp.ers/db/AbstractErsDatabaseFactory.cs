﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.db
{
    abstract public class AbstractErsDatabaseFactory
    {
        public abstract ErsDatabase GetErsDatabase(string connectionString);
        public abstract ErsDatabase GetErsDatabase();

        public abstract ErsDatabase GetNewErsDatabase(string connectionString);
        public abstract ErsDatabase GetNewErsDatabase();
    }
}
