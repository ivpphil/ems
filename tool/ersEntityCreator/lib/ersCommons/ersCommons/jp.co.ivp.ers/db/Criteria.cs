﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Collections;
using System.Reflection;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.db
{
    public class Criteria
        : AND_JOIN_CRITERION, ICriteria
    {
        public Criteria() : base(null) { }

        public Criteria(params CriterionBase[] criterions)
            : base(null)
        {
            foreach (var criterion in criterions)
            {
                this.Add(criterion);
            }
        }

        /// <summary>
        /// パラメータクエリごとのシーケンス番号を保持する
        /// </summary>
        protected static int _DbParameterCountInThread
        {
            get
            {
                return (int)Convert.ToInt32(ErsCommonContext.GetPooledObject("_DbParameterCountInThread"));
            }
            set
            {
                ErsCommonContext.SetPooledObject("_DbParameterCountInThread", value);
            }
        }

        /// <summary>
        /// ラメータクエリごとにシーケンス番号を付与して重複を避ける
        /// </summary>
        public static int DbParameterCountInThread
        {
            get
            {
                return _DbParameterCountInThread++;
            }
        }

        /// <summary>
        /// 演算子 / Operator
        /// </summary>
        public enum Operation
        {
            /// <summary>
            /// field = value
            /// </summary>
            EQUAL,

            /// <summary>
            /// field &lt;&gt; value
            /// </summary>
            NOT_EQUAL,

            /// <summary>
            /// field &gt; value
            /// </summary>
            GREATER_THAN,

            /// <summary>
            /// field &gt;= value
            /// </summary>
            GREATER_EQUAL,

            /// <summary>
            /// field &lt; value
            /// </summary>
            LESS_THAN,

            /// <summary>
            /// field &lt;= value
            /// </summary>
            LESS_EQUAL,

            /// <summary>
            /// field = ANY(value)
            /// </summary>
            ANY_EQUAL,

            /// <summary>
            /// field &lt;&gt; ANY(value)
            /// </summary>
            ANY_NOT_EQUAL,

            /// <summary>
            /// field && ARRAY(value)
            /// </summary>
            ARRAY_AMBERSAND,
        }

        public enum OrderBy
        {
            ORDER_BY_ASC,
            ORDER_BY_DESC
        }

        public long? LIMIT { get; set; }
        public long? OFFSET { get; set; }

        private List<CriterionBase> criterionList = new List<CriterionBase>();
        private List<OrderByBase> OrderByList = new List<OrderByBase>();
        private List<string> GroupByList = new List<string>();

        public bool ForUpdate { get; set; }

        /// <summary>
        /// clear criteria values
        /// </summary>
        public void Clear()
        {
            criterionList.Clear();
            OrderByList.Clear();
            GroupByList.Clear();
            LIMIT = null;
            OFFSET = null;
        }

        /// <summary>
        /// クライテリオンオブジェクトを追加する
        /// </summary>
        public void Add(CriterionBase criterion)
        {
            criterionList.Add(criterion);
        }

        /// <summary>
        /// クライテリオンオブジェクトを取得する
		/// <para>Add a Criterion object</para>
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <param name="operation"></param>
        /// <returns></returns>
        public static CriterionBase GetCriterion(string fieldName, object compareValue, Operation operation)
        {
            var t = Type.GetType("jp.co.ivp.ers.db." + operation.ToString());
            return (CriterionBase)System.Activator.CreateInstance(t, new object[] { fieldName, compareValue });
        }

        /// <summary>
        /// クライテリオンオブジェクトを取得する
		/// <para>Get criterion object</para>
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <param name="operation"></param>
        /// <returns></returns>
        public static CriterionBase GetBetweenCriterion(object fromValue, object toValue, object compareValue)
        {
            return new BETWEEN(fromValue, toValue, compareValue);
        }

        /// <summary>
        /// Gets instance of Criterion which contains a statement and a parameter.
        /// </summary>
        /// <param name="statement"></param>
        /// <returns></returns>
        public static CriterionBase GetUniversalCriterion(string statement, Dictionary<string, object> parameters = null)
        {
            return new UniversalCriterion(statement, parameters);
        }

        /// <summary>
        /// ORDER BYを追加する
		/// <para>Add ORDER BY</para>
        /// </summary>
        /// <param name="field"></param>
        /// <param name="operation"></param>
        public virtual void AddOrderBy(string field, OrderBy operation)
        {
            this.OrderByList.Add(this.GetOrderByCriterion(field, operation));
        }

        public virtual void AddOrderBy(Criteria criteria)
        {
            foreach (var orderBy in criteria.OrderByList)
            {
                this.OrderByList.Add(orderBy);
            }
        }

        /// <summary>
        /// 指定順序でソートするOrderByを追加する
        /// </summary>
        /// <param name="field"></param>
        /// <param name="arrayValue"></param>
        public void AddOrderByArrayIndex<T>(string field, T[] arrayValue, OrderBy orderBy)
        {
            var objOrderBy = new OrderByArrayIndex<T>(field, arrayValue, orderBy);
            OrderByList.Add(objOrderBy);
        }

        public void InsertOrderByToFirst(string field, OrderBy operation)
        {
            OrderByList.Insert(0, this.GetOrderByCriterion(field, operation));
        }

        /// <summary>
        /// GROUP BYを追加する
		/// <para>Add GROUP BY</para>
        /// </summary>
        /// <param name="field"></param>
        /// <param name="operation"></param>
        public virtual void AddGroupBy(string field)
        {
            GroupByList.Add(field);
        }

        /// <summary>
        /// クライテリオンオブジェクトを取得する
		/// <para>Get the criterion object</para>
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <param name="operation"></param>
        /// <returns></returns>
        protected OrderByBase GetOrderByCriterion(string field, OrderBy operation)
        {
            var t = Type.GetType("jp.co.ivp.ers.db." + operation.ToString());
            return (OrderByBase)System.Activator.CreateInstance(t, new object[] { field });
        }

        /// <summary>
        /// クライテリオンをORで結合して新しいクライテリアを返す（ex. (tableA.columnB = 'value1' OR tableC.columD = 'value2')）
		/// <para>Returns the new criteria that are joined by OR criterion (ex. (tableA.columnB = 'value1' OR tableC.columD = 'value2'))</para>
        /// </summary>
        /// <param name="operationBases"></param>
        public static Criteria JoinWithOR(IEnumerable<CriterionBase> criterionList)
        {
            var newCriteria = new Criteria();
            newCriteria.Add(new OR_JOIN_CRITERION(criterionList));
            return newCriteria;
        }

        /// <summary>
        /// クライテリオンをANDで結合して新しいクライテリアを返す（ex. (tableA.columnB = 'value1' AND tableC.columD = 'value2')）
        /// </summary>
        /// <param name="operationBases"></param>
        public static Criteria JoinWithAnd(IEnumerable<CriterionBase> criterionList)
        {
            var newCriteria = new Criteria();
            newCriteria.Add(new AND_JOIN_CRITERION(criterionList));
            return newCriteria;
        }

        /// <summary>
        /// IN句のCriterionを取得する。
		/// <para>Gets the Criterion of clause IN.</para>
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CriterionBase GetInClauseCriterion<T>(string field, IEnumerable<T> value)
        {
            return new IN<T>(field, value);
        }

        /// <summary>
        /// NOT IN句のCriterionを取得する。
		/// <para>Gets the Criterion of clause NOT IN.</para>
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CriterionBase GetNotInClauseCriterion<T>(string field, IEnumerable<T> value)
        {
            return new NOT_IN<T>(field, value);
        }

        /// <summary>
        /// LIKE検索の検索種別を指定する。
		/// <para>Specify the type of LIKE search.</para>
        /// </summary>
        public enum LIKE_SEARCH_TYPE
        {
            /// <summary>
            /// 前方一致
            /// </summary>
            PREFIX_SEARCH,

            /// <summary>
            /// 後方一致
            /// </summary>
            SUFFIX_SEARCH,

            /// <summary>
            /// あいまい検索
            /// </summary>
            AMBIGUOUS_SEARCH
        }

        /// <summary>
        /// LIKE句のCriterionを取得する。
		/// <para>Gets the Criterion of the LIKE clause.</para>
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CriterionBase GetLikeClauseCriterion(string field, object value, LIKE_SEARCH_TYPE type)
        {
            return new LIKE(field, value, type);
        }
        /// <summary>
        /// for wildcard "_"
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <param name="nthCount"></param>
        /// <returns></returns>
        public static CriterionBase GetLikeClauseCriterion(string field, object value, LIKE_SEARCH_TYPE type, string nthString)
        {
            return new LIKE(field, value, type, nthString);
        }

        /// <summary>
        /// NOT LIKE句のCriterionを取得する。
		/// <para>Gets the Criterion of NOT LIKE clause.</para>
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CriterionBase GetNotLikeClauseCriterion(string field, object value, LIKE_SEARCH_TYPE type)
        {
            return new NOT_LIKE(field, value, type);
        }

        /// <summary>
        /// Criteriaからパラメータ付きのORDER BY句を取得する。
		/// <para>Gets the ORDER BY clause with a parameter from the Criteria.</para>
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public string GetOrderBy()
        {
            var retVal = string.Empty;
            foreach (var orderBy in this.OrderByList)
            {
                retVal += "," + orderBy.GetStatement();
            }

            if (!string.IsNullOrEmpty(retVal))
            {
                return retVal.Substring(1);
            }

            return retVal;
        }

        /// <summary>
        /// CriteriaからGroup BY句を取得する。
		/// <para>Gets the Group BY clause from the Criteria.</para>
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public string GetGroupBy()
        {

            var retVal = string.Empty;
            foreach (var groupBy in this.GroupByList)
            {
                retVal += "," + groupBy;
            }

            if (!string.IsNullOrEmpty(retVal))
            {
                return retVal.Substring(1);
            }

            return retVal;
        }

        public string GetDistinctOn(params string[] tableNames)
        {
            var distinctOnList = new List<string>();
            foreach (var orderBy in this.OrderByList)
            {
                if (tableNames != null && !this.CheckContainsTableName(orderBy, tableNames))
                {
                    break;
                }

                var orderByField = orderBy.GetStatement().Replace("ASC","").Replace("DESC", "");
                distinctOnList.Add(orderByField);
            }

            return string.Join(",", distinctOnList);
        }

        private bool CheckContainsTableName(OrderByBase orderBy, string[] tableNames)
        {
            foreach (var tableName in tableNames)
            {
                if (orderBy.field.StartsWith(tableName))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Criteriaからパラメータ付きのLIMIT句・OFFSET句を取得する。
		/// <para>OFFSET LIMIT clause to get the clause with the parameters from the Criteria.</para>
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public string GetOption()
        {
            var retVal = string.Empty;
            if (this.LIMIT != null)
            {
                retVal += " LIMIT " + this.LIMIT;
            }

            if (this.OFFSET != null)
            {
                retVal += " OFFSET " + this.OFFSET;
            }

            if (this.ForUpdate)
            {
                retVal += " FOR UPDATE";
            }

            return retVal;
        }

        public override IEnumerable<CriterionBase> GetCriterionList(IEnumerable<CriterionBase> value)
        {
            return this.criterionList;
        }

        public static ColumnName ColumnName(string value)
        {
            return new ColumnName(value);
        }

        /// <summary>
        /// Clear the criterion in this criteria.
        /// 追加したクライテリオンをクリアする。
        /// </summary>
        /// <param name="filedName"></param>
        protected void RemoveCriterion(string filedName)
        {
            var listTargetCriterion = new List<CriterionBase>();
            foreach (var criterion in this.criterionList)
            {
                if (criterion.field == filedName)
                {
                    listTargetCriterion.Add(criterion);
                }
            }

            foreach (var criterion in listTargetCriterion)
            {
                this.criterionList.Remove(criterion);
            }
        }

        public override IEnumerable<DbParameter> GetParameter(ErsDatabase objDB)
        {
            foreach (var parameter in base.GetParameter(objDB))
            {
                yield return parameter;
            }

            foreach (var orderBy in this.OrderByList)
            {
                foreach(var parameter in orderBy.GetParameter(objDB))
                {
                    yield return parameter;
                }
            }
        }

        /// <summary>
        /// Set this criteria only as Pramater container.
        /// </summary>
        public bool ParamaterOnly { get; set; }

        public override string GetWhere()
        {
            if (this.ParamaterOnly)
            {
                return null;
            }

            return base.GetWhere();
        }
    }
}

