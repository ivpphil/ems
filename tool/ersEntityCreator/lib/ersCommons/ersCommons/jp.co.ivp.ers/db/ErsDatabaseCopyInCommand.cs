﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Data.Common;
using System.IO;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.db
{
    public class ErsDatabaseCopyInCommand
    {
        NpgsqlCopyIn objCopyIn;
        ErsDatabase objDB;
        ErsCopySerializer objErsSerializer;
        DbConnection profilerWrappedConnection;

        public ErsDatabaseCopyInCommand()
            : this(ErsCommonsSetting.ersDatabaseFactory.GetErsDatabase())
        {
        }

        public ErsDatabaseCopyInCommand(ErsDatabase objDB)
        {
            this.objDB = objDB;

            if (this.objDB.connection is StackExchange.Profiling.Data.ProfiledDbConnection)
                profilerWrappedConnection = ((StackExchange.Profiling.Data.ProfiledDbConnection)this.objDB.connection).WrappedConnection;
        }

        public ErsCopySerializer SerializeStreamConnection()
        {
            objErsSerializer = new ErsCopySerializer((NpgsqlConnection)profilerWrappedConnection);

            this.SetCommandNpgsqlCopyIn(this.profilerWrappedConnection.CreateCommand(), objErsSerializer.ToStream);

            objErsSerializer.SetCopyIn(objCopyIn);

            return objErsSerializer;
        }

        public ErsCopySerializer SerializeTransactionConnection()
        {
            this.SetCommandNpgsqlCopyIn(this.profilerWrappedConnection.CreateCommand());
            objErsSerializer = new ErsCopySerializer((NpgsqlConnection)profilerWrappedConnection, objCopyIn);

            return objErsSerializer;
        }

        public DbCommand CreateCommandNpgsqlCopyIn()
        {
            return this.objDB.CreateCommand();
        }

        public void SetCommandNpgsqlCopyIn(DbCommand command, Stream stream = null)
        {
            if (stream == null)
                CommandNpgsqlCopyIn(command);
            else
                CommandNpgsqlCopyIn(command, stream);
        }

        public void SetCommandNpgsqlCopyIn(string queryCommand, Stream stream = null)
        {
            var command = this.CreateCommandNpgsqlCopyIn();
            command.CommandText = queryCommand;

            SetCommandNpgsqlCopyIn(command, stream);
        }

        public void CommandNpgsqlCopyIn(DbCommand command)
        {
            var npgsqlCommand = (NpgsqlCommand)command;
            objCopyIn = new NpgsqlCopyIn(npgsqlCommand, npgsqlCommand.Connection);
        }

        public void CommandNpgsqlCopyIn(DbCommand command, Stream stream)
        {
            var npgsqlCommand = (NpgsqlCommand)command;
            objCopyIn = new NpgsqlCopyIn(npgsqlCommand, npgsqlCommand.Connection, stream);
        }

        public void StartCopyInQueryCommand(string queryCommand)
        {
            SetCopyInQueryCommand(queryCommand);
            objErsSerializer.StartCopyIn();
        }

        public void SetCopyInQueryCommand(string queryCommand)
        {
            objErsSerializer.SetCopyInQueryCommand(queryCommand);
        }

        public void SerializeEndTransaction()
        {
                objErsSerializer.SerializeClose();
        }

        public void SerializeEndStream()
        {
            objErsSerializer.EndCopyIn();
            objErsSerializer.SerializeClose();

        }

        public string GetNpgsqlExceptionMessage(Exception ex)
        {
            if (ex is NpgsqlException)
            {
                var npgsqlEx = ((Npgsql.NpgsqlException)(ex));
                return String.Join(Environment.NewLine, npgsqlEx.Message, npgsqlEx.Where, npgsqlEx.Detail);
            }

            return ex.Message;
        }

        public void Cancel(string message)
        {
            objCopyIn.Cancel(message);
        }

        public void CopyStartFromStream(string commandString, Stream stream)
        {
            var streamConnection = new NpgsqlConnection(this.objDB.ConnectionString);
            using (var connection = ErsDB_parent.OpenConnection(streamConnection))
            {
                var command = new NpgsqlCommand(commandString, streamConnection);

                stream.Position = 0;
                var copyInSream = new NpgsqlCopyIn(command, streamConnection, stream);
                copyInSream.Start();
                copyInSream.End();
            }
        }
    }
}
