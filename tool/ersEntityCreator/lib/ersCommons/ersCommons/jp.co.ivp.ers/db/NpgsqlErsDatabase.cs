﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using Npgsql;
using jp.co.ivp.ers.util;
using StackExchange.Profiling.Data;
using StackExchange.Profiling;
using System.Data;

namespace jp.co.ivp.ers.db
{
    public class NpgsqlErsDatabase
        : ErsDatabase
    {

        public override DbConnection CreateConnection(string connectionString)
        {
            //var profiler = new CompositeDbProfiler(new TraceDbProfiler(this), MiniProfiler.Current);

            //Analyze sql by ProfiledDbConnection class for MiniProfiler.
            //return new ProfiledDbConnection(new NpgsqlConnection(connectionString), profiler);
            return new NpgsqlConnection(connectionString);
        }

        public override DbParameter CreateParameter(string name, object value)
        {
            if (value is string)
            {
                value = ((string)value).Replace(Convert.ToString((char)0), string.Empty);
            }
            else if (value is Enum)
            {
                value = Convert.ChangeType(value, ((Enum)value).GetTypeCode());
            }

            return new NpgsqlParameter(name, value);
        }

        public override DbDataAdapter CreateDataAdapter(DbCommand command)
        {
            return new NpgsqlDataAdapter((NpgsqlCommand)command);
        }

        public override DbDataAdapter CreateDataAdapter()
        {
            return new NpgsqlDataAdapter();
        }

        public override void HandleException(Exception ex, IDbCommand command)
        {
            if (ex is NpgsqlException)
                throw new ErsDatabaseException(ex.Message
                    + Environment.NewLine
                    + command.CommandText
                    + Environment.NewLine
                    + ErsDebug.GetDbCommandParameterString(command.Parameters)
                    + ErsDebug.TryCompileSQL(command.CommandText, command.Parameters)
                    , ((NpgsqlException)ex).Code
                    , ex);
        }

        public override DbConnectionStringBuilder CreateConnectionStringBuilder()
        {
            return new NpgsqlConnectionStringBuilder(this.connection.ConnectionString);
        }
    }
}