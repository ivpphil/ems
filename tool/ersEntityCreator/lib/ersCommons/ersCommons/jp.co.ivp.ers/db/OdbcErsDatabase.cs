﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.Odbc;
using jp.co.ivp.ers.util;
using StackExchange.Profiling.Data;
using StackExchange.Profiling;
using System.Data;

namespace jp.co.ivp.ers.db
{
    public class OdbcErsDatabase
        : ErsDatabase
    {

        public override DbConnection CreateConnection(string connectionString)
        {
            //var profiler = new CompositeDbProfiler(new TraceDbProfiler(this), MiniProfiler.Current);

            //Analyze sql by ProfiledDbConnection class for MiniProfiler.
            //return new ProfiledDbConnection(new OdbcConnection(connectionString), profiler);
            return new OdbcConnection(connectionString);
        }

        public override DbParameter CreateParameter(string name, object value)
        {
            return new OdbcParameter(name, value);
        }

        public override DbDataAdapter CreateDataAdapter(DbCommand command)
        {
            return new OdbcDataAdapter((OdbcCommand)command);
        }

        public override DbDataAdapter CreateDataAdapter()
        {
            return new OdbcDataAdapter();
        }

        public override void HandleException(Exception ex, IDbCommand command)
        {
            if (ex is OdbcException)
                throw new ErsDatabaseException(ex.Message
                    + Environment.NewLine
                    + command.CommandText
                    + Environment.NewLine
                    + ErsDebug.GetDbCommandParameterString(command.Parameters)
                    , Convert.ToString(((OdbcException)ex).ErrorCode)
                    , ex);

            else
                throw ex;
        }

        public override DbConnectionStringBuilder CreateConnectionStringBuilder()
        {
            return new OdbcConnectionStringBuilder(this.connection.ConnectionString);
        }
    }
}