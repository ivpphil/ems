﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StackExchange.Profiling.Data;
using System.Data;
using System.Data.Common;

namespace jp.co.ivp.ers.db
{
    /// <summary>
    /// 複数のIDbProfiler実装クラスのオブジェクトを一つにまとめてProfiledDbConnectionクラスに渡せるようにするクラス
    /// The class that can pass multiple objects implementing IDbProfiler to ProfiledDbConnection
    /// </summary>
    public class CompositeDbProfiler : IDbProfiler
    {
        private readonly IDbProfiler[] profilers;

        public CompositeDbProfiler(params IDbProfiler[] dbProfilers)
        {
            this.profilers = dbProfilers;
        }

        public void ExecuteFinish(IDbCommand profiledDbCommand, ExecuteType executeType, DbDataReader reader)
        {
            foreach (var item in profilers)
            {
                if (item != null && item.IsActive)
                {
                    item.ExecuteFinish(profiledDbCommand, executeType, reader);
                }
            }
        }

        public void ExecuteStart(IDbCommand profiledDbCommand, ExecuteType executeType)
        {
            foreach (var item in profilers)
            {
                if (item != null && item.IsActive)
                {
                    item.ExecuteStart(profiledDbCommand, executeType);
                }
            }
        }

        public bool IsActive
        {
            get
            {
                return true;
            }
        }

        public void OnError(IDbCommand profiledDbCommand, ExecuteType executeType, Exception exception)
        {
            foreach (var item in profilers)
            {
                if (item != null && item.IsActive)
                {
                    item.OnError(profiledDbCommand, executeType, exception);
                }
            }
        }

        public void ReaderFinish(IDataReader reader)
        {
            foreach (var item in profilers)
            {
                if (item != null && item.IsActive)
                {
                    item.ReaderFinish(reader);
                }
            }
        }
    }
}
