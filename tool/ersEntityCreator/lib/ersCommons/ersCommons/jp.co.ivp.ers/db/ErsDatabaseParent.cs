﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;


namespace jp.co.ivp.ers.db
{
    public abstract class ErsDatabase : DbConnection
    {

        public DbConnection connection;

        public abstract DbConnection CreateConnection(string connectionString);

        public abstract DbParameter CreateParameter(string name, object value);

        public abstract DbDataAdapter CreateDataAdapter(DbCommand command);

        public abstract DbConnectionStringBuilder CreateConnectionStringBuilder();

        public abstract DbDataAdapter CreateDataAdapter();

        /// <summary>
        /// Exceptionをハンドルする。
        /// </summary>
        /// <param name="ex"></param>
        public abstract void HandleException(Exception ex, IDbCommand command);

        protected override DbTransaction BeginDbTransaction(System.Data.IsolationLevel isolationLevel)
        {
            return connection.BeginTransaction(isolationLevel);
        }

        public override void ChangeDatabase(string databaseName)
        {
            connection.ChangeDatabase(databaseName);
        }

        public override void Close()
        {
            connection.Close();
        }

        public override string ConnectionString
        {
            get
            {
                return connection.ConnectionString;
            }
            set
            {
                connection.ConnectionString = value;
            }
        }

        protected override DbCommand CreateDbCommand()
        {
            return connection.CreateCommand();
        }

        public override string DataSource
        {
            get { return connection.DataSource; }
        }

        public override string Database
        {
            get { return connection.Database; }
        }

        public override void Open()
        {
            connection.Open();
        }

        public override string ServerVersion
        {
            get { return connection.ServerVersion; }
        }

        public override System.Data.ConnectionState State
        {
            get { return connection.State; }
        }
    }
}
