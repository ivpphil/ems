﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.db
{
    public static class ErsCommonsSetting
    {

        public static AbstractErsDatabaseFactory ersDatabaseFactory;

        public static string strConnectionString = "";

        /// <summary>
		/// 静的初期化 / Static initialization
        /// </summary>
        static ErsCommonsSetting() {

            strConnectionString = SetupConfigReader.GetCommonConfigFileValue("ConnectionStrings");

            string strClassName = SetupConfigReader.GetCommonConfigFileValue("ErsDatabaseFactoryClassName");

            Type t;
            if (String.IsNullOrEmpty(strClassName) ) {
                t = Type.GetType("jp.co.ivp.ers.db.ErsDatabaseFactory");
                ersDatabaseFactory = (AbstractErsDatabaseFactory)Activator.CreateInstance(t);
            } else {

                ersDatabaseFactory = (AbstractErsDatabaseFactory)ErsReflection.CreateInstanceFromFullName(strClassName);
            }

        }

    }
}
