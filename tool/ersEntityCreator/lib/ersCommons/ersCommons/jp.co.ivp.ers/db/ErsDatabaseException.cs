﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace jp.co.ivp.ers.db
{
    public class ErsDatabaseException
        : DbException
    {
        public string Code { get; set; }

        public ErsDatabaseException(string message, string Code, Exception innerException)
            : base(message, innerException)
        {
            this.Code = Code;
        }
    }
}
