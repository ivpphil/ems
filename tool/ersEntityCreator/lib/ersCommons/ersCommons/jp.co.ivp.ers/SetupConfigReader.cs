﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.Configuration;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation.range_check;

namespace jp.co.ivp.ers.util
{
    public class SetupConfigReader
    {
		/// <summary>
        /// Get the pooled object of GetCommonConfigFileValueCache
		/// </summary>
        public static Configuration CommonConfiguration
        {
            get
            {
                var returnVal = (Configuration)ErsCommonContext.GetPooledObject("GetCommonConfigFileValueCache");
                if (returnVal == null)
                {
                    var ConfigurationFilePath = ErsCommonContext.MapPath("~/../setup/");
                    var fileMap =  new WebConfigurationFileMap();
                    var vDirMapBase = new VirtualDirectoryMapping(ConfigurationFilePath, true, "web.config");
                    fileMap.VirtualDirectories.Add("/virtual_common_config", vDirMapBase);

                    returnVal = System.Web.Configuration.WebConfigurationManager.OpenMappedWebConfiguration(fileMap, "/virtual_common_config", "virtual_common_config");
                    ErsCommonContext.SetPooledObject("GetCommonConfigFileValueCache", returnVal);
                }
                return returnVal;
            }
        }

        /// <summary>
        /// Get the pooled object of GetCommonConfigFileValueCache
        /// </summary>
        public static Configuration SiteConfiguration
        {
            get
            {
                var returnVal = (Configuration)ErsCommonContext.GetPooledObject("GetSiteConfigFileValueCache");
                if (returnVal == null)
                {
                    if (ErsCommonContext.IsBatch)
                    {
                        //バッチからweb.configを読み込む
                        var ConfigurationFilePath = ErsCommonContext.MapPath("~/../setup/batch/");
                        var vDirMapBase = new VirtualDirectoryMapping(ConfigurationFilePath, true, "web.config");
                        var fileMap =  new WebConfigurationFileMap();
                        fileMap.VirtualDirectories.Add("/virtual_batch_config", vDirMapBase);

                        returnVal = System.Web.Configuration.WebConfigurationManager.OpenMappedWebConfiguration(fileMap, "/virtual_batch_config", "virtual_batch_config");
                    }
                    else
                    {
                        returnVal = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
                    }
                    ErsCommonContext.SetPooledObject("GetSiteConfigFileValueCache", returnVal);
                }
                return returnVal;
            }
        }

		/// <summary>
		/// Gets the configuration value using a specified key from common setting file.
		/// </summary>
        public static string GetCommonConfigFileValue(string key)
        {
            var element = CommonConfiguration.AppSettings.Settings[key];
            if (element == null)
            {
                return null;
            }
            else
            {
                return element.Value;
            }
        }

		/// <summary>
        /// Returns a collection of configuration keys from common setting file.
		/// </summary>
        public static IEnumerable<string> GetCommonConfigFileKeys()
        {
            foreach (string key in CommonConfiguration.AppSettings.Settings.AllKeys)
            {
                if (!key.Contains("ConnectionStrings"))
                    yield return key;
            }
        }

        /// <summary>
        /// Gets the configuration value using a specified key from site setting file.
        /// </summary>
        public static string GetSiteConfigFileValue(string key)
        {
            var element = SiteConfiguration.AppSettings.Settings[key];
            if (element == null)
            {
                return GetCommonConfigFileValue(key);
            }
            else
            {
                return element.Value;
            }
        }

        /// <summary>
        /// Returns a collection of configuration keys from site setting file.
        /// </summary>
        public static IEnumerable<string> GetSiteConfigFileKeys()
        {
            foreach (string key in SiteConfiguration.AppSettings.Settings.AllKeys)
            {
                if (!key.Contains("ConnectionStrings"))
                    yield return key;
            }
        }

        protected internal SetupConfigReader()
        {
        }

		/// <summary>
		/// Returns the configuration value of nor_url
		/// </summary>
        public virtual string nor_url
        {
            get
            {
                return SiteTypeVariables.GetNorUrl(this.site_type);
            }
        }

		/// <summary>
        /// Returns the configuration value of sec_url
        /// </summary>
        public virtual string sec_url
        {
            get
            {
                return SiteTypeVariables.GetSecUrl(this.site_type);
            }
        }

        /// <summary>
        /// Gets System setting of Postfix backup type using GetConfigFileValue method based on the setup configuration.
        /// </summary>
        public virtual EnumSiteType site_type
        {
            get
            {
                return (EnumSiteType)Enum.Parse(typeof(EnumSiteType), (GetSiteConfigFileValue("site_type")));
            }
        }

        public string root_path
        {
            get
            {
                return GetCommonConfigFileValue("root_path");
            }
        }

        public string pc_nor_url
        {
            get
            {
                return GetCommonConfigFileValue("pc_nor_url");
            }
        }

        public string pc_sec_url
        {
            get
            {
                return GetCommonConfigFileValue("pc_sec_url");
            }
        }

        public string mobile_nor_url
        {
            get
            {
                return GetCommonConfigFileValue("mobile_nor_url");
            }
        }

        public string mobile_sec_url
        {
            get
            {
                return GetCommonConfigFileValue("mobile_sec_url");
            }
        }

        public string smartphone_sec_url
        {
            get
            {
                return GetCommonConfigFileValue("smartphone_sec_url");
            }
        }

        public string smartphone_nor_url
        {
            get
            {
                return GetCommonConfigFileValue("smartphone_nor_url");
            }
        }

        public string contact_nor_url
        {
            get
            {
                return GetCommonConfigFileValue("contact_nor_url");
            }
        }

        public string contact_sec_url
        {
            get
            {
                return GetCommonConfigFileValue("contact_sec_url");
            }
        }

        public string admin_nor_url
        {
            get
            {
                return GetCommonConfigFileValue("admin_nor_url");
            }
        }

        public string admin_sec_url
        {
            get
            {
                return GetCommonConfigFileValue("admin_sec_url");
            }
        }

        /// <summary>
        /// 許可HTMLタグ
		/// <para>HTML tags allowed</para>
        /// </summary>
        public virtual string[] AllowHtmlTag
        {
            get
            {
                if (_AllowHtmlTag == null)
                {
                    _AllowHtmlTag = GetCommonConfigFileValue("allowHtmlTag").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (var i = 0; i < _AllowHtmlTag.Length; i++)
                    {
                        _AllowHtmlTag[i] = _AllowHtmlTag[i].ToLower();
                    }
                }
                return _AllowHtmlTag;
            }
        }

        protected static string[] _AllowHtmlTag
        {
            get
            {
                return (string[])ErsCommonContext.GetPooledObject("_AllowHtmlTag");
            }
            set
            {
                ErsCommonContext.SetPooledObject("_AllowHtmlTag", value);
            }
        }

        /// <summary>
        /// Ignore Route Downloaded File Extensions
        /// </summary>
        public virtual string ignore_file_extensions
        {
            get
            {
                return GetCommonConfigFileValue("ignore_file_extensions");
            }
        }

        /// <summary>
        /// デバッグ
		/// <para>Debug</para>
        /// </summary>
        public virtual bool debug
        {
            get
            {
                return bool.Parse(GetCommonConfigFileValue("debug"));
            }
        }

        //ログパス
		//Log Path
        public virtual string log_path
        {
            get
            {
                return GetCommonConfigFileValue("log_path");
            }
        }

        //ログパス
        //Log Path
        public virtual string site_log_path
        {
            get
            {
                var pathUri = new Uri(this.log_path);
                return new Uri(pathUri, GetSiteConfigFileValue("log_directory") + "\\").LocalPath;
            }
        }

        /// <summary>
        /// ログファイル書き込みユーザ
        /// </summary>
        public string logFileUserName
        {
            get
            {
                return GetCommonConfigFileValue("logFileUserName");
            }
        }

        /// <summary>
        /// ログファイル書き込みユーザ
        /// </summary>
        public string logFileUserPassword
        {
            get
            {
                return GetCommonConfigFileValue("logFileUserPassword");
            }
        }

        public virtual string ProhibitionChars
        {
            get
            {
                return GetCommonConfigFileValue("ProhibitionChars");
            }
        }

        public virtual string ProhibitionCharsReplace
        {
            get
            {
                return GetCommonConfigFileValue("ProhibitionCharsReplace");
            }
        }

        /// <summary>
        /// Get number of links at Page ejection.
        /// </summary>
        public int PagerLinkNumber
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("PagerLinkNumber"));
            }
        }

        /// <summary>
        /// Get number of item on a page 
        /// </summary>
        public int DefaultItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("DefaultItemNumberOnPage"));
            }
        }

        /// <summary>
        /// Get number of item on a zip search page 
        /// </summary>
        public int ZipSearchItemNumberOnPage
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("ZipSearchItemNumberOnPage"));
            }
        }
		/// <summary>
		/// Get configuration value of MessageResource
		/// </summary>
        public string MessageResource
        {
            get
            {
                return GetCommonConfigFileValue("MessageResource");
            }
        }
		/// <summary>
		/// Get configuration value of FieldNameResource
		/// </summary>
        public string FieldNameResource
        {
            get
            {
                return GetCommonConfigFileValue("FieldNameResource");
            }
        }
		/// <summary>
        /// Get configuration value of ReturnUrlResource
		/// </summary>
        public string ReturnUrlResource
        {
            get
            {
                return GetCommonConfigFileValue("ReturnUrlResource");
            }
        }

        /// <summary>
		/// Get configuration value of enableEncryption
		/// </summary>

        public bool enableEncryption
        {
            get
            {
                return Convert.ToBoolean(GetCommonConfigFileValue("enableEncryption"));
            }
        }
		/// <summary>
		/// Get configuration value of enableSSL
		/// </summary>
        public virtual bool enableSSL
        {
            get
            {
                return Convert.ToBoolean(GetCommonConfigFileValue("enableSSL"));
            }
        }
		/// <summary>
		/// Get configuration value of onVEX
		/// </summary>
        public virtual bool onVEX
        {
            get
            {
                return Convert.ToBoolean(GetCommonConfigFileValue("onVEX"));
            }
        }

		/// <summary>
		/// Get configuration value of ValueRangeCheckerClass
		/// </summary>
        public EnumTextValueRangeChecker DefaultTextValueRangeChecker
        {
            get
            {
                EnumTextValueRangeChecker resultValue;
                if(!Enum.TryParse(GetCommonConfigFileValue("DefaultTextValueRangeChecker"), out resultValue))
                {
                    return EnumTextValueRangeChecker.LengthValueRangeChecker;
                }
                return resultValue;
            }
        }

        /// <summary>
        /// Get configuration value of ValueRangeCheckerClass
        /// </summary>
        public virtual IValueRangeChecker GetValueRangeChecker(EnumTextValueRangeChecker? enumRangeChecker)
        {
            if (!enumRangeChecker.HasValue)
            {
                enumRangeChecker = this.DefaultTextValueRangeChecker;
            }

            switch (enumRangeChecker)
            {
                case EnumTextValueRangeChecker.ByteValueRangeChecker:
                    return new ByteValueRangeChecker();
                case EnumTextValueRangeChecker.LengthValueRangeChecker:
                    return new LengthValueRangeChecker();
            }

            return null;
        }

        /// <summary>
        /// Get byte limit for file upload 
        /// </summary>
        public int default_upload_limit_byte
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("default_upload_limit_byte"));
            }
        }

        /// <summary>
        /// Get byte limit for csv upload 
        /// </summary>
        public int csv_upload_limit_byte
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("csv_upload_limit_byte"));
            }
        }

        /// <summary>
        /// Get byte limit for image upload 
        /// </summary>
        public int image_upload_limit_byte
        {
            get
            {
                return Convert.ToInt32(GetCommonConfigFileValue("image_upload_limit_byte"));
            }
        }

        public string default_send_testmail
        {
            get
            {
                return GetCommonConfigFileValue("default_send_testmail");
            }
        }

        /// <summary>
        /// Editor Reader Encoding
        /// </summary>
        public string editor_encoding
        {
            get
            {
                return GetCommonConfigFileValue("editor_encoding");
            }
        }

        /// <summary>
        /// Throw Message in duplicated ransu
        /// </summary>
        public string duplicated_ransu_error_code
        {
            get
            {
                return GetCommonConfigFileValue("duplicated_ransu_error_code");
            }
        }

        public IEnumerable<string> reject_logging_field_names
        {
            get
            {
                var reject_logging_field_names = GetCommonConfigFileValue("reject_logging_field_names");
                if (reject_logging_field_names.HasValue())
                {
                    return reject_logging_field_names.Split(';');
                }
                return new List<string>();
            }
        }

        public bool enableSni
        {
            get
            {
                return Convert.ToBoolean(GetCommonConfigFileValue("enableSni"));
            }
        }

        public string pc_application_root
        {
            get
            {
                return GetCommonConfigFileValue("pc_application_root");
            }
        }

        public string mobile_application_root
        {
            get
            {
                return GetCommonConfigFileValue("mobile_application_root");
            }
        }

        public string contact_application_root
        {
            get
            {
                return GetCommonConfigFileValue("contact_application_root");
            }
        }

        public string admin_application_root
        {
            get
            {
                return GetCommonConfigFileValue("admin_application_root");
            }
        }

        public string smartphone_application_root
        {
            get
            {
                return GetCommonConfigFileValue("smartphone_application_root");
            }
        }

        public string[] notAllowedUpdatingTables
        {
            get
            {
                return GetCommonConfigFileValue("notAllowedUpdatingTables").Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public bool EnableMiniProfiler
        {
            get
            {
                return Convert.ToBoolean(GetSiteConfigFileValue("EnableMiniProfiler"));
            }
        }

        public virtual string[] MiniProfilerAllowedIPs
        {
            get
            {
                return GetCommonConfigFileValue("MiniProfilerAllowedIPs").Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }



    }
}
