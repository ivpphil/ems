﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    public enum EnumSiteType
    {
        /// <summary>
        /// 0: PC
        /// </summary>
        PC,

        /// <summary>
        /// 1: モバイル
        /// </summary>
        MOBILE,

        /// <summary>
        /// 2: スマホ
        /// </summary>
        SMARTPHONE,

        /// <summary>
        /// 3: ＠コンタクト
        /// </summary>
        CONTACT,

        /// <summary>
        /// 4: 管理画面
        /// </summary>
        ADMIN,

        /// <summary>
        /// 5: 監視
        /// </summary>
        MONITOR,

        /// <summary>
        /// 6: バッチ
        /// </summary>
        BATCH
    }
}
