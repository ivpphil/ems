﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.db;
using System.Reflection;
using jp.co.ivp.ers;
using jp.co.ivp.ers.mvc;
using System.Data;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace jp.co.ivp.ers.mvc
{
    public abstract class ErsRepositoryEntity
        : IErsRepositoryEntity
    {
        public abstract int? id { get; set; }

        /// <summary>
        /// プロパティをDictionaryにセットして返す
		/// <para>Returns a set of properties to a dictionary</para>
        /// </summary>
        /// <returns></returns>
        public virtual Dictionary<string, object> GetPropertiesAsDictionary()
        {
            return ErsReflection.GetPropertiesAsDictionary(this, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        }

        /// <summary>
        /// プロパティをDictionaryにセットして返す
		/// <para>Returns a set of properties to a dictionary</para>
        /// </summary>
        /// <returns></returns>
        public virtual Dictionary<string, object> GetPropertiesAsDictionary(ErsDB_parent table)
        {
            var dicModel = this.GetPropertiesAsDictionary();

            var retVal = new Dictionary<string, object>();

            foreach (DataColumn column in table.Schema)
            {
                var key = column.ColumnName;
                if (dicModel.ContainsKey(key))
                {
                    retVal.Add(key, dicModel[key]);
                }
            }

            return retVal;
        }

        /// <summary>
        /// Overwrite publicly writable properties of this Entity using publicly readable properties of model.
        /// </summary>
        /// <param name="model"></param>
        public virtual void OverwriteWithModel(IErsCollectable model)
        {
            var sorceDic = this.GetPropertiesAsDictionary();

            var destDic = model.GetPropertiesAsDictionary();

            sorceDic = ErsCommon.OverwriteDictionary(sorceDic, destDic);

            this.OverwriteWithParameter(sorceDic);
        }

        /// <summary>
        /// Overwrite publicly writable properties of this Entity using dictionary values.
        /// </summary>
        /// <param name="model"></param>
        public virtual void OverwriteWithParameter(IDictionary<string, object> dictionary)
        {
            var dic = ErsCommon.OverwriteDictionary(this.GetPropertiesAsDictionary(), dictionary);
            ErsDebug.WriteUpdatedValuesInOverwriteWithParameter(this, dic);
            ErsReflection.SetPropertyAll(this, dic);
        }
    }
}
