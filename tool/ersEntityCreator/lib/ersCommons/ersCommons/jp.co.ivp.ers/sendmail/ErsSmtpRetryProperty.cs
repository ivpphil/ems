﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSmtpRetryProperty
    {
        public bool enableRetry
        {
            get
            {
                if (!this._enableRetry.HasValue)
                {
                    throw new Exception("Please call SetEnableRetry or SetDisableRetry");
                }

                return _enableRetry.Value;
            }
        }
        private bool? _enableRetry;

        public int retryTermMinutes { get; private set; }

        public int retryIntervalMinutes { get; private set; }

        public string retryTextPath { get; private set; }

        public void SetEnableRetry(int retryTermMinutes, int retryIntervalMinutes, string retryTextPath)
        {
            this._enableRetry = true;
            this.retryTermMinutes = retryTermMinutes;
            this.retryIntervalMinutes = retryIntervalMinutes;
            this.retryTextPath = retryTextPath;
        }

        public void SetDisableRetry()
        {
            this._enableRetry = false;
        }

        public Encoding backupFileEncoding
        {
            get
            {
                return ErsEncoding.ShiftJIS;
            }
        }
    }
}
