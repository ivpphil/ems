﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security;
using System.Runtime.Serialization;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSmtpConnectionException
        : Exception
    {
        public ErsSmtpConnectionException()
            : base()
        {
        }

        public ErsSmtpConnectionException(string message)
            : base(message)
        {
        }

        [SecuritySafeCritical]
        protected ErsSmtpConnectionException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public ErsSmtpConnectionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
