﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSmtpRetryTextManager
    {
        /// <summary>
        /// Get values to retry from file stream
        /// </summary>
        /// <param name="streamMailText"></param>
        /// <param name="enc"></param>
        /// <returns></returns>
        public static ErsSmtpRetryTextContent ParseRetryText(FileStream streamMailText, Encoding enc)
        {
            var content = new ErsSmtpRetryTextContent();

            string fileContents;
            using (var reader = new StreamReader(streamMailText, enc))
            {
                fileContents = reader.ReadToEnd();
            }

            // get index of each value (if the field is omitted, uses next index.)
            var beginOfBody = fileContents.IndexOf("\"body\",");
            if (beginOfBody < 0)
            {
                beginOfBody = 0;
            }

            var beginOfReply = fileContents.IndexOf("\"reply\",");
            if (beginOfReply < 0)
            {
                beginOfReply = beginOfBody;
            }

            var beginOfSubject = fileContents.IndexOf("\"subject\",");
            if (beginOfSubject < 0)
            {
                beginOfSubject = beginOfReply;
            }

            var beginOfCc = fileContents.IndexOf("\"cc\",");
            if (beginOfCc < 0)
            {
                beginOfCc = beginOfSubject;
            }

            var beginOfBcc = fileContents.IndexOf("\"bcc\",");
            if (beginOfBcc < 0)
            {
                beginOfBcc = beginOfCc;
            }

            var beginOfRcpt = fileContents.IndexOf("\"rcpt\",");
            if (beginOfRcpt < 0)
            {
                beginOfRcpt = beginOfBcc;
            }


            var strFrom = GetString(fileContents, "\"from\",", 0, beginOfRcpt);
            content.mailFrom = RemoveHeaderFooter(strFrom, "\"", "\"\r\n");

            var strRcpt = GetString(fileContents, "\"rcpt\",", beginOfRcpt, beginOfBcc);
            content.mailTo = RemoveHeaderFooter(strRcpt, "\"", "\"\r\n");

            var strBcc = GetString(fileContents, "\"bcc\",", beginOfBcc, beginOfCc);
            content.bcc = RemoveHeaderFooter(strBcc, "\"under\"\r\n", "BccEnd\r\n").Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            var strCc = GetString(fileContents, "\"cc\",", beginOfCc, beginOfSubject);
            content.cc = RemoveHeaderFooter(strCc, "\"under\"\r\n", "CcEnd\r\n").Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            var strSubject = GetString(fileContents, "\"subject\",", beginOfSubject, beginOfReply);
            content.subject = RemoveHeaderFooter(strSubject, "\"", "\"\r\n");

            var strReply = GetString(fileContents, "\"reply\",", beginOfReply, beginOfBody);
            content.replyto = RemoveHeaderFooter(strReply, "\"", "\"\r\n");

            var strBody = GetString(fileContents, "\"body\",", beginOfBody, fileContents.Length);
            content.body = RemoveHeaderFooter(strBody, "\"under\"\r\n", null);

            return content;
        }

        /// <summary>
        /// Get value removed header and footer
        /// </summary>
        /// <param name="value"></param>
        /// <param name="header"></param>
        /// <param name="footer"></param>
        /// <returns></returns>
        private static string RemoveHeaderFooter(string value, string header, string footer)
        {
            if (!value.HasValue())
            {
                return null;
            }

            return Regex.Replace(value, string.Format("{0}(.*){1}", header, footer), "$1", RegexOptions.Singleline);
        }

        /// <summary>
        /// Get values located in specified index and remove header for the value.
        /// </summary>
        /// <param name="fileContents"></param>
        /// <param name="targetHeader"></param>
        /// <param name="idxFrom"></param>
        /// <param name="idxTo"></param>
        /// <returns></returns>
        private static string GetString(string fileContents, string targetHeader, int idxFrom, int idxTo)
        {
            var value = fileContents.Substring(idxFrom, idxTo - idxFrom);

            if (!value.HasValue())
            {
                return null;
            }

            return value.Substring(targetHeader.Length);
        }

        /// <summary>
        /// Output text for retry into temporary directory.
        /// </summary>
        public static string OutputRetryText(string retryTextPath, ErsSmtpRetryTextContent content, Encoding enc)
        {
            try
            {
                var sb = new StringBuilder();

                //from
                sb.AppendFormat("\"from\",\"{0}\"\r\n", content.mailFrom);

                //to
                sb.AppendFormat("\"rcpt\",\"{0}\"\r\n", content.mailTo);

                //bcc
                string strBcc = string.Empty;

                if (content.bcc != null)
                {
                    foreach (var val in content.bcc)
                    {
                        if (!string.IsNullOrEmpty(val))
                            strBcc += val + Environment.NewLine;
                    }
                }
                sb.AppendFormat("\"bcc\",\"under\"\r\n");
                sb.Append(strBcc);
                sb.AppendFormat("BccEnd\r\n");

                //cc
                string strCc = string.Empty;

                if (content.cc != null)
                {
                    foreach (var val in content.cc)
                    {
                        if (!string.IsNullOrEmpty(val))
                            strCc += val + Environment.NewLine;
                    }
                }
                sb.AppendFormat("\"cc\",\"under\"\r\n");
                sb.Append(strCc);
                sb.AppendFormat("CcEnd\r\n");

                //subject
                sb.AppendFormat("\"subject\",\"{0}\"\r\n", content.subject);

                //reply_to
                if (!string.IsNullOrEmpty(content.replyto))
                {
                    sb.AppendFormat("\"reply\",\"{0}\"\r\n", content.replyto);
                }

                //body
                sb.AppendFormat("\"body\",\"under\"\r\n");
                sb.Append(content.body);

                return ErsFile.WriteAllWithRandomName(retryTextPath, "txt", sb.ToString(), enc);
            }
            catch (Exception e)
            {
                System.Diagnostics.EventLog.WriteEntry("ErsSmtp", e.ToString(), System.Diagnostics.EventLogEntryType.Error);
                return null;
            }
        }

        /// <summary>
        /// Delete retry text
        /// </summary>
        /// <param name="tempRetryTextFilePath"></param>
        public static void DeleteRetryText(string tempRetryTextFilePath)
        {
            try
            {
                File.Delete(tempRetryTextFilePath);
            }
            catch (Exception e)
            {
                System.Diagnostics.EventLog.WriteEntry("ErsSmtp", e.ToString(), System.Diagnostics.EventLogEntryType.Error);
            }
        }
    }
}
