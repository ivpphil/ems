﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsMailViewContext
         : ErsViewContext
    {
        protected IErsModelBase model;

		/// <summary>
		/// Initialize model
		/// </summary>
		/// <param name="model"></param>
        public void Init(IErsModelBase model, Dictionary<string, IErsModelBase> additionalModels)
        {
            this.model = model;
            this.additionalModels = additionalModels;
        }

        Dictionary<string, IErsModelBase> additionalModels;

        public override ViewDataDictionary GetViewData(ViewContext viewContext)
        {
            ViewDataDictionary dic;
            if(viewContext != null && viewContext.ViewData != null)
            {
                dic = viewContext.ViewData;
            }
            else
            {
                dic = new ViewDataDictionary();
            }

            //設定ファイルの値をセット
			//Set the value in the configuration file
            this.GetModel(dic);

            return dic;
        }

        public virtual void GetModel(ViewDataDictionary dic)
        {
            //Modelをセット
            //Set the Model
            dic["Model"] = model.GetPropertiesAsDictionary();

            //addtionalModel
            if (this.additionalModels != null)
            {
                foreach (var key in this.additionalModels.Keys)
                {
                    dic[key] = additionalModels[key];
                }
            }
        }
    }
}
