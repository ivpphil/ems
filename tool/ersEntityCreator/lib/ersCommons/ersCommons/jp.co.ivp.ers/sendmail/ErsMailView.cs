﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom.Compiler;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.compile;
using jp.co.ivp.ers.util;
using System.Web;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsMailView
        : ErsViewShared
    {
        protected virtual string message { get; set; }

		/// <summary>
		/// Initialize sendmail
		/// </summary>
        public virtual ErsViewShared Init(string path, ErsViewContext context, string message)
        {
            this.message = message;
            return base.Init(path, context, new SetupConfigReader().site_type);
        }
        
        /// <summary>
        /// テンプレートを出力する
		/// Outputs a template
        /// </summary>
        /// <param name="viewContext"></param>
        /// <param name="writer"></param>
        public virtual void Render(System.IO.TextWriter writer)
        {
            var newViewContext = context.GetViewData(null);
            this.GetStatic(newViewContext);

            var compiler = this.GetCompiler();

            var func = compiler.GetDelegate();

            func(writer, context, newViewContext, null);
        }

        private ErsTemplateCompiler GetCompiler()
        {
            var doCompile = !_path.HasValue();

            if (!_path.HasValue())
            {
                _path = "dummy";
            }

            var compiler = new ErsTemplateCompiler(context, _path);

            //コンパイル済みテンプレートがない場合は新たにコンパイル
            if (doCompile || !compiler.IsCompiled)
            {
                var templateParser = new ErsViewCompilerFactory().GetErsMailParser(context, this.enc);

                compiler.Compile(templateParser, message);
            }

            return compiler;
        }

        /// <summary>
        /// 設定ファイル値をViewDataにセットする
        /// <para>ViewData configuration file is set to the value</para>
        /// </summary>
        /// <param name="viewData"></param>
        protected override void GetStatic(IDictionary<string, Object> viewData)
        {
            var setup = new SetupConfigReader();

            viewData["nor_url"] = setup.nor_url;

            viewData["sec_url"] = setup.sec_url;

            if (!ErsCommonContext.IsBatch)
            {
                viewData["dynamic_url"] = (HttpContext.Current.Request.IsSecureConnection) ? setup.sec_url : setup.nor_url;
                viewData["pc_dynamic_url"] = (HttpContext.Current.Request.IsSecureConnection) ? setup.pc_sec_url : setup.pc_nor_url;
            }

            foreach (string key in SetupConfigReader.GetSiteConfigFileKeys())
            {
                //既に値がある場合はセットしない
                if (!viewData.ContainsKey(key))
                {
                    viewData[key] = SetupConfigReader.GetSiteConfigFileValue(key);
                }
            }

            foreach (string key in SetupConfigReader.GetCommonConfigFileKeys())
            {
                //既に値がある場合はセットしない
                if (!viewData.ContainsKey(key))
                {
                    viewData[key] = SetupConfigReader.GetCommonConfigFileValue(key);
                }
            }
        }
    }
 }
