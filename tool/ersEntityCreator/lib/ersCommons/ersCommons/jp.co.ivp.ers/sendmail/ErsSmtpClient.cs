﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.sendmail
{
    public class ErsSmtpClient
        : IDisposable
    {
        private ErsTcpClient tcpClient;
        protected Encoding encoding { get; set; }
        private bool sentQuit = true;
        private static readonly char[] errorPrefixCode = new[] { '2', '3' };
        private static readonly Encoding defaultEncoding = ErsEncoding.ISO2022JP;

        private ErsSmtpClient(Encoding encoding)
        {
            this.encoding = encoding;
            this.tcpClient = new ErsTcpClient(50000);
        }

        /// <summary>
        /// Connect to smtp server
        /// </summary>
        /// <param name="hostName"></param>
        /// <param name="port"></param>
        public static ErsSmtpClient GetConnect(string hostName, int port)
        {
            return GetConnect(hostName, port, defaultEncoding);
        }

        /// <summary>
        /// Connect to smtp server
        /// </summary>
        /// <param name="hostName"></param>
        /// <param name="port"></param>
        public static ErsSmtpClient GetConnect(string hostName, int port, Encoding encoding)
        {
            try
            {
                var smtpClient = new ErsSmtpClient(encoding);
                smtpClient.tcpClient.Connect(hostName, port, encoding);
                return smtpClient;
            }
            catch (Exception e)
            {
                throw new ErsSmtpConnectionException("error at connect", e);
            }
        }

        /// <summary>
        ///  Send message to smtp server
        /// </summary>
        /// <param name="value"></param>
        protected virtual void SendMessage(string value)
        {
            var result = this.tcpClient.SendMessage(value + Environment.NewLine);

            if (string.IsNullOrWhiteSpace(result))
            {
                throw new Exception("result from server is empty");
            }

            var arrResult = this.SplitToLine(result);

            foreach (var line in arrResult)
            {
                if (!string.IsNullOrEmpty(line) && !errorPrefixCode.Contains(line.ElementAt(0)))
                {
                    throw new Exception(result);
                }
            }
        }

        /// <summary>
        /// Send helo
        /// </summary>
        public virtual void SendHelo()
        {
            try
            {
                this.SendMessage("helo ivp.ne.jp");
                this.sentQuit = false;
            }
            catch (Exception e)
            {
                throw new Exception("error at helo", e);
            }
        }

        /// <summary>
        /// Send auth
        /// </summary>
        public virtual void SendAuth(string smtpAuthId, string smtpAuthPass)
        {
            try
            {
                string strAuth = this.ToBase64(smtpAuthId + '\0' + smtpAuthId + '\0' + smtpAuthPass);

                this.SendMessage("auth plain " + strAuth);
            }
            catch (Exception e)
            {
                throw new Exception("error at auth", e);
            }
        }

        /// <summary>
        /// Send mail from
        /// </summary>
        public virtual void SendMailFrom(string mailFrom)
        {
            try
            {
                this.SendMessage("mail from:" + ErsSmtpClient.GetEmailPart(mailFrom));
            }
            catch (Exception e)
            {
                throw new Exception("error at mail from", e);
            }
        }

        /// <summary>
        /// Send receipt to
        /// </summary>
        public virtual void SendReceiptTo(string rcpt, IEnumerable<string> cc, IEnumerable<string> bcc)
        {
            try
            {
                this.SendMessage("rcpt to:" + ErsSmtpClient.GetEmailPart(rcpt)); //send rcpt to:

                if (cc != null)
                {
                    foreach (var email in cc)
                    {
                        if (!string.IsNullOrEmpty(email))
                            this.SendMessage("rcpt to:" + ErsSmtpClient.GetEmailPart(email)); //send rcpt to:
                    }
                }

                if (bcc != null)
                {
                    foreach (var email in bcc)
                    {
                        if (!string.IsNullOrEmpty(email))
                            this.SendMessage("rcpt to:" + ErsSmtpClient.GetEmailPart(email)); //send rcpt to:
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("error at rcpt to", e);
            }
        }

        /// <summary>
        /// Send data
        /// </summary>
        /// <param name="replyTo"></param>
        /// <param name="mailFrom"></param>
        /// <param name="rcpt"></param>
        /// <param name="cc"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        public virtual void SendData(string replyTo, string mailFrom, string rcpt, IEnumerable<string> cc, string subject, string body, string htmlBody)
        {
            try
            {
                var bodyData = new StringBuilder();
                bodyData.AppendLine("data");
                if (!string.IsNullOrEmpty(replyTo))
                {
                    bodyData.AppendLine("Reply-To: " + this.ConvertEmailToMIME(replyTo));
                }
                bodyData.AppendLine("From: " + this.ConvertEmailToMIME(mailFrom));
                bodyData.AppendLine("To: " + this.ConvertEmailToMIME(rcpt));
                if (cc != null)
                {
                    bodyData.Append("Cc: ");
                    foreach (var email in cc)
                    {
                        if (!string.IsNullOrEmpty(email))
                            bodyData.Append(this.ConvertEmailToMIME(email) + ",");
                    }
                    bodyData.Remove(bodyData.Length - 1, 1);
                    bodyData.AppendLine();
                }
                bodyData.AppendLine("Subject:" + this.ToMime(subject));

                //determin if the body is alternative or not.
                if (string.IsNullOrEmpty(htmlBody))
                {
                    this.CreateBody(bodyData, body);
                }
                else
                {
                    this.CreateBody(bodyData, subject, body, htmlBody);
                }

                bodyData.Append(".");

                this.SendMessage(bodyData.ToString());
            }
            catch (Exception e)
            {
                throw new Exception("error at data", e);
            }
        }

        /// <summary>
        /// Create Plane text mail body
        /// </summary>
        /// <param name="body"></param>
        /// <param name="bodyData"></param>
        protected virtual void CreateBody(StringBuilder bodyData, string body)
        {
            bodyData.AppendLine("Content-Type: text/plain;charset=" + this.encoding.BodyName);
            bodyData.AppendLine("Content-Transfer-Encoding: 7bit");
            bodyData.AppendLine();
            bodyData.AppendLine(this.ToJis(this.EscapeDot(body)));
        }

        /// <summary>
        /// Create Alternative mail body
        /// </summary>
        /// <param name="body"></param>
        /// <param name="bodyData"></param>
        protected virtual void CreateBody(StringBuilder bodyData, string subject, string body, string htmlBody)
        {
            var boundary = "__NextPart__" + DateTime.Now.ToString("yyyyMMddHHmmssfffffff");

            bodyData.AppendLine("Content-Type: multipart/alternative; boundary=" + boundary);
            bodyData.AppendLine();
            bodyData.AppendLine("This is a multi-part message in MIME format.");
            bodyData.AppendLine();
            bodyData.AppendLine("--" + boundary);

            CreateBody(bodyData, body);

            bodyData.AppendLine();
            bodyData.AppendLine("--" + boundary);
            bodyData.AppendLine("Content-Type: text/html;charset=" + this.encoding.BodyName);
            bodyData.AppendLine("Content-Transfer-Encoding: 7bit");
            bodyData.AppendLine();
            bodyData.AppendLine("<html>");
            bodyData.AppendLine("<head>");
            bodyData.AppendLine("<title>" + this.ToJis(this.EscapeDot(subject)) + "</title>");
            bodyData.AppendLine("<meta http-equiv=\"content-type\" content=\"text/html; charset=" + this.encoding.BodyName + "\">");
            bodyData.AppendLine("</head>");
            bodyData.AppendLine("<body>");
            bodyData.AppendLine(this.ToJis(this.EscapeDot(htmlBody)));
            bodyData.AppendLine("</body>");
            bodyData.AppendLine("</html>");

            bodyData.AppendLine("--" + boundary + "--");
        }

        /// <summary>
        /// send quit
        /// </summary>
        public virtual void SendQuit()
        {
            try
            {
                this.SendMessage("quit");
                this.sentQuit = true;
            }
            catch
            {
                // 送信済みの為スルー
                //throw new Exception("error at quit", e);
            }
        }

        /// <summary>
        /// Get email part from mail address string
        /// </summary>
        /// <param name="mailAddress"></param>
        /// <returns></returns>
        public static string GetEmailPart(string mailAddress)
        {
            var matchs = SplitMailPart(mailAddress);
            if (matchs.Count == 0)
            {
                return mailAddress.Trim();
            }

            var emailPart = matchs[0].Groups[2].Value;

            return emailPart.Trim();
        }

        /// <summary>
        /// Get name part from mail address string
        /// </summary>
        /// <param name="mailAddress"></param>
        /// <returns></returns>
        public static string GetNamePart(string mailAddress)
        {
            var matchs = SplitMailPart(mailAddress);
            if (matchs.Count == 0)
            {
                return null;
            }

            var namePart = matchs[0].Groups[1].Value;

            return namePart.Trim();
        }

        /// <summary>
        /// Split part of email
        /// </summary>
        /// <param name="mailAddress"></param>
        /// <returns></returns>
        private static MatchCollection SplitMailPart(string mailAddress)
        {
            string operationTagStart = "^(.+)?<(.+)>$";
            var matchs = new Regex(operationTagStart, RegexOptions.IgnoreCase).Matches(mailAddress);
            return matchs;
        }

        /// <summary>
        /// convert jis part to mime
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        protected virtual string ConvertEmailToMIME(string emailAddress)
        {
            var email = ErsSmtpClient.GetEmailPart(emailAddress);
            var name = ErsSmtpClient.GetNamePart(emailAddress);

            if (string.IsNullOrEmpty(name))
            {
                return email;
            }
            else
            {
                return string.Format("{0} <{1}>", this.ToMime(name), email);
            }
        }

        /// <summary>
        /// Convert to Mime
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        protected virtual string ToMime(string value)
        {
            string base64str = this.ToBase64(value);
            return string.Format("=?{0}?B?{1}?=", this.encoding.BodyName, base64str);
        }

        /// <summary>
        /// Convert to Base64
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string ToBase64(string value)
        {
            Encoding srcEnc = Encoding.UTF8;
            byte[] srcBytes = srcEnc.GetBytes(value);

            byte[] destBytes = Encoding.Convert(srcEnc, this.encoding, srcBytes);

            return Convert.ToBase64String(destBytes);
        }

        /// <summary>
        /// Convert to Jis
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        protected virtual string ToJis(string value)
        {
            Encoding srcEnc = Encoding.UTF8;
            byte[] srcBytes = srcEnc.GetBytes(value);

            byte[] destBytes = Encoding.Convert(srcEnc, this.encoding, srcBytes);

            return this.encoding.GetString(destBytes);
        }

        /// <summary>
        /// Implements for IDisposable
        /// </summary>
        public void Dispose()
        {
            if (this.tcpClient != null)
            {
                if (!this.sentQuit)
                {
                    this.tcpClient.Send("quit");
                    this.sentQuit = true;
                }
                this.tcpClient.Dispose();
                this.tcpClient = null;
            }
        }

        /// <summary>
        /// 本文中のドットはエスケープする
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        protected virtual string EscapeDot(string body)
        {
            var arrBody = this.SplitToLine(body);
            var result = new StringBuilder();
            foreach (var line in arrBody)
            {
                if (line.StartsWith("."))
                {
                    result.Append(".");
                }
                result.AppendLine(line);
            }
            return result.ToString();
        }

        /// <summary>
        /// 本文を1行ごとに分割
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        protected string[] SplitToLine(string body)
        {
            return body.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
        }
    }
}
