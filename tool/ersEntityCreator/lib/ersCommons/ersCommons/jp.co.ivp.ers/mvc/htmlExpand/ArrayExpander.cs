﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc.htmlExpand
{
    public class ArrayExpander
        : HtmlExpander
    {
		/// <summary>
		/// Determine if specified object is an Array
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
        protected override bool IfHandleValueOf(object value)
        {
            return value is Array;
        }

		/// <summary>
		/// Concatenate the values of an array object to a single string
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
        protected override object Expand(object value)
        {
            var joinValue = string.Empty;
            foreach (var obj in (Array)value)
            {
                joinValue += "," + Convert.ToString(obj);
            }
            if (string.IsNullOrEmpty(joinValue))
                return string.Empty;
            else
                return joinValue.Substring(1);
        }
    }
}
