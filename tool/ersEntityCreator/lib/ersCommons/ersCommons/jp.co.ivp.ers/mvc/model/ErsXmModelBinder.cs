﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Win32;
using System.IO;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using Microsoft.VisualBasic.FileIO;
using System.Xml;
using System.Reflection;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.model
{
    public class ErsXmlModelBinder
    {
        /// <summary>
        /// ModelにXMLをバインドする
        /// </summary>
        /// <param name="xmlDoc">XmlDocument</param>
        /// <param name="modelBase">IErsModelBase</param>
        public static void Bind(XmlDocument xmlDoc, IErsModelBase modelBase)
        {
            XmlElement elmRoot = xmlDoc.DocumentElement;
            var modelType = modelBase.GetType();

            var dic = GetPropertyDicFromXmlNode(modelType, elmRoot);

            ErsBindModel.ModelBind(modelBase, dic, false);
        }

        /// <summary>
        /// バインドしたディクショナリを返す
        /// </summary>
        /// <param name="xmlDoc">XmlDocument</param>
        /// <param name="modelType">モデルのタイプ</param>
        /// <param name="repeat_node">XMLノードリピートフラグ</param>
        /// <returns>List&lt;object&gt;</returns>
        public static List<object> GetBindDictionaryList(XmlDocument xmlDoc, Type modelType, bool repeat_node = true)
        {
            var retList = new List<object>();

            XmlElement elmRoot = xmlDoc.DocumentElement;

            if (repeat_node)
            {
                if (elmRoot.HasChildNodes)
                {
                    for (var i = 0; i < elmRoot.ChildNodes.Count; i++)
                    {
                        var dic = GetPropertyDicFromXmlNode(modelType, elmRoot.ChildNodes[i]);
                        retList.Add(dic);
                    }
                }
            }
            else
            {
                var dic = GetPropertyDicFromXmlNode(modelType, elmRoot);
                retList.Add(dic);
            }

            return retList;
        }

        /// <summary>
        /// XML子ノードからプロパティのディクショナリを取得する。
        /// </summary>
        /// <param name="modelType">モデルのタイプ</param>
        /// <param name="node">XML子ノード</param>
        /// <returns>ディクショナリ</returns>
        public static Dictionary<string, object> GetPropertyDicFromXmlNode(Type modelType, XmlNode node)
        {
            Dictionary<string, object> retDic = new Dictionary<string, object>();

            // モデルのプロパティベースでセットする
            foreach (var prop in modelType.GetProperties())
            {
                if (prop.GetCustomAttributes(typeof(XmlFieldAttribute), false).Length == 0)
                {
                    continue;
                }

                XmlNode tmpNode = node[prop.Name];

                if (tmpNode == null)
                {
                    // ノードがルートの場合
                    if (node.Name == prop.Name)
                    {
                        tmpNode = node;
                    }
                    else
                    {
                        continue;
                    }
                }

                SetPropertyValueFromXmlNode(prop, tmpNode, retDic);
            }

            return retDic;
        }

        /// <summary>
        /// XMLノードからプロパティの値をセットする。
        /// </summary>
        /// <param name="prop">プロパティ</param>
        /// <param name="node">ノード</param>
        /// <param name="dic">ディクショナリ</param>
        /// <param name="name">ディクショナリキー（指定しない場合はプロパティ名）</param>
        public static void SetPropertyValueFromXmlNode(PropertyInfo prop, XmlNode node, Dictionary<string, object> dic, string name = null, string parentRecordKey= null)
        {
            parentRecordKey = parentRecordKey ?? string.Empty;

            var obj = default(object);

            // BindTable
            var attributes = prop.GetCustomAttributes(typeof(BindTableAttribute), false);
            if (attributes != null & attributes.Count() != 0)
            {
                var bindTable = (BindTableAttribute)attributes.First();
                if (node.HasChildNodes)
                {
                    var t = prop.PropertyType.GetGenericArguments()[0];

                    foreach (var propB in t.GetProperties())
                    {
                        if (propB.GetCustomAttributes(typeof(XmlFieldAttribute), false).Length == 0)
                        {
                            continue;
                        }

                        for (var i = 0; i < node.ChildNodes.Count; i++)
                        {
                            var tmpNode = node.ChildNodes[i][propB.Name];

                            if (tmpNode == null)
                            {
                                continue;
                            }

                            var recordKey = string.Format("{0}_{1}_", parentRecordKey + BindTableAttribute.record_key + bindTable.table_name, i);

                            // Record value
                            SetPropertyValueFromXmlNode(propB, tmpNode, dic, recordKey + propB.Name, recordKey);
                        }
                    }

                    // Record key
                    dic[parentRecordKey + BindTableAttribute.record_key + prop.Name] = BindTableAttribute.record_key + bindTable.table_name;
                }

                return;
            }
            // Array
            else if (prop.PropertyType.IsArray)
            {
                if (node.HasChildNodes)
                {
                    List<string> listStr = new List<string>();

                    for (var i = 0; i < node.ChildNodes.Count; i++)
                    {
                        listStr.Add(node.ChildNodes[i].InnerText);
                    }
                    obj = String.Join(",", listStr);
                }
                else
                {
                    obj = null;
                }
            }
            // Else object
            else
            {
                obj = node.InnerText;
            }

            if (name == null)
            {
                name = prop.Name;
            }

            dic[name] = obj;
        }

        /// <summary>
        /// ModelにXMLをバインドする
        /// </summary>
        /// <param name="xmlFilePath">Filepath of XmlDocument</param>
        /// <param name="modelBase">IErsModelBase</param>
        public static void Bind(string xmlFilePath, IErsModelBase modelBase)
        {
            var target = GetTargetDocument(xmlFilePath);
            Bind(target, modelBase);
        }

        /// <summary>
        /// Read xml document of each newspaper
        /// </summary>
        /// <param name="newspaper_code"></param>
        /// <returns></returns>
        private static XmlDocument GetTargetDocument(string xmlFilePath, Encoding encoding = null)
        {
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }

            //XMLのロード
            var doc = new XmlDocument();
            using (var sr = new StreamReader(xmlFilePath, encoding))
            {
                doc.Load(sr);
                sr.Close();
            }

            return doc;
        }
    }
}
