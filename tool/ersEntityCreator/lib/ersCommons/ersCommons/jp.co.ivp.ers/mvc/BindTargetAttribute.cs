﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers
{
    /// <summary>
    /// ErsModel.OverwriteWithParameter() 及び ErsModel.GetPropertiesAsDictionary() の対象となるプロパティを指定します。
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class BindTargetAttribute
        : Attribute
    {
        public string bindTargetName { get; private set; }

        public BindTargetAttribute(string bindTargetName)
        {
            this.bindTargetName = bindTargetName;
        }
    }
}
