﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc.CommandProcessor.Command;
using jp.co.ivp.ers.db;

namespace jp.co.ivp.ers.mvc.CommandProcessor.Dispatcher
{
    public class DefaultCommandBus : ICommandBus
    {
        public ICommandResult Submit<TCommand>(TCommand command, EnumCommandTransaction beginTransaction) where TCommand : ICommand
        {
            using (var transaction = BeginTransaction(beginTransaction))
            {
                var handlers = DependencyResolver.Current.GetServices<ICommandHandler<TCommand>>();
                if (handlers == null || handlers.Count() == 0)
                {
                    throw new CommandHandlerNotFoundException(typeof(TCommand));
                }

                foreach (var handler in handlers)
                {
                    if (!(handler is ICommandHandler<TCommand>))
                    {
                        throw new CommandHandlerNotFoundException(typeof(TCommand));
                    }
                    if (handler.Submit(command) == new CommandResult(false))
                    {
                        return new CommandResult(false);
                    }
                }

                if (beginTransaction == EnumCommandTransaction.BeginTransaction)
                {
                    transaction.Commit();
                }
            }
            return new CommandResult(true);
 
        }

        private static ErsConnection BeginTransaction(EnumCommandTransaction beginTransaction)
        {
            if (beginTransaction == EnumCommandTransaction.BeginTransaction)
            {
                return ErsDB_parent.BeginTransaction();
            }

            return null;
        }

        public IEnumerable<ValidationResult> Validate<TCommand>(TCommand command) where TCommand : ICommand
        {
            var handler = DependencyResolver.Current.GetService<IValidationHandler<TCommand>>();
            if (!((handler != null) && handler is IValidationHandler<TCommand>))
            {
                throw new ValidationHandlerNotFoundException(typeof(TCommand));
            }
            return handler.Validate(command);
        }
    }
}

