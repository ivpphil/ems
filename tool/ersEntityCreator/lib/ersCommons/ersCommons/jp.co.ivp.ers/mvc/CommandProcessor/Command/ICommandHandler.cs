﻿namespace jp.co.ivp.ers.mvc.CommandProcessor.Command
{
    public interface ICommandHandler<in TCommand> where TCommand: ICommand
    {
        ICommandResult Submit(TCommand command);

    }
}

