﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.CodeDom.Compiler;
using jp.co.ivp.ers.util;
using System.Reflection;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.htmlExpand;

namespace jp.co.ivp.ers.mvc
{
    /// <summary>
    /// View実行時の処理を記述する。プログラムから呼ぶ。
	/// <para>Describes the run-time processing of View. called from the program</para>
    /// </summary>
    public class ErsViewContext
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="context"></param>
        public ErsViewContext()
        {
        }

        /// <summary>
        /// DLLのPathを取得する。（WEBはbin以下。バッチはexeと同位置）
        /// </summary>
        /// <param name="dllName"></param>
        /// <returns></returns>
        protected internal virtual string GetDllPath(string dllName)
        {
            return ErsCommonContext.MapPath("~/bin/" + dllName);
        }

        /// <summary>
        /// ViewDataDictionaryを返す。
        /// <para>Returns ViewDataDictionary.</para>
        /// </summary>
        /// <param name="viewContext"></param>
        /// <returns></returns>
        public virtual ViewDataDictionary GetViewData(ViewContext viewContext)
        {
            if (viewContext.ViewData.Model == null)
            {
                viewContext.ViewData.Model = new ErsErrorModel();
            }

            this.GetModel(viewContext);
            this.GetError(viewContext);
            this.GetInformation(viewContext);
            this.GetOutputHidden(viewContext);
            this.GetSessionHidden(viewContext);

            //addtionalModel
            viewContext.ViewData.Remove(addtionalModelKey);

            return viewContext.ViewData;
        }

        /// <summary>
		/// ModelをViewDataにセットする / Set ViewData to a Model
        /// </summary>
        /// <param name="viewContext"></param>
        /// <returns></returns>
        public virtual void GetModel(ViewContext viewContext)
        {
            var viewData = viewContext.ViewData;

            object model = viewData.Model;

            viewData["Model"] = model;

            //addtionalModel
            var dicModels = (Dictionary<string, ErsModelBase>)viewData[addtionalModelKey];
            if (dicModels != null)
            {
                foreach (var key in dicModels.Keys)
                {
                    viewData[key] = dicModels[key];
                }
            }
        }
        /// <summary>
        /// Viewに渡す追加のModel
        /// </summary>
        public const string addtionalModelKey = "addtionalModel";

        /// <summary>
        /// ModelのエラーをViewDataにセットする
		/// <para>Set error ViewData to the Model</para>
        /// </summary>
        /// <param name="viewContext"></param>
        /// <returns></returns>
        public virtual void GetError(ViewContext viewContext)
        {
            var viewData = viewContext.ViewData;

            var model = (ErsModelBase)viewData.Model;

            if (!(model is ErsModelBase))
            {
                return;
            }

            var dicModelState = (ModelStateDictionary)viewData.ModelState;

            foreach (string property in dicModelState.Keys)
            {
                if (!dicModelState.IsValidField(property))
                {
                    foreach (ModelError e in dicModelState[property].Errors)
                    {
                        model.AddInvalidField(property, e.ErrorMessage);
                    }
                }
            }
        }

        private void GetInformation(ViewContext viewContext)
        {
            if (!viewContext.ViewData.ContainsKey("ers_information_massage_list"))
            {
                return;
            }
            var listInformation = (List<string>)viewContext.ViewData["ers_information_massage_list"];
            viewContext.ViewData["ers_information_massage"] = string.Join(ErsViewHelper.TAG_NEW_LINE, listInformation).Replace(Environment.NewLine, ErsViewHelper.TAG_NEW_LINE);
        }

        /// <summary>
        /// hidden出力項目をViewDataにセットする
		/// <para>ViewData item set to hidden output</para>
        /// </summary>
        protected virtual void GetOutputHidden(ViewContext viewContext)
        {
            var viewData = viewContext.ViewData;

            //view
            object model = viewData.Model;

            if (!(model is ErsModelBase))
            {
                return;
            }

            //各モデルのHidden値を取得
            var listTarget = new List<ErsOutputHiddenTarget>();

            if (model != null)
            {
                ((ErsModelBase)model).GetOutputHidden(listTarget);
            }

            //addtionalModel
            var dicModel = (Dictionary<string, ErsModelBase>)viewData[addtionalModelKey];
            if (dicModel != null)
            {
                foreach (var addModel in dicModel.Values)
                {
                    if (addModel != null)
                    {
                        addModel.GetOutputHidden(listTarget);
                    }
                }
            }

            viewData["OutputHidden"] = listTarget;
        }

        /// <summary>
        /// セッション情報出力項目をViewDataにセットする
		/// <para>ViewData item set to output session information</para>
        /// </summary>
        protected virtual void GetSessionHidden(ViewContext viewContext)
        {
            var viewData = viewContext.ViewData;

            var state = (ErsState)viewData["SessionStateHidden"];

            //sessionの値
            var secureList = new List<Dictionary<string, object>>();
            var normalList = new List<Dictionary<string, object>>();
            if (state != null)
            {
                state.OutputHidden(secureList, true);
                state.OutputHidden(normalList, false);
            }
            viewData["SecureHidden"] = secureList;
            viewData["NormalHidden"] = normalList;
        }
    }

}