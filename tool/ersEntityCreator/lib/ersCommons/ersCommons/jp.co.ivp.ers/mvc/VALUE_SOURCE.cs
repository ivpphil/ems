﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc
{
    public enum VALUE_SOURCE
    {
        FORM,
        QUERY_STRING,
        COOKIES,
        FORM_QUERY_STRING,
    }
}
