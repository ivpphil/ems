﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc.MapperProcessor.Mapper
{
    public class MapperNotFoundException : Exception
    {
        public MapperNotFoundException(Type mappableType)
            : base(string.Format("mapper not found for command type: {0}", mappableType))
        {
        }
  }
}
