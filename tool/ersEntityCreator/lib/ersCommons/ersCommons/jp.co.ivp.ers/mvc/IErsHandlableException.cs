﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace jp.co.ivp.ers
{
    public interface IErsHandlableException
    {
        string returnUrl { get;}

        mvc.ErsModelBase model { get; }

        HttpStatusCode? httpStatus { get; }

        string Message { get; }

        bool isNoBackTo { get; set; }
    }
}
