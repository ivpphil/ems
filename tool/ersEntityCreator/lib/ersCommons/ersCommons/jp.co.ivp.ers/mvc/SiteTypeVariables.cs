﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.IO;

namespace jp.co.ivp.ers.mvc
{
    public class SiteTypeVariables
    {
        /// <summary>
        /// Returns the configuration value of nor_url
        /// </summary>
        public static string GetNorUrl(EnumSiteType site_type)
        {
            var setup = new SetupConfigReader();
            switch (site_type)
            {
                case EnumSiteType.PC:
                    return setup.pc_nor_url;
                case EnumSiteType.MOBILE:
                    return setup.mobile_nor_url;
                case EnumSiteType.CONTACT:
                    return setup.contact_nor_url;
                case EnumSiteType.ADMIN:
                    return setup.admin_nor_url;
                case EnumSiteType.SMARTPHONE:
                    return setup.smartphone_nor_url;
                case EnumSiteType.MONITOR:
                    return setup.admin_nor_url;
                default:
                    throw new Exception("Can not decide the site type.");
            }
        }

        /// <summary>
        /// Returns the configuration value of sec_url
        /// </summary>
        public static string GetSecUrl(EnumSiteType site_type)
        {
            var setup = new SetupConfigReader();
            switch (site_type)
            {
                case EnumSiteType.PC:
                    return setup.pc_sec_url;
                case EnumSiteType.MOBILE:
                    return setup.mobile_sec_url;
                case EnumSiteType.CONTACT:
                    return setup.contact_sec_url;
                case EnumSiteType.ADMIN:
                case EnumSiteType.MONITOR:
                case EnumSiteType.BATCH:
                    return setup.admin_sec_url;
                case EnumSiteType.SMARTPHONE:
                    return setup.smartphone_sec_url;
                default:
                    throw new Exception("Can not decide the site type.");
            }
        }

        /// <summary>
        /// Returns the configuration value of sec_url
        /// </summary>
        public static string GetApplicationRoot(EnumSiteType site_type)
        {
            var setup = new SetupConfigReader();
            switch (site_type)
            {
                case EnumSiteType.PC:
                    return  Path.Combine(setup.root_path , setup.pc_application_root);
                case EnumSiteType.MOBILE:
                    return  Path.Combine(setup.root_path , setup.mobile_application_root);
                case EnumSiteType.CONTACT:
                    return  Path.Combine(setup.root_path , setup.contact_application_root);
                case EnumSiteType.ADMIN:
                case EnumSiteType.MONITOR:
                case EnumSiteType.BATCH:
                    return  Path.Combine(setup.root_path , setup.admin_application_root);
                case EnumSiteType.SMARTPHONE:
                    return Path.Combine(setup.root_path, setup.smartphone_application_root);
                default:
                    throw new Exception("Can not decide the site type.");
            }
        }
    }
}
