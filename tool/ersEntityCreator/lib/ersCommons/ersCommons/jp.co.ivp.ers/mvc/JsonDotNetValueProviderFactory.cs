﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using System.Dynamic;
using Newtonsoft.Json.Converters;
using System.Globalization;
using System.Web;
using System.Net;
using System.Collections;

namespace jp.co.ivp.ers.mvc
{
    public sealed class JsonDotNetValueProviderFactory : ValueProviderFactory
    {
        public override IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            if (controllerContext == null)
                throw new ArgumentNullException("controllerContext");

            if (!controllerContext.HttpContext.Request.ContentType.StartsWith("application/json", StringComparison.OrdinalIgnoreCase))
                return null;

            // use JSON.NET to deserialize object to a dynamic (expando) object
            object JSONObject;

            using (var reader = new StreamReader(controllerContext.HttpContext.Request.InputStream))
            {
                using (var jsonReader = new JsonTextReader(reader))
                {
                    try
                    {
                        if (!jsonReader.Read())
                            return null;

                        var jsonSerializer = new JsonSerializer();

                        // add the dyamic object converter to our serializer
                        jsonSerializer.Converters.Add(new ExpandoObjectConverter());

                        if (jsonReader.TokenType == JsonToken.StartArray)
                        {
                            // if we start with a "[", treat this as an array
                            JSONObject = jsonSerializer.Deserialize<List<ExpandoObject>>(jsonReader);
                        }
                        else
                        {
                            JSONObject = jsonSerializer.Deserialize<ExpandoObject>(jsonReader);
                        }
                    }
                    catch (JsonReaderException e)
                    {
                        throw this.CreateHttpBadRequestException(reader, e);
                    }
                    catch (ArgumentException e)
                    {
                        throw this.CreateHttpBadRequestException(reader, e);
                    }
                }
            }

            // create a backing store to hold all properties for this deserialization
            var backingStore = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            // add all properties to this backing store
            AddToBackingStore(backingStore, String.Empty, JSONObject);

            // return the object in a dictionary value provider so the MVC understands it
            return new DictionaryValueProvider<object>(backingStore, CultureInfo.CurrentCulture);
            
        }

        private HttpException CreateHttpBadRequestException(StreamReader reader, Exception e)
        {
            reader.BaseStream.Seek(0, SeekOrigin.Begin);

            // bodyの値がJson形式になっていない場合はステータス400を返す
            // return HTTP status 400 if the body data for json request isn't json format.
            return new HttpException((int)HttpStatusCode.BadRequest, "Invalid JSON primitive: " + reader.ReadToEnd(), e);
        }

        private static void AddToBackingStore(Dictionary<string, object> backingStore, string prefix, object value)
        {
            var d = value as IDictionary<string, object>;
            if (d != null)
            {
                foreach (KeyValuePair<string, object> entry in d)
                {
                    AddToBackingStore(backingStore, MakePropertyKey(prefix, entry.Key), entry.Value);
                }
                return;
            }

            var l = value as IList;
            if (l != null)
            {
                for (int i = 0; i < l.Count; i++)
                {
                    AddToBackingStore(backingStore, MakeArrayKey(prefix, i), l[i]);
                }
                return;
            }

            // primitive
            backingStore[prefix] = value;
        }

        private static string MakeArrayKey(string prefix, int index)
        {
            return prefix + "[" + index.ToString(CultureInfo.InvariantCulture) + "]";
        }

        private static string MakePropertyKey(string prefix, string propertyName)
        {
            return (String.IsNullOrEmpty(prefix)) ? propertyName : prefix + "." + propertyName;
        }
    }
}
