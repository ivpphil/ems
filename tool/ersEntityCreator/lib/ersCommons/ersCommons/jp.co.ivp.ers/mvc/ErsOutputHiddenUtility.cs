﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using jp.co.ivp.ers.mvc.compile.entity;

namespace jp.co.ivp.ers.mvc
{
    public class ErsOutputHiddenUtility
    {
        /// <summary>
        /// Viewが出力可能なhiddenのグループ名を取得する
        /// <para>If has OutputHidden property returns True.</para>
        /// </summary>
        /// <param name="model"></param>
        internal static IEnumerable<string> GetOutputHiddenGroup(Dictionary<string, bool> OutputHidden, PropertyInfo property)
        {
            if (OutputHidden.Count == 0)
            {
                //Hidden出力なしなので、チェックしない。
                //If no output hidden, disregard
                return null;
            }

            var isOutputHidden = false;
            var attributes = property.GetCustomAttributes(typeof(ErsOutputHiddenAttribute), false);
            foreach (ErsOutputHiddenAttribute attr in attributes)
            {
                foreach (var groupName in attr.groupNames)
                {
                    if (OutputHidden.ContainsKey(groupName) && !OutputHidden[groupName])
                    {
                        //明示的にfalseを指定されている場合は、出力しない
                        return null;
                    }
                    if (OutputHidden.ContainsKey(groupName) && OutputHidden[groupName])
                    {
                        isOutputHidden = true;
                    }
                }
            }

            //出力対象なし
            if (!isOutputHidden)
            {
                return null;
            }

            //出力対象のプロパティなのでグループ名を取得
            var listGroupName = new List<string>();
            foreach (ErsOutputHiddenAttribute attr in attributes)
            {
                foreach (var groupName in attr.groupNames)
                {
                    if (!listGroupName.Contains(groupName))
                    {
                        listGroupName.Add(groupName);
                    }
                }
            }

            return listGroupName;
        }

        /// <summary>
        /// Hiddenの値を保持します
        /// </summary>
        /// <param name="listTarget"></param>
        /// <param name="hiddenGroup"></param>
        /// <param name="dic"></param>
        internal static void AddHidden(List<ErsOutputHiddenTarget> listTarget, IEnumerable<string> hiddenGroup, Dictionary<string, object> dic)
        {
            foreach (var groupName in hiddenGroup)
            {
                AddHiddenValue(listTarget, new[] { dic }, groupName);
            }
        }

        /// <summary>
        /// Hiddenの値を保持している２つのErsOutputHiddenTargetを結合します
        /// </summary>
        /// <param name="listTarget"></param>
        /// <param name="listHiddenValue"></param>
        internal static void Concat(List<ErsOutputHiddenTarget> listTarget, List<ErsOutputHiddenTarget> listHiddenValue)
        {
            var listSource = new List<ErsOutputHiddenTarget>(listHiddenValue);
            foreach (var target in listTarget)
            {
                var sourceTarget = listSource.SingleOrDefault((objTarget) => objTarget.groupName == target.groupName);
                if (sourceTarget.values != null)
                {
                    target.values.AddRange(sourceTarget.values);
                    listSource.Remove(sourceTarget);
                }
            }

            foreach (var sourceTarget in listSource)
            {
                AddHiddenValue(listTarget, sourceTarget.values, sourceTarget.groupName);
            }
        }

        /// <summary>
        /// Hiddenの値を指定されたgroupNameのErsOutputHiddenTargetへ格納します。
        /// </summary>
        /// <param name="listTarget"></param>
        /// <param name="dic"></param>
        /// <param name="groupName"></param>
        private static void AddHiddenValue(List<ErsOutputHiddenTarget> listTarget, IEnumerable<Dictionary<string, object>> dic, string groupName)
        {
            var target = listTarget.SingleOrDefault((objTarget) => objTarget.groupName == groupName);
            if (!target.groupName.HasValue())
            {
                target.groupName = groupName;
                target.values = new List<Dictionary<string, object>>();
                listTarget.Add(target);
            }
            target.values.AddRange(dic);
        }

        /// <summary>
        /// グループ名を取得します（Viewにて呼ばれる）
        /// </summary>
        /// <param name="attributes"></param>
        /// <returns></returns>
        internal static string GetGroupName(ErsTagAttributes attributes)
        {
            var arrGroup_name = new List<string>() { ErsOutputHiddenAttribute.DEFAULT_GROUP_NAME };
            if (attributes.ContainsKey("group_name"))
            {
                if (attributes["group_name"].HasValue())
                {
                    arrGroup_name = attributes["group_name"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                }

                //乱数チェックは常に持ちまわす
                arrGroup_name.Add(ErsOutputHiddenAttribute.PROCESS_COMPLETION_CHECK_RANSU_KEY);

                attributes.Remove("group_name");
            }


            return "\"" + string.Join("\",\"", arrGroup_name) + "\"";

        }
    }
}
