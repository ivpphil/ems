using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using System.Xml.Xsl;
using System.Xml.Linq;
using System.IO;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Collections;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;
using System.Text;
using jp.co.ivp.ers.mvc.compile;
using jp.co.ivp.ers.mvc.template;
using System.Web.Configuration;

namespace jp.co.ivp.ers.mvc
{
    public abstract class ErsViewBase
    {
        //templateファイルパス
        protected string _path;
        protected ErsViewContext context;
        protected EnumSiteType site_type;
        protected Encoding enc;

        /// <summary>
        /// 設定ファイル値をViewDataにセットする
        /// <para>ViewData configuration file is set to the value</para>
        /// </summary>
        /// <param name="viewData"></param>
        protected abstract void GetStatic(IDictionary<string, Object> viewData);

        protected Encoding GetViewContextEncoding(EnumSiteType site_type)
        {
            if (site_type == new SetupConfigReader().site_type)
            {
                if (ErsCommonContext.IsBatch)
                {
                    return Encoding.UTF8;
                }
                else
                {
                    return HttpContext.Current.Request.ContentEncoding;
                }
            }
            else
            {
                var applicationRootPath = SiteTypeVariables.GetApplicationRoot(site_type);
                var vDirMapBase = new VirtualDirectoryMapping(applicationRootPath, true, "web.config");
                var fileMap = new WebConfigurationFileMap();
                fileMap.VirtualDirectories.Add("/virtual_partial_config", vDirMapBase);

                var config = System.Web.Configuration.WebConfigurationManager.OpenMappedWebConfiguration(fileMap, "/virtual_partial_config", "virtual_partial_config");

                var section = config.GetSection("system.web/globalization") as GlobalizationSection;

                if (section == null)
                {
                    return Encoding.UTF8;
                }

                return section.ResponseEncoding;
            }
        }


        /// <summary>
        /// コンパイル済みメソッドを取得する。
        /// </summary>
        /// <returns></returns>
        internal ErsTemplateCompiler.ConvertTemplateDelegate GetFunction()
        {
            var compiler = new ErsTemplateCompiler(context, this._path);

            //コンパイル済みテンプレートがない場合は新たにコンパイル
            if (!compiler.IsCompiled)
            {
                CompilerResultsDictionary.GetLock();
                try
                {
                    var templateParser = new ErsViewCompilerFactory().GetErsTemplateParser(context, this.enc);
                    string templateCode = File.ReadAllText(ErsCommonContext.MapPath(this._path), this.enc);

                    compiler.Compile(templateParser, templateCode);
                }
                finally
                {
                    CompilerResultsDictionary.ReleaseLock();
                }
            }

            //取得できなかった場合は再帰呼び出しを行います。
            var convertTemplateDelegate = compiler.GetDelegate();
            if (convertTemplateDelegate == null)
            {
                return GetFunction();
            }

            return convertTemplateDelegate;
        }
    }
}