﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using System.Web.Mvc;
using System.ComponentModel;
using jp.co.ivp.ers.util;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Routing;
using System.Reflection;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Property)]
    public abstract class BindFileAttributeBase
        : ErsUniversalValidationAttribute, IHtmlBinding
    {
        /// <summary>
        /// Bind posted file
        /// </summary>
        /// <param name="model">instance of target model</param>
        /// <param name="requestValue">value that user posted</param>
        /// <param name="displayName">name for display</param>
        /// <param name="propertyName">name of property</param>
        /// <param name="propertyType">type of target property</param>
        /// <param name="attributes">attributes that the target proeprty has</param>
        /// <param name="validators">validators that the target property has</param>
        /// <param name="checkedValue">this value is stored to the target property</param>
        /// <returns>result of validation</returns>
        public virtual IEnumerable<ValidationResult> BindProperty(object model, object requestValue, string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators, out object checkedValue)
        {
            var ersModel = model as ErsModelBase;

            //check arguments
            if (ersModel == null)
                throw new Exception("can not bind field \"" + model.GetType().Name + "." + propertyName + "\". model \"" + model.GetType().Name + "\" must inherits ErsModelBase class.");

            if (requestValue == null)
            {
                checkedValue = null;
                return null;
            }

            //validate posted value
            var postedFile = new HttpPostedFileWrapper((HttpPostedFile)requestValue);

            //validate fileSize
            this.FileByteSizeValidate(postedFile, propertyName);

            var validationResult = this.Validate(ersModel, postedFile, propertyName, propertyType, attributes, validators);

            if (validationResult == null || validationResult.Count() == 0)
            {
                //OK
                checkedValue = postedFile;
                return null;
            }
            else
            {
                //NG
                checkedValue = null;
                return validationResult;
            }
        }

        /// <summary>
        /// Validate file size of uploaded file
        /// </summary>
        /// <param name="postedFile"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        protected virtual void FileByteSizeValidate(HttpPostedFileBase postedFile, string propertyName)
        {
            var fileLimitByte = this.GetMaxFileByteSize();
            if (fileLimitByte < postedFile.ContentLength)
            {
                var byteMessage = string.Empty;
                if (fileLimitByte >= (1024 * 1024))
                {
                    byteMessage = fileLimitByte / (1024 * 1024) + ErsResources.GetFieldName("megaByte");
                }
                else if (fileLimitByte >= 1024)
                {
                    byteMessage = fileLimitByte / 1024 + ErsResources.GetFieldName("kiloByte");
                }
                else
                {
                    byteMessage = fileLimitByte + ErsResources.GetFieldName("byte");
                }

                //エラーの際は処理を続行できないのでExceptionにて終了します。
                throw new ErsException("10049", ErsResources.GetFieldName(propertyName), byteMessage);
            }
        }

        /// <summary>
        /// Get max byte size of the file.
        /// </summary>
        /// <returns></returns>
        protected abstract int GetMaxFileByteSize();

        /// <summary>
        /// Validate posted file.
        /// </summary>
        /// <param name="model">instance of target model</param>
        /// <param name="postedFile">value that user posted</param>
        /// <param name="displayName">name for display</param>
        /// <param name="propertyName">name of property</param>
        /// <param name="propertyType">type of target property</param>
        /// <param name="attributes">attributes that the target proeprty has</param>
        /// <param name="validators">validators that the target property has</param>
        /// <returns>result of validation</returns>
        public abstract IEnumerable<ValidationResult> Validate(ErsModelBase model, HttpPostedFileBase postedFile, string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators);

        /// <summary>
        /// Get values for this property from RouteData instance
        /// </summary>
        /// <param name="propertyName">name of property</param>
        /// <param name="routeData">instance of routeData</param>
        /// <returns></returns>
        public virtual object GetRequestValue(string propertyName, RouteData routeData)
        {
            HttpPostedFile retObject;
            if (routeData.Values.ContainsKey(propertyName) && routeData.Values[propertyName] is HttpPostedFile)
            {
                retObject = (HttpPostedFile)routeData.Values[propertyName];
            }
            else
            {
                retObject = HttpContext.Current.Request.Files[propertyName];
            }
            if (retObject == null)
                return null;
            else
                return retObject;
        }

        /// <summary>
        /// Get values for this property from dictionary
        /// </summary>
        /// <param name="propertyName">name of property</param>
        /// <param name="valueSource">instance that implements IDictionary</param>
        /// <returns></returns>
        public virtual object GetRequestValue(string propertyName, IDictionary<string, object> valueSource)
        {
            if (!valueSource.ContainsKey(propertyName))
                return null;

            var retObject = valueSource[propertyName];
            if (retObject != null && retObject is HttpPostedFile)
                return retObject;
            else
                return null;
        }

        public virtual void GetOutputHidden(List<ErsOutputHiddenTarget> listTarget, PropertyInfo property, string propertyName_frefix, string propertyName, Func<object> valueFunc, Dictionary<string, bool> OutputHidden)
        {
            //hiddenには出力しない
        }
    }
}
