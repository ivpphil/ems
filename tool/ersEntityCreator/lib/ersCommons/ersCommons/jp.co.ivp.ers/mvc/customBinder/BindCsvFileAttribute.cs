﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Collections.Specialized;
using System.Web.Routing;
using System.Reflection;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Property)]
    public class BindCsvFileAttribute
        : BindFileAttributeBase
    {
        int? fileLimitByte { get; set; }

        /// <summary>
        /// Get Instance of BindFileAttribute
        /// </summary>
        /// <param name="extensions">allowed extension(please specify with lower case characters)</param>
        public BindCsvFileAttribute()
        {
        }

        /// <summary>
        /// Get Instance of BindFileAttribute
        /// </summary>
        /// <param name="extensions">allowed extension(please specify with lower case characters)</param>
        public BindCsvFileAttribute(int fileLimitByte)
        {
            this.fileLimitByte = fileLimitByte;
        }

        public override IEnumerable<ValidationResult> BindProperty(object model, object requestValue, string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators, out object checkedValue)
        {
            if (!propertyType.IsGenericType || propertyType.GetGenericTypeDefinition() != typeof(ErsCsvContainer<>) || !propertyType.GetGenericArguments()[0].IsSubclassOf(typeof(ErsBindableModel)))
            {
                throw new Exception(model.GetType().Name + "." + propertyName + "は、" + "ErsCsvContainer<ErsBindableModelのサブクラス>型である必要があります。");
            }

            //ファイル名かもしくはアップロードされたファイルを受信する。
            string fileName = null;
            IEnumerable<ValidationResult> validationResult = null;
            HttpPostedFileWrapper postedFile = null;
            bool IsValidAtBinding = false;
            if (requestValue is string)
            {
                fileName = (string)requestValue;
                IsValidAtBinding = true;
            }
            else
            {
                validationResult = base.BindProperty(model, requestValue, propertyName, propertyType, attributes, validators, out checkedValue);

                if (validationResult == null && checkedValue != null)
                {
                    postedFile = (HttpPostedFileWrapper)checkedValue;
                    IsValidAtBinding = true;
                }
            }

            var containerType = typeof(ErsCsvContainer<>);
            var fieldType = propertyType.GetGenericArguments()[0];
            var genericType = containerType.MakeGenericType(fieldType);

            var csvContainer = (ErsCsvContainerBase)Activator.CreateInstance(genericType);
            csvContainer.containerModel =  (IErsModelBase)model;
            csvContainer.csv_file = postedFile;
            csvContainer.fileName = fileName;
            csvContainer.IsValidAtBinding = IsValidAtBinding;
            csvContainer.LoadPostedFile();

            checkedValue = csvContainer;

            return validationResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private string GetFileName(string propertyName)
        {
            return Convert.ToString(HttpContext.Current.Request.Form[propertyName]);
        }

        /// <summary>
        /// Validate posted file.
        /// </summary>
        /// <param name="model">instance of target model</param>
        /// <param name="postedFile">value that user posted</param>
        /// <param name="displayName">name for display</param>
        /// <param name="propertyName">name of property</param>
        /// <param name="propertyType">type of target property</param>
        /// <param name="attributes">attributes that the target proeprty has</param>
        /// <param name="validators">validators that the target property has</param>
        /// <returns>result of validation</returns>
        public override IEnumerable<ValidationResult> Validate(ErsModelBase model, HttpPostedFileBase postedFile, string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators)
        {
            if (postedFile.ContentLength == 0)
            {
                return new[] { new ValidationResult("10202") };
            }

            var extensionStr = "csv";

            //拡張子のチェック
            var fileName = postedFile.FileName ?? string.Empty;

            var targetExtension = System.IO.Path.GetExtension(fileName);

            if (!string.IsNullOrEmpty(targetExtension)
                && extensionStr == targetExtension.ToLower().Substring(1))
            {
                //OK
                return null;
            }

            //拡張子エラーの際は処理を続行できないのでExceptionにて終了します。
            throw new ErsException("10208", ErsResources.GetFieldName(propertyName), extensionStr);
        }

        /// <summary>
        /// Get max byte size of the file.
        /// </summary>
        /// <returns></returns>
        protected override int GetMaxFileByteSize()
        {
            if (this.fileLimitByte.HasValue)
            {
                return this.fileLimitByte.Value;
            }
            else
            {
                return new SetupConfigReader().default_upload_limit_byte;
            }
        }

        /// <summary>
        /// Get values for this property from RouteData instance
        /// </summary>
        /// <param name="propertyName">name of property</param>
        /// <param name="routeData">instance of routeData</param>
        /// <returns></returns>
        public override object GetRequestValue(string propertyName, RouteData routeData)
        {
            //CSVファイル名も受信するので、文字列も許可する
            if (routeData.Values.ContainsKey(propertyName))
            {
                if (routeData.Values[propertyName] != null && !(routeData.Values[propertyName] is HttpPostedFile))
                {
                    return routeData.Values[propertyName];
                }
            }
            else
            {
                var requestValue = HttpContext.Current.Request[propertyName];
                if (!string.IsNullOrEmpty(requestValue))
                {
                    return requestValue;
                }
            }

            return base.GetRequestValue(propertyName, routeData);
        }

        /// <summary>
        /// Get values for this property from dictionary
        /// </summary>
        /// <param name="propertyName">name of property</param>
        /// <param name="valueSource">instance that implements IDictionary</param>
        /// <returns></returns>
        public override object GetRequestValue(string propertyName, IDictionary<string, object> valueSource)
        {
            //CSVファイル名も受信するので、文字列も許可する
            if (valueSource.ContainsKey(propertyName) && valueSource[propertyName] != null &&!(valueSource[propertyName] is HttpPostedFile))
                return valueSource[propertyName];

            return base.GetRequestValue(propertyName, valueSource);
        }

        public override void GetOutputHidden(List<ErsOutputHiddenTarget> listTarget, PropertyInfo property, string propertyName_frefix, string propertyName, Func<object> valueFunc, Dictionary<string, bool> OutputHidden)
        {
            var hiddenGroup = ErsOutputHiddenUtility.GetOutputHiddenGroup(OutputHidden, property);

            if (hiddenGroup == null)
            {
                return;
            }

            var csvContainer = (ErsCsvContainerBase)(valueFunc());

            if (csvContainer == null)
            {
                return;
            }

            var dic = new Dictionary<string, object>();
            dic["name"] = propertyName;
            dic["value"] = HttpUtility.HtmlEncode(csvContainer.fileName);
            ErsOutputHiddenUtility.AddHidden(listTarget, hiddenGroup, dic);
        }
    }
}
