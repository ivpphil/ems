﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;
using System.Collections.Specialized;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel;
using jp.co.ivp.ers.util;
using System.ComponentModel.DataAnnotations;
using System.Web.Routing;
using System.Reflection;
using jp.co.ivp.ers.mvc.htmlExpand;

namespace jp.co.ivp.ers.mvc
{
    /// <summary>
    /// 指定したprefixがついたForm値をDictionary&lt;string, string&gt;にセットする。
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class HtmlDictionaryAttribute
        : Attribute, IHtmlBinding
    {

        public HtmlDictionaryAttribute(string key_prefix)
        {
            this.key_prefix = key_prefix;
        }

        public string key_prefix { get; protected set; }

		/// <summary>
		/// Bind property to a model
		/// </summary>
        public virtual IEnumerable<ValidationResult> BindProperty(object model, object requestValue, string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators, out object checkedValue)
        {

            var innerDictionary = (Dictionary<string, object>)requestValue;

            var retDictionary = new Dictionary<string, object>();

            var retResult = new List<ValidationResult>();

            var fieldType = propertyType.GetGenericArguments()[1];

            //validation
            foreach (string key in innerDictionary.Keys)
            {
                //キーをチェック
                var errorMessage = new ErsCheckUniversal().Check(CHK_TYPE.All, key, propertyName, null, null, null, null, null, false, false, false, null);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    retResult.Add(new ValidationResult(errorMessage, new[] { propertyName }));
                }
                else
                {
                    string value = (string)innerDictionary[key];
                    object validatedValue;
                    foreach (var result in ErsBindModel.Validate(propertyName, value, validators, out validatedValue, fieldType, attributes))
                    {
                        retResult.Add(result);
                    }
                    retDictionary.Add(key, validatedValue);
                }
            }

            checkedValue = this.GetConcreteValue(propertyType, retDictionary);
            return retResult;
        }

		/// <summary>
		/// Get requested values and RouteData
		/// </summary>
        public object GetRequestValue(string propertyName, RouteData routeData)
        {
            var retDictionary = new Dictionary<string, object>();
            HtmlDictionaryAttribute.AddRequestDataToDictionary(key_prefix, routeData.Values, retDictionary);
            HtmlDictionaryAttribute.AddRequestDataToDictionary(key_prefix, HttpContext.Current.Request.Form, retDictionary);
            HtmlDictionaryAttribute.AddRequestDataToDictionary(key_prefix, HttpContext.Current.Request.QueryString, retDictionary);
            HtmlDictionaryAttribute.AddRequestDataToDictionary(key_prefix, HttpContext.Current.Request.Files, retDictionary);
            return retDictionary;
        }

		/// <summary>
		/// Get requested values using IDictionary
		/// </summary>
        public object GetRequestValue(string propertyName, IDictionary<string, object> valueSource)
        {
            var retDictionary = new Dictionary<string, object>();
            HtmlDictionaryAttribute.AddRequestDataToDictionary(key_prefix, (Dictionary<string, object>)valueSource, retDictionary);
            return retDictionary;
        }

		/// <summary>
		/// Get requested value using Dictionary
		/// </summary>
        private object GetConcreteValue(Type propertyType, Dictionary<string, object> dictionary)
        {

            var listType = typeof(Dictionary<,>);
            var keyType = propertyType.GetGenericArguments()[0];
            var fieldType = propertyType.GetGenericArguments()[1];
            var genericType = listType.MakeGenericType(propertyType.GetGenericArguments());

            var retDictionary = (System.Collections.IDictionary)Activator.CreateInstance(genericType);

            foreach (var key in dictionary.Keys)
            {
                var value = dictionary[key];
                retDictionary.Add(ErsReflection.ConvertValue(keyType, key), ErsBindModel.GetConcreteValue(fieldType, value));
            }
            return retDictionary;
        }

        /// <summary>
        /// prefixを指定して、Dictionaryを取得する
		/// <para>using specified prefix, set data to dictionary</para>
        /// </summary>
        /// <param name="key_prefix"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        protected internal static void AddRequestDataToDictionary(string key_prefix, NameValueCollection valueSource, Dictionary<string, object> retDictionary)
        {
            //values
            foreach (string key in valueSource.Keys)
            {
                if (key != null && key.StartsWith(key_prefix) && valueSource[key] != null)
                {
                    var dictionaryKey = key.Substring(key_prefix.Length);
                    if (retDictionary.ContainsKey(dictionaryKey))
                    {
                        retDictionary[dictionaryKey] = JoinValue(retDictionary[dictionaryKey], valueSource[key]);
                    }
                    else
                    {
                        retDictionary[dictionaryKey] = valueSource[key];
                    }
                }
            }
        }

        /// <summary>
        /// prefixを指定して、Dictionaryを取得する
		/// <para>Using the specified prefix, set data to dictionary</para>
        /// </summary>
        /// <param name="key_prefix"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        protected internal static void AddRequestDataToDictionary(string key_prefix, HttpFileCollection valueSource, Dictionary<string, object> retDictionary)
        {
            //values
            foreach (string key in valueSource.Keys)
            {
                if (key.StartsWith(key_prefix) && valueSource[key] != null)
                {
                    var dictionaryKey = key.Substring(key_prefix.Length);
                    if (retDictionary.ContainsKey(dictionaryKey))
                    {
                        retDictionary[dictionaryKey] = JoinValue(retDictionary[dictionaryKey], valueSource[key]);
                    }
                    else
                    {
                        retDictionary[dictionaryKey] = valueSource[key];
                    }
                }
            }
        }


        /// <summary>
        /// prefixを指定して、Dictionaryを取得する
		/// <para>Using the specified prefix, set data to dictionary</para>
        /// </summary>
        /// <param name="key_prefix"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        protected internal static void AddRequestDataToDictionary(string key_prefix, IDictionary<string, object> valueSource, Dictionary<string, object> retDictionary)
        {
            //values
            foreach (string key in valueSource.Keys)
            {
                if (key.StartsWith(key_prefix) && valueSource[key] != null)
                {
                    var dictionaryKey = key.Substring(key_prefix.Length);
                    if (retDictionary.ContainsKey(dictionaryKey))
                    {
                        retDictionary[dictionaryKey] = JoinValue(retDictionary[dictionaryKey], valueSource[key]);
                    }
                    else
                    {
                        retDictionary[dictionaryKey] = valueSource[key];
                    }
                }
            }
        }

		/// <summary>
		/// Returns a string or an object of combined source and destination
		/// </summary>
        private static object JoinValue(object src, object dest)
        {
            if (src is string && dest is string)
            {
                return (string)src + "," + (string)dest;
            }
            else
            {
                if (src is object[])
                {
                    var list = new List<object>((object[])src);
                    list.Add(dest);
                    return list.ToArray();
                }
                else
                {
                    return new[] { src, dest };
                }
            }
        }


        public virtual void GetOutputHidden(List<ErsOutputHiddenTarget> listTarget, PropertyInfo property, string propertyName_frefix, string propertyName, Func<object> valueFunc, Dictionary<string, bool> OutputHidden)
        {
            var hiddenGroup = ErsOutputHiddenUtility.GetOutputHiddenGroup(OutputHidden, property);
            if (hiddenGroup != null)
            {
                var value = valueFunc();
                var dicValue = (System.Collections.IDictionary)value;

                foreach (var key in dicValue.Keys)
                {
                    var dic = new Dictionary<string, object>();
                    dic["name"] = this.key_prefix + key;
                    dic["value"] = HttpUtility.HtmlEncode(this.FormatHiddenValue(property, value));
                    ErsOutputHiddenUtility.AddHidden(listTarget, hiddenGroup, dic);
                }
            }
        }

        protected virtual object FormatHiddenValue(PropertyInfo property, object value)
        {
            var outputHiddenAttribute = property.GetCustomAttributes(typeof(ErsOutputHiddenAttribute), false);
            var attrOutput = (ErsOutputHiddenAttribute)outputHiddenAttribute[0];
            return attrOutput.FormatValue(property, value);
        }
    }
}
