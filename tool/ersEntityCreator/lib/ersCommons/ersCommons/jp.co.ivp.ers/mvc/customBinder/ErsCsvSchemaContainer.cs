﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using Microsoft.VisualBasic.FileIO;
using jp.co.ivp.ers.mvc.validation;
using System.IO;

namespace jp.co.ivp.ers.mvc
{
    public class ErsCsvSchemaContainer<TContainer>
        : ErsCsvContainerBase
        where TContainer : ErsBindableModel
    {
        protected string UPLOAD_FILE_PATH;
        private const string sign_key = "key:";

        public ErsCsvSchemaContainer()
        {
            if (ErsCommonContext.IsBatch)
            {
                UPLOAD_FILE_PATH = string.Empty;
            }
            else
            {
                UPLOAD_FILE_PATH = ErsCommonContext.MapPath("~/file_upload/");
            }
        }

        /// <summary>
        /// エンコーディング
        /// </summary>
        protected virtual Encoding encoding
        {
            get
            {
                return ErsEncoding.ShiftJIS;
            }
        }

        /// <summary>
        /// 正常なデータだったインデックスのリスト
        /// <para>List of normal indexes</para>
        /// </summary>
        public IEnumerable<int> validIndexes
        {
            get
            {
                if (this.csvValues == null)
                {
                    yield break;
                }

                for (var index = 0; index < this.csvValues.Count; index++)
                {
                    if (!this.invalidIndexes.Contains(index))
                    {
                        yield return index;
                    }
                }
            }
        }

        public virtual List<string> updatableSchemaList { get; protected set; }
        public virtual List<string> parameterKey { get; protected set; }
        public virtual List<string> updatableSchemaListDefault { get; protected set; }
        public string tableName { get; private set; }

        /// <summary>
        /// 不正なデータだったインデックスのリスト
        /// <para>List of invalid indexes</para>
        /// </summary>
        public List<int> invalidIndexes = new List<int>();

        /// <summary>
        /// CSVファイルから読み込んだDataTable
        /// <para>Read DataTable from CSV file</para>
        /// </summary>
        protected List<object> csvValues;

        /// <summary>
        /// 検証結果が正常なModelのIEnumerableを返す(表示用)。
        /// <para>Model validation results of normal returns IEnumerable (for display).</para>
        /// </summary>
        /// <returns>Modelのプロパティ(ErsDictionary)</returns>
        public IEnumerable<Dictionary<string, object>> ValidSchemaList
        {
            get
            {
                foreach (var index in this.validIndexes)
                {
                    yield return GetValidSchema(index);
                }
            }
        }

        /// <summary>
        /// CSVファイルを読み込む。(call by ErsModelBinder)
        /// <para>read a CSV file. (called by ErsModelBinder)</para>
        /// </summary>
        public override void LoadPostedFile()
        {

            if (!this.IsValidAtBinding)
                return;

            //アップロードデータ読み込み
            //Read data for upload
            if (string.IsNullOrEmpty(this.fileName))
                this.fileName = this.SaveUploadFile();

            //保存済みテンポラリを読み込む
            //Reads temporary saved file
            this.csvValues = this.LoadTempFile(UPLOAD_FILE_PATH + this.fileName);
        }

        /// <summary>
        /// アップロードデータの読み込み
        /// <para>Saving uploaded file</para>
        /// </summary>
        protected virtual string SaveUploadFile()
        {
            this.fileName = Path.GetFileNameWithoutExtension(csv_file.FileName) + DateTime.Now.ToString("_yyyyMMddhhmmddss") + Path.GetExtension(csv_file.FileName);

            ///CSVファイル名が取得成功すれば、保存
            ///CSV file name if the acquisition is successful, save
            csv_file.SaveAs(UPLOAD_FILE_PATH + this.fileName);

            return fileName;
        }

        /// <summary>
        /// CSVファイルを読み込む。(call by ErsModelBinder)
        /// <para>read a CSV file. (called by ErsModelBinder)</para>
        /// </summary>
        public virtual void LoadPostedFile(string filePath, Encoding encoding = null)
        {
            this.SetTableName(filePath);
            this.ValidateTable();

            //保存済みテンポラリを読み込む
            //Reads temporary saved file
            this.csvValues = this.LoadTempFile(filePath, encoding);

            this.IsValidAtBinding = true;
        }


        public virtual void LoadPostedFile(string filePath, Stream stream, Encoding encoding = null)
        {
            this.SetTableName(filePath);
            this.ValidateTable();

            //保存済みテンポラリを読み込む
            //Reads temporary saved file
            this.csvValues = this.LoadTempFile(stream, encoding);

            this.IsValidAtBinding = true;
        }

        private void SetTableName(string filePath)
        {
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            this.tableName = fileName.Split('-').First().RemoveWhiteSpace();
        }

        /// <summary>
        /// CSVファイルからDataTableを読み込む
        /// <para>Read the DataTable from CSV file for ModelState error.</para>
        /// </summary>
        /// <param name="filePath">物理ファイルパス/The physical file path</param>
        /// <returns>データリスト/Data List</returns>
        protected virtual List<object> LoadTempFile(Stream stream, Encoding encoding = null, bool leaveOpen = true)
        {
            if (encoding == null)
                encoding = this.encoding;
            
            var parser = new TextFieldParser(stream, encoding, true, leaveOpen) { Delimiters = this.GetDelimiters() };

            return LoadTempFile(parser);
        }


        protected virtual List<object> LoadTempFile(string filePath, Encoding encoding = null)
        {
            if (encoding == null)
                encoding = this.encoding;

            var parser = new TextFieldParser(filePath, encoding) { Delimiters = this.GetDelimiters() };

            return LoadTempFile(parser);
        }

        /// <summary>
        /// CSVファイルからDataTableを読み込む
        /// <para>Read the DataTable from CSV file for ModelState error.</para>
        /// </summary>
        /// <param name="filePath">物理ファイルパス/The physical file path</param>
        /// <returns>データリスト/Data List</returns>
        protected virtual List<object> LoadTempFile(TextFieldParser parser)
        {

            var retList = new List<object>();
            parameterKey = new List<string>();

            using (parser)
            {
                while (!parser.EndOfData)
                {
                    var row = parser.ReadFields();

                    if (this.updatableSchemaList == null)
                    {
                        this.updatableSchemaList = row.Where(s => s.HasValue()).ToList();
                        this.updatableSchemaListDefault = row.Where(s => s.HasValue()).ToList();

                        this.ValidateDuplicatedFields(this.updatableSchemaList);

                        List<string> tableSchemaList = new List<string>();

                        foreach (var tableSchema in this.updatableSchemaList)
                        {
                            if (tableSchema.Contains(sign_key))
                            {
                                string keyName = tableSchema.Replace(sign_key, "");
                                this.parameterKey.Add(keyName);
                                tableSchemaList.Add(keyName);
                                continue;
                            }
                            tableSchemaList.Add(tableSchema);
                        }

                        this.updatableSchemaList = tableSchemaList;

                        if (parameterKey.Count() == 0)
                        {
                            this.parameterKey.Add(this.updatableSchemaList.First());
                        }

                        //rowから「key:」の文字列を除去
                        for (int i = 0; i < row.Count(); i++)
                        {
                            row[i] = row[i].Replace(sign_key, "");
                        }
                    }

                    var rowDic = this.updatableSchemaList.Zip(row, (key, value) => new { key, value })
                        .ToDictionary(pair => pair.key, pair => (object)pair.value);

                    var dic = new Dictionary<string, object>();
                    this.updatableSchemaList.ForEach(schema => dic.Add(schema, null));

                    dic = ErsCommon.OverwriteDictionary(dic, rowDic);

                    //Removed Non updatable Fields
                    this.RemovedNonUpdatebleFields(dic);

                    retList.Add(dic);
                }
            }

            return retList;
        }

        /// <summary>
        /// ファイル分割のデリミタを取得
        /// <para>Gets the delimiter character</para>
        /// </summary>
        /// <returns>デリミタ/Delimeter</returns>
        protected virtual string[] GetDelimiters()
        {
            return new[] { "," };
        }

        /// <summary>
        ///  検証結果が正常なModelを返す。
        /// <para>returns validated as normal Model(s)</para>
        /// </summary>
        /// <param name="index">データインデックス / Index data</param>
        /// <returns>Model</returns>
        public virtual Dictionary<string, object> GetValidSchema(int index)
        {
            this.CheckIfValidationCalled();

            return this.GetBindSchema(index);
        }

        protected void CheckIfValidationCalled()
        {
            if (!this.IsValidAtBinding || this.csvValues == null)
            {
                throw new Exception("Please call ValidateFile() at first, then check the result.");
            }
        }

        /// <summary>
        /// 検証結果を付加したModelを返す。
        /// <para>returns the added Model with validation results.</para>
        /// </summary>
        /// <param name="index">データインデックス/Index data</param>
        /// <returns>Model</returns>
        public virtual IEnumerable<Dictionary<string, object>> GetValidatedSchema()
        {
            return this.GetValidatedSchema(true);
        }

        /// <summary>
        /// CSVファイルを検証する。
        /// <para>Validates the CSV file</para>
        /// </summary>
        /// <returns>ValidationResult</returns>
        public virtual IEnumerable<Dictionary<string, object>> GetValidatedSchema(bool skipFirstLine)
        {
            if (!this.IsValidAtBinding)
            {
                yield break;
            }
            
            var startIndex = 0;
            if (skipFirstLine)
            {
                this.invalidIndexes.Add(0);
                startIndex = 1;
            }
            for (var index = startIndex; index < csvValues.Count; index++)
            {
                var model = GetSchemaWithValidate(index);

                yield return model;
            }
        }

        /// <summary>
        /// 検証結果を付加したModelを返す。
        /// <para>returns the added Model with validation results.</para>
        /// </summary>
        /// <param name="index">データインデックス/Index data</param>
        /// <returns>Model</returns>
        protected virtual Dictionary<string, object> GetSchemaWithValidate(int index)
        {
            var dicValues = csvValues[index] as Dictionary<string, object>;

            var model = (ErsBindableModel)Activator.CreateInstance(typeof(TContainer));
            model.lineNumber = index + 1;
            this.SetSourceValueInModel(model);

            dicValues = GetReleaseCorrectValues(dicValues);
            this.ValidateDictionarySchema(model, dicValues);

            return dicValues;
        }

        /// <summary>
        ///  バインドしたModelを返す。
        /// </summary>
        /// <param name="index">データインデックス</param>
        /// <returns>Model</returns>
        protected virtual Dictionary<string, object> GetBindSchema(int index)
        {
            var dicValues = csvValues[index] as Dictionary<string, object>;

            var model = (ErsBindableModel)Activator.CreateInstance(typeof(TContainer));
            model.lineNumber = index + 1;
            this.SetSourceValueInModel(model);

            dicValues = GetReleaseCorrectValues(dicValues);
            this.ValidateDictionarySchema(model, dicValues);

            return dicValues;
        }

        private Dictionary<string, object> GetReleaseCorrectValues(Dictionary<string, object> dr)
        {
            //listSchema
            var dicCorrectValues = new Dictionary<string, object>();
            foreach (var columnName in dr.Keys)
            {
                object columnValue = null;
                columnValue = dr[columnName];

                if (columnValue != null)
                {
                    if (columnValue is string && Convert.ToString(columnValue).StartsWith("'"))
                    {
                        columnValue = Convert.ToString(columnValue).Substring(1);
                    }
                }

                dicCorrectValues.Add(columnName, columnValue);
            }

            return dicCorrectValues;
        }

        /// <summary>
        /// SaveValue as csv value
        /// </summary>
        /// <param name="model"></param>
        public virtual void SaveValue(Dictionary<string, object> baseDic)
        {
            if (baseDic.ContainsKey("containerModel"))
            {
                var containerModel = (ErsBindableModel)ErsReflection.ConvertValue(typeof(TContainer), baseDic["containerModel"]);
                var index = (containerModel.lineNumber == 0) ? 0 : containerModel.lineNumber - 1;

                this.csvValues[index] = baseDic;
            }
        }

        /// <summary>
        /// 不正な値としてマークする
        /// </summary>
        /// <param name="index"></param>
        public void MarkRecordAsInvalid(Dictionary<string, object> baseDic)
        {
            if (baseDic.ContainsKey("containerModel"))
            {
                var containerModel = (ErsBindableModel)ErsReflection.ConvertValue(typeof(TContainer), baseDic["containerModel"]);
                var index = (containerModel.lineNumber == 0) ? 0 : containerModel.lineNumber - 1;
                this.invalidIndexes.Add(index);
            }
        }

        /// <summary>
        /// Validate all collected Value
        /// </summary>
        /// <param name="containerModel"></param>
        /// <param name="baseDic"></param>
        private void ValidateDictionarySchema(ErsModelBase containerModel, IDictionary<string, object> baseDic)
        {
            foreach (var columnName in baseDic.Keys)
            {
                string columnValue = (string)ErsReflection.ConvertValue(typeof(string), baseDic[columnName]);

                string invalidMessage = this.ValidateDictionaryColumn(columnName, columnValue);

                if (invalidMessage.HasValue())
                    containerModel.AddInvalidField(columnName, this.ValidateDictionaryColumn(columnName, columnValue));
            }

            baseDic["containerModel"] = containerModel;
        }

        /// <summary>
        /// Validate by Column
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private string ValidateDictionaryColumn(string fieldName, string value)
        {
            var checkSchema = new SchemaForErsValidation(this.tableName, fieldName);
            var displayName = ErsResources.GetFieldName(String.Format("{0}.{1}", tableName, fieldName));

            return new ErsCheckUniversal().CheckWithSchema(displayName, checkSchema, value, checkSchema.isArray, false);
        }

        /// <summary>
        /// Validate TableName
        /// </summary>
        private void ValidateTable()
        {
            var setup = new SetupConfigReader();

            string messageTableKey = "non_updatable_table";

            var dicModel = ((ErsBindableModel)Activator.CreateInstance(typeof(TContainer))).GetPropertiesAsDictionary();

            if (dicModel.ContainsKey("messageTableKey"))
            {
                messageTableKey = Convert.ToString(dicModel["messageTableKey"]);
            }

            if (setup.notAllowedUpdatingTables != null && setup.notAllowedUpdatingTables.Contains(this.tableName))
            {
                throw new ErsException(messageTableKey);
            }
        }

        //Validate the Duplicated Fields
        private void ValidateDuplicatedFields(IEnumerable<string> list)
        {
            var duplicatedFields = list.GroupBy(x => x)
                                        .Where(g => g.Count() > 1)
                                        .Select(y => y.Key)
                                        .ToList();
            if (duplicatedFields.Count() > 0)
            {
                throw new ErsException("duplicated_columns", String.Join(",", duplicatedFields));
            }
        }

        //Removed Non Updatable Field
        private void RemovedNonUpdatebleFields(Dictionary<string, object> dic)
        {
            var retValues = new Dictionary<string, object>();
            string[] nonUpdatebleFields = new string[] { "id" };

            if (dic != null)
            {
                foreach (var field in nonUpdatebleFields)
                {
                    if (dic.ContainsKey(field) && dic.Keys.First() != field)
                        dic.Remove(field);
                }
            }
        }


        /// <summary>
        /// Set Schema Source in Container Model
        /// </summary>
        /// <param name="containerModel"></param>
        protected virtual void SetSourceValueInModel(ErsModelBase containerModel)
        {
            if (containerModel != null)
            {
                var sourceDictionary = new Dictionary<string, object>();
                sourceDictionary.Add("updatableSchemaList", this.updatableSchemaList);
                sourceDictionary.Add("parameterKey", this.parameterKey);
                sourceDictionary.Add("tableName", this.tableName);

                containerModel.OverwriteWithParameter(sourceDictionary);
            }
        }
    }
}
