﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using System.Web.Mvc;
using System.ComponentModel;
using jp.co.ivp.ers.util;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Routing;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Property)]
    public class BindFileAttribute
        : BindFileAttributeBase
    {
        int? fileLimitByte { get; set; }

        /// <summary>
        /// Get Instance of BindFileAttribute
        /// </summary>
        /// <param name="extensions">allowed extension(please specify with lower case characters)</param>
        public BindFileAttribute(params string[] extensions)
        {
            this.extensions = extensions;
        }

        /// <summary>
        /// Get Instance of BindFileAttribute
        /// </summary>
        /// <param name="extensions">allowed extension(please specify with lower case characters)</param>
        public BindFileAttribute(int fileLimitByte, params string[] extensions)
        {
            this.fileLimitByte = fileLimitByte;
            this.extensions = extensions;
        }

        protected string[] extensions { get; set; }

        /// <summary>
        /// Validate posted file.
        /// </summary>
        /// <param name="model">instance of target model</param>
        /// <param name="postedFile">value that user posted</param>
        /// <param name="displayName">name for display</param>
        /// <param name="propertyName">name of property</param>
        /// <param name="propertyType">type of target property</param>
        /// <param name="attributes">attributes that the target proeprty has</param>
        /// <param name="validators">validators that the target property has</param>
        /// <returns>result of validation</returns>
        public override IEnumerable<ValidationResult> Validate(ErsModelBase model, HttpPostedFileBase postedFile, string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators)
        {
            if (this.extensions == null || this.extensions.Length == 0)
            {
                yield break;
            }

            //拡張子のチェック
            var fileName = postedFile.FileName ?? string.Empty;

            var targetExtension = System.IO.Path.GetExtension(fileName);

            if (!string.IsNullOrEmpty(targetExtension)
                && extensions.Contains(targetExtension.ToLower().Substring(1)))
            {
                //OK
                yield break;
            }

            //NG
            string extensionStr = "." + string.Join(" .", extensions);

            //拡張子エラーの際は処理を続行できないのでExceptionにて終了します。
            throw new ErsException("63096", ErsResources.GetFieldName(propertyName), extensionStr);
        }

        /// <summary>
        /// Get max byte size of the file.
        /// </summary>
        /// <returns></returns>
        protected override int GetMaxFileByteSize()
        {
            if (this.fileLimitByte.HasValue)
            {
                return this.fileLimitByte.Value;
            }
            else
            {
                return new SetupConfigReader().default_upload_limit_byte;
            }
        }
    }
}
