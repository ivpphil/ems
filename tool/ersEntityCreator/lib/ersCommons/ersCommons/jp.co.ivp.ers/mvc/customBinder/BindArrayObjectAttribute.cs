﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Property)]
    public class BindArrayObjectAttribute
        : ErsUniversalValidationAttribute, IHtmlBinding
    {
        protected string table_name;
        protected bool manuallyGetError;

		/// <summary>
		/// Bind table_name
		/// </summary>
		/// <param name="table_name"></param>
        public BindArrayObjectAttribute(string table_name)
        {
            this.table_name = table_name;
        }

		/// <summary>
		/// Bind Table_name and manuallyGetError
		/// </summary>
		/// <param name="table_name"></param>
		/// <param name="manuallyGetError"></param>
        public BindArrayObjectAttribute(string table_name, bool manuallyGetError)
        {
            this.table_name = table_name;
            this.manuallyGetError = manuallyGetError;
        }

		/// <summary>
		/// Bind validated properties to a model
		/// </summary>
        public IEnumerable<ValidationResult> BindProperty(object model, object requestValue, string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators, out object checkedValue)
        {
            if (requestValue == null)
            {
                checkedValue = null;
                return null;
            }

            if (!propertyType.IsGenericType || (propertyType.GetGenericTypeDefinition() != typeof(List<>) && propertyType.GetGenericTypeDefinition() != typeof(IList<>) && propertyType.GetGenericTypeDefinition() != typeof(IEnumerable<>)) || !propertyType.GetGenericArguments()[0].IsSubclassOf(typeof(ErsBindableModel)))
            {
                throw new Exception(model.GetType().Name + "." + propertyName + "は、" + "IEnumerable<ErsBindableModelのサブクラス>型、またはList<ErsBindableModelのサブクラス>型である必要があります。");
            }

            var baseDictionary = (Dictionary<string, object>)requestValue;

            var tableDictionary = new Dictionary<string, Dictionary<string, object>>();

            //Dictionaryを行番号で分割
			//Dictionary divided by line number
            foreach (var key in baseDictionary.Keys)
            {
                if (string.IsNullOrEmpty(key))
                {
                    continue;
                }
                var match = Regex.Match(key, "^\\[([0-9]+)?\\]\\[(.+)?\\]");

                if (!match.Success)
                {
                    continue;
                }

                var line_number = match.Groups[1].Value;
                var field_name = match.Groups[2].Value;

                if (!tableDictionary.ContainsKey(line_number))
                    tableDictionary.Add(line_number, new Dictionary<string, object>());

                tableDictionary[line_number].Add(field_name, baseDictionary[key]);
            }

            //ジェネリック型の構築
            return this.GetConcreteValue(propertyName, propertyType, tableDictionary, out checkedValue, model);
        }

		/// <summary>
		/// Get requested property value
		/// </summary>
        public object GetRequestValue(string propertyName, RouteData routeData)
        {
            var retDictionary = new Dictionary<string, object>();
            HtmlDictionaryAttribute.AddRequestDataToDictionary(this.table_name, routeData.Values, retDictionary);
            HtmlDictionaryAttribute.AddRequestDataToDictionary(this.table_name, HttpContext.Current.Request.Form, retDictionary);
            HtmlDictionaryAttribute.AddRequestDataToDictionary(this.table_name, HttpContext.Current.Request.QueryString, retDictionary);
            HtmlDictionaryAttribute.AddRequestDataToDictionary(this.table_name, HttpContext.Current.Request.Files, retDictionary);
            return retDictionary;
        }

		/// <summary>
		/// Get requested value using dictionary
		/// </summary>
        public object GetRequestValue(string propertyName, IDictionary<string, object> valueSource)
        {
            var retDictionary = new Dictionary<string, object>();
            HtmlDictionaryAttribute.AddRequestDataToDictionary(this.table_name, (Dictionary<string, object>)valueSource, retDictionary);
            return retDictionary;

        }

        /// <summary>
		/// Get list of property validation results
		/// </summary>
        private IEnumerable<ValidationResult> GetConcreteValue(string propertyName, Type propertyType, Dictionary<string, Dictionary<string, object>> tableDictionary, out object checkedValue, object container)
        {
            var listType = typeof(List<>);
            var fieldType = propertyType.GetGenericArguments()[0];
            var genericType = listType.MakeGenericType(fieldType);

            var retList = (System.Collections.IList)Activator.CreateInstance(genericType);

            var retResult = new List<ValidationResult>();

            foreach (var lineNumber in tableDictionary.Keys)
            {
                var dictionary = tableDictionary[lineNumber];
                var model = (ErsBindableModel)Activator.CreateInstance(fieldType);
                model.containerModel = (IErsModelBase)container;
                model.lineNumber = Convert.ToInt32(lineNumber) + 1;
                ErsBindModel.ModelBind(model, dictionary, false);

                retList.Add(model);
            }

            checkedValue = retList;

            return retResult;
        }

        /// <summary>
        /// 指定したグループ名を持つErsOutputHidden属性がついたプロパティを出力する
		/// <para>Outputs a property that has the attribute ErsOutputHidden with specified group</para>
        /// </summary>
        /// <param name="model"></param>
        public virtual void GetOutputHidden(List<ErsOutputHiddenTarget> listTarget, PropertyInfo property, string propertyName_frefix, string propertyName, Func<object> valueFunc, Dictionary<string, bool> OutputHidden)
        {
            // 未実装（この形式で送信されることはあっても、"新規実装"をすることはないはず）
        }

        /// <summary>
        /// 指定したグループ名を持つErsOutputHidden属性がついたプロパティを出力する
        /// <para>Outputs a property that has an attribute ErsOutputHidden with specified group</para>
        /// </summary>
        /// <param name="model"></param>
        public void SetOutputHidden(System.Collections.IEnumerable modelList, string groupName, bool value)
        {
            if (modelList == null)
            {
                return;
            }

            foreach (var model in modelList)
            {
                var modelBase = (ErsBindableModel)model;
                modelBase.SetOutputHidden(groupName, value);
            }
        }
    }
}
