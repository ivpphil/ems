﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.mvc;
using System.Web.Mvc;
using System.ComponentModel;
using jp.co.ivp.ers.util;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.ComponentModel.DataAnnotations;
using System.Web.Routing;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Property)]
    public class BindPictureAttribute
        : BindFileAttributeBase
    {
        int? fileLimitByte { get; set; }

		/// <summary>
		/// Bind pictureTypes to Model's pictureTypes
		/// </summary>
        /// <param name="pictureTypes">allowed picture type</param>
        public BindPictureAttribute(params EnumPictureType[] pictureTypes)
        {
            this.pictureTypes = pictureTypes;
        }

        /// <summary>
        /// Get Instance of BindPictureAttribute
        /// </summary>
        /// <param name="pictureTypes">allowed picture type</param>
        public BindPictureAttribute(int fileByteSize, params EnumPictureType[] pictureTypes)
        {
            this.fileLimitByte = fileByteSize;
            this.pictureTypes = pictureTypes;
        }

        protected EnumPictureType[] pictureTypes { get; set; }

        /// <summary>
        /// Validate posted file.
        /// </summary>
        /// <param name="model">instance of target model</param>
        /// <param name="postedFile">value that user posted</param>
        /// <param name="displayName">name for display</param>
        /// <param name="propertyName">name of property</param>
        /// <param name="propertyType">type of target property</param>
        /// <param name="attributes">attributes that the target proeprty has</param>
        /// <param name="validators">validators that the target property has</param>
        /// <returns>result of validation</returns>
        public override IEnumerable<ValidationResult> Validate(ErsModelBase model, HttpPostedFileBase postedFile, string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators)
        {
            if (postedFile.ContentLength == 0)
            {
                yield break;
            }

            Bitmap bitmap = null;
            try
            {
                bitmap = new Bitmap(postedFile.InputStream);
            }
            catch (ArgumentException)
            {
                this.ThrowException(propertyName);
            }

            var decoders = ImageCodecInfo.GetImageDecoders();
            var formatDescription = string.Empty;
            foreach (var decoder in decoders)
            {
                if (decoder.FormatID == bitmap.RawFormat.Guid)
                {
                    formatDescription = decoder.FormatDescription;
                }
            }

            if (!string.IsNullOrEmpty(formatDescription))
            {
                EnumPictureType fd;
                if (Enum.TryParse(formatDescription, out fd))
                {
                    foreach (var pictureType in pictureTypes)
                    {
                        if (pictureType == fd)
                        {
                            //OK
                            yield break;
                        }
                    }
                }
            }

            //NG
            this.ThrowException(propertyName);
        }

        private void ThrowException(string propertyName)
        {
            var arrowedFile = string.Join(", ", this.pictureTypes.Select(picType => picType.ToString().ToLower()).ToArray());

            //エラーの際は処理を続行できないのでExceptionにて終了します。
            throw new ErsException("10205", ErsResources.GetFieldName(propertyName), arrowedFile);
        }

        /// <summary>
        /// Get max byte size of the file.
        /// </summary>
        /// <returns></returns>
        protected override int GetMaxFileByteSize()
        {
            if (this.fileLimitByte.HasValue)
            {
                return this.fileLimitByte.Value;
            }
            else
            {
                return new SetupConfigReader().image_upload_limit_byte;
            }
        }
    }
}
