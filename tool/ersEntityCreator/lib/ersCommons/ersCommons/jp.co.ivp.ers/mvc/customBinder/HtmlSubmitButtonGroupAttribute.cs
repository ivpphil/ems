﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc.validation;
using jp.co.ivp.ers.util;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Routing;
using System.Reflection;

namespace jp.co.ivp.ers.mvc
{
    /// <summary>
    /// 指定したprefixを持つボタンの識別子を取得する(どのボタンが押されたかの判定用)。（&lt;prefix&gt;&lt;識別子&gt;）
	/// <para>(For determination of which button was pressed) to retrieve the identifier of the button with the specified prefix. （&lt;prefix&gt;&lt;識別子&gt;）</para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class HtmlSubmitButtonGroupAttribute
        : ErsUniversalValidationAttribute, IHtmlBinding
    {
        public HtmlSubmitButtonGroupAttribute(string key_prefix)
        {
            this.key_prefix = key_prefix;
        }

        public string key_prefix { get; protected set; }

		/// <summary>
		/// Bind property to a model
		/// </summary>
        public IEnumerable<ValidationResult> BindProperty(object model, object requestValue, string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators, out object checkedValue)
        {
            foreach (string key in (System.Collections.Specialized.NameObjectCollectionBase.KeysCollection)requestValue)
            {
                if (key.StartsWith(key_prefix))
                {
                    return ErsBindModel.Validate(propertyName, key.Substring(key_prefix.Length), validators, out checkedValue, propertyType, attributes);
                }
            }
            checkedValue = null;
            return null;
        }

		/// <summary>
		/// Get requested values
		/// </summary>
        public object GetRequestValue(string propertyName, RouteData routeData)
        {
            return HttpContext.Current.Request.Form.Keys;
        }
		/// <summary>
		/// Get requested values using IDictionart
		/// </summary>
        public object GetRequestValue(string propertyName, IDictionary<string, object> valueSource)
        {
            return valueSource.Keys;
        }

        public virtual void GetOutputHidden(List<ErsOutputHiddenTarget> listTarget, PropertyInfo property, string propertyName_frefix, string propertyName, Func<object> valueFunc, Dictionary<string, bool> OutputHidden)
        {
            var hiddenGroup = ErsOutputHiddenUtility.GetOutputHiddenGroup(OutputHidden, property);
            if (hiddenGroup != null)
            {
                var value = valueFunc();
                var dic = new Dictionary<string, object>();
                dic["name"] = propertyName_frefix + propertyName;
                dic["value"] = HttpUtility.HtmlEncode(value);
                ErsOutputHiddenUtility.AddHidden(listTarget, hiddenGroup, dic);
            }
        }
    }
}
