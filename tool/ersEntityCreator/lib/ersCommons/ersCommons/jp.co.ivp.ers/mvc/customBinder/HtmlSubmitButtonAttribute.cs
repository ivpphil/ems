﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using System.Web.Routing;
using System.Reflection;

namespace jp.co.ivp.ers.mvc
{
    /// <summary>
    /// submitボタンフィールドの場合に付与すると、クリックされたか否かをセットする
	/// <para>Applied after clicking a submit button</para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class HtmlSubmitButtonAttribute
        : ErsUniversalValidationAttribute, IHtmlBinding
    {
		/// <summary>
		/// Bind properties to model
		/// </summary>
        public IEnumerable<ValidationResult> BindProperty(object model, object requestValue, string propertyName, Type propertyType, IEnumerable<Attribute> attributes, IEnumerable<ErsValidationBase> validators, out object checkedValue)
        {
            bool retVal = false;

            var checkVal = (string)requestValue;
            if (!string.IsNullOrEmpty(checkVal))
            {
                var arrValue = checkVal.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var ckVal in arrValue)
                {
                    if (ckVal.Trim().ToLower() != "false")
                    {
                        retVal = true;
                    }
                }
            }
            checkedValue = retVal;

            return new List<ValidationResult>();
        }

		/// <summary>
		/// Get requested property value using a RouteData
		/// </summary>
        public object GetRequestValue(string propertyName, RouteData routeData)
        {
            string checkVal;
            if (routeData.Values.ContainsKey(propertyName) && routeData.Values[propertyName] is string)
            {
                checkVal = (string)routeData.Values[propertyName];
            }
            else
            {
                checkVal = HttpContext.Current.Request[propertyName];
            }
            if (string.IsNullOrEmpty(checkVal))
                checkVal = HttpContext.Current.Request[propertyName + ".x"];

            return checkVal;
        }

		/// <summary>
		/// Get requested property value using an IDictionary
		/// </summary>
        public object GetRequestValue(string propertyName, IDictionary<string, object> valueSource)
        {
            object checkVal = null;
            if (valueSource.ContainsKey(propertyName))
                checkVal = valueSource[propertyName];

            if (checkVal == null || (checkVal is string && string.IsNullOrEmpty((string)checkVal)))
            {
                if (valueSource.ContainsKey(propertyName + ".x"))
                    checkVal = valueSource[propertyName + ".x"];
            }

            return checkVal;
        }

        public virtual void GetOutputHidden(List<ErsOutputHiddenTarget> listTarget, PropertyInfo property, string propertyName_frefix, string propertyName, Func<object> valueFunc, Dictionary<string, bool> OutputHidden)
        {
            var hiddenGroup = ErsOutputHiddenUtility.GetOutputHiddenGroup(OutputHidden, property);
            if (hiddenGroup != null)
            {
                var value = valueFunc();
                var dic = new Dictionary<string, object>();
                dic["name"] = propertyName_frefix + propertyName;
                dic["value"] = HttpUtility.HtmlEncode(value);
                ErsOutputHiddenUtility.AddHidden(listTarget, hiddenGroup, dic);
            }
        }
    }
}
