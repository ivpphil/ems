﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;
using System.Web;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagFormatString
        : ErsTagBase
    {
		/// <summary>
		/// Returns "formatString"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:formatString"; }
        }

        protected static string DEFAULT_VALUE = "#";

		/// <summary>
		/// Returns "#"
		/// </summary>
        protected virtual string defaultValue
        {
            get { return DEFAULT_VALUE; }
        }

		/// <summary>
		/// Returns this.GetType().Name + ".Format"
		/// </summary>
        protected virtual string formatMethodName
        {
            get { return this.GetType().Name + ".Format"; }
        }

		/// <summary>
		/// Replaces open ERS tag 
		/// </summary>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            var formatString = this.defaultValue;

            if (attributes.ContainsKey("value"))
            {
                formatString = attributes["value"];
            }

            var padLeftLength = this.GetIntValue(attributes, "padLeft");
            var padRightLength = this.GetIntValue(attributes, "padRight");
            var fixedLength = this.GetIntValue(attributes, "fixed");

            string post_fix = string.Empty;
            if (attributes.ContainsKey("post_fix"))
            {
                post_fix = attributes["post_fix"];
            }

            return string.Format("<%{0}(\"{1}\", {2}, \"{3}\", {4}, {5}, {6}, \"{7}\");%>", this.formatMethodName, attributes["src"], ErsViewHelper.GetViewVariableString(attributes["src"]), formatString, padLeftLength, padRightLength, fixedLength, post_fix);
        }

		/// <summary>
		/// Get interger value of a specified dictionary item
		/// </summary>
		/// <param name="attributes"></param>
		/// <param name="key"></param>
		/// <returns></returns>
        protected virtual string GetIntValue(ErsTagAttributes attributes, string key)
        {
            int tempIntValue;
            if (attributes.ContainsKey(key) && int.TryParse(attributes[key], out tempIntValue))
            {
                return tempIntValue.ToString();
            }
            else
            {
                return "null";
            }
        }

        /// <summary>
        /// ers:formatDate
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="tw"></param>
        /// <param name="sourceName"></param>
        /// <param name="o"></param>
        /// <param name="value"></param>
        public static void Format(string sourceName, object o, string value, int? padLeftLength, int? padRightLength, int? fixedLength, string post_fix)
        {
            var resultString = string.Format("{0:" + value + "}", o);

            string actual_post_fix = string.Empty;

            if (padLeftLength != null)
            {
                resultString = resultString.PadLeft(padLeftLength.Value);

                if (fixedLength != null)
                {
                    for (var i = 1; ErsCommon.GetByteCount(resultString) > fixedLength.Value; i++)
                    {
                        actual_post_fix = post_fix;
                        resultString = VBStrings.Right(resultString, resultString.Length - 1);
                    }
                }
            }
            else if (padRightLength != null)
            {
                resultString = resultString.PadRight(padRightLength.Value);

                if (fixedLength != null)
                {
                    for (var i = 1; ErsCommon.GetByteCount(resultString) > fixedLength.Value; i++)
                    {
                        actual_post_fix = post_fix;
                        resultString = VBStrings.Left(resultString, resultString.Length - 1);
                    }
                }
            }

            else
            {
                if (fixedLength != null)
                {
                    for (var i = 1; ErsCommon.GetByteCount(resultString) > fixedLength.Value; i++)
                    {
                        actual_post_fix = post_fix;
                        resultString = VBStrings.Left(resultString, resultString.Length - 1);
                    }
                }
            }

            ErsViewHelper.WriteValue(resultString + actual_post_fix);
        }
    }
}
