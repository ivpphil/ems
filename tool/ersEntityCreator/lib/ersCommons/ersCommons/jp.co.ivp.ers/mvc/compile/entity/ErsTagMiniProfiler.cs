﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StackExchange.Profiling;
using System.IO;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    /// <summary>
    /// MiniProfilerによるページ速度計測結果の表示用タグ生成
    /// Generate tags for displaying the profile by MiniProfier
    /// </summary>
    public class ErsTagMiniProfiler
        : ErsTagBase
    {
        protected override string ersTagName
        {
            get { return "ers:MiniProfiler"; }
        }

        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            return "<%ErsTagMiniProfiler.RenderIncludes();%>";
        }

        public static void RenderIncludes()
        {
            var miniProfilerStart = ErsCommonContext.GetPooledObject("_MiniProfilerStart");

            if (!Convert.ToBoolean(miniProfilerStart))
            {
                return;
            }

            ErsViewHelper.Write(MiniProfiler.RenderIncludes().ToHtmlString());
        }
    }
}
