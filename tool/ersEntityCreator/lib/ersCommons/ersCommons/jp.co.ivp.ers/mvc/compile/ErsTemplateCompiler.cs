﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers.util;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using jp.co.ivp.ers.mvc;
using System.Web;
using jp.co.ivp.ers.mvc.template;
using System.Web.Mvc;

namespace jp.co.ivp.ers.mvc.compile
{
    class ErsTemplateCompiler
    {

        public delegate void ConvertTemplateDelegate(TextWriter tw, ErsViewContext context, ViewDataDictionary viewData, object partialModel);

        protected string requestedPath { get; set; }
        protected ErsViewContext viewContext { get; set; }

        /// <summary>
        /// テンプレートがコンパイル済みの場合はTrue 
		/// <para>Returns True if template is compiled</para>
        /// </summary>
        public bool IsCompiled
        {
            get
            {
                return CompilerResultsDictionary.IsCompiled(this.requestedPath);
            }
        }

        /// <summary>
        /// コンパイル済みテンプレート
		/// <para>Compiled templates</para>
        /// </summary>
        protected virtual CompilerResults compilerResults
        {
            get
            {
                return CompilerResultsDictionary.GetCompilerResults(this.requestedPath);
            }
            set
            {
                CompilerResultsDictionary.RegistCompilerResults(this.requestedPath, value);
            }
        }

        /// <summary>
        /// 実行可能なコンパイル済みFunctionを出力する。
		/// <para>Function to get the output of compiled template</para>
        /// </summary>
        /// <returns></returns>
        public ConvertTemplateDelegate GetDelegate()
        {
            var delegateType = typeof(ConvertTemplateDelegate);
            var compilerResults = this.compilerResults;
            if (compilerResults ==null)
            {
                return null;
            }
            var type = compilerResults.CompiledAssembly.GetType("ErsTemplate" + this.GetClassName(), true);
            return (ConvertTemplateDelegate)Delegate.CreateDelegate(delegateType, type.GetMethod("Convert"));
        }

        /// <summary>
        /// コンパイルされるクラス名を取得する
		/// <para>Gets the name of the class that is compiled</para>
        /// </summary>
        /// <returns></returns>
        protected string GetClassName()
        {
            return ManageTemplate.GetClassName(requestedPath.ToUpper());
        }

        /// <summary>
        /// コンストラクタ / Constructor
        /// </summary>
        /// <param name="viewContext"></param>
        /// <param name="requestedPath"></param>
        public ErsTemplateCompiler(ErsViewContext viewContext, string requestedPath)
        {
            this.viewContext = viewContext;
            this.requestedPath = requestedPath;
        }

        /// <summary>
        /// コンパイル済みのテンプレートから関数を取得する
		/// <para>Calls the function to compile a template</para>
        /// </summary>
        /// <param name="tw"></param>
        /// <param name="viewData"></param>
        public virtual void Compile(ErsTemplateParser templateParser, string templateCode)
        {
            string source = templateParser.Parse(templateCode, this.GetClassName());

            this.compilerResults = this.Compile(source);
        }

        /// <summary>
        /// テンプレートをC#コードに変換してコンパイルする
		/// <para>Compile C # code to convert the template</para>
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        protected virtual CompilerResults Compile(string source)
        {

            CompilerParameters param = new CompilerParameters();
            param.GenerateInMemory = true;
            param.ReferencedAssemblies.Add("System.Web.dll");
            param.ReferencedAssemblies.Add(typeof(System.Web.Mvc.ViewDataDictionary).Assembly.Location);
            param.ReferencedAssemblies.Add(typeof(System.ComponentModel.DataAnnotations.IValidatableObject).Assembly.Location);
            param.ReferencedAssemblies.Add("System.Core.dll");
            param.ReferencedAssemblies.Add("Microsoft.CSharp.dll");
            param.ReferencedAssemblies.Add(viewContext.GetDllPath("ersLibrary.dll"));
            param.ReferencedAssemblies.Add(viewContext.GetDllPath("ersCommons.dll"));

            CompilerResults rs = new CSharpCodeProvider().CompileAssemblyFromSource(param, source);

            if (0 < rs.Errors.Count)
            {
                var sw = new StringWriter();
                sw.WriteLine("テンプレートが不正です。");
                foreach (CompilerError err in rs.Errors)
                    sw.WriteLine(err.Line + ":" + err.ErrorText);
                sw.WriteLine("-------------- source --------------");
                sw.WriteLine(source);
                throw new Exception(HttpUtility.HtmlEncode(sw.ToString()));
            }
            return rs;
        }

    }
}
