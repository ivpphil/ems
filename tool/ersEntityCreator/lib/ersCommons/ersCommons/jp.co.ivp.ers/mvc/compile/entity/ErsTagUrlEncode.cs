﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using System.Web;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagUrlEncode
        : ErsTagBase
    {
		/// <summary>
		/// Returns "isError"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:UrlEncode"; }
        }

		/// <summary>
		/// Returns ErsTagIsEqual tag
		/// </summary>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            var encode = string.Empty;
            if (attributes.ContainsKey("encode"))
            {
                encode = attributes["encode"];
            }

            if (attributes.ContainsKey("src"))
            {
                var src = ErsViewHelper.GetViewVariableString(attributes["src"]);

                return string.Format("<%=ErsTagUrlEncode.Display(\"{0}\", {1}, \"{2}\")%>", attributes["src"], src, encode);
            }
            else if (attributes.ContainsKey("value"))
            {
                return string.Format("<%=ErsTagUrlEncode.Display(\"{0}\", \"{1}\", \"{2}\")%>", attributes["value"], attributes["value"], encode);
            }
            return string.Empty;
        }

        /// <summary>
        /// ers:multiLine
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="tw"></param>
        /// <param name="sourceName"></param>
        /// <param name="o"></param>
        /// <param name="value"></param>
        public static string Display(string sourceName, object o, string strEncode)
        {
            if (o != null)
            {
                var encode = Encoding.UTF8;
                if (strEncode.HasValue())
                {
                    encode = Encoding.GetEncoding(strEncode);
                }

                return HttpUtility.UrlEncode(o.ToString(), encode);
            }

            return string.Empty;
        }
    }
}
