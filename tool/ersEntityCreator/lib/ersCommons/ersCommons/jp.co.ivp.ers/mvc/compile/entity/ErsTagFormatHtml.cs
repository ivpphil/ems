﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc.compile.entity
{

    /// <summary>
    /// for medica.
    /// don't use this tag for the field which the user can enter in Front site to prevent XSS.
    /// </summary>
    public class ErsTagFormatHtml
                : ErsTagNonEncode
    {
        protected override string ersTagName
        {
            get { return "ers:formatHtml"; }
        }
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            return string.Format("<%ErsTagFormatHtml.Display(\"{0}\", {1});%>", attributes["src"], ErsViewHelper.GetViewVariableString(attributes["src"]));
        }

        /// <summary>
        /// convert Cr and Lf to break tag without HTML encode
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="tw"></param>
        /// <param name="sourceName"></param>
        /// <param name="o"></param>
        /// <param name="value"></param>
        public static new void Display(string sourceName, object o)
        {
            if (o != null)
            {
                ErsViewHelper.Write(HtmlEncodeTagNotAllowed(o.ToString()).Replace("\r\n", "\n").Replace("\r", ErsViewHelper.TAG_NEW_LINE + "\r").Replace("\n", ErsViewHelper.TAG_NEW_LINE + "\n"));
            }
        }

    }
}
