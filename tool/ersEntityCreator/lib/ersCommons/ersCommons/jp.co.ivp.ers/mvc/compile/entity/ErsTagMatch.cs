﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public struct ErsTagMatch
    {
        public string tag { get; set; }

        public string attributes { get; set; }
    }
}
