﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;
using System.Web;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTemplateForm
        : ErsTagBase
    {
        protected override string ersTagName
        {
            get { return "form"; }
        }

        /// <summary>
        /// ERSタグの開始タグをセットする
        /// <para>Set the start tag for ERS tag</para>
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            string url = string.Empty;
            if (attributes.ContainsKey("action"))
            {
                url = attributes["action"];
            }
            attributes["action"] = "<%ErsViewHelper.ConvertToAbsoluteUri(" + ErsViewHelper.ExpandVariable("\"" + url + "\"") + ");%>";

            var group_name = ErsOutputHiddenUtility.GetGroupName(attributes);

            return "<form " + attributes.ToString() + ">\n" +
                "<%ErsTemplateForm.OutputHidden(\".OutputHidden\", " + ErsViewHelper.ExpandVariable("\"" + url + "\"") + ", " + ErsViewHelper.GetViewVariableString(".OutputHidden") + ", new [] { " + group_name + " });%>\n" +
                "<%ErsTemplateForm.SecureHidden(\".SecureHidden\", " + ErsViewHelper.ExpandVariable("\"" + url + "\"") + ", " + ErsViewHelper.GetViewVariableString(".SecureHidden") + ");%>\n" +
                "<%ErsTemplateForm.NormalHidden(\".NormalHidden\", " + ErsViewHelper.ExpandVariable("\"" + url + "\"") + ", " + ErsViewHelper.GetViewVariableString(".NormalHidden") + ");%>";
        }

        /// <summary>
        /// ERSタグの閉じタグをセットする
        /// <para>Set the close tag for ERS tag</para>
        /// </summary>
        /// <param name="operation"></param>
        /// <returns></returns>
        protected override string ReplaceCloseErsTag()
        {
            return "</form>";
        }

        /// <summary>
        /// 通常のHidden情報を出力する。
		/// <para>Output Hidden information</para>
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="sourceName"></param>
        /// <param name="url"></param>
        /// <param name="values"></param>
        public static void OutputHidden(string sourceName, string url, object values, IEnumerable<string> group_name)
        {
            if (values == null || !ErsViewHelper.CheckErsUrl(url) || !(values is List<ErsOutputHiddenTarget>))
                return;

            var listHidden = ((List<ErsOutputHiddenTarget>)values).FindAll((target)=>group_name.Contains(target.groupName));
            foreach (var hidden in listHidden)
            {
                if (hidden.values != null)
                {
                    WriteHiddenString(hidden.values);
                }
            }
        }

        /// <summary>
        /// 通常のHidden情報を出力する。
        /// <para>Output Hidden information</para>
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="sourceName"></param>
        /// <param name="url"></param>
        /// <param name="values"></param>
        public static void NormalHidden(string sourceName, string url, object values)
        {
            if (values == null || !ErsViewHelper.CheckErsUrl(url) || !(values is List<Dictionary<string, object>>))
                return;

            WriteHiddenString((List<Dictionary<string, object>>)values);
        }

        /// <summary>
        /// セキュアなHidden情報を出力する。
		/// <para>Secure output Hidden information</para>
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="sourceName"></param>
        /// <param name="url"></param>
        /// <param name="values"></param>
        public static void SecureHidden(string sourceName, string url, object values)
        {
            if (values == null || !ErsViewHelper.CheckSessionUrl(url) || !(values is List<Dictionary<string, object>>))
                return;

            WriteHiddenString((List<Dictionary<string, object>>)values);
        }

        /// <summary>
        /// Hidden文字列を取得する。
		/// <para>Get Hidden string</para>
        /// </summary>
        /// <param name="ersList"></param>
        /// <returns></returns>
        public static void WriteHiddenString(List<Dictionary<string, object>> ersList)
        {
            foreach (var item in ersList)
            {
                ErsViewHelper.Write("<input type=\"hidden\" name=\"");
                ErsViewHelper.WriteValue(item["name"]);
                ErsViewHelper.Write("\" value=\"");
                ErsViewHelper.WriteValue(item["value"]);
                ErsViewHelper.Write("\" />");
            }
        }
    }
}
