﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc.compile
{
    public class ErsMailParser
        : ErsTemplateParser
    {
         /// <summary>
        /// コンストラクタ / Constructor
        /// </summary>
        /// <param name="viewContext"></param>
        /// <param name="requestedPath"></param>
        public ErsMailParser(ErsViewContext viewContext, Encoding enc)
            :base(viewContext, enc)
        {

        }

        protected override void ReplaceValueTag(System.IO.StringWriter sw, string variable)
        {
            sw.WriteLine("tw.Write(\"\"+({0}));", variable);
        }
    }
}
