﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagFormatCurrency
        : ErsTagFormatString
    {
        /// <summary>
        /// 改行コードを除去する（開始タグ）
        /// </summary>
        protected override bool IsRemoveOpenNewLine
        {
            get { return false; }
        }

		/// <summary>
		/// Returns "formatCurrency"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:formatCurrency"; }
        }

		/// <summary>
		/// Returns "#,0"
		/// </summary>
        protected override string defaultValue
        {
            get
            {
                return DEFAULT_NUMBER_VALUE;
            }
        }

        private static string DEFAULT_NUMBER_VALUE = "{{ers/default_value/ers}}";

        /// <summary>
        /// ers:formatCurrency
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="tw"></param>
        /// <param name="sourceName"></param>
        /// <param name="o"></param>
        /// <param name="value"></param>
        public static new void Format(string sourceName, object o, string value, int? padLeftLength, int? padRightLength, int? fixedLength, string post_fix)
        {
            if (o is sbyte || o is byte || o is short || o is ushort || o is int || o is uint || o is long || o is ulong || o is int || o is int || o is int)
            {
                //数値型の場合
                if (value == DEFAULT_NUMBER_VALUE)
                {
                    value = "#,0";
                }
                long tempValue;
                if (o != null && Int64.TryParse(o.ToString(), out tempValue))
                {
                    o = tempValue;
                }
            }
            else if (o is float || o is double || o is decimal)
            {
                //浮動小数点型の場合
                if (value == DEFAULT_NUMBER_VALUE)
                {
                    value = "#,0.#,0";
                }
                double tempValue;
                if (o != null && Double.TryParse(o.ToString(), out tempValue))
                {
                    o = tempValue;
                }
            }
            else
            {
                //数値でないときは、ErsTagFormatString.Formatの規定の値を使用する。
                value = ErsTagFormatString.DEFAULT_VALUE;
                o = Convert.ToString(o);
            }

            ErsTagFormatString.Format(sourceName, o, value, padLeftLength, padRightLength, fixedLength, string.Empty);
        }
    }
}
