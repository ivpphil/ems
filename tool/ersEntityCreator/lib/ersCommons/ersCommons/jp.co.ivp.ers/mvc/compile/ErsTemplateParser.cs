﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.compile.entity;

namespace jp.co.ivp.ers.mvc.compile
{
    public class ErsTemplateParser
    {

        protected internal ErsViewContext viewContext { get; set; }

        public static List<ErsTemplateEntityBase> ParserList { get; set; }

        protected internal Encoding enc { get; set; }

         /// <summary>
        /// コンストラクタ / Constructor
        /// </summary>
        /// <param name="viewContext"></param>
        /// <param name="requestedPath"></param>
        public ErsTemplateParser(ErsViewContext viewContext, Encoding enc)
        {
            this.viewContext = viewContext;
            this.enc = enc;
        }

       /// <summary>
        /// テンプレートをC#コードに変換する
		/// <para>Convert the template to c# code</para>
        /// </summary>
        /// <param name="code"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        public virtual string Parse(string code, string className)
        {
            using (StringWriter sw = new StringWriter())
            {
                code = this.ParseErsTemplate(code);

                this.ConvertToCSharpCode(code, sw, className);

                return sw.ToString();
            }
        }

        /// <summary>
        /// FormとリンクにHidden自動付与用のタグを追加する。
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        protected internal virtual string ParseErsTemplate(string code)
        {

            this.CheckCSharpCode(code);

            //タグをParse
            ParserList.ForEach(
                obj =>
                {
                    obj.parser = this;
                    code = obj.Parse(code);
                });

            return code;

        }

        /// <summary>
        /// テンプレートにC#ブロックを書くのは許可しない。
		/// <para>Returns error if c# block has placed in the template</para>
        /// </summary>
        /// <param name="code"></param>
        protected void CheckCSharpCode(string code)
        {
            string CSharpCode = @"<%[^=]|<%=[^%]*\(";
            if (new Regex(CSharpCode).IsMatch(code))
            {
                throw new Exception("C#コードブロックは許可しない");
            }
        }

        /// <summary>
        /// テンプレートをC#のコードに変換する
		/// <para>Convert template to C# code</para>
        /// </summary>
        /// <param name="code"></param>
        /// <param name="sw"></param>
        /// <param name="className"></param>
        protected virtual void ConvertToCSharpCode(string code, StringWriter sw, string className)
        {
            sw.WriteLine("using System;");
            sw.WriteLine("using System.Collections.Generic;");
            sw.WriteLine("using System.IO;");
            sw.WriteLine("using System.Web;");
            sw.WriteLine("using System.Web.Mvc;");
            sw.WriteLine("using jp.co.ivp.ers.util;");
            sw.WriteLine("using jp.co.ivp.ers.mvc;");
            sw.WriteLine("using jp.co.ivp.ers.mvc.compile.entity;");
            sw.WriteLine("using jp.co.ivp.ers;");

            sw.WriteLine("public static class ErsTemplate" + className + " {");
            sw.WriteLine("public static void Convert(TextWriter tw, ErsViewContext context, ViewDataDictionary viewData, object partialModel) {");
            sw.WriteLine("var tempViewData = ErsViewHelper.ViewData;");
            sw.WriteLine("ErsViewHelper.ViewData = viewData;");
            sw.WriteLine("ErsViewHelper.TextWriter = tw;");

            int currentIndex = 0;
            while (0 <= currentIndex && currentIndex < code.Length)
            {
                string valueTagStart = "<%";
                string valueTagEnd = "%>";
                int valueTagStartIndex = code.IndexOf(valueTagStart, currentIndex);
                sw.WriteLine("ErsViewHelper.Write(\"{0}\");", EscapeString(valueTagStartIndex < 0 ? code.Substring(currentIndex) : code.Substring(currentIndex, valueTagStartIndex - currentIndex)));
                if (0 <= valueTagStartIndex)
                {
                    valueTagStartIndex += valueTagStart.Length;
                    int valueTagEndIndex = code.IndexOf(valueTagEnd, valueTagStartIndex);
                    if (0 <= valueTagEndIndex)
                    {
                        string cc = code.Substring(valueTagStartIndex, valueTagEndIndex - valueTagStartIndex);

                        if (cc.StartsWith("="))
                        {
                            string variable = ErsViewHelper.GetViewVariableString(cc.Substring(1));
                            ReplaceValueTag(sw, variable);
                        }
                        else
                        {
                            sw.WriteLine(cc);
                        }
                        valueTagStartIndex = valueTagEndIndex + valueTagEnd.Length;
                    }
                }
                currentIndex = valueTagStartIndex;
            }
            sw.WriteLine("ErsViewHelper.ViewData = tempViewData;");
            sw.WriteLine("}}");
        }

        /// <summary>
        /// &lt;%=***%&gt;の値を出力するC#のCodeを出力する
		/// <para>Replace the value tag &lt;%=***%&gt </para>
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="cc"></param>
        protected virtual void ReplaceValueTag(StringWriter sw, string variable)
        {
            sw.WriteLine("ErsViewHelper.WriteValue(({0}));", variable);
        }

        /// <summary>
        /// C#のCodeの文字列をエスケープする
		/// <para>Escape a string of C # Code</para>
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        protected virtual string EscapeString(string code)
        {
            return code.Replace("\\", "\\e").Replace("\"", "\\\"").Replace("\t", "\\t").Replace("\n", "\\n").Replace("\r", "\\r").Replace("\\e", "\\\\");
        }
    }
}
