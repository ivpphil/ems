﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagIsEqual
        : ErsTagBase
    {
		/// <summary>
		/// Returns True if a specified object is equal with a specified string
		/// </summary>
        public static bool IsEqual(string sourceName, object o, string s)
        {
            if (o != null)
            {
                if (s == null)
                {
                    return false;
                }
                else if (!(o is string) && o is System.Collections.IEnumerable)
                {
                    //列挙可能な方の場合は、要素を含むか検証をする
                    var list = (System.Collections.IEnumerable)o;
                    foreach (var listVal in list)
                    {
                        if (IsEqual(sourceName, listVal, s))
                        {
                            return true;
                        }
                    }
                    return false;
                }
                else if (o.ToString().ToLower() == s.ToLower())
                {
                    return true;
                }
                else if (o is Enum)
                {
                    if (IsEqual(sourceName, Convert.ChangeType(o, ((Enum)o).GetTypeCode()), s))
                    {
                        return true;
                    }

                    var enumS = ErsReflection.ConvertValue(o.GetType(), s);
                    if (enumS != null)
                    {
                        //enum対応
                        return IsEqual(sourceName, o, enumS);
                    }

                    return false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(s))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

		/// <summary>
		/// Returns True if a specified object is equal with another specified object
		/// </summary>
        public static bool IsEqual(string sourceName, object o1, object o2)
        {
            if (o1 == null && o2 == null)
            {
                return true;
            }

            if (o1 == null || o2 == null)
            {
                return false;
            }

            if (!(o1 is string) && o1 is System.Collections.IEnumerable)
            {
                //列挙可能な方の場合は、要素を含むか検証をする
                var list = (System.Collections.IEnumerable)o1;
                foreach (var listVal in list)
                {
                    if (IsEqual(sourceName, listVal, o2))
                    {
                        return true;
                    }
                }
                return false;
            }
            else if (o1.ToString().ToLower() == o2.ToString().ToLower())
            {
                return true;
            }
            else
            {
                if (o1 is Enum || o2 is Enum)
                {
                    if (o1 is Enum)
                    {
                        o1 = Convert.ChangeType(o1, ((Enum)o1).GetTypeCode());
                    }

                    if (o2 is Enum)
                    {
                        o2 = Convert.ChangeType(o2, ((Enum)o2).GetTypeCode());
                    }

                    return IsEqual(sourceName, o1, o2);
                }
                return false;
            }

        }

		/// <summary>
		/// Returns  "isEqual"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:isEqual"; }
        }

        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            if (attributes.ContainsKey("value"))
            {
                //値との比較, Comparison with the "value"
                return string.Format("<%if(ErsTagIsEqual.IsEqual(\"{0}\", {1}, \"{2}\")){{%>", attributes["src"], ErsViewHelper.GetViewVariableString(attributes["src"]), attributes["value"]);
            }
            else if (attributes.ContainsKey("variable"))
            {
                //変数との比較, Comparison with the "variable"
                return string.Format("<%if(ErsTagIsEqual.IsEqual(\"{0}\", {1}, {2})){{%>", attributes["src"], ErsViewHelper.GetViewVariableString(attributes["src"]), ErsViewHelper.GetViewVariableString(attributes["variable"]));
            }

            return string.Empty;
        }

		/// <summary>
		/// Returns '<%}%>'
		/// </summary>
        protected override string ReplaceCloseErsTag()
        {
            return "<%}%>";
        }
    }
}
