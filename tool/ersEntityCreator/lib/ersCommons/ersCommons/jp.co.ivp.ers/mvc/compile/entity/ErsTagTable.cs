﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagTable
        : ErsTagBase
    {

        protected override string ersTagName { get { return "ers:table"; } }

        /// <summary>
        /// ERSタグの開始タグをセットする
		/// <para>Set the start tag of the ERS tags</para>
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            //ループカウンターあり, with loop counter
            var roopcountername = "roopcounter" + attributes["record_key"];
            var src = attributes["src"];
            var variable = ErsViewHelper.GetViewVariableString(src);

            var parent_record_key = "\"\"";
            var parentTableCode = string.Empty;

            if (attributes.ContainsKey("parent_record_key"))
            {
                parent_record_key = ErsViewHelper.GetViewVariableString(attributes["parent_record_key"]);
                parentTableCode = "\" + " + parent_record_key + " + \"";
            }

            var recordkeyHidden = "tw.Write(\"<input type=\\\"hidden\\\" name=\\\"" + parentTableCode + "record_key" + attributes["name"] + "\\\" value=\\\"" + attributes["record_key"] + "\\\" />\");";

            var code = "<%{{var {3} = -1;" +
                recordkeyHidden +
                "ErsTagTable.CheckTableDataSorce(\"{0}\", {1});foreach(object {2} in ErsTagForeach.ForEach(\"{0}\", {1})){{" +
                "{3}++;var {4} = {5} + \"{4}_\" + {3} + \"_\";" +
                "((ErsBindableModel){2}).record_key = {4};%>";
            return string.Format(code, src, variable, attributes["variable"], roopcountername, attributes["record_key"], parent_record_key);
        }

        /// <summary>
        /// ERSタグの閉じタグをセットする
		/// <para>Set the closing tag for tag ERS</para>
        /// </summary>
        /// <param name="operation"></param>
        /// <returns></returns>
        protected override string ReplaceCloseErsTag()
        {
            return "<%}}%>";
        }

        public static void CheckTableDataSorce(string propertyName, object o)
        {
            if (o == null)
            {
                return;
            }

            var propertyType = o.GetType();
            if (!propertyType.IsGenericType || (propertyType.GetGenericTypeDefinition() != typeof(List<>) && propertyType.GetGenericTypeDefinition() != typeof(IList<>) && propertyType.GetGenericTypeDefinition() != typeof(IEnumerable<>)) || !propertyType.GetGenericArguments()[0].IsSubclassOf(typeof(ErsBindableModel)))
            {
                throw new Exception("datasource of &lt;table&gt; tag [" + propertyName + "] have to be IEnumerable&lt;subclass of ErsBindableModel&gt; or IList&lt;subclass of ErsBindableModel&gt;. Please check this html template.");
            }
        }
    }
}
