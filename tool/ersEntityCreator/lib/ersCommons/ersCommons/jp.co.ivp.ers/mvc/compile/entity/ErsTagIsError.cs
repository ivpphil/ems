﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagIsError
        : ErsTagBase
    {
		/// <summary>
		/// Returns "isError"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:isError"; }
        }

		/// <summary>
		/// Returns ErsTagIsEqual tag
		/// </summary>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            var modelName = ErsViewHelper.GetViewVariableString(".Model");

            if (attributes.ContainsKey("model"))
            {
                modelName = attributes["model"];
            }

            if (attributes.ContainsKey("src"))
            {
                //値との比較
                return string.Format("<%if(!ErsTagIsEqual.IsEqual(\"{0}\", ErsTagDispError.GetModelErrorMessage({1}, \"{0}\"), \"\")){{%>", attributes["src"], modelName);
            }
            else if (attributes.ContainsKey("variable"))
            {
                //変数との比較
                return string.Format("<%if(!ErsTagIsEqual.IsEqual(\"{0}\", ErsTagDispError.GetModelErrorMessage({1}, {2}), \"\")){{%>", attributes["variable"], modelName, ErsViewHelper.GetViewVariableString(attributes["variable"]));
            }

            return string.Empty;
        }

		/// <summary>
		/// Returns '<%}%>'
		/// </summary>
        protected override string ReplaceCloseErsTag()
        {
            return "<%}%>";
        }
    }
}
