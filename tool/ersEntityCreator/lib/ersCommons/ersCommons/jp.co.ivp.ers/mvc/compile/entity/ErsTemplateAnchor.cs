﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTemplateAnchor
        : ErsTagBase
    {
        protected override string ReplaceCloseErsTag()
        {
            return "</a>";
        }

        protected override string ersTagName
        {
            get { return "a"; }
        }

        /// <summary>
        /// リンクに、QueryString自動付与用のタグを追加する。
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            //aタグ検索, Search a tag 
            if (attributes.ContainsKey("href") && attributes["href"].HasValue())
            {
                var arrUrl = attributes["href"].Split('#');
                string url = arrUrl[0];
                string anchor = (arrUrl.Length > 1) ? "#" + arrUrl[1] : string.Empty;
                attributes["href"] = "<%ErsViewHelper.ConvertToAbsoluteUri(" + ErsViewHelper.ExpandVariable("\"" + url + "\"") + ");%>";
                if (!string.IsNullOrEmpty(url) || string.IsNullOrEmpty(anchor))
                {
                    attributes["href"] += "<%ErsTemplateAnchor.SecureQueryString(" + ErsViewHelper.ExpandVariable("\"" + url + "\"") + ", " + ErsViewHelper.GetViewVariableString(".SecureHidden") + ", " + ErsViewHelper.GetViewVariableString(".NormalHidden") + ");%>";
                }
                attributes["href"] += anchor;
            }

            return "<a " + attributes.ToString() + ">";
        }

        /// <summary>
        /// セキュアなQueryString情報を出力する。
        /// <para>Secure QueryString</para>
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="sourceName"></param>
        /// <param name="url"></param>
        /// <param name="values"></param>
        public static void SecureQueryString(string url, object secure_values, object normal_values)
        {
            if (secure_values == null || !(secure_values is List<Dictionary<string, object>>) || !(normal_values is List<Dictionary<string, object>>))
            {
                return;
            }

            var queryString = string.Empty;
            if (ErsViewHelper.CheckSessionUrl(url))
            {
                queryString += GetQueryString(url + queryString, (List<Dictionary<string, object>>)secure_values);
            }
            if (ErsViewHelper.CheckErsUrl(url))
            {
                queryString += GetQueryString(url + queryString, (List<Dictionary<string, object>>)normal_values);
            }

            ErsViewHelper.Write(queryString);
        }

        /// <summary>
        /// クエリストリングの値を取得する。
        /// <para>Gets the value of the query string.</para>
        /// </summary>
        /// <param name="url"></param>
        /// <param name="ersList"></param>
        /// <returns></returns>
        public static string GetQueryString(string url, List<Dictionary<string, object>> ersList)
        {
            if (ersList.Count == 0)
                return string.Empty;

            var queryString = string.Empty;
            foreach (var item in ersList)
            {
                if (item["value"] != null && (!(item["value"] is string) || (item["value"] is string && !string.IsNullOrEmpty((string)item["value"]))))
                {
                    queryString += "&" + ErsViewHelper.HtmlEncode(item["name"]) + "=" + ErsViewHelper.HtmlEncode(item["value"]);
                }
            }

            if (string.IsNullOrEmpty(queryString))
                return string.Empty;

            return GetJoinChar(url) + queryString.Substring(1);
        }

        /// <summary>
        /// URLとの結合文字列を取得する。
        /// <para>Get Join Character, if '?' exists return '&' else '?'</para>
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        protected static char GetJoinChar(string url)
        {
            if (url.Contains('?'))
                return '&';

            else
                return '?';
        }
    }
}
