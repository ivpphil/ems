﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagComment
        : ErsTagBase
    {
        protected override string ersTagName
        {
            get { return "ers:comment"; }
        }

        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            return "<%if(false){%>";
        }

        protected override string ReplaceCloseErsTag()
        {
            return "<%}%>";
        }
    }
}
