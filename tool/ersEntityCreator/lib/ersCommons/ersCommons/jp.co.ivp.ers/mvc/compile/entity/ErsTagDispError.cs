﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc;

namespace jp.co.ivp.ers.mvc.compile.entity
{
    public class ErsTagDispError
        : ErsTagBase
    {

		/// <summary>
		/// Returns "dispError"
		/// </summary>
        protected override string ersTagName
        {
            get { return "ers:dispError"; }
        }

		/// <summary>
		/// Replace open ERS tag 
		/// </summary>
        protected override string ReplaceOpenErsTag(ErsTagAttributes attributes)
        {
            var modelName = ErsViewHelper.GetViewVariableString(".Model");

            if (attributes.ContainsKey("model"))
            {
                modelName = attributes["model"];
            }

            if (!attributes.ContainsKey("src"))
            {
                //プロパティ名に指定がなければ、すべてのエラー出力
                var errorFilePath = ErsCommonContext.MapPath("~/Views/Partial/error_input.htm");
                if (File.Exists(errorFilePath))
                {
                    var errorCode = File.ReadAllText(errorFilePath, this.parser.enc);
                    errorCode = this.parser.ParseErsTemplate(errorCode);
                    return string.Format("<%if(!ErsTagIsEqual.IsEqual(\".Error.All\", ErsTagDispError.GetModelErrorMessage({0}, \"All\"), \"\")){{%>{1}<%}}%>", modelName, errorCode);
                }

                return string.Empty;
            }
            else
            {
                return string.Format("<%ErsTagDispError.Display({0}, \"{1}\");%>", modelName, attributes["src"]);
            }
        }

        /// <summary>
        /// HTMLエンコード無しで出力
		/// <para>HTML encode with output</para>
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="tw"></param>
        /// <param name="Error"></param>
        /// <param name="sourceName"></param>
        public static void Display(object argModel, string sourceName)
        {
            var errorMessage = GetModelErrorMessage(argModel, sourceName);

            if (!string.IsNullOrEmpty(errorMessage))
            {
                ErsViewHelper.Write(errorMessage.Replace(Environment.NewLine, ErsViewHelper.TAG_NEW_LINE));
            }
        }

		/// <summary>
		/// Get Model's error message
		/// </summary>
        public static string GetModelErrorMessage(object argModel, object sourceName)
        {
            var model = argModel as ErsModelBase;

            if (model == null)
            {
                return null;
            }

            string strSourceName = sourceName.ToString();

            if (strSourceName == "All")
            {
                return String.Join(Environment.NewLine, model.GetAllErrorMessageList());
            }
            else
            {
                return model.GetFieldErrorMessage(strSourceName);
            }
        }
    }
}
