﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Net;
using System.IO;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc
{
    public class HttpErrorModel
        : ErsModelBase
    {
        [ErsUniversalValidation(type = CHK_TYPE.NumericString, rangeFrom = 3, rangeTo = 3)]
        public virtual string statusCode { get; set; }

        [ErsUniversalValidation(type = CHK_TYPE.OneByteCharacter, rangeTo = 50)]
        public virtual string errorController { get; set; }

        public int validStatusCode { get { return this._statusCode; } }

        private int _statusCode;

        public string errorKey
        {
            get
            {
                return "http_error_" + this.validStatusCode.ToString();
            }
        }

        public virtual string ViewName { get; protected set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!int.TryParse(statusCode, out _statusCode) || !Enum.IsDefined(typeof(HttpStatusCode), _statusCode))
            {
                throw new HttpException((int)HttpStatusCode.NotFound, "HTTP/1.1 404 Not Found");
            }

            yield return null;
        }

        public virtual bool StatusCodeViewExists()
        {
            this.ViewName = this.GetViewName();

            var actualPath = ErsCommonContext.MapPath("~/Views/Partial/" + this.ViewName + ".htm");

            return File.Exists(actualPath);
        }

        protected virtual string GetViewName()
        {
            var viewName = this.errorKey;

            if (HttpContext.Current.Request.IsSecureConnection)
                viewName += "_ssl";

            return viewName;
        }

    }
}
