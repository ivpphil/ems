﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;
using System.Collections.Specialized;
using jp.co.ivp.ers.util;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using jp.co.ivp.ers.mvc;
using jp.co.ivp.ers.mvc.validation;

namespace jp.co.ivp.ers.mvc
{
    public class ErsModelBinder
        : DefaultModelBinder
    {

        private static string escModelStateKey = "jp.co.ivp.ers.mvc.ErsModelBinder.escModelState";

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            ErsDebug.CheckModel(bindingContext.ModelType, bindingContext.ModelName, (string)controllerContext.RouteData.Values["action"]);

            ErsDebug.WriteExecutingLog("Bind model executing");

            return base.BindModel(controllerContext, bindingContext);
        }

        protected override bool OnModelUpdating(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            ////一度退避して初期化
            ErsCommonContext.SetPooledObject(escModelStateKey, new ModelStateDictionary(bindingContext.ModelState));
            bindingContext.ModelState.Clear();

            ErsDebug.CheckController(controllerContext.Controller);

            var model = bindingContext.Model as ErsModelBase;
            var controller = controllerContext.Controller as ErsControllerBase;
            model.controller = controller;

            return base.OnModelUpdating(controllerContext, bindingContext);
        }

        protected override void OnModelUpdated(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var model = bindingContext.Model as ErsModelBase;

            //2012/12/10 ErsSchemaValidationチェックとValidateチェックのエラーメッセージ共存対応
            this.StoreErrorToModel(bindingContext, model);

            var escModelState = (ModelStateDictionary)ErsCommonContext.GetPooledObject(escModelStateKey);

            ////入力値検証の結果を退避して初期化
            this.AddErrorToModelState(bindingContext.ModelState, escModelState, model);
            bindingContext.ModelState.Clear();

            base.OnModelUpdated(controllerContext, bindingContext);

            this.StoreErrorToModel(bindingContext, model);

            this.AddErrorToModelState(escModelState, bindingContext.ModelState, null);
        }

        private void AddErrorToModelState(ModelStateDictionary srcModelState, ModelStateDictionary destModelState, ErsModelBase model)
        {
            ////退避分を追加
            foreach (var err in srcModelState)
            {
                if (err.Value.Errors != null && (model == null || !model.IsValidField(err.Key)))
                {
                    foreach (var error in err.Value.Errors)
                    {
                        destModelState.AddModelError(err.Key, error.ErrorMessage);
                    }
                }
            }
        }

        /// <summary>
        /// エラーをモデルに格納 / Store Error to Model
        /// </summary>
        /// <param name="bindingContext"></param>
        /// <param name="model"></param>
        private void StoreErrorToModel(ModelBindingContext bindingContext, ErsModelBase model)
        {
            if (model != null && !bindingContext.ModelState.IsValid)
            {
                foreach (var result in bindingContext.ModelState)
                {
                    foreach (var error in result.Value.Errors)
                    {
                        model.AddInvalidField(result.Key, error.ErrorMessage);
                    }
                }
            }
        }

        /// <summary>
        /// readonlyなフィールドにはバインドしない
		/// <para>Not bound to a readonly field</para>
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="bindingContext"></param>
        /// <param name="propertyDescriptor"></param>
        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor)
        {
            if (propertyDescriptor.IsReadOnly)
                return;

            var attributes = propertyDescriptor.Attributes.Cast<Attribute>();

            var validators = ErsBindModel.GetValidators(attributes);
            if (validators.Count() == 0)
            {
                //検証クラスが定義されていないクラスにはバインドしない
                return;
            }

            var htmlBinding = ErsBindModel.GetIHtmlBinding(attributes);
            if (htmlBinding == null)
                htmlBinding = new HtmlDefaultBinding();

            //THMLの特殊バインド
            object checkedValue;
            var requestValue = htmlBinding.GetRequestValue(propertyDescriptor.Name, controllerContext.RouteData);
            var tempResults = htmlBinding.BindProperty(
                bindingContext.Model,
                requestValue,
                propertyDescriptor.Name,
                propertyDescriptor.PropertyType,
                attributes, validators, out checkedValue);

            if (tempResults != null)
            {
                var results = tempResults.ToList();//何度も実行されてしまうので、一度Listに変換する。
                if (results.Count > 0)
                {
                    //表示用に不正な値を保持
                    var model = bindingContext.Model as ErsModelBase;
                    if (model != null)
                    {
                        model.InvalidValues[propertyDescriptor.Name] = requestValue;
                    }
                }

                foreach (var result in results)
                {
                    foreach (var name in result.MemberNames)
                    {
                        bindingContext.ModelState.AddModelError(name, result.ErrorMessage);
                    }
                }
            }

            if (checkedValue != null)
            {
                //THMLの特殊バインド(ここでセットする)
                //Bind special HTML (set here)
                SetProperty(controllerContext, bindingContext, propertyDescriptor, checkedValue);
            }
        }
    }
}
