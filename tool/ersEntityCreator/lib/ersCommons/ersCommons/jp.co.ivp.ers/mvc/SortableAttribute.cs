﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;

namespace jp.co.ivp.ers.mvc
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SortableAttribute
        : Attribute, ISortableAttribute
    {
        public int Order { get; private set; }

        public SortableAttribute(int order)
        {
            this.Order = order;
        }
    }
}
