﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc
{
    /// <summary>
    /// Hidden出力の値を保持します
    /// </summary>
    public struct ErsOutputHiddenTarget
    {
        /// <summary>
        /// Hiddenのグループ名
        /// </summary>
        public string groupName { get; set; }

        /// <summary>
        /// Hiddenの有効/無効（有効よりも無効が優先）
        /// </summary>
        public bool enable { get; set; }

        /// <summary>
        /// Hiddenに出力される値
        /// </summary>
        public List<Dictionary<string, object>> values { get; set; }
    }
}
