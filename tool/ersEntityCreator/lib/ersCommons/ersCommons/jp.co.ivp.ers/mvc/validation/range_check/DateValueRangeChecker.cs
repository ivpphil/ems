﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation.validator;

namespace jp.co.ivp.ers.mvc.validation.range_check
{
    public class DateValueRangeChecker
        : IValueRangeChecker
    {
        public const string CurrentDate = "now";

        public const string YesterdayDate = "yesterday";

        public const string TomorrowDate = "tomorrow";

        public virtual bool CheckRange(string argTarget, string argRangeFrom, string argRangeTo, ErsValidatorBase validator)
        {
            bool isValid = true;

            DateTime? rangeFrom;
            DateTime? rangeTo;
            string rangeFromForError;
            string rangeToForError;

            DateTime currentDate = DateTime.Now;

            //チェック対象
            var target = ConvertToNullable.ParseToDateTime(argTarget);

            //From
            if (!string.IsNullOrEmpty(argRangeFrom) && argRangeFrom.ToLower() == CurrentDate)
            {
                rangeFrom = currentDate;
                rangeFromForError = ErsResources.GetFieldName("current_date");
            }
            else if (!string.IsNullOrEmpty(argRangeFrom) && argRangeFrom.ToLower() == YesterdayDate)
            {
                rangeFrom = currentDate.AddDays(-1);
                rangeFromForError = ErsResources.GetFieldName("yesterday_date");
            }
            else if (!string.IsNullOrEmpty(argRangeFrom) && argRangeFrom.ToLower() == TomorrowDate)
            {
                rangeFrom = currentDate.AddDays(1);
                rangeFromForError = ErsResources.GetFieldName("tomorrow_date");
            }
            else
            {
                rangeFrom = ConvertToNullable.ParseToDateTime(argRangeFrom);
                rangeFromForError = this.GetDateForError(rangeFrom);
            }

            //To
            if (!string.IsNullOrEmpty(argRangeTo) && argRangeTo.ToLower() == CurrentDate)
            {
                rangeTo = currentDate;
                rangeToForError = ErsResources.GetFieldName("current_date");
            }
            else if (!string.IsNullOrEmpty(argRangeFrom) && argRangeFrom.ToLower() == YesterdayDate)
            {
                rangeTo = currentDate.AddDays(-1);
                rangeToForError = ErsResources.GetFieldName("yesterday_date");
            }
            else if (!string.IsNullOrEmpty(argRangeFrom) && argRangeFrom.ToLower() == TomorrowDate)
            {
                rangeTo = currentDate.AddDays(1);
                rangeToForError = ErsResources.GetFieldName("tomorrow_date");
            }
            else
            {
                rangeTo = ConvertToNullable.ParseToDateTime(argRangeTo);
                rangeToForError = this.GetDateForError(rangeTo);
            }

            //from-to
            if (rangeFrom != null && rangeTo != null && rangeFrom != null && rangeTo != null && (target < rangeFrom || target > rangeTo))
            {
                if (rangeFrom == rangeTo)
                {
                    validator.AppendError(ErsResources.GetMessage("10048", validator.displayName, rangeFromForError));
                }
                else
                {
                    validator.AppendError(ErsResources.GetMessage("10047", validator.displayName, rangeToForError, rangeFromForError));
                }
                isValid = false;
            }
            //from
            else if (rangeFrom != null && rangeFrom != null && target < rangeFrom)
            {
                validator.AppendError(ErsResources.GetMessage("10045", validator.displayName, rangeFromForError));
                isValid = false;
            }
            //to
            else if (rangeTo != null && rangeTo != null && target > rangeTo)
            {
                validator.AppendError(ErsResources.GetMessage("10046", validator.displayName, rangeToForError));
                isValid = false;
            }

            return isValid;
        }

        protected virtual string GetDateForError(DateTime? rangeFrom)
        {
            if (rangeFrom.HasValue)
            {
                if (rangeFrom == rangeFrom.Value.GetStartForSearch())
                {
                    return rangeFrom.Value.ToString("yyyy/MM/dd");
                }
                else
                {
                    return rangeFrom.Value.ToString("yyyy/MM/dd HH:mm:ss");
                }
            }
            return string.Empty;
        }


        public string CutDown(string argTarget, string argRangeFrom, string argRangeTo)
        {
            DateTime? rangeFrom;
            DateTime? rangeTo;

            DateTime currentDate = DateTime.Now;

            //チェック対象
            var target = ConvertToNullable.ParseToDateTime(argTarget);

            //From
            if (!string.IsNullOrEmpty(argRangeFrom) && argRangeFrom.ToLower() == CurrentDate)
            {
                rangeFrom = currentDate;
            }
            else if (!string.IsNullOrEmpty(argRangeFrom) && argRangeFrom.ToLower() == YesterdayDate)
            {
                rangeFrom = currentDate.AddDays(-1);
            }
            else if (!string.IsNullOrEmpty(argRangeFrom) && argRangeFrom.ToLower() == TomorrowDate)
            {
                rangeFrom = currentDate.AddDays(1);
            }
            else
            {
                rangeFrom = ConvertToNullable.ParseToDateTime(argRangeFrom);
            }

            //To
            if (!string.IsNullOrEmpty(argRangeTo) && argRangeTo.ToLower() == CurrentDate)
            {
                rangeTo = currentDate;
            }
            else if (!string.IsNullOrEmpty(argRangeTo) && argRangeTo.ToLower() == YesterdayDate)
            {
                rangeTo = currentDate.AddDays(-1);
            }
            else if (!string.IsNullOrEmpty(argRangeTo) && argRangeTo.ToLower() == TomorrowDate)
            {
                rangeTo = currentDate.AddDays(1);
            }
            else
            {
                rangeTo = ConvertToNullable.ParseToDateTime(argRangeTo);
            }

            //from
            if (rangeFrom != null && rangeFrom != null && target < rangeFrom)
            {
                return rangeFrom.ToString();
            }
            //to
            else if (rangeTo != null && rangeTo != null && target > rangeTo)
            {
                return rangeTo.ToString();
            }

            return argTarget;
        }
    }
}
