﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    class ErsFormatHyphenNumber
        : ErsFormatBase
    {
		/// <summary>
		/// Convert string to half number and remove [-]
		/// </summary>
        public override string FormatValue(string value, bool CutDown, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            value = this.RemoveInvalidByte(value);

            value = ErsCommon.ConvertToHalfNumber(value);
            //not to truncate "-" when precedes the string
            if (value != String.Empty)
            { 
                    value = value.Replace("-", "");
            }

            if (CutDown)
            {
                value = this.CutDown(value, rangeFrom, rangeTo, rangeChecker);
            }

            return value;
        }

		/// <summary>
		/// Convert string to half number
		/// </summary>
        public override string NormalizeValue(string value)
        {
            value = this.RemoveInvalidByte(value);

            return ErsCommon.ConvertToHalfNumber(value);
        }
    }
}
