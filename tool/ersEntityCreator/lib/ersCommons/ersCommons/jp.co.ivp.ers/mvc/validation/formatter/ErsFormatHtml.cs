﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    class ErsFormatHtml
        : ErsFormatBase
    {
        public override string FormatValue(string value, bool CutDown, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            value = this.RemoveInvalidByte(value);
            
            //xhtmlでないhtmlに対応 / replace xhtml that do not correspond to html
            value = Regex.Replace(value, "<\\s*br\\s*>", "<br />", RegexOptions.IgnoreCase);

            if (CutDown)
            {
                value = this.CutDown(value, rangeFrom, rangeTo, rangeChecker);
            }

            return value;
        }

        public override string NormalizeValue(string value)
        {
            //xhtmlでないhtmlに対応 / replace xhtml that do not correspond to html
            return Regex.Replace(value, "<\\s*br\\s*>", "<br />", RegexOptions.IgnoreCase);
        }
    }
}
