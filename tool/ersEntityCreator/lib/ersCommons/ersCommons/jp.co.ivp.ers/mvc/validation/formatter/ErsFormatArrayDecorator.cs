﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    /// <summary>
    /// 整形を、配列対応にする。/ Corresponding to the sequence
    /// </summary>
    class ErsFormatArrayDecorator
        : ErsFormatBase
    {
        protected ErsFormatBase formatter;

        public ErsFormatArrayDecorator(ErsFormatBase formatter)
        {
            this.formatter = formatter;
        }

        public override string FormatValue(string value, bool CutDown, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            if(value == null)
                value = string.Empty;

            var arrValue = value.Split(new[] { ',' });

            var strRet = string.Empty;
            foreach (var val in arrValue)
            {
                strRet += "," + formatter.FormatValue(val, CutDown, rangeFrom, rangeTo, rangeChecker);
            }

            return strRet.Substring(1);
        }

        public override string NormalizeValue(string value)
        {
            if (value == null)
                value = string.Empty;

            var arrValue = value.Split(new[] { ',' });

            var strRet = string.Empty;
            foreach (var val in arrValue)
            {
                strRet += "," + formatter.NormalizeValue(val);
            }

            return strRet.Substring(1);
        }
    }
}
