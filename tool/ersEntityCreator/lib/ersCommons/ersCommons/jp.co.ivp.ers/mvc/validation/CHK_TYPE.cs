﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.ivp.ers.mvc.validation
{
    public enum CHK_TYPE
    {
        /// <summary>
        /// 全て
        /// </summary>
        All,

        /// <summary>
        /// 全角カナのみ
        /// </summary>
        FullKana,

        /// <summary>
        /// 平仮名のみ
        /// </summary>
        Hiragana,

        /// <summary>
        /// 機種依存文字
        /// </summary>
        External,

        /// <summary>
        /// メールアドレス
        /// </summary>
        EMailAddr,

        /// <summary>
        /// 半角数値
        /// </summary>
        Numeric,

        /// <summary>
        /// 半角数値（文字列）
        /// </summary>
        NumericString,

        /// <summary>
        /// 半角英数
        /// </summary>
        HalfAlphabetOrNumber,

        /// <summary>
        /// URL
        /// </summary>
        WebAddress,

        /// <summary>
        /// 日付
        /// </summary>
        Date,

        /// <summary>
        /// 日付(時:分:秒なし)
        /// </summary>
        DateWithOutTime,

        /// <summary>
        /// 全角（文字列）
        /// </summary>
        FullString,

        /// <summary>
        /// 1バイト文字（禁則除く）
        /// </summary>
        OneByteCharacter,

        /// <summary>
        /// HTML
        /// </summary>
        HTML,

        /// <summary>
        /// テンプレート（&lt;%=%&gt;と&lt;ers:**&gt;を許可）
        /// </summary>
        Templates,

        /// <summary>
        /// IPアドレス
        /// </summary>
        IP,

        /// <summary>
        /// 住所形式
        /// </summary>
        Address,

        // takemoto そのうち正規表現
        /// <summary>
        /// 郵便番号（3-4）
        /// </summary>
        Zip,

        /// <summary>
        /// 半角数値 + "-"
        /// </summary>
        HyphenNumber,

        /// <summary>
        /// 半角数値 + "/"
        /// </summary>
        SlashNumber,

        /// <summary>
        /// 電話番号（5-4-4）
        /// </summary>
        Tel,

        /// <summary>
        /// HTML &amp; テンプレート（&lt;%=%&gt;と&lt;ers:**&gt;を許可）
        /// </summary>
        HTMLTemplates,

        /// <summary>
        /// コネクションストリング
        /// </summary>
        ConnectionString,
    }
}
