﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateConnectionString
        : ErsValidatorBase
    {
        /// <summary>
        /// 全角含むことができる [Can be contain Full string]
        /// </summary>
        public override bool AllowFullString { get { return true; } }

        /// <summary>
        /// 半角含むことができる [Can be contain Half string]
        /// </summary>
        public override bool AllowHalfString { get { return true; } }

        /// <summary>
        /// 禁則チェック [Check prohibition characters]
        /// </summary>
        /// <param name="target">禁則チェック対象文字列 [Target string dor check]</param>
        /// <returns>true : 行わない [Do not] ／ false : 行う [Do]</returns>
        public override bool CheckProhibition(string target)
        {
            return true;
        }

        /// <summary>
        /// 文字種チェック [Check character type]
        /// </summary>
        /// <param name="target">チェック対象文字列 [Target string for check]</param>
        /// <returns>true : チェックOK [Check succeeded] ／ false : チェックNG [Check failed]</returns>
        public override bool CheckType(string target)
        {
            bool isValid = true;

            if (!string.IsNullOrEmpty(target))
            {
                var parameters = target.Split(';');

                foreach (var param in parameters)
                {
                    if (!Regex.IsMatch(param, @"^(Server|Port|Protocol|Database|User Id|Password|SSL|Pooling|MinPoolSize|MaxPoolSize|Encoding|Timeout|CommandTimeout|Sslmode|ConnectionLifeTime|SyncNotification)=.+$"))
                    {
                        this.AppendError(ErsResources.GetMessage("10025", displayName));
                        isValid = false;
                    }
                }
            }

            return isValid;
        }
    }
}
