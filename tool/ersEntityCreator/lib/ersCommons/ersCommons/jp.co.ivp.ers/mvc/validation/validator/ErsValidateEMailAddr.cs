﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateEMailAddr
        : ErsValidatorBase
    {
        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>Returns false</para>
        /// </summary>
        public override bool AllowFullString { get { return false; } }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>Returns true</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return true; } }

        /// <summary>
        /// メールアドレス / E-mail Address
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="target"></param>
        /// <param name="fieldName"></param>
        public override bool CheckType(string target)
        {

            bool isValid = true;

            if (!IsHalf(target.Trim()))
            {
                this.AppendError(ErsResources.GetMessage("10005", displayName));
                isValid = false;
            }
            else if (!Regex.IsMatch(target, "^[0-9a-zA-Z_\\.\\-\\+\\~]+@(?:[0-9a-zA-Z_\\-]+\\.)+[0-9a-zA-Z_\\-]+$"))
            {
                this.AppendError(ErsResources.GetMessage("10005", displayName));
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// 禁則チェック(スルーされる)
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public override bool CheckProhibition(string target)
        {
            //HTMLは、文字チェックする際に禁則チェックを行うので、ここではチェックしない
            return true;
        }

    }
}
