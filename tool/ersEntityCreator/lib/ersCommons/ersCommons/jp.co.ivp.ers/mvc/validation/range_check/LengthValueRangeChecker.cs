﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation.validator;

namespace jp.co.ivp.ers.mvc.validation.range_check
{
    public class LengthValueRangeChecker
        : IValueRangeChecker
    {
        public virtual bool CheckRange(string argTarget, string argRangeFrom, string argRangeTo, ErsValidatorBase validator)
        {
            bool isValid = true;

            string target = argTarget;
            int? rangeFrom = ConvertToNullable.ParseToInt32(argRangeFrom);
            int? rangeTo = ConvertToNullable.ParseToInt32(argRangeTo);

            //from-to
            if (rangeFrom != null && rangeTo != null && rangeFrom != 0 && rangeTo != 0 && (target.Length < rangeFrom || target.Length > rangeTo))
            {
                if (rangeFrom == rangeTo)
                {
                        validator.AppendError(ErsResources.GetMessage("10038", validator.displayName, rangeFrom));
                }
                else
                {
                    validator.AppendError(ErsResources.GetMessage("10035", validator.displayName, rangeFrom, rangeTo));
                }
                isValid = false;
            }
            //from
            else if (rangeFrom != null && rangeFrom != 0 && target.Length < rangeFrom)
            {
                validator.AppendError(ErsResources.GetMessage("10036", validator.displayName, rangeFrom));
                isValid = false;
            }
            //to
            else if (rangeTo != null && rangeTo != 0 && target.Length > rangeTo)
            {
                validator.AppendError(ErsResources.GetMessage("10037", validator.displayName, rangeTo));
                isValid = false;
            }

            return isValid;
        }


        public string CutDown(string argTarget, string argRangeFrom, string argRangeTo)
        {
            string target = argTarget;
            int? rangeFrom = ConvertToNullable.ParseToInt32(argRangeFrom);
            int? rangeTo = ConvertToNullable.ParseToInt32(argRangeTo);

            //from
            if (rangeFrom != null && rangeFrom != 0 && target.Length < rangeFrom)
            {
                return target.PadLeft(rangeFrom.Value);
            }
            //to
            else if (rangeTo != null && rangeTo != 0 && target.Length > rangeTo)
            {
                return target.Substring(0, rangeTo.Value);
            }

            return argTarget;
        }
    }
}
