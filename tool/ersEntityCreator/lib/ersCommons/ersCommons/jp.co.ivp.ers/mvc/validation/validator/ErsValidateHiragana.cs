﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateHiragana
        : ErsValidateFullString
    {
        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>Returns True</para>
        /// </summary>
        public override bool AllowFullString { get { return true; } }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>Returns False</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return false; } }

        /// <summary>
        /// 平仮名のみ / Hiragana only
        /// （\u30FC『ー』、全角・半角数字も許可）
        /// </summary>
        /// <param name="target"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public override bool CheckType(string target)
        {
            if (!IsFull(Regex.Replace(target.Trim(), @"\d|[０-９]", string.Empty)))
            {
                this.AppendError(ErsResources.GetMessage("10040", displayName));
                return false;
            }
            else if (!Regex.IsMatch(target, @"^(\p{IsHiragana}|\u30FC|\d|[０-９])*$"))
            {
                this.AppendError(ErsResources.GetMessage("10040", displayName));
                return false;
            }

            return true;
        }
    }
}
