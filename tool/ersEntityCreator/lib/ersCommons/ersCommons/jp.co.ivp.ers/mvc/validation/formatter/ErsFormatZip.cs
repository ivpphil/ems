﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    class ErsFormatZip
        : ErsFormatBase
    {
		/// <summary>
		/// Convert string to half number and replace value to white space base on [^\\d] expression
		/// </summary>
        public override string FormatValue(string value, bool CutDown, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            value = this.RemoveInvalidByte(value);

            value = ErsCommon.ConvertToHalfNumber(value);
            if (!string.IsNullOrEmpty(value) && value.Length >= 7 && !value.Contains('-'))
            {
                value = value.Substring(0, 3) + "-" + value.Substring(3, 4);
            }

            if (CutDown)
            {
                value = this.CutDown(value, rangeFrom, rangeTo, rangeChecker);
            }

            return value;
        }

        public override string NormalizeValue(string value)
        {
            value = this.RemoveInvalidByte(value);

            return ErsCommon.ConvertToHalfNumber(value);
        }
    }
}
