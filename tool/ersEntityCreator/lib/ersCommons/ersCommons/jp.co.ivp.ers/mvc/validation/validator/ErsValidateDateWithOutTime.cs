﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation.range_check;
using System.Text.RegularExpressions;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateDateWithOutTime
        : ErsValidateDate
    {
        /// <summary>
        /// 日付型チェック
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="target"></param>
        /// <param name="DisplayName"></param>
        /// <returns></returns>
        public override bool CheckType(string target)
        {
            bool isValid = true;

            if (!base.CheckType(target))
            {
                isValid = false;
            }
            else if (!Regex.IsMatch(target, @"\d{4}[/\.年]\d{1,2}[/\.月]\d{1,2}日?$"))
            {
                this.AppendError(ErsResources.GetMessage("10001", displayName));
                isValid = false;
            }

            return isValid;
        }
    }
}
