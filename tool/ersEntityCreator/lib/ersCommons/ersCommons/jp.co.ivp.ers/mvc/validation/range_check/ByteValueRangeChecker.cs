﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation.validator;

namespace jp.co.ivp.ers.mvc.validation.range_check
{
    public class ByteValueRangeChecker
        : IValueRangeChecker
    {
        /// <summary>
        /// レンジチェック / Range check
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="rangeFrom"></param>
        /// <param name="rangeTo"></param>
        public virtual bool CheckRange(string argTarget, string argRangeFrom, string argRangeTo, ErsValidatorBase validator)
        {
            bool isValid = true;

            string target = argTarget;
            int? rangeFrom = ConvertToNullable.ParseToInt32(argRangeFrom);
            int? rangeTo = ConvertToNullable.ParseToInt32(argRangeTo);

            //from-to
            if (rangeFrom != null && rangeTo != null && rangeFrom != 0 && rangeTo != 0 && (ErsCommon.GetByteCount(target) < rangeFrom || ErsCommon.GetByteCount(target) > rangeTo))
            {
                if (rangeFrom == rangeTo)
                {
                    if (validator.AllowFullString && validator.AllowHalfString)
                        validator.AppendError(ErsResources.GetMessage("10030", validator.displayName, rangeFrom / 2, rangeFrom));
                    else if (validator.AllowFullString)
                        validator.AppendError(ErsResources.GetMessage("10031", validator.displayName, rangeFrom / 2));
                    else if (validator.AllowHalfString)
                        validator.AppendError(ErsResources.GetMessage("10032", validator.displayName, rangeFrom));
                }
                else
                {
                    if (validator.AllowFullString && validator.AllowHalfString)
                        validator.AppendError(ErsResources.GetMessage("10013", validator.displayName, rangeFrom / 2, rangeTo / 2, rangeFrom, rangeTo));
                    else if (validator.AllowFullString)
                        validator.AppendError(ErsResources.GetMessage("10014", validator.displayName, rangeFrom / 2, rangeTo / 2));
                    else if (validator.AllowHalfString)
                        validator.AppendError(ErsResources.GetMessage("10015", validator.displayName, rangeFrom, rangeTo));
                }
                isValid = false;
            }
            //from
            else if (rangeFrom != null && rangeFrom != 0 && ErsCommon.GetByteCount(target) < rangeFrom)
            {
                if (validator.AllowFullString && validator.AllowHalfString)
                    validator.AppendError(ErsResources.GetMessage("10016", validator.displayName, rangeFrom / 2, rangeFrom));
                else if (validator.AllowFullString)
                    validator.AppendError(ErsResources.GetMessage("10017", validator.displayName, rangeFrom / 2));
                else if (validator.AllowHalfString)
                    validator.AppendError(ErsResources.GetMessage("10018", validator.displayName, rangeFrom));
                isValid = false;
            }
            //to
            else if (rangeTo != null && rangeTo != 0 && ErsCommon.GetByteCount(target) > rangeTo)
            {
                if (validator.AllowFullString && validator.AllowHalfString)
                    validator.AppendError(ErsResources.GetMessage("10019", validator.displayName, rangeTo / 2, rangeTo));
                else if (validator.AllowFullString)
                    validator.AppendError(ErsResources.GetMessage("10020", validator.displayName, rangeTo / 2));
                else if (validator.AllowHalfString)
                    validator.AppendError(ErsResources.GetMessage("10021", validator.displayName, rangeTo));
                isValid = false;
            }

            return isValid;
        }



        public string CutDown(string argTarget, string argRangeFrom, string argRangeTo)
        {
            string target = argTarget;
            int? rangeFrom = ConvertToNullable.ParseToInt32(argRangeFrom);
            int? rangeTo = ConvertToNullable.ParseToInt32(argRangeTo);

            //from
            if (rangeFrom != null && rangeFrom != 0 && ErsCommon.GetByteCount(target) < rangeFrom)
            {
                while (true)
                {
                    target = " " + target;
                    if (ErsCommon.GetByteCount(target) == rangeFrom)
                    {
                        return target;
                    }
                }
            }
            //to
            else if (rangeTo != null && rangeTo != 0 && ErsCommon.GetByteCount(target) > rangeTo)
            {
                while (true)
                {
                    target = target.Substring(0, target.Length - 1);
                    if (ErsCommon.GetByteCount(target) <= rangeTo)
                    {
                        return target;
                    }
                }
            }

            return argTarget;
        }
    }
}
