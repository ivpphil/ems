﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.validation.validator;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    /// <summary>
	/// 電話番号形式チェック / Check phone number format
    /// </summary>
    class ErsValidateTel
        : ErsValidatorBase
    {
        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>Returns False</para>
        /// </summary>
        public override bool AllowFullString { get { return false; } }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>Returns True</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return true; } }

        /// <summary>
        /// 電話番号 / Phone Number
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="target"></param>
        /// <param name="fieldName"></param>
        public override bool CheckType(string target)
        {

            bool isValid = true;

            if (!Regex.IsMatch(target, @"^\d{1,5}-\d{1,4}-\d{1,4}$"))
            {
                this.AppendError(ErsResources.GetMessage("10039", displayName));
                isValid = false;
            }

            return isValid;
        }
    }
}
