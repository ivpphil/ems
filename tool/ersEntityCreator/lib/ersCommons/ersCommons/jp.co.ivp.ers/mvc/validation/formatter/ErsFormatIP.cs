﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    class ErsFormatIP
        : ErsFormatBase
    {
		/// <summary>
		/// Convert string to half number and remove white space after [.]
		/// </summary>
        public override string FormatValue(string value, bool CutDown, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            value = this.RemoveInvalidByte(value);

            value = ErsCommon.ConvertToHalfNumber(value);
            value = value.Replace("．", ".");

            if (CutDown)
            {
                value = this.CutDown(value, rangeFrom, rangeTo, rangeChecker);
            }

            return value;
        }

		/// <summary>
		/// Remove white space after [.] and then convert string to half number
		/// </summary>
        public override string NormalizeValue(string value)
        {
            value = this.RemoveInvalidByte(value);

            value = value.Replace("．", ".");
            return ErsCommon.ConvertToHalfNumber(value);
        }
    }
}
