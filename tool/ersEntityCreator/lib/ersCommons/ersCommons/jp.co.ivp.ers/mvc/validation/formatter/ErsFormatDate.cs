﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation.range_check;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    class ErsFormatDate
        : ErsFormatBase
    {
        /// <summary>
        /// Format the string value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override string FormatValue(string value, bool CutDown, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            value = this.RemoveInvalidByte(value);

            if (CutDown)
            {
                value = this.CutDown(value, rangeFrom, rangeTo, rangeChecker);
            }

            return value;
        }

        /// <summary>
        /// Normalize the string value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override string NormalizeValue(string value)
        {
            value = this.RemoveInvalidByte(value);

            return string.Empty;
        }

        protected override range_check.IValueRangeChecker GetValueRangeChecker(EnumTextValueRangeChecker? rangeChecker)
        {
            if (rangeChecker.HasValue)
            {
                throw new Exception("CHK_TYPE.Date don't support specifying rangeChecker.");
            }
            
            return new DateValueRangeChecker();
        }
    }
}
