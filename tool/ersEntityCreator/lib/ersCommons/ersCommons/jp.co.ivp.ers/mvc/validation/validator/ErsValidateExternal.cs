﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateExternal
        : ErsValidatorBase
    {
        /// <summary>
        /// 全角を含むことができる場合True
        /// </summary>
        public override bool AllowFullString { get { return true; } }

        /// <summary>
        /// 半角を含むことができる場合True
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return true; } }

        /// <summary>
        /// 文字種別チェック
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public override bool CheckType(string target)
        {
            bool isValid = true;

            Encoding encode = ErsEncoding.ShiftJIS;

            // １文字ずつShift-JISバイトコードでチェックする
            foreach (var c in target)
            {
                byte[] b = encode.GetBytes(c.ToString());

                if (b.Length < 2)
                {
                    continue;
                }

                ushort i = (ushort)((0xFF00 & (b[0] << 8)) | b[1]);

                // 絵文字
                if (i >= (ushort)0xF040 && i <= (ushort)0xF9FF)
                {
                    isValid = false;
                    break;
                }

                switch (b[0])
                {
                    // 13区
                    case 0x87:
                        if (b[1] >= 0x40 && b[1] <= 0x9E)
                        {
                            isValid = false;
                        }
                        break;

                    // 89区・90区
                    case 0xED:
                    // 91区・92区
                    case 0xEE:
                    // 115区・116区
                    case 0xFA:
                    // 117区・118区
                    case 0xFB:
                        if ((b[1] >= 0x40 && b[1] <= 0x9E) || (b[1] >= 9F && b[1] <= 0xFC))
                        {
                            isValid = false;
                        }
                        break;

                    // 119区
                    case 0xFC:
                        if (b[1] >= 0x40 && b[1] <= 0x4B)
                        {
                            isValid = false;
                        }
                        break;
                }
            }

            if (!isValid)
            {
                this.AppendError(ErsResources.GetMessage("10002", displayName));
                return false;
            }

            return true;
        }
    }
}
