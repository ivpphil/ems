﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.mvc.validation.range_check;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    class ErsFormatNumeric
        : ErsFormatBase
    {
		/// <summary>
		/// Convert string to half number
		/// </summary>
        public override string FormatValue(string value, bool CutDown, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            value = this.RemoveInvalidByte(value);

            value = ErsCommon.ConvertToHalfNumber(value);

            if (CutDown)
            {
                value = this.CutDown(value, rangeFrom, rangeTo, rangeChecker);
            }

            return value;
        }

		/// <summary>
		/// Convert string to half number and replace value to white space base on [^\\d-] expression
		/// </summary>
        public override string NormalizeValue(string value)
        {
            value = this.RemoveInvalidByte(value);

            value = ErsCommon.ConvertToHalfNumber(value);
            return new Regex("[^\\d-]").Replace(value, string.Empty);
        }

        protected override range_check.IValueRangeChecker GetValueRangeChecker(EnumTextValueRangeChecker? rangeChecker)
        {
            if (rangeChecker.HasValue)
            {
                throw new Exception("CHK_TYPE.Numeric don't support specifying rangeChecker.");
            }

            return new NumericValueRangeChecker();
        }
    }
}
