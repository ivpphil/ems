﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    class ErsFormatHalfAlphabetOrNumber
        : ErsFormatBase
    {
		/// <summary>
		/// Format value to half-character or half-number
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
        public override string FormatValue(string value, bool CutDown, string rangeFrom, string rangeTo, EnumTextValueRangeChecker? rangeChecker)
        {
            value = this.RemoveInvalidByte(value);

            value = ErsCommon.ConvertToHalfAlphabet(value);
            value = ErsCommon.ConvertToHalfNumber(value);

            if (CutDown)
            {
                value = this.CutDown(value, rangeFrom, rangeTo, rangeChecker);
            }
            return value;
        }
    }
}
