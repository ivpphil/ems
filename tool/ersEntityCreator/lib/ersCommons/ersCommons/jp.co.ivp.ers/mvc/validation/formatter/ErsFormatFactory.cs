﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation.formatter;

namespace jp.co.ivp.ers.mvc.validation.formatter
{
    public class ErsFormatFactory
    {
        /// <summary>
        /// Validatorを取得する。/ Gets the validator
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static ErsFormatBase GetFormatter(CHK_TYPE type, bool isArray)
        {
            ErsFormatBase formatter = null;

            if (type == CHK_TYPE.All)
                formatter = new ErsFormatAll();

            else if (type == CHK_TYPE.EMailAddr)
                formatter = new ErsFormatEMailAddr();

            else if (type == CHK_TYPE.FullKana)
                formatter = new ErsFormatFullKana();

            else if (type == CHK_TYPE.Hiragana)
                formatter = new ErsFormatHiragana();

            else if (type == CHK_TYPE.External)
                formatter = new ErsFormatExternal();

            else if (type == CHK_TYPE.FullString)
                formatter = new ErsFormatFullString();

            else if (type == CHK_TYPE.HalfAlphabetOrNumber)
                formatter = new ErsFormatHalfAlphabetOrNumber();

            else if (type == CHK_TYPE.Numeric)
                formatter = new ErsFormatNumeric();

            else if (type == CHK_TYPE.NumericString)
                formatter = new ErsFormatNumericString();

            else if (type == CHK_TYPE.Date)
                formatter = new ErsFormatDate();

            else if (type == CHK_TYPE.DateWithOutTime)
                formatter = new ErsFormatDateWithOutTime();

            else if (type == CHK_TYPE.WebAddress)
                formatter = new ErsFormatWebAddress();

            else if (type == CHK_TYPE.OneByteCharacter)
                formatter = new ErsFormatOneByteCharacter();

            else if (type == CHK_TYPE.HTML)
                formatter = new ErsFormatHtml();

            else if (type == CHK_TYPE.Templates)
                formatter = new ErsFormatTemplates();

            else if (type == CHK_TYPE.IP)
                formatter = new ErsFormatIP();

                // takemoto そのうち正規表現
            else if (type == CHK_TYPE.Zip)
                formatter = new ErsFormatZip();

            else if (type == CHK_TYPE.HyphenNumber)
                formatter = new ErsFormatHyphenNumber();

            else if (type == CHK_TYPE.SlashNumber)
                formatter = new ErsFormatSlashNumber();

            else if (type == CHK_TYPE.Tel)
                formatter = new ErsFormatTel();

            //for @mail Html_Body field
            else if (type == CHK_TYPE.HTMLTemplates)
                formatter = new ErsFormatHtmlTemplates();

            else if (type == CHK_TYPE.ConnectionString)
                formatter = new ErsFormatConnectionString();

            else
                throw new Exception("未実装のチェック型" + type.ToString());

            if (isArray)
            {
                return new ErsFormatArrayDecorator(formatter);
            }
            else
            {
                return formatter;
            }
        }
    }
}
