﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.util;
using jp.co.ivp.ers.mvc.validation.range_check;

namespace jp.co.ivp.ers.mvc.validation.validator
{
    class ErsValidateDate
        : ErsValidatorBase
    {
        /// <summary>
        /// 全角を含むことができる場合True
		/// <para>Returns false</para>
        /// </summary>
        public override bool AllowFullString { get { return false; } }

        /// <summary>
        /// 半角を含むことができる場合True
		/// <para>Returns true</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool AllowHalfString { get { return true; } }

        /// <summary>
        /// 日付型チェック / Check date type
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="target"></param>
        /// <param name="DisplayName"></param>
        /// <returns></returns>
        public override bool CheckType(string target)
        {
            bool isValid = true;

            //1887年以前はタイムゾーンが異なってしまうので、システムとして受け付けない
            var underLimitDate = new DateTime(1900, 1, 1);

            DateTime time;
            if (!DateTime.TryParse(target, out time))
            {
                this.AppendError(ErsResources.GetMessage("10001", displayName));
                isValid = false;
            }
            else if (time < underLimitDate)
            {
                this.AppendError(ErsResources.GetMessage("10045", displayName, underLimitDate.ToString("yyyy/MM/dd")));
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// 禁則チェック(スルーされる)
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public override bool CheckProhibition(string target)
        {
            //HTMLは、文字チェックする際に禁則チェックを行うので、ここではチェックしない
            return true;
        }

        protected override range_check.IValueRangeChecker GetValueRangeChecker(EnumTextValueRangeChecker? rangeChecker)
        {
            if (rangeChecker.HasValue)
            {
                throw new Exception("CHK_TYPE.Date don't support specifying rangeChecker.");
            }

            return new DateValueRangeChecker();
        }
    }
}
