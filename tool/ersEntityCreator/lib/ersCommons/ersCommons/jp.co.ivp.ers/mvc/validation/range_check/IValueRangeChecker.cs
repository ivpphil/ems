﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jp.co.ivp.ers.mvc.validation.validator;

namespace jp.co.ivp.ers.mvc.validation.range_check
{
    public interface IValueRangeChecker
    {
        bool CheckRange(string argTarget, string argRangeFrom, string argRangeTo, ErsValidatorBase validator);

        string CutDown(string argTarget, string argRangeFrom, string argRangeTo);
    }
}
