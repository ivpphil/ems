﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using jp.co.ivp.ers.util;
using System.Web;
using System.Net;

namespace jp.co.ivp.ers.mvc
{
    public class ErsRequireHttpsAttribute
        : RequireHttpsAttribute
    {
        private bool enable;

        public ErsRequireHttpsAttribute()
            : this(true)
        {

        }

        public ErsRequireHttpsAttribute(bool enable)
        {
            this.enable = enable;
        }

        protected override void HandleNonHttpsRequest(AuthorizationContext filterContext)
        {
            if (new SetupConfigReader().enableSSL && this.enable)
            {
                throw new HttpException((int)HttpStatusCode.Forbidden, ErsResources.GetMessage("requireHttps"));
            }
        }
    }
}
