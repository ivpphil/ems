﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using jp.co.ivp.ers.util;
using System.Data;
using jp.co.ivp.ers.db;
using jp.co.ivp.ers;
using System.Web.Mvc;
using jp.co.ivp.ers.mvc.validation;
using System.Web;
using jp.co.ivp.ers.mvc.htmlExpand;
using jp.co.ivp.ers.mvc.MapperProcessor;

namespace jp.co.ivp.ers.mvc
{
    public abstract class ErsModelBase
         : IValidatableObject, IErsModelBase
    {
        Type t;
        public ErsModelBase()
        {
            this.OutputHidden = new Dictionary<string, bool>();
            this.InvalidValues = new Dictionary<string, object>();
            t = GetType();
            lineNumber = -1;
        }

        /// <summary>
        /// コントローラーへの参照
        /// </summary>
        public virtual ErsControllerBase controller { get; internal set; }

        /// <summary>
        /// 親モデルインスタンス
        /// </summary>
        public IErsModelBase containerModel { get; set; }

        /// <summary>
        /// 行番号/Line Number
        /// </summary>
        public virtual int lineNumber { get; set; }

        /// <summary>
        /// 行名 / Row Names
        /// </summary>
        public virtual string lineName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 検証結果 / Verification result
        /// </summary>
        public virtual bool IsValid
        {
            get
            {
                return this.listInvalidField.Count == 0;
            }
        }

        /// <summary>
        ///  不正入力された値が入る（View表示用）
        /// </summary>
        public Dictionary<string, object> InvalidValues { get; protected internal set; }

        /// <summary>
        /// 不正な値をClearする。（表示させたくない時用）
        /// </summary>
        public virtual void ClearInvalidValues()
        {
            var newDictionary = new Dictionary<string, object>();
            foreach (var key in this.InvalidValues.Keys)
            {
                newDictionary[key] = null;
            }
            this.InvalidValues = newDictionary;
        }

        /// <summary>
        /// 戻り画面無し設定
        /// (true:戻り無し)
        /// </summary>
        public virtual bool isNoBackTo { get { return (this.checkReferer() == false || this._isNotBackTo); } }

        internal bool _isNotBackTo { get; set; }

        /// <summary>
        /// referer存在
        /// (true:referer存在)
        /// </summary>
        /// <returns></returns>
        protected virtual bool checkReferer()
        {
            if (ErsCommonContext.IsBatch)
            {
                return false;
            }

            if (HttpContext.Current == null)
            {
                //別スレッドなどで参照できない場合あり
                return false;
            }

            var referer = HttpContext.Current.Request.ServerVariables["HTTP_REFERER"];

            // リファラが取得出来なければfalse
            if (string.IsNullOrEmpty(referer))
            {
                return false;
            }
            return true;
        }

        protected internal Dictionary<string, bool> OutputHidden { get; set; }

       /// <summary>
       /// 入力値検証後の複合チェックを行う。
	   /// <para>After checking composite input validation</para>
       /// </summary>
       /// <param name="validationContext"></param>
       /// <returns></returns>
        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            yield break;
        }

        /// <summary>
        /// 入力値検証後の複合チェックを行う。(CSV/TableBind用)
		/// <para>After checking composite input validation. (For CSV / TableBind)</para>
        /// </summary>
        /// <param name="lineName"></param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> Validate()
        {
            yield break;
        }
 
        /// <summary>
        /// ErsOutputHidden属性がついたプロパティをすべて出力する
		/// <para>To print all the property that has the attribute ErsOutputHidden</para>
        /// </summary>
        /// <param name="model"></param>
        public void SetOutputHidden(bool value)
        {
            this.SetOutputHidden(ErsOutputHiddenAttribute.DEFAULT_GROUP_NAME, value);
        }

        /// <summary>
        /// 指定したグループ名を持つErsOutputHidden属性がついたプロパティを出力する
		/// <para>To print all the property that has the attribute ErsOutputHidden with specified group</para>
        /// </summary>
        /// <param name="model"></param>
        public void SetOutputHidden(string groupName, bool value)
        {
            this.OutputHidden[groupName] = value;

            //BindTableもセット
            var t = this.GetType();

            foreach (var property in t.GetProperties())
            {
                var attributes = property.GetCustomAttributes(typeof(BindTableAttribute), false);
                if (attributes.Count() != 0)
                {
                    var bindValue = property.GetValue(this, null);
                    ((BindTableAttribute)attributes[0]).SetOutputHidden((System.Collections.IEnumerable)bindValue, groupName, value);
                }
            }
        }

        /// <summary>
        /// public propertyをDictionaryにセットして戻す
		/// set public properties to a Dictionary
        /// </summary>
        /// <returns></returns>
        public virtual Dictionary<string, object> GetPropertiesAsDictionary()
        {

            return ErsReflection.GetPropertiesAsDictionary(this);
           
        }

        /// <summary>
        /// 指定した属性をもつpublic propertyをDictionaryにセットして戻す
		/// <para>Set the Dictionary to return public property with the specified attribute</para>
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> GetPropertiesAsDictionary(Type attr)
        {
            var bindingFlag = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;

            var dicModel = ErsReflection.GetPropertiesAsDictionary(this, bindingFlag, null, attr);

            return dicModel;
        }

        /// <summary>
        /// public propertyをDictionaryにセットして戻す
        /// set public properties to a Dictionary
        /// </summary>
        /// <returns></returns>
        public virtual Dictionary<string, object> GetPropertiesAsDictionary(string bindTargetName)
        {

            return ErsReflection.GetPropertiesAsDictionary(this, BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static, bindTargetName);

        }

        /// <summary>
        /// ModelをDictionaryの値で上書きする
		/// <para>Overwritten by the values ​​of a Dictionary Model</para>
        /// </summary>
        /// <param name="model"></param>
        public virtual void OverwriteWithParameter(IDictionary<string, object> dictionary)
        {
            var dic = ErsCommon.OverwriteDictionary(this.GetPropertiesAsDictionary(), dictionary);
            ErsReflection.SetPropertyAll(this, dictionary);
        }

        /// <summary>
        /// ModelをDictionaryの値で上書きする
		/// <para>Overwritten by the values ​​of a Dictionary Model</para>
        /// </summary>
        /// <param name="model"></param>
        public virtual void OverwriteWithParameter(IDictionary<string, object> dictionary, string bindTargetName)
        {
            var dic = ErsCommon.OverwriteDictionary(this.GetPropertiesAsDictionary(), dictionary);
            ErsReflection.SetPropertyAll(this, dictionary, bindTargetName);
        }

        /// <summary>
        /// 追加の必須チェック。Validateメソッド内で呼ぶ。
		/// <para>Check additional required field called in the Validate method</para>
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public ValidationResult CheckRequired(string displayName, string propertyName, object val)
        {
            if (!this.IsValidField(propertyName))
            {
                return null;
            }

            var result = true;
            if (val == null)
            {
                result = false;
            }
            else if (val is HttpPostedFileBase && ((HttpPostedFileBase)val).ContentLength == 0)
            {
                result = false;
            }
            else if (val is string && string.IsNullOrEmpty((string)val))
            {
                result = false;
            }

            if (!result)
            {
                string ret = ErsResources.GetMessage("10000", displayName);
                return new ValidationResult(ret, new[] { propertyName });
            }
            return null;
        }

        /// <summary>
        /// 追加の必須チェック。Validateメソッド内で呼ぶ。
		/// <para>Check additional required field called in the Validate method</para>
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public ValidationResult CheckRequired(string lineName, string propertyName)
        {
            if (!this.IsValidField(propertyName))
            {
                return null;
            }

            var property = GetProperty(propertyName);
            var attributes = property.GetCustomAttributes(true).OfType<Attribute>();
            var FieldNameKey = ErsResources.GetDisplayName(attributes);
            if (string.IsNullOrEmpty(FieldNameKey))
            {
                var validators = ErsBindModel.GetValidators(attributes);
                foreach (var validator in validators)
                {
                    var schemaValidator = validator as ErsSchemaValidationAttribute;
                    if (schemaValidator != null)
                    {
                        FieldNameKey = schemaValidator.displayNameKey;
                        break;
                    }
                }
                if (string.IsNullOrEmpty(FieldNameKey))
                    FieldNameKey = property.Name;
            }
            var displayName = ErsResources.GetFieldName(FieldNameKey);
            object val = property.GetValue(this, null);
            return CheckRequired(displayName, propertyName, val);

        }

        /// <summary>
        /// 追加の必須チェック。Validateメソッド内で呼ぶ。
		/// <para>Check additional required field called in the Validate method</para>
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public ValidationResult CheckRequired(string propertyName)
        {
            //エラーの無い項目だけ必須チェック
            if (this.IsValidField(propertyName))
            {
                return this.CheckRequired(string.Empty, propertyName);
            }

            return null;

        }

        /// <summary>
        /// PropertyInfoオブジェクトを取得する。
		/// <para>Get PropertyInfo object</para>
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        protected PropertyInfo GetProperty(string propertyName)
        {
            PropertyInfo property = t.GetProperty(propertyName);

            if (property == null)
                throw new Exception("Error occured in CheckRequired method : " + propertyName + " is not difined in " + t.Name + " class ");

            return property;
        }

        /// <summary>
        /// OutputHidden属性のついたプロパティを出力する。
		/// <para>Returns the properties with attributes OutputHidden.</para>
        /// </summary>
        /// <param name="list"></param>
        internal protected virtual void GetOutputHidden(List<ErsOutputHiddenTarget> listTarget)
        {
            var t = this.GetType();

            foreach (var property in t.GetProperties())
            {
                IHtmlBinding binding;
                var attributes = property.GetCustomAttributes(typeof(IHtmlBinding), false);
                if (attributes.Count() != 0)
                {
                    binding = (IHtmlBinding)attributes.First();
                }
                else
                {
                    binding = new HtmlDefaultBinding();
                }

                Func<object> valueFunc = () => property.GetValue(this, null);
                binding.GetOutputHidden(listTarget, property, string.Empty, property.Name, valueFunc, this.OutputHidden);
            }
        }

        /// <summary>
        /// 指定されたフィールドが正しいか
        /// </summary>
        /// <param name="fieldName"></param>
        public bool IsValidField(params string[] fieldName)
        {
            if (fieldName == null)
            {
                return false;
            }

            foreach (var name in fieldName)
            {
                if (this.listInvalidField.ContainsKey(name))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// エラーありフィールド名のリスト
		/// <para>List of invalid fields</para>
        /// </summary>
        protected Dictionary<string, List<string>> listInvalidField = new Dictionary<string, List<string>>();

        /// <summary>
        /// エラーありフィールド名のリストに追加する。
		/// <para>Add invalid field to list of ivalid fields</para>
        /// </summary>
        /// <param name="fieldName"></param>
        public void AddInvalidField(string fieldName, string errorMessage)
        {
            if (this.listInvalidField.ContainsKey(fieldName))
            {
                if (!this.listInvalidField[fieldName].Contains(errorMessage))
                {
                    this.listInvalidField[fieldName].Add(errorMessage);
                }
            }
            else
            {
                var list = new List<string>();
                list.Add(errorMessage);
                this.listInvalidField.Add(fieldName, list);
            }
        }

        /// <summary>
        /// 検証結果をロードします
        /// </summary>
        /// <param name="listValidationResult"></param>
        public void AddInvalidField(IEnumerable<ValidationResult> listValidationResult)
        {
            if (listValidationResult == null)
            {
                return;
            }

            foreach (var validationResult in listValidationResult)
            {
                this.AddInvalidField(validationResult);
            }
        }


        /// <summary>
        /// 検証結果をロードします
        /// </summary>
        /// <param name="listValidationResult"></param>
        public void AddInvalidField(ValidationResult validationResult)
        {
            if (validationResult == null)
            {
                return;
            }

            if (validationResult.MemberNames.Count() == 0)
            {
                this.AddInvalidField("", validationResult.ErrorMessage);
            }
            else
            {
                foreach (var name in validationResult.MemberNames)
                {
                    this.AddInvalidField(name, validationResult.ErrorMessage);
                }
            }
        }

        /// <summary>
        /// エラーありフィールド名のリストから削除する。
		/// <para>Remove from the list of invalid field names</para>
        /// </summary>
        /// <param name="fieldName"></param>
        public void RemoveInvalidField(string fieldName)
        {
            if (this.listInvalidField.ContainsKey(fieldName))
            {
                this.listInvalidField.Remove(fieldName);
            }
        }

        /// <summary>
        /// エラーありフィールド名のリストを取得する
        /// </summary>
        public virtual IEnumerable<string> GetInvalidFieldNames()
        {
            return new List<string>(this.listInvalidField.Keys);
        }

        /// <summary>
        /// 不正な値をモデルから削除する
        /// </summary>
        public void ClearInvalidFields()
        {
            foreach (var key in this.GetInvalidFieldNames())
            {
                this.ClearInvalidField(key);
            }
        }

        /// <summary>
        /// 指定されたプロパティの不正な値を削除する。
        /// </summary>
        /// <param name="key"></param>
        public void ClearInvalidField(string key)
        {
            var prop = t.GetProperty(key);
            if (prop != null)
            {
                if (this.listInvalidField.ContainsKey(key))
                {
                    this.RemoveInvalidField(key);

                    var type = prop.GetType();
                    var defaultValue = type.IsValueType ? Activator.CreateInstance(type) : null;
                    if (prop.CanWrite)
                    {
                        prop.SetValue(this, defaultValue, null);
                    }
                }
            }
        }

        /// <summary>
        /// 全てのエラーメッセージをまとめて取得する。
        /// </summary>
        /// <returns></returns>
        public virtual List<string> GetAllErrorMessageList(string parentLineName = "")
        {
            if (!string.IsNullOrEmpty(parentLineName))
            {
                parentLineName += " ";
            }

            var lineName = parentLineName + this.lineName;

            List<string> allMessage = new List<string>();//全部のエラー文字列を連結

            var t = this.GetType();

            //モデルの項目にあるエラーを列挙
            foreach (var property in t.GetProperties())
            {
                if (property.GetCustomAttributes(typeof(BindTableAttribute), false).Length > 0)
                {
                    var bindTable = property.GetValue(this, null) as IEnumerable<ErsBindableModel>;
                    if (bindTable != null)
                    {
                        foreach (var bindModel in bindTable)
                        {
                            var errorMessageList = bindModel.GetAllErrorMessageList(lineName);
                            AddErrorList(allMessage, errorMessageList);
                        }
                    }
                }
                
                if (!this.IsValidField(property.Name))
                {
                    var errorMessageList = this.GetFieldErrorMessageList(property.Name, lineName);
                    AddErrorList(allMessage, errorMessageList);
                }
            }

            //モデルの項目にないエラーを列挙
            foreach (var errorInfo in this.listInvalidField)
            {
                if (t.GetProperty(errorInfo.Key) == null)
                {
                    var errorMessageList = this.GetFieldErrorMessageList(errorInfo.Key, lineName);

                    if (errorMessageList != null)
                    {
                        foreach (var errorMessage in errorMessageList)
                        {
                            if (!allMessage.Contains(errorMessage) && errorInfo.Key != "exception_detail")
                                allMessage.Add(errorMessage);
                        }
                    }
                }
            }

            return allMessage;
        }

        /// <summary>
        /// エラーのリストを、１つのリストにマージする
        /// </summary>
        /// <param name="allMessage"></param>
        /// <param name="errorMessageList"></param>
        private static void AddErrorList(List<string> allMessage, List<string> errorMessageList)
        {
            if (errorMessageList != null && errorMessageList.Count > 0)
            {
                foreach (var errorMessage in errorMessageList)
                {
                    //同じエラーがある場合は、後者を採用する。
                    if (allMessage.Contains(errorMessage))
                    {
                        allMessage.Remove(errorMessage);
                    }
                    allMessage.Add(errorMessage);
                }
            }
        }

        /// <summary>
        /// プロパティのエラーを取得する。/ Gets the error of the property
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public virtual List<string> GetFieldErrorMessageList(string propertyName, string prefix = null)
        {
            if (!this.listInvalidField.ContainsKey(propertyName))
            {
                return null;
            }
            else
            {
                if (!string.IsNullOrEmpty(prefix))
                {
                    prefix += " ";
                }

                var newList = new List<string>();
                foreach (var errorMessage in this.listInvalidField[propertyName])
                {
                    newList.Add(prefix + errorMessage);
                }
                return newList;
            }
        }

        /// <summary>
        /// プロパティのエラーを取得する。/ Gets the error of the property
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public virtual string GetFieldErrorMessage(string propertyName, string prefix = null)
        {
            var list = this.GetFieldErrorMessageList(propertyName, prefix);

            if (list != null)
            {
                return String.Join(ErsViewHelper.TAG_NEW_LINE, list) + ErsViewHelper.TAG_NEW_LINE;
            }

            return null;
        }

        /// <summary>
        /// インスタンスのクローンを生成します。浅いコピー
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
