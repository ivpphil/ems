using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using System.Xml.Xsl;
using System.Xml.Linq;
using System.IO;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Collections;
using System.Text.RegularExpressions;
using jp.co.ivp.ers.util;
using System.Text;
using jp.co.ivp.ers.mvc.compile;
using jp.co.ivp.ers.mvc.template;
using System.Web.Configuration;

namespace jp.co.ivp.ers.mvc
{
    public class ErsViewShared : ErsViewBase
    {
        /// <summary>
        /// ErsViewContextでViewを初期化
        /// </summary>
        /// <param name="path"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual ErsViewShared Init(string filePath, ErsViewContext context, EnumSiteType site_type)
        {
            this.site_type = site_type;
            this.context = context;

            var rootPath = SiteTypeVariables.GetApplicationRoot(site_type);
            if (filePath.HasValue())
            {
                this._path = new Uri(new Uri(Path.Combine(rootPath, "Views/Partial/")), filePath).LocalPath;
            }
            this.enc = this.GetViewContextEncoding(this.site_type);
            return this;
        }

        /// <summary>
        /// テンプレートを出力する（Partial用）
        /// </summary>
        /// <param name="viewContext"></param>
        /// <param name="writer"></param>
        public virtual void Render(ViewDataDictionary viewContext, System.IO.TextWriter writer, object partialModel)
        {
            if (!this.FileExists())
            {
                return;
            }

            var newViewContext = new ViewDataDictionary(viewContext);
            this.GetStatic(newViewContext);

            var func = GetFunction();
            func(writer, context, newViewContext, partialModel);
            Console.WriteLine(writer);
        }

        private bool FileExists()
        {
            string filePath = ErsCommonContext.MapPath(this._path);
            var extension = Path.GetExtension(filePath);

            //HTMLファイルでない、または存在しないファイルは表示しない
            if ((extension != ".htm" && extension != ".html") || !File.Exists(filePath))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 設定ファイル値をViewDataにセットする
        /// <para>ViewData configuration file is set to the value</para>
        /// </summary>
        /// <param name="viewData"></param>
        protected override void GetStatic(IDictionary<string, Object> viewData)
        {
            viewData["nor_url"] = SiteTypeVariables.GetNorUrl(this.site_type);

            viewData["sec_url"] = SiteTypeVariables.GetSecUrl(this.site_type);

            if (!ErsCommonContext.IsBatch)
            {
                viewData["dynamic_url"] = (HttpContext.Current.Request.IsSecureConnection) ? viewData["sec_url"] : viewData["nor_url"];
                viewData["pc_dynamic_url"] = (HttpContext.Current.Request.IsSecureConnection) ? viewData["pc_sec_url"] : viewData["pc_nor_url"];                
            }
        }
    }
}